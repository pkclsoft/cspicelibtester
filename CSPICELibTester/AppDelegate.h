//
//  AppDelegate.h
//  CSPICELibTester
//
//  Created by Peter Easdown on 24/8/18.
//  Copyright © 2018 PKCLsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

