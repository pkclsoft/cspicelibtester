//
//  sandbox_interface.h
//
//  Created by Peter Easdown on 24/8/18.
//

#ifndef sandbox_interface_h
#define sandbox_interface_h

#include <string.h>

void setDocumentsFolder(const char *str);

char *getDocumentsFolder();

int prependDocumentsFolder(char *file, int file_len);
void removeDocumentsFolder(char *file, int file_len);

/*!
 * Wrapper around the system remove() function that assumes that the file
 * exists within the documents folder.
 */
int removeFile(char *file);

/*!
 * Wrapper around the system rename() function that assumes that the file
 * exists within the documents folder.
 */
int renameFile(char *file, char* toFile);

#endif /* sandbox_interface_h */
