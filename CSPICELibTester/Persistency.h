//
//  Persistency.h
//
//  Provides a wrapper around the "Documents" folder within the app file space.  Essentially,
//  all saved documents used by the app are stored there.  Apart from anything else, this allows
//  iTunes integration.
//
//  Created by Peter Easdown on 23/04/11.
//  Copyright 2011 PKCLsoft. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Persistency : NSObject

/*!
 *  Returns the path of the folder within the app that contains all documents.
 */
+ (NSString *) documentPath;

/*!
 *  Returns a path for the given filename within [self documentPath].
 */
+ (NSString *) pathForFilename:(NSString*)filename;

/*!
 *  Returns an array of NSString objects, where each contains the pathname to a single
 *  document within the folder specified by [self documentPath] with the specified extension.
 */
+ (NSArray*) getAllPathnamesWithExtension:(NSString*)extension;

/*!
 *  Removes the file at the specified path.  A sanity check is made to ensure that the path of
 *  the file is within the path specified by [self documentPath].
 */
+ (void) removeDocumentAtPath:(NSString*)documentPath;

/*!
 * Removes all files from the documents folder.
 */
+ (void) removeAllDocuments;

/*!
 *  Writes the specified file data to the local filesystem.
 */
+ (void) writeData:(NSData*)data toFilename:(NSString*)filename;

@end
