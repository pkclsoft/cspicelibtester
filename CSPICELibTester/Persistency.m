//
//
//  Created by Peter Easdown on 23/04/11.
//

#import "Persistency.h"
#if TARGET_OS_IPHONE
#else
#endif

@implementation Persistency

/*!
 *  Returns the path of the folder within the app that contains all documents.
 */
+ (NSString *) documentPath {

#if TARGET_OS_TV
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
#else
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
#endif
}

/*!
 *  Returns a path for the given filename within [self documentPath].
 */
+ (NSString *) pathForFilename:(NSString*)filename {
    return [[Persistency documentPath] stringByAppendingPathComponent:filename];
}

/*!
 *  Returns an array of NSString objects, where each contains the pathname to a single
 *  document within the folder specified by [self documentPath] with the specified extension.
 */
+ (NSArray*) getAllPathnamesWithExtension:(NSString*)extension {
    NSError *fError;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[Persistency documentPath] error:&fError];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (NSString *file in files) {
        NSLog(@"file found: %@", file);
        if ([[file pathExtension] isEqualToString:extension]) {
            [result addObject:[[Persistency documentPath] stringByAppendingPathComponent:file]];
        }
    }
    
    return result;
}

/*!
 *  Removes the file at the specified path.  A sanity check is made to ensure that the path of
 *  the file is within the path specified by [self documentPath].
 */
+ (void) removeDocumentAtPath:(NSString*)documentPath {
    if ([documentPath hasPrefix:[Persistency documentPath]] == YES) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:documentPath error:&error];
        
        if (error != nil) {
            NSLog(@"Attempt to remove file (%@) failed with error: %@", documentPath, [error description]);
        }
    } else {
        NSLog(@"Attempt to remove file in the wrong location: %@", documentPath);
    }
}

/*!
 * Removes all files from the documents folder.
 */
+ (void) removeAllDocuments {
    NSError *fError;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[Persistency documentPath] error:&fError];

    for (NSString *file in files) {
        [Persistency removeDocumentAtPath:[Persistency pathForFilename:file]];
    }
}


/*!
 *  Writes the specified file data to the local filesystem.
 */
+ (void) writeData:(NSData*)data toFilename:(NSString*)filename {
    NSString *name = [filename stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *path = [Persistency pathForFilename:name];
    [data writeToFile:path atomically:YES];
    NSLog(@"Wrote to: %@", path);
}

@end
