/* f_aaaaphsh.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__11 = 11;
static integer c_n1 = -1;
static logical c_true = TRUE_;
static integer c__749 = 749;
static integer c__1583 = 1583;

/* $Procedure F_AAAAPHSH ( Family of tests for ZZPHSH ) */
/* Subroutine */ int f_aaaaphsh__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    integer m2;
    extern /* Subroutine */ int t_success__(logical *), chckxc_(logical *, 
	    char *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    integer maxvar;
    extern integer intmax_(void), zzhash_(char *, ftnlen);
    integer tmpint;
    extern integer zzphsh_(char *, integer *, integer *, ftnlen);
    extern /* Subroutine */ int szpool_(char *, integer *, logical *, ftnlen);
    extern integer zzshsh_(integer *), zzhash2_(char *, integer *, ftnlen);

/* $ Abstract */

/*     This routine tests the POOL and arbitrary divisor hash functions */
/*     provided by ZZPHSH umbrella. It is named F_AAAAPHSH to ensure */
/*     given the current TSPICE main module assembly process, it is the */
/*     first test family executed by TSPICE. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     None */

/* $ Version */

/* -    Version 1.0.0  31-JUL-2013 (BVS) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_AAAAPHSH", (ftnlen)10);

/*     NO-OP: calling ZZPHSH. */

    tcase_("Call ZZPHSH umbrella directly.", (ftnlen)30);
    tmpint = zzphsh_(" ", &c__1, &c__1, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZPHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    tmpint = zzphsh_("BOO", &c__11, &c__11, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZPHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     The test below cannot be done because CSTART called by TOPEN */
/*     already initialized the POOL via CLPOOL that called ZZSHSH. */

/*     ERROR: calling ZZHASH prior to ZZSHSH, try 1. */

/*      CALL TCASE ( 'Call ZZHASH prior to ZZSHSH, try 1.' ) */

/*      TMPINT = ZZHASH ( ' ' ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(CALLEDOUTOFORDER)', OK ) */
/*      CALL CHCKSI ( 'ZZHASH', TMPINT, '=', 0, 0, OK ) */

/*     ERROR: calling ZZSHSH with bad divisors. */

    tcase_("Call ZZSHSH with invalid divisors", (ftnlen)33);
    tmpint = zzshsh_(&c_n1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZSHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    tmpint = zzshsh_(&c__0);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZSHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = intmax_();
    tmpint = zzshsh_(&i__1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZSHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = intmax_() / 68;
    tmpint = zzshsh_(&i__1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZSHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     The test below cannot be done because CSTART called by TOPEN */
/*     already initialized the POOL via CLPOOL that called ZZSHSH. */

/*     ERROR: calling ZZHASH prior to ZZSHSH, try 2. Although */
/*     ZZSHSH was already called, it never got to save the */
/*     POOL hash divisor. */

/*      CALL TCASE ( 'Call ZZHASH prior to ZZSHSH, try 2.' ) */

/*      TMPINT = ZZHASH ( ' ' ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(CALLEDOUTOFORDER)', OK ) */
/*      CALL CHCKSI ( 'ZZHASH', TMPINT, '=', 0, 0, OK ) */

/*     ERROR: calling ZZHASH2 with bad divisors. */

    tcase_("Call ZZHASH2 with invalid divisors", (ftnlen)34);
    tmpint = zzhash2_(" ", &c_n1, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZHASH2", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)7, (ftnlen)1);
    tmpint = zzhash2_(" ", &c__0, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZHASH2", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)7, (ftnlen)1);
    i__1 = intmax_();
    tmpint = zzhash2_(" ", &i__1, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZHASH2", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)7, (ftnlen)1);
    i__1 = intmax_() / 68;
    tmpint = zzhash2_(" ", &i__1, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("ZZHASH2", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)7, (ftnlen)1);

/*     The test below cannot be done because CSTART called by TOPEN */
/*     already initialized the POOL via CLPOOL that called ZZSHSH. */

/*     ERROR: calling ZZHASH prior to ZZSHSH, try 3. */

/*      CALL TCASE ( 'Call ZZHASH prior to ZZSHSH, try 3.' ) */

/*      TMPINT = ZZHASH ( ' ' ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(CALLEDOUTOFORDER)', OK ) */
/*      CALL CHCKSI ( 'ZZHASH', TMPINT, '=', 0, 0, OK ) */

/*     GOOD: check a few ZZHASH2 values. */

    tcase_("Call ZZHASH2 with a few good values.", (ftnlen)36);
    m2 = 1;
    tmpint = zzhash2_(" ", &m2, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__1, &c__0, ok, (ftnlen)7, (ftnlen)1);
    tmpint = zzhash2_("Hakuna-Matata!", &m2, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__1, &c__0, ok, (ftnlen)7, (ftnlen)1);
    m2 = 5003;
    tmpint = zzhash2_("A", &m2, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)7, (ftnlen)1)
	    ;
    tmpint = zzhash2_("A A", &m2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)7, (ftnlen)1)
	    ;
    tmpint = zzhash2_("a A", &m2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)7, (ftnlen)1)
	    ;
    tmpint = zzhash2_("AA", &m2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__1583, &c__0, ok, (ftnlen)7, (ftnlen)
	    1);
    tmpint = zzhash2_("AA AA", &m2, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__1583, &c__0, ok, (ftnlen)7, (ftnlen)
	    1);
    tmpint = zzhash2_("aa AA", &m2, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH2", &tmpint, "=", &c__1583, &c__0, ok, (ftnlen)7, (ftnlen)
	    1);

/*     The test below cannot be done because CSTART called by TOPEN */
/*     already initialized the POOL via CLPOOL that called ZZSHSH. */

/*     ERROR: calling ZZHASH prior to ZZSHSH, try 4. Although */
/*     ZZHASH2 initialized character-code table, the POOL */
/*     hash divisor is still uninitialized. */

/*      CALL TCASE ( 'Call ZZHASH prior to ZZSHSH, try 4.' ) */

/*      TMPINT = ZZHASH ( ' ' ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(CALLEDOUTOFORDER)', OK ) */
/*      CALL CHCKSI ( 'ZZHASH', TMPINT, '=', 0, 0, OK ) */

/*     GOOD: finally call ZZSHSH. Use MAXVAR of the POOL for */
/*     consistency. */

    tcase_("Call ZZSHSH with a good divisor.", (ftnlen)32);
    szpool_("MAXVAR", &maxvar, &found, (ftnlen)6);
    chcksl_("POOL MAXVAR FOUND", &found, &c_true, ok, (ftnlen)17);
    tmpint = zzshsh_(&maxvar);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZSHSH", &tmpint, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     GOOD: check a few ZZHASH values. */

    tcase_("Call ZZHASH with a few good values.", (ftnlen)35);
    tmpint = zzhash_("A", (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)6, (ftnlen)1);
    tmpint = zzhash_("A A", (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)6, (ftnlen)1);
    tmpint = zzhash_("a A", (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZHASH", &tmpint, "=", &c__749, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_aaaaphsh__ */

