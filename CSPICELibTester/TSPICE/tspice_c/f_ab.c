/* f_ab.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__100 = 100;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__300 = 300;
static logical c_false = FALSE_;
static integer c_n1 = -1;
static integer c__1 = 1;
static integer c__101 = 101;
static integer c__200 = 200;

/* $Procedure      F_AB ( Test DSK AB list routines ) */
/* Subroutine */ int f_ab__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer bval, xval;
    extern /* Subroutine */ int zzaddlnk_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), zzinilnk_(integer *, 
	    integer *, integer *, integer *, integer *);
    static integer i__, j, k;
    extern /* Subroutine */ int zzuntngl_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *);
    static integer n;
    static char label[80];
    extern /* Subroutine */ int zztrvlnk_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *);
    static integer ncell;
    extern /* Subroutine */ int tcase_(char *, ftnlen), filli_(integer *, 
	    integer *, integer *);
    static integer cells[600]	/* was [2][300] */, avals[3], alist__[100], 
	    blist[200];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static integer xvals[300];
    extern /* Subroutine */ int t_success__(logical *);
    static integer nb;
    extern /* Subroutine */ int chckai_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chckxc_(logical *, char *, 
	    logical *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);

/* $ Abstract */

/*     Test the AB list routines used by the DSK subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private AB list routines */

/*        ZZADDLNK */
/*        ZZINILNK */
/*        ZZTRVLNK */
/*        ZZUNTNGL */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     This routine currently does not exercise DSKX02. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 06-JAN-2016  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Local Variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_AB", (ftnlen)4);
/* *********************************************************************** */

/*     ZZINILNK tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Bad cell count", (ftnlen)14);
    zzinilnk_(&c__100, &c__0, &ncell, alist__, cells);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad pointer count", (ftnlen)17);
    zzinilnk_(&c__0, &c__300, &ncell, alist__, cells);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Initialize an AB list", (ftnlen)21);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The A list should have all entries initialized to "null." */

    filli_(&c_n1, &c__100, xvals);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("ALIST", alist__, "=", xvals, &c__100, ok, (ftnlen)5, (ftnlen)1);

/*     The cell array should have all value elements set to 0. */

    for (i__ = 1; i__ <= 300; ++i__) {
	s_copy(label, "CELLS(1,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 2) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)223)], "=", &
		c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/*     The cell array should have all pointer elements set to NULPTR. */

    for (i__ = 1; i__ <= 300; ++i__) {
	s_copy(label, "CELLS(2,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 1) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)236)], "=", &
		c_n1, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/*     The pointer array should have all pointer elements set to NULPTR. */

    for (i__ = 1; i__ <= 100; ++i__) {
	s_copy(label, "ALIST(@)", (ftnlen)80, (ftnlen)8);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &alist__[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("alist", i__1, "f_ab__", (ftnlen)249)], "=", &c_n1, &
		c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/*     The used cell count should be zero on output. */

    chcksi_("NCELL", &ncell, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);
/* *********************************************************************** */

/*     ZZADDLNK tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("AVAL out of range", (ftnlen)17);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzaddlnk_(&c__0, &c__1, &c__100, &c__300, alist__, &ncell, cells);
    chckxc_(&c_true, "SPICE(AVALOUTOFRANGE)", ok, (ftnlen)21);
    zzaddlnk_(&c_n1, &c__1, &c__100, &c__300, alist__, &ncell, cells);
    chckxc_(&c_true, "SPICE(AVALOUTOFRANGE)", ok, (ftnlen)21);
    zzaddlnk_(&c__101, &c__1, &c__100, &c__300, alist__, &ncell, cells);
    chckxc_(&c_true, "SPICE(AVALOUTOFRANGE)", ok, (ftnlen)21);

/* --- Case -------------------------------------------------------- */

    tcase_("Too many cell entries.", (ftnlen)22);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 300; ++i__) {
	zzaddlnk_(&c__1, &i__, &c__100, &c__300, alist__, &ncell, cells);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = i__ + 1;
    zzaddlnk_(&c__1, &i__1, &c__100, &c__300, alist__, &ncell, cells);
    chckxc_(&c_true, "SPICE(CELLARRAYTOOSMALL)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Build trivial list for one A-value.", (ftnlen)35);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 300; ++i__) {
	zzaddlnk_(&c__1, &i__, &c__100, &c__300, alist__, &ncell, cells);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check NCELL. */

    chcksi_("NCELL", &ncell, "=", &c__300, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the pointer array. The first pointer should point to */
/*     the last cell; the other pointers should be null. */

    chcksi_("ALIST(1)", alist__, "=", &c__300, &c__0, ok, (ftnlen)8, (ftnlen)
	    1);
    for (i__ = 2; i__ <= 100; ++i__) {
	s_copy(label, "ALIST(@)", (ftnlen)80, (ftnlen)8);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &alist__[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("alist", i__1, "f_ab__", (ftnlen)337)], "=", &c_n1, &
		c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/*     Check the cell entries. */

    for (i__ = 300; i__ >= 1; --i__) {
	s_copy(label, "CELLS(1,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 2) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)350)], "=", &
		i__, &c__0, ok, (ftnlen)80, (ftnlen)1);
	s_copy(label, "CELLS(2,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (i__ > 1) {
	    xval = i__ - 1;
	} else {
	    xval = -1;
	}
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 1) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)364)], "=", &
		xval, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Build a singleton list for each A-value.", (ftnlen)40);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 100; i__ >= 1; --i__) {
	i__1 = -i__;
	zzaddlnk_(&i__, &i__1, &c__100, &c__300, alist__, &ncell, cells);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check NCELL. */

    chcksi_("NCELL", &ncell, "=", &c__100, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the pointer array. The Ith pointer should point to */
/*     the cell at index MAXA+1-I. */

    for (i__ = 1; i__ <= 100; ++i__) {
	s_copy(label, "ALIST(@)", (ftnlen)80, (ftnlen)8);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = 101 - i__;
	chcksi_(label, &alist__[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("alist", i__1, "f_ab__", (ftnlen)400)], "=", &i__2, &
		c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/*     Check the cell entries. */

    for (i__ = 1; i__ <= 100; ++i__) {
	s_copy(label, "CELLS(1,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xval = -(101 - i__);
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 2) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)415)], "=", &
		xval, &c__0, ok, (ftnlen)80, (ftnlen)1);
	s_copy(label, "CELLS(2,@)", (ftnlen)80, (ftnlen)10);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xval = -1;
	chcksi_(label, &cells[(i__1 = (i__ << 1) - 1) < 600 && 0 <= i__1 ? 
		i__1 : s_rnge("cells", i__1, "f_ab__", (ftnlen)424)], "=", &
		xval, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Build a list for each of three A-values, where the list addition"
	    "s are interleaved.", (ftnlen)82);
    zzinilnk_(&c__100, &c__300, &ncell, alist__, cells);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    avals[0] = 4;
    avals[1] = 1;
    avals[2] = 30;
    n = 20;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    bval = j * 1000 + i__;
	    zzaddlnk_(&avals[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		    "avals", i__2, "f_ab__", (ftnlen)452)], &bval, &c__100, &
		    c__300, alist__, &ncell, cells);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Check NCELL. */

    i__1 = n * 3;
    chcksi_("NCELL", &ncell, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Recover the B values for each A value. */

    for (i__ = 1; i__ <= 3; ++i__) {
	j = alist__[(i__2 = avals[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("avals", i__1, "f_ab__", (ftnlen)471)] - 1) < 100 && 0 
		<= i__2 ? i__2 : s_rnge("alist", i__2, "f_ab__", (ftnlen)471)]
		;
	i__1 = n;
	for (k = 1; k <= i__1; ++k) {
	    bval = cells[(i__2 = (j << 1) - 2) < 600 && 0 <= i__2 ? i__2 : 
		    s_rnge("cells", i__2, "f_ab__", (ftnlen)475)];
	    xval = i__ * 1000 + (n + 1 - k);
	    s_copy(label, "BLIST(@) for AVAL @", (ftnlen)80, (ftnlen)19);
	    repmi_(label, "@", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &bval, "=", &xval, &c__0, ok, (ftnlen)80, (ftnlen)
		    1);
	    j = cells[(i__2 = (j << 1) - 1) < 600 && 0 <= i__2 ? i__2 : 
		    s_rnge("cells", i__2, "f_ab__", (ftnlen)487)];
	}
    }
/* *********************************************************************** */

/*     ZZTRVLNK tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Collect the B-lists for AVALS from the previous ZZADDLNK case.", (
	    ftnlen)62);
    for (i__ = 1; i__ <= 3; ++i__) {
	zztrvlnk_(&avals[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"avals", i__1, "f_ab__", (ftnlen)510)], &c__100, alist__, &
		c__300, cells, &c__200, &nb, blist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check size of returned list. */

	chcksi_("NB", &nb, "=", &n, &c__0, ok, (ftnlen)2, (ftnlen)1);

/*        Check the list itself. */

	i__1 = n;
	for (k = 1; k <= i__1; ++k) {
	    xval = i__ * 1000 + (n + 1 - k);
	    s_copy(label, "BLIST(@) for AVAL @", (ftnlen)80, (ftnlen)19);
	    repmi_(label, "@", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &blist[(i__2 = k - 1) < 200 && 0 <= i__2 ? i__2 : 
		    s_rnge("blist", i__2, "f_ab__", (ftnlen)531)], "=", &xval,
		     &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Pointer for AVAL is null.", (ftnlen)25);
    i__ = -1;
    zztrvlnk_(&c__1, &c__100, &i__, &c__300, cells, &c__200, &nb, blist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check size of returned list. */

    chcksi_("NB", &nb, "=", &c__0, &c__0, ok, (ftnlen)2, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Pointer for AVAL is out of range.", (ftnlen)33);
    i__ = -2;
    zztrvlnk_(&c__1, &c__100, &i__, &c__300, cells, &c__200, &nb, blist);
    chckxc_(&c_true, "SPICE(POINTEROUTOFRANGE)", ok, (ftnlen)24);
    i__ = 0;
    zztrvlnk_(&c__1, &c__100, &i__, &c__300, cells, &c__200, &nb, blist);
    chckxc_(&c_true, "SPICE(POINTEROUTOFRANGE)", ok, (ftnlen)24);
    i__ = 301;
    zztrvlnk_(&c__1, &c__100, &i__, &c__300, cells, &c__200, &nb, blist);
    chckxc_(&c_true, "SPICE(POINTEROUTOFRANGE)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("AVAL out of range.", (ftnlen)18);
    zztrvlnk_(&c__0, &c__100, alist__, &c__300, cells, &c__200, &nb, blist);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("MAXB out of range.", (ftnlen)18);
    zztrvlnk_(&c__1, &c__100, alist__, &c__300, cells, &c__0, &nb, blist);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);
/* --- Case -------------------------------------------------------- */

    tcase_("BLIST too large", (ftnlen)15);
    i__1 = n - 1;
    zztrvlnk_(avals, &c__100, alist__, &c__300, cells, &i__1, &nb, blist);
    chckxc_(&c_true, "SPICE(BARRAYTOOSMALL)", ok, (ftnlen)21);
/* *********************************************************************** */

/*     ZZUNTNGL tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Untangle the interleaved list structure from the last ZZADDLNK c"
	    "ase.", (ftnlen)68);
    zzuntngl_(&c__100, &c__300, cells, &c__200, alist__, &nb, blist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check size of returned array. */

    i__1 = (n + 1) * 3;
    chcksi_("NB", &nb, "=", &i__1, &c__0, ok, (ftnlen)2, (ftnlen)1);

/*     Check the array itself. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Let J be the start index of the Ith list. A count */
/*        should be present at that index. */

	j = alist__[(i__2 = avals[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("avals", i__1, "f_ab__", (ftnlen)636)] - 1) < 100 && 0 
		<= i__2 ? i__2 : s_rnge("alist", i__2, "f_ab__", (ftnlen)636)]
		;
	s_copy(label, "Count for AVAL @", (ftnlen)80, (ftnlen)16);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &blist[(i__1 = j - 1) < 200 && 0 <= i__1 ? i__1 : 
		s_rnge("blist", i__1, "f_ab__", (ftnlen)642)], "=", &n, &c__0,
		 ok, (ftnlen)80, (ftnlen)1);

/*        Check the B-values for the Ith list. */

	i__1 = n;
	for (k = 1; k <= i__1; ++k) {
	    xval = i__ * 1000 + (n + 1 - k);
	    s_copy(label, "BLIST(@) for AVAL @", (ftnlen)80, (ftnlen)19);
	    repmi_(label, "@", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &blist[(i__2 = j + k - 1) < 200 && 0 <= i__2 ? 
		    i__2 : s_rnge("blist", i__2, "f_ab__", (ftnlen)656)], 
		    "=", &xval, &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Output array size too small.", (ftnlen)28);
    zzuntngl_(&c__100, &c__300, cells, &c__1, alist__, &nb, blist);
    chckxc_(&c_true, "SPICE(BARRAYTOOSMALL)", ok, (ftnlen)21);
    t_success__(ok);
    return 0;
} /* f_ab__ */

