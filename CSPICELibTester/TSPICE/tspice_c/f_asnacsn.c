/* f_asnacsn.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static logical c_true = TRUE_;
static doublereal c_b8 = 1.;

/* $Procedure      F_ASNACSN (Family of tests for DASINE and DACOSN) */
/* Subroutine */ int f_asnacsn__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    , t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal dasine_(doublereal *, doublereal *), halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    extern doublereal dacosn_(doublereal *, doublereal *);
    doublereal tstang, tstarg, tol;

/* $ Abstract */

/*     This is a test routine for the arc sine and arc cosine */
/*     functions with argument bound checking. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Version */

/*     TSPICE Version 1.0.0 7-MAR-2006 (LSE) */
/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local variables. */


/*     Begin every test family with an open call. */

    topen_("F_ASNACSN", (ftnlen)9);

/*     Test for tolerance negative error. */

    tol = -1e-12;
    tcase_("Tolerance negative, error in DASINE", (ftnlen)35);
    tstang = dasine_(&c_b4, &tol);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    tcase_("Tolerance negative, error in DACOSN", (ftnlen)35);
    tstang = dacosn_(&c_b8, &tol);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Test for error when argument is too large */

    tol = 1e-12;
    tstarg = 1.00000000001;
    tcase_("Argument too large in DASINE.", (ftnlen)29);
    tstang = dasine_(&tstarg, &tol);
    chckxc_(&c_true, "SPICE(INPUTOUTOFBOUNDS)", ok, (ftnlen)23);
    tcase_("Is value = ASIN (1.0) in DASINE?)", (ftnlen)33);
    d__1 = halfpi_();
    chcksd_("TSTANG", &tstang, "=", &d__1, &c_b4, ok, (ftnlen)6, (ftnlen)1);
    tcase_("Argument too large in DACOSN.", (ftnlen)29);
    tstang = dacosn_(&tstarg, &tol);
    chckxc_(&c_true, "SPICE(INPUTOUTOFBOUNDS)", ok, (ftnlen)23);
    tcase_("Is value = ACOS (1.0) in DACOSN?)", (ftnlen)33);
    chcksd_("TSTANG", &tstang, "=", &c_b4, &c_b4, ok, (ftnlen)6, (ftnlen)1);

/*     Test for error when argument is too small */

    tol = 1e-12;
    tstarg = -1.00000000001;
    tcase_("Argument too small in DASINE.", (ftnlen)29);
    tstang = dasine_(&tstarg, &tol);
    chckxc_(&c_true, "SPICE(INPUTOUTOFBOUNDS)", ok, (ftnlen)23);
    tcase_("Is value = ASIN (-1.0) in DASINE?)", (ftnlen)34);
    d__1 = -halfpi_();
    chcksd_("TSTANG", &tstang, "=", &d__1, &c_b4, ok, (ftnlen)6, (ftnlen)1);
    tcase_("Argument too small in DACOSN.", (ftnlen)29);
    tstang = dacosn_(&tstarg, &tol);
    chckxc_(&c_true, "SPICE(INPUTOUTOFBOUNDS)", ok, (ftnlen)23);
    tcase_("Is value = ACOS (-1.0) in DACOSN?)", (ftnlen)34);
    d__1 = pi_();
    chcksd_("TSTANG", &tstang, "=", &d__1, &c_b4, ok, (ftnlen)6, (ftnlen)1);

/*     All done. */

    t_success__(ok);
    return 0;
} /* f_asnacsn__ */

