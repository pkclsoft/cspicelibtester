/* f_ccifrm.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__127 = 127;
static integer c__128 = 128;
static integer c__2 = 2;
static integer c__0 = 0;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1222 = 1222;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c_n1 = -1;
static integer c__4 = 4;
static integer c__5 = 5;
static integer c__399 = 399;
static integer c__10013 = 10013;

/* $Procedure      F_CCIFRM ( Family of tests for CCIFRM ) */
/* Subroutine */ int f_ccifrm__(logical *ok)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    char name__[32*127];
    integer cent, type__[127], i__, n;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    integer xcode, class__;
    char xname[32];
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    char title[80];
    integer xcent, nvals;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    integer idcode[127], bidids[128];
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), kclear_(void);
    integer frcode, bididx[128];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chckxc_(logical *, char *, 
	    logical *, ftnlen), ccifrm_(integer *, integer *, integer *, char 
	    *, integer *, logical *, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen);
    char frname[32];
    integer bidpol[134], centrd[127], center[127];
    char kvname[32];
    integer clssid, bnmidx[128], bidlst[128];
    char badtxt[80*100];
    integer bnmpol[134];
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen);
    char bnmnms[32*128];
    extern /* Subroutine */ int frinfo_(integer *, integer *, integer *, 
	    integer *, logical *);
    integer typeid[127], bnmlst[128];
    extern /* Subroutine */ int gnpool_(char *, integer *, integer *, integer 
	    *, char *, logical *, ftnlen, ftnlen), zzfdat_(integer *, integer 
	    *, char *, integer *, integer *, integer *, integer *, integer *, 
	    integer *, integer *, char *, integer *, integer *, integer *, 
	    integer *, integer *, ftnlen, ftnlen), lmpool_(char *, integer *, 
	    ftnlen), gcpool_(char *, integer *, integer *, integer *, char *, 
	    logical *, ftnlen, ftnlen);
    char frtext[80*1222];

/* $ Abstract */

/*     This test family checks out the entry point CCIFRM belonging */
/*     to the frame expert utility FRAMEX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None */

/* $ Keywords */

/*   None */

/* $ Declarations */
/* $ Abstract */

/*     This file contains the number of inertial reference */
/*     frames that are currently known by the SPICE toolkit */
/*     software. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of known inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of recognized inertial reference */
/*                frames.  This value is needed by both CHGIRF */
/*                ZZFDAT, and FRAMEX. */

/* $ Author_and_Institution */

/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Abstract */

/*     This file contains the number of non-inertial reference */
/*     frames that are currently built into the SPICE toolkit */
/*     software. */


/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of built-in non-inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of built-in non-inertial reference */
/*                frames.  This value is needed by both  ZZFDAT, and */
/*                FRAMEX. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     W.L. Taber      (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.6.0, 30-OCT-2014 (BVS) */

/*        Increased the number of non-inertial frames from 105 to 106 */
/*        in order to accomodate the following PCK based frame: */

/*           IAU_BENNU */

/* -    SPICELIB Version 1.5.0, 11-OCT-2011 (BVS) */

/*        Increased the number of non-inertial frames from 100 to 105 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CERES */
/*           IAU_PALLAS */
/*           IAU_LUTETIA */
/*           IAU_DAVIDA */
/*           IAU_STEINS */

/* -    SPICELIB Version 1.4.0, 11-MAY-2010 (BVS) */

/*        Increased the number of non-inertial frames from 96 to 100 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_BORRELLY */
/*           IAU_TEMPEL_1 */
/*           IAU_VESTA */
/*           IAU_ITOKAWA */

/* -    SPICELIB Version 1.3.0, 12-DEC-2002 (BVS) */

/*        Increased the number of non-inertial frames from 85 to 96 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CALLIRRHOE */
/*           IAU_THEMISTO */
/*           IAU_MAGACLITE */
/*           IAU_TAYGETE */
/*           IAU_CHALDENE */
/*           IAU_HARPALYKE */
/*           IAU_KALYKE */
/*           IAU_IOCASTE */
/*           IAU_ERINOME */
/*           IAU_ISONOE */
/*           IAU_PRAXIDIKE */

/* -    SPICELIB Version 1.2.0, 02-AUG-2002 (FST) */

/*        Increased the number of non-inertial frames from 81 to 85 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_PAN */
/*           IAU_GASPRA */
/*           IAU_IDA */
/*           IAU_EROS */

/* -    SPICELIB Version 1.1.0, 20-FEB-1997 (WLT) */

/*        Increased the number of non-inertial frames from 79 to 81 */
/*        in order to accomodate the following earth rotation */
/*        models: */

/*           ITRF93 */
/*           EARTH_FIXED */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Abstract */

/*     The parameters below form an enumerated list of the recognized */
/*     frame types.  They are: INERTL, PCK, CK, TK, DYN.  The meanings */
/*     are outlined below. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     INERTL      an inertial frame that is listed in the routine */
/*                 CHGIRF and that requires no external file to */
/*                 compute the transformation from or to any other */
/*                 inertial frame. */

/*     PCK         is a frame that is specified relative to some */
/*                 INERTL frame and that has an IAU model that */
/*                 may be retrieved from the PCK system via a call */
/*                 to the routine TISBOD. */

/*     CK          is a frame defined by a C-kernel. */

/*     TK          is a "text kernel" frame.  These frames are offset */
/*                 from their associated "relative" frames by a */
/*                 constant rotation. */

/*     DYN         is a "dynamic" frame.  These currently are */
/*                 parameterized, built-in frames where the full frame */
/*                 definition depends on parameters supplied via a */
/*                 frame kernel. */

/*     ALL         indicates any of the above classes. This parameter */
/*                 is used in APIs that fetch information about frames */
/*                 of a specified class. */


/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 4.0.0, 08-MAY-2012 (NJB) */

/*       The parameter ALL was added to support frame fetch APIs. */

/* -    SPICELIB Version 3.0.0, 28-MAY-2004 (NJB) */

/*       The parameter DYN was added to support the dynamic frame class. */

/* -    SPICELIB Version 2.0.0, 12-DEC-1996 (WLT) */

/*        Various unused frames types were removed and the */
/*        frame time TK was added. */

/* -    SPICELIB Version 1.0.0, 10-DEC-1995 (WLT) */

/* -& */

/*     End of INCLUDE file frmtyp.inc */

/* $ Brief_I/O */

/*   None */

/* $ Detailed_Input */

/*   None */

/* $ Detailed_Output */

/*   None */

/* $ Parameters */

/*   None */

/* $ Exceptions */

/*   None */

/* $ Files */

/*   None */

/* $ Particulars */

/*   None */

/* $ Examples */

/*   None */

/* $ Restrictions */

/*   None */

/* $ Literature_References */

/*   None */

/* $ Author_and_Institution */

/*   None */

/* $ Version */

/* -    TSPICE Version 1.1.0, 09-AUG-2013 (BVS) */

/*        Updated for changed ZZFDAT calling sequence. Changed error */
/*        cases dealing with missing keywords to work with class 3 */
/*        rather than class 1 frame (to accomodate changed look-up */
/*        priority with built-in examined first. */

/* -    TSPICE Version 1.0.0, 07-NOV-2007 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Built-in frame hashes returned by ZZFDAT. */


/*     Begin every test family with an open call. */

    topen_("F_CCIFRM", (ftnlen)8);
    s_copy(frtext, " FRAME_CASSINI_LGA1               =     -0.8210200000000"
	    "0000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80, " FRAME_-82730_NAME                =    'CASSINI_RPW"
	    "S'", (ftnlen)80, (ftnlen)53);
    s_copy(frtext + 160, " TKFRAME_DSS-64_TOPO_UNITS        =    'DEGREES'", (
	    ftnlen)80, (ftnlen)48);
    s_copy(frtext + 240, " TKFRAME_DSS-17_TOPO_ANGLES       = (   -0.2431264"
	    "9410913031E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 320, "                                        -0.5465782"
	    "3408081398E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 400, "                                         0.1800000"
	    "0000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 480, " OBJECT_399025_FRAME              =    'DSS-25_TOP"
	    "O'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 560, " FRAME_-82763_NAME                =    'CASSINI_MI"
	    "MI_LEMMS2'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 640, " FRAME_-82764_CENTER              =     -0.8200000"
	    "0000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 720, " TKFRAME_DSS-64_TOPO_AXES         = (    0.3000000"
	    "0000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 800, "                                         0.2000000"
	    "0000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 880, "                                         0.3000000"
	    "0000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 960, " FRAME_-82103_CENTER              =     -0.8200000"
	    "0000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1040, " FRAME_CASSINI_RADAR_5            =     -0.828140"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1120, " OBJECT_399054_FRAME              =    'DSS-54_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 1200, " TKFRAME_-82378_UNITS             =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 1280, " FRAME_-82812_CLASS_ID            =     -0.828120"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1360, " TKFRAME_-82740_SPEC              =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 1440, " FRAME_CASSINI_MAG_MINUS          =     -0.823510"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1520, " FRAME_DSS-28_TOPO                =      0.139902"
	    "80000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1600, " FRAME_CASSINI_LGA2               =     -0.821030"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 1680, " TKFRAME_DSS-54_TOPO_UNITS        =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 1760, " TKFRAME_DSS-16_TOPO_ANGLES       = (   -0.243126"
	    "34972224771E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 1840, "                                        -0.546584"
	    "60594124101E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 1920, "                                         0.180000"
	    "00000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 2000, " TKFRAME_-82370_RELATIVE          =    'CASSINI_S"
	    "C_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 2080, " FRAME_DSS-64_TOPO                =      0.139906"
	    "40000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 2160, " TKFRAME_31000_MATRIX             = (    0.100000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2240, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2320, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2400, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2480, "                                         0.100000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2560, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2640, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2720, "                                         0.000000"
	    "00000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 2800, "                                         0.100000"
	    "00000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 2880, " FRAME_1399053_CENTER             =      0.399053"
	    "00000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 2960, " FRAME_-82898_NAME                =    'CASSINI_C"
	    "IRS_RAD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 3040, " FRAME_31000_CLASS                =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 3120, " TKFRAME_-82842_SPEC              =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 3200, " TKFRAME_-82733_RELATIVE          =    'CASSINI_S"
	    "C_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 3280, " TKFRAME_DSS-33_TOPO_RELATIVE     =    'EARTH_FIX"
	    "ED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 3360, " FRAME_1399033_NAME               =    'DSS-33_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 3440, " TKFRAME_DSS-15_TOPO_ANGLES       = (   -0.243112"
	    "80436637821E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 3520, "                                        -0.545781"
	    "46708818899E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 3600, "                                         0.180000"
	    "00000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 3680, " TKFRAME_-82107_SPEC              =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 3760, " FRAME_-82102_CLASS_ID            =     -0.821020"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 3840, " FRAME_-82891_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 3920, " FRAME_1399066_NAME               =    'DSS-66_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 4000, " FRAME_-82762_CLASS_ID            =     -0.827620"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4080, " TKFRAME_DSS-24_TOPO_SPEC         =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 4160, " NAIF_BODY_CODE                   =      0.399064"
	    "00000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4240, " CK_-82763_SCLK                   =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4320, " TKFRAME_-82814_RELATIVE          =    'CASSINI_S"
	    "C_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 4400, " FRAME_1399061_CLASS_ID           =      0.139906"
	    "10000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4480, " TKFRAME_DSS-25_TOPO_RELATIVE     =    'EARTH_FIX"
	    "ED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 4560, " FRAME_-82378_CLASS_ID            =     -0.823780"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4640, " OBJECT_399049_FRAME              =    'DSS-49_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 4720, " TKFRAME_DSS-34_TOPO_UNITS        =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 4800, " FRAME_-82843_CLASS_ID            =     -0.828430"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 4880, " TKFRAME_DSS-14_TOPO_ANGLES       = (   -0.243110"
	    "46126072219E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 4960, "                                        -0.545740"
	    "99118225000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 5040, "                                         0.180000"
	    "00000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 5120, " FRAME_-82372_CLASS               =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 5200, " TKFRAME_-82840_RELATIVE          =    'CASSINI_S"
	    "C_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 5280, " FRAME_1399014_CLASS              =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 5360, " TKFRAME_-82733_AXES              = (    0.300000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 5440, "                                         0.200000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 5520, "                                         0.100000"
	    "00000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 5600, " FRAME_1399043_CLASS              =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 5680, " TKFRAME_DSS-17_TOPO_RELATIVE     =    'EARTH_FIX"
	    "ED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 5760, " FRAME_31001_NAME                 =    'MOON_ME'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 5840, " TKFRAME_DSS-24_TOPO_UNITS        =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 5920, " FRAME_DSS-14_TOPO                =      0.139901"
	    "40000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6000, " TKFRAME_-82813_ANGLES            = (    0.180849"
	    "99999999999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 6080, "                                        -0.120000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 6160, "                                         0.000000"
	    "00000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 6240, " TKFRAME_DSS-13_TOPO_ANGLES       = (   -0.243205"
	    "54047636159E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 6320, "                                        -0.547528"
	    "35732536603E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 6400, "                                         0.180000"
	    "00000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 6480, " FRAME_-82107_CLASS_ID            =     -0.821070"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6560, " CK_-82763_SPK                    =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6640, " TKFRAME_DSS-12_TOPO_SPEC         =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 6720, " FRAME_-82360_CLASS_ID            =     -0.823600"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6800, " FRAME_-82730_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6880, " FRAME_-82008_CLASS               =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 6960, " TKFRAME_-82104_RELATIVE          =    'CASSINI_K"
	    "ABAND'", (ftnlen)80, (ftnlen)55);
    s_copy(frtext + 7040, " TKFRAME_-82792_SPEC              =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 7120, " TKFRAME_-82009_UNITS             =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 7200, " TKFRAME_-82001_AXES              = (    0.300000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 7280, "                                         0.200000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 7360, "                                         0.100000"
	    "00000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 7440, " FRAME_MOON_PA                    =      0.310000"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 7520, " FRAME_1399066_CLASS_ID           =      0.139906"
	    "60000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 7600, " FRAME_-82008_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 7680, " FRAME_CASSINI_UVIS_EUV           =     -0.828420"
	    "00000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 7760, " FRAME_-82814_CLASS               =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 7840, " FRAME_1399016_NAME               =    'DSS-16_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 7920, " TKFRAME_31001_RELATIVE           =    'MOON_ME_D"
	    "E403'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 8000, " TKFRAME_DSS-14_TOPO_UNITS        =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 8080, " FRAME_1399049_NAME               =    'DSS-49_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 8160, " FRAME_-82840_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 8240, " TKFRAME_DSS-12_TOPO_ANGLES       = (   -0.243194"
	    "51024426459E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 8320, "                                        -0.547000"
	    "62904314699E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 8400, "                                         0.180000"
	    "00000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 8480, " FRAME_-82843_CLASS               =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 8560, " TKFRAME_-82845_RELATIVE          =    'CASSINI_S"
	    "C_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 8640, " TKFRAME_-82844_UNITS             =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 8720, " OBJECT_399015_FRAME              =    'DSS-15_TO"
	    "PO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 8800, " FRAME_-82763_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 8880, " TKFRAME_-82350_SPEC              =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 8960, " FRAME_-82102_CENTER              =     -0.820000"
	    "00000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 9040, " TKFRAME_-82103_AXES              = (    0.300000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9120, "                                         0.200000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9200, "                                         0.100000"
	    "00000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 9280, " TKFRAME_-82360_AXES              = (    0.100000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9360, "                                         0.200000"
	    "00000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9440, "                                         0.300000"
	    "00000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 9520, " TKFRAME_-82108_ANGLES            = (   -0.109829"
	    "89689352000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9600, "                                        -0.179970"
	    "52693372001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 9680, "                                         0.000000"
	    "00000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 9760, " TKFRAME_-82368_UNITS             =    'DEGREES'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 9840, " TKFRAME_DSS-66_TOPO_SPEC         =    'ANGLES'", (
	    ftnlen)80, (ftnlen)47);
    s_copy(frtext + 9920, " FRAME_-82105_CLASS               =      0.400000"
	    "00000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10000, " FRAME_-82732_NAME                =    'CASSINI_"
	    "RPWS_EXMINUS'", (ftnlen)80, (ftnlen)61);
    s_copy(frtext + 10080, " TKFRAME_-82106_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 10160, " FRAME_CASSINI_RPWS_EZPLUS        =     -0.82733"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10240, " FRAME_-82765_NAME                =    'CASSINI_"
	    "MIMI_LEMMS_BASE'", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10320, " FRAME_31000_CLASS_ID             =      0.31000"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10400, " FRAME_CASSINI_ISS_NAC_RAD        =     -0.82368"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10480, " FRAME_-82000_NAME                =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 10560, " FRAME_CASSINI_SC_COORD           =     -0.82000"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10640, " FRAME_1399027_CLASS_ID           =      0.13990"
	    "270000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10720, " FRAME_-82890_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 10800, " TKFRAME_-82810_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 10880, " TKFRAME_DSS-23_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 10960, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 11040, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 11120, " FRAME_DSS-33_TOPO                =      0.13990"
	    "330000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 11200, " TKFRAME_PARKES_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 11280, " FRAME_DSS-66_TOPO                =      0.13990"
	    "660000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 11360, " TKFRAME_-82811_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 11440, " FRAME_1399053_CLASS_ID           =      0.13990"
	    "530000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 11520, " TKFRAME_DSS-54_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 11600, " TKFRAME_-82844_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 11680, " TKFRAME_DSS-28_TOPO_ANGLES       = (   -0.24322"
	    "110829953050E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 11760, "                                        -0.54761"
	    "727965969001E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 11840, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 11920, " FRAME_MOON_PA_DE403              =      0.31002"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 12000, " FRAME_-82102_NAME                =    'CASSINI_"
	    "LGA1'", (ftnlen)80, (ftnlen)53);
    s_copy(frtext + 12080, " FRAME_-82765_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 12160, " TKFRAME_DSS-24_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 12240, " TKFRAME_-82890_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 12320, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 12400, "                                         0.20000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 12480, " FRAME_1399033_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 12560, " TKFRAME_-82101_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 12640, " TKFRAME_DSS-27_TOPO_ANGLES       = (   -0.24322"
	    "334902235940E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 12720, "                                        -0.54761"
	    "728205681997E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 12800, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 12880, " FRAME_-82822_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 12960, " TKFRAME_31003_UNITS              =    'ARCSECON"
	    "DS'", (ftnlen)80, (ftnlen)51);
    s_copy(frtext + 13040, " TKFRAME_-82812_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 13120, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 13200, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 13280, " FRAME_-82372_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 13360, " CK_-82000_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 13440, " FRAME_1399017_CENTER             =      0.39901"
	    "700000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 13520, " TKFRAME_DSS-27_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 13600, " TKFRAME_DSS-49_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 13680, " TKFRAME_-82378_ANGLES            = (   -0.85500"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 13760, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 13840, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 13920, " TKFRAME_DSS-16_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 14000, " FRAME_-82000_CLASS_ID            =     -0.82000"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 14080, " TKFRAME_DSS-42_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 14160, " TKFRAME_DSS-26_TOPO_ANGLES       = (   -0.24312"
	    "698297645241E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 14240, "                                        -0.54664"
	    "310768860098E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 14320, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 14400, " FRAME_31003_NAME                 =    'MOON_ME_"
	    "DE403'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 14480, " FRAME_DSS-16_TOPO                =      0.13990"
	    "160000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 14560, " TKFRAME_-82761_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 14640, " TKFRAME_-82845_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 14720, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 14800, "                                        -0.90000"
	    "000000000000E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 14880, " OBJECT_399005_FRAME              =    'PARKES_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 14960, " FRAME_DSS-49_TOPO                =      0.13990"
	    "490000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15040, " FRAME_-82762_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15120, " FRAME_1399014_CLASS_ID           =      0.13990"
	    "140000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15200, " TKFRAME_DSS-17_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 15280, " FRAME_-82820_NAME                =    'CASSINI_"
	    "CAPS'", (ftnlen)80, (ftnlen)53);
    s_copy(frtext + 15360, " FRAME_-82101_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15440, " FRAME_1399034_CENTER             =      0.39903"
	    "400000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15520, " FRAME_CASSINI_CIRS_RAD           =     -0.82898"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 15600, " OBJECT_399034_FRAME              =    'DSS-34_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 15680, " TKFRAME_-82107_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 15760, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 15840, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 15920, " FRAME_-82731_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 16000, " TKFRAME_-82732_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 16080, " FRAME_-82891_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 16160, " TKFRAME_-82892_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 16240, " TKFRAME_DSS-25_TOPO_ANGLES       = (   -0.24312"
	    "463626565020E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 16320, "                                        -0.54662"
	    "388023518702E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 16400, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 16480, " OBJECT_399063_FRAME              =    'DSS-63_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 16560, " FRAME_-82760_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 16640, " TKFRAME_DSS-65_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 16720, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 16800, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 16880, " TKFRAME_-82761_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 16960, " FRAME_-82892_CLASS_ID            =     -0.82892"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 17040, " FRAME_1399028_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 17120, " TKFRAME_-82840_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 17200, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 17280, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 17360, " FRAME_-82822_CLASS_ID            =     -0.82822"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 17440, " FRAME_CASSINI_VIMS_RAD           =     -0.82378"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 17520, " FRAME_1399054_NAME               =    'DSS-54_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 17600, " TKFRAME_-82002_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 17680, " TKFRAME_-82105_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 17760, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 17840, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 17920, " TKFRAME_DSS-24_TOPO_ANGLES       = (   -0.24312"
	    "520504708351E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18000, "                                        -0.54660"
	    "107163981301E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18080, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 18160, " FRAME_-82734_NAME                =    'CASSINI_"
	    "RPWS_LP'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 18240, " TKFRAME_DSS-23_TOPO_ANGLES       = (   -0.24312"
	    "713644946820E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18320, "                                        -0.54660"
	    "449632925499E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18400, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 18480, " CK_-82820_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 18560, " FRAME_1399045_CLASS_ID           =      0.13990"
	    "450000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 18640, " TKFRAME_DSS-53_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18720, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 18800, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 18880, " TKFRAME_DSS-25_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 18960, " FRAME_-82002_NAME                =    'CASSINI_"
	    "SRU-B'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 19040, " TKFRAME_-82009_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 19120, " FRAME_1399023_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 19200, " TKFRAME_-82813_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 19280, " FRAME_-82821_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 19360, " TKFRAME_-82811_ANGLES            = (    0.17915"
	    "000000000001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 19440, "                                        -0.12000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 19520, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 19600, " TKFRAME_DSS-23_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 19680, " FRAME_-82104_NAME                =    'CASSINI_"
	    "XBAND'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 19760, " FRAME_-82361_NAME                =    'CASSINI_"
	    "ISS_WAC'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 19840, " FRAME_-82371_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 19920, " TKFRAME_-82734_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20000, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20080, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 20160, " FRAME_1399016_CENTER             =      0.39901"
	    "600000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 20240, " TKFRAME_-82361_ANGLES            = (   -0.89985"
	    "421000000002E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20320, "                                        -0.69806"
	    "000000000007E-01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20400, "                                         0.89973"
	    "600000000005E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 20480, " TKFRAME_-82368_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 20560, " TKFRAME_-82378_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20640, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20720, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 20800, " TKFRAME_DSS-13_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 20880, " TKFRAME_-82892_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 20960, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 21040, "                                         0.20000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 21120, " FRAME_-82370_CLASS_ID            =     -0.82370"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 21200, " TKFRAME_-82371_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 21280, " FRAME_CASSINI_XBAND              =     -0.82104"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 21360, " TKFRAME_DSS-15_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 21440, " TKFRAME_-82844_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 21520, "                                         0.17993"
	    "125072403001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 21600, "                                        -0.17993"
	    "125077351999E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 21680, " FRAME_-82761_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 21760, " FRAME_-82733_CLASS_ID            =     -0.82733"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 21840, " FRAME_1399033_CENTER             =      0.39903"
	    "300000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 21920, " FRAME_1399049_CENTER             =      0.39904"
	    "900000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 22000, " OBJECT_399024_FRAME              =    'DSS-24_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 22080, " TKFRAME_-82730_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 22160, " TKFRAME_EARTH_FIXED_RELATIVE     =    'ITRF93'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 22240, " TKFRAME_-82106_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 22320, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 22400, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 22480, " OBJECT_399053_FRAME              =    'DSS-53_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 22560, " TKFRAME_-82730_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 22640, " TKFRAME_EARTH_FIXED_SPEC         =    'MATRIX'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 22720, " FRAME_CASSINI_CAPS_ART           =     -0.82821"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 22800, " CK_-82000_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 22880, " TKFRAME_-82740_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 22960, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 23040, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 23120, " FRAME_-82814_CLASS_ID            =     -0.82814"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 23200, " TKFRAME_-82811_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 23280, " FRAME_-82822_NAME                =    'CASSINI_"
	    "CAPS_BASE'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 23360, " FRAME_1399066_CENTER             =      0.39906"
	    "600000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 23440, " TKFRAME_PARKES_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 23520, " TKFRAME_-82372_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 23600, " FRAME_-82840_CLASS_ID            =     -0.82840"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 23680, " FRAME_DSS-54_TOPO                =      0.13990"
	    "540000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 23760, " FRAME_CASSINI_ISS_WAC_RAD        =     -0.82369"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 23840, " TKFRAME_-82842_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 23920, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 24000, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 24080, " FRAME_-82891_NAME                =    'CASSINI_"
	    "CIRS_FP3'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 24160, " FRAME_1399023_NAME               =    'DSS-23_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 24240, " TKFRAME_-82898_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 24320, " TKFRAME_-82107_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 24400, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 24480, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 24560, " FRAME_-82104_CLASS_ID            =     -0.82104"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 24640, " FRAME_-82764_CLASS_ID            =     -0.82764"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 24720, " TKFRAME_-82101_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 24800, " TKFRAME_DSS-24_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 24880, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 24960, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 25040, " TKFRAME_-82761_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 25120, " FRAME_-82369_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25200, " FRAME_CASSINI_UVIS_SOLAR         =     -0.82843"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25280, " FRAME_1399063_CLASS_ID           =      0.13990"
	    "630000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25360, " TKFRAME_DSS-55_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 25440, " FRAME_-82790_CLASS_ID            =     -0.82790"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25520, " FRAME_-82371_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25600, " TKFRAME_-82372_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 25680, " FRAME_-82845_CLASS_ID            =     -0.82845"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25760, " FRAME_1399013_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 25840, " TKFRAME_-82842_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 25920, " FRAME_-82820_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 26000, " TKFRAME_-82810_ANGLES            = (    0.17780"
	    "000000000001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26080, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26160, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 26240, " FRAME_1399042_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 26320, " FRAME_CASSINI_MIMI_INCA          =     -0.82761"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 26400, " FRAME_-82370_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 26480, " TKFRAME_-82733_ANGLES            = (   -0.91200"
	    "000000000003E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26560, "                                        -0.31399"
	    "999999999999E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26640, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 26720, " FRAME_1399015_CENTER             =      0.39901"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 26800, " TKFRAME_-82360_ANGLES            = (   -0.90009"
	    "795999999994E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26880, "                                        -0.33000"
	    "000000000002E-01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 26960, "                                         0.89914"
	    "800000000000E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 27040, " TKFRAME_-82008_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 27120, " FRAME_CASSINI_INMS               =     -0.82740"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 27200, " TKFRAME_DSS-12_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 27280, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 27360, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 27440, " FRAME_CASSINI_SRU-B_RAD          =     -0.82009"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 27520, " TKFRAME_-82792_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 27600, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 27680, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 27760, " TKFRAME_-82106_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 27840, " FRAME_-82813_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 27920, " TKFRAME_-82814_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 28000, " TKFRAME_DSS-43_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 28080, " TKFRAME_-82843_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 28160, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 28240, "                                        -0.11000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 28320, " FRAME_-82106_NAME                =    'CASSINI_"
	    "KUBAND'", (ftnlen)80, (ftnlen)55);
    s_copy(frtext + 28400, " FRAME_-82760_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 28480, " TKFRAME_DSS-34_TOPO_ANGLES       = (   -0.14898"
	    "196500211100E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 28560, "                                        -0.12539"
	    "847787565520E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 28640, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 28720, " TKFRAME_31003_RELATIVE           =    'MOON_PA_"
	    "DE403'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 28800, " TKFRAME_-82792_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 28880, " FRAME_-82842_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 28960, " TKFRAME_-82843_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 29040, " OBJECT_399014_FRAME              =    'DSS-14_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 29120, " TKFRAME_-82105_ANGLES            = (   -0.13256"
	    "335175319001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 29200, "                                        -0.17996"
	    "188215321999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 29280, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 29360, " TKFRAME_DSS-14_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 29440, " FRAME_1399042_NAME               =    'DSS-42_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 29520, " OBJECT_399043_FRAME              =    'DSS-43_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 29600, " TKFRAME_-82350_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 29680, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 29760, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 29840, " FRAME_1399024_CLASS_ID           =      0.13990"
	    "240000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 29920, " FRAME_-82740_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 30000, " TKFRAME_DSS-33_TOPO_ANGLES       = (   -0.14898"
	    "309235951129E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 30080, "                                        -0.12540"
	    "048377056090E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 30160, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 30240, " FRAME_-82104_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 30320, " TKFRAME_-82105_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 30400, " TKFRAME_DSS-66_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 30480, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 30560, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 30640, " FRAME_1399065_CENTER             =      0.39906"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 30720, " NAIF_BODY_NAME                   =    'DSS-64'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 30800, " FRAME_1399066_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 30880, " FRAME_31002_CLASS_ID             =      0.31002"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 30960, " FRAME_-82002_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 31040, " TKFRAME_-82732_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 31120, " TKFRAME_-82765_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 31200, " FRAME_-82791_NAME                =    'CASSINI_"
	    "CDA_ART'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 31280, " TKFRAME_-82893_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 31360, "                                        -0.90002"
	    "291831180528E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 31440, "                                        -0.97402"
	    "825172239985E-01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 31520, " TKFRAME_DSS-66_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 31600, " FRAME_DSS-23_TOPO                =      0.13990"
	    "230000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 31680, " TKFRAME_DSS-63_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 31760, " TKFRAME_-82811_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 31840, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 31920, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 32000, " FRAME_-82368_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 32080, " TKFRAME_DSS-54_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 32160, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 32240, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 32320, " FRAME_1399055_CLASS_ID           =      0.13990"
	    "550000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 32400, " TKFRAME_-82844_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 32480, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 32560, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 32640, " FRAME_-82893_NAME                =    'CASSINI_"
	    "CIRS_FPB'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 32720, " FRAME_1399025_NAME               =    'DSS-25_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 32800, " TKFRAME_DSS-26_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 32880, " FRAME_-82361_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 32960, " TKFRAME_-82369_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 33040, " FRAME_-82764_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 33120, " TKFRAME_-82765_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 33200, " TKFRAME_DSS-53_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 33280, " TKFRAME_-82102_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 33360, " FRAME_1399061_NAME               =    'DSS-61_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 33440, " TKFRAME_DSS-49_TOPO_ANGLES       = (   -0.14826"
	    "351615289471E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 33520, "                                        -0.12299"
	    "839804233260E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 33600, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 33680, " TKFRAME_-82732_ANGLES            = (   -0.16309"
	    "999999999999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 33760, "                                        -0.10759"
	    "999999999999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 33840, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 33920, " FRAME_1399061_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 34000, " FRAME_1399014_CENTER             =      0.39901"
	    "400000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 34080, " CK_-82791_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 34160, " TKFRAME_-82351_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 34240, " TKFRAME_DSS-49_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 34320, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 34400, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 34480, " TKFRAME_DSS-43_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 34560, " FRAME_-82002_CLASS_ID            =     -0.82002"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 34640, " TKFRAME_DSS-42_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 34720, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 34800, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 34880, " TKFRAME_-82842_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 34960, "                                         0.17993"
	    "125071716000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35040, "                                         0.17992"
	    "552119261001E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 35120, " TKFRAME_DSS-14_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 35200, " FRAME_MOON_ME                    =      0.31001"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 35280, " TKFRAME_-82765_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35360, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35440, "                                        -0.90000"
	    "000000000000E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 35520, " TKFRAME_-82104_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35600, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35680, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 35760, " TKFRAME_-82761_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35840, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 35920, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 36000, " FRAME_-82810_NAME                =    'CASSINI_"
	    "RADAR_1'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 36080, " OBJECT_399033_FRAME              =    'DSS-33_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 36160, " FRAME_1399016_CLASS_ID           =      0.13990"
	    "160000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 36240, " TKFRAME_DSS-33_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 36320, " FRAME_CASSINI_MAG_PLUS           =     -0.82350"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 36400, " FRAME_-82843_NAME                =    'CASSINI_"
	    "UVIS_SOLAR'", (ftnlen)80, (ftnlen)59);
    s_copy(frtext + 36480, " FRAME_-82730_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 36560, " TKFRAME_-82731_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 36640, " FRAME_-82890_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 36720, " FRAME_DSS-42_TOPO                =      0.13990"
	    "420000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 36800, " TKFRAME_-82891_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 36880, " TKFRAME_-82740_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 36960, " TKFRAME_-82760_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 37040, " TKFRAME_DSS-13_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 37120, " FRAME_-82108_NAME                =    'CASSINI_"
	    "XBAND_TRUE'", (ftnlen)80, (ftnlen)59);
    s_copy(frtext + 37200, " FRAME_1399042_CLASS_ID           =      0.13990"
	    "420000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 37280, " FRAME_-82792_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 37360, " FRAME_1399027_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 37440, " FRAME_1399064_CENTER             =      0.39906"
	    "400000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 37520, " TKFRAME_-82891_RELATIVE          =    'CASSINI_"
	    "CIRS_FPB'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 37600, " TKFRAME_DSS-23_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 37680, " TKFRAME_31001_SPEC               =    'MATRIX'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 37760, " FRAME_CASSINI_CAPS_BASE          =     -0.82822"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 37840, " TKFRAME_DSS-46_TOPO_ANGLES       = (   -0.14898"
	    "308226653509E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 37920, "                                        -0.12540"
	    "500967083020E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 38000, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 38080, " TKFRAME_DSS-61_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 38160, " TKFRAME_-82892_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 38240, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 38320, "                                        -0.26929"
	    "016371149000E-01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 38400, " TKFRAME_DSS-13_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 38480, " TKFRAME_DSS-45_TOPO_ANGLES       = (   -0.14897"
	    "768621480211E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 38560, "                                        -0.12539"
	    "845671876880E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 38640, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 38720, " TKFRAME_DSS-65_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 38800, " TKFRAME_-82734_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 38880, " FRAME_-82351_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 38960, " FRAME_-82760_NAME                =    'CASSINI_"
	    "MIMI_CHEMS'", (ftnlen)80, (ftnlen)59);
    s_copy(frtext + 39040, " FRAME_CASSINI_ISS_WAC            =     -0.82361"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 39120, " OBJECT_399028_FRAME              =    'DSS-28_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 39200, " TKFRAME_-82101_BORESIGHT         = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 39280, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 39360, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 39440, " TKFRAME_DSS-66_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 39520, " TKFRAME_DSS-25_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 39600, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 39680, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 39760, " FRAME_-82351_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 39840, " FRAME_DSS-25_TOPO                =      0.13990"
	    "250000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 39920, " TKFRAME_-82009_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40000, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40080, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 40160, " TKFRAME_-82009_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 40240, " TKFRAME_-82002_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 40320, " FRAME_CASSINI_RPWS_EXPLUS        =     -0.82731"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 40400, " FRAME_-82730_CLASS_ID            =     -0.82730"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 40480, " TKFRAME_-82813_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40560, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40640, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 40720, " TKFRAME_-82731_ANGLES            = (   -0.16899"
	    "999999999999E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40800, "                                        -0.10759"
	    "999999999999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 40880, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 40960, " FRAME_1399013_CENTER             =      0.39901"
	    "300000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 41040, " FRAME_DSS-61_TOPO                =      0.13990"
	    "610000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 41120, " FRAME_1399027_NAME               =    'DSS-27_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 41200, " FRAME_-82351_NAME                =    'CASSINI_"
	    "MAG_MINUS'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 41280, " CK_-82764_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 41360, " TKFRAME_-82009_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 41440, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 41520, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 41600, " FRAME_31003_CLASS                =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 41680, " TKFRAME_-82368_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 41760, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 41840, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 41920, " FRAME_-82811_CLASS_ID            =     -0.82811"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 42000, " TKFRAME_DSS-43_TOPO_ANGLES       = (   -0.14898"
	    "126789071159E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 42080, "                                        -0.12540"
	    "242326663780E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 42160, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 42240, " TKFRAME_DSS-49_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 42320, " TKFRAME_-82104_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 42400, " TKFRAME_-82361_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 42480, " FRAME_1399063_NAME               =    'DSS-63_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 42560, " TKFRAME_DSS-13_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 42640, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 42720, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 42800, " FRAME_-82372_CLASS_ID            =     -0.82372"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 42880, " TKFRAME_-82371_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 42960, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 43040, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 43120, " TKFRAME_DSS-46_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 43200, " FRAME_-82822_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 43280, " FRAME_1399046_CENTER             =      0.39904"
	    "600000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 43360, " TKFRAME_-82103_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 43440, "                                         0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 43520, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 43600, " OBJECT_399023_FRAME              =    'DSS-23_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 43680, " TKFRAME_DSS-42_TOPO_ANGLES       = (   -0.14898"
	    "126797965500E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 43760, "                                        -0.12540"
	    "067366658261E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 43840, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 43920, " TKFRAME_-82732_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 44000, " FRAME_-82008_NAME                =    'CASSINI_"
	    "SRU-A_RAD'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 44080, " FRAME_-82101_CLASS_ID            =     -0.82101"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44160, " FRAME_1399034_CLASS_ID           =      0.13990"
	    "340000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44240, " FRAME_-82791_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44320, " FRAME_1399017_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44400, " FRAME_-82761_CLASS_ID            =     -0.82761"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44480, " TKFRAME_-82730_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 44560, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 44640, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 44720, " FRAME_1399063_CENTER             =      0.39906"
	    "300000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44800, " FRAME_1399046_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 44880, " FRAME_-82812_NAME                =    'CASSINI_"
	    "RADAR_3'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 44960, " TKFRAME_-82813_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 45040, " TKFRAME_DSS-12_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 45120, " FRAME_-82845_NAME                =    'CASSINI_"
	    "UVIS_HDAC'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 45200, " TKFRAME_PARKES_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 45280, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 45360, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 45440, " TKFRAME_PARKES_TOPO_ANGLES       = (   -0.14826"
	    "351615289471E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 45520, "                                        -0.12299"
	    "839804233260E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 45600, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 45680, " TKFRAME_DSS-26_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 45760, " FRAME_-82842_CLASS_ID            =     -0.82842"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 45840, " TKFRAME_-82891_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 45920, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 46000, "                                         0.26929"
	    "016371149000E-01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 46080, " TKFRAME_-82822_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 46160, " FRAME_1399013_NAME               =    'DSS-13_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 46240, " FRAME_1399046_NAME               =    'DSS-46_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 46320, " FRAME_-82370_NAME                =    'CASSINI_"
	    "VIMS_V'", (ftnlen)80, (ftnlen)55);
    s_copy(frtext + 46400, " TKFRAME_31003_SPEC               =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 46480, " TKFRAME_-82898_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 46560, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 46640, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 46720, " TKFRAME_-82891_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 46800, " FRAME_-82350_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 46880, " TKFRAME_DSS-16_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 46960, " FRAME_-82106_CLASS_ID            =     -0.82106"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 47040, " TKFRAME_-82103_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 47120, " TKFRAME_DSS-64_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 47200, " FRAME_-82370_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 47280, " TKFRAME_-82371_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 47360, " TKFRAME_DSS-55_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 47440, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 47520, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 47600, " FRAME_1399065_CLASS_ID           =      0.13990"
	    "650000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 47680, " FRAME_-82792_CLASS_ID            =     -0.82792"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 47760, " TKFRAME_31000_RELATIVE           =    'MOON_PA_"
	    "DE403'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 47840, " FRAME_1399012_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 47920, " TKFRAME_DSS-27_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 48000, " FRAME_-82740_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48080, " FRAME_-82108_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48160, " FRAME_-82762_NAME                =    'CASSINI_"
	    "MIMI_LEMMS1'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 48240, " TKFRAME_-82844_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 48320, " TKFRAME_-82730_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 48400, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 48480, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 48560, " FRAME_1399012_CENTER             =      0.39901"
	    "200000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48640, " FRAME_1399028_CENTER             =      0.39902"
	    "800000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48720, " FRAME_-82002_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48800, " FRAME_CASSINI_RPWS_LP            =     -0.82734"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 48880, " TKFRAME_-82008_ANGLES            = (    0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 48960, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 49040, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 49120, " FRAME_DSS-27_TOPO                =      0.13990"
	    "270000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 49200, " FRAME_CASSINI_SBAND              =     -0.82107"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 49280, " TKFRAME_-82840_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 49360, "                                         0.17998"
	    "854084310000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 49440, "                                         0.17999"
	    "427042161000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 49520, " TKFRAME_-82108_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 49600, " FRAME_-82812_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 49680, " TKFRAME_-82813_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 49760, " TKFRAME_-82361_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 49840, " FRAME_DSS-63_TOPO                =      0.13990"
	    "630000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 49920, " FRAME_1399045_CENTER             =      0.39904"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 50000, " TKFRAME_-82102_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 50080, "                                         0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 50160, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 50240, " TKFRAME_DSS-43_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 50320, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 50400, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 50480, " TKFRAME_DSS-49_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 50560, " TKFRAME_-82842_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 50640, " FRAME_MOON_ME_DE403              =      0.31003"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 50720, " TKFRAME_DSS-15_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 50800, " OBJECT_399013_FRAME              =    'DSS-13_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 50880, " OBJECT_399042_FRAME              =    'DSS-42_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 50960, " TKFRAME_-82106_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 51040, " FRAME_1399065_NAME               =    'DSS-65_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 51120, " TKFRAME_-82740_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 51200, " FRAME_1399026_CLASS_ID           =      0.13990"
	    "260000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 51280, " FRAME_-82790_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 51360, " CK_-82762_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 51440, " TKFRAME_DSS-55_TOPO_ANGLES       = (   -0.35574"
	    "736739940482E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 51520, "                                        -0.49575"
	    "703528880503E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 51600, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 51680, " FRAME_-82103_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 51760, " TKFRAME_-82104_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 51840, " FRAME_1399065_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 51920, " FRAME_-82369_CLASS_ID            =     -0.82369"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 52000, " TKFRAME_-82890_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52080, "                                         0.40107"
	    "045659157998E-02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52160, "                                        -0.22803"
	    "720246206999E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 52240, " FRAME_-82001_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 52320, " TKFRAME_-82002_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 52400, " TKFRAME_-82732_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52480, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52560, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 52640, " TKFRAME_DSS-54_TOPO_ANGLES       = (   -0.35574"
	    "590387093929E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52720, "                                        -0.49574"
	    "377750704997E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52800, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 52880, " TKFRAME_-82765_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 52960, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 53040, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 53120, " FRAME_31000_NAME                 =    'MOON_PA'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 53200, " FRAME_-82814_NAME                =    'CASSINI_"
	    "RADAR_5'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 53280, " FRAME_DSS-13_TOPO                =      0.13990"
	    "130000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 53360, " FRAME_DSS-46_TOPO                =      0.13990"
	    "460000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 53440, " FRAME_-82351_CLASS_ID            =     -0.82351"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 53520, " FRAME_-82369_NAME                =    'CASSINI_"
	    "ISS_WAC_RAD'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 53600, " TKFRAME_DSS-53_TOPO_ANGLES       = (   -0.35575"
	    "034853158411E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 53680, "                                        -0.49572"
	    "642141262399E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 53760, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 53840, " FRAME_-82734_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 53920, " FRAME_1399015_NAME               =    'DSS-15_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 54000, " FRAME_-82360_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 54080, " TKFRAME_-82361_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 54160, " TKFRAME_-82102_BORESIGHT         = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 54240, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 54320, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 54400, " OBJECT_399066_FRAME              =    'DSS-66_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 54480, " FRAME_-82372_NAME                =    'CASSINI_"
	    "VIMS_IR_SOL'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 54560, " TKFRAME_DSS-26_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 54640, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 54720, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 54800, " FRAME_-82763_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 54880, " TKFRAME_-82822_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 54960, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 55040, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 55120, " TKFRAME_-82893_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 55200, " TKFRAME_DSS-63_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 55280, " FRAME_-82792_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 55360, " FRAME_1399027_CENTER             =      0.39902"
	    "700000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 55440, " TKFRAME_-82372_ANGLES            = (    0.85943"
	    "668669984000E-01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 55520, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 55600, "                                        -0.11063"
	    "025357166001E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 55680, " TKFRAME_-82102_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 55760, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 55840, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 55920, " FRAME_1399013_CLASS_ID           =      0.13990"
	    "130000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56000, " FRAME_-82740_CLASS_ID            =     -0.82740"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56080, " FRAME_-82001_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56160, " FRAME_-82731_NAME                =    'CASSINI_"
	    "RPWS_EXPLUS'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 56240, " TKFRAME_DSS-55_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 56320, " FRAME_-82891_CLASS_ID            =     -0.82891"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56400, " FRAME_-82764_NAME                =    'CASSINI_"
	    "MIMI_LEMMS_ART'", (ftnlen)80, (ftnlen)63);
    s_copy(frtext + 56480, " FRAME_-82821_CLASS_ID            =     -0.82821"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56560, " FRAME_CASSINI_CDA_ART            =     -0.82791"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 56640, " TKFRAME_-82101_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 56720, "                                         0.18000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 56800, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 56880, " TKFRAME_-82001_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 56960, " TKFRAME_DSS-14_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57040, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57120, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 57200, " TKFRAME_EARTH_FIXED_MATRIX       = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57280, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57360, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57440, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57520, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57600, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57680, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57760, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 57840, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 57920, " TKFRAME_DSS-45_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 58000, " FRAME_-82898_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 58080, " TKFRAME_-82730_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 58160, " TKFRAME_-82890_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 58240, " OBJECT_399061_FRAME              =    'DSS-61_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 58320, " FRAME_DSS-65_TOPO                =      0.13990"
	    "650000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 58400, " FRAME_1399061_CENTER             =      0.39906"
	    "100000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 58480, " TKFRAME_-82810_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 58560, " FRAME_1399026_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 58640, " TKFRAME_-82843_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 58720, " FRAME_-82101_NAME                =    'CASSINI_"
	    "HGA'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 58800, " FRAME_1399034_NAME               =    'DSS-34_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 58880, " FRAME_1399055_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 58960, " TKFRAME_-82108_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 59040, " FRAME_CASSINI_KUBAND             =     -0.82106"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59120, " TKFRAME_-82893_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 59200, " FRAME_-82009_CLASS_ID            =     -0.82009"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59280, " FRAME_CASSINI_MIMI_CHEMS         =     -0.82760"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59360, " CK_-82764_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59440, " TKFRAME_DSS-61_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 59520, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 59600, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 59680, " FRAME_-82814_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59760, " TKFRAME_DSS-33_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 59840, " FRAME_CASSINI_VIMS_IR_SOL        =     -0.82372"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 59920, " OBJECT_399027_FRAME              =    'DSS-27_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 60000, " TKFRAME_-82734_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 60080, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 60160, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 60240, " FRAME_-82350_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 60320, " TKFRAME_-82351_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 60400, " FRAME_1399049_CLASS_ID           =      0.13990"
	    "490000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 60480, " FRAME_31002_NAME                 =    'MOON_PA_"
	    "DE403'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 60560, " FRAME_DSS-15_TOPO                =      0.13990"
	    "150000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 60640, " TKFRAME_-82760_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 60720, " TKFRAME_-82898_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 60800, " TKFRAME_DSS-66_TOPO_ANGLES       = (   -0.35574"
	    "858307052830E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 60880, "                                        -0.49570"
	    "024558335497E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 60960, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 61040, " FRAME_1399026_CENTER             =      0.39902"
	    "600000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 61120, " TKFRAME_-82371_ANGLES            = (   -0.36012"
	    "175939433001E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 61200, "                                        -0.90048"
	    "672769633001E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 61280, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 61360, " TEXT_KERNEL_ID                   =    'CASSINI_"
	    "FRAMES V3.9.0 14-OCT-2004 FK'", (ftnlen)80, (ftnlen)77);
    s_copy(frtext + 61440, " TKFRAME_-82002_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 61520, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 61600, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 61680, " FRAME_1399005_CLASS_ID           =      0.13990"
	    "050000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 61760, " TKFRAME_DSS-28_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 61840, " FRAME_CASSINI_XBAND_TRUE         =     -0.82108"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 61920, " FRAME_-82732_CLASS_ID            =     -0.82732"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62000, " FRAME_-82000_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62080, " FRAME_31003_CENTER               =      0.30100"
	    "000000000000E+03", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62160, " FRAME_CASSINI_RPWS               =     -0.82730"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62240, " FRAME_1399017_NAME               =    'DSS-17_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 62320, " FRAME_31002_CLASS                =      0.20000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62400, " TKFRAME_DSS-65_TOPO_ANGLES       = (   -0.35574"
	    "930190246852E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 62480, "                                        -0.49572"
	    "793062938800E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 62560, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 62640, " TKFRAME_-82351_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 62720, " FRAME_1399053_NAME               =    'DSS-53_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 62800, " FRAME_-82813_CLASS_ID            =     -0.82813"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 62880, " TKFRAME_-82761_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 62960, "                                        -0.95000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 63040, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 63120, " FRAME_1399043_CENTER             =      0.39904"
	    "300000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 63200, " TKFRAME_-82104_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 63280, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 63360, "                                         0.20000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 63440, " TKFRAME_-82810_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 63520, " TKFRAME_-82361_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 63600, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 63680, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 63760, " FRAME_-82821_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 63840, " TKFRAME_-82822_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 63920, " TKFRAME_DSS-54_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 64000, " TKFRAME_-82371_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 64080, " TKFRAME_DSS-64_TOPO_ANGLES       = (   -0.35574"
	    "930190246852E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 64160, "                                        -0.49572"
	    "793062938800E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 64240, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 64320, " FRAME_-82733_NAME                =    'CASSINI_"
	    "RPWS_EZPLUS'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 64400, " TKFRAME_DSS-16_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 64480, " TKFRAME_-82734_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 64560, " FRAME_1399016_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 64640, " TKFRAME_DSS-46_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 64720, " FRAME_-82103_CLASS_ID            =     -0.82103"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 64800, " FRAME_-82763_CLASS_ID            =     -0.82763"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 64880, " FRAME_-82001_NAME                =    'CASSINI_"
	    "SRU-A'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 64960, " FRAME_1399045_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 65040, " TKFRAME_DSS-63_TOPO_ANGLES       = (   -0.35575"
	    "199215642402E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 65120, "                                        -0.49568"
	    "789683094202E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 65200, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 65280, " TKFRAME_-82760_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 65360, " TKFRAME_-82008_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 65440, " CK_-82790_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 65520, " FRAME_DSS-34_TOPO                =      0.13990"
	    "340000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 65600, " TKFRAME_-82812_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 65680, " CK_-82820_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 65760, " TKFRAME_DSS-42_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 65840, " FRAME_CASSINI_VIMS_IR            =     -0.82371"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 65920, " FRAME_-82844_CLASS_ID            =     -0.82844"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 66000, " TKFRAME_-82845_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 66080, " TKFRAME_-82822_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 66160, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 66240, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 66320, " FRAME_-82813_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 66400, " FRAME_CASSINI_KABAND             =     -0.82105"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 66480, " TKFRAME_PARKES_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 66560, " FRAME_CASSINI_VIMS_V             =     -0.82370"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 66640, " FRAME_-82103_NAME                =    'CASSINI_"
	    "LGA2'", (ftnlen)80, (ftnlen)53);
    s_copy(frtext + 66720, " FRAME_-82360_NAME                =    'CASSINI_"
	    "ISS_NAC'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 66800, " TKFRAME_-82369_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 66880, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 66960, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 67040, " TKFRAME_DSS-63_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 67120, " TKFRAME_31003_AXES               = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 67200, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 67280, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 67360, " FRAME_-82845_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 67440, " OBJECT_399017_FRAME              =    'DSS-17_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 67520, " FRAME_CASSINI_CIRS_FP1           =     -0.82890"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 67600, " TKFRAME_-82891_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 67680, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 67760, "                                         0.20000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 67840, " TKFRAME_-82370_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 67920, " FRAME_-82108_CLASS_ID            =     -0.82108"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 68000, " OBJECT_399046_FRAME              =    'DSS-46_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 68080, " FRAME_-82361_CLASS_ID            =     -0.82361"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 68160, " TKFRAME_-82105_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 68240, " FRAME_-82369_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 68320, " TKFRAME_-82765_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 68400, " TKFRAME_DSS-61_TOPO_ANGLES       = (   -0.35575"
	    "097846087601E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 68480, "                                        -0.49571"
	    "260262846202E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 68560, "                                         0.18000"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 68640, " TKFRAME_-82370_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 68720, " TKFRAME_-82103_BORESIGHT         = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 68800, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 68880, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 68960, " FRAME_1399025_CENTER             =      0.39902"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 69040, " TKFRAME_-82370_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 69120, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 69200, "                                        -0.90000"
	    "000000000000E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 69280, " FRAME_-82108_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 69360, " TKFRAME_DSS-27_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 69440, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 69520, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 69600, " FRAME_-82107_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 69680, " TKFRAME_-82108_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 69760, " FRAME_31002_CENTER               =      0.30100"
	    "000000000000E+03", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 69840, " FRAME_1399023_CLASS_ID           =      0.13990"
	    "230000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 69920, " FRAME_DSS-17_TOPO                =      0.13990"
	    "170000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70000, " FRAME_CASSINI_UVIS_HSP           =     -0.82844"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70080, " FRAME_-82821_NAME                =    'CASSINI_"
	    "CAPS_ART'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 70160, " TKFRAME_-82760_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 70240, "                                         0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 70320, "                                         0.90000"
	    "000000000000E+02 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 70400, " FRAME_1399042_CENTER             =      0.39904"
	    "200000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70480, " TKFRAME_DSS-61_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 70560, " TKFRAME_DSS-12_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 70640, " FRAME_CASSINI_CIRS_FP3           =     -0.82891"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70720, " FRAME_-82811_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70800, " TKFRAME_-82812_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 70880, " FRAME_DSS-53_TOPO                =      0.13990"
	    "530000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 70960, " FRAME_-82840_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 71040, " OBJECT_399012_FRAME              =    'DSS-12_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 71120, " FRAME_31001_CLASS_ID             =      0.31001"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 71200, " FRAME_-82890_NAME                =    'CASSINI_"
	    "CIRS_FP1'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 71280, " TKFRAME_DSS-65_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 71360, " TKFRAME_DSS-15_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 71440, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 71520, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 71600, " TKFRAME_DSS-53_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 71680, " FRAME_1399055_NAME               =    'DSS-55_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 71760, " FRAME_-82898_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 71840, " TKFRAME_-82106_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 71920, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 72000, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 72080, " FRAME_CASSINI_CIRS_FP4           =     -0.82892"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72160, " TKFRAME_DSS-46_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 72240, " FRAME_1399028_CLASS_ID           =      0.13990"
	    "280000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72320, " FRAME_-82102_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72400, " TKFRAME_-82103_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 72480, " TKFRAME_DSS-55_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 72560, " FRAME_1399064_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72640, " FRAME_CASSINI_MIMI_LEMMS_ART     =     -0.82764"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72720, " TKFRAME_DSS-45_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 72800, " CK_-82762_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72880, " FRAME_1399054_CLASS_ID           =      0.13990"
	    "540000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 72960, " FRAME_-82000_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 73040, " TKFRAME_-82001_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 73120, " CK_-82821_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 73200, " TKFRAME_-82368_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 73280, " FRAME_-82812_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 73360, " TKFRAME_DSS-45_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 73440, " FRAME_-82378_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 73520, " TKFRAME_-82368_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 73600, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 73680, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 73760, " TKFRAME_DSS-34_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 73840, " TKFRAME_31003_ANGLES             = (    0.63898"
	    "600000000002E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 73920, "                                         0.79076"
	    "800000000006E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 74000, "                                         0.14620"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 74080, " TKFRAME_-82814_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 74160, " FRAME_-82840_NAME                =    'CASSINI_"
	    "UVIS_FUV'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 74240, " FRAME_-82845_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 74320, " FRAME_-82733_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 74400, " FRAME_1399005_NAME               =    'PARKES_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 74480, " TKFRAME_-82734_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 74560, " FRAME_-82893_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 74640, " TKFRAME_-82350_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 74720, " TKFRAME_-82360_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 74800, " OBJECT_399065_FRAME              =    'DSS-65_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 74880, " FRAME_-82105_NAME                =    'CASSINI_"
	    "KABAND'", (ftnlen)80, (ftnlen)55);
    s_copy(frtext + 74960, " FRAME_-82001_CLASS_ID            =     -0.82001"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75040, " FRAME_-82762_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75120, " FRAME_1399024_CENTER             =      0.39902"
	    "400000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75200, " TKFRAME_-82369_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 75280, " FRAME_-82107_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75360, " FRAME_-82791_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75440, " TKFRAME_-82792_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 75520, " FRAME_31001_CENTER               =      0.30100"
	    "000000000000E+03", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75600, " FRAME_CASSINI_CAPS               =     -0.82820"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 75680, " TKFRAME_-82893_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 75760, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 75840, "                                         0.20000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 75920, " TKFRAME_-82372_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 76000, " FRAME_CASSINI_ISS_NAC            =     -0.82360"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 76080, " FRAME_1399015_CLASS_ID           =      0.13990"
	    "150000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 76160, " TKFRAME_DSS-25_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 76240, " FRAME_-82893_CLASS_ID            =     -0.82893"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 76320, " TKFRAME_-82890_RELATIVE          =    'CASSINI_"
	    "CIRS_FPB'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 76400, " TKFRAME_-82731_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 76480, " TKFRAME_DSS-15_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 76560, " FRAME_-82790_NAME                =    'CASSINI_"
	    "CDA'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 76640, " TKFRAME_-82792_ANGLES            = (    0.15000"
	    "000000000000E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 76720, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 76800, "                                         0.10500"
	    "000000000000E+03 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 76880, " TKFRAME_DSS-45_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 76960, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 77040, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 77120, " FRAME_DSS-55_TOPO                =      0.13990"
	    "550000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 77200, " TKFRAME_DSS-17_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 77280, " FRAME_CASSINI_MIMI_LEMMS1        =     -0.82762"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 77360, " FRAME_1399025_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 77440, " TKFRAME_-82810_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 77520, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 77600, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 77680, " FRAME_-82378_NAME                =    'CASSINI_"
	    "VIMS_RAD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 77760, " TKFRAME_-82843_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 77840, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 77920, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 78000, " FRAME_-82892_NAME                =    'CASSINI_"
	    "CIRS_FP4'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 78080, " FRAME_1399024_NAME               =    'DSS-24_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 78160, " FRAME_1399054_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 78240, " FRAME_1399046_CLASS_ID           =      0.13990"
	    "460000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 78320, " FRAME_-82898_CLASS_ID            =     -0.82898"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 78400, " TKFRAME_-82108_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 78480, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 78560, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 78640, " FRAME_-82811_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 78720, " TKFRAME_-82101_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 78800, " FRAME_CASSINI_MIMI_LEMMS2        =     -0.82763"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 78880, " TKFRAME_-82008_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 78960, " FRAME_-82734_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 79040, " FRAME_-82361_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 79120, " TKFRAME_-82351_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 79200, "                                         0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 79280, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 79360, " TKFRAME_DSS-33_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 79440, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 79520, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 79600, " CK_-82790_SCLK                   =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 79680, " FRAME_CASSINI_CIRS_FPB           =     -0.82893"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 79760, " FRAME_-82740_NAME                =    'CASSINI_"
	    "INMS'", (ftnlen)80, (ftnlen)53);
    s_copy(frtext + 79840, " OBJECT_399026_FRAME              =    'DSS-26_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 79920, " FRAME_CASSINI_SRU-A              =     -0.82001"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80000, " TKFRAME_DSS-64_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 80080, " FRAME_-82844_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80160, " FRAME_-82810_CLASS_ID            =     -0.82810"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80240, " TKFRAME_-82350_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 80320, " OBJECT_399055_FRAME              =    'DSS-55_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 80400, " FRAME_-82378_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80480, " FRAME_1399023_CENTER             =      0.39902"
	    "300000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80560, " FRAME_-82106_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80640, " FRAME_-82371_CLASS_ID            =     -0.82371"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 80720, " TKFRAME_-82760_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 80800, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 80880, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 80960, " FRAME_31000_CENTER               =      0.30100"
	    "000000000000E+03", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 81040, " FRAME_1399049_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 81120, " FRAME_-82842_NAME                =    'CASSINI_"
	    "UVIS_EUV'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 81200, " FRAME_CASSINI_SRU-B              =     -0.82002"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 81280, " TKFRAME_DSS-28_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 81360, " TKFRAME_DSS-28_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 81440, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 81520, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 81600, " FRAME_-82734_CLASS_ID            =     -0.82734"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 81680, " FRAME_-82107_NAME                =    'CASSINI_"
	    "SBAND'", (ftnlen)80, (ftnlen)54);
    s_copy(frtext + 81760, " TKFRAME_-82731_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 81840, " FRAME_31001_CLASS                =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 81920, " TKFRAME_DSS-28_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 82000, " FRAME_1399033_CLASS_ID           =      0.13990"
	    "330000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 82080, " FRAME_-82760_CLASS_ID            =     -0.82760"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 82160, " FRAME_1399043_NAME               =    'DSS-43_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 82240, " TKFRAME_31000_SPEC               =    'MATRIX'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 82320, " TKFRAME_-82351_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 82400, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 82480, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 82560, " FRAME_-82820_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 82640, " FRAME_CASSINI_MIMI_LEMMS_BASE    =     -0.82765"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 82720, " TKFRAME_-82812_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 82800, " FRAME_CASSINI_CDA_BASE           =     -0.82792"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 82880, " TKFRAME_DSS-16_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 82960, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 83040, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 83120, " FRAME_1399015_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 83200, " TKFRAME_-82733_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 83280, " FRAME_-82792_NAME                =    'CASSINI_"
	    "CDA_BASE'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 83360, " FRAME_-82105_CLASS_ID            =     -0.82105"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 83440, " FRAME_PARKES_TOPO                =      0.13990"
	    "050000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 83520, " FRAME_-82765_CLASS_ID            =     -0.82765"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 83600, " TKFRAME_-82102_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 83680, " FRAME_DSS-24_TOPO                =      0.13990"
	    "240000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 83760, " TKFRAME_-82008_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 83840, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 83920, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 84000, " FRAME_CASSINI_UVIS_HDAC          =     -0.82845"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84080, " FRAME_-82810_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84160, " FRAME_-82009_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84240, " TKFRAME_-82001_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 84320, " FRAME_1399064_CLASS_ID           =      0.13990"
	    "640000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84400, " FRAME_-82791_CLASS_ID            =     -0.82791"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84480, " TKFRAME_-82812_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 84560, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 84640, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 84720, " FRAME_-82733_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84800, " FRAME_-82360_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84880, " FRAME_1399005_CENTER             =      0.39900"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 84960, " TKFRAME_-82350_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 85040, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 85120, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 85200, " TKFRAME_-82845_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 85280, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 85360, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 85440, " TKFRAME_-82378_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 85520, " FRAME_1399026_NAME               =    'DSS-26_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 85600, " FRAME_-82350_NAME                =    'CASSINI_"
	    "MAG_PLUS'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 85680, " TKFRAME_-82843_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 85760, " TKFRAME_DSS-43_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 85840, " FRAME_-82844_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 85920, " TKFRAME_-82845_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 86000, " TKFRAME_DSS-63_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 86080, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 86160, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 86240, " OBJECT_399016_FRAME              =    'DSS-16_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 86320, " FRAME_-82843_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 86400, " TKFRAME_-82103_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 86480, " TKFRAME_-82360_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 86560, " OBJECT_399045_FRAME              =    'DSS-45_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 86640, " TKFRAME_-82370_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 86720, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 86800, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 86880, " FRAME_-82368_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 86960, " TKFRAME_-82369_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 87040, " TKFRAME_-82740_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 87120, "                                         0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 87200, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 87280, " FRAME_-82105_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 87360, " TKFRAME_-82107_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 87440, " TKFRAME_-82360_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 87520, " TKFRAME_-82002_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 87600, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 87680, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 87760, " FRAME_-82106_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 87840, " TKFRAME_-82107_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 87920, " FRAME_1399055_CENTER             =      0.39905"
	    "500000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88000, " TKFRAME_DSS-27_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 88080, " FRAME_1399025_CLASS_ID           =      0.13990"
	    "250000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88160, " FRAME_-82811_NAME                =    'CASSINI_"
	    "RADAR_2'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 88240, " TKFRAME_DSS-23_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 88320, " FRAME_-82844_NAME                =    'CASSINI_"
	    "UVIS_HSP'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 88400, " FRAME_-82810_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88480, " TKFRAME_-82811_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 88560, " FRAME_DSS-43_TOPO                =      0.13990"
	    "430000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88640, " FRAME_-82893_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88720, " TKFRAME_-82840_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 88800, " FRAME_-82368_CLASS_ID            =     -0.82368"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 88880, " FRAME_1399012_NAME               =    'DSS-12_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 88960, " FRAME_31003_CLASS_ID             =      0.31003"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 89040, " FRAME_1399045_NAME               =    'DSS-45_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 89120, " TKFRAME_-82898_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 89200, " FRAME_CASSINI_RPWS_EXMINUS       =     -0.82732"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 89280, " FRAME_1399005_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 89360, " TKFRAME_-82890_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 89440, " TKFRAME_DSS-46_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 89520, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 89600, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 89680, " FRAME_-82101_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 89760, " FRAME_1399034_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 89840, " TKFRAME_-82102_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 89920, " FRAME_CASSINI_UVIS_FUV           =     -0.82840"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90000, " FRAME_-82350_CLASS_ID            =     -0.82350"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90080, " FRAME_1399063_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90160, " FRAME_CASSINI_SRU-A_RAD          =     -0.82008"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90240, " FRAME_-82732_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90320, " FRAME_-82761_NAME                =    'CASSINI_"
	    "MIMI_INCA'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 90400, " TKFRAME_DSS-61_TOPO_UNITS        =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 90480, " FRAME_1399012_CLASS_ID           =      0.13990"
	    "120000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90560, " FRAME_DSS-26_TOPO                =      0.13990"
	    "260000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90640, " FRAME_-82842_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 90720, " TKFRAME_DSS-34_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 90800, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 90880, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 90960, " TKFRAME_-82814_AXES              = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 91040, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 91120, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 91200, " FRAME_-82765_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 91280, " TKFRAME_DSS-42_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 91360, " FRAME_-82732_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 91440, " TKFRAME_-82733_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 91520, " FRAME_-82892_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 91600, " TKFRAME_-82893_UNITS             =    'DEGREES'",
	     (ftnlen)80, (ftnlen)48);
    s_copy(frtext + 91680, " FRAME_-82104_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 91760, " TKFRAME_DSS-65_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 91840, " FRAME_-82890_CLASS_ID            =     -0.82890"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 91920, " OBJECT_399064_FRAME              =    'DSS-64_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 92000, " FRAME_1399028_NAME               =    'DSS-28_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 92080, " FRAME_-82761_CLASS               =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 92160, " FRAME_-82820_CLASS_ID            =     -0.82820"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 92240, " TKFRAME_-82840_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 92320, " TKFRAME_-82001_ANGLES            = (    0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 92400, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 92480, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 92560, " TKFRAME_-82369_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 92640, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 92720, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 92800, " FRAME_-82790_CLASS               =      0.30000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 92880, " TKFRAME_-82105_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 92960, " FRAME_1399064_NAME               =    'DSS-64_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 93040, " TKFRAME_DSS-34_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 93120, " TKFRAME_-82372_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93200, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93280, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 93360, " FRAME_CASSINI_RADAR_1            =     -0.82810"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 93440, " TKFRAME_31001_MATRIX             = (    0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93520, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93600, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93680, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93760, "                                         0.10000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93840, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 93920, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 94000, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 94080, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 94160, " FRAME_1399054_CENTER             =      0.39905"
	    "400000000000E+06", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 94240, " FRAME_1399017_CLASS_ID           =      0.13990"
	    "170000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 94320, " FRAME_1399043_CLASS_ID           =      0.13990"
	    "430000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 94400, " FRAME_-82892_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 94480, " TKFRAME_DSS-26_TOPO_RELATIVE     =    'EARTH_FI"
	    "XED'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 94560, " TKFRAME_DSS-53_TOPO_SPEC         =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 94640, " TKFRAME_-82898_ANGLES            = (   -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 94720, "                                        -0.90000"
	    "000000000000E+02,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 94800, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 94880, " FRAME_-82009_NAME                =    'CASSINI_"
	    "SRU-B_RAD'", (ftnlen)80, (ftnlen)58);
    s_copy(frtext + 94960, " FRAME_CASSINI_RADAR_2            =     -0.82811"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 95040, " TKFRAME_-82892_RELATIVE          =    'CASSINI_"
	    "CIRS_FPB'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 95120, " TKFRAME_-82731_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 95200, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 95280, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 95360, " TKFRAME_-82822_RELATIVE          =    'CASSINI_"
	    "SC_COORD'", (ftnlen)80, (ftnlen)57);
    s_copy(frtext + 95440, " FRAME_-82008_CLASS_ID            =     -0.82008"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 95520, " FRAME_CASSINI_CDA                =     -0.82790"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 95600, " FRAME_-82813_NAME                =    'CASSINI_"
	    "RADAR_4'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 95680, " FRAME_DSS-12_TOPO                =      0.13990"
	    "120000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 95760, " FRAME_DSS-45_TOPO                =      0.13990"
	    "450000000000E+07", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 95840, " TKFRAME_DSS-17_TOPO_AXES         = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 95920, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 96000, "                                         0.30000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 96080, " FRAME_1399024_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 96160, " FRAME_CASSINI_RADAR_3            =     -0.82812"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 96240, " FRAME_-82368_NAME                =    'CASSINI_"
	    "ISS_NAC_RAD'", (ftnlen)80, (ftnlen)60);
    s_copy(frtext + 96320, " FRAME_1399014_NAME               =    'DSS-14_T"
	    "OPO'", (ftnlen)80, (ftnlen)52);
    s_copy(frtext + 96400, " FRAME_1399053_CLASS              =      0.40000"
	    "000000000000E+01", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 96480, " FRAME_-82371_NAME                =    'CASSINI_"
	    "VIMS_IR'", (ftnlen)80, (ftnlen)56);
    s_copy(frtext + 96560, " FRAME_CASSINI_HGA                =     -0.82101"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 96640, " CK_-82791_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 96720, " TKFRAME_-82814_ANGLES            = (    0.18219"
	    "999999999999E+03,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 96800, "                                         0.00000"
	    "000000000000E+00,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 96880, "                                         0.00000"
	    "000000000000E+00 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 96960, " TKFRAME_-82378_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 97040, " FRAME_-82731_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 97120, " TKFRAME_-82892_SPEC              =    'ANGLES'", 
	    (ftnlen)80, (ftnlen)47);
    s_copy(frtext + 97200, " CK_-82821_SPK                    =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 97280, " TKFRAME_-82101_AXES              = (    0.30000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 97360, "                                         0.20000"
	    "000000000000E+01,", (ftnlen)80, (ftnlen)65);
    s_copy(frtext + 97440, "                                         0.10000"
	    "000000000000E+01 )", (ftnlen)80, (ftnlen)66);
    s_copy(frtext + 97520, " FRAME_CASSINI_RADAR_4            =     -0.82813"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 97600, " FRAME_-82009_CENTER              =     -0.82000"
	    "000000000000E+02", (ftnlen)80, (ftnlen)64);
    s_copy(frtext + 97680, " FRAME_-82731_CLASS_ID            =     -0.82731"
	    "000000000000E+05", (ftnlen)80, (ftnlen)64);

/*     Start by checking the built-in frames. */

    zzfdat_(&c__127, &c__128, name__, idcode, center, type__, typeid, centrd, 
	    bnmlst, bnmpol, bnmnms, bnmidx, bidlst, bidpol, bidids, bididx, (
	    ftnlen)32, (ftnlen)32);
    for (i__ = 1; i__ <= 127; ++i__) {

/* ------- Case ---------------------------------------------------- */

	s_copy(xname, name__ + (((i__1 = i__ - 1) < 127 && 0 <= i__1 ? i__1 : 
		s_rnge("name", i__1, "f_ccifrm__", (ftnlen)2662)) << 5), (
		ftnlen)32, (ftnlen)32);
/* Writing concatenation */
	i__2[0] = 26, a__1[0] = "FK case #: checking frame ";
	i__2[1] = 32, a__1[1] = xname;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	namfrm_(xname, &xcode, (ftnlen)32);
	chcksi_("FRCODE", &xcode, "!=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		2);
	if (*ok) {
	    clssid = -999;
	    frinfo_(&xcode, &xcent, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		if (found) {
		    ccifrm_(&class__, &clssid, &frcode, frname, &cent, &found,
			     (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		    chcksc_("FRNAME", frname, "=", xname, ok, (ftnlen)6, (
			    ftnlen)32, (ftnlen)1, (ftnlen)32);
		    chcksi_("FRCODE", &frcode, "=", &xcode, &c__0, ok, (
			    ftnlen)6, (ftnlen)1);
		    chcksi_("CENT", &cent, "=", &xcent, &c__0, ok, (ftnlen)4, 
			    (ftnlen)1);
		}
	    }
	}
    }

/*     Load our frame kernel data into the kernel pool. */

    lmpool_(frtext, &c__1222, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__ = 1;
    gnpool_("FRAME_*_NAME", &i__, &c__1, &n, kvname, &found, (ftnlen)12, (
	    ftnlen)32);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    while(n > 0) {

/* ------- Case ---------------------------------------------------- */

/* Writing concatenation */
	i__2[0] = 26, a__1[0] = "FK case #: checking frame ";
	i__2[1] = 32, a__1[1] = kvname;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gcpool_(kvname, &c__1, &c__1, &nvals, xname, &found, (ftnlen)32, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	namfrm_(xname, &xcode, (ftnlen)32);
	chcksi_("FRCODE", &xcode, "!=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		2);
	if (*ok) {
	    clssid = -999;
	    frinfo_(&xcode, &xcent, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		if (found) {
		    ccifrm_(&class__, &clssid, &frcode, frname, &cent, &found,
			     (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		    chcksc_("FRNAME", frname, "=", xname, ok, (ftnlen)6, (
			    ftnlen)32, (ftnlen)1, (ftnlen)32);
		    chcksi_("FRCODE", &frcode, "=", &xcode, &c__0, ok, (
			    ftnlen)6, (ftnlen)1);
		    chcksi_("CENT", &cent, "=", &xcent, &c__0, ok, (ftnlen)4, 
			    (ftnlen)1);
		}
	    }
	}
	++i__;
	gnpool_("FRAME_*_NAME", &i__, &c__1, &n, kvname, &found, (ftnlen)12, (
		ftnlen)32);
    }

/*     Re-check the built-in frames. Now the kernel pool contains */
/*     a lot of frame specifications, so we'll speed things up a */
/*     bit by sampling rather than looking up each frame. */

    for (i__ = 1; i__ <= 127; i__ += 10) {

/* ------- Case ---------------------------------------------------- */

	s_copy(xname, name__ + (((i__1 = i__ - 1) < 127 && 0 <= i__1 ? i__1 : 
		s_rnge("name", i__1, "f_ccifrm__", (ftnlen)2780)) << 5), (
		ftnlen)32, (ftnlen)32);
/* Writing concatenation */
	i__2[0] = 29, a__1[0] = "FK case #: re-checking frame ";
	i__2[1] = 32, a__1[1] = xname;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	namfrm_(xname, &xcode, (ftnlen)32);
	chcksi_("FRCODE", &xcode, "!=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		2);
	if (*ok) {
	    clssid = -999;
	    frinfo_(&xcode, &xcent, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		if (found) {
		    ccifrm_(&class__, &clssid, &frcode, frname, &cent, &found,
			     (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		    chcksc_("FRNAME", frname, "=", xname, ok, (ftnlen)6, (
			    ftnlen)32, (ftnlen)1, (ftnlen)32);
		    chcksi_("FRCODE", &frcode, "=", &xcode, &c__0, ok, (
			    ftnlen)6, (ftnlen)1);
		    chcksi_("CENT", &cent, "=", &xcent, &c__0, ok, (ftnlen)4, 
			    (ftnlen)1);
		}
	    }
	}
    }

/*     Error cases: */


/* ------- Case ---------------------------------------------------- */

    tcase_("Error: frame class variable has character type", (ftnlen)46);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(badtxt, "FRAME_-999_CLASS = 'X' ", (ftnlen)80, (ftnlen)23);
    lmpool_(badtxt, &c__1, (ftnlen)80);
    ccifrm_(&c__3, &c_n1, &frcode, frname, &cent, &found, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAMEDEF)", ok, (ftnlen)22);

/* ------- Case ---------------------------------------------------- */

    tcase_("Error: frame class ID variable is missing", (ftnlen)41);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(badtxt, "FRAME_-999_CLASS = 3 ", (ftnlen)80, (ftnlen)21);
    lmpool_(badtxt, &c__1, (ftnlen)80);
    ccifrm_(&c__3, &c_n1, &frcode, frname, &cent, &found, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAMEDEF)", ok, (ftnlen)22);

/* ------- Case ---------------------------------------------------- */

    tcase_("Error: frame name variable is missing", (ftnlen)37);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(badtxt, "FRAME_-999_CLASS    = 3 ", (ftnlen)80, (ftnlen)24);
    s_copy(badtxt + 80, "FRAME_-999_CLASS_ID = -1 ", (ftnlen)80, (ftnlen)25);
    lmpool_(badtxt, &c__2, (ftnlen)80);
    ccifrm_(&c__3, &c_n1, &frcode, frname, &cent, &found, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAMEDEF)", ok, (ftnlen)22);

/* ------- Case ---------------------------------------------------- */

    tcase_("Error: frame ID variable is missing", (ftnlen)35);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(badtxt, "FRAME_-999_CLASS    = 3 ", (ftnlen)80, (ftnlen)24);
    s_copy(badtxt + 80, "FRAME_-999_CLASS_ID = -1 ", (ftnlen)80, (ftnlen)25);
    s_copy(badtxt + 160, "FRAME_-999_NAME     = 'BADFRAME' ", (ftnlen)80, (
	    ftnlen)33);
    lmpool_(badtxt, &c__3, (ftnlen)80);
    ccifrm_(&c__3, &c_n1, &frcode, frname, &cent, &found, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAMEDEF)", ok, (ftnlen)22);

/* ------- Case ---------------------------------------------------- */

    tcase_("Error: center ID variable is missing", (ftnlen)36);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(badtxt, "FRAME_-999_CLASS    = 3 ", (ftnlen)80, (ftnlen)24);
    s_copy(badtxt + 80, "FRAME_-999_CLASS_ID = -1 ", (ftnlen)80, (ftnlen)25);
    s_copy(badtxt + 160, "FRAME_-999_NAME     = 'BADFRAME' ", (ftnlen)80, (
	    ftnlen)33);
    s_copy(badtxt + 240, "FRAME_BADFRAME      = -999 ", (ftnlen)80, (ftnlen)
	    27);
    lmpool_(badtxt, &c__4, (ftnlen)80);
    ccifrm_(&c__3, &c_n1, &frcode, frname, &cent, &found, (ftnlen)32);

/*     The short error message we're looking for is signaled */
/*     by ZZDYNBID, which is called directly from CCIFRM. If */
/*     the implementation of CCIFRM changes, this check may */
/*     need to be changed as well. */

    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* ------- Case ---------------------------------------------------- */

    tcase_("Check that built-in has higher priority", (ftnlen)39);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try to override IAU_EARTH that has CLASS = 2 and CLASS_ID = 399. */

    s_copy(badtxt, "FRAME_BADFRAME      = -999 ", (ftnlen)80, (ftnlen)27);
    s_copy(badtxt + 80, "FRAME_-999_NAME     = 'BADFRAME' ", (ftnlen)80, (
	    ftnlen)33);
    s_copy(badtxt + 160, "FRAME_-999_CLASS    = 2 ", (ftnlen)80, (ftnlen)24);
    s_copy(badtxt + 240, "FRAME_-999_CLASS_ID = 399 ", (ftnlen)80, (ftnlen)26)
	    ;
    s_copy(badtxt + 320, "FRAME_-999_CENTER   = -999 ", (ftnlen)80, (ftnlen)
	    27);
    lmpool_(badtxt, &c__5, (ftnlen)80);
    ccifrm_(&c__2, &c__399, &frcode, frname, &cent, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksc_("FRNAME", frname, "=", "IAU_EARTH", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)9);
    chcksi_("FRCODE", &frcode, "=", &c__10013, &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksi_("CENT", &cent, "=", &c__399, &c__0, ok, (ftnlen)4, (ftnlen)1);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_ccifrm__ */

