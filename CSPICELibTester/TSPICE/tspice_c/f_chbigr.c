/* f_chbigr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__100 = 100;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b21 = 1e-14;
static integer c__1 = 1;
static integer c__14 = 14;
static doublereal c_b58 = 1e-10;
static doublereal c_b60 = 1e-12;

/* $Procedure F_CHBIGR ( CHBIGR tests ) */
/* Subroutine */ int f_chbigr__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer degp;
    extern /* Subroutine */ int t_chbigr__(integer *, integer *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    doublereal work[202];
    integer i__, j;
    doublereal p, x, delta;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     tcase_(char *, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmax_(void);
    integer nsamp;
    char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    doublereal cp[100];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chbigr_(
	    integer *, doublereal *, doublereal *, doublereal *, doublereal *,
	     doublereal *), chcksd_(char *, doublereal *, char *, doublereal *
	    , doublereal *, logical *, ftnlen, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    doublereal xp, itgrlp, xitgrl, x2s[2];

/* $ Abstract */

/*     Exercise the SPICELIB Chebyshev expansion integrator */
/*     routine CHBIGR. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine CHBIGR. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 19-MAY-2014 (NJB) */

/*        Bug fix: code for construction of quantity titles */
/*        for some test utility calls was missing, causing */
/*        use of wrong titles in those calls. */

/*        Bug fix: relative error test for the evaluated input Cheby */
/*        expansion was changed to absolute for a case where the */
/*        expansion has a zero crossing. */

/*        Bug fix: work space bounds were increased for consistency */
/*        with the declaration in T_CHBIGR. This update was done */
/*        to help prevent errors if this code is modified later. */
/*        The previous bounds were large enough to allow proper */
/*        operation of this routine. */


/*     Last update was 29-MAR-2014 (NJB) */

/*        Test cases that use multiple samples across */
/*        the domain have been corrected so that both halves */
/*        of the domain are sampled. */

/*        Relative error tests for values near zero have been */
/*        changed to absolute error tests. */

/*        Tolerances have been tightened for some relative */
/*        error tests. */

/*        Previous update was 23-NOV-2013. */

/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Open the test family. */

    topen_("F_CHBIGR", (ftnlen)8);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Exception: expansion degree is negative.", (ftnlen)40);
    cleard_(&c__100, cp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    degp = -1;
    cp[0] = 0.;
    x = 0.;
    x2s[0] = 0.;
    x2s[1] = 1.;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Exception: interval radius is non-positive.", (ftnlen)43);
    cleard_(&c__100, cp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    degp = 0;
    cp[0] = 0.;
    x = 0.;
    x2s[0] = 0.;
    x2s[1] = 0.;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/*     The following cases use input expansions having results that */
/*     can be verified by inspection. */


/*     Initialize the coefficient array with numbers that will cause */
/*     problems if they are incorrectly accessed. */

    d__1 = dpmax_();
    filld_(&d__1, &c__100, cp);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial is f(x) = 6x**5; interval is [10, 30]; X = 30.", (
	    ftnlen)57);

/*     Let our domain be the interval [10, 30]. */

    x2s[0] = 20.;
    x2s[1] = 10.;
    degp = 5;
    cp[0] = 0.;
    cp[1] = 3.75;
    cp[2] = 0.;
    cp[3] = 1.875;
    cp[4] = 0.;
    cp[5] = .375;

/*     Evaluate the function and its integral at X = 30. */

    x = 30.;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The indefinite integral is F(x) = x**6, defined on */
/*     the interval [-1, 1]. The result, scaled to account */
/*     for the change of variables, should be 10. */

    xitgrl = 10.;
    xp = 6.;
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial is f(x) = 6x**5; interval is [10, 30]; X = 20.", (
	    ftnlen)57);

/*     Let our domain be the interval [10, 30]. */

    x2s[0] = 20.;
    x2s[1] = 10.;
    degp = 5;
    cp[0] = 0.;
    cp[1] = 3.75;
    cp[2] = 0.;
    cp[3] = 1.875;
    cp[4] = 0.;
    cp[5] = .375;

/*     Evaluate the function and its integral at X = 20. */

    x = 20.;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The indefinite integral is F(x) = x**6, defined on */
/*     the interval [-1, 1]. The result should be 0. */

    xitgrl = 0.;
    xp = 0.;
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/*     The following cases use T_CHBIGR to produce expected results. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 0; radius is 10000, X = midpoint + radius/2."
	    , (ftnlen)65);
    degp = 0;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 0 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b60, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b60, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 1; radius is 10000, X = midpoint + radius/2."
	    , (ftnlen)65);
    degp = 1;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    cp[1] = 5.;
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 1 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Note that in this case, the function passes through 0 */
/*        at X = 94000. */

	if (j == 21) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b60, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b60, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 2; radius is 10000, X = midpoint + radius/2."
	    , (ftnlen)65);
    degp = 2;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    cp[1] = 5.;
    cp[2] = 7.;
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 2 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 3; radius is 10000, X = midpoint + radius/2."
	    , (ftnlen)65);
    degp = 3;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    cp[1] = 5.;
    cp[2] = 7.;
    cp[3] = 9.;
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 3 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b60, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 4; radius is 10000, X = midpoint - radius/2."
	    , (ftnlen)65);
    degp = 4;

/*     Use a negative value for the midpoint this time. */

    x2s[0] = -1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    cp[1] = 5.;
    cp[2] = 7.;
    cp[3] = 9.;
    cp[4] = 11.;
    x = x2s[0] - x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 4 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 5; radius is 10000, X = midpoint - radius/2."
	    , (ftnlen)65);
    degp = 5;

/*     Use a negative value for the midpoint this time. */

    x2s[0] = -1e5;
    x2s[1] = 1e4;
    cp[0] = 3.;
    cp[1] = 5.;
    cp[2] = 7.;
    cp[3] = 9.;
    cp[4] = 11.;
    cp[4] = -13.;
    x = x2s[0] - x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Continue previous degree 5 case. Sample along domain interval.", (
	    ftnlen)62);
    nsamp = 101;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 15; radius is 10000, X = midpoint + radius/"
	    "2.", (ftnlen)66);
    degp = 15;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)832)] = (doublereal) (degp / 2 - i__);
    }
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 15; radius is 10000, X = midpoint.", (ftnlen)
	    55);
    degp = 15;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)859)] = (doublereal) (degp / 2 - i__);
    }
    x = x2s[0];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use absolute error tests since results should be close to 0. */

    chcksd_("P", &p, "~", &xp, &c_b58, ok, (ftnlen)1, (ftnlen)1);
    chcksd_("ITGRLP", &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 30; radius is 10000, X = midpoint + radius/"
	    "2.", (ftnlen)66);
    degp = 30;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)888)] = (doublereal) (degp / 2 - i__);
    }
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 60; radius is 10000, X = midpoint + radius/"
	    "2.", (ftnlen)66);
    degp = 60;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)917)] = (doublereal) (degp / 2 - i__);
    }
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);
/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 10000, X = midpoint + radius/"
	    "2.", (ftnlen)66);
    degp = 98;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)944)] = (doublereal) (degp / 2 - i__);
    }
    x = x2s[0] + x2s[1] / 2;
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 10000, X = midpoint + radius.",
	     (ftnlen)64);
    degp = 98;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)973)] = (doublereal) (degp / 2 - i__);
    }

/*     Note the abscissa is at the right endpoint of the domain. */

    x = x2s[0] + x2s[1];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 10000, X = midpoint - radius.",
	     (ftnlen)64);
    degp = 98;
    x2s[0] = 1e5;
    x2s[1] = 1e4;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1004)] = (doublereal) (degp / 2 - i__);
    }

/*     Note the abscissa is at the left endpoint of the domain. */

    x = x2s[0] - x2s[1];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 1000000, X = midpoint + radiu"
	    "s.", (ftnlen)66);
    degp = 98;
    x2s[0] = 1e6;
    x2s[1] = 1e6;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1035)] = (doublereal) (degp / 2 - i__);
    }

/*     Note the abscissa is at the right endpoint of the domain. */

    x = x2s[0] + x2s[1];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 1000000, X = midpoint - radiu"
	    "s.", (ftnlen)66);
    degp = 98;
    x2s[0] = 1e6;
    x2s[1] = 1e6;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1067)] = (doublereal) (degp / 2 - i__);
    }

/*     Note the abscissa is at the left endpoint of the domain. */

    x = x2s[0] - x2s[1];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("P", &p, "~/", &xp, &c_b21, ok, (ftnlen)1, (ftnlen)2);
    chcksd_("ITGRLP", &itgrlp, "~/", &xitgrl, &c_b21, ok, (ftnlen)6, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 1000000, X = midpoint.", (
	    ftnlen)57);
    degp = 98;
    x2s[0] = 1e6;
    x2s[1] = 1e6;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1098)] = (doublereal) (degp / 2 - i__);
    }

/*     Note the abscissa is at the left endpoint of the domain. */

    x = x2s[0];
    chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use absolute error tests since results should be close to 0. */

    chcksd_("P", &p, "~", &xp, &c_b58, ok, (ftnlen)1, (ftnlen)1);
    chcksd_("ITGRLP", &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 15; radius is 1000000, sample results along"
	    " the domain.", (ftnlen)76);
    degp = 15;
    x2s[0] = 1e6;
    x2s[1] = 1e6;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1132)] = (doublereal) (degp / 2 - i__);
    }
    nsamp = 10001;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree is 98; radius is 1000000, Sample results along"
	    " the domain.", (ftnlen)76);
    degp = 98;
    x2s[0] = 1e6;
    x2s[1] = 1e6;
    i__1 = degp + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("cp", i__2, 
		"f_chbigr__", (ftnlen)1195)] = (doublereal) (degp / 2 - i__);
    }
    nsamp = 10001;
    delta = x2s[1] * 2 / (nsamp - 1);
    i__1 = nsamp;
    for (j = 1; j <= i__1; ++j) {
	x = x2s[0] - x2s[1] + (j - 1) * delta;
	chbigr_(&degp, cp, x2s, &x, &p, &itgrlp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chbigr__(&c__1, &degp, cp, x2s, &x, work, &xp, &xitgrl);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "P at @", (ftnlen)320, (ftnlen)6);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &p, "~", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)1);
	} else {
	    chcksd_(title, &p, "~/", &xp, &c_b58, ok, (ftnlen)320, (ftnlen)2);
	}
	s_copy(title, "ITGRLP at @", (ftnlen)320, (ftnlen)11);
	repmd_(title, "@", &x, &c__14, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (j == nsamp / 2 + 1) {

/*           Use absolute error tests since results should be close to */
/*           0. */

	    chcksd_(title, &itgrlp, "~", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)1);
	} else {
	    chcksd_(title, &itgrlp, "~/", &xitgrl, &c_b58, ok, (ftnlen)320, (
		    ftnlen)2);
	}
    }
/*      WRITE (*,*) 'P       = ', P */
/*      WRITE (*,*) 'XP      = ', XP */
/*      WRITE (*,*) 'ITGRLP  = ', ITGRLP */
/*      WRITE (*,*) 'XITGRL  = ', XITGRL */

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_chbigr__ */

