/* f_ck06.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__4 = 4;
static doublereal c_b31 = .5;
static logical c_true = TRUE_;
static integer c__0 = 0;
static doublereal c_b110 = 0.;
static integer c__3 = 3;
static doublereal c_b189 = 1.;
static integer c__1 = 1;
static integer c__9 = 9;
static doublereal c_b333 = 1e-14;
static doublereal c_b587 = 1e-6;
static integer c__2 = 2;
static integer c__6 = 6;
static integer c_n2000 = -2000;
static integer c_n1000 = -1000;
static doublereal c_b1195 = 1e-8;
static doublereal c_b1202 = -.5;
static doublereal c_b1282 = 1e-10;

/* $Procedure F_CK06 ( CK data type 06 tests ) */
/* Subroutine */ int f_ck06__(logical *ok)
{
    /* Initialized data */

    static integer pktszs[4] = { 8,4,14,7 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static char name__[80];
    static doublereal cmat[9]	/* was [3][3] */;
    extern /* Subroutine */ int ckgp_(integer *, doublereal *, doublereal *, 
	    char *, doublereal *, doublereal *, logical *, ftnlen), ckr06_(
	    integer *, doublereal *, doublereal *, doublereal *, logical *, 
	    doublereal *, logical *);
    static integer nrec;
    static doublereal qneg[4], rate;
    static integer nseg;
    extern logical even_(integer *);
    extern /* Subroutine */ int ckw06_(integer *, integer *, char *, logical *
	    , doublereal *, doublereal *, char *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen);
    static integer jmax;
    static doublereal davv[3], xrec[200], last;
    static char xref[32];
    static integer skip;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), t_gentag__(integer *, integer *, doublereal *);
    static integer size;
    extern /* Subroutine */ int t_gencsm__(integer *, integer *, integer *, 
	    integer *, char *, doublereal *, integer *, doublereal *, 
	    doublereal *, doublereal *, logical *, ftnlen);
    static doublereal avvs[2520000]	/* was [3][840000] */;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), t_xsubtp__(
	    doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    doublereal *);
    static integer i__, j, k, m, n;
    static doublereal t;
    extern /* Subroutine */ int dafgs_(doublereal *);
    static doublereal delta;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *);
    static char segid[60];
    extern /* Subroutine */ int ckgr06_(integer *, doublereal *, integer *, 
	    integer *, doublereal *), dafps_(integer *, integer *, doublereal 
	    *, integer *, doublereal *), cknm06_(integer *, doublereal *, 
	    integer *), tcase_(char *, ftnlen);
    static doublereal descr[5];
    extern /* Subroutine */ int ckcls_(integer *), ckmp06_(integer *, 
	    doublereal *, integer *, doublereal *, integer *, integer *, 
	    integer *, doublereal *, doublereal *), dafus_(doublereal *, 
	    integer *, integer *, doublereal *, integer *), repmc_(char *, 
	    char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer nmini, segno;
    static doublereal cktol, rates[21000];
    static char title[800];
    static doublereal xcmat[9]	/* was [3][3] */;
    static logical found;
    extern /* Subroutine */ int ckopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), topen_(char *, ftnlen), swapd_(doublereal *, 
	    doublereal *);
    static char xrefs[32*3];
    static doublereal first, xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static doublereal quats[3360000]	/* was [4][840000] */;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer npkts[21000];
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *);
    static doublereal t0, t1;
    static integer xinst;
    static doublereal xavvs[2520000]	/* was [3][840000] */;
    extern /* Subroutine */ int t_success__(logical *), qdq2av_(doublereal *, 
	    doublereal *, doublereal *);
    static doublereal dc[2];
    extern /* Subroutine */ int xf2rav_(doublereal *, doublereal *, 
	    doublereal *);
    static integer ic[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static char ck[255];
    extern /* Subroutine */ int daffna_(logical *), dafbfs_(integer *);
    static doublereal av[3], et;
    static integer begrec, handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), dafcls_(
	    integer *), delfil_(char *, ftnlen), chcksd_(char *, doublereal *,
	     char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static char ckfram[32];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    static doublereal avbuff[2520000]	/* was [3][840000] */, xq[4];
    static integer degres[21000];
    static doublereal xt;
    static integer datidx[21000];
    static doublereal record[200], sclkdp[840000], epochs[840000];
    static integer pktbeg[21000];
    static doublereal ivlbds[21001], packts[6720000], xepoch[840000];
    static logical xavflg;
    extern /* Subroutine */ int dafopr_(char *, integer *, ftnlen);
    static doublereal lstepc;
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    static integer nivals[3];
    static doublereal clkout, xcmats[7560000]	/* was [9][840000] */, xivbds[
	    21001];
    static integer mnsgno, rectot, nintvl, segptr;
    static logical sellst;
    static integer psizes[21000];
    extern /* Subroutine */ int setmsg_(char *, ftnlen);
    extern logical exists_(char *, ftnlen);
    static integer subtps[21000];
    extern /* Subroutine */ int errint_(char *, integer *, ftnlen), vminug_(
	    doublereal *, integer *, doublereal *);
    static integer winsiz;
    static doublereal xquats[3360000]	/* was [4][840000] */;
    static integer pktptr, pktsiz, pkttot, subtyp, xinsts[3];
    extern /* Subroutine */ int dafopw_(char *, integer *, ftnlen), furnsh_(
	    char *, ftnlen), q2m_(doublereal *, doublereal *), unload_(char *,
	     ftnlen), ckgpav_(integer *, doublereal *, doublereal *, char *, 
	    doublereal *, doublereal *, doublereal *, logical *, ftnlen), 
	    t_ckw06__(integer *, integer *, char *, logical *, doublereal *, 
	    doublereal *, char *, integer *, integer *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *,
	     ftnlen, ftnlen);
    static integer xwnsiz;
    extern /* Subroutine */ int sxform_(char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), m2q_(doublereal *, doublereal *);
    static integer pad;
    extern /* Subroutine */ int t_genc06__(integer *, integer *, integer *, 
	    integer *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen);
    static doublereal avq[4], xdq[4], tol, xav[3];
    extern /* Subroutine */ int qxq_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     Exercise routines associated with CK data type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests routines that write and read type 06 CK */
/*     data. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 11-AUG-2015 (NJB) */

/*        Added test case for CKW06 invalid input SCLK rate. */

/* -    TSPICE Version 1.0.0, 24-MAR-2014 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_CK06", (ftnlen)6);
/* ***************************************************************** */
/* * */
/* *    CKW06 error cases: */
/* * */
/* ***************************************************************** */

/*     Test CKW06:  start out with error handling. */


/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 setup", (ftnlen)11);

/*     Initialize the inputs to CKW06. */

    xinst = 3;
    xavflg = TRUE_;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "CK type 06 test segment", (ftnlen)60, (ftnlen)23);

/*     Open a new CK file. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nintvl = 1;
    npkts[0] = 2;
    rates[0] = 1.;
    subtps[0] = 1;
    degres[0] = 1;
    rectot = 0;
    pkttot = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Update total counts of records and packet elements. */
/*        The latter depends on the packet size of the current */
/*        mini-segment. */

	if (subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"subtps", i__2, "f_ck06__", (ftnlen)356)] == 0) {
	    pktsiz = 8;
	} else if (subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
		s_rnge("subtps", i__2, "f_ck06__", (ftnlen)360)] == 1) {
	    pktsiz = 4;
	} else {
	    setmsg_("Unexpected CK type 06 subtype # found in mini-segment #."
		    , (ftnlen)56);
	    errint_("#", &subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 
		    : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)368)], (
		    ftnlen)1);
	    errint_("#", &i__, (ftnlen)1);
	    sigerr_("SPICE(INVALIDSUBTYPE)", (ftnlen)21);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	pkttot += npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_ck06__", (ftnlen)377)] * pktsiz;
	rectot += npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_ck06__", (ftnlen)379)];
    }
    filld_(&c_b31, &pkttot, packts);
    i__1 = rectot;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)388)] = (doublereal) i__;
    }
    first = sclkdp[0];
    last = sclkdp[1];
    ivlbds[0] = first;
    ivlbds[1] = last;
    sellst = TRUE_;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: descriptor times swapped.", (ftnlen)43);
    ckw06_(&handle, &xinst, xref, &xavflg, &last, &first, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: bad frame name.", (ftnlen)33);
    ckw06_(&handle, &xinst, "SPUD", &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)4, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: SEGID too long.", (ftnlen)33);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, "X                "
	    "                               X", &nintvl, npkts, subtps, degres,
	     packts, rates, sclkdp, ivlbds, &sellst, (ftnlen)32, (ftnlen)49);
    chckxc_(&c_true, "SPICE(SEGIDTOOLONG)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: unprintable SEGID characters.", (ftnlen)47);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, "\a", &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: non-positive mini-segment count", (ftnlen)49);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &c__0, npkts,
	     subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    nintvl = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: non-increasing mini-segment bounds", (ftnlen)52)
	    ;
    ivlbds[1] = ivlbds[0];
    first = 1.5;
    last = 1.6;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    ivlbds[0] = 1.;
    ivlbds[1] = 2.;
    first = ivlbds[0];
    last = ivlbds[1];

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: descriptor  start time is too early.", (ftnlen)
	    54);
    d__1 = first - 1.;
    ckw06_(&handle, &xinst, xref, &xavflg, &d__1, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: descriptor end time is too late.", (ftnlen)50);
    d__1 = last + 1.;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &d__1, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: too few packets", (ftnlen)33);
    npkts[0] = 1;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TOOFEWPACKETS)", ok, (ftnlen)20);
    npkts[0] = 2;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: invalid SCLK rate.", (ftnlen)36);
    rates[0] = 0.;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDSCLKRATE)", ok, (ftnlen)22);
    rates[0] = -1.;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDSCLKRATE)", ok, (ftnlen)22);
    rates[0] = 1.;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: invalid subtype.", (ftnlen)34);
    subtps[0] = -1;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDSUBTYPE)", ok, (ftnlen)21);
    subtps[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: polynomial degree too high.", (ftnlen)45);
    degres[0] = 40;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: polynomial degree too low.", (ftnlen)44);
    degres[0] = 0;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case:  odd window size.", (ftnlen)35);
    degres[0] = 2;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADWINDOWSIZE)", ok, (ftnlen)20);
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: epochs for first mini-segment are out of order."
	    , (ftnlen)65);
    nintvl = 1;
    npkts[0] = 4;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)836)] = (doublereal) i__;
    }
    first = sclkdp[0];
    last = sclkdp[(i__1 = npkts[0] - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)840)];
    ivlbds[0] = first;
    ivlbds[1] = last;
    swapd_(&sclkdp[1], &sclkdp[2]);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TIMESOUTOFORDER)", ok, (ftnlen)22);
    swapd_(&sclkdp[1], &sclkdp[2]);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: first mini-segment epoch follows interval star"
	    "t.", (ftnlen)66);
    sclkdp[0] = ivlbds[0] + .1;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSDISAGREE)", ok, (ftnlen)21);
    sclkdp[0] = 1.;

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: last mini-segment epoch precedes interval star"
	    "t.", (ftnlen)66);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)907)] = (doublereal) i__ - npkts[
		0] - 1.;
    }
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSDISAGREE)", ok, (ftnlen)21);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)930)] = (doublereal) i__;
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 *non* error case: epochs for first mini-segment don't cove"
	    "r first mini-segment interval. A gap exists at the end of the fi"
	    "rst mini-segment.", (ftnlen)145);
    nintvl = 1;
    npkts[0] = 4;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)946)] = (doublereal) i__;
    }
    first = sclkdp[0];
    last = sclkdp[(i__1 = npkts[0] - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)950)];
    ivlbds[0] = first;
    ivlbds[1] = last;
    sclkdp[3] += -.5;
    pkttot = npkts[0] << 2;
    filld_(&c_b31, &pkttot, packts);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)981)] = (doublereal) i__;
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: zero quaternion.", (ftnlen)34);
    filld_(&c_b110, &c__4, packts);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, ivlbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(ZEROQUATERNION)", ok, (ftnlen)21);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp"
		, i__2, "f_ck06__", (ftnlen)1013)] = (doublereal) i__;
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: quaternion sign error for subtype 0.", (ftnlen)
	    54);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    rate = rates[0];
    subtps[0] = 0;
    subtyp = subtps[0];
    pktsiz = 8;
    degres[0] = 3;
    t_gencsm__(&segno, &mnsgno, &begrec, &npkts[(i__1 = mnsgno - 1) < 21000 &&
	     0 <= i__1 ? i__1 : s_rnge("npkts", i__1, "f_ck06__", (ftnlen)
	    1047)], xref, &rate, &subtyp, sclkdp, quats, avbuff, ok, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)1053)];
    first = xivbds[0];
    last = xivbds[1];
    pktptr = 1;
    cleard_(&c__3, davv);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1066)], &
		avbuff[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avbuff", i__3, "f_ck06__", (ftnlen)1066)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		1066)]);

/*        Create error in quaternion sequence at packet index 5: */

	if (i__ == 5) {
	    vminug_(&packts[(i__2 = pktptr - 1) < 6720000 && 0 <= i__2 ? i__2 
		    : s_rnge("packts", i__2, "f_ck06__", (ftnlen)1073)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__2 = pktptr - 1) < 6720000 && 0 <= 
		    i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		    1074)]);
	}
    }
    s_copy(segid, "Subtype 0 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADQUATSIGN)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 error case: quaternion sign error for subtype 2.", (ftnlen)
	    54);

/*     Use the inputs from the previous case, where possible. */

    subtps[0] = 2;
    pktsiz = 14;
    pktptr = 1;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1121)], &
		avbuff[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avbuff", i__3, "f_ck06__", (ftnlen)1121)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		1121)]);

/*        Create error in quaternion sequence at packet index 6: */

	if (i__ == 6) {
	    vminug_(&packts[(i__2 = pktptr - 1) < 6720000 && 0 <= i__2 ? i__2 
		    : s_rnge("packts", i__2, "f_ck06__", (ftnlen)1128)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__2 = pktptr - 1) < 6720000 && 0 <= 
		    i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		    1129)]);
	}
    }
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADQUATSIGN)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 non-error case: quaternion negation for subtype 1. Make su"
	    "re no error is signaled.", (ftnlen)88);

/*     Use the inputs from the previous case, where possible. */

    subtps[0] = 1;
    subtyp = subtps[0];
    pktsiz = 4;
    pktptr = 1;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1172)], &
		avbuff[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avbuff", i__3, "f_ck06__", (ftnlen)1172)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		1172)]);

/*        Create error in quaternion sequence at packet index 7: */

	if (i__ == 7) {
	    vminug_(&packts[(i__2 = pktptr - 1) < 6720000 && 0 <= i__2 ? i__2 
		    : s_rnge("packts", i__2, "f_ck06__", (ftnlen)1179)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__2 = pktptr - 1) < 6720000 && 0 <= 
		    i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		    1180)]);
	}
    }
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("CKW06 non-error case: quaternion negation for subtype 3. Make su"
	    "re no error is signaled.", (ftnlen)88);

/*     Use the inputs from the previous case, where possible. */

    subtps[0] = 3;
    subtyp = subtps[0];
    pktsiz = 7;
    pktptr = 1;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1223)], &
		avbuff[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avbuff", i__3, "f_ck06__", (ftnlen)1223)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		1223)]);

/*        Create error in quaternion sequence at packet index 8: */

	if (i__ == 8) {
	    vminug_(&packts[(i__2 = pktptr - 1) < 6720000 && 0 <= i__2 ? i__2 
		    : s_rnge("packts", i__2, "f_ck06__", (ftnlen)1230)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__2 = pktptr - 1) < 6720000 && 0 <= 
		    i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		    1231)]);
	}
    }
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    End of CKW06 error cases: */
/* * */
/* ***************************************************************** */

/*     Close the CK file at the DAF level; CKCLS won't close */
/*     a file without segments. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKW06 Non-error cases: */
/* * */
/* ***************************************************************** */
/* ***************************************************************** */
/* * */
/* *    Trivial case: write an CK containing a small subtype 1 */
/* *    segment. */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Test CKW06: create trivial subtype 1 segment.", (ftnlen)45);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, packts, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = 1;
    degres[0] = 3;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)1313)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("ck06t1.bc", (ftnlen)9)) {
	delfil_("ck06t1.bc", (ftnlen)9);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("ck06t1.bc", "ck06t1.bc", &c__0, &handle, (ftnlen)9, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read data records from trivial subtype 1 segment", (ftnlen)48);
    dafopr_("ck06t1.bc", &handle, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get mini-segment count. */

    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cknm06_(&handle, descr, &nmini);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckmp06_(&handle, descr, &c__1, &rate, &subtyp, &winsiz, &nrec, ivlbds, &
	    lstepc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktsiz = 4;
    i__1 = nrec;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgr06_(&handle, descr, &c__1, &i__, record);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the record. The time tag is the first element. */
/*        We expect exact matches for all elements. */

	s_copy(name__, "Time tag *", (ftnlen)80, (ftnlen)10);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, record, "=", &sclkdp[(i__2 = i__ - 1) < 840000 && 0 <=
		 i__2 ? i__2 : s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)
		1405)], &c_b110, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Subtype code *", (ftnlen)80, (ftnlen)14);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = (doublereal) subtps[0];
	chcksd_(name__, &record[1], "=", &d__1, &c_b110, ok, (ftnlen)80, (
		ftnlen)1);
	s_copy(name__, "Clock rate *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, &record[2], "=", rates, &c_b110, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check the record data. */

	s_copy(name__, "Quaternion *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	k = pktsiz * (i__ - 1) + 1;
	chckad_(name__, &record[3], "=", &packts[(i__2 = k - 1) < 6720000 && 
		0 <= i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		1429)], &pktsiz, &c_b110, ok, (ftnlen)80, (ftnlen)1);
    }

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read data records from trivial subtype 1 segment. Open CK for wr"
	    "ite access but don't modify the file.", (ftnlen)101);
    dafopw_("ck06t1.bc", &handle, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get mini-segment count. */

    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cknm06_(&handle, descr, &nmini);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckmp06_(&handle, descr, &c__1, &rate, &subtyp, &winsiz, &nrec, ivlbds, &
	    lstepc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktsiz = 4;
    i__1 = nrec;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgr06_(&handle, descr, &c__1, &i__, record);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the record. The time tag is the first element. */
/*        We expect exact matches for all elements. */

	s_copy(name__, "Time tag *", (ftnlen)80, (ftnlen)10);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, record, "=", &sclkdp[(i__2 = i__ - 1) < 840000 && 0 <=
		 i__2 ? i__2 : s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)
		1487)], &c_b110, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Subtype code *", (ftnlen)80, (ftnlen)14);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = (doublereal) subtps[0];
	chcksd_(name__, &record[1], "=", &d__1, &c_b110, ok, (ftnlen)80, (
		ftnlen)1);
	s_copy(name__, "Clock rate *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, &record[2], "=", rates, &c_b110, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check the record data. */

	s_copy(name__, "Quaternion *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	k = pktsiz * (i__ - 1) + 1;
	chckad_(name__, &record[3], "=", &packts[(i__2 = k - 1) < 6720000 && 
		0 <= i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (ftnlen)
		1511)], &pktsiz, &c_b110, ok, (ftnlen)80, (ftnlen)1);
    }

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read from trivial subtype 1 segment using CKGP", (ftnlen)46);
    furnsh_("ck06t1.bc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cktol = 0.;
    tol = 1e-14;
    i__1 = nrec;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgp_(&xinst, &sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : 
		s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)1539)], &cktol, 
		xref, cmat, &clkout, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Make sure data were found. */

	s_copy(name__, "FOUND *", (ftnlen)80, (ftnlen)7);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(name__, &found, &c_true, ok, (ftnlen)80);

/*        Check the output time. */

	s_copy(name__, "CLKOUT *", (ftnlen)80, (ftnlen)8);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, &clkout, "=", &sclkdp[(i__2 = i__ - 1) < 840000 && 0 
		<= i__2 ? i__2 : s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)
		1558)], &c_b110, ok, (ftnlen)80, (ftnlen)1);

/*        Check the C-matrix. Some round-off will occur here. */

	s_copy(name__, "CMAT *", (ftnlen)80, (ftnlen)6);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	k = pktsiz * (i__ - 1) + 1;
	q2m_(&packts[(i__2 = k - 1) < 6720000 && 0 <= i__2 ? i__2 : s_rnge(
		"packts", i__2, "f_ck06__", (ftnlen)1569)], xcmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
    }
    unload_("ck06t1.bc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     Tests involving segment containing mini-segments having */
/* *     varied attributes */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Test CKW06: create segment containing mini-segments of all subty"
	    "pes.", (ftnlen)68);

/*     Each mini-segment will have a gap at the end. */

    xinst = -1000;
    xavflg = TRUE_;
    s_copy(xref, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(ckfram, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    nintvl = 4;
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;
    segno = 1;
    n = 0;

/*     Below, we'll use different clock rates for each mini-segment. But */
/*     where we can,we'll adjust the angular velocity and quaternion */
/*     derivatives for the Hermite types so that the angular velocities */
/*     match when converted to radians/s. */

/*     Where applicable, angular velocity will be scaled by the */
/*     corresponding rate. This will allow recovery of the original */
/*     angular velocity after conversion to radians/s. */


/*     Pointer into packet array. */

    pktptr = 1;

/*     Mini-segment 1: */

    mnsgno = 1;
    begrec = 1;
    npkts[0] = 11;
    subtps[0] = 0;
    pktsiz = 8;
    psizes[0] = pktsiz;
    degres[0] = 3;
    rates[0] = .001;
    t_genc06__(&segno, &mnsgno, &begrec, npkts, xref, rates, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xivbds[0] = sclkdp[0];
    first = xivbds[0];

/*     Create quaternion derivatives from quaternions and avvs. */
/*     Pack quaternions and their derivatives into the PACKTS array. */

    cleard_(&c__3, davv);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        We're not using the derivative of angular velocity for */
/*        this subtype. */

	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1654)], &
		avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avvs", i__3, "f_ck06__", (ftnlen)1654)], davv, subtps,
		 &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <= i__4 ?
		 i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)1654)]);
	pktptr += pktsiz;
    }

/*     Mini-segment 2: */

    mnsgno = 2;
    begrec += npkts[0];
    npkts[1] = 12;
    subtps[1] = 1;
    pktsiz = 4;
    psizes[1] = pktsiz;
    degres[1] = 7;
    rates[1] = .002;
    t_genc06__(&segno, &mnsgno, &begrec, &npkts[1], xref, &rates[1], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     No AVV scaling is needed because the vector is */
/*     already considered to have units of radians/s. */

    moved_(epochs, &npkts[1], &sclkdp[(i__1 = pktptr - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("sclkdp", i__1, "f_ck06__", (ftnlen)1681)]);
    xivbds[1] = sclkdp[(i__1 = begrec - 1) < 840000 && 0 <= i__1 ? i__1 : 
	    s_rnge("sclkdp", i__1, "f_ck06__", (ftnlen)1683)];

/*     This mini-segment has subtype 1; we can just */
/*     transfer the quaternions into the packet array. */

    i__2 = npkts[1] << 2;
    moved_(quats, &i__2, &packts[(i__1 = pktptr - 1) < 6720000 && 0 <= i__1 ? 
	    i__1 : s_rnge("packts", i__1, "f_ck06__", (ftnlen)1689)]);
    pktptr += npkts[1] * pktsiz;

/*     Mini-segment 3: */

    mnsgno = 3;
    begrec += npkts[1];
    npkts[2] = 40;
    subtps[2] = 2;
    pktsiz = 14;
    psizes[2] = pktsiz;
    degres[2] = 23;
    rates[2] = .003;
    t_genc06__(&segno, &mnsgno, &begrec, &npkts[2], xref, &rates[2], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    moved_(epochs, &npkts[2], &sclkdp[(i__1 = begrec - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("sclkdp", i__1, "f_ck06__", (ftnlen)1710)]);
    xivbds[2] = sclkdp[(i__1 = begrec - 1) < 840000 && 0 <= i__1 ? i__1 : 
	    s_rnge("sclkdp", i__1, "f_ck06__", (ftnlen)1712)];

/*     Create quaternion derivatives from quaternions and avvs. */
/*     Pack quaternions and their derivatives, as well as the */
/*     avvs and their derivatives, into the PACKTS array. */

    i__1 = npkts[2];
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        We need to generate a derivative of the AVV. We'll */
/*        compute a 2-sided derivative if one's available; */
/*        otherwise we'll use a 1-sided derivative. */

	if (i__ == 1) {
	    delta = (sclkdp[1] - sclkdp[0]) * rates[2];
	    d__1 = 1. / delta;
	    d__2 = -1. / delta;
	    vlcom_(&d__1, &avvs[(i__2 = (i__ + 1) * 3 - 3) < 2520000 && 0 <= 
		    i__2 ? i__2 : s_rnge("avvs", i__2, "f_ck06__", (ftnlen)
		    1729)], &d__2, &avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 
		    <= i__3 ? i__3 : s_rnge("avvs", i__3, "f_ck06__", (ftnlen)
		    1729)], davv);
	} else if (i__ < npkts[2]) {
	    delta = (sclkdp[(i__2 = i__) < 840000 && 0 <= i__2 ? i__2 : 
		    s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)1735)] - 
		    sclkdp[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 : 
		    s_rnge("sclkdp", i__3, "f_ck06__", (ftnlen)1735)]) * 
		    rates[2] / 2;
	    d__1 = 1. / delta;
	    d__2 = -1. / delta;
	    vlcom_(&d__1, &avvs[(i__2 = (i__ + 1) * 3 - 3) < 2520000 && 0 <= 
		    i__2 ? i__2 : s_rnge("avvs", i__2, "f_ck06__", (ftnlen)
		    1737)], &d__2, &avvs[(i__3 = (i__ - 1) * 3 - 3) < 2520000 
		    && 0 <= i__3 ? i__3 : s_rnge("avvs", i__3, "f_ck06__", (
		    ftnlen)1737)], davv);
	} else {
	    delta = (sclkdp[(i__2 = i__ - 1) < 840000 && 0 <= i__2 ? i__2 : 
		    s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)1742)] - 
		    sclkdp[(i__3 = i__ - 2) < 840000 && 0 <= i__3 ? i__3 : 
		    s_rnge("sclkdp", i__3, "f_ck06__", (ftnlen)1742)]) * 
		    rates[2];
	    d__1 = 1. / delta;
	    d__2 = -1. / delta;
	    vlcom_(&d__1, &avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? 
		    i__2 : s_rnge("avvs", i__2, "f_ck06__", (ftnlen)1744)], &
		    d__2, &avvs[(i__3 = (i__ - 1) * 3 - 3) < 2520000 && 0 <= 
		    i__3 ? i__3 : s_rnge("avvs", i__3, "f_ck06__", (ftnlen)
		    1744)], davv);
	}
	t_xsubtp__(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? 
		i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)1749)], &
		avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avvs", i__3, "f_ck06__", (ftnlen)1749)], davv, &
		subtps[2], &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 
		0 <= i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		1749)]);
	pktptr += pktsiz;
    }

/*     Mini-segment 4: */

    mnsgno = 4;
    begrec += npkts[2];
    npkts[3] = 50;
    subtps[3] = 3;
    pktsiz = 7;
    psizes[3] = pktsiz;
    degres[3] = 23;
    rates[3] = .004;
    t_genc06__(&segno, &mnsgno, &begrec, &npkts[3], xref, &rates[3], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xivbds[3] = epochs[0];
    xivbds[4] = epochs[(i__1 = npkts[3] - 1) < 840000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_ck06__", (ftnlen)1775)] + 1.;
    last = xivbds[4];
    moved_(epochs, &npkts[3], &sclkdp[(i__1 = begrec - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("sclkdp", i__1, "f_ck06__", (ftnlen)1778)]);

/*     Copy both quaternions and avvs into the packet array. */

    i__1 = npkts[3];
    for (i__ = 1; i__ <= i__1; ++i__) {
	moved_(&quats[(i__2 = (i__ << 2) - 4) < 3360000 && 0 <= i__2 ? i__2 : 
		s_rnge("quats", i__2, "f_ck06__", (ftnlen)1785)], &c__4, &
		packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 : 
		s_rnge("packts", i__3, "f_ck06__", (ftnlen)1785)]);
	pktptr += 4;
	moved_(&avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)1788)], &c__3, &
		packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 : 
		s_rnge("packts", i__3, "f_ck06__", (ftnlen)1788)]);
	pktptr += 3;
    }

/*     Open new C-kernel. */

    if (exists_("ck06mult.bc", (ftnlen)11)) {
	delfil_("ck06mult.bc", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("ck06mult.bc", "ck06mult.bc", &c__0, &handle, (ftnlen)11, (ftnlen)
	    11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKNM06, CKMP06, CKGR06 Non-error cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Examine the segment in the file CK06MU.", (ftnlen)39);
    dafopr_("ck06mult.bc", &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the mini-segment count: */

    cknm06_(&handle, descr, &nmini);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NMINI", &nmini, "=", &c__4, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the mini-segment parameters: */

    begrec = 1;
    pktptr = 1;
    i__1 = nmini;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckmp06_(&handle, descr, &i__, &rate, &subtyp, &winsiz, &nrec, ivlbds, 
		&lstepc);
	s_copy(name__, "Clock rate *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(name__, &rate, "=", &rates[(i__2 = i__ - 1) < 21000 && 0 <= 
		i__2 ? i__2 : s_rnge("rates", i__2, "f_ck06__", (ftnlen)1881)]
		, &c_b110, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Subtype code *", (ftnlen)80, (ftnlen)14);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(name__, &subtyp, "=", &subtps[(i__2 = i__ - 1) < 21000 && 0 <=
		 i__2 ? i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
		1887)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Packet count *", (ftnlen)80, (ftnlen)14);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(name__, &nrec, "=", &npkts[(i__2 = i__ - 1) < 21000 && 0 <= 
		i__2 ? i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)1893)]
		, &c__0, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Window size *", (ftnlen)80, (ftnlen)13);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (subtyp == 0 || subtyp == 2) {
	    xwnsiz = (degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("degres", i__2, "f_ck06__", (ftnlen)1901)] + 1) / 
		    2;
	} else {
	    xwnsiz = degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("degres", i__2, "f_ck06__", (ftnlen)1903)] + 1;
	}
	chcksi_(name__, &winsiz, "=", &xwnsiz, &c__0, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(name__, "Interval start *", (ftnlen)80, (ftnlen)16);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xt = xivbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge(
		"xivbds", i__2, "f_ck06__", (ftnlen)1912)];
	chcksd_(name__, ivlbds, "=", &xt, &c_b110, ok, (ftnlen)80, (ftnlen)1);
	s_copy(name__, "Interval stop *", (ftnlen)80, (ftnlen)15);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xt = xivbds[(i__2 = i__) < 21001 && 0 <= i__2 ? i__2 : s_rnge("xivbds"
		, i__2, "f_ck06__", (ftnlen)1920)];
	chcksd_(name__, &ivlbds[1], "=", &xt, &c_b110, ok, (ftnlen)80, (
		ftnlen)1);
	s_copy(name__, "Last epoch *", (ftnlen)80, (ftnlen)12);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xt = sclkdp[(i__3 = begrec + npkts[(i__2 = i__ - 1) < 21000 && 0 <= 
		i__2 ? i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)1928)]
		 - 2) < 840000 && 0 <= i__3 ? i__3 : s_rnge("sclkdp", i__3, 
		"f_ck06__", (ftnlen)1928)];
	chcksd_(name__, &lstepc, "=", &xt, &c_b110, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check the packets. */

	pktsiz = psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"psizes", i__2, "f_ck06__", (ftnlen)1935)];
	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_ck06__", (ftnlen)1937)];
	for (j = 1; j <= i__3; ++j) {
	    ckgr06_(&handle, descr, &i__, &j, record);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    k = pktptr + (j - 1) * pktsiz;

/*           Check the record's time tag. */

	    s_copy(name__, "Mini-segment *; record time tag *", (ftnlen)80, (
		    ftnlen)33);
	    repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xt = sclkdp[(i__2 = begrec + j - 2) < 840000 && 0 <= i__2 ? i__2 :
		     s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)1954)];
	    chcksd_(name__, record, "=", &xt, &c_b110, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the subtype and rate. */

	    s_copy(name__, "Mini-segment *; record subtype *", (ftnlen)80, (
		    ftnlen)32);
	    repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    i__2 = i_dnnt(&record[1]);
	    chcksi_(name__, &i__2, "=", &subtyp, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    s_copy(name__, "Mini-segment *; record rate *", (ftnlen)80, (
		    ftnlen)29);
	    repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(name__, &record[2], "=", &rate, &c_b110, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the data portion of the record. */

	    s_copy(name__, "Mini-segment *; packet *", (ftnlen)80, (ftnlen)24)
		    ;
	    repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(name__, &record[3], "=", &packts[(i__2 = k - 1) < 6720000 
		    && 0 <= i__2 ? i__2 : s_rnge("packts", i__2, "f_ck06__", (
		    ftnlen)1986)], &pktsiz, &c_b110, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Update pointers. */

	begrec += npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)1994)];
	pktptr += npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)1995)] * pktsiz;
    }
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKE06 Non-error cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up and check data from CK06MU. Look ups occur at time tags."
	    " Check first mini-segment.", (ftnlen)90);
    furnsh_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    pktsiz = 8;

/*     Generate test data again. */

    t_genc06__(&segno, &mnsgno, &begrec, npkts, xref, rates, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check data from the first mini-segment. */

    pktptr = 1;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = begrec - 1 + i__;
	ckgpav_(&xinst, &sclkdp[(i__3 = j - 1) < 840000 && 0 <= i__3 ? i__3 : 
		s_rnge("sclkdp", i__3, "f_ck06__", (ftnlen)2047)], &cktol, 
		xref, cmat, av, &clkout, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Generate expected values. */

	k = pktptr + (i__ - 1) * pktsiz;
	moved_(&packts[(i__3 = k - 1) < 6720000 && 0 <= i__3 ? i__3 : s_rnge(
		"packts", i__3, "f_ck06__", (ftnlen)2058)], &c__4, xq);
	q2m_(xq, xcmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(&avvs[(i__3 = j * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : s_rnge(
		"avvs", i__3, "f_ck06__", (ftnlen)2063)], xav);
	s_copy(name__, "Mini-segment *;  * CMAT", (ftnlen)80, (ftnlen)23);
	repmi_(name__, "*", &c__1, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(name__, "Mini-segment *;  * AVV", (ftnlen)80, (ftnlen)22);
	repmi_(name__, "*", &c__1, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        For this case, we cannot expect high accuracy. */
/*        We're comparing angular velocity computed from */
/*        interpolated quaternion derivatives to the original */
/*        value. */

	chckad_(name__, av, "~~/", xav, &c__3, &c_b587, ok, (ftnlen)80, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up and check data from CK06MU. Look ups occur at time tags."
	    " Check second mini-segment.", (ftnlen)91);

/*     Check data from the second mini-segment. */

    mnsgno = 2;
    begrec += npkts[0];
    pktsiz = 4;

/*     Generate test data again. */

    t_genc06__(&segno, &mnsgno, &begrec, &npkts[1], xref, &rates[1], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktptr = npkts[0] * psizes[0] + 1;
    i__1 = npkts[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgpav_(&xinst, &epochs[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 
		: s_rnge("epochs", i__3, "f_ck06__", (ftnlen)2116)], &cktol, 
		xref, cmat, av, &clkout, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Generate expected values. */

	k = pktptr + (i__ - 1) * pktsiz;
	moved_(&packts[(i__3 = k - 1) < 6720000 && 0 <= i__3 ? i__3 : s_rnge(
		"packts", i__3, "f_ck06__", (ftnlen)2127)], &c__4, xq);
	q2m_(xq, xcmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(&avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avvs", i__3, "f_ck06__", (ftnlen)2132)], xav);

/*        Scale the expected AV to be compatible with */
/*        AV derived from the quaternions. */

/*         CALL VSCLIP ( 1/RATES(2), XAV ) */
	s_copy(name__, "Mini-segment *;  * CMAT", (ftnlen)80, (ftnlen)23);
	repmi_(name__, "*", &c__2, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(name__, "Mini-segment *;  * AVV", (ftnlen)80, (ftnlen)22);
	repmi_(name__, "*", &c__2, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        For this case, we cannot expect high accuracy. */
/*        We're comparing angular velocity computed from */
/*        interpolated quaternion derivatives to the original */
/*        value. */

	chckad_(name__, av, "~~/", xav, &c__3, &c_b587, ok, (ftnlen)80, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up and check data from CK06MU. Look ups occur at time tags."
	    " Check third mini-segment.", (ftnlen)90);

/*     Check data from the third mini-segment. */

    mnsgno = 3;
    begrec += npkts[1];
    pktsiz = 14;

/*     Generate test data again. */

    t_genc06__(&segno, &mnsgno, &begrec, &npkts[2], xref, &rates[2], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktptr += npkts[1] * psizes[1];
    i__1 = npkts[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgpav_(&xinst, &epochs[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 
		: s_rnge("epochs", i__3, "f_ck06__", (ftnlen)2191)], &cktol, 
		xref, cmat, av, &clkout, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Generate expected values. */

	k = pktptr + (i__ - 1) * pktsiz;
	moved_(&packts[(i__3 = k - 1) < 6720000 && 0 <= i__3 ? i__3 : s_rnge(
		"packts", i__3, "f_ck06__", (ftnlen)2202)], &c__4, xq);
	q2m_(xq, xcmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(&avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avvs", i__3, "f_ck06__", (ftnlen)2207)], xav);

/*        Scale the expected AV to be compatible with */
/*        AV derived from the quaternions. */

/*         CALL VSCLIP ( 1/RATES(3), XAV ) */
	s_copy(name__, "Mini-segment *;  * CMAT", (ftnlen)80, (ftnlen)23);
	repmi_(name__, "*", &c__3, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(name__, "Mini-segment *;  * AVV", (ftnlen)80, (ftnlen)22);
	repmi_(name__, "*", &c__3, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(name__, av, "~~/", xav, &c__3, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up and check data from CK06MU. Look ups occur at time tags."
	    " Check fourth mini-segment.", (ftnlen)91);

/*     Check data from the fourth mini-segment. */

    mnsgno = 4;
    begrec += npkts[2];
    pktsiz = 7;
    t_genc06__(&segno, &mnsgno, &begrec, &npkts[3], xref, &rates[3], epochs, 
	    quats, avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktptr = 1;
    for (i__ = 1; i__ <= 3; ++i__) {
	pktptr += npkts[(i__1 = i__ - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
		"npkts", i__1, "f_ck06__", (ftnlen)2256)] * psizes[(i__3 = 
		i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("psizes", i__3, 
		"f_ck06__", (ftnlen)2256)];
    }
    i__1 = npkts[3];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ckgpav_(&xinst, &epochs[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 
		: s_rnge("epochs", i__3, "f_ck06__", (ftnlen)2262)], &cktol, 
		xref, cmat, av, &clkout, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Generate expected values. */

	k = pktptr + (i__ - 1) * pktsiz;
	moved_(&packts[(i__3 = k - 1) < 6720000 && 0 <= i__3 ? i__3 : s_rnge(
		"packts", i__3, "f_ck06__", (ftnlen)2273)], &c__4, xq);
	q2m_(xq, xcmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(&avvs[(i__3 = i__ * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		s_rnge("avvs", i__3, "f_ck06__", (ftnlen)2278)], xav);
	s_copy(name__, "Mini-segment *;  * CMAT", (ftnlen)80, (ftnlen)23);
	repmi_(name__, "*", &c__4, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix. */

	chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(name__, "Mini-segment *;  * AVV", (ftnlen)80, (ftnlen)22);
	repmi_(name__, "*", &c__4, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check AV. */

	chckad_(name__, av, "~~/", xav, &c__3, &c_b333, ok, (ftnlen)80, (
		ftnlen)3);
    }
    unload_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKR06 Error cases: */
/* * */
/* ***************************************************************** */

/*     Bad mini-segment subtype test: */


/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with an invalid subtype. Try to "
	    "read from this segment.", (ftnlen)800, (ftnlen)80);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, packts, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xinst = -1000;

/*     Set the AV flag to FALSE to set up next test. */

    xavflg = FALSE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = -1;
    degres[0] = 3;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2351)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&xinst, sclkdp, &cktol, xref, cmat, &clkout, &found, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDSUBTYPE)", ok, (ftnlen)21);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to read record with AV.", (ftnlen)27);

/*     Use the CK from the previous test case. */

    dafopr_("test06err.bc", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckr06_(&handle, descr, xivbds, &c_b110, &c_true, record, &found);
    chckxc_(&c_true, "SPICE(NOAVDATA)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad segment data type", (ftnlen)21);
    dafus_(descr, &c__2, &c__6, dc, ic);
    ic[2] = -3;
    dafps_(&c__2, &c__6, dc, ic, descr);
    ckr06_(&handle, descr, xivbds, &c_b110, &c_false, record, &found);
    chckxc_(&c_true, "SPICE(WRONGCKTYPE)", ok, (ftnlen)18);

/*     Close kernel. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 0 and a quaternion "
	    "sign error. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    99);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    subtyp = 0;
    pktsiz = 8;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)2501)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)2501)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		2501)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create error in quaternion sequence at packet index 6: */

	if (i__ == 6) {
	    vminug_(&packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 
		    : s_rnge("packts", i__3, "f_ck06__", (ftnlen)2509)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__3 = pktptr - 1) < 6720000 && 0 <= 
		    i__3 ? i__3 : s_rnge("packts", i__3, "f_ck06__", (ftnlen)
		    2510)]);
	}
    }
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2526)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 0 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&xinst, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADQUATSIGN)", ok, (ftnlen)18);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 2 and a quaternion "
	    "sign error. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    99);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    subtyp = 2;
    pktsiz = 14;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)2622)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)2622)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		2622)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create error in quaternion sequence at packet index 7: */

	if (i__ == 7) {
	    vminug_(&packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 
		    : s_rnge("packts", i__3, "f_ck06__", (ftnlen)2630)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__3 = pktptr - 1) < 6720000 && 0 <= 
		    i__3 ? i__3 : s_rnge("packts", i__3, "f_ck06__", (ftnlen)
		    2631)]);
	}
    }
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2647)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 2 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&xinst, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADQUATSIGN)", ok, (ftnlen)18);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 2 and a window size"
	    " equivalent to 2 mod 4. Try to read from this segment.", (ftnlen)
	    800, (ftnlen)111);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    subtyp = 2;
    pktsiz = 14;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)2745)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)2745)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		2745)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 14;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2762)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 2 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&xinst, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 1 and an even windo"
	    "w size. Try to read from this segment.", (ftnlen)800, (ftnlen)95);
    tcase_(title, (ftnlen)800);
    dafopw_("test06err.bc", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subtyp = 1;
    pktsiz = 4;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)2855)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)2855)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		2855)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -2000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 14;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2872)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&c_n2000, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)
	    32);
    chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 1 and window size t"
	    "oo small. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    97);
    tcase_(title, (ftnlen)800);
    dafopw_("test06err.bc", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subtyp = 1;
    pktsiz = 4;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)2954)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)2954)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		2954)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -2000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 0;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)2971)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&c_n2000, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)
	    32);
    chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Create a trivial segment with subtype 1 and window size t"
	    "oo large. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    97);
    tcase_(title, (ftnlen)800);
    dafopw_("test06err.bc", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subtyp = 1;
    pktsiz = 4;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)3054)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)3054)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		3054)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -2000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 29;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)3071)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Subtype 1 test segment", (ftnlen)60, (ftnlen)22);
    sellst = TRUE_;

/*     Write segment. */

    t_ckw06__(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try a lookup. */

    cktol = 0.;
    ckgp_(&c_n2000, &sclkdp[5], &cktol, xref, cmat, &clkout, &found, (ftnlen)
	    32);
    chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     This is a "non-error" case: we want to make sure that */
/*     quaternion inversion is handled for subtype 1. The */
/*     result of interpolation should be the same as if all */
/*     the quaternions had compatible signs. */

    s_copy(title, "Create a trivial segment with subtype 1 and a quaternion "
	    "sign error. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    99);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    subtyp = 1;
    pktsiz = 4;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)3163)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)3163)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		3163)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create error in quaternion sequence at every even- */
/*        indexed quaternion. */

	if (even_(&i__)) {
	    vminug_(&packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 
		    : s_rnge("packts", i__3, "f_ck06__", (ftnlen)3172)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__3 = pktptr - 1) < 6720000 && 0 <= 
		    i__3 ? i__3 : s_rnge("packts", i__3, "f_ck06__", (ftnlen)
		    3173)]);
	}
    }
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)3189)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Partially inverted subtype 1 segment", (ftnlen)60, (ftnlen)
	    36);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. We can use the SPICELIB type 6 writer */
/*     since it's supposed to allow the quaternion mess we've */
/*     created when the subtype is either 1 or 3. */

    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a second segment for instrument -2000. This one */
/*     has a normal quaternion sequence. */

    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)3246)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)3246)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		3246)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -2000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)3263)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Normal subtype 1 test segment", (ftnlen)60, (ftnlen)29);
    sellst = TRUE_;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try lookups for each instrument. */

    i__1 = n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	t = (sclkdp[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 : s_rnge(
		"sclkdp", i__3, "f_ck06__", (ftnlen)3307)] + sclkdp[(i__2 = 
		i__) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp", i__2, 
		"f_ck06__", (ftnlen)3307)]) / 2;
	cktol = 0.;
	ckgpav_(&c_n2000, &t, &cktol, xref, xcmat, xav, &clkout, &found, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND 1", &found, &c_true, ok, (ftnlen)7);
	ckgpav_(&c_n1000, &t, &cktol, xref, cmat, av, &clkout, &found, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND 2", &found, &c_true, ok, (ftnlen)7);
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)4, (
		ftnlen)3);
	chckad_("CMAT", av, "~~/", xav, &c__3, &c_b333, ok, (ftnlen)4, (
		ftnlen)3);
    }

/*     Unload kernel. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     This is a "non-error" case: same deal as before, this time */
/*     for subtype 3. */

    s_copy(title, "Create a trivial segment with subtype 3 and a quaternion "
	    "sign error. Try to read from this segment.", (ftnlen)800, (ftnlen)
	    99);
    tcase_(title, (ftnlen)800);
    segno = 1;
    mnsgno = 1;
    begrec = 1;
    n = 201;
    s_copy(xref, "MARSIAU", (ftnlen)32, (ftnlen)7);
    subtyp = 3;
    pktsiz = 7;
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)3365)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)3365)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		3365)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create error in quaternion sequence at every even- */
/*        indexed quaternion. */

	if (even_(&i__)) {
	    vminug_(&packts[(i__3 = pktptr - 1) < 6720000 && 0 <= i__3 ? i__3 
		    : s_rnge("packts", i__3, "f_ck06__", (ftnlen)3374)], &
		    c__4, qneg);
	    moved_(qneg, &c__4, &packts[(i__3 = pktptr - 1) < 6720000 && 0 <= 
		    i__3 ? i__3 : s_rnge("packts", i__3, "f_ck06__", (ftnlen)
		    3375)]);
	}
    }
    xinst = -1000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)3391)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Partially inverted subtype 3 segment", (ftnlen)60, (ftnlen)
	    36);
    sellst = TRUE_;

/*     Open new C-kernel. */

    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_("test06err.bc", "Type 06 CK error file", &c__4, &handle, (ftnlen)
	    12, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write segment. We can use the SPICELIB type 6 writer */
/*     since it's supposed to allow the quaternion mess we've */
/*     created when the subtype is either 1 or 3. */

    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a second segment for instrument -2000. This one */
/*     has a normal quaternion sequence. */

    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &c_b189, sclkdp, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	pktptr = (i__ - 1) * pktsiz + 1;
	t_xsubtp__(&quats[(i__3 = (i__ << 2) - 4) < 3360000 && 0 <= i__3 ? 
		i__3 : s_rnge("quats", i__3, "f_ck06__", (ftnlen)3448)], &
		avvs[(i__2 = i__ * 3 - 3) < 2520000 && 0 <= i__2 ? i__2 : 
		s_rnge("avvs", i__2, "f_ck06__", (ftnlen)3448)], davv, &
		subtyp, &pktsiz, &packts[(i__4 = pktptr - 1) < 6720000 && 0 <=
		 i__4 ? i__4 : s_rnge("packts", i__4, "f_ck06__", (ftnlen)
		3448)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    xinst = -2000;
    xavflg = TRUE_;
    nintvl = 1;
    npkts[0] = n;
    rates[0] = .001;
    subtps[0] = subtyp;
    degres[0] = 15;
    xivbds[0] = sclkdp[0];
    xivbds[1] = sclkdp[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge(
	    "sclkdp", i__1, "f_ck06__", (ftnlen)3465)];
    first = xivbds[0];
    last = xivbds[1];
    s_copy(segid, "Normal subtype 3 test segment", (ftnlen)60, (ftnlen)29);
    sellst = TRUE_;
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load kernel for read access. */

    furnsh_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try lookups for each instrument. */

    i__1 = n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	t = (sclkdp[(i__3 = i__ - 1) < 840000 && 0 <= i__3 ? i__3 : s_rnge(
		"sclkdp", i__3, "f_ck06__", (ftnlen)3509)] + sclkdp[(i__2 = 
		i__) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp", i__2, 
		"f_ck06__", (ftnlen)3509)]) / 2;
	cktol = 0.;
	ckgpav_(&c_n2000, &t, &cktol, xref, xcmat, xav, &clkout, &found, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND 1", &found, &c_true, ok, (ftnlen)7);
	ckgpav_(&c_n1000, &t, &cktol, xref, cmat, av, &clkout, &found, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND 2", &found, &c_true, ok, (ftnlen)7);
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)4, (
		ftnlen)3);
	chckad_("CMAT", av, "~~/", xav, &c__3, &c_b333, ok, (ftnlen)4, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("CKR06 error case: negative tolerance.", (ftnlen)37);

/*     We can use the CK from the previous test case. */


/*     Unload kernel; open for DAF read access. */

    unload_("test06err.bc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafopr_("test06err.bc", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = sclkdp[0];
    cktol = -1.;
    ckr06_(&handle, descr, &t, &cktol, &c_true, record, &found);
    chckxc_(&c_true, "SPICE(NEGATIVETOL)", ok, (ftnlen)18);

/*     Unload kernel. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKR06 Non-error cases: */
/* * */
/* ***************************************************************** */

/*     Use CK from previous tests to examine gap and non-zero */
/*     tolerance logic in CKR06. */

/*     These tests use CK06MU. To start out, restore the arrays */
/*     of parameters used to create this file. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time precedes segment start by les"
	    "s than the tolerance. Populate data arrays for further tests.", (
	    ftnlen)125);
    dafopr_("ck06mult.bc", &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cknm06_(&handle, descr, &nintvl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    ckmp06_(&handle, descr, &i__, &rates[(i__3 = i__ - 1) < 21000 && 
		    0 <= i__3 ? i__3 : s_rnge("rates", i__3, "f_ck06__", (
		    ftnlen)3614)], &subtps[(i__2 = i__ - 1) < 21000 && 0 <= 
		    i__2 ? i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
		    3614)], &winsiz, &npkts[(i__4 = i__ - 1) < 21000 && 0 <= 
		    i__4 ? i__4 : s_rnge("npkts", i__4, "f_ck06__", (ftnlen)
		    3614)], ivlbds, &lstepc);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time precedes segment start by les"
	    "s than the tolerance.", (ftnlen)85);
    xinst = -1000;
    s_copy(xref, "GALACTIC", (ftnlen)32, (ftnlen)8);
    segno = 1;

/*     Re-generate data for the first mini-segment. */

    mnsgno = 1;
    begrec = 1;
    pktsiz = npkts[0];
    n = npkts[0];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, rates, epochs, quats, avvs,
	     ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cktol = .5;
    t = epochs[0] - cktol + 1e-6;
    t = epochs[0];
    first = epochs[0];
    last = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epoc"
	    "hs", i__1, "f_ck06__", (ftnlen)3663)] + 1.;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the first epoch. */

    chcksd_("CLKOUT", &clkout, "=", epochs, &c_b110, ok, (ftnlen)6, (ftnlen)1)
	    ;
    vequ_(avvs, xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1195, ok, (ftnlen)2, (ftnlen)3);
    moved_(quats, &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)4, (
	    ftnlen)3);

/*     Convert expected AV to quaternion derivative and back. */
/*     The purpose of this test is to quantify the error made */
/*     by these conversions; this error should dominate the */
/*     error in the AV test above. */

    cleard_(&c__4, avq);
    vscl_(&c_b1202, av, &avq[1]);
    qxq_(xq, avq, xdq);
    qdq2av_(xq, xdq, av);
    chckad_("AV 2", av, "~~/", xav, &c__3, &c_b1195, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Make sure a second look-up produces the same results. */

    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the first epoch. */

    chcksd_("CLKOUT", &clkout, "=", epochs, &c_b110, ok, (ftnlen)6, (ftnlen)1)
	    ;
    vequ_(avvs, xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1195, ok, (ftnlen)2, (ftnlen)3);
    moved_(quats, &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)4, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time precedes segment start by mor"
	    "e than the tolerance.", (ftnlen)85);
    unload_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cktol = .5;
    t = epochs[0] - cktol - 1e-6;
    dafopr_("ck06mult.bc", &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckr06_(&handle, descr, &t, &cktol, &c_false, record, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time exceeds segment stop by more "
	    "than the tolerance.", (ftnlen)83);
    cktol = .5;
    dafus_(descr, &c__2, &c__6, dc, ic);
    t = dc[1] + cktol + 1e-6;
    ckr06_(&handle, descr, &t, &cktol, &c_false, record, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time exceeds segment stop by less "
	    "than the tolerance, but is too far from the last epoch of the la"
	    "st mini-segment for data to be found.", (ftnlen)165);
    furnsh_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-generate data for the last mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[3], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    first = epochs[0];
    last = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epoc"
	    "hs", i__1, "f_ck06__", (ftnlen)3821)] + 1.;
    cktol = .5;
    t = last + cktol / 2;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance is positive; lookup time exceeds segment stop by less "
	    "than the tolerance, and is close enough to the last epoch of the"
	    " last mini-segment for data to be found.", (ftnlen)168);

/*     Re-generate data for the last mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[3], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    first = epochs[0];
    last = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epoc"
	    "hs", i__1, "f_ck06__", (ftnlen)3857)] + 1.;
    cktol = 2.;
    t = last + cktol / 2 - 1e-6;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the last epoch. */

    chcksd_("CLKOUT", &clkout, "=", &epochs[(i__1 = n - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_ck06__", (ftnlen)3874)], &
	    c_b110, ok, (ftnlen)6, (ftnlen)1);
    vequ_(&avvs[(i__1 = n * 3 - 3) < 2520000 && 0 <= i__1 ? i__1 : s_rnge(
	    "avvs", i__1, "f_ck06__", (ftnlen)3877)], xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(&quats[(i__1 = (n << 2) - 4) < 3360000 && 0 <= i__1 ? i__1 : 
	    s_rnge("quats", i__1, "f_ck06__", (ftnlen)3881)], &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/*     Make sure a second look-up produces the same results. */

    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the last epoch. */

    chcksd_("CLKOUT", &clkout, "=", &epochs[(i__1 = n - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_ck06__", (ftnlen)3901)], &
	    c_b110, ok, (ftnlen)6, (ftnlen)1);
    vequ_(&avvs[(i__1 = n * 3 - 3) < 2520000 && 0 <= i__1 ? i__1 : s_rnge(
	    "avvs", i__1, "f_ck06__", (ftnlen)3904)], xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(&quats[(i__1 = (n << 2) - 4) < 3360000 && 0 <= i__1 ? i__1 : 
	    s_rnge("quats", i__1, "f_ck06__", (ftnlen)3908)], &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is gap midpoint; tolerance is zero", (ftnlen)46);
    xinst = -1000;
    xavflg = TRUE_;
    s_copy(xref, "GALACTIC", (ftnlen)32, (ftnlen)8);
    nintvl = 4;
    sellst = TRUE_;
    segno = 1;

/*     Re-generate data for the third mini-segment. */

    mnsgno = 3;
    begrec = npkts[0] + npkts[1] + 1;
    pktsiz = 14;
    n = npkts[2];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[2], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Let T be the midpoint of the gap at the end of the */
/*     third mini-segment. */

    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)3947)] + .5;
    cktol = 0.;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is gap midpoint; tolerance is positive but too small"
	    " to find pointing.", (ftnlen)82);
    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)3964)] + .5;
    cktol = .001;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is in gap and is before gap midpoint; tolerance is 1"
	    "/2 gap length. Mini-segment is not the last.", (ftnlen)108);
    cktol = .5;
    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)3983)] + .5 - 1e-6;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the final epoch. */

    chcksd_("CLKOUT", &clkout, "=", &epochs[(i__1 = n - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_ck06__", (ftnlen)3994)], &
	    c_b110, ok, (ftnlen)6, (ftnlen)1);
    vequ_(&avvs[(i__1 = n * 3 - 3) < 2520000 && 0 <= i__1 ? i__1 : s_rnge(
	    "avvs", i__1, "f_ck06__", (ftnlen)3997)], xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(&quats[(i__1 = (n << 2) - 4) < 3360000 && 0 <= i__1 ? i__1 : 
	    s_rnge("quats", i__1, "f_ck06__", (ftnlen)4001)], &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/*     Make sure a second look-up produces the same results. */

    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the final epoch. */

    chcksd_("CLKOUT", &clkout, "=", &epochs[(i__1 = n - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_ck06__", (ftnlen)4019)], &
	    c_b110, ok, (ftnlen)6, (ftnlen)1);
    vequ_(&avvs[(i__1 = n * 3 - 3) < 2520000 && 0 <= i__1 ? i__1 : s_rnge(
	    "avvs", i__1, "f_ck06__", (ftnlen)4022)], xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(&quats[(i__1 = (n << 2) - 4) < 3360000 && 0 <= i__1 ? i__1 : 
	    s_rnge("quats", i__1, "f_ck06__", (ftnlen)4026)], &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is in gap and is after gap midpoint; tolerance is 1/"
	    "2 gap length.", (ftnlen)77);
    cktol = .5;
    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)4044)] + .5 + 1e-6;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the first epoch of the */
/*     fourth mini-segment. Re-generate data for that mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[3], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("CLKOUT", &clkout, "=", epochs, &c_b110, ok, (ftnlen)6, (ftnlen)1)
	    ;
    vequ_(avvs, xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(quats, &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/*     Make sure a second look-up produces the same results. */

    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The data should be those for the first epoch of the */
/*     fourth mini-segment. Re-generate data for that mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    chcksd_("CLKOUT", &clkout, "=", epochs, &c_b110, ok, (ftnlen)6, (ftnlen)1)
	    ;
    vequ_(avvs, xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(quats, &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is in gap and is before gap midpoint in last mini-se"
	    "gment; tolerance is 1/2 gap length.", (ftnlen)99);

/*     The data should be those for the last epoch of the */
/*     fourth mini-segment. Re-generate data for that mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[3], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)4136)] + .5 - 1e-6;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksd_("CLKOUT", &clkout, "=", &epochs[(i__1 = n - 1) < 840000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_ck06__", (ftnlen)4145)], &
	    c_b110, ok, (ftnlen)6, (ftnlen)1);
    vequ_(&avvs[(i__1 = n * 3 - 3) < 2520000 && 0 <= i__1 ? i__1 : s_rnge(
	    "avvs", i__1, "f_ck06__", (ftnlen)4148)], xav);
    chckad_("AV", av, "~~/", xav, &c__3, &c_b1282, ok, (ftnlen)2, (ftnlen)3);
    moved_(&quats[(i__1 = (n << 2) - 4) < 3360000 && 0 <= i__1 ? i__1 : 
	    s_rnge("quats", i__1, "f_ck06__", (ftnlen)4152)], &c__4, xq);
    q2m_(xq, xcmat);
    chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b1282, ok, (ftnlen)4, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Lookup time is in gap and is after gap midpoint in last mini-seg"
	    "ment; tolerance is 1/2 gap length.", (ftnlen)98);

/*     The data should be those for the last epoch of the */
/*     fourth mini-segment. Re-generate data for that mini-segment. */

    mnsgno = 4;
    begrec = npkts[0] + npkts[1] + npkts[2] + 1;
    pktsiz = 7;
    n = npkts[3];
    t_genc06__(&segno, &mnsgno, &begrec, &n, xref, &rates[3], epochs, quats, 
	    avvs, ok, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = epochs[(i__1 = n - 1) < 840000 && 0 <= i__1 ? i__1 : s_rnge("epochs", 
	    i__1, "f_ck06__", (ftnlen)4181)] + .5 + 1e-6;
    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    unload_("ck06mult.bc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* ***************************************************************** */
/* * */
/* *     CKR06: create CKs for search tests */
/* * */
/* ***************************************************************** */
/* ***************************************************************** */
/* $$$ */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create CK for packet selection logic tests.", (ftnlen)43);

/*     Create CKs for testing. */

/*     The first kernel contains three mini-segments, all for the */
/*     same instrument. The packet counts in these mini-segments */
/*     are such that the first one contains no packet directories, */
/*     the second contains fewer than one directory buffer full, */
/*     and the third contains too many to be buffered. */

/*     The selection flag indicates "select last." */

    s_copy(ck, "ck06big0.bc", (ftnlen)255, (ftnlen)11);
    xinst = -1000;
    s_copy(xref, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(ckfram, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    segno = 1;
    sellst = TRUE_;
    nintvl = 3;
    npkts[0] = 98;
    npkts[1] = 398;
    npkts[2] = 10381;
    subtps[0] = 0;
    subtps[1] = 1;
    subtps[2] = 3;
    psizes[0] = 8;
    psizes[1] = 4;
    psizes[2] = 7;
    degres[0] = 7;
    degres[1] = 3;
    degres[2] = 23;
    rates[0] = 10.;
    rates[1] = 2.;
    rates[2] = 3.;
    pad = 3;

/*     Create time tags and packets. */

    begrec = 1;
    n = 1;
    pktptr = 1;
    i__1 = nintvl;
    for (mnsgno = 1; mnsgno <= i__1; ++mnsgno) {

/*        Record the index of the first non-pad time tag of */
/*        the current mini-segment. */

	datidx[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("dat"
		"idx", i__3, "f_ck06__", (ftnlen)4265)] = n + pad;

/*        Record the corresponding start index in the packet array. */

	pktbeg[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("pkt"
		"beg", i__3, "f_ck06__", (ftnlen)4270)] = pktptr;

/*        Generate time tags and packet data for the current */
/*        mini-segment. */

	rate = rates[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"rates", i__3, "f_ck06__", (ftnlen)4276)];
	subtyp = subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4277)];
	t_gencsm__(&segno, &mnsgno, &begrec, &npkts[(i__3 = mnsgno - 1) < 
		21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_ck06__", 
		(ftnlen)4279)], xref, &rate, &subtyp, epochs, quats, avvs, ok,
		 (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The start time of the current interval is the epoch at */
/*        PAD positions later than the start epoch. */

	xivbds[(i__3 = mnsgno - 1) < 21001 && 0 <= i__3 ? i__3 : s_rnge("xiv"
		"bds", i__3, "f_ck06__", (ftnlen)4286)] = epochs[(i__2 = pad) <
		 840000 && 0 <= i__2 ? i__2 : s_rnge("epochs", i__2, "f_ck06"
		"__", (ftnlen)4286)];

/*        Setting up the data arrays for CKW06 is simple: we simply */
/*        append the data for each mini-segment. */

	moved_(epochs, &npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 
		: s_rnge("npkts", i__3, "f_ck06__", (ftnlen)4292)], &sclkdp[(
		i__2 = n - 1) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp", 
		i__2, "f_ck06__", (ftnlen)4292)]);
	i__4 = npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4294)] * 3;
	moved_(avvs, &i__4, &xavvs[(i__2 = n * 3 - 3) < 2520000 && 0 <= i__2 ?
		 i__2 : s_rnge("xavvs", i__2, "f_ck06__", (ftnlen)4294)]);
	i__4 = npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4295)] << 2;
	moved_(quats, &i__4, &xquats[(i__2 = (n << 2) - 4) < 3360000 && 0 <= 
		i__2 ? i__2 : s_rnge("xquats", i__2, "f_ck06__", (ftnlen)4295)
		]);
	i__2 = npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4297)];
	for (j = 1; j <= i__2; ++j) {
	    q2m_(&quats[(i__3 = (j << 2) - 4) < 3360000 && 0 <= i__3 ? i__3 : 
		    s_rnge("quats", i__3, "f_ck06__", (ftnlen)4298)], &xcmats[
		    (i__4 = (n - 1 + j) * 9 - 9) < 7560000 && 0 <= i__4 ? 
		    i__4 : s_rnge("xcmats", i__4, "f_ck06__", (ftnlen)4298)]);
	}

/*        We're not using angular acceleration. */

	cleard_(&c__3, davv);
	i__3 = npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_ck06__", (ftnlen)4306)];
	for (j = 1; j <= i__3; ++j) {
	    t_xsubtp__(&quats[(i__2 = (j << 2) - 4) < 3360000 && 0 <= i__2 ? 
		    i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)4308)], &
		    avvs[(i__4 = j * 3 - 3) < 2520000 && 0 <= i__4 ? i__4 : 
		    s_rnge("avvs", i__4, "f_ck06__", (ftnlen)4308)], davv, &
		    subtps[(i__5 = mnsgno - 1) < 21000 && 0 <= i__5 ? i__5 : 
		    s_rnge("subtps", i__5, "f_ck06__", (ftnlen)4308)], &
		    psizes[(i__6 = mnsgno - 1) < 21000 && 0 <= i__6 ? i__6 : 
		    s_rnge("psizes", i__6, "f_ck06__", (ftnlen)4308)], record)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    moved_(record, &psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
		    i__2 : s_rnge("psizes", i__2, "f_ck06__", (ftnlen)4312)], 
		    &packts[(i__4 = pktptr - 1) < 6720000 && 0 <= i__4 ? i__4 
		    : s_rnge("packts", i__4, "f_ck06__", (ftnlen)4312)]);
	    pktptr += psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 :
		     s_rnge("psizes", i__2, "f_ck06__", (ftnlen)4314)];
	}

/*        Adjust the starting record number of the next mini-segment */
/*        to account for padding. */

	begrec = begrec - 1 + npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ?
		 i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)4322)] - (
		pad << 1);

/*        Update the count of the records in the segment. */

	n += npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4327)];
    }

/*     The stop time of the last interval is the epoch at */
/*     PAD positions before the final epoch. */

    xivbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("xivbds", 
	    i__1, "f_ck06__", (ftnlen)4335)] = sclkdp[(i__3 = n - pad - 1) < 
	    840000 && 0 <= i__3 ? i__3 : s_rnge("sclkdp", i__3, "f_ck06__", (
	    ftnlen)4335)];

/*     Let the segment bounds coincide with the start and stop */
/*     times of the first and last mini-segments. */

    first = xivbds[0];
    last = xivbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("xivb"
	    "ds", i__1, "f_ck06__", (ftnlen)4342)];

/*     Create the CK. */

    if (exists_(ck, (ftnlen)255)) {
	delfil_(ck, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_(ck, ck, &c__0, &handle, (ftnlen)255, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst, (
	    ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKR06 reduction of order tests */
/* * */
/* ***************************************************************** */

/*     These tests examine the behavior of the CKR06 interpolation */
/*     window construction algorithm for request times near mini-segment */
/*     interpolation interval boundaries. */

/*     We'll work with the third mini-segment of CK06B0. We start by */
/*     looking up the relevant mini-segment attributes: */

/*        - Interpolation interval bounds */
/*        - Window size */
/*        - Packet count */


/* --- Case: ------------------------------------------------------ */

    tcase_("Set up CKR06 reduction of order tests.", (ftnlen)38);
    s_copy(ck, "ck06big0.bc", (ftnlen)255, (ftnlen)11);
    dafopr_(ck, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mnsgno = 3;
    ckmp06_(&handle, descr, &mnsgno, &rate, &subtyp, &winsiz, &nrec, ivlbds, &
	    lstepc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pktsiz = pktszs[(i__1 = subtyp) < 4 && 0 <= i__1 ? i__1 : s_rnge("pktszs",
	     i__1, "f_ck06__", (ftnlen)4422)];

/*     Determine the left hand pad size. */

    pad = 0;
    i__ = 1;
    ckgr06_(&handle, descr, &mnsgno, &i__, record);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = record[0];
    while(t < ivlbds[0]) {
	++pad;
	++i__;
	ckgr06_(&handle, descr, &mnsgno, &i__, record);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t = record[0];
    }

/*     See whether CKR06 returns the expected window for */
/*     request times near the left boundary of the interval. */

    i__1 = pad + 2;
    for (i__ = winsiz / 2 + 1; i__ >= i__1; --i__) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "CK06B0: Reduction of order on left side. Request time"
		" is between tags at indices # and #.", (ftnlen)800, (ftnlen)
		89);
	i__3 = i__ - 1;
	repmi_(title, "#", &i__3, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);

/*        Construct the expected record for a request time */
/*        between time tags at indices I-1 and I. */


/*        N is the expected packet count. */

	n = i__ + winsiz / 2 - 1;
	i__3 = n;
	for (j = 1; j <= i__3; ++j) {
	    ckgr06_(&handle, descr, &mnsgno, &j, record);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    k = (j - 1) * pktsiz + 5;

/*           Insert packet data into the expected record. */

	    moved_(&record[3], &pktsiz, &xrec[(i__2 = k - 1) < 200 && 0 <= 
		    i__2 ? i__2 : s_rnge("xrec", i__2, "f_ck06__", (ftnlen)
		    4484)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the time tag into the expected record. */

	    k = n * pktsiz + 4 + j;
	    xrec[(i__2 = k - 1) < 200 && 0 <= i__2 ? i__2 : s_rnge("xrec", 
		    i__2, "f_ck06__", (ftnlen)4491)] = record[0];
	}

/*        Get the time tags that bracket the request time we */
/*        want to use. */

	k = n * pktsiz + 4 + (i__ - 1);
	t0 = xrec[(i__3 = k - 1) < 200 && 0 <= i__3 ? i__3 : s_rnge("xrec", 
		i__3, "f_ck06__", (ftnlen)4501)];
	t1 = xrec[(i__3 = k) < 200 && 0 <= i__3 ? i__3 : s_rnge("xrec", i__3, 
		"f_ck06__", (ftnlen)4502)];

/*        Generate the request time. */

	t = (t0 + t1) / 2;

/*        Insert the initial parameters into the expected record. */

	xrec[0] = t;
	xrec[1] = (doublereal) subtyp;
	xrec[2] = (doublereal) n;
	xrec[3] = rate;

/*        Look up the record for request time SCLKDP. */

	ckr06_(&handle, descr, &t, &c_b110, &c_true, record, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	s_copy(name__, "Record # packet count", (ftnlen)80, (ftnlen)21);
	repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__3 = i_dnnt(&record[2]);
	chcksi_(name__, &i__3, "=", &n, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "Record #", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    size = n * (pktsiz + 1) + 4;
	    chckad_(name__, record, "=", xrec, &size, &c_b110, ok, (ftnlen)80,
		     (ftnlen)1);
	}
    }

/* @@@ */



/* --- Case: ------------------------------------------------------ */

    tcase_("Determine the right hand pad size.", (ftnlen)34);
    pad = 0;
    i__ = nrec;
    ckgr06_(&handle, descr, &mnsgno, &i__, record);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = record[0];
    while(t > ivlbds[1]) {
	++pad;
	--i__;
	ckgr06_(&handle, descr, &mnsgno, &i__, record);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t = record[0];
    }

/*     See whether CKR06 returns the expected window for */
/*     request times near the right boundary of the interval. */

    i__1 = nrec - pad;
    for (i__ = nrec - winsiz / 2 + 1; i__ <= i__1; ++i__) {

/* --- Case: ------------------------------------------------------ */


/*        Let M represent the loop iteration count. This is the */
/*        offset of I from the value preceding the loop start */
/*        value. */

	m = i__ - (nrec - winsiz / 2);
	s_copy(title, "CK06B0: Reduction of order on left side. Request time"
		" is between tags at indices # and #. Iteration = #.", (ftnlen)
		800, (ftnlen)104);
	i__3 = i__ - 1;
	repmi_(title, "#", &i__3, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(title, "#", &m, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);

/*        Construct the expected record for a request time */
/*        between time tags at indices I-1 and I. */


/*        N is the expected packet count. */

	n = winsiz + 1 - m;
	i__3 = n;
	for (j = 1; j <= i__3; ++j) {
	    k = nrec - n + j;
	    ckgr06_(&handle, descr, &mnsgno, &k, record);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    k = (j - 1) * pktsiz + 5;

/*           Insert packet data into the expected record. */

	    moved_(&record[3], &pktsiz, &xrec[(i__2 = k - 1) < 200 && 0 <= 
		    i__2 ? i__2 : s_rnge("xrec", i__2, "f_ck06__", (ftnlen)
		    4631)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the time tag into the expected record. */

	    k = n * pktsiz + 4 + j;
	    xrec[(i__2 = k - 1) < 200 && 0 <= i__2 ? i__2 : s_rnge("xrec", 
		    i__2, "f_ck06__", (ftnlen)4638)] = record[0];
	}

/*        Get the time tags that bracket the request time we */
/*        want to use. Note that for this case, the offset */
/*        is independent of J. */

	k = n * pktsiz + 4 + winsiz / 2;
	t0 = xrec[(i__3 = k - 1) < 200 && 0 <= i__3 ? i__3 : s_rnge("xrec", 
		i__3, "f_ck06__", (ftnlen)4649)];
	t1 = xrec[(i__3 = k) < 200 && 0 <= i__3 ? i__3 : s_rnge("xrec", i__3, 
		"f_ck06__", (ftnlen)4650)];

/*        Generate the request time. */

	t = (t0 + t1) / 2;

/*        Insert the initial parameters into the expected record. */

	xrec[0] = t;
	xrec[1] = (doublereal) subtyp;
	xrec[2] = (doublereal) n;
	xrec[3] = rate;

/*        Look up the record for request time SCLKDP. */

	ckr06_(&handle, descr, &t, &c_b110, &c_true, record, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	s_copy(name__, "Record # packet count", (ftnlen)80, (ftnlen)21);
	repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__3 = i_dnnt(&record[2]);
	chcksi_(name__, &i__3, "=", &n, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "Record #", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    size = n * (pktsiz + 1) + 4;
	    chckad_(name__, record, "=", xrec, &size, &c_b110, ok, (ftnlen)80,
		     (ftnlen)1);
	}
    }
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKR06 packet selection logic tests */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Packet selection logic test: look up data at time tags.", (ftnlen)
	    55);
    s_copy(ck, "ck06big0.bc", (ftnlen)255, (ftnlen)11);
    furnsh_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cktol = 0.;
    begrec = 1;
    n = 1;
    pktptr = 1;
    i__1 = nintvl;
    for (mnsgno = 1; mnsgno <= i__1; ++mnsgno) {

/*        Create the expected CMAT and AV values. */

	rate = rates[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"rates", i__3, "f_ck06__", (ftnlen)4729)];
	subtyp = subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4730)];
/*         CALL MOVED ( XAVVS (1,N), 3*NPKTS(MNSGNO), AVVS ) */
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = datidx[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"datidx", i__3, "f_ck06__", (ftnlen)4736)];
/*         N      = NPKTS (MNSGNO) - 2*PAD */
	t = sclkdp[(i__3 = j - 1) < 840000 && 0 <= i__3 ? i__3 : s_rnge("scl"
		"kdp", i__3, "f_ck06__", (ftnlen)4740)];
	while(t < xivbds[(i__3 = mnsgno) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		"xivbds", i__3, "f_ck06__", (ftnlen)4742)]) {
	    vequ_(&xavvs[(i__3 = j * 3 - 3) < 2520000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xavvs", i__3, "f_ck06__", (ftnlen)4745)], xav);
	    moved_(&xcmats[(i__3 = j * 9 - 9) < 7560000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xcmats", i__3, "f_ck06__", (ftnlen)4746)], &c__9, 
		    xcmat);

/*           Look up pointing data. */

	    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (! (*ok)) {
		sigerr_("NO POINTING", (ftnlen)11);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_stop("", (ftnlen)0);
	    }
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*           Check output time. */

	    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80, (
		    ftnlen)28);
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check C-matrix. */

	    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (ftnlen)
		    26);
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)
		    80, (ftnlen)3);

/*           Check AV. */

	    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (ftnlen)24)
		    ;
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check angular velocity. */

/*           For derived AV, use a looser tolerance. */

	    if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4801)] == 1) {
		tol = 1e-8;
/*               TOL = MED */
	    } else if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 
		    : s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4807)] == 0) 
		    {
		tol = 1e-10;
	    } else if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 
		    : s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4811)] == 3) 
		    {
		tol = 1e-14;
	    } else {
		tol = 1e-10;
	    }
	    chckad_(name__, av, "~~/", xav, &c__3, &tol, ok, (ftnlen)80, (
		    ftnlen)3);
	    if (! (*ok)) {
		s_stop("", (ftnlen)0);
	    }
	    ++j;
	    pktptr += psizes[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 :
		     s_rnge("psizes", i__3, "f_ck06__", (ftnlen)4824)];
	    t = sclkdp[(i__3 = j - 1) < 840000 && 0 <= i__3 ? i__3 : s_rnge(
		    "sclkdp", i__3, "f_ck06__", (ftnlen)4826)];
	}

/*        Adjust the starting record number of the next mini-segment */
/*        to account for padding. */

	begrec = begrec - 1 + npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ?
		 i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)4833)] - (
		pad << 1);
	n += npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4835)];
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Packet selection logic test: look up data at midpoints between t"
	    "ime tags.", (ftnlen)73);
    i__1 = nintvl;
    for (mnsgno = 1; mnsgno <= i__1; ++mnsgno) {

/*        Create the expected CMAT and AV values. */

	rate = rates[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"rates", i__3, "f_ck06__", (ftnlen)4854)];
	subtyp = subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4855)];
	j = datidx[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"datidx", i__3, "f_ck06__", (ftnlen)4858)];
	pktptr = pktbeg[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		s_rnge("pktbeg", i__3, "f_ck06__", (ftnlen)4859)] + pad * 
		psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		s_rnge("psizes", i__2, "f_ck06__", (ftnlen)4859)];
	n = npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_ck06__", (ftnlen)4860)] - (pad << 1);
	if (j < npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		s_rnge("npkts", i__3, "f_ck06__", (ftnlen)4862)]) {
	    t = (sclkdp[(i__3 = j - 1) < 840000 && 0 <= i__3 ? i__3 : s_rnge(
		    "sclkdp", i__3, "f_ck06__", (ftnlen)4864)] + sclkdp[(i__2 
		    = j) < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp", i__2,
		     "f_ck06__", (ftnlen)4864)]) / 2;
	} else {
	    t = last + 1.;
	}
	while(t < xivbds[(i__3 = mnsgno) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		"xivbds", i__3, "f_ck06__", (ftnlen)4869)]) {

/*           Create expected CMAT and AV. This will only be */
/*           an approximation. */

	    et = t * rate;
	    sxform_(xref, ckfram, &et, xform, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xf2rav_(xform, xcmat, xav);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    m2q_(xcmat, xq);
	    if (xq[0] < 0.) {
		for (i__ = 0; i__ <= 3; ++i__) {
		    xq[(i__3 = i__) < 4 && 0 <= i__3 ? i__3 : s_rnge("xq", 
			    i__3, "f_ck06__", (ftnlen)4886)] = -xq[(i__2 = 
			    i__) < 4 && 0 <= i__2 ? i__2 : s_rnge("xq", i__2, 
			    "f_ck06__", (ftnlen)4886)];
		}
	    }

/*           Look up pointing data. */

	    ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (! (*ok)) {
		sigerr_("NO POINTING", (ftnlen)11);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_stop("", (ftnlen)0);
	    }
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*           Check output time. */

	    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80, (
		    ftnlen)28);
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check C-matrix. */

	    q2m_(xq, xcmat);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (ftnlen)
		    26);
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Use a tight tolerance for CMAT midpoint comparisons. */

	    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (ftnlen)
		    80, (ftnlen)3);

/*           Check angular velocity. */

	    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (ftnlen)24)
		    ;
	    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Set the tolerance for the AV check. */

	    if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4944)] == 1) {

/*              For derived AV, use a looser tolerance. */

		tol = 1e-6;
	    } else if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 
		    : s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4950)] == 0) 
		    {
		tol = 1e-10;
	    } else if (subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 
		    : s_rnge("subtps", i__3, "f_ck06__", (ftnlen)4954)] == 3) 
		    {
		tol = 1e-14;
	    } else {
		tol = 1e-10;
	    }
	    chckad_(name__, av, "~~/", xav, &c__3, &tol, ok, (ftnlen)80, (
		    ftnlen)3);
	    ++j;
	    pktptr += psizes[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 :
		     s_rnge("psizes", i__3, "f_ck06__", (ftnlen)4964)];
	    if (j < npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("npkts", i__3, "f_ck06__", (ftnlen)4966)]) {
		t = (sclkdp[(i__3 = j - 1) < 840000 && 0 <= i__3 ? i__3 : 
			s_rnge("sclkdp", i__3, "f_ck06__", (ftnlen)4968)] + 
			sclkdp[(i__2 = j) < 840000 && 0 <= i__2 ? i__2 : 
			s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)4968)]) / 
			2;
	    } else {
		t = last + 1.;
	    }
	}
    }
    unload_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *     CKR06 interval selection logic tests */
/* * */
/* ***************************************************************** */

/*     $$$ */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create CKs for interval selection logic tests.", (ftnlen)46);

/*     Create CKs for testing. */

/*     The first kernel contains three segments for the three different */
/*     instruments. The interval counts in these segments */
/*     are such that the first one contains no interval directories, */
/*     the second contains fewer than one directory buffer full, */
/*     and the third contains too many to be buffered. */

/*     The selection flag for the first CK is set to indicate */
/*     "select first." */

    s_copy(ck, "ck06big1.bc", (ftnlen)255, (ftnlen)11);

/*     Create the CK. */

    if (exists_(ck, (ftnlen)255)) {
	delfil_(ck, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_(ck, ck, &c__0, &handle, (ftnlen)255, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nseg = 3;
    i__1 = nseg;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xinsts[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("xinsts", 
		i__3, "f_ck06__", (ftnlen)5030)] = i__ * -1000;
    }
    s_copy(xrefs, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(xrefs + 32, "MARSIAU", (ftnlen)32, (ftnlen)7);
    s_copy(xrefs + 64, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(ckfram, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    sellst = FALSE_;

/*     Set the interval counts for the three segments. */

    nivals[0] = 98;
    nivals[1] = 398;
    nivals[2] = 10381;

/*     Create data for the segments and write them to the file. */

    n = 0;
    segptr = 0;
    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {
	segptr += n;

/*        Set the mini-segment attributes for the current segment. */

	xinst = xinsts[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"xinsts", i__3, "f_ck06__", (ftnlen)5059)];
	nintvl = nivals[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"nivals", i__3, "f_ck06__", (ftnlen)5060)];
	s_copy(xref, xrefs + (((i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : 
		s_rnge("xrefs", i__3, "f_ck06__", (ftnlen)5061)) << 5), (
		ftnlen)32, (ftnlen)32);
	i__3 = nintvl;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5067)] = 3;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5068)] = 7;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5069)] = 3;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5070)] = 1.1;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5071)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5075)] = 1;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5076)] = 4;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5077)] = 7;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5078)] = .8;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5079)] = 9;
	    } else {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5083)] = 0;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5084)] = 8;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5085)] = 7;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5086)] = 4.;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5087)] = 11;
	    }
	}
	pad = 1;

/*        Create time tags and packets for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__3 = nintvl;
	for (mnsgno = 1; mnsgno <= i__3; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		    "datidx", i__2, "f_ck06__", (ftnlen)5107)] = n + pad;

/*           Record the corresponding start index in the packet array. */

	    pktbeg[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		    "pktbeg", i__2, "f_ck06__", (ftnlen)5112)] = pktptr;

/*           Generate time tags and packet data for the current */
/*           mini-segment. */

	    rate = rates[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("rates", i__2, "f_ck06__", (ftnlen)5118)];
	    subtyp = subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("subtps", i__2, "f_ck06__", (ftnlen)5119)];
	    t_gencsm__(&segno, &mnsgno, &begrec, &npkts[(i__2 = mnsgno - 1) < 
		    21000 && 0 <= i__2 ? i__2 : s_rnge("npkts", i__2, "f_ck0"
		    "6__", (ftnlen)5121)], xref, &rate, &subtyp, epochs, quats,
		     avvs, ok, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The start time of the current interval is the epoch at */
/*           PAD positions later than the start epoch. */

	    xivbds[(i__2 = mnsgno - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge(
		    "xivbds", i__2, "f_ck06__", (ftnlen)5129)] = epochs[(i__4 
		    = pad) < 840000 && 0 <= i__4 ? i__4 : s_rnge("epochs", 
		    i__4, "f_ck06__", (ftnlen)5129)];

/*           Setting up the data arrays for CKW06 is simple: we simply */
/*           append the data for each mini-segment. */

	    moved_(epochs, &npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
		    i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5135)], &
		    sclkdp[(i__4 = n - 1) < 840000 && 0 <= i__4 ? i__4 : 
		    s_rnge("sclkdp", i__4, "f_ck06__", (ftnlen)5135)]);
	    moved_(epochs, &npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
		    i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5137)], &
		    xepoch[(i__4 = segptr + n - 1) < 840000 && 0 <= i__4 ? 
		    i__4 : s_rnge("xepoch", i__4, "f_ck06__", (ftnlen)5137)]);
	    i__5 = npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5138)] * 3;
	    moved_(avvs, &i__5, &xavvs[(i__4 = (segptr + n) * 3 - 3) < 
		    2520000 && 0 <= i__4 ? i__4 : s_rnge("xavvs", i__4, "f_c"
		    "k06__", (ftnlen)5138)]);
	    i__5 = npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5139)] << 2;
	    moved_(quats, &i__5, &xquats[(i__4 = (segptr + n << 2) - 4) < 
		    3360000 && 0 <= i__4 ? i__4 : s_rnge("xquats", i__4, 
		    "f_ck06__", (ftnlen)5139)]);
	    i__4 = npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5141)];
	    for (j = 1; j <= i__4; ++j) {
		q2m_(&quats[(i__2 = (j << 2) - 4) < 3360000 && 0 <= i__2 ? 
			i__2 : s_rnge("quats", i__2, "f_ck06__", (ftnlen)5142)
			], &xcmats[(i__5 = (segptr + n + j - 1) * 9 - 9) < 
			7560000 && 0 <= i__5 ? i__5 : s_rnge("xcmats", i__5, 
			"f_ck06__", (ftnlen)5142)]);
	    }

/*           We're not using angular acceleration. */

	    cleard_(&c__3, davv);
	    i__2 = npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5151)];
	    for (j = 1; j <= i__2; ++j) {
		t_xsubtp__(&quats[(i__4 = (j << 2) - 4) < 3360000 && 0 <= 
			i__4 ? i__4 : s_rnge("quats", i__4, "f_ck06__", (
			ftnlen)5153)], &avvs[(i__5 = j * 3 - 3) < 2520000 && 
			0 <= i__5 ? i__5 : s_rnge("avvs", i__5, "f_ck06__", (
			ftnlen)5153)], davv, &subtps[(i__6 = mnsgno - 1) < 
			21000 && 0 <= i__6 ? i__6 : s_rnge("subtps", i__6, 
			"f_ck06__", (ftnlen)5153)], &psizes[(i__7 = mnsgno - 
			1) < 21000 && 0 <= i__7 ? i__7 : s_rnge("psizes", 
			i__7, "f_ck06__", (ftnlen)5153)], record);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		moved_(record, &psizes[(i__4 = mnsgno - 1) < 21000 && 0 <= 
			i__4 ? i__4 : s_rnge("psizes", i__4, "f_ck06__", (
			ftnlen)5157)], &packts[(i__5 = pktptr - 1) < 6720000 
			&& 0 <= i__5 ? i__5 : s_rnge("packts", i__5, "f_ck06"
			"__", (ftnlen)5157)]);
		pktptr += psizes[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? 
			i__4 : s_rnge("psizes", i__4, "f_ck06__", (ftnlen)
			5159)];
	    }

/*           Adjust the starting record number of the next mini-segment */
/*           to account for padding. */

	    begrec = begrec - 1 + npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= 
		    i__2 ? i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)
		    5167)] - (pad << 1);

/*           Update the count of the records in the segment. */

	    n += npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5172)];
	}

/*        The stop time of the last interval is the epoch at */
/*        PAD positions before the final epoch. */

	xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge("xivbds", 
		i__3, "f_ck06__", (ftnlen)5180)] = sclkdp[(i__2 = n - pad - 1)
		 < 840000 && 0 <= i__2 ? i__2 : s_rnge("sclkdp", i__2, "f_ck"
		"06__", (ftnlen)5180)];

/*        Let the segment bounds coincide with the start and stop */
/*        times of the first and last mini-segments. */

	first = xivbds[0];
	last = xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		"xivbds", i__3, "f_ck06__", (ftnlen)5187)];
	ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
		npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst,
		 (ftnlen)32, (ftnlen)60);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close the kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     The selection flag for the second CK is set to indicate */
/*     "select last." Other than that, the CK is identical to */
/*     the first. */

    s_copy(ck, "ck06big2.bc", (ftnlen)255, (ftnlen)11);

/*     Create the CK. */

    if (exists_(ck, (ftnlen)255)) {
	delfil_(ck, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ckopn_(ck, ck, &c__0, &handle, (ftnlen)255, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*      CALL TOSTDO ( 'Opened CK '//CK ) */
    nseg = 3;
    i__1 = nseg;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xinsts[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("xinsts", 
		i__3, "f_ck06__", (ftnlen)5239)] = i__ * -1000;
    }
    s_copy(xrefs, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(xrefs + 32, "MARSIAU", (ftnlen)32, (ftnlen)7);
    s_copy(xrefs + 64, "GALACTIC", (ftnlen)32, (ftnlen)8);
    s_copy(ckfram, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    sellst = TRUE_;

/*     Set the interval counts for the three segments. */

    nivals[0] = 98;
    nivals[1] = 398;
    nivals[2] = 10381;

/*     Create data for the segments and write them to the file. */

    n = 0;
    segptr = 0;
    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {
	segptr += n;

/*        Set the mini-segment attributes for the current segment. */

	xinst = xinsts[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"xinsts", i__3, "f_ck06__", (ftnlen)5270)];
	nintvl = nivals[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"nivals", i__3, "f_ck06__", (ftnlen)5271)];
	s_copy(xref, xrefs + (((i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : 
		s_rnge("xrefs", i__3, "f_ck06__", (ftnlen)5272)) << 5), (
		ftnlen)32, (ftnlen)32);
	i__3 = nintvl;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5278)] = 3;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5279)] = 7;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5280)] = 3;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5281)] = 1.1;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5282)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5286)] = 1;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5287)] = 4;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5288)] = 7;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5289)] = .8;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5290)] = 9;
	    } else {
		subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"subtps", i__2, "f_ck06__", (ftnlen)5294)] = 0;
		psizes[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"psizes", i__2, "f_ck06__", (ftnlen)5295)] = 8;
		degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"degres", i__2, "f_ck06__", (ftnlen)5296)] = 7;
		rates[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"rates", i__2, "f_ck06__", (ftnlen)5297)] = 4.;
		npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
			"npkts", i__2, "f_ck06__", (ftnlen)5298)] = 11;
	    }
	}
	pad = 1;

/*        Create time tags and packets for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__3 = nintvl;
	for (mnsgno = 1; mnsgno <= i__3; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		    "datidx", i__2, "f_ck06__", (ftnlen)5318)] = n + pad;

/*           Record the corresponding start index in the packet array. */

	    pktbeg[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		    "pktbeg", i__2, "f_ck06__", (ftnlen)5323)] = pktptr;

/*           Generate time tags and packet data for the current */
/*           mini-segment. */

	    rate = rates[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("rates", i__2, "f_ck06__", (ftnlen)5329)];
	    subtyp = subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("subtps", i__2, "f_ck06__", (ftnlen)5330)];
	    i__5 = npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5332)] << 2;
	    moved_(&xquats[(i__2 = (segptr + n << 2) - 4) < 3360000 && 0 <= 
		    i__2 ? i__2 : s_rnge("xquats", i__2, "f_ck06__", (ftnlen)
		    5332)], &i__5, quats);
	    i__5 = npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5333)] * 3;
	    moved_(&xavvs[(i__2 = (segptr + n) * 3 - 3) < 2520000 && 0 <= 
		    i__2 ? i__2 : s_rnge("xavvs", i__2, "f_ck06__", (ftnlen)
		    5333)], &i__5, avvs);
	    moved_(&xepoch[(i__2 = segptr + n - 1) < 840000 && 0 <= i__2 ? 
		    i__2 : s_rnge("xepoch", i__2, "f_ck06__", (ftnlen)5334)], 
		    &npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5334)], epochs);

/*           The start time of the current interval is the epoch at */
/*           PAD positions later than the start epoch. */

	    xivbds[(i__2 = mnsgno - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge(
		    "xivbds", i__2, "f_ck06__", (ftnlen)5340)] = epochs[(i__4 
		    = pad) < 840000 && 0 <= i__4 ? i__4 : s_rnge("epochs", 
		    i__4, "f_ck06__", (ftnlen)5340)];

/*           Setting up the data arrays for CKW06 is simple: we simply */
/*           append the data for each mini-segment. */

	    moved_(epochs, &npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
		    i__2 : s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5346)], &
		    sclkdp[(i__4 = n - 1) < 840000 && 0 <= i__4 ? i__4 : 
		    s_rnge("sclkdp", i__4, "f_ck06__", (ftnlen)5346)]);

/*           We're not using angular acceleration. */

	    cleard_(&c__3, davv);
	    i__4 = npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5353)];
	    for (j = 1; j <= i__4; ++j) {
		t_xsubtp__(&quats[(i__2 = (j << 2) - 4) < 3360000 && 0 <= 
			i__2 ? i__2 : s_rnge("quats", i__2, "f_ck06__", (
			ftnlen)5355)], &avvs[(i__5 = j * 3 - 3) < 2520000 && 
			0 <= i__5 ? i__5 : s_rnge("avvs", i__5, "f_ck06__", (
			ftnlen)5355)], davv, &subtps[(i__6 = mnsgno - 1) < 
			21000 && 0 <= i__6 ? i__6 : s_rnge("subtps", i__6, 
			"f_ck06__", (ftnlen)5355)], &psizes[(i__7 = mnsgno - 
			1) < 21000 && 0 <= i__7 ? i__7 : s_rnge("psizes", 
			i__7, "f_ck06__", (ftnlen)5355)], record);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		moved_(record, &psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= 
			i__2 ? i__2 : s_rnge("psizes", i__2, "f_ck06__", (
			ftnlen)5359)], &packts[(i__5 = pktptr - 1) < 6720000 
			&& 0 <= i__5 ? i__5 : s_rnge("packts", i__5, "f_ck06"
			"__", (ftnlen)5359)]);
		pktptr += psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("psizes", i__2, "f_ck06__", (ftnlen)
			5361)];
	    }

/*           Adjust the starting record number of the next mini-segment */
/*           to account for padding. */

	    begrec = begrec - 1 + npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= 
		    i__4 ? i__4 : s_rnge("npkts", i__4, "f_ck06__", (ftnlen)
		    5369)] - (pad << 1);

/*           Update the count of the records in the segment. */

	    n += npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5374)];
	}

/*        The stop time of the last interval is the epoch at */
/*        PAD positions before the final epoch. */

	xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge("xivbds", 
		i__3, "f_ck06__", (ftnlen)5382)] = sclkdp[(i__4 = n - pad - 1)
		 < 840000 && 0 <= i__4 ? i__4 : s_rnge("sclkdp", i__4, "f_ck"
		"06__", (ftnlen)5382)];

/*        Let the segment bounds coincide with the start and stop */
/*        times of the first and last mini-segments. */

	first = xivbds[0];
	last = xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		"xivbds", i__3, "f_ck06__", (ftnlen)5389)];
	ckw06_(&handle, &xinst, xref, &xavflg, &first, &last, segid, &nintvl, 
		npkts, subtps, degres, packts, rates, sclkdp, xivbds, &sellst,
		 (ftnlen)32, (ftnlen)60);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close the kernel. */

    ckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

/*  ### */

/*     Perform tests on CK06B1. */

    s_copy(ck, "ck06big1.bc", (ftnlen)255, (ftnlen)11);
    sellst = FALSE_;
    s_copy(title, "Interval/Packet selection logic test:  look up data at ti"
	    "me tags within all three segments of CK #.", (ftnlen)800, (ftnlen)
	    99);
    repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
	    ftnlen)800);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_(title, (ftnlen)800);

/*     Load the test CK. */

    furnsh_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    segptr = 0;
    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {
	segptr += n;

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Interval/Packet selection logic test:  look up data a"
		"t time tags within segment # of CK #.", (ftnlen)800, (ftnlen)
		90);
	repmi_(title, "#", &segno, title, (ftnlen)800, (ftnlen)1, (ftnlen)800)
		;
	repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
		ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);
	xinst = xinsts[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"xinsts", i__3, "f_ck06__", (ftnlen)5466)];
	nintvl = nivals[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"nivals", i__3, "f_ck06__", (ftnlen)5467)];
	s_copy(xref, xrefs + (((i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : 
		s_rnge("xrefs", i__3, "f_ck06__", (ftnlen)5468)) << 5), (
		ftnlen)32, (ftnlen)32);
	i__3 = nintvl;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5474)] = 3;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5475)] = 7;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5476)] = 3;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5477)] = 1.1;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5478)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5482)] = 1;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5483)] = 4;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5484)] = 7;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5485)] = .8;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5486)] = 9;
	    } else {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5490)] = 0;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5491)] = 8;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5492)] = 7;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5493)] = 4.;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5494)] = 11;
	    }
	}

/*        Create time tags and packets for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__3 = nintvl;
	for (mnsgno = 1; mnsgno <= i__3; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
		    "datidx", i__4, "f_ck06__", (ftnlen)5513)] = n + pad;

/*           Generate time tags and packet data for the current */
/*           mini-segment. */

	    moved_(&xepoch[(i__4 = segptr + n - 1) < 840000 && 0 <= i__4 ? 
		    i__4 : s_rnge("xepoch", i__4, "f_ck06__", (ftnlen)5518)], 
		    &npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)5518)], &sclkdp[
		    (i__5 = n - 1) < 840000 && 0 <= i__5 ? i__5 : s_rnge(
		    "sclkdp", i__5, "f_ck06__", (ftnlen)5518)]);
/*           Update the count of the records in the segment. */

	    n += npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)5522)];
	}

/*        Check data for each interval. */

	if (segno == 3) {

/*           SKIP can be reset to 40 to increase execution speed. */

	    skip = 1;
	} else {
	    skip = 1;
	}
	i__3 = nintvl;
	i__4 = skip;
	for (mnsgno = 1; i__4 < 0 ? mnsgno >= i__3 : mnsgno <= i__3; mnsgno +=
		 i__4) {
	    j = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)5540)];
	    if (! sellst && mnsgno > 1) {

/*              Skip the first time tag of each mini-segment but the */
/*              first, since the correct data will come from the */
/*              previous interval. */

		++j;
	    }
	    jmax = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)5552)] - 1 + 
		    npkts[(i__5 = mnsgno - 1) < 21000 && 0 <= i__5 ? i__5 : 
		    s_rnge("npkts", i__5, "f_ck06__", (ftnlen)5552)] - (pad <<
		     1);
	    while(j <= jmax) {
		t = sclkdp[(i__2 = j - 1) < 840000 && 0 <= i__2 ? i__2 : 
			s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)5557)];
		moved_(&xavvs[(i__2 = (segptr + j) * 3 - 3) < 2520000 && 0 <= 
			i__2 ? i__2 : s_rnge("xavvs", i__2, "f_ck06__", (
			ftnlen)5559)], &c__3, xav);
		moved_(&xcmats[(i__2 = (segptr + j) * 9 - 9) < 7560000 && 0 <=
			 i__2 ? i__2 : s_rnge("xcmats", i__2, "f_ck06__", (
			ftnlen)5561)], &c__9, xcmat);

/*              Look up pointing data. */

		cktol = 0.;
		ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (! (*ok)) {
		    sigerr_("NO POINTING", (ftnlen)11);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_stop("", (ftnlen)0);
		}
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*              Check output time. */

		chcksd_("CLKOUT", &clkout, "~", &t, &c_b110, ok, (ftnlen)6, (
			ftnlen)1);
		if (! (*ok)) {

/*                 Re-check with a more specific item name. */

		    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80,
			     (ftnlen)28);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80,
			     (ftnlen)1);
		}

/*              Check C-matrix. */

		chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			ftnlen)4, (ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (
			    ftnlen)26);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			    ftnlen)80, (ftnlen)3);
		}

/*              Check angular velocity. */

/*              For derived AV, use a looser tolerance. */

		if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("subtps", i__2, "f_ck06__", (ftnlen)5621)] == 
			1) {
		    tol = 1e-6;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			5625)] == 0) {
		    tol = 1e-10;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			5629)] == 3) {
		    tol = 1e-14;
		} else {
		    tol = 1e-10;
		}
		chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (
			ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (
			    ftnlen)24);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, 
			    (ftnlen)3);
		}
		++j;
	    }

/*           End of tests for current interval. */

	}

/*        End of tests for current segment. */

    }

/* --- Case: ------------------------------------------------------ */


/*     Perform tests using data lookups at midpoints */
/*     between consecutive time tags. This set of tests */
/*     is for segments using the "select first" option. */

    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Interval/Packet selection logic test:  look up data a"
		"t midpoints between time tags within segment # of CK #.", (
		ftnlen)800, (ftnlen)108);
	repmi_(title, "#", &segno, title, (ftnlen)800, (ftnlen)1, (ftnlen)800)
		;
	repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
		ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);
	sellst = FALSE_;
	xinst = xinsts[(i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		"xinsts", i__4, "f_ck06__", (ftnlen)5694)];
	nintvl = nivals[(i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		"nivals", i__4, "f_ck06__", (ftnlen)5695)];
	s_copy(xref, xrefs + (((i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : 
		s_rnge("xrefs", i__4, "f_ck06__", (ftnlen)5696)) << 5), (
		ftnlen)32, (ftnlen)32);
	i__4 = nintvl;
	for (i__ = 1; i__ <= i__4; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)5702)] = 3;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)5703)] = 7;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)5704)] = 3;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)5705)] = 1.1;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)5706)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)5710)] = 1;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)5711)] = 4;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)5712)] = 7;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)5713)] = .8;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)5714)] = 9;
	    } else {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)5718)] = 0;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)5719)] = 8;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)5720)] = 7;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)5721)] = 4.;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)5722)] = 11;
	    }
	}

/*        Create time tags for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__4 = nintvl;
	for (mnsgno = 1; mnsgno <= i__4; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "datidx", i__3, "f_ck06__", (ftnlen)5741)] = n + pad;
	    rate = rates[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("rates", i__3, "f_ck06__", (ftnlen)5743)];
	    subtyp = subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("subtps", i__3, "f_ck06__", (ftnlen)5744)];
	    t_gentag__(&begrec, &npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= 
		    i__3 ? i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)
		    5746)], epochs);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The start time of the current interval is the epoch at */
/*           PAD positions later than the start epoch. */

	    xivbds[(i__3 = mnsgno - 1) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		    "xivbds", i__3, "f_ck06__", (ftnlen)5753)] = epochs[(i__2 
		    = pad) < 840000 && 0 <= i__2 ? i__2 : s_rnge("epochs", 
		    i__2, "f_ck06__", (ftnlen)5753)];
	    moved_(epochs, &npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? 
		    i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)5755)], &
		    sclkdp[(i__2 = n - 1) < 840000 && 0 <= i__2 ? i__2 : 
		    s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)5755)]);

/*           Adjust the starting record number of the next mini-segment */
/*           to account for padding. */

	    begrec = begrec - 1 + npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= 
		    i__3 ? i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)
		    5761)] - (pad << 1);

/*           Update the count of the records in the segment. */

	    n += npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("npkts", i__3, "f_ck06__", (ftnlen)5766)];
	}

/*        Check data for each interval. */

	if (segno == 3) {
	    skip = 1;
	} else {
	    skip = 1;
	}
	i__4 = nintvl;
	i__3 = skip;
	for (mnsgno = 1; i__3 < 0 ? mnsgno >= i__4 : mnsgno <= i__4; mnsgno +=
		 i__3) {
	    rate = rates[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("rates", i__2, "f_ck06__", (ftnlen)5781)];
	    j = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)5782)];
	    jmax = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)5784)] - 1 + 
		    npkts[(i__5 = mnsgno - 1) < 21000 && 0 <= i__5 ? i__5 : 
		    s_rnge("npkts", i__5, "f_ck06__", (ftnlen)5784)] - (pad <<
		     1);
	    while(j < jmax) {
		t = (sclkdp[(i__2 = j - 1) < 840000 && 0 <= i__2 ? i__2 : 
			s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)5788)] + 
			sclkdp[(i__5 = j) < 840000 && 0 <= i__5 ? i__5 : 
			s_rnge("sclkdp", i__5, "f_ck06__", (ftnlen)5788)]) / 
			2;
/*              The following formula for ET looks strange, but it's */
/*              correct. */

		et = t * rate;

/*              Generate expected results for ET. */

		sxform_(xref, ckfram, &et, xform, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		xf2rav_(xform, xcmat, xav);

/*              Look up pointing data. */

		cktol = 0.;
		ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (! (*ok)) {
		    sigerr_("NO POINTING", (ftnlen)11);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_stop("", (ftnlen)0);
		}
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*              Check output time. */

		chcksd_("CLKOUT", &clkout, "~", &t, &c_b110, ok, (ftnlen)6, (
			ftnlen)1);
		if (! (*ok)) {

/*                 Re-check with a more specific item name. */

		    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80,
			     (ftnlen)28);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80,
			     (ftnlen)1);
		}

/*              Check C-matrix. */

		chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			ftnlen)4, (ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (
			    ftnlen)26);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			    ftnlen)80, (ftnlen)3);
		}

/*              Check angular velocity. */

/*              For derived AV, use a looser tolerance. */

		if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("subtps", i__2, "f_ck06__", (ftnlen)5860)] == 
			1) {
		    tol = 1e-6;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			5864)] == 0) {
		    tol = 1e-10;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			5868)] == 3) {
		    tol = 1e-14;
		} else {
		    tol = 1e-10;
		}
		chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (
			ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (
			    ftnlen)24);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, 
			    (ftnlen)3);
		}
		++j;
		pktptr += psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("psizes", i__2, "f_ck06__", (ftnlen)
			5891)];
	    }

/*           End of tests for current interval. */

	}

/*        End of tests for current segment. */

    }
    unload_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Perform tests on CK06B2. */

    s_copy(ck, "ck06big2.bc", (ftnlen)255, (ftnlen)11);
    sellst = TRUE_;
    s_copy(title, "Interval/Packet selection logic test:  look up data at ti"
	    "me tags within all three segments of CK #.", (ftnlen)800, (ftnlen)
	    99);
    repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
	    ftnlen)800);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_(title, (ftnlen)800);

/*     Load the test CK. */

    furnsh_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    segptr = 0;
    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {
	segptr += n;

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Interval/Packet selection logic test:  look up data a"
		"t time tags within segment # of CK #.", (ftnlen)800, (ftnlen)
		90);
	repmi_(title, "#", &segno, title, (ftnlen)800, (ftnlen)1, (ftnlen)800)
		;
	repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
		ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);
	xinst = xinsts[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"xinsts", i__3, "f_ck06__", (ftnlen)5952)];
	nintvl = nivals[(i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"nivals", i__3, "f_ck06__", (ftnlen)5953)];
	s_copy(xref, xrefs + (((i__3 = segno - 1) < 3 && 0 <= i__3 ? i__3 : 
		s_rnge("xrefs", i__3, "f_ck06__", (ftnlen)5954)) << 5), (
		ftnlen)32, (ftnlen)32);
	i__3 = nintvl;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5960)] = 3;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5961)] = 7;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5962)] = 3;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5963)] = 1.1;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5964)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5968)] = 1;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5969)] = 4;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5970)] = 7;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5971)] = .8;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5972)] = 9;
	    } else {
		subtps[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"subtps", i__4, "f_ck06__", (ftnlen)5976)] = 0;
		psizes[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"psizes", i__4, "f_ck06__", (ftnlen)5977)] = 8;
		degres[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"degres", i__4, "f_ck06__", (ftnlen)5978)] = 7;
		rates[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"rates", i__4, "f_ck06__", (ftnlen)5979)] = 4.;
		npkts[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
			"npkts", i__4, "f_ck06__", (ftnlen)5980)] = 11;
	    }
	}

/*        Create time tags and packets for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__3 = nintvl;
	for (mnsgno = 1; mnsgno <= i__3; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : s_rnge(
		    "datidx", i__4, "f_ck06__", (ftnlen)5998)] = n + pad;
	    moved_(&xepoch[(i__4 = segptr + n - 1) < 840000 && 0 <= i__4 ? 
		    i__4 : s_rnge("xepoch", i__4, "f_ck06__", (ftnlen)6000)], 
		    &npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)6000)], &sclkdp[
		    (i__5 = n - 1) < 840000 && 0 <= i__5 ? i__5 : s_rnge(
		    "sclkdp", i__5, "f_ck06__", (ftnlen)6000)]);

/*           Update the count of the records in the segment. */

	    n += npkts[(i__4 = mnsgno - 1) < 21000 && 0 <= i__4 ? i__4 : 
		    s_rnge("npkts", i__4, "f_ck06__", (ftnlen)6004)];
	}

/*        The stop time of the last interval is the epoch at */
/*        PAD positions before the final epoch. */

	xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge("xivbds", 
		i__3, "f_ck06__", (ftnlen)6012)] = sclkdp[(i__4 = n - pad - 1)
		 < 840000 && 0 <= i__4 ? i__4 : s_rnge("sclkdp", i__4, "f_ck"
		"06__", (ftnlen)6012)];

/*        Let the segment bounds coincide with the start and stop */
/*        times of the first and last mini-segments. */

	first = xivbds[0];
	last = xivbds[(i__3 = nintvl) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		"xivbds", i__3, "f_ck06__", (ftnlen)6019)];

/*        Check data for each interval. */

	if (segno == 3) {
	    skip = 1;
	} else {
	    skip = 1;
	}
	n = 1;
	i__3 = nintvl;
	i__4 = skip;
	for (mnsgno = 1; i__4 < 0 ? mnsgno >= i__3 : mnsgno <= i__3; mnsgno +=
		 i__4) {
	    j = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)6034)];
	    jmax = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)6036)] - 1 + 
		    npkts[(i__5 = mnsgno - 1) < 21000 && 0 <= i__5 ? i__5 : 
		    s_rnge("npkts", i__5, "f_ck06__", (ftnlen)6036)] - (pad <<
		     1);
	    if (sellst && mnsgno < nintvl) {

/*              Skip the last time tag of each mini-segment but the */
/*              last, since the correct data will come from the */
/*              next interval. */

		--jmax;
	    }

/*           In the test below, ".LT." is used for the "select last" */
/*           option. */

	    while(j < jmax) {
		t = sclkdp[(i__2 = j - 1) < 840000 && 0 <= i__2 ? i__2 : 
			s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)6054)];
		moved_(&xquats[(i__2 = (segptr + j << 2) - 4) < 3360000 && 0 
			<= i__2 ? i__2 : s_rnge("xquats", i__2, "f_ck06__", (
			ftnlen)6056)], &c__4, xq);
		moved_(&xavvs[(i__2 = (segptr + j) * 3 - 3) < 2520000 && 0 <= 
			i__2 ? i__2 : s_rnge("xavvs", i__2, "f_ck06__", (
			ftnlen)6057)], &c__3, xav);
		q2m_(xq, xcmat);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Look up pointing data. */

		ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (! (*ok)) {
		    sigerr_("NO POINTING", (ftnlen)11);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_stop("", (ftnlen)0);
		}
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*              Check output time. */

		chcksd_("CLKOUT", &clkout, "~", &t, &c_b110, ok, (ftnlen)6, (
			ftnlen)1);
		if (! (*ok)) {

/*                 Re-check with a more specific item name. */

		    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80,
			     (ftnlen)28);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80,
			     (ftnlen)1);
		}

/*              Check C-matrix. */

		chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			ftnlen)4, (ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (
			    ftnlen)26);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			    ftnlen)80, (ftnlen)3);
		}

/*              Check angular velocity. */

/*              For derived AV, use a looser tolerance. */

		if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("subtps", i__2, "f_ck06__", (ftnlen)6121)] == 
			1) {
		    tol = 1e-6;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			6125)] == 0) {
		    tol = 1e-10;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			6129)] == 3) {
		    tol = 1e-14;
		} else {
		    tol = 1e-10;
		}
		chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (
			ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (
			    ftnlen)24);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, 
			    (ftnlen)3);
		}
		++j;
	    }
	    n += npkts[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_ck06__", (ftnlen)6155)];

/*           End of tests for current interval. */

	}

/*        End of tests for current segment. */

    }

/* --- Case: ------------------------------------------------------ */


/*     Perform tests using data lookups at midpoints */
/*     between consecutive time tags. This set of tests */
/*     is for segments using the "select last" option. */

/*     CK is CK06B2. */

    i__1 = nseg;
    for (segno = 1; segno <= i__1; ++segno) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Interval/Packet selection logic test:  look up data a"
		"t midpoints between time tags within segment # of CK #.", (
		ftnlen)800, (ftnlen)108);
	repmi_(title, "#", &segno, title, (ftnlen)800, (ftnlen)1, (ftnlen)800)
		;
	repmc_(title, "#", ck, title, (ftnlen)800, (ftnlen)1, (ftnlen)255, (
		ftnlen)800);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)800);
	sellst = TRUE_;
	xinst = xinsts[(i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		"xinsts", i__4, "f_ck06__", (ftnlen)6197)];
	nintvl = nivals[(i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		"nivals", i__4, "f_ck06__", (ftnlen)6198)];
	s_copy(xref, xrefs + (((i__4 = segno - 1) < 3 && 0 <= i__4 ? i__4 : 
		s_rnge("xrefs", i__4, "f_ck06__", (ftnlen)6199)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(ckfram, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
	i__4 = nintvl;
	for (i__ = 1; i__ <= i__4; ++i__) {
	    if (i__ % 3 == 0) {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)6206)] = 3;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)6207)] = 7;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)6208)] = 3;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)6209)] = 1.1;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)6210)] = 5;
	    } else if (i__ % 3 == 1) {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)6214)] = 1;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)6215)] = 4;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)6216)] = 7;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)6217)] = .8;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)6218)] = 9;
	    } else {
		subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"subtps", i__3, "f_ck06__", (ftnlen)6222)] = 0;
		psizes[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"psizes", i__3, "f_ck06__", (ftnlen)6223)] = 8;
		degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"degres", i__3, "f_ck06__", (ftnlen)6224)] = 7;
		rates[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"rates", i__3, "f_ck06__", (ftnlen)6225)] = 4.;
		npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
			"npkts", i__3, "f_ck06__", (ftnlen)6226)] = 11;
	    }
	}

/*        Create time tags for the current segment. */

	begrec = 1;
	n = 1;
	pktptr = 1;
	i__4 = nintvl;
	for (mnsgno = 1; mnsgno <= i__4; ++mnsgno) {

/*           Record the index of the first non-pad time tag of */
/*           the current mini-segment. */

	    datidx[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "datidx", i__3, "f_ck06__", (ftnlen)6245)] = n + pad;
	    rate = rates[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("rates", i__3, "f_ck06__", (ftnlen)6247)];
	    subtyp = subtps[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("subtps", i__3, "f_ck06__", (ftnlen)6248)];
	    t_gentag__(&begrec, &npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= 
		    i__3 ? i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)
		    6250)], epochs);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The start time of the current interval is the epoch at */
/*           PAD positions later than the start epoch. */

	    xivbds[(i__3 = mnsgno - 1) < 21001 && 0 <= i__3 ? i__3 : s_rnge(
		    "xivbds", i__3, "f_ck06__", (ftnlen)6257)] = epochs[(i__2 
		    = pad) < 840000 && 0 <= i__2 ? i__2 : s_rnge("epochs", 
		    i__2, "f_ck06__", (ftnlen)6257)];
	    moved_(epochs, &npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? 
		    i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)6259)], &
		    sclkdp[(i__2 = n - 1) < 840000 && 0 <= i__2 ? i__2 : 
		    s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)6259)]);

/*           Adjust the starting record number of the next mini-segment */
/*           to account for padding. */

	    begrec = begrec - 1 + npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= 
		    i__3 ? i__3 : s_rnge("npkts", i__3, "f_ck06__", (ftnlen)
		    6265)] - (pad << 1);

/*           Update the count of the records in the segment. */

	    n += npkts[(i__3 = mnsgno - 1) < 21000 && 0 <= i__3 ? i__3 : 
		    s_rnge("npkts", i__3, "f_ck06__", (ftnlen)6270)];
	}

/*        The stop time of the last interval is the epoch at */
/*        PAD positions before the final epoch. */

	xivbds[(i__4 = nintvl) < 21001 && 0 <= i__4 ? i__4 : s_rnge("xivbds", 
		i__4, "f_ck06__", (ftnlen)6278)] = sclkdp[(i__3 = n - pad - 1)
		 < 840000 && 0 <= i__3 ? i__3 : s_rnge("sclkdp", i__3, "f_ck"
		"06__", (ftnlen)6278)];

/*        Let the segment bounds coincide with the start and stop */
/*        times of the first and last mini-segments. */

	first = xivbds[0];
	last = xivbds[(i__4 = nintvl) < 21001 && 0 <= i__4 ? i__4 : s_rnge(
		"xivbds", i__4, "f_ck06__", (ftnlen)6285)];

/*        Check data for each interval. */

	if (segno == 3) {
	    skip = 1;
	} else {
	    skip = 1;
	}
	i__4 = nintvl;
	i__3 = skip;
	for (mnsgno = 1; i__3 < 0 ? mnsgno >= i__4 : mnsgno <= i__4; mnsgno +=
		 i__3) {
	    rate = rates[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("rates", i__2, "f_ck06__", (ftnlen)6300)];
	    j = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)6301)];
	    jmax = datidx[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("datidx", i__2, "f_ck06__", (ftnlen)6303)] - 1 + 
		    npkts[(i__5 = mnsgno - 1) < 21000 && 0 <= i__5 ? i__5 : 
		    s_rnge("npkts", i__5, "f_ck06__", (ftnlen)6303)] - (pad <<
		     1);
	    while(j < jmax) {
		t = (sclkdp[(i__2 = j - 1) < 840000 && 0 <= i__2 ? i__2 : 
			s_rnge("sclkdp", i__2, "f_ck06__", (ftnlen)6307)] + 
			sclkdp[(i__5 = j) < 840000 && 0 <= i__5 ? i__5 : 
			s_rnge("sclkdp", i__5, "f_ck06__", (ftnlen)6307)]) / 
			2;
/*              The following formula for ET looks strange, but it's */
/*              correct. */

		et = t * rate;

/*              Generate expected results for ET. */

		sxform_(xref, ckfram, &et, xform, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		xf2rav_(xform, xcmat, xav);

/*              Look up pointing data. */

		ckgpav_(&xinst, &t, &cktol, xref, cmat, av, &clkout, &found, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (! (*ok)) {
		    sigerr_("NO POINTING", (ftnlen)11);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_stop("", (ftnlen)0);
		}
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*              Check output time. */

		chcksd_("CLKOUT", &clkout, "~", &t, &c_b110, ok, (ftnlen)6, (
			ftnlen)1);
		if (! (*ok)) {

/*                 Re-check with a more specific item name. */

		    s_copy(name__, "Mini-segment *; CLKOUT no. *", (ftnlen)80,
			     (ftnlen)28);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksd_(name__, &clkout, "~", &t, &c_b110, ok, (ftnlen)80,
			     (ftnlen)1);
		}

/*              Check C-matrix. */

		chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			ftnlen)4, (ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; CMAT no. *", (ftnlen)80, (
			    ftnlen)26);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_(name__, cmat, "~~/", xcmat, &c__9, &c_b333, ok, (
			    ftnlen)80, (ftnlen)3);
		}

/*              Check angular velocity. */

/*              For derived AV, use a looser tolerance. */

		if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("subtps", i__2, "f_ck06__", (ftnlen)6378)] == 
			1) {
		    tol = 1e-6;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			6382)] == 0) {
		    tol = 1e-10;
		} else if (subtps[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("subtps", i__2, "f_ck06__", (ftnlen)
			6386)] == 3) {
		    tol = 1e-14;
		} else {
		    tol = 1e-10;
		}
		chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (
			ftnlen)3);
		if (! (*ok)) {
		    s_copy(name__, "Mini-segment *; AV no. *", (ftnlen)80, (
			    ftnlen)24);
		    repmi_(name__, "*", &mnsgno, name__, (ftnlen)80, (ftnlen)
			    1, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(name__, "*", &j, name__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, 
			    (ftnlen)3);
		}
		++j;
		pktptr += psizes[(i__2 = mnsgno - 1) < 21000 && 0 <= i__2 ? 
			i__2 : s_rnge("psizes", i__2, "f_ck06__", (ftnlen)
			6410)];
	    }

/*           End of tests for current interval. */

	}

/*        End of tests for current segment. */

    }
    unload_(ck, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete CK files.", (ftnlen)27);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("test06err.bc", (ftnlen)12)) {
	delfil_("test06err.bc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06mult.bc", (ftnlen)11)) {
	delfil_("ck06mult.bc", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06t0.bc", (ftnlen)9)) {
	delfil_("ck06t0.bc", (ftnlen)9);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06t1.bc", (ftnlen)9)) {
	delfil_("ck06t1.bc", (ftnlen)9);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06big0.bc", (ftnlen)11)) {
	delfil_("ck06big0.bc", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06big1.bc", (ftnlen)11)) {
	delfil_("ck06big1.bc", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("ck06big2.bc", (ftnlen)11)) {
	delfil_("ck06big2.bc", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_ck06__ */

/* ***************************************************************** */
/* ***************************************************************** */
/* * */
/* * */
/* * */
/* * */
/* *    T E S T   U T I L I T I E S */
/* * */
/* * */
/* * */
/* * */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */

/*     T_GENC06 */

/* ***************************************************************** */

/*     Generate C-matrices for CK type 06 software testing. */

/*        Version 1.0.0 03-FEB-2014 (NJB) */


/*     Output AVV has units of radians/s. */

/* Subroutine */ int t_genc06__(integer *segno, integer *mnsgno, integer *
	begrec, integer *n, char *frame, doublereal *rate, doublereal *epochs,
	 doublereal *quats, doublereal *avvs, logical *ok, ftnlen frame_len)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), log(doublereal);

    /* Local variables */
    doublereal cmat[9]	/* was [3][3] */, axis[3];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), mxmg_(doublereal *, doublereal *, integer *, integer *, 
	    integer *, doublereal *);
    doublereal resx[36]	/* was [6][6] */;
    integer i__, j, k;
    doublereal m, s, angle;
    extern /* Subroutine */ int chkin_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal xform[36]	/* was [6][6] */;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    doublereal dangl1, angle1;
    extern /* Subroutine */ int rav2xf_(doublereal *, doublereal *, 
	    doublereal *), xf2rav_(doublereal *, doublereal *, doublereal *);
    doublereal xf[36]	/* was [6][6] */;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    logical samfrm;
    extern /* Subroutine */ int axisar_(doublereal *, doublereal *, 
	    doublereal *), chkout_(char *, ftnlen), sxform_(char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    extern logical return_(void);
    extern /* Subroutine */ int m2q_(doublereal *, doublereal *);
    extern doublereal dpr_(void);


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_GENC06", (ftnlen)8);

/*     We'll construct a sequence of C-matrices and angular velocities */
/*     representing rotations about a constant axis in the frame */
/*     designated by IREF. Those rotations and corresponding angular */
/*     velocities will be transformed to the requested output frame. */

    samfrm = eqstr_("J2000", frame, (ftnlen)5, frame_len);

/*     Below we define some parameters used for rotation angle */
/*     construction. */

/*     S is a scale factor for the segment number. */

    s = 2.5e9;

/*     M is a scale factor for the mini-segment number. */

    m = 5e4;

/*     AXIS is the rotation axis in the J2000 frame. */

    vpack_(&c_b110, &c_b110, &c_b189, axis);

/*     ANGLE1 is the base angle used to generate all of the rotation */
/*     angles. These angles are computed using the formula */

/*                                          S*SEGNO + M*MNSGNO + I */
/*        ANGLE( SEGNO, MNSGNO, I ) = ANGLE1 */

    d__1 = dpr_();
    d__2 = 1. / (s * *segno + m * *mnsgno + 100000);
    angle1 = pow_dd(&d__1, &d__2);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = *begrec - 1 + i__;
	epochs[i__ - 1] = (doublereal) j;
	d__1 = s * *segno + m * *mnsgno + j;
	angle = pow_dd(&angle1, &d__1);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);

/*        Create an angular velocity vector that is roughly consistent */
/*        with the C-matrix sequence. We'll treat the angle as a */
/*        function of I and differentiate the angle formula with respect */
/*        to I. Note that */

/*           ANGLE = EXP( LOG(ANGLE1) * (S*SEGNO + M*MNSGNO + I) ) */


	dangl1 = angle * log(angle1);

/*        Scale the angular rate from radians/tick to radians/s. */

/*        RATE has units of seconds/tick. */

	dangl1 /= *rate;
	vscl_(&dangl1, axis, &avvs[i__ * 3 - 3]);

/*        Transform the C-matrix and AVV to the output frame */
/*        if necessary. The output frame is the new base frame. */

	if (! samfrm) {
	    sxform_(frame, "J2000", &epochs[i__ - 1], xform, frame_len, (
		    ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    rav2xf_(cmat, &avvs[i__ * 3 - 3], xf);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    mxmg_(xf, xform, &c__6, &c__6, &c__6, resx);
	    xf2rav_(resx, cmat, &avvs[i__ * 3 - 3]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	m2q_(cmat, &quats[(i__ << 2) - 4]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (quats[(i__ << 2) - 4] < 0.) {
	    for (k = 0; k <= 3; ++k) {
		quats[k + (i__ << 2) - 4] = -quats[k + (i__ << 2) - 4];
	    }
	}
    }
    chkout_("T_GENC06", (ftnlen)8);
    return 0;
} /* t_genc06__ */

/* ***************************************************************** */

/*     T_GENCSM ( Generate C-matrices from smooth data ) */

/* ***************************************************************** */

/*     Generate C-matrices for CK type 06 software testing. */
/*     Use smooth data: earth rotation from a test utility PCK. */

/*        Version 1.0.0 03-FEB-2014 (NJB) */

/* Subroutine */ int t_gencsm__(integer *segno, integer *mnsgno, integer *
	begrec, integer *n, char *frame, doublereal *rate, integer *subtyp, 
	doublereal *epochs, doublereal *quats, doublereal *avvs, logical *ok, 
	ftnlen frame_len)
{
    /* Initialized data */

    static logical pass1 = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Local variables */
    doublereal cmat[9]	/* was [3][3] */;
    integer i__, j;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    doublereal xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int xf2rav_(doublereal *, doublereal *, 
	    doublereal *);
    doublereal et, qn[4];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    extern integer touchi_(integer *);
    extern doublereal vdistg_(doublereal *, doublereal *, integer *);
    extern /* Subroutine */ int vminug_(doublereal *, integer *, doublereal *)
	    , tstpck_(char *, logical *, logical *, ftnlen), sxform_(char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen);
    extern logical return_(void);
    extern /* Subroutine */ int m2q_(doublereal *, doublereal *);

/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */

    if (return_()) {
	return 0;
    }
/*      CALL CHKIN ( 'T_GENCSM' ) */
    i__ = touchi_(mnsgno);
    i__ = touchi_(segno);

/*     Load a test PCK on the first pass. */

    if (pass1) {
	tstpck_("test.tpc", &c_true, &c_false, (ftnlen)8);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pass1 = FALSE_;
    }

/*     We'll construct a sequence of C-matrices and angular velocities */
/*     for the orientation of the earth relative to the IREF frame. */
/*     Those rotations and corresponding angular velocities will be */
/*     transformed to the requested output frame. */

    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = *begrec - 1 + i__;
	epochs[i__ - 1] = (doublereal) j;

/*        Convert SCLK to ET for SXFORM lookup. This is necessary */
/*        in order to derived angular velocity consistent with */
/*        orientation. RATE has units of seconds/tick. */

	et = epochs[i__ - 1] * *rate;

/*         Initial orientation is that of the earth relative */
/*         to J2000. */

	sxform_(frame, "IAU_EARTH", &et, xform, frame_len, (ftnlen)9);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2rav_(xform, cmat, &avvs[i__ * 3 - 3]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	m2q_(cmat, &quats[(i__ << 2) - 4]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Adjust data to achieve better consistency, if necessary. */

	if (i__ > 1) {
	    if (*subtyp == 0 || *subtyp == 2) {
		vminug_(&quats[(i__ << 2) - 4], &c__4, qn);
		if (vdistg_(&quats[(i__ << 2) - 4], &quats[(i__ - 1 << 2) - 4]
			, &c__4) > vdistg_(qn, &quats[(i__ - 1 << 2) - 4], &
			c__4)) {

/*                 Replace the original quaternion with its negative. */

		    moved_(qn, &c__4, &quats[(i__ << 2) - 4]);
		}
	    }
	}
    }
/*      CALL CHKOUT ( 'T_GENCSM' ) */
    return 0;
} /* t_gencsm__ */

/* ***************************************************************** */

/*     T_GENTAG (Generate time tags for T_GENCSM) */

/* ***************************************************************** */

/*     Generate just the time tag sequence generated by T_GENCSM */
/*     for a given set of inputs. */

/*        Version 1.0.0 07-FEB-2014 (NJB) */

/* Subroutine */ int t_gentag__(integer *begrec, integer *n, doublereal *
	epochs)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    integer i__, j;
    extern logical return_(void);

/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }

/*     We'll construct a sequence of C-matrices and angular velocities */
/*     for the orientation of the earth relative to the IREF frame. */
/*     Those rotations and corresponding angular velocities will be */
/*     transformed to the requested output frame. */

    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = *begrec - 1 + i__;
	epochs[i__ - 1] = (doublereal) j;
    }
    return 0;
} /* t_gentag__ */

/* ***************************************************************** */

/*     T_XSUBTP */

/* ***************************************************************** */

/*     Transform record subtype. Input is always a quaternion */
/*     and avv; output is any other type. */

/*     For subtype 2, the caller must supply the derivative */
/*     with respect to ticks of the angular velocity. */

/* Subroutine */ int t_xsubtp__(doublereal *quat, doublereal *avv, doublereal 
	*davv, integer *subtyp, integer *size, doublereal *record)
{
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), moved_(doublereal *, 
	    integer *, doublereal *);
    doublereal dq[4];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), sigerr_(
	    char *, ftnlen), setmsg_(char *, ftnlen), errint_(char *, integer 
	    *, ftnlen);
    extern logical return_(void);
    doublereal avq[4];
    extern /* Subroutine */ int qxq_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */


/*     SPICELIB functions */

/*      DOUBLE PRECISION      VDOTG */

/*     Local variables */

    if (return_()) {
	return 0;
    }
/*      CALL CHKIN ( 'T_XSUBTP' ) */
    cleard_(size, record);
    if (*subtyp == 0) {

/*        Create a quaternion derivative from a quaternion and avv. Pack */
/*        the quaternion and its derivative into the RECORD array. */

	moved_(quat, &c__4, record);

/*        From the header of QDQ2AV we have the equation: */

/*                             * */
/*           AV  =  Im ( -2 * Q  * DQ )                        (1) */

/*        Here AV can be viewed as a quaternion with real part */
/*        equal to 0. Then */

/*           DQ = -0.5 * Q * AV */

	avq[0] = 0.;
	vscl_(&c_b1202, avv, &avq[1]);
	qxq_(quat, avq, dq);
	moved_(dq, &c__4, &record[4]);
    } else if (*subtyp == 1) {

/*        No transformation required. */

	moved_(quat, &c__4, record);
    } else if (*subtyp == 2) {

/*        Create a quaternion derivative from a quaternion and avv. Pack */
/*        the quaternion and its derivative into the RECORD array. */

	moved_(quat, &c__4, record);

/*        From the header of QDQ2AV we have the equation: */

/*                             * */
/*           AV  =  Im ( -2 * Q  * DQ )                        (1) */

/*        Here AV can be viewed as a quaternion with real part */
/*        equal to 0. Then */

/*           DQ = -0.5 * Q * AV */


	avq[0] = 0.;
	vscl_(&c_b1202, avv, &avq[1]);
	qxq_(quat, avq, dq);
	moved_(dq, &c__4, &record[4]);

/*        Transfer the avv and its derivative. */

	vequ_(avv, &record[8]);
	vequ_(davv, &record[11]);
    } else if (*subtyp == 3) {

/*        No transformation required. */

	moved_(quat, &c__4, record);
	vequ_(avv, &record[4]);
    } else {
	setmsg_("SUBTYP was #.", (ftnlen)13);
	errint_("#", subtyp, (ftnlen)1);
	sigerr_("SPICE(BUG)", (ftnlen)10);
/*         CALL CHKOUT ( 'T_XSUBTP' ) */
	return 0;
    }
/*      CALL CHKOUT ( 'T_XSUBTP' ) */
    return 0;
} /* t_xsubtp__ */

