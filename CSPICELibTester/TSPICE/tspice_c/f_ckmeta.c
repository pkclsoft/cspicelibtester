/* f_ckmeta.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c_b74 = -10000;
static logical c_true = TRUE_;
static integer c__0 = 0;
static logical c_false = FALSE_;

/* $Procedure      F_CKMETA ( Family of CKMETA tests ) */
/* Subroutine */ int f_ckmeta__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    char ch__1[16];
    cllist cl__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer f_clos(cllist *);

    /* Local variables */
    static integer ckbg[73], cksm[73], i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer spkbg[73], spkid;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer spkpb[73];
    extern /* Subroutine */ int nthwd_(char *, integer *, char *, integer *, 
	    ftnlen, ftnlen), topen_(char *, ftnlen);
    static integer spksm[73], spkps[73], sunit;
    extern /* Subroutine */ int t_success__(logical *);
    extern /* Character */ VOID begdat_(char *, ftnlen);
    static integer idcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     ckmeta_(integer *, char *, integer *, ftnlen);
    static integer sclkbg[73];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    static integer sclkid;
    extern /* Subroutine */ int kilfil_(char *, ftnlen);
    static integer sclkpb[73];
    static char sclkwb[32*73];
    extern /* Subroutine */ int clpool_(void), ldpool_(char *, ftnlen);
    static integer sclksm[73];
    extern /* Character */ VOID begtxt_(char *, ftnlen);
    static integer sclkps[73];
    static char spkkwb[32*73];
    extern /* Subroutine */ int dvpool_(char *, ftnlen);
    static char sclkws[32*73], keywrd[64];
    extern /* Subroutine */ int writln_(char *, integer *, ftnlen);
    static char spkkws[32*73];
    extern /* Subroutine */ int txtopn_(char *, integer *, ftnlen);

/* $ Abstract */

/*     This routine tests the routine CKMETA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     W.L. Taber      (JPL) */

/* $ Version */

/* -    Version 2.0.0  09-SEP-2013 (BVS) */

/*        Added more tests. */

/* -    Version 1.0.0  09-SEP-2013 (WLT) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters. */

/*     NCK is the size of the buffer in CKMETA */


/*     NIDS is the number of IDs we will test. */


/*     Local Variables */


/*     Save everything. */


/*     Begin every test family with an open call. */

    topen_("F_CKMETA", (ftnlen)8);

/*     Populate test arrays and make test text kernel. */

    kilfil_("myconns.ker", (ftnlen)11);
    txtopn_("myconns.ker", &sunit, (ftnlen)11);
    begdat_(ch__1, (ftnlen)16);
    writln_(ch__1, &sunit, (ftnlen)16);
    for (i__ = 1; i__ <= 73; ++i__) {

/*        CK IDs greater than -1000 and CKs IDs equal to or less than */
/*        -1000. */

	cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("cksm", i__1, 
		"f_ckmeta__", (ftnlen)189)] = i__ - 1000;
	ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("ckbg", i__1, 
		"f_ckmeta__", (ftnlen)190)] = i__ * -1000 - i__ % 1000;

/*        Default SCLK and SPK IDs for CK IDs greater than -1000. */

	sclksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("sclksm", 
		i__1, "f_ckmeta__", (ftnlen)195)] = 0;
	spksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("spksm", 
		i__1, "f_ckmeta__", (ftnlen)196)] = 0;

/*        Default SCLK and SPK IDs for CKs IDs equal to or less than */
/*        -1000. */

	sclkbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("sclkbg", 
		i__1, "f_ckmeta__", (ftnlen)202)] = -i__;
	spkbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("spkbg", 
		i__1, "f_ckmeta__", (ftnlen)203)] = -i__;

/*        POOL IDs for SCLK and SPK IDs for CK IDs greater than -1000. */

	sclkps[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("sclkps", 
		i__1, "f_ckmeta__", (ftnlen)208)] = ckbg[(i__2 = i__ - 1) < 
		73 && 0 <= i__2 ? i__2 : s_rnge("ckbg", i__2, "f_ckmeta__", (
		ftnlen)208)];
	spkps[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("spkps", 
		i__1, "f_ckmeta__", (ftnlen)209)] = ckbg[(i__2 = i__ - 1) < 
		73 && 0 <= i__2 ? i__2 : s_rnge("ckbg", i__2, "f_ckmeta__", (
		ftnlen)209)] - 2;

/*        POOL IDs SCLK and SPK IDs for CKs IDs equal to or less than */
/*        -1000. */

	sclkpb[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("sclkpb", 
		i__1, "f_ckmeta__", (ftnlen)215)] = ckbg[(i__2 = i__ - 1) < 
		73 && 0 <= i__2 ? i__2 : s_rnge("ckbg", i__2, "f_ckmeta__", (
		ftnlen)215)] - 1;
	spkpb[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge("spkpb", 
		i__1, "f_ckmeta__", (ftnlen)216)] = ckbg[(i__2 = i__ - 1) < 
		73 && 0 <= i__2 ? i__2 : s_rnge("ckbg", i__2, "f_ckmeta__", (
		ftnlen)216)] - 3;

/*        POOL variables and agents. */

	s_copy(keywrd, "CK_#_SCLK = #", (ftnlen)64, (ftnlen)13);
	repmi_(keywrd, "#", &cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : 
		s_rnge("cksm", i__1, "f_ckmeta__", (ftnlen)222)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	repmi_(keywrd, "#", &sclkps[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 
		: s_rnge("sclkps", i__1, "f_ckmeta__", (ftnlen)223)], keywrd, 
		(ftnlen)64, (ftnlen)1, (ftnlen)64);
	writln_(keywrd, &sunit, (ftnlen)64);
	nthwd_(keywrd, &c__1, sclkws + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? 
		i__1 : s_rnge("sclkws", i__1, "f_ckmeta__", (ftnlen)225)) << 
		5), &j, (ftnlen)64, (ftnlen)32);
	s_copy(keywrd, "CK_#_SPK = #", (ftnlen)64, (ftnlen)12);
	repmi_(keywrd, "#", &cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : 
		s_rnge("cksm", i__1, "f_ckmeta__", (ftnlen)228)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	repmi_(keywrd, "#", &spkps[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 :
		 s_rnge("spkps", i__1, "f_ckmeta__", (ftnlen)229)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	writln_(keywrd, &sunit, (ftnlen)64);
	nthwd_(keywrd, &c__1, spkkws + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? 
		i__1 : s_rnge("spkkws", i__1, "f_ckmeta__", (ftnlen)231)) << 
		5), &j, (ftnlen)64, (ftnlen)32);
	s_copy(keywrd, "CK_#_SCLK = #", (ftnlen)64, (ftnlen)13);
	repmi_(keywrd, "#", &ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : 
		s_rnge("ckbg", i__1, "f_ckmeta__", (ftnlen)234)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	repmi_(keywrd, "#", &sclkpb[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 
		: s_rnge("sclkpb", i__1, "f_ckmeta__", (ftnlen)235)], keywrd, 
		(ftnlen)64, (ftnlen)1, (ftnlen)64);
	writln_(keywrd, &sunit, (ftnlen)64);
	nthwd_(keywrd, &c__1, sclkwb + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? 
		i__1 : s_rnge("sclkwb", i__1, "f_ckmeta__", (ftnlen)237)) << 
		5), &j, (ftnlen)64, (ftnlen)32);
	s_copy(keywrd, "CK_#_SPK = #", (ftnlen)64, (ftnlen)12);
	repmi_(keywrd, "#", &ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : 
		s_rnge("ckbg", i__1, "f_ckmeta__", (ftnlen)240)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	repmi_(keywrd, "#", &spkpb[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 :
		 s_rnge("spkpb", i__1, "f_ckmeta__", (ftnlen)241)], keywrd, (
		ftnlen)64, (ftnlen)1, (ftnlen)64);
	writln_(keywrd, &sunit, (ftnlen)64);
	nthwd_(keywrd, &c__1, spkkwb + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? 
		i__1 : s_rnge("spkkwb", i__1, "f_ckmeta__", (ftnlen)243)) << 
		5), &j, (ftnlen)64, (ftnlen)32);
    }
    begtxt_(ch__1, (ftnlen)16);
    writln_(ch__1, &sunit, (ftnlen)16);
    cl__1.cerr = 0;
    cl__1.cunit = sunit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    clpool_();

/*     Test exception for unrecognized meta item. */

    tcase_("Unrecognized CKMETA item", (ftnlen)24);
    idcode = 1;
    ckmeta_(&c_b74, "IK", &idcode, (ftnlen)2);
    chckxc_(&c_true, "SPICE(UNKNOWNCKMETA)", ok, (ftnlen)20);
    chcksi_("IDCODE", &idcode, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Test default IDs. */

    tcase_("Default ID codes both SCLK and SPK.", (ftnlen)35);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)271)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclksm", i__1, "f_ckmeta__", (ftnlen)
		273)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)275)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spksm", i__1, "f_ckmeta__", (ftnlen)277)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)279)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkbg", i__1, "f_ckmeta__", (ftnlen)
		281)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)283)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkbg", i__1, "f_ckmeta__", (ftnlen)285)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Test POOL IDs. */

    ldpool_("myconns.ker", (ftnlen)11);
    tcase_("POOL ID codes for both SCLK and SPK. ", (ftnlen)37);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)298)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkps", i__1, "f_ckmeta__", (ftnlen)
		300)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)302)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkps", i__1, "f_ckmeta__", (ftnlen)304)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)306)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkpb", i__1, "f_ckmeta__", (ftnlen)
		308)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)310)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkpb", i__1, "f_ckmeta__", (ftnlen)312)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Test default SCLK IDs and POOL SPK IDs */

    for (i__ = 1; i__ <= 73; ++i__) {
	dvpool_(sclkws + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"sclkws", i__1, "f_ckmeta__", (ftnlen)320)) << 5), (ftnlen)32)
		;
	dvpool_(sclkwb + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"sclkwb", i__1, "f_ckmeta__", (ftnlen)321)) << 5), (ftnlen)32)
		;
    }
    tcase_("Default ID codes for SCLK, POOL ID codes for SPK.", (ftnlen)49);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)328)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclksm", i__1, "f_ckmeta__", (ftnlen)
		330)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)332)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkps", i__1, "f_ckmeta__", (ftnlen)334)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)336)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkbg", i__1, "f_ckmeta__", (ftnlen)
		338)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)340)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkpb", i__1, "f_ckmeta__", (ftnlen)342)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Test POOL IDs again to make sure that all watchers are set. */

    clpool_();
    ldpool_("myconns.ker", (ftnlen)11);
    tcase_("POOL ID codes for both SCLK and SPK, pass 2. ", (ftnlen)45);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)357)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkps", i__1, "f_ckmeta__", (ftnlen)
		359)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)361)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkps", i__1, "f_ckmeta__", (ftnlen)363)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)365)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkpb", i__1, "f_ckmeta__", (ftnlen)
		367)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)369)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkpb", i__1, "f_ckmeta__", (ftnlen)371)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Test default SPK IDs and POOL SCLK IDs */

    for (i__ = 1; i__ <= 73; ++i__) {
	dvpool_(spkkws + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"spkkws", i__1, "f_ckmeta__", (ftnlen)379)) << 5), (ftnlen)32)
		;
	dvpool_(spkkwb + (((i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"spkkwb", i__1, "f_ckmeta__", (ftnlen)380)) << 5), (ftnlen)32)
		;
    }
    tcase_("Default ID codes for SPK, POOL ID codes for SCLK.", (ftnlen)49);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)387)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkps[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkps", i__1, "f_ckmeta__", (ftnlen)
		389)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)391)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spksm", i__1, "f_ckmeta__", (ftnlen)393)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)395)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkpb[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkpb", i__1, "f_ckmeta__", (ftnlen)
		397)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)399)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkbg", i__1, "f_ckmeta__", (ftnlen)401)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Clear POOL and test default IDs again. */

    clpool_();
    tcase_("Default ID codes both SCLK and SPK, pass 2", (ftnlen)42);
    for (i__ = 1; i__ <= 73; ++i__) {
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)414)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclksm", i__1, "f_ckmeta__", (ftnlen)
		416)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&cksm[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"cksm", i__1, "f_ckmeta__", (ftnlen)418)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spksm[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spksm", i__1, "f_ckmeta__", (ftnlen)420)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)422)], "SCLK", &sclkid, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SCLKID", &sclkid, "=", &sclkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("sclkbg", i__1, "f_ckmeta__", (ftnlen)
		424)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	ckmeta_(&ckbg[(i__1 = i__ - 1) < 73 && 0 <= i__1 ? i__1 : s_rnge(
		"ckbg", i__1, "f_ckmeta__", (ftnlen)426)], "SPK", &spkid, (
		ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("SPKID", &spkid, "=", &spkbg[(i__1 = i__ - 1) < 73 && 0 <= 
		i__1 ? i__1 : s_rnge("spkbg", i__1, "f_ckmeta__", (ftnlen)428)
		], &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Clean up. */

    kilfil_("myconns.ker", (ftnlen)11);
    t_success__(ok);
    return 0;
} /* f_ckmeta__ */

