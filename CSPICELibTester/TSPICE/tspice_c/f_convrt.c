/* f_convrt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b173 = 1.;
static logical c_true = TRUE_;

/* $Procedure F_CONVRT ( CONVRT tests ) */
/* Subroutine */ int f_convrt__(logical *ok)
{
    /* Initialized data */

    static char anames[32*7] = "radians                         " "degrees  "
	    "                       " "arcminutes                      " "arc"
	    "seconds                      " "hourangle                       " 
	    "minuteangle                     " "secondangle                 "
	    "    ";
    static char dnames[32*17] = "meters                          " "km      "
	    "                        " "cm                              " 
	    "mm                              " "lightsecs                   "
	    "    " "au                              " "m                     "
	    "          " "kilometers                      " "centimeters     "
	    "                " "millimeters                     " "feet      "
	    "                      " "inches                          " "stat"
	    "ute_miles                   " "nautical_miles                  " 
	    "yards                           " "lightyears                  "
	    "    " "parsecs                         ";
    static char tnames[32*7] = "seconds                         " "minutes  "
	    "                       " "hours                           " "day"
	    "s                            " "julian_years                    " 
	    "tropical_years                  " "years                       "
	    "    ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    double sin(doublereal);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char name__[32];
    static doublereal xval;
    static integer i__;
    static char label[50];
    static doublereal scale;
    extern /* Subroutine */ int tcase_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void), tyear_(void);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nunit;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int t_success__(logical *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    extern doublereal clight_(void);
    static doublereal angval[7], dstval[17], timval[7], outval;
    extern /* Subroutine */ int convrt_(doublereal *, char *, char *, 
	    doublereal *, ftnlen, ftnlen);
    extern doublereal rpd_(void), spd_(void);
    static doublereal tol;

/* $ Abstract */

/*     Exercise the SPICELIB unit conversion routine CONVRT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine CONVRT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 13-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     AU value from convrt.for, converted to km: */


/*     Other constants from convrt.for: */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_CONVRT", (ftnlen)8);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup", (ftnlen)5);

/*     Since we can call functions at run time, it's more convenient */
/*     to initialize some constants here than in a DATA statement. */

/*     Start with angular units. */

/*     We'll use RADIANs as our base value. Note that CONVRT */
/*     uses DEGREES. */

    angval[0] = 1.;
    angval[1] = rpd_();
    angval[2] = angval[1] / 60.;
    angval[3] = angval[2] / 60.;
    angval[4] = angval[1] * 15.;
    angval[5] = angval[4] / 60.;
    angval[6] = angval[5] / 60.;

/*     Initialize distance units. We'll use KM as our base unit. */
/*     Note that CONVRT uses meters. */

    dstval[0] = .001;
    dstval[1] = 1.;
    dstval[2] = 1e-5;
    dstval[3] = 1e-6;
    dstval[4] = clight_();
    dstval[5] = 149597870.61368889;
    dstval[6] = .001;
    dstval[7] = 1.;
    dstval[8] = 1e-5;
    dstval[9] = 1e-6;
    dstval[10] = 3.0480000000000004e-4;
    dstval[11] = 2.5400000000000001e-5;
    dstval[12] = 1.6093440000000001;
    dstval[13] = 1.8520000000000001;
    dstval[14] = 9.144e-4;
    dstval[15] = clight_() * jyear_();
    dstval[16] = 149597870.61368889 / sin(rpd_() / 3600);

/*     Initialize time units. We'll use seconds as our base unit. */

    timval[0] = 1.;
    timval[1] = 60.;
    timval[2] = 3600.;
    timval[3] = spd_();
    timval[4] = jyear_();
    timval[5] = tyear_();
    timval[6] = jyear_();

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each time unit to seconds; use a non-identity input valu"
	    "e.", (ftnlen)66);
    nunit = 7;
    scale = 7.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("tnames", i__2, "f_convrt__", (ftnlen)310)) << 5), 
		"SECONDS", &outval, (ftnlen)32, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Time units <#> in seconds.", (ftnlen)50, (ftnlen)26);
	repmc_(label, "#", tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("tnames", i__2, "f_convrt__", (ftnlen)314)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = timval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge("tim"
		"val", i__2, "f_convrt__", (ftnlen)316)] * scale;
	if (eqstr_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("tnames", i__2, "f_convrt__", (ftnlen)318)) << 5), 
		"TROPICAL_YEARS", (ftnlen)32, (ftnlen)14)) {
	    tol = 1e-10;
	} else {
	    tol = 1e-14;
	}
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each time unit to seconds; use a non-identity input valu"
	    "e. Change case of input string.", (ftnlen)95);
    nunit = 7;
    scale = 7.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge(
		"tnames", i__2, "f_convrt__", (ftnlen)343)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, name__, "SECONDS", &outval, (ftnlen)32, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Time units <#> in seconds.", (ftnlen)50, (ftnlen)26);
	repmc_(label, "#", tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("tnames", i__2, "f_convrt__", (ftnlen)349)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = timval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge("tim"
		"val", i__2, "f_convrt__", (ftnlen)351)] * scale;
	if (eqstr_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("tnames", i__2, "f_convrt__", (ftnlen)353)) << 5), 
		"TROPICAL_YEARS", (ftnlen)32, (ftnlen)14)) {
	    tol = 1e-10;
	} else {
	    tol = 1e-14;
	}
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert seconds to each time unit; use a non-identity input valu"
	    "e.", (ftnlen)66);
    nunit = 7;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, "SECONDS", tnames + (((i__2 = i__ - 1) < 7 && 0 <= 
		i__2 ? i__2 : s_rnge("tnames", i__2, "f_convrt__", (ftnlen)
		378)) << 5), &outval, (ftnlen)7, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Seconds in time units <#>.", (ftnlen)50, (ftnlen)26);
	repmc_(label, "#", tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("tnames", i__2, "f_convrt__", (ftnlen)382)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / timval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("timval", i__2, "f_convrt__", (ftnlen)384)];
	if (eqstr_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("tnames", i__2, "f_convrt__", (ftnlen)386)) << 5), 
		"TROPICAL_YEARS", (ftnlen)32, (ftnlen)14)) {
	    tol = 1e-10;
	} else {
	    tol = 1e-14;
	}
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert seconds to each time unit; use a non-identity input valu"
	    "e. Change case of output string.", (ftnlen)96);
    nunit = 7;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge(
		"tnames", i__2, "f_convrt__", (ftnlen)411)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, "SECONDS", name__, &outval, (ftnlen)7, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Seconds in time units <#>.", (ftnlen)50, (ftnlen)26);
	repmc_(label, "#", tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("tnames", i__2, "f_convrt__", (ftnlen)417)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / timval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("timval", i__2, "f_convrt__", (ftnlen)419)];
	if (eqstr_(tnames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("tnames", i__2, "f_convrt__", (ftnlen)421)) << 5), 
		"TROPICAL_YEARS", (ftnlen)32, (ftnlen)14)) {
	    tol = 1e-10;
	} else {
	    tol = 1e-14;
	}
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each distance unit to km; use a non-identity input value."
	    , (ftnlen)65);
    nunit = 17;
    scale = 7.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 :
		 s_rnge("dnames", i__2, "f_convrt__", (ftnlen)446)) << 5), 
		"KM", &outval, (ftnlen)32, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Distance units <#> in km.", (ftnlen)50, (ftnlen)25);
	repmc_(label, "#", dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? 
		i__2 : s_rnge("dnames", i__2, "f_convrt__", (ftnlen)450)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = dstval[(i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : s_rnge(
		"dstval", i__2, "f_convrt__", (ftnlen)452)] * scale;
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each distance unit to km; use a non-identity input value"
	    ". Change case of input string.", (ftnlen)94);
    nunit = 17;
    scale = 7.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : s_rnge(
		"dnames", i__2, "f_convrt__", (ftnlen)475)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, name__, "KM", &outval, (ftnlen)32, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Distance units <#> in km.", (ftnlen)50, (ftnlen)25);
	repmc_(label, "#", dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? 
		i__2 : s_rnge("dnames", i__2, "f_convrt__", (ftnlen)481)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = dstval[(i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : s_rnge(
		"dstval", i__2, "f_convrt__", (ftnlen)483)] * scale;
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert km to each distance unit; use a non-identity input value."
	    , (ftnlen)65);
    nunit = 17;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, "KM", dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? 
		i__2 : s_rnge("dnames", i__2, "f_convrt__", (ftnlen)505)) << 
		5), &outval, (ftnlen)2, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "KM in distance units <#>.", (ftnlen)50, (ftnlen)25);
	repmc_(label, "#", dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? 
		i__2 : s_rnge("dnames", i__2, "f_convrt__", (ftnlen)509)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / dstval[(i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : 
		s_rnge("dstval", i__2, "f_convrt__", (ftnlen)511)];
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert km to each distance unit; use a non-identity input value"
	    ". Change case of input string.", (ftnlen)94);
    nunit = 17;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : s_rnge(
		"dnames", i__2, "f_convrt__", (ftnlen)535)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, "KM", name__, &outval, (ftnlen)2, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "KM in distance units <#>.", (ftnlen)50, (ftnlen)25);
	repmc_(label, "#", dnames + (((i__2 = i__ - 1) < 17 && 0 <= i__2 ? 
		i__2 : s_rnge("dnames", i__2, "f_convrt__", (ftnlen)541)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / dstval[(i__2 = i__ - 1) < 17 && 0 <= i__2 ? i__2 : 
		s_rnge("dstval", i__2, "f_convrt__", (ftnlen)543)];
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each angular unit to radians; use a non-identity input v"
	    "alue.", (ftnlen)69);
    nunit = 7;
    scale = 11.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("anames", i__2, "f_convrt__", (ftnlen)566)) << 5), 
		"RADIANS", &outval, (ftnlen)32, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Angular units <#> in radians.", (ftnlen)50, (ftnlen)29)
		;
	repmc_(label, "#", anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("anames", i__2, "f_convrt__", (ftnlen)570)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = angval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge("ang"
		"val", i__2, "f_convrt__", (ftnlen)572)] * scale;
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert each angular unit to radians; use a non-identity input v"
	    "alue. Change case of input string.", (ftnlen)98);
    nunit = 7;
    scale = 7.;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge(
		"anames", i__2, "f_convrt__", (ftnlen)596)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, name__, "RADIANS", &outval, (ftnlen)32, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "Angular units <#> in radians.", (ftnlen)50, (ftnlen)29)
		;
	repmc_(label, "#", anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("anames", i__2, "f_convrt__", (ftnlen)602)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = angval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge("ang"
		"val", i__2, "f_convrt__", (ftnlen)604)] * scale;
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert radians to each angular unit; use a non-identity input v"
	    "alue.", (ftnlen)69);
    nunit = 7;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	convrt_(&scale, "RADIANS", anames + (((i__2 = i__ - 1) < 7 && 0 <= 
		i__2 ? i__2 : s_rnge("anames", i__2, "f_convrt__", (ftnlen)
		626)) << 5), &outval, (ftnlen)7, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "RADIANS in angular units <#>.", (ftnlen)50, (ftnlen)29)
		;
	repmc_(label, "#", anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("anames", i__2, "f_convrt__", (ftnlen)630)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / angval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("angval", i__2, "f_convrt__", (ftnlen)632)];
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert radians to each angular unit; use a non-identity input v"
	    "alue. Change case of input string.", (ftnlen)98);
    nunit = 7;
    scale = .33333333333333331;
    i__1 = nunit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ucase_(anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : s_rnge(
		"anames", i__2, "f_convrt__", (ftnlen)656)) << 5), name__, (
		ftnlen)32, (ftnlen)32);
	convrt_(&scale, "RADIANS", name__, &outval, (ftnlen)7, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "RADIANS in angular units <#>.", (ftnlen)50, (ftnlen)29)
		;
	repmc_(label, "#", anames + (((i__2 = i__ - 1) < 7 && 0 <= i__2 ? 
		i__2 : s_rnge("anames", i__2, "f_convrt__", (ftnlen)662)) << 
		5), label, (ftnlen)50, (ftnlen)1, (ftnlen)32, (ftnlen)50);
	xval = scale / angval[(i__2 = i__ - 1) < 7 && 0 <= i__2 ? i__2 : 
		s_rnge("angval", i__2, "f_convrt__", (ftnlen)664)];
	tol = 1e-14;
	chcksd_(label, &outval, "~/", &xval, &tol, ok, (ftnlen)50, (ftnlen)2);
    }
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Incompatible units.", (ftnlen)19);
    convrt_(&c_b173, "RADIANS", "KM", &outval, (ftnlen)7, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);
    convrt_(&c_b173, "RADIANS", "SECONDS", &outval, (ftnlen)7, (ftnlen)7);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);
    convrt_(&c_b173, "SECONDS", "M", &outval, (ftnlen)7, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);
    convrt_(&c_b173, "SECONDS", "RADIANS", &outval, (ftnlen)7, (ftnlen)7);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);
    convrt_(&c_b173, "DEGREES", "M", &outval, (ftnlen)7, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);
    convrt_(&c_b173, "DEGREES", "HOURS", &outval, (ftnlen)7, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INCOMPATIBLEUNITS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unrecognized units.", (ftnlen)19);
    convrt_(&c_b173, "SECONDS", "Z", &outval, (ftnlen)7, (ftnlen)1);
    chckxc_(&c_true, "SPICE(UNITSNOTREC)", ok, (ftnlen)18);
    convrt_(&c_b173, "X", "RADIANS", &outval, (ftnlen)1, (ftnlen)7);
    chckxc_(&c_true, "SPICE(UNITSNOTREC)", ok, (ftnlen)18);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_convrt__ */

