/* f_crdcnv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__14 = 14;
static integer c__2 = 2;
static doublereal c_b25 = 1e-11;
static doublereal c_b177 = 6378.137;
static doublereal c_b178 = .0033528131778969143;
static doublereal c_b186 = 0.;
static doublereal c_b196 = 1e-9;
static integer c__3 = 3;
static doublereal c_b234 = 1.;
static doublereal c_b238 = .5;
static doublereal c_b244 = -1.;
static doublereal c_b259 = 1.1;
static doublereal c_b291 = 1e-15;
static doublereal c_b301 = 2.;
static integer c__399 = 399;

/* $Procedure F_CRDCNV (Family of tests on coordinate conversions) */
/* Subroutine */ int f_crdcnv__(logical *ok)
{
    /* Initialized data */

    static doublereal grid[5] = { -1.,0.,.01,1.,1e4 };
    static integer body[6] = { 299,399,499,599,699,799 };

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    doublereal d__1, d__2, d__3;
    char ch__1[88];

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal), atan2(doublereal, doublereal), d_mod(doublereal *
	    , doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char case__[80];
    static doublereal clat, gvec[3], elat, glat, galt;
    static integer ndim;
    static doublereal llat, erho, elon, long__, glon, llon, slat, slon;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal r__, eclat, radii[3], z__, range, nadir[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen), repmi_(
	    char *, char *, integer *, char *, ftnlen, ftnlen, ftnlen), 
	    topen_(char *, ftnlen);
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *);
    static integer ib;
    static doublereal ra, er;
    extern doublereal pi_(void);
    static doublereal ex, ey, ez;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static integer ix, iy;
    extern doublereal halfpi_(void);
    static integer iz;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     recrad_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    radrec_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    t_pck08__(char *, logical *, logical *, ftnlen), recgeo_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), reclat_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), georec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), bodvcd_(
	    integer *, char *, integer *, integer *, doublereal *, ftnlen), 
	    reccyl_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    cylrec_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    recsph_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    sphrec_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    srfrec_(integer *, doublereal *, doublereal *, doublereal *);
    static doublereal radius;
    extern /* Subroutine */ int latcyl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), cyllat_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), latsph_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), sphlat_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal srfeqn;
    extern /* Subroutine */ int cylsph_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), sphcyl_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal dec, era, rec[3], rho, lon;

/* $ Abstract */

/*     This routine performs rudimentary tests on coordinate conversion */
/*     routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     M.C. Kim      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 27-JUL-2016 (MCK)(BVS) */

/*        Initial code to test */

/*           CYLLAT */
/*           CYLREC */
/*           CYLSPH */
/*           GEOREC */
/*           LATCYL */
/*           LATREC */
/*           LATSPH */
/*           RADREC */
/*           RECCYL */
/*           RECGEO */
/*           RECLAT */
/*           RECRAD */
/*           RECSPH */
/*           SRFREC */
/*           SPHCYL */
/*           SPHLAT */
/*           SPHREC */

/* -& */
/* $ Index_Entries */

/*     test coordinate conversion routines */

/* -& */

/*     SPICELIB Functions */


/*     grid dimension of a cube ( NGRID x NGRID x NGRID ) */


/*     number of NAIF bodies used for test */


/*     numerical tolerances */


/*     equatorial radius of a reference ellipsoid */


/*     flattening of the reference ellipsoid */


/*     polar radius of the reference ellipsoid */


/*     Test PCK. */


/*     Local variables */


/*     grid locations */


/*     NAIF body ID numbers */


/*     grid index on x-axis */


/*     grid index on y-axis */


/*     grid index on z-axis */


/*     Listed below are coordinate values computed in this routine at */
/*     each grid point. All angles are in radians. */


/*     x-component of the right-handed Cartesian frame */


/*     y-component of the right-handed Cartesian frame */


/*     z-component of the right-handed Cartesian frame */


/*     distance from origin */


/*     distance from z-axis */


/*     latitude in [ -HALFPI(), +HALFPI() ] */


/*     co-latitude in [ 0.0D0, +PI() ] */


/*     east longitude in ( -PI(), +PI() ] */


/*     right ascension in [ 0.0D0, TWOPI() ) */


/*     Listed below are coordinate values returned from SPICELIB */
/*     routines. */


/*     rectangular coordinates */


/*     cylindrical coordinates */


/*     spherical coordinates */


/*     latitudinal coordinates */


/*     RAD ( range, right ascension, declination ) coordinates */


/*     geodetic coordinates */


/*     surface coordinates */


/*     naif body ID number */


/*     geodetic altitude vector */


/*     evaluation of spheroidal surface equation */


/*     origin to nadir vector */


/*     radii of tri-axial ellipsoid */


/*     number of radii */


/*     test case description */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_CRDCNV", (ftnlen)8);

/*     Create the PCK file, load it, and delete it. We need it to get */
/*     values of radii of naif bodies. */

    t_pck08__("test_0008.tpc", &c_true, &c_false, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test loop over grid points of the NGRID x NGRID x NGRID cube. */

    for (ix = 1; ix <= 5; ++ix) {
	for (iy = 1; iy <= 5; ++iy) {
	    for (iz = 1; iz <= 5; ++iz) {

/*              rectangular coordinates of a grid point */

		ex = grid[(i__1 = ix - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
			"grid", i__1, "f_crdcnv__", (ftnlen)388)];
		ey = grid[(i__1 = iy - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
			"grid", i__1, "f_crdcnv__", (ftnlen)389)];
		ez = grid[(i__1 = iz - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
			"grid", i__1, "f_crdcnv__", (ftnlen)390)];

/*              Exclude the origin from test. */

		if (ex == 0. && ex == 0. && ex == 0.) {
		    ex = 1.;
		    ey = 1.;
		    ez = 1.;
		}

/*              radius from origin */

/* Computing 2nd power */
		d__1 = ex;
/* Computing 2nd power */
		d__2 = ey;
/* Computing 2nd power */
		d__3 = ez;
		er = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);

/*              radius from z-axis */

/* Computing 2nd power */
		d__1 = ex;
/* Computing 2nd power */
		d__2 = ey;
		erho = sqrt(d__1 * d__1 + d__2 * d__2);

/*              right ascension in [ 0.0D0, TWOPI() ) */

		d__1 = atan2(ey, ex);
		d__2 = twopi_();
		era = d_mod(&d__1, &d__2);
		if (era < 0.) {
		    era += twopi_();
		}

/*              east longitude in ( -PI(), PI() ] */

		elon = atan2(ey, ex);
		if (elon == -pi_()) {
		    elon = pi_();
		}

/*              co-latitude in [ 0.0D0, PI() ] */

		eclat = atan2(erho, ez);

/*              latitude in [ -HALFPI(), HALFPI() ] */

		elat = atan2(ez, erho);

/*              Identity the grid point on the test description. */

		s_copy(case__, "# # #", (ftnlen)80, (ftnlen)5);
		repmd_(case__, "#", &ex, &c__14, case__, (ftnlen)80, (ftnlen)
			1, (ftnlen)80);
		repmd_(case__, "#", &ey, &c__14, case__, (ftnlen)80, (ftnlen)
			1, (ftnlen)80);
		repmd_(case__, "#", &ez, &c__14, case__, (ftnlen)80, (ftnlen)
			1, (ftnlen)80);

/*              Test rectangular to cylindrical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RECCYL: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Pack the grid point into a rectangular coordinate */
/*              vector. */

		vpack_(&ex, &ey, &ez, rec);

/*              Convert to cylindrical coordinates. */

		reccyl_(rec, &rho, &lon, &z__);

/*              The cylindrical longitude, LON, returned from subroutine */
/*              RECCYL is in the range of [ 0.0D0, TWOPI() ). This */
/*              conflicts with the range of longitude, ( -PI(), +PI() ], */
/*              defined in some other subroutines. That is, */

		lon = atan2(rec[1], rec[0]);

/*              The above line, inserted to suppress error messages, */
/*              should be removed after fixing the inconsistency in the */
/*              definition of longitude. */


/*              Check the converted coordinates. */

		chcksd_("RHO", &rho, "~", &erho, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("LON", &lon, "~", &elon, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("Z", &z__, "~", &ez, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;

/*              Test cylindrical to rectangular coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "CYLREC: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert cylindrical coordinates to rectangular */
/*              coordinates. */

		cylrec_(&rho, &lon, &z__, rec);

/*              Check the reverted coordinates. */

		chcksd_("REC(1)", rec, "~", &ex, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(2)", &rec[1], "~", &ey, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(3)", &rec[2], "~", &ez, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);

/*              Test rectangular to spherical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RECSPH: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Pack the grid point into a rectangular coordinate */
/*              vector. */

		vpack_(&ex, &ey, &ez, rec);

/*              Convert to spherical coordinates. */

		recsph_(rec, &r__, &clat, &long__);

/*              Check the converted coordinates. */

		chcksd_("R", &r__, "~", &er, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;
		chcksd_("CLAT", &clat, "~", &eclat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LONG", &long__, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test spherical to rectangular coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "SPHREC: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert spherical coordinates to rectangular coordinates. */

		sphrec_(&r__, &clat, &long__, rec);

/*              Check the reverted coordinates. */

		chcksd_("REC(1)", rec, "~", &ex, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(2)", &rec[1], "~", &ey, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(3)", &rec[2], "~", &ez, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);

/*              Test rectangular to latitudinal coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RECLAT: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Pack the grid point into a rectangular coordinate */
/*              vector. */

		vpack_(&ex, &ey, &ez, rec);

/*              Convert to latitudinal coordinates. */

		reclat_(rec, &radius, &llon, &llat);

/*              Check the converted coordinates. */

		chcksd_("RADIUS", &radius, "~", &er, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("LLON", &llon, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LLAT", &llat, "~", &elat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test latitudinal to rectangular coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "LATREC: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert latitudinal coordinates to rectangular */
/*              coordinates. */

		latrec_(&radius, &llon, &llat, rec);

/*              Check the reverted coordinates. */

		chcksd_("REC(1)", rec, "~", &ex, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(2)", &rec[1], "~", &ey, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(3)", &rec[2], "~", &ez, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);

/*              Test cylindrical to spherical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "CYLSPH: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Convert cylindrical coordinates to latitudinal */
/*              coordinates. */

		cylsph_(&rho, &lon, &z__, &r__, &clat, &long__);

/*              Check the converted coordinates. */

		chcksd_("R", &r__, "~", &er, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;
		chcksd_("CLAT", &clat, "~", &eclat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LONG", &long__, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test spherical to cylindrical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "SPHCYL: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert latitudinal coordinates to cylindrical */
/*              coordinates. */

		sphcyl_(&r__, &clat, &long__, &rho, &lon, &z__);

/*              Check the reverted coordinates. */

		chcksd_("RHO", &rho, "~", &erho, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("LON", &lon, "~", &elon, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("Z", &z__, "~", &ez, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;

/*              Test latitudinal to spherical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "LATSPH: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Convert latitudinal coordinates to spherical */
/*              coordinates. */

		latsph_(&radius, &llon, &llat, &r__, &clat, &long__);

/*              Check the converted coordinates. */

		chcksd_("R", &r__, "~", &er, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;
		chcksd_("CLAT", &clat, "~", &eclat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LONG", &long__, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test spherical to latitudinal coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "SPHLAT: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert spherical coordinates to latitudinal coordinates. */

		sphlat_(&r__, &clat, &long__, &radius, &llon, &llat);

/*              Check the reverted coordinates. */

		chcksd_("RADIUS", &radius, "~", &er, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("LLON", &llon, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LLAT", &llat, "~", &elat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test latitudinal to cylindrical coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "LATCYL: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Convert latitudinal coordinates to cylindrical */
/*              coordinates. */

		latcyl_(&radius, &llon, &llat, &rho, &lon, &z__);

/*              Check the converted coordinates. */

		chcksd_("RHO", &rho, "~", &erho, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("LON", &lon, "~", &elon, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);
		chcksd_("Z", &z__, "~", &ez, &c_b25, ok, (ftnlen)1, (ftnlen)1)
			;

/*              Test cylindrical to latitudinal coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "CYLLAT: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert cylindrical coordinates to latitudinal */
/*              coordinates. */

		cyllat_(&rho, &lon, &z__, &radius, &llon, &llat);

/*              Check the reverted coordinates. */

		chcksd_("RADIUS", &radius, "~", &er, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("LLON", &llon, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);
		chcksd_("LLAT", &llat, "~", &elat, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Test rectangular to RAD coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RECRAD: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Pack the grid point into a rectangular coordinate */
/*              vector. */

		vpack_(&ex, &ey, &ez, rec);

/*              Convert rectangular coordinates to RAD coordinates. */

		recrad_(rec, &range, &ra, &dec);

/*              Check the converted coordinates. */

		chcksd_("RANGE", &range, "~", &er, &c_b25, ok, (ftnlen)5, (
			ftnlen)1);
		chcksd_("RA", &ra, "~", &era, &c_b25, ok, (ftnlen)2, (ftnlen)
			1);
		chcksd_("DEC", &dec, "~", &elat, &c_b25, ok, (ftnlen)3, (
			ftnlen)1);

/*              Test RAD to rectangular coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RADREC: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert RAD coordinates to rectangular coordinates. */

		radrec_(&range, &ra, &dec, rec);

/*              Check the converted coordinates. */

		chcksd_("REC(1)", rec, "~", &ex, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(2)", &rec[1], "~", &ey, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(3)", &rec[2], "~", &ez, &c_b25, ok, (ftnlen)6, (
			ftnlen)1);

/*              Test rectangular to geodetic coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "RECGEO: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Pack the grid point into a rectangular coordinate */
/*              vector. */

		vpack_(&ex, &ey, &ez, rec);

/*              Convert rectangular coordinates to geodetic coordinates. */

		recgeo_(rec, &c_b177, &c_b178, &glon, &glat, &galt);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              First, check the geodetic longitude, which should be the */
/*              same as longitudes in other coordinate system. */

		chcksd_("GLON", &glon, "~", &elon, &c_b25, ok, (ftnlen)4, (
			ftnlen)1);

/*              Derive the geodetic altitude vector, GVEC, which is */
/*              vertically pointing from the nadir on the surface of the */
/*              reference ellipsoid to the grid point. */
		latrec_(&galt, &glon, &glat, gvec);

/*              Get rectangular coordinates of the nadir. */

		vsub_(rec, gvec, nadir);

/*              Being on the surface of the reference ellipsoid, the */
/*              rectangular coordinates of the nadir should satisfy the */
/*              spheroidal surface equation associated with the reference */
/*              ellipsoid. */

/*              Evaluate the spheroidal surface equation. */

/* Computing 2nd power */
		d__1 = nadir[0] / 6378.137;
/* Computing 2nd power */
		d__2 = nadir[1] / 6378.137;
/* Computing 2nd power */
		d__3 = nadir[2] / 6356.752298215968;
		srfeqn = d__1 * d__1 + d__2 * d__2 + d__3 * d__3 - 1.;

/*              Check the nadir is on the surface. */

		chcksd_("SRFEQN", &srfeqn, "~", &c_b186, &c_b25, ok, (ftnlen)
			6, (ftnlen)1);

/*              Test geodetic to rectangular coordinate conversion. */

/* Writing concatenation */
		i__2[0] = 8, a__1[0] = "GEOREC: ";
		i__2[1] = 80, a__1[1] = case__;
		s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		tcase_(ch__1, (ftnlen)88);

/*              Revert geodetic coordinates to rectangular coordinates. */

		georec_(&glon, &glat, &galt, &c_b177, &c_b178, rec);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the reverted coordinates. */

		chcksd_("REC(1)", rec, "~", &ex, &c_b196, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(2)", &rec[1], "~", &ey, &c_b196, ok, (ftnlen)6, (
			ftnlen)1);
		chcksd_("REC(3)", &rec[2], "~", &ez, &c_b196, ok, (ftnlen)6, (
			ftnlen)1);

/*              Test surface to rectangular coordinates conversion. */


/*              Set the surface coordinates ( longitude and latitude ) */
/*              computed at the grid point. */

		slon = elon;
		slat = elat;

/*              Loop over NAIF bodies */

		for (ib = 1; ib <= 6; ++ib) {

/*                 Identity NAIF body and surface point on the test */
/*                 description. */

		    s_copy(case__, "# # #", (ftnlen)80, (ftnlen)5);
		    repmi_(case__, "#", &body[(i__1 = ib - 1) < 6 && 0 <= 
			    i__1 ? i__1 : s_rnge("body", i__1, "f_crdcnv__", (
			    ftnlen)832)], case__, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    repmd_(case__, "#", &slon, &c__14, case__, (ftnlen)80, (
			    ftnlen)1, (ftnlen)80);
		    repmd_(case__, "#", &slat, &c__14, case__, (ftnlen)80, (
			    ftnlen)1, (ftnlen)80);
/* Writing concatenation */
		    i__2[0] = 8, a__1[0] = "SRFREC: ";
		    i__2[1] = 80, a__1[1] = case__;
		    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)88);
		    tcase_(ch__1, (ftnlen)88);

/*                 Convert surface coordinates to rectangular */
/*                 coordinates. */

		    srfrec_(&body[(i__1 = ib - 1) < 6 && 0 <= i__1 ? i__1 : 
			    s_rnge("body", i__1, "f_crdcnv__", (ftnlen)842)], 
			    &slon, &slat, nadir);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Convert rectangular coordinates to latitudinal */
/*                 coordinates. */

		    reclat_(nadir, &radius, &llon, &llat);

/*                 The longitude and latitude pair of latitudinal */
/*                 coordinates should be that of surface coordinates. */

		    chcksd_("LLON", &llon, "~", &elon, &c_b25, ok, (ftnlen)4, 
			    (ftnlen)1);
		    chcksd_("LLAT", &llat, "~", &elat, &c_b25, ok, (ftnlen)4, 
			    (ftnlen)1);

/*                 Get semi-axes, RADII, of the naif body. */

		    bodvcd_(&body[(i__1 = ib - 1) < 6 && 0 <= i__1 ? i__1 : 
			    s_rnge("body", i__1, "f_crdcnv__", (ftnlen)862)], 
			    "RADII", &c__3, &ndim, radii, (ftnlen)5);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Being on the surface of the naif body, the recovered */
/*                 rectangular coordinates should satisfy the spheroidal */
/*                 surface equation associated with the naif body. */

/*                 Evaluate the spheroidal surface equation. */

/* Computing 2nd power */
		    d__1 = nadir[0] / radii[0];
/* Computing 2nd power */
		    d__2 = nadir[1] / radii[1];
/* Computing 2nd power */
		    d__3 = nadir[2] / radii[2];
		    srfeqn = d__1 * d__1 + d__2 * d__2 + d__3 * d__3 - 1.;

/*                 Check the nadir is on the surface. */

		    chcksd_("SRFEQN", &srfeqn, "~", &c_b186, &c_b25, ok, (
			    ftnlen)6, (ftnlen)1);
		}
	    }
	}
    }

/*     Exception test cases. */

    tcase_("GEOREC exceptions", (ftnlen)17);
    georec_(&c_b234, &c_b234, &c_b234, &c_b186, &c_b238, rec);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    georec_(&c_b234, &c_b234, &c_b234, &c_b244, &c_b238, rec);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    georec_(&c_b234, &c_b234, &c_b234, &c_b234, &c_b234, rec);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    georec_(&c_b234, &c_b234, &c_b234, &c_b234, &c_b259, rec);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    tcase_("RECGEO exceptions", (ftnlen)17);
    vpack_(&c_b234, &c_b234, &c_b234, rec);
    recgeo_(rec, &c_b186, &c_b238, &glon, &glat, &galt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    recgeo_(rec, &c_b244, &c_b238, &glon, &glat, &galt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    recgeo_(rec, &c_b234, &c_b234, &glon, &glat, &galt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    recgeo_(rec, &c_b234, &c_b259, &glon, &glat, &galt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     (0,0,0) test cases. */

    vpack_(&c_b186, &c_b186, &c_b186, rec);
    tcase_("RECCYL (0,0,0) input", (ftnlen)20);
    reccyl_(rec, &rho, &lon, &z__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RHO", &rho, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("LON", &lon, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("Z", &z__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    tcase_("RECGEO (0,0,0) input", (ftnlen)20);
    recgeo_(rec, &c_b301, &c_b238, &glon, &glat, &galt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("GLON", &glon, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    d__1 = halfpi_();
    chcksd_("GLAT", &glat, "=", &d__1, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("GALT", &galt, "=", &c_b244, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("RECLAT (0,0,0) input", (ftnlen)20);
    reclat_(rec, &radius, &llon, &llat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RADIUS", &radius, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LLON", &llon, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LLAT", &llat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("RECRAD (0,0,0) input", (ftnlen)20);
    recrad_(rec, &range, &ra, &dec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RANGE", &range, "=", &c_b186, &c_b291, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("RA", &ra, "=", &c_b186, &c_b291, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("DEC", &dec, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    tcase_("RECSPH (0,0,0) input", (ftnlen)20);
    recsph_(rec, &r__, &clat, &long__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("R", &r__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    chcksd_("CLAT", &clat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LONG", &long__, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("CYLLAT (0,0,0) input", (ftnlen)20);
    cyllat_(&c_b186, &c_b186, &c_b186, &radius, &llon, &llat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RADIUS", &radius, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LLON", &llon, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LLAT", &llat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("CYLREC (0,0,0) input", (ftnlen)20);
    cylrec_(&c_b186, &c_b186, &c_b186, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    tcase_("CYLSPH (0,0,0) input", (ftnlen)20);
    cylsph_(&c_b186, &c_b186, &c_b186, &r__, &clat, &long__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("R", &r__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    chcksd_("CLAT", &clat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LONG", &long__, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("GEOREC (0,0,0) input", (ftnlen)20);
    georec_(&c_b186, &c_b186, &c_b186, &c_b301, &c_b238, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", &c_b301, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    tcase_("LATCYL (0,0,0) input", (ftnlen)20);
    latcyl_(&c_b186, &c_b186, &c_b186, &rho, &lon, &z__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RHO", &rho, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("LON", &lon, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("Z", &z__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    tcase_("LATREC (0,0,0) input", (ftnlen)20);
    latrec_(&c_b186, &c_b186, &c_b186, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    tcase_("LATSPH (0,0,0) input", (ftnlen)20);
    latsph_(&c_b186, &c_b186, &c_b186, &r__, &clat, &long__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("R", &r__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    d__1 = halfpi_();
    chcksd_("CLAT", &clat, "=", &d__1, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LONG", &long__, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("RADREC (0,0,0) input", (ftnlen)20);
    radrec_(&c_b186, &c_b186, &c_b186, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    tcase_("SPHCYL (0,0,0) input", (ftnlen)20);
    sphcyl_(&c_b186, &c_b186, &c_b186, &rho, &lon, &z__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RHO", &rho, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("LON", &lon, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("Z", &z__, "=", &c_b186, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    tcase_("SPHLAT (0,0,0) input", (ftnlen)20);
    sphlat_(&c_b186, &c_b186, &c_b186, &radius, &llon, &llat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RADIUS", &radius, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LLON", &llon, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    d__1 = halfpi_();
    chcksd_("LLAT", &llat, "=", &d__1, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("SPHREC (0,0,0) input", (ftnlen)20);
    sphrec_(&c_b186, &c_b186, &c_b186, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    tcase_("SRFREC (0,0) input", (ftnlen)18);
    bodvcd_(&c__399, "RADII", &c__3, &ndim, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfrec_(&c__399, &c_b186, &c_b186, rec);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("REC(1)", rec, "=", radii, &c_b291, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("REC(2)", &rec[1], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("REC(3)", &rec[2], "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Test cases, with one or more items set to 0. */

    tcase_("CYLLAT (0,HALFPI,0) input", (ftnlen)25);
    d__1 = halfpi_();
    cyllat_(&c_b186, &d__1, &c_b186, &radius, &llon, &llat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RADIUS", &radius, "=", &c_b186, &c_b291, ok, (ftnlen)6, (ftnlen)
	    1);
    d__1 = halfpi_();
    chcksd_("LLON", &llon, "=", &d__1, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LLAT", &llat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    tcase_("RECCYL (0,0,1) input", (ftnlen)20);
    vpack_(&c_b186, &c_b186, &c_b234, rec);
    reccyl_(rec, &rho, &lon, &z__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("RHO", &rho, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("LON", &lon, "=", &c_b186, &c_b291, ok, (ftnlen)3, (ftnlen)1);
    chcksd_("Z", &z__, "=", &c_b234, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    tcase_("RECSPH (0,0,1) input", (ftnlen)20);
    vpack_(&c_b186, &c_b186, &c_b234, rec);
    recsph_(rec, &r__, &clat, &long__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("R", &r__, "=", &c_b234, &c_b291, ok, (ftnlen)1, (ftnlen)1);
    chcksd_("CLAT", &clat, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("LONG", &long__, "=", &c_b186, &c_b291, ok, (ftnlen)4, (ftnlen)1);

/*     Test cases with angular inputs outside of normal ranges. */

/*     THESE TEST HAVE NOT BEED IMPLEMENTED FOR N0066. THEY MIGHT */
/*     BE IMPLEMENTED AT A LATER TIME. */

    t_success__(ok);
    return 0;
} /* f_crdcnv__ */

