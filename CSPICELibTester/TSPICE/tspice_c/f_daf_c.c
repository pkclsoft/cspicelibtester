/*

-Procedure f_daf_c ( Test wrappers for DAF routines )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the DAF routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_daf_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Files
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Particulars
 
   This routine tests the wrappers for the DAF routines. 
   
   These are:
      
      dafbbs_c
      dafbfs_c
      dafcls_c
      dafcs_c
      daffna_c
      daffpa_c
      dafgda_c
      dafgn_c
      dafgs_c
      dafopr_c
      dafus_c
             
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Literature_References
 
   None. 
 
-Version

   -tspice_c Version 2.0.0 17-NOV-2006 (NJB)
 
      Added test cases for 
 
         dafac_c
         dafdc_c
         dafec_c
         dafgda_c
          

   -tspice_c Version 1.1.0 21-NOV-2001 (NJB)

      SPICE(DAFNOSUCHHANDLE) short error message was updated to 
      SPICE(NOSUCHHANDLE), in order to match that signaled by
      the new handle manager subsystem.

   -tspice_c Version 1.0.0 03-SEP-1999 (NJB)  

-&
*/

{ /* Begin f_daf_c */

 
   /*
   Constants
   */
   #define ERRLEN          321
   #define GDABSP          "gda.bsp"
   #define SPK1            "daftest.bsp"
   #define SPK2            "cstest.bsp"
   #define SIDLEN          41
   #define DSCSIZ          5
   #define PHOENIX         -9
   #define ND              2
   #define NI              6
   #define NSTATES         100 
   
   /*
   This is the number of segments in the SPK file created by tstspk_c.
   If that routine changes, this "constant" might need to change as 
   well.
   */
   #define N_SPK_SEG       46
   #define CLINSZ          1001
   #define CBUFSZ          15
   #define NCHUNKS         10
   #define XBUFSZ        ( NCHUNKS * CBUFSZ )   
   #define NRESV         ( XBUFSZ  * CLINSZ ) + 1
   
   /*
   Local variables
   */
   SpiceBoolean            done;
   SpiceBoolean            found;
   SpiceBoolean            found2;

   static SpiceChar        cbuffer [ CBUFSZ ][ CLINSZ ];
   static SpiceChar        xbuffer [ XBUFSZ ][ CLINSZ ];
   SpiceChar               segid   [ SIDLEN ];

   SpiceDouble             dc      [ ND ];
   SpiceDouble             first;
   SpiceDouble             last;
   SpiceDouble             segpars [ 4 ];
   SpiceDouble             start;
   SpiceDouble             step;
   SpiceDouble             stop;
   SpiceDouble             sum     [ DSCSIZ  ];
   SpiceDouble             states  [ NSTATES ][6];
   SpiceDouble             xstates [ NSTATES ][6];
   
 
   SpiceInt                baddr ;
   SpiceInt                begin;
   SpiceInt                body;
   SpiceInt                center;
   SpiceInt                degree;
   SpiceInt                eaddr;
   SpiceInt                end;
   SpiceInt                frame;
   SpiceInt                han2;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                ic      [ NI ];
   SpiceInt                j;
   SpiceInt                n;
   SpiceInt                nseg;
   SpiceInt                type;
   SpiceInt                unit;






   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_daf_c" );
   

   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Create but do not load an SPK file.
   */
   tstspk_c ( SPK1, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   Case 1:
   */
   tcase_c ( "Test the forward search routines.  Count the SPK "
             "segments."                                         );
 
   dafopr_c ( SPK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   nseg = 0;
   
   dafbfs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffna_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   while ( found )
   {
      nseg++ ;
      
      daffna_c ( &found );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
   
   chcksi_c ( "(forward) Segment count", nseg, "=", N_SPK_SEG, 0, ok ); 
   
   
   
   
   /*
   Case 2:
   */
   tcase_c ( "Test the backward search routines.  Count the SPK "
             "segments."                                         );
   
   nseg = 0;
   
   dafbbs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffpa_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   while ( found )
   {
      nseg++ ;
      
      daffpa_c ( &found );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
   
   chcksi_c ( "(backward) Segment count", nseg, "=", N_SPK_SEG, 0, ok );   
   


   
   /*
   Case 3:
   */
   tcase_c ( "Test dafgs_c and dafgn_c.  Examine the descriptor " 
             "for the Phoenix spacecraft."                       );

   nseg = 0;
   
   dafbfs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffna_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   while ( found )
   {
      nseg++;
      
      dafgs_c  ( sum );
      dafgn_c  ( SIDLEN, segid );

      spkuds_c ( sum, 
                 &body,  &center,  &frame,  &type,
                 &first, &last,    &begin,  &end  );

      
      if ( body == PHOENIX )
      {

         chcksc_c ( "segid",  segid,  "=",   "PHOENIX SPACECRAFT", ok );
         
         chcksi_c ( "center", center, "=",   301,    0, ok ); 
         chcksi_c ( "type",   type,   "=",   5,      0, ok ); 
         chcksi_c ( "frame",  frame,  "=",   17,     0, ok ); 
         chcksd_c ( "first",  first,  "=",  -5.e8,   0, ok ); 
         chcksd_c ( "last",   last,   "=",   5.e8,   0, ok ); 
      
         break;
      }
      
      daffna_c ( &found );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
   
   chcksi_c ( "index of Phoenix seg", nseg, "=",  1,   0, ok ); 
   
 



   /*
   Case 4:
   */
   tcase_c ( "Test dafus_c Examine the descriptor " 
             "for the Phoenix spacecraft."            );

   nseg = 0;
   
   dafbfs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffna_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   while ( found )
   {
      nseg++;
      
      dafgs_c  ( sum );
      dafgn_c  ( SIDLEN, segid );

      dafus_c ( sum, ND, NI, dc, ic );
      
      body   = ic[0];
      center = ic[1];
      frame  = ic[2];
      type   = ic[3];
      
      first  = dc[0];
      last   = dc[1];
            
      if ( body == PHOENIX )
      {

         chcksc_c ( "segid",  segid,  "=",   "PHOENIX SPACECRAFT", ok );
         
         chcksi_c ( "center", center, "=",   301,    0, ok ); 
         chcksi_c ( "type",   type,   "=",   5,      0, ok ); 
         chcksi_c ( "frame",  frame,  "=",   17,     0, ok ); 
         chcksd_c ( "first",  first,  "=",  -5.e8,   0, ok ); 
         chcksd_c ( "last",   last,   "=",   5.e8,   0, ok ); 
      
         break;
      }
      
      daffna_c ( &found );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
   
   chcksi_c ( "index of Phoenix seg", nseg, "=",  1,   0, ok ); 
   


   /*
   Case 5:
   */
   tcase_c ( "Test dafcs_c.  Start a search on SPK1.  Create and "
             "load a second SPK.  Start a search on the second SPK. "
             "Continue the search on the first SPK."                  );
             
             
             
             
   nseg = 0;
   
   dafbfs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffna_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create and load a second SPK file.
   */
   tstspk_c ( SPK2, SPICETRUE, &han2 );
   chckxc_c ( SPICEFALSE, " ", ok );
             
   /*
   Start a backward search on the second SPK.
   */
   dafbbs_c ( han2 );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffpa_c ( &found2 );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Resume the search on the first SPK.
   */
   dafcs_c ( handle );
   
   while ( found )
   {
      nseg++;
      
      dafgs_c  ( sum );
      dafgn_c  ( SIDLEN, segid );

      dafus_c ( sum, ND, NI, dc, ic );
      
      body   = ic[0];
      center = ic[1];
      frame  = ic[2];
      type   = ic[3];
      
      first  = dc[0];
      last   = dc[1];
            
      if ( body == PHOENIX )
      {

         chcksc_c ( "segid",  segid,  "=",   "PHOENIX SPACECRAFT", ok );
         
         chcksi_c ( "center", center, "=",   301,    0, ok ); 
         chcksi_c ( "type",   type,   "=",   5,      0, ok ); 
         chcksi_c ( "frame",  frame,  "=",   17,     0, ok ); 
         chcksd_c ( "first",  first,  "=",  -5.e8,   0, ok ); 
         chcksd_c ( "last",   last,   "=",   5.e8,   0, ok ); 
      
         break;
      }
      
      daffna_c ( &found );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
   
   chcksi_c ( "index of Phoenix seg", nseg, "=",  1,   0, ok ); 
   
             
             
   
   /*
   Case 6:
   */
   
   tcase_c ( "Close the SPK file with dafcls_c.  Make sure the "
             "DAF handle to logical unit mapping fails with the error "
             "SPICE(NOSUCHHANDLE)."                                );
             
   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   dafhlu_ ( &handle, &unit );
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );
  
   /*
   Get rid of the SPK files.
   */ 
   removeFile   ( SPK1 );
   removeFile   ( SPK2 );
  
   
   /*
   Case 7:
   */
   tcase_c ( "Test dafgda_c:  first create a small SPK file "
             "containing known data.  Read the data back using "
             "dafgda_c."       );


   /*
   Create a type 8 segment in a new SPK file. 
   */
   spkopn_c ( GDABSP, GDABSP, NRESV, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i = 0;  i < NSTATES;  i++  )
   {
      for ( j = 0;  j < 6;  j++  )
      {
         xstates[i][j] = i*1000.0 + j; 
      } 
   }

   step   = 100.0;
   start  = 10.;
   stop   = start + (NSTATES-1)*step;
   degree = 1;

   spkw08_c ( handle,  399,     3,                     "J2000", 
              start,   stop,    "type 8 test segment", degree,
              NSTATES, xstates, start,                 step     );
   chckxc_c ( SPICEFALSE, " ", ok );

   spkcls_c ( handle );                         
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Open the SPK file for read access and read the 
   data from the first segment.  Although we know
   the data array starts at address 385, we'll go
   through the motions of a proper search in the
   interest of comprehensibility.
   */

   dafopr_c ( GDABSP, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dafbfs_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   daffna_c ( &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   dafgs_c  ( sum );
   chckxc_c ( SPICEFALSE, " ", ok );

   dafus_c  ( sum, ND, NI, dc, ic );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   baddr = ic[4];
   eaddr = ic[5];

   /*
   Grab the states in one shot.  Compare to the values
   we wrote.  We expect an *exact* match.
   */
   dafgda_c ( handle,  
              baddr,  
              baddr + 6*NSTATES - 1,  
              (SpiceDouble *) states  );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "states", 
              (SpiceDouble *) states, 
              "=", 
              (SpiceDouble *) xstates, 
              6*NSTATES, 
              0.0, 
              ok                          );

   /*
   Grab the segment parameters:  start epoch, step size, degree,
   and state count.  Check each one.  Again, we expect exact matches.
   */
   dafgda_c ( handle, eaddr-3, eaddr, segpars );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "start",   segpars[0], "=", start,   0.0, ok );
   chcksd_c ( "step",    segpars[1], "=", step,    0.0, ok );
   chcksd_c ( "degree",  segpars[2], "=", 
                         (SpiceDouble)degree,      0.0, ok );
   chcksd_c ( "NSTATES", segpars[3], "=", NSTATES, 0.0, ok );

   /*
   Close the file but don't delete it. 
   */
   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );



  
   /*
   Case 8:
   */
   tcase_c ( "Test dafac_c and dafec_c:  add comments to the SPK file "
             "created in the previous test case using dafac_c.  Read "
             "the comments back using dafec_c."                        );

   /*
   Fill up the large buffer with text. 
   */

   for ( i = 0;  i < XBUFSZ;  i++ )
   {
      for ( j = 0; j < CLINSZ-1;  j++ )
      {
         xbuffer[i][j] = (SpiceChar) ( '0' +  (int)( (i+j)%10 ) ); 
      }
      xbuffer[i][CLINSZ-1] = (SpiceChar)0;

      /* printf ( "%s\n", xbuffer[i] ); */
   }
   

   /*
   Add comments to the file in chunks. 
   */
   dafopw_c ( GDABSP, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


   for ( i = 0;  i < NCHUNKS;  i++ )
   {
      j = i * CBUFSZ * CLINSZ;

      memmove ( cbuffer, ((char *)xbuffer)+j, CBUFSZ*CLINSZ );

      dafac_c ( handle, CBUFSZ, CLINSZ, cbuffer );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Close the file; then open the file for read access. 
   */
   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dafopr_c ( GDABSP, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Read the comments in chunks.  Compare each line
   to the line in the buffer of "expected" text.
   */
   done = SPICEFALSE;
   i    = 0;

   while ( !done )
   {
      /*
      Read a chunk of comment text. 
      */
      dafec_c ( handle, CBUFSZ, CLINSZ, &n, cbuffer, &done );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Compare each line in the buffer to the corresponding line of
      "expected" text.
      */
      for ( j = 0;  j < n;  j++ )
      {
         chcksc_c ( "cbuffer", 
                    cbuffer[j], 
                    "=",
                    xbuffer[ (i*CBUFSZ) + j ],
                    ok                          ); 
      }

      /*
      Increment the chunk index. 
      */
      ++i;
   }

   /*
   Make sure we read all of the comment text. 
   */
   chcksi_c ( "chunk count", i, "=", NCHUNKS, 0, ok ); 
   

   /*
   Case 8:
   */
   tcase_c ( "Test dafdc_c:  delete the comments from the file."  );


   /*
   Close the file; re-open it for write access. 
   */
   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dafopw_c ( GDABSP, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Delete comments. 
   */
   dafdc_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Try to fetch comments.  We should find none. 
   */
   dafec_c ( handle, CBUFSZ, CLINSZ, &n, cbuffer, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "line count", n,   "=", 0, 0, ok ); 
   chcksl_c ( "done flag", done, SPICETRUE, ok );

   /*
   Now close and delete the file. 
   */
   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   removeFile ( GDABSP );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_daf_c */

