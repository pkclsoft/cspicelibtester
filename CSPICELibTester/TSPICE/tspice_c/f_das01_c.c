/*

-Procedure f_das01_c ( Test wrappers for DAS routines, subset 1 )

 
-Abstract
 
   Perform tests on CSPICE wrappers for a subset of the DAS 
   routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_das01_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for a subset of the CSPICE DAS
   routines. 
   
   The subset is:
      
      dasac_c
      dasec_c

-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 3.0.0 10-DEC-2016 (NJB)  

       Added test cases for 

          dasdc_c
          dashfn_c
          dasopw_c 
          dasrfr_c

   -tspice_c Version 2.0.0 05-OCT-2006 (NJB)  

       Added test cases for dascls_c and dasopr_c.

   -tspice_c Version 1.1.0 02-MAR-2003 (NJB)  

       Added separate tcase_c calls for error cases.

   -tspice_c Version 1.0.0 25-FEB-2003 (NJB)  

-Index_Entries

   test das wrappers
-&
*/

{ /* Begin f_das01_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define EK1             "test1.ek"
   #define FILSIZ          256
   #define IDWLEN          9
   #define IFNLEN          61
   #define LNSIZE          101
   #define MAXBUF          100
   #define MAXROW          20
   #define NMAX            100 

   SpiceBoolean            done;

   SpiceChar               buffer  [MAXBUF][LNSIZE];
   SpiceChar               buffer2 [MAXBUF][LNSIZE];
   SpiceChar             * fname;
   SpiceChar             * ftype;
   SpiceChar               idword  [ IDWLEN ];
   SpiceChar               ifnbuf  [ IFNLEN ];
   SpiceChar             * ifname;
   SpiceChar               label   [LNSIZE];
   SpiceChar               nambuf  [FILSIZ];
   SpiceChar             * xifnam;

   SpiceDouble             ddata  [ NMAX ];
   SpiceDouble             xddata [ NMAX ];

   SpiceInt                fileno;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                n;
   SpiceInt                ncomc;
   SpiceInt                ncomch;
   SpiceInt                ncomr;
   SpiceInt                nresvc;
   SpiceInt                nresvr;

   void                  * ptr;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_das01_c" );
   
   /*
   Fill the comment buffer. 
   */
   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( buffer[i], 
                "This is line %3d of the text buffer-----"
                "--------------------------------------->",
                (int)i                                     );
   }       



   tcase_c ( "Create a test EK as a sample DAS file." );
   fileno = 0;
  
   tstek_c  ( EK1, fileno, MAXROW, SPICEFALSE, &handle, ok );
   chckxc_c ( SPICEFALSE, " ", ok );



   tcase_c ( "Add the buffer contents to the comment area of the EK." );

   ekopw_c ( EK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasac_c ( handle, MAXBUF, LNSIZE, buffer );
   chckxc_c ( SPICEFALSE, " ", ok );

   ekcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );





   tcase_c ( "Extract the buffer contents from the comment area "
             "of the EK."                                        );

   ekopr_c ( EK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasec_c ( handle, MAXBUF, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "buffer count", n, "=", MAXBUF, 0, ok );

   chcksl_c ( "done", done, SPICETRUE, ok );
   
   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( label, "buffer line #%d", (int)i );

      chcksc_c ( label, buffer2[i], "=",  buffer[i], ok );
   }       




   tcase_c ( "Extract the buffer contents from the comment area "
             "of the EK, but this time do it in two chunks."      );

   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( buffer2[i], "%s", "" );
   }       

   dasec_c ( handle, MAXBUF/2, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "buffer count (0)", n, "=", MAXBUF/2, 0, ok );

   chcksl_c ( "done (0)", done, SPICEFALSE, ok );
   
   ptr = buffer2[MAXBUF/2];

   dasec_c ( handle, MAXBUF/2, LNSIZE, &n, ptr, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "done (1)", done, SPICETRUE, ok );

   chcksi_c ( "buffer count (1)", n, "=", MAXBUF/2, 0, ok );
   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( label, "buffer line #%d", (int)i );

      chcksc_c ( label, buffer2[i], "=",  buffer[i], ok );
   }       

   tcase_c ( "Repeat the previous test to make sure dasec_c has "
             "been re-initialized when the last comments were read."  );


   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( buffer2[i], "%s", "" );
   }       

   dasec_c ( handle, MAXBUF/2, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "buffer count (0)", n, "=", MAXBUF/2, 0, ok );

   chcksl_c ( "done (0)", done, SPICEFALSE, ok );
   
   ptr = buffer2[MAXBUF/2];

   dasec_c ( handle, MAXBUF/2, LNSIZE, &n, ptr, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "done (1)", done, SPICETRUE, ok );

   chcksi_c ( "buffer count (1)", n, "=", MAXBUF/2, 0, ok );
   for ( i = 0;  i < MAXBUF;  i++ )
   {
      sprintf ( label, "buffer line #%d", (int)i );

      chcksc_c ( label, buffer2[i], "=",  buffer[i], ok );
   }     


   /*
   Test dasopw_c, dasdc_c, and dasrfr_c.
   */
   tcase_c ( "Close EK1; re-open the file for write access" );

   dascls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasopw_c ( EK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Get the file record parameters  from the EK file,
   */
   dasrfr_c ( handle,  IDWLEN,  IFNLEN, idword, ifnbuf,
              &nresvr, &nresvc, &ncomr, &ncomc         );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the ID word.
   */
   chcksc_c ( "idword", idword, "=", "DAS/EK", ok );

   /*
   Check the internal file name.
   */
   xifnam = "ek test file #0";

   chcksc_c ( "ifname", ifnbuf, "=", xifnam, ok );

   /*
   Check the comment record and character counts.
   */
   chcksi_c ( "ncomc", ncomc, "=", MAXBUF*81,          0, ok );
   chcksi_c ( "ncomr", ncomr, "=", ((ncomc-1)/1024)+1, 0, ok );

   /*
   Check the reserved record and character counts.
   */
   chcksi_c ( "nresvc", nresvc, "=", 0, 0, ok );
   chcksi_c ( "nresvr", nresvr, "=", 0, 0, ok );


   /*
   Delete the comments from EK1. 
   */
   dasdc_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the comment area of EK1; make sure there are no
   remaining comments. 
   */
   dasec_c ( handle, MAXBUF, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "n", n, "=", 0, 0, ok );
   chcksl_c ( "done", done, SPICETRUE, ok );

   /*
   Restore the comments and the contents of buffer2 for later use. 
   */
   dasac_c ( handle, MAXBUF, LNSIZE, buffer );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasec_c ( handle, MAXBUF, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   dasac_c error checks: 
   */
   tcase_c ( "Set up for dasac_c error checks." );

   ekcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   ekopw_c ( EK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


   tcase_c ( "Negative buffer size." );

   dasac_c ( handle, -1, LNSIZE, buffer );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARGUMENT)", ok );


   tcase_c ( "Null buffer pointer." );

   dasac_c ( handle, MAXBUF, LNSIZE, NULLCPTR );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   tcase_c ( "String length 1." );

   dasac_c ( handle, MAXBUF, 1, buffer );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );


   tcase_c ( "Non-printing character in buffer." );
   buffer[0][0] = 1;

   dasac_c ( handle, MAXBUF, LNSIZE, buffer );
   chckxc_c ( SPICETRUE, "SPICE(ILLEGALCHARACTER)", ok );

   buffer[0][0] = (SpiceChar)'T';

   /*
   dasec_c error checks: 
   */
   tcase_c ( "Set up for dasac_c error checks." );
   ekcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   ekopr_c ( EK1, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


   tcase_c ( "Negative buffer size." );

   dasec_c ( handle, -1, LNSIZE, &n, buffer2, &done );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARGUMENT)", ok );


   tcase_c ( "Null buffer pointer." );

   dasec_c ( handle, MAXBUF, LNSIZE, &n, NULLCPTR, &done );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   tcase_c ( "String length 1." );

   dasec_c ( handle, MAXBUF, 1, &n, buffer2, &done );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );


   tcase_c ( "Comment line too long." );

   dasec_c ( handle, MAXBUF, 10, &n, buffer2, &done );
   chckxc_c ( SPICETRUE, "SPICE(COMMENTTOOLONG)", ok );



   /*
   Test dashfn_c:
  
   Map the handle of EK1 back to a file name.
   */
   tcase_c ( "Test dashfn_c: recover name of EK1 from its handle." );

   dashfn_c ( handle, FILSIZ, nambuf );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksc_c ( "nambuf", nambuf, "=", EK1, ok );
   

   tcase_c ( "Test dashfn_c: output string too short to "
             "accommodate any data."                     );

   dashfn_c ( handle, 1, nambuf );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );


   tcase_c ( "Test dashfn_c: output string too short to "
             "hold full file name. File name should be "
             "truncated."                               );

   dashfn_c ( handle, 5, nambuf );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksc_c ( "nambuf", nambuf, "=", "test", ok );



   tcase_c ( "Test dashfn_c: output string pointer is null." );

   dashfn_c ( handle, FILSIZ, NULL );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

  



   /*
   Get rid of the EK file.
   */

   ekcls_c ( handle );

   TRASH   ( EK1 );
   
   
   /*
   Create a small DAS file and close it using dascls_c. 
   */

   tcase_c ( "test dascls_c:  create and close DAS file." );
   /*
   We'll give the file the same internal file name
   as the file's actual name.  We don't require any
   comment records.
   */
   fname  = "TEST.DAS";
   ftype  = "TEST";
   ifname = fname;
   ncomch = 0;

   dasonw_ ( (SpiceChar *) fname,
             (SpiceChar *) ftype,
             (SpiceChar *) ifname,
             (integer   *) &ncomch,
             (integer   *) &handle,
             (ftnlen     ) strlen(fname),        
             (ftnlen     ) strlen(ftype),        
             (ftnlen     ) strlen(ifname)  );

   chckxc_c ( SPICEFALSE, " ", ok );


   for ( i = 0;  i < NMAX;  i++ )
   {
      xddata[i] = (SpiceDouble)i; 
   }

   n = NMAX;

   dasadd_ ( &handle, &n, xddata );
   chckxc_c ( SPICEFALSE, " ", ok );

   dascls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );



   tcase_c ( "Test dasopr_c:  open the DAS file for "
             "read access and check the contents."   );


   dasopr_c ( fname, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   i = 1;
   dasrdd_ ( &handle, &i, &n, ddata );

   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "ddata", ddata, "=", xddata, n, 0.0, ok );


   /*
   Close and delete the DAS file. 
   */
   dascls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   removeFile ( fname );

   


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_das01_c */

