/* f_dasa2l.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__8 = 8;
static doublereal c_b135 = 0.;
static integer c__30 = 30;
static doublereal c_b473 = 1.;
static doublereal c_b474 = 3.;
static integer c__257 = 257;

/* $Procedure      F_DASA2L ( Test DAS address calculation routine ) */
/* Subroutine */ int f_dasa2l__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };
    static integer prev[3] = { 3,1,2 };
    static integer recszs[3] = { 1024,128,256 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;
    char ch__1[1];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static integer seed, free;
    static char cval[1];
    static doublereal dval;
    static integer ival;
    static char path[50*36], fast[255*10];
    extern /* Subroutine */ int t_pthget__(char *, ftnlen), t_pthcmp__(char *,
	     char *, ftnlen, ftnlen), t_pthnew__(void);
    static integer i__, j, k, n;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer hfast[10], lastc, lastd, recno, lasti;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer dtype;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static char ftype[4];
    static integer hwrit[10];
    extern /* Subroutine */ int dasa2l_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), t_success__(logical *
	    ), dasadc_(integer *, integer *, integer *, integer *, char *, 
	    ftnlen), dasadd_(integer *, integer *, doublereal *);
    static integer bighan, clbase, handle;
    extern /* Subroutine */ int dasadi_(integer *, integer *, integer *), 
	    delfil_(char *, ftnlen), dasham_(integer *, char *, ftnlen);
    static char access[10];
    extern /* Subroutine */ int dasllc_(integer *), chckxc_(logical *, char *,
	     logical *, ftnlen), chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), dasrdi_(integer *, 
	    integer *, integer *, integer *);
    static integer ncomch;
    extern /* Subroutine */ int dashfs_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *);
    static logical fclose;
    static integer fileno, nschem, xclbas;
    static char errdas[255];
    static integer addrss;
    static char rdnseg[255*10];
    static integer hrdnsg[10], lastla[3], clsize, clnwds[100000];
    static char wrtabl[255*10];
    static integer lastrc[3], lastwd[3], ncomrc;
    static char exppth[50*36];
    static integer nclust, nresvc, cltyps[100000];
    extern logical exists_(char *, ftnlen);
    static integer nresvr, wordno, xclsiz, xrecno, xwrdno;
    static logical sgrgat;
    extern /* Subroutine */ int dasonw_(char *, char *, char *, integer *, 
	    integer *, ftnlen, ftnlen, ftnlen), daswbr_(integer *), sigerr_(
	    char *, ftnlen), dasufs_(integer *, integer *, integer *, integer 
	    *, integer *, integer *, integer *, integer *, integer *), 
	    dasuri_(integer *, integer *, integer *, integer *, integer *), 
	    tstdas_(char *, char *, integer *, integer *, integer *, integer *
	    , integer *, logical *, logical *, integer *, integer *, ftnlen, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), dasopr_(char *, integer *, ftnlen), 
	    dascls_(integer *), dasrdd_(integer *, integer *, integer *, 
	    doublereal *), chcksd_(char *, doublereal *, char *, doublereal *,
	     doublereal *, logical *, ftnlen, ftnlen), dasrdc_(integer *, 
	    integer *, integer *, integer *, integer *, char *, ftnlen), 
	    dasopw_(char *, integer *, ftnlen), ssizec_(integer *, char *, 
	    ftnlen), appndc_(char *, char *, ftnlen, ftnlen), daslla_(integer 
	    *, integer *, integer *, integer *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static char das0[255], das1[255];
    extern /* Subroutine */ int p_dasa2l__(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), t_dasa2l__(integer *,
	     integer *, integer *, integer *, integer *, integer *, integer *)
	    ;

/* $ Abstract */

/*     Test the SPICELIB DAS address calculation routine DASA2L. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include File:  F_DASA2L Parameters */

/*        f_dasa2l.inc  Version 1    12-APR-2014 (NJB) */



/*     The parameters below are names of logic paths in DASA2L. */
/*     These are used by F_DASA2L and P_DASA2L (an instrumented */
/*     test version of DASA2L). */


/*     End Include File:  F_DASA2L Parameters */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the routine DASA2L. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 25-APR-2014 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Other functions */


/*     Local parameters */


/*     Words per data record, for each data type: */


/*     Directory forward pointer location */

/*      INTEGER               FWDLOC */
/*      PARAMETER           ( FWDLOC = 2 ) */

/*     Directory address range locations */


/*     Index of highest address in a `range array': */

/*      INTEGER               HIGH */
/*      PARAMETER           ( HIGH = 2 ) */

/*     Location of first type descriptor */

/*      INTEGER               BEGDSC */
/*      PARAMETER           ( BEGDSC = 9 ) */

/*     File table size */

/*      INTEGER               MAXFIL */
/*      PARAMETER           ( MAXFIL = 20 ) */

/*     Number of cluster descriptors in cluster directory: */


/*     Data word numbering scheme codes: */

/*     SCHADD indicates that each numeric word contains */
/*     its own address. Character words contain their */
/*     addresses mod 128. */


/*     SCHCOD indicates that each numeric word contains */
/*     a code based on the file number, record, and address. */

/*      INTEGER               SCHCOD */
/*      PARAMETER           ( SCHCOD = 2 ) */

/*     Additional parameters */

/*      INTEGER               TOTFIL */
/*      PARAMETER           ( TOTFIL = 30 ) */
/*      INTEGER               MAXLOD */
/*      PARAMETER           ( MAXLOD = 20 ) */

/*     DASA2L file buffer size: */

/*      INTEGER               A2LBSZ */
/*      PARAMETER           ( A2LBSZ = 20 ) */

/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_DASA2L", (ftnlen)8);
/* ********************************************************************** */

/*     DASA2L error cases: */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Bad data type", (ftnlen)13);
    s_copy(errdas, "error.das", (ftnlen)255, (ftnlen)9);
    if (exists_(errdas, (ftnlen)255)) {
	delfil_(errdas, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dasonw_(errdas, "TEST", errdas, &c__0, &handle, (ftnlen)255, (ftnlen)4, (
	    ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dtype = -1;
    addrss = 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASINVALIDTYPE)", ok, (ftnlen)21);

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DP address out of range", (ftnlen)23);
    s_copy(errdas, "error.das", (ftnlen)255, (ftnlen)9);
    if (exists_(errdas, (ftnlen)255)) {
	delfil_(errdas, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dasonw_(errdas, "TEST", errdas, &c__0, &handle, (ftnlen)255, (ftnlen)4, (
	    ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 10;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = (doublereal) i__;
	dasadd_(&handle, &c__1, &d__1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasadi_(&handle, &c__1, &i__);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)&ch__1[0] = i__;
	dasadc_(&handle, &c__1, &c__1, &c__1, ch__1, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dtype = 2;
    addrss = n + 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);
    addrss = 0;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("INT address out of range", (ftnlen)24);
    dtype = 3;
    addrss = n + 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);
    addrss = 0;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("CHR address out of range", (ftnlen)24);
    dtype = 1;
    addrss = n + 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);
    addrss = 0;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASNOSUCHADDRESS)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("FAILED is TRUE after DASHFS; new file case", (ftnlen)42);
    sigerr_("HFS setup", (ftnlen)9);
    dtype = 1;
    addrss = 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "HFS setup", ok, (ftnlen)9);

/* --- Case -------------------------------------------------------- */

    tcase_("FAILED is TRUE after DASHFS; known file case", (ftnlen)44);
    dtype = 1;
    addrss = 1;
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sigerr_("HFS setup", (ftnlen)9);
    dasa2l_(&handle, &dtype, &addrss, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "HFS setup", ok, (ftnlen)9);

/* --- Case -------------------------------------------------------- */

    tcase_("DASRRI failure: attempt to read non-existent record.", (ftnlen)52)
	    ;

/*     Set the maximum integer address in the file summary */
/*     high enough so that DASA2L will look beyond the last */
/*     directory. */

    ival = 1000000;

    dashfs_(&handle, &nresvr, &nresvc, &ncomrc, &ncomch, &free, lastla, 
	    lastrc, lastwd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    lastla[2] = ival;
    dasufs_(&handle, &nresvr, &nresvc, &ncomrc, &ncomch, &free, lastla, 
	    lastrc, lastwd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasa2l_(&handle, &c__3, &ival, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(DASFILEREADFAILED)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad directory record: promises more data than are really availab"
	    "le.", (ftnlen)67);

/*     Assign a big value to the upper integer address of the first */
/*     directory record. */

    ival = 1000000;
    dasuri_(&handle, &c__2, &c__8, &c__8, &ival);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get DASFM in on the conspiracy: update the file's summary to */
/*     agree with the lie told by the directory record's integer address */
/*     range. */

    dashfs_(&handle, &nresvr, &nresvc, &ncomrc, &ncomch, &free, lastla, 
	    lastrc, lastwd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    lastla[2] = ival;
    dasufs_(&handle, &nresvr, &nresvc, &ncomrc, &ncomch, &free, lastla, 
	    lastrc, lastwd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasa2l_(&handle, &c__3, &ival, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_true, "SPICE(BADDASDIRECTORY)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Clean up error test file.", (ftnlen)25);

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(errdas, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     DASA2L normal cases: */

/* ********************************************************************** */

/*     The following set of tests relies on the DAS reader routines */
/*     to exercise DASA2L. */


/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only INT data.", (ftnlen)40);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 1;
    cltyps[0] = 3;
    clnwds[0] = 3000;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains only INT data.", (ftnlen)41);

/*     Open file for read access; check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }

/*     Close DAS. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only DP data.", (ftnlen)39);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 1;
    cltyps[0] = 2;
    clnwds[0] = 1500;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains only DP data.", (ftnlen)40);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only CHR data.", (ftnlen)40);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 1;
    cltyps[0] = 1;
    clnwds[0] = 5000;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains only CHR data.", (ftnlen)41);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only INT and DP data.", (ftnlen)47);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 2;
    cltyps[0] = 3;
    clnwds[0] = 3000;
    cltyps[1] = 2;
    clnwds[1] = 1500;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close file so as to leave it unsegregated. */

/*     This is a special case: we need to create a read-only, */
/*     unsegregated file having the data types of its first */
/*     two clusters in descending numeric order (DP < INT). */
/*     This file is used to test a branch of the segregation */
/*     detection logic in DASA2L. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only and unsegregated; contains only INT and DP data."
	    , (ftnlen)65);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only and segregated; contains only INT and DP data.", 
	    (ftnlen)63);

/*     Segregate the file. */

    dasopw_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only INT and CHAR data.", (ftnlen)49);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 2;
    cltyps[0] = 3;
    clnwds[0] = 5000;
    cltyps[1] = 1;
    clnwds[1] = 2500;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains only INT and CHAR data.", (ftnlen)50);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains only CHAR and DP data.", (ftnlen)48);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 2;
    cltyps[0] = 1;
    clnwds[0] = 5000;
    cltyps[1] = 2;
    clnwds[1] = 2500;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains only CHAR and DP data.", (ftnlen)49);
    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is writable; contains INT, CHAR and DP data.", (ftnlen)48);
    s_copy(das0, "test0.das", (ftnlen)255, (ftnlen)9);
    if (exists_(das0, (ftnlen)255)) {
	delfil_(das0, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    nclust = 3;
    cltyps[0] = 3;
    clnwds[0] = 1500;
    cltyps[1] = 1;
    clnwds[1] = 5000;
    cltyps[2] = 2;
    clnwds[2] = 2500;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das0, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &handle, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Verify write access. */

    dasham_(&handle, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Check data. */

    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is read-only; contains INT, CHAR and DP data.", (ftnlen)49);
/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close DAS. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is segregated; contains INT, CHAR and DP data.", (ftnlen)50);

/*     Open existing DAS for write access; close after segregation. */

    dasopw_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check data. */

    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdi_(&handle, &i__, &i__, &ival);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (ival != i__) {
	    chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (ftnlen)1)
		    ;
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = i__ % 128;
	if (*(unsigned char *)cval != j) {
	    *(unsigned char *)&ch__1[0] = j;
	    chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
		    ftnlen)1, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasrdd_(&handle, &i__, &i__, &dval);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (dval != (doublereal) i__) {
	    d__1 = (doublereal) i__;
	    chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Close DAS. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is segregated; contains INT, CHAR and DP data. Check logic p"
	    "ath for first d.p. lookup. Path case 3 : FAST, UNKNOWN FILE.", (
	    ftnlen)124);
    dasopr_(das0, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create expected path. */

    ssizec_(&c__30, exppth, (ftnlen)50);
    appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
    appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
    appndc_("UNKNOWN FILE", exppth, (ftnlen)12, (ftnlen)50);
    appndc_("BUFFER SHIFT", exppth, (ftnlen)12, (ftnlen)50);
    appndc_("BUFFER INSERTION", exppth, (ftnlen)16, (ftnlen)50);
    appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
    appndc_("LOOK UP ACCESS METHOD", exppth, (ftnlen)21, (ftnlen)50);
    appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
    appndc_("SEGREGATION CHECK", exppth, (ftnlen)17, (ftnlen)50);
    appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
    appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
    appndc_("FAST FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)50);
    appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Execute instrumented version of DASA2L. */

    t_pthnew__();
    p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check actual path. */

    ssizec_(&c__30, path, (ftnlen)50);
    t_pthget__(path, (ftnlen)50);
    t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DAS is segregated; contains INT, CHAR and DP data. Check logic p"
	    "ath for second lookup (type = INT). Path case 1: FAST, SAME FILE."
	    , (ftnlen)129);
    p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create expected path. */

    ssizec_(&c__30, exppth, (ftnlen)50);
    appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
    appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
    appndc_("FAST FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)50);
    appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Execute instrumented version of DASA2L. */

    t_pthnew__();
    p_dasa2l__(&handle, &c__3, &c__1, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check actual path. */

    ssizec_(&c__30, path, (ftnlen)50);
    t_pthget__(path, (ftnlen)50);
    t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */


/*     Test address calculation for a non-trivial, unsegregated file. */


/*     This file contains multiple cluster directories. All possible */
/*     combinations of data types of adjacent clusters occur. */

    tcase_("Create non-trivial, unsegregated file.", (ftnlen)38);

/*     Set cluster data types. */

    nclust = 0;
    for (i__ = 1; i__ <= 2; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    ++nclust;

/*           Set the first data type of the record. */

	    cltyps[(i__1 = nclust - 1) < 100000 && 0 <= i__1 ? i__1 : s_rnge(
		    "cltyps", i__1, "f_dasa2l__", (ftnlen)1551)] = j;
	    if (i__ == 1) {

/*              Insert one "previous" type at cluster position 2. */
/*              This exercises a branch of the segregation detection */
/*              logic in DASA2L. */

		++nclust;
		cltyps[(i__1 = nclust - 1) < 100000 && 0 <= i__1 ? i__1 : 
			s_rnge("cltyps", i__1, "f_dasa2l__", (ftnlen)1560)] = 
			prev[(i__3 = cltyps[(i__2 = nclust - 2) < 100000 && 0 
			<= i__2 ? i__2 : s_rnge("cltyps", i__2, "f_dasa2l__", 
			(ftnlen)1560)] - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"prev", i__3, "f_dasa2l__", (ftnlen)1560)];
		for (k = 3; k <= 247; ++k) {
		    ++nclust;
		    cltyps[(i__1 = nclust - 1) < 100000 && 0 <= i__1 ? i__1 : 
			    s_rnge("cltyps", i__1, "f_dasa2l__", (ftnlen)1566)
			    ] = next[(i__3 = cltyps[(i__2 = nclust - 2) < 
			    100000 && 0 <= i__2 ? i__2 : s_rnge("cltyps", 
			    i__2, "f_dasa2l__", (ftnlen)1566)] - 1) < 3 && 0 
			    <= i__3 ? i__3 : s_rnge("next", i__3, "f_dasa2l__"
			    , (ftnlen)1566)];
		}
	    } else {
		for (k = 2; k <= 247; ++k) {
		    ++nclust;
		    cltyps[(i__1 = nclust - 1) < 100000 && 0 <= i__1 ? i__1 : 
			    s_rnge("cltyps", i__1, "f_dasa2l__", (ftnlen)1575)
			    ] = prev[(i__3 = cltyps[(i__2 = nclust - 2) < 
			    100000 && 0 <= i__2 ? i__2 : s_rnge("cltyps", 
			    i__2, "f_dasa2l__", (ftnlen)1575)] - 1) < 3 && 0 
			    <= i__3 ? i__3 : s_rnge("prev", i__3, "f_dasa2l__"
			    , (ftnlen)1575)];
		}
	    }
	}
    }

/*     Set cluster record counts. */

    seed = -1;
    i__1 = nclust;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = t_randd__(&c_b473, &c_b474, &seed);
	j = i_dnnt(&d__1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	clnwds[(i__2 = i__ - 1) < 100000 && 0 <= i__2 ? i__2 : s_rnge("clnwds"
		, i__2, "f_dasa2l__", (ftnlen)1595)] = j * recszs[(i__4 = 
		cltyps[(i__3 = i__ - 1) < 100000 && 0 <= i__3 ? i__3 : s_rnge(
		"cltyps", i__3, "f_dasa2l__", (ftnlen)1595)] - 1) < 3 && 0 <= 
		i__4 ? i__4 : s_rnge("recszs", i__4, "f_dasa2l__", (ftnlen)
		1595)];
    }

/*  @@@ */

    s_copy(das1, "big_nonseg.das", (ftnlen)255, (ftnlen)14);
    if (exists_(das1, (ftnlen)255)) {
	delfil_(das1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
    ncomrc = 0;
    fileno = 1;
    sgrgat = FALSE_;
    fclose = FALSE_;
    nschem = 1;
    tstdas_(das1, ftype, &ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &
	    fclose, &nschem, &bighan, (ftnlen)255, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Read from the big, non-segregated, writable DAS", (ftnlen)47);

/*     Verify write access. */

    dasham_(&bighan, access, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("ACCESS", access, "=", "WRITE", ok, (ftnlen)6, (ftnlen)10, (
	    ftnlen)1, (ftnlen)5);

/*     Get maximum addresses of each data type. */

    daslla_(&bighan, &lastc, &lastd, &lasti);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check integer data. */

    i__1 = lasti;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasa2l_(&bighan, &c__3, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__3, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);

/*        We're not going to call CHCKSI for each item, because that */
/*        call is way too slow. Check for a mismatched output first. */

	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/*     Check d.p. data. */

    i__1 = lastd;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasa2l_(&bighan, &c__2, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__2, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);
	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/*     Check character data. */

    i__1 = lastc;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasa2l_(&bighan, &c__1, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__1, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);
	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from the big, non-segregated, read-only DAS", (ftnlen)48);

/*     Close DAS without segregating: write buffered records */
/*     (mandatory!) and execute low-level close. */

    daswbr_(&bighan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&bighan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get maximum addresses of each data type. */

    dasopr_(das1, &bighan, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daslla_(&bighan, &lastc, &lastd, &lasti);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're going to use a skip value of 20 in the following tests */
/*     in order to speed them up. The file's cluster structure */
/*     should be the same as that of the writable file; all we've */
/*     done is change the file's status to read-only. */


/*     Check integer data. */

    i__1 = lasti;
    for (i__ = 1; i__ <= i__1; i__ += 20) {
	dasa2l_(&bighan, &c__3, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__3, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);

/*        We're not going to call CHCKSI for each item, because that */
/*        call is way too slow. Check for a mismatched output first. */

	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/*     Check d.p. data. */

    i__1 = lastd;
    for (i__ = 1; i__ <= i__1; i__ += 20) {
	dasa2l_(&bighan, &c__2, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__2, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);
	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/*     Check character data. */

    i__1 = lastc;
    for (i__ = 1; i__ <= i__1; i__ += 20) {
	dasa2l_(&bighan, &c__1, &i__, &clbase, &clsize, &recno, &wordno);
	t_dasa2l__(&bighan, &c__1, &i__, &xclbas, &xclsiz, &xrecno, &xwrdno);
	if (recno != xrecno || wordno != xwrdno || clbase != xclbas || clsize 
		!= xclsiz) {
	    chcksi_("RECNO", &recno, "=", &xrecno, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chcksi_("WORDNO", &wordno, "=", &xwrdno, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLBASE", &clbase, "=", &xclbas, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksi_("CLSIZE", &clsize, "=", &xclsiz, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/*     Make the big file writable for the following tests. */

    dascls_(&bighan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasopw_(das1, &bighan, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */


/*     Buffering tests follow. */

/*     We'll use sets of files with the following attributes: */

/*        "fast": readonly and segregated */
/*        read-only, non-segregated */
/*        writable */

    tcase_("Create a set of writable DAS files.", (ftnlen)35);
    for (i__ = 1; i__ <= 10; ++i__) {
	s_copy(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"wrtabl", i__1, "f_dasa2l__", (ftnlen)1839)) * 255, "writabl"
		"e#.das", (ftnlen)255, (ftnlen)13);
	i__3 = i__ - 1;
	repmi_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"wrtabl", i__1, "f_dasa2l__", (ftnlen)1840)) * 255, "#", &
		i__3, wrtabl + ((i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : 
		s_rnge("wrtabl", i__2, "f_dasa2l__", (ftnlen)1840)) * 255, (
		ftnlen)255, (ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		s_rnge("wrtabl", i__1, "f_dasa2l__", (ftnlen)1843)) * 255, (
		ftnlen)255)) {
	    delfil_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		    s_rnge("wrtabl", i__1, "f_dasa2l__", (ftnlen)1844)) * 255,
		     (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
	ncomrc = 0;
	fileno = 1;
	nclust = 4;
	cltyps[0] = 3;
	clnwds[0] = 768;
	cltyps[1] = 1;
	clnwds[1] = 3000;
	cltyps[2] = 2;
	clnwds[2] = 400;
	cltyps[3] = 3;
	clnwds[3] = 1;
	sgrgat = FALSE_;
	fclose = FALSE_;
	nschem = 1;
	tstdas_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"wrtabl", i__1, "f_dasa2l__", (ftnlen)1864)) * 255, ftype, &
		ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &fclose, &
		nschem, &hwrit[(i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : 
		s_rnge("hwrit", i__2, "f_dasa2l__", (ftnlen)1864)], (ftnlen)
		255, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The files are left open. */

    }

/*     Create a set of read-only, non-segregated DAS files. */

    for (i__ = 1; i__ <= 10; ++i__) {
	s_copy(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)1880)) * 255, "readnon"
		"seg#.das", (ftnlen)255, (ftnlen)15);
	i__3 = i__ - 1;
	repmi_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)1881)) * 255, "#", &
		i__3, rdnseg + ((i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : 
		s_rnge("rdnseg", i__2, "f_dasa2l__", (ftnlen)1881)) * 255, (
		ftnlen)255, (ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		s_rnge("rdnseg", i__1, "f_dasa2l__", (ftnlen)1884)) * 255, (
		ftnlen)255)) {
	    delfil_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		    s_rnge("rdnseg", i__1, "f_dasa2l__", (ftnlen)1885)) * 255,
		     (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
	ncomrc = 0;
	fileno = 1;
	nclust = 4;
	cltyps[0] = 3;
	clnwds[0] = 768;
	cltyps[1] = 1;
	clnwds[1] = 3000;
	cltyps[2] = 2;
	clnwds[2] = 400;
	cltyps[3] = 3;
	clnwds[3] = 1;
	sgrgat = FALSE_;
	fclose = FALSE_;
	nschem = 1;
	tstdas_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)1905)) * 255, ftype, &
		ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &fclose, &
		nschem, &hrdnsg[(i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : 
		s_rnge("hrdnsg", i__2, "f_dasa2l__", (ftnlen)1905)], (ftnlen)
		255, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The files are closed without being segregated. */

	daswbr_(&hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)1913)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dasllc_(&hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)1916)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a set of fast DAS files. */

    for (i__ = 1; i__ <= 10; ++i__) {
	s_copy(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)1927)) * 255, "fast#.das",
		 (ftnlen)255, (ftnlen)9);
	i__3 = i__ - 1;
	repmi_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)1928)) * 255, "#", &i__3, 
		fast + ((i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		"fast", i__2, "f_dasa2l__", (ftnlen)1928)) * 255, (ftnlen)255,
		 (ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		s_rnge("fast", i__1, "f_dasa2l__", (ftnlen)1931)) * 255, (
		ftnlen)255)) {
	    delfil_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : 
		    s_rnge("fast", i__1, "f_dasa2l__", (ftnlen)1932)) * 255, (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	s_copy(ftype, "TEST", (ftnlen)4, (ftnlen)4);
	ncomrc = 0;
	fileno = 1;
	nclust = 4;
	cltyps[0] = 3;
	clnwds[0] = 768;
	cltyps[1] = 1;
	clnwds[1] = 3000;
	cltyps[2] = 2;
	clnwds[2] = 400;
	cltyps[3] = 3;
	clnwds[3] = 1;
	sgrgat = FALSE_;
	fclose = FALSE_;
	nschem = 1;
	tstdas_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)1953)) * 255, ftype, &
		ncomrc, &fileno, &nclust, cltyps, clnwds, &sgrgat, &fclose, &
		nschem, &hfast[(i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : 
		s_rnge("hfast", i__2, "f_dasa2l__", (ftnlen)1953)], (ftnlen)
		255, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The files are segregated and closed. */

	dascls_(&hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)1961)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from writable files in interleaved order", (ftnlen)45);
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hwrit[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hwrit", i__2, "f_dasa2l__", (ftnlen)1977)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }

/*           Include a lookup in a complex, unsegregated, read-only */
/*           file. */

	    dasa2l_(&bighan, &c__3, &c__1, &clbase, &clsize, &recno, &wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hwrit[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hwrit", i__2, "f_dasa2l__", (ftnlen)2002)];
	    dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    j = i__ % 128;
	    if (*(unsigned char *)cval != j) {
		*(unsigned char *)&ch__1[0] = j;
		chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
			ftnlen)1, (ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__1, &clbase, &clsize, &recno, &wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hwrit[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hwrit", i__2, "f_dasa2l__", (ftnlen)2025)];
	    dasrdd_(&handle, &i__, &i__, &dval);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (dval != (doublereal) i__) {
		d__1 = (doublereal) i__;
		chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
			ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__1, &clbase, &clsize, &recno, &wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[3];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hwrit[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hwrit", i__2, "f_dasa2l__", (ftnlen)2047)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }

/*           Include a lookup in a complex, unsegregated, read-only */
/*           file. */

	    dasa2l_(&bighan, &c__3, &c__1, &clbase, &clsize, &recno, &wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from writable files; path check. Path case 7: WRITABLE, SAM"
	    "E FILE.", (ftnlen)71);
    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2078)];

/*        Do one read to get the file's attributes buffered. */

	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from writable files; path check. Path case 8: WRITABLE, KNO"
	    "WN FILE.", (ftnlen)72);

/*     Do one read of the last file so the next file read */
/*     will be a different one. */

    handle = hwrit[9];
    p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2146)];

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from writable files; path check. Path case 9: WRITABLE, UNK"
	    "NOWN FILE.", (ftnlen)74);

/*     Close the files and re-open them for write access. */

    for (i__ = 1; i__ <= 10; ++i__) {

/*        Close the file without segregating it. */

	daswbr_(&hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2199)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dasllc_(&hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2201)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	dasopw_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"wrtabl", i__1, "f_dasa2l__", (ftnlen)2208)) * 255, &hwrit[(
		i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hwrit", 
		i__2, "f_dasa2l__", (ftnlen)2208)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2216)];

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("UNKNOWN FILE", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER SHIFT", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER INSERTION", exppth, (ftnlen)16, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("LOOK UP ACCESS METHOD", exppth, (ftnlen)21, (ftnlen)50);
	appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from read-only, unsegregated files in interleaved order", (
	    ftnlen)60);
    for (k = 1; k <= 10; ++k) {
	dasopr_(rdnseg + ((i__1 = k - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)2267)) * 255, &hrdnsg[(
		i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hrdnsg", 
		i__2, "f_dasa2l__", (ftnlen)2267)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hrdnsg[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hrdnsg", i__2, "f_dasa2l__", (ftnlen)2275)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hrdnsg[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hrdnsg", i__2, "f_dasa2l__", (ftnlen)2298)];
	    dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    j = i__ % 128;
	    if (*(unsigned char *)cval != j) {
		*(unsigned char *)&ch__1[0] = j;
		chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
			ftnlen)1, (ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hrdnsg[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hrdnsg", i__2, "f_dasa2l__", (ftnlen)2321)];
	    dasrdd_(&handle, &i__, &i__, &dval);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (dval != (doublereal) i__) {
		d__1 = (doublereal) i__;
		chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
			ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[3];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hrdnsg[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hrdnsg", i__2, "f_dasa2l__", (ftnlen)2343)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from read-only, unsegregated files twice; path check. Path "
	    "case 4: READONLY, SAME FILE.", (ftnlen)92);
    for (i__ = 1; i__ <= 10; ++i__) {

/*        Do one read from the current file. */

	handle = hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2373)];
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from read-only, unsegregated files; path check. Path case 5"
	    ": READONLY, KNOWN FILE.", (ftnlen)87);

/*     Do one read from the last file. This ensures that the */
/*     first file read in the loop below is not the same */
/*     as that previously read. */

    handle = hrdnsg[9];
    p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2436)];

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from read-only, unsegregated files; path check. Path case 6"
	    ": READONLY, UNKNOWN FILE.", (ftnlen)89);

/*     Close the files and re-open them for read access. All files */
/*     will be unknown. */

    for (i__ = 1; i__ <= 10; ++i__) {
	dascls_(&hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2487)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	dasopr_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)2492)) * 255, &hrdnsg[(
		i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hrdnsg", 
		i__2, "f_dasa2l__", (ftnlen)2492)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2499)];

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("UNKNOWN FILE", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER SHIFT", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER INSERTION", exppth, (ftnlen)16, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("LOOK UP ACCESS METHOD", exppth, (ftnlen)21, (ftnlen)50);
	appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
	appndc_("SEGREGATION CHECK", exppth, (ftnlen)17, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("SLOW FILE CLUSTER SEARCH", exppth, (ftnlen)24, (ftnlen)50);
	appndc_("SLOW FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from fast files in interleaved order", (ftnlen)41);

/*     Close the read-only, unsegregated files. */

    for (k = 1; k <= 10; ++k) {
	dascls_(&hrdnsg[(i__1 = k - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2555)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (k = 1; k <= 10; ++k) {
	dasopr_(fast + ((i__1 = k - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)2561)) * 255, &hfast[(
		i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hfast", i__2,
		 "f_dasa2l__", (ftnlen)2561)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = clnwds[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hfast[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hfast", i__2, "f_dasa2l__", (ftnlen)2569)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }

/*           Include a lookup in a complex, unsegregated, read-only */
/*           file. */

	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hfast[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hfast", i__2, "f_dasa2l__", (ftnlen)2596)];
	    dasrdc_(&handle, &i__, &i__, &c__1, &c__1, cval, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    j = i__ % 128;
	    if (*(unsigned char *)cval != j) {
		*(unsigned char *)&ch__1[0] = j;
		chcksc_("CVAL", cval, "=", ch__1, ok, (ftnlen)4, (ftnlen)1, (
			ftnlen)1, (ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hfast[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hfast", i__2, "f_dasa2l__", (ftnlen)2620)];
	    dasrdd_(&handle, &i__, &i__, &dval);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (dval != (doublereal) i__) {
		d__1 = (doublereal) i__;
		chcksd_("DVAL", &dval, "=", &d__1, &c_b135, ok, (ftnlen)4, (
			ftnlen)1);
	    }
	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    i__1 = clnwds[3];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 1; k <= 10; ++k) {
	    handle = hfast[(i__2 = k - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		    "hfast", i__2, "f_dasa2l__", (ftnlen)2641)];
	    dasrdi_(&handle, &i__, &i__, &ival);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (ival != i__) {
		chcksi_("IVAL", &ival, "=", &i__, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
	    }

/*           Include a lookup in a complex, unsegregated, read-only */
/*           file. */

	    dasa2l_(&bighan, &c__3, &c__257, &clbase, &clsize, &recno, &
		    wordno);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from fast files twice; path check. Path case 1: FAST, SAME "
	    "FILE.", (ftnlen)69);
    for (i__ = 1; i__ <= 10; ++i__) {

/*        Do one read from the current file. */

	handle = hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2675)];
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("FAST FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

/*        The file associated with HANDLE is now the last one read. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from fast files in interleaved order: path check. Path case"
	    " 2: FAST, KNOWN FILE.", (ftnlen)85);

/*     Prep the P_DASA2L buffer by reading from each */
/*     fast file. */

    for (i__ = 1; i__ <= 10; ++i__) {
	handle = hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2727)];
	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {

/*        This file is not the last one read. */

	handle = hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2741)];

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("FAST FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Execute instrumented version of DASA2L. */

	t_pthnew__();
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/*     CALL T_PTHDSP ( PATH ) */
/* --- Case -------------------------------------------------------- */

    tcase_("Read from newly opened fast files; path check. Path case 3: FAST"
	    ", UNKNOWN FILE.", (ftnlen)79);
    for (i__ = 1; i__ <= 10; ++i__) {
	dascls_(&hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2787)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	dasopr_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)2793)) * 255, &hfast[(
		i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hfast", 
		i__2, "f_dasa2l__", (ftnlen)2793)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create expected path. */

	ssizec_(&c__30, exppth, (ftnlen)50);
	appndc_("DATA TYPE CHECK", exppth, (ftnlen)15, (ftnlen)50);
	appndc_("NOT SAME FILE", exppth, (ftnlen)13, (ftnlen)50);
	appndc_("UNKNOWN FILE", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER SHIFT", exppth, (ftnlen)12, (ftnlen)50);
	appndc_("BUFFER INSERTION", exppth, (ftnlen)16, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("LOOK UP ACCESS METHOD", exppth, (ftnlen)21, (ftnlen)50);
	appndc_("GET FILE SUMMARY (0)", exppth, (ftnlen)20, (ftnlen)50);
	appndc_("SEGREGATION CHECK", exppth, (ftnlen)17, (ftnlen)50);
	appndc_("SET TBFAST", exppth, (ftnlen)10, (ftnlen)50);
	appndc_("ADDRESS RANGE CHECK", exppth, (ftnlen)19, (ftnlen)50);
	appndc_("FAST FILE SET CLUSTER PARAMS", exppth, (ftnlen)28, (ftnlen)
		50);
	appndc_("CALCULATE RECORD AND WORD", exppth, (ftnlen)25, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Do one read from the current file. */

	t_pthnew__();
	handle = hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2821)];
	p_dasa2l__(&handle, &c__2, &c__1, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check actual path. */

	ssizec_(&c__30, path, (ftnlen)50);
	t_pthget__(path, (ftnlen)50);
	t_pthcmp__(path, exppth, (ftnlen)50, (ftnlen)50);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Clean up", (ftnlen)8);
    delfil_(das0, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daswbr_(&bighan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasllc_(&bighan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(das1, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 10; ++i__) {
	daswbr_(&hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2861)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dasllc_(&hwrit[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hwrit", i__1, "f_dasa2l__", (ftnlen)2864)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(wrtabl + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"wrtabl", i__1, "f_dasa2l__", (ftnlen)2867)) * 255, (ftnlen)
		255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	dascls_(&hrdnsg[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hrdnsg", i__1, "f_dasa2l__", (ftnlen)2874)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(rdnseg + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"rdnseg", i__1, "f_dasa2l__", (ftnlen)2877)) * 255, (ftnlen)
		255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 10; ++i__) {
	dascls_(&hfast[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hfast", i__1, "f_dasa2l__", (ftnlen)2884)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(fast + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"fast", i__1, "f_dasa2l__", (ftnlen)2887)) * 255, (ftnlen)255)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_dasa2l__ */

/* ********************************************************************** */

/*     P A T H   T E S T   U T I L I T I E S */

/* ********************************************************************** */

/*     Utilities for execution path checking: */

/* Subroutine */ int t_path__0_(int n__, char *str, char *path, ftnlen 
	str_len, ftnlen path_len)
{
    extern /* Subroutine */ int copyc_(char *, char *, ftnlen, ftnlen), 
	    appndc_(char *, char *, ftnlen, ftnlen), ssizec_(integer *, char *
	    , ftnlen);
    static char svpath[50*36];

    /* Parameter adjustments */
    if (path) {
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_t_pthnew;
	case 2: goto L_t_pthapp;
	case 3: goto L_t_pthget;
	}

    return 0;

/*     Initialize path. */


L_t_pthnew:
    ssizec_(&c__30, svpath, (ftnlen)50);
    return 0;

/*     Append action to path. */


L_t_pthapp:
    appndc_(str, svpath, str_len, (ftnlen)50);
    return 0;

/*     Get stored path. */


L_t_pthget:
    copyc_(svpath, path, (ftnlen)50, path_len);
    return 0;
} /* t_path__ */

/* Subroutine */ int t_path__(char *str, char *path, ftnlen str_len, ftnlen 
	path_len)
{
    return t_path__0_(0, str, path, str_len, path_len);
    }

/* Subroutine */ int t_pthnew__(void)
{
    return t_path__0_(1, (char *)0, (char *)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int t_pthapp__(char *str, ftnlen str_len)
{
    return t_path__0_(2, str, (char *)0, str_len, (ftnint)0);
    }

/* Subroutine */ int t_pthget__(char *path, ftnlen path_len)
{
    return t_path__0_(3, (char *)0, path, (ftnint)0, path_len);
    }


/*     Detect path mismatch. */

/* Subroutine */ int t_pthcmp__0_(int n__, char *path, char *exppth, ftnlen 
	path_len, ftnlen exppth_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    integer i__;
    extern integer cardc_(char *, ftnlen);
    integer n;
    extern /* Subroutine */ int chkin_(char *, ftnlen), errch_(char *, char *,
	     ftnlen, ftnlen);
    integer cp, cx, nm;
    extern /* Subroutine */ int sigerr_(char *, ftnlen), chkout_(char *, 
	    ftnlen), setmsg_(char *, ftnlen), errint_(char *, integer *, 
	    ftnlen), tostdo_(char *, ftnlen);

    /* Parameter adjustments */
    if (exppth) {
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_t_pthdsp;
	}

    chkin_("T_PTHCMP", (ftnlen)8);
    cp = cardc_(path, path_len);
    cx = cardc_(exppth, exppth_len);
    nm = min(cp,cx);
    n = 0;
    i__1 = nm;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (s_cmp(path + (i__ + 5) * path_len, exppth + (i__ + 5) * 
		exppth_len, path_len, exppth_len) == 0) {
	    ++n;
	}
    }
    if (cp != cx) {
	setmsg_("Path was expected to have length # but had length #. Last e"
		"lement of actual path was <#>. Last element common to both p"
		"aths was <#>.", (ftnlen)132);
	errint_("#", &cx, (ftnlen)1);
	errint_("#", &cp, (ftnlen)1);
	if (cp > 0) {
	    errch_("#", path + (cp + 5) * path_len, (ftnlen)1, path_len);
	} else {
	    errch_("#", "<empty>", (ftnlen)1, (ftnlen)7);
	}
	if (n > 0) {
	    errch_("#", exppth + (n + 5) * exppth_len, (ftnlen)1, exppth_len);
	} else {
	    errch_("#", "<empty>", (ftnlen)1, (ftnlen)7);
	}
	sigerr_("SPICE(BADPATHLENGTH)", (ftnlen)20);
	chkout_("T_PTHCMP", (ftnlen)8);
	return 0;
    }
    i__ = 1;
    while(i__ <= cardc_(exppth, exppth_len)) {
	if (s_cmp(path + (i__ + 5) * path_len, exppth + (i__ + 5) * 
		exppth_len, path_len, exppth_len) != 0) {
	    setmsg_("Path element # was expected to be <#> but was <#>.", (
		    ftnlen)50);
	    errint_("#", &i__, (ftnlen)1);
	    errch_("#", exppth + (i__ + 5) * exppth_len, (ftnlen)1, 
		    exppth_len);
	    errch_("#", path + (i__ + 5) * path_len, (ftnlen)1, path_len);
	    sigerr_("SPICE(BADPATH)", (ftnlen)14);
	    chkout_("T_PTHCMP", (ftnlen)8);
	    return 0;
	}
	++i__;
    }
    chkout_("T_PTHCMP", (ftnlen)8);
    return 0;

/*     Display path. */


L_t_pthdsp:
    chkin_("T_PTHDSP", (ftnlen)8);
    i__1 = cardc_(path, path_len);
    for (i__ = 1; i__ <= i__1; ++i__) {
	tostdo_(path + (i__ + 5) * path_len, path_len);
    }
    chkout_("T_PTHDSP", (ftnlen)8);
    return 0;
} /* t_pthcmp__ */

/* Subroutine */ int t_pthcmp__(char *path, char *exppth, ftnlen path_len, 
	ftnlen exppth_len)
{
    return t_pthcmp__0_(0, path, exppth, path_len, exppth_len);
    }

/* Subroutine */ int t_pthdsp__(char *path, ftnlen path_len)
{
    return t_pthcmp__0_(1, path, (char *)0, path_len, (ftnint)0);
    }

