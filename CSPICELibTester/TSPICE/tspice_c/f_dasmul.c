/* f_dasmul.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__1 = 1;
static doublereal c_b65 = 0.;
static integer c__12 = 12;

/* $Procedure F_DASMUL ( DAS test, multiple DAS files ) */
/* Subroutine */ int f_dasmul__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, j, k;
    static char cdata[12];
    static doublereal ddata[12];
    static integer idata[12];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer recno;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), t_success__(
	    logical *), chckad_(char *, doublereal *, char *, doublereal *, 
	    integer *, doublereal *, logical *, ftnlen, ftnlen), chckai_(char 
	    *, integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    static integer handle[5000];
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), delfil_(char *, 
	    ftnlen), dasrdc_(integer *, integer *, integer *, integer *, 
	    integer *, char *, ftnlen), dasrdd_(integer *, integer *, integer 
	    *, doublereal *);
    static char xcdata[12];
    static doublereal xddata[12];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     dasrdi_(integer *, integer *, integer *, integer *), dascls_(
	    integer *);
    static integer xidata[12];
    static logical fclose;
    static integer nschem, nfiles, ncomrc;
    static logical sgrgat;
    static integer clnwds[1000];
    extern /* Subroutine */ int dasopr_(char *, integer *, ftnlen), tstdas_(
	    char *, char *, integer *, integer *, integer *, integer *, 
	    integer *, logical *, logical *, integer *, integer *, ftnlen, 
	    ftnlen);
    static integer nclust, cltyps[1000];
    extern logical exists_(char *, ftnlen);
    static char das[255*5000];

/* $ Abstract */

/*     Exercise DAS routines using multiple DAS files. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the ability of the DAS system to read from */
/*     FTSIZE open DAS files, where FTSIZE is the limit imposed by */
/*     the handle manager subsystem. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     This file currently open DAS files using DASOPR. */
/*     When the required supporting code is available, the */
/*     DASOPR calls can be replaced by calls to FURNSH and */
/*     KINFO. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-FEB-2017 (NJB) */

/*        Removed unneeded declarations. */

/*        26-FEB-2015 (NJB) */

/*          Original version. */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved all. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_DASMUL", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DAS files.", (ftnlen)24);
    nfiles = 5000;
    nfiles = 5000;
    ncomrc = 2;
    nclust = 3;
    cltyps[0] = 2;
    cltyps[1] = 1;
    cltyps[2] = 3;
    clnwds[0] = 5;
    clnwds[1] = 7;
    clnwds[2] = 12;
    sgrgat = FALSE_;
    fclose = TRUE_;
    nschem = 2;
    i__1 = nfiles;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"das", i__2, "f_dasmul__", (ftnlen)228)) * 255, "mult_test_#"
		".das", (ftnlen)255, (ftnlen)15);
	repmi_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"das", i__2, "f_dasmul__", (ftnlen)229)) * 255, "#", &i__, 
		das + ((i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : s_rnge(
		"das", i__3, "f_dasmul__", (ftnlen)229)) * 255, (ftnlen)255, (
		ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : 
		s_rnge("das", i__2, "f_dasmul__", (ftnlen)232)) * 255, (
		ftnlen)255)) {
	    delfil_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : 
		    s_rnge("das", i__2, "f_dasmul__", (ftnlen)233)) * 255, (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	tstdas_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"das", i__2, "f_dasmul__", (ftnlen)237)) * 255, "DSK", &
		ncomrc, &i__, &nclust, cltyps, clnwds, &sgrgat, &fclose, &
		nschem, &handle[(i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : 
		s_rnge("handle", i__3, "f_dasmul__", (ftnlen)237)], (ftnlen)
		255, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = nfiles;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dasopr_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"das", i__2, "f_dasmul__", (ftnlen)247)) * 255, &handle[(i__3 
		= i__ - 1) < 5000 && 0 <= i__3 ? i__3 : s_rnge("handle", i__3,
		 "f_dasmul__", (ftnlen)247)], (ftnlen)255);
/*         CALL FURNSH ( DAS(I) ) */
	chckxc_(&c_false, " ", ok, (ftnlen)1);
/*         CALL KINFO ( DAS(I), FILTYP, SOURCE, HANDLE(I), FOUND ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */
/*         CALL CHCKSL ( 'FOUND', FOUND, .TRUE., OK ) */
    }

/*     Read data from open files. */

    i__1 = nfiles;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nclust;
	for (j = 1; j <= i__2; ++j) {
	    if (cltyps[(i__3 = j - 1) < 1000 && 0 <= i__3 ? i__3 : s_rnge(
		    "cltyps", i__3, "f_dasmul__", (ftnlen)265)] == 3) {
		dasrdi_(&handle[(i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : 
			s_rnge("handle", i__3, "f_dasmul__", (ftnlen)267)], &
			c__1, &clnwds[(i__4 = j - 1) < 1000 && 0 <= i__4 ? 
			i__4 : s_rnge("clnwds", i__4, "f_dasmul__", (ftnlen)
			267)], idata);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		recno = 3;
		i__4 = clnwds[(i__3 = j - 1) < 1000 && 0 <= i__3 ? i__3 : 
			s_rnge("clnwds", i__3, "f_dasmul__", (ftnlen)272)];
		for (k = 1; k <= i__4; ++k) {
		    xidata[(i__3 = k - 1) < 12 && 0 <= i__3 ? i__3 : s_rnge(
			    "xidata", i__3, "f_dasmul__", (ftnlen)273)] = i__ 
			    * 100000 + recno * 1000 + k;
		}
		chckai_("IDATA", idata, "=", xidata, &clnwds[(i__4 = j - 1) < 
			1000 && 0 <= i__4 ? i__4 : s_rnge("clnwds", i__4, 
			"f_dasmul__", (ftnlen)276)], ok, (ftnlen)5, (ftnlen)1)
			;
	    } else if (cltyps[(i__4 = j - 1) < 1000 && 0 <= i__4 ? i__4 : 
		    s_rnge("cltyps", i__4, "f_dasmul__", (ftnlen)280)] == 2) {
		dasrdd_(&handle[(i__4 = i__ - 1) < 5000 && 0 <= i__4 ? i__4 : 
			s_rnge("handle", i__4, "f_dasmul__", (ftnlen)282)], &
			c__1, &clnwds[(i__3 = j - 1) < 1000 && 0 <= i__3 ? 
			i__3 : s_rnge("clnwds", i__3, "f_dasmul__", (ftnlen)
			282)], ddata);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		recno = 1;
		i__3 = clnwds[(i__4 = j - 1) < 1000 && 0 <= i__4 ? i__4 : 
			s_rnge("clnwds", i__4, "f_dasmul__", (ftnlen)287)];
		for (k = 1; k <= i__3; ++k) {
		    xddata[(i__4 = k - 1) < 12 && 0 <= i__4 ? i__4 : s_rnge(
			    "xddata", i__4, "f_dasmul__", (ftnlen)288)] = i__ 
			    * 100000 + recno * 1000 + (doublereal) k;
		}
		chckad_("DDATA", ddata, "=", xddata, &clnwds[(i__3 = j - 1) < 
			1000 && 0 <= i__3 ? i__3 : s_rnge("clnwds", i__3, 
			"f_dasmul__", (ftnlen)291)], &c_b65, ok, (ftnlen)5, (
			ftnlen)1);
	    } else {
		dasrdc_(&handle[(i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : 
			s_rnge("handle", i__3, "f_dasmul__", (ftnlen)296)], &
			c__1, &clnwds[(i__4 = j - 1) < 1000 && 0 <= i__4 ? 
			i__4 : s_rnge("clnwds", i__4, "f_dasmul__", (ftnlen)
			296)], &c__1, &c__12, cdata, (ftnlen)12);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		recno = 2;
		i__4 = clnwds[(i__3 = j - 1) < 1000 && 0 <= i__3 ? i__3 : 
			s_rnge("clnwds", i__3, "f_dasmul__", (ftnlen)301)];
		for (k = 1; k <= i__4; ++k) {
		    xidata[(i__3 = k - 1) < 12 && 0 <= i__3 ? i__3 : s_rnge(
			    "xidata", i__3, "f_dasmul__", (ftnlen)303)] = i__ 
			    * 100000 + recno * 1000 + k;
		    *(unsigned char *)&xcdata[k - 1] = (char) (xidata[(i__3 = 
			    k - 1) < 12 && 0 <= i__3 ? i__3 : s_rnge("xidata",
			     i__3, "f_dasmul__", (ftnlen)305)] % 128);
		}
		chcksc_("CDATA", cdata, "=", xcdata, ok, (ftnlen)5, (ftnlen)
			12, (ftnlen)1, (ftnlen)12);
	    }
	}
    }
    i__1 = nfiles;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dascls_(&handle[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"handle", i__2, "f_dasmul__", (ftnlen)319)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up: delete DAS files.", (ftnlen)27);
    i__1 = nfiles;
    for (i__ = 1; i__ <= i__1; ++i__) {
	delfil_(das + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"das", i__2, "f_dasmul__", (ftnlen)332)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_dasmul__ */

