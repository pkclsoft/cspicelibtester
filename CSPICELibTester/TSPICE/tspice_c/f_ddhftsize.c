/* f_ddhftsize.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__8 = 8;
static integer c__6 = 6;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c_n999 = -999;
static integer c__2 = 2;
static doublereal c_b50 = 0.;
static logical c_true = TRUE_;
static integer c__4 = 4;
static doublereal c_b60 = 1e-14;
static integer c__12 = 12;
static integer c__3 = 3;
static integer c__13000 = 13000;

/* $Procedure F_DDHFTSIZE ( Handle Manager File Table Size ) */
/* Subroutine */ int f_ddhftsize__(logical *ok)
{
    /* System generated locals */
    address a__1[3];
    integer i__1, i__2, i__3, i__4, i__5[3];
    doublereal d__1, d__2, d__3;
    char ch__1[32];
    cllist cl__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_clos(cllist *);

    /* Local variables */
    extern /* Subroutine */ int ckgp_(integer *, doublereal *, doublereal *, 
	    char *, doublereal *, doublereal *, logical *, ftnlen), ckw03_(
	    integer *, doublereal *, doublereal *, integer *, char *, logical 
	    *, char *, integer *, doublereal *, doublereal *, doublereal *, 
	    integer *, doublereal *, ftnlen, ftnlen), t_elapsd__(logical *, 
	    char *, char *, ftnlen, ftnlen);
    integer unit;
    extern /* Subroutine */ int eul2m_(doublereal *, doublereal *, doublereal 
	    *, integer *, integer *, integer *, doublereal *);
    doublereal quat1[4], quat2[4];
    integer i__;
    char fname[32*5001];
    extern /* Subroutine */ int tcase_(char *, ftnlen), ckcls_(integer *), 
	    ucase_(char *, char *, ftnlen, ftnlen);
    char mknam[32];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), pckw02_(integer *, integer *, char *, 
	    doublereal *, doublereal *, char *, doublereal *, integer *, 
	    integer *, doublereal *, doublereal *, ftnlen, ftnlen), ckopn_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    extern integer rtrim_(char *, ftnlen);
    doublereal quats[8]	/* was [4][2] */;
    extern /* Subroutine */ int spkw09_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    char mktxt[32*5100];
    extern /* Subroutine */ int t_success__(logical *);
    doublereal state1[6], state2[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern /* Character */ VOID begdat_(char *, ftnlen);
    integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int kclear_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen);
    doublereal coeffs[6]	/* was [2][3] */;
    logical delker;
    char tcname[80];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), bodmat_(integer *, doublereal *, doublereal *);
    logical makker;
    extern /* Subroutine */ int pckcls_(integer *);
    doublereal epochs[2];
    char fnmpat[32];
    integer maxcnt, maxrld;
    extern /* Subroutine */ int spkgeo_(integer *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen);
    extern /* Character */ VOID begtxt_(char *, ftnlen);
    extern /* Subroutine */ int rotate_(doublereal *, integer *, doublereal *)
	    , spkcls_(integer *), pckopn_(char *, char *, integer *, integer *
	    , ftnlen, ftnlen);
    doublereal clkout;
    extern /* Subroutine */ int writla_(integer *, char *, integer *, ftnlen);
    doublereal states[12]	/* was [6][2] */;
    extern /* Subroutine */ int furnsh_(char *, ftnlen), spkopn_(char *, char 
	    *, integer *, integer *, ftnlen, ftnlen);
    logical report;
    extern /* Subroutine */ int tostdo_(char *, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int m2q_(doublereal *, doublereal *);
    integer nmktxt;
    extern /* Subroutine */ int txtopn_(char *, integer *, ftnlen);
    doublereal mat[9]	/* was [3][3] */, avs[6]	/* was [3][2] */;

/* $ Abstract */

/*     Maximum handle manager file table size tests. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/* $ Abstract */

/*     Parameter declarations for the DAF/DAS handle manager. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DAF, DAS */

/* $ Keywords */

/*     PRIVATE */

/* $ Particulars */

/*     This include file contains parameters defining limits and */
/*     integer codes that are utilized in the DAF/DAS handle manager */
/*     routines. */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     F.S. Turner       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.5.0, 10-MAR-2014 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-INTEL. */

/* -    SPICELIB Version 2.4.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-LINUX-64BIT-IFORT. */

/* -    SPICELIB Version 2.3.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-GFORTRAN. */

/* -    SPICELIB Version 2.2.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GFORTRAN. */

/* -    SPICELIB Version 2.1.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GCC_C. */

/* -    SPICELIB Version 2.0.0, 12-APR-2012 (BVS) */

/*        Increased FTSIZE (from 1000 to 5000). */

/* -    SPICELIB Version 1.20.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL. */

/* -    SPICELIB Version 1.19.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-CC_C. */

/* -    SPICELIB Version 1.18.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-64BIT-CC_C. */

/* -    SPICELIB Version 1.17.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-NATIVE_C. */

/* -    SPICELIB Version 1.16.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-WINDOWS-64BIT-IFORT. */

/* -    SPICELIB Version 1.15.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-LINUX-64BIT-GFORTRAN. */

/* -    SPICELIB Version 1.14.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-64BIT-MS_C. */

/* -    SPICELIB Version 1.13.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-INTEL_C. */

/* -    SPICELIB Version 1.12.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-IFORT. */

/* -    SPICELIB Version 1.11.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-GFORTRAN. */

/* -    SPICELIB Version 1.10.0, 18-MAR-2009 (BVS) */

/*        Updated for PC-LINUX-GFORTRAN. */

/* -    SPICELIB Version 1.9.0, 18-MAR-2009 (BVS) */

/*        Updated for MAC-OSX-GFORTRAN. */

/* -    SPICELIB Version 1.8.0, 19-FEB-2008 (BVS) */

/*        Updated for PC-LINUX-IFORT. */

/* -    SPICELIB Version 1.7.0, 14-NOV-2006 (BVS) */

/*        Updated for PC-LINUX-64BIT-GCC_C. */

/* -    SPICELIB Version 1.6.0, 14-NOV-2006 (BVS) */

/*        Updated for MAC-OSX-INTEL_C. */

/* -    SPICELIB Version 1.5.0, 14-NOV-2006 (BVS) */

/*        Updated for MAC-OSX-IFORT. */

/* -    SPICELIB Version 1.4.0, 14-NOV-2006 (BVS) */

/*        Updated for PC-WINDOWS-IFORT. */

/* -    SPICELIB Version 1.3.0, 26-OCT-2005 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-GCC_C. */

/* -    SPICELIB Version 1.2.0, 03-JAN-2005 (BVS) */

/*        Updated for PC-CYGWIN_C. */

/* -    SPICELIB Version 1.1.0, 03-JAN-2005 (BVS) */

/*        Updated for PC-CYGWIN. */

/* -    SPICELIB Version 1.0.1, 17-JUL-2002 */

/*        Added MAC-OSX environments. */

/* -    SPICELIB Version 1.0.0, 07-NOV-2001 */

/* -& */

/*     Unit and file table size parameters. */

/*     FTSIZE     is the maximum number of files (DAS and DAF) that a */
/*                user may have open simultaneously. */


/*     RSVUNT     is the number of units protected from being locked */
/*                to a particular handle by ZZDDHHLU. */


/*     SCRUNT     is the number of units protected for use by scratch */
/*                files. */


/*     UTSIZE     is the maximum number of logical units this manager */
/*                will utilize at one time. */


/*     Access method enumeration.  These parameters are used to */
/*     identify which access method is associated with a particular */
/*     handle.  They need to be synchronized with the STRAMH array */
/*     defined in ZZDDHGSD in the following fashion: */

/*        STRAMH ( READ   ) = 'READ' */
/*        STRAMH ( WRITE  ) = 'WRITE' */
/*        STRAMH ( SCRTCH ) = 'SCRATCH' */
/*        STRAMH ( NEW    ) = 'NEW' */

/*     These values are used in the file table variable FTAMH. */


/*     Binary file format enumeration.  These parameters are used to */
/*     identify which binary file format is associated with a */
/*     particular handle.  They need to be synchronized with the STRBFF */
/*     array defined in ZZDDHGSD in the following fashion: */

/*        STRBFF ( BIGI3E ) = 'BIG-IEEE' */
/*        STRBFF ( LTLI3E ) = 'LTL-IEEE' */
/*        STRBFF ( VAXGFL ) = 'VAX-GFLT' */
/*        STRBFF ( VAXDFL ) = 'VAX-DFLT' */

/*     These values are used in the file table variable FTBFF. */


/*     Some random string lengths... more documentation required. */
/*     For now this will have to suffice. */


/*     Architecture enumeration.  These parameters are used to identify */
/*     which file architecture is associated with a particular handle. */
/*     They need to be synchronized with the STRARC array defined in */
/*     ZZDDHGSD in the following fashion: */

/*        STRARC ( DAF ) = 'DAF' */
/*        STRARC ( DAS ) = 'DAS' */

/*     These values will be used in the file table variable FTARC. */


/*     For the following environments, record length is measured in */
/*     characters (bytes) with eight characters per double precision */
/*     number. */

/*     Environment: Sun, Sun FORTRAN */
/*     Source:      Sun Fortran Programmer's Guide */

/*     Environment: PC, MS FORTRAN */
/*     Source:      Microsoft Fortran Optimizing Compiler User's Guide */

/*     Environment: Macintosh, Language Systems FORTRAN */
/*     Source:      Language Systems FORTRAN Reference Manual, */
/*                  Version 1.2, page 12-7 */

/*     Environment: PC/Linux, g77 */
/*     Source:      Determined by experiment. */

/*     Environment: PC, Lahey F77 EM/32 Version 4.0 */
/*     Source:      Lahey F77 EM/32 Language Reference Manual, */
/*                  page 144 */

/*     Environment: HP-UX 9000/750, FORTRAN/9000 Series 700 computers */
/*     Source:      FORTRAN/9000 Reference-Series 700 Computers, */
/*                  page 5-110 */

/*     Environment: NeXT Mach OS (Black Hardware), */
/*                  Absoft Fortran Version 3.2 */
/*     Source:      NAIF Program */


/*     The following parameter defines the size of a string used */
/*     to store a filenames on this target platform. */


/*     The following parameter controls the size of the character record */
/*     buffer used to read data from non-native files. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests maximum handle manager file table size. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     B.V. Semenov     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 02-AUG-2013 (BVS) */


/* -& */

/*     Local Parameters */


/*     SPICELIB functions */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_DDHFTSIZE", (ftnlen)11);

/*     Set the number of files we will test. */

    maxcnt = 5000;

/*     Set the number of files we will load second time. */

    maxrld = min(200,maxcnt);

/*     Set flags indicating whether we try to make kernels and whether we */
/*     try delete kernels. */

    makker = TRUE_;
    delker = TRUE_;

/*     Set flag to report elapsed time (or not.) */

/*      REPORT = .TRUE. */
    report = FALSE_;
    t_elapsd__(&c_false, "no report on first call", "total", (ftnlen)23, (
	    ftnlen)5);

/* --- Case: ------------------------------------------------------ */

/*     Test MAXCNT CKs. */

/*     Make and load CKs. */

    s_copy(tcname, "Handle Manager: # CKs", (ftnlen)80, (ftnlen)21);
    repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    tcase_(tcname, (ftnlen)80);

/*     Make CK names. */

    s_copy(fnmpat, "file_ck#.bc", (ftnlen)32, (ftnlen)11);
    i__1 = maxcnt + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	repmi_(fnmpat, "#", &i__, fname + (((i__2 = i__ - 1) < 5001 && 0 <= 
		i__2 ? i__2 : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)
		212)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
    }

/*     Make CK files. */

    if (makker) {
	cleard_(&c__8, quats);
	cleard_(&c__6, avs);
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (exists_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
		    : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)225)) << 
		    5), (ftnlen)32)) {
		delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
			: s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)226))
			 << 5), (ftnlen)32);
	    }
	    ckopn_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)229)) << 5)
		    , " ", &c__0, &handle, (ftnlen)32, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    epochs[0] = (doublereal) i__ - .5;
	    epochs[1] = (doublereal) i__ + .5;
	    d__1 = i__ * .001 - 5e-4;
	    rotate_(&d__1, &c__1, mat);
	    m2q_(mat, quats);
	    d__1 = i__ * .001 + 5e-4;
	    rotate_(&d__1, &c__1, mat);
	    m2q_(mat, &quats[4]);
	    ckw03_(&handle, epochs, &epochs[1], &c_n999, "J2000", &c_false, 
		    fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)241)) << 5)
		    , &c__2, epochs, quats, avs, &c__1, epochs, (ftnlen)5, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ckcls_(&handle);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_elapsd__(&report, "Making CKs", "running", (ftnlen)10, (ftnlen)7);

/*     Load all CKs. Then load the first MAXRLD CKs again to check that */
/*     they are recongized as known and don't overflow the file table. */

    i__1 = maxcnt;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)272)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = maxrld;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)279)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elapsd__(&report, "Loading CKs", "running", (ftnlen)11, (ftnlen)7);

/*     Read CKs. Check rotations. */

    i__1 = maxcnt;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = (doublereal) i__;
	ckgp_(&c_n999, &d__1, &c_b50, "J2000", mat, &clkout, &found, (ftnlen)
		5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("CKGP FOUND", &found, &c_true, ok, (ftnlen)10);
	m2q_(mat, quat1);
	d__1 = i__ * .001;
	rotate_(&d__1, &c__1, mat);
	m2q_(mat, quat2);
	chckad_("CKGP quaternion", quat1, "~", quat2, &c__4, &c_b60, ok, (
		ftnlen)15, (ftnlen)1);
    }
    t_elapsd__(&report, "Reading CKs", "running", (ftnlen)11, (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

/*     Cause error by loading one more CK. */

    if (maxcnt == 5000) {
	s_copy(tcname, "Handle Manager: #+1 CKs", (ftnlen)80, (ftnlen)23);
	repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	tcase_(tcname, (ftnlen)80);
	i__ = maxcnt + 1;
	ckopn_(fname + (((i__1 = i__ - 1) < 5001 && 0 <= i__1 ? i__1 : s_rnge(
		"fname", i__1, "f_ddhftsize__", (ftnlen)327)) << 5), fname + (
		((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : s_rnge("fname",
		 i__2, "f_ddhftsize__", (ftnlen)327)) << 5), &c__0, &handle, (
		ftnlen)32, (ftnlen)32);
	chckxc_(&c_true, "SPICE(DAFFTFULL)", ok, (ftnlen)16);
    }

/*     Unload all CKs. Doing this one-by-one is EXTREMELY slow! */

/*      DO I = 1, MAXCNT */

/*         WRITE (*,*) 'unloading CK ', I */

/*         CALL UNLOAD ( FNAME(I) ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */

/*      END DO */
    kclear_();
    if (delker) {
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)350)) << 5)
		    , (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_elapsd__(&report, "Deleting CKs", "running", (ftnlen)12, (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

/*     Test MAXCNT SPKs. */

/*     Make and load SPKs. */

    s_copy(tcname, "Handle Manager: # SPKs", (ftnlen)80, (ftnlen)22);
    repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    tcase_(tcname, (ftnlen)80);

/*     Make SPK names. Set MK name. */

    s_copy(fnmpat, "file_spk#.bsp", (ftnlen)32, (ftnlen)13);
    i__1 = maxcnt + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	repmi_(fnmpat, "#", &i__, fname + (((i__2 = i__ - 1) < 5001 && 0 <= 
		i__2 ? i__2 : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)
		376)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
    }
    s_copy(mknam, "file_spks.tm", (ftnlen)32, (ftnlen)12);

/*     Make SPK files. */

    if (makker) {

/*        Set initial MK lines. */

	s_copy(mktxt, "KPL/MK", (ftnlen)32, (ftnlen)6);
	begdat_(ch__1, (ftnlen)32);
	s_copy(mktxt + 32, ch__1, (ftnlen)32, (ftnlen)32);
	s_copy(mktxt + 64, "PATH_SYMBOLS    = (", (ftnlen)32, (ftnlen)19);
	s_copy(mktxt + 96, "'FILE'", (ftnlen)32, (ftnlen)6);
	s_copy(mktxt + 128, "'SPK'", (ftnlen)32, (ftnlen)5);
	s_copy(mktxt + 160, "'BSP'", (ftnlen)32, (ftnlen)5);
	s_copy(mktxt + 192, ")", (ftnlen)32, (ftnlen)1);
	s_copy(mktxt + 224, "PATH_VALUES    = (", (ftnlen)32, (ftnlen)18);
	s_copy(mktxt + 256, "'file'", (ftnlen)32, (ftnlen)6);
	s_copy(mktxt + 288, "'spk'", (ftnlen)32, (ftnlen)5);
	s_copy(mktxt + 320, "'bsp'", (ftnlen)32, (ftnlen)5);
	s_copy(mktxt + 352, ")", (ftnlen)32, (ftnlen)1);
	s_copy(mktxt + 384, "KERNELS_TO_LOAD = (", (ftnlen)32, (ftnlen)19);
	nmktxt = 13;
	cleard_(&c__12, states);
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {

/*           Make next SPK. */

	    if (exists_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
		    : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)411)) << 
		    5), (ftnlen)32)) {
		delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
			: s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)412))
			 << 5), (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    spkopn_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)416)) << 5)
		    , " ", &c__0, &handle, (ftnlen)32, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    epochs[0] = (doublereal) i__ - .5;
	    epochs[1] = (doublereal) i__ + .5;
	    states[0] = epochs[0];
	    states[6] = epochs[1];
	    spkw09_(&handle, &c_n999, &c__3, "J2000", epochs, &epochs[1], 
		    fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)425)) << 5)
		    , &c__1, &c__2, states, epochs, (ftnlen)5, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    spkcls_(&handle);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Make MK lines for this SPK. */

	    ++nmktxt;
/* Writing concatenation */
	    i__5[0] = 1, a__1[0] = "'";
	    i__5[1] = rtrim_(fname + (((i__4 = i__ - 1) < 5001 && 0 <= i__4 ? 
		    i__4 : s_rnge("fname", i__4, "f_ddhftsize__", (ftnlen)446)
		    ) << 5), (ftnlen)32), a__1[1] = fname + (((i__3 = i__ - 1)
		     < 5001 && 0 <= i__3 ? i__3 : s_rnge("fname", i__3, "f_d"
		    "dhftsize__", (ftnlen)446)) << 5);
	    i__5[2] = 1, a__1[2] = "'";
	    s_cat(mktxt + (((i__2 = nmktxt - 1) < 5100 && 0 <= i__2 ? i__2 : 
		    s_rnge("mktxt", i__2, "f_ddhftsize__", (ftnlen)446)) << 5)
		    , a__1, i__5, &c__3, (ftnlen)32);
	    ucase_(mktxt + (((i__2 = nmktxt - 1) < 5100 && 0 <= i__2 ? i__2 : 
		    s_rnge("mktxt", i__2, "f_ddhftsize__", (ftnlen)447)) << 5)
		    , mktxt + (((i__3 = nmktxt - 1) < 5100 && 0 <= i__3 ? 
		    i__3 : s_rnge("mktxt", i__3, "f_ddhftsize__", (ftnlen)447)
		    ) << 5), (ftnlen)32, (ftnlen)32);
	    repmc_(mktxt + (((i__2 = nmktxt - 1) < 5100 && 0 <= i__2 ? i__2 : 
		    s_rnge("mktxt", i__2, "f_ddhftsize__", (ftnlen)448)) << 5)
		    , "F", "$F", mktxt + (((i__3 = nmktxt - 1) < 5100 && 0 <= 
		    i__3 ? i__3 : s_rnge("mktxt", i__3, "f_ddhftsize__", (
		    ftnlen)448)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)2, (
		    ftnlen)32);
	    repmc_(mktxt + (((i__2 = nmktxt - 1) < 5100 && 0 <= i__2 ? i__2 : 
		    s_rnge("mktxt", i__2, "f_ddhftsize__", (ftnlen)449)) << 5)
		    , "_", "_$", mktxt + (((i__3 = nmktxt - 1) < 5100 && 0 <= 
		    i__3 ? i__3 : s_rnge("mktxt", i__3, "f_ddhftsize__", (
		    ftnlen)449)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)2, (
		    ftnlen)32);
	    repmc_(mktxt + (((i__2 = nmktxt - 1) < 5100 && 0 <= i__2 ? i__2 : 
		    s_rnge("mktxt", i__2, "f_ddhftsize__", (ftnlen)450)) << 5)
		    , ".", ".$", mktxt + (((i__3 = nmktxt - 1) < 5100 && 0 <= 
		    i__3 ? i__3 : s_rnge("mktxt", i__3, "f_ddhftsize__", (
		    ftnlen)450)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)2, (
		    ftnlen)32);
	}

/*        Make finishing MK lines and write MK. */

	s_copy(mktxt + (((i__1 = nmktxt) < 5100 && 0 <= i__1 ? i__1 : s_rnge(
		"mktxt", i__1, "f_ddhftsize__", (ftnlen)457)) << 5), ")", (
		ftnlen)32, (ftnlen)1);
	begtxt_(ch__1, (ftnlen)32);
	s_copy(mktxt + (((i__1 = nmktxt + 1) < 5100 && 0 <= i__1 ? i__1 : 
		s_rnge("mktxt", i__1, "f_ddhftsize__", (ftnlen)458)) << 5), 
		ch__1, (ftnlen)32, (ftnlen)32);
	s_copy(mktxt + (((i__1 = nmktxt + 2) < 5100 && 0 <= i__1 ? i__1 : 
		s_rnge("mktxt", i__1, "f_ddhftsize__", (ftnlen)459)) << 5), 
		"End of MK file.", (ftnlen)32, (ftnlen)15);
	nmktxt += 3;
	if (exists_(mknam, (ftnlen)32)) {
	    delfil_(mknam, (ftnlen)32);
	}
	txtopn_(mknam, &unit, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	writla_(&nmktxt, mktxt, &unit, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	cl__1.cerr = 0;
	cl__1.cunit = unit;
	cl__1.csta = 0;
	f_clos(&cl__1);
    }
    t_elapsd__(&report, "Making SPKs", "running", (ftnlen)11, (ftnlen)7);

/*     Load all SPKs. Then load the first MAXRLD SPKs again to check that */
/*     they are recongized as known and don't overflow the file table. */

    furnsh_(mknam, (ftnlen)32);
/*      DO I = 1, MAXCNT */

/*         CALL FURNSH ( FNAME(I) ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */

/*      END DO */
    i__1 = maxrld;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)493)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elapsd__(&report, "Loading SPKs", "running", (ftnlen)12, (ftnlen)7);

/*     Read SPKs. Check states. */

    cleard_(&c__6, state2);
    i__1 = maxcnt;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = (doublereal) i__;
	spkgeo_(&c_n999, &d__1, "J2000", &c__3, state1, &lt, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	state2[0] = (doublereal) i__;
	chckad_("SPKGEO state", state1, "~", state2, &c__6, &c_b60, ok, (
		ftnlen)12, (ftnlen)1);
    }
    t_elapsd__(&report, "Reading SPKs", "running", (ftnlen)12, (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

/*     Cause error by trying to make one more SPK. */

    if (maxcnt == 5000) {
	s_copy(tcname, "Handle Manager: #+1 SPKs", (ftnlen)80, (ftnlen)24);
	repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	tcase_(tcname, (ftnlen)80);
	i__ = maxcnt + 1;
	spkopn_(fname + (((i__1 = i__ - 1) < 5001 && 0 <= i__1 ? i__1 : 
		s_rnge("fname", i__1, "f_ddhftsize__", (ftnlen)537)) << 5), 
		fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)537)) << 5), &
		c__0, &handle, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_true, "SPICE(DAFFTFULL)", ok, (ftnlen)16);
    }

/*     Unload all SPKs. Doing this one-by-one is EXTREMELY slow! */

/*      DO I = 1, MAXCNT */

/*         WRITE (*,*) 'unloading SPK ', I */

/*         CALL UNLOAD ( FNAME(I) ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */

/*      END DO */
    kclear_();
    if (delker) {
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)560)) << 5)
		    , (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	delfil_(mknam, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elapsd__(&report, "Deleting SPKs", "running", (ftnlen)13, (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

/*     Test MAXCNT PCKs. */

/*     Make and load PCKs. */

    s_copy(tcname, "Handle Manager: # PCKs", (ftnlen)80, (ftnlen)22);
    repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    tcase_(tcname, (ftnlen)80);

/*     Make PCK names. */

    s_copy(fnmpat, "file_pck#.bpc", (ftnlen)32, (ftnlen)13);
    i__1 = maxcnt + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	repmi_(fnmpat, "#", &i__, fname + (((i__2 = i__ - 1) < 5001 && 0 <= 
		i__2 ? i__2 : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)
		588)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
    }

/*     Make PCK files. */

    if (makker) {
	cleard_(&c__8, quats);
	cleard_(&c__6, avs);
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (exists_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
		    : s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)601)) << 
		    5), (ftnlen)32)) {
		delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 
			: s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)602))
			 << 5), (ftnlen)32);
	    }
	    pckopn_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)605)) << 5)
		    , " ", &c__0, &handle, (ftnlen)32, (ftnlen)1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    epochs[0] = (doublereal) i__ - .5;
	    epochs[1] = (doublereal) i__ + .5;
	    coeffs[0] = (doublereal) i__ * .001;
	    coeffs[1] = 0.;
	    coeffs[2] = (doublereal) i__ * .002;
	    coeffs[3] = 0.;
	    coeffs[4] = (doublereal) i__ * .003;
	    coeffs[5] = 0.;
	    d__1 = epochs[1] - epochs[0];
	    pckw02_(&handle, &c__13000, "J2000", epochs, &epochs[1], fname + (
		    ((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : s_rnge(
		    "fname", i__2, "f_ddhftsize__", (ftnlen)620)) << 5), &
		    d__1, &c__1, &c__1, coeffs, epochs, (ftnlen)5, (ftnlen)32)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    pckcls_(&handle);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_elapsd__(&report, "Making PCKs", "running", (ftnlen)11, (ftnlen)7);

/*     Load all PCKs. Then load the first MAXRLD PCKs again to check that */
/*     they are recongized as known and don't overflow the file table. */

    i__1 = maxcnt;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)649)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = maxrld;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)656)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elapsd__(&report, "Loading PCKs", "running", (ftnlen)12, (ftnlen)7);

/*     Read PCKs. Check rotations. */

    i__1 = maxcnt;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = (doublereal) i__;
	bodmat_(&c__13000, &d__1, mat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("BODMAT FOUND", &found, &c_true, ok, (ftnlen)12);
	m2q_(mat, quat1);
	d__1 = (doublereal) i__ * .003;
	d__2 = (doublereal) i__ * .002;
	d__3 = (doublereal) i__ * .001;
	eul2m_(&d__1, &d__2, &d__3, &c__3, &c__1, &c__3, mat);
	m2q_(mat, quat2);
	chckad_("BODMAT quaternion", quat1, "~", quat2, &c__4, &c_b60, ok, (
		ftnlen)17, (ftnlen)1);
    }
    t_elapsd__(&report, "Reading PCKs", "running", (ftnlen)12, (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

/*     Cause error by loading one more PCK. */

    if (maxcnt == 5000) {
	s_copy(tcname, "Handle Manager: #+1 PCKs", (ftnlen)80, (ftnlen)24);
	repmi_(tcname, "#", &maxcnt, tcname, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	tcase_(tcname, (ftnlen)80);
	i__ = maxcnt + 1;
	pckopn_(fname + (((i__1 = i__ - 1) < 5001 && 0 <= i__1 ? i__1 : 
		s_rnge("fname", i__1, "f_ddhftsize__", (ftnlen)705)) << 5), 
		fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)705)) << 5), &
		c__0, &handle, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_true, "SPICE(DAFFTFULL)", ok, (ftnlen)16);
    }

/*     Unload all PCKs. Doing this one-by-one is EXTREMELY slow! */

/*      DO I = 1, MAXCNT */

/*         WRITE (*,*) 'unloading PCK ', I */

/*         CALL UNLOAD ( FNAME(I) ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */

/*      END DO */
    kclear_();
    if (delker) {
	i__1 = maxcnt;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    delfil_(fname + (((i__2 = i__ - 1) < 5001 && 0 <= i__2 ? i__2 : 
		    s_rnge("fname", i__2, "f_ddhftsize__", (ftnlen)728)) << 5)
		    , (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_elapsd__(&report, "Deleting PCKs", "running", (ftnlen)13, (ftnlen)7);

/*     Close out the test family. */

    t_elapsd__(&report, "Total", "TOTAL", (ftnlen)5, (ftnlen)5);
    tostdo_(" ", (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_ddhftsize__ */

