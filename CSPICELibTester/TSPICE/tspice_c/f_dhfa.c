/* f_dhfa.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = -199999999.;
static doublereal c_b5 = 0.;
static logical c_true = TRUE_;
static doublereal c_b9 = 1e3;
static logical c_false = FALSE_;
static doublereal c_b23 = 1e-12;

/* $Procedure F_DHFA ( DHFA, DVSEP family tests ) */
/* Subroutine */ int f_dhfa__(logical *ok)
{
    /* Initialized data */

    static doublereal zero6[6] = { 0.,0.,0.,0.,0.,0. };
    static doublereal one6[6] = { 1.,1.,1.,1.,1.,1. };

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    extern doublereal dhfa_(doublereal *, doublereal *);
    static integer seed;
    static doublereal adot, r__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal bodyr, state[6];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal val;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);

/* $ Abstract */

/*     This routine tests the SPICELIB routines */

/*        ZZGFDHFA */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.1 06-JUL-2009 (EDW) */

/*        Rename of the ZZDHA call to DHFA. */

/* -    TSPICE Version 1.0.0, 15-OCT-2008 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Begin every test family with an open call. */

    topen_("F_DHFA", (ftnlen)6);

/*     Case 1 */

    tcase_("Non-positive body radius.", (ftnlen)25);
    seed = -1804030;
    bodyr = t_randd__(&c_b4, &c_b5, &seed);
    val = dhfa_(one6, &bodyr);
    chckxc_(&c_true, "SPICE(BADRADIUS)", ok, (ftnlen)16);

/*     Case 2 */

    tcase_("Position vector equals zero vector.", (ftnlen)35);
    val = dhfa_(zero6, &c_b9);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Case 3 */

    tcase_("Range less-than body radius.", (ftnlen)28);
    val = dhfa_(one6, &c_b9);
    chckxc_(&c_true, "SPICE(BADGEOMETRY)", ok, (ftnlen)18);

/*     Case 4 */

/*     Circular orbits; constant half angle. Physically */
/*     realistic orbits not required, only geometry with */
/*     -   - */
/*     R . V = 0 */

    tcase_("DHFA Circular orbits.", (ftnlen)21);
/*         . */
/*     x * x = 1 */
/*         . */
/*     y * y = -1/2 */
/*         . */
/*     z * z = -1/2 */

    state[0] = 1e8;
    state[1] = 1.1e12;
    state[2] = -2.3e10;
    state[3] = 1e-8;
    state[4] = -5.4999999999999998e-13;
    state[5] = 1.15e10;
    val = dhfa_(state, &c_b9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DHFA Circular", &val, "~", &c_b5, &c_b23, ok, (ftnlen)13, (
	    ftnlen)1);

/*     Case 5 */

/*     Linear orbits. */
/*     Physically realistic orbits not required, only geometry with */
/*     -   - */
/*     R . V = 1 */

    tcase_("DHFA Linear orbits.", (ftnlen)19);
/*         . */
/*     x * x = 1/3 */
/*         . */
/*     y * y = 1/3 */
/*         . */
/*     z * z = 1/3 */

    state[0] = 1e4;
    state[1] = 1.1e8;
    state[2] = -2.3e6;
    state[3] = -3.6666666666666665;
    state[4] = -7.6333333333333329;
    state[5] = -6.7666666666666666;

/*     Reduce the function for the half angle derivative given */
/*     -   - */
/*     R . V = 1 */

/*     to the form */

/*     d              - body_radius                 1 */
/*     --(ALPHA) =  --------------------- *      ------ */
/*     dt                  2             2  1/2       2 */
/*                   (range - body_radius )      range */

    r__ = vnorm_(state);
/* Computing 2nd power */
    d__1 = r__;
    adot = -1e3 / sqrt(d__1 * d__1 - 1e6);
/* Computing 2nd power */
    d__1 = r__;
    adot /= d__1 * d__1;
    val = dhfa_(state, &c_b9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DHFA Linear", &val, "~", &adot, &c_b23, ok, (ftnlen)11, (ftnlen)
	    1);
    t_success__(ok);
    return 0;
} /* f_dhfa__ */

