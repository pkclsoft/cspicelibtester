/* f_dla.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c_b62 = 1000000;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c_n1 = -1;
static integer c__3 = 3;
static integer c__80 = 80;
static integer c_n999 = -999;
static integer c__8 = 8;
static doublereal c_b245 = 0.;

/* $Procedure      F_DLA ( Test DLA routines ) */
/* Subroutine */ int f_dla__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer nseg, xdsc[8], xbwd, xfwd, i__, j, ncadd;
    static char cdata[80*100];
    static doublereal ddata[100];
    static char label[80];
    static integer idata[100];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), movei_(integer *, integer *, integer *), 
	    topen_(char *, ftnlen), t_success__(logical *), chckac_(char *, 
	    char *, char *, char *, integer *, logical *, ftnlen, ftnlen, 
	    ftnlen, ftnlen), chckad_(char *, doublereal *, char *, doublereal 
	    *, integer *, doublereal *, logical *, ftnlen, ftnlen), dasadc_(
	    integer *, integer *, integer *, integer *, char *, ftnlen), 
	    dasadd_(integer *, integer *, doublereal *), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), dasadi_(integer *, integer *, integer *), dlabbs_(
	    integer *, integer *, logical *);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *), 
	    delfil_(char *, ftnlen), cleari_(integer *, integer *), dasrdd_(
	    integer *, integer *, integer *, doublereal *), chckxc_(logical *,
	     char *, logical *, ftnlen);
    static char xdatac[80*100];
    static doublereal xdatad[100];
    static integer xbasec, ncomch, xbased, xdatai[100], xbasei;
    extern logical dlassg_(integer *, integer *, integer *, integer *);
    extern /* Subroutine */ int dlaopn_(char *, char *, char *, integer *, 
	    integer *, ftnlen, ftnlen, ftnlen);
    static logical issame;
    extern /* Subroutine */ int dascls_(integer *), dasopr_(char *, integer *,
	     ftnlen);
    static integer nfound;
    extern /* Subroutine */ int dasrdi_(integer *, integer *, integer *, 
	    integer *), chcksi_(char *, integer *, char *, integer *, integer 
	    *, logical *, ftnlen, ftnlen), dlabns_(integer *), dlaens_(
	    integer *), dlafns_(integer *, integer *, integer *, logical *);
    static integer nxtdsc[8], prvdsc[8], xsizec, xsized, xsizei;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int dasrdc_(integer *, integer *, integer *, 
	    integer *, integer *, char *, ftnlen), dlafps_(integer *, integer 
	    *, integer *, logical *);
    static integer han0, han1;

/* $ Abstract */

/*     Test the SPICELIB DLA routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file das.inc */

/*     This include file declares public parameters for the DAS */
/*     subsystem. */

/*        Version 1.0.0 10-FEB-2017 (NJB) */

/*     Parameter declarations follow. */


/*     DAS file table size: */

/*        The parameter name is FTSIZE. The value of the parameter is */
/*        defined in the include file */

/*           zzddhman.inc */

/*        That value is duplicated here, since zzddhman.inc contains */
/*        other declarations that conflict with some of those in DAS */
/*        routines. */


/*     Capacity of DAS data records: */

/*        -- NWD double precision numbers. */
/*        -- NWI integers. */
/*        -- NWC characters. */
/*     These parameters are named to enhance ease of maintenance of */
/*     the code; the values should not be changed. */

/*     End of include file das.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DLA routines */

/*        DLABBS */
/*        DLABFS */
/*        DLABNS */
/*        DLAENS */
/*        DLAFNS */
/*        DLAFPS */
/*        DLAOPN */
/*        DLASSG */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 06-JAN-2017 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Save all variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_DLA", (ftnlen)5);
/* ********************************************************************** */

/*     DLAOPN tests */

/* ********************************************************************** */

/*     DLAOPN error cases: */


/* --- Case -------------------------------------------------------- */

    tcase_("Blank DLA file name", (ftnlen)19);
    if (exists_("dlatest0.dla", (ftnlen)12)) {
	delfil_("dlatest0.dla", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ncomch = 0;
    dlaopn_(" ", "DSK", "dlatest0.dla", &ncomch, &handle, (ftnlen)1, (ftnlen)
	    3, (ftnlen)12);
    chckxc_(&c_true, "SPICE(BLANKFILENAME)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("File exists", (ftnlen)11);
    ncomch = 0;
    dlaopn_("dlatest0.dla", "DSK", "dlatest0.dla", &ncomch, &handle, (ftnlen)
	    12, (ftnlen)3, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlaopn_("dlatest0.dla", "DSK", "dlatest0.dla", &ncomch, &han1, (ftnlen)12,
	     (ftnlen)3, (ftnlen)12);
    chckxc_(&c_true, "SPICE(FILEOPENFAIL)", ok, (ftnlen)19);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dlatest0.dla", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Blank file type", (ftnlen)15);
    ncomch = 0;
    dlaopn_("dlatest0.dla", " ", "dlatest0.dla", &ncomch, &handle, (ftnlen)12,
	     (ftnlen)1, (ftnlen)12);
    chckxc_(&c_true, "SPICE(BLANKFILETYPE)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("Non-printing character in file type", (ftnlen)35);
    ncomch = 0;
    dlaopn_("dlatest0.dla", "\000", "dlatest0.dla", &ncomch, &handle, (ftnlen)
	    12, (ftnlen)1, (ftnlen)12);
    chckxc_(&c_true, "SPICE(ILLEGALCHARACTER)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad comment record count", (ftnlen)24);
    ncomch = -1;
    dlaopn_("dlatest0.dla", "DSK", "dlatest0.dla", &ncomch, &handle, (ftnlen)
	    12, (ftnlen)3, (ftnlen)12);
    chckxc_(&c_true, "SPICE(BADRECORDCOUNT)", ok, (ftnlen)21);

/*     DLAOPN normal cases: */


/* --- Case -------------------------------------------------------- */

    tcase_("Create DLA file without data", (ftnlen)28);
    ncomch = 0;
    dlaopn_("dlatest0.dla", "DSK", "dlatest0.dla", &ncomch, &han0, (ftnlen)12,
	     (ftnlen)3, (ftnlen)12);

/*     Close file and re-open for read access. */

    dascls_(&han0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasopr_("dlatest0.dla", &han0, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check DLA version. */

    dasrdi_(&han0, &c__1, &c__1, idata);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("DLA version", idata, "=", &c_b62, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/*     Check forward and backward pointers. */

    dasrdi_(&han0, &c__2, &c__2, idata);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("Begin ptr", idata, "=", &c_n1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    dasrdi_(&han0, &c__3, &c__3, idata);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("End ptr", idata, "=", &c_n1, &c__0, ok, (ftnlen)7, (ftnlen)1);

/*     Leave file open for read access for further tests. */


/* --- Case -------------------------------------------------------- */

    tcase_("Create DLA file with 10 segments.", (ftnlen)33);
    if (exists_("dlatest1.dla", (ftnlen)12)) {
	delfil_("dlatest1.dla", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Add space for comment characters, just to make sure this */
/*     doesn't cause problems. */

    ncomch = 4096;
    dlaopn_("dlatest1.dla", "DSK", "dlatest1.dla", &ncomch, &han1, (ftnlen)12,
	     (ftnlen)3, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nseg = 10;
    i__1 = nseg;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dlabns_(&han1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = 100 - i__;
	for (j = 1; j <= i__2; ++j) {
	    idata[(i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge("idata", 
		    i__3, "f_dla__", (ftnlen)358)] = i__ * 1000 + j;
	}
	i__2 = 100 - i__;
	for (j = 1; j <= i__2; ++j) {
	    ddata[(i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge("ddata", 
		    i__3, "f_dla__", (ftnlen)364)] = (doublereal) (i__ * 1000 
		    + j);
	}
	i__2 = 100 - i__;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(cdata + ((i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		    "cdata", i__3, "f_dla__", (ftnlen)370)) * 80, "Segment #"
		    " line #.", (ftnlen)80, (ftnlen)17);
	    repmi_(cdata + ((i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		    "cdata", i__3, "f_dla__", (ftnlen)372)) * 80, "#", &i__, 
		    cdata + ((i__4 = j - 1) < 100 && 0 <= i__4 ? i__4 : 
		    s_rnge("cdata", i__4, "f_dla__", (ftnlen)372)) * 80, (
		    ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(cdata + ((i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		    "cdata", i__3, "f_dla__", (ftnlen)375)) * 80, "#", &j, 
		    cdata + ((i__4 = j - 1) < 100 && 0 <= i__4 ? i__4 : 
		    s_rnge("cdata", i__4, "f_dla__", (ftnlen)375)) * 80, (
		    ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(cdata + (((i__3 = j - 1) < 100 && 0 <= i__3 ? i__3 : 
		    s_rnge("cdata", i__3, "f_dla__", (ftnlen)378)) * 80 + 77),
		     "-->", (ftnlen)3, (ftnlen)3);
	}
	i__2 = 100 - i__;
	dasadi_(&han1, &i__2, idata);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = 100 - i__;
	dasadd_(&han1, &i__2, ddata);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ncadd = (100 - i__) * 80;
	dasadc_(&han1, &ncadd, &c__1, &c__80, cdata, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlaens_(&han1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close file and re-open for read access. */

    dascls_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasopr_("dlatest1.dla", &han1, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     DLABFS/DLAFNS tests */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DLABFS: bad handle", (ftnlen)18);
    dlabfs_(&c_n999, dladsc, &found);
    chckxc_(&c_true, "SPICE(INVALIDHANDLE)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("DLAFNS: bad handle", (ftnlen)18);
    dlafns_(&c_n999, dladsc, nxtdsc, &found);
    chckxc_(&c_true, "SPICE(INVALIDHANDLE)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("DLABFS: Start forward search on empty list", (ftnlen)42);

/*     DLA0 is assumed to be open at this point. */

    cleari_(&c__8, dladsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__8, xdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&han0, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    chckai_("DLADSC", dladsc, "=", xdsc, &c__8, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DLABFS/DLAFNS: perform forward search on 10-segment DLA.", (
	    ftnlen)56);

/*     DLA1 is assumed to be open at this point. */

    cleari_(&c__8, prvdsc);
    nfound = 0;
    dlabfs_(&han1, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    xbasei = 3;
    xbasec = 0;
    xbased = 0;
    while(found) {
	++nfound;

/*        Check out the descriptor components. */

/*        Base addresses precede the first address in use by */
/*        a DLA segment. */

/*        The integer base is shifted by the amount of integer */
/*        data in the previous segment, plus the size of a DLA */
/*        descriptor, since DLA descriptors reside in DAS integer */
/*        address space. */

	xbasei = xbasei + 8 + prvdsc[3];
	s_copy(label, "IBASE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[2], "=", &xbasei, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Set the expected size based on the formula used */
/*        for constructing the file. */

	xsizei = 100 - nfound;
	s_copy(label, "ISIZE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[3], "=", &xsizei, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check d.p. base and size. */

	xbased += prvdsc[5];
	s_copy(label, "DBASE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[4], "=", &xbased, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Set the expected size based on the formula used */
/*        for constructing the file. */

	xsized = 100 - nfound;
	s_copy(label, "DSIZE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[5], "=", &xsized, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check character base and size. */

	xbasec += prvdsc[7];
	s_copy(label, "CBASE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[6], "=", &xbasec, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Set the expected size based on the formula used */
/*        for constructing the file. */

	xsizec = (100 - nfound) * 80;
	s_copy(label, "CSIZE segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[7], "=", &xsizec, &c__0, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check the forward pointer. The pointer should contain */
/*        the DAS integer address of the first element of */
/*        the next descriptor, or NULPTR if the current segment */
/*        is the last. */

	if (nfound < nseg) {
	    xfwd = dladsc[2] + dladsc[3] + 1;
	} else {
	    xfwd = -1;
	}
	s_copy(label, "FWD PTR segment #", (ftnlen)80, (ftnlen)17);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &dladsc[1], "=", &xfwd, &c__0, ok, (ftnlen)80, (ftnlen)
		1);

/*        Check the backward pointer. The pointer should contain */
/*        the DAS integer address of the first element of */
/*        the previous descriptor, or NULPTR if the current segment */
/*        is the first. */

	if (nfound > 1) {
	    xbwd = prvdsc[2] - 7;
	} else {
	    xbwd = -1;
	}
	s_copy(label, "FWD PTR segment #", (ftnlen)80, (ftnlen)17);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, dladsc, "=", &xbwd, &c__0, ok, (ftnlen)80, (ftnlen)1);

/*        Prepare to check data. Fill buffers with expected */
/*        values. */

	i__1 = 100 - nfound;
	for (j = 1; j <= i__1; ++j) {
	    xdatai[(i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("xdatai",
		     i__2, "f_dla__", (ftnlen)619)] = nfound * 1000 + j;
	}
	i__1 = 100 - nfound;
	for (j = 1; j <= i__1; ++j) {
	    xdatad[(i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("xdatad",
		     i__2, "f_dla__", (ftnlen)625)] = (doublereal) (nfound * 
		    1000 + j);
	}
	i__1 = 100 - nfound;
	for (j = 1; j <= i__1; ++j) {
	    s_copy(xdatac + ((i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : 
		    s_rnge("xdatac", i__2, "f_dla__", (ftnlen)631)) * 80, 
		    "Segment # line #.", (ftnlen)80, (ftnlen)17);
	    repmi_(xdatac + ((i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : 
		    s_rnge("xdatac", i__2, "f_dla__", (ftnlen)633)) * 80, 
		    "#", &nfound, xdatac + ((i__3 = j - 1) < 100 && 0 <= i__3 
		    ? i__3 : s_rnge("xdatac", i__3, "f_dla__", (ftnlen)633)) *
		     80, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(xdatac + ((i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : 
		    s_rnge("xdatac", i__2, "f_dla__", (ftnlen)636)) * 80, 
		    "#", &j, xdatac + ((i__3 = j - 1) < 100 && 0 <= i__3 ? 
		    i__3 : s_rnge("xdatac", i__3, "f_dla__", (ftnlen)636)) * 
		    80, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(xdatac + (((i__2 = j - 1) < 100 && 0 <= i__2 ? i__2 : 
		    s_rnge("xdatac", i__2, "f_dla__", (ftnlen)639)) * 80 + 77)
		    , "-->", (ftnlen)3, (ftnlen)3);
	}

/*        Check integer data. */

	i__1 = xbasei + 1;
	i__2 = xbasei + xsizei;
	dasrdi_(&han1, &i__1, &i__2, idata);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "IDATA segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, idata, "=", xdatai, &xsizei, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check d.p. data. */

	i__1 = xbased + 1;
	i__2 = xbased + xsized;
	dasrdd_(&han1, &i__1, &i__2, ddata);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "DDATA segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, ddata, "=", xdatad, &xsized, &c_b245, ok, (ftnlen)80, (
		ftnlen)1);

/*        Check character data. */

	i__1 = xbasec + 1;
	i__2 = xbasec + xsizec;
	dasrdc_(&han1, &i__1, &i__2, &c__1, &c__80, cdata, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "CDATA segment #", (ftnlen)80, (ftnlen)15);
	repmi_(label, "#", &nfound, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        For this check we must convert the character count to */
/*        an array element count. */

	i__1 = xsizec / 80;
	chckac_(label, cdata, "=", xdatac, &i__1, ok, (ftnlen)80, (ftnlen)80, 
		(ftnlen)1, (ftnlen)80);

/*        Fetch next segment. */

	movei_(dladsc, &c__8, prvdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlafns_(&han1, prvdsc, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (nfound > nseg) {

/*           Escape from loop if necessary. */

	    found = FALSE_;
	}
    }
    chcksi_("NFOUND", &nfound, "=", &nseg, &c__0, ok, (ftnlen)6, (ftnlen)1);
/* ********************************************************************** */

/*     DLABBS/DLAFPS tests */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DLABBS: bad handle", (ftnlen)18);
    dlabbs_(&c_n999, dladsc, &found);
    chckxc_(&c_true, "SPICE(INVALIDHANDLE)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("DLAFPS: bad handle", (ftnlen)18);
    dlafps_(&c_n999, dladsc, prvdsc, &found);
    chckxc_(&c_true, "SPICE(INVALIDHANDLE)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("DLABBS: Start backward search on empty list", (ftnlen)43);
    cleari_(&c__8, dladsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__8, xdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabbs_(&han0, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    chckai_("DLADSC", dladsc, "=", xdsc, &c__8, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DLABBS/DLAFPS: perform backward search on 10-segment DLA.", (
	    ftnlen)57);

/*     DLA1 is assumed to be open at this point. */

    nfound = 0;
    dlabbs_(&han1, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     We're going to assume at this point that the forward */
/*     search routines are working. We'll use them to produce */
/*     the expected descriptors. */

    i__1 = nseg;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Produce the expected descriptor for the Ith segment. */

	dlabfs_(&han1, xdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = i__ - 1;
	for (j = 1; j <= i__2; ++j) {
	    movei_(xdsc, &c__8, dladsc);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dlafns_(&han1, dladsc, xdsc, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Do a backward search to locate the Ith segment. */

	dlabbs_(&han1, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	i__2 = nseg - i__;
	for (j = 1; j <= i__2; ++j) {
	    dlafps_(&han1, dladsc, prvdsc, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, dladsc);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compare descriptors. */

	s_copy(label, "DLADSC segment #", (ftnlen)80, (ftnlen)16);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, dladsc, "=", xdsc, &c__8, ok, (ftnlen)80, (ftnlen)1);
    }
/* ********************************************************************** */

/*     DLASSG tests */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DLASSG: compare DLA descriptors in 10-segment DLA file.", (ftnlen)
	    55);
    dlabfs_(&han1, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    for (i__ = 2; i__ <= 10; ++i__) {
	dlafns_(&han1, dladsc, nxtdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	issame = dlassg_(&han1, &han1, dladsc, nxtdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("ISSAME", &issame, &c_false, ok, (ftnlen)6);
	movei_(nxtdsc, &c__8, dladsc);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DLASSG: compare matching descriptors and handles.", (ftnlen)49);
    issame = dlassg_(&han1, &han1, dladsc, dladsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISSAME", &issame, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("DLASSG: compare matching descriptors and different handles.", (
	    ftnlen)59);
    i__1 = han1 + 1;
    issame = dlassg_(&han1, &i__1, dladsc, dladsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISSAME", &issame, &c_false, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("DLASSG: compare matching handles and different descriptors.", (
	    ftnlen)59);

/*     Make sure that a difference in any descriptor element is */
/*     detected. */

    movei_(dladsc, &c__8, nxtdsc);
    for (i__ = 1; i__ <= 8; ++i__) {

/*        Modify the Ith element of NXTDSC. */

	nxtdsc[(i__1 = i__ - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge("nxtdsc", 
		i__1, "f_dla__", (ftnlen)897)] = -nxtdsc[(i__2 = i__ - 1) < 8 
		&& 0 <= i__2 ? i__2 : s_rnge("nxtdsc", i__2, "f_dla__", (
		ftnlen)897)];
	issame = dlassg_(&han1, &han1, dladsc, nxtdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("ISSAME", &issame, &c_false, ok, (ftnlen)6);

/*        Restore original element value. */

	nxtdsc[(i__1 = i__ - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge("nxtdsc", 
		i__1, "f_dla__", (ftnlen)906)] = -nxtdsc[(i__2 = i__ - 1) < 8 
		&& 0 <= i__2 ? i__2 : s_rnge("nxtdsc", i__2, "f_dla__", (
		ftnlen)906)];
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Clean up DLA files", (ftnlen)18);
    dascls_(&han0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("dlatest0.dla", (ftnlen)12)) {
	delfil_("dlatest0.dla", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dascls_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("dlatest1.dla", (ftnlen)12)) {
	delfil_("dlatest1.dla", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_dla__ */

