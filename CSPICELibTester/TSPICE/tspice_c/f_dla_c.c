/*

-Procedure f_dla_c ( DLA tests )

 
-Abstract
 
   Exercise the CSPICE wrappers for the DLA routines.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "SpiceZst.h"
   #include "tutils_c.h"
   

   void f_dla_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrappers

      dlabbs_c
      dlabfs_c
      dlafns_c
      dlafps_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)
   
       Declared data arrays "static" to avoid stack problems.

    Original version 19-AUG-2016 (NJB)

-Index_Entries

   test plate utility routines

-&
*/

{ /* Begin f_plt_c */

 
   /*
   Constants
   */

   #define DLA0            "dlatest0.dla"
   #define DLA1            "dlatest1.dla"

   #define CBUFLN           81
   #define CBUFSZ          100
   #define DBUFSZ          100
   #define IBUFSZ          100
   #define LNSIZE          256
   #define NULPTR           -1

   /*
   Local variables
   */
   SpiceDLADescr           dladsc;
   SpiceDLADescr           nxtdsc;
   SpiceDLADescr           prvdsc;

   SpiceBoolean            found;

   static SpiceChar        cdata  [ CBUFSZ ][ CBUFLN ];
   SpiceChar             * fCvalsArr;
   static SpiceChar        label  [ LNSIZE ];
   static SpiceChar        xdatac [ CBUFSZ ][ CBUFLN ];

   static SpiceDouble      ddata  [ DBUFSZ ];
   static SpiceDouble      xdatad [ DBUFSZ ];

   SpiceInt                b;
   SpiceInt                baddr;
   SpiceInt                e;
   SpiceInt                eaddr;
   SpiceInt                fCvalsLen;
   SpiceInt                han1;
   SpiceInt                i;
   static SpiceInt         idata  [ IBUFSZ ];
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                nc;
   SpiceInt                ncadd;
   SpiceInt                ncomch;
   SpiceInt                nfound;
   SpiceInt                nseg;
   SpiceInt                nstr;
   SpiceInt                nw;
   SpiceInt                xbasec;
   SpiceInt                xbased;
   SpiceInt                xbasei;
   SpiceInt                xbwd;
   static SpiceInt         xdatai [ IBUFSZ ];
   SpiceInt                xfwd;
   SpiceInt                xsizec;
   SpiceInt                xsized;
   SpiceInt                xsizei;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dla_c" );
   


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: Create DLA file with 10 segments." );

   /*
   Note that we can't use a DSK for these tests, since DSKs don't contain
   character data. 
   */ 

   if ( exists_c(DLA1) ) 
   {
      removeFile( DLA1 );
   }

   /*
   Add space for comment characters, just to make sure this
   doesn't cause problems.
   */
   ncomch = 4096;

   dlaopn_ ( DLA1, "DSK",         DLA1,          &ncomch, 
             &han1, strlen(DLA1), strlen("DSK"), strlen(DLA1) );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Populate data buffers. 
   */
   nseg = 10;

   for ( i = 0;  i < nseg;  i++ )
   {
      dlabns_ ( &han1 );
      chckxc_c ( SPICEFALSE, " ", ok );

      for ( j = 0;  j < IBUFSZ-i;  j++ )
      {
         idata[j] = 1000 * i + j;
      }

      for ( j = 0;  j < DBUFSZ-i;  j++ )
      {
         ddata[j] = 1000.0 * i + j;
      }

      for ( j = 0;  j < CBUFSZ-i;  j++ )
      {
         
         nc = sprintf( cdata[j], "Segment %d line %d", (int)i, (int)j );
  
         /*
         Pad the string with trailing blanks. 
         */
         for ( k = nc;  k < CBUFLN;  k++ )
         {
            cdata[j][k] = ' ';
         }

         /*
         Add a marker at the right side of the string.
         */
         sprintf( cdata[j]+CBUFLN-4, "-->%c", NULLCHAR );
  
         /*
         printf( "%s\n", cdata[j] );
         */
        
      }

      /*
      Write the data to the DLA file. 
      */
      nw = IBUFSZ-i;

      dasadi_( &han1, &nw, idata );
      chckxc_c ( SPICEFALSE, " ", ok );


      nw = DBUFSZ-i;

      dasadd_( &han1, &nw, ddata );
      chckxc_c ( SPICEFALSE, " ", ok );


      /*
      Create a Fortran-style string array.
      */
      nstr = CBUFSZ-i;

      C2F_MapStrArr ( "f_dla_c",
                       nstr, CBUFLN, cdata, &fCvalsLen, &fCvalsArr );
      chckxc_c ( SPICEFALSE, " ", ok );


      ncadd = (nstr)*(CBUFLN-1);

      b     = 1;
      e     = CBUFLN-1;

      dasadc_ ( &han1, &ncadd, &b, &e, fCvalsArr, fCvalsLen );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Free the Fortran-style string array. 
      */
      free ( fCvalsArr );


      dlaens_ ( &han1 );
      chckxc_c ( SPICEFALSE, " ", ok );

   }
 

   /*
   Close file and re-open for read access. 
   */
   dascls_c( han1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasopr_c( DLA1, &han1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   *********************************************************************
   *
   *
   *   Forward traversal tests
   *
   *
   *********************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlabfs_c error: bad handle." );

   dlabfs_c( -999, &dladsc, &found );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlafns_c error: bad handle." );

   dlafns_c( -999, &dladsc, &nxtdsc, &found );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlabfs_c/dlafns_c: perform forward search on "
             "10-segment DLA."                           );

   /*
   DLA1 is assumed to be open at this point. 
   */

   dlabfs_c( han1, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   prvdsc.isize = 0;
   prvdsc.dsize = 0;
   prvdsc.csize = 0;

   chcksl_c ( "found", found, SPICETRUE, ok );

   xbasei = 3;
   xbasec = 0;
   xbased = 0;
   nfound = 0;

   while ( found )
   {
      ++nfound;
      
 
      /*
      Check out the descriptor components.
 
      Base addresses precede the first address in use by
      a DLA segment.
 
      The integer base is shifted by the amount of integer
      data in the previous segment, plus the size of a DLA
      descriptor, since DLA descriptors reside in DAS integer
      address space.
      */
      xbasei = xbasei + SPICE_DLA_DSCSIZ + prvdsc.isize;

      sprintf ( label, "IBASE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.ibase, "=", xbasei, 0, ok );

      /*
      Set the expected size based on the formula used
      for constructing the file. 
      */
      xsizei = IBUFSZ - nfound + 1;

      sprintf ( label, "ISIZE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.isize, "=", xsizei, 0, ok );


      /*
      Check d.p. base and size.
      */
      xbased = xbased + prvdsc.dsize;

      sprintf ( label, "DBASE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.dbase, "=", xbased, 0, ok );

      /*
      Set the expected size based on the formula used
      for constructing the file. 
      */
      xsized = DBUFSZ - nfound + 1;

      sprintf ( label, "DSIZE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.dsize, "=", xsized, 0, ok );


      /*
      Check character base and size.
      */
      xbasec = xbasec + prvdsc.csize;

      sprintf ( label, "CBASE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.cbase, "=", xbasec, 0, ok );

      /*
      Set the expected size based on the formula used
      for constructing the file. 

      Note that the null terminators in the character buffer
      don't get written to the DLA file.
      */
      xsizec = (CBUFSZ - nfound + 1)*(CBUFLN-1);

      sprintf ( label, "CSIZE segment %d", (int)nfound );

      chcksi_c ( label, dladsc.csize, "=", xsizec, 0, ok );

      /*
      Check the forward pointer. The pointer should contain
      the DAS integer address of the first element of 
      the next descriptor, or NULPTR if the current segment
      is the last. 
      */
      if ( nfound < nseg )
      {
         xfwd = dladsc.ibase + SPICE_DLA_DSCSIZ + dladsc.isize + 1;
      }
      else
      {
         xfwd = NULPTR;
      }

      /*
      Check the backward pointer. The pointer should contain
      the DAS integer address of the first element of 
      the previous descriptor, or NULPTR if the current segment
      is the first. 
      */
      if ( nfound > 1 )
      {
         xbwd = prvdsc.ibase + 1;
      }
      else
      {
         xbwd = NULPTR;
      }

      /*
      Prepare to check data. Fill buffers with expected
      values. 
      */
      
      i = nfound-1;
       
      for ( j = 0;  j < IBUFSZ-i;  j++ )
      {
         xdatai[j] = 1000 * i + j;
      }

      for ( j = 0;  j < DBUFSZ-i;  j++ )
      {
         xdatad[j] = 1000.0 * i + j;
      }

      for ( j = 0;  j < CBUFSZ-i;  j++ )
      {
         
         nc = sprintf( xdatac[j], "Segment %d line %d", (int)i, (int)j );
  
         /*
         Pad the string with trailing blanks. 
         */
         for ( k = nc;  k < CBUFLN;  k++ )
         {
            xdatac[j][k] = ' ';
         }

         /*
         Add a marker at the right side of the string.
         */
         sprintf( xdatac[j]+CBUFLN-4, "-->%c", NULLCHAR );
  
         /*
         printf( "%s\n", cdata[j] );
         */
      }

      /*
      Check integer data. 
      */
      baddr = xbasei + 1;
      eaddr = baddr  + xsizei - 1;

      dasrdi_ ( &han1, &baddr, &eaddr, idata );

      sprintf ( label, "IDATA segment %d", (int)nfound );

      chckai_c ( label, idata, "=", xdatai, xsizei, ok ); 


      /*
      Check d.p. data. 
      */
      baddr = xbased + 1;
      eaddr = baddr  + xsized - 1;

      dasrdd_ ( &han1, &baddr, &eaddr, ddata );

      sprintf ( label, "DDATA segment %d", (int)nfound );

      chckad_c ( label, ddata, "=", xdatad, xsized, 0.0, ok ); 


      /*
      Check character data. 
      */
      baddr = xbasec + 1;
      eaddr = baddr  + xsizec - 1;
      b     = 1;
      e     = CBUFLN-1;

      dasrdc_ ( &han1, &baddr, &eaddr, &b, &e, (char *)cdata, CBUFLN-1 );

      /*
      Convert the Fortran-style string array to C style.
      This transformation is done in place.
      */
      nstr = CBUFSZ-i;

      F2C_ConvertTrStrArr ( nstr, CBUFLN, (char *)cdata );

      for ( j = 0;  j < nstr;  j++ )
      {
         sprintf ( label, "CDATA segment %d string %d", (int)nfound, j );
        
         /*
         Note that incrementing the string pointers by j amounts to 
         an increment of j*CBUFLN bytes. We then cast the pointer
         to type (SpicChar*) to suppress compiler warnings.
         */
         chcksc_c ( label, (SpiceChar*)(cdata +j), "=",
                           (SpiceChar*)(xdatac+j), ok   ); 
      }

      /*
      Fetch next segment. 
      */
      prvdsc = dladsc;

      dlafns_c ( han1, &prvdsc, &dladsc, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Escape from loop if necessary.
      */ 
      if ( nfound > nseg )
      {
         found = SPICEFALSE;
      }

   }



   /*
   *********************************************************************
   *
   *
   *   Backward traversal tests
   *
   *
   *********************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlabbs_c error: bad handle." );

   dlabbs_c( -999, &dladsc, &found );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlafps_c error: bad handle." );

   dlafps_c( -999, &dladsc, &prvdsc, &found );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );





   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dlabbs_c/dlafps_c: perform backward search on "
             "10-segment DLA."                                );
 

   dlabbs_c ( han1, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );


   for ( i = nseg-1;  i >= 0;  i-- )
   {
      sprintf ( label, "Segment %d found", (int)i );

      chcksl_c ( label, found, SPICETRUE, ok );


      xbasei = dladsc.ibase;
      xbased = dladsc.dbase;
      xbasec = dladsc.cbase;

      xsizei = dladsc.isize;
      xsized = dladsc.dsize;
      xsizec = dladsc.csize;


      /*
      Prepare to check data. Fill buffers with expected
      values. 
      */
             
      for ( j = 0;  j < IBUFSZ-i;  j++ )
      {
         xdatai[j] = 1000 * i + j;
      }

      for ( j = 0;  j < DBUFSZ-i;  j++ )
      {
         xdatad[j] = 1000.0 * i + j;
      }

      for ( j = 0;  j < CBUFSZ-i;  j++ )
      {
         
         nc = sprintf( xdatac[j], "Segment %d line %d", (int)i, (int)j );
  
         /*
         Pad the string with trailing blanks. 
         */
         for ( k = nc;  k < CBUFLN;  k++ )
         {
            xdatac[j][k] = ' ';
         }

         /*
         Add a marker at the right side of the string.
         */
         sprintf( xdatac[j]+CBUFLN-4, "-->%c", NULLCHAR );
  
         /*
         printf( "%s\n", cdata[j] );
         */
      }

      /*
      Check integer data. 
      */
      baddr = xbasei + 1;
      eaddr = baddr  + xsizei - 1;

      dasrdi_ ( &han1, &baddr, &eaddr, idata );

      sprintf ( label, "IDATA segment %d", (int)i );

      chckai_c ( label, idata, "=", xdatai, xsizei, ok ); 

      /*
      Check d.p. data. 
      */
      baddr = xbased + 1;
      eaddr = baddr  + xsized - 1;

      dasrdd_ ( &han1, &baddr, &eaddr, ddata );

      sprintf ( label, "DDATA segment %d", (int)i );

      chckad_c ( label, ddata, "=", xdatad, xsized, 0.0, ok ); 


      /*
      Check character data. 
      */
      baddr = xbasec + 1;
      eaddr = baddr  + xsizec - 1;
      b     = 1;
      e     = CBUFLN-1;

      dasrdc_ ( &han1, &baddr, &eaddr, &b, &e, (char *)cdata, CBUFLN-1 );

      /*
      Convert the Fortran-style string array to C style.
      This transformation is done in place.
      */
      nstr = CBUFSZ-i;

      F2C_ConvertTrStrArr ( nstr, CBUFLN, (char *)cdata );

      for ( j = 0;  j < nstr;  j++ )
      {
         sprintf ( label, "CDATA segment %d string %d", (int)i, (int)j );
        
         /*
         Note that incrementing the string pointers by j amounts to 
         an increment of j*CBUFLN bytes. We then cast the pointer
         to type (SpicChar*) to suppress compiler warnings.
         */
         chcksc_c ( label, (SpiceChar*)(cdata +j), "=",
                           (SpiceChar*)(xdatac+j), ok   ); 
      }

      /*
      Look up previous segment. 
      */
      dlafps_c ( han1, &dladsc, &prvdsc, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      dladsc = prvdsc;

   }






   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up DLA files." );

   dascls_c ( han1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   if ( exists_c(DLA1) ) 
   { 
      removeFile( DLA1 );
   }




   t_success_c ( ok );
   
   
} /* End f_plt_c */

