/* f_dsk02.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__10000 = 10000;
static integer c__20000 = 20000;
static doublereal c_b16 = .23;
static integer c_b17 = 1000000;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__10 = 10;
static integer c__0 = 0;
static integer c__9 = 9;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__3 = 3;
static integer c__5 = 5;
static integer c__6 = 6;
static integer c__7 = 7;
static integer c__8 = 8;
static integer c__14 = 14;
static integer c_b149 = 100000;
static integer c__11 = 11;
static integer c__12 = 12;
static integer c__13 = 13;
static integer c_n1 = -1;
static integer c__15 = 15;
static integer c__24 = 24;
static doublereal c_b312 = 0.;
static integer c__16 = 16;
static integer c__17 = 17;
static integer c__18 = 18;
static integer c__25 = 25;
static doublereal c_b556 = 1e-14;

/* $Procedure      F_DSK02 ( Test DSK type 2 low-level routines ) */
/* Subroutine */ int f_dsk02__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static integer addr__, nlat;
    static doublereal last;
    static integer nlon;
    static doublereal work[2000000]	/* was [2][1000000] */;
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__;
    static integer surf2, i__, j, k, n;
    static char label[80];
    extern /* Subroutine */ int dskb02_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    integer *, integer *, integer *, integer *, integer *);
    static doublereal dbuff[30000];
    extern /* Subroutine */ int dskd02_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, doublereal *);
    static char frame[32];
    static integer ibuff[100000];
    extern /* Subroutine */ int dski02_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), dskgd_(integer *, 
	    integer *, doublereal *), tcase_(char *, ftnlen), dskn02_(integer 
	    *, integer *, integer *, doublereal *), dskp02_(integer *, 
	    integer *, integer *, integer *, integer *, integer *);
    static integer plate[3];
    extern /* Subroutine */ int dskv02_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *);
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int dskw02_(integer *, integer *, integer *, 
	    integer *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *,
	     integer *, integer *, doublereal *, integer *, ftnlen), dskz02_(
	    integer *, integer *, integer *, integer *), repmi_(char *, char *
	    , integer *, char *, ftnlen, ftnlen, ftnlen);
    static integer xncgr;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal first, xform[9]	/* was [3][3] */;
    extern doublereal twopi_(void);
    static integer dlads2[8];
    extern /* Subroutine */ int t_success__(logical *), dskrb2_(integer *, 
	    doublereal *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *), dskmi2_(integer *, doublereal *, 
	    integer *, integer *, doublereal *, integer *, integer *, integer 
	    *, integer *, logical *, integer *, doublereal *, doublereal *, 
	    integer *);
    static doublereal mncor1, mncor2, mncor3, spaxd2[10], spaxd3[10];
    static integer plats2[60000];
    static doublereal mxcor1, mxcor2, mxcor3;
    static integer spaxi2[1000000], spaxi3[1000000];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static doublereal vrtcs2[30000], vrtcs3[30000];
    static integer dladsc[8], handle, vlsiz3, np, vpsiz3;
    extern /* Subroutine */ int delfil_(char *, ftnlen), cleard_(integer *, 
	    doublereal *);
    extern doublereal halfpi_(void);
    static integer framid, nv;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     dlabfs_(integer *, integer *, logical *);
    extern logical exists_(char *, ftnlen);
    static doublereal corpar[10], dskdsc[24], finscl, mn32, mn33, mx32, mx33, 
	    normal[3], ovtbds[6]	/* was [2][3] */, ovxori[3], ovxsiz, 
	    spaixd[10], varray[9]	/* was [3][3] */, vrtces[30000], 
	    xnorml[3];
    static integer bodyid, corscl, dclass, np2, nv2, nvxtot, nxtdsc[8], 
	    ocrscl, onp, onv, onvxtt, han1, ovgrxt[3], ovlsiz, ovpsiz, ovtlsz,
	     plates[60000], spaixi[1000000], spxisz, surfid, vgrext[3], 
	    vpsize, vlsize, vtxlsz, xnp, xnv;
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen), dskopn_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), dskcls_(
	    integer *, logical *), mxv_(doublereal *, doublereal *, 
	    doublereal *), dasopr_(char *, integer *, ftnlen), chcksl_(char *,
	     logical *, logical *, logical *, ftnlen), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), dlafns_(integer *, integer *, integer *, logical *), 
	    chcksd_(char *, doublereal *, char *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen), pltnrm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), vhatip_(doublereal *), kclear_(void),
	     dascls_(integer *);

/* $ Abstract */

/*     Test the DSK type 2 low-level routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSK type 2 routines */

/*        DSKB02 */
/*        DSKD02 */
/*        DSKI02 */
/*        DSKN02 */
/*        DSKP02 */
/*        DSKV02 */
/*        DSKZ02 */

/*     and the generic DSK routine */

/*        DSKGD */

/*     It exercises, but does not fully test, the routines */

/*        DSKMI2 */
/*        DSKRB2 */
/*        DSKW02 */

/*     The above routines have their own test families. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-AUG-2016  (NJB) */

/*        Removed tests specific to DSKW02; those tests are now */
/*        included in F_DSKW02. */

/*     29-JUL-2016  (NJB) */

/*        Added further tests for DSKW02, DSKRB2, and DSKMI2. */


/*     31-MAR-2016  (NJB) */

/*        Based on TDK routine F_DSK02 Version 5.0.0, 07-JUN-2010  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */

/*     Save everything to avoid stack problems. */


/*     Begin every test family with an open call. */

    topen_("F_DSK02", (ftnlen)7);

/* --- Case -------------------------------------------------------- */

    tcase_("Create new Mars DSK files.", (ftnlen)26);
    if (exists_("dsk02_test0.bds", (ftnlen)15)) {
	delfil_("dsk02_test0.bds", (ftnlen)15);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("dsk02_test1.bds", (ftnlen)15)) {
	delfil_("dsk02_test1.bds", (ftnlen)15);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }


/*     Create vertices and plates. */

/*     The Mars radii used here need not be consistent with */
/*     the current generic PCK. */

    a = 3396.19;
    b = a;
    c__ = 3376.2;
    nlon = 20;
    nlat = 10;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, vrtces, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a spatial index for the plate set. */

/*     Use a heuristic formula for the fine scale. */

/* Computing MAX */
    d__3 = (doublereal) np;
    d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
    finscl = max(d__1,d__2);

/*     Pick a one-size-fits-all value for the coarse scale. */

    corscl = 10;

/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv, vrtces, &np, plates, &finscl, &corscl, &c_b17, &vpsize, &
		vlsize, &c_true, &spxisz, work, spaixd, spaixi);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv, vrtces, &np, plates, &c__1, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Set segment attribute inputs. */

    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    first = -jyear_() * 100;
    last = jyear_() * 100;
    cleard_(&c__10, corpar);
    dclass = 2;
    bodyid = 499;
    surfid = 1;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(frame, &framid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write the file. */

    dskopn_("dsk02_test0.bds", "dsk02_test0.bds", &c__0, &handle, (ftnlen)15, 
	    (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&handle, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a second segment having higher resolution. */

    nlon = 30;
    nlat = 15;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv2, vrtcs2, 
	    &np2, plats2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     Create a spatial index for the plate set. */

/*     Use a heuristic formula for the fine scale. */

/* Computing MAX */
    d__3 = (doublereal) np;
    d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
    finscl = max(d__1,d__2);

/*     Pick a one-size-fits-all value for the coarse scale. */

    corscl = 10;

/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv2, vrtcs2, &np2, plats2, &finscl, &corscl, &c_b17, &vpsize,
		 &vlsize, &c_true, &spxisz, work, spaxd2, spaxi2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv2, vrtcs2, &np2, plats2, &c__1, corpar, &mn32, &mx32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     This segment has its own surface ID. */

    surf2 = 2;
    if (*ok) {
	dskw02_(&handle, &bodyid, &surf2, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mn32, &mx32, &first, &
		last, &nv2, vrtcs2, &np2, plats2, spaxd2, spaxi2, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close the file. */

    if (*ok) {
	dskcls_(&handle, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a second DSK file containing data similar to that */
/*     of the first segment. We want the segment's DAS address */
/*     ranges to be identical to those of the first segment, */
/*     but both the integer and d.p. data to be different. To */
/*     achieve this, we'll rotate the vertices to a different */
/*     frame. We'll still label the frame as IAU_MARS. */


/*     Let XFORM be a matrix that permutes the standard basis */
/*     vectors. */

    cleard_(&c__9, xform);
    xform[3] = 1.;
    xform[7] = 1.;
    xform[2] = 1.;
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	k = (i__ - 1) * 3 + 1;
	mxv_(xform, &vrtces[(i__2 = k - 1) < 30000 && 0 <= i__2 ? i__2 : 
		s_rnge("vrtces", i__2, "f_dsk02__", (ftnlen)534)], &vrtcs3[(
		i__3 = k - 1) < 30000 && 0 <= i__3 ? i__3 : s_rnge("vrtcs3", 
		i__3, "f_dsk02__", (ftnlen)534)]);
    }

/*     Create a spatial index for this data set. */


/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv, vrtcs3, &np, plates, &finscl, &corscl, &c_b17, &vpsize, &
		vlsize, &c_true, &spxisz, work, spaxd3, spaxi3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv, vrtcs3, &np, plates, &c__1, corpar, &mn33, &mx33);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dskopn_("dsk02_test1.bds", "dsk02_test1.bds", &c__0, &han1, (ftnlen)15, (
	    ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han1, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mn33, &mx33, &first, &
		last, &nv, vrtcs3, &np, plates, spaxd3, spaxi3, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (*ok) {
	dskcls_(&han1, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* *********************************************************************** */

/*     DSKI02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check DSK segment's vertex and plate counts.", (ftnlen)52)
	    ;
    dasopr_("dsk02_test0.bds", &handle, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xnv = nv;
    xnp = np;
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, &nv);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NV", &nv, "=", &xnv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    dski02_(&handle, dladsc, &c__2, &c__1, &c__1, &n, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NP", &np, "=", &xnp, &c__0, ok, (ftnlen)2, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check voxel grid extents.", (ftnlen)33);
    dski02_(&handle, dladsc, &c__4, &c__1, &c__3, &n, vgrext);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the voxel grid extent sub-array of the */
/*     integer spatial index component. */

    chckai_("VGREXT", vgrext, "=", spaixi, &c__3, ok, (ftnlen)6, (ftnlen)1);

/*     We'll use the total voxel count later. */

    nvxtot = vgrext[0] * vgrext[1] * vgrext[2];

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check coarse voxel grid scale.", (ftnlen)38);
    dski02_(&handle, dladsc, &c__5, &c__1, &c__1, &n, &corscl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the coarse voxel scale sub-array of the */
/*     integer spatial index component. */

    chcksi_("CORSCL", &corscl, "=", &spaixi[3], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check voxel pointer count.", (ftnlen)34);
    dski02_(&handle, dladsc, &c__6, &c__1, &c__1, &n, &vpsize);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VOXNPT", &vpsize, "=", &spaixi[4], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check voxel-plate correspondence list size.", (ftnlen)51);
    dski02_(&handle, dladsc, &c__7, &c__1, &c__1, &n, &vlsize);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VOXNPL", &vlsize, "=", &spaixi[5], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check vertex-plate correspondence list size.", (ftnlen)52)
	    ;
    dski02_(&handle, dladsc, &c__8, &c__1, &c__1, &n, &vtxlsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VTXNPL", &vtxlsz, "=", &spaixi[6], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check coarse grid", (ftnlen)25);
    dski02_(&handle, dladsc, &c__14, &c__1, &c_b149, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the coarse grid size first. */

/* Computing 3rd power */
    i__1 = corscl;
    xncgr = nvxtot / (i__1 * (i__1 * i__1));
    chcksi_("NCGR", &n, "=", &xncgr, &c__0, ok, (ftnlen)4, (ftnlen)1);
    if (*ok) {
	chckai_("CGRPTR", ibuff, "=", &spaixi[7], &n, ok, (ftnlen)6, (ftnlen)
		1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check plates.", (ftnlen)21);
    i__1 = np * 3;
    dski02_(&handle, dladsc, &c__9, &c__1, &i__1, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = np * 3;
    chckai_("PLATES", ibuff, "=", plates, &i__1, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check voxel-plate pointer array.", (ftnlen)40);
    dski02_(&handle, dladsc, &c__10, &c__1, &vpsize, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = 100008;
    chckai_("VOXPTR", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dsk02__", (ftnlen)761)]
	    , &vpsize, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check voxel-plate correspondence list.", (ftnlen)46);
    dski02_(&handle, dladsc, &c__11, &c__1, &vlsize, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008;
    chckai_("VOXLST", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dsk02__", (ftnlen)778)]
	    , &vlsize, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check vertex-plate pointer array.", (ftnlen)41);
    dski02_(&handle, dladsc, &c__12, &c__1, &nv, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008 + vlsize;
    chckai_("VRTPTR", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dsk02__", (ftnlen)796)]
	    , &nv, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: Check vertex-plate correspondence list.", (ftnlen)47);
    dski02_(&handle, dladsc, &c__13, &c__1, &vtxlsz, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008 + vlsize + nv;
    chckai_("VRTLST", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dsk02__", (ftnlen)813)]
	    , &vtxlsz, ok, (ftnlen)6, (ftnlen)1);

/*     The following cases exercise the logic for use of saved values. */


/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: read from second segment.", (ftnlen)33);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dski02_(&handle, nxtdsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV2", ibuff, "=", &nv2, &c__0, ok, (ftnlen)3, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: read from first segment of first file again.", (ftnlen)52)
	    ;

/*     This call resets the previous DLA descriptor to the first one of */
/*     the first file. This sets up the next test, which shows that */
/*     DSKI02 can detect a segment change when the DLA segment */
/*     descriptor start addresses match those of the previous segment, */
/*     but the handle changes. */

    if (*ok) {
	dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV", ibuff, "=", &nv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: read from first segment of second file.", (ftnlen)47);
    dasopr_("dsk02_test1.bds", &han1, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&han1, dlads2, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {

/*        Check voxel-plate correspondence list. This is the call */
/*        to DSKI02 where the input handle changes. We use IBUFSZ */
/*        as the "room" argument so we don't need to look up the */
/*        list size. We want to look up the voxel-plate list at */
/*        this point because this is an integer structure that */
/*        differs depending on which file we're reading. */

	dski02_(&han1, dlads2, &c__11, &c__1, &c_b149, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We need to look up the size of the voxel-plate pointer */
/*        array in order to get the correct index of the voxel-plate */
/*        list in the spatial index. */

	dski02_(&han1, dlads2, &c__6, &c__1, &c__1, &n, &vpsiz3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the voxel-plate list size as well. */

	dski02_(&han1, dlads2, &c__7, &c__1, &c__1, &n, &vlsiz3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Compare against the corresponding sub-array of the */
/*        integer spatial index component. */

	addr__ = vpsiz3 + 100008;
	chckai_("VOXLST", ibuff, "=", &spaxi3[(i__1 = addr__ - 1) < 1000000 &&
		 0 <= i__1 ? i__1 : s_rnge("spaxi3", i__1, "f_dsk02__", (
		ftnlen)908)], &vlsiz3, ok, (ftnlen)6, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02: read from first segment of first file again.", (ftnlen)52)
	    ;
    if (*ok) {
	dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV", ibuff, "=", &nv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02 error: invalid keyword", (ftnlen)29);
    dski02_(&handle, dladsc, &c_n1, &c__1, &c__1, &n, ibuff);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02 error: invalid room value", (ftnlen)32);
    dski02_(&handle, dladsc, &c__9, &c__1, &c__0, &n, ibuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dski02_(&handle, dladsc, &c__9, &c__1, &c_n1, &n, ibuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKI02 error: invalid start value", (ftnlen)33);
    dski02_(&handle, dladsc, &c__9, &c_n1, &c__1, &n, ibuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
    i__1 = np * 3 + 1;
    dski02_(&handle, dladsc, &c__9, &i__1, &np, &n, ibuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     DSKD02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: check DSK descriptor", (ftnlen)28);
    dskd02_(&handle, dladsc, &c__15, &c__1, &c__24, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check descriptor elements. */

    i__1 = i_dnnt(&dbuff[1]);
    chcksi_("BODYID", &i__1, "=", &bodyid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(dbuff);
    chcksi_("SURFID", &i__1, "=", &surfid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[4]);
    chcksi_("FRAMID", &i__1, "=", &framid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[2]);
    chcksi_("DCLASS", &i__1, "=", &dclass, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[3]);
    chcksi_("DTYPE", &i__1, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[5]);
    chcksi_("CORSYS", &i__1, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MNCOR1", &dbuff[16], "=", &mncor1, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR1", &dbuff[17], "=", &mxcor1, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR2", &dbuff[18], "=", &mncor2, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR2", &dbuff[19], "=", &mxcor2, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR3", &dbuff[20], "=", &mncor3, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR3", &dbuff[21], "=", &mxcor3, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("CORPAR", &dbuff[6], "=", corpar, &c__10, &c_b312, ok, (ftnlen)6, 
	    (ftnlen)1);
    chcksd_("FIRST", &dbuff[22], "=", &first, &c_b312, ok, (ftnlen)5, (ftnlen)
	    1);
    chcksd_("LAST", &dbuff[23], "=", &last, &c_b312, ok, (ftnlen)4, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: check vertex bounds", (ftnlen)27);
    dskd02_(&handle, dladsc, &c__16, &c__1, &c__6, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("VTXBDS", dbuff, "=", spaixd, &c__6, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: check voxel origin", (ftnlen)26);
    dskd02_(&handle, dladsc, &c__17, &c__1, &c__3, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("VOXORI", dbuff, "=", &spaixd[6], &c__3, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: check voxel size", (ftnlen)24);
    dskd02_(&handle, dladsc, &c__18, &c__1, &c__1, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("VOXSIZ", dbuff, "=", &spaixd[9], &c_b312, ok, (ftnlen)6, (ftnlen)
	    1);

/*     The following cases exercise the logic for use of saved values. */


/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: read from second segment.", (ftnlen)33);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dskd02_(&handle, nxtdsc, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaxd2[9], &c_b312, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: read from first segment of second file.", (ftnlen)47);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dskd02_(&han1, dlads2, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaxd3[9], &c_b312, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: read from first segment of first file again.", (ftnlen)52)
	    ;
    if (*ok) {
	dskd02_(&handle, dladsc, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaixd[9], &c_b312, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02 error: invalid keyword", (ftnlen)29);
    dskd02_(&handle, dladsc, &c_n1, &c__1, &c__1, &n, dbuff);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02 error: invalid room value", (ftnlen)32);
    dskd02_(&handle, dladsc, &c__15, &c__1, &c__0, &n, dbuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dskd02_(&handle, dladsc, &c__15, &c__1, &c_n1, &n, dbuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02 error: invalid start value", (ftnlen)33);
    dskd02_(&handle, dladsc, &c__15, &c_n1, &c__1, &n, dbuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
    dskd02_(&handle, dladsc, &c__15, &c__25, &c__24, &n, dbuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     DSKB02 tests */

/* *********************************************************************** */
    tcase_("DSKB02: Check parameters from first segment of first file", (
	    ftnlen)57);
    dskb02_(&handle, dladsc, &onv, &onp, &onvxtt, ovtbds, &ovxsiz, ovxori, 
	    ovgrxt, &ocrscl, &ovtlsz, &ovpsiz, &ovlsiz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NV", &onv, "=", &xnv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    chcksi_("NP", &onp, "=", &xnp, &c__0, ok, (ftnlen)2, (ftnlen)1);

/*     Check the voxel grid extent out of order so it can be used to */
/*     check the total count. */

    chckai_("VGREXT", ovgrxt, "=", spaixi, &c__3, ok, (ftnlen)6, (ftnlen)1);
    j = vgrext[0] * vgrext[1] * vgrext[2];
    chcksi_("NVXTOT", &onvxtt, "=", &j, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chckad_("VTXBDS", ovtbds, "=", spaixd, &c__6, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("VOXSIZ", &ovxsiz, "=", &spaixd[9], &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("VOXORI", ovxori, "=", &spaixd[6], &c__3, &c_b312, ok, (ftnlen)6, 
	    (ftnlen)1);
    chcksi_("CORSCL", &ocrscl, "=", &spaixi[3], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksi_("VTXLSZ", &ovtlsz, "=", &spaixi[6], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksi_("VOXPSZ", &ovpsiz, "=", &spaixi[4], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksi_("VOXLSZ", &ovlsiz, "=", &spaixi[5], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
/* *********************************************************************** */

/*     DSKZ02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKZ02: Check DSK segment's vertex and plate counts.", (ftnlen)52)
	    ;
    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NV", &nv, "=", &xnv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    chcksi_("NP", &np, "=", &xnp, &c__0, ok, (ftnlen)2, (ftnlen)1);
/* *********************************************************************** */

/*     DSKV02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKV02: get vertices from first segment of first file in one cal"
	    "l.", (ftnlen)66);
    dskv02_(&handle, dladsc, &c__1, &nv, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nv * 3;
    chckad_("VRTCES", dbuff, "=", vrtces, &i__1, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKV02: get vertices from first segment of first file one at a t"
	    "ime.", (ftnlen)68);
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dskv02_(&handle, dladsc, &i__, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 3 + 1;
	chckad_("VRTCES", dbuff, "=", &vrtces[(i__2 = j - 1) < 30000 && 0 <= 
		i__2 ? i__2 : s_rnge("vrtces", i__2, "f_dsk02__", (ftnlen)
		1223)], &c__3, &c_b312, ok, (ftnlen)6, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKV02 error: bad ROOM value", (ftnlen)28);
    dskv02_(&handle, dladsc, &c__1, &c__0, &n, dbuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dskv02_(&handle, dladsc, &c__1, &c_n1, &n, dbuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKV02 error: bad START value", (ftnlen)29);
    dskv02_(&handle, dladsc, &c__0, &nv, &n, dbuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
    i__1 = nv + 1;
    dskv02_(&handle, dladsc, &i__1, &nv, &n, dbuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     DSKP02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKP02: get plates from first segment of first file in one call.",
	     (ftnlen)64);
    dskp02_(&handle, dladsc, &c__1, &np, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = np * 3;
    chckai_("PLATES", ibuff, "=", plates, &i__1, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKP02: get plates from first segment of first file one at a time"
	    , (ftnlen)65);
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dskp02_(&handle, dladsc, &i__, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 3 + 1;
	chckai_("PLATES", ibuff, "=", &plates[(i__2 = j - 1) < 60000 && 0 <= 
		i__2 ? i__2 : s_rnge("plates", i__2, "f_dsk02__", (ftnlen)
		1286)], &c__3, ok, (ftnlen)6, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKP02 error: bad room value", (ftnlen)28);
    dskp02_(&handle, dladsc, &c__1, &c__0, &n, ibuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dskp02_(&handle, dladsc, &c__1, &c_n1, &n, ibuff);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKP02 error: bad START value", (ftnlen)29);
    dskp02_(&handle, dladsc, &c__0, &np, &n, ibuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
    i__1 = np + 1;
    dskp02_(&handle, dladsc, &i__1, &np, &n, ibuff);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     DSKN02 tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKN02: check normal vectors for all plates in first segment.", (
	    ftnlen)61);
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get the normal vector for the Ith plate. */

	dskn02_(&handle, dladsc, &i__, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the Ith plate; look up its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, plate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    k = plate[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("plate",
		     i__2, "f_dsk02__", (ftnlen)1345)];
	    dskv02_(&handle, dladsc, &k, &c__1, &n, &varray[(i__2 = j * 3 - 3)
		     < 9 && 0 <= i__2 ? i__2 : s_rnge("varray", i__2, "f_dsk"
		    "02__", (ftnlen)1347)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the expected normal vector. Note that the output */
/*        of PLTNRM does not have unit length. */

	pltnrm_(varray, &varray[3], &varray[6], xnorml);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vhatip_(xnorml);
	s_copy(label, "Normal @", (ftnlen)80, (ftnlen)8);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(label, normal, "~~/", xnorml, &c__3, &c_b556, ok, (ftnlen)80, 
		(ftnlen)3);
    }
/* *********************************************************************** */

/*     DSKGD tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKGD: check descriptor of first segment of first file.", (ftnlen)
	    55);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check descriptor elements. */

    i__1 = i_dnnt(&dskdsc[1]);
    chcksi_("BODYID", &i__1, "=", &bodyid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(dskdsc);
    chcksi_("SURFID", &i__1, "=", &surfid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dskdsc[4]);
    chcksi_("FRAMID", &i__1, "=", &framid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dskdsc[2]);
    chcksi_("DCLASS", &i__1, "=", &dclass, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dskdsc[3]);
    chcksi_("DTYPE", &i__1, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = i_dnnt(&dskdsc[5]);
    chcksi_("CORSYS", &i__1, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MNCOR1", &dskdsc[16], "=", &mncor1, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR1", &dskdsc[17], "=", &mxcor1, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR2", &dskdsc[18], "=", &mncor2, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR2", &dskdsc[19], "=", &mxcor2, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR3", &dskdsc[20], "=", &mncor3, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR3", &dskdsc[21], "=", &mxcor3, &c_b312, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("CORPAR", &dskdsc[6], "=", corpar, &c__10, &c_b312, ok, (ftnlen)6,
	     (ftnlen)1);
    chcksd_("FIRST", &dskdsc[22], "=", &first, &c_b312, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksd_("LAST", &dskdsc[23], "=", &last, &c_b312, ok, (ftnlen)4, (ftnlen)
	    1);


/*     Clean up. */


/* --- Case -------------------------------------------------------- */

    tcase_("Unload and delete DSK files.", (ftnlen)28);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dsk02_test0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dsk02_test1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dsk02__ */

