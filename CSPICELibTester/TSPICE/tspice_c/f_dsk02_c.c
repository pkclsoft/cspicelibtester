/*

-Procedure f_dsk02_c ( Test wrappers for DSK type 2 functions )

 
-Abstract
 
   Perform tests on CSPICE wrappers for DSK type 2
   functions. 
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <stdio.h>
   #include <math.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_dsk02_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the DSK type 2 routines

      dskb02_c
      dskd02_c
      dski02_c
      dskn02_c
      dskp02_c
      dskv02_c
      dskz02_c

   and the generic DSK routine

      dskgd_c

   It exercises, but does not fully test, the routines

      dskmi2_c
      dskrb2_c
      dskw02_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Original version 20-AUG-2016 (NJB)

-Index_Entries

   test dsk type 2 routines

-&
*/

{ /* Begin f_dsk02_c */

 
   /*
   Constants
   */
 
   #define DSK0            "test0.bds"
   #define DSK1            "test1.bds"
   #define DSK2            "baddsk.bds"

   #define VTIGHT          ( 1.e-14 )

   #define DTYPE           2
   #define FRNMLN          33
   #define IBUFSZ          SPICE_DSK02_MAXCGR
   #define LNSIZE          81
   #define MAXP            20000
   #define MAXV            10000
   #define VOXPSZ          100000
   #define VOXNPL          200000
   #define MXIXSZ          1000000
   #define WORKSZ          1000000
   #define DBUFSZ          ( 3 * MAXV )
   #define LATSYS          SPICE_DSK_LATSYS

   /*
   Local variables
   */
   SPICEDOUBLE_CELL        ( vout,  3*MAXV );
   SPICEDOUBLE_CELL        ( vout2, 3*MAXV );
   SPICEDOUBLE_CELL        ( vout3, 3*MAXV );
   SPICEINT_CELL           ( pout,  3*MAXP );
   SPICEINT_CELL           ( pout2, 3*MAXP );
   SPICEINT_CELL           ( pout3, 3*MAXP );

   SpiceDLADescr           dladsc;
   SpiceDLADescr           dlads2;
   SpiceDLADescr           nxtdsc;

   SpiceDSKDescr           dskdsc;

   SpiceBoolean            found;
 
   SpiceChar             * frame;
   SpiceChar               label  [ LNSIZE ];

   SpiceDouble             a;
   SpiceDouble             b;
   SpiceDouble             c;
   SpiceDouble             corpar [ SPICE_DSK_NSYPAR ];
   static SpiceDouble      dbuff  [ DBUFSZ ];
   SpiceDouble             finscl;
   SpiceDouble             fnscl2;
   SpiceDouble             first;
   SpiceDouble             last;
   SpiceDouble             mn32;
   SpiceDouble             mn33;
   SpiceDouble             mncor1;
   SpiceDouble             mncor2;
   SpiceDouble             mncor3;
   SpiceDouble             mx32;
   SpiceDouble             mx33;
   SpiceDouble             mxcor1;
   SpiceDouble             mxcor2;
   SpiceDouble             mxcor3;
   SpiceDouble             normal [3];
   SpiceDouble             ovtbds [3][2];
   SpiceDouble             ovxori [3];
   SpiceDouble             ovxsiz;
   static SpiceDouble      spaixd [ SPICE_DSK02_IXDFIX ];
   static SpiceDouble      spaxd2 [ SPICE_DSK02_IXDFIX ];
   static SpiceDouble      spaxd3 [ SPICE_DSK02_IXDFIX ];
   SpiceDouble             varray [3][3];
   SpiceDouble          (* vrtces )[3];
   SpiceDouble          (* vrtcs2 )[3];
   SpiceDouble          (* vrtcs3 )[3];
   SpiceDouble             xform  [3][3];
   SpiceDouble             xnorml [3];

   SpiceInt                addr;
   SpiceInt                bodyid;
   SpiceInt                corscl;
   SpiceInt                dclass;
   SpiceInt                framid;
   SpiceInt                handle;
   SpiceInt                han1;
   SpiceInt                han2;
   SpiceInt                i;
   static SpiceInt         ibuff  [ IBUFSZ ];
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                np;
   SpiceInt                np2;
   SpiceInt                nv;
   SpiceInt                nv2;
   SpiceInt                nvxtot;
   SpiceInt                ocrscl;
   SpiceInt                onp;
   SpiceInt                onv;
   SpiceInt                onvxtt;
   SpiceInt                ovgrxt [3];
   SpiceInt                ovlsiz;
   SpiceInt                ovpsiz;
   SpiceInt                ovtlsz;
   SpiceInt                plate  [3];
   SpiceInt             (* plates)[3];
   SpiceInt             (* plats2)[3];
   SpiceInt             (* plats3)[3];
   static SpiceInt         spaixi [ MXIXSZ ];
   static SpiceInt         spaxi2 [ MXIXSZ ];
   static SpiceInt         spaxi3 [ MXIXSZ ];
   SpiceInt                spxisz;
   SpiceInt                surfid;
   SpiceInt                surf2;
   SpiceInt                vgrext [3];
   SpiceInt                vpsiz3;
   SpiceInt                vpsize;
   SpiceInt                vlsiz3;
   SpiceInt                vlsize;
   SpiceInt                vtxlsz;
   static SpiceInt         work   [ WORKSZ ][2];
   SpiceInt                xncgr;
   SpiceInt                xnp;
   SpiceInt                xnv;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dsk02_c" );
   

   
   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Create new Mars DSK files" );

 
   if ( exists_c(DSK0) )
   {
      removeFile( DSK0 );
   }

   if ( exists_c(DSK1) )
   {
      removeFile( DSK1 );
   }


   /*
   Create vertices and plates.

   The Mars radii used here need not be consistent with
   the current generic PCK.
   */

   a    = 3396.19;
   b    = a;
   c    = 3376.20;

   nlon = 20;
   nlat = 10;

   zzellplt_c ( a, b, c, nlon, nlat, &vout, &pout ); 

   chckxc_c ( SPICEFALSE, " ", ok );              

   nv     = card_c(&vout) / 3;
   np     = card_c(&pout) / 3;

   vrtces = (SpiceDouble (*)[3]) (vout.data);
   plates = (SpiceInt    (*)[3]) (pout.data);


   /*
   Create a spatial index for the plate set.

   Use a heuristic formula for the fine scale.
   */
   finscl = maxd_c ( 2,  1.0,  pow( np, 0.23) / 8 );

   /*
   Pick a one-size-fits-all value for the coarse scale.
   */
   corscl = 10;

   /*
   Set the spatial index integer component size.
   */
   vpsize = VOXPSZ;
   vlsize = VOXNPL;
   spxisz = MXIXSZ;

   if ( ok ) 
   {
      /*
      Create a spatial index that includes a vertex-plate mapping.
      */
      dskmi2_c ( nv,     vrtces, np,     plates, finscl,   
                 corscl, WORKSZ, vpsize, vlsize, SPICETRUE,   
                 spxisz, work,   spaixd, spaixi            );

      chckxc_c ( SPICEFALSE, " ", ok );
   }



   /*
   Generate bounds for the 3rd coordinate.
   */
   if ( ok ) 
   {
      dskrb2_c ( nv,     vrtces, np,      plates, 
                 LATSYS, corpar, &mncor3, &mxcor3  );

      chckxc_c ( SPICEFALSE, " ", ok );
   }
     
   
   /*
   Set segment attribute inputs.
   */
   mncor1 =  0.0;
   mxcor1 =  twopi_c();
   mncor2 = -halfpi_c();
   mxcor2 =  halfpi_c();

   first  = -jyear_c() * 100;
   last   =  jyear_c() * 100;

   for ( i = 0; i < SPICE_DSK_NSYPAR; i++ )
   {
      corpar[i] = 0.0;
   }

   dclass =   2;
   bodyid = 499;
   surfid =   1;
   frame  = "IAU_MARS";

   namfrm_c ( frame, &framid );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Write the file.
   */
   dskopn_c ( DSK0, DSK0, 0, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   if ( ok )
   {
      dskw02_c ( handle,      bodyid,      surfid, 
                 dclass,      frame,       LATSYS,    
                 corpar,      mncor1,      mxcor1,
                 mncor2,      mxcor2,      mncor3,     
                 mxcor3,      first,       last,        
                 nv,          vrtces,      np,         
                 plates,      spaixd,      spaixi );

      chckxc_c ( SPICEFALSE, " ", ok );
   }



   /*
   Create a second segment having higher resolution.
   */   
   nlon = 30;
   nlat = 15;

   zzellplt_c ( a, b, c, nlon, nlat, &vout2, &pout2 ); 

   chckxc_c ( SPICEFALSE, " ", ok );              

   nv2    = card_c(&vout2) / 3;
   np2    = card_c(&pout2) / 3;

   vrtcs2 = (SpiceDouble (*)[3]) (vout2.data);
   plats2 = (SpiceInt    (*)[3]) (pout2.data);

   /*
   Create a spatial index for the plate set.

   Use a heuristic formula for the fine scale.
   */
   fnscl2 = maxd_c ( 2,  1.0,  pow( np, 0.23) / 8 );

   /*
   Pick a one-size-fits-all value for the coarse scale.
   */
   corscl = 10;

   /*
   Set the spatial index integer component size.
   */
   vpsize = VOXPSZ;
   vlsize = VOXNPL;
   spxisz = MXIXSZ;

   if ( ok ) 
   {
      /*
      Create a spatial index that includes a vertex-plate mapping.
      */
      dskmi2_c ( nv2,    vrtcs2, np2,    plats2, fnscl2,   
                 corscl, WORKSZ, vpsize, vlsize, SPICETRUE,   
                 spxisz, work,   spaxd2, spaxi2            );

      chckxc_c ( SPICEFALSE, " ", ok );
   }



   /*
   Generate bounds for the 3rd coordinate.
   */
   if ( ok ) 
   {
      dskrb2_c ( nv2,    vrtcs2, np2,   plats2, 
                 LATSYS, corpar, &mn32, &mx32  );

      chckxc_c ( SPICEFALSE, " ", ok );
   }


   /*
   This segment has its own surface ID. 
   */
   surf2 = 2;

   if ( ok )
   {
      dskw02_c ( handle,      bodyid,      surf2, 
                 dclass,      frame,       LATSYS,    
                 corpar,      mncor1,      mxcor1,
                 mncor2,      mxcor2,      mn32,     
                 mx32,        first,       last,        
                 nv2,         vrtcs2,      np2,         
                 plats2,      spaxd2,      spaxi2 );

      chckxc_c ( SPICEFALSE, " ", ok );
   }


   /*
   Close the file.
   */     
   if ( ok ) 
   {
      dskcls_c ( handle, SPICETRUE );
      chckxc_c ( SPICEFALSE, " ", ok );
   }




   /*
   Create a second DSK file containing data similar to that
   of the first segment. We want the segment's DAS address
   ranges to be identical to those of the first segment,
   but both the integer and d.p. data to be different. To
   achieve this, we'll rotate the vertices to a different
   frame. We'll still label the frame as IAU_MARS.

   Let `xform' be a matrix that permutes the standard basis
   vectors.

   */

   a    = 3396.19;
   b    = a;
   c    = 3376.20;

   nlon = 20;
   nlat = 10;

   zzellplt_c ( a, b, c, nlon, nlat, &vout3, &pout3 ); 
   chckxc_c ( SPICEFALSE, " ", ok );              

   nv     = card_c(&vout3) / 3;
   np     = card_c(&pout3) / 3;

   vrtcs3 = (SpiceDouble (*)[3]) (vout3.data);
   plats3 = (SpiceInt    (*)[3]) (pout3.data);

   cleard_c ( 9, (SpiceDouble *)xform );


   xform[0][1] = 1.0;
   xform[1][2] = 1.0;
   xform[2][0] = 1.0;


   for ( i = 0; i < nv; i++ )
   {
      mxv_c ( xform, vrtces[i], vrtcs3[i] );
   }

   /*
   Create a spatial index for the plate set.
  */
   vpsize = VOXPSZ;
   vlsize = VOXNPL;
   spxisz = MXIXSZ;


   if ( ok ) 
   {
      /*
      Create a spatial index that includes a vertex-plate mapping.
      */
      dskmi2_c ( nv,     vrtcs3, np,     plats3, finscl,   
                 corscl, WORKSZ, vpsize, vlsize, SPICETRUE,   
                 spxisz, work,   spaxd3, spaxi3            );

      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Generate bounds for the 3rd coordinate.
   */
   if ( ok ) 
   {
      dskrb2_c ( nv,     vrtcs3, np,    plats3, 
                 LATSYS, corpar, &mn33, &mx33  );

      chckxc_c ( SPICEFALSE, " ", ok );
   }


   dskopn_c ( DSK1, DSK1, 0, &han1 );
   chckxc_c ( SPICEFALSE, " ", ok );



   if ( ok )
   {
      dskw02_c ( han1,        bodyid,      surfid, 
                 dclass,      frame,       LATSYS,    
                 corpar,      mncor1,      mxcor1,
                 mncor2,      mxcor2,      mn33,     
                 mx33,        first,       last,        
                 nv,          vrtcs3,      np,         
                 plats3,      spaxd3,      spaxi3 );

      chckxc_c ( SPICEFALSE, " ", ok );
   }


   /*
   Close the file.
   */     
   if ( ok ) 
   {
      dskcls_c ( han1, SPICETRUE );
      chckxc_c ( SPICEFALSE, " ", ok );
   }


   /*
   *********************************************************************
   *
   *     dski02_c tests
   *
   *********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check DSK segment''s vertex "
             "and plate counts."                    );
 

   dasopr_c ( DSK0, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   xnv = nv;
   xnp = np;

   dlabfs_c ( handle, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   dski02_c ( handle, &dladsc, SPICE_DSK02_KWNV, 0, 1, &n, &nv );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "nv", nv, "=", xnv, 0, ok );


   dski02_c ( handle, &dladsc, SPICE_DSK02_KWNP, 0, 1, &n, &np );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "np", np, "=", xnp, 0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check voxel grid extents." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVGRX, 0, 3, &n, vgrext );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compare against the voxel grid extent sub-array of the 
   integer spatial index component. 
   */
   chckai_c ( "vgrext", vgrext, "=", spaixi+SPICE_DSK02_SIVGRX, 3, ok );
   
   nvxtot = vgrext[0] * vgrext[1] * vgrext[2];



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check coarse voxel grid scale." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWCGSC, 0, 1, &n, &corscl );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compare against the coarse voxel scale sub-array of the 
   integer spatial index component. 
   */
   chcksi_c ( "corscl", corscl, "=", spaixi[SPICE_DSK02_SICGSC], 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check voxel pointer count." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVXPS, 0, 1, &n, &vpsize );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compare against the corresponding sub-array of the 
   integer spatial index component. 
   */
   chcksi_c ( "vpsize", vpsize, "=", spaixi[SPICE_DSK02_SIVXNP], 0, ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check voxel-plate correspondence list size." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVXLS, 0, 1, &n, &vlsize );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compare against the corresponding sub-array of the 
   integer spatial index component. 
   */
   chcksi_c ( "vlsize", vlsize, "=", spaixi[SPICE_DSK02_SIVXNL], 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check vertex-plate correspondence list size." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVTLS, 0, 1, &n, &vtxlsz );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compare against the corresponding sub-array of the 
   integer spatial index component. 
   */
   chcksi_c ( "vtxlsz", vtxlsz, "=", spaixi[SPICE_DSK02_SIVTNL], 0, ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check coarse grid." );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWCGPT, 0, SPICE_DSK02_MAXCGR, 
              &n,     ibuff                                              );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the coarse grid size first.
   */

   xncgr = nvxtot / pow( corscl, 3 );

   chcksi_c ( "ncgr", n, "=", xncgr, 0, ok );

   if ( ok )
   {
      chckai_c ( "cgrptr", ibuff, "=", spaixi+SPICE_DSK02_SICGRD, n, ok );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check plates" );
 
   dski02_c ( handle, &dladsc, SPICE_DSK02_KWPLAT, 0, 3*np, 
              &n,     ibuff                                   );
   chckxc_c ( SPICEFALSE, " ", ok );


   chckai_c ( "plates", ibuff, "=", (SpiceInt*)plates, 3*np, ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check voxel-plate pointer array." );


   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVXPT, 0, vpsize, 
              &n,     ibuff                                   );
   chckxc_c ( SPICEFALSE, " ", ok );

   addr = SPICE_DSK02_SICGRD + SPICE_DSK02_MAXCGR;

   chckai_c ( "voxptr", ibuff, "=", spaixi+addr, vpsize, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check voxel-plate correspondence list." );


   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVXPL, 0, vlsize, 
              &n,     ibuff                                   );
   chckxc_c ( SPICEFALSE, " ", ok );

   addr = SPICE_DSK02_SICGRD + SPICE_DSK02_MAXCGR + vpsize;

   chckai_c ( "voxlst", ibuff, "=", spaixi+addr, vlsize, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check vertex-plate pointer array." );


   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVTPT, 0, nv, 
              &n,     ibuff                                   );
   chckxc_c ( SPICEFALSE, " ", ok );

   addr = SPICE_DSK02_SICGRD + SPICE_DSK02_MAXCGR + vpsize + vlsize;

   chckai_c ( "vrtptr", ibuff, "=", spaixi+addr, nv, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: Check vertex-plate correspondence list." );


   dski02_c ( handle, &dladsc, SPICE_DSK02_KWVTPL, 0, vtxlsz, 
              &n,     ibuff                                   );
   chckxc_c ( SPICEFALSE, " ", ok );

   addr =   SPICE_DSK02_SICGRD + SPICE_DSK02_MAXCGR 
          + vpsize + vlsize + nv;

   chckai_c ( "vrtlst", ibuff, "=", spaixi+addr, vtxlsz, ok );


   /*
   The following cases exercise the logic for use of saved values.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: read from second segment." );


   dlafns_c ( handle, &dladsc, &nxtdsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( ok )
   {
      dski02_c ( handle, &nxtdsc, SPICE_DSK02_KWNV, 0, 1, &n, ibuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksi_c ( "nv2", ibuff[0], "=", nv2, 0, ok );
   }
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: read from first segment of first file again." );

   /*
   This call resets the previous DLA descriptor to the first one of
   the first file. This sets up the next test, which shows that
   dski02_c can detect a segment change when the DLA segment
   descriptor start addresses match those of the previous segment,
   but the handle changes.
   */

   if ( ok ) 
   {
      dski02_c ( handle, &dladsc, SPICE_DSK02_KWNV, 0, 1, &n, ibuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksi_c ( "nv", ibuff[0], "=", nv, 0, ok );
   }

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: read from first segment of second file." );

   dasopr_c ( DSK1, &han1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   dlabfs_c ( han1, &dlads2, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( ok )
   {
      /*
      Check voxel-plate correspondence list. This is the call
      to dski02_c where the input handle changes. We use IBUFSZ
      as the "room" argument so we don't need to look up the
      list size. We want to look up the voxel-plate list at
      this point because this is an integer structure that
      differs depending on which file we're reading.
      */
      dski02_c ( han1, &dlads2, SPICE_DSK02_KWVXPL, 0, IBUFSZ, &n, ibuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      We need to look up the size of the voxel-plate pointer
      array in order to get the correct index of the voxel-plate
      list in the spatial index.
      */
      dski02_c ( han1, &dlads2, SPICE_DSK02_KWVXPS, 0, 1, &n, &vpsiz3 );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Get the voxel-plate list size as well. 
      */
      dski02_c ( han1, &dlads2, SPICE_DSK02_KWVXLS, 0, 1, &n, &vlsiz3 );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Compare against the corresponding sub-array of the 
      integer spatial index component.
      */

      addr = SPICE_DSK02_SICGRD + SPICE_DSK02_MAXCGR + vpsiz3;

      chckai_c ( "VOXLST", ibuff, "=", spaxi3+addr, vlsiz3, ok );
   }

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c: read from first segment of first file again." );

   /*
   This call resets the previous DLA descriptor to the first one of
   the first file. This sets up the next test, which shows that
   dski02_c can detect a segment change when the DLA segment
   descriptor start addresses match those of the previous segment,
   but the handle changes.
   */

   if ( ok ) 
   {
      dski02_c ( handle, &dladsc, SPICE_DSK02_KWNV, 0, 1, &n, ibuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksi_c ( "nv", ibuff[0], "=", nv, 0, ok );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c error: invalid keyword." );

   dski02_c ( handle, &dladsc, -1, 0, 1, &n, ibuff );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c error: invalid room value." );

   dski02_c ( handle, &dladsc, SPICE_DSK02_KWPLAT, 0,  0, &n, ibuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   dski02_c ( handle, &dladsc, SPICE_DSK02_KWPLAT, 0, -1, &n, ibuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dski02_c error: invalid start value." );

   dski02_c ( handle, &dladsc, SPICE_DSK02_KWPLAT, -1,     np, &n, ibuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dski02_c ( handle, &dladsc, SPICE_DSK02_KWPLAT, 3*np+1, np, &n, ibuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );





   /*
   *********************************************************************
   *
   *     dskd02_c tests
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: Check vertex bounds." );


   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWVTBD, 0, 6, 
              &n,     dbuff                               );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "vtxbds", dbuff, "=", spaixd+SPICE_DSK02_SIVTBD, 
              6,        0.0,   ok                             );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: Check voxel origin." );


   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWVXOR, 0, 3, 
              &n,     dbuff                               );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "voxori", dbuff, "=", spaixd+SPICE_DSK02_SIVXOR, 
              3,        0.0,   ok                             );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: Check voxel size." );


   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWVXSZ, 0, 1, 
              &n,     dbuff                               );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "voxsiz", dbuff[0], "=", spaixd[SPICE_DSK02_SIVXSZ], 
              0.0,   ok                                           );




   /*
   The following cases exercise the logic for use of saved values.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: read from second segment." );


   dlafns_c ( handle, &dladsc, &nxtdsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( ok )
   {
      dskd02_c ( handle, &nxtdsc, SPICE_DSK02_KWVXSZ, 0, 1, &n, dbuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( "voxsiz", dbuff[0], "=", spaxd2[SPICE_DSK02_SIVXSZ], 
                 0.0,        ok                                         );
   }
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: read from first segment of second file." );

   dasopr_c ( DSK1, &han1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   dlabfs_c ( han1, &dlads2, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( ok )
   {
      dskd02_c ( han1, &dlads2, SPICE_DSK02_KWVXSZ, 0, 1, &n, dbuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( "voxsiz", dbuff[0], "=", spaxd3[SPICE_DSK02_SIVXSZ], 
                 0.0,      ok                                         );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c: read from first segment of first file again." );

   if ( ok ) 
   {
      dskd02_c ( handle, &dladsc, SPICE_DSK02_KWVXSZ, 0, 1, &n, dbuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( "voxsiz", dbuff[0], "=", spaixd[SPICE_DSK02_SIVXSZ], 
                 0.0,      ok                                         );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c error: invalid keyword." );

   dskd02_c ( handle, &dladsc, -1, 0, 1, &n, dbuff );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c error: invalid room value." );

   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWDSC, 0,  0, &n, dbuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWDSC, 0, -1, &n, dbuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskd02_c error: invalid start value." );

   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWDSC, -1,     np, &n, dbuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dskd02_c ( handle, &dladsc, SPICE_DSK02_KWDSC, 3*np+1, np, &n, dbuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );



   /*
   *********************************************************************
   *
   *     dskb02_c tests
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskb02_c: Check parameters from first segment "
             "of first file."                                 );


   dskb02_c ( handle,  &dladsc, &onv,   &onp,   &onvxtt,
              ovtbds,  &ovxsiz, ovxori, ovgrxt, &ocrscl,
              &ovtlsz, &ovpsiz, &ovlsiz                  );

   chckxc_c ( SPICEFALSE, " ", ok );


   chcksi_c ( "nv", onv, "=", xnv, 0, ok );
   chcksi_c ( "np", onp, "=", xnp, 0, ok );

   /*
   Check the voxel grid extent out of order so it can be used to
   check the total count. 
   */

   chckai_c ( "vgrext", ovgrxt, "=", spaixi+SPICE_DSK02_SIVGRX, 3, ok );

   j = vgrext[0] * vgrext[1] * vgrext[2];

   chcksi_c ( "nvxtot", onvxtt, "=", j, 0, ok );

   chckad_c ( "vtxbds", (SpiceDouble*)ovtbds, "=", 
              spaixd+SPICE_DSK02_SIVTBD, 6, 
              0.0,      ok                      );

   chcksd_c ( "voxsiz", ovxsiz, "=", spaixd[SPICE_DSK02_SIVXSZ], 
              0.0,      ok                                       );

   chckad_c ( "voxori", ovxori, "=", spaixd+SPICE_DSK02_SIVXOR, 3, 
              0.0,      ok                                        );

   chcksi_c ( "corscl", ocrscl, "=", spaixi[SPICE_DSK02_SICGSC],
              0,        ok );

   chcksi_c ( "vtxlsz", ovtlsz, "=", spaixi[SPICE_DSK02_SIVTNL],
              0,        ok );

   chcksi_c ( "voxpsz", ovpsiz, "=", spaixi[SPICE_DSK02_SIVXNP],
              0,        ok );

   chcksi_c ( "voxlsz", ovlsiz, "=", spaixi[SPICE_DSK02_SIVXNL],
              0,        ok );





   /*
   *********************************************************************
   *
   *     dskz02_c tests
   *
   *********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskz02_c: Check DSK segment's vertex and plate counts." );


   dskz02_c ( handle, &dladsc, &onv, &onp );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "nv", onv, "=", xnv, 0, ok );
   chcksi_c ( "np", onp, "=", xnp, 0, ok );


   /*
   *********************************************************************
   *
   *     dskv02_c tests
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskv02_c: Get vertices from first segment"
             "of first file in one call."                );

   dskv02_c ( handle, &dladsc, 1, nv, &n, (SpiceDouble (*)[3])dbuff );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "vrtces", dbuff, "=", (SpiceDouble*)vrtces, 3*nv, 
              0.0,      ok                                      );
  

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskv02_c: Get vertices from first segment "
             "of first file one at a time."                );

   
   for ( i = 0;  i < nv;  i++ )
   {
      /*
      The `start' index is 1-based. 
      */
      dskv02_c ( handle, &dladsc, i+1, 3, &n, (SpiceDouble (*)[3])dbuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Note that `vrtces' is a pointer to an array of 3 SpiceDoubles. 
      */
      chckad_c ( "vrtces", dbuff, "=", (SpiceDouble*)(vrtces+i), 3, 
                 0.0,      ok                                       );
   }
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskv02_c error: Bad `room' value." );

   dskv02_c ( handle, &dladsc, 1,  0, &n, (SpiceDouble (*)[3])dbuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   dskv02_c ( handle, &dladsc, 1, -1, &n, (SpiceDouble (*)[3])dbuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskv02_c error: Bad `start' value." );

   dskv02_c ( handle, &dladsc,  0,  1, &n, (SpiceDouble (*)[3])dbuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dskv02_c ( handle, &dladsc, -1,  1, &n, (SpiceDouble (*)[3])dbuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );




   /*
   *********************************************************************
   *
   *     dskp02_c tests
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskp02_c: Get plates from first segment"
             "of first file in one call."                );

   dskp02_c ( handle, &dladsc, 1, np, &n, (SpiceInt(*)[3])ibuff );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckai_c ( "plates", ibuff, "=", (SpiceInt*)plates, 3*np, ok );
  

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskp02_c: Get plates from first segment "
             "of first file one at a time."                );

   
   for ( i = 0;  i < np;  i++ )
   {
      /*
      The `start' index is 1-based. 
      */
      dskp02_c ( handle, &dladsc, i+1, 3, &n, (SpiceInt(*)[3])ibuff );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Note that `plates' is a pointer to an array of 3 SpiceInts. 
      */
      chckai_c ( "plates", ibuff, "=", (SpiceInt*)(plates+i), 3, ok );
   }
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskp02_c error: Bad `room' value." );

   dskp02_c ( handle, &dladsc, 1,  0, &n, (SpiceInt(*)[3])ibuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   dskp02_c ( handle, &dladsc, 1, -1, &n, (SpiceInt(*)[3])ibuff );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskp02_c error: Bad `start' value." );

   dskp02_c ( handle, &dladsc,  0,  1, &n, (SpiceInt(*)[3])ibuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dskp02_c ( handle, &dladsc, -1,  1, &n, (SpiceInt(*)[3])ibuff );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );




   /*
   *********************************************************************
   *
   *     dskn02_c tests
   *
   *********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskn02: check normal vectors for all plates "
             "in first segment."                           );

   for ( i = 1;  i <= np;  i++ )
   {
      /*
      Get the normal vector for the ith plate. 
      */

      dskn02_c ( handle, &dladsc, i, normal );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Get the Ith plate; look up its vertices.
      */
      dskp02_c ( handle, &dladsc, i, 1, &n, (SpiceInt(*)[3])plate );
      chckxc_c ( SPICEFALSE, " ", ok );

      for ( j = 0; j < 3; j++ )
      {
         k = plate[j];

         dskv02_c ( handle, &dladsc, k, 1, &n, 
                    (SpiceDouble(*)[3])varray[j] );
         chckxc_c ( SPICEFALSE, " ", ok );
      }

      /*
      Compute the expected normal vector. Note that the output
      of pltnrm_c does not have unit length. 
      */

      pltnrm_c ( varray[0], varray[1], varray[2], xnorml );
      chckxc_c ( SPICEFALSE, " ", ok );

      vhat_c ( xnorml, xnorml );

      strncpy ( label, "Normal @", LNSIZE );

      repmi_c ( label, "@", i, LNSIZE, label );
      chckad_c ( label, normal, "~~/", xnorml, 3, VTIGHT, ok );
   }




   /*
   *********************************************************************
   *
   *     dskgd_c tests
   *
   *********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgd_c:  check descriptor of first segment "
             "of first file."                            );


   dskgd_c ( handle, &dladsc, &dskdsc );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check descriptor elements.
   */

   chcksi_c ( "bodyid", dskdsc.center, "=", bodyid, 0, ok );
   chcksi_c ( "surfid", dskdsc.surfce, "=", surfid, 0, ok );
   chcksi_c ( "framid", dskdsc.frmcde, "=", framid, 0, ok );
   chcksi_c ( "dclass", dskdsc.dclass, "=", dclass, 0, ok );
   chcksi_c ( "dtype",  dskdsc.dtype,  "=", DTYPE,  0, ok );
   chcksi_c ( "corsys", dskdsc.corsys, "=", LATSYS, 0, ok );

   chckad_c ( "corpar", dskdsc.corpar, "=", corpar, 
              SPICE_DSK_NSYPAR,        0.0, ok );

   chcksd_c ( "co1min", dskdsc.co1min, "=", mncor1, 0.0, ok );
   chcksd_c ( "co1max", dskdsc.co1max, "=", mxcor1, 0.0, ok );
   chcksd_c ( "co2min", dskdsc.co2min, "=", mncor2, 0.0, ok );
   chcksd_c ( "co2max", dskdsc.co2max, "=", mxcor2, 0.0, ok );
   chcksd_c ( "co3min", dskdsc.co3min, "=", mncor3, 0.0, ok );
   chcksd_c ( "co3max", dskdsc.co3max, "=", mxcor3, 0.0, ok );
   chcksd_c ( "start",  dskdsc.start,  "=", first,  0.0, ok );
   chcksd_c ( "stop",   dskdsc.stop,   "=", last,   0.0, ok );




   /*
   These tests exercise error handling functions that are
   not exercised elsewhere. 

   We need to have at least one case that causes an error
   to be signaled in the underlying f2c'd Fortran routine.
   */

   /*
   *********************************************************************
   *
   *     Additional error tests
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskmi2_c error: invalid vertex count." );
   

   dskmi2_c ( -1,     vrtcs3, np,     plats3, finscl,   
              corscl, WORKSZ, vpsize, vlsize, SPICETRUE,   
              spxisz, work,   spaxd3, spaxi3            );

   chckxc_c ( SPICETRUE, "SPICE(BADVERTEXCOUNT)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskrb2_c error: invalid coordinate system." );

   dskrb2_c ( nv,  vrtcs3, np,    plats3, 
              -1,  corpar, &mn33, &mx33  );

   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskopn_c error: blank file name." );

   dskopn_c ( " ", " ", 0, &handle );

   chckxc_c ( SPICETRUE, "SPICE(BLANKFILENAME)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskopn_c error: empty input string." );

   dskopn_c ( "", " ", 0, &handle );

   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   dskopn_c ( "spud", "", 0, &handle );

   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskopn_c error: null pointer input argument." );

   dskopn_c ( NULL, " ", 0, &handle );

   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   dskopn_c ( "spud", NULL, 0, &handle );

   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskcls_c error: bad handle." );

   dskcls_c ( 0, SPICEFALSE );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskw02_c error: invalid handle." );


   dskw02_c ( 0,           bodyid,      surf2, 
              dclass,      frame,       LATSYS,    
              corpar,      mncor1,      mxcor1,
              mncor2,      mxcor2,      mn32,     
              mx32,        first,       last,        
              nv2,         vrtcs2,      np2,         
              plats2,      spaxd2,      spaxi2 );

   chckxc_c ( SPICETRUE, "SPICE(DASNOSUCHHANDLE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskw02_c error: empty string input argument." );

   /*
   Get a valid handle so we can get to the other error checks. 
   */
   dskopn_c ( DSK2, " ", 0, &han2 );
   chckxc_c ( SPICEFALSE, " ", ok );


   dskw02_c ( han2,        bodyid,      surf2, 
              dclass,      "",          LATSYS,    
              corpar,      mncor1,      mxcor1,
              mncor2,      mxcor2,      mn32,     
              mx32,        first,       last,        
              nv2,         vrtcs2,      np2,         
              plats2,      spaxd2,      spaxi2 );

   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskw02_c error: null pointer input argument." );

   /*
   Get a valid handle so we can get to the other error checks. 
   */
   dskw02_c ( han2,        bodyid,      surf2, 
              dclass,      NULL,        LATSYS,    
              corpar,      mncor1,      mxcor1,
              mncor2,      mxcor2,      mn32,     
              mx32,        first,       last,        
              nv2,         vrtcs2,      np2,         
              plats2,      spaxd2,      spaxi2 );

   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );



   

   /*
   Clean up. 
   */
   
   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Unload and delete DSK files." );

   dascls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile ( DSK0 ); 

   dascls_c ( han1 );
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile ( DSK1 );

   dascls_c ( han2 );
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile ( DSK2 );

    
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dsk02_c */

