/*

-Procedure f_dskg02_c ( Test DSK type 2 geometry routines )

 
-Abstract
 
   Perform tests on the DSKLIB_C type 2 geometry routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"


   void f_dskg02_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Perform tests on the DSKLIB_C type 2 geometry routines.
   Routines covered by this family:

      dskx02_c
      llgrid_pl02
      limb_pl02
      subpt_pl02
      subsol_pl02
      term_pl02
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version 

   -tspice_c Version 1.0.0 27-OCT-2016 (NJB) 
 
      Updated include file references. Updated "bad handle"
      short error message tests to accommodate integration of
      the DAS and handle manager subsystems: the short error
      message is not generated within the handle manager 
      subsystem.

      Loosened tolerances for limb tests involving off-limb
      and surface-intersecting vectors.

      26-JUN-2014 (NJB) 

         Loosened tolerances for ellipsoid and DSK sub-solar point
         point agreement, with Phobos used as the target body.
         Only code for the "intercept" case was modified.

      24-JUN-2014 (NJB) 

         Added tests of limb_pl02 error handling:
         no ID for observer name, observer same 
         object as target.

      13-JUN-2014 (NJB) 

         Added limb-finding tests for dskx02_c.

      08-JUN-2014 (NJB) 

         Bug fix: in term_pl02 terminator point solar incidence angle
         tests, changed computation of target-source position vectors 
         to use `fixref' as the frame argument rather than J2000.

         Added term_pl02 error test case for missing light source radii.

      06-JUN-2014 (NJB) 
   
         Tweaked initial longitude in llgrid_pl02 tests
         to move it away from plate boundaries.

         Corrected title for limb_pl02 and term_pl02 normal cases.

      21-MAY-2014 (NJB) 


-Index_Entries

   test dsk type_2 geometry routines

-&
*/

{ /* Begin f_dskg02_c */

 
   /*
   Prototypes 
   */
               
   /*
   Constants
   */
   #define                 DSKPHB  "dskg02_phobos.bds"
   #define                 PCK0    "dskg02_test.tpc"
   #define                 SPK0    "dskg02_test.bsp"
    
   #define                 LOOSE   1.0e-4
   #define                 SINGLE  1.0e-7
   #define                 MEDTOL  1.0e-8
   #define                 TIGHT   1.0e-12
   #define                 VTIGHT  1.0e-14
   #define                 NCORR   5
   #define                 NTIMES  100
   #define                 MSGLEN  401
   #define                 MAXPNT  200
   #define                 MAXITR  200
   #define                 LNSIZE  81

   /*
   Local variables
   */
   SpiceBoolean            found;

   SpiceChar             * abcorr;
   SpiceChar             * abcorrs[] = { "NONE", "CN+S", "CN", "LT", "LT+S" };
   SpiceChar             * emethod;
   SpiceChar             * fixref;
   SpiceChar             * kvname;
   SpiceChar             * obsrvr;
   SpiceChar               label  [ LNSIZE ];
   SpiceChar             * method;
   SpiceChar             * source;
   SpiceChar             * target;
   SpiceChar             * trmtyp;
   SpiceChar               title  [ MSGLEN ];

   SpiceDLADescr           dladsc;
   SpiceDSKDescr           dskdsc;

   SpiceDouble             alt;
   SpiceDouble             angdel;
   SpiceDouble             badrad[3];
   SpiceDouble             cnvtol;
   SpiceDouble             cylmat[3][3];
   SpiceDouble             cylvec[3];
   SpiceDouble             delta;
   SpiceDouble             diff;
   SpiceDouble             dist;
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             emissn;
   SpiceDouble             et0;
   SpiceDouble             et;
   SpiceDouble             grid   [ MAXPNT ][2];
   SpiceDouble             lat;
   SpiceDouble             level;
   SpiceDouble             limbpts[ MAXPNT ][3];
   SpiceDouble             lon;
   SpiceDouble             lower;
   SpiceDouble             lt;
   SpiceDouble             ltsun;
   SpiceDouble             maxang;
   SpiceDouble             midpt;
   SpiceDouble             minang;
   SpiceDouble             normal [3];
   SpiceDouble             npoint [3];
   SpiceDouble             obspos [3];
   SpiceDouble             phase;
   SpiceDouble             r;
   SpiceDouble             radii  [3];
   SpiceDouble             raydir [3];
   SpiceDouble             sep;
   SpiceDouble             solar;
   SpiceDouble             spoint [3];
   SpiceDouble             spoints[ MAXPNT ] [3];
   SpiceDouble             srcang;
   SpiceDouble             srclt;
   SpiceDouble             srcdst;
   SpiceDouble             srcpos [3];
   SpiceDouble             srcrad [3];
   SpiceDouble             srcvec [3];
   SpiceDouble             srfobs [3];
   SpiceDouble             srfsun [3];
   SpiceDouble             srfvec [3];
   SpiceDouble             sunpos [3];
   SpiceDouble             tdelta;
   SpiceDouble             termpts[ MAXPNT ][3];
   SpiceDouble             theta;
   SpiceDouble             tol;
   SpiceDouble             trgepc;
   SpiceDouble             trgpos [3];   
   SpiceDouble             tstdir [3];
   SpiceDouble             up     [3];
   SpiceDouble             upper;
   SpiceDouble             vertex [3];
   SpiceDouble             vlon;
   SpiceDouble             vr;
   SpiceDouble             vz;
   SpiceDouble             vmag;
   SpiceDouble             x      [3];
   SpiceDouble             xalt;
   SpiceDouble             xemssn;
   SpiceDouble             xlat;
   SpiceDouble             xlon;
   SpiceDouble             xphase;
   SpiceDouble             xplat;
   SpiceDouble             xplon;
   SpiceDouble             xpr;
   SpiceDouble             xpt    [3];
   SpiceDouble             xr;
   SpiceDouble             xsolar;
   SpiceDouble             xspoint[3];
   SpiceDouble             y      [3];
   SpiceDouble             z      [3];

   SpiceEllipse            limb;

   SpiceInt                coridx;
   SpiceInt                dskhan;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                npoints;
   SpiceInt                plateID;
   SpiceInt                plateIDs [ MAXPNT ];
   SpiceInt                spkhan;
   SpiceInt                timidx;
   SpiceInt                xPlateID;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dskg02_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );  

   /*
   Create and load the generic test kernels. 
   */

 
   if ( exists_c(SPK0) )
   {
      removeFile( SPK0 );
   }
   
   tstspk_c ( SPK0, SPICETRUE, &spkhan );   
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Keep PCK after loading it. 
   */
   tstpck_c ( PCK0, SPICETRUE, SPICETRUE );
   chckxc_c( SPICEFALSE, " ", ok );

   tstlsk_c();
   chckxc_c( SPICEFALSE, " ", ok );
 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create Phobos type 2 DSK file." );  

  
   if ( exists_c(DSKPHB) )
   {
      removeFile( DSKPHB ); 
   }
   zzt_boddsk_c ( DSKPHB, "Phobos", "IAU_PHOBOS", SPICEFALSE, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );   

   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );



   /*
    ******************************************************************
    *
    *
    *  subpt_pl02 tests
    *
    *
    ******************************************************************
   */    

   /*
    ******************************************************************
    *
    *
    *  subpt_pl02 error cases
    *
    *
    ******************************************************************
   */    
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02 error case init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;
   method  = "Intercept";
 
   obsrvr  = "Mars";
   target  = "Phobos";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";
   et      = 0.0;
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: bad method" );

   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );

   /*
   Find the sub-observer point on the target.
   */
   method = "wrong";
   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(DUBIOUSMETHOD)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: bad target" );


   /*
   Find the sub-observer point on the target.
   */
   method = "ellipsoid near point";
   target = "x";
   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: bad observer" );


   /*
   Find the sub-observer point on the target.
   */
   target = "phobos";
   obsrvr = "x";

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: target and observer coincide" );


   /*
   Find the sub-observer point on the target.
   */
   target = "phobos";
   obsrvr = target;

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: missing frame definition" );

   /*
   IMPLEMENT LATER
   */

   /*
   target = "phobos";
   obsrvr = "Mars";

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: bad aberration correction" );

   target = "phobos";
   obsrvr = "Mars";

   abcorr = "xxx";

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );
  

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: transmission aberration correction" );

   target = "phobos";
   obsrvr = "Mars";

   abcorr = "XCN";

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: no DSK loaded" );

   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   abcorr = "NONE";

   
   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );
   

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: target radii not in kernel pool" );


   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   dvpool_c ( kvname );
   chckxc_c( SPICEFALSE, " ", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(KERNELVARNOTFOUND)", ok );

   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );
   chckxc_c( SPICEFALSE, " ", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: DSK segment doesn't contain data for target" );

   /*
   TO BE IMPLEMENTED LATER 
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: DSK segment's data type is not 2" );

   /*
   TO BE IMPLEMENTED LATER 
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: ray toward sub-point misses target" );

   /*
   TO BE IMPLEMENTED LATER 
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: input string pointer is null" );


   subpt_pl02 ( dskhan, &dladsc, NULL,   target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, NULL, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                NULL,   obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, NULL,    spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02: input string is empty" );

   subpt_pl02 ( dskhan, &dladsc, "",     target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, "",   et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                "",     obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, "",      spoint, &alt,   &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );





   /*
    ******************************************************************
    *
    *
    *  subpt_pl02 normal cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subpt_pl02 time init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   /* 
   ---- Case ---------------------------------------------------------
   */
  
   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;


         abcorr = abcorrs[ coridx ];

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Intercept";
         emethod = "Intercept";

         sprintf ( title, 
                   "subpt_pl02: find sub-Mars point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                                    );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-observer point on the target.
         */
         subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                      abcorr, obsrvr,  spoint, &alt,   &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Find the sub-observer point on the target using an
         ellipsoidal model.
         */
         subpt_c ( emethod, target, et,    
                   abcorr,  obsrvr, xspoint, &xalt ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compare sub-observer points' planetocentric longitude/latitude.
         */
         reclat_c ( spoint,  &r,  &lon,  &lat );
         chckxc_c ( SPICEFALSE, " ", ok );

         reclat_c ( xspoint, &xr, &xlon, &xlat );
         chckxc_c ( SPICEFALSE, " ", ok );


         tol = MEDTOL;

         chcksd_c ( "lon", lon, "~/", xlon, tol, ok );
         chcksd_c ( "lat", lat, "~/", xlat, tol, ok );


         /*
         Compare observer altitudes. Make the absolute tolerance
         50 cm.
         */
         tol = 5.0e-4;
         chcksd_c ( "alt", alt, "~", xalt, tol, ok );

         /*
         Check the distance between the points. Tolerance = 50cm.
         */
         tol = 5.0e-4;

         chckad_c (  "spoint", spoint, "~", xspoint, 3, tol, ok );


         /*
         Now perform a more stringent test. Create a ray using
         the target-observer position and observer-to ellipsoid
         sub-point vector. See how close the ray-surface intercept 
         is to the sub-point.
         */
         spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         vminus_c ( trgpos, obspos );
         vsub_c   ( xspoint, obspos, raydir );
         
         dskx02_c ( dskhan, &dladsc, obspos, raydir, &xPlateID, xpt, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "xpt found", found, SPICETRUE, ok );

         /*
         The intercept should be on the same plate as the sub-point. 
         */
         chcksi_c ( "plateID", plateID, "=", xPlateID, 0, ok );

         /*
         The intercept and the sub-point should be very close together. 
         */
         tol = 1.e-10;

         chckad_c ( "xpt", xpt, "~~/", spoint, 3, tol, ok );


         /* 
         ---- Case ---------------------------------------------------------
         */

         method  = "Ellipsoid Near Point";
         emethod = "Near Point";

         sprintf ( title, 
                   "subpt_pl02: find sub-Mars point on Phobos. Method = "
                   "%s. Abcorr = %s.",
                   method,
                   abcorr                                                );

         tcase_c ( title );


         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";
         abcorr  = "NONE";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-observer point on the target.
         */
         subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                      abcorr, obsrvr,  spoint, &alt,   &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Find the sub-observer point on the target using an
         ellipsoidal model.
         */
         subpt_c ( emethod, target, et,    
                   abcorr,  obsrvr, xspoint, &xalt ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compare sub-observer points' planetocentric longitude/latitude.

         Note that, due to the fact that the target is triaxial, not
         even longitudes will agree.
         */
         reclat_c ( spoint,  &r,  &lon,  &lat );
         chckxc_c ( SPICEFALSE, " ", ok );

         reclat_c ( xspoint, &xr, &xlon, &xlat );
         chckxc_c ( SPICEFALSE, " ", ok );

         tol = LOOSE;

         chcksd_c ( "lon", lon, "~/", xlon, tol, ok );
         chcksd_c ( "lat", lat, "~/", xlat, tol, ok );

         /*
         Compare observer altitudes. 
         */
         tol = 1.e-3;
         chcksd_c ( "alt", alt, "~", xalt, tol, ok );

         /*
         Check the distance between the points. Tolerance = 50cm.
         */
         tol = 5.0e-4;

         chckad_c (  "spoint", spoint, "~", xspoint, 3, tol, ok );



         /*
         Now perform a more stringent test. Create a ray using
         the target-observer position and observer-to ellipsoid
         sub-point vector. See how close the ray-surface intercept 
         is to the sub-point.
         */
         spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         vminus_c ( trgpos, obspos );
         vsub_c   ( xspoint, obspos, raydir );
         
         dskx02_c ( dskhan, &dladsc, obspos, raydir, &xPlateID, xpt, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "xpt found", found, SPICETRUE, ok );

         /*
         The intercept should be on the same plate as the sub-point. 
         */
         chcksi_c ( "plateID", plateID, "=", xPlateID, 0, ok );

         /*
         The intercept and the sub-point should be very close together. 
         */
         tol = TIGHT;

         chckad_c ( "xpt", xpt, "~~/", spoint, 3, tol, ok );

      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */







   /*
    ******************************************************************
    *
    *
    *  subsol_pl02 tests
    *
    *
    ******************************************************************
   */    

   /*
    ******************************************************************
    *
    *
    *  subsol_pl02 error cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02 error case init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;
   method  = "Intercept";
 
   obsrvr  = "Mars";
   target  = "Phobos";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";
   et      = 0.0;
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: bad method" );

   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );

   /*
   Find the sub-observer point on the target.
   */
   method = "wrong";
   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(DUBIOUSMETHOD)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: bad target" );


   /*
   Find the sub-observer point on the target.
   */
   method = "ellipsoid near point";
   target = "x";
   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: bad observer" );


   /*
   Find the sub-observer point on the target.
   */
   target = "phobos";
   obsrvr = "x";

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: target and observer coincide" );


   /*
   Find the sub-observer point on the target.
   */
   target = "phobos";
   obsrvr = target;

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: missing frame definition" );

   /*
   IMPLEMENT LATER
   */

   /*
   target = "phobos";
   obsrvr = "Mars";

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: bad aberration correction" );

   target = "phobos";
   obsrvr = "Mars";

   abcorr = "xxx";

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );
  

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: transmission aberration correction" );

   target = "phobos";
   obsrvr = "Mars";

   abcorr = "XCN";

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: no DSK loaded" );

   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   abcorr = "NONE";

   
   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );
   

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: target radii not in kernel pool" );


   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   dvpool_c ( kvname );
   chckxc_c( SPICEFALSE, " ", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(KERNELVARNOTFOUND)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );
   chckxc_c( SPICEFALSE, " ", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: DSK segment doesn't contain data for target" );

   /*
   TO BE IMPLEMENTED LATER 
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: DSK segment's data type is not 2" );

   /*
   TO BE IMPLEMENTED LATER 
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: ray toward sub-point misses target" );

   /*
   TO BE IMPLEMENTED LATER 
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: input string pointer is null" );


   subsol_pl02 ( dskhan, &dladsc, NULL,   target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, NULL, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                NULL,   obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, NULL,    spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02: input string is empty" );

   subsol_pl02 ( dskhan, &dladsc, "",     target, et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, "",   et,    
                abcorr, obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                "",     obsrvr,  spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, "",      spoint, &dist,  &plateID ); 
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );




   /*
    ******************************************************************
    *
    *
    *  subsol_pl02 normal cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "subsol_pl02 time init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   /* 
   ---- Case ---------------------------------------------------------
   */

   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      abcorr = abcorrs[ coridx ];

      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Intercept";
         emethod = "Intercept";

         sprintf ( title, 
                   "subsol_pl02: find sub-solar point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                                      );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-solar point on the target.
         */
         subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                       abcorr, obsrvr,  spoint, &dist,  &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subsol_c ( emethod, target, et,    
                   abcorr,  obsrvr, xspoint ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compare sub-solar points planetocentric longitude/latitude.
         */
         reclat_c ( spoint,  &r,  &lon,  &lat );
         chckxc_c ( SPICEFALSE, " ", ok );

         reclat_c ( xspoint, &xr, &xlon, &xlat );
         chckxc_c ( SPICEFALSE, " ", ok );


         tol = MEDTOL;

         chcksd_c ( "lon", lon, "~/", xlon, tol, ok );

         tol = 5.e-7;

         chcksd_c ( "lat", lat, "~/", xlat, tol, ok );

         /*
         Check the distance between the points. Tolerance = 20cm.
         */
         tol = 2.0e-4;

         chckad_c (  "spoint", spoint, "~", xspoint, 3, tol, ok );


         /*
         Check the observer-subsolar point distance. We need to
         call a different CSPICE routine to get an expected value. 

         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subslr_c ( "Intercept: ellipsoid", target,  et,      fixref,
                     abcorr,    obsrvr,    xspoint, &trgepc,  srfvec ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Tolerance = 1m. 
         */
         tol = 1.0e-3;
         chcksd_c (  "dist", dist, "~", vnorm_c(srfvec), tol, ok );


         /* 
         ---- Case ---------------------------------------------------------
         */

         method  = "Ellipsoid Near Point";
         emethod = "Near Point";

         sprintf ( title, 
                   "subsol_pl02: find sub-solar point on Phobos. Method = "
                   "%s. Abcorr = %s.",
                   method,
                   abcorr                                                );

         tcase_c ( title );


         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";


         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-solar point on the target.
         */
         subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                       abcorr, obsrvr,  spoint, &dist,  &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subsol_c ( emethod, target, et,    
                   abcorr,  obsrvr,  xspoint ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compare sub-solar points' planetocentric longitude/latitude.

         Note that, due to the fact that the target is triaxial, not
         even longitudes will agree.
         */
         reclat_c ( spoint,  &r,  &lon,  &lat );
         chckxc_c ( SPICEFALSE, " ", ok );

         reclat_c ( xspoint, &xr, &xlon, &xlat );
         chckxc_c ( SPICEFALSE, " ", ok );

         tol = LOOSE;

         chcksd_c ( "lon", lon, "~/", xlon, tol, ok );
         chcksd_c ( "lat", lat, "~/", xlat, tol, ok );

         /*
         Check the distance between the points. Tolerance = 50cm.
         */
         tol = 5.0e-4;

         chckad_c (  "spoint", spoint, "~", xspoint, 3, tol, ok );

         /*
         Check the observer-subsolar point distance. We need to
         call a different CSPICE routine to get an expected value. 

         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subslr_c ( "Near point: ellipsoid", target,  et,      fixref,
                     abcorr,     obsrvr,     xspoint, &trgepc, srfvec ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Tolerance = 1m. 
         */
         tol = 1.0e-3;
         chcksd_c (  "dist", dist, "~", vnorm_c(srfvec), tol, ok );

      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */




   /*
    ******************************************************************
    *
    *
    *  illum_pl02 tests
    *
    *
    ******************************************************************
   */    


   /*
    ******************************************************************
    *
    *
    *  illum_pl02 error cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02 error test setup" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   method  = "Intercept";
 
   obsrvr  = "Mars";
   target  = "Phobos";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";
   et      = 0.0;

   /*
   Create a valid surface point.
   */
   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: spoint is the zero vector" );


   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );

   vpack_c  ( 0.0, 0.0, 0.0, xspoint );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                obsrvr, xspoint, &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(ZEROVECTOR)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: bad target name" );

   illum_pl02 ( dskhan, &dladsc, "xxx", et,     abcorr,
                obsrvr, spoint, &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: bad observer name" );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                "yyy",  spoint, &phase,  &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02 error: bad aberration correction" );

   illum_pl02 ( dskhan, &dladsc, target, et,     "zzz",
                obsrvr, spoint, &phase,  &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(SPKINVALIDOPTION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02 error: transmission aberration correction" );

   illum_pl02 ( dskhan, &dladsc, target, et,     "XCN",
                obsrvr, spoint, &phase,  &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: target and observer coincide" );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                target, spoint, &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: target doesn't match DSK segment" );

   illum_pl02 ( dskhan, &dladsc, "Saturn", et,  abcorr,
                obsrvr, spoint, &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(TARGETMISMATCH)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );

   chckxc_c( SPICEFALSE, " ", ok );
   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: no DSK loaded" );


   /*
   DAS can read data from buffered records even after the
   associated DAS file has been unloaded. To ensure we try
   to read from the file, we'll pick a surface point for
   which we don't have buffered topography data.
   */
   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   vminus_c ( spoint, xspoint );

   illum_pl02 ( dskhan, &dladsc,  target, et+2e4, abcorr,
                obsrvr, xspoint,  &phase, &solar, &emissn );
   
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: input string pointer is null" );


   illum_pl02 ( dskhan, &dladsc, NULL,   et,      abcorr,
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illum_pl02 ( dskhan, &dladsc, target, et,     NULL,
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                NULL,   spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02: input string is empty" );

   illum_pl02 ( dskhan, &dladsc, "",   et,      abcorr,
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illum_pl02 ( dskhan, &dladsc, target, et,     "",
                obsrvr, spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                "",   spoint,  &phase, &solar, &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



   /*
    ******************************************************************
    *
    *
    *  illum_pl02 normal cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_pl02 time init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   /* 
   ---- Case ---------------------------------------------------------
   */

   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      abcorr = abcorrs[ coridx ];

      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Ellipsoid near point";
         emethod = "near point";

         sprintf ( title, 
                   "illum_pl02: find illumination angles at "
                   "sub-solar point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                          );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-solar point on the target.
         */
         subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                       abcorr, obsrvr,  spoint, &dist,  &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute illumination angles at the sub-solar point on
         the DSK model. 
         */
         illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                      obsrvr, spoint,  &phase, &solar, &emissn );
         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subsol_c ( emethod, target, et,    
                    abcorr,  obsrvr, xspoint ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute illumination angles at the sub-solar point on
         the ellipsoidal model. 
         */
         illum_c ( target,  et,      abcorr,  obsrvr,
                   xspoint, &xphase, &xsolar, &xemssn ); 

         /*
         Compare illumination angles.

         We won't expect very close agreement, since the surface
         points at which the angles are computed are different, 
         and because the outward normal vectors at those points
         differ by even more.

         The phase angle doesn't involve the surface normal,
         so it should agree relatively well with that for the
         ellipsoid.          
         */
         tol = SINGLE;

         chcksd_c ( "phase",  phase,  "~/", xphase, tol, ok );

         /*
         We expect only very crude agreement for the angles that
         involve the surface normal. 
         */
         tol = 5.e-2;

         chcksd_c ( "emissn", emissn, "~/", xemssn, tol, ok ); 

         /*
         The solar incidence angle should be small at the sub-solar
         point, so use an absolute comparison. 
         */
         tol = 1.e-2;
         chcksd_c ( "solar",  solar,  "~", xsolar, tol, ok );


         /*
         Now for some more stringent tests: we'll compute the
         illumination angles explicitly and compare to those 
         returned by illum_pl02.

         Get the target-observer vector. We must start with the
         observer-target vector and negate it, then subtract
         the position of the surface point.
         */
         spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
         chckxc_c ( SPICEFALSE, " ", ok );

         if ( coridx == 0 ) 
         {
            trgepc = et;
         }
         else
         {
            trgepc = et - lt;
         }

         vminus_c ( trgpos, obspos );
         vsub_c   ( obspos, spoint, srfobs );

         spkpos_c ( "sun", trgepc, fixref, abcorr, target, sunpos, &ltsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the vector from the surface point to the sun. 
         */
         vsub_c ( sunpos,  spoint, srfsun );

         /*
         Get the outward normal vector for the plate containing 
         `spoint'. 
         */
         dskn02_c ( dskhan, &dladsc, plateID, normal );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the expected illumination angles. 
         */
         xphase = vsep_c ( srfobs, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xsolar = vsep_c ( normal, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xemssn = vsep_c ( normal, srfobs );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         The emission angle presents no numeric problems. 
         */
         tol = 1.e-11;
         chcksd_c ( "(2) emissn", emissn, "~/", xemssn, tol, ok ); 
         
         /*
         The solar incidence angle should be small. Look for 
         absolute agreement at the sub-nanoradian level. 
         */
         tol = TIGHT;
         chcksd_c ( "(2) solar",  solar,  "~",  xsolar, tol, ok ); 

         /*
         The phase angle should be numerically well-behaved. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) phase",  phase,  "~/", xphase, tol, ok );

      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */


   /* 
   ---- Case ---------------------------------------------------------
   */

   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      abcorr = abcorrs[ coridx ];

      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Ellipsoid near point";
         emethod = "near point";

         sprintf ( title, 
                   "illum_pl02: find illumination angles at "
                   "sub-Mars point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                        );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-observer point on the target.
         */
         subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                      abcorr, obsrvr,  spoint, &alt,   &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         printf( "DSK subpt = %f %f %f\n", spoint[0],spoint[1], spoint[2]);
         */

         /*
         Compute illumination angles at the sub-observer point on
         the DSK model. 
         */
         illum_pl02 ( dskhan, &dladsc, target, et,     abcorr,
                      obsrvr, spoint,  &phase, &solar, &emissn );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         printf("DSK angles: phase: %f solar: %f emissn: %f\n",
               phase, solar, emissn );
         */

         /*
         Find the sub-observer point on the target using an
         ellipsoidal model.
         */
         subpt_c ( emethod, target, et,    
                   abcorr,  obsrvr, xspoint, &alt ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         printf( "ell subpt = %f %f %f\n", xspoint[0],xspoint[1], xspoint[2]);
         */

         /*
         Compute illumination angles at the sub-observer point on
         the ellipsoidal model. 
         */
         illum_c ( target,  et,      abcorr,  obsrvr,
                   xspoint, &xphase, &xsolar, &xemssn ); 

         /*
         Compare illumination angles.

         We won't expect very close agreement, since the surface
         points at which the angles are computed are different, 
         and because the outward normal vectors at those points
         differ by even more.

         The phase angle doesn't involve the surface normal,
         so it should agree relatively well with that for the
         ellipsoid.          
         */
         tol = SINGLE;

         chcksd_c ( "phase",  phase,  "~/", xphase, tol, ok );

         /*
         We expect only very crude agreement for the angles that
         involve the surface normal. 
         */
         tol = 5.e-2;
         chcksd_c ( "solar",  solar,  "~/", xsolar, tol, ok );

         /*
         The emission angle should be small at the sub-solar
         point, so use an absolute comparison. 
         */
         tol = 2.e-2;

         chcksd_c ( "emissn", emissn, "~", xemssn, tol, ok ); 


         /*
         Now for some more stringent tests: we'll compute the
         illumination angles explicitly and compare to those 
         returned by illum_pl02.

         Get the target-observer vector. We must start with the
         observer-target vector and negate it, then subtract
         the position of the surface point.
         */
         spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
         chckxc_c ( SPICEFALSE, " ", ok );

         if ( coridx == 0 ) 
         {
            trgepc = et;
         }
         else
         {
            trgepc = et - lt;
         }

         vminus_c ( trgpos, obspos );
         vsub_c   ( obspos, spoint, srfobs );

         spkpos_c ( "sun", trgepc, fixref, abcorr, target, sunpos, &ltsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the vector from the surface point to the sun. 
         */
         vsub_c ( sunpos,  spoint, srfsun );

         /*
         Get the outward normal vector for the plate containing 
         `spoint'. 
         */
         dskn02_c ( dskhan, &dladsc, plateID, normal );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the expected illumination angles. 
         */
         xphase = vsep_c ( srfobs, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xsolar = vsep_c ( normal, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xemssn = vsep_c ( normal, srfobs );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         The solar incidence angle presents no numeric problems. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) solar", solar, "~/", xsolar, tol, ok ); 
         
         /*
         The emission angle should be small. Look for 
         absolute agreement at the sub-nanoradian level. 
         */
         tol = TIGHT;
         chcksd_c ( "(2) emissn",  emissn,  "~",  xemssn, tol, ok ); 

         /*
         The phase angle should be numerically well-behaved. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) phase",  phase,  "~/", xphase, tol, ok );

      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */



   /*
    ******************************************************************
    *
    *
    *  dskx02_c tests
    *
    *
    ******************************************************************
   */    

   /*
    ******************************************************************
    *
    *
    *  dskx02_pl02 error cases
    *
    *
    ******************************************************************
   */    
   
   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   This is a problem case. Return to it later. But don't forget it!
   */

   /*
   tcase_c ( "Bad file handle." );

   vpack_c  ( 0.0, 0.0, 1.0e4, vertex );
   vminus_c ( vertex, raydir );

   dskx02_c ( 0, &dladsc, vertex, raydir, &plateID, xpt, &found );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Direction vector is zero." );

   vpack_c ( 0.0, 0.0, 0.0, raydir );

   dskx02_c ( 0, &dladsc, vertex, raydir, &plateID, xpt, &found );
   chckxc_c ( SPICETRUE, "SPICE(RAYISZEROVECTOR)", ok );



   /*
    ******************************************************************
    *
    *
    *  dskx02_pl02 normal cases
    *
    *
    ******************************************************************
   */    
   


   /*
    ******************************************************************
    *
    *
    *  "Spear" intersection and non-intersection tests
    *
    *
    ******************************************************************
   */    
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskx02: spear test setup" );

   /*
   Spear test: we'll shoot a set of rays, each having a vertex far
   from the target center, at the target. 
   */


   /*
   Get the DSK descriptor from the segment. Fetch the maximum
   radius.
   */
   dskgd_c ( dskhan, &dladsc, &dskdsc );
   chckxc_c ( SPICEFALSE, " ", ok );

   r = dskdsc.co3max;

   /*
   Pick a vertex radius much larger than the target radius. 
   */
   r *= 1.e3;

   /*
   Loop over the lat and lon grid. 
   */
   nlat = 20;
   nlon = 50;

   dlat =   pi_c() / (nlat-1);
   dlon = 2*pi_c() / (nlon-1);

   for ( i = 0;  i < nlat;  i++  )
   {
      lat = halfpi_c() - i * dlat;

      for ( j = 0;  j < nlon;  j++ )
      {
         lon = j * dlon;

         /* 
         ---- Case ---------------------------------------------------------
         */
         sprintf ( title, "Spear test for lat = %f (deg) and "
                          "lon = %f (deg).", 
                          lat * dpr_c(), 
                          lon * dpr_c() );
         tcase_c ( title);


         /*
         Create the ray vertex and direction.
         */
         latrec_c ( r, lon, lat, vertex );

         vminus_c ( vertex, raydir );
         
         vmag = vnorm_c( vertex );

         /*
         Find the ray-surface intercept. 
         */
         dskx02_c ( dskhan, &dladsc, vertex, raydir, &plateID, xpt, &found );
         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         We expect to always find the intercept. 
         */
         chcksl_c ( "found", found, SPICETRUE, ok );

         

         if ( found )
         {
            /*
            The longitude and latitude of the intercept should match 
            those of the vertex.
            */
            reclat_c ( xpt, &xpr, &xplon, &xplat );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( xplon - lon > pi_c() )
            {
               xplon -= twopi_c();
            }
            if ( lon - xplon > pi_c() )
            {
               xplon += twopi_c();
            }

            tol = TIGHT;

            /*
            Longitude is undefined at the poles. Check it for  
            all other latitudes.
            */
            if (  fabs(lat)  <  ( halfpi_c() - 1.e-6 )  )
            {
               chcksd_c ( "xplon", xplon, "~", lon, tol, ok );
            }

            chcksd_c ( "xplat", xplat, "~", lat, tol, ok );

            /*
            Check the radial position of the intercept. 
            */
            vsub_c ( xpt, vertex, srfvec );

            tol = MEDTOL;

            chcksd_c ( "xpr", xpr, "~", (vmag - vnorm_c(srfvec)), tol, ok );

            /*
            Check the outward normal on the intercept plate. It must 
            not point more than 90 degrees away from the vertex. 
            */
            dskn02_c ( dskhan, &dladsc, plateID, normal );
            chckxc_c ( SPICEFALSE, " ", ok );

            sep = pi_c() - vsep_c ( normal, srfvec );

            chcksd_c ( "sep", sep, "<", halfpi_c(), 0.0, ok );

         }

         /* 
         ---- Case ---------------------------------------------------------
         */     
         sprintf ( title, "Spear non-intersection test for lat = %f (deg) and "
                          "lon = %f (deg).", 
                          lat * dpr_c(), 
                          lon * dpr_c() );
         tcase_c ( title);      

         /*
         Create the ray vertex and direction.
         */
         latrec_c ( r, lon, lat, vertex );

         /*
         The ray direction is parallel to the vertex. 
         */        
         vequ_c ( vertex, raydir );
 
         /*
         Find the ray-surface intercept. 
         */                 
         dskx02_c ( dskhan, &dladsc, vertex, raydir, &plateID, xpt, &found );
         chckxc_c ( SPICEFALSE, " ", ok );       

         /*
         We  expect never to find the intercept. 
         */      
         chcksl_c ( "found", found, SPICEFALSE, ok );
         
         if ( ( i == 1 ) || ( i == nlat-1 ) )
         {
            /*
            This is a polar case.

            Don't perform redundant tests for this latitude value. 
            */
            break;
         }

      }
   }



   /*
    ******************************************************************
    *
    *
    *  Limb-finding tests
    *
    *
    ******************************************************************
   */    
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskx02: limb test setup" );

   /*
   Limb test: for a comprehensive set of viewing points, we'll
   find sparse sets of limb points. Limb points will be found
   via binary searches conducted in planes containing the viewing
   point and target center.
   */

   /*
   Get the radii of the target. 
   */
   bodvrd_c ( "Phobos", "RADII", 3, &n, radii );
 
   /*
   Get the DSK descriptor from the segment. Fetch the maximum
   radius.
   */
   dskgd_c ( dskhan, &dladsc, &dskdsc );
   chckxc_c ( SPICEFALSE, " ", ok );

   r = dskdsc.co3max;

   /*
   Pick a vertex radius on scale similar to that of the target radius. 
   */
   r *= 3;


   /*
   Loop over the lat and lon grid. 
   */
   nlat =  7;
   nlon =  8;

   dlat =   pi_c() / (nlat-1);
   dlon = 2*pi_c() / (nlon-1);

   for ( i = 0;  i < nlat;  i++  )
   {
      lat = halfpi_c() - i * dlat;

      for ( j = 0;  j < nlon;  j++ )
      {
         lon = j * dlon;

         /* 
         ---- Case ---------------------------------------------------------
         */
         sprintf ( title, "Limb test for lat = %f (deg) and "
                          "lon = %f (deg).", 
                          lat * dpr_c(), 
                          lon * dpr_c() );
         tcase_c ( title);

         /*
         Create the viewing point and direction toward the 
         target center.
         */
         latrec_c ( r, lon, lat, vertex );

         vminus_c ( vertex, raydir );

         /*
         Find the limb of the reference ellipsoid. 
         */
         edlimb_c ( radii[0], radii[1], radii[2], vertex, &limb );

         /*
         Build a frame in which to compute the limb as
         seen from `vertex'. Caution: the first argument
         of `frame' is in-out.
         */
         vequ_c( vertex, x );

         frame_c ( x, y, z );
 
         /*
         Compute NPTS limb points for the current vertex.
         */
         npoints = 5;

         delta   = twopi_c() / npoints;
 
         for ( k = 0;  k < npoints;  k++ )
         { 
            theta = (k-1)*delta;
 
            /*
            We'll search for a limb point on the DSK surface within the
            plane containing `vertex', the target center, and `up':
            */
            vlcom_c( cos(theta), y, sin(theta), z, up );
 
            /*
            `normal' is the axis about which we'll rotate
            test vectors:
            */
            ucrss_c ( up, vertex, normal );
 
            /*
            Let `minang' and `maxang' bound the set of angles
            by which we'll rotate `raydir'.
            */
            minang = asin( radii[2] / r );
            maxang = asin( radii[0] / r );

            /*
            Solve for the limb angle by bisection:
            */
            lower  = minang * 0.5;
            upper  = maxang * 1.5;

            diff   = upper - lower;
            n      = 0;
            cnvtol = 1.e-13;
            
            while( fabs(diff) > cnvtol )
            {
               ++n;

               /*
               Produce the test ray direction.
               */
               midpt = ( upper + lower ) / 2;

               vrotv_c ( raydir, normal, midpt, tstdir );

               /*
               Does the test ray hit the object?
               */ 
               dskx02_c ( dskhan,   &dladsc, vertex, tstdir, 
                          &plateID, xpt,     &found         );
               chckxc_c ( SPICEFALSE, " ", ok );       

               if ( found )
               {
                  /*
                  `midpt' is too low.
                  */
                  lower = midpt;
               }
               else
               {
                  /*
                  `midpt' is too high.
                  */
                  upper = midpt;
               }

               diff = upper - lower;

               if ( n > MAXITR )
               {
                  setmsg_c ( "Limb point loop has reached # "
                             "iterations. Lat (deg) = #; Lon (deg) "
                             "= #; theta (deg) = #."                );
                  errint_c ( "#", n                                 );
                  errdp_c  ( "#", lat   * dpr_c()                   );
                  errdp_c  ( "#", lon   * dpr_c()                   );
                  errdp_c  ( "#", theta * dpr_c()                   );
                  sigerr_c ( "SPICE(BUG)"                           );

                  chckxc_c ( SPICEFALSE, " ", ok );                       
               }
            } 
 
            /*
            The limb point is `xpt', which was the last intercept
            found. Find the distance of `xpt' from the limb ellipse.
            */
            npelpt_c ( xpt, &limb, npoint, &dist );

            /*
            We're looking for agreement at the 100m level. We
            expect only loose agreement because limb point location
            is very sensitive to the shape of the target.
            */
            sprintf ( label, "dist %ld", (long)k );

            tol = 0.1;

            chcksd_c ( label, dist, "~", 0.0, tol, ok );


            /*
            Verify that shifting `xpt' slightly in the "up" direction
            produces a point such that a ray emanating from the vertex
            and passing through this point misses the target.

            Verify that the opposite shift produces a point within
            the target body.

            Note that the convergence tolerance applies to an angle,
            whereas the shift is a distance.            
            */            
            vlcom_c ( 1.0, xpt, 1.e-6, up, npoint );

            vsub_c  ( npoint, vertex, tstdir );

            dskx02_c ( dskhan,   &dladsc, vertex, tstdir, 
                       &plateID, xpt,     &found         );
            chckxc_c ( SPICEFALSE, " ", ok );       

            sprintf ( label, "found (up) %ld", (long)k );

            chcksl_c ( label, found, SPICEFALSE, ok );
 

            vlcom_c ( 1.0, xpt, -1.e-6, up, npoint );

            vsub_c  ( npoint, vertex, tstdir );

            dskx02_c ( dskhan,   &dladsc, vertex, tstdir, 
                       &plateID, xpt,     &found         );
            chckxc_c ( SPICEFALSE, " ", ok );       

            sprintf ( label, "found (down) %ld", (long)k );

            chcksl_c ( label, found, SPICETRUE, ok );
         }
         /*
         End of limb point tests for current lat/lon. 
         */

         if ( ( i == 1 ) || ( i == nlat-1 ) )
         {
            /*
            This is a polar case.

            Don't perform redundant tests for this latitude value. 
            */
            break;
         }
      }
      /*
      End of longitude loop. 
      */
   }
   /*
   End of longitude loop. 
   */



   /*
    ******************************************************************
    *
    *
    *  llgrid_pl02 tests
    *
    *
    ******************************************************************
    */    

   /*
    ******************************************************************
    *
    *
    *  llgrid_pl02 error cases
    *
    *
    ******************************************************************
   */    
   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   tcase_c ( "Bad file handle." );
   */

   /*
   This is a problem case. Return to it later. But don't forget it!
   */

   /*
  
   npoints = 3;
  
   llgrid_pl02 ( 0, &dladsc, npoints, (CONS2D)grid, spoints, plateIDs );
   chckxc_c    ( SPICETRUE, "SPICE(INVALIDHANDLE)", ok );
   */

   /*
    ******************************************************************
    *
    *
    *  llgrid_pl02 normal cases
    *
    *
    ******************************************************************
   */    


   /*
   The macro below will suppress compiler warnings about a
   type mismatch for the `grid' argument.
   */
   #define CONS2D ConstSpiceDouble (*) [2]
 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Generate surface points on a 10x20 grid." );

   /*
   Loop over the lat and lon grid. 
   */
   
   nlat    = 10;
   nlon    = 20;
   npoints = nlat * nlon;

   dlat =   pi_c() / (nlat-1);
   dlon = 2*pi_c() / (nlon-1);

   k = 0;

   for ( i = 0;  i < nlat;  i++  )
   {
      lat = halfpi_c() - i * dlat;

      for ( j = 0;  j < nlon;  j++ )
      {
         lon = MEDTOL + j * dlon;

         grid[k][0] = lon;
         grid[k][1] = lat;

         k++;
      }
   }

   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );

   /*
   Get the DSK descriptor from the segment. Fetch the maximum
   radius.
   */
   dskgd_c ( dskhan, &dladsc, &dskdsc );
   chckxc_c ( SPICEFALSE, " ", ok );

   r = dskdsc.co3max;


   /*
   Pick a vertex radius somewhat larger than the target radius. 
   */
   r *= 2;

   /*
   Generate surface points. 
   */
   llgrid_pl02 ( dskhan, &dladsc, npoints, (CONS2D)grid, spoints, plateIDs );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Use dskx02_c to check the surface points and plates from llgrid_pl02. 
   */
   for ( k = 0;  k < npoints;  k++ )
   {
      /*
      Find the latitude and longitude corresponding to the ith point. 
      */
      i    = k / nlon;
      j    = k % nlon;
      
      xlat = halfpi_c() - i * dlat;
      xlon = MEDTOL + j * dlon;

      /* 
      ---- Case ---------------------------------------------------------
      */
      sprintf ( title, "llgrid_pl02 test for lat = %f (deg) and "
                        "lon = %f (deg).", 
                        xlat * dpr_c(), 
                        xlon * dpr_c() );
      tcase_c ( title);

      latrec_c ( r, xlon, xlat, vertex );
      vminus_c ( vertex,        raydir );
      
      dskx02_c ( dskhan, &dladsc, vertex, raydir, &xPlateID, xspoint, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "found", found, SPICETRUE, ok );

      /*
      Check the plate ID for vertices other than those at the poles.
      At the poles, round-off can generate unpredictable selections
      of plates constituting the polar caps.
      */
      
      if (  ( i > 0 ) && ( i < (nlat-1) )  ) 
      {
         chcksi_c ( "plateID", plateIDs[k], "=", xPlateID, 0, ok );
      }

      /*
      All of the vertices should be pretty close to what dskx02_c
      can generate. 
      */
      tol = 3 * TIGHT;

      chckad_c ( "spoint", spoints[k], "~~/", xspoint, 3, tol, ok );

   }




   /*
    ******************************************************************
    *
    *
    *  limb_pl02 tests
    *
    *
    ******************************************************************
   */    

   /*
    ******************************************************************
    *
    *
    *  limb_pl02 error cases
    *
    *
    ******************************************************************
   */
    
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: error test setup" );

   target  = "Phobos";
   obsrvr  = "Mars";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";

   npoints = 3;

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad aberration correction" );


   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  "XX",    obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(SPKINVALIDOPTION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad target" );


   limb_pl02 ( dskhan,  &dladsc, "XXX",   et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad observer" );


   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  "XXX",   npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: target coincides with observer" );


   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  target,   npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: target doesn't match DSK descriptor" );


   limb_pl02 ( dskhan,        &dladsc, "Saturn", et, 
               "IAU_SATURN",  abcorr,  obsrvr,   npoints, 
               &trgepc,       obspos,  limbpts,  plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(DSKTARGETMISMATCH)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad observer" );


   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  "YYY",   npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad frame name" );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               "ZZZ",   abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(UNKNOWNFRAME)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad frame center" );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               "J2000", abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFRAME)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad value of npoints" );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  0, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDCOUNT)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  -1, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDCOUNT)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad semi-axis lengths" );

   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   vpack_c ( -1.0, radii[1], radii[2], badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(DEGENERATECASE)", ok );

   vpack_c ( radii[0], 0.0, radii[2], badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(DEGENERATECASE)", ok );

   vpack_c ( radii[0], radii[1], 0.0, badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(DEGENERATECASE)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: target radii not in kernel pool" );


   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   dvpool_c ( kvname );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );

   chckxc_c ( SPICETRUE, "SPICE(KERNELVARNOTFOUND)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );

   chckxc_c( SPICEFALSE, " ", ok );
   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: no DSK loaded" );

   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: input string pointer is null" );


   limb_pl02 ( dskhan,  &dladsc, NULL,    et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               NULL,    abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  NULL,    obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  NULL,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: input string is empty" );


   limb_pl02 ( dskhan,  &dladsc, "",      et, 
               fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               "",      abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  "",      obsrvr,  npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   limb_pl02 ( dskhan,  &dladsc, target,  et, 
               fixref,  abcorr,  "",    npoints, 
               &trgepc, obspos,  limbpts, plateIDs );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


 
   /*
    ******************************************************************
    *
    *
    *  limb_pl02 normal cases
    *
    *
    ******************************************************************
   */    
 

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02 setup" );

   target  = "Phobos";
   obsrvr  = "Mars";
   fixref  = "IAU_PHOBOS";

   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta  = jyear_c()/10;

   npoints = 10;

   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );
 
   /*
   Generate limbs as seen from a fixed observer, for a sequence
   of input times.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
  
   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et     = et0 + timidx*tdelta;

         abcorr = abcorrs[ coridx ];


         /* 
         ---- Case -------------------------------------------------------
         */
         sprintf ( title, 
                   "limb_pl02: with Mars as a viewing location, "
                   "find a set of limb points on Phobos. "
                   "Abcorr = %s. ET = %25.17e. ",
                   abcorr,
                   et                                          );

         tcase_c ( title);


         /*
         Compute a set of limb points for the current 
         viewing geometry.
         */
         limb_pl02 ( dskhan,  &dladsc, target,  et, 
                     fixref,  abcorr,  obsrvr,  npoints, 
                     &trgepc, obspos,  limbpts, plateIDs );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We'll first check the limb points by mapping each one to
         the "intercept" near point on the reference ellipsoid.
         Those near points should have emission angles very close
         to 90 degrees.

         For the test plate model we're using, the near points and
         the corresponding limb points should be close together.
         */
         for ( k = 0;  k < npoints;  k++ )
         {
            /*
            Find the level surface parameter of the limb point. 
            */
            level =    pow(  limbpts[k][0]/radii[0],  2 )
                    +  pow(  limbpts[k][1]/radii[1],  2 )
                    +  pow(  limbpts[k][2]/radii[2],  2 );

            if ( level <= 0.0 ) 
            {
               setmsg_c ( "Limb point level should be strictly "
                          "positive but was #. Index was #."    );
               errdp_c  ( "#", level                            );
               errint_c ( "#", k                                );
               sigerr_c ( "SPICE(TESTBUG)"                      );

               chckxc_c ( SPICEFALSE, " ", ok );
            }

            /*
            Find the intercept point on the reference ellipsoid of
            the ray emanating from the origin and containing the 
            limb point.
            */              
            vscl_c ( 1.0 / sqrt(level), limbpts[k], npoint );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Check the distance between the limb point and the 
            near point. Look for agreement at the 1 meter level. 
            */
            tol = 1.e-3;

            dist = vdist_c( limbpts[k], npoint );

            sprintf ( label, "dist %ld", (long)k );

            chcksd_c ( label, dist, "~", 0.0, tol, ok );

            /*
            Find the illumination angles at the ellipsoid near point. 

            We use illum_c rather than ilumin_c for compatibility
            of light time computations.
            */
            illum_c ( target, et,     abcorr, obsrvr, npoint, 
                      &phase, &solar, &emissn                 );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Presuming we have the correct point on the ellipsoid,
            the emission angle should be very close to pi/2. 
            */
            tol = 1.e-10;

            sprintf ( label, "emissn %ld", (long)k );

            chcksd_c ( label, emissn, "~/", halfpi_c(), tol, ok );

            /*
            Now check the plate returned along with the limb point.
            We can use llgrid_pl02 to generate an expected plate ID.

            We don't expect plate IDs to match for points at the poles,
            but we probably won't have any limb points at the poles.
            The test will need to be updated if any such points are
            found.
            */
            reclat_c ( limbpts[k], &xr, &xlon, &xlat );
            chckxc_c ( SPICEFALSE, " ", ok );

            grid[0][0] = xlon;
            grid[0][1] = xlat;

            llgrid_pl02 ( dskhan,  &dladsc, 1, (CONS2D)grid, 
                          spoints, &xPlateID                );
            chckxc_c ( SPICEFALSE, " ", ok );


            sprintf ( label, "plateID %ld", (long)k );

            chcksi_c ( label, plateIDs[k], "=", xPlateID, 0, ok );
         }

         /*
         Check the angular spacing of the limb points about
         the observer-target center line. The spacing should be
         uniform.

         We'll need a reference frame having its z-axis parallel
         to the observer-target vector.
         */
         spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
         chckxc_c ( SPICEFALSE, " ", ok );

         vminus_c ( trgpos, obspos );

         twovec_c ( obspos, 3, limbpts[0], 1, cylmat );

         angdel = twopi_c() / npoints;

         for ( k = 0;  k < npoints;  k++ )
         {
            mxv_c ( cylmat, limbpts[k], cylvec );

            reccyl_c ( cylvec, &vr, &vlon, &vz );

            /*printf ( "k = %ld, vlon (deg) %f\n", 
            (long)k, vlon*dpr_c() );*/

            xlon = k * angdel;

            if ( xlon-vlon > pi_c() )
            {
               xlon -= twopi_c();
            }
            else if ( vlon-xlon > pi_c() )
            {
               xlon += twopi_c();              
            }

            sprintf ( label, "vlon %ld", (long)k );

            tol = VTIGHT;

            chcksd_c ( label, vlon, "~", xlon, tol, ok );
         }
      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */




   /*
    ******************************************************************
    *
    *
    *  term_pl02 tests
    *
    *
    ******************************************************************
   */    

   /*
    ******************************************************************
    *
    *
    *  term_pl02 error tests
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: error test setup" );

   source  = "Sun";
   trmtyp  = "UMBRAL";
   target  = "Phobos";
   obsrvr  = "Mars";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";
  
   npoints = 3;

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad terminator type" );

   
   term_pl02 ( dskhan,  &dladsc, "ZZ",    source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad aberration correction" );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  "YY",    obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(SPKINVALIDOPTION)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad target name" );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  "XX",
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad observer name" );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  "YY",    npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NOTRANSLATION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad source name" );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  "xx",    target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NOTRANSLATION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad frame center" );

   term_pl02 ( dskhan,  &dladsc,       trmtyp,  source,  target,
               et,      "IAU_SATURN",  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,        termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFIXREF)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "limb_pl02: bad value of npoints" );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  0, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSIZE)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  -1, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSIZE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: target doesn't match DSK descriptor" );

   term_pl02 ( dskhan,  &dladsc,      trmtyp,  source,  "Saturn",
               et,      "IAU_SATURN", abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,       termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(DSKTARGETMISMATCH)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: bad semi-axis lengths" );

   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   vpack_c ( -1.0, radii[1], radii[2], badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDAXISLENGTH)", ok );

   vpack_c ( radii[0], 0.0, radii[2], badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDAXISLENGTH)", ok );

   vpack_c ( radii[0], radii[1], 0.0, badrad );

   pdpool_c ( kvname, 3, badrad );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDAXISLENGTH)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: target radii not in kernel pool" );


   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );

   kvname = "BODY401_RADII";

   dvpool_c ( kvname );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(KERNELVARNOTFOUND)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: source radii not in kernel pool" );


   bodvrd_c ( "SUN", "RADII", 3, &n, radii );

   kvname = "BODY10_RADII";

   dvpool_c ( kvname );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(KERNELVARNOTFOUND)", ok );


   pdpool_c ( kvname, 3, radii );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );

   chckxc_c( SPICEFALSE, " ", ok );
   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: no DSK loaded" );

   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: input string pointer is null" );

   term_pl02 ( dskhan,  &dladsc, NULL,    source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  NULL,    target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  NULL,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      NULL,    abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  NULL,    obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  NULL,    npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02: input string is empty" );


   term_pl02 ( dskhan,  &dladsc, "",      source,  target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  "",      target,
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  "",  
               et,      fixref,  abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      "",      abcorr,  obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  "",      obsrvr,  npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   term_pl02 ( dskhan,  &dladsc, trmtyp,  source,  target,
               et,      fixref,  abcorr,  "",      npoints, 
               &trgepc, obspos,  termpts, plateIDs         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
    ******************************************************************
    *
    *
    *  term_pl02 normal tests
    *
    *
    ******************************************************************
   */    



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "term_pl02 setup" );

   target  = "Phobos";
   obsrvr  = "Mars";
   source  = "Sun";
   fixref  = "IAU_PHOBOS";

   bodvrd_c ( "PHOBOS", "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta  = jyear_c()/10;

   npoints = 10;

   bodvrd_c ( source, "RADII", 3, &n, srcrad );
   

   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );
 
   /*
   Generate umbral terminators as seen from a fixed observer, for a
   sequence of input times.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
  
   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et     = et0 + timidx*tdelta;

         abcorr = abcorrs[ coridx ];


         /* 
         ---- Case -------------------------------------------------------
         */
         sprintf ( title, 
                   "term_pl02: with Mars as a viewing location, "
                   "find a set of UMBRAL terminator points on Phobos. "
                   "Abcorr = %s. ET = %25.17e.",
                   abcorr,
                   et          );

         tcase_c ( title);


         /*
         Compute a set of limb points for the current 
         viewing geometry.
         */
         term_pl02 ( dskhan,  &dladsc, "UMBRAL", source,  target,  
                     et,      fixref,  abcorr,   obsrvr,  npoints, 
                     &trgepc, obspos,  termpts,  plateIDs         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We'll first check the terminator points by mapping each one to
         the "intercept" near point on the reference ellipsoid.
         Those near points should have solar incidence angles very close
         to 90 degrees + adj, where adj is an adjustment angle that 
         accounts for the angular radius of the light source.

         For the test plate model we're using, the near points and
         the corresponding terminator points should be close together.
         */
         for ( k = 0;  k < npoints;  k++ )
         {
            /*
            Find the level surface parameter of the terminator point. 
            */
            level =    pow(  limbpts[k][0]/radii[0],  2 )
                    +  pow(  limbpts[k][1]/radii[1],  2 )
                    +  pow(  limbpts[k][2]/radii[2],  2 );

            if ( level <= 0.0 ) 
            {
               setmsg_c ( "Terminator point level should be strictly "
                          "positive but was #. Index was #."          );
               errdp_c  ( "#", level                                  );
               errint_c ( "#", k                                      );
               sigerr_c ( "SPICE(TESTBUG)"                            );

               chckxc_c ( SPICEFALSE, " ", ok );
            }

            /*
            Find the intercept point on the reference ellipsoid of
            the ray emanating from the origin and containing the 
            terminator point.
            */              
            vscl_c ( 1.0 / sqrt(level), termpts[k], npoint );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Check the distance between the terminator point and the 
            near point. Look for agreement at the 1 meter level. 
            */
            tol = 1.e-3;

            dist = vdist_c( termpts[k], npoint );

            sprintf ( label, "dist %ld", (long)k );

            chcksd_c ( label, dist, "~", 0.0, tol, ok );

            /*
            Find the illumination angles at the ellipsoid near point. 

            We use illum_c rather than ilumin_c for compatibility
            of light time computations.
            */
            illum_c ( target, et,     abcorr, obsrvr, npoint, 
                      &phase, &solar, &emissn                 );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Estimate the angular radius of the light source as
            seen from the terminator point.
            */
            spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( coridx == 0 ) 
            {
               trgepc = et;
            }
            else
            {
               trgepc = et - lt;
            }

            spkpos_c ( source, trgepc, "J2000", 
                       abcorr, target, srcpos, &srclt );
            chckxc_c ( SPICEFALSE, " ", ok );

            vsub_c ( srcpos, npoint, srcvec );

            srcdst = vnorm_c( srcvec );

            srcang = asin ( srcrad[0] / srcdst );

            /*
            For points on the umbral terminator, the incidence angle
            is increased by approximately the angular radius of the 
            light source.
            */
            xsolar = halfpi_c() + srcang;

            tol    = MEDTOL;

            sprintf ( label, "solar %ld", (long)k );

            chcksd_c ( label, solar, "~/", xsolar, tol, ok );

            /*
            Now check the plate returned along with the terminator
            point. We can use llgrid_pl02 to generate an expected plate
            ID.
 
            We don't expect plate IDs to match for points at the poles,
            but we probably won't have any terminator points at the
            poles. The test will need to be updated if any such points
            are found.
            */
            reclat_c ( termpts[k], &xr, &xlon, &xlat );
            chckxc_c ( SPICEFALSE, " ", ok );

            grid[0][0] = xlon;
            grid[0][1] = xlat;

            llgrid_pl02 ( dskhan,  &dladsc, 1, (CONS2D)grid, 
                          spoints, &xPlateID                );
            chckxc_c ( SPICEFALSE, " ", ok );


            sprintf ( label, "plateID %ld", (long)k );

            chcksi_c ( label, plateIDs[k], "=", xPlateID, 0, ok );
         }

 
      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */



   /*
   Generate penumbral terminators as seen from a fixed observer, for a
   sequence of input times.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
  
   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et     = et0 + timidx*tdelta;

         abcorr = abcorrs[ coridx ];


         /* 
         ---- Case -------------------------------------------------------
         */
         sprintf ( title, 
                   "term_pl02: with Mars as a viewing location, "
                   "find a set of PENUMBRAL terminator points on Phobos. "
                   "Abcorr = %s. ET = %25.17e.",
                   abcorr,
                   et         );

         tcase_c ( title);


         /*
         Compute a set of limb points for the current 
         viewing geometry.
         */
         term_pl02 ( dskhan,  &dladsc, "PENUMBRAL", source,  target,  
                     et,      fixref,  abcorr,      obsrvr,  npoints, 
                     &trgepc, obspos,  termpts,     plateIDs         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We'll first check the terminator points by mapping each one to
         the "intercept" near point on the reference ellipsoid.
         Those near points should have solar incidence angles very close
         to 90 degrees + adj, where adj is an adjustment angle that 
         accounts for the angular radius of the light source.

         For the test plate model we're using, the near points and
         the corresponding terminator points should be close together.
         */
         for ( k = 0;  k < npoints;  k++ )
         {
            /*
            Find the level surface parameter of the terminator point. 
            */
            level =    pow(  limbpts[k][0]/radii[0],  2 )
                    +  pow(  limbpts[k][1]/radii[1],  2 )
                    +  pow(  limbpts[k][2]/radii[2],  2 );

            if ( level <= 0.0 ) 
            {
               setmsg_c ( "Terminator point level should be strictly "
                          "positive but was #. Index was #."          );
               errdp_c  ( "#", level                                  );
               errint_c ( "#", k                                      );
               sigerr_c ( "SPICE(TESTBUG)"                            );

               chckxc_c ( SPICEFALSE, " ", ok );
            }

            /*
            Find the intercept point on the reference ellipsoid of
            the ray emanating from the origin and containing the 
            terminator point.
            */              
            vscl_c ( 1.0 / sqrt(level), termpts[k], npoint );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Check the distance between the terminator point and the 
            near point. Look for agreement at the 1 meter level. 
            */
            tol = 1.e-3;

            dist = vdist_c( termpts[k], npoint );

            sprintf ( label, "dist %ld", (long)k );

            chcksd_c ( label, dist, "~", 0.0, tol, ok );

            /*
            Find the illumination angles at the ellipsoid near point. 

            We use illum_c rather than ilumin_c for compatibility
            of light time computations.
            */
            illum_c ( target, et,     abcorr, obsrvr, npoint, 
                      &phase, &solar, &emissn                 );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Estimate the angular radius of the light source as
            seen from the terminator point.
            */
            spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( coridx == 0 ) 
            {
               trgepc = et;
            }
            else
            {
               trgepc = et - lt;
            }

            spkpos_c ( source, trgepc, "J2000", 
                       abcorr, target, srcpos, &srclt );
            chckxc_c ( SPICEFALSE, " ", ok );

            vsub_c ( srcpos, npoint, srcvec );

            srcdst = vnorm_c( srcvec );

            srcang = asin ( srcrad[0] / srcdst );

            /*
            For points on the penumbral terminator, the incidence angle
            is decreased by approximately the angular radius of the 
            light source.
            */
            xsolar = halfpi_c() - srcang;

            tol    = MEDTOL;

            sprintf ( label, "solar %ld", (long)k );

            chcksd_c ( label, solar, "~/", xsolar, tol, ok );

            /*
            Now check the plate returned along with the terminator
            point. We can use llgrid_pl02 to generate an expected plate
            ID.
 
            We don't expect plate IDs to match for points at the poles,
            but we probably won't have any terminator points at the
            poles. The test will need to be updated if any such points
            are found.
            */
            reclat_c ( termpts[k], &xr, &xlon, &xlat );
            chckxc_c ( SPICEFALSE, " ", ok );

            grid[0][0] = xlon;
            grid[0][1] = xlat;

            llgrid_pl02 ( dskhan,  &dladsc, 1, (CONS2D)grid, 
                          spoints, &xPlateID                );
            chckxc_c ( SPICEFALSE, " ", ok );


            sprintf ( label, "plateID %ld", (long)k );

            chcksi_c ( label, plateIDs[k], "=", xPlateID, 0, ok );
         }
 
      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */




   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Clean up kernels." );  

   
   /*
   Unload and delete our test SPK file. 
   */
   spkuef_c ( spkhan );
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile   ( SPK0 );

   /*
   Clean up text PCK. 
   */
   kclear_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile ( PCK0 );


   /*
   Unload and delete our test Phobos DSK file. 
   */
   dascls_c ( dskhan );
   chckxc_c ( SPICEFALSE, " ", ok );
  
   removeFile   ( DSKPHB );  
  
      
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dskg02_c */

