/*

-Procedure f_dskg02b_c ( Test DSK type 2 geometry routines )

 
-Abstract
 
   Perform tests on the DSKLIB_C type 2 geometry routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"


   void f_dskg02b_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Perform tests on the DSKLIB_C type 2 geometry routines.
   Routines covered by this family:

      illum_plid_pl02
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version 

   -tspice_c Version 1.0.0 26-JUL-2016 (NJB) 

      Now uses t_cg_c. References to include files have been
      updated.

      09-SEP-2014 (NJB) 
 
         Original version.


-Index_Entries

   test dsk type_2 geometry routines, part b

-&
*/

{ /* Begin f_dskg02b_c */

 


   /* 
   Constants
   */
   #define                 DSKSOC  "dskg02b_phobos_soc.bds"
   #define                 DSKPHB  "dskg02b_phobos.bds"
   #define                 PCK0    "dskg02b_test.tpc"
   #define                 SPK0    "dskg02b_test.bsp"
    
   #define                 LOOSE   1.0e-4
   #define                 SINGLE  1.0e-6
   #define                 MEDTOL  1.0e-8
   #define                 TIGHT   1.0e-12
   #define                 VTIGHT  1.0e-14
   #define                 NCORR   5
   #define                 NTIMES  100
   #define                 MSGLEN  401
   #define                 MAXPNT  200
   #define                 MAXITR  200
   #define                 LNSIZE  81

   /*
   Local variables
   */
   SpiceBoolean            found;
   SpiceBoolean            lit;
   SpiceBoolean            visible;
   SpiceBoolean            xlit;
   SpiceBoolean            xvisible;

   SpiceChar             * abcorr;
   SpiceChar             * abcorrs[] = { "NONE", "CN+S", "CN", "LT", "LT+S" };
   SpiceChar             * emethod;
   SpiceChar             * fixref;
   SpiceChar             * obsrvr;
   SpiceChar             * method;
   SpiceChar             * surfce;
   SpiceChar             * target;
   SpiceChar               title  [ MSGLEN ];

   SpiceDLADescr           dladsc;

   SpiceDouble             alt;
   SpiceDouble             dist;
   SpiceDouble             emissn;
   SpiceDouble             et0;
   SpiceDouble             et;
   SpiceDouble             lt;
   SpiceDouble             ltsun;
   SpiceDouble             normal [3];
   SpiceDouble             obsvec [3];
   SpiceDouble             phase;
   SpiceDouble             raydir [3];
   SpiceDouble             solar;
   SpiceDouble             spoint [3];
   SpiceDouble             srfobs [3];
   SpiceDouble             srfsun [3];
   SpiceDouble             srfvec [3];
   SpiceDouble             sunsta [6];
   SpiceDouble             sunvec [3];
   SpiceDouble             tdelta;
   SpiceDouble             tol;
   SpiceDouble             trgepc;
   SpiceDouble             trgpos [3];   
   SpiceDouble             trgsta [6];   
   SpiceDouble             verts  [3][3];
   SpiceDouble             xemssn;
   SpiceDouble             xphase;
   SpiceDouble             xpt    [3];
   SpiceDouble             xsolar;
   SpiceDouble             xspoint[3];
   SpiceDouble             xtrgepc;


   SpiceInt                coridx;
   SpiceInt                dskhan;
   SpiceInt                dskhan1;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                n;
   SpiceInt                np;
   SpiceInt                nv;
   SpiceInt                plate  [3];
   SpiceInt                plateID;
   SpiceInt                plateID1;
   SpiceInt                spkhan;
   SpiceInt                targid;
   SpiceInt                timidx;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dskg02b_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );  

   /*
   Create and load the generic test kernels. 
   */

   
   if ( exists_c(SPK0) )
   {
      removeFile( SPK0 );
   }
   
   tstspk_c ( SPK0, SPICETRUE, &spkhan );   
   chckxc_c( SPICEFALSE, " ", ok );
   

   /*
   Keep PCK after loading it. 
   */
   tstpck_c ( PCK0, SPICETRUE, SPICETRUE );
   chckxc_c( SPICEFALSE, " ", ok );

   tstlsk_c();
   chckxc_c( SPICEFALSE, " ", ok );
 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create Phobos type 2 DSK file." );  
  
   /*
   If the file DSKPHB already exists, use it. Otherwise create
   a new instance of it.
   */
   if ( !exists_c(DSKPHB) )
   {
      zzt_boddsk_c ( DSKPHB, "Phobos", "IAU_PHOBOS", SPICEFALSE, &dskhan );
      chckxc_c( SPICEFALSE, " ", ok );   
   }

   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

 
   /*
   Find the first segment in the Phobos DSK file. 
   */
   dlabfs_c ( dskhan, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksl_c ( "found", found, SPICETRUE, ok );





   /*
    ******************************************************************
    *
    *
    *  illum_plid_pl02 tests
    *
    *
    ******************************************************************
   */    


   /*
    ******************************************************************
    *
    *
    *  illum_plid_pl02 error cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02 error test setup" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   method  = "Intercept";
 
   obsrvr  = "Mars";
   target  = "Phobos";
   fixref  = "IAU_PHOBOS";
   abcorr  = "NONE";
   et      = 0.0;

   /*
   Create a valid surface point.
   */
   subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                abcorr, obsrvr,  spoint, &alt,   &plateID ); 
   chckxc_c ( SPICEFALSE, " ", ok );


 


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: bad plate ID" );

   plateID = -1;

   illum_plid_pl02 ( dskhan, &dladsc, target,  et,       abcorr,
                     obsrvr, spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: bad target name" );

   plateID = 1;

   illum_plid_pl02 ( dskhan, &dladsc, "xxx",   et,       abcorr,
                     obsrvr, spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: bad observer name" );

   illum_plid_pl02 ( dskhan, &dladsc, target,  et,       abcorr,
                     "yyy",  spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02 error: bad aberration correction" );

   illum_plid_pl02 ( dskhan, &dladsc, target,  et,       "zzz",
                     obsrvr, spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02 error: transmission aberration correction" );

   illum_plid_pl02 ( dskhan, &dladsc, target,  et,       "XCN",
                     obsrvr, spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: target and observer coincide" );

   illum_plid_pl02 ( dskhan, &dladsc, target,  et,       abcorr,
                     target, spoint,  plateID, &trgepc,  srfvec,
                     &phase, &solar,  &emissn, &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: target doesn't match DSK segment" );

   illum_plid_pl02 ( dskhan, &dladsc, "Saturn", et,       abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(TARGETMISMATCH)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: missing PCK data for target" );

   dvpool_c ( "BODY401_PM" );

   chckxc_c( SPICEFALSE, " ", ok );
   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );

   furnsh_c ( PCK0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: no DSK loaded" );


   /*
   DAS can read data from buffered records even after the
   associated DAS file has been unloaded. To ensure we try
   to read from the file, we'll pick a surface point for
   which we don't have buffered topography data.
   */
   dascls_c ( dskhan );
   chckxc_c( SPICEFALSE, " ", ok );

   vminus_c ( spoint, xspoint );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et+2e4,   abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   
   chckxc_c ( SPICETRUE, "SPICE(NOSUCHHANDLE)", ok );

   /*
   Re-load DSK for a few other tests. 
   */
   dasopr_c ( DSKPHB, &dskhan );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: no SPK loaded" );

   abcorr = "NONE";

   spkuef_c ( spkhan );
   chckxc_c( SPICEFALSE, " ", ok );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );

   /*
   Restore SPK. 
   */
   spklef_c ( SPK0, &spkhan );
   chckxc_c( SPICEFALSE, " ", ok );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: input string pointer is null" );


   illum_plid_pl02 ( dskhan, &dladsc, NULL,     et,       abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       NULL,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       abcorr,
                     NULL,   spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02: input string pointer is empty" );


   illum_plid_pl02 ( dskhan, &dladsc, "",       et,       abcorr,
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       "",  
                     obsrvr, spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illum_plid_pl02 ( dskhan, &dladsc, target,   et,       abcorr,
                     "",     spoint,  plateID,  &trgepc,  srfvec,
                     &phase, &solar,  &emissn,  &visible, &lit   );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



   /*
    ******************************************************************
    *
    *
    *  illum_plid_pl02 normal cases
    *
    *
    ******************************************************************
   */    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illum_plid_pl02 time init" );

   str2et_c ( "1990 jan 1", &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   tdelta = jyear_c()/10;

   /* 
   ---- Case ---------------------------------------------------------
   */

   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      abcorr = abcorrs[ coridx ];

      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Ellipsoid near point";
         emethod = "near point";

         sprintf ( title, 
                   "illum_plid_pl02: find illumination angles at "
                   "sub-solar point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                          );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-solar point on the target.
         */
         subsol_pl02 ( dskhan, &dladsc, method, target, et,    
                       abcorr, obsrvr,  spoint, &dist,  &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute illumination angles at the sub-solar point on
         the DSK model. 
         */
         illum_plid_pl02 ( dskhan, &dladsc, target,  et,       abcorr,
                           obsrvr, spoint,  plateID, &trgepc,  srfvec,
                           &phase, &solar,  &emissn, &visible, &lit   );
         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         Find the sub-solar point on the target using an
         ellipsoidal model.
         */
         subsol_c ( emethod, target, et,    
                    abcorr,  obsrvr, xspoint ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute illumination angles at the sub-solar point on
         the ellipsoidal model. 
         */
         illum_c ( target,  et,      abcorr,  obsrvr,
                   xspoint, &xphase, &xsolar, &xemssn ); 

         /*
         Compare illumination angles.

         We won't expect very close agreement, since the surface
         points at which the angles are computed are different, 
         and because the outward normal vectors at those points
         differ by even more.

         The phase angle doesn't involve the surface normal,
         so it should agree relatively well with that for the
         ellipsoid.          
         */
         tol = SINGLE;

         chcksd_c ( "phase",  phase,  "~/", xphase, tol, ok );

         /*
         We expect only very crude agreement for the angles that
         involve the surface normal. 
         */
         tol = 5.e-2;

         chcksd_c ( "emissn", emissn, "~/", xemssn, tol, ok ); 

         /*
         The solar incidence angle should be small at the sub-solar
         point, so use an absolute comparison. 
         */
         tol = 1.e-2;
         chcksd_c ( "solar",  solar,  "~", xsolar, tol, ok );


         /*
         Now for some more stringent tests: we'll compute the
         illumination angles explicitly and compare to those 
         returned by illum_plid_pl02.

         Get the target-observer vector. We must start with the
         observer-target vector and negate it, then subtract
         the position of the surface point.
         */
         spkcpt_c ( spoint,   target, fixref, et,     fixref, 
                    "TARGET", abcorr, obsrvr, trgsta, &lt    );
         chckxc_c ( SPICEFALSE, " ", ok );


         if ( coridx == 0 ) 
         {
            xtrgepc = et;
         }
         else
         {
            xtrgepc = et - lt;
         }

         vequ_c   ( trgsta, trgpos );
         vminus_c ( trgpos, srfobs );


         /*
         Compute the vector from the surface point to the sun. 
         */
         spkcpo_c ( "sun",  xtrgepc, fixref, "OBSERVER", abcorr, 
                    spoint, target,  fixref,  sunsta,    &ltsun );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         vequ_c ( sunsta, srfsun );

         /*
         Get the outward normal vector for the plate containing 
         `spoint'. 
         */
         dskn02_c ( dskhan, &dladsc, plateID, normal );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the expected illumination angles. 
         */
         xphase = vsep_c ( srfobs, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xsolar = vsep_c ( normal, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xemssn = vsep_c ( normal, srfobs );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         The emission angle presents no numeric problems. 
         */
         tol = 1.e-11;
         chcksd_c ( "(2) emissn", emissn, "~/", xemssn, tol, ok ); 

         /*
         Check the visibility flag. 
         */
         xvisible = emissn < halfpi_c();

         chcksi_c ( "visible", visible, "=", xvisible, 0, ok );

         /*printf ( "visible = %ld\n", (long)visible);*/


         /*
         The solar incidence angle should be small. Look for 
         absolute agreement at the sub-nanoradian level. 
         */
         tol = TIGHT;
         chcksd_c ( "(2) solar",  solar,  "~",  xsolar, tol, ok ); 


         /*
         Check the illumination flag. 
         */
         xlit = solar < halfpi_c();

         chcksi_c ( "lit", lit, "=", xlit, 0, ok );

         /*
         printf ( "solar (deg) = %f, lit = %ld\n", solar*dpr_c(),(long)lit);
         */

         /*
         The phase angle should be numerically well-behaved. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) phase",  phase,  "~/", xphase, tol, ok );

         /*
         Check the returned target epoch and observer-spoint vector. 
         */
         tol = VTIGHT;

         chcksd_c ( "trgepc", trgepc, "~", xtrgepc, tol, ok );


         tol = VTIGHT;

         chckad_c ( "srfvec", srfvec, "~~/", trgpos, 3, tol, ok );
      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   for ( coridx = 0;  coridx < NCORR;  coridx++ )
   {
      
      abcorr = abcorrs[ coridx ];

      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;

         /* 
         ---- Case ---------------------------------------------------------
         */
         method  = "Ellipsoid near point";
         emethod = "near point";

         sprintf ( title, 
                   "illum_plid_pl02: find illumination angles at "
                   "sub-Mars point on Phobos. Method = "
                   "%s. Abcorr = %s. ET = %25.17e",
                   method,
                   abcorr,
                   et                                        );

         tcase_c ( title );

         obsrvr  = "Mars";
         target  = "Phobos";
         fixref  = "IAU_PHOBOS";

         /*
         Find the first segment in the Phobos DSK file. 
         */
         dlabfs_c ( dskhan, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
         chcksl_c ( "found", found, SPICETRUE, ok );

         /*
         Find the sub-observer point on the target.
         */
         subpt_pl02 ( dskhan, &dladsc, method, target, et,    
                      abcorr, obsrvr,  spoint, &alt,   &plateID ); 
         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         Compute illumination angles at the sub-observer point on
         the DSK model. 
         */
         illum_plid_pl02 ( dskhan, &dladsc, target,  et,       abcorr,
                           obsrvr, spoint,  plateID, &trgepc,  srfvec,
                           &phase, &solar,  &emissn, &visible, &lit   );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         printf("DSK angles: phase: %f solar: %f emissn: %f\n",
               phase, solar, emissn );
         */

         /*
         Find the sub-observer point on the target using an
         ellipsoidal model.
         */
         subpt_c ( emethod, target, et,    
                   abcorr,  obsrvr, xspoint, &alt ); 
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute illumination angles at the sub-observer point on
         the ellipsoidal model. 
         */
         illum_c ( target,  et,      abcorr,  obsrvr,
                   xspoint, &xphase, &xsolar, &xemssn ); 

         /*
         Compare illumination angles.

         We won't expect very close agreement, since the surface
         points at which the angles are computed are different, 
         and because the outward normal vectors at those points
         differ by even more.

         The phase angle doesn't involve the surface normal,
         so it should agree relatively well with that for the
         ellipsoid.          
         */
         tol = SINGLE;

         chcksd_c ( "phase",  phase,  "~/", xphase, tol, ok );

         /*
         We expect only very crude agreement for the angles that
         involve the surface normal. 
         */
         tol = 5.e-2;
         chcksd_c ( "solar",  solar,  "~/", xsolar, tol, ok );

         /*
         The emission angle should be small at the sub-solar
         point, so use an absolute comparison. 
         */
         tol = 2.e-2;

         chcksd_c ( "emissn", emissn, "~", xemssn, tol, ok ); 


         /*
         Now for some more stringent tests: we'll compute the
         illumination angles explicitly and compare to those 
         returned by illum_plid_pl02.

         Get the target-observer vector. We must start with the
         observer-target vector and negate it, then subtract
         the position of the surface point.
         */
         spkcpt_c ( spoint,   target, fixref, et,     fixref, 
                    "TARGET", abcorr, obsrvr, trgsta, &lt    );
         chckxc_c ( SPICEFALSE, " ", ok );

         if ( coridx == 0 ) 
         {
            xtrgepc = et;
         }
         else
         {
            xtrgepc = et - lt;
         }
         
         vequ_c   ( trgsta, trgpos );
         vminus_c ( trgpos, srfobs );

         /*
         Compute the vector from the surface point to the sun. 
         */
         spkcpo_c ( "sun",  xtrgepc, fixref, "OBSERVER", abcorr, 
                    spoint, target,  fixref,  sunsta,    &ltsun );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         vequ_c ( sunsta, srfsun );

         /*
         Get the outward normal vector for the plate containing 
         `spoint'. 
         */
         dskn02_c ( dskhan, &dladsc, plateID, normal );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Compute the expected illumination angles. 
         */
         xphase = vsep_c ( srfobs, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xsolar = vsep_c ( normal, srfsun );
         chckxc_c ( SPICEFALSE, " ", ok );

         xemssn = vsep_c ( normal, srfobs );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         The solar incidence angle presents no numeric problems. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) solar", solar, "~/", xsolar, tol, ok ); 

         /*
         Check the illumination flag. 
         */
         xlit = solar < halfpi_c();

         chcksi_c ( "lit", lit, "=", xlit, 0, ok );
         
         /*
         The emission angle should be small. Look for 
         absolute agreement at the sub-nanoradian level. 
         */
         tol = TIGHT;
         chcksd_c ( "(2) emissn",  emissn,  "~",  xemssn, tol, ok ); 

         /*
         Check the visibility flag. 
         */
         xvisible = emissn < halfpi_c();

         chcksi_c ( "visible", visible, "=", xvisible, 0, ok );

         /*
         The phase angle should be numerically well-behaved. 
         */
         tol = VTIGHT;
         chcksd_c ( "(2) phase",  phase,  "~/", xphase, tol, ok );


         /*
         Check the returned target epoch and observer-spoint vector. 
         */
         tol = VTIGHT;

         chcksd_c ( "trgepc", trgepc, "~", xtrgepc, tol, ok );


         tol = VTIGHT;

         chckad_c ( "srfvec", srfvec, "~~/", trgpos, 3, tol, ok );

      }
      /*
      End of time loop.
      */ 
   }
   /*
   End of aberration correction loop.
   */
 

   /*****************************************************************
    *
    *
    *
    *  Tests using self-occulting shape follow:
    *
    *
    * 
    *****************************************************************/



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup for tests using self-occulting shape." );

   /*
   Unload our test Phobos DSK file. 
   */
   dascls_c ( dskhan );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create new Phobos DSK containing simple, self-occulting shape. 
   */
   if ( exists_c(DSKSOC) )
   {
      removeFile( DSKSOC );
   }

   target = "PHOBOS";
   targid = 401;
   surfce = target;
   fixref = "IAU_PHOBOS";

   t_cg_c ( targid, targid, fixref, DSKSOC );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasopr_c ( DSKSOC, &dskhan1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   dlabfs_c ( dskhan1, &dladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Get the plate count for this DSK segment. 
   */
   dskz02_c ( dskhan1, &dladsc, &nv, &np );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

   for ( coridx = 0;  coridx < NCORR;  coridx++ ) 
   {      
      abcorr = abcorrs[ coridx ];
    
  
      for ( timidx = 0;  timidx < NTIMES;  timidx++ )
      {
         et = et0 + timidx*tdelta;
         
         for ( i = 0;  i < np;  i++ )
         {
            /* 
            ---- Case ---------------------------------------------------------
            */
            plateID = i+1;

            sprintf ( title, 
                      "illum_plid_pl02: find illumination angles at "
                      "plate %ld centroid on Phobos. "
                      "Abcorr = %s. ET = %25.17e.",
                      (long) plateID,
                      abcorr,
                      et                                        );

            tcase_c ( title );  


            /*
            Look up plate `i' and its vertices. 
            */
            dskp02_c ( dskhan1, &dladsc,
                       plateID, 1,   &n, (SpiceInt (*)[3])plate );
            chckxc_c ( SPICEFALSE, " ", ok );

            for ( j = 0; j < 3; j++ )
            {
               dskv02_c ( dskhan1,  &dladsc, 
                          plate[j], 1,   &n, (SpiceDouble (*)[3]) verts[j] );
               chckxc_c ( SPICEFALSE, " ", ok );
            }

            /*
            Compute the plate's centroid. 
            */
            vlcom3_c ( 1.0/3, verts[0], 
                       1.0/3, verts[1], 
                       1.0/3, verts[2], spoint );

            /*
            Compute illumination angles and visbility flags at the centroid. 
            */

            illum_plid_pl02 ( dskhan1, &dladsc, target,  et,       abcorr,
                              obsrvr,  spoint,  plateID, &trgepc,  srfvec,
                              &phase,  &solar,  &emissn, &visible, &lit   );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Now we'll compute the illumination angles explicitly and
            compare to those returned by illum_plid_pl02.

            Get the target-observer vector. We must start with the
            observer-target vector and negate it, then subtract
            the position of the surface point.
            */
            spkcpt_c ( spoint,   target, fixref, et,     fixref, 
                       "TARGET", abcorr, obsrvr, trgsta, &lt    );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( coridx == 0 ) 
            {
               xtrgepc = et;
            }
            else
            {
               xtrgepc = et - lt;
            }

            vequ_c   ( trgsta, trgpos );
            vminus_c ( trgpos, srfobs );

            /*
            Compute the vector from the surface point to the sun. 
            */
            spkcpo_c ( "sun",  xtrgepc, fixref, "OBSERVER", abcorr, 
                       spoint, target,  fixref,  sunsta,    &ltsun );
            chckxc_c ( SPICEFALSE, " ", ok );

            vequ_c ( sunsta, srfsun );

            /*
            Get the outward normal vector for the plate containing 
            `spoint'. 
            */
            dskn02_c ( dskhan1, &dladsc, plateID, normal );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            Compute the expected illumination angles. 
            */
            xphase = vsep_c ( srfobs, srfsun );
            chckxc_c ( SPICEFALSE, " ", ok );

            xsolar = vsep_c ( normal, srfsun );
            chckxc_c ( SPICEFALSE, " ", ok );

            xemssn = vsep_c ( normal, srfobs );
            chckxc_c ( SPICEFALSE, " ", ok );

            /*
            The solar incidence angle presents no numeric problems. 
            */
            tol = VTIGHT;
            chcksd_c ( "(2) solar", solar, "~/", xsolar, tol, ok ); 

            /*
            Check the illumination flag. 

            We'll see whether `spoint' is visible from the center of
            the sun.
            */
            vadd_c   ( spoint,  srfsun, sunvec );
            vminus_c ( srfsun,  raydir );
            
            dskx02_c ( dskhan1, &dladsc,   sunvec, 
                       raydir,  &plateID1, xpt,    &found );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( !found ) 
            {
               /*
               There's no occultation, so `xlit' depends only on the
               solar incidence angle. 
               */ 
               xlit = xsolar < halfpi_c();
            }
            else if ( plateID1 == plateID )
            {
               /*
               We consider the plate to be unable to occult itself. Again,
               `xlit' depends only on the solar incidence angle. 
               */ 
               xlit = xsolar < halfpi_c();
            }
            else
            {
               /*
               If the intercept is closer to the sun than is `spoint',
               we have an occultation. Otherwise, `xlit' depends only
               on the solar incidence angle.
 
               Use a 1 cm tolerance for the distance check.
               */

               dist = vdist_c(sunvec, xpt) - vnorm_c(srfsun);

               xlit =     ( dist > - 1.e-5 )

                      &&  ( xsolar  <  halfpi_c() );                      
            }

            chcksi_c ( "lit", lit, "=", xlit, 0, ok );
           

            /*
            The emission angle should be small. Look for 
            absolute agreement at the sub-nanoradian level. 
            */
            tol = TIGHT;
            chcksd_c ( "(2) emissn",  emissn,  "~",  xemssn, tol, ok ); 

            /*
            Check the visibility flag. 

            We'll see whether `spoint' is visible from the center of Mars.
            */
            vadd_c   ( spoint,  srfobs, obsvec );
            vminus_c ( srfobs,  raydir );
            
            dskx02_c ( dskhan1, &dladsc,   obsvec, 
                       raydir,  &plateID1, xpt,    &found );
            chckxc_c ( SPICEFALSE, " ", ok );

            if ( !found ) 
            {
               /*
               There's no occultation, so `xvisible' depends only on the
               emission angle. 
               */ 
               xvisible = xemssn < halfpi_c();
            }
            else if ( plateID1 == plateID )
            {
               /*
               We consider the plate to be unable to occult itself. Again,
               `xvisible' depends only on the emission angle. 
               */ 
               xvisible = xemssn < halfpi_c();
            }
            else
            {
               /*
               If the intercept is closer to the observer than is `spoint',
               we have an occultation. Otherwise, `xvisible' depends only
               on the emission angle.

               Use a 1 cm tolerance for the distance check.
               */

               dist     = vdist_c(obsvec, xpt) - vnorm_c(srfobs);

               xvisible =      ( dist > - 1.e-5 )

                           &&  ( xemssn  <  halfpi_c() );                      
            }

            chcksi_c ( "visible", visible, "=", xvisible, 0, ok );
 

            /*
            The phase angle should be numerically well-behaved. 
            */
            tol = VTIGHT;
            chcksd_c ( "(2) phase",  phase,  "~/", xphase, tol, ok );


            /*
            Check the returned target epoch and observer-spoint vector. 
            */
            tol = VTIGHT;

            chcksd_c ( "trgepc", trgepc, "~", xtrgepc, tol, ok );


            tol = VTIGHT;

            chckad_c ( "srfvec", srfvec, "~~/", trgpos, 3, tol, ok );
         }
      }
   }


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Clean up kernels." );  

   
   /*
   Unload and delete our test SPK file. 
   */
   spkuef_c ( spkhan );
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile   ( SPK0 );

   /*
   Clean up text PCK. 
   */
   kclear_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   removeFile ( PCK0 );


   /*
   Unload and delete our test Phobos DSK files. 
   */
   dascls_c ( dskhan );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   dascls_c ( dskhan1 );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Remove the self-occulting shape kernel.
   */
   removeFile   ( DSKSOC );

   /*
   Remove the tesellated ellipsoid kernel. 
   */
   removeFile   ( DSKPHB );  
    
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dskg02b_c */

