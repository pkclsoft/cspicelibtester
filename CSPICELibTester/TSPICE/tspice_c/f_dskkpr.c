/* f_dskkpr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static logical c_true = TRUE_;

/* $Procedure F_DSKKPR ( DSK KEEPER tests ) */
/* Subroutine */ int f_dskkpr__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    char ch__1[32];
    cllist cl__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), f_clos(cllist *), 
	    i_dnnt(doublereal *);

    /* Local variables */
    char meta[255];
    integer hans[5000];
    char dsks[255*5000];
    integer unit;
    extern /* Subroutine */ int t_smldsk__(integer *, integer *, char *, char 
	    *, ftnlen, ftnlen);
    integer i__, n;
    extern /* Subroutine */ int kdata_(integer *, char *, char *, char *, 
	    char *, integer *, logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    char fname[255], frame[32];
    extern /* Subroutine */ int dskgd_(integer *, integer *, doublereal *), 
	    tcase_(char *, ftnlen), kinfo_(char *, char *, char *, integer *, 
	    logical *, ftnlen, ftnlen, ftnlen);
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    integer total;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    extern /* Character */ VOID begdat_(char *, ftnlen);
    integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *), 
	    chcksc_(char *, char *, char *, char *, logical *, ftnlen, ftnlen,
	     ftnlen, ftnlen), delfil_(char *, ftnlen), kclear_(void), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(
	    char *, logical *, logical *, logical *, ftnlen);
    integer bodyid;
    doublereal dskdsc[24];
    integer nsmall;
    extern /* Character */ VOID begtxt_(char *, ftnlen);
    char filtyp[4], source[255];
    extern logical exists_(char *, ftnlen);
    char kertxt[80*12];
    integer surfid;
    extern /* Subroutine */ int furnsh_(char *, ftnlen), ktotal_(char *, 
	    integer *, ftnlen), unload_(char *, ftnlen), writln_(char *, 
	    integer *, ftnlen), txtopn_(char *, integer *, ftnlen);
    logical fnd;
    char dsk[255];

/* $ Abstract */

/*     Exercise entry points of the SPICELIB routine KEEPER, using */
/*     DSK files. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests entry points of the SPICELIB routine KEEPER, */
/*     using DSK files. Entry points exercised by this test family are: */

/*        FURNSH */
/*        UNLOAD */
/*        KCLEAR */
/*        KTOTAL */
/*        KINFO */
/*        KDATA */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 10-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Test utility functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Initial values */


/*     Open the test family. */

    topen_("F_DSKKPR", (ftnlen)8);
/* *********************************************************************** */

/*     KEEPER entry point tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create a DSK file.", (ftnlen)25);
    bodyid = 499;
    surfid = 1;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(dsk, "keeper_test_0.bds", (ftnlen)255, (ftnlen)17);
    if (exists_(dsk, (ftnlen)255)) {
	delfil_(dsk, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_smldsk__(&bodyid, &surfid, frame, dsk, (ftnlen)32, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("FURNSH test: load a DSK file.", (ftnlen)29);
    furnsh_(dsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("KTOTAL test: count loaded DSK files.", (ftnlen)36);
    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (dsk)", &total, "=", &c__1, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);
    ktotal_("ALL", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (all)", &total, "=", &c__1, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("UNLOAD test: unload the DSK file.", (ftnlen)33);
    unload_(dsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of loaded DSKs. */

    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (dsk)", &total, "=", &c__0, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("KCLEAR test: load and then unload the DSK file.", (ftnlen)47);
    furnsh_(dsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of loaded DSKs. */

    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (dsk)", &total, "=", &c__0, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("FURNSH test: load DSKs via meta-kernel.", (ftnlen)39);
    s_copy(kertxt, "KPL/MK", (ftnlen)80, (ftnlen)6);
    begdat_(ch__1, (ftnlen)32);
    s_copy(kertxt + 80, ch__1, (ftnlen)80, (ftnlen)32);
    s_copy(kertxt + 160, "KERNELS_TO_LOAD = ( ", (ftnlen)80, (ftnlen)20);
    s_copy(kertxt + 240, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kertxt + 320, "   'keeper_test_1.bds'", (ftnlen)80, (ftnlen)22);
    s_copy(kertxt + 400, "   'keeper_test_2.bds'", (ftnlen)80, (ftnlen)22);
    s_copy(kertxt + 480, "   'keeper_test_3.bds'", (ftnlen)80, (ftnlen)22);
    s_copy(kertxt + 560, "   'keeper_test_4.bds'", (ftnlen)80, (ftnlen)22);
    s_copy(kertxt + 640, "   'keeper_test_5.bds'", (ftnlen)80, (ftnlen)22);
    s_copy(kertxt + 720, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kertxt + 800, " ) ", (ftnlen)80, (ftnlen)3);
    begtxt_(ch__1, (ftnlen)32);
    s_copy(kertxt + 880, ch__1, (ftnlen)80, (ftnlen)32);
    nsmall = 5;
    i__1 = nsmall;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)305)) * 255, "keeper_tes"
		"t_#.bds", (ftnlen)255, (ftnlen)17);
	repmi_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)306)) * 255, "#", &i__, 
		dsks + ((i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : s_rnge(
		"dsks", i__3, "f_dskkpr__", (ftnlen)306)) * 255, (ftnlen)255, 
		(ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_smldsk__(&bodyid, &i__, frame, dsks + ((i__2 = i__ - 1) < 5000 && 0 
		<= i__2 ? i__2 : s_rnge("dsks", i__2, "f_dskkpr__", (ftnlen)
		309)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create meta-kernel. */

    s_copy(meta, "keeper_meta.tm", (ftnlen)255, (ftnlen)14);
    if (exists_(meta, (ftnlen)255)) {
	delfil_(meta, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    txtopn_(meta, &unit, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 12; ++i__) {
	writln_(kertxt + ((i__1 = i__ - 1) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"kertxt", i__1, "f_dskkpr__", (ftnlen)329)) * 80, &unit, (
		ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    cl__1.cerr = 0;
    cl__1.cunit = unit;
    cl__1.csta = 0;
    f_clos(&cl__1);

/*     Load meta-kernel. */

    furnsh_(meta, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("KTOTAL test (MK): count loaded DSK files.", (ftnlen)41);
    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (dsk)", &total, "=", &nsmall, &c__0, ok, (ftnlen)11, (
	    ftnlen)1);
    ktotal_("ALL", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nsmall + 1;
    chcksi_("TOTAL (all)", &total, "=", &i__1, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("KINFO (MK): check info on loaded DSKs.", (ftnlen)38);

/*     Check info on meta-kernel. */

    kinfo_(meta, filtyp, source, &handle, &found, (ftnlen)255, (ftnlen)4, (
	    ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksc_("FILTYP", filtyp, "=", "META", ok, (ftnlen)6, (ftnlen)4, (ftnlen)
	    1, (ftnlen)4);

/*     Check info on DSKs. */

    i__1 = nsmall;
    for (i__ = 1; i__ <= i__1; ++i__) {
	kinfo_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)380)) * 255, filtyp, 
		source, &handle, &found, (ftnlen)255, (ftnlen)4, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	if (*ok) {
	    chcksc_("FILTYP", filtyp, "=", "DSK", ok, (ftnlen)6, (ftnlen)4, (
		    ftnlen)1, (ftnlen)3);
	    chcksc_("SOURCE", source, "=", meta, ok, (ftnlen)6, (ftnlen)255, (
		    ftnlen)1, (ftnlen)255);

/*           Check the handle of the Ith DSK. */

/*           Use the handle to extract the surface ID of the */
/*           sole segment of the current file. This ID is unique */
/*           among the loaded DSKs. */

	    dlabfs_(&handle, dladsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskgd_(&handle, dladsc, dskdsc);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    surfid = i_dnnt(dskdsc);
	    chcksi_("SURFID", &surfid, "=", &i__, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("KDATA (MK): check info on loaded DSKs.", (ftnlen)38);

/*     Check info on meta-kernel. */

    kdata_(&c__1, "META", fname, filtyp, source, &handle, &found, (ftnlen)4, (
	    ftnlen)255, (ftnlen)4, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksc_("FNAME", fname, "=", meta, ok, (ftnlen)5, (ftnlen)255, (ftnlen)1, 
	    (ftnlen)255);
    chcksc_("FILTYP", filtyp, "=", "META", ok, (ftnlen)6, (ftnlen)4, (ftnlen)
	    1, (ftnlen)4);

/*     Check info on DSKs. */

    i__1 = nsmall;
    for (i__ = 1; i__ <= i__1; ++i__) {
	kdata_(&i__, "DSK", fname, filtyp, source, &hans[(i__2 = i__ - 1) < 
		5000 && 0 <= i__2 ? i__2 : s_rnge("hans", i__2, "f_dskkpr__", 
		(ftnlen)433)], &found, (ftnlen)3, (ftnlen)255, (ftnlen)4, (
		ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	if (*ok) {
	    chcksc_("FILE", fname, "=", dsks + ((i__2 = i__ - 1) < 5000 && 0 
		    <= i__2 ? i__2 : s_rnge("dsks", i__2, "f_dskkpr__", (
		    ftnlen)440)) * 255, ok, (ftnlen)4, (ftnlen)255, (ftnlen)1,
		     (ftnlen)255);
	    chcksc_("FILTYP", filtyp, "=", "DSK", ok, (ftnlen)6, (ftnlen)4, (
		    ftnlen)1, (ftnlen)3);
	    chcksc_("SOURCE", source, "=", meta, ok, (ftnlen)6, (ftnlen)255, (
		    ftnlen)1, (ftnlen)255);

/*           Check the handle of the Ith DSK. */

/*           Use the handle to extract the surface ID of the */
/*           sole segment of the current file. This ID is unique */
/*           among the loaded DSKs. */

	    dlabfs_(&hans[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : 
		    s_rnge("hans", i__2, "f_dskkpr__", (ftnlen)451)], dladsc, 
		    &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskgd_(&hans[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		    "hans", i__2, "f_dskkpr__", (ftnlen)454)], dladsc, dskdsc)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    surfid = i_dnnt(dskdsc);
	    chcksi_("SURFID", &surfid, "=", &i__, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("UNLOAD: unload meta-kernel.", (ftnlen)27);
    unload_(meta, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (dsk)", &total, "=", &c__0, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("FURNSH: reload meta-kernel.", (ftnlen)27);
    furnsh_(meta, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Tests using the maximum number of loaded DSKs follow. */


/* --- Case: ------------------------------------------------------ */

    tcase_("FURNSH test: load 5000 DSK files.", (ftnlen)33);
    n = 5000;

/*     Create the DSKs that weren't there already from the */
/*     meta-kernel test cases. */

    i__1 = n;
    for (i__ = nsmall + 1; i__ <= i__1; ++i__) {
	s_copy(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)508)) * 255, "keeper_tes"
		"t_#.bds", (ftnlen)255, (ftnlen)17);
	repmi_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)509)) * 255, "#", &i__, 
		dsks + ((i__3 = i__ - 1) < 5000 && 0 <= i__3 ? i__3 : s_rnge(
		"dsks", i__3, "f_dskkpr__", (ftnlen)509)) * 255, (ftnlen)255, 
		(ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_smldsk__(&bodyid, &i__, frame, dsks + ((i__2 = i__ - 1) < 5000 && 0 
		<= i__2 ? i__2 : s_rnge("dsks", i__2, "f_dskkpr__", (ftnlen)
		512)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)519)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("KTOTAL: check loaded DSK count.", (ftnlen)31);
    ktotal_("DSK", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nsmall + n;
    chcksi_("TOTAL (dsk)", &total, "=", &i__1, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("KINFO: check info on loaded DSKs.", (ftnlen)33);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	kinfo_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)545)) * 255, filtyp, 
		source, &handle, &found, (ftnlen)255, (ftnlen)4, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	if (*ok) {
	    chcksc_("FILTYP", filtyp, "=", "DSK", ok, (ftnlen)6, (ftnlen)4, (
		    ftnlen)1, (ftnlen)3);
	    if (i__ > nsmall) {
		chcksc_("SOURCE", source, "=", " ", ok, (ftnlen)6, (ftnlen)
			255, (ftnlen)1, (ftnlen)1);
	    }

/*           Check the handle of the Ith DSK. */

/*           Use the handle to extract the surface ID of the */
/*           sole segment of the current file. This ID is unique */
/*           among the loaded DSKs. */

	    dlabfs_(&handle, dladsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskgd_(&handle, dladsc, dskdsc);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    surfid = i_dnnt(dskdsc);
	    chcksi_("SURFID", &surfid, "=", &i__, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("KCLEAR: unload all 5000 DSKs.", (ftnlen)29);

/*     Unload all files. */

    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: unloading via UNLOAD is still an n**2 process: it's */
/*     too slow with 5000 files. */


/* --- Case: ------------------------------------------------------ */

    tcase_("KDATA: check info on loaded DSKs.", (ftnlen)33);

/*     Reload DSKs. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	furnsh_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)605)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	kdata_(&i__, "DSK", fname, filtyp, source, &hans[(i__2 = i__ - 1) < 
		5000 && 0 <= i__2 ? i__2 : s_rnge("hans", i__2, "f_dskkpr__", 
		(ftnlen)611)], &found, (ftnlen)3, (ftnlen)255, (ftnlen)4, (
		ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	if (*ok) {
	    chcksc_("FILE", fname, "=", dsks + ((i__2 = i__ - 1) < 5000 && 0 
		    <= i__2 ? i__2 : s_rnge("dsks", i__2, "f_dskkpr__", (
		    ftnlen)618)) * 255, ok, (ftnlen)4, (ftnlen)255, (ftnlen)1,
		     (ftnlen)255);
	    chcksc_("FILTYP", filtyp, "=", "DSK", ok, (ftnlen)6, (ftnlen)4, (
		    ftnlen)1, (ftnlen)3);
	    chcksc_("SOURCE", source, "=", " ", ok, (ftnlen)6, (ftnlen)255, (
		    ftnlen)1, (ftnlen)1);

/*           Check the handle of the Ith DSK. */

/*           Use the handle to extract the surface ID of the */
/*           sole segment of the current file. This ID is unique */
/*           among the loaded DSKs. */

	    dlabfs_(&hans[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : 
		    s_rnge("hans", i__2, "f_dskkpr__", (ftnlen)629)], dladsc, 
		    &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskgd_(&hans[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		    "hans", i__2, "f_dskkpr__", (ftnlen)632)], dladsc, dskdsc)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    surfid = i_dnnt(dskdsc);
	    chcksi_("SURFID", &surfid, "=", &i__, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("KCLEAR/KTOTAL: verify keeper clear operation.", (ftnlen)45);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ktotal_("ALL", &total, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("TOTAL (ALL)", &total, "=", &c__0, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/*     Clean up. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    delfil_(dsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	delfil_(dsks + ((i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_dskkpr__", (ftnlen)669)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    delfil_(meta, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_dskkpr__ */

