/*

-Procedure f_dskkpr_c ( DSK KEEPER tests )

 
-Abstract
 
   Exercise wrappers for the entry points of the SPICELIB routine
   KEEPER, using DSK files.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_dskkpr_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests CSPICE wrapper for the entry points of the
   SPICELIB routine KEEPER, using DSK files. Wrappers exercised by
   this test family are:

      furnsh_c
      unload_c
      kclear_c
      ktotal_c
      kinfo_c
      kdata_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

       Previous version 24-MAR-2016 (NJB)

-Index_Entries

   test keeper entry point wrappers using dsks

-&
*/

{ /* Begin f_dskkpr_c */

 
   /*
   Constants
   */
   #define MAXN            5000
   #define LNSIZE          81
   #define KERSIZ          12
   #define FRNMLN          32
   #define FILSIZ          256
   #define TYPLEN          5

   /*
   Local variables
   */
   FILE                  * metaFile;

   SpiceDLADescr           dladsc;
   SpiceDSKDescr           dskdsc;

   SpiceChar             * dsk;
   SpiceChar               dsks   [ MAXN ][ FILSIZ ];   
   SpiceChar               filtyp [ TYPLEN ];
   SpiceChar               fname  [ FILSIZ ];
   SpiceChar             * frame;
   SpiceChar             * kertxt [ KERSIZ ];
   SpiceChar             * meta;
   SpiceChar               source [ FILSIZ ];

   SpiceBoolean            fnd;
   SpiceBoolean            found;

   SpiceInt                bodyid;
   SpiceInt                handle;
   SpiceInt                hans   [MAXN];
   SpiceInt                i;
   SpiceInt                n;
   SpiceInt                nsmall;
   SpiceInt                surfid;
   SpiceInt                total;

   int                     wrstat;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dskkpr_c" );
   

   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a DSK file." );
 
   bodyid = 499;
   surfid = 1;
   frame  = "IAU_MARS";

   dsk    = "keeper_test_0.bds";

   if ( exists_c(dsk) )
   {
      removeFile( dsk );
   }

   t_smldsk_c( bodyid, surfid, frame, dsk );
   chckxc_c  ( SPICEFALSE, " ", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "furnsh_c test: load a DSK file." );

   furnsh_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "ktotal_c test: count loaded DSK files." );

   ktotal_c ( "DSK", &total );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (dsk)", total, "=", 1, 0, ok );

   ktotal_c ( "ALL", &total);
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (all)", total, "=", 1, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "unload_c test: unload the DSK file." );

   unload_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of loaded DSKs. 
   */
   ktotal_c ( "dsk", &total);
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (dsm)", total, "=", 0, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kclear_c test: load and then unload the DSK file." );


   furnsh_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok );

   kclear_c();
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of loaded DSKs. 
   */
   ktotal_c ( "dsk", &total);
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (dsm)", total, "=", 0, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "furnsh_c test: load DSKs via a meta-kernel." );


   kertxt[0]  = "KPL/MK";
   kertxt[1]  = "\\begindata";
   kertxt[2]  = "KERNELS_TO_LOAD = ( ";
   kertxt[3]  = " ";
   kertxt[4]  = "   'keeper_test_1.bds'";
   kertxt[5]  = "   'keeper_test_2.bds'";
   kertxt[6]  = "   'keeper_test_3.bds'";
   kertxt[7]  = "   'keeper_test_4.bds'";
   kertxt[8]  = "   'keeper_test_5.bds'";
   kertxt[9]  = " ";
   kertxt[10] = " ) ";
   kertxt[11] = "\\begintext";


   nsmall = 5;

   for ( i = 0; i < nsmall; i++ )
   {
      strncpy ( dsks[i], "keeper_test_#.bds", FILSIZ );

      repmi_c  ( dsks[i], "#", i+1, FILSIZ, dsks[i] );
      chckxc_c ( SPICEFALSE, " ", ok );

      t_smldsk_c ( bodyid, i+1, frame, dsks[i] );
      chckxc_c   ( SPICEFALSE, " ", ok );
   }


   /*
   Write the meta-kernel. 
   */
   meta = "keeper_meta.tm";

   metaFile = fopen( meta, "w" );

   if ( !metaFile )
   {
      setmsg_c ( "Attempt to open file # failed." );
      errch_c  ( "#", meta                        );
      sigerr_c ( "SPICE(FILEOPENFAILED)"          );
      chckxc_c   ( SPICEFALSE, " ", ok );      
   }

   for ( i = 0; i < KERSIZ; i++ )
   {
      wrstat = fprintf( metaFile, "%s\n", kertxt[i] );

      if ( wrstat < 0 )
      {
         setmsg_c ( "Attempt to write line # to file # failed." );
         errint_c ( "#", (SpiceInt)wrstat                       );
         errch_c  ( "#", meta                                   );
         sigerr_c ( "SPICE(FILEWRITEFAILED)"                    );
         chckxc_c ( SPICEFALSE, " ", ok );      
      }
   }

   fclose( metaFile );


   /*
   Load the meta-kernel. 
   */
   furnsh_c ( meta );
   chckxc_c   ( SPICEFALSE, " ", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "ktotal_c test (MK): count loaded DSK files." );

   ktotal_c ( "DSK", &total );
   chckxc_c   ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (dsk)", total, "=", nsmall,   0, ok );

   ktotal_c ( "ALL", &total );
   chckxc_c   ( SPICEFALSE, " ", ok );

   chcksi_c ( "TOTAL (all)", total, "=", nsmall+1, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kinfo_c test (MK): check info on loaded DSK files." );

   /*
   Check info on meta-kernel first. 
   */
   kinfo_c ( meta, TYPLEN, FILSIZ, filtyp, source, &handle, &found );
   chckxc_c   ( SPICEFALSE, " ", ok );

   chcksl_c ( "found",  found, SPICETRUE, ok );
   chcksc_c ( "filtyp", filtyp, "=", "META", ok );

   /*
   Check info on DSKs. 
   */
   for ( i = 0;  i < nsmall;  i++ )
   {
      kinfo_c ( dsks[i], TYPLEN, FILSIZ, filtyp, source, &handle, &found );
      chckxc_c( SPICEFALSE, " ", ok );

      if ( ok )
      {
         chcksc_c ( "filtyp", filtyp, "=", "DSK",  ok );
         chcksc_c ( "source", source, "=", meta,   ok );
      
         /*
         Check the handle of the Ith DSK.

         Use the handle to extract the surface ID of the 
         sole segment of the current file. This ID is unique
         among the loaded DSKs.           
         */
         dlabfs_c ( handle, &dladsc, &fnd );
         chckxc_c( SPICEFALSE, " ", ok );

         dskgd_c ( handle, &dladsc, &dskdsc );
         chckxc_c( SPICEFALSE, " ", ok );

         surfid = dskdsc.surfce;

         chcksi_c ( "surfid", surfid, "=", i+1, 0, ok );
      }
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kdata_c test (MK): check info on loaded DSK files." );

   /*
   Check info on meta-kernel first. 
   */
   kdata_c ( 0,     "META", FILSIZ, TYPLEN,  FILSIZ, 
             fname, filtyp, source, &handle, &found );
   chckxc_c   ( SPICEFALSE, " ", ok );

   chcksl_c ( "found",  found, SPICETRUE, ok );
   chcksc_c ( "filtyp", filtyp, "=", "META", ok );

   /*
   Check info on DSKs. 
   */
   for ( i = 0;  i < nsmall;  i++ )
   {
      kdata_c ( i,     "DSK",  FILSIZ, TYPLEN,  FILSIZ, 
                fname, filtyp, source, &handle, &found );

      chckxc_c( SPICEFALSE, " ", ok );

      if ( ok )
      {
         chcksc_c ( "filtyp", filtyp, "=", "DSK",  ok );
         chcksc_c ( "source", source, "=", meta,   ok );
      
         /*
         Check the handle of the Ith DSK.

         Use the handle to extract the surface ID of the 
         sole segment of the current file. This ID is unique
         among the loaded DSKs.           
         */
         dlabfs_c ( handle, &dladsc, &fnd );
         chckxc_c( SPICEFALSE, " ", ok );

         dskgd_c ( handle, &dladsc, &dskdsc );
         chckxc_c( SPICEFALSE, " ", ok );

         surfid = dskdsc.surfce;

         chcksi_c ( "surfid", surfid, "=", i+1, 0, ok );
      }
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "unload_c test: unload meta_kernel." );

   unload_c ( meta );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   ktotal_c ( "dsk", &total );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "total (dsk)", total, "=", 0, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "furnsh_c: reload meta_kernel." );

   furnsh_c ( meta );
   chckxc_c ( SPICEFALSE, " ", ok );
    



  /*
   *
   *  Tests using the maximum number of loaded DSKs follow. 
   *
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "furnsh_c test: Load 5000 DSK files." );

   n = 5000;

   /*
   Create the DSKs that weren't there already from the
   meta-kernel test cases. 
   */

   for ( i = nsmall;  i < n;  i++ )
   {
      strncpy ( dsks[i], "keeper_test_#.bds", FILSIZ );

      repmi_c  ( dsks[i], "#", i+1, FILSIZ, dsks[i] );
      chckxc_c ( SPICEFALSE, " ", ok );
   
      t_smldsk_c( bodyid, i+1, frame, dsks[i] );
      chckxc_c  ( SPICEFALSE, " ", ok );
   }

   for ( i = 0;  i < n;  i++ )
   {
      furnsh_c ( dsks[i] );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "ktotal_c: check loaded DSK count." );


   ktotal_c ( "dsk", &total );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "total", total, "=", n + nsmall, 0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kinfo_c test: check info on loaded DSK files." );

 
   /*
   Check info on DSKs. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      kinfo_c ( dsks[i], TYPLEN, FILSIZ, filtyp, source, hans+i, &found );
      chckxc_c( SPICEFALSE, " ", ok );

      if ( ok )
      {
         chcksc_c ( "filtyp", filtyp, "=", "DSK",  ok );

         if ( i >= nsmall )
         {
            /*
            The source string should be empty. 
            */
            chcksi_c ( "strlen(source)", strlen(source), "=", 0, 0,  ok );
         }
      
         /*
         Check the handle of the Ith DSK.

         Use the handle to extract the surface ID of the 
         sole segment of the current file. This ID is unique
         among the loaded DSKs.           
         */
         dlabfs_c ( hans[i], &dladsc, &fnd );
         chckxc_c( SPICEFALSE, " ", ok );

         dskgd_c ( hans[i], &dladsc, &dskdsc );
         chckxc_c( SPICEFALSE, " ", ok );

         surfid = dskdsc.surfce;

         chcksi_c ( "surfid", surfid, "=", i+1, 0, ok );
      }
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kclear_c: unload all 5000 DSKs." );

   /*
   Unload all files. 
   */
   kclear_c();
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Note: unloading via unload_c is still an n**2 process: it's
   too slow with 5000 files.
   */




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kdata_c: check info on loaded DSK files." );

   /*
   Reload DSKs.
   */
   for ( i = 0;  i < n;  i++ )
   {
      furnsh_c ( dsks[i] );
      chckxc_c( SPICEFALSE, " ", ok );
   }


   /*
   Check info on DSKs. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      kdata_c ( i,     "DSK",  FILSIZ, TYPLEN,  FILSIZ, 
                fname, filtyp, source, hans+i, &found );

      chckxc_c( SPICEFALSE, " ", ok );

      if ( ok )
      {
         chcksc_c ( "filtyp", filtyp, "=", "DSK", ok );

         /*
         The source string should be empty. 
         */
         chcksi_c ( "strlen(source)", strlen(source), "=", 0, 0, ok );
        
      
         /*
         Check the handle of the Ith DSK.

         Use the handle to extract the surface ID of the 
         sole segment of the current file. This ID is unique
         among the loaded DSKs.           
         */
         dlabfs_c ( hans[i], &dladsc, &fnd );
         chckxc_c( SPICEFALSE, " ", ok );

         dskgd_c ( hans[i], &dladsc, &dskdsc );
         chckxc_c( SPICEFALSE, " ", ok );

         surfid = dskdsc.surfce;

         chcksi_c ( "surfid", surfid, "=", i+1, 0, ok );
      }
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "kclear_c/ktotal_c: verify kclear_c operation." );

   kclear_c();
   chckxc_c( SPICEFALSE, " ", ok );

   ktotal_c( "all", &total );
   chckxc_c( SPICEFALSE, " ", ok );

   chcksi_c( "total (all)", total, "=", 0, 0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );
   

   removeFile( dsk );


   for ( i = 0;  i < n;  i++ )
   {
      removeFile( dsks[i] );
   }

   removeFile( meta );

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dskkpr_c */

