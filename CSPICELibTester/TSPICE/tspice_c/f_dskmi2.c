/* f_dskmi2.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10000 = 10000;
static integer c__20000 = 20000;
static logical c_false = FALSE_;
static integer c__60000 = 60000;
static integer c_b10 = 100000;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c__6 = 6;
static doublereal c_b65 = 0.;
static logical c_true = TRUE_;

/* $Procedure      F_DSKMI2 ( Test DSKMI2 ) */
/* Subroutine */ int f_dskmi2__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer nlat, nlon, size, work[200000]	/* was [2][100000] */,
	     nvox[3];
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__;
    extern /* Subroutine */ int zzmkspin_(integer *, integer *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, integer *, doublereal *, doublereal *, integer *, integer *, 
	    integer *, integer *, integer *, doublereal *, integer *);
    static integer i__;
    extern /* Subroutine */ int zzvrtplt_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     tcase_(char *, ftnlen);
    static integer cells[200000]	/* was [2][100000] */;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal verts[30000]	/* was [3][10000] */;
    extern /* Subroutine */ int t_success__(logical *), dskmi2_(integer *, 
	    doublereal *, integer *, integer *, doublereal *, integer *, 
	    integer *, integer *, integer *, logical *, integer *, integer *, 
	    doublereal *, integer *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static integer np;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    static integer nv;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    static doublereal finscl;
    static integer corscl;
    static doublereal spaixd[10];
    static integer plates[60000]	/* was [3][20000] */;
    static doublereal extent[6]	/* was [2][3] */;
    static integer cgrptr[100000], spaixi[330000];
    static logical makvtl;
    static integer reqsiz, nvlist;
    static doublereal voxori[3], voxsiz;
    static integer nvxlst, nvxptr, nvxtot, spxisz, voxlsz, voxnpl, voxnpt, 
	    voxpsz, vplist[60000], vrtptr[10000], vxlist[100000], voxptr[
	    60000], worksz;

/* $ Abstract */

/*     Test the SPICELIB routine DSKMI2. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine DSKMI2. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 08-AUG-2016  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Local Variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_DSKMI2", (ftnlen)8);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Set-up: create plates for a tessellated ellipsoid. NLON = 100; N"
	    "LAT = 50; NV = 4902; NP = 9800.", (ftnlen)95);
    a = 3e3;
    b = 2e3;
    c__ = 1e3;
    nlon = 100;
    nlat = 50;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, verts, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Setup: create spatial index using the plate set from the previou"
	    "s case. Call ZZMKSPIN directly.", (ftnlen)95);

/*     Set voxel scales. */

    finscl = 2.;
    corscl = 4;

/*     Set sizes of the workspace and output arrays. */

    zzmkspin_(&np, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &c_b10,
	     cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &nvxlst, 
	    vxlist, extent, cgrptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Setup: vertex-plate map using the plate set from the previous ca"
	    "se. Call ZZVRTPLT directly.", (ftnlen)91);
    zzvrtplt_(&nv, &np, plates, &c_b10, &c_b10, cells, vrtptr, &nvlist, 
	    vplist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Create spatial index using the plate set from the previous case."
	    " Include a vertex-plate map.", (ftnlen)92);

/*     Set voxel scales. */

    finscl = 2.;
    corscl = 4;

/*     Make a plate-vertex map. */

    makvtl = TRUE_;

/*     Set sizes of the workspace and output arrays. */

    worksz = 100000;
    voxpsz = 60000;
    voxlsz = 100000;
    spxisz = 330000;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &
	    voxlsz, &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The following cases test the integer component of */
/*     the spatial index. This component is nothing but */
/*     a concatenation of the integer outputs of ZZMKSPIN. */


/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel grid extents from DSKMI2.", (ftnlen)37);
    chckai_("Voxel grid extents", spaixi, "=", nvox, &c__3, ok, (ftnlen)18, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check coarse voxel scale from DSKMI2.", (ftnlen)37);
    chcksi_("Coarse voxel scale", &spaixi[3], "=", &corscl, &c__0, ok, (
	    ftnlen)18, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check coarse voxel pointers from DSKMI2.", (ftnlen)40);
    chckai_("Coarse voxel pointers", &spaixi[7], "=", cgrptr, &c_b10, ok, (
	    ftnlen)21, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex-plate map from DSKMI2.", (ftnlen)35);

/*     DSKMI2 should have simply packaged the map created by */
/*     ZZVRTPLT. */

/*     Check the map size first. */

    size = spaixi[6];
    chckai_("vertex-plate map size", &size, "=", &nvlist, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the vertex-plate pointers. Compute the start index of the */
/*     pointer array. */

    voxnpt = spaixi[4];
    voxnpl = spaixi[5];
    i__ = voxnpt + 100008 + voxnpl;
    chckai_("vertex-plate pointers", &spaixi[(i__1 = i__ - 1) < 330000 && 0 <=
	     i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskmi2__", (ftnlen)361)],
	     "=", vrtptr, &nv, ok, (ftnlen)21, (ftnlen)1);

/*     Compute the start index of the map itself. */

    i__ += nv;
    chckai_("vertex-plate map", &spaixi[(i__1 = i__ - 1) < 330000 && 0 <= 
	    i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskmi2__", (ftnlen)370)], 
	    "=", vplist, &nvlist, ok, (ftnlen)16, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel-plate map from DSKMI2.", (ftnlen)34);

/*     Check the voxel pointer count. */

    chcksi_("voxel pointer count", &voxnpt, "=", &nvxptr, &c__0, ok, (ftnlen)
	    19, (ftnlen)1);

/*     Check the voxel-plate pointers. Compute the start index of the */
/*     pointer array. */

    i__ = 100008;
    chckai_("voxel-plate pointers", &spaixi[(i__1 = i__ - 1) < 330000 && 0 <= 
	    i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskmi2__", (ftnlen)390)], 
	    "=", voxptr, &nvxptr, ok, (ftnlen)20, (ftnlen)1);

/*     Check the voxel-plate list size. */

    chcksi_("voxel-plate list size", &voxnpl, "=", &nvxlst, &c__0, ok, (
	    ftnlen)21, (ftnlen)1);

/*     Check the voxel-plate list. Compute the start index of the */
/*     list. */

    i__ += voxnpt;
    chckai_("voxel-plate list", &spaixi[(i__1 = i__ - 1) < 330000 && 0 <= 
	    i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskmi2__", (ftnlen)405)], 
	    "=", vxlist, &voxnpl, ok, (ftnlen)16, (ftnlen)1);

/*     The following cases test the double precision component of */
/*     the spatial index. */


/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex bounds from DSKMI2.", (ftnlen)32);
    chckad_("vertex bounds", spaixd, "=", extent, &c__6, &c_b65, ok, (ftnlen)
	    13, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel grid origin from DSKMI2.", (ftnlen)36);
    chckad_("voxel grid origin", &spaixd[6], "=", voxori, &c__3, &c_b65, ok, (
	    ftnlen)17, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check fine voxel size from DSKMI2.", (ftnlen)34);
    chcksd_("fine voxel size", &spaixd[9], "=", &voxsiz, &c_b65, ok, (ftnlen)
	    15, (ftnlen)1);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Bad fine voxel scale.", (ftnlen)21);

/*     Set voxel scales. */

    finscl = -2.;
    corscl = 4;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &
	    voxlsz, &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(BADFINEVOXELSCALE)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad coarse voxel scale.", (ftnlen)23);

/*     Set voxel scales. */

    finscl = 2.;
    corscl = 0;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &
	    voxlsz, &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(BADCOARSEVOXSCALE)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad vertex count.", (ftnlen)17);
    finscl = 2.;
    corscl = 4;
    i__ = 2;
    dskmi2_(&i__, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &
	    voxlsz, &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(BADVERTEXCOUNT)", ok, (ftnlen)21);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad plate count.", (ftnlen)16);
    i__ = 0;
    dskmi2_(&nv, verts, &i__, plates, &finscl, &corscl, &worksz, &voxpsz, &
	    voxlsz, &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(BADPLATECOUNT)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("Workspace too small.", (ftnlen)20);
    i__ = np;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &i__, &voxpsz, &voxlsz,
	     &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(WORKSPACETOOSMALL)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Voxel-plate pointer array too small.", (ftnlen)36);
    i__ = 0;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &i__, &voxlsz,
	     &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(PTRARRAYTOOSMALL)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("Voxel-plate list too small.", (ftnlen)27);
    i__ = np;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &i__,
	     &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(PLATELISTTOOSMALL)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Integer spatial index component too small.", (ftnlen)42);
    reqsiz = voxpsz + 100007 + voxlsz + (nv << 1) + np * 3;
    i__ = reqsiz - 1;
    dskmi2_(&nv, verts, &np, plates, &finscl, &corscl, &worksz, &voxpsz, &i__,
	     &makvtl, &spxisz, work, spaixd, spaixi);
    chckxc_(&c_true, "SPICE(INTINDEXTOOSMALL)", ok, (ftnlen)23);
    t_success__(ok);
    return 0;
} /* f_dskmi2__ */

