/* f_dskobj.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__5000 = 5000;
static integer c__0 = 0;
static integer c__10 = 10;
static integer c__3 = 3;
static integer c__1 = 1;
static integer c__199 = 199;

/* $Procedure      F_DSKOBJ ( Test DSK body/surface coverage routines ) */
/* Subroutine */ int f_dskobj__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    cllist cl__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer), f_clos(cllist *);

    /* Local variables */
    static integer unit;
    extern /* Subroutine */ int t_smldsk__(integer *, integer *, char *, char 
	    *, ftnlen, ftnlen);
    static integer i__, j, k, n;
    extern integer cardi_(integer *);
    extern /* Subroutine */ int dasbt_(char *, integer *, ftnlen), tcase_(
	    char *, ftnlen);
    static logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen), tstek_(char *, 
	    integer *, integer *, logical *, integer *, ftnlen), t_success__(
	    logical *), chckai_(char *, integer *, char *, integer *, integer 
	    *, logical *, ftnlen, ftnlen);
    static integer handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static integer framid;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     cidfrm_(integer *, integer *, char *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    static integer xn;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static char frname[32];
    extern /* Subroutine */ int scardi_(integer *, integer *);
    static integer bodyid;
    extern /* Subroutine */ int dskobj_(char *, integer *, ftnlen);
    static integer bodset[5006];
    extern integer isrchi_(integer *, integer *, integer *);
    static integer bodlst[10], xbdset[5006];
    extern /* Subroutine */ int dsksrf_(char *, integer *, integer *, ftnlen);
    static integer srfset[5006];
    extern /* Subroutine */ int ssizei_(integer *, integer *), insrti_(
	    integer *, integer *);
    static integer xsfset[5006];
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int tstspk_(char *, logical *, logical *, ftnlen),
	     txtopn_(char *, integer *, ftnlen);

/* $ Abstract */

/*     Test the SPICELIB DSK body/surface coverage routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare public surface name/ID mapping parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     NAIF_IDS */

/* $ Keywords */

/*     CONVERSION */
/*     NAME */
/*     STRING */
/*     SURFACE */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 02-DEC-2015 (NJB) */

/* -& */

/*     Maximum number of surface name/ID mapping entries: */


/*     Maximum length of a surface name string: */


/*     End of file srftrn.inc. */

/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. User software should not include this file */
/*     due to the volatile nature of this file. */

/*     Declare private surface name/ID mapping parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     NAIF_IDS */

/* $ Keywords */

/*     CONVERSION */
/*     NAME */
/*     STRING */
/*     SURFACE */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 04-FEB-2017 (NJB) */

/*        Original version 03-DEC-2015 (NJB) */

/* -& */

/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing, this size is set to the */
/*     first prime number greater than MXNSRF defined in the public */
/*     include file */

/*        srftrn.inc. */


/*     Singly-linked list pool lower bound: */


/*     End of file zzsrftrn.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSK body/surface coverage routines */

/*        DSKOBJ */
/*        DSKSRF */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 16-MAY-2016 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_DSKOBJ", (ftnlen)8);

/* --- Case -------------------------------------------------------- */

    tcase_("Setup: create DSK files", (ftnlen)23);

/*     Start fresh. Since we're going to create a DSK and */
/*     then append to it, we don't want to create an ever-growing */
/*     monster file. */

    if (exists_("dskobj_test0.bds", (ftnlen)16)) {
	delfil_("dskobj_test0.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a body list. */

    for (i__ = 1; i__ <= 9; ++i__) {
	bodlst[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("bodlst", 
		i__1, "f_dskobj__", (ftnlen)220)] = i__ * 100 + 99;
    }
    bodlst[9] = 10;

/*     Create a DSK file containing NDSK0 segments. */

    for (i__ = 1; i__ <= 10; ++i__) {

/*        Generate a body ID for the Ith segment. Pick */
/*        a body from the body list. Recall we need to */
/*        restrict the body to the set for which there */
/*        are built-in body-fixed frame definitions. */

	j = (i__ - 1) % 10 + 1;
	bodyid = bodlst[(i__1 = j - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"bodlst", i__1, "f_dskobj__", (ftnlen)236)];
	cidfrm_(&bodyid, &framid, frname, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	t_smldsk__(&bodyid, &i__, frname, "dskobj_test0.bds", (ftnlen)32, (
		ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a DSK file containing NDSK1 segments. */

    if (exists_("dskobj_test1.bds", (ftnlen)16)) {
	delfil_("dskobj_test1.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 1000; ++i__) {

/*        Generate a body ID for the Ith segment. Pick */
/*        a body from the body list. Recall we need to */
/*        restrict the body to the set for which there */
/*        are built-in body-fixed frame definitions. */

	j = (i__ - 1) % 10 + 1;
	bodyid = bodlst[(i__1 = j - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"bodlst", i__1, "f_dskobj__", (ftnlen)266)];
	cidfrm_(&bodyid, &framid, frname, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	i__1 = -i__;
	t_smldsk__(&bodyid, &i__1, frname, "dskobj_test1.bds", (ftnlen)32, (
		ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check the body set for DSK0.", (ftnlen)28);

/*     Create the expected body set. */

    ssizei_(&c__5000, xbdset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 10; ++i__) {
	insrti_(&bodlst[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"bodlst", i__1, "f_dskobj__", (ftnlen)291)], xbdset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Get the body set for DSK0. */

    ssizei_(&c__5000, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("dskobj_test0.bds", bodset, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check set cardinality. */

    n = cardi_(bodset);
    xn = 10;
    chcksi_("CARDI(BODSET)", &n, "=", &xn, &c__0, ok, (ftnlen)13, (ftnlen)1);
    if (*ok) {

/*        Check the body set. */

	chckai_("BODSET", &bodset[6], "=", &xbdset[6], &xn, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check the body set for DSK1.", (ftnlen)28);

/*     Create the expected body set. */

    ssizei_(&c__5000, xbdset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 10; ++i__) {
	insrti_(&bodlst[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"bodlst", i__1, "f_dskobj__", (ftnlen)333)], xbdset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Get the body set for DSK1. */

    ssizei_(&c__5000, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("dskobj_test1.bds", bodset, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check set cardinality. */

    n = cardi_(bodset);
    xn = 10;
    chcksi_("CARDI(BODSET)", &n, "=", &xn, &c__0, ok, (ftnlen)13, (ftnlen)1);
    if (*ok) {

/*        Check the body set. */

	chckai_("BODSET", &bodset[6], "=", &xbdset[6], &xn, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check the surface set for each body in DSK0.", (ftnlen)44);
    ssizei_(&c__5000, srfset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__5000, xsfset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardi_(&c__0, bodset);
    dskobj_("dskobj_test0.bds", bodset, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(bodset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	scardi_(&c__0, srfset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create the expected surface set. */

	scardi_(&c__0, xsfset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	bodyid = bodset[(i__2 = i__ + 5) < 5006 && 0 <= i__2 ? i__2 : s_rnge(
		"bodset", i__2, "f_dskobj__", (ftnlen)390)];
	j = isrchi_(&bodyid, &c__10, bodlst);
	k = j;
	while(k <= 10) {
	    insrti_(&k, xsfset);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    k += 10;
	}
	xn = cardi_(xsfset);
	dsksrf_("dskobj_test0.bds", &bodset[(i__2 = i__ + 5) < 5006 && 0 <= 
		i__2 ? i__2 : s_rnge("bodset", i__2, "f_dskobj__", (ftnlen)
		407)], srfset, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the cardinality of the surface set. */

	n = cardi_(srfset);
	chcksi_("CARDI(SRFSET)", &n, "=", &xn, &c__0, ok, (ftnlen)13, (ftnlen)
		1);

/*        Check the surface set itself. */

	if (*ok) {
	    chckai_("SRFSET", &srfset[6], "=", &xsfset[6], &xn, ok, (ftnlen)6,
		     (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check the surface set for each body in DSK1.", (ftnlen)44);
    ssizei_(&c__5000, srfset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__5000, xsfset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardi_(&c__0, bodset);
    dskobj_("dskobj_test1.bds", bodset, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(bodset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	scardi_(&c__0, srfset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create the expected surface set. */

	scardi_(&c__0, xsfset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	bodyid = bodset[(i__2 = i__ + 5) < 5006 && 0 <= i__2 ? i__2 : s_rnge(
		"bodset", i__2, "f_dskobj__", (ftnlen)454)];
	j = isrchi_(&bodyid, &c__10, bodlst);
	k = j;
	while(k <= 1000) {
	    i__2 = -k;
	    insrti_(&i__2, xsfset);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    k += 10;
	}
	xn = cardi_(xsfset);
	dsksrf_("dskobj_test1.bds", &bodset[(i__2 = i__ + 5) < 5006 && 0 <= 
		i__2 ? i__2 : s_rnge("bodset", i__2, "f_dskobj__", (ftnlen)
		471)], srfset, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the cardinality of the surface set. */

	n = cardi_(srfset);
	chcksi_("CARDI(SRFSET)", &n, "=", &xn, &c__0, ok, (ftnlen)13, (ftnlen)
		1);

/*        Check the surface set itself. */

	if (*ok) {
	    chckai_("SRFSET", &srfset[6], "=", &xsfset[6], &xn, ok, (ftnlen)6,
		     (ftnlen)1);
	}
    }
/* *********************************************************************** */


/*     DSKOBJ error cases */


/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKOBJ: output cell is too small.", (ftnlen)33);
    ssizei_(&c__3, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("dskobj_test0.bds", bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(CELLTOOSMALL)", ok, (ftnlen)19);

/*     This call will leave the DSK closed. */


/* --- Case -------------------------------------------------------- */

    tcase_("DSKOBJ: DSK doesn't exist.", (ftnlen)26);
    scardi_(&c__0, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("XXX", bodset, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FILENOTFOUND)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKOBJ: file is a non-DSK DAS.", (ftnlen)30);
    tstek_("dskobj_test0.bes", &c__1, &c__10, &c_false, &handle, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardi_(&c__0, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("dskobj_test0.bes", bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKOBJ: file is a DAF.", (ftnlen)22);
    tstspk_("dskobj_test0.bsp", &c_false, &c_true, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskobj_("dskobj_test0.bsp", bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDARCHTYPE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKOBJ: file is an XFR file.", (ftnlen)28);
    if (exists_("dskobj_test0.xfr", (ftnlen)16)) {
	delfil_("dskobj_test0.xfr", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    txtopn_("dskobj_test0.xfr", &unit, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasbt_("dskobj_test0.bds", &unit, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cl__1.cerr = 0;
    cl__1.cunit = unit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    dskobj_("dskobj_test0.xfr", bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDFORMAT)", ok, (ftnlen)20);
/* *********************************************************************** */


/*     DSKSRF error cases */


/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKSRF: DSK doesn't exist.", (ftnlen)26);
    scardi_(&c__0, srfset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dsksrf_("XXX", &c__1, srfset, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FILENOTFOUND)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKSRF: file is a non-DSK DAS.", (ftnlen)30);
    dsksrf_("dskobj_test0.bes", &c__1, srfset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKSRF: file is a DAF.", (ftnlen)22);
    dsksrf_("dskobj_test0.bsp", &c__1, srfset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDARCHTYPE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKSRF: file is an XFR file.", (ftnlen)28);
    dsksrf_("dskobj_test0.xfr", &c__1, bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(INVALIDFORMAT)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKSRF: output cell is too small.", (ftnlen)33);
    ssizei_(&c__3, bodset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dsksrf_("dskobj_test1.bds", &c__199, bodset, (ftnlen)16);
    chckxc_(&c_true, "SPICE(CELLTOOSMALL)", ok, (ftnlen)19);

/*     This call will leave the DSK closed. */


/* --- Case -------------------------------------------------------- */

    tcase_("Clean up: delete kernels.", (ftnlen)25);
    delfil_("dskobj_test0.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskobj_test0.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskobj_test1.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskobj_test0.xfr", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskobj_test0.bes", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dskobj__ */

