/*

-Procedure f_dskobj_c ( DSK coverage API tests )

 
-Abstract
 
   Exercise the CSPICE wrappers dskobj_c and dsksrf_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_dskobj_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrappers

      dskobj_c
      dsksrf_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)
  
       Previous version 02-AUG-2016 (NJB)

-Index_Entries

   test dsk coverage routines

-&
*/

{ /* Begin f_dskobj_c */

 
   /*
   Constants
   */
   #define DSK0               "dskobj_test0.bds"
   #define DSK1               "dskobj_test1.bds"
   #define SPK0               "dskobj_test0.bsp"
   #define SPK1               "dskobj_test1.bsp"
   #define XFR0               "dskobj_test0.xfr"
   #define EK0                "dskobj_test0.bes"


   #define FRNMLN          33
   #define MAXSIZ          5000
   #define NBOD            10
   #define NDSK0           10
   #define NDSK1           1000
   


   /*
   Local variables
   */
   SPICEINT_CELL           ( bodset, MAXSIZ );
   SPICEINT_CELL           ( srfset, MAXSIZ );
   SPICEINT_CELL           ( xbdset, MAXSIZ );
   SPICEINT_CELL           ( xsfset, MAXSIZ );

   SpiceBoolean            found;

   SpiceChar               frname  [ FRNMLN ];

   SpiceInt                bodlst  [NBOD] ;
   SpiceInt                bodyid;
   SpiceInt                framid;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                surfid;
   SpiceInt                unit;
   SpiceInt                xn;

   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dskobj_c" );
   

   /*
   *********************************************************************
   *
   *
   *   setup
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create DSK files." );

   if ( exists_c(DSK0) )
   {
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile ( DSK0 );

   }
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create a body list. 
   */
   for ( i = 0;  i < NBOD-1;  i++ )
   {
      bodlst[i] = 100*(i+1) + 99;
   }

   bodlst[NBOD-1] = 10;

   /*
   Create a DSK file containing NDSK0 segments. 
   */
   for ( i = 0;  i < NDSK0;  i++ )
   {
      /*
      Generate a body ID for the Ith segment. Pick a body from the body
      list. Recall we need to restrict the body to the set for which
      there are built-in body-fixed frame definitions.
      */

      j      = i%NBOD;
      bodyid = bodlst[j];

      cidfrm_c ( bodyid, FRNMLN, &framid, frname, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "cidfrm found", found, SPICETRUE, ok );

      surfid = i + 1;

      t_smldsk_c ( bodyid, surfid, frname, DSK0 );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

 
   /*
   Create a DSK file containing NDSK1 segments. 
   */
   for ( i = 0;  i < NDSK1;  i++ )
   {
      /*
      Generate a body ID for the Ith segment. Pick a body from the body
      list. Recall we need to restrict the body to the set for which
      there are built-in body-fixed frame definitions.
      */

      j      = i%NBOD;
      bodyid = bodlst[j];

      cidfrm_c ( bodyid, FRNMLN, &framid, frname, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "cidfrm found", found, SPICETRUE, ok );

      surfid = -(i + 1);

      t_smldsk_c ( bodyid, surfid, frname, DSK1 );
      chckxc_c ( SPICEFALSE, " ", ok );
   }


   /*
   *********************************************************************
   *
   *
   *   dskobj_c normal cases
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Check the body set for DSK0." );

   /*
   Create the expected body set. 
   */
   for ( i = 0;  i < NBOD;  i++ )
   {
      insrti_c ( bodlst[i], &xbdset );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Get the body set for DSK0. 
   */

   dskobj_c ( DSK0, &bodset );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check cell cardinality. 
   */
   n  = card_c( &bodset );
   xn = NBOD;

   chcksi_c ( "card_c(&bodset)", n, "=", xn, 0, ok );

   if ( ok )
   {
      /*
      Check the body set. 
      */
      chckai_c ( "bodset", (SpiceInt *)(bodset.data), "=",
                           (SpiceInt *)(xbdset.data), xn,  ok );      
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Check the body set for DSK1." );

   /*
   Create the expected body set. 
   */
   for ( i = 0;  i < NBOD;  i++ )
   {
      insrti_c ( bodlst[i], &xbdset );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Get the body set for DSK1. 
   */

   dskobj_c ( DSK1, &bodset );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check cell cardinality. 
   */
   n  = card_c( &bodset );
   xn = NBOD;

   chcksi_c ( "card_c(&bodset)", n, "=", xn, 0, ok );

   if ( ok )
   {
      /*
      Check the body set. 
      */
      chckai_c ( "bodset", (SpiceInt *)(bodset.data), "=",
                           (SpiceInt *)(xbdset.data), xn,  ok );      
   }


   /*
   *********************************************************************
   *
   *
   *   dsksrf_c normal cases
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Check the surface set for each body in DSK0." );


   scard_c  ( 0, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( DSK0, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );


   for ( i = 0;  i < card_c(&bodset);  i++ )
   {
      scard_c( 0, &srfset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Create the expected surface set. 
      */
      scard_c( 0, &xsfset );
      chckxc_c ( SPICEFALSE, " ", ok );

      SPICE_CELL_GET_I (&bodset, i, &bodyid );

      j = isrchi_c ( bodyid, NBOD, bodlst );
      k = j;


      while ( k < NDSK0 )
      {
         surfid = k + 1;

         insrti_c ( surfid, &xsfset );
         chckxc_c ( SPICEFALSE, " ", ok );

         k  += NBOD;
      }

      xn = card_c( &xsfset );
      
      dsksrf_c ( DSK0, bodyid, &srfset );
      chckxc_c ( SPICEFALSE, " ", ok );


      /*
      Check the cardinality of the surface set. 
      */
      if ( ok )
      {
         chckai_c ( "srfset", (SpiceInt *)(srfset.data), "=",
                              (SpiceInt *)(xsfset.data), xn,  ok );
      }      
   }
     


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Check the surface set for each body in DSK1." );


   scard_c  ( 0, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( DSK1, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );


   for ( i = 0;  i < card_c(&bodset);  i++ )
   {
      scard_c( 0, &srfset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Create the expected surface set. 
      */
      scard_c( 0, &xsfset );
      chckxc_c ( SPICEFALSE, " ", ok );

      SPICE_CELL_GET_I (&bodset, i, &bodyid );

      j = isrchi_c ( bodyid, NBOD, bodlst );
      k = j;

      while ( k < NDSK1 )
      {
         surfid = -(k + 1);

         insrti_c ( surfid, &xsfset );
         chckxc_c ( SPICEFALSE, " ", ok );

         k  += NBOD;
      }

      xn = card_c( &xsfset );
      
      dsksrf_c ( DSK1, bodyid, &srfset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the cardinality of the surface set. 
      */
      if ( ok )
      {
         chckai_c ( "srfset", (SpiceInt *)(srfset.data), "=",
                              (SpiceInt *)(xsfset.data), xn,  ok );
      }      
   }
     


   /*
   *********************************************************************
   *
   *
   *   dskobj_c error cases
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskobj_c: output cell is too small." );

   ssize_c  ( 3, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( DSK0, &bodset );
   chckxc_c ( SPICETRUE, "SPICE(CELLTOOSMALL)", ok );

   /*
   This call will leave the DSK closed. 
   */


   /*
   Because dskobj_c accepts an input file name, rather than
   a handle, there are quite a few things that can go wrong
   with the file itself. 
   */

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskobj_c: DSK doesn't exist." );

   scard_c  ( 0, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( "xxx", &bodset );
   chckxc_c ( SPICETRUE, "SPICE(FILENOTFOUND)", ok );


   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskobj_c: file is a non-DSK DAS file." );


   tstek_c  ( EK0, 1, 10, SPICEFALSE, &handle, ok );
   chckxc_c ( SPICEFALSE, " ", ok );

   scard_c  ( 0, &bodset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( EK0, &bodset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFILETYPE)", ok );

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskobj_c: file is a DAF." );

   tstspk_c ( SPK0, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( SPK0, &bodset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARCHTYPE)", ok );


   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskobj_c: file is a transfer file." );

   /*
   This test relies on direct calls to f2c'd routines; it can't
   be directly replicated in MATLAB and IDL.
   */

   if ( exists_c(XFR0) )
   {
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile( XFR0 );
   }
   chckxc_c ( SPICEFALSE, " ", ok );

   txtopn_  ( XFR0, &unit, (ftnlen)(strlen(XFR0)) );
   chckxc_c ( SPICEFALSE, " ", ok );

   dasbt_   ( DSK0, &unit, (ftnlen)(strlen(XFR0))  );
   chckxc_c ( SPICEFALSE, " ", ok );

   ftncls_c ( unit );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskobj_c ( XFR0, &bodset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFORMAT)", ok );




   /*
   *********************************************************************
   *
   *
   *   dsksrf_c error cases
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dsksrf_c: output cell is too small." );

   ssize_c  ( 3, &srfset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dsksrf_c ( DSK1, 10, &srfset );
   chckxc_c ( SPICETRUE, "SPICE(CELLTOOSMALL)", ok );

   /*
   This call will leave the DSK closed. 
   */


   /*
   Because dsksrf_c accepts an input file name, rather than
   a handle, there are quite a few things that can go wrong
   with the file itself. 
   */

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dsksrf_c: DSK doesn't exist." );

   ssize_c  ( MAXSIZ, &srfset );
   chckxc_c ( SPICEFALSE, " ", ok );

   scard_c  ( 0,      &srfset );
   chckxc_c ( SPICEFALSE, " ", ok );

   dsksrf_c ( "xxx", 1, &srfset );
   chckxc_c ( SPICETRUE, "SPICE(FILENOTFOUND)", ok );


   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dsksrf_c: file is a non-DSK DAS file." );

 
   dsksrf_c ( EK0, 1, &srfset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFILETYPE)", ok );

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dsksrf_c: file is a DAF." );
 

   dsksrf_c ( SPK0, 1, &srfset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARCHTYPE)", ok );


   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dsksrf_c: file is a transfer file." );

   /*
   This test relies on direct calls to f2c'd routines; it can't
   be directly replicated in MATLAB and IDL.
   */
   dsksrf_c ( XFR0, 1, &srfset );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFORMAT)", ok );





   /*
   *********************************************************************
   *
   *
   *   clean up
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up: unload and delete kernels." );


   kclear_c();

   removeFile ( SPK0 );
   removeFile ( DSK0 );
   removeFile ( DSK1 );
   removeFile ( XFR0 );
   removeFile ( EK0  );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dskobj_c */

