/* f_dskrb2.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10000 = 10000;
static integer c__20000 = 20000;
static logical c_false = FALSE_;
static doublereal c_b10 = 3e4;
static doublereal c_b11 = 4e4;
static doublereal c_b12 = 5e4;
static doublereal c_b13 = 0.;
static doublereal c_b15 = 1.;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__10 = 10;
static integer c__0 = 0;

/* $Procedure      F_DSKRB2 ( Test DSKRB2 ) */
/* Subroutine */ int f_dskrb2__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer nlat;
    static doublereal dist;
    static integer nlon;
    static doublereal xmin, xmax;
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__, f;
    static integer i__, n;
    static doublereal r__, s;
    extern /* Subroutine */ int tcase_(char *, ftnlen), dskp02_(integer *, 
	    integer *, integer *, integer *, integer *, integer *), vpack_(
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal pnear[3];
    extern doublereal dpmin_(void), dpmax_(void);
    extern /* Subroutine */ int dskv02_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int dskz02_(integer *, integer *, integer *, 
	    integer *), topen_(char *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    extern /* Subroutine */ int pltnp_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *);
    static doublereal verts[30000]	/* was [3][10000] */;
    extern /* Subroutine */ int t_success__(logical *), dskrb2_(integer *, 
	    doublereal *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal mncor3;
    extern /* Subroutine */ int vlcom3_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal mxcor3;
    static integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *);
    static integer np;
    extern /* Subroutine */ int delfil_(char *, ftnlen), cleard_(integer *, 
	    doublereal *), chcksd_(char *, doublereal *, char *, doublereal *,
	     doublereal *, logical *, ftnlen, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    static integer nv;
    extern /* Subroutine */ int recgeo_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), chcksl_(
	    char *, logical *, logical *, logical *, ftnlen), kclear_(void);
    extern logical exists_(char *, ftnlen);
    static char fixref[32];
    static doublereal center[3], corpar[10], normal[3], pltctr[3], alt, lat, 
	    rcross;
    static integer bodyid, corsys, plates[60000]	/* was [3][20000] */, 
	    ncross, npolyv;
    static doublereal lon;
    static integer shapix, surfid;
    extern /* Subroutine */ int dasopr_(char *, integer *, ftnlen), dascls_(
	    integer *);
    static doublereal tol;
    extern /* Subroutine */ int t_torus__(integer *, integer *, char *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, char *, ftnlen, ftnlen);

/* $ Abstract */

/*     Test the SPICELIB routine DSKRB2. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine DSKRB2. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 09-AUG-2016  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_DSKRB2", (ftnlen)8);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/*     Loop over the set of shapes. */

    for (shapix = 1; shapix <= 2; ++shapix) {

/* --- Case -------------------------------------------------------- */

	if (shapix == 1) {
	    tcase_("Set-up: create plates for a tessellated ellipsoid. NLON "
		    "= 100; NLAT = 50; NV = 4902; NP = 9800.", (ftnlen)95);
	    a = 3e3;
	    b = a;
	    c__ = 1e3;
	    nlon = 100;
	    nlat = 50;
	    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, 
		    verts, &np, plates);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	} else if (shapix == 2) {

/*           Create a plate set consisting of a tessellated torus. */
/*           We'll create a DSK file containing the model, then */
/*           we'll extract the vertices and plates. We don't need */
/*           the file; it's just simpler this way. */

	    tcase_("Set-up: create plates for a tessellated torus.", (ftnlen)
		    46);
	    bodyid = 401;
	    surfid = 1;
	    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
	    npolyv = 100;
	    ncross = 100;
	    r__ = 3e3;
	    rcross = 300.;
	    vpack_(&c_b10, &c_b11, &c_b12, center);
	    vpack_(&c_b13, &c_b13, &c_b15, normal);
	    if (exists_("dskrb2_test.bds", (ftnlen)15)) {
		delfil_("dskrb2_test.bds", (ftnlen)15);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    t_torus__(&bodyid, &surfid, fixref, &npolyv, &ncross, &r__, &
		    rcross, center, normal, "dskrb2_test.bds", (ftnlen)32, (
		    ftnlen)15);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Extract the plate set from the DSK we just created. */

	    dasopr_("dskrb2_test.bds", &handle, (ftnlen)15);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dlabfs_(&handle, dladsc, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DLABFS found", &found, &c_true, ok, (ftnlen)12);
	    dskz02_(&handle, dladsc, &nv, &np);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskv02_(&handle, dladsc, &c__1, &nv, &n, verts);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dskp02_(&handle, dladsc, &c__1, &np, &n, plates);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dascls_(&handle);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/* --- Case -------------------------------------------------------- */

	tcase_("Check the radius extents of the plate set.", (ftnlen)42);
	corsys = 1;
	cleard_(&c__10, corpar);
	dskrb2_(&nv, verts, &np, plates, &corsys, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the expected radius extents. */

	xmax = dpmin_();
	xmin = dpmax_();

/*        The maximum radius occurs at a vertex. */

	i__1 = nv;
	for (i__ = 1; i__ <= i__1; ++i__) {
/* Computing MAX */
	    d__1 = xmax, d__2 = vnorm_(&verts[(i__2 = i__ * 3 - 3) < 30000 && 
		    0 <= i__2 ? i__2 : s_rnge("verts", i__2, "f_dskrb2__", (
		    ftnlen)335)]);
	    xmax = max(d__1,d__2);
	}

/*        The minimum radius could occur in the interior of */
/*        a plate. We must find the minimum distance of each */
/*        plate from the origin. */

	i__1 = np;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    pltnp_(origin, &verts[(i__3 = plates[(i__2 = i__ * 3 - 3) < 60000 
		    && 0 <= i__2 ? i__2 : s_rnge("plates", i__2, "f_dskrb2__",
		     (ftnlen)345)] * 3 - 3) < 30000 && 0 <= i__3 ? i__3 : 
		    s_rnge("verts", i__3, "f_dskrb2__", (ftnlen)345)], &verts[
		    (i__5 = plates[(i__4 = i__ * 3 - 2) < 60000 && 0 <= i__4 ?
		     i__4 : s_rnge("plates", i__4, "f_dskrb2__", (ftnlen)345)]
		     * 3 - 3) < 30000 && 0 <= i__5 ? i__5 : s_rnge("verts", 
		    i__5, "f_dskrb2__", (ftnlen)345)], &verts[(i__7 = plates[(
		    i__6 = i__ * 3 - 1) < 60000 && 0 <= i__6 ? i__6 : s_rnge(
		    "plates", i__6, "f_dskrb2__", (ftnlen)345)] * 3 - 3) < 
		    30000 && 0 <= i__7 ? i__7 : s_rnge("verts", i__7, "f_dsk"
		    "rb2__", (ftnlen)345)], pnear, &dist);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xmin = min(xmin,dist);
	}
	chcksd_("MNCOR3", &mncor3, "~/", &xmin, &tol, ok, (ftnlen)6, (ftnlen)
		2);
	chcksd_("MXCOR3", &mxcor3, "~/", &xmax, &tol, ok, (ftnlen)6, (ftnlen)
		2);

/* --- Case -------------------------------------------------------- */

	tcase_("Check the Z extents of the plate set.", (ftnlen)37);

/*        The coordinate system is rectangular. */

	corsys = 3;
	cleard_(&c__10, corpar);
	dskrb2_(&nv, verts, &np, plates, &corsys, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the expected Z extents. The extreme values occur at */
/*        vertices. */

	xmax = dpmin_();
	xmin = dpmax_();
	i__1 = nv;
	for (i__ = 1; i__ <= i__1; ++i__) {
/* Computing MIN */
	    d__1 = xmin, d__2 = verts[(i__2 = i__ * 3 - 1) < 30000 && 0 <= 
		    i__2 ? i__2 : s_rnge("verts", i__2, "f_dskrb2__", (ftnlen)
		    388)];
	    xmin = min(d__1,d__2);
/* Computing MAX */
	    d__1 = xmax, d__2 = verts[(i__2 = i__ * 3 - 1) < 30000 && 0 <= 
		    i__2 ? i__2 : s_rnge("verts", i__2, "f_dskrb2__", (ftnlen)
		    389)];
	    xmax = max(d__1,d__2);
	}
	chcksd_("MNCOR3", &mncor3, "~/", &xmin, &tol, ok, (ftnlen)6, (ftnlen)
		2);
	chcksd_("MXCOR3", &mxcor3, "~/", &xmax, &tol, ok, (ftnlen)6, (ftnlen)
		2);

/* --- Case -------------------------------------------------------- */

	tcase_("Check the altitude extents of the plate set. Use the spheroi"
		"d parameters that were used to create the tessellated sphero"
		"id.", (ftnlen)123);

/*        The coordinate system is rectangular. */

	corsys = 4;
	cleard_(&c__10, corpar);
	corpar[0] = a;
	f = (a - c__) / a;
	corpar[1] = f;
	dskrb2_(&nv, verts, &np, plates, &corsys, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the expected altitude extents. The maximum */
/*        values occur at vertices The minimum values can */
/*        occur with plates' interiors. */

	xmax = dpmin_();
	xmin = dpmax_();
	i__1 = nv;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    recgeo_(&verts[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 : 
		    s_rnge("verts", i__2, "f_dskrb2__", (ftnlen)430)], &a, &f,
		     &lon, &lat, &alt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xmax = max(xmax,alt);
	}

/*        Minimum altitudes are a little more complicated. We don't */
/*        compute exact minima; we simply compute a reasonable lower */
/*        bound for the altitude of each plate. This is done by finding */
/*        the altitude of the plate's center and subtracting the maximum */
/*        distance from the center of the plate's vertices. */

	i__1 = np;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s = .33333333333333331;
	    vlcom3_(&s, &verts[(i__3 = plates[(i__2 = i__ * 3 - 3) < 60000 && 
		    0 <= i__2 ? i__2 : s_rnge("plates", i__2, "f_dskrb2__", (
		    ftnlen)448)] * 3 - 3) < 30000 && 0 <= i__3 ? i__3 : 
		    s_rnge("verts", i__3, "f_dskrb2__", (ftnlen)448)], &s, &
		    verts[(i__5 = plates[(i__4 = i__ * 3 - 2) < 60000 && 0 <= 
		    i__4 ? i__4 : s_rnge("plates", i__4, "f_dskrb2__", (
		    ftnlen)448)] * 3 - 3) < 30000 && 0 <= i__5 ? i__5 : 
		    s_rnge("verts", i__5, "f_dskrb2__", (ftnlen)448)], &s, &
		    verts[(i__7 = plates[(i__6 = i__ * 3 - 1) < 60000 && 0 <= 
		    i__6 ? i__6 : s_rnge("plates", i__6, "f_dskrb2__", (
		    ftnlen)448)] * 3 - 3) < 30000 && 0 <= i__7 ? i__7 : 
		    s_rnge("verts", i__7, "f_dskrb2__", (ftnlen)448)], pltctr)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    recgeo_(pltctr, &a, &f, &lon, &lat, &alt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
	    d__1 = vdist_(pltctr, &verts[(i__3 = plates[(i__2 = i__ * 3 - 3) <
		     60000 && 0 <= i__2 ? i__2 : s_rnge("plates", i__2, "f_d"
		    "skrb2__", (ftnlen)456)] * 3 - 3) < 30000 && 0 <= i__3 ? 
		    i__3 : s_rnge("verts", i__3, "f_dskrb2__", (ftnlen)456)]),
		     d__2 = vdist_(pltctr, &verts[(i__5 = plates[(i__4 = i__ *
		     3 - 2) < 60000 && 0 <= i__4 ? i__4 : s_rnge("plates", 
		    i__4, "f_dskrb2__", (ftnlen)456)] * 3 - 3) < 30000 && 0 <=
		     i__5 ? i__5 : s_rnge("verts", i__5, "f_dskrb2__", (
		    ftnlen)456)]), d__1 = max(d__1,d__2), d__2 = vdist_(
		    pltctr, &verts[(i__7 = plates[(i__6 = i__ * 3 - 1) < 
		    60000 && 0 <= i__6 ? i__6 : s_rnge("plates", i__6, "f_ds"
		    "krb2__", (ftnlen)456)] * 3 - 3) < 30000 && 0 <= i__7 ? 
		    i__7 : s_rnge("verts", i__7, "f_dskrb2__", (ftnlen)456)]);
	    dist = max(d__1,d__2);
/* Computing MIN */
	    d__1 = xmin, d__2 = alt - dist;
	    xmin = min(d__1,d__2);
	}

/*        Note: use absolute comparisons for the ellipsoid case, */
/*        since the expected value may be zero. */

	tol = 1e-12;
	if (shapix == 1) {
	    chcksd_("MNCOR3", &mncor3, "~", &xmin, &tol, ok, (ftnlen)6, (
		    ftnlen)1);
	    chcksd_("MXCOR3", &mxcor3, "~", &xmax, &tol, ok, (ftnlen)6, (
		    ftnlen)1);
	} else {
	    chcksd_("MNCOR3", &mncor3, "~/", &xmin, &tol, ok, (ftnlen)6, (
		    ftnlen)2);
	    chcksd_("MXCOR3", &mxcor3, "~/", &xmax, &tol, ok, (ftnlen)6, (
		    ftnlen)2);
	}
    }
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Bad coordinate system.", (ftnlen)22);
    dskrb2_(&nv, verts, &np, plates, &c__0, corpar, &mncor3, &mxcor3);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad coordinate parameters.", (ftnlen)26);
    corsys = 4;
    corpar[0] = 0.;
    dskrb2_(&nv, verts, &np, plates, &corsys, corpar, &mncor3, &mxcor3);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    corsys = 4;
    corpar[0] = a;
    corpar[1] = 2.;
    dskrb2_(&nv, verts, &np, plates, &corsys, corpar, &mncor3, &mxcor3);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     Clean up. */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Clean up.", (ftnlen)9);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskrb2_test.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dskrb2__ */

