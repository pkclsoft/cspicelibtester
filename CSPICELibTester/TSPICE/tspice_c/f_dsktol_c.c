/*

-Procedure f_dsktol_c ( DSK tolerance API tests )

 
-Abstract
 
   Exercise the CSPICE wrappers dskgtl_c and dskstl_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_dsktol_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrappers

      dskgtl_c
      dskstl_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Previous version 02-AUG-2016 (NJB)

-Index_Entries

   test dsk tolerance routines

-&
*/

{ /* Begin f_dsktol_c */

 
   /*
   Constants
   */
  

   /*
   Local variables
   */
   SpiceDouble             dpval;
   SpiceDouble             newval;
 

   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dsktol_c" );
   

   /*
   *********************************************************************
   *
   *
   *   dskgtl_c tests
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get expansion fraction." );

   dskgtl_c ( SPICE_DSK_KEYXFR, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "XFRACT", dpval, "=", SPICE_DSK_XFRACT, 0.0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get greedy segment parameter." );

   dskgtl_c ( SPICE_DSK_KEYSGR, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "SGREED", dpval, "=", SPICE_DSK_SGREED, 0.0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get segment pad margin." );

   dskgtl_c ( SPICE_DSK_KEYSPM, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "SGPADM", dpval, "=", SPICE_DSK_SGPADM, 0.0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get surface-point membership margin." );

   dskgtl_c ( SPICE_DSK_KEYPTM, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "PTMEMM", dpval, "=", SPICE_DSK_PTMEMM, 0.0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get angular rounding margin." );

   dskgtl_c ( SPICE_DSK_KEYAMG, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "ANGMRG", dpval, "=", SPICE_DSK_ANGMRG, 0.0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: get longitude alias margin." );

   dskgtl_c ( SPICE_DSK_KEYLAL, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "LONALI", dpval, "=", SPICE_DSK_LONALI, 0.0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: keyword out of range." );

   dskgtl_c ( 0, &dpval );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dskgtl_c ( 7, &dpval );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );


   /*
   *********************************************************************
   *
   *
   *   dskstl_c tests
   *
   *
   *********************************************************************
   */
 
  

    /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: set expansion fraction." );


   newval = 1.0e-3;

   dskstl_c ( SPICE_DSK_KEYXFR, newval );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskgtl_c ( SPICE_DSK_KEYXFR, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "XFRACT", dpval, "=", newval, 0.0, ok );

   /*
   Restore default. 
   */
   dskstl_c ( SPICE_DSK_KEYXFR, SPICE_DSK_XFRACT );
   chckxc_c ( SPICEFALSE, " ", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: set greedy segment parameter." );

   newval = 1.0e-2;

   dskstl_c ( SPICE_DSK_KEYSGR, newval );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskgtl_c ( SPICE_DSK_KEYSGR, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "SGREED", dpval, "=", newval, 0.0, ok );

   /*
   Restore default. 
   */
   dskstl_c ( SPICE_DSK_KEYSGR, SPICE_DSK_SGREED );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: set segment pad margin." );

   newval = 1.0e-2;

   dskstl_c ( SPICE_DSK_KEYSPM, newval );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskgtl_c ( SPICE_DSK_KEYSPM, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "SGPADM", dpval, "=", newval, 0.0, ok );

   /*
   Restore default. 
   */
   dskstl_c ( SPICE_DSK_KEYSPM, SPICE_DSK_SGPADM );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: set surface-point membership margin." );

   newval = 1.e-3;

   dskstl_c ( SPICE_DSK_KEYPTM, newval );
   chckxc_c ( SPICEFALSE, " ", ok );

   dskgtl_c ( SPICE_DSK_KEYPTM, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "PTMEMM", dpval, "=", newval, 0.0, ok );

   /*
   Restore default. 
   */
   dskstl_c ( SPICE_DSK_KEYPTM, SPICE_DSK_PTMEMM );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: try to set angular rounding margin." );

   /*
   This parameter is not adjustable.
   */

   newval = 1.0e-3;

   dskstl_c ( SPICE_DSK_KEYAMG, newval );
   chckxc_c ( SPICETRUE, "SPICE(IMMUTABLEVALUE)", ok );

   /*
   Make sure no change occurred. 
   */
   dskgtl_c ( SPICE_DSK_KEYAMG, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "ANGMRG", dpval, "=", SPICE_DSK_ANGMRG, 0.0, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskstl_c: try to set longitude alias margin." );

   /*
   This parameter is not adjustable.
   */

   newval = 1.0e-3;

   dskstl_c ( SPICE_DSK_KEYLAL, newval );
   chckxc_c ( SPICETRUE, "SPICE(IMMUTABLEVALUE)", ok );

   /*
   Make sure no change occurred. 
   */
   dskgtl_c ( SPICE_DSK_KEYLAL, &dpval );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "LONALI", dpval, "=", SPICE_DSK_LONALI, 0.0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskgtl_c: keyword out of range." );

   dskstl_c ( 0, dpval );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   dskstl_c ( 7, dpval );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dsktol_c */

