/* f_dskw02.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__10000 = 10000;
static integer c__20000 = 20000;
static doublereal c_b16 = .23;
static integer c_b17 = 1000000;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__10 = 10;
static integer c__0 = 0;
static integer c__9 = 9;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__3 = 3;
static integer c__5 = 5;
static integer c__6 = 6;
static integer c__7 = 7;
static integer c__8 = 8;
static integer c__14 = 14;
static integer c_b149 = 100000;
static integer c__11 = 11;
static integer c__12 = 12;
static integer c__13 = 13;
static integer c__15 = 15;
static integer c__24 = 24;
static doublereal c_b286 = 0.;
static integer c__16 = 16;
static integer c__17 = 17;
static integer c__18 = 18;
static integer c_n1 = -1;
static integer c_b840 = 16000003;
static integer c_b864 = 32000001;

/* $Procedure      F_DSKW02 ( Test DSK type 2 writer ) */
/* Subroutine */ int f_dskw02__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static integer addr__, nlat;
    static doublereal last;
    static integer nlon;
    static doublereal work[2000000]	/* was [2][1000000] */;
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__;
    static integer surf2;
    static doublereal f;
    static integer i__, j, k, n;
    static doublereal dbuff[30000];
    static integer w;
    static doublereal x, delta;
    static char frame[32];
    static integer ibuff[100000];
    extern /* Subroutine */ int dski02_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), dskd02_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, doublereal 
	    *), dskgd_(integer *, integer *, doublereal *), tcase_(char *, 
	    ftnlen);
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int dskw02_(integer *, integer *, integer *, 
	    integer *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *,
	     integer *, integer *, doublereal *, integer *, ftnlen), movei_(
	    integer *, integer *, integer *);
    static integer xncgr, corix;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal first, xform[9]	/* was [3][3] */;
    extern doublereal twopi_(void);
    static integer dlads2[8];
    extern /* Subroutine */ int t_success__(logical *), dskrb2_(integer *, 
	    doublereal *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *), dskmi2_(integer *, doublereal *, 
	    integer *, integer *, doublereal *, integer *, integer *, integer 
	    *, integer *, logical *, integer *, doublereal *, doublereal *, 
	    integer *);
    static doublereal mncor1, mncor2, mncor3, spaxd2[10], spaxd3[10];
    static integer plats2[60000];
    static doublereal mxcor1, mxcor2, mxcor3;
    static integer spaxi2[1000000], spaxi3[1000000];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static doublereal vrtcs2[30000], vrtcs3[30000], re;
    extern doublereal pi_(void);
    static integer dladsc[8], handle, vlsiz3, np, vpsiz3;
    extern /* Subroutine */ int delfil_(char *, ftnlen), cleard_(integer *, 
	    doublereal *);
    extern doublereal halfpi_(void);
    static doublereal rp;
    static integer framid, nv;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    extern logical exists_(char *, ftnlen);
    static doublereal altlim, corpar[10], dskdsc[24], finscl, maxlon, minlon, 
	    mn32, mn33, mx32, mx33, spaixd[10], tol, vrtces[30000];
    static integer bodyid, corscl, corsys, dclass, np2, nv2, nvxtot, nxtdsc[8]
	    , plates[60000], spaixi[1000000], spxisz, surfid, vgrext[3], han1,
	     han2, han3, vpsize, vlsize, vtxlsz, xnp, xnv;
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen), dskopn_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), dskcls_(
	    integer *, logical *), mxv_(doublereal *, doublereal *, 
	    doublereal *), dasopr_(char *, integer *, ftnlen), dlabfs_(
	    integer *, integer *, logical *), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), dlafns_(integer 
	    *, integer *, integer *, logical *), chcksd_(char *, doublereal *,
	     char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen), 
	    dascls_(integer *), dasopw_(char *, integer *, ftnlen), kclear_(
	    void);

/* $ Abstract */

/*     Test the DSK type 2 writer DSKW02. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSK type 2 writer */

/*        DSKW02 */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-OCT-2016  (NJB) */


/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */

/*     Save everything to avoid stack problems. */


/*     Begin every test family with an open call. */

    topen_("F_DSKW02", (ftnlen)8);

/* --- Case -------------------------------------------------------- */

    tcase_("Create new Mars DSK files.", (ftnlen)26);
    if (exists_("dskw02_test0.bds", (ftnlen)16)) {
	delfil_("dskw02_test0.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("dskw02_test1.bds", (ftnlen)16)) {
	delfil_("dskw02_test1.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }


/*     Create vertices and plates. */

/*     The Mars radii used here need not be consistent with */
/*     the current generic PCK. */

    a = 3396.19;
    b = a;
    c__ = 3376.2;
    nlon = 20;
    nlat = 10;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, vrtces, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a spatial index for the plate set. */

/*     Use a heuristic formula for the fine scale. */

/* Computing MAX */
    d__3 = (doublereal) np;
    d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
    finscl = max(d__1,d__2);

/*     Pick a one-size-fits-all value for the coarse scale. */

    corscl = 10;

/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv, vrtces, &np, plates, &finscl, &corscl, &c_b17, &vpsize, &
		vlsize, &c_true, &spxisz, work, spaixd, spaixi);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv, vrtces, &np, plates, &c__1, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Set segment attribute inputs. */

    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    first = -jyear_() * 100;
    last = jyear_() * 100;
    cleard_(&c__10, corpar);
    dclass = 2;
    bodyid = 499;
    surfid = 1;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(frame, &framid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write the file. */

    dskopn_("dskw02_test0.bds", "dskw02_test0.bds", &c__0, &handle, (ftnlen)
	    16, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&handle, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a second segment having higher resolution. */

    nlon = 30;
    nlat = 15;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv2, vrtcs2, 
	    &np2, plats2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     Create a spatial index for the plate set. */

/*     Use a heuristic formula for the fine scale. */

/* Computing MAX */
    d__3 = (doublereal) np;
    d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
    finscl = max(d__1,d__2);

/*     Pick a one-size-fits-all value for the coarse scale. */

    corscl = 10;

/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv2, vrtcs2, &np2, plats2, &finscl, &corscl, &c_b17, &vpsize,
		 &vlsize, &c_true, &spxisz, work, spaxd2, spaxi2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv2, vrtcs2, &np2, plats2, &c__1, corpar, &mn32, &mx32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     This segment has its own surface ID. */

    surf2 = 2;
    if (*ok) {
	dskw02_(&handle, &bodyid, &surf2, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mn32, &mx32, &first, &
		last, &nv2, vrtcs2, &np2, plats2, spaxd2, spaxi2, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close the file. */

    if (*ok) {
	dskcls_(&handle, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a second DSK file containing data similar to that */
/*     of the first segment. We want the segment's DAS address */
/*     ranges to be identical to those of the first segment, */
/*     but both the integer and d.p. data to be different. To */
/*     achieve this, we'll rotate the vertices to a different */
/*     frame. We'll still label the frame as IAU_MARS. */


/*     Let XFORM be a matrix that permutes the standard basis */
/*     vectors. */

    cleard_(&c__9, xform);
    xform[3] = 1.;
    xform[7] = 1.;
    xform[2] = 1.;
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	k = (i__ - 1) * 3 + 1;
	mxv_(xform, &vrtces[(i__2 = k - 1) < 30000 && 0 <= i__2 ? i__2 : 
		s_rnge("vrtces", i__2, "f_dskw02__", (ftnlen)507)], &vrtcs3[(
		i__3 = k - 1) < 30000 && 0 <= i__3 ? i__3 : s_rnge("vrtcs3", 
		i__3, "f_dskw02__", (ftnlen)507)]);
    }

/*     Create a spatial index for this data set. */


/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv, vrtcs3, &np, plates, &finscl, &corscl, &c_b17, &vpsize, &
		vlsize, &c_true, &spxisz, work, spaxd3, spaxi3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv, vrtcs3, &np, plates, &c__1, corpar, &mn33, &mx33);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dskopn_("dskw02_test1.bds", "dskw02_test1.bds", &c__0, &han1, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han1, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mn33, &mx33, &first, &
		last, &nv, vrtcs3, &np, plates, spaxd3, spaxi3, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (*ok) {
	dskcls_(&han1, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* *********************************************************************** */


/*     The following tests examine the files that were created. */


/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Check DSK segment's vertex and plate counts.", (ftnlen)44);
    dasopr_("dskw02_test0.bds", &handle, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xnv = nv;
    xnp = np;
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, &nv);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NV", &nv, "=", &xnv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    dski02_(&handle, dladsc, &c__2, &c__1, &c__1, &n, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NP", &np, "=", &xnp, &c__0, ok, (ftnlen)2, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel grid extents.", (ftnlen)25);
    dski02_(&handle, dladsc, &c__4, &c__1, &c__3, &n, vgrext);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the voxel grid extent sub-array of the */
/*     integer spatial index component. */

    chckai_("VGREXT", vgrext, "=", spaixi, &c__3, ok, (ftnlen)6, (ftnlen)1);

/*     We'll use the total voxel count later. */

    nvxtot = vgrext[0] * vgrext[1] * vgrext[2];

/* --- Case -------------------------------------------------------- */

    tcase_("Check coarse voxel grid scale.", (ftnlen)30);
    dski02_(&handle, dladsc, &c__5, &c__1, &c__1, &n, &corscl);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the coarse voxel scale sub-array of the */
/*     integer spatial index component. */

    chcksi_("CORSCL", &corscl, "=", &spaixi[3], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel pointer count.", (ftnlen)26);
    dski02_(&handle, dladsc, &c__6, &c__1, &c__1, &n, &vpsize);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VOXNPT", &vpsize, "=", &spaixi[4], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel-plate correspondence list size.", (ftnlen)43);
    dski02_(&handle, dladsc, &c__7, &c__1, &c__1, &n, &vlsize);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VOXNPL", &vlsize, "=", &spaixi[5], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex-plate correspondence list size.", (ftnlen)44);
    dski02_(&handle, dladsc, &c__8, &c__1, &c__1, &n, &vtxlsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    chcksi_("VTXNPL", &vtxlsz, "=", &spaixi[6], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check coarse grid", (ftnlen)17);
    dski02_(&handle, dladsc, &c__14, &c__1, &c_b149, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the coarse grid size first. */

/* Computing 3rd power */
    i__1 = corscl;
    xncgr = nvxtot / (i__1 * (i__1 * i__1));
    chcksi_("NCGR", &n, "=", &xncgr, &c__0, ok, (ftnlen)4, (ftnlen)1);
    if (*ok) {
	chckai_("CGRPTR", ibuff, "=", &spaixi[7], &n, ok, (ftnlen)6, (ftnlen)
		1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check plates.", (ftnlen)13);
    i__1 = np * 3;
    dski02_(&handle, dladsc, &c__9, &c__1, &i__1, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = np * 3;
    chckai_("PLATES", ibuff, "=", plates, &i__1, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel-plate pointer array.", (ftnlen)32);
    dski02_(&handle, dladsc, &c__10, &c__1, &vpsize, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = 100008;
    chckai_("VOXPTR", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskw02__", (ftnlen)734)
	    ], &vpsize, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel-plate correspondence list.", (ftnlen)38);
    dski02_(&handle, dladsc, &c__11, &c__1, &vlsize, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008;
    chckai_("VOXLST", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskw02__", (ftnlen)751)
	    ], &vlsize, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex-plate pointer array.", (ftnlen)33);
    dski02_(&handle, dladsc, &c__12, &c__1, &nv, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008 + vlsize;
    chckai_("VRTPTR", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskw02__", (ftnlen)769)
	    ], &nv, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex-plate correspondence list.", (ftnlen)39);
    dski02_(&handle, dladsc, &c__13, &c__1, &vtxlsz, &n, ibuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare against the corresponding sub-array of the */
/*     integer spatial index component. */

    addr__ = vpsize + 100008 + vlsize + nv;
    chckai_("VRTLST", ibuff, "=", &spaixi[(i__1 = addr__ - 1) < 1000000 && 0 
	    <= i__1 ? i__1 : s_rnge("spaixi", i__1, "f_dskw02__", (ftnlen)786)
	    ], &vtxlsz, ok, (ftnlen)6, (ftnlen)1);

/*     The following cases exercise the logic for use of saved values. */


/* --- Case -------------------------------------------------------- */

    tcase_("Read from second segment.", (ftnlen)25);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dski02_(&handle, nxtdsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV2", ibuff, "=", &nv2, &c__0, ok, (ftnlen)3, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from first segment of first file again.", (ftnlen)44);

/*     This call resets the previous DLA descriptor to the first one of */
/*     the first file. This sets up the next test, which shows that */
/*     DSKI02 can detect a segment change when the DLA segment */
/*     descriptor start addresses match those of the previous segment, */
/*     but the handle changes. */

    if (*ok) {
	dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV", ibuff, "=", &nv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from first segment of second file.", (ftnlen)39);
    dasopr_("dskw02_test1.bds", &han1, (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&han1, dlads2, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {

/*        Check voxel-plate correspondence list. This is the call */
/*        to DSKI02 where the input handle changes. We use IBUFSZ */
/*        as the "room" argument so we don't need to look up the */
/*        list size. We want to look up the voxel-plate list at */
/*        this point because this is an integer structure that */
/*        differs depending on which file we're reading. */

	dski02_(&han1, dlads2, &c__11, &c__1, &c_b149, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We need to look up the size of the voxel-plate pointer */
/*        array in order to get the correct index of the voxel-plate */
/*        list in the spatial index. */

	dski02_(&han1, dlads2, &c__6, &c__1, &c__1, &n, &vpsiz3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the voxel-plate list size as well. */

	dski02_(&han1, dlads2, &c__7, &c__1, &c__1, &n, &vlsiz3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Compare against the corresponding sub-array of the */
/*        integer spatial index component. */

	addr__ = vpsiz3 + 100008;
	chckai_("VOXLST", ibuff, "=", &spaxi3[(i__1 = addr__ - 1) < 1000000 &&
		 0 <= i__1 ? i__1 : s_rnge("spaxi3", i__1, "f_dskw02__", (
		ftnlen)881)], &vlsiz3, ok, (ftnlen)6, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from first segment of first file again.", (ftnlen)44);
    if (*ok) {
	dski02_(&handle, dladsc, &c__1, &c__1, &c__1, &n, ibuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("NV", ibuff, "=", &nv, &c__0, ok, (ftnlen)2, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check DSK descriptor", (ftnlen)20);
    dskd02_(&handle, dladsc, &c__15, &c__1, &c__24, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check descriptor elements. */

    i__1 = i_dnnt(&dbuff[1]);
    chcksi_("BODYID", &i__1, "=", &bodyid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(dbuff);
    chcksi_("SURFID", &i__1, "=", &surfid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[4]);
    chcksi_("FRAMID", &i__1, "=", &framid, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[2]);
    chcksi_("DCLASS", &i__1, "=", &dclass, &c__0, ok, (ftnlen)6, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[3]);
    chcksi_("DTYPE", &i__1, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = i_dnnt(&dbuff[5]);
    chcksi_("CORSYS", &i__1, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MNCOR1", &dbuff[16], "=", &mncor1, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR1", &dbuff[17], "=", &mxcor1, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR2", &dbuff[18], "=", &mncor2, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR2", &dbuff[19], "=", &mxcor2, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MNCOR3", &dbuff[20], "=", &mncor3, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("MXCOR3", &dbuff[21], "=", &mxcor3, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("CORPAR", &dbuff[6], "=", corpar, &c__10, &c_b286, ok, (ftnlen)6, 
	    (ftnlen)1);
    chcksd_("FIRST", &dbuff[22], "=", &first, &c_b286, ok, (ftnlen)5, (ftnlen)
	    1);
    chcksd_("LAST", &dbuff[23], "=", &last, &c_b286, ok, (ftnlen)4, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Check vertex bounds", (ftnlen)19);
    dskd02_(&handle, dladsc, &c__16, &c__1, &c__6, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("VTXBDS", dbuff, "=", spaixd, &c__6, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel origin", (ftnlen)18);
    dskd02_(&handle, dladsc, &c__17, &c__1, &c__3, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("VOXORI", dbuff, "=", &spaixd[6], &c__3, &c_b286, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel size", (ftnlen)16);
    dskd02_(&handle, dladsc, &c__18, &c__1, &c__1, &n, dbuff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("VOXSIZ", dbuff, "=", &spaixd[9], &c_b286, ok, (ftnlen)6, (ftnlen)
	    1);

/*     The following cases exercise the logic for use of saved values. */


/* --- Case -------------------------------------------------------- */

    tcase_("Read from second segment.", (ftnlen)25);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dskd02_(&handle, nxtdsc, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaxd2[9], &c_b286, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("DSKD02: read from first segment of second file.", (ftnlen)47);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (*ok) {
	dskd02_(&han1, dlads2, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaxd3[9], &c_b286, ok, (ftnlen)6, (
		ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Read from first segment of first file again.", (ftnlen)44);
    if (*ok) {
	dskd02_(&handle, dladsc, &c__18, &c__1, &c__1, &n, dbuff);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("VOXSIZ", dbuff, "=", &spaixd[9], &c_b286, ok, (ftnlen)6, (
		ftnlen)1);
    }
/* *********************************************************************** */

/*     DSKW02 non-error, exceptional cases */

/* *********************************************************************** */
/* *********************************************************************** */


/*     Cases relating to longitude/latitude bounds are performed for */
/*     both latitudinal and planetodetic coordinate systems. */


/* *********************************************************************** */
    for (corix = 1; corix <= 2; ++corix) {
	if (corix == 1) {
	    corsys = 1;
	    cleard_(&c__3, corpar);
	} else {
	    corsys = 4;
	    corpar[0] = a;
	    corpar[1] = (a - c__) / a;
	}


/* --- Case -------------------------------------------------------- */

	tcase_("Create a segment with longitude bounds slightly beyond -pi:p"
		"i.", (ftnlen)62);

/*        Prepare to write a normal one-segment file. */

	if (exists_("dskw02_test3.bds", (ftnlen)16)) {
	    delfil_("dskw02_test3.bds", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Create vertices and plates. */

/*        The Mars radii used here need not be consistent with */
/*        the current generic PCK. */

	a = 3396.19;
	b = a;
	c__ = 3376.2;
	nlon = 20;
	nlat = 10;
	zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, 
		vrtces, &np, plates);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create a spatial index for the plate set. */

/*        Use a heuristic formula for the fine scale. */

/* Computing MAX */
	d__3 = (doublereal) np;
	d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
	finscl = max(d__1,d__2);

/*        Pick a one-size-fits-all value for the coarse scale. */

	corscl = 10;

/*        Set the spatial index integer component size. */

	vpsize = 100000;
	vlsize = 200000;
	spxisz = 1000000;
	if (*ok) {

/*           Create a spatial index that includes a vertex-plate mapping. */

	    dskmi2_(&nv, vrtces, &np, plates, &finscl, &corscl, &c_b17, &
		    vpsize, &vlsize, &c_true, &spxisz, work, spaixd, spaixi);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Generate bounds for the 3rd coordinate. */

	if (*ok) {
	    dskrb2_(&nv, vrtces, &np, plates, &c__1, corpar, &mncor3, &mxcor3)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Set segment attribute inputs. */

	delta = 1e-13;
	mncor1 = -pi_() - delta;
	mxcor1 = pi_() + delta;
	mncor2 = 0.;
	mxcor2 = halfpi_();
	first = -jyear_() * 100;
	last = jyear_() * 100;
	cleard_(&c__10, corpar);
	dclass = 2;
	bodyid = 499;
	surfid = 1;
	s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
	namfrm_(frame, &framid, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Write the file. */

	dskopn_("dskw02_test3.bds", "dskw02_test3.bds", &c__0, &han3, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    dskw02_(&han3, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Close the file. */

	dskcls_(&han3, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Examine the longitude bounds in the DSK descriptor of the */
/*        file's first and only segment. */

	dasopr_("dskw02_test3.bds", &han3, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlabfs_(&han3, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS found", &found, &c_true, ok, (ftnlen)12);
	dskgd_(&han3, dladsc, dskdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	minlon = dskdsc[16];
	maxlon = dskdsc[17];

/*        The longitude bounds of the segment should have been trimmed */
/*        so the total extent is 2*pi. */

	tol = 1e-14;
	chcksd_("MINLON", &minlon, "~", &mncor1, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        We expect the upper bound to be shifted left to make */
/*        the total extent 2*pi. */

	x = mxcor1 - delta * 2;
	chcksd_("MAXLON", &maxlon, "~", &x, &tol, ok, (ftnlen)6, (ftnlen)1);

/*        Close the file. */

	dascls_(&han3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	tcase_("Create a segment with the lower longitude bound slightly bey"
		"ond -2*pi.", (ftnlen)70);
	delta = 1e-13;
	mncor1 = pi_() * -2 - delta;
	mxcor1 = 0.;

/*        Prepare to write to a new file. */


/*        Prepare to write a normal one-segment file. */

	if (exists_("dskw02_test3.bds", (ftnlen)16)) {
	    delfil_("dskw02_test3.bds", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	dskopn_("dskw02_test3.bds", "dskw02_test3.bds", &c__0, &han3, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {

/*           Write the segment. */

	    dskw02_(&han3, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Close the file. */

	dskcls_(&han3, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Examine the longitude bounds in the DSK descriptor of the */
/*        file's first segment. */

	dasopr_("dskw02_test3.bds", &han3, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlabfs_(&han3, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS found", &found, &c_true, ok, (ftnlen)12);
	dskgd_(&han3, dladsc, dskdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	minlon = dskdsc[16];
	maxlon = dskdsc[17];

/*        The longitude bounds of the segment should have been trimmed */
/*        so the total extent is 2*pi. */

	tol = 1e-14;

/*        We expect the upper bound to be shifted right to move it into */
/*        the valid range [-2*pi, 2*pi]. */

	d__1 = pi_() * -2;
	chcksd_("MINLON", &minlon, "~", &d__1, &tol, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksd_("MAXLON", &maxlon, "~", &mxcor1, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        Close the file. */

	dascls_(&han3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	tcase_("Create a segment with the upper longitude bound slightly bey"
		"ond 2*pi.", (ftnlen)69);
	delta = 1e-13;
	mncor1 = 0.;
	mxcor1 = pi_() * 2 + delta;

/*        Prepare to write to an existing file. */

	dasopw_("dskw02_test3.bds", &han3, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {

/*           Write the segment. */

	    dskw02_(&han3, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Close the file. */

	dskcls_(&han3, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Examine the longitude bounds in the DSK descriptor of the */
/*        file's second segment. */

	dasopr_("dskw02_test3.bds", &han3, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlabfs_(&han3, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS found", &found, &c_true, ok, (ftnlen)12);
	dlafns_(&han3, dladsc, nxtdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLAFNS found", &found, &c_true, ok, (ftnlen)12);
	movei_(nxtdsc, &c__8, dladsc);
	dskgd_(&han3, dladsc, dskdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	minlon = dskdsc[16];
	maxlon = dskdsc[17];

/*        The longitude bounds of the segment should have been trimmed */
/*        so the total extent is 2*pi. */

	tol = 1e-14;

/*        We expect the upper bound to be shifted left to move it into */
/*        the valid range [-2*pi, 2*pi]. */

	chcksd_("MINLON", &minlon, "~", &mncor1, &tol, ok, (ftnlen)6, (ftnlen)
		1);
	d__1 = pi_() * 2;
	chcksd_("MAXLON", &maxlon, "~", &d__1, &tol, ok, (ftnlen)6, (ftnlen)1)
		;

/*        Close the file. */

	dascls_(&han3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }



/*     This is the end of the latitudinal/planetodetic non-error, */
/*     exceptional case loop. */



/* *********************************************************************** */

/*     DSKW02 error cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: frame cannot be mapped to ID code.", (ftnlen)48);

/*     Prepare to write a normal one-segment file. */

    if (exists_("dskw02_test2.bds", (ftnlen)16)) {
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create vertices and plates. */

/*     The Mars radii used here need not be consistent with */
/*     the current generic PCK. */

    a = 3396.19;
    b = a;
    c__ = 3376.2;
    nlon = 20;
    nlat = 10;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, vrtces, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a spatial index for the plate set. */

/*     Use a heuristic formula for the fine scale. */

/* Computing MAX */
    d__3 = (doublereal) np;
    d__1 = 1., d__2 = pow_dd(&d__3, &c_b16) / 8;
    finscl = max(d__1,d__2);

/*     Pick a one-size-fits-all value for the coarse scale. */

    corscl = 10;

/*     Set the spatial index integer component size. */

    vpsize = 100000;
    vlsize = 200000;
    spxisz = 1000000;
    if (*ok) {

/*        Create a spatial index that includes a vertex-plate mapping. */

	dskmi2_(&nv, vrtces, &np, plates, &finscl, &corscl, &c_b17, &vpsize, &
		vlsize, &c_true, &spxisz, work, spaixd, spaixi);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Generate bounds for the 3rd coordinate. */

    if (*ok) {
	dskrb2_(&nv, vrtces, &np, plates, &c__1, corpar, &mncor3, &mxcor3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Set segment attribute inputs. */

    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    first = -jyear_() * 100;
    last = jyear_() * 100;
    cleard_(&c__10, corpar);
    dclass = 2;
    bodyid = 499;
    surfid = 1;
    s_copy(frame, "IAU_XXX", (ftnlen)32, (ftnlen)7);
    namfrm_(frame, &framid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write the file. */

    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(FRAMEIDNOTFOUND)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the frame name. */

    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: time bounds are out of order.", (ftnlen)43);
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &last, &
		first, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(TIMESOUTOFORDER)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */


/*     Cases relating to longitude/latitude bounds are performed for */
/*     both latitudinal and planetodetic coordinate systems. */


/* *********************************************************************** */
    for (corix = 1; corix <= 2; ++corix) {
	if (corix == 1) {
	    corsys = 1;
	    cleard_(&c__3, corpar);
	} else {
	    corsys = 4;
	    corpar[0] = a;
	    corpar[1] = (a - c__) / a;
	}

/* --- Case -------------------------------------------------------- */

	tcase_("DSKW02 error: longitude bounds out of bounds.", (ftnlen)45);
	mncor1 = -twopi_() - 2e-12;
	dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	}
	mncor1 = 0.;
	mxcor1 = twopi_() + 2e-12;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	dskcls_(&han2, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Restore longitude bounds. */

	mncor1 = 0.;
	mxcor1 = twopi_();

/* --- Case -------------------------------------------------------- */

	tcase_("DSKW02 error: maximum longitude bound exceeds minimum longit"
		"ude by an excessive amount.", (ftnlen)87);
	mncor1 = -pi_();
	mxcor1 = pi_() + 2e-12;
	dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(INVALIDLONEXTENT)", ok, (ftnlen)23);
	}
	dskcls_(&han2, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	tcase_("DSKW02 error: minimum longitude bound exceeds maximum longit"
		"ude by an excessive amount.", (ftnlen)87);

/*        This is the case where the maximum is less than the */
/*        minimum by enough so that adding 2*pi to the maximum */
/*        doesn't make it greater than the minimum. */

	mncor1 = pi_();
	mxcor1 = -pi_() - 2e-12;
	dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(INVALIDLONEXTENT)", ok, (ftnlen)23);
	}
	dskcls_(&han2, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Restore longitude bounds. */

	mncor1 = 0.;
	mxcor1 = twopi_();

/* --- Case -------------------------------------------------------- */

	tcase_("DSKW02 error: latitude bounds out of range.", (ftnlen)43);
	dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {

/*           Lower bound is too low. */

	    mncor2 = -halfpi_() - 2e-12;
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*           Lower bound is too high. */

	    mncor2 = halfpi_() - 4.9999999999999999e-13;
	    mxcor2 = halfpi_();
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*           Upper bound is too high. */

	    mncor2 = -halfpi_();
	    mxcor2 = halfpi_() + 2e-12;
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*           Upper bound is too low. */

	    mncor2 = -halfpi_();
	    mxcor2 = -halfpi_() + 4.9999999999999999e-13;
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	}
	dskcls_(&han2, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	tcase_("DSKW02 error: latitude bounds out of order.", (ftnlen)43);
	mncor2 = pi_() / 4;
	mxcor2 = -pi_() / 4;
	dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)
		16, (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, 
		    &mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
	}
	dskcls_(&han2, &c_true);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_("dskw02_test2.bds", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	mncor2 = -pi_() / 2;
	mxcor2 = pi_() / 2;
    }



/*     This is the end of the latitudinal/planetodetic error case loop. */




/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: radius bounds out of range.", (ftnlen)41);
    corsys = 1;
    mncor3 = -1e-16;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    mncor3 = 0.;
    x = mxcor3;
    mxcor3 = 0.;
    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, &mncor1,
	     &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    mxcor3 = -1e-16;
    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, &mncor1,
	     &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore MXCOR3. */

    mxcor3 = x;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: longitude bounds are equal.", (ftnlen)41);
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mncor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: radius bounds out of order.", (ftnlen)41);
    mncor3 = 1e4;
    mxcor3 = 1e3;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: radius bounds are equal.", (ftnlen)38);
    mncor3 = 1e4;
    mxcor3 = mncor3;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: altitude bounds out of order.", (ftnlen)43);
    re = 1e4;
    rp = 5e3;
    f = .5;
    corpar[0] = re;
    corpar[1] = f;
    mncor3 = 10.;
    mxcor3 = -10.;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: altitude bounds are equal.", (ftnlen)40);
    re = 1e4;
    rp = 5e3;
    f = .5;
    corpar[0] = re;
    corpar[1] = f;
    mncor3 = 10.;
    mxcor3 = mncor3;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: lower altitude bound less than or equal to min{ -a"
	    "**2/b, -b**2/a }. Oblate case.", (ftnlen)94);
    re = 1e4;
    rp = 5e3;
    f = .5;
    corpar[0] = re;
    corpar[1] = f;
/* Computing MIN */
/* Computing 2nd power */
    d__3 = re;
/* Computing 2nd power */
    d__4 = rp;
    d__1 = -(d__3 * d__3) / rp, d__2 = -(d__4 * d__4) / re;
    altlim = min(d__1,d__2);
    mncor3 = altlim - 1e-6;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(DEGENERATESURFACE)", ok, (ftnlen)24);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: lower altitude bound less than or equal to min{ -a"
	    "**2/b, -b**2/a }. Prolate case.", (ftnlen)95);
    re = 1e4;
    rp = 1.5e4;
    f = -.5;
    corpar[0] = re;
    corpar[1] = f;
/* Computing MIN */
/* Computing 2nd power */
    d__3 = re;
/* Computing 2nd power */
    d__4 = rp;
    d__1 = -(d__3 * d__3) / rp, d__2 = -(d__4 * d__4) / re;
    altlim = min(d__1,d__2);
    mncor3 = altlim - 1e-6;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(DEGENERATESURFACE)", ok, (ftnlen)24);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: invalid equatorial radius.", (ftnlen)40);
    re = -1e4;
    f = .5;
    corpar[0] = re;
    corpar[1] = f;
    mncor3 = -10.;
    mxcor3 = 10.;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    re = 0.;
    corpar[0] = re;
    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &mncor1, &
	    mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: invalid flattening coefficient.", (ftnlen)45);
    re = 1e4;
    f = 1.5;
    corpar[0] = re;
    corpar[1] = f;
    mncor3 = -10.;
    mxcor3 = 10.;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__4, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mncor3 = 0.;
    mxcor3 = 1e4;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bounds are equal. Rectangular coordinate case.", (
	    ftnlen)60);
    mncor1 = -10.;
    mxcor1 = 10.;
    mncor2 = -20.;
    mxcor2 = 20.;
    mncor3 = -30.;
    mxcor3 = 30.;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Set X bounds equal. */

	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__3, corpar, &
		mncor1, &mncor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    }

/*     Set Y bounds equal. */

    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__3, corpar, &mncor1, &
	    mxcor1, &mxcor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);

/*     Set Z bounds equal. */

    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__3, corpar, &mncor1, &
	    mxcor1, &mncor2, &mxcor2, &mncor3, &mncor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad coordinate system.", (ftnlen)36);
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    corsys = -1;
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &corsys, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad vertex index within plate.", (ftnlen)44);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the index range of the vertices within the integer */
/*     spatial index. */

    i__ = np / 2;
    j = (i__ - 1) * 3 + 2;
    k = plates[(i__1 = j - 1) < 60000 && 0 <= i__1 ? i__1 : s_rnge("plates", 
	    i__1, "f_dskw02__", (ftnlen)2429)];
    plates[(i__1 = j - 1) < 60000 && 0 <= i__1 ? i__1 : s_rnge("plates", i__1,
	     "f_dskw02__", (ftnlen)2431)] = 0;
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(BADVERTEXINDEX)", ok, (ftnlen)21);
    }

/*     Restore the plate. */

    plates[(i__1 = j - 1) < 60000 && 0 <= i__1 ? i__1 : s_rnge("plates", i__1,
	     "f_dskw02__", (ftnlen)2449)] = k;
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad vertex count.", (ftnlen)31);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &c__0, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &c_n1, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &c_b840, vrtces, &np, plates, spaixd, spaixi, (ftnlen)
		32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad plate count", (ftnlen)29);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &c__0, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &c_n1, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &c_b864, plates, spaixd, spaixi, (ftnlen)
		32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad voxel grid extents.", (ftnlen)37);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the extents in the integer spatial index. */

    if (*ok) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    w = spaixi[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : 
		    s_rnge("spaixi", i__1, "f_dskw02__", (ftnlen)2601)];
	    spaixi[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge(
		    "spaixi", i__1, "f_dskw02__", (ftnlen)2603)] = 0;
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	    spaixi[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge(
		    "spaixi", i__1, "f_dskw02__", (ftnlen)2617)] = 100000001;
	    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &
		    first, &last, &nv, vrtces, &np, plates, spaixd, spaixi, (
		    ftnlen)32);
	    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*           Restore the extent. */

	    spaixi[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge(
		    "spaixi", i__1, "f_dskw02__", (ftnlen)2634)] = w;
	}
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: fine voxel count too large.", (ftnlen)41);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the extents in the integer spatial index. */

    if (*ok) {
	movei_(spaixi, &c__3, ibuff);
	spaixi[0] = 100000;
	spaixi[1] = 1000000;
	spaixi[2] = 1;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the voxel grid extents. */

    movei_(ibuff, &c__3, spaixi);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: coarse voxel scale out of range.", (ftnlen)46);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the extents in the integer spatial index. */

    if (*ok) {

/*        Save the coarse scale and the grid extents. */

	w = spaixi[3];
	movei_(spaixi, &c__3, ibuff);
	spaixi[3] = 2;
	spaixi[0] = 100;
	spaixi[1] = 100;
	spaixi[2] = 100;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	spaixi[3] = 0;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	spaixi[3] = -1;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*        Make the scale greater than the cube root of the */
/*        total fine voxel count. */

	spaixi[3] = 101;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the voxel grid extents and the coarse scale. */

    movei_(ibuff, &c__3, spaixi);
    spaixi[3] = w;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: cube of coarse voxel scale does not evenly divide "
	    "fine voxel count.", (ftnlen)81);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the extents in the integer spatial index. */

    if (*ok) {

/*        Save the coarse scale and the grid extents. */

	w = spaixi[3];
	movei_(spaixi, &c__3, ibuff);
	spaixi[3] = 3;
	spaixi[0] = 10;
	spaixi[1] = 10;
	spaixi[2] = 10;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(INCOMPATIBLESCALE)", ok, (ftnlen)24);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the voxel grid extents and the coarse scale. */

    movei_(ibuff, &c__3, spaixi);
    spaixi[3] = w;
/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: coarse voxel count too large.", (ftnlen)43);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Locate the extents in the integer spatial index. */

    if (*ok) {

/*        Save the coarse scale and the grid extents. */

	w = spaixi[3];
	movei_(spaixi, &c__3, ibuff);
	spaixi[3] = 1;
	spaixi[0] = 100;
	spaixi[1] = 100;
	spaixi[2] = 100;
	dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &
		mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
		last, &nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the voxel grid extents and the coarse scale. */

    movei_(ibuff, &c__3, spaixi);
    spaixi[3] = w;

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: bad data class.", (ftnlen)29);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskopn_("dskw02_test2.bds", "dskw02_test2.bds", &c__0, &han2, (ftnlen)16, 
	    (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	dskw02_(&han2, &bodyid, &surfid, &c_n1, frame, &c__1, corpar, &mncor1,
		 &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &
		nv, vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
	chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    }
    dskcls_(&han2, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test2.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("DSKW02 error: write to closed file.", (ftnlen)35);
    mncor1 = 0.;
    mxcor1 = twopi_();
    mncor2 = -halfpi_();
    mxcor2 = halfpi_();
    mncor3 = 0.;
    mxcor3 = 1e4;
    dskw02_(&han2, &bodyid, &surfid, &dclass, frame, &c__1, corpar, &mncor1, &
	    mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &last, &nv, 
	    vrtces, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_true, "SPICE(DASNOSUCHHANDLE)", ok, (ftnlen)22);


/*     Clean up. */


/* --- Case -------------------------------------------------------- */

    tcase_("Unload and delete DSK files.", (ftnlen)28);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test0.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test1.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&han3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("dskw02_test3.bds", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dskw02__ */

