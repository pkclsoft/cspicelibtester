/* f_dskx02.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2100 = 2100;
static integer c_n1 = -1;
static integer c__0 = 0;
static integer c__3 = 3;
static integer c_b57 = 100000;
static integer c__6 = 6;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static doublereal c_b91 = 1e-14;
static integer c__4 = 4;
static integer c__5 = 5;
static integer c__8 = 8;

/* $Procedure F_DSKX02 ( DSKX02 tests ) */
/* Subroutine */ int f_dskx02__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_stop(char *
	    , ftnlen);

    /* Local variables */
    static integer plid;
    static doublereal xoff, last, zoff;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static integer nvox;
    static doublereal xxpt[3];
    static integer i__, j, k;
    static char frame[32];
    extern /* Subroutine */ int tcase_(char *, ftnlen), filli_(integer *, 
	    integer *, integer *);
    extern doublereal dpmin_(void), dpmax_(void);
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int dskw02_(integer *, integer *, integer *, 
	    integer *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *,
	     integer *, integer *, doublereal *, integer *, ftnlen), dskx02_(
	    integer *, integer *, doublereal *, doublereal *, integer *, 
	    doublereal *, logical *), movei_(integer *, integer *, integer *);
    static integer xplid;
    static char title[320];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static doublereal first;
    static integer ncvox;
    static doublereal verts[9000]	/* was [3][3000] */;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal mncor1, mncor2, mncor3, mxcor1, mxcor2, mxcor3;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *);
    static integer np, vi;
    extern /* Subroutine */ int cleari_(integer *, integer *), delfil_(char *,
	     ftnlen);
    static integer to, nv;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    static integer dclass;
    extern logical exists_(char *, ftnlen);
    static doublereal corpar[10];
    static char dsk[255];
    static doublereal origin[3], raydir[3], spaixd[10], vertex[3], voxsiz, 
	    vtxbds[6]	/* was [2][3] */;
    static integer center, cgrptr[100000], cgrscl, corsys, plates[6000]	/* 
	    was [3][2000] */, prvdsc[8], spaixi[120000], surfce, vgrext[3], 
	    voxnpt, voxnpl, voxptr[2100], voxplt[2100];
    extern /* Subroutine */ int dskopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), dskcls_(integer *, logical *), dasopr_(char *, 
	    integer *, ftnlen);
    static doublereal xpt[3];
    extern /* Subroutine */ int dascls_(integer *), dasopw_(char *, integer *,
	     ftnlen), cleard_(integer *, doublereal *), dlafns_(integer *, 
	    integer *, integer *, logical *);

/* $ Abstract */

/*     Exercise the DSK type 2 ray-surface intercept routine DSKX02. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKLIB type 2 ray-surface intercept */
/*     routine DSKX02. */

/*        Currently tests are limited to a subset of the */
/*        routine's logic dealing with competing intercept */
/*        solutions. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 09-SEP-2014 (NJB) */

/*        Updated to work with SPICELIB version of DSKW02. */

/*     09-SEP-2014 (NJB) */

/*        Corrected pointer entries in spatial index */
/*        for voxels 3 and 4. Corrected null pointer values */
/*        in coarse voxel grid. */

/* -    20-JUN-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_DSKX02", (ftnlen)8);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create DSK containing type 2 segment", (ftnlen)36);

/*     We're going to create a plate model and associated spatial */
/*     index data structures entirely by hand. */

/*     The voxel grid has voxel extents */

/*        4 x 4 x 4 */

/*     The coarse voxel scale is 2. */

/*     The edge length of each voxel is 10 km. */

    for (i__ = 1; i__ <= 3; ++i__) {
	vgrext[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vgrext", 
		i__1, "f_dskx02__", (ftnlen)261)] = 4;
    }
    cgrscl = 2;
    voxsiz = 10.;
    ncvox = 8;

/*     The voxel grid is centered at the reference frame center. */

    for (i__ = 1; i__ <= 3; ++i__) {
	origin[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("origin", 
		i__1, "f_dskx02__", (ftnlen)273)] = -vgrext[(i__2 = i__ - 1) <
		 3 && 0 <= i__2 ? i__2 : s_rnge("vgrext", i__2, "f_dskx02__", 
		(ftnlen)273)] * voxsiz / 2;
    }

/*     Set up plates and vertices: */

/*        Voxel (1,1,1) contains a plate facing in the -X direction: */

    verts[0] = -19.;
    verts[1] = -18.;
    verts[2] = -19.;
    verts[3] = -19.;
    verts[4] = -19.;
    verts[5] = -19.;
    verts[6] = -19.;
    verts[7] = -18.5;
    verts[8] = -18.;
    plates[0] = 1;
    plates[1] = 2;
    plates[2] = 3;

/*     Add a plate that should be occulted by plate 1, as seen from */
/*     the -X direction. */

/*     Plate 2 is a copy of plate 1, shifted by 1 in the +X direction. */

/*     Vertices 4-6 define this plate. */

    for (i__ = 1; i__ <= 3; ++i__) {
	verts[(i__1 = (i__ + 3) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)307)] = verts[(i__2 = 
		i__ * 3 - 3) < 9000 && 0 <= i__2 ? i__2 : s_rnge("verts", 
		i__2, "f_dskx02__", (ftnlen)307)] + 1.;
	for (j = 2; j <= 3; ++j) {
	    verts[(i__1 = j + (i__ + 3) * 3 - 4) < 9000 && 0 <= i__1 ? i__1 : 
		    s_rnge("verts", i__1, "f_dskx02__", (ftnlen)310)] = verts[
		    (i__2 = j + i__ * 3 - 4) < 9000 && 0 <= i__2 ? i__2 : 
		    s_rnge("verts", i__2, "f_dskx02__", (ftnlen)310)];
	}
    }
    plates[3] = 4;
    plates[4] = 5;
    plates[5] = 6;

/*     Add a plate that normally should occult plate 1, as seen from */
/*     the -X direction. However, this plate will be "owned" by voxel 2, */
/*     which is voxel 1's neighbor on the +X side. Thus this plate should */
/*     be examined as a target only if plates 1 and 2 are missed. */


/*     Plate 3 is a copy of plate 1, shifted by 1/2 in the -X direction. */

/*     Vertices 7-9 define this plate. */

    for (i__ = 1; i__ <= 3; ++i__) {
	verts[(i__1 = (i__ + 6) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)332)] = verts[(i__2 = 
		i__ * 3 - 3) < 9000 && 0 <= i__2 ? i__2 : s_rnge("verts", 
		i__2, "f_dskx02__", (ftnlen)332)] - .5;
	for (j = 2; j <= 3; ++j) {
	    verts[(i__1 = j + (i__ + 6) * 3 - 4) < 9000 && 0 <= i__1 ? i__1 : 
		    s_rnge("verts", i__1, "f_dskx02__", (ftnlen)335)] = verts[
		    (i__2 = j + i__ * 3 - 4) < 9000 && 0 <= i__2 ? i__2 : 
		    s_rnge("verts", i__2, "f_dskx02__", (ftnlen)335)];
	}
    }
    plates[6] = 7;
    plates[7] = 8;
    plates[8] = 9;

/*     Add a plate that normally should occult plate 1, as seen from */
/*     the -X direction. However, this plate has its vertex orientation */
/*     reversed. */

/*     Vertices 10-12 define this plate. */

    verts[27] = verts[0] - .75;
    verts[30] = verts[27];
    verts[33] = verts[27];

/*     The Y component of vertex 10 is that of vertex 2; */
/*     the Y component of vertex 11 is that of vertex 1. */

    verts[28] = verts[4];
    verts[31] = verts[1];
    verts[34] = verts[7];
    verts[29] = verts[2];
    verts[32] = verts[5];
    verts[35] = verts[8];
    plates[9] = 10;
    plates[10] = 11;
    plates[11] = 12;

/*     Add a plate on the opposite side of the grid from plate 1, */
/*     but also facing in the same direction as plate one. Offset */
/*     this plate in the +Z direction by 1 km. */

/*     This plate is owned by voxel 4, which is contained in */
/*     coarse voxel 2. */

    for (i__ = 1; i__ <= 3; ++i__) {
	verts[(i__1 = (i__ + 12) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)381)] = verts[0] + 
		voxsiz * 3;
	verts[(i__1 = (i__ + 12) * 3 - 2) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)383)] = verts[(i__2 = 
		i__ * 3 - 2) < 9000 && 0 <= i__2 ? i__2 : s_rnge("verts", 
		i__2, "f_dskx02__", (ftnlen)383)];
	verts[(i__1 = (i__ + 12) * 3 - 1) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)385)] = verts[(i__2 = 
		i__ * 3 - 1) < 9000 && 0 <= i__2 ? i__2 : s_rnge("verts", 
		i__2, "f_dskx02__", (ftnlen)385)] + 1.;
    }
    plates[12] = 13;
    plates[13] = 14;
    plates[14] = 15;
    nv = 15;
    np = 5;

/*     Compute vertex extents. */

    for (i__ = 1; i__ <= 3; ++i__) {
	vtxbds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_dskx02__", (ftnlen)401)] = dpmax_();
	vtxbds[(i__1 = (i__ << 1) - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_dskx02__", (ftnlen)402)] = dpmin_();
    }
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
/* Computing MIN */
	    d__1 = vtxbds[(i__3 = (j << 1) - 2) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_dskx02__", (ftnlen)409)], d__2 =
		     verts[(i__4 = j + i__ * 3 - 4) < 9000 && 0 <= i__4 ? 
		    i__4 : s_rnge("verts", i__4, "f_dskx02__", (ftnlen)409)];
	    vtxbds[(i__2 = (j << 1) - 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_dskx02__", (ftnlen)409)] = min(d__1,
		    d__2);
/* Computing MAX */
	    d__1 = vtxbds[(i__3 = (j << 1) - 1) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_dskx02__", (ftnlen)410)], d__2 =
		     verts[(i__4 = j + i__ * 3 - 4) < 9000 && 0 <= i__4 ? 
		    i__4 : s_rnge("verts", i__4, "f_dskx02__", (ftnlen)410)];
	    vtxbds[(i__2 = (j << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_dskx02__", (ftnlen)410)] = max(d__1,
		    d__2);
	}
    }

/*     Set the voxel-plate mapping. */

    cleari_(&c__2100, voxplt);

/*     Initialize all pointers to null. */

    filli_(&c_n1, &c__2100, voxptr);

/*     The voxel pointer list must contain an entry for */
/*     each voxel belonging to a non-empty coarse voxel. */
/*     Coarse voxels 1 and 2 are non-empty. */

    voxnpt = 16;

/*     The first voxel contains plates 1 and 2. The pointer */
/*     associated with this voxel points to the start of the */
/*     voxel's plate list. */

    voxptr[0] = 1;

/*     The second voxel contains plate 3. The pointer */
/*     associated with this voxel points to the start of the */
/*     voxel's plate list, which follows the list for voxel 1. */

    voxptr[1] = 5;

/*     The fourth voxel contains plate 5. The pointer */
/*     associated with this voxel points to the start of the */
/*     voxel's plate list, which follows the list for voxel 2. */
/*     Note that the voxel pointer list contains 6 null entries */
/*     following the entry for voxel 2, since there are entries */
/*     for every voxel in the coarse voxel containing voxel 1. */

    voxptr[9] = 7;

/*     The first element of a plate list is the count. */
/*     The plate numbers follow. */

/*     Entries for voxel 1: */

    voxplt[0] = 3;
    voxplt[1] = 1;
    voxplt[2] = 2;
    voxplt[3] = 4;

/*     Entries for voxel 2: */

    voxplt[4] = 1;
    voxplt[5] = 3;

/*     Entries for voxel 4: */

    voxplt[6] = 1;
    voxplt[7] = 5;

/*     VOXNPL is the number of elements in the plate list, including */
/*     plate counts. */

    voxnpl = 8;

/*     The first coarse voxel is non-empty and points */
/*     to the voxel pointer for the first voxel. */

/*     The second coarse voxel is non-empty and points */
/*     to the voxel pointer for the fourth voxel. */

/*     The rest of the coarse grid contains null values. */

    filli_(&c__0, &ncvox, cgrptr);
    cgrptr[0] = 1;
    cgrptr[1] = 9;

/*     We will not use the vertex-plate list. */


/*     Pack the integer component of the spatial index array. */

    movei_(vgrext, &c__3, spaixi);
    spaixi[3] = cgrscl;
    spaixi[4] = voxnpt;
    spaixi[5] = voxnpl;
    spaixi[6] = 0;
    movei_(cgrptr, &c_b57, &spaixi[7]);
    i__ = 100008;
    movei_(voxptr, &voxnpt, &spaixi[(i__1 = i__ - 1) < 120000 && 0 <= i__1 ? 
	    i__1 : s_rnge("spaixi", i__1, "f_dskx02__", (ftnlen)516)]);
    i__ += voxnpt;
    movei_(voxplt, &voxnpl, &spaixi[(i__1 = i__ - 1) < 120000 && 0 <= i__1 ? 
	    i__1 : s_rnge("spaixi", i__1, "f_dskx02__", (ftnlen)520)]);

/*     Pack the d.p. component of the spatial index array. */

    moved_(vtxbds, &c__6, spaixd);
    vequ_(origin, &spaixd[6]);
    spaixd[9] = voxsiz;

/*     Set up ID and coverage parameters for the segment. These */
/*     are required in order to create a DSK segment but have no */
/*     other use for the tests performed here. */

    surfce = 499;
    center = 4;
    dclass = 2;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    corsys = 3;
    mncor1 = vtxbds[0];
    mxcor1 = vtxbds[1];
    mncor2 = vtxbds[2];
    mxcor2 = vtxbds[3];
    mncor3 = vtxbds[4];
    mxcor3 = vtxbds[5];
    first = -1.;
    last = 1.;

/*     Open a new DSK file. */

    s_copy(dsk, "dskx02.bds", (ftnlen)255, (ftnlen)10);
    if (exists_(dsk, (ftnlen)255)) {
	delfil_(dsk, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dskopn_(dsk, dsk, &c__0, &handle, (ftnlen)255, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Write the segment. */

    dskw02_(&handle, &center, &surfce, &dclass, frame, &corsys, corpar, &
	    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
	    last, &nv, verts, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskcls_(&handle, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find ray intercept on plate 1; ray vertex is on the -X side.", (
	    ftnlen)60);
    dasopr_(dsk, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray vertex is on the -X side of the grid, and the ray */
/*     points strictly in the +X direction. */

    vertex[0] = voxsiz * -3;
    vertex[1] = (verts[1] + verts[4]) / 2;
    vertex[2] = (verts[2] + verts[8]) / 2;
    raydir[0] = 1.;
    raydir[1] = 0.;
    raydir[2] = 0.;
    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("PLID", &plid, "=", &c__1, &c__0, ok, (ftnlen)4, (ftnlen)1);
	vequ_(vertex, xxpt);
	xxpt[0] = verts[0];
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b91, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Find ray intercept on plate 4; ray vertex is on the +X side.", (
	    ftnlen)60);

/*     The ray vertex is on the +X side of the grid, and the ray */
/*     points strictly in the -X direction. */

/*     Plates other than plate 4 should be invisible due to their */
/*     vertex orientation. */


    vertex[0] = voxsiz * 3;
    vertex[1] = (verts[1] + verts[4]) / 2;
    vertex[2] = (verts[2] + verts[8]) / 2;
    raydir[0] = -1.;
    raydir[1] = 0.;
    raydir[2] = 0.;
    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("PLID", &plid, "=", &c__4, &c__0, ok, (ftnlen)4, (ftnlen)1);
	vequ_(vertex, xxpt);
	xxpt[0] = verts[27];
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b91, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Find ray intercept on plate 5; ray vertex is on the -X side.", (
	    ftnlen)60);

/*     This case requires examination of the second group of */
/*     voxels within the DSKX02 loop that works with one voxel */
/*     group at a time. */

/*     The ray vertex is on the -X side of the grid, and the ray */
/*     points strictly in the +X direction. */

    vertex[0] = voxsiz * -3;
    vertex[1] = (verts[37] + verts[40]) / 2;
    vertex[2] = (verts[38] + verts[44]) / 2;
    raydir[0] = 1.;
    raydir[1] = 0.;
    raydir[2] = 0.;
    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("PLID", &plid, "=", &c__5, &c__0, ok, (ftnlen)4, (ftnlen)1);
	vequ_(vertex, xxpt);
	xxpt[0] = verts[36];
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b91, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Shoot a ray through empty voxels in coarse voxel 1.", (ftnlen)51);

/*     Voxels above the bottom layer are empty. In coarse voxel 1, */
/*     the voxel IDs of the upper layer are */

/*        17, 18, 21, 22 */



    vertex[0] = voxsiz * -2.5;
    vertex[1] = voxsiz * -.5;
    vertex[2] = voxsiz * -.5;
    raydir[0] = 1.;
    raydir[1] = -1.;
    raydir[2] = 0.;
    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Shoot a ray through empty coarse voxels on the top layer of the "
	    "grid.", (ftnlen)69);
    vertex[0] = voxsiz * -3;
    vertex[1] = voxsiz * .5;
    vertex[2] = voxsiz * 1.5;
    raydir[0] = 1.;
    raydir[1] = 0.;
    raydir[2] = 0.;
    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */


/*     Add a segment for testing handling of duplicate plates. */

    tcase_("Add second segment to DSK.", (ftnlen)26);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasopw_(dsk, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The voxel grid has voxel extents */

/*        100 x 1 x 1 */

/*     The coarse voxel scale is 1. */

/*     The edge length of each voxel is 10 km. */

    vgrext[0] = 100;
    vgrext[1] = 1;
    vgrext[2] = 1;
    cgrscl = 1;
    voxsiz = 10.;
    ncvox = 100;

/*     The voxel grid origin coincides with the reference frame center. */

    cleard_(&c__3, origin);

/*     Voxel 1 contains 10 plates. */

/*     Each other voxel "owns" the plates in the voxel behind it in the */
/*     -X direction, plus its own plates. The plates physically */
/*     contained in voxel `n' are shifted in the +Z direction by (n-1) */
/*     km relative to the plate set in voxel 1. */


/*     Create the vertices and plates for the first voxel. */

/*     The vertices are orders to as to make the normal vectors point */
/*     in the -X direction. */

    for (i__ = 1; i__ <= 10; ++i__) {
	j = (i__ - 1) * 3;
	verts[(i__1 = (j + 1) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)807)] = 5.;
	verts[(i__1 = (j + 1) * 3 - 2) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)808)] = (doublereal) i__;
	verts[(i__1 = (j + 1) * 3 - 1) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)809)] = 0.;
	verts[(i__1 = (j + 2) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)811)] = 5.;
	verts[(i__1 = (j + 2) * 3 - 2) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)812)] = (doublereal) (
		i__ - 1);
	verts[(i__1 = (j + 2) * 3 - 1) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)813)] = 0.;
	verts[(i__1 = (j + 3) * 3 - 3) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)815)] = 5.;
	verts[(i__1 = (j + 3) * 3 - 2) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)816)] = i__ - .5;
	verts[(i__1 = (j + 3) * 3 - 1) < 9000 && 0 <= i__1 ? i__1 : s_rnge(
		"verts", i__1, "f_dskx02__", (ftnlen)817)] = 1.;
	for (k = 1; k <= 3; ++k) {
	    plates[(i__1 = k + i__ * 3 - 4) < 6000 && 0 <= i__1 ? i__1 : 
		    s_rnge("plates", i__1, "f_dskx02__", (ftnlen)820)] = j + 
		    k;
	}
    }
    nv = 30;
    np = 10;

/*     Set up the voxel-plate mapping. */

    voxnpt = 1;
    voxptr[0] = 1;
    voxplt[0] = 10;
    for (i__ = 1; i__ <= 10; ++i__) {
	voxplt[(i__1 = i__) < 2100 && 0 <= i__1 ? i__1 : s_rnge("voxplt", 
		i__1, "f_dskx02__", (ftnlen)838)] = i__;
    }
    voxnpl = 11;
    to = voxnpl + 1;

/*     Set the coarse grid pointers. */

    filli_(&c_n1, &ncvox, cgrptr);
    cgrptr[0] = 1;

/*     Create vertices, plates, and spatial index entries for */
/*     the other voxels. */

    nvox = vgrext[0];
    i__1 = nvox;
    for (vi = 2; vi <= i__1; ++vi) {

/*        Create vertices and plates for the current voxel. */

	for (i__ = 1; i__ <= 10; ++i__) {
	    j = (np + i__ - 1) * 3;
	    xoff = (vi - 1) * voxsiz;
	    zoff = (vi - 1) * ((voxsiz - 1) / nvox);
	    for (k = 1; k <= 3; ++k) {
		verts[(i__2 = (j + k) * 3 - 3) < 9000 && 0 <= i__2 ? i__2 : 
			s_rnge("verts", i__2, "f_dskx02__", (ftnlen)871)] = 
			verts[0] + xoff;
		verts[(i__2 = (j + k) * 3 - 2) < 9000 && 0 <= i__2 ? i__2 : 
			s_rnge("verts", i__2, "f_dskx02__", (ftnlen)872)] = 
			verts[(i__3 = (k + (i__ - 1) * 3) * 3 - 2) < 9000 && 
			0 <= i__3 ? i__3 : s_rnge("verts", i__3, "f_dskx02__",
			 (ftnlen)872)];
		verts[(i__2 = (j + k) * 3 - 1) < 9000 && 0 <= i__2 ? i__2 : 
			s_rnge("verts", i__2, "f_dskx02__", (ftnlen)873)] = 
			verts[(i__3 = k * 3 - 1) < 9000 && 0 <= i__3 ? i__3 : 
			s_rnge("verts", i__3, "f_dskx02__", (ftnlen)873)] + 
			zoff;
	    }
	    for (k = 1; k <= 3; ++k) {
		plates[(i__2 = k + (np + i__) * 3 - 4) < 6000 && 0 <= i__2 ? 
			i__2 : s_rnge("plates", i__2, "f_dskx02__", (ftnlen)
			878)] = j + k;
	    }
	}

/*        Set the coarse grid pointer for the current coarse voxel. */

	cgrptr[(i__2 = vi - 1) < 100000 && 0 <= i__2 ? i__2 : s_rnge("cgrptr",
		 i__2, "f_dskx02__", (ftnlen)887)] = vi;

/*        Set the voxel pointer for the current voxel. */

	voxptr[(i__2 = vi - 1) < 2100 && 0 <= i__2 ? i__2 : s_rnge("voxptr", 
		i__2, "f_dskx02__", (ftnlen)892)] = to;

/*        Fill in the plate list for the current voxel. */

	voxplt[(i__2 = to - 1) < 2100 && 0 <= i__2 ? i__2 : s_rnge("voxplt", 
		i__2, "f_dskx02__", (ftnlen)897)] = 20;

/*        Add the new plates to the plate list. */

	for (i__ = 1; i__ <= 10; ++i__) {
	    voxplt[(i__2 = to + i__ - 1) < 2100 && 0 <= i__2 ? i__2 : s_rnge(
		    "voxplt", i__2, "f_dskx02__", (ftnlen)903)] = np + i__;
	}

/*        Add the plates from the preceding voxel to the plate list. */

	for (i__ = 1; i__ <= 10; ++i__) {
	    voxplt[(i__2 = to + 10 + i__ - 1) < 2100 && 0 <= i__2 ? i__2 : 
		    s_rnge("voxplt", i__2, "f_dskx02__", (ftnlen)910)] = np - 
		    10 + i__;
	}
	np += 10;
	nv += 30;
	voxnpl += 21;
	to = voxnpl + 1;
    }
    voxnpt = nvox;

/*     We will not use the vertex-plate list. */


/*     Compute vertex extents. */

    for (i__ = 1; i__ <= 3; ++i__) {
	vtxbds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_dskx02__", (ftnlen)933)] = dpmax_();
	vtxbds[(i__1 = (i__ << 1) - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_dskx02__", (ftnlen)934)] = dpmin_();
    }
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
/* Computing MIN */
	    d__1 = vtxbds[(i__3 = (j << 1) - 2) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_dskx02__", (ftnlen)941)], d__2 =
		     verts[(i__4 = j + i__ * 3 - 4) < 9000 && 0 <= i__4 ? 
		    i__4 : s_rnge("verts", i__4, "f_dskx02__", (ftnlen)941)];
	    vtxbds[(i__2 = (j << 1) - 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_dskx02__", (ftnlen)941)] = min(d__1,
		    d__2);
/* Computing MAX */
	    d__1 = vtxbds[(i__3 = (j << 1) - 1) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_dskx02__", (ftnlen)942)], d__2 =
		     verts[(i__4 = j + i__ * 3 - 4) < 9000 && 0 <= i__4 ? 
		    i__4 : s_rnge("verts", i__4, "f_dskx02__", (ftnlen)942)];
	    vtxbds[(i__2 = (j << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_dskx02__", (ftnlen)942)] = max(d__1,
		    d__2);
	}
    }

/*     Pack the integer component of the spatial index array. */

    movei_(vgrext, &c__3, spaixi);
    spaixi[3] = cgrscl;
    spaixi[4] = voxnpt;
    spaixi[5] = voxnpl;
    spaixi[6] = 0;
    movei_(cgrptr, &c_b57, &spaixi[7]);
    i__ = 100008;
    movei_(voxptr, &voxnpt, &spaixi[(i__1 = i__ - 1) < 120000 && 0 <= i__1 ? 
	    i__1 : s_rnge("spaixi", i__1, "f_dskx02__", (ftnlen)963)]);
    i__ += voxnpt;
    movei_(voxplt, &voxnpl, &spaixi[(i__1 = i__ - 1) < 120000 && 0 <= i__1 ? 
	    i__1 : s_rnge("spaixi", i__1, "f_dskx02__", (ftnlen)967)]);

/*     Pack the d.p. component of the spatial index array. */

    moved_(vtxbds, &c__6, spaixd);
    vequ_(origin, &spaixd[6]);
    spaixd[9] = voxsiz;


/*     Set up ID and coverage parameters for the segment. These */
/*     are required in order to create a DSK segment but have no */
/*     other use for the tests performed here. */

    surfce = 599;
    center = 5;
    dclass = 2;
    s_copy(frame, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    corsys = 3;
    mncor1 = 0.;
    mxcor1 = vgrext[0] * voxsiz;
    mncor2 = 0.;
    mxcor2 = vgrext[1] * voxsiz;
    mncor3 = 0.;
    mxcor3 = vgrext[2] * voxsiz;
    first = -1.;
    last = 1.;

/*     Write the segment. */

    dskw02_(&handle, &center, &surfce, &dclass, frame, &corsys, corpar, &
	    mncor1, &mxcor1, &mncor2, &mxcor2, &mncor3, &mxcor3, &first, &
	    last, &nv, verts, &np, plates, spaixd, spaixi, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskcls_(&handle, &c_true);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find ray-plate intercepts in the second segment.", (ftnlen)48);
    dasopr_(dsk, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    movei_(dladsc, &c__8, prvdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlafns_(&handle, prvdsc, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    i__1 = nvox;
    for (vi = 1; vi <= i__1; ++vi) {
	for (i__ = 1; i__ <= 10; ++i__) {
	    s_copy(title, "Intercept on plate # of voxel #.", (ftnlen)320, (
		    ftnlen)32);
	    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &vi, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);

/* --- Case: ------------------------------------------------------ */

	    tcase_(title, (ftnlen)320);
	    vertex[0] = -1.;
	    vertex[1] = i__ - .5;
	    vertex[2] = (vi - 1) * .09 + .95;
	    raydir[0] = 1.;
	    raydir[1] = 0.;
	    raydir[1] = 0.;
	    dskx02_(&handle, dladsc, vertex, raydir, &plid, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (! (*ok)) {
		s_stop("", (ftnlen)0);
	    }
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    if (found) {
		xplid = (vi - 1) * 10 + i__;
		chcksi_("PLID", &plid, "=", &xplid, &c__0, ok, (ftnlen)4, (
			ftnlen)1);
		vequ_(vertex, xxpt);
		xxpt[0] = verts[0] + (vi - 1) * voxsiz;
		chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b91, ok, (ftnlen)3,
			 (ftnlen)3);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up kernels.", (ftnlen)17);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(dsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_dskx02__ */

