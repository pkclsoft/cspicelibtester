/*

-Procedure f_dskxv_c ( dskxv_c tests )

 
-Abstract
 
   Exercise the CSPICE wrapper dskxv_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_dskxv_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrapper 

      dskxv_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Previous version 01-AUG-2016 (NJB)

-Index_Entries

   test dskxv_c

-&
*/

{ /* Begin f_dskxv_c */

 
   /*
   Constants
   */
   #define PCK0            "dskxv_test.tpc"
   #define TITLEN          321
   #define TIGHT           1.e-12
   #define MAXN            10000

   /*
   Local macros 
   */
   #define V3              (SpiceDouble (*)[3])

   /*
   Local variables
   */
   SpiceBoolean            fnd;
   static SpiceBoolean     fndarr [MAXN];
   SpiceBoolean            found;
   SpiceBoolean            pri;

   SpiceDLADescr           nxtdsc;
   SpiceDLADescr           xdladsc;
   SpiceDSKDescr           xdskdsc;

   SpiceChar             * dsk;
   SpiceChar             * fixref;
   SpiceChar             * frame;
   SpiceChar             * target;
   SpiceChar               title  [ TITLEN ];

   static SpiceDouble      dirarr [MAXN][3];
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             et;
   SpiceDouble             lat;
   SpiceDouble             lon;
   SpiceDouble             r;
   SpiceDouble             raydir [3];
   SpiceDouble             tol;
   SpiceDouble             vertex [3];
   static SpiceDouble      vrtarr [MAXN][3];
   SpiceDouble             xpt    [3];
   static SpiceDouble      xptarr [MAXN][3];
   SpiceDouble             xxpt   [3];

   SpiceInt                bodyid;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                nrays;
   SpiceInt                nslat;
   SpiceInt                nslon;
   SpiceInt                nsurf;
   SpiceInt                plid;
   SpiceInt                surfid;
   SpiceInt                srflst  [ SPICE_SRF_MAXSRF ];
   SpiceInt                xhandle;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dskxv_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a text PCK." );
   
   if ( exists_c(PCK0) ) 
   {
      removeFile(PCK0);
   }

   /*
   Don't load the PCK; do save it. 
   */
   tstpck_c ( PCK0, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load via furnsh_c to avoid later complexities. 
   */
   furnsh_c ( PCK0 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a DSK file containing segments for "
             "Mars and Saturn."                                 );
 

   /*
   We'll use a test utility that creates a tessellated plate model DSK. 
   */
   bodyid = 499;
   surfid = 1;
   frame  = "IAU_MARS";
   nlon   = 80;
   nlat   = 40;

   dsk    = "dskxv_test_0.bds";

   if ( exists_c(dsk) )
   {
      removeFile( dsk );
   }

   /*
   Create the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Add a Saturn segment. 
   */
   bodyid = 699;
   surfid = 2;
   frame  = "IAU_SATURN";
   nlon   = 60;
   nlat   = 30;

   /*
   Append to the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 


   /*
   Load the dsk for later use. 
   */
   furnsh_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: prepare for Mars spear test by obtaining DLA and DSK "
             "segment descriptors."                                   );

   dasopr_c ( dsk, &xhandle );
   chckxc_c ( SPICEFALSE, " ", ok ); 

   dlabfs_c ( xhandle, &xdladsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok ); 

   dskgd_c  ( xhandle, &xdladsc, &xdskdsc );
   chckxc_c ( SPICEFALSE, " ", ok ); 

  
   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   Perform spear test. We'll create a set of rays pointing inward
   toward the target (Mars) and find the surface intercepts of these
   rays.

   This test does not exercise the vectorization capability of dskxv_c.
   We'll get to that later on.

   */

   /*
   The surface list consists of one surface ID (for the only Mars
   segment).
   */
   target    = "Mars";
   fixref    = "IAU_MARS";
   nsurf     = 1;
   srflst[0] = 1;

   /*
   Pick our longitude and latitude band counts so we don't 
   end up sampling from plate boundaries. This simplifies
   our checks on the outputs.
   */
   nslon = 37;
   nslat = 23;

   dlon  = twopi_c() / nslon;
   dlat  = pi_c()    / nslat;

   /*
   Pick a magnitude for the ray's vertex. 
   */
   r = 1.0e6;

   /*
   Pick an evaluation epoch. 
   */
   et = 10 * jyear_c();

   /*
   The prioritization flag is always "false" for the N0066 version
   of SPICE. 
   */
   pri = SPICEFALSE;



   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         lat = halfpi_c() - j*dlat;

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Mars spear test for lon # (deg), lat #(deg).",
                   TITLEN                                         );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );

         tcase_c ( title );


         /*
         Create the ray's vertex and direction vector.
         */
         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxv_c ( pri, target,     nsurf,     srflst, et,    fixref,
                   1,   V3 vertex,  V3 raydir, V3 xpt, &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "dskxv_c found", found, SPICETRUE, ok );


         /*
         Use descriptor information to find expected intercept. 
         */
         dskx02_c ( xhandle, &xdladsc, vertex, 
                    raydir,  &plid,    xxpt,   &fnd );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok );
         
         /*
         Check the intercept. 
         */
         tol = TIGHT;

         chckad_c ( "xpt", xpt, "~~/", xxpt, 3, tol, ok );

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Mars spear test for lon # (deg), lat #(deg). "
                   "using empty surface list.",
                   TITLEN                                         );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );

         tcase_c ( title );

         /*
         We're going to repeat the previous test, but this time
         we'll use an empty surface list. The results should not
         change.
         */
         nsurf = 0;

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxv_c ( pri, target,     nsurf,     srflst, et,    fixref,
                   1,   V3 vertex,  V3 raydir, V3 xpt, &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "dskxv_c found", found, SPICETRUE, ok );


         /*
         Use descriptor information to find expected intercept. 
         */
         dskx02_c ( xhandle, &xdladsc, vertex, 
                    raydir,  &plid,   xxpt,   &fnd );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok );
         
         /*
         Check the intercept. 
         */
         tol = TIGHT;

         chckad_c ( "xpt", xpt, "~~/", xxpt, 3, tol, ok );
      }

   }



   /*
    ********************************************************************

   MARS VECTORIZED CASE



   Now repeat the previous tests, this time using vectorized
   calls to dskxv_c. 

   Start out by creating arrays of ray vertices and direction
   vectors.

    ********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Mars vectorized intercept test. Surface "
             "list is non-empty." );
   
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         
         lat = halfpi_c() - j*dlat;

         /*
         Create the ray's vertex and direction vector.
         */
         latrec_c ( r,    lon,  lat, vrtarr[k] );
         vminus_c ( vrtarr[k],       dirarr[k] );

         ++k;

         /*
         printf ( "k, dirarr[i] = %d %f %f %f\n", (int), 
                  dirarr[i][0], dirarr[i]
         */
      }
   }

   nrays     = k;

   nsurf     = 1;
   srflst[0] = 1;


   /*
   Find ray-surface intercepts for all the rays.
   */
   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results. 
   */

   for ( k = 0;  k < nrays;  k++ )
   {
      /* 
      ---- Case ------------------------------------------------------
      */

      strncpy ( title,
                "Mars vectorized intercept test. Surface "
                "list is non-empty. Checking #th elements of "
                "output arrays.",
                TITLEN                                         );
      repmi_c ( title, "#", k, TITLEN, title );
      
      tcase_c ( title );

      /*
      We expect to find an intercept every time. 
      */
      chcksl_c ( "dskxv_c found", fndarr[k], SPICETRUE, ok );


      /*
      Use descriptor information to find expected intercept. 
      */
      dskx02_c ( xhandle,   &xdladsc, vrtarr[k], 
                 dirarr[k], &plid,    xxpt,     &fnd );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok ); 

      /*
      Check the intercept. 
      */
      tol = TIGHT;

      chckad_c ( "xpt", xptarr[k], "~~/", xxpt, 3, tol, ok );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Mars vectorized intercept test. Surface "
             "list is empty." );



   nsurf = 0;

   /*
   Find ray-surface intercepts for all the rays.
   */
   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results. 
   */

   for ( k = 0;  k < nrays;  k++ )
   {
      /* 
      ---- Case ------------------------------------------------------
      */

      strncpy ( title,
                "Mars vectorized intercept test. Surface "
                "list is empty. Checking #th elements of "
                "output arrays.",
                TITLEN                                         );
      repmi_c ( title, "#", k, TITLEN, title );
      
      tcase_c ( title );

      /*
      We expect to find an intercept every time. 
      */
      chcksl_c ( "dskxv_c found", fndarr[k], SPICETRUE, ok );


      /*
      Use descriptor information to find expected intercept. 
      */
      dskx02_c ( xhandle,   &xdladsc, vrtarr[k], 
                 dirarr[k], &plid,    xxpt,     &fnd );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok ); 

      /*
      Check the intercept. 
      */
      tol = TIGHT;

      chckad_c ( "xpt", xptarr[k], "~~/", xxpt, 3, tol, ok );
   }





   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: prepare for Saturn spear test by obtaining "
             "DLA and DSK segment descriptors."                  );

   dlafns_c ( xhandle, &xdladsc, &nxtdsc, &found );
   chckxc_c ( SPICEFALSE, " ", ok ); 
   xdladsc = nxtdsc;

   dskgd_c  ( xhandle, &xdladsc, &xdskdsc );
   chckxc_c ( SPICEFALSE, " ", ok ); 

   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   Perform spear test. We'll create a set of rays pointing inward
   toward the target (Saturn) and find the surface intercepts of these
   rays.
   */

   /*
   The surface list consists of one surface ID (for the only Saturn
   segment).
   */
   target    = "Saturn";
   fixref    = "IAU_SATURN";
   nsurf     = 1;
   srflst[0] = 2;

   /*
   Pick our longitude and latitude band counts so we don't 
   end up sampling from plate boundaries. This simplifies
   our checks on the outputs.
   */
   nslon = 37;
   nslat = 23;

   dlon  = twopi_c() / nslon;
   dlat  = pi_c()    / nslat;

   /*
   Pick a magnitude for the ray's vertex. 
   */
   r = 1.0e6;

   /*
   Pick an evaluation epoch. 
   */
   et = -10 * jyear_c();

   /*
   The prioritization flag is always "false" for the N0066 version
   of SPICE. 
   */
   pri = SPICEFALSE;



   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         lat = halfpi_c() - j*dlat;

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Saturn spear test for lon # (deg), lat #(deg).",
                   TITLEN                                         );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );

         tcase_c ( title );


         /*
         Create the ray's vertex and direction vector.
         */
         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxv_c ( pri, target,     nsurf,     srflst, et,    fixref,
                   1,   V3 vertex,  V3 raydir, V3 xpt, &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "dskxv_c found", found, SPICETRUE, ok );


         /*
         Use descriptor information to find expected intercept. 
         */
         dskx02_c ( xhandle, &xdladsc, vertex, 
                    raydir,  &plid,    xxpt,   &fnd );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok );
         
         /*
         Check the intercept. 
         */
         tol = TIGHT;

         chckad_c ( "xpt", xpt, "~~/", xxpt, 3, tol, ok );



         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Saturn spear test for lon # (deg), lat #(deg). "
                   "using empty surface list.",
                   TITLEN                                         );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );

         tcase_c ( title );

         /*
         We're going to repeat the previous test, but this time
         we'll use an empty surface list. The results should not
         change.
         */
         nsurf = 0;

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxv_c ( pri, target,     nsurf,     srflst, et,    fixref,
                   1,   V3 vertex,  V3 raydir, V3 xpt, &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "dskxv_c found", found, SPICETRUE, ok );


         /*
         Use descriptor information to find expected intercept. 
         */
         dskx02_c ( xhandle, &xdladsc, vertex, 
                    raydir,   &plid,   xxpt,   &fnd );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok );
         
         /*
         Check the intercept. 
         */
         tol = TIGHT;

         chckad_c ( "xpt", xpt, "~~/", xxpt, 3, tol, ok );
      }

   }


   /*
    ********************************************************************

   SATURN VECTORIZED CASE



   Now repeat the previous tests, this time using vectorized
   calls to dskxv_c. 

   Start out by creating arrays of ray vertices and direction
   vectors.

    ********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn vectorized intercept test. Surface "
             "list is non-empty." );
   

   /*
   We don't need to re-create the rays. We're using the same ones
   we used for Mars. 
   */

   nrays     = k;

   nsurf     = 1;
   srflst[0] = 2;


   /*
   Find ray-surface intercepts for all the rays.
   */
   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results. 
   */

   for ( k = 0;  k < nrays;  k++ )
   {
      /* 
      ---- Case ------------------------------------------------------
      */

      strncpy ( title,
                "Saturn vectorized intercept test. Surface "
                "list is non-empty. Checking #th elements of "
                "output arrays.",
                TITLEN                                         );
      repmi_c ( title, "#", k, TITLEN, title );
      
      tcase_c ( title );

      /*
      We expect to find an intercept every time. 
      */
      chcksl_c ( "dskxv_c found", fndarr[k], SPICETRUE, ok );


      /*
      Use descriptor information to find expected intercept. 
      */
      dskx02_c ( xhandle,   &xdladsc, vrtarr[k], 
                 dirarr[k], &plid,    xxpt,     &fnd );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok ); 

      /*
      Check the intercept. 
      */
      tol = TIGHT;

      chckad_c ( "xpt", xptarr[k], "~~/", xxpt, 3, tol, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn vectorized intercept test. Surface "
             "list is empty." );
   

   nsurf = 0;

   /*
   Find ray-surface intercepts for all the rays.
   */
   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results. 
   */

   for ( k = 0;  k < nrays;  k++ )
   {
      /* 
      ---- Case ------------------------------------------------------
      */

      strncpy ( title,
                "Saturn vectorized intercept test. Surface "
                "list is empty. Checking #th elements of "
                "output arrays.",
                TITLEN                                         );
      repmi_c ( title, "#", k, TITLEN, title );
      
      tcase_c ( title );

      /*
      We expect to find an intercept every time. 
      */
      chcksl_c ( "dskxv_c found", fndarr[k], SPICETRUE, ok );


      /*
      Use descriptor information to find expected intercept. 
      */
      dskx02_c ( xhandle,   &xdladsc, vrtarr[k], 
                 dirarr[k], &plid,    xxpt,     &fnd );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "dskx02_c fnd", fnd, SPICETRUE, ok ); 

      /*
      Check the intercept. 
      */
      tol = TIGHT;

      chckad_c ( "xpt", xptarr[k], "~~/", xxpt, 3, tol, ok );
   }



   /*
   *********************************************************************
   *
   *
   *   dskxv_c error cases
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskxv_c error: invalid ray count." );

   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             0,     vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDCOUNT)", ok );

   dskxv_c ( pri,   target, nsurf,  srflst, et,    fixref,
             -1,    vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDCOUNT)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskxv_c error: frame name not found." );


   /*
   We need not test all of the SPICELIB error cases, but we must test
   at least one; this will exercise the error handling logic in the
   wrapper.
   */
   
   dskxv_c ( pri,   target, nsurf,  srflst, et,    "XXX",
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

 
   /*
   For CSPICE, we must check handling of bad input strings.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskxv_c error: empty input string." );


   /*
   Use the last values of all the inputs. 
   */

   dskxv_c ( pri,   "", nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   dskxv_c ( pri,   target, nsurf,  srflst, et,    "",
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "dskxv_c error: null input string." );

   dskxv_c ( pri,   NULL, nsurf,  srflst, et,    fixref,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   dskxv_c ( pri,   target, nsurf,  srflst, et,    NULL,
             nrays, vrtarr, dirarr, xptarr, fndarr         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );


   /*
   Undo the dasopr_c call we made earlier.
   */
   dascls_c ( xhandle );
   chckxc_c ( SPICEFALSE, " ", ok );    

   kclear_c();

   removeFile( dsk );

 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dskxv_c */

