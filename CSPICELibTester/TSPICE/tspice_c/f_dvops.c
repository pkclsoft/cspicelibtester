/* f_dvops.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b8 = 0.;
static integer c__6 = 6;
static doublereal c_b43 = 1e-13;
static doublereal c_b44 = 10.;

/* $Procedure F_DVOPS (Family of vector operations derivative tests) */
/* Subroutine */ int f_dvops__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double sqrt(doublereal);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    doublereal llim, ulim;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    doublereal expv[6], sout[6];
    integer seed1, i__;
    doublereal dvmag;
    extern /* Subroutine */ int tcase_(char *, ftnlen), dvhat_(doublereal *, 
	    doublereal *), repmd_(char *, char *, doublereal *, integer *, 
	    char *, ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer *,
	     char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dvdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal vtemp[3];
    extern /* Subroutine */ int vperp_(doublereal *, doublereal *, doublereal 
	    *), vcrss_(doublereal *, doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int unorm_(doublereal *, doublereal *, doublereal 
	    *);
    doublereal s1[6], s2[6], x1[3];
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    doublereal length;
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *), ducrss_(
	    doublereal *, doublereal *, doublereal *), dvcrss_(doublereal *, 
	    doublereal *, doublereal *);
    extern doublereal dvnorm_(doublereal *);
    doublereal mag, cmp, mag_log__;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    doublereal exp__;
    char txt[64];
    doublereal prt1[3], prt2[3];

/* $ Abstract */

/*     This routine performs rudimentary tests on a collection */
/*     of derivative of vector operation */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/* -    TSPICE Version 2.0.0, 27-SEP-2005 (NJB) */

/* $ Version */

/* -   TSPICE Version 2.1.0, 03-MAY-2010 (EDW) */

/*       Added test family for DVNORM. */

/* -   TSPICE Version 2.0.0, 27-SEP-2005 (NJB) */

/*       Updated to remove non-standard use of duplicate arguments */
/*       in VSCL, VPERP, and UNORM. */

/* -& */
/* $ Index_Entries */

/*     test DVDOT, DVCRSS, DVHAT, DUCRSS, DVNORM */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_DVOPS", (ftnlen)7);
    s1[0] = 1.;
    s1[1] = 2.;
    s1[2] = 1.;
    s1[3] = 1.;
    s1[4] = -1.;
    s1[5] = 3.;
    s2[0] = 2.;
    s2[1] = 3.;
    s2[2] = 4.;
    s2[3] = 1.;
    s2[4] = 2.;
    s2[5] = 3.;

/*     Case 1 */

    tcase_("Test DVDOT.", (ftnlen)11);
    exp__ = s1[0] * s2[3] + s1[3] * s2[0] + s1[1] * s2[4] + s1[4] * s2[1] + 
	    s1[2] * s2[5] + s1[5] * s2[2];
    cmp = dvdot_(s1, s2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DVDOT", &cmp, "=", &exp__, &c_b8, ok, (ftnlen)5, (ftnlen)1);

/*     Case 2 */

    tcase_("Test DVCRSS.", (ftnlen)12);
    vcrss_(s1, s2, expv);
    vcrss_(s1, &s2[3], prt1);
    vcrss_(&s1[3], s2, prt2);
    vadd_(prt1, prt2, &expv[3]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dvcrss_(s1, s2, sout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SOUT", sout, "=", expv, &c__6, &c_b8, ok, (ftnlen)4, (ftnlen)1);

/*     Case 3 */

    tcase_("Test DVHAT with a state having non-zero position.", (ftnlen)49);
    unorm_(s1, expv, &length);
    vperp_(&s1[3], expv, &expv[3]);
    d__1 = 1. / length;
    vsclip_(&d__1, &expv[3]);
    dvhat_(s1, sout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SOUT", sout, "=", expv, &c__6, &c_b8, ok, (ftnlen)4, (ftnlen)1);

/*     Case 4 */

    tcase_("Test DVHAT with a state having zero position.", (ftnlen)45);
    s1[0] = 0.;
    s1[1] = 0.;
    s1[2] = 0.;
    expv[0] = 0.;
    expv[1] = 0.;
    expv[2] = 0.;
    expv[3] = s1[3];
    expv[4] = s1[4];
    expv[5] = s1[5];
    dvhat_(s1, sout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SOUT", sout, "=", expv, &c__6, &c_b8, ok, (ftnlen)4, (ftnlen)1);

/*     Case 5 */

    tcase_("Test DUCRSS", (ftnlen)11);
    s1[0] = 1.;
    s1[1] = 2.;
    s1[2] = 1.;
    vcrss_(s1, s2, expv);
    vcrss_(s1, &s2[3], prt1);
    vcrss_(&s1[3], s2, prt2);
    vadd_(prt1, prt2, &expv[3]);
    unorm_(expv, vtemp, &length);
    vequ_(vtemp, expv);
    vperp_(&expv[3], expv, vtemp);
    d__1 = 1. / length;
    vscl_(&d__1, vtemp, &expv[3]);
    ducrss_(s1, s2, sout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SOUT", sout, "=", expv, &c__6, &c_b8, ok, (ftnlen)4, (ftnlen)1);

/*     Case 6 */

/*     Test DVNORM, parallel, anti-parallel, zero vector cases. */

    seed1 = -82763653;
    llim = -2.;
    ulim = 25.;
    x1[0] = 1.;
    x1[1] = sqrt(2.);
    x1[2] = sqrt(3.);
    s_copy(txt, "DVNORM ZERO VECTOR CASE", (ftnlen)64, (ftnlen)23);
    tcase_(txt, (ftnlen)64);
    s1[0] = 0.;
    s1[1] = 0.;
    s1[2] = 0.;
    s1[3] = x1[0];
    s1[4] = x1[1];
    s1[5] = x1[2];
    dvmag = dvnorm_(s1);
    chcksd_("DVNORM", &dvmag, "=", &c_b8, &c_b43, ok, (ftnlen)6, (ftnlen)1);

/*     Create a set of 6x1 arrays with, the first three components */
/*     describing a vector with a random magnitude. */


/*     Note, 5000 is an arbitrary value. */

    for (i__ = 1; i__ <= 5000; ++i__) {
	mag_log__ = t_randd__(&llim, &ulim, &seed1);
	mag = pow_dd(&c_b44, &mag_log__);
	s_copy(txt, "DVNORM parallel %d, MAG_LOG %f", (ftnlen)64, (ftnlen)30);
	repmi_(txt, "%d", &i__, txt, (ftnlen)64, (ftnlen)2, (ftnlen)64);
	repmd_(txt, "%f", &mag_log__, &c__6, txt, (ftnlen)64, (ftnlen)2, (
		ftnlen)64);
	tcase_(txt, (ftnlen)64);
	s1[0] = x1[0] * mag;
	s1[1] = x1[1] * mag;
	s1[2] = x1[2] * mag;
	s1[3] = x1[0];
	s1[4] = x1[1];
	s1[5] = x1[2];
	dvmag = dvnorm_(s1);
	d__1 = vnorm_(x1);
	chcksd_(txt, &dvmag, "~", &d__1, &c_b43, ok, (ftnlen)64, (ftnlen)1);
	mag_log__ = t_randd__(&llim, &ulim, &seed1);
	mag = pow_dd(&c_b44, &mag_log__);
	s_copy(txt, "DVNORM anti-parallel %d, MAG_LOG %f", (ftnlen)64, (
		ftnlen)35);
	repmi_(txt, "%d", &i__, txt, (ftnlen)64, (ftnlen)2, (ftnlen)64);
	repmd_(txt, "%f", &mag_log__, &c__6, txt, (ftnlen)64, (ftnlen)2, (
		ftnlen)64);
	tcase_(txt, (ftnlen)64);
	s1[0] = x1[0] * mag;
	s1[1] = x1[1] * mag;
	s1[2] = x1[2] * mag;
	s1[3] = -x1[0];
	s1[4] = -x1[1];
	s1[5] = -x1[2];
	dvmag = dvnorm_(s1);
	d__1 = -vnorm_(x1);
	chcksd_(txt, &dvmag, "~", &d__1, &c_b43, ok, (ftnlen)64, (ftnlen)1);
	mag_log__ = t_randd__(&llim, &ulim, &seed1);
	mag = pow_dd(&c_b44, &mag_log__);
	s_copy(txt, "DVNORM orthogonal %d, MAG_LOG %f", (ftnlen)64, (ftnlen)
		32);
	repmi_(txt, "%d", &i__, txt, (ftnlen)64, (ftnlen)2, (ftnlen)64);
	repmd_(txt, "%f", &mag_log__, &c__6, txt, (ftnlen)64, (ftnlen)2, (
		ftnlen)64);
	tcase_(txt, (ftnlen)64);

/*        Create a 6x1 array with the property the dot product of the */
/*        first three components with the second three components */
/*        has value zero. */

	s2[0] = x1[0] * mag;
	s2[1] = x1[1] * mag;
	s2[2] = x1[2] * mag;
	s2[3] = 1. / s2[0];
	s2[4] = 1. / s2[1];
	s2[5] = -2. / s2[2];
	dvmag = dvnorm_(s2);
	chcksd_(txt, &dvmag, "~", &c_b8, &c_b43, ok, (ftnlen)64, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_dvops__ */

