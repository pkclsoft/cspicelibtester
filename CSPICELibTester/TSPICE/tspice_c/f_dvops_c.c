/*

-Procedure f_dvops_c (Family of vector operations derivative tests )


-Abstract

   Perform tests on CSPICE wrappers for vector derivative functions.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "tutils_c.h"

   void f_dvops_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrappers for vector derivative routines.
   The current set is:
   
      dvdot_c
      dvcrss_c
      dvhat_c
      ducrss_c
      dvnorm_c
   
-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   E.D. Wright    (JPL)

-Version

   -tspice_c Version 1.1.0, 10-FEB-2017 (NJB)

       Removed unneeded declarations.       

   -tspice_c Version 1.0.0 02-MAY-2010 (EDW)

-Index_Entries

   test vector derivative routines

-&
*/

{ /* Begin f_dvops_c
   */


   /*
   Constants
   */

   #define  TIGHT          1.e-13
   #define LENOUT          65

   /*
   Local constants, randomly selected.
   */

   SpiceDouble     x1   [3] =
                           { 1., sqrt(2.), sqrt(3.) };

   SpiceDouble     s1   [6] =
                           { 1., 2., 1., 1., -1., 3. };

   SpiceDouble     s2   [6] =
                           { 2., 3., 4., 1.,  2., 3. };


   SpiceDouble             cmp;
   SpiceDouble             exp;
   SpiceDouble             length;
   SpiceDouble             mag_log;
   SpiceDouble             mag;
   SpiceDouble             ulim;
   SpiceDouble             llim;
   SpiceDouble             dvmag;

   SpiceDouble             prt1   [3];
   SpiceDouble             prt2   [3];
   SpiceDouble             vtemp  [3];

   SpiceDouble             expv   [6];
   SpiceDouble             sout   [6];

   SpiceInt                i;
   SpiceInt                seed1;

   SpiceChar               txt[LENOUT];

   topen_c ( "f_dvops_c" );


   /*
   Case 1
   */
   tcase_c ( "Test DVDOT." );

      exp = s1[0] * s2[3] + s1[3] * s2[0]
          + s1[1] * s2[4] + s1[4] * s2[1]
          + s1[2] * s2[5] + s1[5] * s2[2];

      cmp = dvdot_c( s1, s2 );
      chcksd_c ( "dvdot_c", cmp, "=", exp, 0., ok );


   /*
   Case 2
   */
   tcase_c ( "Test DVCRSS." );

      vcrss_c ( s1,       s2,       expv );
      vcrss_c ( &(s1[0]), &(s2[3]), prt1 );
      vcrss_c ( &(s1[3]), &(s2[0]), prt2 );
      vadd_c  ( prt1,     prt2,     &(expv[3]) );

      dvcrss_c ( s1, s2, sout );
      chckad_c ( "sout (1)", sout, "=", expv, 6, 0., ok );


   /*
   Case 3
   */
   tcase_c ( "Test DVHAT with a state having non-zero position." );

      unorm_c ( s1,         expv,       &length    );
      vperp_c ( &(s1[3]),   expv,       &(expv[3]) );
      vscl_c  ( 1./length,  &(expv[3]), &(expv[3]) );

      dvhat_c ( s1, sout );
      chckad_c ( "sout (2)", sout, "=", expv, 6, 0., ok );


   /*
   Case 4
   */
   tcase_c ( "Test DVHAT with a state having zero position." );

      s1[0]   = 0.;
      s1[1]   = 0.;
      s1[2]   = 0.;

      expv[0] = 0.;
      expv[1] = 0.;
      expv[2] = 0.;
      expv[3] = s1[3];
      expv[4] = s1[4];
      expv[5] = s1[5];

      dvhat_c ( s1, sout );
      chckad_c ( "sout (3)", sout, "=", expv, 6, 0., ok );


   /*
   Case 5
   */
   tcase_c ( "Test DUCRSS" );

      s1[0]   = 1.;
      s1[1]   = 2.;
      s1[2]   = 1.;

      vcrss_c ( s1,          s2,         expv       );
      vcrss_c ( &(s1[0]),    &(s2[3]),   &(prt1[0]) );
      vcrss_c ( &(s1[3]),    &(s2[0]),   prt2       );
      vadd_c  ( prt1,        prt2,       &(expv[3]) );
      unorm_c ( expv,        vtemp,      &length    );
      vequ_c  ( vtemp,                   expv       );
      vperp_c ( &(expv[3]),  &(expv[0]), vtemp      );
      vscl_c  ( 1.00/length, vtemp,      &(expv[3]) );

      ducrss_c ( s1, s2, sout );
      chckad_c ( "sout (4)", sout, "=", expv, 6, 0., ok );


   /*
   Case 6

   Test dvnorm_c, parallel, anti-parallel, zero vector cases.

   */

   seed1 = -82763653;
   llim  = -2.;
   ulim  = 25.;

   sprintf(txt, "DVNORM ZERO VECTOR CASE" );
   tcase_c( txt );

   s1[0] =  0.;
   s1[1] =  0.;
   s1[2] =  0.;
   s1[3] =  x1[0];
   s1[4] =  x1[1];
   s1[5] =  x1[2];

   dvmag = dvnorm_c( s1 );

   chcksd_c ( txt, dvmag, "=", 0., TIGHT, ok );

   /*

   Create a set of 6x1 arrays with, the first three components
   describing a vector with a random magnitude.

   Note, 5000 is an arbitrary value.
   */
   for( i=0; i<5000; i++ )
      {

      mag_log = t_randd__( &llim, &ulim, &seed1);
      mag     = pow(10., mag_log );
      
      sprintf(txt, "DVNORM parallel #d, MAG_LOG #f" );

      repmi_c( txt, "#d", i, LENOUT, txt );
      repmd_c( txt, "#f", mag_log, 6, LENOUT, txt );
      tcase_c( txt );

      s1[0] =  x1[0] * mag;
      s1[1] =  x1[1] * mag;
      s1[2] =  x1[2] * mag;
      s1[3] =  x1[0];
      s1[4] =  x1[1];
      s1[5] =  x1[2];

      dvmag = dvnorm_c( s1 );

      chcksd_c ( txt, dvmag, "~", vnorm_c(x1), TIGHT, ok );


      mag_log = t_randd__( &llim, &ulim, &seed1);
      mag     = pow(10., mag_log );

      sprintf(txt, "DVNORM anti-parallel #d, MAG_LOG #f" );

      repmi_c( txt, "#d", i, LENOUT, txt );
      repmd_c( txt, "#f", mag_log, 6, LENOUT, txt );
      tcase_c( txt );

      s1[0] =  x1[0] * mag;
      s1[1] =  x1[1] * mag;
      s1[2] =  x1[2] * mag;
      s1[3] = -x1[0];
      s1[4] = -x1[1];
      s1[5] = -x1[2];

      dvmag = dvnorm_c( s1 );

      chcksd_c ( txt, dvmag, "~", -vnorm_c(x1), TIGHT, ok );


      mag_log = t_randd__( &llim, &ulim, &seed1);
      mag     = pow(10., mag_log );

      sprintf(txt, "DVNORM orthogonal #d, MAG_LOG #f" );

      repmi_c( txt, "#d", i, LENOUT, txt );
      repmd_c( txt, "#f", mag_log, 6, LENOUT, txt );
      tcase_c( txt );

      s2[0] =  x1[0] * mag;
      s2[1] =  x1[1] * mag;
      s2[2] =  x1[2] * mag;
      s2[3] =  1./s1[0];
      s2[4] =  1./s1[1];
      s2[5] = -2./s1[2];

      dvmag = dvnorm_c( s2 );

      chcksd_c ( txt, dvmag, "~", 0., TIGHT, ok );

      }


   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );

} /* End f_dvops_c */

