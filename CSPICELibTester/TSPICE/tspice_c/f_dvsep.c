/* f_dvsep.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b19 = 0.;
static doublereal c_b20 = 1e-12;
static doublereal c_b26 = -2.;
static doublereal c_b27 = 25.;
static doublereal c_b28 = 10.;
static integer c__8 = 8;
static integer c__6 = 6;

/* $Procedure F_DVSEP ( DVSEP family tests ) */
/* Subroutine */ int f_dvsep__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *), mxvg_(
	    doublereal *, doublereal *, integer *, integer *, doublereal *);
    integer seed1, seed2;
    doublereal crss1[3], crss2[3];
    integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal colat;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmd_(char *, char *, doublereal *, integer *, 
	    char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmax_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    extern doublereal dvsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal trans[36]	/* was [6][6] */;
    extern /* Subroutine */ int vcrss_(doublereal *, doublereal *, doublereal 
	    *);
    extern doublereal twopi_(void);
    doublereal s1[6], s2[6];
    extern /* Subroutine */ int t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), delfil_(
	    char *, ftnlen), kclear_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), t_pck08__(char *, logical *, logical *, 
	    ftnlen), sphrec_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), furnsh_(char *, ftnlen), sxform_(char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal mag, val, mag_log__, vel[3], lon;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    char txt[128];
    doublereal rec1[3], rec2[3], s1_t__[6], s2_t__[6];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        DVSEP */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 01-APR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Begin every test family with an open call. */

    topen_("F_DVSEP", (ftnlen)7);

/*     Create a PCK, load using FURNSH. */

    t_pck08__("dvsep.pck", &c_false, &c_true, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("dvsep.pck", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 1 */

    tcase_("DVSEP overflow.", (ftnlen)15);

/*     Two state vectors, one rather odd. */

    s1[0] = 3.;
    s1[1] = 4.;
    s1[2] = 0.;
    s1[3] = -9.;
    s1[4] = 2.;
    s1[5] = .1;

/*     S2 has a very high derivative of the unit vector. */

    s2[0] = 1.;
    s2[1] = 0.;
    s2[2] = 1.;
    s2[3] = 1.;
    s2[4] = dpmax_() * .9;
    s2[5] = 0.;
    val = dvsep_(s1, s2);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);

/*     Case 2 */

    tcase_("DVSEP S1xS2 = 0", (ftnlen)15);
    s1[0] = 25.;
    s1[1] = 0.;
    s1[2] = 0.;
    s1[3] = 2.;
    s1[4] = -1.;
    s1[5] = 1.;
    s2[0] = -s1[0];
    s2[1] = -s1[1];
    s2[2] = -s1[2];
    s2[3] = -s1[3];
    s2[4] = -s1[4];
    s2[5] = -s1[5];
    val = dvsep_(s1, s2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Expect a separation angle rate of zero. */

    chcksd_("DVSEP S1xS2 = 0 (1)", &val, "=", &c_b19, &c_b20, ok, (ftnlen)19, 
	    (ftnlen)1);

/*     Case 3 */


/*     Create two random vectors in R3, REC1 and REC2, each with */
/*     magnitude MAG. Use the cross product operations */

/*        CRSS1 = REC1  x REC2 */
/*        CRSS2 = CRSS1 x REC1 */

/*     to construct a velocity unit vector orthogonal to REC1 in the */
/*     plane of REC1 and REC2 oriented such that REC1 rotates towards */
/*     REC2 with REC2 constant. */

/*     In this case */

/*            d(theta) */
/*        v = -------- * || REC1 || */
/*            dt */

/*     with v = 1, then */

/*        d(theta)     -1           -1 */
/*        -------- =  ______    =   --- */
/*        dt */
/*                  || REC1 ||      MAG */

    seed1 = -82763653;
    seed2 = -273661;

/*     Define an arbitrary rotation from J2000 to something */
/*     not referenced against earth rotation at an arbitrary */
/*     ephemeris time, ET = 0 in this case. */

    sxform_("J2000", "IAU_MOON", &c_b19, trans, (ftnlen)5, (ftnlen)8);

/*     Note, 5000 is also an arbitrary value. */

    for (i__ = 1; i__ <= 5000; ++i__) {
	s_copy(txt, "S1, S2 random test #, MAG_LOG #", (ftnlen)128, (ftnlen)
		31);
	repmi_(txt, "#", &i__, txt, (ftnlen)128, (ftnlen)1, (ftnlen)128);
	mag_log__ = t_randd__(&c_b26, &c_b27, &seed1);
	mag = pow_dd(&c_b28, &mag_log__);
	repmd_(txt, "#", &mag_log__, &c__8, txt, (ftnlen)128, (ftnlen)1, (
		ftnlen)128);
	tcase_(txt, (ftnlen)128);
	d__1 = pi_();
	colat = t_randd__(&c_b19, &d__1, &seed1);
	d__1 = twopi_();
	lon = t_randd__(&c_b19, &d__1, &seed2);
	sphrec_(&mag, &colat, &lon, rec1);
	d__1 = pi_();
	colat = t_randd__(&c_b19, &d__1, &seed1);
	d__1 = twopi_();
	lon = t_randd__(&c_b19, &d__1, &seed2);
	sphrec_(&mag, &colat, &lon, rec2);
	vcrss_(rec1, rec2, crss1);
	vcrss_(crss1, rec1, crss2);
	vhat_(crss2, vel);
	vpack_(rec1, &rec1[1], &rec1[2], s1);
	vpack_(vel, &vel[1], &vel[2], &s1[3]);
	vpack_(rec2, &rec2[1], &rec2[2], s2);
	vpack_(&c_b19, &c_b19, &c_b19, &s2[3]);

/*        Apply the transformation, TRANS, to S1 and S2, creating new */
/*        vectors without or with fewer zero components, particularly */
/*        the S2 velocity components. */

	mxvg_(trans, s1, &c__6, &c__6, s1_t__);
	mxvg_(trans, s2, &c__6, &c__6, s2_t__);
	val = dvsep_(s1_t__, s2_t__);
	d__1 = -1. / mag;
	chcksd_(txt, &val, "~", &d__1, &c_b20, ok, (ftnlen)128, (ftnlen)1);
    }

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("dvsep.pck", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dvsep__ */

