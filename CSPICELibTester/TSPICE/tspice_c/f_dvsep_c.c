/*

-Procedure f_dvsep_c ( Test dvsep_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper dvsep_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "tutils_c.h"
   
   doublereal t_randd__(doublereal *lower, doublereal *upper, integer *seed);

   void f_dvsep_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for dvsep_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   E.D. Wright    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 01-APR-2009 (EDW) 

-Index_Entries

   test dvsep_c

-&
*/

{ /* Begin f_dvsep_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define TIGHT           1.e-12   
   #define LENOUT          65
   #define PCK             "dvsep.pck"

   /*
   Local variables
   */

   SpiceDouble             val;
   SpiceDouble             colat;
   SpiceDouble             lon;
   SpiceDouble             mag_log;
   SpiceDouble             mag;
   SpiceDouble             ulim;
   SpiceDouble             llim;
   SpiceDouble             vel   [ 3 ];
   SpiceDouble             rec1  [ 3 ];
   SpiceDouble             rec2  [ 3 ];
   SpiceDouble             crss1 [ 3 ];
   SpiceDouble             crss2 [ 3 ];
   SpiceDouble             s1    [ 6 ];
   SpiceDouble             s2    [ 6 ];
   SpiceDouble             s1_t  [ 6 ];
   SpiceDouble             s2_t  [ 6 ];
   SpiceDouble             trans [ 6 ][ 6 ];

   SpiceInt                i;
   SpiceInt                seed1;
   SpiceInt                seed2;

   SpiceChar               txt[LENOUT];


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_dvsep_c" );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Case 1
   */
   tcase_c( "DVSEP overflow." );

   /*
   Two state vectors, one rather odd.
   */
   s1[0] = 3.; 
   s1[1] = 4.;
   s1[2] = 0.;
   s1[3] = -9.; 
   s1[4] = 2.;
   s1[5] = 0.1;

   /*
   s2 has a very high derivative of the unit vector.
   */
   s2[0] = 1.;
   s2[1] = 0.;
   s2[2] = 1.;
   s2[3] = 1.;
   s2[4] = 0.9 * dpmax_c();
   s2[5] = 0.;

   val = dvsep_c( s1, s2 );
   chckxc_c ( SPICETRUE, "SPICE(NUMERICOVERFLOW)", ok );


   /*
   Case 2
   */
   tcase_c( "DVSEP S1xS2 = 0" );

   s1[0] = 25; 
   s1[1] = 0;
   s1[2] = 0.;
   s1[3] = 2.; 
   s1[4] = -1.;
   s1[5] = 1.;

   s2[0] = -s1[0];
   s2[1] = -s1[1];
   s2[2] = -s1[2];
   s2[3] = -s1[3];
   s2[4] = -s1[4];
   s2[5] = -s1[5];

   val = dvsep_c( s1, s2 );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c( "DVSEP S1xS2 = 0", val, "=", 0., TIGHT, ok );


   /*   
   Case 3
   


   Create two random vectors in R3, REC1 and REC2, each with 
   magnitude MAG. Use the cross product operations 

      CRSS1 = REC1  x REC2
      CRSS2 = CRSS1 x REC1

   to construct a velocity unit vector orthogonal to REC1 in the  
   plane of REC1 and REC2 oriented such that REC1 rotates towards
   REC2 with REC2 constant.

   In this case

          d(theta) 
      v = -------- * || REC1 ||
          dt

   with v = 1, then

      d(theta)     -1           -1
      -------- =  ______    =   ---
      dt
                || REC1 ||      MAG
 
   */

   seed1 = -82763653L;
   seed2 = -273661L;

   /*
   Define an arbitrary rotation from J2000 to something
   not referenced against earth rotation at an arbitrary
   ephemeris time, ET = 0 in this case.
   */
   sxform_c ( "J2000", "IAU_MOON", 0., trans );

   /*
   Note, 5000 is also an arbitrary value.
   */
   for( i=0; i<5000; i++ )
      {
      sprintf(txt, "S1, S2 random test #, MAG_LOG #" );

      repmi_c ( txt, "#", i, LENOUT, txt );

      llim = -2.;
      ulim = 25.;
      mag_log = t_randd__( &llim, &ulim, &seed1);
      mag     = pow(10., mag_log );

      repmd_c( txt, "#", mag_log, 6, LENOUT, txt );
      tcase_c( txt );

      llim = 0.;
      ulim = pi_c();
      colat = t_randd__( &llim, &ulim, &seed1);

      llim = 0.;
      ulim = twopi_c();
      lon = t_randd__( &llim, &ulim, &seed2);

      sphrec_c( mag, colat, lon, rec1 );

      llim = 0.;
      ulim = pi_c();
      colat = t_randd__( &llim, &ulim, &seed1);

      llim = 0.;
      ulim = twopi_c();
      lon = t_randd__( &llim, &ulim, &seed2);

      sphrec_c( mag, colat, lon, rec2 );

      vcrss_c( rec1,  rec2, crss1 );
      vcrss_c( crss1, rec1, crss2 );

      vhat_c( crss2, vel );
      s1[0] = rec1[0];
      s1[1] = rec1[1];
      s1[2] = rec1[2];
      s1[3] = vel [0];
      s1[4] = vel [1];
      s1[5] = vel [2];

      s2[0] = rec2[0];
      s2[1] = rec2[1];
      s2[2] = rec2[2];
      s2[3] = 0.;
      s2[4] = 0.;
      s2[5] = 0.;

      /*
      Apply the transformation, 'trans', to s1 and s2, creating new 
      vectors without or with fewer zero components, particularly 
      the s2 velocity components.
      */
      mxvg_c( trans, s1, 6, 6, s1_t);
      mxvg_c( trans, s2, 6, 6, s2_t);
      
      val = dvsep_c( s1_t, s2_t );
      chcksd_c( txt, val, "~", -1./mag, TIGHT, ok );

      }


   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();
   
   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_dvsep_c */

