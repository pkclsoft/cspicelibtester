/* f_dwpool.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__26003 = 26003;
static logical c_false = FALSE_;
static integer c_b10 = 130015;
static integer c__1 = 1;
static logical c_true = TRUE_;
static integer c__0 = 0;
static integer c__4000 = 4000;
static integer c__1000 = 1000;

/* $Procedure      F_DWPOOL ( DWPOOL tests ) */
/* Subroutine */ int f_dwpool__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer nset;
    static char vars__[32*26003];
    extern /* Subroutine */ int zzgapool_(char *, char *, integer *, integer *
	    , char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer i__, j;
    extern integer cardc_(char *, ftnlen);
    static integer n;
    extern /* Subroutine */ int zzvupool_(char *, integer *, integer *, char *
	    , ftnlen, ftnlen);
    static char agent[32];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int movec_(char *, integer *, char *, ftnlen, 
	    ftnlen), repmi_(char *, char *, integer *, char *, ftnlen, ftnlen,
	     ftnlen);
    static integer nused;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nvars__;
    extern /* Subroutine */ int t_success__(logical *), chckac_(char *, char *
	    , char *, char *, integer *, logical *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), clearc_(integer *, char *, ftnlen), chcksc_(char *, char 
	    *, char *, char *, logical *, ftnlen, ftnlen, ftnlen, ftnlen), 
	    cleari_(integer *, integer *), scardc_(integer *, char *, ftnlen),
	     validc_(integer *, integer *, char *, ftnlen), chckxc_(logical *,
	     char *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    static integer navail, nagent;
    extern /* Subroutine */ int orderc_(char *, integer *, integer *, ftnlen);
    static char agents[32*130015];
    static integer ordvec[130015];
    static logical update;
    extern /* Subroutine */ int lnkini_(integer *, integer *);
    extern integer lnknfn_(integer *);
    static char agtset[32*130021];
    static doublereal dpvals[400000];
    static char xagset[32*130021];
    static integer onagnt;
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), clpool_(void), ssizec_(integer *, char *, ftnlen), 
	    cvpool_(char *, logical *, ftnlen), insrtc_(char *, char *, 
	    ftnlen, ftnlen);
    static char uwagnt[32*130015];
    extern /* Subroutine */ int dwpool_(char *, ftnlen);
    static integer onvars;
    static char okvset[32*130021];
    extern /* Subroutine */ int swpool_(char *, integer *, char *, ftnlen, 
	    ftnlen);
    static integer uwpool[260042]	/* was [2][130021] */;
    static char uwvars[32*26009];
    static integer uwptrs[26003];

/* $ Abstract */

/*     This routine tests the SPICELIB POOL entry point */

/*        DWPOOL */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB kernel pool */
/*     utility routine DWPOOL. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 13-JUN-2013 (BVS) */

/*        Set MAXVAR, MAXVAL, MAXAGT, and MXNOTE to values */
/*        used in POOL. */

/* -    TSPICE Version 1.0.0, 19-MAR-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_DWPOOL", (ftnlen)8);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Initializations", (ftnlen)15);
    ssizec_(&c__26003, uwvars, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__26003, uwptrs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    lnkini_(&c_b10, uwpool);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    clearc_(&c__26003, uwagnt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizec_(&c_b10, agtset, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizec_(&c_b10, okvset, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizec_(&c_b10, xagset, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Find out what's already in the watcher system. */
/*     Save the original set of watched variables in */
/*     OKVSET. */

    zzvupool_(okvset, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    onvars = cardc_(okvset, (ftnlen)32);

/*     It's not helpful to save the original agent pool, but */
/*     we will save the number of entries. */

    onagnt = 130015 - lnknfn_(uwpool);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Basic SWPOOL/CVPOOL checks: 1 agent, 1 variable, variable exists"
	    " in pool.", (ftnlen)73);
    s_copy(agent, "AGENT1", (ftnlen)32, (ftnlen)6);
    nvars__ = 1;
    s_copy(vars__, "VAR1", (ftnlen)32, (ftnlen)4);
    dpvals[0] = 999.;
    pdpool_(vars__, &c__1, dpvals, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    swpool_(agent, &nvars__, vars__, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect the agent to receive an update */
/*     notice as soon as the watch is set. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 0", &update, &c_true, ok, (ftnlen)8);

/*     Now the next check should show no update. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 1", &update, &c_false, ok, (ftnlen)8);

/*     Clear the kernel pool; an update should be */
/*     indicated. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 2", &update, &c_true, ok, (ftnlen)8);

/*     Now the next check should show no update. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 1", &update, &c_false, ok, (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Basic SWPOOL/CVPOOL checks: 1 agent, 1 variable, variable does n"
	    "ot exist in pool.", (ftnlen)81);
    s_copy(agent, "AGENT2", (ftnlen)32, (ftnlen)6);
    nvars__ = 1;
    s_copy(vars__, "VAR2", (ftnlen)32, (ftnlen)4);
    swpool_(agent, &nvars__, vars__, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect the agent to receive an update */
/*     notice as soon as the watch is set. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 0", &update, &c_true, ok, (ftnlen)8);

/*     Now the next check should show no update. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 1", &update, &c_false, ok, (ftnlen)8);

/*     Clear the kernel pool; an update should be */
/*     indicated. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 3", &update, &c_true, ok, (ftnlen)8);

/*     Now the next check should show no update. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 4", &update, &c_false, ok, (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set one watch and delete it; variable exists.", (ftnlen)45);
    s_copy(agent, "AGENT3", (ftnlen)32, (ftnlen)6);
    nvars__ = 1;
    s_copy(vars__, "VAR3", (ftnlen)32, (ftnlen)4);
    dpvals[0] = -999.;
    pdpool_(vars__, &c__1, dpvals, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    swpool_(agent, &nvars__, vars__, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect the agent to receive an update */
/*     notice as soon as the watch is set. Clear */
/*     the update notice. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the watch. */

    dwpool_(agent, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Clear the kernel pool; an update should be */
/*     indicated if the watch is still set. We */
/*     expect that the watch isn't set. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now the next check should show no update. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Update 0", &update, &c_false, ok, (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Delete the watches from the previous tests.", (ftnlen)43);
    cvpool_("AGENT1", &update, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dwpool_("AGENT1", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cvpool_("AGENT2", &update, (ftnlen)6);
    dwpool_("AGENT2", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cvpool_("AGENT3", &update, (ftnlen)6);
    dwpool_("AGENT3", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set multiple watches on one variable; variable does not exist in"
	    " the kernel pool.", (ftnlen)81);
    s_copy(vars__, "VAR1", (ftnlen)32, (ftnlen)4);
    nagent = 100;
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)436)) << 5), 
		"AGENT#", (ftnlen)32, (ftnlen)6);
	repmi_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)437)) << 5), 
		"#", &i__, agents + (((i__3 = i__ - 1) < 130015 && 0 <= i__3 ?
		 i__3 : s_rnge("agents", i__3, "f_dwpool__", (ftnlen)437)) << 
		5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)439)) << 5), &
		c__1, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Since these watches are new, each agent should be notified */
/*     of an update. Verify this. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)453)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(1) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)464)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Examine the watcher system; see whether we have the */
/*     expected contents. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the agents for VAR(1). */

    ssizec_(&c_b10, agtset, (ftnlen)32);
    zzgapool_(vars__, uwvars, uwptrs, uwpool, uwagnt, agtset, (ftnlen)32, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We must sort the expected agent array before the comparison. */

    movec_(agents, &nagent, xagset + 192, (ftnlen)32, (ftnlen)32);
    validc_(&c_b10, &nagent, xagset, (ftnlen)32);
    chckac_("AGTSET", agtset + 192, "=", xagset + 192, &nagent, ok, (ftnlen)6,
	     (ftnlen)32, (ftnlen)1, (ftnlen)32);

/*     The order of deletion can be important, so we're going to */
/*     delete the watches in: */

/*         - order of insertion */
/*         - reverse order of insertion */
/*         - alphabetically sorted order */

/*     We'll re-set the watches in the original order each time we */
/*     re-set them. */

/*     Delete watches in order of insertion. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dwpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)510)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (0)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set watches in the original order. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)536)) << 5), &
		c__1, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(2) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)543)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(3) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)554)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in the reverse of the order of insertion. */

    for (i__ = nagent; i__ >= 1; --i__) {
	dwpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)567)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set watches in the original order. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)593)) << 5), &
		c__1, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(4) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)600)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(5) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)611)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in the reverse of the order of insertion. */

    for (i__ = nagent; i__ >= 1; --i__) {
	dwpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)623)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (2)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set watches in the original order. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)649)) << 5), &
		c__1, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(6) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)656)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(7) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)667)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in sorted order. */

    orderc_(agents, &nagent, ordvec, (ftnlen)32);
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = ordvec[(i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : s_rnge(
		"ordvec", i__2, "f_dwpool__", (ftnlen)682)];
	dwpool_(agents + (((i__2 = j - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)684)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (3)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set multiple watches on multiple variables; variables do not exi"
	    "st in the kernel pool. Each agent watches the same variables.", (
	    ftnlen)125);
    nvars__ = 19;
    nagent = 11;
    i__1 = nvars__;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)719)) << 5), "VAR"
		"S#", (ftnlen)32, (ftnlen)5);
	repmi_(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)720)) << 5), "#", &
		i__, vars__ + (((i__3 = i__ - 1) < 26003 && 0 <= i__3 ? i__3 :
		 s_rnge("vars", i__3, "f_dwpool__", (ftnlen)720)) << 5), (
		ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)727)) << 5), 
		"AGENT#", (ftnlen)32, (ftnlen)6);
	repmi_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)728)) << 5), 
		"#", &i__, agents + (((i__3 = i__ - 1) < 130015 && 0 <= i__3 ?
		 i__3 : s_rnge("agents", i__3, "f_dwpool__", (ftnlen)728)) << 
		5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)730)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Since these watches are new, each agent should be notified */
/*     of an update. Verify this. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)744)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(1) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)755)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Examine the watcher system; see whether we have the */
/*     expected contents. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We must sort the expected agent array before the comparison. */

    movec_(agents, &nagent, xagset + 192, (ftnlen)32, (ftnlen)32);
    validc_(&c_b10, &nagent, xagset, (ftnlen)32);

/*     Get the agents for each kernel variable; compare these to */
/*     our sorted agent set. */

    i__1 = nvars__;
    for (i__ = 1; i__ <= i__1; ++i__) {
	zzgapool_(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)784)) << 5), 
		uwvars, uwptrs, uwpool, uwagnt, agtset, (ftnlen)32, (ftnlen)
		32, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckac_("AGTSET", agtset + 192, "=", xagset + 192, &nagent, ok, (
		ftnlen)6, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    }

/*     Delete watches in order of insertion. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dwpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)797)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (0)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)822)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(2) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)828)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(3) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)839)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in the reverse of the order of insertion. */

    for (i__ = nagent; i__ >= 1; --i__) {
	dwpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)851)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)877)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(4) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)883)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(5) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)894)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in sorted order. */

    orderc_(agents, &nagent, ordvec, (ftnlen)32);
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = ordvec[(i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : s_rnge(
		"ordvec", i__2, "f_dwpool__", (ftnlen)908)];
	dwpool_(agents + (((i__2 = j - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)910)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (3)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set multiple watches on multiple variables; variables do not exi"
	    "st in the kernel pool. Each agent watches a different set of var"
	    "iables.", (ftnlen)135);

/*     The sets of watched variables are disjoint in this test case. */
/*     Each variable is watched by only one agent. */

    nvars__ = 31;
    nagent = 21;
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)952)) << 5), 
		"AGENT#", (ftnlen)32, (ftnlen)6);
	repmi_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)953)) << 5), 
		"#", &i__, agents + (((i__3 = i__ - 1) < 130015 && 0 <= i__3 ?
		 i__3 : s_rnge("agents", i__3, "f_dwpool__", (ftnlen)953)) << 
		5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)957)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)958)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)958)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)961)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)961)) <<
		     5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)966)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Since these watches are new, each agent should be notified */
/*     of an update. Verify this. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)980)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(1) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)991)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Examine the watcher system; see whether we have the */
/*     expected contents. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the agent (note: singular) for each kernel variable. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Re-create the name VARS(J). */

	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1014)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1015)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1015))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1018)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1018)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Get the current watch state. */

	    zzgapool_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1024)) << 5), 
		    uwvars, uwptrs, uwpool, uwagnt, agtset, (ftnlen)32, (
		    ftnlen)32, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "CARDC(AGTSET) (#,#)", (ftnlen)80, (ftnlen)19);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    i__3 = cardc_(agtset, (ftnlen)32);
	    chcksi_(qname, &i__3, "=", &c__1, &c__0, ok, (ftnlen)80, (ftnlen)
		    1);
	    chcksc_("AGTSET (0)", agtset + 192, "=", agents + (((i__3 = i__ - 
		    1) < 130015 && 0 <= i__3 ? i__3 : s_rnge("agents", i__3, 
		    "f_dwpool__", (ftnlen)1036)) << 5), ok, (ftnlen)10, (
		    ftnlen)32, (ftnlen)1, (ftnlen)32);
	}
    }

/*     Delete watches in order of insertion. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dwpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1047)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (0)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1076)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1077)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1077))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1080)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1080)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1085)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(2) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1091)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(3) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1102)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in the reverse of the order of insertion. */

    for (i__ = nagent; i__ >= 1; --i__) {
	dwpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1114)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1142)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1143)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1143))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1146)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1146)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1151)) << 5), &
		nvars__, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "(4) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1157)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*        Make sure a second test indicates "no update." */

	s_copy(qname, "(5) Update #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1168)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
    }

/*     Delete watches in sorted order. */

    orderc_(agents, &nagent, ordvec, (ftnlen)32);
    i__1 = nagent;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = ordvec[(i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : s_rnge(
		"ordvec", i__2, "f_dwpool__", (ftnlen)1182)];
	dwpool_(agents + (((i__2 = j - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1184)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (2)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set multiple watches on multiple variables; variables do not exi"
	    "st in the kernel pool. Variables are watched by multiple agents.",
	     (ftnlen)128);

/*     The sets of watched variables are disjoint in this test case. */
/*     Each variable is watched by multiple agents. */

    nset = 5;
    nvars__ = 21;
    nagent = 11;
    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Each variable name includes I and J. */

	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1229)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1230)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1230))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1233)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1233)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {

/*           Each agent name includes I and J. */

	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1243)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1244)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1244)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1247)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1247)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Set a watch for the Jth agent on the Ith */
/*           set of variables. */

	    swpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1253)) << 5),
		     &nvars__, vars__, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Since these watches are new, each agent should be notified */
/*     of an update. Verify this. */

    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(qname, "(0) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1274)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1275)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1275)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1278)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1278)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1281)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*           Make sure a second test indicates "no update." */

	    s_copy(qname, "(1) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1295)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
	}
    }

/*     Examine the watcher system; see whether we have the */
/*     expected contents. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the agent (note: singular) for each kernel variable. */

    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Re-create the agent and variable names. */

	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Each variable name includes I and J. */

	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1323)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1324)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1324))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1327)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1327)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Create an agent set while we're creating agent names. */

	scardc_(&c__0, xagset, (ftnlen)32);
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {

/*           Each agent name includes I and J. */

	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1341)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1342)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1342)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1345)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1345)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    insrtc_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1348)) << 5),
		     xagset, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Get the current watch state. */

	    zzgapool_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1359)) << 5), 
		    uwvars, uwptrs, uwpool, uwagnt, agtset, (ftnlen)32, (
		    ftnlen)32, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "CARDC(AGTSET) (#,#)", (ftnlen)80, (ftnlen)19);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    i__3 = cardc_(agtset, (ftnlen)32);
	    chcksi_(qname, &i__3, "=", &nagent, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    chckac_("AGTSET (0)", agtset + 192, "=", xagset + 192, &nagent, 
		    ok, (ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	}
    }

/*     Delete watches in order of insertion. */

    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1385)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1386)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1386)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1389)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1389)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dwpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1392)) << 5),
		     (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (0)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Each variable name includes I and J. */

	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1424)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1425)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1425))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1428)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1428)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {

/*           Each agent name includes I and J. */

	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1438)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1439)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1439)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1442)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1442)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Set a watch for the Jth agent on the Ith */
/*           set of variables. */

	    swpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1448)) << 5),
		     &nvars__, vars__, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Check update status. */

	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(qname, "(2) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1464)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1465)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1465)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1468)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1468)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1471)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*           Make sure a second test indicates "no update." */

	    s_copy(qname, "(3) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1485)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
	}
    }

/*     Delete watches in the reverse of the order of insertion. */

    for (i__ = nset; i__ >= 1; --i__) {
	for (j = nagent; j >= 1; --j) {
	    s_copy(agents + (((i__1 = j - 1) < 130015 && 0 <= i__1 ? i__1 : 
		    s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1502)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__1 = j - 1) < 130015 && 0 <= i__1 ? i__1 : 
		    s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1503)) << 5),
		     "#", &i__, agents + (((i__2 = j - 1) < 130015 && 0 <= 
		    i__2 ? i__2 : s_rnge("agents", i__2, "f_dwpool__", (
		    ftnlen)1503)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__1 = j - 1) < 130015 && 0 <= i__1 ? i__1 : 
		    s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1506)) << 5),
		     "#", &j, agents + (((i__2 = j - 1) < 130015 && 0 <= i__2 
		    ? i__2 : s_rnge("agents", i__2, "f_dwpool__", (ftnlen)
		    1506)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dwpool_(agents + (((i__1 = j - 1) < 130015 && 0 <= i__1 ? i__1 : 
		    s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1509)) << 5),
		     (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Re-set the watches. */

/*     This time, create a set of all agents. */

    scardc_(&c__0, agtset, (ftnlen)32);
    i__1 = nset;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = nvars__;
	for (j = 1; j <= i__2; ++j) {

/*           Each variable name includes I and J. */

	    s_copy(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1544)) << 5), 
		    "VARS#_#", (ftnlen)32, (ftnlen)7);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1545)) << 5), 
		    "#", &i__, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 
		    ? i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1545))
		     << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(vars__ + (((i__3 = j - 1) < 26003 && 0 <= i__3 ? i__3 : 
		    s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1548)) << 5), 
		    "#", &j, vars__ + (((i__4 = j - 1) < 26003 && 0 <= i__4 ? 
		    i__4 : s_rnge("vars", i__4, "f_dwpool__", (ftnlen)1548)) 
		    << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {

/*           Each agent name includes I and J. */

	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1558)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1559)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1559)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1562)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1562)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Set a watch for the Jth agent on the Ith */
/*           set of variables. */

	    swpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1568)) << 5),
		     &nvars__, vars__, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Add this agent to the agent set. */

	    insrtc_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1574)) << 5),
		     agtset, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Check update status. */

	i__2 = nagent;
	for (j = 1; j <= i__2; ++j) {
	    s_copy(qname, "(4) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1590)) << 5),
		     "AGENTS#_#", (ftnlen)32, (ftnlen)9);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1591)) << 5),
		     "#", &i__, agents + (((i__4 = j - 1) < 130015 && 0 <= 
		    i__4 ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (
		    ftnlen)1591)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1594)) << 5),
		     "#", &j, agents + (((i__4 = j - 1) < 130015 && 0 <= i__4 
		    ? i__4 : s_rnge("agents", i__4, "f_dwpool__", (ftnlen)
		    1594)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1597)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_true, ok, (ftnlen)80);

/*           Make sure a second test indicates "no update." */

	    s_copy(qname, "(5) Update # #", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    cvpool_(agents + (((i__3 = j - 1) < 130015 && 0 <= i__3 ? i__3 : 
		    s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1611)) << 5),
		     &update, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &update, &c_false, ok, (ftnlen)80);
	}
    }

/*     Delete watches in sorted order. */

    i__1 = cardc_(agtset, (ftnlen)32);
    for (i__ = 1; i__ <= i__1; ++i__) {
	dwpool_(agtset + (((i__2 = i__ + 5) < 130021 && 0 <= i__2 ? i__2 : 
		s_rnge("agtset", i__2, "f_dwpool__", (ftnlen)1626)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/*     Check the watch system status. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Try to delete watch with update pending", (ftnlen)39);
    s_copy(agent, "AGENT1", (ftnlen)32, (ftnlen)6);
    nvars__ = 1;
    s_copy(vars__, "VAR1", (ftnlen)32, (ftnlen)4);
    dpvals[0] = -999.;
    pdpool_(vars__, &c__1, dpvals, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    swpool_(agent, &nvars__, vars__, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the watch. */

    dwpool_(agent, (ftnlen)32);
    chckxc_(&c_true, "SPICE(UPDATEPENDING)", ok, (ftnlen)20);

/*     Clean up the watch. */

    cvpool_(agent, &update, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dwpool_(agent, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Determine the number of watches set outside of this test family.",
	     (ftnlen)64);
    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nused = cardc_(uwvars, (ftnlen)32);
    navail = 26003 - nused;

/*     Make sure we have enough watches available so the rest */
/*     of our tests make sense. The exact number of available */
/*     slots in the watched variable set depends not only on */
/*     the Toolkit version but the position of this test family */
/*     in the sequence of TSPICE tests. */

    chcksi_("NAVAIL", &navail, ">", &c__4000, &c__0, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* ---- Case ------------------------------------------------------------- */

    tcase_("Kernel variable set overflow: large variable set", (ftnlen)48);
    i__1 = navail + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1721)) << 5), 
		"VAR(#)", (ftnlen)32, (ftnlen)6);
	repmi_(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1722)) << 5), 
		"#", &i__, vars__ + (((i__3 = i__ - 1) < 26003 && 0 <= i__3 ? 
		i__3 : s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1722)) << 5)
		, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    }
    i__1 = navail + 1;
    swpool_("AGENT1", &i__1, vars__, (ftnlen)6, (ftnlen)32);
    chckxc_(&c_true, "SPICE(KERVARSETOVERFLOW)", ok, (ftnlen)24);

/*     We expect that no new watch was set. Check the watch */
/*     system. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Kernel variable set overflow: single variable", (ftnlen)45);
    i__1 = navail;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1751)) << 5), 
		"VAR(#)", (ftnlen)32, (ftnlen)6);
	repmi_(vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? i__2 : 
		s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1752)) << 5), 
		"#", &i__, vars__ + (((i__3 = i__ - 1) < 26003 && 0 <= i__3 ? 
		i__3 : s_rnge("vars", i__3, "f_dwpool__", (ftnlen)1752)) << 5)
		, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	swpool_("AGENT1", &c__1, vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= 
		i__2 ? i__2 : s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1754)
		) << 5), (ftnlen)6, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     The last straw. */

    swpool_("AGENT1", &c__1, "last straw", (ftnlen)6, (ftnlen)10);
    chckxc_(&c_true, "SPICE(KERVARSETOVERFLOW)", ok, (ftnlen)24);

/*     Clean up. */

    cvpool_("AGENT1", &update, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dwpool_("AGENT1", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the watch system. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Too many agents for one variable", (ftnlen)32);
    for (i__ = 1; i__ <= 1000; ++i__) {
	s_copy(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1797)) << 5), 
		"AGENTS(#)", (ftnlen)32, (ftnlen)9);
	repmi_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1798)) << 5), 
		"#", &i__, agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ?
		 i__2 : s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1798)) <<
		 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	swpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1800)) << 5), &
		c__1, "VAR1", (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     The last straw. */

    swpool_("last straw", &c__1, "VAR1", (ftnlen)10, (ftnlen)4);
    chckxc_(&c_true, "SPICE(TOOMANYWATCHES)", ok, (ftnlen)21);

/*     Clean up. */

    for (i__ = 1; i__ <= 1000; ++i__) {
	cvpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1816)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dwpool_(agents + (((i__1 = i__ - 1) < 130015 && 0 <= i__1 ? i__1 : 
		s_rnge("agents", i__1, "f_dwpool__", (ftnlen)1819)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Agent pool overflow", (ftnlen)19);

/*     Start out by finding out how much room is in */
/*     the agent list. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = lnknfn_(uwpool) / 1000;
    for (i__ = 1; i__ <= 1000; ++i__) {
	s_copy(vars__ + (((i__1 = i__ - 1) < 26003 && 0 <= i__1 ? i__1 : 
		s_rnge("vars", i__1, "f_dwpool__", (ftnlen)1856)) << 5), 
		"VARS(#)", (ftnlen)32, (ftnlen)7);
	repmi_(vars__ + (((i__1 = i__ - 1) < 26003 && 0 <= i__1 ? i__1 : 
		s_rnge("vars", i__1, "f_dwpool__", (ftnlen)1857)) << 5), 
		"#", &i__, vars__ + (((i__2 = i__ - 1) < 26003 && 0 <= i__2 ? 
		i__2 : s_rnge("vars", i__2, "f_dwpool__", (ftnlen)1857)) << 5)
		, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1864)) << 5), 
		"AGENTS(#)", (ftnlen)32, (ftnlen)9);
	repmi_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1865)) << 5), 
		"#", &i__, agents + (((i__3 = i__ - 1) < 130015 && 0 <= i__3 ?
		 i__3 : s_rnge("agents", i__3, "f_dwpool__", (ftnlen)1865)) <<
		 5), (ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	swpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1868)) << 5), &
		c__1000, vars__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     The last straw. */

    swpool_("last straw", &c__1000, vars__, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(AGENTLISTOVERFLOW)", ok, (ftnlen)24);

/*     Clean up. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	cvpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1884)) << 5), &
		update, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dwpool_(agents + (((i__2 = i__ - 1) < 130015 && 0 <= i__2 ? i__2 : 
		s_rnge("agents", i__2, "f_dwpool__", (ftnlen)1887)) << 5), (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the watch system. */

    zzvupool_(uwvars, uwptrs, uwpool, uwagnt, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckac_("UWVARS (1)", uwvars + 192, "=", okvset + 192, &onvars, ok, (
	    ftnlen)10, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    n = 130015 - lnknfn_(uwpool);
    chcksi_("N", &n, "=", &onagnt, &c__0, ok, (ftnlen)1, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_dwpool__ */

