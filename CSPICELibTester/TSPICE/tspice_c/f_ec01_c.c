/*

-Procedure f_ec01_c ( Test wrappers for elements routines )


-Abstract

   Perform tests on CSPICE wrappers for elements conversion functions.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/

   #include <math.h>
   #include "SpiceUsr.h"
   #include "tutils_c.h"

   void f_ec01_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   None.

-Files

   None.

-Particulars

   This routine tests the wrappers for orbit elements conversion
   routines. The current set is:

      conics_c
      oscelts_c
      oscltx_c

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   N.J  Bachman   (JPL)
   E.D. Wright    (JPL)

-Version

   -tspice_c Version 2.0.0 26-JAN-2017 (NJB) (EDW)

   -tspice_c Version 1.0.0 22-JUL-1999 (EDW)

-Index_Entries

   test osculating element routines

-&
*/

{ /* Begin f_ec01_c */

   #define  TOL               1.e-10

   /*
   Local variables
   */
   #define INUSE           11

   /*
   Local variables
   */
   SpiceDouble             ecc;
   SpiceDouble             h      [3];
   SpiceDouble             h2;
   SpiceDouble             m0;
   SpiceDouble             nu;
   SpiceDouble             p;
   SpiceDouble             r;
   SpiceDouble             rp;
   SpiceDouble             vtest8 [8];
   SpiceDouble             vout8  [8];
   SpiceDouble             voutx  [SPICE_OSCLTX_NELTS];
   SpiceDouble             vout6  [6];
   SpiceDouble             xa;
   SpiceDouble             xr;
   SpiceDouble             xtau;

   static SpiceDouble      mu = 398600.1;
   static SpiceDouble      et = -83241202.9223976;

   SpiceInt                i;


   /* An abritrary Geosynch vehicle, TDRS 4, J2000 */

   static SpiceDouble               state  [6] =
           { -36048.15231483, -21884.39544710,  336.39689817,
              1.59510580    , -2.62787910    , -0.01687365  };


   topen_c ( "f_ec01_c" );


   /*-oscelt_c */

   tcase_c ( "Elements conversion tests - oscelt_c" );

   vtest8[0] = 42160.755416;
   vtest8[1] = 1.3892359522e-004;
   vtest8[2] = 9.6830217480e-003;
   vtest8[3] = 1.5136684133;
   vtest8[4] = 5.4966592305;
   vtest8[5] = 2.9599931171;
   vtest8[6] = et;
   vtest8[7] = mu;
   oscelt_c ( state, et, mu, vout8 );
   chckxc_c ( SPICEFALSE, " ", ok );
   chckad_c ( "vout8/oscelt_c", vout8, "~/", vtest8, 8, TOL, ok );


   /*
   conics_c should reverse the action of oscelt, so we'll use
   the oscelt_c output as conics_c input.
   */

   /*-conics_c */
   tcase_c ("Elements conversion tests - conics_c" );
   conics_c ( vout8, et, vout6 );
   chckxc_c ( SPICEFALSE, " ", ok );
   chckad_c ( "vout6/conics_c", vout6, "~/", state, 6, TOL, ok );


   /*
   Repeat test using oscltx_c: 
   */

   tcase_c ("Elements conversion tests - oscltx_c" );

   oscltx_c ( state, et, mu, voutx );
   chckxc_c ( SPICEFALSE, " ", ok );
   chckad_c ( "vout8/oscelt_c", voutx, "~/", vtest8, 8, TOL, ok );

   /*
   Check the values of A and tau returned by oscltx_c. 
   */
   rp   = voutx[0];
   ecc  = voutx[1];
   
   xa   = rp / (1.0 - ecc);

   chcksd_c ( "A", voutx[9], "~/", xa, TOL, ok );



   xtau = 2*pi_c() * sqrt( pow(xa,3) / mu );

   chcksd_c ( "tau", voutx[10], "~/", xtau, TOL, ok );


   /*
   Check the value of nu returned by oscltx_c. 
   */
   nu = voutx[8];

   xr = vnorm_c( state );

   vcrss_c ( state, state+3, h );

   h2 = vdot_c( h, h );

   p  = h2 / mu;

   r  = p / (  1.0  +  ( ecc * cos(nu))  );


   chcksd_c ( "r from nu", r, "~/", xr, TOL, ok );

   /*
   The signs of nu and m0 should match. 
   */
   m0 = voutx[5];

   chcksd_c ( "nu/m0", nu/m0, ">", 0.0, 0.0, ok );
      


   /*
   Make sure the output array from oscltx_c is zero-padded 
   beyond the indices currently in use.
   */
   for ( i = INUSE;  i < SPICE_OSCLTX_NELTS;  i++ )
   {
      chcksd_c ( "pad", voutx[i], "=", 0.0, 0.0, ok );
   }


   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );

} /* End f_ec01_c */


