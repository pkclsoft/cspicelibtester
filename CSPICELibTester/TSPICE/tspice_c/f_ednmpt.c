/* f_ednmpt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 1.;
static integer c__14 = 14;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b40 = 0.;
static logical c_true = TRUE_;

/* $Procedure F_EDNMPT ( EDNMPT tests ) */
/* Subroutine */ int f_ednmpt__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static doublereal dlat, dlon, a, b, c__;
    static integer i__, j;
    static doublereal p[3], r__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    static char title[160];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nstep;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), latrec_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), ednmpt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal normal[3];
    extern /* Subroutine */ int surfnm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal xnorml[3], lat, lon, tol;

/* $ Abstract */

/*     Exercise the SPICELIB routine EDNMPT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine EDNMPT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 17-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_EDNMPT", (ftnlen)8);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere tests", (ftnlen)12);
    r__ = 7.;
    a = r__;
    b = r__;
    c__ = r__;
    nstep = 10;
    dlon = pi_() * 2 / nstep;
    dlat = pi_() / nstep;
    i__1 = nstep;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = i__ * dlon;
	i__2 = nstep;
	for (j = 0; j <= i__2; ++j) {
	    lat = pi_() / 2 - j * dlat;
	    latrec_(&c_b4, &lon, &lat, xnorml);

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Sphere: normal = (#,#,#)", (ftnlen)160, (ftnlen)24)
		    ;
	    repmd_(title, "#", xnorml, &c__14, title, (ftnlen)160, (ftnlen)1, 
		    (ftnlen)160);
	    repmd_(title, "#", &xnorml[1], &c__14, title, (ftnlen)160, (
		    ftnlen)1, (ftnlen)160);
	    repmd_(title, "#", &xnorml[2], &c__14, title, (ftnlen)160, (
		    ftnlen)1, (ftnlen)160);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)160);
	    ednmpt_(&a, &b, &c__, xnorml, p);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check that we can recover the input normal */
/*           from P. */

	    surfnm_(&a, &b, &c__, p, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-14;
	    chckad_("NORMAL", normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)
		    6, (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial ellipsoid tests", (ftnlen)24);
    a = 100.;
    b = 50.;
    c__ = 3.;
    nstep = 10;
    dlon = pi_() * 2 / nstep;
    dlat = pi_() / nstep;
    i__1 = nstep;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = i__ * dlon;
	i__2 = nstep;
	for (j = 0; j <= i__2; ++j) {
	    lat = pi_() / 2 - j * dlat;
	    latrec_(&c_b4, &lon, &lat, xnorml);

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Ellipsoid: normal = (#,#,#)", (ftnlen)160, (ftnlen)
		    27);
	    repmd_(title, "#", xnorml, &c__14, title, (ftnlen)160, (ftnlen)1, 
		    (ftnlen)160);
	    repmd_(title, "#", &xnorml[1], &c__14, title, (ftnlen)160, (
		    ftnlen)1, (ftnlen)160);
	    repmd_(title, "#", &xnorml[2], &c__14, title, (ftnlen)160, (
		    ftnlen)1, (ftnlen)160);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)160);
	    ednmpt_(&a, &b, &c__, xnorml, p);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check that we can recover the input normal */
/*           from P. */

	    surfnm_(&a, &b, &c__, p, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-14;
	    chckad_("NORMAL", normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)
		    6, (ftnlen)3);
	}
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid semi-axis lengths.", (ftnlen)26);
    a = 100.;
    b = 50.;
    c__ = 3.;
    vpack_(&c_b40, &c_b40, &c_b4, xnorml);
    d__1 = -a;
    ednmpt_(&d__1, &b, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    d__1 = -b;
    ednmpt_(&a, &d__1, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    d__1 = -c__;
    ednmpt_(&a, &b, &d__1, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&c_b40, &b, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&a, &c_b40, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&a, &b, &c_b40, xnorml, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid semi-axis length after scaling.", (ftnlen)39);
    a = 1e300;
    b = 1e-300;
    c__ = 3.;
    vpack_(&c_b40, &c_b40, &c_b4, xnorml);
    ednmpt_(&a, &b, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(AXISUNDERFLOW)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Zero normal vector.", (ftnlen)19);
    a = 1.;
    b = a;
    c__ = a;
    cleard_(&c__3, xnorml);
    ednmpt_(&a, &b, &c__, xnorml, p);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */


/*     The degenerate case error resulting from a non-positive */
/*     reciprocal of the square of lambda should never occur. */
/*     A disposable version of the code should be used to */
/*     exercise the error handling for this case. */


/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_ednmpt__ */

