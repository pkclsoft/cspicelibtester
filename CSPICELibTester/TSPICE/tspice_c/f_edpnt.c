/* f_edpnt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 1.;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b9 = 10.;
static integer c__14 = 14;
static integer c__3 = 3;
static doublereal c_b25 = 0.;

/* $Procedure F_EDPNT ( EDPNT tests ) */
/* Subroutine */ int f_edpnt__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static doublereal dlat, dlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal a, b, c__;
    static integer i__, j, k;
    static doublereal p[3], scale;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen), edpnt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static logical found;
    static char title[160];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nstep;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    static doublereal dscale, sp[3];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer nscale;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), latrec_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal minscl, maxscl;
    extern /* Subroutine */ int ednmpt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), surfpt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *);
    static doublereal lat, lon, tol, xpt[3];

/* $ Abstract */

/*     Exercise the SPICELIB routine EDPNT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine EDPNT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 17-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_EDPNT", (ftnlen)7);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial ellipsoid tests", (ftnlen)24);
    a = 100.;
    b = 50.;
    c__ = 3.;
    nstep = 10;
    dlon = pi_() * 2 / nstep;
    dlat = pi_() / nstep;
    nscale = 20;
    minscl = -100.;
    maxscl = 100.;
    dscale = (maxscl - minscl) / nscale;
    i__1 = nstep;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = i__ * dlon;
	i__2 = nstep;
	for (j = 0; j <= i__2; ++j) {
	    lat = pi_() / 2 - j * dlat;
	    latrec_(&c_b4, &lon, &lat, p);
	    surfpt_(origin, p, &a, &b, &c__, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect that a result will always be found. */

	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    i__3 = nscale;
	    for (k = 0; k <= i__3; ++k) {
		d__1 = minscl + k * dscale;
		scale = pow_dd(&c_b9, &d__1);
		vscl_(&scale, xpt, sp);

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Ellipsoid: point = (#,#,#)", (ftnlen)160, (
			ftnlen)26);
		repmd_(title, "#", sp, &c__14, title, (ftnlen)160, (ftnlen)1, 
			(ftnlen)160);
		repmd_(title, "#", &sp[1], &c__14, title, (ftnlen)160, (
			ftnlen)1, (ftnlen)160);
		repmd_(title, "#", &sp[2], &c__14, title, (ftnlen)160, (
			ftnlen)1, (ftnlen)160);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)160);
		edpnt_(sp, &a, &b, &c__, p);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check P. */

		tol = 1e-14;
		chckad_("P", p, "~~/", xpt, &c__3, &tol, ok, (ftnlen)1, (
			ftnlen)3);
	    }
	}
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid semi-axis lengths.", (ftnlen)26);
    a = 100.;
    b = 50.;
    c__ = 3.;
    vpack_(&c_b25, &c_b25, &c_b4, sp);
    d__1 = -a;
    ednmpt_(&d__1, &b, &c__, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    d__1 = -b;
    ednmpt_(&a, &d__1, &c__, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    d__1 = -c__;
    ednmpt_(&a, &b, &d__1, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&c_b25, &b, &c__, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&a, &c_b25, &c__, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    ednmpt_(&a, &b, &c_b25, sp, p);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid semi-axis length after scaling.", (ftnlen)39);
    a = 1e300;
    b = 1e-300;
    c__ = 3.;
    vpack_(&c_b25, &c_b25, &c_b4, sp);
    ednmpt_(&a, &b, &c__, sp, p);
    chckxc_(&c_true, "SPICE(AXISUNDERFLOW)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */


/*     The degenerate case error resulting from a non-positive */
/*     reciprocal of the square of lambda should never occur. */
/*     A disposable version of the code should be used to */
/*     exercise the error handling for this case. */


/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_edpnt__ */

