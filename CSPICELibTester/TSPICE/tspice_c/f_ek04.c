/* f_ek04.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__20 = 20;
static logical c_true = TRUE_;
static integer c__5 = 5;
static integer c__3 = 3;
static integer c__7 = 7;
static integer c__8 = 8;
static integer c__9 = 9;
static integer c__10 = 10;

/* $Procedure F_EK04 ( EK tests, subset 4 ) */
/* Subroutine */ int f_ek04__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static char cval[2048];
    static doublereal dval;
    static integer ival;
    static logical null;
    extern /* Subroutine */ int zzekcdsc_(integer *, integer *, char *, 
	    integer *, ftnlen), zzeksdsc_(integer *, integer *, integer *);
    extern logical zzekscmp_(integer *, integer *, integer *, integer *, 
	    integer *, integer *, integer *, char *, doublereal *, integer *, 
	    logical *, ftnlen);
    extern /* Subroutine */ int zzektrdp_(integer *, integer *, integer *, 
	    integer *);
    static integer i__;
    static char cdata[2048*20];
    static doublereal ddata[20];
    static integer idata[20];
    extern /* Subroutine */ int eklef_(char *, integer *, ftnlen);
    static char decls[200*100];
    static logical match;
    static doublereal tdata[20];
    extern /* Subroutine */ int tcase_(char *, ftnlen), filli_(integer *, 
	    integer *, integer *), ekcls_(integer *), ekuef_(integer *);
    static integer segno;
    extern /* Subroutine */ int ekopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    static integer ncols, dtype;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nrows, cldsc1[11];
    extern /* Subroutine */ int t_success__(logical *);
    static integer cldsc3[11], cldsc4[11], cldsc5[11];
    extern /* Subroutine */ int ekaclc_(integer *, integer *, char *, char *, 
	    integer *, logical *, integer *, integer *, ftnlen, ftnlen), 
	    ekacld_(integer *, integer *, char *, doublereal *, integer *, 
	    logical *, integer *, integer *, ftnlen), ekacli_(integer *, 
	    integer *, char *, integer *, integer *, logical *, integer *, 
	    integer *, ftnlen);
    static integer handle;
    extern /* Subroutine */ int ekifld_(integer *, char *, integer *, integer 
	    *, char *, char *, integer *, integer *, ftnlen, ftnlen, ftnlen), 
	    ekffld_(integer *, integer *, integer *);
    static char tabnam[64];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char cnames[32*100];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static integer segdsc[24];
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static integer eltidx, entszs[20], rcptrs[20], recptr, wkindx[20];
    static logical nlflgs[20];
    static integer row;

/* $ Abstract */

/*     Exercise the SPICELIB EK routines, subset 4. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Column Attribute Descriptor Parameters */

/*        ekattdsc.inc Version 1    23-AUG-1995 (NJB) */


/*     This include file declares parameters used in EK column */
/*     attribute descriptors.  Column attribute descriptors are */
/*     a simplified version of column descriptors:  attribute */
/*     descriptors describe attributes of a column but do not contain */
/*     addresses or pointers. */


/*     Size of column attribute descriptor */


/*     Indices of various pieces of attribute descriptors: */


/*     ATTSIZ is the index of the column's class code.  (We use the */
/*     word `class' to distinguish this item from the column's data */
/*     type.) */


/*     ATTTYP is the index of the column's data type code (CHR, INT, DP, */
/*     or TIME).  The type is actually implied by the class, but it */
/*     will frequently be convenient to look up the type directly. */



/*     ATTLEN is the index of the column's string length value, if the */
/*     column has character type.  A value of IFALSE in this element of */
/*     the descriptor indicates that the strings have variable length. */


/*     ATTSIZ is the index of the column's element size value.  This */
/*     descriptor element is meaningful for columns with fixed-size */
/*     entries.  For variable-sized columns, this value is IFALSE. */


/*     ATTIDX is the location of a flag that indicates whether the column */
/*     is indexed.  The flag takes the value ITRUE if the column is */
/*     indexed and otherwise takes the value IFALSE. */


/*     ATTNFL is the index of a flag indicating whether nulls are */
/*     permitted in the column.  The value at location NFLIDX is */
/*     ITRUE if nulls are permitted and IFALSE otherwise. */


/*     End Include Section:  EK Column Attribute Descriptor Parameters */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Boolean Enumerated Type */


/*        ekbool.inc Version 1   21-DEC-1994 (NJB) */


/*     Within the EK system, boolean values sometimes must be */
/*     represented by integer or character codes.  The codes and their */
/*     meanings are listed below. */

/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     Character code indicating `true': */


/*     Character code indicating `false': */


/*     End Include Section:  EK Boolean Enumerated Type */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Column Descriptor Parameters */

/*        ekcoldsc.inc Version 6    23-AUG-1995 (NJB) */


/*     Note:  The column descriptor size parameter CDSCSZ  is */
/*     declared separately in the include section CDSIZE$INC.FOR. */

/*     Offset of column descriptors, relative to start of segment */
/*     integer address range.  This number, when added to the last */
/*     integer address preceding the segment, yields the DAS integer */
/*     base address of the first column descriptor.  Currently, this */
/*     offset is exactly the size of a segment descriptor.  The */
/*     parameter SDSCSZ, which defines the size of a segment descriptor, */
/*     is declared in the include file eksegdsc.inc. */


/*     Size of column descriptor */


/*     Indices of various pieces of column descriptors: */


/*     CLSIDX is the index of the column's class code.  (We use the */
/*     word `class' to distinguish this item from the column's data */
/*     type.) */


/*     TYPIDX is the index of the column's data type code (CHR, INT, DP, */
/*     or TIME).  The type is actually implied by the class, but it */
/*     will frequently be convenient to look up the type directly. */



/*     LENIDX is the index of the column's string length value, if the */
/*     column has character type.  A value of IFALSE in this element of */
/*     the descriptor indicates that the strings have variable length. */


/*     SIZIDX is the index of the column's element size value.  This */
/*     descriptor element is meaningful for columns with fixed-size */
/*     entries.  For variable-sized columns, this value is IFALSE. */


/*     NAMIDX is the index of the base address of the column's name. */


/*     IXTIDX is the data type of the column's index.  IXTIDX */
/*     contains a type value only if the column is indexed. For columns */
/*     that are not indexed, the location IXTIDX contains the boolean */
/*     value IFALSE. */


/*     IXPIDX is a pointer to the column's index.  IXTPDX contains a */
/*     meaningful value only if the column is indexed.  The */
/*     interpretation of the pointer depends on the data type of the */
/*     index. */


/*     NFLIDX is the index of a flag indicating whether nulls are */
/*     permitted in the column.  The value at location NFLIDX is */
/*     ITRUE if nulls are permitted and IFALSE otherwise. */


/*     ORDIDX is the index of the column's ordinal position in the */
/*     list of columns belonging to the column's parent segment. */


/*     METIDX is the index of the column's integer metadata pointer. */
/*     This pointer is a DAS integer address. */


/*     The last position in the column descriptor is reserved.  No */
/*     parameter is defined to point to this location. */


/*     End Include Section:  EK Column Descriptor Parameters */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Column Name Size */

/*        ekcnamsz.inc Version 1    17-JAN-1995 (NJB) */


/*     Size of column name, in characters. */


/*     End Include Section:  EK Column Name Size */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Operator Codes */

/*        ekopcd.inc  Version 1  30-DEC-1994 (NJB) */


/*     Within the EK system, operators used in EK queries are */
/*     represented by integer codes.  The codes and their meanings are */
/*     listed below. */

/*     Relational expressions in EK queries have the form */

/*        <column name> <operator> <value> */

/*     For columns containing numeric values, the operators */

/*        EQ,  GE,  GT,  LE,  LT,  NE */

/*     may be used; these operators have the same meanings as their */
/*     Fortran counterparts.  For columns containing character values, */
/*     the list of allowed operators includes those in the above list, */
/*     and in addition includes the operators */

/*        LIKE,  UNLIKE */

/*     which are used to compare strings to a template.  In the character */
/*     case, the meanings of the parameters */

/*        GE,  GT,  LE,  LT */

/*     match those of the Fortran lexical functions */

/*        LGE, LGT, LLE, LLT */


/*     The additional unary operators */

/*        ISNULL, NOTNUL */

/*     are used to test whether a value of any type is null. */



/*     End Include Section:  EK Operator Codes */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK General Limit Parameters */

/*        ekglimit.inc  Version 1    21-MAY-1995 (NJB) */


/*     This file contains general limits for the EK system. */

/*     MXCLSG is the maximum number of columns allowed in a segment. */
/*     This limit applies to logical tables as well, since all segments */
/*     in a logical table must have the same column definitions. */


/*     End Include Section:  EK General Limit Parameters */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Query Limit Parameters */

/*        ekqlimit.inc  Version 3    16-NOV-1995 (NJB) */

/*           Parameter MAXCON increased to 1000. */

/*        ekqlimit.inc  Version 2    01-AUG-1995 (NJB) */

/*           Updated to support SELECT clause. */


/*        ekqlimit.inc  Version 1    07-FEB-1995 (NJB) */


/*     These limits apply to character string queries input to the */
/*     EK scanner.  This limits are part of the EK system's user */
/*     interface:  the values should be advertised in the EK required */
/*     reading document. */


/*     Maximum length of an input query:  MAXQRY.  This value is */
/*     currently set to twenty-five 80-character lines. */


/*     Maximum number of columns that may be listed in the */
/*     `order-by clause' of a query:  MAXSEL.  MAXSEL = 50. */


/*     Maximum number of tables that may be listed in the `FROM */
/*     clause' of a query: MAXTAB. */


/*     Maximum number of relational expressions that may be listed */
/*     in the `constraint clause' of a query: MAXCON. */

/*     This limit applies to a query when it is represented in */
/*     `normalized form': that is, the constraints have been */
/*     expressed as a disjunction of conjunctions of relational */
/*     expressions. The number of relational expressions in a query */
/*     that has been expanded in this fashion may be greater than */
/*     the number of relations in the query as orginally written. */
/*     For example, the expression */

/*             ( ( A LT 1 ) OR ( B GT 2 ) ) */
/*        AND */
/*             ( ( C NE 3 ) OR ( D EQ 4 ) ) */

/*     which contains 4 relational expressions, expands to the */
/*     equivalent normalized constraint */

/*             (  ( A LT 1 ) AND ( C NE 3 )  ) */
/*        OR */
/*             (  ( A LT 1 ) AND ( D EQ 4 )  ) */
/*        OR */
/*             (  ( B GT 2 ) AND ( C NE 3 )  ) */
/*        OR */
/*             (  ( B GT 2 ) AND ( D EQ 4 )  ) */

/*     which contains eight relational expressions. */



/*     MXJOIN is the maximum number of tables that can be joined. */


/*     MXJCON is the maximum number of join constraints allowed. */


/*     Maximum number of order-by columns that may be used in the */
/*     `order-by clause' of a query: MAXORD. MAXORD = 10. */


/*     Maximum number of tokens in a query: 500. Tokens are reserved */
/*     words, column names, parentheses, and values. Literal strings */
/*     and time values count as single tokens. */


/*     Maximum number of numeric tokens in a query: */


/*     Maximum total length of character tokens in a query: */


/*     Maximum length of literal string values allowed in queries: */
/*     MAXSTR. */


/*     End Include Section:  EK Query Limit Parameters */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Segment Descriptor Parameters */

/*        eksegdsc.inc  Version 8  06-NOV-1995 (NJB) */


/*     All `base addresses' referred to below are the addresses */
/*     *preceding* the item the base applies to.  This convention */
/*     enables simplied address calculations in many cases. */

/*     Size of segment descriptor.  Note:  the include file ekcoldsc.inc */
/*     must be updated if this parameter is changed.  The parameter */
/*     CDOFF in that file should be kept equal to SDSCSZ. */


/*     Index of the segment type code: */


/*     Index of the segment's number.  This number is the segment's */
/*     index in the list of segments contained in the EK to which */
/*     the segment belongs. */


/*     Index of the DAS integer base address of the segment's integer */
/*     meta-data: */


/*     Index of the DAS character base address of the table name: */


/*     Index of the segment's column count: */


/*     Index of the segment's record count: */


/*     Index of the root page number of the record tree: */


/*     Index of the root page number of the character data page tree: */


/*     Index of the root page number of the double precision data page */
/*     tree: */


/*     Index of the root page number of the integer data page tree: */


/*     Index of the `modified' flag: */


/*     Index of the `initialized' flag: */


/*     Index of the shadowing flag: */


/*     Index of the companion file handle: */


/*     Index of the companion segment number: */


/*     The next three items are, respectively, the page numbers of the */
/*     last character, d.p., and integer data pages allocated by the */
/*     segment: */


/*     The next three items are, respectively, the page-relative */
/*     indices of the last DAS word in use in the segment's */
/*     last character, d.p., and integer data pages: */


/*     Index of the DAS character base address of the column name list: */


/*     The last descriptor element is reserved for future use.  No */
/*     parameter is defined to point to this location. */


/*     End Include Section:  EK Segment Descriptor Parameters */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Table Name Size */

/*        ektnamsz.inc Version 1    17-JAN-1995 (NJB) */


/*     Size of table name, in characters. */


/*     End Include Section:  EK Table Name Size */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  EK Data Types */

/*        ektype.inc Version 1  27-DEC-1994 (NJB) */


/*     Within the EK system, data types of EK column contents are */
/*     represented by integer codes.  The codes and their meanings */
/*     are listed below. */

/*     Integer codes are also used within the DAS system to indicate */
/*     data types; the EK system makes no assumptions about compatibility */
/*     between the codes used here and those used in the DAS system. */


/*     Character type: */


/*     Double precision type: */


/*     Integer type: */


/*     `Time' type: */

/*     Within the EK system, time values are represented as ephemeris */
/*     seconds past J2000 (TDB), and double precision numbers are used */
/*     to store these values.  However, since time values require special */
/*     treatment both on input and output, and since the `TIME' column */
/*     has a special role in the EK specification and code, time values */
/*     are identified as a type distinct from double precision numbers. */


/*     End Include Section:  EK Data Types */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include File:  SPICELIB Error Handling Parameters */

/*        errhnd.inc  Version 2    18-JUN-1997 (WLT) */

/*           The size of the long error message was */
/*           reduced from 25*80 to 23*80 so that it */
/*           will be accepted by the Microsoft Power Station */
/*           FORTRAN compiler which has an upper bound */
/*           of 1900 for the length of a character string. */

/*        errhnd.inc  Version 1    29-JUL-1997 (NJB) */



/*     Maximum length of the long error message: */


/*     Maximum length of the short error message: */


/*     End Include File:  SPICELIB Error Handling Parameters */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the EK query constraint checking routine */

/*        ZZEKSCMP */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     This test family must be extended to comprehensively test its */
/*     target source code. */

/*     Full tests must include */

/*        Test every combination of column type and data type. */
/*        Test every combination of null and null-null data. */
/*        Test use of vector column entries. */
/*        Test use of segments from different files. */


/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 29-MAY-2010 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local Parameters */

/*      CHARACTER*(*)         EK2 */
/*      PARAMETER           ( EK2    = 'test2.ek' ) */

/*     Local Variables */


/*     Saved variables */


/*     To avoid portability problems, make all data static. */


/*     Open the test family. */

    topen_("F_EK04", (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create a custom EK.", (ftnlen)27);

/*     We need an EK having columns containing scalar strings */
/*     longer than MAXSTR. */

/*     Open a new EK. */

    ekopn_("test1.ek", "test1.ek", &c__0, &handle, (ftnlen)8, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Since many EK routines don't return upon entry, */
/*     exit here if we couldn't open the file. */

    if (! (*ok)) {
	t_success__(ok);
	return 0;
    }

/*     Start building a segment by creating a schema for an EK table. */

    s_copy(tabnam, "TAB1", (ftnlen)64, (ftnlen)4);
    ncols = 5;
    nrows = 5;
    s_copy(cnames, "CCOL1", (ftnlen)32, (ftnlen)5);
    s_copy(cnames + 32, "CCOL2", (ftnlen)32, (ftnlen)5);
    s_copy(cnames + 64, "ICOL1", (ftnlen)32, (ftnlen)5);
    s_copy(cnames + 96, "DCOL1", (ftnlen)32, (ftnlen)5);
    s_copy(cnames + 128, "TCOL1", (ftnlen)32, (ftnlen)5);
    s_copy(decls, "DATATYPE = CHARACTER*(*), INDEXED  = TRUE, NULLS_OK = TRUE"
	    , (ftnlen)200, (ftnlen)58);
    s_copy(decls + 200, decls, (ftnlen)200, (ftnlen)200);
    s_copy(decls + 400, "DATATYPE = INTEGER, INDEXED  = TRUE, NULLS_OK = TRUE"
	    , (ftnlen)200, (ftnlen)52);
    s_copy(decls + 600, "DATATYPE = DOUBLE PRECISION, INDEXED  = TRUE, NULLS"
	    "_OK = TRUE", (ftnlen)200, (ftnlen)61);
    s_copy(decls + 800, "DATATYPE = TIME, INDEXED  = TRUE, NULLS_OK = TRUE", (
	    ftnlen)200, (ftnlen)49);

/*     Initiate a fast load. */

    ekifld_(&handle, tabnam, &ncols, &nrows, cnames, decls, &segno, rcptrs, (
	    ftnlen)64, (ftnlen)32, (ftnlen)200);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the data for the first column. We need strings */
/*     longer than MAXSTR, and we want both strings that */
/*     are identical up to their first MAXSTR characters, and */
/*     strings that differ in their first MAXSTR characters. */

    i__1 = nrows;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(cdata + (((i__2 = i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge(
		"cdata", i__2, "f_ek04__", (ftnlen)288)) << 11), " ", (ftnlen)
		2048, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 1023; ++i__) {
	*(unsigned char *)&cdata[i__ - 1] = 'X';
	*(unsigned char *)&cdata[i__ + 2047] = 'X';
	*(unsigned char *)&cdata[i__ + 4095] = 'X';
	*(unsigned char *)&cdata[i__ + 6143] = 'X';
    }
    s_copy(cdata + 1023, "ZAB", (ftnlen)3, (ftnlen)3);
    s_copy(cdata + 3071, "ZAC", (ftnlen)3, (ftnlen)3);
    s_copy(cdata + 5119, "WAB", (ftnlen)3, (ftnlen)3);
    s_copy(cdata + 7167, "VAB", (ftnlen)3, (ftnlen)3);
    s_copy(cdata + 9215, "VAB", (ftnlen)3, (ftnlen)3);

/*     Add the first column to the segment. */

    filli_(&c__1, &c__20, entszs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 20; ++i__) {
	nlflgs[(i__1 = i__ - 1) < 20 && 0 <= i__1 ? i__1 : s_rnge("nlflgs", 
		i__1, "f_ek04__", (ftnlen)313)] = FALSE_;
    }

/*     Make the last entry null. */

    nlflgs[(i__1 = nrows - 1) < 20 && 0 <= i__1 ? i__1 : s_rnge("nlflgs", 
	    i__1, "f_ek04__", (ftnlen)319)] = TRUE_;
    ekaclc_(&handle, &segno, cnames, cdata, entszs, nlflgs, rcptrs, wkindx, (
	    ftnlen)32, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Give the second column the same data as the first. */

    ekaclc_(&handle, &segno, cnames + 32, cdata, entszs, nlflgs, rcptrs, 
	    wkindx, (ftnlen)32, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create data for and load the integer column. */

    i__1 = nrows;
    for (i__ = 1; i__ <= i__1; ++i__) {
	idata[(i__2 = i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("idata", 
		i__2, "f_ek04__", (ftnlen)338)] = i__ * 100;
    }
    ekacli_(&handle, &segno, cnames + 64, idata, entszs, nlflgs, rcptrs, 
	    wkindx, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create data for and load the d.p. column. */

    i__1 = nrows;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ddata[(i__2 = i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("ddata", 
		i__2, "f_ek04__", (ftnlen)350)] = (doublereal) (i__ * 100);
    }
    ekacld_(&handle, &segno, cnames + 96, ddata, entszs, nlflgs, rcptrs, 
	    wkindx, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create data for and load the time column. */

    i__1 = nrows;
    for (i__ = 1; i__ <= i__1; ++i__) {
	tdata[(i__2 = i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("tdata", 
		i__2, "f_ek04__", (ftnlen)362)] = i__ * 86400.;
    }
    ekacld_(&handle, &segno, cnames + 128, tdata, entszs, nlflgs, rcptrs, 
	    wkindx, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now that we've finished adding columns to the segment, */
/*     finish the load. */

    ekffld_(&handle, &segno, rcptrs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the EK. */

    ekcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the EK for read access. We'll use the handle, so */
/*     it's easier to use the load entry point from the query */
/*     manager. */

    eklef_("test1.ek", &handle, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare strings that are identical up to length MAXSTR."
	    , (ftnlen)65);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata + 2048, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = FALSE_;
    row = 1;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare a string element than is less than a given sca"
	    "lar value.", (ftnlen)74);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = FALSE_;
    row = 4;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 4. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XV", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XV", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare a string element than is greater than a given "
	    "scalar value.", (ftnlen)77);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata + 6144, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = FALSE_;
    row = 1;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_true, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare a non-null string element to a null value.", (
	    ftnlen)60);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata + 6144, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = TRUE_;
    row = 1;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare a null string element to a null value.", (
	    ftnlen)56);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata + 8192, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = TRUE_;
    row = 5;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 5. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_true, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_false, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on long strings. */

    tcase_("ZZEKSCMP: compare a null string element to a non-null value.", (
	    ftnlen)60);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the first column. */

    zzekcdsc_(&handle, segdsc, cnames, cldsc1, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the second entry. */
/*     Since these entries agree up to the first MAXSTR characters, */
/*     we expect equality. */

    eltidx = 1;
    dtype = 1;
    s_copy(cval, cdata + 6144, (ftnlen)2048, (ftnlen)2048);
    dval = 0.;
    ival = 0;
    null = FALSE_;
    row = 5;

/*     For class three columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD03 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 5. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__7, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)14);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*X", &dval, &ival, &null, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (0)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__8, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, "*XZ", &dval, &ival, &null, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOT LIKE MATCH (1)", &match, &c_false, ok, (ftnlen)18);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_true, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc1, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_false, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on integers. */

    tcase_("ZZEKSCMP: compare matching integers.", (ftnlen)36);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the third column. */

    zzekcdsc_(&handle, segdsc, cnames + 64, cldsc3, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 1 to the first entry. */
/*     We expect equality. */

    eltidx = 1;
    dtype = 3;
    s_copy(cval, " ", (ftnlen)2048, (ftnlen)1);
    dval = 0.;
    ival = idata[0];
    null = FALSE_;
    row = 1;

/*     For class one columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD01 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc3, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc3, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc3, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc3, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc3, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on d.p numbers. */

    tcase_("ZZEKSCMP: compare matching d.p. numbers.", (ftnlen)40);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the fourth column. */

    zzekcdsc_(&handle, segdsc, cnames + 96, cldsc4, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 4 to the first entry. */
/*     We expect equality. */

    eltidx = 1;
    dtype = 2;
    s_copy(cval, " ", (ftnlen)2048, (ftnlen)1);
    dval = ddata[0];
    ival = 0;
    null = FALSE_;
    row = 1;

/*     For class two columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD02 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc4, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc4, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc4, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc4, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc4, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);
/* --- Case: ------------------------------------------------------ */


/*     Test operation of ZZEKSCMP on times. */

    tcase_("ZZEKSCMP: compare matching times.", (ftnlen)33);

/*     Get the segment descriptor for the first segment of EK1. */

    zzeksdsc_(&handle, &c__1, segdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the column descriptor for the fifth column. */

    zzekcdsc_(&handle, segdsc, cnames + 128, cldsc5, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare the first entry of column 5 to the first entry. */
/*     We expect equality. */

    eltidx = 1;
    dtype = 4;
    s_copy(cval, " ", (ftnlen)2048, (ftnlen)1);
    dval = tdata[0];
    ival = 0;
    null = FALSE_;
    row = 1;

/*     For class two columns, the "row" input required by ZZEKSCMP */
/*     is actually a pointer to the base of a data structure called an */
/*     "EK record pointer." (See usage in ZZEKRD02 for an example; */
/*     also see ekrecptr.inc.) We need to obtain this record pointer */
/*     for the row of interest: row 1. */

    zzektrdp_(&handle, &segdsc[6], &row, &recptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    match = zzekscmp_(&c__1, &handle, segdsc, cldsc5, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EQ MATCH", &match, &c_true, ok, (ftnlen)8);
    match = zzekscmp_(&c__5, &handle, segdsc, cldsc5, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("LT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__3, &handle, segdsc, cldsc5, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GT MATCH", &match, &c_false, ok, (ftnlen)8);
    match = zzekscmp_(&c__9, &handle, segdsc, cldsc5, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISNULL MATCH", &match, &c_false, ok, (ftnlen)12);
    match = zzekscmp_(&c__10, &handle, segdsc, cldsc5, &recptr, &eltidx, &
	    dtype, cval, &dval, &ival, &null, (ftnlen)2048);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("NOTNUL MATCH", &match, &c_true, ok, (ftnlen)12);

/* --- Case: ------------------------------------------------------ */


/*     Unload the EKs and delete the files. */

    tcase_("Unload EKs from query system.", (ftnlen)29);
    ekuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test1.ek", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_ek04__ */

