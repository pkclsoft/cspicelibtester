/*

-Procedure f_eqncpv_c ( Test eqncpv_c )

-Abstract

   Perform tests on the CSPICE wrapper eqncpv_c.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   void f_eqncpv_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrapper for eqncpv_c. The routine parallels
   in logic and structure the Fortran test family F_EQNCPV.

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   N.J. Bachman    (JPL)
   W.L. Taber      (JPL)

-Version

   -tspice_c Version 1.0.0 11-NOV-2011 (EDW)

-Index_Entries

   test eqncpv_c

-&
*/

{ /* Begin f_eqncpv_c */

   /*
   Local constants
   */


   /*
   Local macros
   */


   /*
   Local variables
   */

   SpiceInt               i;
   SpiceInt               j;

   SpiceDouble            a;
   SpiceDouble            argp;
   SpiceDouble            decpol;
   SpiceDouble            ecc;
   SpiceDouble            et;
   SpiceDouble            fivdpd;
   SpiceDouble            gm;
   SpiceDouble            inc;
   SpiceDouble            m0;
   SpiceDouble            n;
   SpiceDouble            node;
   SpiceDouble            p;
   SpiceDouble            rapol;
   SpiceDouble            t0;
   SpiceDouble            tendpd;
   SpiceDouble            theta;
   SpiceDouble            k      [3];
   SpiceDouble            x      [3];
   SpiceDouble            y      [3];
   SpiceDouble            z      [3];
   SpiceDouble            state  [6];
   SpiceDouble            state1 [6];
   SpiceDouble            state2 [6];
   SpiceDouble            tmpsta [6];
   SpiceDouble            elts   [8];
   SpiceDouble            eqel   [9];
   SpiceDouble            inrt   [3][3];
   SpiceDouble            rot    [3][3];
   SpiceDouble            rotfrm [6][6];
   SpiceDouble            rotto  [6][6];
   SpiceDouble            temp   [6][6];
   SpiceDouble            toinrt [6][6];
   SpiceDouble            xform  [6][6];


   /*
   Static variables
   */


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_eqncpv_c" );


   /*
   Case 1:
   */

   tcase_c( "This compares EQNCPV with CONICS for the "
            "case when the rates of node and argument of "
            "periapse are zero and the pole of the "
            "central frame is aligned with the pole of an "
            "inertial frame. " );

   p    =      1.0e4;
   gm   = 398600.436;
   ecc  =      0.1;
   a    = p/( 1. - ecc );
   n    = sqrt ( gm / a ) / a;
   argp = 30. * rpd_c();
   node = 15. * rpd_c();
   inc  = 10. * rpd_c();
   m0   = 45. * rpd_c();
   t0   = -100000000.;


   elts[0] = p;
   elts[1] = ecc;
   elts[2] = inc;
   elts[3] = node;
   elts[4] = argp;
   elts[5] = m0;
   elts[6] = t0;
   elts[7] = gm;


   eqel[0] = a;
   eqel[1] = ecc*sin(argp+node);
   eqel[2] = ecc*cos(argp+node);
   eqel[3] = m0 + argp + node;
   eqel[4] = tan(inc/2.)*sin(node);
   eqel[5] = tan(inc/2.)*cos(node);
   eqel[6] = 0.;
   eqel[7] = n;
   eqel[8] = 0.;

   rapol    = -halfpi_c();
   decpol   =  halfpi_c();



   et = t0 - 10000.0;

   for ( i = 0; i < 100; i++)
      {
      et = et + 250.;

      conics_c ( elts, et, state1 );
      eqncpv_c ( et,   t0, eqel, rapol, decpol, state2 );

      chckxc_c ( SPICEFALSE, " ",  ok );

      chckad_c ("state (1)", state2, "~/", state1, 6, 5.0e-12, ok );
      }




   /*
   Case 2:
   */

   tcase_c ( "Test to make sure we can accurately compute "
             "the state of an object that has non-zero "
             "rates for the longitude of the ascending "
             "node. " );


   p    =      1.0e4;
   gm   = 398600.436;
   ecc  =      0.1;
   a    = p/( 1. - ecc );
   n    = sqrt ( gm / a ) / a;
   argp = 30. * rpd_c();
   node = 15. * rpd_c();
   inc  = 10. * rpd_c();
   m0   = 45. * rpd_c();
   t0   = -100000000.;


   elts[0] = p;
   elts[1] = ecc;
   elts[2] = inc;
   elts[3] = node;
   elts[4] = argp;
   elts[5] = m0;
   elts[6] = t0;
   elts[7] = gm;

   /*
   We want a rate for the node of 10 degrees/day.
   */
   tendpd  = ( 10. / 86400. ) * rpd_c();

   eqel[0] = a;
   eqel[1] = ecc*sin(argp+node);
   eqel[2] = ecc*cos(argp+node);
   eqel[3] = m0 + argp + node;
   eqel[4] = tan(inc/2.)*sin(node);
   eqel[5] = tan(inc/2.)*cos(node);
   eqel[6] = tendpd;
   eqel[7] = n + tendpd;
   eqel[8] = tendpd;

   rapol    = -halfpi_c();
   decpol   =  halfpi_c();


   et = t0 - 10000.;

   for ( i = 0; i < 100; i++)
      {

      et    =  et + 250.;

      theta = (et - t0) * tendpd;

      /*       _        -
              | Rz    0  |
      xform = |          |
              | dRz   Rz |
              | ---      |
              | dt       |
               _        _
      */

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*tendpd;
      xform[4][0] =  cos(theta)*tendpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*tendpd;
      xform[4][1] = -sin(theta)*tendpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      conics_c ( elts, et, state );
      mxvg_c   ( xform, state, 6, 6, state1 );
      eqncpv_c ( et,   t0, eqel, rapol, decpol, state2 );

      chckxc_c ( SPICEFALSE, " ",  ok );

      chckad_c ("state (2)", state2, "~/", state1, 6, 5.0e-12, ok );
      }




    /*
   Case 3:
   */

   tcase_c ( "Test to make sure that we can accurately "
             "compute the state of an object that has a "
             "non-zero rate for the argument of periapse." );


   p    =      1.0e4;
   gm   = 398600.436;
   ecc  =      0.1;
   a    = p/( 1. - ecc );
   n    = sqrt ( gm / a ) / a;
   argp = 30. * rpd_c();
   node = 15. * rpd_c();
   inc  = 10. * rpd_c();
   m0   = 45. * rpd_c();
   t0   = -100000000.;


   elts[0] = p;
   elts[1] = ecc;
   elts[2] = inc;
   elts[3] = node;
   elts[4] = argp;
   elts[5] = m0;
   elts[6] = t0;
   elts[7] = gm;

   /*
   we want a rate for the argument of 5 degrees/day.
   */
   fivdpd  = ( 5. / 86400. ) * rpd_c();

   eqel[0] = a;
   eqel[1] = ecc*sin(argp+node);
   eqel[2] = ecc*cos(argp+node);
   eqel[3] = m0 + argp + node;
   eqel[4] = tan(inc/2.)*sin(node);
   eqel[5] = tan(inc/2.)*cos(node);
   eqel[6] = fivdpd;
   eqel[7] = n + fivdpd;
   eqel[8] = 0.;

   rapol   = -halfpi_c();
   decpol  =  halfpi_c();


   rot[0][0] =   cos(node);
   rot[1][0] =   sin(node);
   rot[2][0] =   0.;

   rot[0][1] =  -cos(inc) * sin(node);
   rot[1][1] =   cos(inc) * cos(node);
   rot[2][1] =   sin(inc);

   rot[0][2] =   sin(inc) * sin(node);
   rot[1][2] =  -sin(inc) * cos(node);
   rot[2][2] =   cos(inc);


    for ( i = 0; i < 3; i++)
      {
      for ( j = 0; j < 3; j++)
         {
         rotto[i  ][  j] = rot[i][j];
         rotto[i+3][j+3] = rot[i][j];
         rotto[i+3][j  ] = 0.;
         rotto[i  ][j+3] = 0.;
         }
      }

   xposeg_c ( rotto, 6, 6, rotfrm );


   et = t0 - 10000.;

   for ( i = 0; i < 100; i++)
      {

      et      =  et        + 250.;
      theta   =  (et - t0) * fivdpd;

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*fivdpd;
      xform[4][0] =  cos(theta)*fivdpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*fivdpd;
      xform[4][1] = -sin(theta)*fivdpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      mxmg_c ( xform, rotfrm, 6, 6, 6, temp  );
      mxmg_c ( rotto, temp,   6, 6, 6, xform );

      conics_c ( elts,  et,   state );
      mxvg_c   ( xform,       state,    6, 6,   state1 );

      eqncpv_c ( et,   t0, eqel, rapol, decpol, state2 );
      chckxc_c ( SPICEFALSE, " ",  ok );

      chckad_c ("state (3)", state2, "~/", state1, 6, 5.0e-12, ok );
      }




   /*
   Case 4:
   */

   tcase_c ( "Test the equinoctial propagator when "
             "precession of both the node and argument of "
             "periapse are non-zero." );

   p    =      1.0e4;
   gm   = 398600.436;
   ecc  =      0.1;
   a    = p/( 1. - ecc );
   n    = sqrt ( gm / a ) / a;
   argp = 30. * rpd_c();
   node = 15. * rpd_c();
   inc  = 10. * rpd_c();
   m0   = 45. * rpd_c();
   t0   = -100000000.;


   elts[0] = p;
   elts[1] = ecc;
   elts[2] = inc;
   elts[3] = node;
   elts[4] = argp;
   elts[5] = m0;
   elts[6] = t0;
   elts[7] = gm;


   eqel[0] = a;
   eqel[1] = ecc*sin(argp+node);
   eqel[2] = ecc*cos(argp+node);
   eqel[3] = m0 + argp + node;
   eqel[4] = tan(inc/2.)*sin(node);
   eqel[5] = tan(inc/2.)*cos(node);
   eqel[6] = fivdpd + tendpd;
   eqel[7] = n + fivdpd + tendpd;
   eqel[8] = tendpd;

   rapol   = -halfpi_c();
   decpol  =  halfpi_c();


   rot[0][0] =   cos(node);
   rot[1][0] =   sin(node);
   rot[2][0] =   0.;

   rot[0][1] =  -cos(inc) * sin(node);
   rot[1][1] =   cos(inc) * cos(node);
   rot[2][1] =   sin(inc);

   rot[0][2] =   sin(inc) * sin(node);
   rot[1][2] =  -sin(inc) * cos(node);
   rot[2][2] =   cos(inc);


   for ( i = 0; i < 3; i++)
      {
      for ( j = 0; j < 3; j++)
         {
         rotto[i  ][  j] = rot[i][j];
         rotto[i+3][j+3] = rot[i][j];
         rotto[i+3][j  ] = 0.;
         rotto[i  ][j+3] = 0.;
         }
      }

   xposeg_c ( rotto, 6, 6, rotfrm );


   et = t0 - 10000.;

   for ( i = 0; i < 100; i++)
      {

      et      =  et        + 250.;
      theta   =  (et - t0) * fivdpd;

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*fivdpd;
      xform[4][0] =  cos(theta)*fivdpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*fivdpd;
      xform[4][1] = -sin(theta)*fivdpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      mxmg_c ( xform, rotfrm, 6, 6, 6, temp  );
      mxmg_c ( rotto, temp,   6, 6, 6, xform );

      conics_c ( elts,  et,   state1 );
      mxvg_c   ( xform,       state1,    6, 6,   state );

      theta = (et - t0) * tendpd;

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*tendpd;
      xform[4][0] =  cos(theta)*tendpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*tendpd;
      xform[4][1] = -sin(theta)*tendpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      mxvg_c   ( xform, state,  6, 6,           state1 );

      eqncpv_c ( et,   t0, eqel, rapol, decpol, state2 );
      chckxc_c ( SPICEFALSE, " ",  ok );

      chckad_c ("state (4)", state2, "~/", state1, 6, 5.0e-12, ok );
      }



   /*
   Case 5:
   */

   tcase_c ( "Apply the same test as the previous case "
             "with the RA and DEC of the pole of the "
             "equatorial frame set so that the axes of the "
             "equatorial frame are not aligned with the "
             "inertial frame." );

   p    =      1.0e4;
   gm   = 398600.436;
   ecc  =      0.1;
   a    = p/( 1. - ecc );
   n    = sqrt ( gm / a ) / a;
   argp = 30. * rpd_c();
   node = 15. * rpd_c();
   inc  = 10. * rpd_c();
   m0   = 45. * rpd_c();
   t0   = -100000000.;


   elts[0] = p;
   elts[1] = ecc;
   elts[2] = inc;
   elts[3] = node;
   elts[4] = argp;
   elts[5] = m0;
   elts[6] = t0;
   elts[7] = gm;


   eqel[0] = a;
   eqel[1] = ecc*sin(argp+node);
   eqel[2] = ecc*cos(argp+node);
   eqel[3] = m0 + argp + node;
   eqel[4] = tan(inc/2.)*sin(node);
   eqel[5] = tan(inc/2.)*cos(node);
   eqel[6] = fivdpd + tendpd;
   eqel[7] = n + fivdpd + tendpd;
   eqel[8] = tendpd;

   rapol   = 30.0 * rpd_c();
   decpol  = 60.0 * rpd_c();


   rot[0][0] =   cos(node);
   rot[1][0] =   sin(node);
   rot[2][0] =   0.;

   rot[0][1] =  -cos(inc) * sin(node);
   rot[1][1] =   cos(inc) * cos(node);
   rot[2][1] =   sin(inc);

   rot[0][2] =   sin(inc) * sin(node);
   rot[1][2] =  -sin(inc) * cos(node);
   rot[2][2] =   cos(inc);


   z[0]     =   cos(rapol)*cos(decpol);
   z[1]     =   sin(rapol)*cos(decpol);
   z[2]     =   sin(decpol);

   k[0]     = 0.;
   k[1]     = 0.;
   k[2]     = 1.;

   ucrss_c ( k, z, x );
   ucrss_c ( z, x, y );

   inrt[0][0] = x[0];
   inrt[1][0] = x[1];
   inrt[2][0] = x[2];

   inrt[0][1] = y[0];
   inrt[1][1] = y[1];
   inrt[2][1] = y[2];

   inrt[0][2] = z[0];
   inrt[1][2] = z[1];
   inrt[2][2] = z[2];


   for ( i = 0; i < 3; i++)
      {
      for ( j = 0; j < 3; j++)
         {
         rotto[i  ][  j] = rot[i][j];
         rotto[i+3][j+3] = rot[i][j];
         rotto[i+3][j  ] = 0.;
         rotto[i  ][j+3] = 0.;

         toinrt[i  ][  j] = inrt[i][j];
         toinrt[i+3][j+3] = inrt[i][j];
         toinrt[i+3][j  ] = 0.;
         toinrt[i  ][j+3] = 0.;
         }
      }

   xposeg_c ( rotto, 6, 6, rotfrm );


   et = t0 - 10000.;

   for ( i = 0; i < 100; i++)
      {

      et      =  et        + 250.;
      theta   =  (et - t0) * fivdpd;

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*fivdpd;
      xform[4][0] =  cos(theta)*fivdpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*fivdpd;
      xform[4][1] = -sin(theta)*fivdpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      mxmg_c ( xform, rotfrm, 6, 6, 6, temp  );
      mxmg_c ( rotto, temp,   6, 6, 6, xform );

      conics_c ( elts,  et,   state1 );
      mxvg_c   ( xform,       state1,    6, 6,   state );

      theta = (et - t0) * tendpd;

      xform[0][0] =  cos(theta);
      xform[1][0] =  sin(theta);
      xform[2][0] =  0.;
      xform[3][0] = -sin(theta)*tendpd;
      xform[4][0] =  cos(theta)*tendpd;
      xform[5][0] =  0.;

      xform[0][1] = -sin(theta);
      xform[1][1] =  cos(theta);
      xform[2][1] =  0.;
      xform[3][1] = -cos(theta)*tendpd;
      xform[4][1] = -sin(theta)*tendpd;
      xform[5][1] =  0.;

      xform[0][2] =  0.;
      xform[1][2] =  0.;
      xform[2][2] =  1.;
      xform[3][2] =  0.;
      xform[4][2] =  0.;
      xform[5][2] =  0.;

      xform[0][3] =  0.;
      xform[1][3] =  0.;
      xform[2][3] =  0.;
      xform[3][3] =  cos(theta);
      xform[4][3] =  sin(theta);
      xform[5][3] =  0.;

      xform[0][4] =  0.;
      xform[1][4] =  0.;
      xform[2][4] =  0.;
      xform[3][4] = -sin(theta);
      xform[4][4] =  cos(theta);
      xform[5][4] =  0.;

      xform[0][5] =  0.;
      xform[1][5] =  0.;
      xform[2][5] =  0.;
      xform[3][5] =  0.;
      xform[4][5] =  0.;
      xform[5][5] =  1.;

      mxvg_c ( xform,  state,  6, 6, state1 );
      mxvg_c ( toinrt, state1, 6, 6, tmpsta );
      MOVED  ( tmpsta, 6,            state1 );

      eqncpv_c ( et,   t0, eqel, rapol, decpol, state2 );
      chckxc_c ( SPICEFALSE, " ",  ok );

      chckad_c ("state (5)", state2, "~/", state1, 6, 5.0e-12, ok );
      }



   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );

}


