/* f_fndcmp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c_b4 = 1000000;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__4 = 4;
static doublereal c_b476 = 1.;
static doublereal c_b527 = 2.;

/* $Procedure F_FNDCMP ( FNDCMP tests ) */
/* Subroutine */ int f_fndcmp__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static integer seed, ngap;
    static logical grid[1000000];
    extern logical even_(integer *);
    static integer xbds[1000000], vset[1000006];
    static logical grid1[1000000], grid2[1000000];
    static integer i__, j, k, n;
    static char label[80];
    static integer w;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer ngrid, ncomp;
    static logical value;
    static integer ncols;
    extern /* Subroutine */ int movei_(logical *, integer *, logical *), 
	    repmi_(char *, char *, integer *, char *, ftnlen, ftnlen, ftnlen);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nrows;
    extern /* Subroutine */ int t_success__(logical *), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), chckxc_(logical *, char *, logical *, ftnlen), chcksi_(
	    char *, integer *, char *, integer *, integer *, logical *, 
	    ftnlen, ftnlen), scardi_(integer *, integer *), fndcmp_(integer *,
	     integer *, logical *, integer *, logical *, integer *, integer *,
	     integer *, integer *, integer *, integer *, integer *, integer *)
	    , chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static integer mrkset[1000006];
    extern /* Subroutine */ int ssizei_(integer *, integer *);
    static integer tmpset[1000006], minpxx[1000000], minpxy[1000000], maxpxx[
	    1000000], maxpxy[1000000], col;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static integer row;

/* $ Abstract */

/*     Exercise the DSKBRIEF gap component finding routine FNDCMP. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKBRIEF supporting routine FNDCMP. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 08-FEB-2017 (NJB) */

/*        Updated to accommodate FNDCMP's argument */
/*        list change. */

/*        Original version 30-SEP-2016 (NJB) */

/* -& */

/*     TESTUTIL functions */


/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_FNDCMP", (ftnlen)8);
/* ********************************************************************** */

/*     FNDCMP error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Output arrays are too small to accommodate the output rec"
	    "tangle set.", (ftnlen)320, (ftnlen)68);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);
    value = FALSE_;

/*     Note: the row and column count must be odd in order to */
/*     achieve the checkerboard patter we want. */

    nrows = 5;
    ncols = 5;
    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (even_(&i__)) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)233)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)236)] = TRUE_;
	    }
	}
    }

/*     J is the maximum number of output components. */

    j = 5;
    fndcmp_(&nrows, &ncols, &value, &j, grid, vset, mrkset, tmpset, &ncomp, 
	    minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_true, "SPICE(ARRAYTOOSMALL)", ok, (ftnlen)20);
/* ********************************************************************** */

/*     FNDCMP normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: 3x3 grid, no gaps.", (ftnlen)320, (ftnlen)32)
	    ;
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    nrows = 3;
    ncols = 3;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)283)] = TRUE_;
	}
    }

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: 3x3 grid, one big gap.", (ftnlen)320, (
	    ftnlen)36);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)326)] = FALSE_;
	}
    }

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of the single gap. */

    chcksi_("MINPXX", minpxx, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXX", maxpxx, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MINPXY", minpxy, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXY", maxpxy, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, gap at element (2,2).", (ftnlen)320, (ftnlen)31);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)376)] = TRUE_;
	}
    }
    col = 2;
    row = 2;
    i__ = (col - 1) * nrows + row;
    grid[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge("grid", i__1,
	     "f_fndcmp__", (ftnlen)386)] = FALSE_;

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of the single gap. */

    chcksi_("MINPXX", minpxx, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXX", maxpxx, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MINPXY", minpxy, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXY", maxpxy, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, middle column is a gap.", (ftnlen)320, (ftnlen)
	    33);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)432)] = col != 2;
	}
    }

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of the single gap. */

    chcksi_("MINPXX", minpxx, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXX", maxpxx, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MINPXY", minpxy, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXY", maxpxy, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, middle row is a gap.", (ftnlen)320, (ftnlen)30);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)484)] = row != 2;
	}
    }

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of the single gap. */

    chcksi_("MINPXX", minpxx, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXX", maxpxx, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MINPXY", minpxy, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("MAXPXY", maxpxy, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, all corners are gaps", (ftnlen)320, (ftnlen)30);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if ((row == 1 || row == 3) && (col == 1 || col == 3)) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)538)] = FALSE_;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)540)] = TRUE_;
	    }
	}
    }

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__4, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of all four gaps. */

    xbds[0] = 1;
    xbds[1] = 1;
    xbds[2] = 3;
    xbds[3] = 3;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 1;
    xbds[2] = 3;
    xbds[3] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    xbds[2] = 1;
    xbds[3] = 3;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    xbds[2] = 1;
    xbds[3] = 3;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, all corners are present; central cross is a gap."
	    , (ftnlen)320, (ftnlen)58);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if ((row == 1 || row == 3) && (col == 1 || col == 3)) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)618)] = TRUE_;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)620)] = FALSE_;
		++ngap;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__3, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of all three gaps. */

    xbds[0] = 1;
    xbds[1] = 2;
    xbds[2] = 2;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 2;
    xbds[2] = 2;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 2;
    xbds[1] = 1;
    xbds[2] = 3;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 2;
    xbds[1] = 1;
    xbds[2] = 3;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)696)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)696)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)696)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)696)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, first column and first row are gaps", (ftnlen)
	    320, (ftnlen)45);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 1 || col == 1) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)733)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)736)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 2;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 1;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 1;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)805)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)805)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)805)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)805)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, last column and first row are gaps", (ftnlen)320,
	     (ftnlen)44);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 1 || col == 3) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)842)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)845)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 2;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)914)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)914)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)914)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)914)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, first column and last row are gaps", (ftnlen)320,
	     (ftnlen)44);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;

/*     We'll count the gaps this time. */

    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 3 || col == 1) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)956)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)959)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 2;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 3;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)1029)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)1029)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)1029)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)1029)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, last column and last row are gaps", (ftnlen)320, 
	    (ftnlen)43);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;

/*     We'll count the gaps this time. */

    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 3 || col == 3) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1071)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1074)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 1;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 2;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)1144)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)1144)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)1144)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)1144)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, last column and last row are gaps", (ftnlen)320, 
	    (ftnlen)43);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;

/*     We'll count the gaps this time. */

    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 3 || col == 3) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1187)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1190)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 1;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 2;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)1260)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)1260)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)1260)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)1260)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "3x3 grid, last column and last row are gaps", (ftnlen)320, 
	    (ftnlen)43);
    tcase_(title, (ftnlen)320);
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;

/*     We'll count the gaps this time. */

    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (row == 3 || col == 3) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1303)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1306)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NCOMP", &ncomp, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check bounds of both gaps. */

    xbds[0] = 1;
    xbds[1] = 3;
    chckai_("MINPXX", minpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 3;
    chckai_("MAXPXX", maxpxx, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 1;
    chckai_("MINPXY", minpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);
    xbds[0] = 3;
    xbds[1] = 2;
    chckai_("MAXPXY", maxpxy, "=", xbds, &ncomp, ok, (ftnlen)6, (ftnlen)1);

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)1376)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)1376)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)1376)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)1376)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "101x101 grid. Every other pixel is a gap.", (ftnlen)320, (
	    ftnlen)41);
    tcase_(title, (ftnlen)320);

/*     We must have an odd row count to create the checkerboard */
/*     pattern we desire. */

    nrows = 101;
    ncols = 101;
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    ngap = 0;
    i__1 = ncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = nrows;
	for (row = 1; row <= i__2; ++row) {
	    i__ = (col - 1) * nrows + row;
	    if (even_(&i__)) {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1423)] = FALSE_;
		++ngap;
	    } else {
		grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
			"grid", i__3, "f_fndcmp__", (ftnlen)1426)] = TRUE_;
	    }
	}
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    n = ncols * nrows;
    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test the results: create a new grid with no gaps; fill in */
/*     each output component with gap values. The result should */
/*     match the copy of the input grid. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	grid2[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge("grid2",
		 i__2, "f_fndcmp__", (ftnlen)1455)] = TRUE_;
    }
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__4 = maxpxx[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		"maxpxx", i__3, "f_fndcmp__", (ftnlen)1460)];
	for (j = minpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : 
		s_rnge("minpxx", i__2, "f_fndcmp__", (ftnlen)1460)]; j <= 
		i__4; ++j) {
	    i__5 = maxpxy[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : 
		    s_rnge("maxpxy", i__3, "f_fndcmp__", (ftnlen)1462)];
	    for (k = minpxy[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : 
		    s_rnge("minpxy", i__2, "f_fndcmp__", (ftnlen)1462)]; k <= 
		    i__5; ++k) {
		w = (j - 1) * nrows + k;
		grid2[(i__2 = w - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
			"grid2", i__2, "f_fndcmp__", (ftnlen)1466)] = FALSE_;
	    }
	}
    }

/*     Did we recover the grid? */

/*     Use CHCKSL and a loop. */

    *ok = TRUE_;
    i__ = 1;
    while(i__ <= n && *ok) {
	if (grid2[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge(
		"grid2", i__1, "f_fndcmp__", (ftnlen)1484)] != grid1[(i__4 = 
		i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("grid1", i__4,
		 "f_fndcmp__", (ftnlen)1484)]) {
	    s_copy(label, "GRID2 element *", (ftnlen)80, (ftnlen)15);
	    repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksl_(label, &grid2[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? 
		    i__1 : s_rnge("grid2", i__1, "f_fndcmp__", (ftnlen)1489)],
		     &grid1[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : 
		    s_rnge("grid1", i__4, "f_fndcmp__", (ftnlen)1489)], ok, (
		    ftnlen)80);
	}
	++i__;
    }

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge(
		"maxpxx", i__4, "f_fndcmp__", (ftnlen)1516)] - minpxx[(i__5 = 
		i__ - 1) < 1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxx", 
		i__5, "f_fndcmp__", (ftnlen)1516)] + 1) * (maxpxy[(i__2 = i__ 
		- 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge("maxpxy", i__2, 
		"f_fndcmp__", (ftnlen)1516)] - minpxy[(i__3 = i__ - 1) < 
		1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxy", i__3, "f_fndc"
		"mp__", (ftnlen)1516)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);
/* *********************************************************************** */



/*     Random cases */



/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "300x300 grid. Random pixels are selected as gap members.", 
	    (ftnlen)320, (ftnlen)56);
    tcase_(title, (ftnlen)320);
    nrows = 300;
    ncols = 300;
    n = ncols * nrows;
    scardi_(&c__0, mrkset);
    scardi_(&c__0, tmpset);
    scardi_(&c__0, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	grid[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("grid", 
		i__4, "f_fndcmp__", (ftnlen)1563)] = TRUE_;
    }

/*     Set J to the nominal number of gap pixels. */

    j = 10000;
    ngap = 0;
    seed = -1;
    i__1 = j;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__2 = (doublereal) n;
	d__1 = t_randd__(&c_b476, &d__2, &seed);
	j = i_dnnt(&d__1);

/*        We might have duplicate values of J. Count the current pixel */
/*        only if it's not already a gap pixel. */

	if (grid[(i__4 = j - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("grid",
		 i__4, "f_fndcmp__", (ftnlen)1581)]) {
	    ++ngap;
	}
	grid[(i__4 = j - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("grid", 
		i__4, "f_fndcmp__", (ftnlen)1585)] = FALSE_;
    }

/*     Get a copy of the input grid, since GRID is an in-out argument. */

    movei_(grid, &n, grid1);

/*     Find the gaps in the grid. */

    fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &ncomp,
	     minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test the results: create a new grid with no gaps; fill in */
/*     each output component with gap values. The result should */
/*     match the copy of the input grid. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	grid2[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("grid2",
		 i__4, "f_fndcmp__", (ftnlen)1610)] = TRUE_;
    }
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = maxpxx[(i__5 = i__ - 1) < 1000000 && 0 <= i__5 ? i__5 : s_rnge(
		"maxpxx", i__5, "f_fndcmp__", (ftnlen)1615)];
	for (j = minpxx[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : 
		s_rnge("minpxx", i__4, "f_fndcmp__", (ftnlen)1615)]; j <= 
		i__2; ++j) {
	    i__3 = maxpxy[(i__5 = i__ - 1) < 1000000 && 0 <= i__5 ? i__5 : 
		    s_rnge("maxpxy", i__5, "f_fndcmp__", (ftnlen)1617)];
	    for (k = minpxy[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : 
		    s_rnge("minpxy", i__4, "f_fndcmp__", (ftnlen)1617)]; k <= 
		    i__3; ++k) {
		w = (j - 1) * nrows + k;
		grid2[(i__4 = w - 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge(
			"grid2", i__4, "f_fndcmp__", (ftnlen)1621)] = FALSE_;
	    }
	}
    }

/*     Did we recover the grid? */

/*     Use CHCKSL and a loop. */

    *ok = TRUE_;
    i__ = 1;
    while(i__ <= n && *ok) {
	if (grid2[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? i__1 : s_rnge(
		"grid2", i__1, "f_fndcmp__", (ftnlen)1639)] != grid1[(i__2 = 
		i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge("grid1", i__2,
		 "f_fndcmp__", (ftnlen)1639)]) {
	    s_copy(label, "GRID2 element *", (ftnlen)80, (ftnlen)15);
	    repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksl_(label, &grid2[(i__1 = i__ - 1) < 1000000 && 0 <= i__1 ? 
		    i__1 : s_rnge("grid2", i__1, "f_fndcmp__", (ftnlen)1644)],
		     &grid1[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : 
		    s_rnge("grid1", i__2, "f_fndcmp__", (ftnlen)1644)], ok, (
		    ftnlen)80);
	}
	++i__;
    }

/*     Find the gaps in the inverse grid. These should */
/*     combine to form the coverage of the original grid. */

    value = ! value;
    fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
	    ncomp, minpxx, maxpxx, minpxy, maxpxy);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Count the pixels in the gaps. */

    j = 0;
    i__1 = ncomp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j += (maxpxx[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		"maxpxx", i__2, "f_fndcmp__", (ftnlen)1671)] - minpxx[(i__3 = 
		i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("minpxx", 
		i__3, "f_fndcmp__", (ftnlen)1671)] + 1) * (maxpxy[(i__4 = i__ 
		- 1) < 1000000 && 0 <= i__4 ? i__4 : s_rnge("maxpxy", i__4, 
		"f_fndcmp__", (ftnlen)1671)] - minpxy[(i__5 = i__ - 1) < 
		1000000 && 0 <= i__5 ? i__5 : s_rnge("minpxy", i__5, "f_fndc"
		"mp__", (ftnlen)1671)] + 1);
    }

/*     Check the coverage count versus that implied by the */
/*     gap count. */

    i__1 = n - ngap;
    chcksi_("J (coverage)", &j, "=", &i__1, &c__0, ok, (ftnlen)12, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Following is a sequence of random cases using small grids. */
/*     we'll use gap sizes of 1 : NGRID-1, where the locations */
/*     of the gap pixels are selected randomly. */


    nrows = 18;
    ncols = 23;
    ngrid = nrows * ncols;

/*     The nominal gap size is the number of random indices we */
/*     select. Since some indices may overlap, the actual gap */
/*     size may be smaller. */

    i__1 = ngrid - 1;
    for (n = 1; n <= i__1; ++n) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Random case: grid size = #; nominal gap count = #.", (
		ftnlen)320, (ftnlen)50);
	repmi_(title, "#", &ngrid, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		;
	repmi_(title, "#", &n, title, (ftnlen)320, (ftnlen)1, (ftnlen)320);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)320);

/*        The nominal gap size is N. */

/*        Initialize the grid, then set the gap pixels. */

/*        VALUE is the logical value indicating a gap. */

	value = FALSE_;
	i__2 = ngrid;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)1725)] = TRUE_;
	}
	ngap = 0;
	seed = -1;
	i__2 = n;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    d__2 = (doublereal) (ngrid - 1);
	    d__1 = t_randd__(&c_b527, &d__2, &seed);
	    j = i_dnnt(&d__1);

/*           We might have duplicate values of J. Count the current pixel */
/*           only if it's not already a gap pixel. */

	    if (grid[(i__3 = j - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid", i__3, "f_fndcmp__", (ftnlen)1739)]) {
		++ngap;
	    }
	    grid[(i__3 = j - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge("grid",
		     i__3, "f_fndcmp__", (ftnlen)1743)] = FALSE_;
	}
/*         WRITE (*,*) 'N, NGAP = ', N, NGAP */

/*        Get a copy of the input grid, since GRID is an in-out argument. */

	movei_(grid, &ngrid, grid1);

/*        Find the gaps in the grid. */

	fndcmp_(&nrows, &ncols, &value, &c_b4, grid, vset, mrkset, tmpset, &
		ncomp, minpxx, maxpxx, minpxy, maxpxy);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Test the results: create a new grid with no gaps; fill in */
/*        each output component with gap values. The result should */
/*        match the copy of the input grid. */

	i__2 = ngrid;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    grid2[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : s_rnge(
		    "grid2", i__3, "f_fndcmp__", (ftnlen)1768)] = TRUE_;
	}
	i__2 = ncomp;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i__5 = maxpxx[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : 
		    s_rnge("maxpxx", i__4, "f_fndcmp__", (ftnlen)1773)];
	    for (j = minpxx[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : 
		    s_rnge("minpxx", i__3, "f_fndcmp__", (ftnlen)1773)]; j <= 
		    i__5; ++j) {
		i__6 = maxpxy[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 :
			 s_rnge("maxpxy", i__4, "f_fndcmp__", (ftnlen)1775)];
		for (k = minpxy[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? 
			i__3 : s_rnge("minpxy", i__3, "f_fndcmp__", (ftnlen)
			1775)]; k <= i__6; ++k) {
		    w = (j - 1) * nrows + k;
		    grid2[(i__3 = w - 1) < 1000000 && 0 <= i__3 ? i__3 : 
			    s_rnge("grid2", i__3, "f_fndcmp__", (ftnlen)1779)]
			     = FALSE_;
		}
	    }
	}

/*        Did we recover the grid? */

/*        Use CHCKSL and a loop. */

	*ok = TRUE_;
	i__ = 1;
	while(i__ <= ngrid && *ok) {
	    if (grid2[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 ? i__2 : s_rnge(
		    "grid2", i__2, "f_fndcmp__", (ftnlen)1797)] != grid1[(
		    i__5 = i__ - 1) < 1000000 && 0 <= i__5 ? i__5 : s_rnge(
		    "grid1", i__5, "f_fndcmp__", (ftnlen)1797)]) {
		s_copy(label, "GRID2 element *", (ftnlen)80, (ftnlen)15);
		repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chcksl_(label, &grid2[(i__2 = i__ - 1) < 1000000 && 0 <= i__2 
			? i__2 : s_rnge("grid2", i__2, "f_fndcmp__", (ftnlen)
			1802)], &grid1[(i__5 = i__ - 1) < 1000000 && 0 <= 
			i__5 ? i__5 : s_rnge("grid1", i__5, "f_fndcmp__", (
			ftnlen)1802)], ok, (ftnlen)80);
	    }
	    ++i__;
	}

/*        Find the gaps in the inverse grid. These should */
/*        combine to form the coverage of the original grid. */

	value = ! value;
	fndcmp_(&nrows, &ncols, &value, &c_b4, grid1, vset, mrkset, tmpset, &
		ncomp, minpxx, maxpxx, minpxy, maxpxy);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Count the pixels in the gaps. */

	j = 0;
	i__2 = ncomp;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    j += (maxpxx[(i__5 = i__ - 1) < 1000000 && 0 <= i__5 ? i__5 : 
		    s_rnge("maxpxx", i__5, "f_fndcmp__", (ftnlen)1829)] - 
		    minpxx[(i__6 = i__ - 1) < 1000000 && 0 <= i__6 ? i__6 : 
		    s_rnge("minpxx", i__6, "f_fndcmp__", (ftnlen)1829)] + 1) *
		     (maxpxy[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? i__3 : 
		    s_rnge("maxpxy", i__3, "f_fndcmp__", (ftnlen)1829)] - 
		    minpxy[(i__4 = i__ - 1) < 1000000 && 0 <= i__4 ? i__4 : 
		    s_rnge("minpxy", i__4, "f_fndcmp__", (ftnlen)1829)] + 1);
	}

/*        Check the coverage count versus that implied by the */
/*        gap count. */

	i__2 = ngrid - ngap;
	chcksi_("J (coverage)", &j, "=", &i__2, &c__0, ok, (ftnlen)12, (
		ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_fndcmp__ */

