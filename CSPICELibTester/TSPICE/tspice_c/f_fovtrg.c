/* f_fovtrg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static doublereal c_b167 = 0.;
static doublereal c_b168 = 1.;
static doublereal c_b169 = 2.;
static doublereal c_b177 = -1.;
static doublereal c_b187 = -2.;
static integer c__30000 = 30000;

/* $Procedure      F_FOVTRG ( FOVTRG family tests ) */
/* Subroutine */ int f_fovtrg__(logical *ok)
{
    /* Initialized data */

    static char abcors[5*5] = "NONE " "CN   " "XCN  " "CN+S " "XCN+S";
    static char instrs[36*4] = "ALPHA_CIRCLE_NONE                   " "ALPHA"
	    "_ELLIPSE_NONE                  " "ALPHA_RECTANGLE_NONE          "
	    "      " "ALPHA_DIAMOND_NONE                  ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static char inst[36];
    static integer n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal bsite[3];
    extern /* Subroutine */ int natik_(char *, char *, char *, logical *, 
	    logical *, ftnlen, ftnlen, ftnlen), repmc_(char *, char *, char *,
	     char *, ftnlen, ftnlen, ftnlen, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[200];
    static doublereal times[20]	/* was [2][5][2] */;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , bods2c_(char *, integer *, logical *, ftnlen);
    static doublereal badbnd[30000]	/* was [3][10000] */, badrad[3];
    static integer nc;
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen), chcksl_(char *, logical *, logical *, 
	    logical *, ftnlen);
    static char abcorr[200], tframe[32], kvname[32], target[36], newfrm[32], 
	    obsrvr[36], tshape[9];
    static doublereal fovbnd[30000]	/* was [3][10000] */, svradi[3];
    static integer instid, shpitr;
    static logical result[2], visibl;
    extern /* Subroutine */ int tstlsk_(void), tstpck_(char *, logical *, 
	    logical *, ftnlen), tstspk_(char *, logical *, integer *, ftnlen),
	     natpck_(char *, logical *, logical *, ftnlen), natspk_(char *, 
	    logical *, integer *, ftnlen), fovtrg_(char *, char *, char *, 
	    char *, char *, char *, doublereal *, logical *, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), gdpool_(char *, integer *, 
	    integer *, integer *, doublereal *, logical *, ftnlen), dvpool_(
	    char *, ftnlen), pdpool_(char *, integer *, doublereal *, ftnlen),
	     pcpool_(char *, integer *, char *, ftnlen, ftnlen), vminus_(
	    doublereal *, doublereal *), spkuef_(integer *);
    static integer ins, tmp, han2;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        FOVTRG */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine FOVTRG. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     S.C. Krening     (JPL) */
/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 13-FEB-2012 (SCK) (NJB) */

/* -& */

/*     SPICELIB functions */

/*      DOUBLE PRECISION      SPD */

/*     Local parameters */


/*     Local variables */


/*     Saved Variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_FOVTRG", (ftnlen)8);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("fovtrg.tpc", &c_true, &c_false, (ftnlen)10);

/*     Load an SPK file as well. */

    tstspk_("zzgffvu.bsp", &c_true, &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK, PCK/FK, and IK */
/*     files. */

    natpck_("nat.tpc", &c_true, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natik_("nat.ti", "nat.bsp", "nat.tpc", &c_true, &c_false, (ftnlen)6, (
	    ftnlen)7, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

/*     Instrument not recognized. */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad observer or target name.", (ftnlen)28);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BET", (ftnlen)36, (ftnlen)3);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SN", (ftnlen)36, (ftnlen)2);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer equals target", (ftnlen)22);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad shape specification.", (ftnlen)24);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(tshape, "Line", (ftnlen)9, (ftnlen)4);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSHAPE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad shape: ray.", (ftnlen)15);
    s_copy(tshape, "RAY", (ftnlen)9, (ftnlen)3);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XS", (ftnlen)200, (ftnlen)2);
    s_copy(tframe, " ", (ftnlen)32, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad reference frame TFRAME", (ftnlen)26);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, "EME2000", (ftnlen)32, (ftnlen)7);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Blank reference frame TFRAME for ellipsoidal target", (ftnlen)51);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, " ", (ftnlen)32, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame TFRAME not centered on target when TSHAPE requir"
	    "es it.", (ftnlen)70);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-existent reference frame.", (ftnlen)29);
    s_copy(target, "beta", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, "EME2000", (ftnlen)32, (ftnlen)7);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction for ellipsoid target.", (ftnlen)47);
    s_copy(abcorr, "XS", (ftnlen)200, (ftnlen)2);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction for point target.", (ftnlen)43);
    s_copy(abcorr, "S", (ftnlen)200, (ftnlen)1);
    s_copy(tshape, "POINT", (ftnlen)9, (ftnlen)5);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction for ray target.", (ftnlen)41);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(tshape, "Ray", (ftnlen)9, (ftnlen)3);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No target radii in kernel pool", (ftnlen)30);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dvpool_("BODY399_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad target radii count", (ftnlen)22);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pdpool_("BODY399_RADII", &c__2, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii values", (ftnlen)23);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b167, &c_b168, &c_b169, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    vpack_(&c_b168, &c_b177, &c_b169, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    vpack_(&c_b168, &c_b168, &c_b187, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ID code for instrument.", (ftnlen)26);
    s_copy(inst, "ALPHA_ELLIPSE", (ftnlen)36, (ftnlen)13);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Instrument parameters missing from kernel pool.", (ftnlen)47);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    s_copy(kvname, "INS#_FOV_SHAPE", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);

/*     Delete the instrument shape from the kernel pool. */

    dvpool_(kvname, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SHAPEMISSING)", ok, (ftnlen)19);

/*     Restore the instrument shape value. */

    pcpool_(kvname, &c__1, "ELLIPSE", (ftnlen)32, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Degenerate FOV ellipse.", (ftnlen)23);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Fetch the instrument boresight vector from the kernel pool. */

    s_copy(kvname, "INS#_BORESIGHT", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__3, &n, bsite, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the instrument boundary vectors from the kernel pool. */

    s_copy(kvname, "INS#_FOV_BOUNDARY", (ftnlen)32, (ftnlen)17);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__30000, &n, fovbnd, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the first boundary vector equal to the boresight vector. */

    moved_(fovbnd, &n, badbnd);
    moved_(bsite, &c__3, badbnd);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore the first boundary vector and set the second */
/*     equal to the boresight vector. */

    moved_(fovbnd, &c__3, badbnd);
    moved_(bsite, &c__3, &badbnd[3]);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore the instrument FOV boundary vectors. */

    pdpool_(kvname, &n, fovbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("FOV boundary vector has excessive angular separation from boresi"
	    "ght.", (ftnlen)68);
    s_copy(inst, "ALPHA_RECTANGLE_NONE", (ftnlen)36, (ftnlen)20);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Fetch the instrument boresight vector from the kernel pool. */

    s_copy(kvname, "INS#_BORESIGHT", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__3, &n, bsite, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the instrument boundary vectors from the kernel pool. */

    s_copy(kvname, "INS#_FOV_BOUNDARY", (ftnlen)32, (ftnlen)17);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__30000, &n, fovbnd, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the third boundary vector equal to the *negative* */
/*     of the boresight vector. */

    moved_(fovbnd, &n, badbnd);
    vminus_(bsite, &badbnd[6]);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(FACENOTFOUND)", ok, (ftnlen)19);

/*     Restore the instrument FOV boundary vectors. */

    pdpool_(kvname, &n, fovbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

/*     At this point, we need an actual, non-empty confinement */
/*     window. */

    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "ITRF93", (ftnlen)32, (ftnlen)6);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(tframe, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(tframe, "IAU_SUN", (ftnlen)32, (ftnlen)7);
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No TFRAME orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "ITRF93", (ftnlen)32, (ftnlen)6);
    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &et, &visibl, (
	    ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     We'll start out with cases using an ellipsoidal target and */
/*     geometric states. We'll use the four instruments defined */
/*     in nat.ti: */

/*        ALPHA_CIRCLE_NONE */
/*        ALPHA_ELLIPSE_NONE */
/*        ALPHA_RECTANGLE_NONE */
/*        ALPHA_DIAMOND_NONE */

/*     These have the FOV shapes */

/*        ELLIPSE */
/*        CIRCLE */
/*        RECTANGLE */
/*        POLYGON */

/*     and track body Alpha so body Beta's FOV entry and */
/*     exit times match the start and stop times of Beta's */
/*     transit across Alpha. */

    result[0] = TRUE_;
    result[1] = FALSE_;

/*     The TIMES variable is indexed according to the following in which */
/*     case is an expected result of visible or not visible, abcorr are */
/*     ordered as shown below, and the target shape is either ellipsoid */
/*     or point. (case, abcorr, target shape) */

/*     1: None */
/*     2: CN */
/*     3: XCN */
/*     4: CN+S */
/*     5: XCN+S */

/*     The times in the TIMES variable below are from GFTFOV. */

    times[0] = 1.;
    times[1] = 602.;
    times[2] = 3.;
    times[3] = 603.;
    times[4] = -1.;
    times[5] = 599.;
    times[6] = 3.;
    times[7] = 603.;
    times[8] = -1.;
    times[9] = 599.;
    times[10] = 61.;
    times[11] = 542.;
    times[12] = 63.;
    times[13] = 543.;
    times[14] = 59.;
    times[15] = 539.;
    times[16] = 63.;
    times[17] = 543.;
    times[18] = 59.;
    times[19] = 539.;
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);

/*     Loop over two target shape cases: ellipsoid and point. */
/*     For the ellipsoid cases, the start times should be one */
/*     minute early and the stop times should be one minute late. */

    for (shpitr = 1; shpitr <= 2; ++shpitr) {
	if (shpitr == 1) {
	    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
	    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
	} else {
	    s_copy(tshape, "POINT", (ftnlen)9, (ftnlen)5);
	}

/*     Loop through the instruments. */

	for (ins = 1; ins <= 4; ++ins) {
	    s_copy(inst, instrs + ((i__1 = ins - 1) < 4 && 0 <= i__1 ? i__1 : 
		    s_rnge("instrs", i__1, "f_fovtrg__", (ftnlen)934)) * 36, (
		    ftnlen)36, (ftnlen)36);

/*           Loop through the ABCORR values (5 of them) */

	    for (nc = 1; nc <= 5; ++nc) {

/* --- Case: ------------------------------------------------------ */


		s_copy(abcorr, abcors + ((i__1 = nc - 1) < 5 && 0 <= i__1 ? 
			i__1 : s_rnge("abcors", i__1, "f_fovtrg__", (ftnlen)
			943)) * 5, (ftnlen)200, (ftnlen)5);

/*              Set up the TCASE call. */

		s_copy(title, "Target #; target shape #; Inst. #; #; inst fr"
			"ame #", (ftnlen)200, (ftnlen)50);
		repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", tshape, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)9, (ftnlen)200);
		repmc_(title, "#", inst, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)200, (ftnlen)200);
		repmc_(title, "#", newfrm, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)32, (ftnlen)200);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)200);
		for (tmp = 1; tmp <= 2; ++tmp) {

/*                 Is it visible? */

		    fovtrg_(inst, target, tshape, tframe, abcorr, obsrvr, &
			    times[(i__1 = tmp + (nc + shpitr * 5 << 1) - 13) <
			     20 && 0 <= i__1 ? i__1 : s_rnge("times", i__1, 
			    "f_fovtrg__", (ftnlen)965)], &visibl, (ftnlen)36, 
			    (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)200, (
			    ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("VISIBL", &visibl, &result[(i__1 = tmp - 1) < 2 &&
			     0 <= i__1 ? i__1 : s_rnge("result", i__1, "f_fo"
			    "vtrg__", (ftnlen)969)], ok, (ftnlen)6);
		}
	    }
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK files. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgffvu.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_fovtrg__ */

