/*

-Procedure f_fovtrg_c ( Test fovtrg_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper fovtrg_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_fovtrg_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for fovtrg_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   S.C. Krening    (JPL)
 
-Version
 
   -tspice_c Version 1.1.0, 10-FEB-2017 (NJB)

       Removed unneeded declarations.       

   -tspice_c Version 1.0.0 13-FEB-2012 (SCK) 

-Index_Entries

   test fovtrg_c

-&
*/

{ /* Begin f_fovtrg_c */

   /*
   Prototypes 
   */
   int natik_  ( char    *nameik, 
                 char    *spk, 
                 char    *pck, 
                 logical *loadik, 
                 logical *keepik, 
                 ftnlen  nameik_len, 
                 ftnlen  spk_len, 
                 ftnlen  pck_len    );

   int natpck_ ( char    *file, 
                 logical *loadpc, 
                 logical *keeppc, 
                 ftnlen  file_len );

   int natspk_ ( char    *file, 
                 logical *load, 
                 integer *handle,
                 ftnlen  file_len );

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define IK              "nat.ti"   
   #define PCK             "nat.tpc"   
   #define SPK             "nat.bsp"   
   #define TIMTOL          1.e-6 

   /*
   Local variables
   */
   integer                 handle;

   static logical          keepik  = SPICETRUE;
   static logical          keeppc  = SPICETRUE;
   static logical          loadik  = SPICETRUE;
   static logical          loadpc  = SPICETRUE;
   static logical          loadspk = SPICETRUE;

   SpiceDouble             et;
   
   SpiceBoolean            visible;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_fovtrg_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Create and load Nat's solar system SPK, PCK/FK, and IK files.
   */
 

   /*
   Create and load a PCK file. Do NOT delete the file afterward.
   */
   natpck_ ( ( char      * ) PCK,
             ( logical   * ) &loadpc, 
             ( logical   * ) &keeppc, 
             ( ftnlen      ) strlen(PCK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Load an SPK file as well.
   */
   natspk_ ( ( char      * ) SPK,
             ( logical   * ) &loadspk, 
             ( integer   * ) &handle, 
             ( ftnlen      ) strlen(SPK) );   
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load an IK.
   */
   natik_  ( ( char      * ) IK,
             ( char      * ) SPK,
             ( char      * ) PCK,
             ( logical   * ) &loadik, 
             ( logical   * ) &keepik, 
             ( ftnlen      ) strlen(IK),
             ( ftnlen      ) strlen(SPK),   
             ( ftnlen      ) strlen(PCK)  );   
   chckxc_c ( SPICEFALSE, " ", ok );

   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Bad input string pointers" );

   str2et_c ( "2000 jan 1 TDB", &et );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   fovtrg_c ( NULLCPTR,   "beta", "ellipsoid", "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", NULLCPTR, "ellipsoid", "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", NULLCPTR, "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", NULLCPTR,
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", "betafixed",
              NULLCPTR, "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", "betafixed",
              "none", NULLCPTR,  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   
   tcase_c ( "Empty input strings" );

   fovtrg_c ( "",   "beta", "ellipsoid", "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "", "ellipsoid", "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "", "betafixed",
              "none", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", "betafixed",
              "", "sun",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", "betafixed",
              "none", "",  &et,     &visible );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */
   
   et = 1.0;
   
   fovtrg_c ( "ALPHA_ELLIPSE_NONE", "beta", "ellipsoid", "betafixed",
              "none", "sun",  &et,     &visible );
              
   chcksl_c ( "visible", visible, SPICETRUE, ok );
   
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( (SpiceInt)handle );
   TRASH   ( SPK    );
   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_fovtrg_c */
   
   
   
   
   
   
   
   
   
   
   









