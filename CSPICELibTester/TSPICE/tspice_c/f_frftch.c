/* f_frftch.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__128 = 128;
static logical c_false = FALSE_;
static integer c__127 = 127;
static integer c__0 = 0;
static integer c_n1 = -1;
static logical c_true = TRUE_;
static integer c_n2 = -2;
static integer c__6 = 6;
static integer c__3 = 3;
static integer c__1 = 1;
static integer c__5000 = 5000;

/* $Procedure      F_FRFTCH ( Family of tests for frame fetch APIs ) */
/* Subroutine */ int f_frftch__(logical *ok)
{
    /* Initialized data */

    static integer nclass[5] = { 0,5,6,7,8 };

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer bign, ctrs[127], i__, n;
    extern integer cardi_(integer *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer frmid;
    static char names[32*127];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static integer idset[5006];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), movei_(integer *, integer *, integer *);
    static char title[400];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer types[127];
    extern /* Subroutine */ int t_success__(logical *), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    static integer idcdes[127], bidids[128];
    extern /* Subroutine */ int kclear_(void);
    static integer frcode, frclid, bididx[128];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     scardi_(integer *, integer *), chcksi_(char *, integer *, char *,
	     integer *, integer *, logical *, ftnlen, ftnlen);
    static char frname[32];
    static integer bidpol[134];
    extern /* Subroutine */ int validi_(integer *, integer *, integer *), 
	    appndi_(integer *, integer *);
    static integer centrd[127];
    static char kvname[32];
    static integer frcent, bnmidx[128], bidlst[128], clsidx, idlist[5000], 
	    bnmpol[134], frclss, xidset[5006], bnmlst[128];
    static char bnmnms[32*128];
    static integer ncount;
    extern /* Subroutine */ int zzfdat_(integer *, integer *, char *, integer 
	    *, integer *, integer *, integer *, integer *, integer *, integer 
	    *, char *, integer *, integer *, integer *, integer *, integer *, 
	    ftnlen, ftnlen), ssizei_(integer *, integer *), bltfrm_(integer *,
	     integer *), pipool_(char *, integer *, integer *, ftnlen);
    static integer typids[127];
    extern /* Subroutine */ int pcpool_(char *, integer *, char *, ftnlen, 
	    ftnlen), kplfrm_(integer *, integer *), shelli_(integer *, 
	    integer *);

/* $ Abstract */

/*     This test family checks out the frame ID fetch APIs. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     The parameters below form an enumerated list of the recognized */
/*     frame types.  They are: INERTL, PCK, CK, TK, DYN.  The meanings */
/*     are outlined below. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     INERTL      an inertial frame that is listed in the routine */
/*                 CHGIRF and that requires no external file to */
/*                 compute the transformation from or to any other */
/*                 inertial frame. */

/*     PCK         is a frame that is specified relative to some */
/*                 INERTL frame and that has an IAU model that */
/*                 may be retrieved from the PCK system via a call */
/*                 to the routine TISBOD. */

/*     CK          is a frame defined by a C-kernel. */

/*     TK          is a "text kernel" frame.  These frames are offset */
/*                 from their associated "relative" frames by a */
/*                 constant rotation. */

/*     DYN         is a "dynamic" frame.  These currently are */
/*                 parameterized, built-in frames where the full frame */
/*                 definition depends on parameters supplied via a */
/*                 frame kernel. */

/*     ALL         indicates any of the above classes. This parameter */
/*                 is used in APIs that fetch information about frames */
/*                 of a specified class. */


/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 4.0.0, 08-MAY-2012 (NJB) */

/*       The parameter ALL was added to support frame fetch APIs. */

/* -    SPICELIB Version 3.0.0, 28-MAY-2004 (NJB) */

/*       The parameter DYN was added to support the dynamic frame class. */

/* -    SPICELIB Version 2.0.0, 12-DEC-1996 (WLT) */

/*        Various unused frames types were removed and the */
/*        frame time TK was added. */

/* -    SPICELIB Version 1.0.0, 10-DEC-1995 (WLT) */

/* -& */

/*     End of INCLUDE file frmtyp.inc */

/* $ Abstract */

/*     This file contains the number of inertial reference */
/*     frames that are currently known by the SPICE toolkit */
/*     software. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of known inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of recognized inertial reference */
/*                frames.  This value is needed by both CHGIRF */
/*                ZZFDAT, and FRAMEX. */

/* $ Author_and_Institution */

/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Abstract */

/*     This file contains the number of non-inertial reference */
/*     frames that are currently built into the SPICE toolkit */
/*     software. */


/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of built-in non-inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of built-in non-inertial reference */
/*                frames.  This value is needed by both  ZZFDAT, and */
/*                FRAMEX. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     W.L. Taber      (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.6.0, 30-OCT-2014 (BVS) */

/*        Increased the number of non-inertial frames from 105 to 106 */
/*        in order to accomodate the following PCK based frame: */

/*           IAU_BENNU */

/* -    SPICELIB Version 1.5.0, 11-OCT-2011 (BVS) */

/*        Increased the number of non-inertial frames from 100 to 105 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CERES */
/*           IAU_PALLAS */
/*           IAU_LUTETIA */
/*           IAU_DAVIDA */
/*           IAU_STEINS */

/* -    SPICELIB Version 1.4.0, 11-MAY-2010 (BVS) */

/*        Increased the number of non-inertial frames from 96 to 100 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_BORRELLY */
/*           IAU_TEMPEL_1 */
/*           IAU_VESTA */
/*           IAU_ITOKAWA */

/* -    SPICELIB Version 1.3.0, 12-DEC-2002 (BVS) */

/*        Increased the number of non-inertial frames from 85 to 96 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CALLIRRHOE */
/*           IAU_THEMISTO */
/*           IAU_MAGACLITE */
/*           IAU_TAYGETE */
/*           IAU_CHALDENE */
/*           IAU_HARPALYKE */
/*           IAU_KALYKE */
/*           IAU_IOCASTE */
/*           IAU_ERINOME */
/*           IAU_ISONOE */
/*           IAU_PRAXIDIKE */

/* -    SPICELIB Version 1.2.0, 02-AUG-2002 (FST) */

/*        Increased the number of non-inertial frames from 81 to 85 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_PAN */
/*           IAU_GASPRA */
/*           IAU_IDA */
/*           IAU_EROS */

/* -    SPICELIB Version 1.1.0, 20-FEB-1997 (WLT) */

/*        Increased the number of non-inertial frames from 79 to 81 */
/*        in order to accomodate the following earth rotation */
/*        models: */

/*           ITRF93 */
/*           EARTH_FIXED */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the frame ID code fetch APIs listed below: */

/*        BLTFRM */
/*        KPLFRM */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 09-FEB-2016 (NJB) */

/* -       22-JUN-2015 (NJB) */

/*        Updated to exercise KPLFRM buffering logic. */

/* -    TSPICE Version 1.1.0, 09-AUG-2013 (BVS) */

/*        Updated for changed ZZFDAT calling sequence. */

/* -    TSPICE Version 1.0.0, 23-MAY-2012 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Built-in frame hashes returned by ZZFDAT. */


/*     Saved variables */

/*     Save all variables to avoid CSPICE stack overflow */
/*     problems. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_FRFTCH", (ftnlen)8);
/* ********************************************************************** */


/*     BLTFRM test cases */


/* ********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("BLTFRM test setup: get data for built-in frames.", (ftnlen)48);

/*     Fetch built-in frame attributes. */

    ncount = 127;
    zzfdat_(&ncount, &c__128, names, idcdes, ctrs, types, typids, centrd, 
	    bnmlst, bnmpol, bnmnms, bnmidx, bidlst, bidpol, bidids, bididx, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize sets. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__127, xidset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */


/*     Check the ability of BLTFRM to fetch all IDs of built-in */
/*     frames of a given class. */

    for (clsidx = 1; clsidx <= 5; ++clsidx) {
	s_copy(title, "BLTFRM test: fetch class <class> built-in frames.", (
		ftnlen)400, (ftnlen)49);
	repmi_(title, "<class>", &clsidx, title, (ftnlen)400, (ftnlen)7, (
		ftnlen)400);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

	tcase_(title, (ftnlen)400);

/*        Empty the sets. */

	scardi_(&c__0, idset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	scardi_(&c__0, xidset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the expected result set. */

	n = 0;
	i__1 = ncount;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (types[(i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : s_rnge(
		    "types", i__2, "f_frftch__", (ftnlen)295)] == clsidx) {
		appndi_(&idcdes[(i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : 
			s_rnge("idcdes", i__2, "f_frftch__", (ftnlen)297)], 
			xidset);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		++n;
	    }
	}
	validi_(&c__127, &n, xidset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Now fetch from BLTFRM the built-in frames of class CLSIDX. */

	bltfrm_(&clsidx, idset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check set cardinality first. */

	i__1 = cardi_(idset);
	i__2 = cardi_(xidset);
	chcksi_("Card(IDSET)", &i__1, "=", &i__2, &c__0, ok, (ftnlen)11, (
		ftnlen)1);
	if (*ok) {

/*           Check set contents. */

	    i__1 = cardi_(idset);
	    chckai_("IDSET", idset, "=", xidset, &i__1, ok, (ftnlen)5, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */


/*     Check the ability of BLTFRM to fetch all IDs of built-in */
/*     frames. */

    s_copy(title, "BLTFRM test: fetch IDs of all built-in frames.", (ftnlen)
	    400, (ftnlen)46);

/* ---- Case ------------------------------------------------------------- */

    tcase_(title, (ftnlen)400);

/*     Empty the sets. */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardi_(&c__0, xidset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the expected result set. */

    n = ncount;
    movei_(idcdes, &ncount, &xidset[6]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    validi_(&c__127, &n, xidset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now fetch the inertial, built-in frames from BLTFRM. */

    bltfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check set cardinality first. */

    i__1 = cardi_(idset);
    i__2 = cardi_(xidset);
    chcksi_("Card(IDSET)", &i__1, "=", &i__2, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);
    if (*ok) {

/*        Check set contents. */

	i__1 = cardi_(idset);
	chckai_("IDSET", idset, "=", xidset, &i__1, ok, (ftnlen)5, (ftnlen)1);
    }


/*     BLTFRM error cases */



/* ---- Case ------------------------------------------------------------- */

    tcase_("BLTFRM: bad frame class.", (ftnlen)24);

/*     Class 0: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bltfrm_(&c__0, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/*     Class -2: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bltfrm_(&c_n2, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/*     Class 6: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bltfrm_(&c__6, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("BLTFRM: output set too small.", (ftnlen)29);

/*     Set the frame ID set size to 3. */

    ssizei_(&c__3, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try to fetch the built-in inertial frames. */

    bltfrm_(&c__1, idset);
    chckxc_(&c_true, "SPICE(SETTOOSMALL)", ok, (ftnlen)18);
/* ********************************************************************** */


/*     KPLFRM test cases */


/* ********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: setup: insert frame specs into pool.", (ftnlen)44);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__2 = nclass[(i__1 = clsidx - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"nclass", i__1, "f_frftch__", (ftnlen)468)];
	for (i__ = 1; i__ <= i__2; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create a frame name assignment and insert this */
/*           into the pool. */

	    s_copy(kvname, "FRAME_<ID>_NAME", (ftnlen)32, (ftnlen)15);
	    repmi_(kvname, "<ID>", &frmid, kvname, (ftnlen)32, (ftnlen)4, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    pcpool_(kvname, &c__1, frname, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class into the pool. */

	    s_copy(kvname, "FRAME_<name>_CLASS", (ftnlen)32, (ftnlen)18);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclss = clsidx;
	    pipool_(kvname, &c__1, &frclss, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class ID into the pool. Make */
/*           the class ID the negative of the ID. */

	    s_copy(kvname, "FRAME_<name>_CLASS_ID", (ftnlen)32, (ftnlen)21);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclid = -frmid;
	    pipool_(kvname, &c__1, &frclid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame center into the pool. Make */
/*           the center 10x the ID. */

	    s_copy(kvname, "FRAME_<name>_CENTER", (ftnlen)32, (ftnlen)19);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frcent = frmid * 10;
	    pipool_(kvname, &c__1, &frcent, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */


/*     Check the ability of KPLFRM to fetch all IDs of kernel pool */
/*     frames of a given class. */

    for (clsidx = 1; clsidx <= 5; ++clsidx) {
	s_copy(title, "KPLFRM test: fetch class <class> kernel pool frames.", 
		(ftnlen)400, (ftnlen)52);
	repmi_(title, "<class>", &clsidx, title, (ftnlen)400, (ftnlen)7, (
		ftnlen)400);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

	tcase_(title, (ftnlen)400);

/*        Initialize the sets. */

	ssizei_(&c__127, idset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ssizei_(&c__127, xidset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the expected result set. */

	n = 0;
	i__1 = nclass[(i__2 = clsidx - 1) < 5 && 0 <= i__2 ? i__2 : s_rnge(
		"nclass", i__2, "f_frftch__", (ftnlen)572)];
	for (i__ = 1; i__ <= i__1; ++i__) {
	    frcode = clsidx * 100000 + i__;
	    appndi_(&frcode, xidset);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ++n;
	}
	validi_(&c__127, &n, xidset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Now fetch from KPLFRM the IDs of the kernel pool frames */
/*        of class CLSIDX. */

	kplfrm_(&clsidx, idset);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check set cardinality first. */

	i__1 = cardi_(idset);
	i__2 = cardi_(xidset);
	chcksi_("Card(IDSET)", &i__1, "=", &i__2, &c__0, ok, (ftnlen)11, (
		ftnlen)1);
	if (*ok) {

/*           Check set contents. */

	    i__1 = cardi_(idset);
	    chckai_("IDSET", idset, "=", xidset, &i__1, ok, (ftnlen)5, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */


/*     Check the ability of KPLFRM to fetch all IDs of kernel pool */
/*     frames. */

    tcase_("KPLFRM: fetch IDs of all loaded frames.", (ftnlen)39);

/*     Empty the sets. */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardi_(&c__0, xidset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Form a set containing the IDs of all loaded frames. */

    n = 0;
    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__2 = nclass[(i__1 = clsidx - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"nclass", i__1, "f_frftch__", (ftnlen)638)];
	for (i__ = 1; i__ <= i__2; ++i__) {

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;
	    appndi_(&frmid, xidset);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ++n;
	}
    }
    validi_(&c__127, &n, xidset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now fetch from KPLFRM the IDs of the kernel pool frames. */

    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check set cardinality first. */

    i__2 = cardi_(idset);
    i__1 = cardi_(xidset);
    chcksi_("Card(IDSET)", &i__2, "=", &i__1, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);
    if (*ok) {

/*        Check set contents. */

	i__2 = cardi_(idset);
	chckai_("IDSET", idset, "=", xidset, &i__2, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Non-error exceptions: */


/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled on an attempt to fetch ID"
	    "s of class 1 frames. The returned set should be empty.", (ftnlen)
	    118);
    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c__1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check set cardinality. */

    i__2 = cardi_(idset);
    chcksi_("Card(IDSET)", &i__2, "=", &c__0, &c__0, ok, (ftnlen)11, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled and no frames are found i"
	    "f only FRAME_<name> assignments are present.", (ftnlen)108);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__1 = nclass[(i__2 = clsidx - 1) < 5 && 0 <= i__2 ? i__2 : s_rnge(
		"nclass", i__2, "f_frftch__", (ftnlen)721)];
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Fetch all of the frames. We shouldn't find any. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(idset);
    chcksi_("CARDI(IDSET)", &i__1, "=", &c__0, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled and no frames are found i"
	    "f only FRAME_<name> and FRAME_<ID code>_NAME assignments are pre"
	    "sent.", (ftnlen)133);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__2 = nclass[(i__1 = clsidx - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"nclass", i__1, "f_frftch__", (ftnlen)780)];
	for (i__ = 1; i__ <= i__2; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create a frame name assignment and insert this */
/*           into the pool. */

	    s_copy(kvname, "FRAME_<ID>_NAME", (ftnlen)32, (ftnlen)15);
	    repmi_(kvname, "<ID>", &frmid, kvname, (ftnlen)32, (ftnlen)4, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    pcpool_(kvname, &c__1, frname, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Fetch all of the frames. We shouldn't find any. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__2 = cardi_(idset);
    chcksi_("CARDI(IDSET)", &i__2, "=", &c__0, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled and no frames are found i"
	    "f only FRAME_<name> and FRAME_<name>_CLASS assignments are prese"
	    "nt.", (ftnlen)131);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__1 = nclass[(i__2 = clsidx - 1) < 5 && 0 <= i__2 ? i__2 : s_rnge(
		"nclass", i__2, "f_frftch__", (ftnlen)851)];
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class into the pool. */

	    s_copy(kvname, "FRAME_<name>_CLASS", (ftnlen)32, (ftnlen)18);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclss = clsidx;
	    pipool_(kvname, &c__1, &frclss, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Fetch all of the frames. We shouldn't find any. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(idset);
    chcksi_("CARDI(IDSET)", &i__1, "=", &c__0, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled and no frames are found i"
	    "f only FRAME_<name> and FRAME_<ID code>_CLASS assignments are pr"
	    "esent.", (ftnlen)134);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__2 = nclass[(i__1 = clsidx - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"nclass", i__1, "f_frftch__", (ftnlen)922)];
	for (i__ = 1; i__ <= i__2; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class into the pool. */

	    s_copy(kvname, "FRAME_<code>_CLASS", (ftnlen)32, (ftnlen)18);
	    repmi_(kvname, "<code>", &frmid, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclss = clsidx;
	    pipool_(kvname, &c__1, &frclss, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Fetch all of the frames. We shouldn't find any. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__2 = cardi_(idset);
    chcksi_("CARDI(IDSET)", &i__2, "=", &c__0, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: make sure no error is signaled and no frames are found i"
	    "f FRAME_<name> and FRAME_<ID code>_NAME assignments are present "
	    "and the frame names don't match.", (ftnlen)160);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of frame specifications and insert these into */
/*     the kernel pool. Frame classes 2-5 will be represented in */
/*     this set. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__1 = nclass[(i__2 = clsidx - 1) < 5 && 0 <= i__2 ? i__2 : s_rnge(
		"nclass", i__2, "f_frftch__", (ftnlen)995)];
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert a non-matching frame name into the pool. */

	    s_copy(kvname, "FRAME_<code>_NAME", (ftnlen)32, (ftnlen)17);
	    repmi_(kvname, "<code>", &frmid, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    pcpool_(kvname, &c__1, "XXX", (ftnlen)32, (ftnlen)3);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Fetch all of the frames. We shouldn't find any. */

    ssizei_(&c__127, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(idset);
    chcksi_("CARDI(IDSET)", &i__1, "=", &c__0, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check frame buffer handling for large frame counts.", (ftnlen)51);
    ssizei_(&c__5000, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Insert a large number of frame specs into the kernel pool. */
/*     The specs don't have to be complete---only the parts checked */
/*     by KPLFRM are needed. */

    bign = 5000;
    i__1 = bign;
    for (i__ = 1; i__ <= i__1; ++i__) {
	clsidx = i__ % 3 + 1;
	s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)25);
	repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (ftnlen)32)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create an ID code for this frame. */

	frmid = clsidx * 100000 + i__;

/*        Insert the frame ID assignment into the pool. */

	pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Store the frame ID we just created. */

	idlist[(i__2 = i__ - 1) < 5000 && 0 <= i__2 ? i__2 : s_rnge("idlist", 
		i__2, "f_frftch__", (ftnlen)1092)] = frmid;

/*        Insert the assignment of the frame name into */
/*        the kernel pool. */

	s_copy(kvname, "FRAME_<code>_NAME", (ftnlen)32, (ftnlen)17);
	repmi_(kvname, "<code>", &frmid, kvname, (ftnlen)32, (ftnlen)6, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pcpool_(kvname, &c__1, frname, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Insert the frame's class into the pool. */

	s_copy(kvname, "FRAME_<code>_CLASS", (ftnlen)32, (ftnlen)18);
	repmi_(kvname, "<code>", &frmid, kvname, (ftnlen)32, (ftnlen)6, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_(kvname, &c__1, &clsidx, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Now fetch the frame names. */

    kplfrm_(&c_n1, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__ = cardi_(idset);
    chcksi_("IDSET card", &i__, "=", &bign, &c__0, ok, (ftnlen)10, (ftnlen)1);

/*     Sort the stored ID codes. */

    shelli_(&bign, idlist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure that the stored ID set matches the */
/*     set returned by KPLFRM. */

    chckai_("IDSET", &idset[6], "=", idlist, &bign, ok, (ftnlen)5, (ftnlen)1);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: restore frame specs.", (ftnlen)27);

/*     Restore the frame specifications for use in later test cases. */

    for (clsidx = 2; clsidx <= 5; ++clsidx) {
	i__2 = nclass[(i__1 = clsidx - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"nclass", i__1, "f_frftch__", (ftnlen)1153)];
	for (i__ = 1; i__ <= i__2; ++i__) {
	    s_copy(frname, "FRCLASS_<class>_FRAME_<n>", (ftnlen)32, (ftnlen)
		    25);
	    repmi_(frname, "<class>", &clsidx, frname, (ftnlen)32, (ftnlen)7, 
		    (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(frname, "<n>", &i__, frname, (ftnlen)32, (ftnlen)3, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(kvname, "FRAME_<name>", (ftnlen)32, (ftnlen)12);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create an ID code for this frame. */

	    frmid = clsidx * 100000 + i__;

/*           Insert the frame ID assignment into the pool. */

	    pipool_(kvname, &c__1, &frmid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Create a frame name assignment and insert this */
/*           into the pool. */

	    s_copy(kvname, "FRAME_<ID>_NAME", (ftnlen)32, (ftnlen)15);
	    repmi_(kvname, "<ID>", &frmid, kvname, (ftnlen)32, (ftnlen)4, (
		    ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    pcpool_(kvname, &c__1, frname, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class into the pool. */

	    s_copy(kvname, "FRAME_<name>_CLASS", (ftnlen)32, (ftnlen)18);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclss = clsidx;
	    pipool_(kvname, &c__1, &frclss, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame class ID into the pool. Make */
/*           the class ID the negative of the ID. */

	    s_copy(kvname, "FRAME_<name>_CLASS_ID", (ftnlen)32, (ftnlen)21);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frclid = -frmid;
	    pipool_(kvname, &c__1, &frclid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Insert the frame center into the pool. Make */
/*           the center 10x the ID. */

	    s_copy(kvname, "FRAME_<name>_CENTER", (ftnlen)32, (ftnlen)19);
	    repmc_(kvname, "<name>", frname, kvname, (ftnlen)32, (ftnlen)6, (
		    ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    frcent = frmid * 10;
	    pipool_(kvname, &c__1, &frcent, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     KPLFRM error cases */


/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: bad frame class.", (ftnlen)24);

/*     Class 0: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c__0, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/*     Class -2: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c_n2, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/*     Class 6: */

    scardi_(&c__0, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kplfrm_(&c__6, idset);
    chckxc_(&c_true, "SPICE(BADFRAMECLASS)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("KPLFRM: output set too small.", (ftnlen)29);

/*     Set the frame ID set size to 3. */

    ssizei_(&c__3, idset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Try to fetch all kernel pool frames. */

    kplfrm_(&c_n1, idset);
    chckxc_(&c_true, "SPICE(SETTOOSMALL)", ok, (ftnlen)18);
    t_success__(ok);
    return 0;
} /* f_frftch__ */

