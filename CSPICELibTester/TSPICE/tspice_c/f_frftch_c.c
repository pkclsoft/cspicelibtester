/*

-Procedure f_frftch_c ( Test wrappers for frame fetch routines )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the frame ID fetch routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include <string.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_frftch_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for a subset of the CSPICE frame
   system routines. 
   
   The subset is:
      
      bltfrm_c
      kplfrm_c
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   B.V. Semenov    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 07-SEP-2013 (NJB)(BVS)

-Index_Entries

   test framex wrappers

-&
*/

{ /* Begin f_frftch_c */

 
   /*
   Constants
   */
   #define FRNMLN          33
   #define KVNMLN          33
   #define LNSIZE         401
   #define NCL              6
   #define MAXBLT      (  SPICE_NFRAME_NNINRT  \
                        + SPICE_NFRAME_NINERT )
   #define MAXBFR      (  MAXBLT + 1 )
   #define MAXFRM        1000

   /*
   Local variables
   */
   SPICEDOUBLE_CELL      ( wrong,  MAXFRM );
   SPICEINT_CELL         ( idset,  MAXFRM );
   SPICEINT_CELL         ( xidset, MAXFRM );
   
   SpiceChar               frname [ FRNMLN ];
   SpiceChar               kvname [ KVNMLN ];
   SpiceChar               names  [ MAXBLT ][ FRNMLN ];
   SpiceChar               title  [ LNSIZE ];

   SpiceInt                centrd [ MAXBLT ];
   SpiceInt                clsidx;
   SpiceInt                ctrs   [ MAXBLT ];
   SpiceInt                frcent;
   SpiceInt                frclid;
   SpiceInt                frclss;
   SpiceInt                frcode;
   SpiceInt                frmid;
   SpiceInt                i;
   SpiceInt                idcdes [ MAXFRM ]; 
   SpiceInt                n;
   static SpiceInt         nclass [ NCL ] = { 0, 0, 5, 6, 7, 8 };
   SpiceInt                ncount;
   SpiceInt                ncont2;
   SpiceInt                types  [ MAXBLT ];
   SpiceInt                typids [ MAXBLT ];

   SpiceInt                bnmlst [ MAXBFR ];
   SpiceInt                bnmpol [ MAXBFR + 6 ];
   SpiceChar               bnmnms [ MAXBFR ][ FRNMLN ];
   SpiceInt                bnmidx [ MAXBFR ];

   SpiceInt                bidlst [ MAXBFR ];
   SpiceInt                bidpol [ MAXBFR + 6 ];
   SpiceInt                bidids [ MAXBFR ];
   SpiceInt                bididx [ MAXBFR ];

   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_frftch_c" );
   
 

   /*
   **********************************************************************
 
 
      SpiceFrm.h test cases
 
 
   **********************************************************************
   */


   /*
   Check frame class parameter values. 
   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_INERTL" );

   chcksi_c ( "SPICE_FRMTYP_INERTL", SPICE_FRMTYP_INERTL, "=", 1, 0, ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_PCK" );

   chcksi_c ( "SPICE_FRMTYP_PCK", SPICE_FRMTYP_PCK, "=", 2, 0, ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_CK" );

   chcksi_c ( "SPICE_FRMTYP_CK", SPICE_FRMTYP_CK, "=", 3, 0, ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_TK" );

   chcksi_c ( "SPICE_FRMTYP_TK", SPICE_FRMTYP_TK, "=", 4, 0, ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_DYN" );

   chcksi_c ( "SPICE_FRMTYP_DYN", SPICE_FRMTYP_DYN, "=", 5, 0, ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Check SPICE_FRMTYP_ALL" );

   chcksi_c ( "SPICE_FRMTYP_ALL", SPICE_FRMTYP_ALL, "=", -1, 0, ok );



   /*
   **********************************************************************
 
 
      bltfrm_c test cases
 
 
   **********************************************************************
   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "bltfrm_c test setup: get data for built-in frames." );


   /*
   Fetch built-in frame attributes.
   */
   ncount = MAXBLT;
   ncont2 = MAXBFR;

   /*
   zzfdat_  ( (integer   * ) &ncount,
              (char      * ) names,
              (integer   * ) idcdes,
              (integer   * ) ctrs,
              (integer   * ) types,
              (integer   * ) typids,
              (integer   * ) norder,
              (integer   * ) corder,
              (integer   * ) centrd,
              (ftnlen      ) FRNMLN-1  );
   */

   zzfdat_  ( (integer   * ) &ncount, 
              (integer   * ) &ncont2, 
              (char      * ) names,
              (integer   * ) idcdes,
              (integer   * ) ctrs,
              (integer   * ) types,
              (integer   * ) typids,
              (integer   * ) centrd,
              (integer   * ) bnmlst,
              (integer   * ) bnmpol,
              (char      * ) bnmnms,
              (integer   * ) bnmidx,
              (integer   * ) bidlst,
              (integer   * ) bidpol,
              (integer   * ) bidids,
              (integer   * ) bididx, 
              (ftnlen      ) FRNMLN-1, 
              (ftnlen      ) FRNMLN-1 );

   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   ---- Case -------------------------------------------------------------
   */

   /*
   Check the ability of bltfrm_c to fetch all IDs of built-in
   frames of a given class.
   */

   for ( clsidx = 1;  clsidx <= 5;  clsidx++ )
   {
      strncpy ( title, 
                "bltfrm_c test: fetch class <class> built-in frames.",
                LNSIZE                                               );

      repmi_c ( title, "<class>", clsidx, LNSIZE, title );
      chckxc_c ( SPICEFALSE, " ", ok );


      /*
      ---- Case -------------------------------------------------------------
      */
      tcase_c ( title );

      
      /*
      Get the expected result set. 
      */
      scard_c ( 0, &xidset );
      chckxc_c ( SPICEFALSE, " ", ok );

      n = 0;

      for ( i = 0;  i < ncount;  i++ )
      {
         if ( types[i] == clsidx )
         {
            appndi_c ( idcdes[i], &xidset );
            chckxc_c ( SPICEFALSE, " ", ok );

            ++n;
         }
      }

      valid_c ( MAXBLT, n, &xidset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Now fetch from bltfrm_c the built-in frames of class CLSIDX.
      */
      bltfrm_c ( clsidx, &idset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check set cardinality first.
      */
      chcksi_c ( "Card(idset)", card_c(&idset ), "=", 
                                card_c(&xidset), 0,   ok );
      if ( ok ) 
      {
         /*
         Check set contents.
         */
         chckai_c ( "IDSET", (SpiceInt *)(idset.data ), "=",
                             (SpiceInt *)(xidset.data), card_c(&idset), ok );
      }
   }   



   /*
   ---- Case -------------------------------------------------------------
   */

   /*
   Check the ability of bltfrm_c to fetch all IDs of all built-in
   frames.
   */
   tcase_c ( "BLTFRM test: fetch IDs of all built-in frames." );

   /*
   Get the expected result set. 
   */
   scard_c ( 0, &xidset );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i = 0;  i < ncount;  i++ )
   {
      appndi_c ( idcdes[i], &xidset );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   valid_c ( MAXBLT, ncount, &xidset );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Now fetch from bltfrm_c the IDs of all built-in frames.
   */
   bltfrm_c ( SPICE_FRMTYP_ALL, &idset );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check set cardinality first.
   */
   chcksi_c ( "Card(idset)", card_c(&idset ), "=", 
                             card_c(&xidset), 0,   ok );
   if ( ok ) 
   {
      /*
      {
         int j;
         for ( j = 0;  j < card_c(&idset);  j++ )
         {
            printf ( "idset[%ld] = %ld\n", 
                     (long)j,
                     (long)(  ((SpiceInt *)(idset.data))[j]  )  );
         }
      }
      */



      /*
      Check set contents.
      */
      chckai_c ( "IDSET", (SpiceInt *)(idset.data ), "=",
                          (SpiceInt *)(xidset.data), card_c(&idset), ok );
   }


   /*

   bltfrm_c error cases 

   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "bltfrm_c: invalid set data type." );

   bltfrm_c ( SPICE_FRMTYP_ALL, &wrong );
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "bltfrm_c: set too small." );

   ssize_c ( 2, &idset );
   chckxc_c ( SPICEFALSE, " ", ok );

   bltfrm_c ( SPICE_FRMTYP_ALL, &idset );
   chckxc_c ( SPICETRUE, "SPICE(SETTOOSMALL)", ok );








   
   /*
   **********************************************************************
 
 
      kplfrm_c test cases
 
 
   **********************************************************************
   */
 
 
   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "kplfrm_c: setup: insert frame specs into pool." );

   /*
   Create a set of frame specifications and insert these into
   the kernel pool. Frame classes 2-5 will be represented in
   this set.
   */

   for ( clsidx = 2;  clsidx <= 5;  clsidx++ )
   {
      for ( i = 1;  i <= nclass[clsidx];  i++ )
      {
         strncpy ( frname, "FRCLASS_<class>_FRAME_<n>", FRNMLN );
         repmi_c ( frname, "<class>", clsidx, FRNMLN, frname );
         chckxc_c ( SPICEFALSE, " ", ok );

         repmi_c ( frname, "<n>", i, FRNMLN, frname );
         chckxc_c ( SPICEFALSE, " ", ok );

         strncpy ( kvname, "FRAME_<name>", KVNMLN );
         repmc_c ( kvname, "<name>",  frname, KVNMLN, kvname );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         printf ( "%s \n", kvname );
         */

         /*
         Create an ID code for this frame.
         */
         frmid = ( 100000 * clsidx ) + i;

         /*
         Insert the frame ID assignment into the pool.
         */      
         pipool_c ( kvname, 1, &frmid );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Create a frame name assignment and insert this 
         into the pool.
         */
         strncpy ( kvname, "FRAME_<ID>_NAME", KVNMLN );
         repmi_c ( kvname, "<ID>",  frmid, KVNMLN, kvname );
         chckxc_c ( SPICEFALSE, " ", ok );

         pcpool_c ( kvname, 1, FRNMLN, frname );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Insert the frame class into the pool.
         */
         strncpy ( kvname, "FRAME_<ID>_CLASS", KVNMLN );
         repmi_c ( kvname, "<ID>",  frmid, KVNMLN, kvname );
         chckxc_c ( SPICEFALSE, " ", ok );

         frclss = clsidx;
         pipool_c ( kvname, 1, &frclss );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Insert the frame class ID into the pool. Make
         the class ID the negative of the ID.
         */
         strncpy ( kvname, "FRAME_<ID>_CLASS_ID", KVNMLN );
         repmi_c ( kvname, "<ID>",  frmid, KVNMLN, kvname );
         chckxc_c ( SPICEFALSE, " ", ok );

         frclid = -frmid;
         pipool_c ( kvname, 1, &frclid );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Insert the frame center into the pool. Make
         the center 10x the ID.
         */
         strncpy ( kvname, "FRAME_<name>_CENTER", KVNMLN );
         repmc_c ( kvname, "<name>",  frname, KVNMLN, kvname );
         chckxc_c ( SPICEFALSE, " ", ok );

         frcent = 10 * frmid;
         pipool_c ( kvname, 1, &frcent );
         chckxc_c ( SPICEFALSE, " ", ok );

      }

   }  

 
   /*
   ---- Case -------------------------------------------------------------
   */
 
   /*
   Check the ability of kplfrm_c to fetch all IDs of kernel pool
   frames of a given class.
   */

   for ( clsidx = 1;  clsidx <= 5;  clsidx++ )
   {
 
      strncpy( title,
               "kplfrm_c test: fetch class <class> kernel pool frames.",
               LNSIZE                                                 );
      repmi_c ( title, "<class>", clsidx, LNSIZE, title );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      ---- Case -------------------------------------------------------------
      */
      tcase_c ( title );

      /*
      Initialize the sets. 
      */
      ssize_c ( MAXFRM, &idset  );
      chckxc_c ( SPICEFALSE, " ", ok );

      ssize_c ( MAXFRM, &xidset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Get the expected result set.
      */
      
      n = 0;

      for ( i = 1;  i <= nclass[clsidx];  i++ )
      {
         frcode = ( 100000 * clsidx ) + i;

         appndi_c ( frcode, &xidset );
         chckxc_c ( SPICEFALSE, " ", ok );

         ++n;
      }

      valid_c ( MAXFRM, n, &xidset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Now fetch from kplfrm_c the IDs of the kernel pool frames
      of class `clsidx'.
      */
      kplfrm_c ( clsidx, &idset );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check set cardinality first.
      */
      chcksi_c ( "Card(idset)", card_c(&idset ), "=", 
                                card_c(&xidset), 0,   ok );

      /*
      printf ( "card: %ld \n", (long)(card_c(&idset))  );
      */

      if ( ok ) 
      {

         /*
         {
            int j;
            for ( j = 0;  j < card_c(&idset);  j++ )
            {
               printf ( "idset[%ld] = %ld\n", 
                        (long)j,
                        (long)(  ((SpiceInt *)(idset.data))[j]  )  );
            }
         }
         */

                  
         /*
         Check set contents.
         */
         chckai_c ( "IDSET", (SpiceInt *)(idset.data ), "=",
                             (SpiceInt *)(xidset.data), card_c(&idset), ok );
      }

   }


   /*
   ---- Case -------------------------------------------------------------
   */
 
   /*
   Check the ability of kplfrm_c to fetch the IDs of all loaded frames.
   */

   tcase_c ( "kplfrm_c: fetch IDs of all loaded frames." );


   scard_c ( 0, &xidset );
   chckxc_c ( SPICEFALSE, " ", ok );

   n = 0;

   for ( clsidx = 2;  clsidx <= 5;  clsidx++ )
   {
      for ( i = 1;  i <= nclass[clsidx];  i++ )
      {
         /*
         Create an ID code for this frame.
         */
         frcode = ( 100000 * clsidx ) + i;

         appndi_c ( frcode, &xidset );
         chckxc_c ( SPICEFALSE, " ", ok );

         ++n;
      }
   }
   valid_c ( MAXFRM, n, &xidset );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Now fetch from kplfrm_c the IDs of the kernel pool frames.      
   */
   kplfrm_c ( SPICE_FRMTYP_ALL, &idset );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check set cardinality first.
   */
   chcksi_c ( "Card(idset)", card_c(&idset ), "=", 
                             card_c(&xidset), 0,   ok );
   if ( ok ) 
   {
      /*
      Check set contents.
      */
      chckai_c ( "IDSET", (SpiceInt *)(idset.data ), "=",
                          (SpiceInt *)(xidset.data), card_c(&idset), ok );
   }   


   /*

   kplfrm_c error cases 

   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "kplfrm_c: invalid set data type." );

   kplfrm_c ( SPICE_FRMTYP_ALL, &wrong );
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "kplfrm_c: set too small." );

   ssize_c ( 2, &idset );
   chckxc_c ( SPICEFALSE, " ", ok );

   kplfrm_c ( SPICE_FRMTYP_ALL, &idset );
   chckxc_c ( SPICETRUE, "SPICE(SETTOOSMALL)", ok );





   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_frftch_c */

