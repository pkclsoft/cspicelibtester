/*

-Procedure f_ge02_c ( Test wrappers for geometry routines, subset 2 )

 
-Abstract
 
   Perform tests on CSPICE wrappers for a subset of the geometry 
   routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_ge02_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for a subset of the CSPICE geometry
   routines. 
   
   The subset is:

      sincpt_c
      subpnt_c
      subslr_c
      ilumin_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 2.0.0 10-OCT-2013 (NJB) 
   
      In the sincpt_c case where the intercept is 
      supposed to coincide with the sub-solar point,
      the tolerances for the intercept latitude and
      for the cartesian representation of the point were
      loosened from TIGHT_TOL to MED_TOL.

      Corrected order of header sections.

   -tspice_c Version 1.0.0 31-JAN-2008 (NJB) 


-Index_Entries

   test high_level geometry routines

-&
*/

{ /* Begin f_ge02_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define ERRLEN          321
   #define SPK             "geomtest.bsp"
   
   #define UTC             "1999 Jan 1"
   #define LOOSE_TOL       1.e-7
   #define MED_LOOSE_TOL   1.e-10
   #define MED_TOL         1.e-11
   #define TIGHT_TOL       1.e-12
   #define VTIGHT_TOL      1.e-14
   
   /*
   Local variables
   */
   SpiceBoolean            found;

   SpiceDouble             alt;
   SpiceDouble             dist;
   SpiceDouble             dvec     [3];
   SpiceDouble             emissn;
   SpiceDouble             et;
   SpiceDouble             exppnt   [3];
   SpiceDouble             f;
   SpiceDouble             lat;
   SpiceDouble             lon;
   SpiceDouble             lt;
   SpiceDouble             obspos   [3];
   SpiceDouble             phase;
   SpiceDouble             rad;
   SpiceDouble             radii    [3];
   SpiceDouble             re;
   SpiceDouble             rp;
   SpiceDouble             solar;
   SpiceDouble             spoint   [3];
   SpiceDouble             srfvec   [3];
   SpiceDouble             sunAlt;
   SpiceDouble             sunLat;
   SpiceDouble             sunLon;
   SpiceDouble             sunRad;
   SpiceDouble             sunState [6];
   SpiceDouble             trgepc;
   SpiceDouble             xepoch;
   SpiceDouble             xform    [3][3];
   SpiceDouble             xspoint  [3];
   SpiceDouble             xvec     [3];

   SpiceInt                handle;
   SpiceInt                dim;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_ge02_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   Create and load a PCK file. Delete the file afterwards.
   */
   tstpck_c ( "test.pck", SPICETRUE, SPICEFALSE );
   
   
   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   subpnt_c tests:
   */

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subpnt_c.  Find the sub-solar point of the sun "
             "on the Earth using the INTERCEPT definition."       );
 
 
   str2et_c ( UTC, &et );
   
   subpnt_c  ( "INTERCEPT: ellipsoid", 
               "earth", et, "IAU_EARTH", 
               "NONE",      "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );

   reclat_c ( spoint, &rad, &lon, &lat );

   /*
   Get the state of the sun in Earth bodyfixed coordinates at et.
   */
   spkgeo_c ( 10, et, "IAU_EARTH", 399, sunState, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   reclat_c ( sunState, &sunRad, &sunLon, &sunLat );

   /*
   Make sure the directional coordinates match up.
   */
   chcksd_c ( "Sub point lon", lon, "~/", sunLon, TIGHT_TOL, ok );
   chcksd_c ( "Sub point lat", lat, "~/", sunLat, TIGHT_TOL, ok );
  
   /*
   Use sincpt_c to check spoint, trgepc and srfvec.  
   */
   
   /*
   Map srfvec to the J2000 frame. 
   */
   pxform_c ( "iau_earth", "j2000", trgepc, xform );
   mxv_c    ( xform, srfvec, dvec );
   
   sincpt_c ( "Ellipsoid",
              "earth", et,      "IAU_EARTH",  "NONE",  "sun", "J2000", dvec,
              xspoint, &xepoch, xvec,         &found                       );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( found  )
   {
      /*
      The intercept epochs found by sincpt_c and subpnt_c should
      match. 
      */
      chcksd_c ( "trgepc", trgepc, "~", xepoch, TIGHT_TOL, ok );

      /*
      The intercept surface points found by sincpt_c and subpnt_c should
      match. 
      */
      chckad_c ( "spoint", spoint, "~~/", xspoint, 3, MED_TOL, ok );
   }



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subpnt_c.  Find the sub-solar point of the sun "
             "on the Earth using the NEARPOINT definition."       );
 
 
   str2et_c ( UTC, &et );
   
   subpnt_c  ( "NEARPOINT: ellipsoid", "399", et, "IAU_EARTH", 
               "NONE", "10", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   We'll need the radii of the earth.
   */
   bodvcd_c ( 399, "RADII", 3, &dim, radii );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   re = radii[0];
   rp = radii[2];
   
   f  =  ( re - rp ) / re;
   
   recgeo_c ( spoint, re, f, &lon, &lat, &alt );

   /*
   Get the state of the sun in Earth bodyfixed coordinates at et.
   */
   spkgeo_c ( 10, et, "IAU_EARTH", 399, sunState, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   recgeo_c ( sunState, re, f, &sunLon, &sunLat, &sunAlt );


   /*
   Make sure the directional coordinates match up.
   */
   chcksd_c ( "Sub point lon", lon, "~/", sunLon, TIGHT_TOL, ok );
   chcksd_c ( "Sub point lat", lat, "~/", sunLat, TIGHT_TOL, ok );
  


   /*
   ------ Case -------------------------------------------------------
   */

   tcase_c ( "Test subpnt_c string error checking." );
 
   /*
   Check string error cases:
   
      1) Null variable name.
      2) Empty variable name.
      
   */

   subpnt_c ( NULLCPTR, "earth", et, "IAU_EARTH", 
             "NONE", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   subpnt_c ( "", "earth", et, "IAU_EARTH", 
              "NONE", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
            
   subpnt_c ( "NEARPOINT: ellipsoid", NULLCPTR, et, "IAU_EARTH", 
              "NONE", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpnt_c ( "NEARPOINT: ellipsoid", "", et, "IAU_EARTH", 
              "NONE", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subpnt_c ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH", 
              NULLCPTR, "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpnt_c ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH",
              "", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subpnt_c ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH", 
              "NONE", NULLCPTR, spoint, &trgepc, srfvec);
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subpnt_c ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH", 
              "NONE", "", spoint, &trgepc, srfvec);
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );




   /*
   ilumin_c tests:
   */

   /*
   ------ Case -------------------------------------------------------
   */

   tcase_c ( "Test ilumin_c.  Find the illumination angles on the "
             "earth as seen from the moon, evaluated at the "
             "sub-moon point (NEARPOINT method)."                );
 
 
   subpnt_c  ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH",
              "NONE", "moon", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   ilumin_c ( "Ellipsoid", "earth", et, "IAU_EARTH", "NONE", "moon",  
              spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICEFALSE, " ", ok );
  
  
   /*
   We should have an emission angle of zero.
   */
   chcksd_c ( "Emission angle", emissn, "~", 0.0, TIGHT_TOL, ok );
   
   /*
   The phase angle should match the solar incidence angle.
   */
   chcksd_c ( "Phase angle", phase, "~", solar, TIGHT_TOL, ok );
   

   /*
   Use sincpt_c to check trgepc and srfvec.  
   */
   
   /*
   Map srfvec to the J2000 frame. 
   */
   pxform_c ( "iau_earth", "j2000", trgepc, xform );
   mxv_c    ( xform, srfvec, dvec );
   
   sincpt_c ( "Ellipsoid",
              "earth", et,      "IAU_EARTH", "NONE", "moon", "J2000", dvec,
              xspoint, &xepoch, xvec,        &found                       );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( found  )
   {
      /*
      The intercept epochs found by sincpt_c and subpnt_c should
      match. 
      */
      chcksd_c ( "trgepc", trgepc, "~", xepoch, TIGHT_TOL, ok );

      /*
      The intercept surface points found by sincpt_c and subpnt_c should
      match. 
      */
      chckad_c ( "spoint", spoint, "~~/", xspoint, 3, MED_TOL, ok );
   }




   /*
   ------ Case -------------------------------------------------------
   */

   /*
   Repeat test using integer codes to represent target and
   observer. 
   */
   tcase_c ( "Repeat tests with integer codes." );
 
 
   subpnt_c  ( "NEARPOINT: ellipsoid", "399", et, "IAU_EARTH", 
               "NONE", "301", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   ilumin_c ("Ellipsoid", "399",   et,     "IAU_EARTH", "NONE", "301",  
              spoint,     &trgepc, srfvec, &phase,      &solar, &emissn );
   chckxc_c ( SPICEFALSE, " ", ok );
  
  
   /*
   We should have an emission angle of zero.
   */
   chcksd_c ( "Emission angle", emissn, "~", 0.0, TIGHT_TOL, ok );
   
   /*
   The phase angle should match the solar incidence angle.
   */
   chcksd_c ( "Phase angle", phase, "~", solar, TIGHT_TOL, ok );
   

   /*
   ------ Case -------------------------------------------------------
   */

   tcase_c ( "Test ilumin_c string error checking." );
 
   /*
   Check string error cases:
   
      1) Null variable name.
      2) Empty variable name.
      
   */

   ilumin_c  ( NULLCPTR, "earth", et,     "IAU_EARTH", "NONE",  "sun", 
               spoint,      &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   ilumin_c  ( "",  "earth", et,     "IAU_EARTH", "NONE",  "sun", 
               spoint,      &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

              
   ilumin_c  ( "Ellipsoid", NULLCPTR,  et,      "NONE",  "IAU_EARTH", "sun", 
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   ilumin_c  ( "Ellipsoid", "",  et,     "IAU_EARTH", "NONE",  "sun", 
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
            


   ilumin_c  ( "Ellipsoid", "earth",   et, "IAU_EARTH",  NULLCPTR,  "sun", 
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   ilumin_c  ( "Ellipsoid", "earth",   et, "IAU_EARTH", "",   "sun", 
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   ilumin_c  ( "Ellipsoid", "earth",   et, "IAU_EARTH", "NONE",  NULLCPTR,  
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   

   ilumin_c  ( "Ellipsoid", "earth",   et, "IAU_EARTH", "NONE",  "", 
               spoint,  &trgepc, srfvec, &phase,  &solar,  &emissn );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );





   /*
   subslr_c tests:
   */

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c.  Find the sub-solar point of the sun "
             "on the Earth using the NEARPOINT definition."       );
 
 
   str2et_c ( UTC, &et );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subslr_c ( "NEARPOINT:ellipsoid", "earth", et, "IAU_EARTH",
              "NONE", "sun", spoint, &trgepc, srfvec  );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subpnt_c ( "NEARPOINT: ellipsoid", "earth", et, "IAU_EARTH",
              "NONE", "sun", exppnt, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the surface points match up.
   */
   chckad_c ( "Geometric sub solar point", 
              spoint, 
              "~~/", 
              exppnt, 
              3,
              TIGHT_TOL, 
              ok                         );
  

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Repeat test using integer ID codes." );
 
 
   str2et_c ( UTC, &et );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subslr_c ( "NEARPOINT: ellipsoid", "399", et, "iau_earth",
              "NONE", "10", spoint, &trgepc, srfvec      );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subpnt_c ( "NEARPOINT: ellipsoid", "399", et, "IAU_EARTH",
              "NONE", "10", exppnt, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the surface points match up.
   */
   chckad_c ( "Geometric sub solar point", 
              spoint, 
              "~~/", 
              exppnt, 
              3,
              TIGHT_TOL, 
              ok                         );
  

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c.  Find the sub-solar point of the sun "
             "on the Earth using the INTERCEPT definition."       );
 
   str2et_c ( UTC, &et );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subslr_c ( "INTERCEPT: ellipsoid", "earth", et, "iau_earth",
              "NONE", "sun", spoint, &trgepc, srfvec           );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   subpnt_c ( "INTERCEPT: ellipsoid", "earth", et, "iau_earth",
              "NONE", "sun", exppnt, &trgepc,  srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the surface points match up.
   */
   chckad_c ( "Geometric sub solar point", 
              spoint, 
              "~~/", 
              exppnt, 
              3,
              TIGHT_TOL, 
              ok                         );
  

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c:  make sure the solar incidence angle "
             "at the sub-solar point on the moon as seen from the "
             "earth is zero. Use LT+S correction. Near point method." );


   subslr_c ( "near point: ellipsoid", "moon", et, "iau_moon",
              "LT+S", "earth", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );



   ilumin_c ( "Ellipsoid", "moon",  et,    "IAU_MOON", "LT+S",  "earth",  
              spoint,      &trgepc, srfvec, &phase,    &solar,  &emissn );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   printf ( "near point case: spoint: %21.6e %21.6e %21.6e \n", 
            spoint[0], spoint[1], spoint[2]  );
   */

   chcksd_c ( "solar incidence angle", solar, "~", 0.0, TIGHT_TOL, ok );
   

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c:  make sure the solar incidence angle "
             "at the sub-solar point on the Earth as seen from the "
             "Moon is zero. Use LT+S correction. Near point method." );


   /*
   This case uses an oblate target, so the "near point" definition
   really should give different results than the "intercept"
   definition. Use CN+S corrections to minimize light time errors.
   */
   subslr_c ( "near point: ellipsoid", "earth", et, "iau_earth",
              "CN+S", "moon", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );


   ilumin_c ( "Ellipsoid", "earth",  et,    "IAU_earth", "CN+S",  "moon",  
              spoint,      &trgepc, srfvec, &phase,    &solar,  &emissn );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "solar incidence angle", solar, "~", 0.0, TIGHT_TOL, ok );
   

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c: use sincpt_c to check spoint, trgepc, "
             "and srfvec."                                          );

   subslr_c ( "near point: ellipsoid", "earth", et, "iau_earth",
              "CN+S", "moon", spoint, &trgepc, srfvec );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Map srfvec to the J2000 frame. 
   */
   pxform_c ( "iau_earth", "j2000", trgepc, xform );
   mxv_c    ( xform, srfvec, dvec );
   
   sincpt_c ( "Ellipsoid",
              "earth", et,      "IAU_EARTH",  "CN+S",  "moon", "J2000", dvec,
              xspoint, &xepoch, xvec,         &found                       );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( found  )
   {
      /*
      The intercept epochs found by sincpt_c and subpnt_c should
      match. 
      */
      chcksd_c ( "trgepc", trgepc, "~", xepoch, TIGHT_TOL, ok );

      /*
      The intercept surface points found by sincpt_c and subpnt_c should
      match. 
      */
      chckad_c ( "spoint", spoint, "~~/", xspoint, 3, MED_LOOSE_TOL, ok );
   }


      
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c:  make sure the solar incidence angle "
             "at the sub-solar point on the moon as seen from the "
             "earth is zero. Use LT+S correction. Intercept method." );

   
   subslr_c ( "intercept: ellipsoid", 
              "moon",  et,     "iau_moon", "LT+S", 
              "earth", spoint, &trgepc,     srfvec  );
   chckxc_c ( SPICEFALSE, " ", ok );

   ilumin_c ( "ellipsoid", "moon",  et,     "IAU_MOON", "LT+S",  "earth",  
              spoint,      &trgepc, srfvec, &phase,     &solar,  &emissn );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksd_c ( "solar incidence angle", solar, "~", 0.0, TIGHT_TOL, ok );
   
   
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test subslr_c string error checking." );
 
   /*
   Check string error cases:
   
      1) Null variable name.
      2) Empty variable name.
      
   */

   subslr_c ( NULLCPTR, "earth", et, "iau_earth", "NONE", 
              "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   subslr_c ( "", "earth", et, "iau_earth", "NONE",
              "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
            
   subslr_c ( "Near point: ellipsoid", NULLCPTR, et, "iau_earth", 
              "NONE", "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subslr_c ( "Near point: ellipsoid", "", et, "iau_earth", "NONE",
              "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subslr_c ( "Near point: ellipsoid", "earth", et, "iau_earth", NULLCPTR,
              "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subslr_c ( "Near point: ellipsoid", "earth", et, "iau_earth", "",
              "sun", spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   subslr_c ( "Near point: ellipsoid", "earth", et, "iau_earth", "NONE", 
              NULLCPTR, spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   subslr_c ( "Near point: ellipsoid", "earth", et, "iau_earth", "NONE",
              "",  spoint, &trgepc, srfvec );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );




   /*
   sincpt_c tests:
   */

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test sincpt_c.  Find the sub-solar point of the sun "
             "on the Earth. Compare to subpnt_c using the INTERCEPT "
             "definition."                                           );
 
 
   str2et_c ( UTC, &et );
   
   subpnt_c  ( "INTERCEPT: ellipsoid", 
               "earth", et, "IAU_EARTH", "NONE", "sun", 
               xspoint, &trgepc, srfvec                             );
   chckxc_c ( SPICEFALSE, " ", ok );

   reclat_c ( xspoint, &sunRad, &sunLon, &sunLat );


   /*
   Re-compute using sincpt_c. 
   */
   spkpos_c ( "earth", et, "J2000", "NONE", "sun", dvec, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "NONE", "Sun", "J2000", dvec,
              spoint,  &trgepc, srfvec, &found                      );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( found  )
   {
      reclat_c ( spoint, &rad, &lon, &lat );

      /*
      Make sure the directional coordinates match up.
      */
      chcksd_c ( "surf xpoint lon", lon, "~/", sunLon, TIGHT_TOL, ok );
      chcksd_c ( "surf xpoint lat", lat, "~/", sunLat, MED_TOL,   ok );

      chcksd_c ( "trgepc", trgepc, "~", et, TIGHT_TOL, ok );

      /*
      Check the intercept point error in terms of offset magnitude.
      ( "~~" is the symbol for L2 comparison used by chckad_c.)
      */
      chckad_c ( "spoint", spoint, "~~", xspoint, 3, LOOSE_TOL, ok );
   }

   /*
   Re-compute using integer ID codes.
   */
   spkpos_c ( "399", et, "J2000", "NONE", "10", dvec, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   sincpt_c ( "Ellipsoid",
              "399", et, "IAU_EARTH",  "NONE",  "10",  "J2000", dvec,
              spoint,     &trgepc, srfvec, &found                   );
   chckxc_c ( SPICEFALSE, " ", ok );

   dist = vnorm_c ( srfvec );
   vsub_c ( spoint, srfvec, obspos );

   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( found  )
   {
      reclat_c ( spoint, &rad, &lon, &lat );

      /*
      Make sure the directional coordinates match up.
      */
      chcksd_c ( "surf xpoint lon", lon, "~/", sunLon, TIGHT_TOL,  ok );
      chcksd_c ( "surf xpoint lat", lat, "~/", sunLat, MED_TOL,    ok );

      chcksd_c ( "trgepc", trgepc, "~", et, TIGHT_TOL, ok );

      /*
      Check the intercept point error in terms of offset relative
      magnitude.
      ( "~~/" is the symbol for the relative L2 comparison 
      used by chckad_c.)
      */
      chckad_c ( "spoint", spoint, "~~/", xspoint, 3, MED_TOL, ok );
   }


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test sincpt_c string error checking." );
 
   /*
   Check string error cases:
   
      1) Null variable name.
      2) Empty variable name.
      
   */

   sincpt_c ( NULLCPTR,
              "Earth", et, "IAU_EARTH", "NONE", "Sun", "J2000", dvec,
              spoint,  &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "",
              "Earth", et, "IAU_EARTH", "NONE", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   sincpt_c ( "Ellipsoid",
              NULLCPTR, et, "IAU_EARTH", "NONE", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "Ellipsoid",
              "",  et, "IAU_EARTH", "NONE", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   sincpt_c ( "Ellipsoid",
              "Earth", et, NULLCPTR, "LT", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "", "lt", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", NULLCPTR, "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "", "Sun", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "NONE", NULLCPTR, "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "NONE", "", "J2000", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "NONE", "Sun", NULLCPTR, dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   sincpt_c ( "Ellipsoid",
              "Earth", et, "IAU_EARTH", "NONE",  "Sun",  "", dvec,
              spoint,     &trgepc, srfvec, &found         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( handle );
   TRASH   ( SPK    );
   
   

   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_ge02_c */

