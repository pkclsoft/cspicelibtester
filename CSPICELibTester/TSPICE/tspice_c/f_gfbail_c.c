/*

-Procedure f_gfbail_c ( Test CSPICE GF interrupt handling routines )

 
-Abstract
 
   Perform tests on the CSPICE GF interrupt handling routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <signal.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "SpiceZst.h"
   #include "tutils_c.h"
 


   /*
   File scope parameters 
   */
   #define  TRCMAX         5000 

   /*
   File scope variables 
   */

   /*
   These variables are used for instrumented bail-out
   testing. 
   */
   static SpiceBoolean     bailEnabled;
   static SpiceInt         signalAt;

   static SpiceChar        traceback   [ TRCMAX ];

   static SpiceInt         bailCount;
   static SpiceInt         bailTrigger;




   void f_gfbail_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Test the GF step interrupt handing routines

      gfbail_c
      gfclrh_c
      gfinth_c
      zzgfsavh_c
      zzgfgeth_c

   Also test usage of interrupt handling features by

      gfevnt_c
      gffove_c
      gfocce_c
      
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)
 
-Version

   -tspice_c Version 1.1.0 12-JAN-2012 (EDW)
 
      Edit to 'xtrace' string in the
      
         "Test gfevnt_c: handle  interrupt signal"
      
      test case. The call-back list returned now shows the 
      use of ZZGFRELX instead of ZZGFREL and ZZGFSOLVX 
      instead of ZZGFSOLV.

   -tspice_c Version 1.0.0 29-MAR-2009 (NJB) (EDW)

-Index_Entries

   test CSPICE interrupt handling routines

-&
*/

{ /* Begin f_gfbail_c */

   /*
   Prototypes 
   */
   SpiceBoolean            gfbail_i   ( void );
   SpiceBoolean            zzgfgeth_c ( void );
   void                    zzgfsavh_c ( SpiceBoolean status );

   int                     natik_  ( char    *nameik, 
                                     char    *spk, 
                                     char    *pck, 
                                     logical *loadik, 
                                     logical *keepik, 
                                     ftnlen  nameik_len, 
                                     ftnlen  spk_len, 
                                     ftnlen  pck_len    );

   int                     natpck_ ( char    *file, 
                                     logical *loadpc, 
                                     logical *keeppc, 
                                     ftnlen  file_len );

   int                     natspk_ ( char    *file, 
                                     logical *load, 
                                     integer *handle,
                                     ftnlen  file_len );


   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Local constants 
   */
   #define  MAXWIN         100000
   #define  CNVTOL         1.e-6
   #define  SPK            "f_gfbail_c.bsp"   
   #define  TIMFMT         "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)"
   #define  TIMLEN         41
   #define  IK             "nat.ti"   
   #define  PCK            "nat.tpc"   
   #define  NATSPK         "nat.bsp" 
   #define  PARAMLN        81
   #define  GFEVNT_MAXPAR  20
   
   /*
   Local variables
   */
   static logical          keepik  = SPICETRUE;
   static logical          keeppc  = SPICETRUE;
   static logical          loadik  = SPICETRUE;
   static logical          loadpc  = SPICETRUE;
   static logical          loadspk = SPICETRUE;


   SPICEDOUBLE_CELL      ( cnfine, MAXWIN );
   SPICEDOUBLE_CELL      ( result, MAXWIN );

   SpiceBoolean            qlpars  [GFEVNT_MAXPAR];

   SpiceBoolean            bail;
   SpiceBoolean            rpt;
   SpiceBoolean            status;

   SpiceChar               qcpars  [ GFEVNT_MAXPAR ][ PARAMLN ];
   SpiceChar               qpnams  [ GFEVNT_MAXPAR ][ PARAMLN ];

   SpiceChar             * gquant;
   SpiceChar             * relate;
   SpiceChar             * win0;
   SpiceChar             * win1;
   SpiceChar             * xtrace;

   SpiceDouble             adjust;
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             qdpars [GFEVNT_MAXPAR];
   SpiceDouble             raydir [3] = { 0.0, 0.0, 0.0 };
   SpiceDouble             refval;

   SpiceInt                handle;
   SpiceInt                NatSPKHan;
   SpiceInt                qipars [GFEVNT_MAXPAR];
   SpiceInt                qnpars;



   void                ( * defINTHandler )(int);
   void                ( * handler       )(int);
   void                ( * prevhandler   )(int);
 


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfbail_c" );

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Obtain default SIGINT signal handler." );

   /*
   Pass in a bogus function pointer; it's the return value
   we want. 
   */
   defINTHandler = signal ( SIGINT, (void(*)(int))1 );

   /*
   Make sure `signal' call succeeded: the returned value should
   not be the ANSI C macro SIG_ERR. 
   */
   chcksl_c ( "signal status (0)", (defINTHandler == SIG_ERR),
              SPICEFALSE,          ok                          );


   /*
   Now restore the original handler.
   */
   handler = signal ( SIGINT, defINTHandler );

   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzgfsavh_c/zzgfgeth_c: save and fetch status." );
   

   zzgfsavh_c ( SPICEFALSE );

   chckxc_c ( SPICEFALSE, " ", ok );
 

   /*
   Retrieve status. 
   */
   status = zzgfgeth_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "status (0)", status, SPICEFALSE,  ok );



   zzgfsavh_c ( SPICETRUE );

   chckxc_c ( SPICEFALSE, " ", ok );
 

   /*
   Retrieve status. 
   */
   status = zzgfgeth_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "status (1)", status, SPICETRUE,  ok ); 


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfbail_c/gfclrh_c: fetch/clear status." );


   zzgfsavh_c ( SPICETRUE );

   chckxc_c ( SPICEFALSE, " ", ok );
 

   /*
   Retrieve status. 
   */
   status = gfbail_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "status (0)", status, SPICETRUE,  ok );



   gfclrh_c();

   chckxc_c ( SPICEFALSE, " ", ok );
 

   /*
   Retrieve status. 
   */
   status = gfbail_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "status (1)", status, SPICEFALSE,  ok ); 



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfinth_c: handle interrupt signal." );


   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Make gfinth_c the handler for the signal SIGINT.
   */

   handler = signal ( SIGINT, gfinth_c );

   /*
   Check for an error. 
   */
   chcksl_c ( "signal status", (handler == SIG_ERR),
              SPICEFALSE,      ok                   );



   /*
   Check the interrupt signal handler status. We expect the status
   to be clear.
   */
   status = gfbail_c();
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "gfbail_c return (0)", status, SPICEFALSE,  ok );

   /*
   Raise an interrupt signal. 
   */
   raise ( SIGINT );

   /*
   Check the interrupt signal handler status again. We expect to 
   see that an interrupt was received.
   */
   status = gfbail_c();
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "gfbail_c return (1)", status, SPICETRUE,  ok );


   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the interrupt signal handler status. We expect the status
   to be clear.
   */
   status = gfbail_c();
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "gfbail_c return (2)", status, SPICEFALSE,  ok );

   
   /*
   Restore the original interrupt signal handler. 
   */
   prevhandler = signal ( SIGINT, defINTHandler );

   chcksl_c ( "handler restoration status", (prevhandler == SIG_ERR),
              SPICEFALSE,                    ok                       );




   /*
   "Real" interrupt handling tests follow. 
   */


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   

   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   Create and load a PCK file. Delete the file afterward.
   */
   tstpck_c ( "test.pck", SPICETRUE, SPICEFALSE );
   
   
   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfocce_c: handle interrupt signal." );

   /*
   We'll trigger an interrupt during a root-finding search and
   make sure that gfocce_c aborts when it gets the signal. 
   */

   /*
   Obtain the TDB time bounds of the confinement
   window, which is a single interval in this case.

   We use bounds for an occultation we know can be
   found using our test kernels. The solar eclipse in
   2001 that we'd normally use never happened according
   to these kernels.
   */
   win0 = "2002 NOV 01 00:00:00 TDB";
   win1 = "2002 DEC 01 00:00:00 TDB";

   str2et_c ( win0, &et0 );
   str2et_c ( win1, &et1 );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Insert the time bounds into the confinement
   window.
   */
   wninsd_c ( et0, et1, &cnfine );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Select a 3600-second step. We'll ignore any occultations
   lasting less than 3600 seconds. Use the spoof step size
   static variable rather than the default step size setting
   routine.
   */
   gfsstp_c ( 3600.0 );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Turn on interrupt handling.
   */
   bail = SPICETRUE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICETRUE;
   bailCount     = 0;
   bailTrigger   = 100;
   signalAt      = -1;
   traceback[0]  = (char)0;

   /*
   Since we're not using gfbail_c as our bail-out
   routine, we'll have to establish the SIGINT
   signal handler ourselves. 
   */
   prevhandler = signal ( SIGINT, gfinth_c );

   chcksl_c ( "prevhandler (0)", (prevhandler == SIG_ERR),
              SPICEFALSE,        ok                       );


   /*
   We should be able to raise an interrupt signal without stopping
   tspice_c. 
   */
   status = raise ( SIGINT );

   chcksi_c ( "raise status (0)", status, "=", 0, 0, ok );


   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Perform the search. Use the stand-in bail-out
   test function gfbail_i.
   */
   gfocce_c ( "ANY",                            
              "MOON",     "ellipsoid",  "IAU_MOON", 
              "SUN",      "ellipsoid",  "IAU_SUN",  
              "LT",       "EARTH",      CNVTOL,    
              gfstep_c,   gfrefn_c,     rpt,       
              gfrepi_c,   gfrepu_c,     gfrepf_c, 
              bail,       gfbail_i,     &cnfine,   
              &result                              );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   If the interrupt was handled properly, the signal should have 
   resulted in a call to the CSPICE signal handler gfinth_c at
   call number `bailTrigger'.
   */
   chcksi_c ( "signalAt", signalAt, "=", bailTrigger, 0, ok );

   /*
   Check the location in the call tree when the interrupt was
   received. We expect that zzgfsolv_ was stepping through
   the confinement window when the signal was detected. The
   top of the stack (last name in the message) should be the 
   bail-out routine gfbail_i itself.
   */
   
   xtrace = "tspice_c --> gfocce_c --> GFOCCE --> ZZGFSOLV --> "
            "zzadbail_c --> gfbail_i";

   chcksc_c ( "traceback", traceback, "=", xtrace, ok );

   /*
   We want to use the GF handler only within the GF call tree.
   Make sure that the default SIGINT handler has been restored.
   Since the ANSI C library function `signal' returns the 
   previous handler, we can the set the handler to saved
   default and find out if the previous handler was already
   the default, or if it was something else.
   */
   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfocce_c. Make sure
   it's gfinth_c. The point is to make sure that gfocce_c doesn't
   touch the signal handler before returning when non-SPICE signal
   handling is used.
   */ 
   chcksl_c ( "handler",   (handler == gfinth_c),
              SPICETRUE,   ok                     );




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfocce_c with default interrupt handling. "
             "Make sure default handler is restored." );


   /*
   The whole point of this is to make sure that the handler that
   was in place before gfocce_c was called is restored by the time
   gfocce_c exits. We don't actually raise an exception for this test. 
   */
   gfocce_c ( "ANY",                            
              "MOON",     "ellipsoid",  "IAU_MOON", 
              "SUN",      "ellipsoid",  "IAU_SUN",  
              "LT",       "EARTH",      CNVTOL,    
              gfstep_c,   gfrefn_c,     rpt,       
              gfrepi_c,   gfrepu_c,     gfrepf_c, 
              bail,       gfbail_c,     &cnfine,   
              &result                              );

   chckxc_c ( SPICEFALSE, " ", ok );

   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfocce_c. Make sure
   it's defINTHandler.
   */ 
   chcksl_c ( "handler",   (handler == defINTHandler),
              SPICETRUE,   ok                         );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfocce_c *without* interrupt handling." );

  
   /*
   Make sure the bail-out test routine is never touched. 
   */
   /*
   Turn off interrupt handling.
   */
   bail = SPICEFALSE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICEFALSE;
   bailCount     = 0;


   gfocce_c ( "ANY",                            
              "MOON",     "ellipsoid",  "IAU_MOON", 
              "SUN",      "ellipsoid",  "IAU_SUN",  
              "LT",       "EARTH",      CNVTOL,    
              gfstep_c,   gfrefn_c,     rpt,       
              gfrepi_c,   gfrepu_c,     gfrepf_c, 
              bail,       gfbail_i,     &cnfine,   
              &result                              );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   See how many times gfbail_i was called. The call count
   should be zero.
   */
   chcksi_c ( "bailCount", bailCount, "=", 0, 0, ok );


   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfocce_c. Make sure
   it's defINTHandler.
   */ 
   chcksl_c ( "handler",  (handler == defINTHandler),
              SPICETRUE,  ok                         );




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels for FOV tests." );
   

   /*
   Create and load Nat's solar system SPK, PCK/FK, and IK files.
   */
 

   /*
   Create and load a PCK file. Do NOT delete the file afterward.
   */
   natpck_ ( ( char      * ) PCK,
             ( logical   * ) &loadpc, 
             ( logical   * ) &keeppc, 
             ( ftnlen      ) strlen(PCK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Load an SPK file as well.
   */
   natspk_ ( ( char      * ) NATSPK,
             ( logical   * ) &loadspk, 
             ( integer   * ) &NatSPKHan, 
             ( ftnlen      ) strlen(NATSPK) );   
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load an IK.
   */
   natik_  ( ( char      * ) IK,
             ( char      * ) NATSPK,
             ( char      * ) PCK,
             ( logical   * ) &loadik, 
             ( logical   * ) &keepik, 
             ( ftnlen      ) strlen(IK),
             ( ftnlen      ) strlen(SPK),   
             ( ftnlen      ) strlen(PCK)  );   
   chckxc_c ( SPICEFALSE, " ", ok );




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gffove_c: handle interrupt signal." );

   /*
   We'll trigger an interrupt during a root-finding search and
   make sure that gffove_c aborts when it gets the signal. 
   */

   /*
   Normal search: find appearances of beta in FOV of 

      ALPHA_RECTANGLE_NONE


   Obtain the TDB time bounds of the confinement
   window, which is a single interval in this case.

   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Use a 300 second step. 
   */
   gfsstp_c ( 300.0 );

   /*
   Turn on interrupt handling.
   */
   bail = SPICETRUE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICETRUE;
   bailCount     = 0;
   bailTrigger   = 100;
   signalAt      = -1;
   traceback[0]  = (char)0;

   /*
   Since we're not using gfbail_c as our bail-out
   routine, we'll have to establish the SIGINT
   signal handler ourselves. 
   */
   prevhandler = signal ( SIGINT, gfinth_c );

   chcksl_c ( "prevhandler (0)",  (prevhandler == SIG_ERR),
              SPICEFALSE,         ok                         );


   /*
   We should be able to raise an interrupt signal without stopping
   tspice_c. 
   */
   status = raise ( SIGINT );

   chcksi_c ( "raise status (0)", status, "=", 0, 0, ok );


   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Finally, perform the search. Use the stand-in bail-out
   test function gfbail_i.
   */
   gffove_c ( "ALPHA_RECTANGLE_NONE",  "ellipsoid",
              raydir,     "beta",      "betafixed",
              "none",     "sun",       CNVTOL, 
              gfstep_c,   gfrefn_c,    SPICEFALSE,
              gfrepi_c,   gfrepu_c,    gfrepf_c,
              bail,       gfbail_i,    &cnfine,    &result );

   chckxc_c( SPICEFALSE, " ", ok );

   /*
   If the interrupt was handled properly, the signal should have 
   resulted in a call to the CSPICE signal handler gfinth_c at
   call number `bailTrigger'.
   */
   chcksi_c ( "signalAt", signalAt, "=", bailTrigger, 0, ok );

   /*
   Check the location in the call tree when the interrupt was
   received. We expect that zzgfsolv_ was stepping through
   the confinement window when the signal was detected. The
   top of the stack (last name in the message) should be the 
   bail-out routine gfbail_i itself.
   */
   
   xtrace = "tspice_c --> gffove_c --> GFFOVE --> ZZGFSOLV --> "
            "zzadbail_c --> gfbail_i";

   chcksc_c ( "traceback", traceback, "=", xtrace, ok );

   /*
   We want to use the GF handler only within the GF call tree.
   Make sure that the default SIGINT handler has been restored.
   Since the ANSI C library function `signal' returns the 
   previous handler, we can the set the handler to saved
   default and find out if the previous handler was already
   the default, or if it was something else.
   */
   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfocce_c. Make sure
   it's gfinth_c. The point is to make sure that gfocce_c doesn't
   touch the signal handler before returning when non-SPICE signal
   handling is used.
   */ 
   chcksl_c ( "handler",  (handler == gfinth_c),
              SPICETRUE,  ok                    );




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gffove_c with default interrupt handling. "
             "Make sure default handler is restored." );


   /*
   Use a 300 second step. 
   */
   gfsstp_c ( 300.0 );


   /*
   The whole point of this is to make sure that the handler that
   was in place before gffove_c was called is restored by the time
   gffove_c exits. We don't actually raise an exception for this test. 
   */
   gffove_c ( "ALPHA_RECTANGLE_NONE",  "ellipsoid",
              raydir,     "beta",      "betafixed",
              "none",     "sun",       CNVTOL, 
              gfstep_c,   gfrefn_c,    SPICEFALSE,
              gfrepi_c,   gfrepu_c,    gfrepf_c,
              bail,       gfbail_c,    &cnfine,    &result );
 
   chckxc_c ( SPICEFALSE, " ", ok );

   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gffove_c. Make sure
   it's defINTHandler.
   */ 
   chcksl_c ( "handler",  (handler == defINTHandler),
              SPICETRUE,  ok                            );




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gffove_c *without* interrupt handling." );

  
   /*
   Make sure the bail-out test routine is never touched. 
   */


   /*
   Turn off interrupt handling.
   */
   bail = SPICEFALSE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICEFALSE;
   bailCount     = 0;
 
   gffove_c ( "ALPHA_RECTANGLE_NONE",  "ellipsoid",
              raydir,     "beta",      "betafixed",
              "none",     "sun",       CNVTOL, 
              gfstep_c,   gfrefn_c,    SPICEFALSE,
              gfrepi_c,   gfrepu_c,    gfrepf_c,
              bail,       gfbail_i,    &cnfine,    &result );
 
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   See how many times gfbail_i was called. The call count
   should be zero.
   */
   chcksi_c ( "bailCount", bailCount, "=", 0, 0, ok );


   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gffove_c. Make sure
   it's defINTHandler.
   */ 
   chcksl_c ( "handler",  (handler == defINTHandler),
              SPICETRUE,  ok                            );
  
   
  


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfevnt_c: handle interrupt signal." );

   /*
   We'll trigger an interrupt during a root-finding search and
   make sure that gfevnt_c aborts when it gets the signal. 


   Normal search: find times when angular separation of Alpha
   and Beta is less than 5 degrees.

   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Use a 300 second step. 
   */
   gfsstp_c ( 300.0 );

   /*
   Turn on interrupt handling.
   */
   bail = SPICETRUE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICETRUE;
   bailCount     = 0;
   bailTrigger   = 100;
   signalAt      = -1;
   traceback[0]  = (char)0;

   /*
   Since we're not using gfbail_c as our bail-out
   routine, we'll have to establish the SIGINT
   signal handler ourselves. 
   */
   prevhandler = signal ( SIGINT, gfinth_c );

   chcksl_c ( "prevhandler (0)",  (prevhandler == SIG_ERR),
              SPICEFALSE,         ok                         );


   /*
   We should be able to raise an interrupt signal without stopping
   tspice_c. 
   */
   status = raise ( SIGINT );

   chcksi_c ( "raise status (0)", status, "=", 0, 0, ok );


   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   gquant = "ANGULAR SEPARATION";
   qnpars = 9;

   /*
   Set up other search parameters.
   */

   strncpy ( qpnams[0], "TARGET1",         PARAMLN );
   strncpy ( qpnams[1], "FRAME1",          PARAMLN );
   strncpy ( qpnams[2], "SHAPE1",          PARAMLN );
   strncpy ( qpnams[3], "TARGET2",         PARAMLN );
   strncpy ( qpnams[4], "FRAME2",          PARAMLN );
   strncpy ( qpnams[5], "SHAPE2",          PARAMLN );
   strncpy ( qpnams[6], "OBSERVER",        PARAMLN );
   strncpy ( qpnams[7], "ABCORR",          PARAMLN );
   strncpy ( qpnams[8], "REFERENCE FRAME", PARAMLN );

   strncpy ( qcpars[0], "ALPHA",           PARAMLN );
   strncpy ( qcpars[1], "ALPHAFIXED",      PARAMLN );
   strncpy ( qcpars[2], "SPHERE",          PARAMLN );
   strncpy ( qcpars[3], "BETA",            PARAMLN );
   strncpy ( qcpars[4], "BETAFIXED",       PARAMLN );
   strncpy ( qcpars[5], "SPHERE",          PARAMLN );
   strncpy ( qcpars[6], "SUN",             PARAMLN );
   strncpy ( qcpars[7], "NONE",            PARAMLN );
   strncpy ( qcpars[8], "J2000",           PARAMLN );


   relate = "<";
   refval = 5.0 * rpd_c();
   adjust = 0.0;

   /*
   Finally, perform the search. Use the stand-in bail-out
   test function gfbail_i.
   */
   gfevnt_c ( gfstep_c,   gfrefn_c,   gquant,    qnpars,
              PARAMLN,    qpnams,     qcpars,    qdpars,
              qipars,     qlpars,     relate,    refval,
              CNVTOL,     adjust,     rpt,       gfrepi_c,   
              gfrepu_c,   gfrepf_c,   MAXWIN, 
              bail,       gfbail_i,   &cnfine,   &result          );

   chckxc_c( SPICEFALSE, " ", ok );

   /*
   If the interrupt was handled properly, the signal should have 
   resulted in a call to the CSPICE signal handler gfinth_c at
   call number `bailTrigger'.
   */
   chcksi_c ( "signalAt", signalAt, "=", bailTrigger, 0, ok );

   /*
   Check the location in the call tree when the interrupt was
   received. We expect that zzgfsolv_ was stepping through
   the confinement window when the signal was detected. The
   top of the stack (last name in the message) should be the 
   bail-out routine gfbail_i itself.
   */
   
   xtrace = "tspice_c --> gfevnt_c --> GFEVNT --> ZZGFRELX --> "
            "ZZGFSOLVX --> zzadbail_c --> gfbail_i";

   chcksc_c ( "traceback", traceback, "=", xtrace, ok );

   /*
   We want to use the GF handler only within the GF call tree.
   Make sure that the default SIGINT handler has been restored.
   Since the ANSI C library function `signal' returns the 
   previous handler, we can the set the handler to saved
   default and find out if the previous handler was already
   the default, or if it was something else.
   */
   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfocce_c. Make sure
   it's gfinth_c. The point is to make sure that gfocce_c doesn't
   touch the signal handler before returning when non-SPICE signal
   handling is used.
   */ 
   chcksl_c ( "handler",  (handler == gfinth_c),
              SPICETRUE,  ok                     );

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfevnt_c with default interrupt handling. "
             "Make sure default handler is restored." );


   /*
   Use a 300 second step. 
   */
   gfsstp_c ( 300.0 );


   /*
   The whole point of this is to make sure that the handler that
   was in place before gfevnt_c was called is restored by the time
   gfevnt_c exits. We don't actually raise an exception for this test. 
   */
   gfevnt_c ( gfstep_c,   gfrefn_c,   gquant,    qnpars,
              PARAMLN,    qpnams,     qcpars,    qdpars,
              qipars,     qlpars,     relate,    refval,
              CNVTOL,     adjust,     rpt,       gfrepi_c,   
              gfrepu_c,   gfrepf_c,   MAXWIN,
              bail,       gfbail_i,   &cnfine,   &result          ); 
 
   chckxc_c ( SPICEFALSE, " ", ok );

   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfevnt_c. Make sure
   it's defINTHandler.
   */ 
   chcksl_c ( "handler",  (handler == defINTHandler),
              SPICETRUE,  ok                            );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test gfevnt_c *without* interrupt handling." );

  
   /*
   Make sure the bail-out test routine is never touched. 
   */


   /*
   Turn off interrupt handling.
   */
   bail = SPICEFALSE;
   rpt  = SPICEFALSE;


   /*
   We're almost ready to make the call. Now set up the bail-out
   parameters. Note that these are file scope variables.
   */
   bailEnabled   = SPICEFALSE;
   bailCount     = 0; 
 
   gfevnt_c ( gfstep_c,   gfrefn_c,   gquant,    qnpars,
              PARAMLN,    qpnams,     qcpars,    qdpars,
              qipars,     qlpars,     relate,    refval,
              CNVTOL,     adjust,     rpt,       gfrepi_c,   
              gfrepu_c,   gfrepf_c,   MAXWIN, 
              bail,       gfbail_i,   &cnfine,   &result          ); 

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   See how many times gfbail_i was called. The call count
   should be zero.
   */
   chcksi_c ( "bailCount", bailCount, "=", 0, 0, ok );


   handler = signal ( SIGINT, defINTHandler );

   /*
   Now `handler' is the handler left in place by gfevnt_c. Make sure
   it's defINTHandler.
   */    
   chcksl_c ( "handler",  (handler == defINTHandler),
              SPICETRUE,  ok                            );
 


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK files.
   */
   spkuef_c ( handle );
   TRASH    ( SPK    );

 
   spkuef_c ( NatSPKHan );
   TRASH    ( NATSPK    );
 
     
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 


   
} /* End f_gfbail_c */





/* 
*********************************************************

   Utilities supporting instrumented bail-out testing

********************************************************* 
*/

  
/*
   gfbail_c stand-in
*/

SpiceBoolean gfbail_i()
{

   /*
   Prototypes 
   */
   SpiceBoolean            zzgfgeth_c( void );


   /*
   Local variables 
   */
   SpiceBoolean            status;

   void                  * ptr;


   /*
   Prove we got here. 
   */
   chkin_c ( "gfbail_i" );


   /*
   Increment the call count. 
   */

   ++bailCount;

   /*
   The return status is SPICEFALSE until we discover otherwise. 
   */
   status = SPICEFALSE;

   /*
   If instrumented bail-out handling is enabled, raise a
   SIGINT signal once the call count reaches the preset limit. 
   */

   if ( bailEnabled )
   {
      if ( bailCount == bailTrigger ) 
      {
         ptr = (void *) signal ( SIGINT, gfinth_c );

         raise ( SIGINT );
      }
   

      /*
      Retrieve the interrupt status.
      */
      status = zzgfgeth_c();

      if (  status  &&  ( signalAt < 0 )  )
      {
         /*
         Record the fact that the signal was obtained
         on the current call. 
         */
         signalAt = bailCount;
      }


      if ( status && ( bailCount == bailTrigger )  ) 
      {
         /*
         Retrieve the "quick traceback" message. 
         */
         qcktrc_( traceback, TRCMAX-1 );

         F2C_ConvertStr ( TRCMAX, traceback );
      }

   }

   chkout_c ( "gfbail_i" );

   /*
   Return the interrupt status to the caller. 
   */
   return ( status );
}
