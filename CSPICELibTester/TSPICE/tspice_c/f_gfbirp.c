/* f_gfbirp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__200 = 200;
static integer c__20 = 20;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__20000 = 20000;
static doublereal c_b76 = 0.;
static doublereal c_b107 = 1e-12;
static integer c__3 = 3;

/* $Procedure      F_GFBIRP ( Test GF binary quantity progress reporting ) */
/* Subroutine */ int f_gfbirp__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static integer nivl;
    extern /* Subroutine */ int t_gfrepf__();
    static char inst[36];
    extern /* Subroutine */ int t_gfrepi__();
    static doublereal step;
    extern /* Subroutine */ int t_gfrini__(integer *, integer *, integer *, 
	    integer *, doublereal *, char *, ftnlen), t_gfuini__(void);
    extern /* Subroutine */ int t_gfrepu__();
    extern /* Subroutine */ int t_gfrplo__(integer *, integer *, integer *, 
	    doublereal *), t_gfrtrm__(integer *, integer *, integer *);
    static integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int natik_(char *, char *, char *, logical *, 
	    logical *, ftnlen, ftnlen, ftnlen), repmi_(char *, char *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen)
	    , t_success__(logical *), chckai_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen);
    extern logical gfbail_();
    extern /* Subroutine */ int gfocce_(char *, char *, char *, char *, char *
	    , char *, char *, char *, char *, doublereal *, U_fp, U_fp, 
	    logical *, U_fp, U_fp, U_fp, logical *, L_fp, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), cleard_(integer *, doublereal *), 
	    chcksc_(char *, char *, char *, char *, logical *, ftnlen, ftnlen,
	     ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *);
    static doublereal cnfine[206];
    extern /* Subroutine */ int gfrefn_();
    static char abcorr[5];
    extern /* Subroutine */ int gfstep_();
    static char msglog[78*2*20], obsrvr[36], target[36], tframe[32], tshape[9]
	    ;
    static doublereal cnflog[4120]	/* was [206][20] */, et0, et1, measur,
	     raydir[3], replog[60000]	/* was [3][20000] */, result[206], 
	    xmesur;
    static integer mw, ncalls, nupdat, seqlog[20000], trmlog[20000], xsqlog[
	    20000];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), natpck_(char *, logical *, logical *, ftnlen),
	     natspk_(char *, logical *, integer *, ftnlen), ssized_(integer *,
	     doublereal *), wninsd_(doublereal *, doublereal *, doublereal *),
	     gfsstp_(doublereal *), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), gffove_(char *, 
	    char *, doublereal *, char *, char *, char *, char *, doublereal *
	    , U_fp, U_fp, logical *, U_fp, U_fp, U_fp, logical *, L_fp, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), spkuef_(integer *), delfil_(char *, ftnlen);
    static doublereal tol;
    static logical rpt;
    static integer han2;

/* $ Abstract */

/*     Test the GF binary state quantity progress reporting and */
/*     interrupt handling functionality. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF subsystem's use of the default */
/*     progress reporting and interrupt handling functions. */
/*     The point is to test the callers of the functions, not */
/*     the functions themselves. */

/*     The routines exercised by this test family are */

/*        GFFOVE */
/*        GFOCCE */
/*        ZZGFREL */
/*        ZZGFSOLV */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 08-MAR-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Maximum progress report prefix or suffix length. */
/*     MXMSG is declared in zzgf.inc. */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFBIRP", (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK, PCK/FK, and IK */
/*     files. */

    natpck_("nat.tpc", &c_true, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natik_("nat.ti", "nat.bsp", "nat.tpc", &c_true, &c_false, (ftnlen)6, (
	    ftnlen)7, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Binary state search tests */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run simple occultation search using GFOCCE.", (ftnlen)43);

/*     Search for any occultation of ALPHA by BETA, abcorr = NONE. */


/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfbirp__", (ftnlen)301)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Note: the stellar aberration correction spec should be ignored. */

    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 5 intervals. */

    nivl = 5;
    xmesur = 0.;
    i__1 = nivl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et0 = (i__ - 3) * 3600. - 900.;
	et1 = et0 + 1800.;
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmesur = xmesur + et1 - et0;
    }
    bail = FALSE_;
    rpt = TRUE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-6;
    gfocce_("ANY", "beta", "ellipsoid", "betafixed", "alpha", "ellipsoid", 
	    "alphafixed", "NONE", "sun", &tol, (U_fp)gfstep_, (U_fp)gfrefn_, &
	    rpt, (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &bail, 
	    (L_fp)gfbail_, cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now that the search is done, the interesting work starts. We're */
/*     going to do a post-mortem on the progress reporting calls that */
/*     were made during the search. */

/*     Start out by fetching the information that was passed to T_GFREPI */
/*     during the search. */

    mw = 200;
    t_gfrini__(&c__20, &mw, &ncalls, seqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Occultation searches require one pass, so we expect that */
/*     just one call to T_GFREPI was made. */

    chcksi_("No. of T_GFREPI calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", seqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefix and suffix. */

    chcksc_("Prefix", msglog, "=", "Occultation/transit search", ok, (ftnlen)
	    6, (ftnlen)78, (ftnlen)1, (ftnlen)26);
    chcksc_("Suffix", msglog + 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (
	    ftnlen)1, (ftnlen)5);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the sequence numbers of the calls. They should */
/*     range from 2 to NUPDAT+1 and should be in increasing */
/*     order. */

    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xsqlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsqlog",
		 i__2, "f_gfbirp__", (ftnlen)398)] = i__ + 1;
    }
    chckai_("Update SEQLOG", seqlog, "=", xsqlog, &nupdat, ok, (ftnlen)13, (
	    ftnlen)1);

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)421)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)421)], &
		c_b76, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)422)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)422)], &
		c_b76, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)426)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)426)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfbirp__", (
			ftnlen)434)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfbirp__", (ftnlen)434)], &c_b76, ok, (ftnlen)80, (
			ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfbirp__", (
			ftnlen)438)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfbirp__", (ftnlen)438)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b107, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Occultation searches require one pass, so we expect that */
/*     just one call to T_GFRTRM was made. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first termination call. Take */
/*     into account that the first call was to the init routine. */

    i__1 = nupdat + 2;
    chcksi_("T_GFREPF seq", trmlog, "=", &i__1, &c__0, ok, (ftnlen)12, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run simple FOV search using GFFOVE.", (ftnlen)35);

/*     We'll use the instrument defined in nat.ti: */

/*        ALPHA_ELLIPSE_NONE */

/*     This instrument tracks body Alpha so body Beta's FOV entry and */
/*     exit times match the start and stop times of Beta's transit */
/*     across Alpha. */

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);
    scardd_(&c__0, cnfine);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfbirp__", (ftnlen)503)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 3 intervals. */

    nivl = 3;
    xmesur = 0.;
    i__1 = nivl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et0 = (i__ - 3) * 3600. - 800.;
	et1 = et0 + 1600.;
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmesur = xmesur + et1 - et0;
    }
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 300.;
    tol = 1e-6;
    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    cleard_(&c__3, raydir);
    rpt = TRUE_;
    bail = FALSE_;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    gffove_(inst, tshape, raydir, target, tframe, abcorr, obsrvr, &tol, (U_fp)
	    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, 
	    (U_fp)t_gfrepf__, &bail, (L_fp)gfbail_, cnfine, result, (ftnlen)
	    36, (ftnlen)9, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now that the search is done, the interesting work starts. We're */
/*     going to do a post-mortem on the progress reporting calls that */
/*     were made during the search. */

/*     Start out by fetching the information that was passed to T_GFREPI */
/*     during the search. */

    mw = 200;
    t_gfrini__(&c__20, &mw, &ncalls, seqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     FOV searches require one pass. */

    chcksi_("No. of T_GFREPI calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", seqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefix and suffix. */

    chcksc_("Prefix", msglog, "=", "Target visibility search", ok, (ftnlen)6, 
	    (ftnlen)78, (ftnlen)1, (ftnlen)24);
    chcksc_("Suffix", msglog + 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (
	    ftnlen)1, (ftnlen)5);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the sequence numbers of the calls. They should */
/*     range from 2 to NUPDAT+1 and should be in increasing */
/*     order. */

    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xsqlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsqlog",
		 i__2, "f_gfbirp__", (ftnlen)602)] = i__ + 1;
    }
    chckai_("Update SEQLOG", seqlog, "=", xsqlog, &nupdat, ok, (ftnlen)13, (
	    ftnlen)1);

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)625)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)625)], &
		c_b76, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)626)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)626)], &
		c_b76, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfbirp__", (ftnlen)630)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfbirp__", (ftnlen)630)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfbirp__", (
			ftnlen)638)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfbirp__", (ftnlen)638)], &c_b76, ok, (ftnlen)80, (
			ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfbirp__", (
			ftnlen)642)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfbirp__", (ftnlen)642)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b107, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     FOV searches require one pass, so we expect that */
/*     just one call to T_GFRTRM was made. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first termination call. Take */
/*     into account that the first call was to the init routine. */

    i__1 = nupdat + 2;
    chcksi_("T_GFREPF seq", trmlog, "=", &i__1, &c__0, ok, (ftnlen)12, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    t_success__(ok);
    return 0;
} /* f_gfbirp__ */

