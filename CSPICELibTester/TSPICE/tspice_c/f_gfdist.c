/* f_gfdist.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__20000 = 20000;
static integer c__5 = 5;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__6 = 6;
static integer c__40000 = 40000;
static integer c__3 = 3;
static doublereal c_b270 = 1e-4;
static doublereal c_b348 = 3e-6;
static doublereal c_b375 = 1.9999999999999999e-6;
static doublereal c_b386 = 30.;
static doublereal c_b391 = 0.;
static doublereal c_b417 = 1e-12;
static doublereal c_b593 = 1e-6;

/* $Procedure F_GFDIST ( GFDIST family tests ) */
/* Subroutine */ int f_gfdist__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal left, comp[20006], span, dist, step, work[300090]	/* 
	    was [20006][15] */;
    static char time0[50], time1[50];
    static integer i__, j, k;
    static doublereal t;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), repmd_(char *, 
	    char *, doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmax_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal right;
    static char title[80];
    static doublereal resgt[20006], xleft, xtime;
    static integer count;
    static doublereal reslt[20006];
    static integer ixmax, ixmin;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal cnfin1[20006], cnfin2[20006];
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static integer handle;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int scardd_(integer *, doublereal *);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static char abcorr[80];
    static doublereal absmin, absmax;
    extern integer wncard_(doublereal *);
    static char target[36];
    static doublereal adjust;
    static char obsrvr[36];
    static doublereal et0, et1, maxdst, mindst, refval, result[20006], stepsz,
	     xright;
    extern /* Subroutine */ int tstlsk_(void), tstspk_(char *, logical *, 
	    integer *, ftnlen), ssized_(integer *, doublereal *), gfdist_(
	    char *, char *, char *, char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, integer *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), wninsd_(doublereal 
	    *, doublereal *, doublereal *), wnfetd_(doublereal *, integer *, 
	    doublereal *, doublereal *), spkpos_(char *, doublereal *, char *,
	     char *, char *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen), wndifd_(doublereal *, doublereal *, doublereal *)
	    , gfstol_(doublereal *), spkuef_(integer *), delfil_(char *, 
	    ftnlen);
    static doublereal beg, end;
    extern doublereal spd_(void);
    static doublereal pos[3], res2[20006];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFDIST */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFDIST. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     B.V. Semenov     (JPL) */
/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.2.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.1.0, 20-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.0.0, 01-MAR-2009 (NJB) (BVS) (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFDIST", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load an SPK file as well. */

    tstspk_("gfdist.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL CHCKXC ( .FALSE., ' ', OK ) */
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid result window size", (ftnlen)26);
    ssized_(&c__1, result);
    step = 10.;
    adjust = 0.;
    refval = 0.;
    gfdist_("MOON", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    ssized_(&c__0, result);
    gfdist_("MOON", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Non-positive step size", (ftnlen)22);
    ssized_(&c__20000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__20000, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 0.;
    adjust = 0.;
    gfdist_("MOON", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = -1.;
    gfdist_("MOON", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Workspace too small", (ftnlen)19);
    step = spd_() * 7;
    left = 0.;
    right = spd_() * 60;
    ssized_(&c__20000, result);
    ssized_(&c__20000, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Size is zero. */

    gfdist_("MOON", "NONE", "EARTH", "LOCMAX", &refval, &adjust, &step, 
	    cnfine, &c__0, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)
	    5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Size is positive but below limit. */

    gfdist_("MOON", "NONE", "EARTH", "LOCMAX", &refval, &adjust, &step, 
	    cnfine, &c__1, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)
	    5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Size is positive but is too small (error discovery at */
/*     search time). */

    gfdist_("MOON", "NONE", "EARTH", "LOCMAX", &refval, &adjust, &step, 
	    cnfine, &c__2, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)
	    5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);

/*     Window count is positive but below limit. */

    gfdist_("MOON", "NONE", "EARTH", "LOCMAX", &refval, &adjust, &step, 
	    cnfine, &c__20000, &c__4, work, result, (ftnlen)4, (ftnlen)4, (
	    ftnlen)5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Result window too small", (ftnlen)23);
    step = spd_() * 7;
    left = 0.;
    right = spd_() * 60;
    ssized_(&c__2, result);
    ssized_(&c__20000, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfdist_("MOON", "NONE", "EARTH", "LOCMAX", &refval, &adjust, &step, 
	    cnfine, &c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (
	    ftnlen)5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(OUTOFROOM)", ok, (ftnlen)16);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid operator", (ftnlen)16);
    step = 10.;
    gfdist_("MOON", "NONE", "EARTH", "!=", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)2);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Negative adjustment value", (ftnlen)25);
    step = 10.;
    adjust = -1.;
    gfdist_("MOON", "NONE", "EARTH", "ABSMAX", &refval, &adjust, &step, 
	    cnfine, &c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (
	    ftnlen)5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and observer are identical", (ftnlen)33);
    gfdist_("MOON", "NONE", "MOON", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)4, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized target or observer", (ftnlen)31);
    step = 1.;
    gfdist_("MOOON", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)5, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    gfdist_("MOON", "NONE", "ERTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)4, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    left = 0.;
    right = spd_() * 60;
    ssized_(&c__20000, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = spd_() * 7;
    adjust = 0.;
    gfdist_("GASPRA", "NONE", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)6, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction specifiers", (ftnlen)36);
    left = 0.;
    right = spd_() * 60;
    ssized_(&c__20000, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = spd_() * 7;
    adjust = 0.;
    gfdist_("MOON", "S", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)1, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfdist_("MOON", "XS", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)2, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfdist_("MOON", "RLT", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)3, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfdist_("MOON", "XRLT", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)4, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfdist_("MOON", "z", "EARTH", ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)1, (ftnlen)5, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */


/*     To speed things up, we're first going to create a couple of */
/*     "smarter" confinement windows. The first is the set of times when */
/*     the uncorrected distance is at least 404000 km. We'll call this */
/*     CNFIN1. */

    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(title, "Find times when #-# distance > # km. ABCORR = #.", (ftnlen)
	    80, (ftnlen)48);
    repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
	    ftnlen)80);
    repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
	    ftnlen)80);
    repmd_(title, "#", &refval, &c__6, title, (ftnlen)80, (ftnlen)1, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(time1, "2000 APR 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfin1);
    ssized_(&c__40000, cnfin2);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = spd_();
    refval = 4.04e5;
    adjust = 0.;
    gfdist_(target, "NONE", obsrvr, ">", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, cnfin1, (ftnlen)36, (ftnlen)4, (ftnlen)36, 
	    (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(cnfin1);
    chcksi_("CNFIN1 cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(cnfin1);
    for (j = 1; j <= i__1; ++j) {
	wnfetd_(cnfin1, &j, &left, &right);
    }

/*     The second confinement window is used for searches for minima. */
/*     This will be the window over which the uncorrected distance is at */
/*     less than 375000 km. We'll call this CNFIN2. */

    refval = 3.75e5;
    adjust = 0.;
    gfdist_(target, "NONE", obsrvr, "<", &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, cnfin2, (ftnlen)36, (ftnlen)4, (ftnlen)36, 
	    (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(cnfin2);
    chcksi_("CNFIN2 cardinality", &i__1, "=", &c__3, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(cnfin2);
    for (j = 1; j <= i__1; ++j) {
	wnfetd_(cnfin2, &j, &left, &right);
/*         WRITE (*,*) RIGHT-LEFT */
    }

/*     Check equality constraint for each aberration correction. */

    for (i__ = 1; i__ <= 9; ++i__) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfdist__", (ftnlen)646)) * 80, (
		ftnlen)80, (ftnlen)80);
	step = spd_();
	refval = 404100.;
	adjust = 0.;
	s_copy(title, "Find times when #-# distance = # km. ABCORR = #.", (
		ftnlen)80, (ftnlen)48);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmd_(title, "#", &refval, &c__6, title, (ftnlen)80, (ftnlen)1, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfdist_("MOON", abcorr, "EARTH", "=", &refval, &adjust, &step, cnfin1,
		 &c__20000, &c__5, work, result, (ftnlen)4, (ftnlen)80, (
		ftnlen)5, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We need to have at least two intervals for */
/*        a reasonable test case. */

	i__1 = wncard_(result);
	chcksi_("RESULT cardinality", &i__1, ">", &c__1, &c__0, ok, (ftnlen)
		18, (ftnlen)1);

/*        Check distance at the endpoint of the */
/*        intervals of the result window. */

	i__1 = wncard_(result);
	for (j = 1; j <= i__1; ++j) {
	    wnfetd_(result, &j, &left, &right);

/*           Check the distance at the interval's endpoints. */

	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
/*            CALL SPKEZR ( TARGET, LEFT,  'J2000', ABCORR, */
/*     .                    OBSRVR, STATE, LT               ) */
/*            CALL CHCKXC ( .FALSE., ' ',   OK ) */
/*            CALL VHAT ( POS, UHAT ) */
/*            RR = VDOT ( STATE(4), UHAT ) */
/*            WRITE (*,*) 'Range rate = ', RR */
	    s_copy(title, "Interval # start: distance", (ftnlen)80, (ftnlen)
		    26);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &dist, "~", &refval, &c_b270, ok, (ftnlen)80, (
		    ftnlen)1);
	    spkpos_(target, &right, "J2000", abcorr, obsrvr, pos, &lt, (
		    ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    s_copy(title, "Interval # stop: distance", (ftnlen)80, (ftnlen)25)
		    ;
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &dist, "~", &refval, &c_b270, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/*     Check > and < constraints for each aberration correction. */

    ssized_(&c__20000, reslt);
    ssized_(&c__20000, resgt);
    ssized_(&c__20000, comp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 9; ++i__) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfdist__", (ftnlen)731)) * 80, (
		ftnlen)80, (ftnlen)80);
	step = spd_();
	refval = 404100.;
	adjust = 0.;
	s_copy(title, "Find times when #-# distance > # km. ABCORR = #.", (
		ftnlen)80, (ftnlen)48);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmd_(title, "#", &refval, &c__6, title, (ftnlen)80, (ftnlen)1, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Search using the > operator. */

	gfdist_(target, abcorr, obsrvr, ">", &refval, &adjust, &step, cnfin1, 
		&c__20000, &c__5, work, resgt, (ftnlen)36, (ftnlen)80, (
		ftnlen)36, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We need to have at least two intervals for */
/*        a reasonable test case. */

	i__1 = wncard_(resgt);
	chcksi_("RESGT cardinality", &i__1, ">", &c__2, &c__0, ok, (ftnlen)17,
		 (ftnlen)1);

/*        Check distance at the endpoints of the */
/*        intervals of the RESGT result window. */

/*        At the first start and the last stop, simply */
/*        check that the inequality is satisfied, since */
/*        these bounds may be imposed by the confinement */
/*        window. */

/*        Also check distance at NSAMP interior points of the */
/*        each interval of the RESGT result window. */

	i__1 = wncard_(resgt);
	for (j = 1; j <= i__1; ++j) {

/*           Check the distance at the interval's endpoints. */

	    wnfetd_(resgt, &j, &left, &right);
/*            WRITE (*,*) 'RESGT interval: ', J, */
/*     .                  LEFT, RIGHT, RIGHT-LEFT */
	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    s_copy(title, "RESGT Interval # start: distance", (ftnlen)80, (
		    ftnlen)32);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    if (j == 1) {
		d__1 = refval - 1e-4;
		chcksd_(title, &dist, ">", &d__1, &c_b270, ok, (ftnlen)80, (
			ftnlen)1);
	    } else {
		chcksd_(title, &dist, "~", &refval, &c_b270, ok, (ftnlen)80, (
			ftnlen)1);
	    }
	    spkpos_(target, &right, "J2000", abcorr, obsrvr, pos, &lt, (
		    ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    s_copy(title, "RESGT Interval # stop: distance", (ftnlen)80, (
		    ftnlen)31);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    if (j == wncard_(resgt)) {
		d__1 = refval - 1e-4;
		chcksd_(title, &dist, ">", &d__1, &c_b270, ok, (ftnlen)80, (
			ftnlen)1);
	    } else {
		chcksd_(title, &dist, "~", &refval, &c_b270, ok, (ftnlen)80, (
			ftnlen)1);
	    }

/*           Sample interior points in the current interval. */

	    span = right - left;
	    stepsz = span / 101;
	    for (k = 1; k <= 100; ++k) {
		t = left + k * stepsz;
		spkpos_(target, &t, "J2000", abcorr, obsrvr, pos, &lt, (
			ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		dist = vnorm_(pos);
		s_copy(title, "RESGT Interval # sample # distance", (ftnlen)
			80, (ftnlen)34);
		repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		repmi_(title, "#", &k, title, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		d__1 = refval - 1e-4;
		chcksd_(title, &dist, ">", &d__1, &c_b270, ok, (ftnlen)80, (
			ftnlen)1);
	    }

/*           We've completed tests for the current interval */
/*           of RESGT. */

	}

/* ---- Case ------------------------------------------------------------- */


/*        Search using the < constraint. */

/*        Use the settings of */

/*           OSERVR */
/*           TARGET */
/*           REFVAL */
/*           ABCORR */
/*           STEP */

/*        from the previous > test case. */

	s_copy(title, "Find times when #-# distance < # km. ABCORR = #.", (
		ftnlen)80, (ftnlen)48);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmd_(title, "#", &refval, &c__6, title, (ftnlen)80, (ftnlen)1, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Find the complement of the result window relative */
/*        to the confinement window. */

	wndifd_(cnfin1, resgt, comp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Search using the < operator. */

	gfdist_(target, abcorr, obsrvr, "<", &refval, &adjust, &step, cnfin1, 
		&c__20000, &c__5, work, reslt, (ftnlen)36, (ftnlen)80, (
		ftnlen)36, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We need to have at least two intervals for */
/*        a reasonable test case. */

	i__1 = wncard_(reslt);
	chcksi_("RESLT cardinality", &i__1, ">", &c__1, &c__0, ok, (ftnlen)17,
		 (ftnlen)1);

/*        Test RESLT: we expect this window to be very close */
/*        to the same window as COMP. */

	i__1 = wncard_(reslt);
	for (j = 1; j <= i__1; ++j) {
/*            WRITE (*,*) 'RESLT Interval: ', J, LEFT, RIGHT, */
/*     .                  RIGHT-LEFT */
	    wnfetd_(reslt, &j, &left, &right);
	    wnfetd_(comp, &j, &xleft, &xright);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(title, "RESLT Interval # start time", (ftnlen)80, (ftnlen)
		    27);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &left, "~", &xleft, &c_b348, ok, (ftnlen)80, (
		    ftnlen)1);
	    s_copy(title, "RESLT Interval # stop time", (ftnlen)80, (ftnlen)
		    26);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &right, "~", &xright, &c_b348, ok, (ftnlen)80, (
		    ftnlen)1);
	}

/*        We've completed tests for the current aberration */
/*        correction. */

    }

/*     We've completed tests of the > and < operators for */
/*     each aberration correction. */


/*     Check LOCMAX and ABSMAX, and ABSMAX with adjustment */
/*     constraints for each aberration correction. */

    for (i__ = 1; i__ <= 9; ++i__) {

/* ---- Case ------------------------------------------------------------- */


/*        Switch observer and target. */

	s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
	s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfdist__", (ftnlen)949)) * 80, (
		ftnlen)80, (ftnlen)80);
	step = spd_();
	refval = 4.04e5;
	adjust = 0.;
	s_copy(title, "Find times when #-# distance is a local maximum. ABCO"
		"RR = #.", (ftnlen)80, (ftnlen)60);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Search using the LOCMAX operator. */

	gfdist_(target, abcorr, obsrvr, "LOCMAX", &refval, &adjust, &step, 
		cnfin1, &c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)
		80, (ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = wncard_(result);
	chcksi_("RESULT cardinality", &i__1, ">", &c__2, &c__0, ok, (ftnlen)
		18, (ftnlen)1);
	absmax = 0.;
	ixmax = 0;
	i__1 = wncard_(result);
	for (j = 1; j <= i__1; ++j) {

/*           Fetch the endpoints of the current */
/*           interval. We expect them to match within 2*CNVTOL. */

	    wnfetd_(result, &j, &left, &right);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(title, "Interval # left/right endpoint", (ftnlen)80, (
		    ftnlen)30);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &left, "~", &right, &c_b375, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the distance at TDELTA before and after */
/*           the interval's left endpoint. */

	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    maxdst = vnorm_(pos);
	    if (maxdst > absmax) {
		absmax = maxdst;
		ixmax = j;
	    }
	    for (k = 1; k <= 2; ++k) {
		if (k == 1) {
		    t = left - 30.;
		    s_copy(title, "Interval # start-# sec: distance", (ftnlen)
			    80, (ftnlen)32);
		} else {
		    t = left + 30.;
		    s_copy(title, "Interval # start+# sec: distance", (ftnlen)
			    80, (ftnlen)32);
		}
		spkpos_(target, &t, "J2000", abcorr, obsrvr, pos, &lt, (
			ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		dist = vnorm_(pos);
		repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		repmd_(title, "#", &c_b386, &c__6, title, (ftnlen)80, (ftnlen)
			1, (ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(title, &dist, "<", &maxdst, &c_b391, ok, (ftnlen)80, (
			ftnlen)1);
	    }

/*           We've completed tests for the current interval */
/*           of RESULT. */

	}

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Find times when #-# distance is an absolute maximum. "
		"ABCORR = #.", (ftnlen)80, (ftnlen)64);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Now search for the absolute maximum distance. */

	ssized_(&c__20000, res2);
	gfdist_(target, abcorr, obsrvr, "ABSMAX", &refval, &adjust, &step, 
		cnfin1, &c__20000, &c__5, work, res2, (ftnlen)36, (ftnlen)80, 
		(ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect a result window containing just 1 interval. */

	i__1 = wncard_(res2);
	chcksi_("RES2 cardinality (0)", &i__1, "=", &c__1, &c__0, ok, (ftnlen)
		20, (ftnlen)1);

/*        Fetch the endpoints of the result window's single */
/*        interval. We expect them to match within 2*CNVTOL. */

	wnfetd_(result, &c__1, &left, &right);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "Interval 1 left/right endpoint", (ftnlen)80, (ftnlen)
		30);
	chcksd_(title, &left, "~", &right, &c_b375, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check the distance and time of the maximum against those */
/*        obtained from the local maximum search. */

	t = res2[6];
	spkpos_(target, &t, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)5, (ftnlen)80, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dist = vnorm_(pos);
	chcksd_("Abs max distance", &dist, "~/", &absmax, &c_b417, ok, (
		ftnlen)16, (ftnlen)2);
	xtime = result[(i__1 = (ixmax - 1 << 1) + 6) < 20006 && 0 <= i__1 ? 
		i__1 : s_rnge("result", i__1, "f_gfdist__", (ftnlen)1093)];
	chcksd_("Abs max epoch", &t, "~", &xtime, &c_b348, ok, (ftnlen)13, (
		ftnlen)1);

/*        Now search for the absolute maximum distance, using a */
/*        non-zero adjustment value. */


/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Find times when #-# distance is within # of an absolu"
		"te maximum. ABCORR = #.", (ftnlen)80, (ftnlen)76);
	adjust = 100.;
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmd_(title, "#", &adjust, &c__6, title, (ftnlen)80, (ftnlen)1, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	scardd_(&c__0, res2);
	gfdist_(target, abcorr, obsrvr, "ABSMAX", &refval, &adjust, &step, 
		cnfin1, &c__20000, &c__5, work, res2, (ftnlen)36, (ftnlen)80, 
		(ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect a result window containing at least 1 interval. */

	i__1 = wncard_(res2);
	chcksi_("RES2 cardinality (1)", &i__1, ">", &c__0, &c__0, ok, (ftnlen)
		20, (ftnlen)1);

/*        Check the distance the result window's endpoints */
/*        against that obtained from the absolute maximum search. */

	i__1 = wncard_(res2);
	for (j = 1; j <= i__1; ++j) {
	    wnfetd_(res2, &j, &left, &right);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    d__1 = absmax - adjust;
	    chcksd_("Adjusted ABSMAX distance, left", &dist, "~", &d__1, &
		    c_b270, ok, (ftnlen)30, (ftnlen)1);
	    spkpos_(target, &right, "J2000", abcorr, obsrvr, pos, &lt, (
		    ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    d__1 = absmax - adjust;
	    chcksd_("Adjusted ABSMAX distance, right", &dist, "~", &d__1, &
		    c_b270, ok, (ftnlen)31, (ftnlen)1);
	}

/*        We've completed tests for the current aberration */
/*        correction. */

    }

/*     Check LOCMIN and ABSMIN, and ABSMIN with adjustment */
/*     constraints for each aberration correction. */

    for (i__ = 1; i__ <= 9; ++i__) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
	s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfdist__", (ftnlen)1188)) * 80, (
		ftnlen)80, (ftnlen)80);
	step = spd_();
	refval = 3.74e5;
	adjust = 0.;
	s_copy(title, "Find times when #-# distance is a local minimum. ABCO"
		"RR = #.", (ftnlen)80, (ftnlen)60);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Search using the LOCMIN operator. */

	gfdist_(target, abcorr, obsrvr, "LOCMIN", &refval, &adjust, &step, 
		cnfin2, &c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)
		80, (ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = wncard_(result);
	chcksi_("RESULT cardinality", &i__1, ">", &c__2, &c__0, ok, (ftnlen)
		18, (ftnlen)1);
	absmin = dpmax_();
	ixmin = 0;
	i__1 = wncard_(result);
	for (j = 1; j <= i__1; ++j) {

/*           Fetch the endpoints of the current */
/*           interval. We expect them to match within 2*CNVTOL. */

	    wnfetd_(result, &j, &left, &right);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(title, "Interval # left/right endpoint", (ftnlen)80, (
		    ftnlen)30);
	    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksd_(title, &left, "~", &right, &c_b375, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the distance at TDELTA before and after */
/*           the interval's left endpoint. */

	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    mindst = vnorm_(pos);
	    if (mindst < absmin) {
		absmin = mindst;
		ixmin = j;
	    }
	    for (k = 1; k <= 2; ++k) {
		if (k == 1) {
		    t = left - 30.;
		    s_copy(title, "Interval # start-# sec: distance", (ftnlen)
			    80, (ftnlen)32);
		} else {
		    t = left + 30.;
		    s_copy(title, "Interval # start+# sec: distance", (ftnlen)
			    80, (ftnlen)32);
		}
		spkpos_(target, &t, "J2000", abcorr, obsrvr, pos, &lt, (
			ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		dist = vnorm_(pos);
		repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		repmd_(title, "#", &c_b386, &c__6, title, (ftnlen)80, (ftnlen)
			1, (ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(title, &dist, ">", &mindst, &c_b391, ok, (ftnlen)80, (
			ftnlen)1);
	    }

/*           We've completed tests for the current interval */
/*           of RESULT. */

	}

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Find times when #-# distance is an absolute minimum. "
		"ABCORR = #.", (ftnlen)80, (ftnlen)64);
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Now search for the absolute minimum distance. */

	ssized_(&c__20000, res2);
	gfdist_(target, abcorr, obsrvr, "ABSMIN", &refval, &adjust, &step, 
		cnfin2, &c__20000, &c__5, work, res2, (ftnlen)36, (ftnlen)80, 
		(ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect a result window containing just 1 interval. */

	i__1 = wncard_(res2);
	chcksi_("RES2 cardinality (2)", &i__1, "=", &c__1, &c__0, ok, (ftnlen)
		20, (ftnlen)1);

/*        Fetch the endpoints of the result window's single */
/*        interval. We expect them to match within 2*CNVTOL. */

	wnfetd_(result, &c__1, &left, &right);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(title, "Interval 1 left/right endpoint", (ftnlen)80, (ftnlen)
		30);
	chcksd_(title, &left, "~", &right, &c_b375, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check the distance and time of the minimum against those */
/*        obtained from the local minimum search. */

	t = res2[6];
	spkpos_(target, &t, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)5, (ftnlen)80, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dist = vnorm_(pos);
	chcksd_("Abs min distance", &dist, "~/", &absmin, &c_b417, ok, (
		ftnlen)16, (ftnlen)2);
	xtime = result[(i__1 = (ixmin - 1 << 1) + 6) < 20006 && 0 <= i__1 ? 
		i__1 : s_rnge("result", i__1, "f_gfdist__", (ftnlen)1333)];
	chcksd_("Abs min epoch", &t, "~", &xtime, &c_b348, ok, (ftnlen)13, (
		ftnlen)1);

/*        Now search for the absolute minimum distance, using a */
/*        non-zero adjustment value. */


/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Find times when #-# distance is within # of an absolu"
		"te minimum. ABCORR = #.", (ftnlen)80, (ftnlen)76);
	adjust = 100.;
	repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmd_(title, "#", &adjust, &c__6, title, (ftnlen)80, (ftnlen)1, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	scardd_(&c__0, res2);
	gfdist_(target, abcorr, obsrvr, "ABSMIN", &refval, &adjust, &step, 
		cnfin2, &c__20000, &c__5, work, res2, (ftnlen)36, (ftnlen)80, 
		(ftnlen)36, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect a result window containing at least 1 interval. */

	i__1 = wncard_(res2);
	chcksi_("RES2 cardinality (3)", &i__1, ">", &c__0, &c__0, ok, (ftnlen)
		20, (ftnlen)1);

/*        Check the distance the result window's endpoints */
/*        against that obtained from the absolute minimum search. */

	i__1 = wncard_(res2);
	for (j = 1; j <= i__1; ++j) {
	    wnfetd_(res2, &j, &left, &right);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    spkpos_(target, &left, "J2000", abcorr, obsrvr, pos, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    d__1 = absmin + adjust;
	    chcksd_("Adjusted ABSMIN distance, left", &dist, "~", &d__1, &
		    c_b270, ok, (ftnlen)30, (ftnlen)1);
	    spkpos_(target, &right, "J2000", abcorr, obsrvr, pos, &lt, (
		    ftnlen)36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(pos);
	    d__1 = absmin + adjust;
	    chcksd_("Adjusted ABSMIN distance, right", &dist, "~", &d__1, &
		    c_b270, ok, (ftnlen)31, (ftnlen)1);
	}

/*        We've completed tests for the current aberration */
/*        correction. */

    }

/*     Case */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    step = spd_();
    refval = 3.74e5;
    adjust = 0.;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfdist_(target, abcorr, obsrvr, "ABSMIN", &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b270);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfdist_(target, abcorr, obsrvr, "ABSMIN", &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b391, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b593);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfdist.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfdist__ */

