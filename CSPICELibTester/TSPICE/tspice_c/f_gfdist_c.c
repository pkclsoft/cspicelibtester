/*

-Procedure f_gfdist_c ( Test gfdist_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfdist_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfdist_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfdist_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version

   -tspice_c Version 1.2.0, 08-FEB-2017 (EDW) (NJB)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.

       Removed unneeded declarations.       

   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 25-JUL-2008 (NJB) 

-Index_Entries

   test gfdist_c

-&
*/

{ /* Begin f_gfdist_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define SPK             "gfdist_c.bsp"   
   #define MED_REL         1.e-10
   #define MED_ABS         1.e-5
   #define MAXWIN          100000
   
   /*
   Local variables
   */

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );
   SPICEDOUBLE_CELL      ( tooShort, 1      );

   SPICEINT_CELL         ( badtype,  MAXWIN );

   SpiceDouble             adjust;
   SpiceDouble             dist;
   SpiceDouble             endpts [2];
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             lt;
   SpiceDouble             pos    [3];
   SpiceDouble             refval;
   SpiceDouble             step;

   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                n;
   SpiceInt                nintvls;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfdist_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   Create and load a PCK file. Delete the file afterward.
   */
   tstpck_c ( "test.pck", SPICETRUE, SPICEFALSE );
   
   
   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   

   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Bad input string pointers" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;

   gfdist_c ( NULLCPTR, "NONE", "EARTH", ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfdist_c ( "MOON", NULLCPTR, "EARTH", ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfdist_c ( "MOON", "NONE", NULLCPTR, ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfdist_c ( "MOON", "NONE", "EARTH", NULLCPTR, refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Empty input strings" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;

   gfdist_c ( "", "NONE", "EARTH", ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfdist_c ( "MOON", "", "EARTH", ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfdist_c ( "MOON", "NONE", "", ">",     refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfdist_c ( "MOON", "NONE", "EARTH", "", refval, 
              adjust,   step,   nintvls, &cnfine, &result );        
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad cell data type" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;

   gfdist_c ( "", "NONE", "EARTH", ">",  refval, 
              adjust,   step,   nintvls, &badtype, &result );       
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );

   gfdist_c ( "", "NONE", "EARTH", ">",  refval, 
              adjust,   step,   nintvls, &cnfine, &badtype );       
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );
 

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Non-positive interval count" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = 0;

   gfdist_c ( "MOON", "NONE", "EARTH", ">",     refval, 
              adjust, step,   nintvls, &cnfine, &result );          
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );



   nintvls = -1;

   gfdist_c ( "MOON", "NONE", "EARTH", ">",     refval, 
              adjust, step,   nintvls, &cnfine, &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

    tcase_c ( "Non-positive interval count" );

    str2et_c ( "2000 jan 1 TDB", &et0 );
    chckxc_c ( SPICEFALSE, " ", ok );

    str2et_c ( "2000 feb 1 TDB", &et1 );
    chckxc_c( SPICEFALSE, " ", ok );

    wninsd_c ( et0, et1, &cnfine );
    chckxc_c ( SPICEFALSE, " ", ok );

    step    = 30000.;
    adjust  = 0.;
    refval  = 0.;
    nintvls = 0;

    gfdist_c ( "MOON", "NONE", "EARTH", ">",     refval, 
               adjust, step,   nintvls, &cnfine, &result );         
    chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Result window too small (detected before search)" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;

   ssize_c ( 2, &result );
   chckxc_c( SPICEFALSE, " ", ok );


   gfdist_c ( "MOON", "NONE", "EARTH", "LOCMAX", refval, 
              adjust, step,   nintvls, &cnfine,  &tooShort );         
    
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by ZZGFWSTS.
   */
   chckxc_c ( SPICETRUE, "SPICE(INVALIDDIMENSION)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Result window too small (detected during search)" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;

   ssize_c ( 2, &result );
   chckxc_c( SPICEFALSE, " ", ok );


   gfdist_c ( "MOON", "NONE", "EARTH", "LOCMAX", refval, 
              adjust, step,   nintvls, &cnfine,  &result );         
    
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by ZZGFWSTS.
   */
   chckxc_c ( SPICETRUE, "SPICE(OUTOFROOM)", ok );



   /*
   A second search using the equality operator. Here the error
   is trapped in COPYD:
   */
   refval = 390000.;
   
   gfdist_c ( "MOON", "NONE", "EARTH", "=",     refval, 
              adjust, step,   nintvls, &cnfine, &result );
    
   /*
   Note that the short error message depends on the relational
   operator. In this case the error is trapped by WNINSD.
   */
   chckxc_c ( SPICETRUE, "SPICE(WINDOWEXCESS)", ok );

   ssize_c ( MAXWIN, &result );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Normal search: Earth-Moon distance is 390000 km." );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = spd_c();
   adjust  = 0.;
   refval  = 390000.;
   nintvls = MAXWIN/2;


   gfdist_c ( "MOON", "XCN+S", "EARTH", "=",     refval, 
              adjust, step,    nintvls, &cnfine, &result );         

   chckxc_c ( SPICEFALSE, " ", ok );

   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );

   /*
   Check the distance at each solution time. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      wnfetd_c ( &result, i, endpts, endpts+1 );
      chckxc_c ( SPICEFALSE, " ", ok );

      spkpos_c ( "MOON", endpts[0], "J2000", "XCN+S", "Earth", pos, &lt );
      chckxc_c ( SPICEFALSE, " ", ok );

      dist = vnorm_c( pos );

      /*
      Check relative and absolute errors. 
      */
      chcksd_c ( "dist", dist, "~/", refval, MED_REL, ok );
      chcksd_c ( "dist", dist, "~",  refval, MED_ABS, ok );
   }




   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( handle );
   TRASH   ( SPK    );
   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfdist_c */

