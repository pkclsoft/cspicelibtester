/* f_gfevnt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__20000 = 20000;
static integer c__2 = 2;
static doublereal c_b48 = 1e-6;
static integer c__15 = 15;
static integer c__0 = 0;

/* $Procedure F_GFEVNT ( GFEVNT family tests ) */
/* Subroutine */ int f_gfevnt__(logical *ok)
{
    /* Initialized data */

    static char cnames[80*7] = ">                                           "
	    "                                    " "=                        "
	    "                                                       " "<     "
	    "                                                                "
	    "          " "LOCMAX                                             "
	    "                             " "LOCMIN                          "
	    "                                                " "ABSMAX       "
	    "                                                                "
	    "   " "ABSMIN                                                    "
	    "                      ";
    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn+s                             "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT+S                                                         "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";
    static char enames[80*3] = "DISTANCE                                    "
	    "                                    " "ANGULAR SEPARATION       "
	    "                                                       " "COORDI"
	    "NATE                                                            "
	    "          ";
    static char xnames[80*4] = "ANGSPD                                      "
	    "                                    " "APPDIAM                  "
	    "                                                       " "PHASE "
	    "                                                                "
	    "          " "RNGRAT                                             "
	    "                             ";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2], i__3;
    char ch__1[89], ch__2[13], ch__3[93];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static logical bail;
    static doublereal step, work[300090]	/* was [20006][15] */;
    static char time0[80], time1[80];
    static integer i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char title[80], event[80];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , str2et_(char *, doublereal *, ftnlen);
    extern logical gfbail_();
    static integer handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen), scardd_(integer *, 
	    doublereal *), kclear_(void);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    extern /* Subroutine */ int gfrefn_();
    static char relate[80];
    extern /* Subroutine */ int gfrepi_(), gfrepf_();
    static doublereal refval, et0, et1, adjust, result[20006];
    static integer qnpars;
    static char qpnams[80*10], qcpars[80*10];
    static doublereal qdpars[10];
    static integer qipars[10];
    static logical qlpars[10];
    extern /* Subroutine */ int gfrepu_(), gfstep_();
    extern /* Subroutine */ int tstlsk_(void), t_pck08__(char *, logical *, 
	    logical *, ftnlen), furnsh_(char *, ftnlen), tstspk_(char *, 
	    logical *, integer *, ftnlen), ssized_(integer *, doublereal *), 
	    wninsd_(doublereal *, doublereal *, doublereal *), gfsstp_(
	    doublereal *), gfevnt_(U_fp, U_fp, char *, integer *, char *, 
	    char *, doublereal *, integer *, logical *, char *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *, U_fp, U_fp, 
	    U_fp, integer *, integer *, doublereal *, logical *, L_fp, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern doublereal spd_(void);
    static logical rpt;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFEVNT */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the mid-level SPICELIB */
/*     geometry routine GFEVNT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 03-MAR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Geometric quantity name. */



/*     Relational string */


/*     Quantity definition parameter arrays: */


/*     Routines to set step size, refine transition times */
/*     and report work. */


/*     Initial values */


/*     Coded and usable event conditions. */


/*     Coded but not yet usable event conditions. Use */
/*     these condition names to test unknown EVENT values. */
/*     Eventualy the elements of this list will exist in the */
/*     ENAMES list. */


/*     Begin every test family with an open call. */

    topen_("F_GFEVNT", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    t_pck08__("gfevnt.pck", &c_false, &c_true, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfevnt.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an SPK, load using FURNSH. */

    tstspk_("gfevnt.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfevnt.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    s_copy(time1, "2000 APR 1  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__20000, cnfine);
    ssized_(&c__20000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*   Case 1 */

/*     Test the implemented geometric quantities will cause an */
/*     error signal for a QNPARS beyond the GFEVNT MAXPAR value. */

    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);
    qnpars = 11;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "LT+S", (ftnlen)80, (ftnlen)4);
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add 2 points to the confinement interval window. */

    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(event, enames + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("enames", i__1, "f_gfevnt__", (ftnlen)341)) * 80, (
		ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	i__2[0] = 18, a__1[0] = "QNPARS > MAXPARS: ";
	i__2[1] = 80, a__1[1] = event;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    }

/*   Case 2 */

/*     Test the unimplemented geometric quantities will cause an */
/*     error signal. */

    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);
    qnpars = 3;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "LT+S", (ftnlen)80, (ftnlen)4);
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    for (i__ = 1; i__ <= 4; ++i__) {
	scardd_(&c__0, cnfine);
	scardd_(&c__0, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Add 2 points to the confinement interval window. */

	wninsd_(&et0, &et1, cnfine);
	s_copy(event, xnames + ((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("xnames", i__1, "f_gfevnt__", (ftnlen)391)) * 80, (
		ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	i__2[0] = 24, a__1[0] = "Unimplemented quantity: ";
	i__2[1] = 80, a__1[1] = event;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);
    }

/*   Case 3 */

/*     Cause an error signal due to an invalid relation operator. */

    s_copy(event, "DISTANCE", (ftnlen)80, (ftnlen)8);
    qnpars = 3;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "LT+S", (ftnlen)80, (ftnlen)4);
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "===", (ftnlen)80, (ftnlen)3);
/* Writing concatenation */
    i__2[0] = 9, a__1[0] = "Invalid: ";
    i__2[1] = 80, a__1[1] = relate;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)89);
    tcase_(ch__1, (ftnlen)89);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, cnfine, 
	    &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &c__20000, &
	    c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80,
	     (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*     Case 4 */

/*     Run a valid, simple search using each relation operator */
/*     and each aberration correction. */

    s_copy(event, "DISTANCE", (ftnlen)80, (ftnlen)8);
    qnpars = 3;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    for (i__ = 1; i__ <= 7; ++i__) {
	for (j = 1; j <= 9; ++j) {
	    scardd_(&c__0, cnfine);
	    scardd_(&c__0, result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    wninsd_(&et0, &et1, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(relate, cnames + ((i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 
		    : s_rnge("cnames", i__1, "f_gfevnt__", (ftnlen)480)) * 80,
		     (ftnlen)80, (ftnlen)80);
	    s_copy(qcpars + 160, corr + ((i__1 = j - 1) < 9 && 0 <= i__1 ? 
		    i__1 : s_rnge("corr", i__1, "f_gfevnt__", (ftnlen)481)) * 
		    80, (ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	    i__2[0] = 7, a__1[0] = relate;
	    i__2[1] = 6, a__1[1] = corr + ((i__1 = j - 1) < 9 && 0 <= i__1 ? 
		    i__1 : s_rnge("corr", i__1, "f_gfevnt__", (ftnlen)483)) * 
		    80;
	    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)13);
	    tcase_(ch__2, (ftnlen)13);
	    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, 
		    qcpars, qdpars, qipars, qlpars, relate, &refval, &c_b48, &
		    adjust, cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)
		    gfrepf_, &c__20000, &c__15, work, &bail, (L_fp)gfbail_, 
		    result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*   Case 5 */

/*     Event DISTANCE - Test for an error when QPNAMS has */
/*     an empty element. */

    s_copy(event, "DISTANCE", (ftnlen)80, (ftnlen)8);
    qnpars = 3;
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = qnpars;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
	s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
	s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 160, "LT+S", (ftnlen)80, (ftnlen)4);
/* Writing concatenation */
	i__2[0] = 13, a__1[0] = "Empty value: ";
	i__2[1] = 80, a__1[1] = qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ?
		 i__3 : s_rnge("qpnams", i__3, "f_gfevnt__", (ftnlen)531)) * 
		80;
	s_cat(ch__3, a__1, i__2, &c__2, (ftnlen)93);
	tcase_(ch__3, (ftnlen)93);
	s_copy(qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ? i__3 : s_rnge(
		"qpnams", i__3, "f_gfevnt__", (ftnlen)532)) * 80, " ", (
		ftnlen)80, (ftnlen)1);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(MISSINGVALUE)", ok, (ftnlen)19);
    }

/*   Case 6 */

/*     Event ANGULAR SEPARATION - Test for an error when QPNAMS has */
/*     an empty element. */

    s_copy(event, "ANGULAR SEPARATION", (ftnlen)80, (ftnlen)18);
    qnpars = 8;
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = qnpars;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qpnams, "TARGET1", (ftnlen)80, (ftnlen)7);
	s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 80, "FRAME1", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 80, "NULL", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 160, "SHAPE1", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 160, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(qpnams + 240, "TARGET2", (ftnlen)80, (ftnlen)7);
	s_copy(qcpars + 240, "EARTH", (ftnlen)80, (ftnlen)5);
	s_copy(qpnams + 320, "FRAME2", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 320, "NULL", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 400, "SHAPE2", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 400, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(qpnams + 480, "OBSERVER", (ftnlen)80, (ftnlen)8);
	s_copy(qcpars + 480, "SUN", (ftnlen)80, (ftnlen)3);
	s_copy(qpnams + 560, "ABCORR", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 560, "NONE", (ftnlen)80, (ftnlen)4);
/* Writing concatenation */
	i__2[0] = 13, a__1[0] = "Empty value: ";
	i__2[1] = 80, a__1[1] = qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ?
		 i__3 : s_rnge("qpnams", i__3, "f_gfevnt__", (ftnlen)588)) * 
		80;
	s_cat(ch__3, a__1, i__2, &c__2, (ftnlen)93);
	tcase_(ch__3, (ftnlen)93);
	s_copy(qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ? i__3 : s_rnge(
		"qpnams", i__3, "f_gfevnt__", (ftnlen)589)) * 80, " ", (
		ftnlen)80, (ftnlen)1);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(MISSINGVALUE)", ok, (ftnlen)19);
    }

/*   Case 7 */

/*     Event COORDINATE - Test for an error when QPNAMS has */
/*     an empty element. */

    s_copy(event, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    step = spd_() * 1.;
    refval = 0.;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = qnpars;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
	s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
	s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
	s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
	s_copy(qcpars + 240, "RECTANGULAR", (ftnlen)80, (ftnlen)11);
	s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
	s_copy(qcpars + 320, "X", (ftnlen)80, (ftnlen)1);
	s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
	s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
	s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
	s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
	s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
	s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
	s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
	s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
	s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
	qdpars[0] = 0.;
	qdpars[1] = 0.;
	qdpars[2] = 0.;
/* Writing concatenation */
	i__2[0] = 13, a__1[0] = "Empty value: ";
	i__2[1] = 80, a__1[1] = qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ?
		 i__3 : s_rnge("qpnams", i__3, "f_gfevnt__", (ftnlen)660)) * 
		80;
	s_cat(ch__3, a__1, i__2, &c__2, (ftnlen)93);
	tcase_(ch__3, (ftnlen)93);
	s_copy(qpnams + ((i__3 = i__ - 1) < 10 && 0 <= i__3 ? i__3 : s_rnge(
		"qpnams", i__3, "f_gfevnt__", (ftnlen)661)) * 80, " ", (
		ftnlen)80, (ftnlen)1);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(MISSINGVALUE)", ok, (ftnlen)19);
    }

/*   Case 8 */

/*     Event DISTANCE - Test for an error when ADJUST !=0 for */
/*     RELATE != 'ABSMAX' or 'ABSMIN */

    s_copy(event, "DISTANCE", (ftnlen)80, (ftnlen)8);
    qnpars = 3;
    step = spd_() * 1.;
    refval = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "LT+S", (ftnlen)80, (ftnlen)4);
    adjust = 1.;
    for (i__ = 1; i__ <= 5; ++i__) {
	scardd_(&c__0, cnfine);
	scardd_(&c__0, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(relate, cnames + ((i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnames", i__1, "f_gfevnt__", (ftnlen)715)) * 80, (
		ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	i__2[0] = 26, a__1[0] = "DISTANCE: Non-zero ADJUST ";
	i__2[1] = 80, a__1[1] = relate;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    }

/*   Case 9 */

/*     Event ANGULAR SEPARATION - Test for an error when ADJUST !=0 for */
/*     RELATE != 'ABSMAX' */

    s_copy(event, "ANGULAR SEPARATION", (ftnlen)80, (ftnlen)18);
    qnpars = 8;
    step = spd_() * 1.;
    refval = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qpnams, "TARGET1", (ftnlen)80, (ftnlen)7);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "FRAME1", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 80, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 160, "SHAPE1", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "TARGET2", (ftnlen)80, (ftnlen)7);
    s_copy(qcpars + 240, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 320, "FRAME2", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 400, "SHAPE2", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 400, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 480, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 480, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qpnams + 560, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 560, "NONE", (ftnlen)80, (ftnlen)4);
    adjust = 1.;
    for (i__ = 1; i__ <= 5; ++i__) {
	scardd_(&c__0, cnfine);
	scardd_(&c__0, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(relate, cnames + ((i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnames", i__1, "f_gfevnt__", (ftnlen)781)) * 80, (
		ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	i__2[0] = 36, a__1[0] = "ANGULAR SEPARATION: Non zero ADJUST ";
	i__2[1] = 80, a__1[1] = relate;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    }

/*   Case 10 */

/*     Event COORDINATE - Test for an error when ADJUST !=0 for */
/*     RELATE != 'ABSMAX' */

    s_copy(event, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    step = spd_() * 1.;
    refval = 0.;
    rpt = FALSE_;
    bail = FALSE_;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qcpars + 240, "RECTANGULAR", (ftnlen)80, (ftnlen)11);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qcpars + 320, "X", (ftnlen)80, (ftnlen)1);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    qdpars[0] = 0.;
    qdpars[1] = 0.;
    qdpars[2] = 0.;
    adjust = 1.;
    for (i__ = 1; i__ <= 5; ++i__) {
	scardd_(&c__0, cnfine);
	scardd_(&c__0, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(relate, cnames + ((i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnames", i__1, "f_gfevnt__", (ftnlen)853)) * 80, (
		ftnlen)80, (ftnlen)80);
/* Writing concatenation */
	i__2[0] = 28, a__1[0] = "COORDINATE: Non zero ADJUST ";
	i__2[1] = 80, a__1[1] = relate;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, event, &qnpars, qpnams, qcpars, 
		qdpars, qipars, qlpars, relate, &refval, &c_b48, &adjust, 
		cnfine, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &
		c__20000, &c__15, work, &bail, (L_fp)gfbail_, result, (ftnlen)
		80, (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);
    }

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfevnt.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfevnt.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfevnt__ */

