/*

-Procedure f_gfevnt_c ( Test gfevnt_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfevnt_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceGF.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfevnt_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfevnt_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 11-MAR-2009 (NJB)(EDW)

-Index_Entries

   test gfevnt_c

-&
*/

{ /* Begin f_gfevnt_c */


   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   #define string_copy(src, dest)      strncpy( dest, src,  strlen(src) ); \
                                       dest[strlen(src)] = '\0';

   /*
   Constants
   */
   #define SPK             "gfevnt.bsp"
   #define PCK             "gfevnt.pck"
   #define LSK             "gfevnt.lsk"
   #define MEDTOL          1.e-4
   #define BDNMLN          36 
   #define LNSIZE          80 
   #define MAXVAL          20000
   #define NENAMES         3
   #define NXNAMES         4
   #define NC              7
   #define NCORR           9 
 
   /*
   Initial values
   */

   SpiceChar             * CNAMES[]   = { ">",
                                        "=",
                                        "<",
                                        "LOCMAX",
                                        "LOCMIN",
                                        "ABSMAX",
                                        "ABSMIN",
                                        };

   SpiceChar             * CORR[]   = { "NONE", 
                                        "lt", 
                                        " lt+s",
                                        " cn",
                                        " cn + s",
                                        "XLT",  
                                        "XLT + S", 
                                        "XCN", 
                                        "XCN+S" 
                                        };
   
   /*
    Coded and usable event conditions.
   */
   SpiceChar             * ENAMES[] = { "DISTANCE",
                                        "ANGULAR SEPARATION",
                                        "COORDINATE", 
                                        }; 

   /*
   Coded but not yet usable event conditions. Use
   these condition names to test unknown EVENT values.
   Eventualy the elements of this list will exist in the
   ENAMES list.
   */
   SpiceChar             * XNAMES[] = { "ANGSPD",
                                        "APPDIAM", 
                                        "PHASE",
                                        "RNGRAT",
                                        };
 
   SpiceInt                qnpars;
   SpiceInt                mw;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                lenvals;
   SpiceInt                handle;

   SpiceChar               title [LNSIZE*2];
   SpiceChar               relate [LNSIZE];
   SpiceChar             * event;

   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             step;
   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             tol;

   SpiceBoolean            bail;
   SpiceBoolean            rpt;
         
   /*
   Declare qpnams and qcpars with the same dimensions.
   SPICE_GFEVNT_MAXPAR defined in SpiceGF.h.
   */
   SpiceChar               qpnams[SPICE_GFEVNT_MAXPAR][LNSIZE];
   SpiceChar               qcpars[SPICE_GFEVNT_MAXPAR][LNSIZE];
   SpiceDouble             qdpars[SPICE_GFEVNT_MAXPAR];
   SpiceInt                qipars[SPICE_GFEVNT_MAXPAR];
   SpiceBoolean            qlpars[SPICE_GFEVNT_MAXPAR];
         
   SPICEDOUBLE_CELL      ( cnfine,   MAXVAL );
   SPICEDOUBLE_CELL      ( result,   MAXVAL );



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfevnt_c" );


   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );

   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   /*
   Create an SPK, load using FURNSH.
   */
   tstspk_c ( SPK, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );
 

   /*
   Create a confinement window from ET0 and ET1.
   */
   str2et_c ( "2000 JAN 1  00:00:00 TDB", &et0 );
   str2et_c ( "2000 APR 1  00:00:00 TDB", &et1 );


   /*
   Case 1

     Test the implemented geometric quantities will cause an error 
     signal for a 'qnpars' beyond the gfevnt_c SPICE_GFEVNT_MAXPAR value.
   */

   string_copy( "ABSMAX",   relate    );
   string_copy( "TARGET",   qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "OBSERVER", qpnams[1] );
   string_copy( "EARTH",    qcpars[1] );
   string_copy( "ABCORR",   qpnams[2] );
   string_copy( "LT+S",     qcpars[2] );

   qnpars = SPICE_GFEVNT_MAXPAR + 1;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;
   mw     = MAXVAL;

   gfsstp_c ( step );

   for ( i=0; i<NENAMES; i++ )
      {

      scard_c ( 0, &cnfine );
      scard_c ( 0, &result );

      /*
      Add 2 points to the confinement interval window.
      */

      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );
   
      event = ENAMES[i];
      sprintf( title, "QNPARS > MAXPARS: %s",  event  );
   
      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDCOUNT)", ok);
      }



   /*
   Case 2

     Test the unimplemented geometric quantities will cause an
     error signal.
   */

   string_copy( "ABSMAX",   relate    );
   string_copy( "TARGET",   qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "OBSERVER", qpnams[1] );
   string_copy( "EARTH",    qcpars[1] );
   string_copy( "ABCORR",   qpnams[2] );
   string_copy( "LT+S",     qcpars[2] );

   qnpars = 3;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   for ( i=0; i<NXNAMES; i++ )
      {

      event = XNAMES[i];
      sprintf( title, "Unimplemented quantity: %s",  event  );
   
      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);
      }



   /*
   Case 3

     Cause an error signal due to an invalid relation operator.
   */

   event = ENAMES[0];
   string_copy( "ABSMAX",   relate    );
   string_copy( "TARGET",   qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "OBSERVER", qpnams[1] );
   string_copy( "EARTH",    qcpars[1] );
   string_copy( "ABCORR",   qpnams[2] );
   string_copy( "LT+S",     qcpars[2] );

   qnpars = 3;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   sprintf( title, "Invalid: ===" );
   
   tcase_c ( title );

   gfevnt_c ( gfstep_c,
              gfrefn_c,
              "===",
              qnpars,
              lenvals,
              qpnams,
              qcpars,
              qdpars,
              qipars,
              qlpars,
              relate,
              refval,
              tol,
              adjust,
              rpt,
              gfrepi_c,
              gfrepu_c,
              gfrepf_c,
              mw,
              bail,
              gfbail_c,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);



   /*
   Case 4

     Run a valid, simple search using each relation operator
     and each aberration correction.
   */

   event = ENAMES[0];

   string_copy( "TARGET",   qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "OBSERVER", qpnams[1] );
   string_copy( "EARTH",    qcpars[1] );
   string_copy( "ABCORR",   qpnams[2] );

   qnpars = 3;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;
   mw     = MAXVAL;

   gfsstp_c ( step );

   for ( i=0; i<NC; i++ )
      {

      for ( j=0; j<NCORR; j++ )
         {

         /*
         Add 2 points to the confinement interval window.
         */
         scard_c ( 0, &cnfine );
         scard_c ( 0, &result );
         wninsd_c ( et0, et1, &cnfine );
         chckxc_c ( SPICEFALSE, " ", ok );

         string_copy( CNAMES[i],   relate    );
         string_copy( CORR[j],     qcpars[2] );
   
         sprintf( title, "%s %s", relate, qcpars[2] );
         tcase_c ( title );

         gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
         chckxc_c ( SPICEFALSE, " ", ok);
         }

      }



   /*
   Case 5

     Event DISTANCE - Test for an error when QPNAMS has 
     an empty element.
   */

   event = ENAMES[0];
   string_copy( "=", relate );

   qnpars = 3;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i=0; i<qnpars; i++ )
      {

      string_copy( "TARGET",   qpnams[0] );
      string_copy( "MOON",     qcpars[0] );
      string_copy( "OBSERVER", qpnams[1] );
      string_copy( "EARTH",    qcpars[1] );
      string_copy( "ABCORR",   qpnams[2] );
      string_copy( "LT+S",     qcpars[2] );

      sprintf( title, "Empty vale: %s", qpnams[i] );
      string_copy( " ", qpnams[i] );
 
      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(MISSINGVALUE)", ok);
      }



   /*
   Case 6

     Event ANGULAR_SEPARATION - Test for an error when QPNAMS has 
     an empty element.
   */

   event = ENAMES[1];
   string_copy( "=", relate );

   qnpars = 8;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i=0; i<qnpars; i++ )
      {

      string_copy( "TARGET1",  qpnams[0] );
      string_copy( "MOON",     qcpars[0] );
      string_copy( "FRAME1",   qpnams[1] );
      string_copy( "NULL",     qcpars[1] );
      string_copy( "SHAPE1",   qpnams[2] );
      string_copy( "SPHERE",   qcpars[2] );
      string_copy( "TARGET2",  qpnams[3] );
      string_copy( "EARTH",    qcpars[3] );
      string_copy( "FRAME2",   qpnams[4] );
      string_copy( "NULL",     qcpars[4] );
      string_copy( "SHAPE2",   qpnams[5] );
      string_copy( "SPHERE",   qcpars[5] );
      string_copy( "OBSERVER", qpnams[6] );
      string_copy( "SUN",      qcpars[6] );
      string_copy( "ABCORR",   qpnams[7] );
      string_copy( "NONE",     qcpars[7] );

      sprintf( title, "Empty value: %s", qpnams[i] );
      string_copy( " ", qpnams[i] );
 
      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(MISSINGVALUE)", ok);
      }


   /*
   Case 7

     Event COORDINATE - Test for an error when QPNAMS has 
     an empty element.
   */

   event = ENAMES[2];
   string_copy( "=", relate );

   qnpars = 10;
   step   = 1.*spd_c() ;
   refval =  0.;
   adjust =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i=0; i<qnpars; i++ )
      {

      string_copy( "TARGET",            qpnams[0] );
      string_copy( "MOON",              qcpars[0] );
      string_copy( "OBSERVER",          qpnams[1] );
      string_copy( "EARTH",             qcpars[1] );
      string_copy( "ABCORR",            qpnams[2] );
      string_copy( "NONE",              qcpars[2] );
      string_copy( "COORDINATE_SYSTEM", qpnams[3] );
      string_copy( "RECTANGULAR",       qcpars[3] );
      string_copy( "COORDINATE",        qpnams[4] );
      string_copy( "X",                 qcpars[4] );
      string_copy( "REFERENCE_FRAME",   qpnams[5] );
      string_copy( "J2000",             qcpars[5] );
      string_copy( "VECTOR_DEFINITION", qpnams[6] );
      string_copy( "POSITION",          qcpars[6] );
      string_copy( "METHOD",            qpnams[7] );
      string_copy( " ",                 qcpars[7] );
      string_copy( "DREF",              qpnams[8] );
      string_copy( " ",                 qcpars[8] );
      string_copy( "DVEC",              qpnams[9] );
      qdpars[0] = 0.;
      qdpars[1] = 0.;
      qdpars[2] = 0.;
      

      sprintf( title, "Empty value: %s", qpnams[i] );
      string_copy( " ", qpnams[i] );
 
      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(MISSINGVALUE)", ok);
      }


   /*
   Case 8

     Event DISTANCE - Test for an error when ADJUST !=0 for
     RELATE != 'ABSMAX'
   */

   event = ENAMES[0];
   string_copy( "=", relate );

   qnpars = 3;
   step   = 1.*spd_c() ;
   refval =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );

   /*
   Add 2 points to the confinement interval window.
   */
   scard_c ( 0, &cnfine );
   scard_c ( 0, &result );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   string_copy( "TARGET",   qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "OBSERVER", qpnams[1] );
   string_copy( "EARTH",    qcpars[1] );
   string_copy( "ABCORR",   qpnams[2] );
   string_copy( "LT+S",     qcpars[2] );

   adjust =  1.;

   /*
   Loop over the comparison opperators except the final two in CNAMES:
   ABSMAX and ABSMIN.
   */
   for ( i=0; i<NC-2; i++ )
      {

      /*
      Add 2 points to the confinement interval window.
      */
      scard_c ( 0, &cnfine );
      scard_c ( 0, &result );
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      string_copy( CNAMES[i], relate );
      sprintf( title, "DISTANCE: Non-zero ADJUST %s", relate );

      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDVALUE)", ok);
      }


   /*
   Case 9

     Event ANGULAR_SEPARATION - Test for an error when ADJUST !=0 for
     RELATE != 'ABSMAX'
   */

   event = ENAMES[1];

   qnpars = 8;
   step   = 1.*spd_c() ;
   refval =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );
   
   string_copy( "TARGET1",  qpnams[0] );
   string_copy( "MOON",     qcpars[0] );
   string_copy( "FRAME1",   qpnams[1] );
   string_copy( "NULL",     qcpars[1] );
   string_copy( "SHAPE1",   qpnams[2] );
   string_copy( "SPHERE",   qcpars[2] );
   string_copy( "TARGET2",  qpnams[3] );
   string_copy( "EARTH",    qcpars[3] );
   string_copy( "FRAME2",   qpnams[4] );
   string_copy( "NULL",     qcpars[4] );
   string_copy( "SHAPE2",   qpnams[5] );
   string_copy( "SPHERE",   qcpars[5] );
   string_copy( "OBSERVER", qpnams[6] );
   string_copy( "SUN",      qcpars[6] );
   string_copy( "ABCORR",   qpnams[7] );
   string_copy( "NONE",     qcpars[7] );

   adjust =  1.;

  /*
   Loop over the comparison opperators except the final two in CNAMES:
   ABSMAX and ABSMIN.
   */
   for ( i=0; i<NC-2; i++ )
      {

      /*
      Add 2 points to the confinement interval window.
      */
      scard_c ( 0, &cnfine );
      scard_c ( 0, &result );
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      string_copy( CNAMES[i], relate );
      sprintf( title, "ANGULAR_SEPARATION: Non zero ADJUST %s", relate );

      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDVALUE)", ok);
      }



   /*
   Case 10

     Event COORDINATE - Test for an error when ADJUST !=0 for
     RELATE != 'ABSMAX'
   */

   event = ENAMES[2];

   qnpars = 10;
   step   = 1.*spd_c() ;
   refval =  0.;
   lenvals= LNSIZE;
   tol    = SPICE_GF_CNVTOL;

   rpt    = SPICEFALSE;
   bail   = SPICEFALSE;
   mw     = MAXVAL;

   gfsstp_c ( step );

   string_copy( "TARGET",            qpnams[0] );
   string_copy( "MOON",              qcpars[0] );
   string_copy( "OBSERVER",          qpnams[1] );
   string_copy( "EARTH",             qcpars[1] );
   string_copy( "ABCORR",            qpnams[2] );
   string_copy( "NONE",              qcpars[2] );
   string_copy( "COORDINATE SYSTEM", qpnams[3] );
   string_copy( "RECTANGULAR",       qcpars[3] );
   string_copy( "COORDINATE",        qpnams[4] );
   string_copy( "X",                 qcpars[4] );
   string_copy( "REFERENCE FRAME",   qpnams[5] );
   string_copy( "J2000",             qcpars[5] );
   string_copy( "VECTOR DEFINITION", qpnams[6] );
   string_copy( "POSITION",          qcpars[6] );
   string_copy( "METHOD",            qpnams[7] );
   string_copy( " ",                 qcpars[7] );
   string_copy( "DREF",              qpnams[8] );
   string_copy( " ",                 qcpars[8] );
   string_copy( "DVEC",              qpnams[9] );
      
   qdpars[0] = 0.;
   qdpars[1] = 0.;
   qdpars[2] = 0.;

   adjust =  1.;

   /*
   Loop over the comparison opperators except the final two in CNAMES:
   ABSMAX and ABSMIN.
   */
   for ( i=0; i<NC-2; i++ )
      {

      /*
      Add 2 points to the confinement interval window.
      */
      scard_c ( 0, &cnfine );
      scard_c ( 0, &result );
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      string_copy( CNAMES[i], relate );
      sprintf( title, "COORDINATE: Non zero ADJUST %s", relate );

      tcase_c ( title );

      gfevnt_c ( gfstep_c,
                 gfrefn_c,
                 event,
                 qnpars,
                 lenvals,
                 qpnams,
                 qcpars,
                 qdpars,
                 qipars,
                 qlpars,
                 relate,
                 refval,
                 tol,
                 adjust,
                 rpt,
                 gfrepi_c,
                 gfrepu_c,
                 gfrepf_c,
                 mw,
                 bail,
                 gfbail_c,
                 &cnfine,
                 &result );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDVALUE)", ok);
      }





   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfevnt_c */


