/*

-Procedure f_gffove_c ( Test gffove_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gffove_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gffove_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gffove_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 26-JAN-2009 (NJB) 

-Index_Entries

   test gffove_c

-&
*/

{ /* Begin f_gffove_c */


   /*
   Prototypes 
   */
   int natik_  ( char    *nameik, 
                 char    *spk, 
                 char    *pck, 
                 logical *loadik, 
                 logical *keepik, 
                 ftnlen  nameik_len, 
                 ftnlen  spk_len, 
                 ftnlen  pck_len    );

   int natpck_ ( char    *file, 
                 logical *loadpc, 
                 logical *keeppc, 
                 ftnlen  file_len );

   int natspk_ ( char    *file, 
                 logical *load, 
                 integer *handle,
                 ftnlen  file_len );

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define IK              "nat.ti"   
   #define PCK             "nat.tpc"   
   #define SPK             "nat.bsp"   
   #define TIMTOL          1.e-6 
   #define MAXWIN          100000
   #define LNSIZE          81
   
   /*
   Local variables
   */
   integer                 handle;

   static logical          keepik  = SPICETRUE;
   static logical          keeppc  = SPICETRUE;
   static logical          loadik  = SPICETRUE;
   static logical          loadpc  = SPICETRUE;
   static logical          loadspk = SPICETRUE;

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );
   SPICEDOUBLE_CELL      ( tooShort, 1      );

   SPICEINT_CELL         ( badtype,  MAXWIN );

   SpiceChar             * inst;
   SpiceChar               qname  [ LNSIZE ];

   SpiceDouble             endpts [2];
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             raydir [3];
   SpiceDouble             step;
   SpiceDouble             xtime;


   SpiceInt                i;
   SpiceInt                n;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gffove_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Create and load Nat's solar system SPK, PCK/FK, and IK files.
   */
 

   /*
   Create and load a PCK file. Do NOT delete the file afterward.
   */
   natpck_ ( ( char      * ) PCK,
             ( logical   * ) &loadpc, 
             ( logical   * ) &keeppc, 
             ( ftnlen      ) strlen(PCK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Load an SPK file as well.
   */
   natspk_ ( ( char      * ) SPK,
             ( logical   * ) &loadspk, 
             ( integer   * ) &handle, 
             ( ftnlen      ) strlen(SPK) );   
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load an IK.
   */
   natik_  ( ( char      * ) IK,
             ( char      * ) SPK,
             ( char      * ) PCK,
             ( logical   * ) &loadik, 
             ( logical   * ) &keepik, 
             ( ftnlen      ) strlen(IK),
             ( ftnlen      ) strlen(SPK),   
             ( ftnlen      ) strlen(PCK)  );   
   chckxc_c ( SPICEFALSE, " ", ok );

   
   
   

   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Bad input string pointers" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   inst    = "ALPHA_ELLIPSE_NONE";
   step    = 30000.;

   vpack_c ( 1.0, 2.0, 3.0, raydir );

   gffove_c ( NULLCPTR,   "ellipsoid", raydir, "beta", "betafixed",
              "none",     "sun",       TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gffove_c ( inst,        NULLCPTR,  raydir, "beta", "betafixed",
              "none",     "sun",      TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gffove_c ( inst,       "ellipsoid",  raydir, NULLCPTR, "betafixed",
              "none",     "sun",        TIMTOL,
              &gfstep_c,  &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,    &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gffove_c ( inst,       "ellipsoid", raydir, "beta", NULLCPTR,
              "none",     "sun",       TIMTOL,   
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gffove_c ( inst,       "ellipsoid", raydir,  "beta", "betafixed",
              NULLCPTR,   "sun",       TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gffove_c ( inst,       "ellipsoid",  raydir, "beta",  "betafixed",
              "none",     NULLCPTR,     TIMTOL,
              &gfstep_c,  &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,    &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

 


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Empty input strings" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;

   /*
   Note: the frame string is allowed to be empty. It'll
   be checked in zzgffvin_. 
   */
   gffove_c ( "",         "ellipsoid", raydir, "beta", "betafixed",
              "none",     "sun",       TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   gffove_c ( inst,       "",          raydir, "beta", "betafixed",
              "none",     "sun",       TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   This should drop through to the f2c'd routine gffove_.
   */
   gffove_c ( inst,        "ellipsoid",  raydir, "", "betafixed",
              "none",      "sun",        TIMTOL,
              &gfstep_c,   &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,   &gfrepu_c,    &gfrepf_c,
              SPICEFALSE,  &gfbail_c,
              &cnfine,     &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   /*
   This should drop through to the f2c'd routine gffove_.
   */
   gffove_c ( inst,        "ellipsoid",  raydir, "beta", "", 
              "none",      "sun",        TIMTOL,
              &gfstep_c,   &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,   &gfrepu_c,    &gfrepf_c,
              SPICEFALSE,  &gfbail_c,
              &cnfine,     &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFRAME)", ok );


   gffove_c ( inst,       "ellipsoid", raydir,  "beta", "betafixed",
              "",         "sun",       TIMTOL,
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   This should drop through to the f2c'd routine gffove_.
   */
   gffove_c ( inst,        "ellipsoid",  raydir, "beta",  "betafixed",
              "none",      "",           TIMTOL,
              &gfstep_c,   &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,   &gfrepu_c,    &gfrepf_c,
              SPICEFALSE,  &gfbail_c,
              &cnfine,     &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

  
 
  

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad cell data type" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 feb 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   gfsstp_c ( step );

   gffove_c ( inst,         "ellipsoid", raydir,    "beta",  "betafixed",
              "none",       "sun",       TIMTOL, 
              &gfstep_c,    &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,    &gfrepu_c,   &gfrepf_c,
              SPICEFALSE,   &gfbail_c,
              &badtype,     &result );
 
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );

  
   gffove_c ( inst,       "ellipsoid", raydir, "beta", "betafixed",
              "none",     "sun",       TIMTOL,   
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &badtype );
 
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Invalid shape" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   gfsstp_c ( step );

   gffove_c ( inst,       "box",       raydir, "beta", "betafixed",
              "none",     "sun",        TIMTOL,   
              &gfstep_c,  &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,    &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );     
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by gffove_.
   */
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSHAPE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Zero direction vector for ray target" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 30000.;
   gfsstp_c ( step );
  
   vpack_c ( 0.0, 0.0, 0.0, raydir );

   gffove_c ( inst,       "RAY",       raydir, "beta", "betafixed",
              "none",     "sun",        TIMTOL,   
              &gfstep_c,  &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,    &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );     
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by gffove_.
   */
   chckxc_c ( SPICETRUE, "SPICE(ZEROVECTOR)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Result window too small (detected before search)" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 300.;
   gfsstp_c ( step );

   gffove_c ( inst,       "ellipsoid",  raydir, "beta", "betafixed",
              "none",     "sun",        TIMTOL,   
              &gfstep_c,  &gfrefn_c,    SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,    &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &tooShort );     
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by gffove_.
   */
   chckxc_c ( SPICETRUE, "SPICE(WINDOWTOOSMALL)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Result window too small (detected during search)" );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   scard_c  ( 0,        &cnfine );
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = 300.;
   gfsstp_c ( step );
 
   ssize_c ( 2, &result );
   chckxc_c( SPICEFALSE, " ", ok );


   gffove_c ( inst,       "ellipsoid", raydir, "beta", "betafixed",
              "none",     "sun",       TIMTOL,    
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );          
   /*
   Note that the routine signaling the error depends on the relational
   operator. In this case the error is trapped by wninsd_.
   */
   chckxc_c ( SPICETRUE, "SPICE(WINDOWEXCESS)", ok );

   /*
   Restore original result window size. 
   */
   ssize_c ( MAXWIN, &result );
   chckxc_c( SPICEFALSE, " ", ok );


   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Normal search: find appearances of beta "
             "in FOV of ALPHA_RECTANGLE_NONE."           );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step = 300.0;
   gfsstp_c ( step );

   gffove_c ( "ALPHA_RECTANGLE_NONE",  "ellipsoid",
              raydir,     "beta",      "betafixed",
              "none",     "sun",       TIMTOL, 
              &gfstep_c,  &gfrefn_c,   SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,   &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c( SPICEFALSE, " ", ok );

   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );

   /*
   Check the entry and exit times. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      wnfetd_c ( &result, i, endpts, endpts+1 );
      chckxc_c ( SPICEFALSE, " ", ok );

      xtime = i * spd_c();

      strncpy ( qname, "Appearance start *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[0], "~", xtime, TIMTOL, ok );

      xtime += 600.0;

      strncpy ( qname, "Appearance stop *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[1], "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Normal search: find appearances of beta "
             "in FOV of ALPHA_ELLIPSE_NONE."           );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step = 300.0;
   gfsstp_c ( step );

   gffove_c ( "ALPHA_ELLIPSE_NONE",   "ellipsoid",
              raydir,     "beta",     "betafixed",
              "none",     "sun",      TIMTOL, 
              &gfstep_c,  &gfrefn_c,  SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,  &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );
   chckxc_c( SPICEFALSE, " ", ok );

   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );

   /*
   Check the entry and exit times. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      wnfetd_c ( &result, i, endpts, endpts+1 );
      chckxc_c ( SPICEFALSE, " ", ok );

      xtime = i * spd_c();

      strncpy ( qname, "Appearance start *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[0], "~", xtime, TIMTOL, ok );

      xtime += 600.0;

      strncpy ( qname, "Appearance stop *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[1], "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Normal search: find appearances of the Sun-beta "
             "direction ray in FOV of ALPHA_ELLIPSE_NONE." );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step = 300.0;

   /*
   The boresight vector points along the +X axis of the FOV frame
   of this instrument. 
   */
   vpack_c  ( 1.0, 0.0, 0.0, raydir );

   gffove_c ( "alpha_ellipse_none",  "ray",   raydir, "beta", "beta_view_xy",
              "none",     "sun",     TIMTOL, 
              &gfstep_c,  &gfrefn_c,  SPICEFALSE,
              &gfrepi_c,  &gfrepu_c,  &gfrepf_c,
              SPICEFALSE, &gfbail_c,
              &cnfine,    &result );  
   chckxc_c( SPICEFALSE, " ", ok );

   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );

   /*
   Check the entry and exit times. 
   */
   for ( i = 0;  i < n;  i++ )
   {
      wnfetd_c ( &result, i, endpts, endpts+1 );
      chckxc_c ( SPICEFALSE, " ", ok );

      xtime = i * spd_c() + 60.0;

      strncpy ( qname, "Appearance start *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[0], "~", xtime, TIMTOL, ok );

      xtime += 480.0;

      strncpy ( qname, "Appearance stop *", LNSIZE );
      repmi_c ( qname, "*", i, LNSIZE, qname );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksd_c ( qname, endpts[1], "~", xtime, TIMTOL, ok );
   }



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( (SpiceInt)handle );
   TRASH   ( SPK    );
   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gffove_c */

