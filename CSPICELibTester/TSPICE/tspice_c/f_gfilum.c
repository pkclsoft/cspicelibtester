/* f_gfilum.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__200 = 200;
static integer c__399 = 399;
static integer c__5 = 5;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__3 = 3;
static doublereal c_b287 = 0.;
static integer c__4 = 4;
static integer c__0 = 0;
static doublereal c_b306 = 1e-6;
static integer c__8 = 8;
static integer c__25 = 25;
static integer c__12 = 12;
static integer c__14 = 14;
static doublereal c_b542 = 1e-5;
static doublereal c_b552 = 1e-9;
static integer c__15 = 15;
static doublereal c_b825 = 1e-4;

/* $Procedure      F_GFILUM ( GFILUM family tests ) */
/* Subroutine */ int f_gfilum__(logical *ok)
{
    /* Initialized data */

    static char corrs[5*5] = "NONE " "LT   " "CN   " "LT+S " "CN+S ";
    static char relops[6*9] = "=     " "<     " ">     " "LOCMIN" "LOCMAX" 
	    "ABSMIN" "ABSMIN" "ABSMAX" "ABSMAX";
    static doublereal vals[9] = { 65.,70.,60.,0.,0.,0.,0.,0.,0. };
    static doublereal adj[9] = { 0.,0.,0.,0.,0.,0.,.05,0.,.05 };

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int gfpa_(char *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal left, last, step;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static doublereal work[3090]	/* was [206][15] */;
    static char time0[50];
    static integer i__;
    extern integer cardd_(doublereal *);
    static integer n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal phase;
    static integer octid;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static char coord[25];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal right;
    extern /* Subroutine */ int copyd_(doublereal *, doublereal *);
    static doublereal solar;
    static char illum[36], title[800];
    static doublereal first, xtime;
    static integer count;
    static doublereal start;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    static integer opidx;
    extern /* Subroutine */ int topen_(char *, ftnlen), spkw08_(integer *, 
	    integer *, integer *, char *, doublereal *, doublereal *, char *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), bodn2c_(char *, integer *, logical *, ftnlen), 
	    t_success__(logical *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal np[3];
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int scardd_(integer *, doublereal *);
    static doublereal cnfine[206];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char abcorr[5];
    static doublereal modcfn[206];
    static char relate[6];
    static integer xn;
    extern integer wncard_(doublereal *);
    static char method[80], angtyp[25], corsys[25], fixref[32], obsrvr[36], 
	    srfref[32], target[36];
    static doublereal adjust, emissn, et0, et1, finish, modres[206], refval, 
	    result[206], spoint[3], srfvec[3], states[12]	/* was [6][2] 
	    */, trgepc, xrsult[206];
    static integer coridx;
    extern /* Subroutine */ int natspk_(char *, logical *, integer *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen);
    static doublereal beg;
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     tstpck_(char *, logical *, logical *, ftnlen), tstlsk_(void), 
	    ssized_(integer *, doublereal *), wninsd_(doublereal *, 
	    doublereal *, doublereal *), srfrec_(integer *, doublereal *, 
	    doublereal *, doublereal *), gfilum_(char *, char *, char *, char 
	    *, char *, char *, char *, doublereal *, char *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal end;
    extern /* Subroutine */ int bodvrd_(char *, char *, integer *, integer *, 
	    doublereal *, ftnlen, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), wnfetd_(
	    doublereal *, integer *, doublereal *, doublereal *), lmpool_(
	    char *, integer *, ftnlen), spkopn_(char *, char *, integer *, 
	    integer *, ftnlen, ftnlen), pdpool_(char *, integer *, doublereal 
	    *, ftnlen), nearpt_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    static doublereal alt, lat;
    extern /* Subroutine */ int spkcls_(integer *), furnsh_(char *, ftnlen), 
	    ilumin_(char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    extern doublereal rpd_(void), spd_(void);
    static doublereal lon;
    extern /* Subroutine */ int gfposc_(char *, char *, char *, char *, char *
	    , char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), spkpos_(
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), gfstol_(doublereal 
	    *), spkuef_(integer *), delfil_(char *, ftnlen), unload_(char *, 
	    ftnlen);
    static doublereal pos[3];
    static char txt[80*25];
    static integer han1, han2;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFILUM */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFILUM. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.0.0, 02-APR-2014 (NJB)(EDW) */

/*        Previous version was 29-NOV-2012 (NJB)(EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */

/*      INTEGER               NAMLEN */
/*      PARAMETER           ( NAMLEN = 32 ) */
/*      INTEGER               NREF */
/*      PARAMETER           ( NREF = 3 ) */

/*     Local variables */


/*     The workspace array has to handle the largest workspace */
/*     we'll use, which is that required by GFPOSC. */


/*     Saved everything. */


/*     Initial values */


/*     Note: the absolute extremum operators are repeated because */
/*     they're used with both zero and non-zero adjustment values. */


/*     Reference values to be used with binary operators: */
/*     These values are in degrees; they must be converted to */
/*     radians before use. */


/*     Ajustment values to be used with absolute extremum operators: */


/*     Begin every test family with an open call. */

    topen_("F_GFILUM", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("generic.bsp", &c_true, &han2, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a confinement window. Initialize this and */
/*     the result window. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + (i__ - 1) * spd_() + 3600.;
	right = left + 79200.;
	wninsd_(&left, &right, cnfine);
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Non-positive step size", (ftnlen)22);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = 1e3;
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 0.;
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = -1.;
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Workspace window size too small.", (ftnlen)32);
    ssized_(&c__1, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = 1e3;
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__1, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small (detected before search)", (ftnlen)48);
    ssized_(&c__1, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = 1e3;
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small (detected during search)", (ftnlen)48);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = 1e3;
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 TDB", &et0, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 5 TDB", &et1, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 300.;
    ssized_(&c__2, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(OUTOFROOM)", ok, (ftnlen)16);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized value of FIXREF.", (ftnlen)29);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, "XYZ", abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)3,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("FIXREF is not centered on the target body.", (ftnlen)42);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, "IAU_MARS", abcorr, obsrvr, spoint,
	     relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)8,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad computation method.", (ftnlen)23);
    step = 1.;
    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    s_copy(method, "DSK", (ftnlen)80, (ftnlen)3);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad illumination angle type", (ftnlen)27);
    step = 1.;
    s_copy(angtyp, "XYZ", (ftnlen)25, (ftnlen)3);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad relational operator", (ftnlen)23);
    step = 1.;
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(relate, ">=", (ftnlen)6, (ftnlen)2);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction values", (ftnlen)32);
    step = 1.;
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    gfilum_(method, angtyp, target, illum, fixref, "S", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)1, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfilum_(method, angtyp, target, illum, fixref, "XS", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)2, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfilum_(method, angtyp, target, illum, fixref, "RLT", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)3, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfilum_(method, angtyp, target, illum, fixref, "XRLT", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)4, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfilum_(method, angtyp, target, illum, fixref, "Z", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)1, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Transmission aberration correction values.", (ftnlen)42);
    step = 1.;
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);
    gfilum_(method, angtyp, target, illum, fixref, "XLT", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)3, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    gfilum_(method, angtyp, target, illum, fixref, "XCN", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)3, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    gfilum_(method, angtyp, target, illum, fixref, "XLT+S", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    gfilum_(method, angtyp, target, illum, fixref, "XCN+S", obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad value of ADJUST.", (ftnlen)20);
    adjust = -1.;
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized target, observer, or illumination source.", (ftnlen)
	    54);
    adjust = 0.;
    gfilum_(method, angtyp, "X", illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)1, (ftnlen)36, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    gfilum_(method, angtyp, target, "X", fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)1, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, "X", spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)1, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and observer are identical.", (ftnlen)34);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, target, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and illumination source are identical.", (ftnlen)45);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, target, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for observer", (ftnlen)24);
    step = 1.;
    gfilum_(method, angtyp, target, illum, fixref, abcorr, "GASPRA", spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)6, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for target", (ftnlen)22);
    step = 1.;
    s_copy(fixref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    gfilum_(method, angtyp, "GASPRA", illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)6, (ftnlen)36, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for illumination source", (ftnlen)35);
    step = 1.;
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    gfilum_(method, angtyp, target, "GASPRA", fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)6, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for target.", (ftnlen)35);
    step = 1e3;
    gfilum_(method, angtyp, target, illum, "ITRF93", abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)6,
	     (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Simple cases using Nat's solar system */
/* * */
/* ********************************************************************* */

/*     The cases below all involve finding times when local extrema are */
/*     attained. In each case we know the correct answers. All of these */
/*     tests are done with aberration corrections set to 'NONE'. */

/*     Following these tests, a series of comparisons is performed using */
/*     results produced by alternate methods. The second set of tests is */
/*     comprehensive: those tests use all combinations of operators and */
/*     aberration corrections. */


/* ---- Case ------------------------------------------------------------- */


/*     All expected event times are based on Nat's solar system. */

    tcase_("Local minimum of emission angle at north pole of ALPHA; observer"
	    " is SUN; abcorr = NONE", (ftnlen)86);
    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    s_copy(relate, "locmin", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    s_copy(target, " ALPHA", (ftnlen)36, (ftnlen)6);
    s_copy(fixref, " ALPHAFIXED", (ftnlen)32, (ftnlen)11);
    s_copy(obsrvr, " SUN", (ftnlen)36, (ftnlen)4);
    s_copy(illum, " SUN", (ftnlen)36, (ftnlen)4);
    bodvrd_("ALPHA", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b287, &c_b287, &radii[2], spoint);
    s_copy(time0, "2000 JAN 1 12:00:00 TDB", (ftnlen)50, (ftnlen)23);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check sun position for debugging: */
/*     CALL SPKPOS ( OBSRVR, ET0, FIXREF, 'NONE', TARGET, POS, LT ) */
/*     WRITE (*,*) 'Position of sun wrt alpha at J2000:' */
/*     WRITE (*,*) POS */

    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     The period of ALPHA's orbit is 7 days. This can be derived */
/*     from the relative angular velocities of ALPHA and BETA */
/*     and the period of the occultation starts. */

/*     A minimum of the emission angle should occur whenever ALPHA's */
/*     north pole points toward the sun. This should happen at the */
/*     J2000 epoch and every 7 days before or after. */

/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + (i__ - 1) * spd_() * 7 - 3600.;
	right = left + 7200.;
	wninsd_(&left, &right, cnfine);
    }

/*     We can use a large step. The extrema should be 12 hours */
/*     apart. */

    step = 3.6e4;
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	xtime = et0 + (doublereal) (i__ - 1) * spd_() * 7;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Dump event times for debugging: */

/*         CALL TIMOUT ( LEFT, */
/*     .                 'YYYY MON DD HR:MN:SC.###### ::TDB (TDB)', */
/*     .                 TIMSTR ) */
/*         WRITE (*,*) I, ' ',  TIMSTR */

/*         CALL TIMOUT ( RIGHT, */
/*     .                 'YYYY MON DD HR:MN:SC.###### ::TDB (TDB)', */
/*     .                 TIMSTR ) */
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */


/*     All expected event times are based on Nat's solar system. */

    tcase_("Local minimum of solar incidence angle at north pole of ALPHA; o"
	    "bserver is SUN; abcorr = NONE", (ftnlen)93);

/*     This is a repeat of the previous case, with only the angle */
/*     type changed. */

    s_copy(angtyp, " Incidence", (ftnlen)25, (ftnlen)10);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	xtime = et0 + (doublereal) (i__ - 1) * spd_() * 7;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */


/*     All expected event times are based on Nat's solar system. */

    tcase_("Local maximum of emission angle at north pole of ALPHA; observer"
	    " is SUN; abcorr = NONE", (ftnlen)86);
    s_copy(relate, "LOCMAX", (ftnlen)6, (ftnlen)6);

/*     ALPHA orbits with constant angular velocity on a circular path, */
/*     so local maxima occur 1/2 period apart from local minima. */


/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + spd_() * 3.5 + (i__ - 1) * spd_() * 7 - 3600.;
	right = left + 7200.;
	wninsd_(&left, &right, cnfine);
    }
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	xtime = et0 + spd_() * 3.5 + (i__ - 1) * spd_() * 7;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Dump event times for debugging: */

/*         CALL TIMOUT ( LEFT, */
/*     .                 'YYYY MON DD HR:MN:SC.###### ::TDB (TDB)', */
/*     .                 TIMSTR ) */
/*         WRITE (*,*) I, ' ',  TIMSTR */

/*         CALL TIMOUT ( RIGHT, */
/*     .                 'YYYY MON DD HR:MN:SC.###### ::TDB (TDB)', */
/*     .                 TIMSTR ) */
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Local maximum of solar incidence angle at north pole of ALPHA; o"
	    "bserver is SUN; abcorr = NONE", (ftnlen)93);

/*     This is a repeat of the previous case, with only the angle */
/*     type changed. */

    s_copy(angtyp, "  Incidence", (ftnlen)25, (ftnlen)11);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	xtime = et0 + spd_() * 3.5 + (i__ - 1) * spd_() * 7;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Local minimum of phase angle at -X axis of ALPHA; observer is BE"
	    "TA; abcorr = NONE", (ftnlen)81);

/*     This is a phase angle search. */

    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);

/*     This time we want to use Beta as the observer. Note that */
/*     the surface point on Alpha's north pole won't do for this */
/*     computation. */

/*     We're going to work around this problem by changing the */
/*     orientation of Alpha. We'll use the ALPHA_VIEW_XY frame as */
/*     Alpha's body-fixed frame. This is a two-vector dynamic frame */
/*     in which the +X axis points from the sun to Alpha at all times. */
/*     We'll create a sun-facing surface point on Alpha at the tip */
/*     of Alpha's -X axis. */

    bodvrd_("ALPHA", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -radii[0];
    vpack_(&d__1, &c_b287, &c_b287, spoint);
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);

/*     Set up the participants. */

    s_copy(obsrvr, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(relate, "LOCMIN", (ftnlen)6, (ftnlen)6);
    step = 3600.;

/*     Set the confinement window to cover a single 4-day time interval. */

    et0 = 0.;
    et1 = et0 + spd_() * 4;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__8, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);

/*        The phase angle is minimized at the central time of */
/*        each occultation and midway between these times. */

	xtime = (i__ - 1) * spd_() / 2 + 300.;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Repeat the previous test with the illumination source set to BET"
	    "A and the observer set to SUN.", (ftnlen)94);

/*     We need at least one test in which the sun is not the illumination */
/*     source. The event times should be unchanged. */


/*     This is a phase angle search. */

    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);

/*     This time we want to use Beta as the observer. Note that */
/*     the surface point on Alpha's north pole won't do for this */
/*     computation. */

/*     We're going to work around this problem by changing the */
/*     orientation of Alpha. We'll use the ALPHA_VIEW_XY frame as */
/*     Alpha's body-fixed frame. This is a two-vector dynamic frame */
/*     in which the +X axis points from the sun to Alpha at all times. */
/*     We'll create a sun-facing surface point on Alpha at the tip */
/*     of Alpha's -X axis. */

    bodvrd_("ALPHA", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -radii[0];
    vpack_(&d__1, &c_b287, &c_b287, spoint);
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);

/*     Set up the participants. */

    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(relate, "LOCMIN", (ftnlen)6, (ftnlen)6);
    step = 3600.;

/*     Set the confinement window to cover a single 4-day time interval. */

    et0 = 0.;
    et1 = et0 + spd_() * 4;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__8, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check event start time. */

	s_copy(title, "Event # beg time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);

/*        The phase angle is minimized at the central time of */
/*        each occultation and midway between these times. */

	xtime = (i__ - 1) * spd_() / 2 + 300.;
	chcksd_(title, &left, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);

/*        Check event end time; this should equal the begin time */
/*        since we're searching for an extremum. */

	s_copy(title, "Event # end time", (ftnlen)800, (ftnlen)16);
	repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	chcksd_(title, &right, "~", &xtime, &c_b306, ok, (ftnlen)800, (ftnlen)
		1);
    }
/* ********************************************************************* */
/* * */
/* *    Comprehensive cases using comparisons against alternate */
/* *    computations */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    PHASE angle tests */
/* * */
/* ********************************************************************* */

/*     We'use use the earth, moon, and sun as the three participating */
/*     bodies. The surface point will be the OCTL telescope; we'll */
/*     create an SPK file for this point. We also need an FK for a */
/*     topocentric frame centered at the point. */

/*     Though it's not strictly necessary, we'll use real data for */
/*     these kernels, with one exception: we'll use the terrestrial */
/*     reference frame IAU_EARTH rather than ITRF93. */

/*     The original reference frame specifications follow: */


/*        Topocentric frame OCTL_TOPO */

/*           The Z axis of this frame points toward the zenith. */
/*           The X axis of this frame points North. */

/*           Topocentric frame OCTL_TOPO is centered at the site OCTL */
/*           which has Cartesian coordinates */

/*              X (km):                 -0.2448937761729E+04 */
/*              Y (km):                 -0.4667935793438E+04 */
/*              Z (km):                  0.3582748499430E+04 */

/*           and planetodetic coordinates */

/*              Longitude (deg):      -117.6828380000000 */
/*              Latitude  (deg):        34.3817491000000 */
/*              Altitude   (km):         0.2259489999999E+01 */

/*           These planetodetic coordinates are expressed relative to */
/*           a reference spheroid having the dimensions */

/*              Equatorial radius (km):  6.3781400000000E+03 */
/*              Polar radius      (km):  6.3567523100000E+03 */

/*           All of the above coordinates are relative to the frame */
/*           ITRF93. */


/* ---- Case ------------------------------------------------------------- */


/*     This isn't a test, but we'll call it that so we'll have */
/*     a meaningful label in any error messages that arise. */

    tcase_("Create OCTL kernels.", (ftnlen)20);

/*     As mentioned, we go with a frame that's more convenient than */
/*     ITRF93: */

    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Prepare a frame kernel in a string array. */

    s_copy(txt, "FRAME_OCTL_TOPO            =  1398962", (ftnlen)80, (ftnlen)
	    37);
    s_copy(txt + 80, "FRAME_1398962_NAME         =  'OCTL_TOPO' ", (ftnlen)80,
	     (ftnlen)42);
    s_copy(txt + 160, "FRAME_1398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 240, "FRAME_1398962_CLASS_ID     =  1398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 320, "FRAME_1398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 400, "OBJECT_398962_FRAME        =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 480, "TKFRAME_1398962_RELATIVE   =  'IAU_EARTH' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 560, "TKFRAME_1398962_SPEC       =  'ANGLES' ", (ftnlen)80, (
	    ftnlen)39);
    s_copy(txt + 640, "TKFRAME_1398962_UNITS      =  'DEGREES' ", (ftnlen)80, 
	    (ftnlen)40);
    s_copy(txt + 720, "TKFRAME_1398962_AXES       =  ( 3, 2, 3 )", (ftnlen)80,
	     (ftnlen)41);
    s_copy(txt + 800, "TKFRAME_1398962_ANGLES     =  ( -242.3171620000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 880, "                                 -55.6182509000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 960, "                                 180.0000000000000  )",
	     (ftnlen)80, (ftnlen)53);
    s_copy(txt + 1040, "NAIF_BODY_NAME            +=  'OCTL' ", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1120, "NAIF_BODY_CODE            +=  398962", (ftnlen)80, (
	    ftnlen)36);

/*     It will be convenient to have a version of this frame that */
/*     has the +Z axis pointed down instead of up. */

    s_copy(txt + 1200, "FRAME_OCTL_FLIP            =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1280, "FRAME_2398962_NAME         =  'OCTL_FLIP' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1360, "FRAME_2398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 1440, "FRAME_2398962_CLASS_ID     =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1520, "FRAME_2398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 1600, "TKFRAME_2398962_RELATIVE   =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1680, "TKFRAME_2398962_SPEC       =  'ANGLES' ", (ftnlen)80, 
	    (ftnlen)39);
    s_copy(txt + 1760, "TKFRAME_2398962_UNITS      =  'DEGREES' ", (ftnlen)80,
	     (ftnlen)40);
    s_copy(txt + 1840, "TKFRAME_2398962_AXES       =  ( 3, 2, 3 )", (ftnlen)
	    80, (ftnlen)41);
    s_copy(txt + 1920, "TKFRAME_2398962_ANGLES     =  ( 0, 180.0, 0 ) ", (
	    ftnlen)80, (ftnlen)46);
    lmpool_(txt, &c__25, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now create an SPK file containing a type 8 segment for OCTL. */

    spkopn_("octl_test.bsp", "octl_test.bsp", &c__0, &handle, (ftnlen)13, (
	    ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize both states to zero. */

    cleard_(&c__12, states);

/*     The first position component: */

    spoint[0] = -2448.937761729;
    spoint[1] = -4667.935793438;
    spoint[2] = 3582.74849943;

/*     We're going to use a version of the OCTL position */
/*     that has zero altitude relative to the earth's */
/*     reference ellipsoid. This is done to enable */
/*     consistency checks to be done using GFPOSC. */

/*     For compatibility with the topocentric frame */
/*     specification above, we'll use the following */
/*     earth radii: */

    radii[0] = 6378.14;
    radii[1] = 6378.14;
    radii[2] = 6356.75231;
    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    nearpt_(spoint, radii, &radii[1], &radii[2], np, &alt);
    vequ_(np, spoint);
    vequ_(spoint, states);

/*     The second position matches the first: we don't model */
/*     plate motion. */

    vequ_(spoint, &states[6]);

/*     Time bounds for the segment: */

    first = spd_() * -50 * 365.25;
    step = spd_() * 100 * 365.25;
    last = first + step - 1e-6;

/*     Get the OCTL ID code from the kernel we just */
/*     loaded. */

    bodn2c_("OCTL", &octid, &found, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Write the segment. */

    spkw08_(&handle, &octid, &c__399, fixref, &first, &last, "octl", &c__1, &
	    c__2, states, &first, &step, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now load the OCTL SPK file. */

    furnsh_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Phase angle tests: we'll compare results from GFILUM to those */
/*     obtained using GFPA. Note that the surface point must be */
/*     an ephemeris object in order to carry out these tests. */

    s_copy(angtyp, " Phase", (ftnlen)25, (ftnlen)6);
    s_copy(illum, "Sun", (ftnlen)36, (ftnlen)3);
    s_copy(target, " 399", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, " moon", (ftnlen)36, (ftnlen)5);

/*     Note that FIXREF has already been set. */


/*     Set up the confinement window. */

    s_copy(time0, "2011 JAN 1", (ftnlen)50, (ftnlen)10);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Search over approximately two months. */

    et1 = et0 + spd_() * 60;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);

/*     Use a 10 day step. We expect the extrema to */
/*     be about 14 days apart. */

    step = spd_() * 10;

/*     Loop over all operators and aberration corrections. */

    for (opidx = 1; opidx <= 9; ++opidx) {

/*        Convert the reference value to radians. */

	s_copy(relate, relops + ((i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("relops", i__1, "f_gfilum__", (ftnlen)1632)) * 6, (
		ftnlen)6, (ftnlen)6);
	refval = vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"vals", i__1, "f_gfilum__", (ftnlen)1633)] * rpd_();
	adjust = adj[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"adj", i__1, "f_gfilum__", (ftnlen)1634)];
	for (coridx = 1; coridx <= 5; ++coridx) {
	    s_copy(abcorr, corrs + ((i__1 = coridx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("corrs", i__1, "f_gfilum__", (ftnlen)1639)) 
		    * 5, (ftnlen)5, (ftnlen)5);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "Phase angle search: RELATE = #; REFVAL (deg) = #;"
		    " ADJUST = #; ABCORR = #; observer = #; target = #; illum"
		    " source = #; FIXREF = #; SPOINT = ( # # # ).", (ftnlen)
		    800, (ftnlen)149);
	    repmc_(title, "#", relate, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    6, (ftnlen)800);
	    repmd_(title, "#", &vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? 
		    i__1 : s_rnge("vals", i__1, "f_gfilum__", (ftnlen)1649)], 
		    &c__14, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &adjust, &c__14, title, (ftnlen)800, (ftnlen)1,
		     (ftnlen)800);
	    repmc_(title, "#", abcorr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    5, (ftnlen)800);
	    repmc_(title, "#", obsrvr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", target, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", illum, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", fixref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    repmd_(title, "#", spoint, &c__14, title, (ftnlen)800, (ftnlen)1, 
		    (ftnlen)800);
	    repmd_(title, "#", &spoint[1], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &spoint[2], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)800);

/*           Initialize the result windows. */

	    ssized_(&c__200, result);
	    ssized_(&c__200, xrsult);

/*           Find the expected result window. Note that the */
/*           target is OCTL in this case. */

	    gfpa_("OCTL", illum, abcorr, obsrvr, relate, &refval, &adjust, &
		    step, cnfine, &c__200, &c__5, work, xrsult, (ftnlen)4, (
		    ftnlen)36, (ftnlen)5, (ftnlen)36, (ftnlen)6);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {

/*              Search for the condition of interest using GFILUM. */

		gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr,
			 spoint, relate, &refval, &adjust, &step, cnfine, &
			c__200, &c__5, work, result, (ftnlen)80, (ftnlen)25, (
			ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)
			36, (ftnlen)6);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (*ok) {

/*                 Compare result window cardinalities first. */

		    xn = wncard_(xrsult);

/*                 Test family validity check: we're not supposed to */
/*                 have any cases where the results are empty. */

		    chcksi_("XN", &xn, ">", &c__0, &c__0, ok, (ftnlen)2, (
			    ftnlen)1);
		    n = wncard_(result);
		    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)
			    1);
		    if (*ok) {

/*                    Compare result window interval bounds. */

			i__1 = n << 1;
			chckad_("RESULT", &result[6], "~", &xrsult[6], &i__1, 
				&c_b542, ok, (ftnlen)6, (ftnlen)1);
			if (*ok) {

/*                       The result window found by GFILUM agrees */
/*                       with that found by GFPA. Check the actual */
/*                       angular values at the window endpoints */
/*                       for the cases where the operator is */
/*                       binary. */

			    if (s_cmp(relate, "=", (ftnlen)6, (ftnlen)1) == 0)
				     {

/*                          Check the phase angle at each interval */
/*                          endpoint. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksd_(title, &phase, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksd_(title, &phase, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				}
			    } else if (s_cmp(relate, "<", (ftnlen)6, (ftnlen)
				    1) == 0 || s_cmp(relate, ">", (ftnlen)6, (
				    ftnlen)1) == 0) {

/*                          Perform checks only at interval endpoints */
/*                          contained in the interior of the confinement */
/*                          window. At the confinement window boundaries, */
/*                          the inequality may be satisfied without the */
/*                          value being equal to the reference value. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (start > et0) {
					chcksd_(title, &phase, "~", &refval, &
						c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (finish < et1) {
					chcksd_(title, &phase, "~", &refval, &
						c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				}

/*                          End of phase angle value check loop. */

			    }

/*                       End of phase angle checks. */

			}

/*                    End of IF block for successful RESULT check. */

		    }

/*                 End of interval endpoint checks. */

		}

/*              End of IF block for successful GFILUM call. */

	    }

/*           End of IF block for successful GFPA call. */

	}

/*        End of aberration correction loop. */

    }

/*     End of operator loop. */

/* ********************************************************************* */
/* * */
/* *    EMISSION angle tests */
/* * */
/* ********************************************************************* */

/*     Emission angle tests: we'll compare results from GFILUM to those */
/*     obtained using GFPOSC. Note that the surface point must be */
/*     an ephemeris object having an associated topocentric frame */
/*     in order to carry out these tests. */


/*     The emission angle is the supplement of the colatitude, */
/*     measured in the surface point topocentric frame, of */
/*     the observer-surface point vector. Equivalently, the emission */
/*     angle is the colatitude of the observer-surface point vector */
/*     in the "flip" frame, which has its +Z axis pointing downward. */

/*     We can use GFPOSC to find times when this colatitude, measured */
/*     in the flip frame, meets conditions on the emission angle. */

    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(srfref, "OCTL_FLIP", (ftnlen)32, (ftnlen)9);
    s_copy(corsys, "SPHERICAL", (ftnlen)25, (ftnlen)9);
    s_copy(coord, "COLATITUDE", (ftnlen)25, (ftnlen)10);
    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);

/*     For the emission angle test, we don't need to use */
/*     a month-long confinement window. A few days is enough. */

    et1 = et0 + spd_() * 3;

/*     Use a 6 hour step. We expect the extrema to */
/*     be 12+delta hours apart, where delta may be */
/*     a few hours. */

    step = 21600.;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Loop over all operators and aberration corrections. */

    for (opidx = 1; opidx <= 9; ++opidx) {

/*        Convert the reference value to radians. */

	s_copy(relate, relops + ((i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("relops", i__1, "f_gfilum__", (ftnlen)1902)) * 6, (
		ftnlen)6, (ftnlen)6);
	refval = vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"vals", i__1, "f_gfilum__", (ftnlen)1903)] * rpd_();
	adjust = adj[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"adj", i__1, "f_gfilum__", (ftnlen)1904)];
	for (coridx = 1; coridx <= 5; ++coridx) {
	    s_copy(abcorr, corrs + ((i__1 = coridx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("corrs", i__1, "f_gfilum__", (ftnlen)1909)) 
		    * 5, (ftnlen)5, (ftnlen)5);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "Emission angle search: RELATE = #; REFVAL (deg) ="
		    " #; ADJUST = #; ABCORR = #; observer = #; target = #; il"
		    "lum source = #; FIXREF = #; SPOINT = ( # # # ); SRFREF ="
		    " #.", (ftnlen)800, (ftnlen)164);
	    repmc_(title, "#", relate, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    6, (ftnlen)800);
	    repmd_(title, "#", &vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? 
		    i__1 : s_rnge("vals", i__1, "f_gfilum__", (ftnlen)1920)], 
		    &c__14, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &adjust, &c__14, title, (ftnlen)800, (ftnlen)1,
		     (ftnlen)800);
	    repmc_(title, "#", abcorr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    5, (ftnlen)800);
	    repmc_(title, "#", obsrvr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", target, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", illum, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", fixref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    repmd_(title, "#", spoint, &c__14, title, (ftnlen)800, (ftnlen)1, 
		    (ftnlen)800);
	    repmd_(title, "#", &spoint[1], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &spoint[2], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmc_(title, "#", srfref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)800);

/*           Initialize the result windows. */

	    ssized_(&c__200, result);
	    ssized_(&c__200, xrsult);

/*           Find the expected result window. Note that the */
/*           target is OCTL in this case. */

	    gfposc_("OCTL", srfref, abcorr, obsrvr, corsys, coord, relate, &
		    refval, &adjust, &step, cnfine, &c__200, &c__15, work, 
		    xrsult, (ftnlen)4, (ftnlen)32, (ftnlen)5, (ftnlen)36, (
		    ftnlen)25, (ftnlen)25, (ftnlen)6);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {

/*              Search for the condition of interest using GFILUM. */

		gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr,
			 spoint, relate, &refval, &adjust, &step, cnfine, &
			c__200, &c__5, work, result, (ftnlen)80, (ftnlen)25, (
			ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)
			36, (ftnlen)6);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (*ok) {

/*                 Compare result window cardinalities first. */

		    xn = wncard_(xrsult);

/*                 Test family validity check: we're not supposed to */
/*                 have any cases where the results are empty. */

		    chcksi_("XN", &xn, ">", &c__0, &c__0, ok, (ftnlen)2, (
			    ftnlen)1);
/*                  WRITE (*,*) RELATE, '  ', 'XN = ', XN */
		    n = wncard_(result);
		    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)
			    1);
		    if (*ok) {

/*                    Compare result window interval bounds. */

			i__1 = n << 1;
			chckad_("RESULT", &result[6], "~", &xrsult[6], &i__1, 
				&c_b542, ok, (ftnlen)6, (ftnlen)1);
			if (*ok) {

/*                       The result window found by GFILUM agrees */
/*                       with that found by GFPOSC. Check the actual */
/*                       angular values at the window endpoints */
/*                       for the cases where the operator is */
/*                       binary. */

			    if (s_cmp(relate, "=", (ftnlen)6, (ftnlen)1) == 0)
				     {

/*                          Check the emission angle at each interval */
/*                          endpoint. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksd_(title, &emissn, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chcksd_(title, &emissn, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				}
			    } else if (s_cmp(relate, "<", (ftnlen)6, (ftnlen)
				    1) == 0 || s_cmp(relate, ">", (ftnlen)6, (
				    ftnlen)1) == 0) {

/*                          Perform checks only at interval endpoints */
/*                          contained in the interior of the confinement */
/*                          window. At the confinement window boundaries, */
/*                          the inequality may be satisfied without the */
/*                          value being equal to the reference value. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (start > et0) {
					chcksd_(title, &emissn, "~", &refval, 
						&c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (finish < et1) {
					chcksd_(title, &emissn, "~", &refval, 
						&c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				}

/*                          End of emission angle value check loop. */

			    }

/*                       End of emission angle checks. */

			}

/*                    End of IF block for successful RESULT check. */

		    }

/*                 End of interval endpoint checks. */

		}

/*              End of IF block for successful GFILUM call. */

	    }

/*           End of IF block for successful GFPOSC call. */

	}

/*        End of aberration correction loop. */

    }

/*     End of operator loop. */

/* ********************************************************************* */
/* * */
/* *    INCIDENCE angle tests */
/* * */
/* ********************************************************************* */

/*     Incidence angle tests: we'll compare results from GFILUM to */
/*     those obtained using GFPOSC, where in the latter case, the */
/*     surface point is treated as the observer. Results obtained using */
/*     GFILUM must have one-way observer-surface point light time */
/*     subtracted in order to be comparable to those from GFPOSC. */

/*     Note that the surface point must be an ephemeris object having an */
/*     associate topocentric frame in order to carry out these tests. */

/*     In the geometric case, the solar incidence angle is the */
/*     colatitude, measured in the surface point topocentric frame, of */
/*     the surface point-sun vector. When aberration corrections are */
/*     used, the surface point-sun vector must be computed at an epoch */
/*     corrected for observer-surface point light time. */

/*     We can use GFPOSC to find times when this colatitude, measured in */
/*     the OCTL topocentric frame, meets conditions on the solar */
/*     incidence angle. */

    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(srfref, "OCTL_TOPO", (ftnlen)32, (ftnlen)9);
    s_copy(corsys, "SPHERICAL", (ftnlen)25, (ftnlen)9);
    s_copy(coord, "COLATITUDE", (ftnlen)25, (ftnlen)10);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);

/*     For the solar incidence angle test, we don't need to use */
/*     a month-long confinement window. A few days is enough. */


/*     Set up the confinement window. */

    s_copy(time0, "2011 JAN 1", (ftnlen)50, (ftnlen)10);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et1 = et0 + spd_() * 3;

/*     Use a 6 hour step. We expect the extrema to */
/*     be 12+delta hours apart, where delta may be */
/*     a few hours. */

    step = 21600.;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Loop over all operators and aberration corrections. */

    for (opidx = 1; opidx <= 9; ++opidx) {

/*        Convert the reference value to radians. */

	s_copy(relate, relops + ((i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("relops", i__1, "f_gfilum__", (ftnlen)2191)) * 6, (
		ftnlen)6, (ftnlen)6);
	refval = vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"vals", i__1, "f_gfilum__", (ftnlen)2192)] * rpd_();
	adjust = adj[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"adj", i__1, "f_gfilum__", (ftnlen)2193)];
	for (coridx = 1; coridx <= 5; ++coridx) {
	    s_copy(abcorr, corrs + ((i__1 = coridx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("corrs", i__1, "f_gfilum__", (ftnlen)2198)) 
		    * 5, (ftnlen)5, (ftnlen)5);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "Solar incidence angle search: RELATE = #; REFVAL "
		    "(deg) = #; ADJUST = #; ABCORR = #; observer = #; target "
		    "= #; illum source = #; FIXREF = #; SPOINT = ( # # # ); S"
		    "RFREF = #.", (ftnlen)800, (ftnlen)171);
	    repmc_(title, "#", relate, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    6, (ftnlen)800);
	    repmd_(title, "#", &vals[(i__1 = opidx - 1) < 9 && 0 <= i__1 ? 
		    i__1 : s_rnge("vals", i__1, "f_gfilum__", (ftnlen)2209)], 
		    &c__14, title, (ftnlen)800, (ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &adjust, &c__14, title, (ftnlen)800, (ftnlen)1,
		     (ftnlen)800);
	    repmc_(title, "#", abcorr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    5, (ftnlen)800);
	    repmc_(title, "#", obsrvr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", target, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", illum, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", fixref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    repmd_(title, "#", spoint, &c__14, title, (ftnlen)800, (ftnlen)1, 
		    (ftnlen)800);
	    repmd_(title, "#", &spoint[1], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &spoint[2], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmc_(title, "#", srfref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)800);

/*           Initialize the result windows. */

	    ssized_(&c__200, result);
	    ssized_(&c__200, xrsult);
	    ssized_(&c__200, modcfn);
	    ssized_(&c__200, modres);
	    if (eqstr_(abcorr, "NONE", (ftnlen)5, (ftnlen)4)) {
		copyd_(cnfine, modcfn);
	    } else {

/*              Create a modified confinement window: this is the */
/*              original confinement window, adjusted for one-way light */
/*              time between the observer and the surface point. */

		i__1 = cardd_(cnfine);
		for (i__ = 1; i__ <= i__1; ++i__) {
		    et = cnfine[(i__2 = i__ + 5) < 206 && 0 <= i__2 ? i__2 : 
			    s_rnge("cnfine", i__2, "f_gfilum__", (ftnlen)2244)
			    ];
		    spkpos_("OCTL", &et, "J2000", abcorr, obsrvr, pos, &lt, (
			    ftnlen)4, (ftnlen)5, (ftnlen)5, (ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    modcfn[(i__2 = i__ + 5) < 206 && 0 <= i__2 ? i__2 : 
			    s_rnge("modcfn", i__2, "f_gfilum__", (ftnlen)2250)
			    ] = et - lt;
		}
		i__1 = cardd_(cnfine);
		scardd_(&i__1, modcfn);
	    }

/*           Find the expected result window. Note that the */
/*           target is the sun and the observer is OCTL in this case. */

	    gfposc_("SUN", srfref, abcorr, "OCTL", corsys, coord, relate, &
		    refval, &adjust, &step, modcfn, &c__200, &c__15, work, 
		    xrsult, (ftnlen)3, (ftnlen)32, (ftnlen)5, (ftnlen)4, (
		    ftnlen)25, (ftnlen)25, (ftnlen)6);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (*ok) {

/*              Search for the condition of interest using GFILUM. */

		gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr,
			 spoint, relate, &refval, &adjust, &step, cnfine, &
			c__200, &c__5, work, result, (ftnlen)80, (ftnlen)25, (
			ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)
			36, (ftnlen)6);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (*ok) {

/*                 Compare result window cardinalities first. */

		    xn = wncard_(xrsult);

/*                 Test family validity check: we're not supposed to */
/*                 have any cases where the results are empty. */

		    chcksi_("XN", &xn, ">", &c__0, &c__0, ok, (ftnlen)2, (
			    ftnlen)1);
		    n = wncard_(result);
		    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)
			    1);
		    if (*ok) {

/*                    Compare result window interval bounds. */

/*                    Here's where things get a bit tricky. We can't */
/*                    just compare RESULT against XRSULT, because */
/*                    these windows contain event times for different */
/*                    locations. They're directly comparable only */
/*                    when aberration corrections are turned off. */

			if (eqstr_(abcorr, "NONE", (ftnlen)5, (ftnlen)4)) {

/*                       No problem: this is the geometric case. */

			    i__1 = n << 1;
			    chckad_("RESULT", &result[6], "~", &xrsult[6], &
				    i__1, &c_b542, ok, (ftnlen)6, (ftnlen)1);
			} else {

/*                       Modify the window of event times by subtracting */
/*                       the applicable one-way light time from each */
/*                       element. */

			    ssized_(&c__200, modres);
			    i__1 = cardd_(result);
			    for (i__ = 1; i__ <= i__1; ++i__) {
				et = result[(i__2 = i__ + 5) < 206 && 0 <= 
					i__2 ? i__2 : s_rnge("result", i__2, 
					"f_gfilum__", (ftnlen)2325)];

/*                          Find the light time from the surface point */
/*                          to the observer, where the reception time */
/*                          is ET. */

				spkpos_("OCTL", &et, "J2000", abcorr, obsrvr, 
					pos, &lt, (ftnlen)4, (ftnlen)5, (
					ftnlen)5, (ftnlen)36);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				modres[(i__2 = i__ + 5) < 206 && 0 <= i__2 ? 
					i__2 : s_rnge("modres", i__2, "f_gfi"
					"lum__", (ftnlen)2335)] = result[(i__3 
					= i__ + 5) < 206 && 0 <= i__3 ? i__3 :
					 s_rnge("result", i__3, "f_gfilum__", 
					(ftnlen)2335)] - lt;
			    }
			    if (*ok) {

/*                          Compare the transformed result window to */
/*                          the expected window. */

				i__1 = n << 1;
				chckad_("RESULT (modified)", &modres[6], 
					"~", &xrsult[6], &i__1, &c_b542, ok, (
					ftnlen)17, (ftnlen)1);
			    }
			}
			if (*ok) {

/*                       The result window found by GFILUM agrees */
/*                       with that found by GFPOSC. Check the actual */
/*                       angular values at the window endpoints */
/*                       for the cases where the operator is */
/*                       binary. */

			    if (s_cmp(relate, "=", (ftnlen)6, (ftnlen)1) == 0)
				     {

/*                          Check the solar incidence angle at each */
/*                          interval endpoint. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksd_(title, &solar, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksd_(title, &solar, "~", &refval, &
					    c_b552, ok, (ftnlen)800, (ftnlen)
					    1);
				}
			    } else if (s_cmp(relate, "<", (ftnlen)6, (ftnlen)
				    1) == 0 || s_cmp(relate, ">", (ftnlen)6, (
				    ftnlen)1) == 0) {

/*                          Perform checks only at interval endpoints */
/*                          contained in the interior of the confinement */
/*                          window. At the confinement window boundaries, */
/*                          the inequality may be satisfied without the */
/*                          value being equal to the reference value. */

				i__1 = n;
				for (i__ = 1; i__ <= i__1; ++i__) {
				    wnfetd_(result, &i__, &start, &finish);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    s_copy(title, "Angle at start of interva"
					    "l #", (ftnlen)800, (ftnlen)28);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &start, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (start > et0) {
					chcksd_(title, &solar, "~", &refval, &
						c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				    s_copy(title, "Angle at end of interval #"
					    , (ftnlen)800, (ftnlen)26);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    800, (ftnlen)1, (ftnlen)800);
				    ilumin_(method, "EARTH", &finish, fixref, 
					    abcorr, obsrvr, spoint, &trgepc, 
					    srfvec, &phase, &solar, &emissn, (
					    ftnlen)80, (ftnlen)5, (ftnlen)32, 
					    (ftnlen)5, (ftnlen)36);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (finish < et1) {
					chcksd_(title, &solar, "~", &refval, &
						c_b552, ok, (ftnlen)800, (
						ftnlen)1);
				    }
				}

/*                          End of solar incidence angle value check */
/*                          loop. */

			    }

/*                       End of solar incidence angle checks. */

			}

/*                    End of IF block for successful RESULT check. */

		    }

/*                 End of interval endpoint checks. */

		}

/*              End of IF block for successful GFILUM call. */

	    }

/*           End of IF block for successful GFPOSC call. */

	}

/*        End of aberration correction loop. */

    }

/*     End of operator loop. */


/*     Case */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)800, (
	    ftnlen)39);
    tcase_(title, (ftnlen)800);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    s_copy(relate, "locmin", (ftnlen)6, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    s_copy(target, " ALPHA", (ftnlen)36, (ftnlen)6);
    s_copy(fixref, " ALPHAFIXED", (ftnlen)32, (ftnlen)11);
    s_copy(obsrvr, " SUN", (ftnlen)36, (ftnlen)4);
    s_copy(illum, " SUN", (ftnlen)36, (ftnlen)4);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    step = 3600.;
    left = 0.;
    right = spd_() * 10.;
    scardd_(&c__0, cnfine);
    scardd_(&c__0, result);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b825);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfilum_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, 
	    relate, &refval, &adjust, &step, cnfine, &c__200, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)
	    32, (ftnlen)5, (ftnlen)36, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b287, ok, (ftnlen)800, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b306);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK files. */

    spkuef_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfilum__ */

