/*

-Procedure f_gfilum_c ( Test gfilum_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfilum_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfilum_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the GF wrapper gfilum_c.
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version 

   -tspice_c Version 1.3.0, 08-FEB-2017 (EDW)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.

   -tspice_c Version 1.2.0, 02-APR-2014 (NJB)

       Argument `handle' in call to tstspk_ made to generate
       SPK2 was renamed to `han2'.

       Time tolerances were loosened for OCTL tests.

   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 15-JUL-2012 (NJB) 

-Index_Entries

   test gfilum_c

-&
*/

{ /* Begin f_gfilum_c */

 
   /*
   Constants
   */
   #define OCTL            "OCTL"
   #define OCTSPK          "octl_test.bsp"
   #define PCK             "nat.tpc"
   #define PCK2            "generic.tpc"
   #define SPK1            "site_spkcpv.bsp"
   #define SPK2            "generic.bsp"

   #define BDNMLN          37
   #define CRDLEN          26
   #define FRNMLN          33
   #define LNSIZE          81
   #define MAXIVL          100
   #define MAXWIN        ( 2 * MAXIVL )
   #define MSGLEN          801
   #define NCORR           5
   #define NREL            9
   #define OPLEN           7
   #define SYSLEN          26
   #define TIMLEN          51
   #define TYPLEN          26
   #define TXTLEN          81
   #define TXTSIZ          25
   

   #define ANGTOL          1.e-9
   #define TIMTOL          1.e-6
   #define LOOSE           1.e-5
 
   /*
   Local variables
   */
   SPICEDOUBLE_CELL      ( cnfine, MAXWIN );
   SPICEDOUBLE_CELL      ( result, MAXWIN );
   SPICEDOUBLE_CELL      ( xrsult, MAXWIN );
   SPICEDOUBLE_CELL      ( modcfn, MAXWIN );
   SPICEDOUBLE_CELL      ( modres, MAXWIN );

   SPICEINT_CELL         ( badcfn, 10 );
   SPICEINT_CELL         ( badres, 10 );

   static logical          fflag = 0;
   static logical          tflag = 1;

   SpiceBoolean            found;

   SpiceChar             * abcorr;
   SpiceChar             * angtyp;
   SpiceChar             * coord;
   SpiceChar             * corrs [] = {"NONE", "LT", "CN", "LT+S", "CN+S"};
   SpiceChar             * corsys;
   SpiceChar             * fixref;
   SpiceChar             * illum;
   SpiceChar             * method;
   SpiceChar             * obsrvr;
   SpiceChar             * relate;

   SpiceChar             * relops[] = { "=",      "<",      ">",  
                                        "LOCMIN", "LOCMAX", "ABSMIN", 
                                        "ABSMIN", "ABSMAX", "ABSMAX" };
   SpiceChar             * srfref;
   SpiceChar             * target;
   SpiceChar             * time0;
   SpiceChar               title  [ MSGLEN ];
   SpiceChar               txtbuf [ TXTSIZ ][ TXTLEN ];
   SpiceChar             * txtptr [ TXTSIZ ];


   SpiceDouble             adj[]    = { 0.0,   0.0,    0.0,  0.0, 0.0,
                                        0.0,   5.e-2,  0.0,  5.e-2    };

   SpiceDouble             adjust;
   SpiceDouble             alt;
   SpiceDouble             emissn;
   SpiceDouble             et;
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             finish;
   SpiceDouble             first;
   SpiceDouble             last;
   SpiceDouble             lat;
   SpiceDouble             left;
   SpiceDouble             lon;
   SpiceDouble             lt;
   SpiceDouble             np      [ 3 ];
   SpiceDouble             phase;
   SpiceDouble             pos     [ 3 ];
   SpiceDouble             radii   [ 3 ];
   SpiceDouble             refval;
   SpiceDouble             right;
   SpiceDouble             solar;
   SpiceDouble             spoint  [ 3 ];
   SpiceDouble             srfvec  [ 3 ];
   SpiceDouble             start;
   SpiceDouble             states  [ 2 ][ 6 ];
   SpiceDouble             step;
   SpiceDouble             trgepc;
   
   SpiceDouble             vals  [] = { 65.0, 70.0, 60.0,  0.0, 0.0, 0.0,
                                        0.0,   0.0,  0.0                };
   SpiceDouble             xtime;


   SpiceInt                coridx;
   SpiceInt                han1;
   SpiceInt                han2;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                n;
   SpiceInt                octid;
   SpiceInt                opidx;
   SpiceInt                xn;

   



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfilum_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );  

   /*
   Create and load the generic test kernels. 
   */
   natspk_ ( SPK1, &tflag, &han1, strlen(SPK1) );
   chckxc_c( SPICEFALSE, " ", ok );

   tstspk_ ( SPK2, &tflag, &han2, strlen(SPK2) );   
   chckxc_c( SPICEFALSE, " ", ok );

   natpck_ ( PCK, &tflag, &fflag, strlen(PCK) );
   chckxc_c( SPICEFALSE, " ", ok );

   tstpck_ ( PCK2, &tflag, &fflag, strlen(PCK2) );
   chckxc_c( SPICEFALSE, " ", ok );

   tstlsk_c();
   chckxc_c( SPICEFALSE, " ", ok );


   /*
   Set up a confinement window. Initialize this and 
   the result window.
   */

   time0 = "2000 JAN 1  00:00:00 TDB";
      
   str2et_c ( time0, &et0 );
   chckxc_c( SPICEFALSE, " ", ok );


   /*
   Create a confinement window with 4 intervals.
   */

   for ( i = 0;  i < 4;  i++ )
   {
      left  = et0  +  ( i * spd_c() )  + 3600.0;
      right = left +                     3600 * 22;

      wninsd_c ( left, right, &cnfine );
      chckxc_c( SPICEFALSE, " ", ok );

   }
 


   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Non-positive step size." );  

   method = "Ellipsoid";
   angtyp = "INCIDENCE";
   target = "EARTH";
   illum  = "SUN";
   fixref = "IAU_EARTH";
   abcorr = "CN+S";
   obsrvr = "MOON";
   relate = "LOCMAX";
   refval =  0.0;
   adjust =  0.0;
   step   =  1.e3;

   lon    =  100.0 * rpd_c();
   lat    =   30.0 * rpd_c();

   srfrec_c ( 399, lon, lat, spoint );
   chckxc_c ( SPICEFALSE, " ", ok );

   step   =  0.0;

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result        );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );


   step   = -1.0;

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   tcase_c ( "Workspace window size too small" );  

   This case is not applicable to gfilum_c.
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Result window too small (detected before search)." );  

   ssize_c ( 1, &result );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              1,  &cnfine, &result             );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDDIMENSION)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Result window too small (detected during search)." );  

   method = "Ellipsoid";
   angtyp = "INCIDENCE";
   target = "EARTH";
   illum  = "SUN";
   fixref = "IAU_EARTH";
   abcorr = "CN+S";
   obsrvr = "MOON";
   relate = "LOCMAX";
   refval =  0.0;
   adjust =  0.0;

   lon    =  100.0 * rpd_c();
   lat    =   30.0 * rpd_c();

   srfrec_c ( 399, lon, lat, spoint );
   chckxc_c ( SPICEFALSE, " ", ok );


   str2et_c ( "2000 JAN 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 JAN 5 TDB", &et1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   step   =  300.0;

   ssize_c ( 2, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(OUTOFROOM)", ok );

 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Unrecognized value of FIXREF." );  

   ssize_c ( MAXWIN, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              "XYZ",   abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(UNKNOWNFRAME)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "FIXREF is not centered on the target body." );  

   ssize_c ( MAXWIN, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfilum_c ( method,      angtyp,  target, illum,  
              "IAU_MARS",  abcorr,  obsrvr, spoint, 
              relate,      refval,  adjust, step,       
              MAXWIN,      &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDFRAME)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad computation method." );  


   angtyp = "EMISSION";
   method = "DSK";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDMETHOD)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad illumination angle type." );  

   step = 1.0e3;

   angtyp = "XYZ";
   method = "Ellipsoid";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad relational operator." );  

   angtyp = "INCIDENCE";
  
   relate = ">=";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad aberration correction values." );  

   relate = "LOCMAX";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "S",     obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XS",    obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "RLT",   obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XRLT",   obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "Z",     obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Transmission aberration correction values." );  

   relate = "LOCMAX";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XLT",   obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XCN",   obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XLT+S", obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  "XCN+S", obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Bad value of ADJUST." );  

   adjust = -1.0;
   relate = "absmax";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   relate = "=";

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Unrecognized target, observer, or illumination source." );

   
   adjust = 0.0;


   gfilum_c ( method,  angtyp,  "X",    illum,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   gfilum_c ( method,  angtyp,  target, "X",  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  "X",    spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Target and observer are identical." );


   ssize_c ( MAXWIN, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfilum_c ( method,  angtyp,  target, illum,  
              fixref,  abcorr,  target, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Target and illumination source are identical." );


   ssize_c ( MAXWIN, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfilum_c ( method,  angtyp,  target, target,  
              fixref,  abcorr,  obsrvr, spoint, 
              relate,  refval,  adjust, step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "No SPK data for observer." );


   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  "GASPRA", spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "No SPK data for target." );

   fixref = "IAU_GASPRA";

   gfilum_c ( method,  angtyp,  "GASPRA", illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "No SPK data for illumination source." );

   fixref = "IAU_EARTH";

   gfilum_c ( method,  angtyp,  target,   "GASPRA",  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "No PCK orientation data for target." );

   fixref = "ITRF93";

   gfilum_c ( method,  angtyp,  target,   "GASPRA",  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(FRAMEDATANOTFOUND)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "NULL input string pointers." );

   fixref = "IAU_EARTH";


   gfilum_c ( NULL,    angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  NULL,   target,    illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  angtyp,  NULL,     illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  angtyp,  target,   NULL,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              NULL,    abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  NULL,    obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  NULL,     spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
  
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Empty input strings." );


   gfilum_c ( "",      angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  "",     target,    illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  angtyp,  "",       illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  angtyp,  target,   "",    
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              "",      abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  "",      obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  "",       spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
    

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Wrong cell data types." );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &badcfn, &result         );
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );



   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &badres         );
   chckxc_c ( SPICETRUE, "SPICE(TYPEMISMATCH)", ok );
   


   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */

   /*
   *********************************************************************
 
   Simple cases using Nat's solar system

   *********************************************************************
   */

   /*
   The cases below all involve finding times when local extrema are
   attained. In each case we know the correct answers. All of these
   tests are done with aberration corrections set to "NONE".

   Following these tests, a series of comparisons is performed using
   results produced by alternate methods. The second set of tests is
   comprehensive: those tests use all combinations of operators and
   aberration corrections.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   /*
   All expected event times are based on Nat's solar system.
   */
   tcase_c ( "Local minimum of emission angle at north "
             "pole of ALPHA; observer is SUN; abcorr = NONE" );


   angtyp = "emission";
   relate = "locmin";
   refval = 0.0;
   adjust = 0.0;

   abcorr = "none";

   target = "alpha";
   fixref = "alphafixed";
   obsrvr = "sun";
   illum  = "sun";

   bodvrd_c ( "alpha", "RADII", 3, &n, radii );
   chckxc_c( SPICEFALSE, " ", ok );

   vpack_c ( 0.0, 0.0, radii[2], spoint );

   time0 = "2000 JAN 1 12:00:00 TDB";

   str2et_c ( time0, &et0 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   ssize_c ( MAXWIN, &cnfine );
   chckxc_c( SPICEFALSE, " ", ok );

   ssize_c ( MAXWIN, &result );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   The period of ALPHA's orbit is 7 days. This can be derived
   from the relative angular velocities of ALPHA and BETA
   and the period of the occultation starts.

   A minimum of the emission angle should occur whenever ALPHA's
   north pole points toward the sun. This should happen at the
   J2000 epoch and every 7 days before or after.

   Create a confinement window with 4 intervals.
   */
   for ( i = 0;  i < 4;  i++ )
   {
      left  = et0  +  (i * spd_c() * 7)   -      3600.0;
      right = left                        +  2 * 3600.0;

      wninsd_c ( left, right, &cnfine );
   }


   /*
   We can use a large step. The extrema should be 12 hours apart.
   */
   step   = 10 * 3600.0;

   method = "ellipsoid";

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksi_c ( "result cardinality", wncard_c(&result), "=", 4, 0, ok );

   
   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = et0 + (i * spd_c() * 7 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }
   


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Local minimum of solar incidence angle at north "
             "pole of ALPHA; observer is SUN; abcorr = NONE" );


   angtyp = "incidence";

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );


   chcksi_c ( "result cardinality", wncard_c(&result), "=", 4, 0, ok );

   
   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = et0 + (i * spd_c() * 7 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Local maximum of emission angle at north "
             "pole of ALPHA; observer is SUN; abcorr = NONE" );


   angtyp = "emission";
   relate = "locmax";

   /*
   ALPHA orbits with constant angular velocity on a circular path,
   so local maxima occur 1/2 period apart from local minima.

   Create a confinement window with 4 intervals.
   */
   scard_c ( 0, &cnfine );

   for ( i = 0;  i < 4;  i++ )
   {
      left  = et0  +  3.5*spd_c() + (i * spd_c() * 7)   -      3600.0;
      right = left                                      +  2 * 3600.0;

      wninsd_c ( left, right, &cnfine );
   }


   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "result cardinality", wncard_c(&result), "=", 4, 0, ok );


   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = et0 + (3.5 * spd_c() ) + (i * spd_c() * 7 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Local maximum of incidence angle at north "
             "pole of ALPHA; observer is SUN; abcorr = NONE" );


   angtyp = "incidence";
   relate = "locmax";

   /*
   ALPHA orbits with constant angular velocity on a circular path,
   so local maxima occur 1/2 period apart from local minima.

   Create a confinement window with 4 intervals.
   */
   scard_c ( 0, &cnfine );

   for ( i = 0;  i < 4;  i++ )
   {
      left  = et0  +  3.5*spd_c() + (i * spd_c() * 7)   -      3600.0;
      right = left                                      +  2 * 3600.0;

      wninsd_c ( left, right, &cnfine );
   }


   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "result cardinality", wncard_c(&result), "=", 4, 0, ok );


   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = et0 + (3.5 * spd_c() ) + (i * spd_c() * 7 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Local minimum of phase angle at -X axis of "
             "ALPHA; observer is BETA; abcorr = NONE" );

   /*
   This is a phase angle search.
   */
   angtyp = "phase";

   /*
   This time we want to use Beta as the observer. Note that
   the surface point on Alpha's north pole won't do for this
   computation.

   We're going to work around this problem by changing the
   orientation of Alpha. We'll use the ALPHA_VIEW_XY frame as
   Alpha's body-fixed frame. This is a two-vector dynamic frame
   in which the +X axis points from the sun to Alpha at all times.
   We'll create a sun-facing surface point on Alpha at the tip
   of Alpha's -X axis.
   */
   bodvrd_c ( "ALPHA", "RADII", 3, &n, radii );
   chckxc_c( SPICEFALSE, " ", ok );

   vpack_c ( -radii[2], 0.0, 0.0, spoint );

   fixref = "ALPHA_VIEW_XY";

   /*
   Set up the participants. 
   */

   obsrvr = "beta";
   target = "alpha";
   illum  = "sun";
   relate = "locmin";
   step   = 3600.0;

   /*
   Set the confinement window to cover a single 4-day time interval.
   */
   et0 = 0.0;
   et1 = et0 + ( 4 * spd_c() );

   scard_c ( 0, &cnfine );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c( SPICEFALSE, " ", ok );

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Note that the phase angle is zero when the beta-sun-alpha angle
   is zero or 180 degrees, so there are two minima of the phase angle
   per 24 hours.
   */
   chcksi_c ( "result cardinality", wncard_c(&result), "=", 8, 0, ok );


   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = 300  +  ( i * spd_c()/2 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Repeat the previous test with the illumination "
             "source set to BETA and the observer set to SUN." );  

   obsrvr = "sun";
   illum  = "beta";

   gfilum_c ( method,  angtyp,  target,   illum,  
              fixref,  abcorr,  obsrvr,   spoint, 
              relate,  refval,  adjust,   step,       
              MAXWIN,  &cnfine, &result         );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Note that the phase angle is zero when the beta-sun-alpha angle
   is zero or 180 degrees, so there are two minima of the phase angle
   per 24 hours.
   */
   chcksi_c ( "result cardinality", wncard_c(&result), "=", 8, 0, ok );


   for ( i = 0;  i < wncard_c(&result);  i++ )
   {
      wnfetd_c ( &result, i, &left, &right );

      /*
      Check event start time.
      */

      sprintf ( title, "Event %ld beg time", (long)i );
      
      xtime = 300  +  ( i * spd_c()/2 );

      chcksd_c ( title, left, "~", xtime, TIMTOL, ok );
      
      /*
      Check event end time; this should equal the begin time
      since we're searching for an extremum.
      */

      sprintf ( title, "Event %ld end time", (long)i );
      
      chcksd_c ( title, right, "~", xtime, TIMTOL, ok );
   }


   /*
   *********************************************************************
   *
   *    Comprehensive cases using comparisons against alternate
   *    computations
   *
   *********************************************************************
   */

 
   /*
   *********************************************************************
   *
   *    PHASE angle tests
   *
   *********************************************************************
   */ 
     
   /*
   We'll use the earth, moon, and sun as the three participating
   bodies. The surface point will be the OCTL telescope; we'll
   create an SPK file for this point. We also need an FK for a
   topocentric frame centered at the point.

   Though it's not strictly necessary, we'll use real data for
   these kernels, with one exception: we'll use the terrestrial
   reference frame IAU_EARTH rather than ITRF93.

   The original reference frame specifications follow:


      Topocentric frame OCTL_TOPO

         The Z axis of this frame points toward the zenith.
         The X axis of this frame points North.

         Topocentric frame OCTL_TOPO is centered at the site OCTL
         which has Cartesian coordinates

            X (km):                 -0.2448937761729E+04
            Y (km):                 -0.4667935793438E+04
            Z (km):                  0.3582748499430E+04

         and planetodetic coordinates

            Longitude (deg):      -117.6828380000000
            Latitude  (deg):        34.3817491000000
            Altitude   (km):         0.2259489999999E+01

         These planetodetic coordinates are expressed relative to
         a reference spheroid having the dimensions

            Equatorial radius (km):  6.3781400000000E+03
            Polar radius      (km):  6.3567523100000E+03

         All of the above coordinates are relative to the frame
         ITRF93.
   */





   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Create OCTL kernels." );  

   /*
   This isn't a test, but we'll call it that so we'll have
   a meaningful label in any error messages that arise.
   */

   /*
   As mentioned, we go with a frame that's more convenient than
   ITRF93:
   */
   fixref = "IAU_EARTH";

   /*
   Prepare a frame kernel in a string array. We'll start with
   an array of string pointers, then create a string array.
   */
   
   txtptr[ 0] = "FRAME_OCTL_TOPO            =  1398962";
   txtptr[ 1] = "FRAME_1398962_NAME         =  'OCTL_TOPO' ";
   txtptr[ 2] = "FRAME_1398962_CLASS        =  4";
   txtptr[ 3] = "FRAME_1398962_CLASS_ID     =  1398962";
   txtptr[ 4] = "FRAME_1398962_CENTER       =  398962";

   txtptr[ 5] = "OBJECT_398962_FRAME        =  'OCTL_TOPO' ";

   txtptr[ 6] = "TKFRAME_1398962_RELATIVE   =  'IAU_EARTH' ";
   txtptr[ 7] = "TKFRAME_1398962_SPEC       =  'ANGLES' ";
   txtptr[ 8] = "TKFRAME_1398962_UNITS      =  'DEGREES' ";
   txtptr[ 9] = "TKFRAME_1398962_AXES       =  ( 3, 2, 3 )";
   txtptr[10] = "TKFRAME_1398962_ANGLES     =  ( -242.3171620000000,";
   txtptr[11] = "                                 -55.6182509000000,";
   txtptr[12] = "                                 180.0000000000000  )";
   txtptr[13] = "NAIF_BODY_NAME            +=  'OCTL' ";
   txtptr[14] = "NAIF_BODY_CODE            +=  398962";

   /*
   It will be convenient to have a version of this frame that
   has the +Z axis pointed down instead of up.
   */
   txtptr[15] = "FRAME_OCTL_FLIP            =  2398962";
   txtptr[16] = "FRAME_2398962_NAME         =  'OCTL_FLIP' ";
   txtptr[17] = "FRAME_2398962_CLASS        =  4";
   txtptr[18] = "FRAME_2398962_CLASS_ID     =  2398962";
   txtptr[19] = "FRAME_2398962_CENTER       =  398962";

   txtptr[20] = "TKFRAME_2398962_RELATIVE   =  'OCTL_TOPO' ";
   txtptr[21] = "TKFRAME_2398962_SPEC       =  'ANGLES' ";
   txtptr[22] = "TKFRAME_2398962_UNITS      =  'DEGREES' ";
   txtptr[23] = "TKFRAME_2398962_AXES       =  ( 3, 2, 3 )";
   txtptr[24] = "TKFRAME_2398962_ANGLES     =  ( 0, 180.0, 0 ) ";
   
   /*
   Populate a string array for use by lmpool_c. 
   */
   for ( i = 0;  i < TXTSIZ;  i++ )
   {
      strncpy ( txtbuf[i], txtptr[i], TXTLEN );
   }

   lmpool_c ( txtbuf, TXTLEN, TXTSIZ );
   chckxc_c( SPICEFALSE, " ", ok );



   /*
   Now create an SPK file containing a type 8 segment for OCTL.
   */
   spkopn_c ( OCTSPK, OCTSPK, 0, &handle );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Initialize both states to zero.
   */
   i = 12;
   cleard_ ( (integer *)&i, (doublereal *)states );

   /*
   The first position component:
   */
   spoint[0] =   -0.2448937761729e4;
   spoint[1] =   -0.4667935793438e4;
   spoint[2] =    0.3582748499430e4;

   /*
   We're going to use a version of the OCTL position
   that has zero altitude relative to the earth's
   reference ellipsoid. This is done to enable
   consistency checks to be done using GFPOSC.

   For compatibility with the topocentric frame
   specification above, we'll use the following
   earth radii:
   */
   radii[0]  = 6.3781400000000e+03;
   radii[1]  = 6.3781400000000e+03;
   radii[2]  = 6.3567523100000e+03;

   pdpool_c ( "BODY399_RADII", 3, radii );

   nearpt_c ( spoint, radii[0], radii[1], radii[2], np, &alt );

   vequ_c ( np, spoint );

   vequ_c ( spoint, states[0] );

   /*
   The second position matches the first: we don't model
   plate motion.
   */
   vequ_c ( spoint, states[1] );

   /*
   Time bounds for the segment:
   */
   first = ( -50 ) * spd_c() * 365.25;
   step  = ( 100 ) * spd_c() * 365.25;

   last  = first + step - 1.e-6;

   /*
   Get the OCTL ID code from the kernel we just
   loaded.
   */
   bodn2c_c ( OCTL, &octid, &found );

   chckxc_c( SPICEFALSE, " ", ok );
   chcksl_c( "found", found, SPICETRUE, ok );

   /*
   Write the segment.
   */   
   spkw08_c ( handle, octid,  399,     fixref, 
              first,  last,   "octl",  1,      
              2,      states, first,   step   );
   chckxc_c( SPICEFALSE, " ", ok );

   spkcls_c ( handle );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Now load the OCTL SPK file.
   */
   furnsh_c ( OCTSPK );
   chckxc_c( SPICEFALSE, " ", ok );





   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Phase angle test setup." );  


   /*
   Phase angle tests: we'll compare results from gfilum_c to those
   obtained using gfpa_c. Note that the surface point must be
   an ephemeris object in order to carry out these tests.
   */
   angtyp = " Phase";

   illum  = "Sun";
   target = " 399";
   obsrvr = " moon";
   fixref = "IAU_EARTH";
      

   /*
   Set up the confinement window.
   */
   time0 = "2011 JAN 1";

   str2et_c ( time0, &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Search over approximately two months.
   */
   et1 = et0 + ( 60.0 * spd_c() );

   scard_c ( 0, &cnfine );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Use a 10 day step. We expect the extrema to
   be about 14 days apart.
   */
   step = 10 * spd_c();

   /*
   Loop over all operators and aberration corrections.
   */

   for ( opidx = 0;  opidx < NREL;  opidx++ )
   {
      /*
      Convert the reference value to radians.
      */
      relate = relops [ opidx ];
      refval = vals   [ opidx ] * rpd_c();
      adjust = adj    [ opidx ];


      for ( coridx = 0;  coridx < NCORR;  coridx++ )
      {
         abcorr = corrs[ coridx ];


         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title,
                   "Phase angle search: RELATE = #; "
                   "REFVAL (deg) = #; AJDUST = #; ABCORR = #; "
                   "observer = #; target = #; illum source = #; "
                   "FIXREF = #; SPOINT = ( # # # ).",
                   MSGLEN                                        );

         repmc_c ( title, "#", relate,          MSGLEN, title );
         repmd_c ( title, "#", vals[opidx], 14, MSGLEN, title );
         repmd_c ( title, "#", adjust,      14, MSGLEN, title );
         repmc_c ( title, "#", abcorr,          MSGLEN, title );
         repmc_c ( title, "#", obsrvr,          MSGLEN, title );
         repmc_c ( title, "#", target,          MSGLEN, title );
         repmc_c ( title, "#", illum,           MSGLEN, title );
         repmc_c ( title, "#", fixref,          MSGLEN, title );
         repmd_c ( title, "#", spoint[0],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[1],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[2],   14, MSGLEN, title );
         chckxc_c( SPICEFALSE, " ", ok );

         tcase_c ( title );  

         /*
         printf ( "%s \n\n", title  );
         */

         /*
         Find the expected result window. Note that the
         target is OCTL in this case.
         */         
         gfpa_c ( OCTL,   illum,   abcorr, obsrvr,
                  relate, refval,  adjust, step,
                  MAXWIN, &cnfine, &xrsult          );
         
         chckxc_c( SPICEFALSE, " ", ok );

         if ( ok )
         {
            /*
            Search for the condition of interest using gfilum_c.
            */

            gfilum_c ( method,  angtyp,  target,   illum,  
                       fixref,  abcorr,  obsrvr,   spoint, 
                       relate,  refval,  adjust,   step,       
                       MAXWIN,  &cnfine, &result         );
            chckxc_c ( SPICEFALSE, " ", ok );
 
            if ( ok )
            {
               /*
               Compare result window cardinalities first.
               */
               xn = wncard_c( &xrsult );

               /*
               Test family validity check: we're not supposed to
               have any cases where the results are empty.
               */
               chcksi_c ( "xn", xn, ">", 0, 0, ok );

               n = wncard_c( &result );

               chcksi_c ( "n", n, "=", xn, 0, ok );

               if ( ok ) 
               {
                  /*
                  Compare result window interval bounds. 
                  */
                  chckad_c ( "result", 
                             (SpiceDouble *)(result.data), 
                             "~", 
                             (SpiceDouble *)(xrsult.data), 
                             2*n,
                             LOOSE,
                             ok                           );
                     
                  if ( ok )
                  {
                     /*
                     The result window found by gfilum_c agrees
                     with that found by gfpa_c. Check the actual
                     angular values at the window endpoints
                     for the cases where the operator is
                     binary.
                     */

                     if ( eqstr_c( relate, "=" ) )
                     {
                        /*
                        Check the phase angle at each interval
                        endpoint.
                        */

                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           strncpy ( title, 
                                     "Angle at start of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", start, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, phase, "~", refval, ANGTOL, ok );

                           strncpy ( title, 
                                     "Angle at end of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", finish, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, phase, "~", refval, ANGTOL, ok );
                        }
                     }

                     else if (    eqstr_c( relate, "<" )
                               || eqstr_c( relate, ">" )  )
                     {
                        /*
                        Perform checks only at interval endpoints
                        contained in the interior of the confinement
                        window. At the confinement window boundaries,
                        the inequality may be satisfied without the
                        value being equal to the reference value.
                        */
                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           if ( start > et0 )
                           {
                              strncpy ( title, 
                                        "Angle at start of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", start, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, phase, "~", 
                                                refval, ANGTOL, ok );
                           }

                           if ( finish < et1 )
                           {
                              strncpy ( title, 
                                        "Angle at end of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", finish, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, phase, "~", 
                                                refval, ANGTOL, ok );
                           }
                        }
                        /*
                        End of phase angle value check
                        loop.
                        */
                     }
                     /*
                     End of phase angle checks.
                     */
                  }
                  /*
                  End of IF block for successful RESULT check.
                  */
               }
               /*
               End of interval endpoint checks.
               */
            }
            /*
            End of IF block for successful gfilum_c call.
            */
         }
         /*
         End of IF block for successful gfpa_c call.
         */
      }
      /*
      End of aberration correction loop.
      */
   }

   /*
   End of operator loop.
   */



   /*
   *********************************************************************
   *
   *    EMISSION angle tests
   *
   *********************************************************************
   */ 

   /*
   Emission angle tests: we'll compare results from gfilum_c to those
   obtained using gfposc_c. Note that the surface point must be
   an ephemeris object having an associated topocentric frame
   in order to carry out these tests.
   
   The emission angle is the supplement of the colatitude,
   measured in the surface point topocentric frame, of
   the observer-surface point vector. Equivalently, the emission
   angle is the colatitude of the observer-surface point vector
   in the "flip" frame, which has its +Z axis pointing downward.

   We can use gfposc_c to find times when this colatitude, measured
   in the flip frame, meets conditions on the emission angle.
   */



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Emission angle test setup." );  

   angtyp = " emission";

   illum  = "Sun";
   target = " 399";
   obsrvr = " moon";
   fixref = " iau_earth";
   srfref = "OCTL_FLIP";
   corsys = "spherical";
   coord  = " colatitude";

   /*
   Set up the confinement window.
   */
   time0 = "2011 JAN 1";

   str2et_c ( time0, &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   For the emission angle test, we don't need to use
   a month-long confinement window. A few days is enough.
   */
   et1 = et0 + ( 3 * spd_c() );

   scard_c ( 0, &cnfine );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Use a 6 hour step. We expect the extrema to
   be 12+delta hours apart, where delta may be
   a few hours.
   */
   step   = 6.0 * 3600.0;

   /*
   Loop over all operators and aberration corrections.
   */

   for ( opidx = 0;  opidx < NREL;  opidx++ )
   {
      /*
      Convert the reference value to radians.
      */
      relate = relops [ opidx ];
      refval = vals   [ opidx ] * rpd_c();
      adjust = adj    [ opidx ];


      for ( coridx = 0;  coridx < NCORR;  coridx++ )
      {
         abcorr = corrs[ coridx ];


         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title,
                   "Emission angle search: RELATE = #; "
                   "REFVAL (deg) = #; AJDUST = #; ABCORR = #; "
                   "observer = #; target = #; illum source = #; "
                   "FIXREF = #; SPOINT = ( # # # ).",
                   MSGLEN                                        );

         repmc_c ( title, "#", relate,          MSGLEN, title );
         repmd_c ( title, "#", vals[opidx], 14, MSGLEN, title );
         repmd_c ( title, "#", adjust,      14, MSGLEN, title );
         repmc_c ( title, "#", abcorr,          MSGLEN, title );
         repmc_c ( title, "#", obsrvr,          MSGLEN, title );
         repmc_c ( title, "#", target,          MSGLEN, title );
         repmc_c ( title, "#", illum,           MSGLEN, title );
         repmc_c ( title, "#", fixref,          MSGLEN, title );
         repmd_c ( title, "#", spoint[0],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[1],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[2],   14, MSGLEN, title );
         chckxc_c( SPICEFALSE, " ", ok );

         tcase_c ( title );  

         /*
         printf ( "%s \n\n", title  );
         */

         /*
         Find the expected result window. Note that the
         target is OCTL in this case.
         */         
         gfposc_c ( OCTL,   srfref, abcorr, obsrvr,
                    corsys, coord,  relate, refval,
                    adjust, step,   MAXWIN, &cnfine, &xrsult );
         
         chckxc_c( SPICEFALSE, " ", ok );

         if ( ok )
         {
            /*
            Search for the condition of interest using gfilum_c.
            */

            gfilum_c ( method,  angtyp,  target,   illum,  
                       fixref,  abcorr,  obsrvr,   spoint, 
                       relate,  refval,  adjust,   step,       
                       MAXWIN,  &cnfine, &result         );
            chckxc_c ( SPICEFALSE, " ", ok );
 
            if ( ok )
            {
               /*
               Compare result window cardinalities first.
               */
               xn = wncard_c( &xrsult );

               /*
               Test family validity check: we're not supposed to
               have any cases where the results are empty.
               */
               chcksi_c ( "xn", xn, ">", 0, 0, ok );

               n = wncard_c( &result );

               chcksi_c ( "n", n, "=", xn, 0, ok );

               if ( ok ) 
               {
                  /*
                  Compare result window interval bounds. 
                  */
                  chckad_c ( "result", 
                             (SpiceDouble *)(result.data), 
                             "~", 
                             (SpiceDouble *)(xrsult.data), 
                             2*n,
                             LOOSE,
                             ok                           );
                     
                  if ( ok )
                  {
                     /*
                     The result window found by gfilum_c agrees
                     with that found by gfposc_c. Check the actual
                     angular values at the window endpoints
                     for the cases where the operator is
                     binary.
                     */

                     if ( eqstr_c( relate, "=" ) )
                     {
                        /*
                        Check the emission angle at each interval
                        endpoint.
                        */

                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           strncpy ( title, 
                                     "Angle at start of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", start, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, emissn, "~", refval, ANGTOL, ok );

                           strncpy ( title, 
                                     "Angle at end of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", finish, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, emissn, "~", refval, ANGTOL, ok );
                        }
                     }

                     else if (    eqstr_c( relate, "<" )
                               || eqstr_c( relate, ">" )  )
                     {
                        /*
                        Perform checks only at interval endpoints
                        contained in the interior of the confinement
                        window. At the confinement window boundaries,
                        the inequality may be satisfied without the
                        value being equal to the reference value.
                        */
                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           if ( start > et0 )
                           {
                              strncpy ( title, 
                                        "Angle at start of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", start, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, emissn, "~", 
                                                refval, ANGTOL, ok );
                           }

                           if ( finish < et1 )
                           {
                              strncpy ( title, 
                                        "Angle at end of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", finish, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, emissn, "~", 
                                                refval, ANGTOL, ok );
                           }
                        }
                        /*
                        End of emission angle value check
                        loop.
                        */
                     }
                     /*
                     End of emission angle checks.
                     */
                  }
                  /*
                  End of IF block for successful RESULT check.
                  */
               }
               /*
               End of interval endpoint checks.
               */
            }
            /*
            End of IF block for successful gfilum_c call.
            */
         }
         /*
         End of IF block for successful gfposc_c call.
         */
      }
      /*
      End of aberration correction loop.
      */
   }

   /*
   End of operator loop.
   */


   /*
   *********************************************************************
   *
   *    INCIDENCE angle tests
   *
   *********************************************************************
   */ 

   /*
   Incidence angle tests: we'll compare results from gfilum_c to
   those obtained using gfposc_c, where in the latter case, the
   surface point is treated as the observer. Results obtained using
   gfilum_c must have one-way observer-surface point light time
   subtracted in order to be comparable to those from gfposc_c.

   Note that the surface point must be an ephemeris object having an
   associate topocentric frame in order to carry out these tests.

   In the geometric case, the solar incidence angle is the
   colatitude, measured in the surface point topocentric frame, of
   the surface point-sun vector. When aberration corrections are
   used, the surface point-sun vector must be computed at an epoch
   corrected for observer-surface point light time.

   We can use gfposc_c to find times when this colatitude, measured in
   the OCTL topocentric frame, meets conditions on the solar
   incidence angle.
   */
   obsrvr = "MOON";
   target = "EARTH";
   illum  = "SUN";

   srfref = "OCTL_TOPO";
   corsys = "SPHERICAL";
   coord  = "COLATITUDE";

   angtyp = "INCIDENCE";
 
   /*
   For the solar incidence angle test, we don't need to use
   a month-long confinement window. A few days is enough.
   */
   /*
   Set up the confinement window.
   */
   time0 = "2011 JAN 1";

   str2et_c ( time0, &et0 );
   chckxc_c( SPICEFALSE, " ", ok );

   et1 = et0 + ( 3 * spd_c() );
 
   /*
   Use a 6 hour step. We expect the extrema to
   be 12+delta hours apart, where delta may be
   a few hours.
   */
   step   = 6.0 * 3600;
 
  
   scard_c ( 0, &cnfine );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c( SPICEFALSE, " ", ok );


   /*
   Loop over all operators and aberration corrections.
   */

   for ( opidx = 0;  opidx < NREL;  opidx++ )
   {
      /*
      Convert the reference value to radians.
      */
      relate = relops [ opidx ];
      refval = vals   [ opidx ] * rpd_c();
      adjust = adj    [ opidx ];


      for ( coridx = 0;  coridx < NCORR;  coridx++ )
      {
         abcorr = corrs[ coridx ];


         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title,
                   "Solar incidence angle search: RELATE = #; "
                   "REFVAL (deg) = #; AJDUST = #; ABCORR = #; "
                   "observer = #; target = #; illum source = #; "
                   "FIXREF = #; SPOINT = ( # # # ).",
                   MSGLEN                                        );

         repmc_c ( title, "#", relate,          MSGLEN, title );
         repmd_c ( title, "#", vals[opidx], 14, MSGLEN, title );
         repmd_c ( title, "#", adjust,      14, MSGLEN, title );
         repmc_c ( title, "#", abcorr,          MSGLEN, title );
         repmc_c ( title, "#", obsrvr,          MSGLEN, title );
         repmc_c ( title, "#", target,          MSGLEN, title );
         repmc_c ( title, "#", illum,           MSGLEN, title );
         repmc_c ( title, "#", fixref,          MSGLEN, title );
         repmd_c ( title, "#", spoint[0],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[1],   14, MSGLEN, title );
         repmd_c ( title, "#", spoint[2],   14, MSGLEN, title );
         chckxc_c( SPICEFALSE, " ", ok );

         tcase_c ( title );  

         /*
         printf ( "%s \n\n", title  );
         */


         if (  eqstr_c( abcorr, "NONE" )  )
         {
            copy_c ( &cnfine, &modcfn );
         }
         else
         {
            /*
            Create a modified confinement window: this is
            the original confinement window, adjusted for
            one-way light time between the observer and the
            surface point.
            */

            for ( i = 0;  i < card_c(&cnfine);  i++ )
            {
               et = ((SpiceDouble *)(cnfine.data))[i];

               spkpos_c( OCTL, et, "j2000", abcorr, obsrvr, pos, &lt );
               chckxc_c( SPICEFALSE, " ", ok );

               ((SpiceDouble *)(modcfn.data))[i] = et - lt;
            }
            scard_c ( card_c(&cnfine), &modcfn );
         }

         /*
         printf ( "et = %f lt = %f card(cnfine)
          = %ld \n", et,lt, (long)card_c(&cnfine)  );

         exit(0);
         */

         /*
         Find the expected result window. Note that the
         target is the sun and the observer is OCTL in this case.
         */         
         gfposc_c ( illum,  srfref, abcorr, OCTL,
                    corsys, coord,  relate, refval,
                    adjust, step,   MAXWIN, &modcfn, &xrsult );
         
         chckxc_c( SPICEFALSE, " ", ok );

         if ( ok )
         {
            /*
            Search for the condition of interest using gfilum_c.
            */

            gfilum_c ( method,  angtyp,  target,   illum,  
                       fixref,  abcorr,  obsrvr,   spoint, 
                       relate,  refval,  adjust,   step,       
                       MAXWIN,  &cnfine, &result         );
            chckxc_c ( SPICEFALSE, " ", ok );
 
            if ( ok )
            {
               /*
               Compare result window cardinalities first.
               */
               xn = wncard_c( &xrsult );

               /*
               Test family validity check: we're not supposed to
               have any cases where the results are empty.
               */
               chcksi_c ( "xn", xn, ">", 0, 0, ok );

               n = wncard_c( &result );

               chcksi_c ( "n", n, "=", xn, 0, ok );

               if ( ok ) 
               {
                  /*
                  Compare result window interval bounds. 

                  Here's where things get a bit tricky. We can't
                  just compare RESULT against XRSULT, because
                  these windows contain event times for different
                  locations. They're directly comparable only
                  when aberration corrections are turned off.
                  */

                  if (  eqstr_c( abcorr, "NONE" )  )
                  {
                     /*
                     No problem: this is the geometric case.
                     */
                     chckad_c ( "result", 
                                (SpiceDouble *)(result.data), 
                                "~", 
                                (SpiceDouble *)(xrsult.data), 
                                2*n,
                                LOOSE,
                                ok                           );
                  }
                  else
                  {
                     /*
                     Modify the window of event times by subtracting
                     the applicable one-way light time from each
                     element.
                     */
                     scard_c ( 0, &modres );

                     for ( i = 0;  i < card_c(&result);  i++ )
                     {
                        et = ((SpiceDouble *)(result.data))[i];

                        /*
                        Find the light time from the surface point
                        to the observer, where the reception time
                        is ET.
                        */
                        spkpos_c ( OCTL,   et,  "J2000", abcorr,
                                   obsrvr, pos, &lt              );
 
                        ((SpiceDouble *)(modres.data))[i] = et - lt;

                     }
                     
                     if ( ok )
                     {
                        /*
                        Compare the transformed result window to
                        the expected window.
                        */
                        chckad_c ( "result (modified)", 
                                   (SpiceDouble *)(modres.data), 
                                   "~", 
                                   (SpiceDouble *)(xrsult.data), 
                                   2*n,
                                   LOOSE,
                                   ok );
                     }
                  }
                     
                  if ( ok )
                  {
                     /*
                     The result window found by gfilum_c agrees
                     with that found by gfposc_c. Check the actual
                     angular values at the window endpoints
                     for the cases where the operator is
                     binary.
                     */

                     if ( eqstr_c( relate, "=" ) )
                     {
                        /*
                        Check the solar incidence angle at each interval
                        endpoint.
                        */

                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           strncpy ( title, 
                                     "Angle at start of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", start, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, solar, "~", refval, ANGTOL, ok );

                           strncpy ( title, 
                                     "Angle at end of interval #",
                                     MSGLEN                          );
                           repmi_c ( title, "#", i, MSGLEN, title );
                                       
                           ilumin_c ( method, "earth", finish, 
                                      fixref, abcorr,  obsrvr,
                                      spoint, &trgepc, srfvec,
                                      &phase, &solar,  &emissn );

                           chckxc_c ( SPICEFALSE, " ", ok );

                           chcksd_c ( title, solar, "~", refval, ANGTOL, ok );
                        }
                     }

                     else if (    eqstr_c( relate, "<" )
                               || eqstr_c( relate, ">" )  )
                     {
                        /*
                        Perform checks only at interval endpoints
                        contained in the interior of the confinement
                        window. At the confinement window boundaries,
                        the inequality may be satisfied without the
                        value being equal to the reference value.
                        */
                        for ( i = 0;  i < n;  i++ )
                        {
                           wnfetd_c ( &result, i, &start, &finish );
                           chckxc_c ( SPICEFALSE, " ", ok );

                           if ( start > et0 )
                           {
                              strncpy ( title, 
                                        "Angle at start of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", start, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, solar, "~", 
                                                refval, ANGTOL, ok );
                           }

                           if ( finish < et1 )
                           {
                              strncpy ( title, 
                                        "Angle at end of interval #",
                                        MSGLEN                          );
                              repmi_c ( title, "#", i, MSGLEN, title );

                              ilumin_c ( method, "earth", finish, 
                                         fixref, abcorr,  obsrvr,
                                         spoint, &trgepc, srfvec,
                                         &phase, &solar,  &emissn );

                              chckxc_c ( SPICEFALSE, " ", ok );

                              chcksd_c ( title, solar, "~", 
                                                refval, ANGTOL, ok );
                           }
                        }
                        /*
                        End of solar incidence angle value check
                        loop.
                        */
                     }
                     /*
                     End of solar incidence angle checks.
                     */
                  }
                  /*
                  End of IF block for successful RESULT check.
                  */
               }
               /*
               End of interval endpoint checks.
               */
            }
            /*
            End of IF block for successful gfilum_c call.
            */
         }
         /*
         End of IF block for successful gfposc_c call.
         */
      }
      /*
      End of aberration correction loop.
      */
   }
   /*
   End of operator loop.
   */




   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Clean up kernels." );  

   
   /*
   Unload and delete our test SPK files. 
   */
   spkuef_c ( han1 );
   chckxc_c( SPICEFALSE, " ", ok );
   removeFile ( SPK1 );

   spkuef_c ( han2 );
   chckxc_c( SPICEFALSE, " ", ok );
   removeFile ( SPK2 );

   unload_c ( OCTSPK );
   chckxc_c( SPICEFALSE, " ", ok );
   removeFile ( OCTSPK );

      
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfilum_c */

