/* f_gflorp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__200 = 200;
static integer c__3 = 3;
static integer c__20 = 20;
static integer c__6 = 6;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__20000 = 20000;
static doublereal c_b109 = 0.;
static doublereal c_b146 = 1e-6;
static integer c__7 = 7;
static integer c__9 = 9;
static integer c__8 = 8;
static integer c__2 = 2;

/* $Procedure F_GFLORP ( Test GF longitude progress reporting ) */
/* Subroutine */ int f_gflorp__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static integer nivl;
    extern /* Subroutine */ int t_gfrepf__();
    static doublereal step;
    extern /* Subroutine */ int t_gfrepi__();
    extern /* Subroutine */ int t_gfrini__(integer *, integer *, integer *, 
	    integer *, doublereal *, char *, ftnlen);
    static doublereal work[3090]	/* was [206][15] */;
    extern /* Subroutine */ int t_gfuini__(void);
    extern /* Subroutine */ int t_gfrepu__();
    extern /* Subroutine */ int t_gfrplo__(integer *, integer *, integer *, 
	    doublereal *), t_gfrtrm__(integer *, integer *, integer *);
    static integer i__, j;
    extern integer cardd_(doublereal *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer npass;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckai_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), str2et_(char *, doublereal *, ftnlen);
    extern logical gfbail_();
    extern doublereal pi_(void);
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksc_(
	    char *, char *, char *, char *, logical *, ftnlen, ftnlen, ftnlen,
	     ftnlen);
    static char op[80];
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal cnfine[206];
    extern /* Subroutine */ int gfrefn_();
    static doublereal cnflog[4120]	/* was [206][20] */, cnfmes;
    extern /* Subroutine */ int gfstep_();
    static char gquant[80], msglog[78*2*20], qcpars[80*20], qpnams[80*20], 
	    xprefx[78];
    static doublereal adjust, centrl, et0, et1, measur, qdpars[20], refval, 
	    replog[60000]	/* was [3][20000] */, result[206];
    static integer isqlog[20], mw, ncalls, nupdat, nw, qipars[20], qnpars, 
	    seqlog[20000], totcal, trmlog[20000], xsqlog[20000];
    static logical qlpars[20];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen), ssized_(integer *,
	     doublereal *), wninsd_(doublereal *, doublereal *, doublereal *),
	     gfsstp_(doublereal *), gfevnt_(U_fp, U_fp, char *, integer *, 
	    char *, char *, doublereal *, integer *, logical *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *,
	     U_fp, U_fp, U_fp, integer *, integer *, doublereal *, logical *, 
	    L_fp, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), chcksi_(char 
	    *, integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), scardd_(integer *, doublereal *), spkuef_(integer *), 
	    delfil_(char *, ftnlen);
    extern doublereal rpd_(void), spd_(void);
    static doublereal tol;
    static logical rpt;

/* $ Abstract */

/*     Test the GF subsystem's use of passed-in progress reporting and */
/*     interrupt handling functions for the "longitude coordinate" */
/*     quantity. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF subsystem's use of the default progress */
/*     reporting functions in the context of searches involving */
/*     longitude or right ascension constraints. The point is to */
/*     test the callers of the functions, not the functions themselves. */

/*     The routines exercised by this test family are */

/*        GFEVNT */
/*        ZZGFLONG */
/*        ZZGFCSLV */
/*        ZZGFREL */
/*        ZZGFSOLV */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 08-MAR-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Maximum progress report prefix or suffix length. */
/*     MXMSG is declared in zzgf.inc. */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFLORP", (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("gflorp.tpc", &c_true, &c_false, (ftnlen)10);

/*     Load an SPK file as well. */

    tstspk_("gflorp.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude LOCMIN search using GFEVNT.", (ftnlen)43);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gflorp__", (ftnlen)304)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     when a local minimum occurs. */


/*     Note: the epoch below was derived using SPK and */
/*     PCK files created by the test utilities called above. */

    str2et_("2000 JUN 05 04:09:54.717 TDB", &centrl, (ftnlen)28);
    nivl = 3;
    cnfmes = 0.;
    i__1 = nivl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et0 = -spd_() + (i__ - 2 << 2) * spd_() + centrl;
	et1 = et0 + spd_() * 2;
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	cnfmes = cnfmes + et1 - et0;
    }
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 80, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "LOCMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 3e3;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity local extrema searches (excluding longitude, RA, */
/*     etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 6 progress reporting passes. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__6, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 6", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)422)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)424)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude local extrema searches require 6 passes, */
/*     so we expect 6 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__6, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)461)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)461)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)464)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)464)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)472)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)497)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)497)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)498)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)498)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)502)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)502)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)510)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)510)], &c_b109, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)514)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)514)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)530)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)532)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)532)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude LOCMAX search using GFEVNT.", (ftnlen)43);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator and the confinement window. */

    s_copy(op, "LOCMAX", (ftnlen)80, (ftnlen)6);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     when a local maximum occurs. */

    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the epoch below was derived using SPK and */
/*     PCK files created by the test utilities called above. */

    str2et_("2000 JUN 16 04:37:06.960 TDB", &centrl, (ftnlen)28);
    nivl = 3;
    cnfmes = 0.;
    i__1 = nivl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et0 = -spd_() + (i__ - 2 << 2) * spd_() + centrl;
	et1 = et0 + spd_() * 2;
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	cnfmes = cnfmes + et1 - et0;
    }

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity local extrema searches (excluding longitude, RA, */
/*     etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 6 progress reporting passes. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__6, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 6", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)645)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)647)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude local extrema searches require 6 passes, */
/*     so we expect 6 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__6, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)684)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)684)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)687)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)687)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)695)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)720)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)720)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)721)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)721)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)725)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)725)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)733)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)733)], &c_b109, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)737)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)737)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)753)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)755)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)755)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMAX search using GFEVNT. Max is in 2nd quadra"
	    "nt.", (ftnlen)67);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("1999 SEP 25", &et0, (ftnlen)11);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 150;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 150;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)903)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)905)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)942)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)942)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)945)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)945)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)953)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)978)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)978)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)979)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)979)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)983)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)983)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)991)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)991)], &c_b109, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)995)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)995)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)1011)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)1013)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)1013)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMAX search using GFEVNT. Max is in right half"
	    " circle.", (ftnlen)72);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("1999 SEP 25", &et0, (ftnlen)11);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 90;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 90;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)1160)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)1162)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)1199)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)1199)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)1202)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)1202)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)1210)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1235)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1235)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1236)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1236)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1240)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1240)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)1248)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)1248)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)1252)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)1252)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)1268)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)1270)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)1270)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMAX search using GFEVNT. Max is in 4th quadra"
	    "nt.", (ftnlen)67);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("1999 SEP 25", &et0, (ftnlen)11);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)1416)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)1418)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)1455)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)1455)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)1458)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)1458)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)1466)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1491)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1491)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1492)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1492)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1496)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1496)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)1504)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)1504)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)1508)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)1508)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)1524)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)1526)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)1526)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMIN search using GFEVNT. Min is in 2nd quadra"
	    "nt.", (ftnlen)67);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("2000 JUL 15", &et0, (ftnlen)11);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)1673)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)1675)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)1712)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)1712)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)1715)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)1715)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)1723)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1748)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1748)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1749)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1749)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)1753)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)1753)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)1761)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)1761)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)1765)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)1765)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)1781)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)1783)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)1783)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMIN search using GFEVNT. Min is in right half."
	    , (ftnlen)65);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("2000 FEB 15", &et0, (ftnlen)11);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)1931)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)1933)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)1970)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)1970)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)1973)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)1973)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)1981)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2006)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2006)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2007)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2007)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2011)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2011)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)2019)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)2019)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)2023)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)2023)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)2039)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)2041)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)2041)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude ABSMIN search using GFEVNT. Min is in 3rd quadra"
	    "nt.", (ftnlen)67);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */

    str2et_("2000 OCT 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA unadjusted absolute extrema searches require */
/*     7 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)2191)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)2193)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude absolute extrema searches require 7 passes, */
/*     so we expect 7 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__7, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)2230)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)2230)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)2233)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)2233)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)2241)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2266)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2266)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2267)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2267)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2271)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2271)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)2279)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)2279)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)2283)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)2283)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)2299)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)2301)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)2301)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMAX search using GFEVNT. Max is in 4th qua"
	    "drant.", (ftnlen)70);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 FEB 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)2458)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)2460)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)2497)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)2497)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)2500)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)2500)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)2508)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2533)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2533)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2534)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2534)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2538)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2538)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)2546)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)2546)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)2550)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)2550)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)2566)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)2568)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)2568)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMAX search using GFEVNT. Max is in left ha"
	    "lf circle.", (ftnlen)74);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 SEP 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)2722)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)2724)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)2761)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)2761)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)2764)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)2764)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)2772)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2797)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2797)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2798)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2798)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)2802)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)2802)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)2810)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)2810)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)2814)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)2814)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)2830)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)2832)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)2832)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMAX search using GFEVNT. Max is in 1st qua"
	    "drant.", (ftnlen)70);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 APR 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)2985)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)2987)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)3024)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)3024)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)3027)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)3027)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)3035)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3060)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3060)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3061)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3061)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3065)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3065)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)3073)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)3073)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)3077)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)3077)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)3093)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)3095)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)3095)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMIN search using GFEVNT. Min is in 4th qua"
	    "drant.", (ftnlen)70);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 FEB 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)3247)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)3249)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)3286)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)3286)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)3289)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)3289)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)3297)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3322)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3322)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3323)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3323)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3327)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3327)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)3335)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)3335)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)3339)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)3339)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)3355)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)3357)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)3357)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMIN search using GFEVNT. Min is in left ha"
	    "lf circle.", (ftnlen)74);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 SEP 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)3510)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)3512)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)3549)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)3549)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)3552)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)3552)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)3560)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3585)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3585)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3586)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3586)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3590)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3590)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)3598)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)3598)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)3602)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)3602)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)3618)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)3620)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)3620)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA adjusted ABSMIN search using GFEVNT. Min is in 1st qua"
	    "drant.", (ftnlen)70);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 APR 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;

/*     Note non-zero adjustment value: */

    adjust = rpd_() * 2.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA adjusted absolute extrema searches require */
/*     9 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 7-9", (ftnlen)78, (ftnlen)24);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)3774)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)3776)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude adjusted absolute extrema searches require 7-9 passes; */
/*     in this case we expect 9 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__9, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)3813)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)3813)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)3816)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)3816)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)3824)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3849)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3849)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3850)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3850)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)3854)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)3854)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)3862)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)3862)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)3866)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)3866)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)3882)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)3884)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)3884)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run an RA < (inequality) search using GFEVNT. Reference value is"
	    " in 1st quadrant.", (ftnlen)81);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 APR 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "RA/DEC", (ftnlen)80, (ftnlen)6);
    s_copy(qcpars + 320, "RIGHT ASCENSION", (ftnlen)80, (ftnlen)15);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "<", (ftnlen)80, (ftnlen)1);
    refval = pi_() / 4;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA inequality searches require 8 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__8, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 8", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)4033)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)4035)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude inequality searches require 8 passes, */
/*     so we expect 8 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__8, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)4072)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)4072)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)4075)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)4075)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)4083)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4108)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4108)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4109)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4109)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4113)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4113)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)4121)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)4121)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)4125)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)4125)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)4141)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)4143)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)4143)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude > (inequality) search using GFEVNT. Reference va"
	    "lue is in 1st quadrant.", (ftnlen)87);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 APR 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, ">", (ftnlen)80, (ftnlen)1);
    refval = pi_() / 4;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA inequality searches require 8 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__8, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 8", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__3 = (i__ << 1) - 2) < 40 && 0 <= i__3 ? 
		i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)4291)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__3 = (i__ << 1) - 1) < 40 && 0 <= i__3 
		? i__3 : s_rnge("msglog", i__3, "f_gflorp__", (ftnlen)4293)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude inequality searches require 8 passes, */
/*     so we expect 8 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__8, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__3 = i__ - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge(
		"trmlog", i__3, "f_gflorp__", (ftnlen)4330)] - isqlog[(i__2 = 
		i__ - 1) < 20 && 0 <= i__2 ? i__2 : s_rnge("isqlog", i__2, 
		"f_gflorp__", (ftnlen)4330)] - 1;
	i__3 = ncalls;
	for (j = 1; j <= i__3; ++j) {
	    xsqlog[(i__2 = j - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge("xsql"
		    "og", i__2, "f_gflorp__", (ftnlen)4333)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)4333)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__3 = totcal) < 20000 && 0 <= i__3 ? i__3 : 
		s_rnge("seqlog", i__3, "f_gflorp__", (ftnlen)4341)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4366)], 
		">=", &replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4366)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4367)], 
		"<=", &replog[(i__2 = i__ * 3 - 2) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4367)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? i__3 : 
		    s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4371)] == 
		    replog[(i__2 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4371)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)4379)], ">=", &replog[(i__2 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__2 ? i__2 : s_rnge("replog", 
			i__2, "f_gflorp__", (ftnlen)4379)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__3 = i__ * 3 - 1) < 60000 && 0 <= 
			i__3 ? i__3 : s_rnge("replog", i__3, "f_gflorp__", (
			ftnlen)4383)] - replog[(i__2 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__2 ? i__2 : s_rnge("replog", i__2, 
			"f_gflorp__", (ftnlen)4383)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = cardd_(&cnflog[(i__3 = i__ * 206 - 206) < 4120 && 0 <= i__3 ? 
		i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (ftnlen)4399)]);
	for (j = 2; j <= i__2; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__3 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__3 ? i__3 : s_rnge("cnflog", i__3, "f_gflorp__", (
		    ftnlen)4401)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)4401)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a longitude equality search using GFEVNT. Reference value is"
	    " in 1st quadrant.", (ftnlen)81);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For absolute extrema, the Sun and Earth provide a good test */
/*     case, since the Earth-Sun position vector traverses the full */
/*     range of possible longitudes. */

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     near the epoch of the absolute minimum. */


    str2et_("2000 APR 1", &et0, (ftnlen)10);
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = et1 + spd_();
    et1 = et0 + spd_() * 10;
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 80, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LONGITUDE", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 400, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 480, "POSITION", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "=", (ftnlen)80, (ftnlen)1);
    refval = pi_() / 4;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 1e5;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude/RA equality searches require 2 passes. */

/*     Look up the log of the progress report initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)4550)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gflorp__", (ftnlen)4552)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Longitude equality searches require 2 passes, */
/*     so we expect 2 report termination calls. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gflorp__", (ftnlen)4589)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gflorp__", (ftnlen)4589)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gflorp__", (ftnlen)4592)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gflorp__", (ftnlen)4592)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gflorp__", (ftnlen)4600)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4625)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4625)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4626)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4626)], &
		c_b109, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gflorp__", (ftnlen)4630)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gflorp__", (ftnlen)4630)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)4638)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gflorp__", (ftnlen)4638)], &c_b109, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gflorp__", (
			ftnlen)4642)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gflorp__", (ftnlen)4642)];
	    }
	}
    }

/*     Find the sum of the measures of the confinement windows */
/*     for each pass. */

    cnfmes = 0.;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = cardd_(&cnflog[(i__2 = i__ * 206 - 206) < 4120 && 0 <= i__2 ? 
		i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (ftnlen)4658)]);
	for (j = 2; j <= i__3; j += 2) {
	    cnfmes = cnfmes + cnflog[(i__2 = j + i__ * 206 - 201) < 4120 && 0 
		    <= i__2 ? i__2 : s_rnge("cnflog", i__2, "f_gflorp__", (
		    ftnlen)4660)] - cnflog[(i__4 = j - 1 + i__ * 206 - 201) < 
		    4120 && 0 <= i__4 ? i__4 : s_rnge("cnflog", i__4, "f_gfl"
		    "orp__", (ftnlen)4660)];
	}
    }

/*     Compare the measure of the reported progress to the sum of */
/*     the measures of the confinement windows used for each */
/*     search. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b146, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gflorp.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     To be added, if necessary. */


/* ---- Case ------------------------------------------------------------- */

    t_success__(ok);
    return 0;
} /* f_gflorp__ */

