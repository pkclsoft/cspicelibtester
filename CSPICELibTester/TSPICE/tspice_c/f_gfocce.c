/* f_gfocce.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__200 = 200;
static integer c__1 = 1;
static integer c__0 = 0;
static doublereal c_b53 = 1e-6;
static integer c__4 = 4;

/* $Procedure      F_GFOCCE ( GFOCCE family tests ) */
/* Subroutine */ int f_gfocce__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static doublereal left, step;
    static integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmi_(char *, char *,
	     integer *, char *, ftnlen, ftnlen, ftnlen);
    static doublereal right;
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime;
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    extern logical gfbail_();
    extern /* Subroutine */ int gfocce_(char *, char *, char *, char *, char *
	    , char *, char *, char *, char *, doublereal *, U_fp, U_fp, 
	    logical *, U_fp, U_fp, U_fp, logical *, L_fp, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *), delfil_(char *, ftnlen);
    static doublereal cnfine[206];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    extern /* Subroutine */ int gfrefn_(), gfrepf_(), gfrepi_();
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen);
    extern /* Subroutine */ int gfrepu_(), gfstep_();
    extern /* Subroutine */ int natspk_(char *, logical *, integer *, ftnlen);
    static doublereal et0, et1, result[206];
    static char timstr[50];
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     tstlsk_(void), ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), gfsstp_(doublereal *), 
	    tstspk_(char *, logical *, integer *, ftnlen), wnfetd_(doublereal 
	    *, integer *, doublereal *, doublereal *), timout_(doublereal *, 
	    char *, char *, ftnlen, ftnlen), spkuef_(integer *);
    extern doublereal spd_(void);
    static doublereal tol;
    static logical rpt;
    static integer han1, han2;
    static char utc0[50], utc1[50];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFOCCE */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine partially tests the mid-level SPICELIB geometry */
/*     routine GFOCCE. Complete testing of GFOCCE requires execution of */
/*     the test families */

/*        F_GFOCLT */
/*        F_ZZGFOCU */

/*     as well. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 22-FEB-2016 (NJB) */

/*        Updated expected short error messages. */

/* -    TSPICE Version 1.1.0, 15-APR-2014 (EDW) */

/*        Added explicit declaration of GFBAIL type. */

/* -    TSPICE Version 1.0.0, 17-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     SPICELIB default functions for */

/*        - Interrupt handling (no-op function):   GFBAIL */
/*        - Search refinement:                     GFREFN */
/*        - Progress report termination:           GFREPF */
/*        - Progress report initialization:        GFREPI */
/*        - Progress report update:                GFREPU */
/*        - Search step size "get" function:       GFSTEP */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFOCCE", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("generic.bsp", &c_true, &han2, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a confinement window. Initialize this and */
/*     the result window. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + (i__ - 1) * spd_() + 3600.;
	right = left + 79200.;
	wninsd_(&left, &right, cnfine);
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small (detected before search)", (ftnlen)48);
    ssized_(&c__1, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 TDB", &et0, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 5 TDB", &et1, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    rpt = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-6;
    gfocce_("ANY", "MOON", "ellipsoid", "IAU_MOON", "SUN", "ellipsoid", "IAU"
	    "_SUN", "LT", "EARTH", &c_b53, (U_fp)gfstep_, (U_fp)gfrefn_, &rpt, 
	    (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_,
	     cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)9, (ftnlen)8, (
	    ftnlen)3, (ftnlen)9, (ftnlen)7, (ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(WINDOWTOOSMALL)", ok, (ftnlen)21);

/*     Restore original result window size. */

    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad convergence tolerance.", (ftnlen)26);
    str2et_("2000 JAN 1 TDB", &et0, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 5 TDB", &et1, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    rpt = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 0.;
    gfocce_("ANY", "MOON", "ellipsoid", "IAU_MOON", "SUN", "ellipsoid", "IAU"
	    "_SUN", "LT", "EARTH", &tol, (U_fp)gfstep_, (U_fp)gfrefn_, &rpt, (
	    U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, 
	    cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)9, (ftnlen)8, (
	    ftnlen)3, (ftnlen)9, (ftnlen)7, (ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized shape for front target", (ftnlen)35);
    tol = 1e-6;
    gfocce_("ANY", "MOON", "ray", "IAU_MOON", "SUN", "ellipsoid", "IAU_SUN", 
	    "LT", "EARTH", &tol, (U_fp)gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)
	    gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, 
	    cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)3, (ftnlen)8, (
	    ftnlen)3, (ftnlen)9, (ftnlen)7, (ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized shape for back target", (ftnlen)34);
    tol = 1e-6;
    gfocce_("ANY", "MOON", "point", "IAU_MOON", "SUN", "ray", "IAU_SUN", 
	    "LT", "EARTH", &tol, (U_fp)gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)
	    gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, 
	    cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)5, (ftnlen)8, (
	    ftnlen)3, (ftnlen)3, (ftnlen)7, (ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Any occultation of ALPHA by BETA, abcorr = LT+S", (ftnlen)47);

/*     Note: the stellar aberration correction spec should be ignored. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    rpt = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-6;
    gfocce_("ANY", "beta", "ellipsoid", "betafixed", "alpha", "ellipsoid", 
	    "alphafixed", "LT+S", "sun", &tol, (U_fp)gfstep_, (U_fp)gfrefn_, &
	    rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)
	    gfbail_, cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)9, (ftnlen)
	    9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);
	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        Check occultation start time. */

	xtime = (doublereal) (i__ - 1) * spd_() + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b53, ok, (ftnlen)80, (ftnlen)1);

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 600. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b53, ok, (ftnlen)80, (ftnlen)1)
		;
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK files. */

    spkuef_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfocce__ */

