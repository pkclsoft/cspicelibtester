/* f_gfoclt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__200 = 200;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__3 = 3;
static integer c__0 = 0;
static doublereal c_b378 = 239.999;
static doublereal c_b379 = 260.;
static doublereal c_b411 = 59.999000000000002;
static doublereal c_b412 = 80.;
static integer c__4 = 4;
static doublereal c_b456 = 3e-6;
static integer c__8 = 8;
static doublereal c_b856 = 3.5e-5;
static doublereal c_b972 = 1.75e-4;
static doublereal c_b1087 = 1e-4;
static doublereal c_b1109 = 0.;
static doublereal c_b1110 = 1e-6;
static integer c__10 = 10;

/* $Procedure      F_GFOCLT ( GFOCLT family tests ) */
/* Subroutine */ int f_gfoclt__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double asin(doublereal), sin(doublereal);

    /* Local variables */
    static char back[32];
    static doublereal arad[3], brad[3], left;
    static integer nlat;
    static doublereal last;
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal step, a, c__;
    static integer i__, n;
    static doublereal range;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    extern doublereal jyear_(void);
    static doublereal asize;
    static logical found;
    static doublereal right;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime, first;
    static char front[32];
    static doublereal start;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    static char aframe[32], bframe[32];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    static char fframe[32];
    static doublereal lt;
    static char bshape[32];
    static doublereal cnfine[206];
    static char fshape[32], abcorr[5];
    static doublereal palpha[3], pmcoef[3];
    extern integer wncard_(doublereal *);
    static doublereal finish;
    extern logical exists_(char *, ftnlen);
    static char obsrvr[32], occtyp[32], timstr[50];
    static doublereal bounds[4]	/* was [2][2] */, corpar[10], et0, et1, 
	    newrad[3], result[206], rscale, xfinsh, xstart;
    static integer bodyid;
    static doublereal beg;
    static integer corsys, surfid;
    static logical makvtl, usepad;
    extern /* Subroutine */ int natspk_(char *, logical *, integer *, ftnlen),
	     chckxc_(logical *, char *, logical *, ftnlen), tstspk_(char *, 
	    logical *, integer *, ftnlen), natpck_(char *, logical *, logical 
	    *, ftnlen);
    extern logical odd_(integer *);
    static doublereal end;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     tstlsk_(void), ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), gfoclt_(char *, char *,
	     char *, char *, char *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), gdpool_(char *, 
	    integer *, integer *, integer *, doublereal *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    dvpool_(char *, ftnlen), scardd_(integer *, doublereal *), 
	    pdpool_(char *, integer *, doublereal *, ftnlen), spkuef_(integer 
	    *);
    extern doublereal rpd_(void), spd_(void);
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), wnfetd_(doublereal *, 
	    integer *, doublereal *, doublereal *), timout_(doublereal *, 
	    char *, char *, ftnlen, ftnlen), bodvrd_(char *, char *, integer *
	    , integer *, doublereal *, ftnlen, ftnlen), spkpos_(char *, 
	    doublereal *, char *, char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal tol;
    extern /* Subroutine */ int gfstol_(doublereal *), unload_(char *, ftnlen)
	    , delfil_(char *, ftnlen), natdsk_(char *, char *, integer *, 
	    integer *, char *, integer *, integer *, ftnlen, ftnlen, ftnlen), 
	    bodvcd_(integer *, char *, integer *, integer *, doublereal *, 
	    ftnlen), furnsh_(char *, ftnlen);
    static integer han1, han2;
    static char utc0[50], utc1[50];
    extern /* Subroutine */ int t_secds2__(integer *, integer *, char *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    logical *, logical *, char *, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFOCLT */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFOCLT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 3.1.0, 31-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLV. */

/* -    TSPICE Version 3.0.0, 23-MAY-2016 (NJB) */

/*        Updated to test support for DSK shapes. */

/*        Updated expected keywords for invalid shape cases. */

/* -    TSPICE Version 2.3.0, 20-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 2.2.0, 26-JUL-2008 (NJB) */

/*        Updated to account for change in GFOCLT's argument list. */
/*        Tests of error handling for small result window were added. */

/* -    TSPICE Version 2.1.0, 14-APR-2008 (NJB) */

/*        Kernel clean-up now consists only of */
/*        SPK unloading via SPKUEF and SPK file */
/*        deletion. */

/* -    TSPICE Version 2.0.0, 12-APR-2008 (NJB) */

/*        Added test cases using zero radii and blank frame */
/*        names for point targets. Added cases for invalid */
/*        aberration corrections. Added cases to test */
/*        handling of missing SPK and PCK orientation data. */

/* -    TSPICE Version 1.1.0, 13-MAR-2008 (NJB) */

/*        Corrected one syntax error and numerous expressions */
/*        that caused FTNCHEK warnings. */

/* -    TSPICE Version 1.0.0, 15-NOV-2007 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Looser time tolerance for point/ellipsoid */
/*     occultations where the ellipsoid is */
/*     in front: */


/*     Still looser time tolerance for point/ellipsoid */
/*     occultations where the ellipsoid is */
/*     in back: */

/*      INTEGER               NREF */
/*      PARAMETER           ( NREF = 3 ) */

/*     Local variables */


/*     Saved everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFOCLT", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("generic.bsp", &c_true, &han2, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a confinement window. Initialize this and */
/*     the result window. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + (i__ - 1) * spd_() + 3600.;
	right = left + 79200.;
	wninsd_(&left, &right, cnfine);
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small (detected before search)", (ftnlen)48);
    ssized_(&c__1, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(WINDOWTOOSMALL)", ok, (ftnlen)21);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small (detected during search)", (ftnlen)48);
    str2et_("2000 JAN 1 TDB", &et0, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 5 TDB", &et1, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 300.;
    ssized_(&c__2, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfoclt_("Any", "beta", "ELLIPSOID", "betaFIXED", "alpha", "ellipsoid", 
	    "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);
    ssized_(&c__200, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized shape for front target", (ftnlen)35);
    gfoclt_("Any", "ALPHA", "pnt", "ALPHAFIXED", "BETA", "ellipsoid", "BETAF"
	    "IXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5, (
	    ftnlen)3, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2, 
	    (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/*     Missing "unprioritized" keyword: */

    gfoclt_("Any", "ALPHA", "DSK", "ALPHAFIXED", "BETA", "ellipsoid", "BETAF"
	    "IXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5, (
	    ftnlen)3, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2, 
	    (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized shape for back target", (ftnlen)34);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsod", "BETA"
	    "FIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5, 
	    (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)8, (ftnlen)9, (ftnlen)2,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/*     Missing "unprioritized" keyword: */

    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "DSK", "BETAFIXED",
	     "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5, (
	    ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)3, (ftnlen)9, (ftnlen)2, 
	    (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);
/* ---- Case ------------------------------------------------------------- */

    tcase_("Two point targets", (ftnlen)17);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "point", "BETAFIX"
	    "ED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5, (
	    ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)5, (ftnlen)9, (ftnlen)2, 
	    (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDSHAPECOMBO)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Targets are identical", (ftnlen)21);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "ALPHA", "ellipsoid", 
	    "BETAFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Front target and observer are identical", (ftnlen)39);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "alpha", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (
	    ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Back target and observer are identical", (ftnlen)38);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "beta", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (
	    ftnlen)2, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Non-positive step size", (ftnlen)22);
    step = 0.;
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = -1.;
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad occultation type", (ftnlen)20);
    step = 1.;
    gfoclt_("Ay", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BETA"
	    "FIXED", "LT", "sun", &step, cnfine, result, (ftnlen)2, (ftnlen)5, 
	    (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOCCTYPE)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation type unsuitable for point target", (ftnlen)44);
    step = 1.;
    gfoclt_("Annular", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "LT", "sun", &step, cnfine, result, (ftnlen)7, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADTYPESHAPECOMBO)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized targets or observer", (ftnlen)32);
    step = 1.;
    gfoclt_("Any", "ALPH", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BETA"
	    "FIXED", "LT", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)4, 
	    (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETAA", "ellipsoid", 
	    "BETAFIXED", "LT", "sun", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "sunN", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (
	    ftnlen)2, (ftnlen)4);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction values", (ftnlen)32);
    step = 1.;
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "S", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)5, 
	    (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)1,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "XS", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "RLT", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (
	    ftnlen)3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "XRLT", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "z", "sun", &step, cnfine, result, (ftnlen)3, (ftnlen)5, 
	    (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)1,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for observer", (ftnlen)24);
    step = 1.;
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "GASPRA", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for front target", (ftnlen)28);
    step = 1.;
    gfoclt_("Any", "GASPRA", "point", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)6, (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for back target", (ftnlen)27);
    step = 1.;
    gfoclt_("Any", "ALPHA", "ellipsoid", "ALPHAFIXED", "gaspra", "point", 
	    "BETAFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)6, (ftnlen)5, (ftnlen)9, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for front target; both targets are ellip"
	    "soids.", (ftnlen)70);
    step = 1e3;
    gfoclt_("Any", "EARTH", "ELLIPSOID", "ITRF93", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)9, (ftnlen)6, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2,
	     (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for back target; both targets are ellips"
	    "oids.", (ftnlen)69);
    step = 1e3;
    gfoclt_("Any", "ALPHA", "ELLIPSOID", "ALPHAFIXED", "EARTH", "ellipsoid", 
	    "ITRF93", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    5, (ftnlen)9, (ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)6, (
	    ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for front target; front target is ellips"
	    "oid; back is point.", (ftnlen)83);

/*     Capture Beta's prime meridian data; delete it from */
/*     the kernel pool. */

    gdpool_("BODY2000_PM", &c__1, &c__3, &n, pmcoef, &found, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dvpool_("BODY2000_PM", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In order to exercise the SINCPT call, we need to use a */
/*     start time such that the bounding sphere logic is skipped. */
/*     The specific times used here are dependent on the geometry */
/*     of Nat's solar system. */

    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b378, &c_b379, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 2.;
    gfoclt_("Any", "beta", "ellipsoid", "betafixed", "alpha", "point", "ALPH"
	    "AFIXED", "none", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Restore the PM data. */

    pdpool_("BODY2000_PM", &c__3, pmcoef, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for back target; front target is point; "
	    "back is ellipsoid.", (ftnlen)82);

/*     Because the frame ALPHAFIXED is a TK frame defined */
/*     relative to the PCK frame BETAAFIXED, we can make */
/*     the SINCPT call, which accesses the ALPHAFIXED frame, */
/*     fail by damaging the data for the for BETAAFIXED frame. */

/*     This scheme depends on the Nat's solar system PCK */
/*     implementation. */

/*     Capture Beta's prime meridian data; delete it from */
/*     the kernel pool. */

    gdpool_("BODY2000_PM", &c__1, &c__3, &n, pmcoef, &found, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dvpool_("BODY2000_PM", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In order to exercise the SINCPT call, we need to use a */
/*     start time such that the bounding sphere logic is skipped. */
/*     The specific times used here are dependent on the geometry */
/*     of Nat's solar system. */

    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b411, &c_b412, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 2.;
    gfoclt_("Any", "beta", "point", "betafixed", "alpha", "ellipsoid", "ALPH"
	    "AFIXED", "none", "Sun", &step, cnfine, result, (ftnlen)3, (ftnlen)
	    4, (ftnlen)5, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Restore the PM data. */

    pdpool_("BODY2000_PM", &c__3, pmcoef, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */


/*     Unload the generic test SPK, so we don't have interference from */
/*     the sun ephemeris. */

    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     All expected event times are based on Nat's solar system. */

    tcase_("Any occultation of ALPHA by BETA, abcorr = NONE", (ftnlen)47);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Create a confinement window with 4 intervals. */

    for (i__ = 1; i__ <= 4; ++i__) {
	left = et0 + (i__ - 1) * spd_() + 3600.;
	right = left + 79200.;
	wninsd_(&left, &right, cnfine);
    }
    step = 60.;
    gfoclt_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_();
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 600.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Any occultation of ALPHA by BETA, abcorr = LT+S", (ftnlen)47);

/*     Note: the stellar aberration correction spec should be ignored. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "LT+S", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);
	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        Check occultation start time. */

	xtime = (doublereal) (i__ - 1) * spd_() + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 600. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Annular transit of ALPHA by BETA, abcorr = NONE", (ftnlen)47);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("ANNULAR", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid",
	     "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)7, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 120.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 480.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Annular transit of ALPHA by BETA, abcorr = CN", (ftnlen)45);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("ANNULAR", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid",
	     "ALPHAFIXED", "CN", "SUN", &step, cnfine, result, (ftnlen)7, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 120. + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 480. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */


/*     All expected events time are based on Nat's solar system. */

    tcase_("Partial occultation of ALPHA by BETA, abcorr = NONE", (ftnlen)51);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Partial", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid",
	     "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)7, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__8, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);
	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        Check occultation start time. */

	if (odd_(&i__)) {
	    xtime = (doublereal) ((i__ + 1) / 2 - 1) * spd_();
	} else {
	    xtime = (doublereal) (i__ / 2 - 1) * spd_() + 480.;
	}
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	if (odd_(&i__)) {
	    xtime = (doublereal) ((i__ + 1) / 2 - 1) * spd_() + 120.;
	} else {
	    xtime = (doublereal) (i__ / 2 - 1) * spd_() + 600.;
	}
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */


/*     All expected events time are based on Nat's solar system. */

    tcase_("Partial occultation of ALPHA by BETA, abcorr = XLT", (ftnlen)50);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Partial", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid",
	     "ALPHAFIXED", "XLT", "SUN", &step, cnfine, result, (ftnlen)7, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)3, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__8, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);
	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        Check occultation start time. */

	if (odd_(&i__)) {
	    xtime = (doublereal) ((i__ + 1) / 2 - 1) * spd_() - 1.;
	} else {
	    xtime = (doublereal) (i__ / 2 - 1) * spd_() + 480. - 1.;
	}
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	if (odd_(&i__)) {
	    xtime = (doublereal) ((i__ + 1) / 2 - 1) * spd_() + 120. - 1.;
	} else {
	    xtime = (doublereal) (i__ / 2 - 1) * spd_() + 600. - 1.;
	}
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Full occultation of ALPHA by BETA, abcorr = NONE. No events expe"
	    "cted.", (ftnlen)69);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Full", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     There should be no full occultations. */

    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__0, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Full occultation, abcorr = NONE. ALPHA re-sized to 1/2 angular s"
	    "ize of BETA.", (ftnlen)76);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up the radii of alpha. */

    bodvrd_("ALPHA", "RADII", &c__3, &n, arad, (ftnlen)5, (ftnlen)5);

/*     Compute the scale factor needed to reduce the */
/*     angular radius in the x-y plane of ALPHA to 1/8 its original */
/*     size. Recall that alpha's x-axis is parallel to the J2000 z-axis, */
/*     the +y-axis points toward the Sun at noon, and Alpha's +z axis */
/*     points in the J2000 -y direction at noon. So Alpha's */
/*     angular size in the x-y plane is controlled by the lengths */
/*     of its y and z axes, which are equal. */

    spkpos_("ALPHA", &et0, "J2000", "NONE", "SUN", palpha, &lt, (ftnlen)5, (
	    ftnlen)5, (ftnlen)4, (ftnlen)3);
    range = vnorm_(palpha);
    asize = asin(arad[1] / range);
    newrad[1] = sin(asize / 8.) * range;
    rscale = newrad[1] / arad[1];

/*     Produce the new radii of ALPHA and store these */
/*     in the kernel pool. */

    vscl_(&rscale, arad, newrad);

/*     Because of alpha's large vertical dimension, we must shrink */
/*     this dimension even farther to make total occultation possible. */
/*     We shrink Alpha's x-axis dimension (which is aligned with the */
/*     J2000 z-axis) by a factor of 4, making the total shrinkage */
/*     factor in this dimension equal to 32. */

    newrad[0] /= 4;
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);

/*     Search for full occultations using the modified */
/*     version of ALPHA. */

    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Full", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "NONE", "SUN", &step, cnfine, result, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 270.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 330.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/*     Restore original radii of ALPHA. */

    pdpool_("BODY1000_RADII", &c__3, arad, (ftnlen)14);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Full occultation, abcorr = LT. ALPHA re-sized to 1/2 angular siz"
	    "e of BETA.", (ftnlen)74);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up the radii of alpha. */

    bodvrd_("ALPHA", "RADII", &c__3, &n, arad, (ftnlen)5, (ftnlen)5);

/*     Compute the scale factor needed to reduce the */
/*     angular radius of ALPHA to 1/8 its original size. */

    spkpos_("ALPHA", &et0, "J2000", "NONE", "SUN", palpha, &lt, (ftnlen)5, (
	    ftnlen)5, (ftnlen)4, (ftnlen)3);
    range = vnorm_(palpha);
    asize = asin(arad[1] / range);
    newrad[1] = sin(asize / 8.) * range;
    rscale = newrad[1] / arad[1];

/*     Produce the new radii of ALPHA and store these */
/*     in the kernel pool. */

    vscl_(&rscale, arad, newrad);

/*     Because of alpha's large vertical dimension, we must shrink */
/*     this dimension even farther to make total occultation possible. */
/*     We shrink Alpha's x-axis dimension (which is aligned with the */
/*     J2000 z-axis) by a factor of 4, making the total shrinkage */
/*     factor in this dimension equal to 32. */

    newrad[0] /= 4;
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);

/*     Search for full occultations using the modified */
/*     version of ALPHA. */

    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Full", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 270. + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 330. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/*     Restore original radii of ALPHA. */

    pdpool_("BODY1000_RADII", &c__3, arad, (ftnlen)14);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation of ALPHA center by BETA, abcorr = NONE.", (ftnlen)51);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "point", " ", 
	    "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4, (
	    ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (
	    ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 240.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 360.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }
/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation of ALPHA center by BETA, abcorr = NONE, radii of ALP"
	    "HA set to 0", (ftnlen)75);
    bodvrd_("ALPHA", "RADII", &c__3, &n, arad, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, newrad);
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "point", " ", 
	    "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4, (
	    ftnlen)9, (ftnlen)9, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (
	    ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 240.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 360.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/*     Restore the original radii. */

    pdpool_("BODY1000_RADII", &c__3, arad, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation of ALPHA center by BETA, abcorr = LT.", (ftnlen)49);

/*     Note that we use a looser time tolerance for these events, */
/*     since the light time correction for the ellipsoid is not */
/*     based on distance from the observer to the ellipsoid's center, */
/*     but rather to the intercept point, which is closer to */
/*     the observer. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "point", " ", 
	    "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4, (ftnlen)
	    9, (ftnlen)9, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)2, (ftnlen)
	    3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 240. + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b856, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 360. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b856, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Transit of BETA center across ALPHA, abcorr = NONE.", (ftnlen)51);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "POINT", " ", "ALPHA", "ELLIPSOID", "ALPHAFIXED", 
	    "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4, (
	    ftnlen)5, (ftnlen)1, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, 
	    (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 60.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 540.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Transit of BETA center across ALPHA, abcorr = NONE, BETA raii se"
	    "t to 0", (ftnlen)70);
    bodvrd_("BETA", "RADII", &c__3, &n, brad, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, newrad);
    pdpool_("BODY2000_RADII", &c__3, newrad, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "POINT", " ", "ALPHA", "ELLIPSOID", "ALPHAFIXED", 
	    "NONE", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4, (
	    ftnlen)5, (ftnlen)1, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, 
	    (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 60.;
	chcksd_(title, &left, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 540.;
	chcksd_(title, &right, "~", &xtime, &c_b456, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }
    pdpool_("BODY2000_RADII", &c__3, brad, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Transit of BETA center across ALPHA, abcorr = LT.", (ftnlen)49);
    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "BETA", "POINT", "BETAFIXED", "ALPHA", "ELLIPSOID", "ALPH"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4,
	     (ftnlen)5, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__4, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);
    i__1 = wncard_(result);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &left, &right);

/*        Check occultation start time. */

	s_copy(title, "Occ # beg time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 60. + 1.;
	chcksd_(title, &left, "~", &xtime, &c_b972, ok, (ftnlen)80, (ftnlen)1)
		;

/*        Check occultation end time. */

	s_copy(title, "Occ # end time", (ftnlen)80, (ftnlen)14);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	xtime = (doublereal) (i__ - 1) * spd_() + 540. + 1.;
	chcksd_(title, &right, "~", &xtime, &c_b972, ok, (ftnlen)80, (ftnlen)
		1);
	timout_(&left, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) I, ' ',  TIMSTR */
	timout_(&right, "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)", timstr, (
		ftnlen)39, (ftnlen)50);
/*         WRITE (*,*) '  ', TIMSTR */
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Any occultation of BETA by ALPHA, abcorr = LT+S", (ftnlen)47);

/*     Since Alpha can't occult Beta as seen from the Sun, we */
/*     should produce an empty result window. */


/*     Note: the stellar aberration correction spec should be ignored. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "ALPHA", "ellipsoid", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "LT+S", "SUN", &step, cnfine, result, (ftnlen)3, (
	    ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__0, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation of BETA center by ALPHA, abcorr = LT", (ftnlen)48);

/*     Since Alpha can't occult Beta as seen from the Sun, we */
/*     should produce an empty result window. */


/*     Note: the stellar aberration correction spec should be ignored. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "ALPHA", "ellipsoid", "ALPHAFIXED", "BETA", "point", "BET"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)5, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__0, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Occultation of BETA by ALPHA center, abcorr = LT", (ftnlen)48);

/*     Since Alpha can't occult Beta as seen from the Sun, we */
/*     should produce an empty result window. */


/*     Note: the stellar aberration correction spec should be ignored. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);
    wninsd_(&et0, &et1, cnfine);
    step = 60.;
    gfoclt_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)5,
	     (ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("RESULT cardinality", &i__1, "=", &c__0, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);

/*     Case */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    s_copy(utc0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(utc1, "2000 JAN 05 00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(utc0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(utc1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 60.;
    gfoclt_("Any", "BETA", "POINT", "BETAFIXED", "ALPHA", "ELLIPSOID", "ALPH"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4,
	     (ftnlen)5, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    n = wncard_(result);
    chcksi_("N", &n, "!=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b1087);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfoclt_("Any", "BETA", "POINT", "BETAFIXED", "ALPHA", "ELLIPSOID", "ALPH"
	    "AFIXED", "LT", "SUN", &step, cnfine, result, (ftnlen)3, (ftnlen)4,
	     (ftnlen)5, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)
	    2, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    n = wncard_(result);
    chcksi_("N", &n, "!=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b1109, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b1110);
/* *********************************************************************** */

/*     DSK tests */

/* *********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Create a DSK containing shape models for bodies Alpha and Beta.", 
	    (ftnlen)63);

/*     Make sure the generic SPK is unloaded! We need the sun ephemeris */
/*     from Nat's SPK. */

    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/*     This block is suitable for use in F_GFOCLT. We don't actually */
/*     have to have the high-resolution DSK patches to test OCCULT. */

/*     We'll enhance the generic DSK models for Alpha and Beta by */
/*     appending segments containing small patches of high-resolution */
/*     data for the surface regions that participate in computation of */
/*     accurate occultation ingress and egress times. We use small */
/*     patches because the size and run time needed to create full */
/*     models at this resolution would be prohibitive. */

/*     Start by creating the basic DSK. */

    if (exists_("gfoclt_nat.bds", (ftnlen)14)) {
	unload_("gfoclt_nat.bds", (ftnlen)14);
	delfil_("gfoclt_nat.bds", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Use low resolution tessellations for the main models. */

    nlon = 20;
    nlat = 10;
    s_copy(aframe, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
    s_copy(bframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    natdsk_("gfoclt_nat.bds", aframe, &nlon, &nlat, bframe, &nlon, &nlat, (
	    ftnlen)14, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create high-resolution patches for Alpha. At the J2000 epoch, */
/*     the ALPHAFIXED +X axis is aligned with the J2000 +Z axis. The */
/*     ALPHAFIXED +Y axis is aligned with the J2000 +Y axis. */


/*     Create the -Y patch for body Alpha. The patch covers */
/*     the lon/lat rectangle */

/*        -92 deg. <= lon <= -88 deg. */
/*          0 deg. <= lat <=   4 deg. */

/*     Note that Alpha' body-fixed Z axis lies in Alpha's orbital */
/*     plane. */

    bodyid = 1000;
    surfid = 2;
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, arad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make the patch spherical, using the ellipsoid's Z */
/*     semi-axis length. */

    c__ = arad[2];
    corsys = 1;
    cleard_(&c__10, corpar);
    makvtl = FALSE_;
    bounds[0] = rpd_() * -92.;
    bounds[1] = rpd_() * -88.;
    bounds[2] = rpd_() * 0.;
    bounds[3] = rpd_() * 4.;
    nlat = 200;
    nlon = 200;
    usepad = TRUE_;

/*     Append the patch segment to the existing DSK. */

    t_secds2__(&bodyid, &surfid, aframe, &first, &last, &corsys, corpar, 
	    bounds, &c__, &c__, &c__, &nlon, &nlat, &makvtl, &usepad, "gfocl"
	    "t_nat.bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the +Y patch for body Alpha. The patch covers */
/*     the lon/lat rectangle */

/*         88 deg. <= lon <=  92 deg. */
/*          0 deg. <= lat <=   4 deg. */

    surfid = 3;
    bounds[0] = rpd_() * 88.;
    bounds[1] = rpd_() * 92.;
    bounds[2] = rpd_() * 0.;
    bounds[3] = rpd_() * 4.;
    nlat = 200;
    nlon = 200;
    usepad = TRUE_;

/*     Append the patch segment to the existing DSK. */

    t_secds2__(&bodyid, &surfid, aframe, &first, &last, &corsys, corpar, 
	    bounds, &c__, &c__, &c__, &nlon, &nlat, &makvtl, &usepad, "gfocl"
	    "t_nat.bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create high-resolution patches for Beta. At the J2000 epoch, */
/*     the BETAFIXED +X axis is aligned with the J2000 +Y axis. The */
/*     BETAFIXED +Y axis is aligned with the J2000 -X axis. */

    bodyid = 2000;
    surfid = 2;
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, brad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make the patch spherical, using the ellipsoid's X */
/*     semi-axis length. */

    a = brad[0];
    corsys = 1;
    cleard_(&c__10, corpar);
    makvtl = FALSE_;
    bounds[0] = rpd_() * 0.;
    bounds[1] = rpd_() * 4.;
    bounds[2] = rpd_() * -2.;
    bounds[3] = rpd_() * 2.;
    nlat = 200;
    nlon = 200;
    usepad = TRUE_;
    t_secds2__(&bodyid, &surfid, bframe, &first, &last, &corsys, corpar, 
	    bounds, &a, &a, &a, &nlon, &nlat, &makvtl, &usepad, "gfoclt_nat."
	    "bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bounds[0] = rpd_() * 178.;
    bounds[1] = rpd_() * -178.;
    bounds[2] = rpd_() * -2.;
    bounds[3] = rpd_() * 2.;
    t_secds2__(&bodyid, &surfid, bframe, &first, &last, &corsys, corpar, 
	    bounds, &a, &a, &a, &nlon, &nlat, &makvtl, &usepad, "gfoclt_nat."
	    "bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the DSK for subsequent use. */

    furnsh_("gfoclt_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find transit of point BETA across DSK ALPHA near the epoch 2000 "
	    "JAN 1 12:05:00 TDB", (ftnlen)82);
    str2et_("2000 JAN 1 11:00:00 TDB", &et0, (ftnlen)23);
    str2et_("2000 JAN 1 13:00:00 TDB", &et1, (ftnlen)23);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(occtyp, "ANY", (ftnlen)32, (ftnlen)3);
    s_copy(obsrvr, "SUN", (ftnlen)32, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    s_copy(front, "BETA", (ftnlen)32, (ftnlen)4);
    s_copy(fshape, "POINT", (ftnlen)32, (ftnlen)5);
    s_copy(fframe, " ", (ftnlen)32, (ftnlen)1);
    s_copy(back, "ALPHA", (ftnlen)32, (ftnlen)5);
    s_copy(bframe, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
    s_copy(bshape, "DSK/UNPRIORITIZED", (ftnlen)32, (ftnlen)17);
    gfoclt_(occtyp, front, fshape, fframe, back, bshape, bframe, abcorr, 
	    obsrvr, &step, cnfine, result, (ftnlen)32, (ftnlen)32, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)5, (
	    ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    start = result[6];
    finish = result[7];
    xstart = 60.;
    xfinsh = 540.;

/*     On cayenne (PC/Linux/gfortran 64-bit), this tolerance may be */
/*     reduced to 1.D-6. */

    tol = 3e-6;
    chcksd_("START", &start, "~", &xstart, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FINISH", &finish, "~", &xfinsh, &tol, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find occultation by DSK BETA of point ALPHA near the epoch 2000 "
	    "JAN 1 12:05:00 TDB", (ftnlen)82);
    str2et_("2000 JAN 1 11:00:00 TDB", &et0, (ftnlen)23);
    str2et_("2000 JAN 1 13:00:00 TDB", &et1, (ftnlen)23);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(occtyp, "ANY", (ftnlen)32, (ftnlen)3);
    s_copy(obsrvr, "SUN", (ftnlen)32, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    s_copy(front, "BETA", (ftnlen)32, (ftnlen)4);
    s_copy(fshape, "DSK/UNPRIORITIZED", (ftnlen)32, (ftnlen)17);
    s_copy(fframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(back, "ALPHA", (ftnlen)32, (ftnlen)5);
    s_copy(bframe, " ", (ftnlen)32, (ftnlen)1);
    s_copy(bshape, "POINT", (ftnlen)32, (ftnlen)5);
    gfoclt_(occtyp, front, fshape, fframe, back, bshape, bframe, abcorr, 
	    obsrvr, &step, cnfine, result, (ftnlen)32, (ftnlen)32, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)5, (
	    ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    start = result[6];
    finish = result[7];
    xstart = 240.;
    xfinsh = 360.;

/*     On cayenne (PC/Linux/gfortran 64-bit), this tolerance may be */
/*     reduced to 1.D-6. */

    tol = 3e-6;
    chcksd_("START", &start, "~", &xstart, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FINISH", &finish, "~", &xfinsh, &tol, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK and DSK files. */

    spkuef_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("gfoclt_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfoclt_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfoclt__ */

