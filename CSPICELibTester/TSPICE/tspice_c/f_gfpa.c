/* f_gfpa.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__5000 = 5000;
static integer c__2 = 2;
static integer c__0 = 0;
static integer c__5 = 5;
static integer c__1000 = 1000;
static integer c__10 = 10;
static integer c__2000 = 2000;
static doublereal c_b200 = 1e-4;
static doublereal c_b235 = 0.;
static integer c__1 = 1;
static doublereal c_b257 = 1e-6;

/* $Procedure F_GFPA ( GFPA family tests ) */
/* Subroutine */ int f_gfpa__(logical *ok)
{
    /* Initialized data */

    static char corr[80*5] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double asin(doublereal);

    /* Local variables */
    extern /* Subroutine */ int gfpa_(char *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal left, posa[3], posb[3], step, work[25030]	/* was [5006][
	    5] */;
    static integer i__, j;
    extern /* Subroutine */ int zztstpck_(char *, ftnlen);
    static doublereal alpha;
    extern /* Subroutine */ int zztstlsk_(char *, ftnlen), tcase_(char *, 
	    ftnlen), repmc_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal value, right;
    static integer ndays;
    static char title[80], lumin[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int scardd_(integer *, doublereal *);
    static doublereal cnfine[5006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer nw;
    static char abcorr[80];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), kclear_(void);
    static char relate[80];
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    extern integer wncard_(doublereal *);
    static char target[80];
    static doublereal adjust, refval;
    static char obsrvr[80];
    static doublereal result[5006];
    extern /* Subroutine */ int furnsh_(char *, ftnlen), natpck_(char *, 
	    logical *, logical *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *
	    , doublereal *, doublereal *), wnfetd_(doublereal *, integer *, 
	    doublereal *, doublereal *), spkezp_(integer *, doublereal *, 
	    char *, char *, integer *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), gfstol_(doublereal *);
    static doublereal beg, end;
    extern doublereal spd_(void);
    static integer han1;
    extern /* Subroutine */ int zzgfpaq_(doublereal *, integer *, integer *, 
	    integer *, char *, doublereal *, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFPA */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB geometry */
/*     routine GFPA. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.0.0, 18-JUN-2013 (EDW)(BVS) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFPA", (ftnlen)6);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);

/*     Create an LSK, load using FURNSH. */

    zztstlsk_("gfpa.tls", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfpa.tls", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    zztstpck_("gfpa.pck", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfpa.pck", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    left = 0.;
    right = spd_() * 360;
    ssized_(&c__5000, result);
    ssized_(&c__2, cnfine);
    scardd_(&c__0, result);
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*    Error cases */


/*     Case 1 */

    tcase_("Non-positive step size", (ftnlen)22);
    step = 0.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Case 2 */

    tcase_("Non unique body IDs.", (ftnlen)20);
    step = 1.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    s_copy(target, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(obsrvr, "SUN", (ftnlen)80, (ftnlen)3);
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*     Case 3 */

    tcase_("Invalid aberration correction specifier", (ftnlen)39);
    step = 1.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "X", (ftnlen)80, (ftnlen)1);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*     Case 4 */

    tcase_("Invalid relations operator", (ftnlen)26);
    step = 1.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "==", (ftnlen)80, (ftnlen)2);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*     Case 5 */

    tcase_("Invalid body names", (ftnlen)18);
    step = 1.;
    adjust = 0.;
    s_copy(target, "X", (ftnlen)80, (ftnlen)1);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "X", (ftnlen)80, (ftnlen)1);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(obsrvr, "X", (ftnlen)80, (ftnlen)1);
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/*     Case 6 */

    tcase_("Negative adjustment value", (ftnlen)25);
    step = 1.;
    adjust = -1.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 7 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    step = 1.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "DAWN", (ftnlen)80, (ftnlen)4);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*     Case 8 */

    tcase_("Invalid value for MW and NW", (ftnlen)27);
    step = 1.;
    adjust = 0.;
    s_copy(target, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__0, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Work window count below limit - NW = 4 */

    nw = 4;
    step = 1.;
    adjust = 0.;
    s_copy(target, "ALPHA", (ftnlen)80, (ftnlen)5);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "BETA", (ftnlen)80, (ftnlen)4);
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = 0.;
    gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &nw, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 9 */

/*     Loops over aberration corrections for a configuration with */
/*     a known geometry and behavior. */

    step = spd_() * .2;
    adjust = 0.;
    refval = 0.;
    s_copy(target, "ALPHA", (ftnlen)80, (ftnlen)5);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "BETA", (ftnlen)80, (ftnlen)4);

/*     Confine for ninety days. */

    ndays = 90;
    left = 0.;
    right = ndays * spd_();
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {

/*        Observe the phase angle of ALPHA from BETA. Two */
/*        minimum values per day will occur. */

	s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);
	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfpa__", (ftnlen)537)) * 80, (ftnlen)
		80, (ftnlen)80);
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
		cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80,
		 (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */
/*        We expect two events per day over CNFINE. */

	count = 0;
	count = wncard_(result);
	i__1 = ndays << 1;
	chcksi_("COUNT", &count, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Two maximum values per day will occur. */

	s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
		cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80,
		 (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */
/*        We expect two events per day over CNFINE. */

	count = 0;
	count = wncard_(result);
	i__1 = ndays << 1;
	chcksi_("COUNT", &count, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Check for a specific reference value. */

    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    refval = .1;
    for (i__ = 1; i__ <= 5; ++i__) {
	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfpa__", (ftnlen)594)) * 80, (ftnlen)
		80, (ftnlen)80);
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfpa_(target, lumin, abcorr, obsrvr, relate, &refval, &adjust, &step, 
		cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80,
		 (ftnlen)80, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	count = 0;
	count = wncard_(result);
	i__1 = count;
	for (j = 1; j <= i__1; ++j) {
	    wnfetd_(result, &j, &beg, &end);
	    zzgfpaq_(&beg, &c__1000, &c__10, &c__2000, abcorr, &value, (
		    ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(title, &refval, "~", &value, &c_b200, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/*     Case 10 */

    s_copy(title, "Check phase angle against known value", (ftnlen)80, (
	    ftnlen)37);
    tcase_(title, (ftnlen)80);

/*     We can compute the value of the maximum phase angle based */
/*     on the ALPHA-BETA geometry, the half-angle of the BETA */
/*     orbit as seen from ALPHA. */

/*        sin(alpha) = beta_orbit_radius     5.246368076245e+05 */
/*                     ------------------ ~  ------------------ */
/*                     alpha_orbit_radius    2.098547206045e+06 */

    spkezp_(&c__1000, &left, "J2000", corr, &c__10, posa, &lt, (ftnlen)5, (
	    ftnlen)80);
    spkezp_(&c__2000, &left, "J2000", corr, &c__10, posb, &lt, (ftnlen)5, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    alpha = asin(vnorm_(posb) / vnorm_(posa));
    step = spd_() * .2;
    adjust = 0.;
    s_copy(target, "ALPHA", (ftnlen)80, (ftnlen)5);
    s_copy(lumin, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(obsrvr, "BETA", (ftnlen)80, (ftnlen)4);
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);
    gfpa_(target, lumin, corr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    i__1 = count;
    for (j = 1; j <= i__1; ++j) {
	wnfetd_(result, &j, &beg, &end);
	zzgfpaq_(&beg, &c__1000, &c__10, &c__2000, corr, &value, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(title, &alpha, "~", &value, &c_b200, ok, (ftnlen)80, (ftnlen)
		1);
    }

/*     Minimum phase angle should have value approximately 0. */

    s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);
    gfpa_(target, lumin, corr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    i__1 = count;
    for (j = 1; j <= i__1; ++j) {
	wnfetd_(result, &j, &beg, &end);
	zzgfpaq_(&beg, &c__1000, &c__10, &c__2000, corr, &value, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(title, &c_b235, "~", &value, &c_b200, ok, (ftnlen)80, (ftnlen)
		1);
    }

/*     Case 11 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);
    wnfetd_(result, &c__1, &beg, &end);

/*     Re-run a valid search after using GFSTOL. */

    gfstol_(&c_b200);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    gfpa_(target, lumin, corr, obsrvr, relate, &refval, &adjust, &step, 
	    cnfine, &c__5000, &c__5, work, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b235, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b257);

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfpa.pck", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfpa.tls", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    return 0;
} /* f_gfpa__ */

