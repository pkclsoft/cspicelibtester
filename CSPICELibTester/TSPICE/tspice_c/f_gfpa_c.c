/*

-Procedure f_gfpa_c ( Test gfpa_c )

-Abstract

   Perform tests on the CSPICE wrapper gfpa_c.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   void zzgfpaq(  SpiceDouble      et,
                  SpiceInt         targ,
                  SpiceInt         illum,
                  SpiceInt         obs,
                  SpiceChar      * abcorr,
                  SpiceDouble    * value );

   void f_gfpa_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrapper for gfpa_c.

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)

-Version

   -tspice_c Version 1.1.0, 08-FEB-2017 (EDW)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.

   -tspice_c Version 1.0.0 09-MAY-2012 (EDW)

-Index_Entries

   test gfpa_c

-&
*/

{ /* Begin f_gfpa_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Constants
   */
   #define SPK             "gfpa.bsp"
   #define PCK             "gfpa.pck"
   #define LSK             "gfpa.tls"
   #define SPK1            "nat.bsp"
   #define PCK1            "nat.pck"
   #define MEDIUM           1.e-4
   #define LNSIZE          80
   #define NINTVL          5000
   #define NCORR           5

   SpiceInt                han1;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                count;
   SpiceInt                ndays;

   SpiceChar               abcorr [LNSIZE];
   SpiceChar               target [LNSIZE];
   SpiceChar               lumin  [LNSIZE];
   SpiceChar               relate [LNSIZE];
   SpiceChar               obsrvr [LNSIZE];
   SpiceChar               title  [LNSIZE];
   SpiceChar             * CORR   [] = { "NONE",
                                         "lt",
                                         " lt+s",
                                         " cn",
                                         " cn + s" };

   SpiceDouble             alpha;
   SpiceDouble             left;
   SpiceDouble             right;
   SpiceDouble             step;
   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             beg;
   SpiceDouble             end;
   SpiceDouble             lt;
   SpiceDouble             value;
   SpiceDouble             posa [3];
   SpiceDouble             posb [3];

   SPICEDOUBLE_CELL      ( cnfine,   NINTVL*2 );
   SPICEDOUBLE_CELL      ( result,   NINTVL*2 );

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfpa_c" );


   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );

   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create the PCK for Nat's Solar System.
   */
   natpck_( PCK1, (logical *) &false_,
                  (logical *) &true_,
                  (ftnlen) strlen(PCK1) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create the SPK for Nat's Solar System.
   */
   natspk_( SPK1, (logical *) &false_,
                  &han1,
                  (ftnlen) strlen(SPK1) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   left  = 0.;
   right = spd_c();

   scard_c( 0, &cnfine);

   wninsd_c ( left, right, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Error cases
   */

   /*
   Case 1
   */
   tcase_c ( "Non-positive step size" );

   step   = 0;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA",  LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA", LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok);


   /*
   Case 2
   */
   tcase_c ( "Non unique body IDs" );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA",  LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "ALPHA", LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   repmc_c( "#", "#", "SUN",   LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA", LNSIZE, obsrvr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",  LNSIZE, lumin  );
   repmc_c( "#", "#", "SUN",  LNSIZE, obsrvr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   /*
   Case 3
   */
   tcase_c ( "Invalid aberration correction specifier" );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",  LNSIZE,  obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "X",     LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok);



   /*
   Case 4
   */
   tcase_c ( "Invalid relations operator" );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA",  LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "Earth", LNSIZE,  obsrvr );
   repmc_c( "#", "#", "==",    LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);


   /*
   Case 5
   */
   tcase_c ( "Invalid body names" );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "X",     LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",  LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "X",     LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",  LNSIZE, obsrvr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",  LNSIZE, lumin  );
   repmc_c( "#", "#", "X",    LNSIZE, obsrvr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   /*
   Case 6
   */
   tcase_c ( "Negative adjustment value" );

   step   = 1;
   adjust =-1.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",  LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);


   /*
   Case 7
   */
   tcase_c ( "Ephemeris data unavailable" );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "DAWN",  LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok);


   /*
   Case 8
   */
   tcase_c ( "Invalid value for nintvls." );

   step   = 1;
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA",  LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA", LNSIZE, obsrvr );
   repmc_c( "#", "#", "=",     LNSIZE, relate);
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            0,
            &cnfine,
            &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);


   /*
   Case 9

   Loops over aberration corrections for a configuration with
   a known geometry and behavior.
   */

   step   =  0.2 * spd_();
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA", LNSIZE, target );
   repmc_c( "#", "#", "SUN",   LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",  LNSIZE, obsrvr );
   repmc_c( "#", "#", "NONE",  LNSIZE, abcorr );

   /*
   Confine for ninety days.
   */
   ndays = 90.;
   left  = 0.;
   right = ndays * spd_c();

   scard_c( 0, &cnfine);

   wninsd_c ( left, right, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i = 0; i < NCORR; i++)
      {

      /*
      Observe the phase angle of ALPHA from BETA. Two
      minimum values per day will occur.
      */
      repmc_c( "#", "#", CORR[i],   LNSIZE, abcorr );
      repmc_c( "#", "#", "LOCMIN",  LNSIZE, relate );

      repmc_c( "# #", "#", abcorr,  LNSIZE, title );
      repmc_c( title, "#", relate,  LNSIZE, title );
      tcase_c ( title );

      gfpa_c ( target,
               lumin,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               NINTVL,
               &cnfine,
               &result );
      chckxc_c ( SPICEFALSE, " ", ok);


      /*
      Check the number of intervals in the result window.
      We expect two events per day over CNFINE.
      */
      count = 0;
      count = wncard_c( &result );
      chcksi_c ( "LOCMIN COUNT", count, "=", 2*ndays, 0, ok );


      /*
      Two maximum values per day will occur.
      */
      repmc_c( "#", "#", "LOCMAX",  LNSIZE, relate );
      repmc_c( "# #", "#", abcorr,  LNSIZE, title );
      repmc_c( title, "#", relate,  LNSIZE, title );
      tcase_c( title );

      gfpa_c ( target,
               lumin,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               NINTVL,
               &cnfine,
               &result );
      chckxc_c ( SPICEFALSE, " ", ok);

      count = 0;
      count = wncard_c( &result );
      chcksi_c ( "LOCMAX COUNT", count, "=", 2*ndays, 0, ok );
      }


   repmc_c( "#", "#", "=",  LNSIZE, relate );
   refval = 0.1;

   for ( i = 0; i < NCORR; i++)
      {

      /*
      Observe the phase angle of ALPHA from BETA. Two
      minimum values per day will occur.
      */
      repmc_c( "#", "#", CORR[i],   LNSIZE, abcorr );

      repmc_c( "# #", "#", abcorr,  LNSIZE, title );
      repmc_c( title, "#", relate,  LNSIZE, title );
      tcase_c ( title );

      gfpa_c ( target,
               lumin,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               NINTVL,
               &cnfine,
               &result );
      chckxc_c ( SPICEFALSE, " ", ok);


      /*
      Check the number of intervals in the result window.
      We expect two events per day over CNFINE.
      */
      count = 0;
      count = wncard_c( &result );

      for ( j = 0; j < count; j++)
         {
         wnfetd_c( &result, j, &beg, &end );

         (void)zzgfpaq( beg, 1000, 10, 2000, abcorr, &value );
         chckxc_c ( SPICEFALSE, " ", ok);

         chcksd_c ( title, refval, "~", value, MEDIUM, ok );
         }

      }




   /*
   Case 10
   */
   repmc_c( "#", "#", "Check phase angle against known value",
                                              LNSIZE, title );
   tcase_c( title );

   /*
   We can compute the value of the maximum phase angle based
   on the ALPHA-BETA geometry, the half-angle of the BETA
   orbit as seen from ALPHA.

      sin(alpha) = beta_orbit_radius     5.246368076245e+05
                   ------------------ ~  ------------------
                   alpha_orbit_radius    2.098547206045e+06
   */
   spkezp_c( 1000, left, "J2000", CORR[0], 10, posa, &lt );
   spkezp_c( 2000, left, "J2000", CORR[0], 10, posb, &lt );
   chckxc_c ( SPICEFALSE, " ", ok);

   alpha = asin( vnorm_c( posb )/vnorm_c( posa ) );

   step   =  0.2 * spd_();
   adjust = 0.;
   refval = 0.;
   repmc_c( "#", "#", "ALPHA",  LNSIZE, target );
   repmc_c( "#", "#", "SUN",    LNSIZE, lumin  );
   repmc_c( "#", "#", "BETA",   LNSIZE, obsrvr );
   repmc_c( "#", "#", "NONE",   LNSIZE, abcorr );
   repmc_c( "#", "#", "LOCMAX", LNSIZE, relate );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICEFALSE, " ", ok);

   count = 0;
   count = wncard_c( &result);

   for ( i = 0; i < count; i++)
      {
      wnfetd_c( &result, i, &beg, &end );

      (void)zzgfpaq( beg, 1000, 10, 2000, abcorr, &value );
      chckxc_c ( SPICEFALSE, " ", ok);

      chcksd_c ( title, alpha, "~", value, MEDIUM, ok );
      }


   /*
   Minimum phase angle should have value approximately 0.
   */
   repmc_c( "#", "#", "LOCMIN", LNSIZE, relate );

   gfpa_c ( target,
            lumin,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            NINTVL,
            &cnfine,
            &result );
   chckxc_c ( SPICEFALSE, " ", ok);

   count = 0;
   count = wncard_c( &result);

   for ( i = 0; i < count; i++)
      {
      wnfetd_c( &result, i, &beg, &end );

      (void)zzgfpaq( beg, 1000, 10, 2000, abcorr, &value );
      chckxc_c ( SPICEFALSE, " ", ok);

      chcksd_c ( title, 0, "~", value, MEDIUM, ok );
      }




   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );


   } /* End f_gfpa_c */



/*
-Procedure zzgfspq ( Test implementation, phase angle quantity )

-Abstract

   A pure C implementation of the function that calculates
   phase angle quantity. Use this function only as
   part of the GF phase test family.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   None.

-Brief_I/O

   None.

-Detailed_Input

   None.

-Detailed_Output

   None.

-Parameters

   None.

-Exceptions

   None.

-Files

   None.

-Particulars

   None.

-Examples

   None.

-Restrictions

   This routine supports the GF phase angle event test family.
   Use only in that circumstance.

-Literature_References

   None.

-Author_and_Institution

   E.D. Wright     (JPL)

-Version

   -tspice_c Version 1.0.0 31-OCT-2010 (EDW)

-Index_Entries

   None.

-&
*/
void zzgfpaq(  SpiceDouble      et,
               SpiceInt         targ,
               SpiceInt         illum,
               SpiceInt         obs,
               SpiceChar      * abcorr,
               SpiceDouble    * value )
   {

   SpiceDouble       pv1[3];
   SpiceDouble       pv2[3];
   SpiceDouble       lt;
   SpiceDouble       sep;
   SpiceChar       * ref = "J2000";

   spkezp_c( targ, et, ref, abcorr, obs, pv1, &lt);

   if ( eqstr_c( abcorr, "NONE" ) )
      {
      spkezp_c( illum, et,      ref, abcorr, targ, pv2, &lt);
      }
   else
      {
      spkezp_c( illum, et - lt, ref, abcorr, targ, pv2, &lt);
      }

   sep    = vsep_c( pv1, pv2 );

   *value = pi_c() - sep;

   }



