/* f_gfrefn.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b3 = -6.;
static doublereal c_b4 = 14.;
static doublereal c_b5 = 10.;
static integer c__12 = 12;
static doublereal c_b14 = 1e-12;

/* $Procedure F_GFREFN ( GFREFN family tests ) */
/* Subroutine */ int f_gfrefn__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    doublereal rlog, texp;
    integer seed1, i__;
    doublereal t;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmd_(char *, char *,
	     doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen), topen_(
	    char *, ftnlen);
    logical s1;
    doublereal t1, t2;
    logical s2;
    extern /* Subroutine */ int t_success__(logical *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), gfrefn_(doublereal *, doublereal *, logical *, 
	    logical *, doublereal *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    char txt[128];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFREFN */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.1, 19-DEC-2010 (EDW) */

/*        Corrected typo in CHCKSD call, "~" to "~/". */

/* -    TSPICE Version 1.0.0, 03-MAR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Begin every test family with an open call. */

    topen_("F_GFREFN", (ftnlen)8);
    seed1 = -82763653;

/*     Note, 5000 is an arbitrary value. */

    for (i__ = 1; i__ <= 5000; ++i__) {

/*        Set T1 and T2 so that TEXP = (T1 + T2)/2. */

	rlog = t_randd__(&c_b3, &c_b4, &seed1);
	t1 = pow_dd(&c_b5, &rlog) * 1.;
	t2 = pow_dd(&c_b5, &rlog) * 3.;
	texp = pow_dd(&c_b5, &rlog) * 2.;
	s_copy(txt, "T1 = +/- #1, T2 = +/-#2, random test", (ftnlen)128, (
		ftnlen)36);
	repmd_(txt, "#1", &t1, &c__12, txt, (ftnlen)128, (ftnlen)2, (ftnlen)
		128);
	repmd_(txt, "#2", &t2, &c__12, txt, (ftnlen)128, (ftnlen)2, (ftnlen)
		128);
	tcase_(txt, (ftnlen)128);

/*        T1 < T2. */

	gfrefn_(&t1, &t2, &s1, &s2, &t);
	chcksd_(txt, &t, "~/", &texp, &c_b14, ok, (ftnlen)128, (ftnlen)2);

/*        Negate the lot; T2 < T1 */

	d__1 = -t1;
	d__2 = -t2;
	gfrefn_(&d__1, &d__2, &s1, &s2, &t);
	d__1 = -texp;
	chcksd_(txt, &t, "~/", &d__1, &c_b14, ok, (ftnlen)128, (ftnlen)2);
    }
    t_success__(ok);
    return 0;
} /* f_gfrefn__ */

