/*

-Procedure f_gfrefn_c ( Test CSPICE GF refinement API )

 
-Abstract
 
   Perform tests on the CSPICE GF search refinement routine gfrefn_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
 
   void f_gfrefn_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Test the GF search refinement routine gfrefn_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 28-FEB-2009 (NJB) 

-Index_Entries

   test CSPICE adapters

-&
*/

{ /* Begin f_gfrefn_c */

   /*
   Local parameters 
   */
   #define  VTIGHT       ( 1.e-14 ) 

   /*
   Local variables
   */
   SpiceBoolean            s1[2] = { SPICETRUE, SPICEFALSE };
   SpiceBoolean            s2[2] = { SPICETRUE, SPICEFALSE };

   SpiceDouble             scale;
   SpiceDouble             t;
   SpiceDouble             t1;
   SpiceDouble             t2;

   SpiceInt                i;
   SpiceInt                j;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfrefn_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Normal case: t2 > t1." );
   

   for ( i = 0;  i < 2;  i++  )
   {
      for ( j = 0;  j < 2;  j++  )
      {
         scale = ( 10.0 * i ) + j;

         t1 = 5.0 * scale;
         t2 = 7.0 * scale;

         gfrefn_c ( t1, t2, s1[i], s2[j], &t );

         chckxc_c ( SPICEFALSE, " ", ok );

         chcksd_c ( "t", t, "~", scale * 6.0, VTIGHT, ok );
      }

   }


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Reversed input times: t2 < t1." );
  
  
   for ( i = 0;  i < 2;  i++  )
   {
      for ( j = 0;  j < 2;  j++  )
      {
         scale = ( 10.0 * i ) + j;

         t1 = 15.0 * scale;
         t2 =  7.0 * scale;

         gfrefn_c ( t1, t2, s1[i], s2[j], &t );

         chckxc_c ( SPICEFALSE, " ", ok );

         chcksd_c ( "t", t, "~", scale * 11.0, VTIGHT, ok );
      }

   }

     

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Equal input times" );
   

 
   for ( i = 0;  i < 2;  i++  )
   {
      for ( j = 0;  j < 2;  j++  )
      {
         scale = ( 10.0 * i ) + j;

         t1 = -scale;
         t2 = -scale;

         gfrefn_c ( t1, t2, s1[i], s2[j], &t );

         chckxc_c ( SPICEFALSE, " ", ok );

         chcksd_c ( "t", t, "~", -scale, VTIGHT, ok );
      }

   }


     
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfrefn_c */





 
