/* f_gfrprt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__400 = 400;
static integer c__0 = 0;
static doublereal c_b31 = 1e-12;
static doublereal c_b34 = 0.;
static doublereal c_b84 = 1.;
static doublereal c_b85 = .5;
static logical c_true = TRUE_;
static doublereal c_b111 = 2.;
static doublereal c_b116 = 1.5;

/* $Procedure      F_GFRPRT ( Test GFRPRT entry points ) */
/* Subroutine */ int f_gfrprt__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;
    cllist cl__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer f_clos(cllist *);

    /* Local variables */
    static doublereal incr, freq, span;
    static integer unit;
    extern /* Subroutine */ int zzgfwkmo_(integer *, doublereal *, doublereal 
	    *, integer *, char *, char *, doublereal *, ftnlen, ftnlen), 
	    zzgfwkun_(integer *);
    static integer i__, j;
    static doublereal t, delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer nsamp;
    static doublereal total, xincr, xfreq;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal start;
    static integer xunit;
    extern /* Subroutine */ int t_success__(logical *), chcksc_(char *, char *
	    , char *, char *, logical *, ftnlen, ftnlen, ftnlen, ftnlen), 
	    delfil_(char *, ftnlen);
    static integer tcheck;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char begmsg[55];
    static integer xcheck;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksd_(char *, doublereal 
	    *, char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen),
	     gfrepi_(doublereal *, char *, char *, ftnlen, ftnlen);
    static char endmsg[13];
    extern integer wncard_(doublereal *);
    static doublereal finish;
    static char xbegms[55];
    extern /* Subroutine */ int gfrepf_(void), wnfetd_(doublereal *, integer *
	    , doublereal *, doublereal *), gfrepu_(doublereal *, doublereal *,
	     doublereal *);
    static char xendms[13];
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *);
    static doublereal window[406], xtotal;
    extern /* Subroutine */ int gfrprt_(doublereal *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int txtopn_(char *, integer *, ftnlen);

/* $ Abstract */

/*     Test the GF progress reporting entry points contained in GFRPRT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF progress reporting routines */

/*        GFREPI */
/*        GFREPU */
/*        GFREPF */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFRPRT", (ftnlen)8);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test GFREPI: Make sure correct parameters are passed to ZZGFTSWK."
	    , (ftnlen)65);

/*     Open a text file to capture the progress report. */

    if (exists_("zzgfrpwk.txt", (ftnlen)12)) {
	delfil_("zzgfrpwk.txt", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    txtopn_("zzgfrpwk.txt", &xunit, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwkun_(&xunit);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window. */

    ssized_(&c__400, window);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 399; i__ += 2) {
	d__1 = (doublereal) i__;
	d__2 = (doublereal) (i__ + 1);
	wninsd_(&d__1, &d__2, window);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xtotal = 200.;
    s_copy(xbegms, "@--------------------Message prefix-------------------@", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "@**suffix***@", (ftnlen)13, (ftnlen)13);

/*     Call the progress report initializer. */

    gfrepi_(window, xbegms, xendms, (ftnlen)55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the current report parameters from the ZZGFRPWK monitor. */

    zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (ftnlen)
	    55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the stored values from ZZGFWKMO against the expected */
/*     values. */

/*     The default poll duration and call count are 1 second and */
/*     4 calls, respectively. These values are hard-coded in GFREPI. */

    xfreq = 1.;
    xcheck = 4;
    chcksi_("UNIT", &unit, "=", &xunit, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("TOTAL", &total, "~", &xtotal, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FREQ", &freq, "=", &xfreq, &c_b34, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("TCHECK", &tcheck, "=", &xcheck, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("BEGMSG", begmsg, "=", xbegms, ok, (ftnlen)6, (ftnlen)55, (ftnlen)
	    1, (ftnlen)55);
    chcksc_("ENDMSG", endmsg, "=", xendms, ok, (ftnlen)6, (ftnlen)13, (ftnlen)
	    1, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test GFREPU: Make sure correct work increments are passed to ZZG"
	    "FWKIN.", (ftnlen)70);
    i__1 = wncard_(window);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(window, &i__, &start, &finish);
	span = finish - start;

/*        Use progressively smaller samples. */

	nsamp = i__ + 2;
	delta = span / (nsamp - 1);
	i__2 = nsamp;
	for (j = 1; j <= i__2; ++j) {
	    t = start + (j - 1) * delta;
	    gfrepu_(&start, &finish, &t);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (
		    ftnlen)55, (ftnlen)13);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect the last work increment to be DELTA for all */
/*           but the first value of T. */

	    if (j == 1) {
		xincr = 0.;
	    } else {
		xincr = delta;
	    }
	    s_copy(qname, "INCR I=1*; J=*", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(qname, "*", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(qname, &incr, "~", &xincr, &c_b31, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test GFREPF: Make sure correct parameters are passed to ZZGFWKAD"
	    " and ZZGFWKIN.", (ftnlen)78);
    gfrepf_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     Fetch the current report parameters from the ZZGFRPWK monitor. */

    zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (ftnlen)
	    55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the stored values from ZZGFWKMO against the expected */
/*     values. */

/*     GFREPF is supposed to set the poll duration and call count to 0 */
/*     seconds and 1 call, respectively. The work increment should be */
/*     set to 0.D0. These values are hard-coded in GFREPF. */

    xfreq = 0.;
    xcheck = 1;
    chcksi_("UNIT", &unit, "=", &xunit, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("TOTAL", &total, "~", &xtotal, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FREQ", &freq, "=", &xfreq, &c_b34, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("TCHECK", &tcheck, "=", &xcheck, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("BEGMSG", begmsg, "=", xbegms, ok, (ftnlen)6, (ftnlen)55, (ftnlen)
	    1, (ftnlen)55);
    chcksc_("ENDMSG", endmsg, "=", xendms, ok, (ftnlen)6, (ftnlen)13, (ftnlen)
	    1, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up: delete log file.", (ftnlen)26);
    if (exists_("zzgfrpwk.txt", (ftnlen)12)) {
	cl__1.cerr = 0;
	cl__1.cunit = xunit;
	cl__1.csta = 0;
	f_clos(&cl__1);
	delfil_("zzgfrpwk.txt", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Call to umbrella routine", (ftnlen)24);
    gfrprt_(window, " ", " ", &c_b34, &c_b84, &c_b85, (ftnlen)1, (ftnlen)1);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPI: report message prefix is too long", (ftnlen)41);
    gfrepi_(window, "@------------------------------------------------------"
	    "------------------------------------#", " ", (ftnlen)92, (ftnlen)
	    1);
    chckxc_(&c_true, "SPICE(MESSAGETOOLONG)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPI: report message suffix is too long", (ftnlen)41);
    gfrepi_(window, " ", "@-------------------------------------------------"
	    "-----------------------------------------#", (ftnlen)1, (ftnlen)
	    92);
    chckxc_(&c_true, "SPICE(MESSAGETOOLONG)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPI: report message prefix contains non-printing characters.", 
	    (ftnlen)63);
    gfrepi_(window, "@-------#\004", " ", (ftnlen)10, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPI: report message suffix contains non-printing characters.", 
	    (ftnlen)63);
    gfrepi_(window, " ", "#-------!\005", (ftnlen)1, (ftnlen)10);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPU: interval bounds out of order.", (ftnlen)37);
    gfrepu_(&c_b84, &c_b34, &c_b111);
    chckxc_(&c_true, "SPICE(BADENDPOINTS)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("GFREPU: input time outside of interval.", (ftnlen)39);
    gfrepu_(&c_b84, &c_b116, &c_b111);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    t_success__(ok);
    return 0;
} /* f_gfrprt__ */

