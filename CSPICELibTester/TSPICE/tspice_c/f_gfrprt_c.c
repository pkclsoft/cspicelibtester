/*

-Procedure f_gfrprt_c ( Test CSPICE GF progress report APIs )

 
-Abstract
 
   Perform tests on the CSPICE GF progress report APIs.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "SpiceZst.h"
   #include "tutils_c.h"
 
   void f_gfrprt_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Test the GF progress reporting APIs

      gfrepi_c
      gfrepu_c
      gfrepf_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 08-MAR-2009 (NJB) 

-Index_Entries

   test CSPICE progress reporting interface routines

-&
*/

{ /* Begin f_gfrprt_c */

   
   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Local parameters
   */
   #define REPFIL         "gfrprt.txt"
   #define VTIGHT         ( 1.e-12 )
   #define LNSIZE         81
   #define MAXWIN         400
   #define BMSGLN         33
   #define EMSGLN         33


   /*
   Local variables
   */
   SPICEDOUBLE_CELL ( window, MAXWIN );


   SpiceChar               begmsg [ BMSGLN ];
   SpiceChar               endmsg [ EMSGLN ];
   SpiceChar               fullmsg[ LNSIZE ];
   SpiceChar               qname  [ LNSIZE ];
   SpiceChar             * xbegms;
   SpiceChar             * xendms;

   SpiceDouble             delta;
   SpiceDouble             finish;
   SpiceDouble             freq;
   SpiceDouble             incr;
   SpiceDouble             span;
   SpiceDouble             start;
   SpiceDouble             t;
   SpiceDouble             total;
   SpiceDouble             xfreq;
   SpiceDouble             xincr;
   SpiceDouble             xtotal;

   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                nsamp;
   SpiceInt                tcheck;
   SpiceInt                unit;
   SpiceInt                xcheck;
   SpiceInt                xunit;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfrprt_c" );




   /*
   *********************************************************************
   *
   *    Normal cases
   *
   *********************************************************************
   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Test gfrepi_c: Make sure correct parameters are "
             "passed to ZZGFTSWK."                              );


   /*
   Open a text file to capture the progress report.
   */
   if ( exists_c( REPFIL) ) 
   {
      TRASH( REPFIL );
   }
        

   txtopn_ ( ( char    * ) REPFIL,
             ( integer * ) &xunit,
             ( ftnlen    ) strlen(REPFIL) );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Tell the work reporting system to write to REPFIL. 
   */
   zzgfwkun_ ( &xunit );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Populate a confinement window. 
   */

   for ( i = 0;  i < MAXWIN-1;  i += 2 )
   {
      wninsd_c ( (SpiceDouble)i, (SpiceDouble)(i+1), &window );
      chckxc_c ( SPICEFALSE, " ", ok );

   }

   xtotal = MAXWIN / 2;

   xbegms = "@-------Message prefix---@";
   xendms = "@**suffix**@";

   /*
   Call the progress report initializer.
   */
   gfrepi_c ( &window, xbegms, xendms );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Fetch the current report parameters from the ZZGFRPWK monitor.
   */
   zzgfwkmo_ ( ( integer    * ) &unit, 
               ( doublereal * ) &total,
               ( doublereal * ) &freq,
               ( integer    * ) &tcheck,
               ( char       * ) begmsg,
               ( char       * ) endmsg,
               ( doublereal * ) &incr,
               ( ftnlen       ) BMSGLN,
               ( ftnlen       ) EMSGLN   );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Convert the output strings to C type. 
   */
   F2C_ConvertStr ( BMSGLN, begmsg );
   F2C_ConvertStr ( EMSGLN, endmsg );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the stored values from ZZGFWKMO against the expected values.
   */

   /*
   The default poll duration and call count are 1 second and
   4 calls, respectively. These values are hard-coded in GFREPI.
   */

   xfreq  = 1.0;
   xcheck = 4;

   chcksi_c ( "unit",   unit,   "=", xunit,  0,      ok );
   chcksd_c ( "total",  total,  "~", xtotal, VTIGHT, ok );
   chcksd_c ( "freq",   freq,   "=", xfreq,  0.0 ,   ok );
   chcksi_c ( "tcheck", tcheck, "=", xcheck, 0,      ok );
   chcksc_c ( "begmsg", begmsg, "=", xbegms,         ok );
   chcksc_c ( "endmsg", endmsg, "=", xendms,         ok );
 

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Test gfrepu_c: Make sure correct work increments are "
             "passed to ZZGFWKIN."                                    );


   for ( i = 0;  i < wncard_c( &window );  i++  )
   {
      wnfetd_c ( &window, i, &start, &finish );

      span = finish - start;

      /*
      Use progressively smaller samples.
      */
      nsamp = i + 2;

      delta = span / (nsamp-1);

      for ( j = 0;  j < nsamp;  j++ )
      {
         t = start + j*delta;

         gfrepu_c ( start, finish, t );

         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         Fetch the current report parameters from the ZZGFRPWK monitor.
         */
         zzgfwkmo_ ( ( integer    * ) &unit, 
                     ( doublereal * ) &total,
                     ( doublereal * ) &freq,
                     ( integer    * ) &tcheck,
                     ( char       * ) begmsg,
                     ( char       * ) endmsg,
                     ( doublereal * ) &incr,
                     ( ftnlen       ) BMSGLN,
                     ( ftnlen       ) EMSGLN   );

         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Convert the output strings to C type. 
         */
         F2C_ConvertStr ( BMSGLN, begmsg );
         F2C_ConvertStr ( EMSGLN, endmsg );

         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         We expect the last work increment to be delta for all
         but the first value of t.
         */
         if ( j == 0 ) 
         {            
            xincr = 0.0;
         }
         else
         {
            xincr = delta;
         }

         strncpy ( qname, "INCR I=1*; J=*", LNSIZE );

         repmi_c ( qname, "*", i, LNSIZE, qname );
         repmi_c ( qname, "*", j, LNSIZE, qname );

         chckxc_c ( SPICEFALSE, " ", ok );

         chcksd_c ( qname, incr, "~", xincr, VTIGHT, ok );
      }
   }


   
   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Test gfrepf_c: Make sure correct  work increments are "
             "passed to ZZGFWKAD and ZZGFWKIN."                       );

 

   gfrepf_c();

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Fetch the current report parameters from the ZZGFRPWK monitor.
   */
   zzgfwkmo_ ( ( integer    * ) &unit, 
               ( doublereal * ) &total,
               ( doublereal * ) &freq,
               ( integer    * ) &tcheck,
               ( char       * ) begmsg,
               ( char       * ) endmsg,
               ( doublereal * ) &incr,
               ( ftnlen       ) BMSGLN,
               ( ftnlen       ) EMSGLN   );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Convert the output strings to C type. 
   */
   F2C_ConvertStr ( BMSGLN, begmsg );
   F2C_ConvertStr ( EMSGLN, endmsg );

   chckxc_c ( SPICEFALSE, " ", ok );

 
   /*
   Check the stored values from ZZGFWKMO against the expected
   values.

   GFREPF is supposed to set the poll duration and call count to 0
   seconds and 1 call, respectively. The work increment should be
   set to 0.D0. These values are hard-coded in GFREPF.
   */
   xfreq  = 0.0;
   xcheck = 1; 

   chcksi_c ( "unit",   unit,   "=", xunit,  0,      ok );
   chcksd_c ( "total",  total,  "~", xtotal, VTIGHT, ok );
   chcksd_c ( "freq",   freq,   "=", xfreq,  0.0 ,   ok );
   chcksi_c ( "tcheck", tcheck, "=", xcheck, 0,      ok );
   chcksc_c ( "begmsg", begmsg, "=", xbegms,         ok );
   chcksc_c ( "endmsg", endmsg, "=", xendms,         ok );





   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   if ( exists_c(REPFIL) )
   {
      ftncls_c ( xunit );
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile ( REPFIL );
      chckxc_c ( SPICEFALSE, " ", ok );
   }
 



   /*
   *********************************************************************
   *
   *    Error cases
   *
   *********************************************************************
   */

 


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: null message pointers" );

   gfrepi_c ( &window, NULLCPTR, " " );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   gfrepi_c ( &window, " ", NULLCPTR );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: empty message strings" );

   gfrepi_c ( &window, "", "xxx" );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   gfrepi_c ( &window, "yyy", "" );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: report message prefix is too long" );

   gfrepi_c ( &window, 
              "@---------------------------------------------#"
              "@---------------------------------------------#",
              " "                                                );

   chckxc_c ( SPICETRUE, "SPICE(MESSAGETOOLONG)", ok );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: report message suffix is too long" );

   gfrepi_c ( &window, 
              " ",
              "@---------------------------------------------#"  );

   chckxc_c ( SPICETRUE, "SPICE(MESSAGETOOLONG)", ok );

 

   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: report message prefix contains "
             "non-printing characters."                );

   sprintf ( fullmsg, "%s%c%s", "@-------#", (char)4, " " );
   gfrepi_c ( &window,  fullmsg, " " );

   chckxc_c ( SPICETRUE, "SPICE(NONPRINTABLECHARS)", ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepi_c: report message suffix contains "
             "non-printing characters."                );

   sprintf ( fullmsg, "%s%s%c", " ", "@-------#", (char)5 );

   gfrepi_c ( &window,  " ", fullmsg );

   chckxc_c ( SPICETRUE, "SPICE(NONPRINTABLECHARS)", ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepu_c: interval bounds out of order."  );

   gfrepu_c ( 1.0, 0.0, 2.0 );

   chckxc_c ( SPICETRUE, "SPICE(BADENDPOINTS)", ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "gfrepu_c: input time outside of interval."  );

   gfrepu_c ( 1.0, 1.50, 2.0 );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );



   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfrprt_c */





 
