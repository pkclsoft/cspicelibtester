/* f_gfrr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__20000 = 20000;
static integer c__2 = 2;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__5 = 5;
static doublereal c_b188 = 1e-4;
static doublereal c_b203 = 0.;
static doublereal c_b204 = 1e-6;

/* $Procedure F_GFRR ( GFRR family tests ) */
/* Subroutine */ int f_gfrr__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal left;
    extern /* Subroutine */ int gfrr_(char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    static doublereal step, work[100030]	/* was [20006][5] */;
    static integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal right;
    static integer ndays;
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count, handl1, handl2;
    extern /* Subroutine */ int t_success__(logical *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), scardd_(integer *, doublereal *), kclear_(void);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char abcorr[80];
    static integer mw;
    static char relate[36];
    extern integer wncard_(doublereal *);
    static char target[36];
    static doublereal adjust;
    static char obsrvr[36];
    static doublereal refval, result[20006];
    static integer nw;
    extern /* Subroutine */ int tstlsk_(void), furnsh_(char *, ftnlen), 
	    tstspk_(char *, logical *, integer *, ftnlen), natpck_(char *, 
	    logical *, logical *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *
	    , doublereal *, doublereal *), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), wnfetd_(
	    doublereal *, integer *, doublereal *, doublereal *), gfstol_(
	    doublereal *), delfil_(char *, ftnlen);
    static doublereal beg, end;
    extern doublereal spd_(void);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFRR */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFRR. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.2.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.1.0, 20-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.0.0, 25-AUG-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFRR", (ftnlen)6);
    tcase_("Setup: create and load SPK and LSK files.", (ftnlen)41);

/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load an SPK file as well. */

    tstspk_("gfrr.bsp", &c_false, &handl1, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfrr.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.bsp", &c_false, &handl2, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    left = 0.;
    right = spd_() * 360;
    ssized_(&c__20000, result);
    ssized_(&c__2, cnfine);
    scardd_(&c__0, result);
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 1 */

    tcase_("Invalid result window size", (ftnlen)26);
    ssized_(&c__1, result);
    step = 10.;
    adjust = 0.;
    refval = 0.;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(relate, ">", (ftnlen)36, (ftnlen)1);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    ssized_(&c__0, result);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 2 */

    tcase_("Non-positive step size", (ftnlen)22);
    ssized_(&c__20000, result);
    ssized_(&c__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 0.;
    adjust = 0.;
    refval = 0.;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(relate, ">", (ftnlen)36, (ftnlen)1);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = -1.;
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Case 3 */

    tcase_("Invalid aberration correction specifier", (ftnlen)39);
    step = spd_() * 7;
    adjust = 0.;
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    refval = 0.;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    mw = 20000;
    nw = 5;
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "X", (ftnlen)80, (ftnlen)1);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*     Case 4 */

    tcase_("Invalid relations operator", (ftnlen)26);
    step = spd_() * 7;
    adjust = 0.;
    refval = 0.;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    mw = 20000;
    nw = 5;
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(relate, "!=", (ftnlen)36, (ftnlen)2);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*     Case 5 */

    tcase_("Invalid body names", (ftnlen)18);
    mw = 20000;
    nw = 5;
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    s_copy(target, "X", (ftnlen)36, (ftnlen)1);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "X", (ftnlen)36, (ftnlen)1);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/*     Case 6 */

    tcase_("Negative adjustment value", (ftnlen)25);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    refval = 0.;
    step = spd_() * 7;
    mw = 20000;
    nw = 5;
    adjust = -1.;
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 7 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    s_copy(target, "DAWN", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = spd_() * 7;
    mw = 20000;
    nw = 5;
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*     Case 8 */

    tcase_("Invalid value for MW, NW", (ftnlen)24);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    step = spd_() * 7;

/*     Usable size of WORK windows is zero. */

    mw = 0;
    nw = 5;
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Usable size of WORK windows is positive but below limit. */

    mw = 1;
    nw = 5;
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Usable size of WORK windows is positive but is too small */
/*     to hold all intervals found across CNFINE. CNFINE spans */
/*     360 days, the local maximums occur approximately every */
/*     28 days. */


    mw = 6;
    nw = 5;
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);

/*      Work window count below limit - NW = 4 */

    mw = 20000;
    nw = 4;
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 9 */

/*     Loops over aberration corrections for a configuration with */
/*     a known geometry and behavior. */

    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "BETA", (ftnlen)36, (ftnlen)4);
    refval = 0.;
    adjust = 0.;
    step = spd_() * .4;
    mw = 20000;
    nw = 5;

/*     Confine for ninety days. */

    ndays = 90;
    left = 0.;
    right = ndays * spd_();
    scardd_(&c__0, cnfine);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 9; ++i__) {
	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfrr__", (ftnlen)578)) * 80, (ftnlen)
		80, (ftnlen)80);
	s_copy(relate, "LOCMAX", (ftnlen)36, (ftnlen)6);
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine,
		 &mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */
/*        We expect one event per day. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &ndays, &c__0, ok, (ftnlen)5, (ftnlen)1)
		;
	s_copy(relate, "LOCMIN", (ftnlen)36, (ftnlen)6);
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine,
		 &mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */
/*        We expect one event per day. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &ndays, &c__0, ok, (ftnlen)5, (ftnlen)1)
		;

/*        Each orbit of BETA includes a local minimum and local */
/*        maximum range rate event with respect to ALPHA. As the */
/*        orbit is periodic and closed, two events per orbit must */
/*        exist where the sign of the range rate changes., i.e. */
/*        the range rate equals zero. */

	s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
	refval = 0.;
	repmc_("# #", "#", relate, title, (ftnlen)3, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine,
		 &mw, &nw, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */
/*        We expect two events per day. */

	count = 0;
	count = wncard_(result);
	i__1 = ndays << 1;
	chcksi_("COUNT", &count, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Case 10 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "BETA", (ftnlen)36, (ftnlen)4);
    refval = 0.;
    adjust = 0.;
    step = spd_() * .4;
    s_copy(relate, "LOCMIN", (ftnlen)36, (ftnlen)6);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    left = 0.;
    right = spd_() * 2.;
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b188);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine, &
	    c__20000, &c__5, work, result, (ftnlen)36, (ftnlen)80, (ftnlen)36,
	     (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b203, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b204);

/*     Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfrr.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfrr__ */

