/*

-Procedure f_gfrr_c ( Test gfrr_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfrr_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfrr_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfdist_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.2.0, 08-FEB-2017 (EDW)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.
 
   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 25-JUL-2008 (NJB) 

-Index_Entries

   test gfdist_c

-&
*/

{ /* Begin f_gfrr_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   #define string_copy(src, dest)      strncpy( dest, src,  strlen(src) ); \
                                       dest[strlen(src)] = '\0';
   /*
   Constants
   */
   #define SPK             "gfrr.bsp" 
   #define PCK             "gfrr.pck"
   #define LSK             "gfrr.tls"
   #define SPK1            "nat.bsp" 
   #define PCK1            "nat.pck"
   #define MED_REL         1.e-10
   #define MED_ABS         1.e-5
   #define LNSIZE          80 
   #define MAXWIN          100000
   #define NCORR           9 

   /*
   Local variables
   */

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );

   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             step;
   SpiceDouble             left;
   SpiceDouble             right;

   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                count;
   SpiceInt                ndays;
   SpiceInt                nintvls;
   SpiceInt                han1;

   SpiceChar               abcorr[LNSIZE];
   SpiceChar               title [LNSIZE*2];
   SpiceChar               target[LNSIZE];
   SpiceChar               obsrvr[LNSIZE];
   SpiceChar               relate[LNSIZE];

   SpiceChar             * CORR[]   = { "NONE", 
                                        "lt", 
                                        " lt+s",
                                        " cn",
                                        " cn + s",
                                        "XLT",  
                                        "XLT + S", 
                                        "XCN", 
                                        "XCN+S" 
                                        };

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfrr_c" );


   tcase_c ( "Setup: create and load SPK and LSK files." );
   
   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   natpck_( PCK1, (logical *) &false_, 
                  (logical *) &true_, 
                  (ftnlen) strlen(PCK) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create an SPK, load using FURNSH.
   */
   tstspk_c ( SPK, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   natspk_( SPK1, (logical *) &false_, 
                  &han1, 
                  (ftnlen) strlen(SPK1) ); 
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a confinement window.
   */
   left  = 0.;
   right = 360. * spd_c();
   wninsd_c ( left, right, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /* 
   Error cases
   */


   /*
   Case 1
   */
   tcase_c ( "Invalid result window size" );
 
   ssize_c( 1, &result);

   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "moon",  target);
   string_copy( "earth", obsrvr);
   string_copy( ">",     relate);
   string_copy( "none",  abcorr);
      
   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDDIMENSION)", ok );
 

   /*
   Case 2
   */
   tcase_c ( "Non-positive step size" );
 
   ssize_c( MAXWIN, &result);

   step    = 0.;
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "moon",  target);
   string_copy( "earth", obsrvr);
   string_copy( ">",     relate);
   string_copy( "none",  abcorr);
      
   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );

   step    = -1.;

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );


   /*
   Case 3
   */
   tcase_c ( "Invalid aberration correction specifier" );
 
   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "moon",   target);
   string_copy( "earth",  obsrvr);
   string_copy( "LOCMAX", relate);
   string_copy( "x",      abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );


   /*
   Case 4
   */
   tcase_c ( "Invalid relations operator" );
 
   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "moon",   target);
   string_copy( "earth",  obsrvr);
   string_copy( "!=",     relate);
   string_copy( "none",   abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok );



   /*
   Case 5
   */
   tcase_c ( "Invalid body names" );
 
   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "X",      target);
   string_copy( "sun",    obsrvr);
   string_copy( "LOCMAX", relate);
   string_copy( "none",   abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

   string_copy( "earth",  target);
   string_copy( "x",      obsrvr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );


   /*
   Case 6
   */
   tcase_c ( "Negative adjustment value" );
 
   step    = 7. * spd_c();
   adjust  = -1.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "moon",   target);
   string_copy( "sun",    obsrvr);
   string_copy( "LOCMAX", relate);
   string_copy( "none",   abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );


   /*
   Case 7
   */
   tcase_c ( "Ephemeris data unavailable" );
 
   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "dawn",   target);
   string_copy( "sun",    obsrvr);
   string_copy( "LOCMAX", relate);
   string_copy( "none",   abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok );


   /*
   Case 8
   */
   tcase_c ( "Invalid value for nintvls" );

   /*
   Usable size of workspace is zero.
   */
 
   step    = 7. * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = 0;
   string_copy( "moon",   target);
   string_copy( "sun",    obsrvr);
   string_copy( "LOCMAX", relate);
   string_copy( "none",   abcorr);

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );


   /*
   Usable size of work windows is positive but is too small
   to hold all intervals found across 'cnfine'. 'cnfine' spans 
   360 days, the local maximums occur approximately every 
   28 days.
   */

   nintvls = 3;

   gfrr_c ( target,
            abcorr,
            obsrvr,
            relate,
            refval,
            adjust,
            step,
            nintvls,
            &cnfine,
            &result );

   chckxc_c ( SPICETRUE, "SPICE(WINDOWEXCESS)", ok );



   /*
   Case 9

   Loops over aberration corrections for a configuration with
   a known geometry and behavior.
   */

   ndays = 90;
   left  = 0.;
   right = ndays * spd_c();

   scard_c  ( 0, &cnfine);
   wninsd_c ( left, right, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = .4 * spd_c();
   adjust  = 0.;
   refval  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "alpha",   target);
   string_copy( "beta",    obsrvr);
   string_copy( "none",   abcorr);

   for ( i=0; i<NCORR; i++ )
      {

      string_copy( CORR[i], abcorr);
      string_copy( "LOCMAX", relate);

      repmc_c ( "# #", "#", relate, LNSIZE, title );
      repmc_c ( title, "#", abcorr, LNSIZE, title );
      tcase_c ( title );
      
      gfrr_c ( target,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               nintvls,
               &cnfine,
               &result );

      chckxc_c ( SPICEFALSE, " ", ok );

      count = 0;
      count = wncard_c ( &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      We expect one event per day. 
      */
      chcksi_c ( "count", count, "=", ndays, 0, ok );
 
 
      string_copy( "LOCMIN", relate);

      repmc_c ( "# #", "#", relate, LNSIZE, title );
      repmc_c ( title, "#", abcorr, LNSIZE, title );
      tcase_c ( title );
      
      gfrr_c ( target,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               nintvls,
               &cnfine,
               &result );

      chckxc_c ( SPICEFALSE, " ", ok );

      count = 0;
      count = wncard_c ( &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      We expect one event per day. 
      */
      chcksi_c ( "count", count, "=", ndays, 0, ok );


      /*
      Each orbit of BETA includes a local minimum and local 
      maximum range rate event with respect to ALPHA. As the 
      orbit is periodic and closed, two events per orbit must 
      exist where the sign of the range rate changes., i.e. 
      the range rate equals zero.
      */
      string_copy( "=", relate);
      refval = 0.;

      repmc_c ( "# #", "#", relate, LNSIZE, title );
      repmc_c ( title, "#", abcorr, LNSIZE, title );
      tcase_c ( title );
      
      gfrr_c ( target,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               nintvls,
               &cnfine,
               &result );

      chckxc_c ( SPICEFALSE, " ", ok );

      count = 0;
      count = wncard_c ( &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      We expect two events per day.
      */
      chcksi_c ( "count", count, "=", 2*ndays, 0, ok );

      }




   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfrr_c */

