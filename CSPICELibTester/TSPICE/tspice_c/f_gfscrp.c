/* f_gfscrp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__200 = 200;
static integer c__3 = 3;
static integer c__20 = 20;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__20000 = 20000;
static doublereal c_b111 = 0.;
static doublereal c_b142 = 1e-12;
static integer c__2 = 2;
static doublereal c_b674 = .5;

/* $Procedure F_GFSCRP ( Test GF sub-observer coord progress reporting ) */
/* Subroutine */ int f_gfscrp__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static integer nivl;
    extern /* Subroutine */ int t_gfrepf__();
    static doublereal step;
    extern /* Subroutine */ int t_gfrepi__();
    extern /* Subroutine */ int t_gfrini__(integer *, integer *, integer *, 
	    integer *, doublereal *, char *, ftnlen);
    static doublereal work[3090]	/* was [206][15] */;
    extern /* Subroutine */ int t_gfuini__(void);
    extern /* Subroutine */ int t_gfrepu__();
    extern /* Subroutine */ int t_gfrplo__(integer *, integer *, integer *, 
	    doublereal *), t_gfrtrm__(integer *, integer *, integer *);
    static integer i__, j;
    extern integer cardd_(doublereal *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), copyd_(doublereal *, doublereal *);
    static integer npass;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckai_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), str2et_(char *, doublereal *, ftnlen);
    extern logical gfbail_();
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksc_(
	    char *, char *, char *, char *, logical *, ftnlen, ftnlen, ftnlen,
	     ftnlen);
    static char op[80];
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal cnfine[206];
    extern /* Subroutine */ int gfrefn_();
    static doublereal cnflog[4120]	/* was [206][20] */, cnfmes;
    extern /* Subroutine */ int gfstep_();
    static char gquant[80], msglog[78*2*20], qcpars[80*20], qpnams[80*20], 
	    xprefx[78];
    static doublereal adjust, centrl, et0, et1, expcnf[206], measur, qdpars[
	    20], refval, replog[60000]	/* was [3][20000] */, result[206], 
	    xmesur;
    static integer isqlog[20], mw, ncalls, nupdat, nw, qipars[20], qnpars, 
	    seqlog[20000], totcal, trmlog[20000], xsqlog[20000];
    static logical qlpars[20];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen), ssized_(integer *,
	     doublereal *), wninsd_(doublereal *, doublereal *, doublereal *),
	     gfsstp_(doublereal *), gfevnt_(U_fp, U_fp, char *, integer *, 
	    char *, char *, doublereal *, integer *, logical *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *,
	     U_fp, U_fp, U_fp, integer *, integer *, doublereal *, logical *, 
	    L_fp, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), chcksi_(char 
	    *, integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), scardd_(integer *, doublereal *), wnexpd_(doublereal *, 
	    doublereal *, doublereal *), spkuef_(integer *), delfil_(char *, 
	    ftnlen);
    static doublereal tol;
    static logical rpt;

/* $ Abstract */

/*     Test the GF subsystem's use of passed-in progress reporting and */
/*     interrupt handling functions for the "sub-observer point */
/*     coordinate" quantity. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF subsystem's use of the default progress */
/*     reporting functions in the context of searches involving */
/*     sub-observer point coordinate constraints. The point is to test */
/*     the callers of the functions, not the functions themselves. */

/*     The routines exercised by this test family are */

/*        GFEVNT */
/*        ZZGFREL */
/*        ZZGFSOLV */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 08-MAR-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Maximum progress report prefix or suffix length. */
/*     MXMSG is declared in zzgf.inc. */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFSCRP", (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK file. */

    tstpck_("gfscrp.tpc", &c_true, &c_false, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load an SPK file as well. */

    tstspk_("gfscrp.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate LOCMIN search using GFEVNT.", 
	    (ftnlen)63);
    ssized_(&c__200, cnfine);
    ssized_(&c__200, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)302)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create a confinement window with 3 intervals. We pick */
/*     as the central epoch of the middle interval a time */
/*     when a local maximum occurs. */


/*     Note: the epoch below was derived using a DE ephemeris and */
/*     probably doesn't match the value that would be obtained */
/*     using the test SPK. */

    str2et_("2000 JUN 21 01:27:06.528438 TDB", &centrl, (ftnlen)31);
    nivl = 3;
    cnfmes = 0.;
    i__1 = nivl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et0 = (i__ - 2) * 3600. - 800. + centrl;
	et1 = et0 + 1600.;
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	cnfmes = cnfmes + et1 - et0;
    }
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    qnpars = 10;
    s_copy(qpnams, "TARGET", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 80, "OBSERVER", (ftnlen)80, (ftnlen)8);
    s_copy(qpnams + 160, "ABCORR", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 240, "COORDINATE SYSTEM", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 320, "COORDINATE", (ftnlen)80, (ftnlen)10);
    s_copy(qpnams + 400, "REFERENCE FRAME", (ftnlen)80, (ftnlen)15);
    s_copy(qpnams + 480, "VECTOR DEFINITION", (ftnlen)80, (ftnlen)17);
    s_copy(qpnams + 560, "METHOD", (ftnlen)80, (ftnlen)6);
    s_copy(qpnams + 640, "DREF", (ftnlen)80, (ftnlen)4);
    s_copy(qpnams + 720, "DVEC", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(qcpars + 80, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(qcpars + 160, "NONE", (ftnlen)80, (ftnlen)4);
    s_copy(qcpars + 240, "LATITUDINAL", (ftnlen)80, (ftnlen)11);
    s_copy(qcpars + 320, "LATITUDE", (ftnlen)80, (ftnlen)8);
    s_copy(qcpars + 400, "IAU_EARTH", (ftnlen)80, (ftnlen)9);
    s_copy(qcpars + 480, "SUB-OBSERVER POINT", (ftnlen)80, (ftnlen)18);
    s_copy(qcpars + 560, "NEAR POINT: ELLIPSOID", (ftnlen)80, (ftnlen)21);
    s_copy(qcpars + 640, " ", (ftnlen)80, (ftnlen)1);
    cleard_(&c__3, qdpars);
    mw = 200;
    nw = 15;
    s_copy(op, "LOCMIN", (ftnlen)80, (ftnlen)6);
    refval = 0.;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity local extrema searches (excluding longitude, RA, */
/*     etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there was 1 progress reporting pass. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 1", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)421)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)423)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity extrema searches require 1 pass, */
/*     so we expect one report termination call. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)460)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)460)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)463)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)463)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)471)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)496)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)496)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)497)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)497)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)501)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)501)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)509)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)509)], &c_b111, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)513)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)513)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate ABSMIN search using GFEVNT.", 
	    (ftnlen)63);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     Initialize inputs for the search. We'll keep the */
/*     previous confinement window. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)557)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity absolute extrema searches (excluding longitude, */
/*     RA, etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there was 1 progress reporting pass. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 1", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)611)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)613)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity extrema searches require 1 pass, */
/*     so we expect one report termination call. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)650)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)650)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)653)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)653)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)661)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)686)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)686)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)687)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)687)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)691)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)691)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)699)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)699)], &c_b111, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)703)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)703)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate LOCMAX search using GFEVNT.", 
	    (ftnlen)63);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "LOCMAX", (ftnlen)80, (ftnlen)6);

/*     This search will produce an empty result window, but that */
/*     has no bearing on progress reporting. */

/*     Initialize inputs for the search. We'll keep the */
/*     previous confinement window. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)749)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity local extremum searches (excluding longitude, RA, */
/*     etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there was 1 progress reporting pass. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 1", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)803)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)805)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity extrema searches require 1 pass, */
/*     so we expect one report termination call. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)842)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)842)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)845)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)845)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)853)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)878)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)878)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)879)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)879)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)883)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)883)])
		     {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)891)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 1)
			 < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)891)], &c_b111, ok, (ftnlen)80, 
			(ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)895)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)895)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate ABSMAX search using GFEVNT.", 
	    (ftnlen)63);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     This search will produce an empty result window, but that */
/*     has no bearing on progress reporting. */

/*     Initialize inputs for the search. We'll keep the */
/*     previous confinement window. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)941)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity absolute extrema searches (excluding longitude, */
/*     RA, etc.) require one pass. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there was 1 progress reporting pass. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 1", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)995)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)997)) * 
		78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity extrema searches require 1 pass, */
/*     so we expect one report termination call. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__1, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)1034)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)1034)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)1037)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)1037)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)1045)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1070)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1070)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1071)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1071)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1075)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1075)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1083)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)1083)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1087)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)1087)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. */

    chcksd_("MEASUR", &measur, "~", &cnfmes, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate > inequality search using GF"
	    "EVNT.", (ftnlen)69);

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)1127)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    mw = 200;
    nw = 15;
    s_copy(op, ">", (ftnlen)80, (ftnlen)1);
    refval = 3.8e5;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity inequality searches (excluding longitude, RA, */
/*     etc.) require 2 passes. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 2 initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1192)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1194)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sub-observer point coordinate inequality searches require 2 */
/*     passes. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

/*         WRITE (*,*) I, ISQLOG(I), TRMLOG(I) */
	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)1232)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)1232)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)1235)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)1235)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)1243)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1268)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1268)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1269)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1269)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1273)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1273)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1281)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)1281)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1285)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)1285)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. Since the first pass covers the */
/*     confinement window, and the second and third partial passes */
/*     also combine to cover the confinement window, the total */
/*     progress measure should be twice that of the confinement */
/*     window. */

    xmesur = cnfmes * 2;
    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate < inequality search using GF"
	    "EVNT.", (ftnlen)69);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "<", (ftnlen)80, (ftnlen)1);

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)1334)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    mw = 200;
    nw = 15;
    refval = 3.8e5;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity inequality searches (excluding longitude, RA, */
/*     etc.) require 2 passes. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 2 initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1403)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1405)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sub-observer point coordinate inequality searches require 2 */
/*     passes. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

/*         WRITE (*,*) I, ISQLOG(I), TRMLOG(I) */
	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)1443)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)1443)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)1446)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)1446)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)1454)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1479)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1479)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1480)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1480)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1484)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1484)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1492)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)1492)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1496)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)1496)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. Since the first pass covers the */
/*     confinement window, and the second and third partial passes */
/*     also combine to cover the confinement window, the total */
/*     progress measure should be twice that of the confinement */
/*     window. */

    xmesur = cnfmes * 2;
    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate equality search using GFEVNT."
	    , (ftnlen)65);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator and the confinement window. */

    s_copy(op, "=", (ftnlen)80, (ftnlen)1);

/*     The confinement window actually used by ZZGFREL in an */
/*     equality search is obtained by expanding the input */
/*     confinement window by 1 second. */

    ssized_(&c__200, expcnf);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)1553)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    copyd_(cnfine, expcnf);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wnexpd_(&c_b674, &c_b674, expcnf);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    mw = 200;
    nw = 15;
    refval = 3.8e5;
    adjust = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity equality searches (excluding longitude, RA, */
/*     etc.) require 2 passes. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 2 initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1628)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1630)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     sub-observer point coordinate equality searches require 2 passes. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

/*         WRITE (*,*) I, ISQLOG(I), TRMLOG(I) */
	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)1667)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)1667)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)1670)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)1670)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)1678)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1703)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1703)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1704)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1704)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1708)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1708)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1716)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)1716)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1720)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)1720)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. Since the first pass covers the */
/*     confinement window, and the second and third partial passes */
/*     also combine to cover the confinement window, the total */
/*     progress measure should be twice that of the expanded */
/*     confinement window. */

    xmesur = 0.;
    i__1 = cardd_(expcnf);
    for (i__ = 2; i__ <= i__1; i__ += 2) {
	xmesur = xmesur + expcnf[(i__2 = i__ + 5) < 206 && 0 <= i__2 ? i__2 : 
		s_rnge("expcnf", i__2, "f_gfscrp__", (ftnlen)1740)] - expcnf[(
		i__3 = i__ + 4) < 206 && 0 <= i__3 ? i__3 : s_rnge("expcnf", 
		i__3, "f_gfscrp__", (ftnlen)1740)];
    }
    xmesur *= 2;
    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate adjusted ABSMIN search using"
	    " GFEVNT.", (ftnlen)72);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "ABSMIN", (ftnlen)80, (ftnlen)6);
    adjust = 1.;

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)1783)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    mw = 200;
    nw = 15;
    refval = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity adjusted absolute extrema searches (excluding */
/*     longitude, RA, etc.) require 2 passes. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 2 initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1848)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)1850)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     sub-observer point coordinate adjusted absolute extrema searches */
/*     require 2 */
/*     passes. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

/*         WRITE (*,*) I, ISQLOG(I), TRMLOG(I) */
	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)1889)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)1889)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)1892)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)1892)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)1900)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1925)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1925)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1926)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1926)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)1930)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)1930)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1938)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)1938)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)1942)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)1942)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. Since the first pass covers the */
/*     confinement window, and the second and third partial passes */
/*     also combine to cover the confinement window, the total */
/*     progress measure should be twice that of the confinement */
/*     window. */

    xmesur = cnfmes * 2;
    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Run a sub-observer point coordinate adjusted ABSMAX search using"
	    " GFEVNT.", (ftnlen)72);

/*     We'll use all inputs from the previous case except for the */
/*     relational operator. */

    s_copy(op, "ABSMAX", (ftnlen)80, (ftnlen)6);
    adjust = 1.;

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);

/*     Initialize the progress reporting test utility package. */

    t_gfuini__();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the confinement window log. */

    for (i__ = 1; i__ <= 20; ++i__) {
	ssized_(&c__200, &cnflog[(i__1 = i__ * 206 - 206) < 4120 && 0 <= i__1 
		? i__1 : s_rnge("cnflog", i__1, "f_gfscrp__", (ftnlen)1994)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(gquant, "COORDINATE", (ftnlen)80, (ftnlen)10);
    mw = 200;
    nw = 15;
    refval = 0.;
    tol = 1e-6;
    rpt = TRUE_;
    bail = FALSE_;
    step = 300.;
    gfsstp_(&step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfevnt_((U_fp)gfstep_, (U_fp)gfrefn_, gquant, &qnpars, qpnams, qcpars, 
	    qdpars, qipars, qlpars, op, &refval, &tol, &adjust, cnfine, &rpt, 
	    (U_fp)t_gfrepi__, (U_fp)t_gfrepu__, (U_fp)t_gfrepf__, &mw, &nw, 
	    work, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 200;
    t_gfrini__(&c__20, &mw, &npass, isqlog, cnflog, msglog, (ftnlen)78);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scalar quantity adjusted absolute extrema searches (excluding */
/*     longitude, RA, etc.) require 2 passes. */

/*     Look up the log of the progress report initialization calls. We */
/*     expect that there were 2 initialization calls. */

    chcksi_("No. of T_GFREPI calls", &npass, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Check the sequence number of the first call. */

    chcksi_("SEQLOG(1)", isqlog, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Check the progress report message prefixes and suffixes. */

    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "Prefix *", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xprefx, "Coordinate pass * of 2", (ftnlen)78, (ftnlen)22);
	repmi_(xprefx, "*", &i__, xprefx, (ftnlen)78, (ftnlen)1, (ftnlen)78);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksc_(qname, msglog + ((i__2 = (i__ << 1) - 2) < 40 && 0 <= i__2 ? 
		i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)2059)) * 
		78, "=", xprefx, ok, (ftnlen)80, (ftnlen)78, (ftnlen)1, (
		ftnlen)78);
	chcksc_("Suffix", msglog + ((i__2 = (i__ << 1) - 1) < 40 && 0 <= i__2 
		? i__2 : s_rnge("msglog", i__2, "f_gfscrp__", (ftnlen)2061)) *
		 78, "=", "done.", ok, (ftnlen)6, (ftnlen)78, (ftnlen)1, (
		ftnlen)5);
    }

/*     Get the log of the report termination calls. */

    t_gfrtrm__(&c__20, &ncalls, trmlog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sub-observer point coordinate adjusted absolute extrema searches */
/*     require 2 passes. */

    chcksi_("No. of T_GFRTRM calls", &ncalls, "=", &c__2, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);

/*     Retrieve the log of calls made to the update routine. */

    t_gfrplo__(&c__20000, &nupdat, seqlog, replog);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the updates for each pass. */

    totcal = 0;
    i__1 = npass;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check the sequence numbers of the calls. They should */
/*        range from the sequence number of the last init */
/*        call +1 to the sequence number of the Ith termination */
/*        call -1. They should be in increasing */
/*        order. */

/*         WRITE (*,*) I, ISQLOG(I), TRMLOG(I) */
	ncalls = trmlog[(i__2 = i__ - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		"trmlog", i__2, "f_gfscrp__", (ftnlen)2099)] - isqlog[(i__3 = 
		i__ - 1) < 20 && 0 <= i__3 ? i__3 : s_rnge("isqlog", i__3, 
		"f_gfscrp__", (ftnlen)2099)] - 1;
	i__2 = ncalls;
	for (j = 1; j <= i__2; ++j) {
	    xsqlog[(i__3 = j - 1) < 20000 && 0 <= i__3 ? i__3 : s_rnge("xsql"
		    "og", i__3, "f_gfscrp__", (ftnlen)2102)] = isqlog[(i__4 = 
		    i__ - 1) < 20 && 0 <= i__4 ? i__4 : s_rnge("isqlog", i__4,
		     "f_gfscrp__", (ftnlen)2102)] + j;
	}
	s_copy(qname, "Update SEQLOG, pass *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(qname, &seqlog[(i__2 = totcal) < 20000 && 0 <= i__2 ? i__2 : 
		s_rnge("seqlog", i__2, "f_gfscrp__", (ftnlen)2110)], "=", 
		xsqlog, &ncalls, ok, (ftnlen)80, (ftnlen)1);
	totcal += ncalls;
    }

/*     Make sure that: */

/*       - Each update time lies within the corresponding interval. */

/*       - Within each interval, the update times are monotonically */
/*         non-decreasing. */

/*     Also record the measure of the reported progress. */

    measur = 0.;
    i__1 = nupdat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(qname, "(0) Update time no. *", (ftnlen)80, (ftnlen)21);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)2135)], 
		">=", &replog[(i__3 = i__ * 3 - 3) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)2135)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= i__2 ? 
		i__2 : s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)2136)], 
		"<=", &replog[(i__3 = i__ * 3 - 2) < 60000 && 0 <= i__3 ? 
		i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)2136)], &
		c_b111, ok, (ftnlen)80, (ftnlen)2);
	if (i__ > 1) {
	    if (replog[(i__2 = i__ * 3 - 3) < 60000 && 0 <= i__2 ? i__2 : 
		    s_rnge("replog", i__2, "f_gfscrp__", (ftnlen)2140)] == 
		    replog[(i__3 = (i__ - 1) * 3 - 3) < 60000 && 0 <= i__3 ? 
		    i__3 : s_rnge("replog", i__3, "f_gfscrp__", (ftnlen)2140)]
		    ) {

/*              The current interval is the same as the previous one. */

		s_copy(qname, "(1) Update time no. *", (ftnlen)80, (ftnlen)21)
			;
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_(qname, &replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)2148)], ">=", &replog[(i__3 = (i__ - 1) * 3 - 
			1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("replog", 
			i__3, "f_gfscrp__", (ftnlen)2148)], &c_b111, ok, (
			ftnlen)80, (ftnlen)2);
		measur = measur + replog[(i__2 = i__ * 3 - 1) < 60000 && 0 <= 
			i__2 ? i__2 : s_rnge("replog", i__2, "f_gfscrp__", (
			ftnlen)2152)] - replog[(i__3 = (i__ - 1) * 3 - 1) < 
			60000 && 0 <= i__3 ? i__3 : s_rnge("replog", i__3, 
			"f_gfscrp__", (ftnlen)2152)];
	    }
	}
    }

/*     Compare the measure of the reported progress to that of the */
/*     confinement window. Since the first pass covers the */
/*     confinement window, and the second and third partial passes */
/*     also combine to cover the confinement window, the total */
/*     progress measure should be twice that of the confinement */
/*     window. */

    xmesur = cnfmes * 2;
    chcksd_("MEASUR", &measur, "~", &xmesur, &c_b142, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfscrp.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     To be added, if necessary. */


/* ---- Case ------------------------------------------------------------- */

    t_success__(ok);
    return 0;
} /* f_gfscrp__ */

