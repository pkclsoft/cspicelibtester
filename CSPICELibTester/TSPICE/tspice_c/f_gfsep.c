/* f_gfsep.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__10000 = 10000;
static doublereal c_b52 = 0.;
static integer c__5 = 5;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__0 = 0;
static doublereal c_b277 = 1e-8;
static integer c__2 = 2;
static integer c__1000 = 1000;
static integer c__2000 = 2000;
static integer c__10 = 10;
static doublereal c_b591 = 1e-4;
static doublereal c_b612 = 1e-6;

/* $Procedure F_GFSEP ( GFSEP family tests ) */
/* Subroutine */ int f_gfsep__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";
    static char shapes[80*2] = "POINT                                       "
	    "                                    " "SPHERE                   "
	    "                                                       ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double asin(doublereal);

    /* Local variables */
    static doublereal rada[3], radb[3], left, posa[3], posb[3], step;
    extern doublereal vsep_(doublereal *, doublereal *);
    static doublereal work[50030]	/* was [10006][5] */;
    static char targ1[80], targ2[80], time0[50], time1[50];
    static integer i__, j;
    extern /* Subroutine */ int zztstpck_(char *, ftnlen);
    static doublereal hanga, hangb;
    extern /* Subroutine */ int zztstlsk_(char *, ftnlen), tcase_(char *, 
	    ftnlen), gfsep_(char *, char *, char *, char *, char *, char *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen), repmc_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal right;
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *);
    static char frame1[80], frame2[80], shape1[80], shape2[80];
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int scardd_(integer *, doublereal *);
    static doublereal cnfine[10006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static char abcorr[80];
    extern /* Subroutine */ int kclear_(void), delfil_(char *, ftnlen);
    extern integer wncard_(doublereal *);
    static doublereal adjust, et0, et1, refval, result[10006];
    extern /* Subroutine */ int furnsh_(char *, ftnlen), natpck_(char *, 
	    logical *, logical *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *
	    , doublereal *, doublereal *), wnfetd_(doublereal *, integer *, 
	    doublereal *, doublereal *), spkpos_(char *, doublereal *, char *,
	     char *, char *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen), bodvrd_(char *, char *, integer *, integer *, 
	    doublereal *, ftnlen, ftnlen), gfstol_(doublereal *);
    static doublereal beg, end, pi___;
    static integer dim;
    extern doublereal spd_(void);
    static doublereal sep;
    static integer han1;
    extern /* Subroutine */ int zzgfspq_(doublereal *, integer *, integer *, 
	    doublereal *, doublereal *, integer *, char *, char *, doublereal 
	    *, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFSEP */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFSEP. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.3.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.2.0, 20-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.1.0, 07-JUN-2010 (EDW) */

/*        Minor edit to code comments eliminating typo. */

/*        Edits to code to improve structure and readability. */

/* -    TSPICE Version 1.0.0, 17-DEC-2008 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFSEP", (ftnlen)7);
    pi___ = pi_();
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);

/*     Create an LSK, load using FURNSH. */

    zztstlsk_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    zztstpck_("gfsep.pck", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsep.pck", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    s_copy(time1, "2000 APR 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__10000, cnfine);
    ssized_(&c__10000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*    Error cases */


/*   Case 1 */

    tcase_("Non-positive step size", (ftnlen)22);
    step = 0.;
    adjust = 0.;
    s_copy(targ1, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    s_copy(targ2, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*   Case 2 */

    tcase_("Non unique body IDs.", (ftnlen)20);
    step = spd_();
    s_copy(targ1, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "MOON", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)4, (ftnlen)1);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "EARTH", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)5, (ftnlen)1);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*   Case 3 */

    tcase_("Invalid aberration correction specifier", (ftnlen)39);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, "X", "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)1, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*   Case 4 */

    tcase_("Invalid relations operator", (ftnlen)26);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "==", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*   Case 5 */

    tcase_("Invalid body names", (ftnlen)18);
    s_copy(targ1, "X", (ftnlen)80, (ftnlen)1);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MOON", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "ABSMAX"
	    , &c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, 
	    (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "X", (ftnlen)80, (ftnlen)1);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "MOON", "ABSM"
	    "AX", &c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, 
	    result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)
	    80, (ftnlen)80, (ftnlen)80, (ftnlen)4, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "X", "ABSMAX", 
	    &c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)1, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/*     Case 6 */

    tcase_("Negative adjustment value", (ftnlen)25);
    adjust = -1.;
    s_copy(targ1, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "ABSMAX"
	    , &c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, 
	    (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 7 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    adjust = 0.;

/*     A vehicle/probe has a point shape so frame name not processed. */

    s_copy(targ1, "DAWN", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "MARS", (ftnlen)80, (ftnlen)4);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "IAU_MARS", (ftnlen)80, (ftnlen)8);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "ABSMAX"
	    , &c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, 
	    (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*   Case 8 */

    tcase_("Unknown shape name.", (ftnlen)19);
    step = spd_();
    s_copy(targ1, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "PANCAKE", (ftnlen)80, (ftnlen)7);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);
    s_copy(targ1, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape2, "BLOB", (ftnlen)80, (ftnlen)4);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__10000, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*   Case 9 */

    tcase_("Invalid value for MW", (ftnlen)20);
    step = spd_();
    s_copy(targ1, "MOON", (ftnlen)80, (ftnlen)4);
    s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame1, "NULL", (ftnlen)80, (ftnlen)4);
    s_copy(targ2, "EARTH", (ftnlen)80, (ftnlen)5);
    s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
    s_copy(frame2, "NULL", (ftnlen)80, (ftnlen)4);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__1, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, corr, "SUN", "=", &
	    c_b52, &adjust, &step, cnfine, &c__3, &c__5, work, result, (
	    ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 10 */

    tcase_("ABSMIN", (ftnlen)6);

/*     Perform a simple search using ALPHA and BETA for times */
/*     of minimum angular separation as seen from the sun. */
/*     Recall ALPHA - BETA occultation occurs every day */
/*     at 12:00 PM TDB. */

    step = 60.;
    adjust = 0.;
    refval = 0.;

/*     Store the time bounds of our search interval in */
/*     the CNFINE confinement window. */

    str2et_("2000 JAN 01 TDB", &et0, (ftnlen)15);
    str2et_("2000 JAN 02 TDB", &et1, (ftnlen)15);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    s_copy(frame1, "ALPHAFIXED", (ftnlen)80, (ftnlen)10);
    s_copy(frame2, "BETAFIXED", (ftnlen)80, (ftnlen)9);
    for (i__ = 1; i__ <= 2; ++i__) {
	for (j = 1; j <= 2; ++j) {
	    gfsep_("ALPHA", shapes + ((i__1 = i__ - 1) < 2 && 0 <= i__1 ? 
		    i__1 : s_rnge("shapes", i__1, "f_gfsep__", (ftnlen)627)) *
		     80, frame1, "BETA", shapes + ((i__2 = j - 1) < 2 && 0 <= 
		    i__2 ? i__2 : s_rnge("shapes", i__2, "f_gfsep__", (ftnlen)
		    627)) * 80, frame2, "NONE", "SUN", "ABSMIN", &refval, &
		    adjust, &step, cnfine, &c__10000, &c__5, work, result, (
		    ftnlen)5, (ftnlen)80, (ftnlen)80, (ftnlen)4, (ftnlen)80, (
		    ftnlen)80, (ftnlen)4, (ftnlen)3, (ftnlen)6);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the number of intervals in the result window. */

	    count = 0;
	    count = wncard_(result);
	    chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    if (count == 1) {
		wnfetd_(result, &c__1, &beg, &end);
		spkpos_("ALPHA", &beg, "J2000", "NONE", "SUN", posa, &lt, (
			ftnlen)5, (ftnlen)5, (ftnlen)4, (ftnlen)3);
		spkpos_("BETA", &beg, "J2000", "NONE", "SUN", posb, &lt, (
			ftnlen)4, (ftnlen)5, (ftnlen)4, (ftnlen)3);

/*              The angular separation should be within the MEDIUM */
/*              tolerance of 0.D0. */

		d__1 = vsep_(posa, posb);
		chcksd_("LOCMIN", &d__1, "~", &c_b52, &c_b277, ok, (ftnlen)6, 
			(ftnlen)1);
	    }
	}
    }
    bodvrd_("ALPHA", "RADII", &c__3, &dim, rada, (ftnlen)5, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);
    bodvrd_("BETA", "RADII", &c__3, &dim, radb, (ftnlen)4, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/*     Perform the test blocks for each aberration correction value. */

    s_copy(targ1, "ALPHA", (ftnlen)80, (ftnlen)5);
    s_copy(frame1, "ALPHAFIXED", (ftnlen)80, (ftnlen)10);
    s_copy(targ2, "BETA", (ftnlen)80, (ftnlen)4);
    s_copy(frame2, "BETAFIXED", (ftnlen)80, (ftnlen)9);
    for (j = 1; j <= 9; ++j) {
	s_copy(abcorr, corr + ((i__1 = j - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_gfsep__", (ftnlen)683)) * 80, (ftnlen)
		80, (ftnlen)80);

/*        Case 11 */

	repmc_("Relative =, sphere/sphere #", "#", abcorr, title, (ftnlen)27, 
		(ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);

/*        Perform a simple search using ALPHA and BETA for times */
/*        angular separation equal zero for the allowed body shapes. */

	refval = 0.;
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"=", &refval, &adjust, &step, cnfine, &c__10000, &c__5, work, 
		result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
		ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Two events should exist during the defined time interval. */
/*        We test one of the two. */

	if (count == 2) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));
	    hangb = asin(radb[0] / vnorm_(posb));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of the sum of body's half angles. */

	    d__1 = vsep_(posa, posb);
	    d__2 = hanga + hangb;
	    chcksd_(title, &d__1, "~", &d__2, &c_b277, ok, (ftnlen)80, (
		    ftnlen)1);
	}

/*     Case 12 */

	repmc_("Relative =, sphere/point #", "#", abcorr, title, (ftnlen)26, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "POINT", (ftnlen)80, (ftnlen)5);
	refval = 0.;
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"=", &refval, &adjust, &step, cnfine, &c__10000, &c__5, work, 
		result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
		ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Two events should exist during the defined time interval. */
/*        We test one of the two. */

	if (count == 2) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body ALPHA's half angle. */

	    d__1 = vsep_(posa, posb);
	    chcksd_(title, &d__1, "~", &hanga, &c_b277, ok, (ftnlen)80, (
		    ftnlen)1);
	}

/*        Case 13 */

	repmc_("Relative =, point/sphere #", "#", abcorr, title, (ftnlen)26, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
	refval = 0.;
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"=", &refval, &adjust, &step, cnfine, &c__10000, &c__5, work, 
		result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, (
		ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Two events should exist during the defined time interval. */
/*        We test one of the two. */

	if (count == 2) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hangb = asin(radb[0] / vnorm_(posb));

/*          The angular separation should be within the MEDIUM */
/*          tolerance of body BETA's half angle. */

	    d__1 = vsep_(posa, posb);
	    chcksd_(title, &d__1, "~", &hangb, &c_b277, ok, (ftnlen)80, (
		    ftnlen)1);
	}

/*        Case 14 */

	tcase_("Relative =, point/point", (ftnlen)23);
	s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
	s_copy(shape2, "POINT", (ftnlen)80, (ftnlen)5);
	refval = 0.;
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        One event should exist during the defined time interval */
/*        (as we search for a non-zero separation). */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of zero. */

	    d__1 = vsep_(posa, posb);
	    chcksd_("Relative =, point/point", &d__1, "~", &c_b52, &c_b277, 
		    ok, (ftnlen)23, (ftnlen)1);
	}

/*        Case 15 */

	repmc_("ABSMAX #, sphere/sphere", "#", abcorr, title, (ftnlen)23, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMAX", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        One events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));
	    hangb = asin(radb[0] / vnorm_(posb));
	    zzgfspq_(&beg, &c__1000, &c__2000, rada, radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of PI - HANGA - HANGB. */

	    d__1 = pi___ - (hanga + hangb);
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Case 16 */

	repmc_("ABSMAX #, sphere/point", "#", abcorr, title, (ftnlen)22, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "POINT", (ftnlen)80, (ftnlen)5);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMAX", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Once events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, rada, &c_b52, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance  of PI - HANGA. */

	    d__1 = pi___ - hanga;
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Case 17 */

	repmc_("ABSMAX #, point/sphere", "#", abcorr, title, (ftnlen)22, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMAX", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Once events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hangb = asin(radb[0] / vnorm_(posb));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, &c_b52, radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of PI - HANGB. */

	    d__1 = pi___ - hangb;
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Case 18 */

	repmc_("ABSMAX #, point/point", "#", abcorr, title, (ftnlen)21, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
	s_copy(shape2, "POINT", (ftnlen)80, (ftnlen)5);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMAX", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Once events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, &c_b52, &c_b52, &c__10, abcorr,
		     "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of PI. */

	    chcksd_(title, &pi___, "~", &sep, &c_b277, ok, (ftnlen)80, (
		    ftnlen)1);
	}

/*        Case 19 */

	repmc_("ABSMIN #, sphere/sphere", "#", abcorr, title, (ftnlen)23, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        One events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));
	    hangb = asin(radb[0] / vnorm_(posb));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, rada, radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of - HANGA - HANGB. */

	    d__1 = -(hanga + hangb);
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Case 20 */

	repmc_("ABSMIN #, sphere/point", "#", abcorr, title, (ftnlen)22, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "SPHERE", (ftnlen)80, (ftnlen)6);
	s_copy(shape2, "POINT", (ftnlen)80, (ftnlen)5);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        One events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ1, &beg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hanga = asin(rada[0] / vnorm_(posa));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, rada, &c_b52, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of -HANGA. */

	    d__1 = -hanga;
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        Case 21 */

	repmc_("ABSMIN #, point/sphere", "#", abcorr, title, (ftnlen)22, (
		ftnlen)1, (ftnlen)80, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(shape1, "POINT", (ftnlen)80, (ftnlen)5);
	s_copy(shape2, "SPHERE", (ftnlen)80, (ftnlen)6);
	gfsep_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, "SUN", 
		"ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, 
		work, result, (ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)80, 
		(ftnlen)80, (ftnlen)80, (ftnlen)80, (ftnlen)3, (ftnlen)6);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the number of intervals in the result window. */

	count = 0;
	count = wncard_(result);
	chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Once events should exist during the defined time interval. */

	if (count == 1) {
	    wnfetd_(result, &c__1, &beg, &end);
	    spkpos_(targ2, &beg, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)
		    80, (ftnlen)5, (ftnlen)80, (ftnlen)3);
	    hangb = asin(radb[0] / vnorm_(posb));

/*           The angular separation should be within the MEDIUM */
/*           tolerance of body BETA's half angle. */

	    zzgfspq_(&beg, &c__1000, &c__2000, &c_b52, radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)80, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           The angular separation should be within the MEDIUM */
/*           tolerance of - HANGB. */

	    d__1 = -hangb;
	    chcksd_(title, &d__1, "~", &sep, &c_b277, ok, (ftnlen)80, (ftnlen)
		    1);
	}
    }

/*     Case 22 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    step = 60.;
    adjust = 0.;
    refval = 0.;
    s_copy(frame1, "ALPHAFIXED", (ftnlen)80, (ftnlen)10);
    s_copy(frame2, "BETAFIXED", (ftnlen)80, (ftnlen)9);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsep_("ALPHA", shapes, frame1, "BETA", shapes, frame2, "NONE", "SUN", 
	    "ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, work,
	     result, (ftnlen)5, (ftnlen)80, (ftnlen)80, (ftnlen)4, (ftnlen)80,
	     (ftnlen)80, (ftnlen)4, (ftnlen)3, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b591);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsep_("ALPHA", shapes, frame1, "BETA", shapes, frame2, "NONE", "SUN", 
	    "ABSMIN", &refval, &adjust, &step, cnfine, &c__10000, &c__5, work,
	     result, (ftnlen)5, (ftnlen)80, (ftnlen)80, (ftnlen)4, (ftnlen)80,
	     (ftnlen)80, (ftnlen)4, (ftnlen)3, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b52, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b612);

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfsep.pck", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfsep__ */

