/*

-Procedure f_gfsep_c ( Test gfsep_c )

-Abstract
 
   Perform tests on the CSPICE wrapper gfsep_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfsep_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfsep_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)
 
-Version

   -tspice_c Version 1.2.0, 09-FEB-2017 (EDW) (NJB)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.

       Removed unneeded declarations.       
       
   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 23-DEC-2008 (NJB)(EDW)

-Index_Entries

   test gfsep_c

-&
*/

{ /* Begin f_gfsep_c */

   void zzgfspq(  SpiceDouble      et, 
                  SpiceInt         of1, 
                  SpiceInt         of2, 
                  SpiceDouble      r1, 
                  SpiceDouble      r2, 
                  SpiceInt         from, 
                  SpiceChar      * abcorr, 
                  SpiceChar      * ref, 
                  SpiceDouble    * value );


   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Constants
   */
   #define SPK             "gfsep.bsp"
   #define PCK             "gfsep.pck"
   #define LSK             "gfsep.tls"
   #define SPK1            "nat.bsp" 
   #define PCK1            "nat.pck" 
   #define MEDIUM          1.e-8
   #define LNSIZE          80 
   #define MAXWIN          10000 
   #define NCORR           9 
 
   SpiceInt                han1;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                count;
   SpiceInt                dim;

   SpiceChar             * abcorr;
   SpiceChar               title [ LNSIZE ];
   SpiceChar             * CORR  [] = { "NONE", 
                                        "lt", 
                                        " lt+s",
                                        " cn",
                                        " cn + s",
                                        "XLT",  
                                        "XLT + S", 
                                        "XCN", 
                                        "XCN+S" 
                                        };
   SpiceChar             * SHAPES[] = { "POINT", 
                                        "SPHERE" };

   SpiceDouble             PI_ = pi_c();
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             step;
   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             beg;
   SpiceDouble             end;
   SpiceDouble             lt;
   SpiceDouble             hanga;
   SpiceDouble             hangb;
   SpiceDouble             sep;
   SpiceDouble             posa[3];
   SpiceDouble             posb[3];
   SpiceDouble             rada[3];
   SpiceDouble             radb[3];
   
   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfsep_c" );


   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );

   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   natpck_( PCK1, (logical *) &false_, 
                  (logical *) &true_, 
                  (ftnlen) strlen(PCK) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create an SPK, load using FURNSH.
   */
   natspk_( SPK1, (logical *) &false_, 
                  &han1, 
                  (ftnlen) strlen(SPK1) ); 
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create a confinement window from ET0 and ET1.
   */

   str2et_c ( "2000 JAN 1  00:00:00 TDB", &et0 );
   str2et_c ( "2000 APR 1  00:00:00 TDB", &et1 );

   scard_c( 0, &cnfine);

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Error cases
   */

   /*
   Case 1
   */
   tcase_c ( "Non-positive step size." );

   step   = 0.;
   adjust = 0.;
   gfsep_c ( "MOON",  "SPHERE", "NULL",
             "EARTH", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok);


   /*
   Case 2
   */
   tcase_c ( "Non unique body IDs." );

   step   = spd_c();

   gfsep_c ( "MOON", "SPHERE", "NULL",
             "MOON", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "MOON",  "SPHERE", "NULL",
              CORR[0], 
             "MOON",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   gfsep_c ( "EARTH",  "SPHERE", "NULL",
             "MOON",   "SPHERE", "NULL",
              CORR[0], 
             "EARTH",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   /*
   Case 3
   */
   tcase_c ( "Invalid aberration correction specifier." );

   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "MOON",  "SPHERE", "NULL",
             "X", 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok);


   /*
   Case 4
   */
   tcase_c ( "Invalid relations operator." );

   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "MOON",  "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "==",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);
   
   
   /*
   Case 5
   */

   tcase_c ( "Invalid body names." );

   gfsep_c ( "X",    "SPHERE", "NULL",
             "MOON", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "X",     "SPHERE", "NULL",
              CORR[0], 
             "MOON",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "MOON",  "SPHERE", "NULL",
              CORR[0], 
             "X",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);


   /*
   Case 6
   */

   tcase_c ( "Negative adjustment value." );

   adjust = -1.;
   
   gfsep_c ( "EARTH", "SPHERE", "NULL",
             "MOON",  "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);


   /*
   Case 7
   */

   tcase_c ( "Ephemeris data unavailable." );

   adjust = 0.;

   gfsep_c ( "DAWN", "POINT", "NULL",
             "MARS", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok);


   /*
   Case 8
   */

   tcase_c ( "Unknown shape name." );

   gfsep_c ( "MOON",  "PANCAKE", "NULL", 
             "EARTH", "SPHERE",  "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);


   gfsep_c ( "MOON",  "SPHERE", "NULL",
             "EARTH", "BLOB",   "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);



   /*
   Case 9
   */

   tcase_c ( "Invalid value for nintvls." );

   gfsep_c ( "MOON",  "SPHERE", "NULL",
             "EARTH", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              0,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);


   gfsep_c ( "MOON",  "SPHERE", "NULL",
             "EARTH", "SPHERE", "NULL",
              CORR[0], 
             "SUN",   
             "=",
              0.,     
              adjust,
              step,
              -1,
              &cnfine,
              &result );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);


   /*
   Case 10
   */

   tcase_c ( "ABSMIN" );

   /*
   Perform a simple search using ALPHA and BETA for times
   of minimum angular separation as seen from the sun.
   Recall ALPHA - BETA occultation occurs every day 
   at 12:00 PM TDB.
   */

   step   = 60.;
   adjust = 0.;
   refval = 0.;

   /*
   Store the time bounds of our search interval in
   the CNFINE confinement window.
   */
   str2et_c ( "2000 JAN 01 00:00:00 TDB", &et0 );
   str2et_c ( "2000 JAN 02 00:00:00 TDB", &et1 );
   
   scard_c  ( 0, &cnfine );
   
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i=0; i<2; i++ )
      {

      for( j=0; j<2; j++)
         {

         gfsep_c ( "ALPHA", SHAPES[i], "NULL",
                   "BETA",  SHAPES[j], "NULL",
                   "NONE",
                   "SUN",
                   "ABSMIN",
                   refval,     
                   adjust,
                   step,
                   MAXWIN,
                   &cnfine,
                   &result );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Check the number of intervals in the result window.
         */

         count = 0;
         count = wncard_c( &result);
         chcksi_c ( "COUNT", count, "=", 1, 0, ok );
         
         if( count ==1 )
            {
            wnfetd_c( &result, 0, &beg, &end );
            
            spkpos_c( "alpha", beg, "J2000", "NONE", "SUN", posa, &lt);
            spkpos_c( "beta",  beg, "J2000", "NONE", "SUN", posb, &lt);

            /*
            The angular separation should be within the MEDIUM
            tolerance of 0.D0.
            */  
            chcksd_c ( "ABSMIN", vsep_c(posa, posb), "~", 0., MEDIUM, ok );

            }

         }
      
      }

   bodvrd_c( "alpha", "RADII", 3, &dim, rada );
   chckxc_c ( SPICEFALSE, " ", ok );

   bodvrd_c( "beta",  "RADII", 3, &dim, radb );
   chckxc_c ( SPICEFALSE, " ", ok );


   for ( j=0; j<NCORR; j++ )
      {
      
      abcorr = CORR[j];

      /*
      Case 11
      */
      repmc_c( "Relative =, sphere/sphere #", "#", abcorr, LNSIZE, title );
      tcase_c( title);

      /*
      Perform a simple search using ALPHA and BETA for times
      angular separation equal zero for the allowed body shapes.
      */
      
      refval = 0.;
      
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "=",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      */
      
      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 2, 0, ok );

      if( count == 2 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );
         hangb = asin( radb[0]/vnorm_c(posb) );

         /*
         The angular separation should be within the MEDIUM
         tolerance of the sum of body's half angles.
         */
         chcksd_c ( title, vsep_c(posa, posb), "~", hanga + hangb, MEDIUM, ok );
         }


      /*
      Case 12
      */
      repmc_c( "Relative =, sphere/point #", "#", abcorr, LNSIZE, title );
      tcase_c( title);

      /*
      Perform a simple search using ALPHA and BETA for times
      angular separation equal zero for the allowed body shapes.
      */
      
      refval = 0.;
      
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "POINT", "NULL",
                 abcorr,
                "SUN",
                "=",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      */
      
      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 2, 0, ok );

      if( count == 2 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );

         /*
         The angular separation should be within the MEDIUM
         tolerance of body ALPHA's half angle.
         */
         chcksd_c ( title, vsep_c(posa, posb), "~", hanga, MEDIUM, ok );
         }


      /*
      Case 13
      */
      repmc_c( "Relative =, point/sphere #", "#", abcorr, LNSIZE, title );
      tcase_c( title);

      /*
      Perform a simple search using ALPHA and BETA for times
      angular separation equal zero for the allowed body shapes.
      */
      
      refval = 0.;
      
      gfsep_c ( "ALPHA", "POINT", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "=",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      */
      
      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 2, 0, ok );

      if( count == 2 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hangb = asin( radb[0]/vnorm_c(posb) );

         /*
         The angular separation should be within the MEDIUM
         tolerance of body BETA's half angle.
         */
         chcksd_c ( title, vsep_c(posa, posb), "~", hangb, MEDIUM, ok );
         }



      /*
      Case 14
      */
      tcase_c( "Relative ABSMIN point/point" );

      /*
      Perform a simple search using ALPHA and BETA for times
      angular separation equals zero (absolute minimum) for 
      the allowed body shapes.
      */
      
      refval = 0.;

      gfsep_c ( "ALPHA", "POINT", "NULL",
                "BETA",  "POINT", "NULL",
                 abcorr,
                "SUN",
                "ABSMIN",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check the number of intervals in the result window.
      */
      
      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         /*
         The angular separation should be within the MEDIUM
         tolerance of zero.
         */
         chcksd_c ( "Relative =, point/point", vsep_c(posa, posb), 
                                               "~", 
                                               refval, MEDIUM, ok );
         }


      /*
      Case 15
      */
      repmc_c( "ABSMAX #, sphere/sphere", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "ABSMAX",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );
         hangb = asin( radb[0]/vnorm_c(posb) );
 
         (void) zzgfspq( beg, 1000, 2000, rada[0], radb[0], 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of PI - HANGA - HANGB.
         */
         chcksd_c ( title,  PI_ - (hanga+hangb), "~", sep, MEDIUM, ok ); 
         }


      /*
      Case 16
      */
      repmc_c( "ABSMAX #, sphere/point", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "POINT", "NULL",
                 abcorr,
                "SUN",
                "ABSMAX",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );
 
         (void) zzgfspq( beg, 1000, 2000, rada[0], 0., 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of PI - HANGA.
         */
         chcksd_c ( title,  PI_ -hanga, "~", sep, MEDIUM, ok ); 
         }


      /*
      Case 17
      */
      repmc_c( "ABSMAX #, point/sphere", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "POINT", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "ABSMAX",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );

         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hangb = asin( radb[0]/vnorm_c(posb) );
 
         (void) zzgfspq( beg, 1000, 2000, 0., radb[0], 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of PI - HANGB.
         */
         chcksd_c ( title,  PI_ -hangb, "~", sep, MEDIUM, ok ); 
         }


      /*
      Case 18
      */
      repmc_c( "ABSMAX #, point/point", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "POINT", "NULL",
                "BETA",  "POINT", "NULL",
                 abcorr,
                "SUN",
                "ABSMAX",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );

         (void) zzgfspq( beg, 1000, 2000, 0., 0., 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of PI.         
         */
         chcksd_c ( title,  PI_ , "~", sep, MEDIUM, ok ); 
         }


      /*
      Case 19
      */
      repmc_c( "ABSMIN #, sphere/sphere", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "ABSMIN",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );
         hangb = asin( radb[0]/vnorm_c(posb) );
 
         (void) zzgfspq( beg, 1000, 2000, rada[0], radb[0], 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of - HANGA - HANGB.
         */
         chcksd_c ( title, - (hanga+hangb), "~", sep, MEDIUM, ok ); 
         }


      /*
      Case 20
      */
      repmc_c( "ABSMIN #, sphere/point", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "SPHERE", "NULL",
                "BETA",  "POINT", "NULL",
                 abcorr,
                "SUN",
                "ABSMIN",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );

         spkpos_c( "alpha", beg, "J2000", abcorr, "SUN", posa, &lt);

         hanga = asin( rada[0]/vnorm_c(posa) );
 
         (void) zzgfspq( beg, 1000, 2000, rada[0], 0., 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of -HANGA.
         */
         chcksd_c ( title, -hanga, "~", sep, MEDIUM, ok ); 
         }



      /*
      Case 21
      */
      repmc_c( "ABSMIN #, point/sphere", "#", abcorr, LNSIZE, title );
      tcase_c( title);
 
      gfsep_c ( "ALPHA", "POINT", "NULL",
                "BETA",  "SPHERE", "NULL",
                 abcorr,
                "SUN",
                "ABSMIN",
                 refval,     
                 adjust,
                 step,
                 MAXWIN,
                 &cnfine,
                 &result );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      One events should exist during the defined time interval.
      */

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 1, 0, ok );

      if( count == 1 )
         {
         wnfetd_c( &result, 0, &beg, &end );
            
         spkpos_c( "beta",  beg, "J2000", abcorr, "SUN", posb, &lt);

         hangb = asin( radb[0]/vnorm_c(posb) );
 
         (void) zzgfspq( beg, 1000, 2000, 0., radb[0], 10, 
                         abcorr, "J2000", &sep );

         /*
         The angular separation should be within the MEDIUM
         tolerance of - HANGB.
         */
         chcksd_c ( title, -hangb, "~", sep, MEDIUM, ok ); 
         }

      }




   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfsep_c */



/*
-Procedure zzgfspq ( Test implementation, separation quantity )

-Abstract

   A pure C implementation of the function that calculates
   angular separation quantity. Use this function only as
   part of the GF separation test family.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   None.

-Brief_I/O

   None.

-Detailed_Input

   None.

-Detailed_Output

   None.

-Parameters

   None.

-Exceptions

   None.

-Files

   None.

-Particulars

   None.

-Examples

   None.

-Restrictions

   This routine supports the GF separation event test family.
   Use only in that circumstance.

-Literature_References

   None.

-Author_and_Institution

   E.D. Wright     (JPL)

-Version

   -tspice_c Version 1.0.0 12-AUG-2008 (EDW) 

-Index_Entries

   None.

-&
*/
void zzgfspq(  SpiceDouble      et, 
               SpiceInt         of1, 
               SpiceInt         of2, 
               SpiceDouble      r1, 
               SpiceDouble      r2, 
               SpiceInt         from, 
               SpiceChar      * abcorr, 
               SpiceChar      * ref, 
               SpiceDouble    * value )
   {

   SpiceDouble       pv1[3];
   SpiceDouble       pv2[3];
   SpiceDouble       lt;
   SpiceDouble       range1;
   SpiceDouble       range2;
   SpiceDouble       ang1;
   SpiceDouble       ang2;
   SpiceDouble       theta;

   spkezp_c( of1, et, ref, abcorr, from, pv1, &lt);
   spkezp_c( of2, et, ref, abcorr, from, pv2, &lt);

   range1 = vnorm_c( pv1 );
   range2 = vnorm_c( pv2 );

   if ( range1 > r1 )
      {
      ang1 = asin( r1/range1 );
      }
   else
      {
      ang1 = halfpi_c();
      }

   if ( range2 > r2 )
      {
      ang2 = asin( r2/range2 );
      }
   else
      {
      ang2 = halfpi_c();
      }

   theta = vsep_c( pv1, pv2 );
   *value= theta - ang1 - ang2;

   }

