/* f_gfsntc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__20000 = 20000;
static integer c__15 = 15;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c__30 = 30;
static integer c__1 = 1;
static doublereal c_b210 = 1e-6;
static doublereal c_b314 = 1e-4;
static doublereal c_b329 = 0.;

/* $Procedure F_GFSNTC ( GFSNTC family tests ) */
/* Subroutine */ int f_gfsntc__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";
    static doublereal x[3] = { 1.,0.,0. };
    static doublereal y[3] = { 0.,1.,0. };
    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char dref[36];
    static doublereal left, step, work[300090]	/* was [20006][15] */;
    static char time0[80], time1[80];
    static integer i__, j;
    static char mdesc[80*22];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char coord[36];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static doublereal mrefs[22], right;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80], items[36*3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count;
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    static integer handle;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *), kclear_(void);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal timbeg[2];
    static char abcorr[80];
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen);
    static char relate[36];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    extern integer wncard_(doublereal *);
    static char method[36], target[36], fixref[36];
    static doublereal refval;
    static char obsrvr[36], crdsys[36];
    static doublereal et0, et1, adjust, result[20006], timend[2];
    extern /* Subroutine */ int tstlsk_(void), furnsh_(char *, ftnlen), 
	    natpck_(char *, logical *, logical *, ftnlen), tstspk_(char *, 
	    logical *, integer *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *
	    , doublereal *, doublereal *), gfsntc_(char *, char *, char *, 
	    char *, char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), bodvrd_(char *, 
	    char *, integer *, integer *, doublereal *, ftnlen, ftnlen), 
	    lparse_(char *, char *, integer *, integer *, char *, ftnlen, 
	    ftnlen, ftnlen), wnfetd_(doublereal *, integer *, doublereal *, 
	    doublereal *), gfstol_(doublereal *), delfil_(char *, ftnlen);
    static doublereal beg, end, rad[3];
    static integer dim;
    extern doublereal rpd_(void), spd_(void);
    static integer han1;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFSNTC */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFSNTC. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.2.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.1.0, 27-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.0.0, 12-FEB-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved all. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFSNTC", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    t_pck08__("gfsntc.pck", &c_false, &c_true, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsntc.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an SPK, load using FURNSH. */

    tstspk_("gfsntc.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsntc.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    s_copy(time1, "2000 JAN 2  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__20000, cnfine);
    ssized_(&c__20000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*    Error cases */


/*   Case 1 */

    tcase_("Non-positive step size", (ftnlen)22);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "IAU_EARTH", (ftnlen)36, (ftnlen)9);
    s_copy(method, "Ellipsoid", (ftnlen)36, (ftnlen)9);
    s_copy(abcorr, corr, (ftnlen)80, (ftnlen)80);
    s_copy(dref, "IAU_EARTH", (ftnlen)36, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "LATITUDE", (ftnlen)36, (ftnlen)8);
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    refval = 0.;
    adjust = 0.;
    step = 0.;
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*   Case 2 */

    tcase_("Non unique body IDs.", (ftnlen)20);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    step = 1.;
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*   Case 3 */

    tcase_("Invalid aberration correction specifier", (ftnlen)39);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(abcorr, "X", (ftnlen)80, (ftnlen)1);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*   Case 4 */

    tcase_("Invalid relations operator", (ftnlen)26);
    s_copy(abcorr, corr, (ftnlen)80, (ftnlen)80);
    s_copy(relate, "==", (ftnlen)36, (ftnlen)2);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*   Case 5 */

    tcase_("Invalid body names", (ftnlen)18);
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    s_copy(target, "X", (ftnlen)36, (ftnlen)1);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "X", (ftnlen)36, (ftnlen)1);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/*     Case 6 */

    tcase_("Negative adjustment value", (ftnlen)25);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    adjust = -1.;
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 7 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    adjust = 0.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "DAWN", (ftnlen)36, (ftnlen)4);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*     Case 8 */

    tcase_("Unknown coordinate system", (ftnlen)25);
    adjust = 0.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(crdsys, "X", (ftnlen)36, (ftnlen)1);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Case 9 */

    tcase_("Unknown coordinate.", (ftnlen)19);
    adjust = 0.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "X", (ftnlen)36, (ftnlen)1);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Case 10 */

    for (i__ = 1; i__ <= 2; ++i__) {
	if (i__ == 1) {
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)36, (ftnlen)14);
	} else {
	    s_copy(crdsys, "GEODETIC", (ftnlen)36, (ftnlen)8);
	}
	s_copy(title, "Unknown FIXREF frame for #", (ftnlen)80, (ftnlen)26);
	repmc_(title, "#", crdsys, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(coord, "ALTITUDE", (ftnlen)36, (ftnlen)8);
	s_copy(fixref, "X", (ftnlen)36, (ftnlen)1);
	gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, 
		coord, relate, &refval, &adjust, &step, cnfine, &c__20000, &
		c__15, work, result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)80, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);
    }

/*     Case 11 */

    tcase_("Invalid target frame", (ftnlen)20);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "LATITUDE", (ftnlen)36, (ftnlen)8);
    s_copy(fixref, "IAU_MARS", (ftnlen)36, (ftnlen)8);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/*     Define the coordinate test conditions. */

    bodvrd_("ALPHA", "RADII", &c__3, &dim, rad, (ftnlen)5, (ftnlen)5);

/*     Conditions: all conditions should occur once per */
/*     GAMMA orbit (delta_t = 24 hours). */

    s_copy(mdesc, "RECTANGULAR   : X           : <", (ftnlen)80, (ftnlen)31);
    s_copy(mdesc + 80, "RECTANGULAR   : Y           : <", (ftnlen)80, (ftnlen)
	    31);
    s_copy(mdesc + 160, "RECTANGULAR   : Z           : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 240, "RECTANGULAR   : X           : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 320, "RECTANGULAR   : Y           : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 400, "RECTANGULAR   : Z           : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 480, "LATITUDINAL   : RADIUS      : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 560, "LATITUDINAL   : LATITUDE    : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 640, "LATITUDINAL   : LATITUDE    : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 720, "RA/DEC        : RANGE       : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 800, "RA/DEC        : DECLINATION : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 880, "RA/DEC        : DECLINATION : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 960, "SPHERICAL     : RADIUS      : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1040, "SPHERICAL     : COLATITUDE  : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1120, "SPHERICAL     : COLATITUDE  : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1200, "CYLINDRICAL   : RADIUS      : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1280, "CYLINDRICAL   : Z           : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1360, "CYLINDRICAL   : Z           : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1440, "CYLINDRICAL   : LONGITUDE   : >", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1520, "SPHERICAL     : LONGITUDE   : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1600, "LATITUDINAL   : LONGITUDE   : <", (ftnlen)80, (
	    ftnlen)31);
    s_copy(mdesc + 1680, "RA/DEC        : RIGHT ASCENSION : >", (ftnlen)80, (
	    ftnlen)35);

/*     Test conditions reference values: */

    mrefs[0] = rad[0];
    mrefs[1] = rad[1];
    mrefs[2] = rad[2];
    mrefs[3] = -rad[0];
    mrefs[4] = -rad[1];
    mrefs[5] = -rad[2];
    mrefs[6] = 0.;
    mrefs[7] = rpd_() * -90.;
    mrefs[8] = rpd_() * 90.;
    mrefs[9] = 0.;
    mrefs[10] = rpd_() * -90.;
    mrefs[11] = rpd_() * 90.;
    mrefs[12] = 0.;
    mrefs[13] = 0.;
    mrefs[14] = rpd_() * 180.;
    mrefs[15] = 0.;
    mrefs[16] = rad[2];
    mrefs[17] = -rad[2];
    mrefs[18] = 0.;
    mrefs[19] = 0.;
    mrefs[20] = 0.;
    mrefs[21] = 0.;

/*     Time interval thirty days. */

    s_copy(time0, "2000 JAN 1   18:00:00 TDB", (ftnlen)80, (ftnlen)25);
    s_copy(time1, "2000 JAN 31  18:00:00 TDB", (ftnlen)80, (ftnlen)25);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "ALPHAFIXED", (ftnlen)36, (ftnlen)10);
    s_copy(method, "ELLIPSOID", (ftnlen)36, (ftnlen)9);
    s_copy(obsrvr, "GAMMA", (ftnlen)36, (ftnlen)5);
    s_copy(dref, "J2000", (ftnlen)36, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);

/*     Case 12, Sweeps of the GAMMA centered J2000 Z axis on ALPHA. */

    step = spd_() * .052083333333333336;
    for (i__ = 1; i__ <= 22; ++i__) {
	scardd_(&c__0, cnfine);
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = 37, a__1[0] = "MDESC(#) ALPHA sweeps from GAMMA, Z, ";
	i__2[1] = 80, a__1[1] = mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? 
		i__1 : s_rnge("mdesc", i__1, "f_gfsntc__", (ftnlen)637)) * 80;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Parse from the MDESC(I) string the coordinate system, */
/*        coordinate, and relation operator. */

	lparse_(mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mdesc", i__1, "f_gfsntc__", (ftnlen)646)) * 80, ":", &c__3, &
		dim, items, (ftnlen)80, (ftnlen)1, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
	s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
	s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
	refval = mrefs[(i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mrefs", i__1, "f_gfsntc__", (ftnlen)652)];
	gfsntc_(target, fixref, method, abcorr, obsrvr, dref, z__, crdsys, 
		coord, relate, &refval, &adjust, &step, cnfine, &c__20000, &
		c__15, work, result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)80, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = wncard_(result);
	chcksi_("WNCARD(RESULT)", &i__1, "=", &c__30, &c__0, ok, (ftnlen)14, (
		ftnlen)1);
	timbeg[0] = 0.;
	timend[0] = 0.;
	timbeg[1] = 0.;
	timend[1] = 0.;
	if (*ok) {
	    wnfetd_(result, &c__1, timbeg, timend);
	    i__1 = wncard_(result);
	    for (j = 2; j <= i__1; ++j) {
		wnfetd_(result, &j, &timbeg[1], &timend[1]);
		d__1 = timbeg[1] - timbeg[0];
		d__2 = spd_();
		chcksd_("Z SWEEP BEG", &d__1, "~", &d__2, &c_b210, ok, (
			ftnlen)11, (ftnlen)1);
		d__1 = timend[1] - timend[0];
		d__2 = spd_();
		chcksd_("Z SWEEP END", &d__1, "~", &d__2, &c_b210, ok, (
			ftnlen)11, (ftnlen)1);
		timbeg[0] = timbeg[1];
		timend[0] = timend[1];
	    }
	}
    }

/*     Case 13,  Sweeps of the GAMMA centered J2000 Y axis on ALPHA. */

    step = spd_() * .125;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 22; ++i__) {
/* Writing concatenation */
	i__2[0] = 37, a__1[0] = "MDESC(#) ALPHA sweeps from GAMMA, Y, ";
	i__2[1] = 80, a__1[1] = mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? 
		i__1 : s_rnge("mdesc", i__1, "f_gfsntc__", (ftnlen)711)) * 80;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Parse from the MDESC(I) string the coordinate system, */
/*        coordinate, and relation operator. */

	lparse_(mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mdesc", i__1, "f_gfsntc__", (ftnlen)720)) * 80, ":", &c__3, &
		dim, items, (ftnlen)80, (ftnlen)1, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
	s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
	s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
	refval = mrefs[(i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mrefs", i__1, "f_gfsntc__", (ftnlen)726)];
	gfsntc_(target, fixref, method, abcorr, obsrvr, dref, y, crdsys, 
		coord, relate, &refval, &adjust, &step, cnfine, &c__20000, &
		c__15, work, result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)80, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = wncard_(result);
	chcksi_("WNCARD(RESULT)", &i__1, "=", &c__30, &c__0, ok, (ftnlen)14, (
		ftnlen)1);
	timbeg[0] = 0.;
	timend[0] = 0.;
	timbeg[1] = 0.;
	timend[1] = 0.;
	if (*ok) {
	    wnfetd_(result, &c__1, timbeg, timend);
	    i__1 = wncard_(result);
	    for (j = 2; j <= i__1; ++j) {
		wnfetd_(result, &j, &timbeg[1], &timend[1]);
		d__1 = timbeg[1] - timbeg[0];
		d__2 = spd_();
		chcksd_("Y SWEEP BEG", &d__1, "~", &d__2, &c_b210, ok, (
			ftnlen)11, (ftnlen)1);
		d__1 = timend[1] - timend[0];
		d__2 = spd_();
		chcksd_("Y SWEEP END", &d__1, "~", &d__2, &c_b210, ok, (
			ftnlen)11, (ftnlen)1);
		timbeg[0] = timbeg[1];
		timend[0] = timend[1];
	    }
	}
    }

/*     Case 14, Sweeps of the GAMMA centered J2000 X axis on ALPHA. */

    step = spd_() * .052083333333333336;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 22; ++i__) {
/* Writing concatenation */
	i__2[0] = 37, a__1[0] = "MDESC(#) ALPHA sweeps from GAMMA, X, ";
	i__2[1] = 80, a__1[1] = mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? 
		i__1 : s_rnge("mdesc", i__1, "f_gfsntc__", (ftnlen)785)) * 80;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Parse from the MDESC(I) string the coordinate system, */
/*        coordinate, and relation operator. */

	lparse_(mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mdesc", i__1, "f_gfsntc__", (ftnlen)794)) * 80, ":", &c__3, &
		dim, items, (ftnlen)80, (ftnlen)1, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
	s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
	s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
	refval = mrefs[(i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge(
		"mrefs", i__1, "f_gfsntc__", (ftnlen)800)];
	gfsntc_(target, fixref, method, abcorr, obsrvr, dref, x, crdsys, 
		coord, relate, &refval, &adjust, &step, cnfine, &c__20000, &
		c__15, work, result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)80, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The J2000 X unit with origin at GAMMA's center should never */
/*        intersect ALPHA. */

	i__1 = wncard_(result);
	chcksi_("WNCARD(RESULT)", &i__1, "=", &c__0, &c__0, ok, (ftnlen)14, (
		ftnlen)1);
    }

/*     Case 15 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL. */

    i__ = 1;

/*     Parse from the MDESC(I) string the coordinate system, */
/*     coordinate, and relation operator. */

    s_copy(time0, "2000 JAN 1   18:00:00 TDB", (ftnlen)80, (ftnlen)25);
    s_copy(time1, "2000 JAN 31  18:00:00 TDB", (ftnlen)80, (ftnlen)25);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "ALPHAFIXED", (ftnlen)36, (ftnlen)10);
    s_copy(method, "ELLIPSOID", (ftnlen)36, (ftnlen)9);
    s_copy(obsrvr, "GAMMA", (ftnlen)36, (ftnlen)5);
    s_copy(dref, "J2000", (ftnlen)36, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    step = spd_() * .052083333333333336;
    i__ = 1;
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Parse from the MDESC(I) string the coordinate system, */
/*     coordinate, and relation operator. */

    lparse_(mdesc + ((i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge("mde"
	    "sc", i__1, "f_gfsntc__", (ftnlen)874)) * 80, ":", &c__3, &dim, 
	    items, (ftnlen)80, (ftnlen)1, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
    s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
    s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
    refval = mrefs[(i__1 = i__ - 1) < 22 && 0 <= i__1 ? i__1 : s_rnge("mrefs",
	     i__1, "f_gfsntc__", (ftnlen)880)];
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, z__, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b314);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsntc_(target, fixref, method, abcorr, obsrvr, dref, z__, crdsys, coord, 
	    relate, &refval, &adjust, &step, cnfine, &c__20000, &c__15, work, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)
	    36, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b329, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b210);

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfsntc.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfsntc.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfsntc__ */

