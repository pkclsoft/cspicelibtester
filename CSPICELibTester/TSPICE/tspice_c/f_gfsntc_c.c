/*

-Procedure f_gfsntc_c ( Test gfsntc_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfsntc_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_gfsntc_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfsntc_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)
 
-Version

   -tspice_c Version 1.2.0, 10-FEB-2017 (EDW) (NJB)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.
 
       Removed unneeded declarations.       

   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 25-JUL-2008 (NJB)(EDW)

-Index_Entries

   test gfsntc_c

-&
*/

{ /* Begin f_gfsntc_c */


   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   #define string_copy(src, dest)      strncpy( dest, src,  strlen(src) ); \
                                       dest[strlen(src)] = '\0';
                                       
   /*
   Constants
   */
   #define SPK             "gfsntc.bsp"
   #define PCK             "gfsntc.pck"
   #define LSK             "gfsntc.tls"
   #define SPK1            "nat.bsp" 
   #define PCK1            "nat.pck" 
   #define CNVTOL          1.e-6
   #define MEDTOL          1.e-4
   #define LNSIZE          80 
   #define MAXWIN          10000 
   #define NCORR           9 
 
   SpiceInt                handle;
   SpiceInt                han1;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                ntest;
   SpiceInt                dim;
   SpiceInt                count;

   SpiceChar             * abcorr;
   SpiceChar               title [LNSIZE*2];
   SpiceChar               target[LNSIZE];
   SpiceChar               obsrvr[LNSIZE];
   SpiceChar               crdsys[LNSIZE];
   SpiceChar               coord [LNSIZE];
   SpiceChar               relate[LNSIZE];
   SpiceChar               dref  [LNSIZE];
   SpiceChar               method[LNSIZE];
   SpiceChar               fixref[LNSIZE];

   SpiceChar             * CORR[]   = { "NONE", 
                                        "lt", 
                                        " lt+s",
                                        " cn",
                                        " cn + s",
                                        "XLT",  
                                        "XLT + S", 
                                        "XCN", 
                                        "XCN+S" 
                                        };

   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             step;
   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             rad[3];
   SpiceDouble             x[] = { 1., 0., 0. };
   SpiceDouble             y[] = { 0., 1., 0. };
   SpiceDouble             z[] = { 0., 0., 1. };

   
   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfsntc_c" );


   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );

   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   natpck_( PCK1, (logical *) &false_, 
                  (logical *) &true_, 
                  (ftnlen) strlen(PCK) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create an SPK, load using FURNSH.
   */
   tstspk_c ( SPK, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   natspk_( SPK1, (logical *) &false_, 
                  &han1, 
                  (ftnlen) strlen(SPK1) ); 
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create a confinement window from ET0 and ET1.
   */

   str2et_c ( "2000 JAN 1  00:00:00 TDB", &et0 );
   str2et_c ( "2000 JAN 2  00:00:00 TDB", &et1 );

   scard_c( 0, &cnfine);

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Error cases
   */

   /*
   Case 1
   */
   tcase_c ( "Non-positive step size." );

   string_copy( "EARTH",       target );
   string_copy( "IAU_EARTH",   fixref );
   string_copy( "ELLIPSOID",   method );
   string_copy( "IAU_EARTH",   dref   );
   string_copy( "SUN",         obsrvr );
   string_copy( "LATITUDINAL", crdsys );
   string_copy( "LATITUDE",    coord  );
   string_copy( "=",           relate );

   abcorr = CORR[0];

   refval = 0.;
   step   = 0.;
   adjust = 0.;

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok);


   /*
   Case 2
   */
   tcase_c ( "Non unique body IDs." );

   string_copy( "EARTH", target );
   string_copy( "EARTH", obsrvr );

   step   = 1.;

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok);


   /*
   Case 3
   */
   tcase_c ( "Invalid aberration correction specifier." );

   string_copy( "MOON",  target );
   string_copy( "EARTH", obsrvr );

   gfsntc_c ( target,
              fixref,
              method,
              "X",
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok);


   /*
   Case 4
   */
   tcase_c ( "Invalid relations operator" );

   string_copy( "==", relate );

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok);


   /*
   Case 5
   */
   tcase_c ( "Invalid body names." );

   string_copy( "=",     relate );
   string_copy( "X",     target );
   string_copy( "EARTH", obsrvr );

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);

   string_copy( "MOON", target );
   string_copy( "X",    obsrvr );

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok);



   /*
   Case 6
   */
   tcase_c ( "Negative adjustment value." );

   string_copy( "MOON",  target );
   string_copy( "EARTH", obsrvr );

   adjust = -1.;

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok);



   /*
   Case 7
   */
   tcase_c ( "Ephemeris data unavailable." );

   string_copy( "EARTH", target );
   string_copy( "DAWN",  obsrvr );

   adjust = 0.;

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok);



   /*
   Case 8
   */
   tcase_c ( "Unknown coordinate system." );

   string_copy( "SUN", obsrvr );
   string_copy( "X",   crdsys );

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok);



   /*
   Case 9
   */
   tcase_c ( "Unknown coordinate." );

   string_copy( "X",           coord );
   string_copy( "LATITUDINAL", crdsys );

   gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
   chckxc_c ( SPICETRUE, "SPICE(NOTSUPPORTED)", ok);



   /*
   Case 10
   */
   for( i=0; i<2; i++ )
      {
      
      switch (i)
         {
         case 0:   
            string_copy( "PLANETOGRAPHIC", crdsys ); 
            break;
         
         case 1:
            string_copy( "GEODETIC",       crdsys ); 
            break;
         
         default:
            break;
         }
      
      repmc_c( "Unknown fixref frame for #.", "#", crdsys, LNSIZE, title );
      tcase_c ( title );

      string_copy( "ALTITUDE", coord );
      string_copy( "X",        fixref);

      gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
      chckxc_c ( SPICETRUE, "SPICE(NOFRAME)", ok);

      }




   /*
   Event test block.

   Define the coordinate test conditions.

   Conditions: all conditions should occur once per
   GAMMA orbit (delta_t = 24 hours).
   */

   bodvrd_c( "alpha", "RADII", 3, &dim, rad );

   /*
   Define a block for event tests. Assign the test conditions 
   in the MDESC and MREFS arrays.
   */
   {

   SpiceChar * MDESC[] = {   "RECTANGULAR   : X           : <",
                             "RECTANGULAR   : Y           : <",
                             "RECTANGULAR   : Z           : <",
                             "RECTANGULAR   : X           : >",
                             "RECTANGULAR   : Y           : >",
                             "RECTANGULAR   : Z           : >",
                             "LATITUDINAL   : RADIUS      : >",
                             "LATITUDINAL   : LATITUDE    : >",
                             "LATITUDINAL   : LATITUDE    : <",
                             "RA/DEC        : RANGE       : >",
                             "RA/DEC        : DECLINATION : >",
                             "RA/DEC        : DECLINATION : <",
                             "SPHERICAL     : RADIUS      : >",
                             "SPHERICAL     : COLATITUDE  : >",
                             "SPHERICAL     : COLATITUDE  : <",
                             "CYLINDRICAL   : RADIUS      : >",
                             "CYLINDRICAL   : Z           : <",
                             "CYLINDRICAL   : Z           : >",
                             "CYLINDRICAL   : LONGITUDE   : >",
                             "SPHERICAL     : LONGITUDE   : <",
                             "LATITUDINAL   : LONGITUDE   : <",
                             "RA/DEC        : RIGHT ASCENSION : >" };

   /*
   Test conditions reference values.
   */
   SpiceDouble   MREFS[] = { rad[0],
                             rad[1],
                             rad[2],
                            -rad[0],
                            -rad[1],
                            -rad[2],
                             0.,
                            -90.*rpd_c(),
                             90.*rpd_c(),
                             0.,
                            -90.*rpd_c(),
                             90.*rpd_c(),
                             0.,
                             0.,
                             180.*rpd_c(),
                             0.,
                             rad[2],
                            -rad[2],
                             0.,
                             0.,
                             0.,
                             0. };

   SpiceChar     items[3][LNSIZE];
   SpiceDouble   timbeg[2];
   SpiceDouble   timend[2];

   /*
   Time interval thirty days.
   */
   str2et_c ( "2000 JAN 1  18:00:00 TDB", &et0 );
   str2et_c ( "2000 JAN 31 18:00:00 TDB", &et1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   adjust = 0.;

   string_copy( "ALPHA",      target );   
   string_copy( "ALPHAFIXED", fixref );
   string_copy( "ELLIPSOID",  method );
   string_copy( "GAMMA",      obsrvr );
   string_copy( "J2000",      dref   );

   abcorr = CORR[0];


   /*
   Case 12, Sweeps of the GAMMA centered J2000 Z axis on ALPHA.
   */

   step  = spd_c() * (1.25/24.);
   ntest = sizeof(MDESC)/sizeof(MDESC[0]);

   for (i=0; i<ntest; i++ )
      {
      
      scard_c( 0, &cnfine);
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );
      
      (void) sprintf( title, "ALPHA sweeps from GAMMA, Z, %s", MDESC[i] );
      tcase_c (title);
      
      lparse_c ( MDESC[i], ":", 3, LNSIZE, &dim, items );
      
      string_copy( items[0], crdsys );
      string_copy( items[1], coord  );
      string_copy( items[2], relate );
      
      refval = MREFS[i];

      gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              z,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
      chckxc_c ( SPICEFALSE, " ", ok);

      wnfild_c( 1e-3, &result);

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 30, 0, ok );
      
      timbeg[0] = 0.;
      timend[0] = 0.;
      timbeg[1] = 0.;
      timend[1] = 0.;
      
      if ( *ok )
         {
         wnfetd_c( &result, 0, &timbeg[0], &timend[0] );
         
         for ( j=1; j<count; j++ )
            {            
            wnfetd_c( &result, j, &timbeg[1], &timend[1] );

            /*
            Confirm the time separating the start times for subseqent 
            intervals and the end times for subsequent intervals has value
            one day in seconds.
            */
            chcksd_c ( "Z SWEEP BEG", timbeg[1] - timbeg[0], "~", spd_c(), 
                                                             CNVTOL, ok );
            chcksd_c ( "Z SWEEP END", timend[1] - timend[0], "~", spd_c(), 
                                                             CNVTOL, ok );

            timbeg[0] = timbeg[1];
            timend[0] = timend[1]; 
            }
         
         }
      
      }



   /*
   Case 13, Sweeps of the GAMMA centered J2000 Y axis on ALPHA.
   */

   step  = spd_c() * (3.25/24.);

   for (i=0; i<ntest; i++ )
      {
      
      scard_c( 0, &cnfine);
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );
      
      (void) sprintf( title, "ALPHA sweeps from GAMMA, Y, %s", MDESC[i] );
      tcase_c (title);
      
      lparse_c ( MDESC[i], ":", 3, LNSIZE, &dim, items );
      
      string_copy( items[0], crdsys );
      string_copy( items[1], coord  );
      string_copy( items[2], relate );
      
      refval = MREFS[i];

      gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              y,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
      chckxc_c ( SPICEFALSE, " ", ok);

      wnfild_c( 1e-3, &result);

      count = 0;
      count = wncard_c( &result);
      chcksi_c ( "COUNT", count, "=", 30, 0, ok );
      
      timbeg[0] = 0.;
      timend[0] = 0.;
      timbeg[1] = 0.;
      timend[1] = 0.;
      
      if ( *ok )
         {
         wnfetd_c( &result, 0, &timbeg[0], &timend[0] );
         
         for ( j=1; j<count; j++ )
            {            
            wnfetd_c( &result, j, &timbeg[1], &timend[1] );

            /*
            Confirm the time separating the start times for subseqent 
            intervals and the end times for subsequent intervals has value
            one day in seconds.
            */
            chcksd_c ( "Y SWEEP BEG", timbeg[1] - timbeg[0], "~", spd_c(), 
                                                             CNVTOL, ok );
            chcksd_c ( "Y SWEEP END", timend[1] - timend[0], "~", spd_c(), 
                                                             CNVTOL, ok );

            timbeg[0] = timbeg[1];
            timend[0] = timend[1]; 
            }
         
         }
      
      }



   /*
   Case 14, Sweeps of the GAMMA centered J2000 X axis on ALPHA.
   */

   step  = spd_c() * (1.25/24.);

   for (i=0; i<ntest; i++ )
      {
      
      scard_c( 0, &cnfine);
      wninsd_c ( et0, et1, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );
      
      (void) sprintf( title, "ALPHA sweeps from GAMMA, X, %s", MDESC[i] );
      tcase_c (title);
      
      lparse_c ( MDESC[i], ":", 3, LNSIZE, &dim, items );
      
      string_copy( items[0], crdsys );
      string_copy( items[1], coord  );
      string_copy( items[2], relate );
      
      refval = MREFS[i];

      gfsntc_c ( target,
              fixref,
              method,
              abcorr,
              obsrvr,
              dref, 
              x,
              crdsys,
              coord,
              relate,
              refval,
              adjust,
              step,
              MAXWIN,
              &cnfine,
              &result  );
      chckxc_c ( SPICEFALSE, " ", ok);

      wnfild_c( 1e-3, &result);

      count = 0;
      count = wncard_c( &result);

      /*
      The J2000 X unit with origin at GAMMA's center should never 
      intersect ALPHA.
      */
      chcksi_c ( "COUNT", count, "=", 0, 0, ok );

      }

   }
   /*
   End of event test block
   */



              
   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfsntc_c */


