/* f_gfstep.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b6 = 0.;
static logical c_true = TRUE_;
static doublereal c_b17 = -1.;

/* $Procedure      F_GFSTEP ( Test GFSTEP ) */
/* Subroutine */ int f_gfstep__(logical *ok)
{
    doublereal step;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    extern doublereal dpmax_(void);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal xstep;
    extern /* Subroutine */ int t_success__(logical *);
    doublereal et;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen), gfstep_(doublereal *, 
	    doublereal *), gfsstp_(doublereal *);

/* $ Abstract */

/*     Test the GF step size routine GFSTEP and step setting */
/*     routine GFSSTP. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     Test the GF step size routine GFSTEP and step setting */
/*     routine GFSSTP. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.1.0, 01-FEB-2009 (NJB) */

/*        Initialized ET to avoid portability problems. */
/*        Changed value for negative step case. */

/* -    SPICELIB Version 2.0.0, 22-AUG-2008 (NJB) */

/*        "Not initialized" test case has been commented */
/*        out. This case can be used only when this family */
/*        is the first one that uses GFSTEP. */

/* -    SPICELIB Version 1.0.0, 02-AUG-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFSTEP", (ftnlen)8);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

/*      CALL TCASE ( 'Call GFSTEP while step is uninitialized.' ) */
/*      CALL GFSTEP ( 0.D0, STEP ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(NOTINITIALIZED)', OK ) */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set zero step.", (ftnlen)14);

/*     Give ET an initial value. */

    et = 1e9;

/*     Set initial, valid step value. */

    xstep = 300.;
    gfsstp_(&xstep);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsstp_(&c_b6);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Retrieve step. */

    gfstep_(&et, &step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("STEP", &step, "=", &xstep, &c_b6, ok, (ftnlen)4, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Set negative step.", (ftnlen)18);

/*     Set initial, valid step value. */

    xstep = 300.;
    gfsstp_(&xstep);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsstp_(&c_b17);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Retrieve step. */

    gfstep_(&et, &step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("STEP", &step, "=", &xstep, &c_b6, ok, (ftnlen)4, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Make sure positive step sizes are accepted and saved.", (ftnlen)
	    53);

/*     ET should be ignored. Use an unusual value of ET. */

    et = dpmax_();
    xstep = 1.;
    gfsstp_(&xstep);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfstep_(&et, &step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("STEP (1)", &step, "=", &xstep, &c_b6, ok, (ftnlen)8, (ftnlen)1);
    et = -(dpmax_() / 2);
    xstep = dpmax_();
    gfsstp_(&xstep);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfstep_(&et, &step);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("STEP (DPMAX())", &step, "=", &xstep, &c_b6, ok, (ftnlen)14, (
	    ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfstep__ */

