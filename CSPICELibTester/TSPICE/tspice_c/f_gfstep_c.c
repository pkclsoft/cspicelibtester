/*

-Procedure f_gfstep_c ( Test CSPICE GF step size APIs )

 
-Abstract
 
   Perform tests on the CSPICE GF step size APIs.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
 
   void f_gfstep_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   Test the GF step size routine gfstep_c and step setting
   routine gfsstp_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 04-APR-2009 (NJB) 

-Index_Entries

   test CSPICE adapters

-&
*/

{ /* Begin f_gfstep_c */

   
   /*
   Local variables
   */
   SpiceDouble             et;
   SpiceDouble             step;
   SpiceDouble             xstep;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfstep_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set zero step." );
   
   /*
   Set initial, valid step value. 
   */
   xstep = 3.e2;

   gfsstp_c ( xstep );
   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   gfsstp_c ( 0.0 );
 
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );

   /*
   Retrieve step. 
   */
   et = 1.e10;

   gfstep_c ( et, &step );

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "step", step, "=", xstep, 0.0, ok );


   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set negative step." );
   
   /*
   Set initial, valid step value. 
   */
   xstep = 3.e2;

   gfsstp_c ( xstep );
   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   gfsstp_c ( -1.0 );
 
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );

   /*
   Retrieve step. 
   */
   gfstep_c ( et, &step );

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "step", step, "=", xstep, 0.0, ok );




   /*
   *********************************************************************
   *
   *    Normal cases
   *
   *********************************************************************
   */

   /*
   ---- Case -------------------------------------------------------------
   */
   tcase_c ( "Make sure positive step sizes are accepted "
             "and saved."                                  );


   /*
   `et' should be ignored. Use an unusual value of `et'.
   */
   et    = dpmax_c();

   xstep = 1.0;

   gfsstp_c ( xstep );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfstep_c ( et, &step );

   chckxc_c( SPICEFALSE, " ", ok );

   chcksd_c ( "step (1)", step, "=", xstep, 0.0, ok );


   et    = - ( dpmax_c()/2 );
   xstep = dpmax_c();

   gfsstp_c ( xstep );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfstep_c ( et, &step );

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "step (dpmax())", step, "=", xstep, 0.0, ok );



 
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

     
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfstep_c */





 
