/*

-Procedure f_gfstol_c ( Test gfstol_c )


-Abstract

   Perform tests on the CSPICE wrapper gfstol_c.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"


   void f_gfstol_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrappers for gfstol_c.

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   E.D. Wright    (JPL)

-Version

   -tspice_c Version 1.0.0, 09-MAR-2017 (EDW)

-Index_Entries

   test gfstol_c

-&
*/

{ /* Begin f_gfstol_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define SPK             "gfstol_c.bsp"
   #define LINLEN          81
   #define MAXWIN          20

   /*
   Local variables
   */

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( result,   MAXWIN );

   SpiceDouble             adjust;
   SpiceDouble             beg;
   SpiceDouble             end;
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             left;
   SpiceDouble             refval;
   SpiceDouble             right;
   SpiceDouble             step;

   SpiceInt                handle;
   SpiceInt                n;
   SpiceInt                nintvls;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfstol_c" );

   /*
   Case 1
   */
   tcase_c ( "Set up: create and load kernels." );

   /*
   Make sure the kernel pool doesn't contain any unexpected
   definitions.
   */
   kclear_c();

   /*
   Load a leapseconds kernel.

   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create and load a PCK file. Delete the file afterward.
   */
   tstpck_c ( "test.pck", SPICETRUE, SPICEFALSE );


   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   Case 2
   */

   tcase_c ( "Normal search: Earth-Moon distance is 390000 km." );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 mar 1 TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );

   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step    = spd_c();
   adjust  = 0.;
   refval  = 390000.;
   nintvls = MAXWIN/2;


   gfdist_c ( "MOON", "NONE", "EARTH", "=",     refval,
              adjust, step,    nintvls, &cnfine, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   n = 0;
   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals.
   */
   chcksi_c ( "n", n, "!=", 0, 0, ok );

   wnfetd_c ( &result, 0, &left, &right );

   scard_c ( 0, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Set a non standard tolerance.
   */

   gfstol_c( 1e-4 );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfdist_c ( "MOON", "NONE", "EARTH", "=",     refval,
              adjust, step,    nintvls, &cnfine, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   n = 0;
   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "n", n, "!=", 0, 0, ok );

   wnfetd_c ( &result, 0, &beg, &end );

   chcksd_c ( "left-beg", left, "!=", beg, 0, ok );

   /*
   Reset the convergence tolerance to default.
   */
   gfstol_c( 1e-6 );


   /*
   Case n
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( handle );
   TRASH   ( SPK    );

   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );


} /* End f_gfstol_c */

