/* f_gfsubc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__10000 = 10000;
static integer c__15 = 15;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c__30 = 30;
static integer c__1 = 1;
static doublereal c_b198 = 1e-6;
static doublereal c_b232 = 1e-4;
static doublereal c_b279 = 0.;

/* $Procedure F_GFSUBC ( GFSUBC family tests ) */
/* Subroutine */ int f_gfsubc__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";

    /* System generated locals */
    address a__1[2], a__2[3];
    integer i__1, i__2[2], i__3, i__4[3];
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static doublereal left, step, work[150090]	/* was [10006][15] */;
    static char time0[80], time1[80];
    static integer i__, j, k;
    static char mdesc[80*12];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char coord[36];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static doublereal mrefs[12], right;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80], items[36*3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count, ntest;
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    static integer handle;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *), kclear_(void);
    static doublereal cnfine[10006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal timbeg[2];
    static char abcorr[80];
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen);
    static char relate[36];
    extern /* Subroutine */ int gfsubc_(char *, char *, char *, char *, char *
	    , char *, char *, char *, doublereal *, doublereal *, doublereal *
	    , doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    extern integer wncard_(doublereal *);
    static char method[36], target[36], fixref[36], obsrvr[36], crdsys[36];
    static doublereal refval, et0, et1, adjust, result[10006], timend[2];
    extern /* Subroutine */ int tstlsk_(void), furnsh_(char *, ftnlen), 
	    natpck_(char *, logical *, logical *, ftnlen), tstspk_(char *, 
	    logical *, integer *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *
	    , doublereal *, doublereal *), bodvrd_(char *, char *, integer *, 
	    integer *, doublereal *, ftnlen, ftnlen), lparse_(char *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen, ftnlen), chcksi_(
	    char *, integer *, char *, integer *, integer *, logical *, 
	    ftnlen, ftnlen), wnfetd_(doublereal *, integer *, doublereal *, 
	    doublereal *), gfstol_(doublereal *), delfil_(char *, ftnlen);
    static doublereal beg, end, rad[3];
    static integer dim;
    extern doublereal rpd_(void), spd_(void);
    static integer han1;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFSUBC */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFSUBC. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.3.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.2.0, 27-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.1.0, 07-JUN-2010 (NJB)(EDW) */

/*        The TSTSPK and NATSPK calls do not load the created */
/*        kernels after creation. The Keeper subsystem controls */
/*        kernel loading and unloading. */

/*        Removed unneeded and confusion causing WNFILD call. */

/*        Edited header to include proper author citation for NJB. */

/*        Minor edit to code comments eliminating typo. */

/* -    TSPICE Version 1.0.0, 12-FEB-2009 (NJB)(EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFSUBC", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    t_pck08__("gfsubc.pck", &c_false, &c_true, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsubc.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an SPK, load using FURNSH. */

    tstspk_("gfsubc.bsp", &c_false, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsubc.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.bsp", &c_false, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    s_copy(time1, "2000 APR 1  00:00:00 TDB", (ftnlen)80, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__10000, cnfine);
    ssized_(&c__10000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*    Error cases */


/*   Case 1 */

    tcase_("Non-positive step size", (ftnlen)22);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(method, "Near point: ellipsoid", (ftnlen)36, (ftnlen)21);
    s_copy(fixref, "IAU_EARTH", (ftnlen)36, (ftnlen)9);
    s_copy(abcorr, corr, (ftnlen)80, (ftnlen)80);
    s_copy(crdsys, "RECTANGULAR", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "X", (ftnlen)36, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    refval = 0.;
    step = 0.;
    adjust = 0.;
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*   Case 2 */

    tcase_("Non unique body IDs.", (ftnlen)20);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    step = 1.;
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*   Case 3 */

    tcase_("Invalid aberration correction specifier", (ftnlen)39);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(abcorr, "X", (ftnlen)80, (ftnlen)1);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*   Case 4 */

    tcase_("Invalid relations operator", (ftnlen)26);
    s_copy(abcorr, corr, (ftnlen)80, (ftnlen)80);
    s_copy(relate, "==", (ftnlen)36, (ftnlen)2);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*   Case 5 */

    tcase_("Invalid body names", (ftnlen)18);
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    s_copy(target, "X", (ftnlen)36, (ftnlen)1);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "X", (ftnlen)36, (ftnlen)1);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/*     Case 6 */

    tcase_("Negative adjustment value", (ftnlen)25);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    adjust = -1.;
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 7 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    adjust = 0.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "DAWN", (ftnlen)36, (ftnlen)4);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*     Case 8 */

    tcase_("Unknown coordinate system", (ftnlen)25);
    adjust = 1.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(crdsys, "X", (ftnlen)36, (ftnlen)1);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Case 9 */

    tcase_("Unknown coordinate system", (ftnlen)25);
    adjust = 0.;
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "X", (ftnlen)36, (ftnlen)1);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Case 10 */

    for (i__ = 1; i__ <= 2; ++i__) {
	if (i__ == 1) {
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)36, (ftnlen)14);
	} else {
	    s_copy(crdsys, "GEODETIC", (ftnlen)36, (ftnlen)8);
	}
	s_copy(title, "Unknown frame for #", (ftnlen)80, (ftnlen)19);
	repmc_(title, "#", crdsys, title, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
		ftnlen)80);
	tcase_(title, (ftnlen)80);
	s_copy(coord, "ALTITUDE", (ftnlen)36, (ftnlen)8);
	s_copy(fixref, "X", (ftnlen)36, (ftnlen)1);
	gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate,
		 &refval, &adjust, &step, cnfine, &c__10000, &c__15, work, 
		result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (
		ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);
    }

/*     Case 11 */

    tcase_("Invalid target frame", (ftnlen)20);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "LATITUDE", (ftnlen)36, (ftnlen)8);
    s_copy(fixref, "IAU_MARS", (ftnlen)36, (ftnlen)8);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/*     Define the coordinate test conditions. */

    bodvrd_("ALPHA", "RADII", &c__3, &dim, rad, (ftnlen)5, (ftnlen)5);
    s_copy(mdesc, "RECTANGULAR : X : >", (ftnlen)80, (ftnlen)19);
    mrefs[0] = 0.;
    s_copy(mdesc + 80, "RECTANGULAR : Y : <", (ftnlen)80, (ftnlen)19);
    mrefs[1] = 0.;
    s_copy(mdesc + 160, "LATITUDINAL : LONGITUDE : <", (ftnlen)80, (ftnlen)27)
	    ;
    mrefs[2] = rpd_() * -90.;
    s_copy(mdesc + 240, "RA/DEC : RIGHT ASCENSION : >", (ftnlen)80, (ftnlen)
	    28);
    mrefs[3] = rpd_() * 270.;
    s_copy(mdesc + 320, "SPHERICAL : LONGITUDE : <", (ftnlen)80, (ftnlen)25);
    mrefs[4] = rpd_() * -90.;
    s_copy(mdesc + 400, "CYLINDRICAL : LONGITUDE : >", (ftnlen)80, (ftnlen)27)
	    ;
    mrefs[5] = rpd_() * 270.;
    s_copy(mdesc + 480, "RECTANGULAR : X : LOCMAX", (ftnlen)80, (ftnlen)24);
    mrefs[6] = 0.;
    s_copy(mdesc + 560, "RECTANGULAR : Y : LOCMAX", (ftnlen)80, (ftnlen)24);
    mrefs[7] = 0.;
    s_copy(mdesc + 640, "RECTANGULAR : X : LOCMIN", (ftnlen)80, (ftnlen)24);
    mrefs[8] = 0.;
    s_copy(mdesc + 720, "RECTANGULAR : Y : LOCMIN", (ftnlen)80, (ftnlen)24);
    mrefs[9] = 0.;
    s_copy(mdesc + 800, "SPHERICAL : LONGITUDE : =", (ftnlen)80, (ftnlen)25);
    mrefs[10] = rpd_() * 5.;
    s_copy(mdesc + 880, "CYLINDRICAL : LONGITUDE : =", (ftnlen)80, (ftnlen)27)
	    ;
    mrefs[11] = rpd_() * 269.;

/*     Time interval thirty days. */

    s_copy(time0, "2000 JAN 1   3:00:00 TDB", (ftnlen)80, (ftnlen)24);
    s_copy(time1, "2000 JAN 31  3:00:00 TDB", (ftnlen)80, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_(time1, &et1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    s_copy(method, "Near point: ellipsoid", (ftnlen)36, (ftnlen)21);
    s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "ALPHAFIXED", (ftnlen)36, (ftnlen)10);
    s_copy(obsrvr, "GAMMA", (ftnlen)36, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);

/*     Case 12 */

    step = spd_() * .20833333333333334;
    ntest = 12;
    for (i__ = 1; i__ <= 12; ++i__) {
	scardd_(&c__0, cnfine);
	wninsd_(&et0, &et1, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = 9, a__1[0] = "MDESC(#) ";
	i__2[1] = 80, a__1[1] = mdesc + ((i__1 = i__ - 1) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("mdesc", i__1, "f_gfsubc__", (ftnlen)621)) * 80;
	s_cat(title, a__1, i__2, &c__2, (ftnlen)80);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);

/*        Parse from the MDESC(I) string the coordinate system, */
/*        coordinate, and relation operator. */

	lparse_(mdesc + ((i__1 = i__ - 1) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"mdesc", i__1, "f_gfsubc__", (ftnlen)630)) * 80, ":", &c__3, &
		dim, items, (ftnlen)80, (ftnlen)1, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
	s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
	s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
	refval = mrefs[(i__1 = i__ - 1) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"mrefs", i__1, "f_gfsubc__", (ftnlen)636)];
	gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate,
		 &refval, &adjust, &step, cnfine, &c__10000, &c__15, work, 
		result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (
		ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = wncard_(result);
	chcksi_("WNCARD(RESULT)", &i__1, "=", &c__30, &c__0, ok, (ftnlen)14, (
		ftnlen)1);
	timbeg[0] = 0.;
	timend[0] = 0.;
	timbeg[1] = 0.;
	timend[1] = 0.;
	if (*ok) {
	    wnfetd_(result, &c__1, timbeg, timend);
	    i__1 = wncard_(result);
	    for (j = 2; j <= i__1; ++j) {
		wnfetd_(result, &j, &timbeg[1], &timend[1]);
		d__1 = timbeg[1] - timbeg[0];
		d__2 = spd_();
		chcksd_("SWEEP BEG", &d__1, "~", &d__2, &c_b198, ok, (ftnlen)
			9, (ftnlen)1);
		d__1 = timend[1] - timend[0];
		d__2 = spd_();
		chcksd_("SWEEP END", &d__1, "~", &d__2, &c_b198, ok, (ftnlen)
			9, (ftnlen)1);
		timbeg[0] = timbeg[1];
		timend[0] = timend[1];
	    }
	}
    }

/*     Case 13 */

/*     Test the aberration correction values in a search. */
/*     Reduce the error tolerance to MEDTOL to account for the */
/*     light-time calculation artifacts. */

    s_copy(mdesc, "SPHERICAL     : LONGITUDE       : =", (ftnlen)80, (ftnlen)
	    35);
    mrefs[0] = rpd_() * -90.;
    step = spd_() * .20833333333333334;
    ntest = 1;
    i__1 = ntest;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (k = 2; k <= 9; ++k) {
	    s_copy(abcorr, corr + ((i__3 = k - 1) < 9 && 0 <= i__3 ? i__3 : 
		    s_rnge("corr", i__3, "f_gfsubc__", (ftnlen)698)) * 80, (
		    ftnlen)80, (ftnlen)80);
	    scardd_(&c__0, cnfine);
	    wninsd_(&et0, &et1, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Writing concatenation */
	    i__4[0] = 9, a__2[0] = "MDESC(#) ";
	    i__4[1] = 6, a__2[1] = abcorr;
	    i__4[2] = 80, a__2[2] = mdesc + ((i__3 = i__ - 1) < 12 && 0 <= 
		    i__3 ? i__3 : s_rnge("mdesc", i__3, "f_gfsubc__", (ftnlen)
		    705)) * 80;
	    s_cat(title, a__2, i__4, &c__3, (ftnlen)80);
	    repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    tcase_(title, (ftnlen)80);

/*           Parse from the MDESC(I) string the coordinate system, */
/*           coordinate, and relation operator. */

	    lparse_(mdesc + ((i__3 = i__ - 1) < 12 && 0 <= i__3 ? i__3 : 
		    s_rnge("mdesc", i__3, "f_gfsubc__", (ftnlen)714)) * 80, 
		    ":", &c__3, &dim, items, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(crdsys, items, (ftnlen)36, (ftnlen)36);
	    s_copy(coord, items + 36, (ftnlen)36, (ftnlen)36);
	    s_copy(relate, items + 72, (ftnlen)36, (ftnlen)36);
	    refval = mrefs[(i__3 = i__ - 1) < 12 && 0 <= i__3 ? i__3 : s_rnge(
		    "mrefs", i__3, "f_gfsubc__", (ftnlen)720)];
	    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, 
		    relate, &refval, &adjust, &step, cnfine, &c__10000, &
		    c__15, work, result, (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		    ftnlen)80, (ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)36)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    i__3 = wncard_(result);
	    chcksi_("WNCARD(RESULT)", &i__3, "=", &c__30, &c__0, ok, (ftnlen)
		    14, (ftnlen)1);
	    timbeg[0] = 0.;
	    timend[0] = 0.;
	    timbeg[1] = 0.;
	    timend[1] = 0.;
	    if (*ok) {
		wnfetd_(result, &c__1, timbeg, timend);
		i__3 = wncard_(result);
		for (j = 2; j <= i__3; ++j) {
		    wnfetd_(result, &j, &timbeg[1], &timend[1]);

/*                 Confirm the time separating the start times for */
/*                 subsequent intervals and the end times for subsequent */
/*                 intervals has value one day in seconds. */

		    d__1 = timbeg[1] - timbeg[0];
		    d__2 = spd_();
		    chcksd_("SWEEP BEG", &d__1, "~", &d__2, &c_b232, ok, (
			    ftnlen)9, (ftnlen)1);
		    d__1 = timend[1] - timend[0];
		    d__2 = spd_();
		    chcksd_("SWEEP END", &d__1, "~", &d__2, &c_b232, ok, (
			    ftnlen)9, (ftnlen)1);
		    timbeg[0] = timbeg[1];
		    timend[0] = timend[1];
		}
	    }
	}
    }

/*     Case 14 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(method, "Near point: ellipsoid", (ftnlen)36, (ftnlen)21);
    s_copy(fixref, "IAU_EARTH", (ftnlen)36, (ftnlen)9);
    s_copy(abcorr, corr, (ftnlen)80, (ftnlen)80);
    s_copy(crdsys, "RECTANGULAR", (ftnlen)36, (ftnlen)11);
    s_copy(coord, "X", (ftnlen)36, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    refval = 0.;
    step = spd_() * .20833333333333334;
    adjust = 0.;
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b232);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfsubc_(target, fixref, method, abcorr, obsrvr, crdsys, coord, relate, &
	    refval, &adjust, &step, cnfine, &c__10000, &c__15, work, result, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)80, (ftnlen)36, (
	    ftnlen)36, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b279, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b198);

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfsubc.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfsubc.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfsubc__ */

