/* f_gftfov.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__20000 = 20000;
static doublereal c_b34 = 0.;
static doublereal c_b36 = 1.;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static doublereal c_b202 = 2.;
static doublereal c_b210 = -1.;
static doublereal c_b220 = -2.;
static integer c__30000 = 30000;
static doublereal c_b459 = 1.9999999999999999e-6;
static doublereal c_b559 = 1e-4;
static doublereal c_b573 = 1e-6;

/* $Procedure      F_GFTFOV ( Test GFTFOV ) */
/* Subroutine */ int f_gftfov__(logical *ok)
{
    /* Initialized data */

    static char abcors[5*5] = "NONE " "CN   " "XCN  " "CN+S " "XCN+S";
    static char instrs[36*4] = "ALPHA_CIRCLE_NONE                   " "ALPHA"
	    "_ELLIPSE_NONE                  " "ALPHA_RECTANGLE_NONE          "
	    "      " "ALPHA_DIAMOND_NONE                  ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal left, step;
    static char inst[36];
    static logical xmit;
    static integer i__;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static doublereal radii[3], delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[40];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static doublereal bsite[3];
    extern /* Subroutine */ int natik_(char *, char *, char *, logical *, 
	    logical *, ftnlen, ftnlen, ftnlen), repmc_(char *, char *, char *,
	     char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal endpt[2];
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal right;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static char title[200];
    static doublereal xtime;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static logical uselt;
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *), bods2c_(char *, integer *, logical *, 
	    ftnlen);
    static doublereal badbnd[30000]	/* was [3][10000] */, badrad[3];
    static integer nc, handle;
    extern /* Subroutine */ int scardd_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    extern integer wncard_(doublereal *);
    static char abcorr[200], kvname[32], newfrm[32], oldfrm[32], obsrvr[36], 
	    target[36], tframe[32], tshape[9];
    static doublereal fovbnd[30000]	/* was [3][10000] */, raydir[3], 
	    result[20006], svradi[3];
    static integer instid;
    static logical attblk[6];
    static doublereal beg;
    extern /* Subroutine */ int tstlsk_(void), tstpck_(char *, logical *, 
	    logical *, ftnlen), tstspk_(char *, logical *, integer *, ftnlen),
	     natpck_(char *, logical *, logical *, ftnlen), natspk_(char *, 
	    logical *, integer *, ftnlen), ssized_(integer *, doublereal *), 
	    gftfov_(char *, char *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), gdpool_(char *, integer *, integer *, 
	    integer *, doublereal *, logical *, ftnlen);
    static doublereal end;
    extern /* Subroutine */ int dvpool_(char *, ftnlen), pdpool_(char *, 
	    integer *, doublereal *, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), pcpool_(char *, integer *, char *, 
	    ftnlen, ftnlen), vminus_(doublereal *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), gcpool_(char *, 
	    integer *, integer *, integer *, char *, logical *, ftnlen, 
	    ftnlen), suffix_(char *, integer *, char *, ftnlen, ftnlen), 
	    cmprss_(char *, integer *, char *, char *, ftnlen, ftnlen, ftnlen)
	    , chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), wnfetd_(doublereal *, integer *, 
	    doublereal *, doublereal *), gfstol_(doublereal *), spkuef_(
	    integer *);
    extern doublereal spd_(void);
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static integer ins, han2;

/* $ Abstract */

/*     Test the GF FOV search API GFTFOV. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any exceptions. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF AP routine GFTFOV. The underlying */
/*     GF FOV search utility routines */

/*        ZZGFFVIN */
/*        ZZGFFVST */

/*     are implicitly tested by this family together with */

/*        F_GFRFOV. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 1.2.0, 31-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLV. */

/* -    TSPICE Version 1.1.0, 20-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     Local parameters */


/*     Local variables */


/*     Saved Variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_GFTFOV", (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgffvu.tpc", &c_true, &c_false, (ftnlen)11);

/*     Load an SPK file as well. */

    tstspk_("zzgffvu.bsp", &c_true, &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK, PCK/FK, and IK */
/*     files. */

    natpck_("nat.tpc", &c_true, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natik_("nat.ti", "nat.bsp", "nat.tpc", &c_true, &c_false, (ftnlen)6, (
	    ftnlen)7, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     The following error cases involve invalid initialization */
/*     values or missing data discovered at search time. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Bad step size", (ftnlen)13);
    ssized_(&c__20000, cnfine);
    ssized_(&c__20000, result);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BET", (ftnlen)36, (ftnlen)3);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 0.;
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small", (ftnlen)23);
    ssized_(&c__20000, cnfine);
    ssized_(&c__0, result);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 300.;
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(WINDOWTOOSMALL)", ok, (ftnlen)21);

/*     Restore window sizes. */

    ssized_(&c__20000, cnfine);
    ssized_(&c__20000, result);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad observer or target name.", (ftnlen)28);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BET", (ftnlen)36, (ftnlen)3);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SN", (ftnlen)36, (ftnlen)2);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer equals target", (ftnlen)22);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction for ellipsoid target.", (ftnlen)47);
    s_copy(abcorr, "XS", (ftnlen)200, (ftnlen)2);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction for point target.", (ftnlen)43);
    s_copy(abcorr, "S", (ftnlen)200, (ftnlen)1);
    s_copy(tshape, "POINT", (ftnlen)9, (ftnlen)5);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction for ray target.", (ftnlen)41);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(tshape, "Ray", (ftnlen)9, (ftnlen)3);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad shape specification.", (ftnlen)24);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(tshape, "Line", (ftnlen)9, (ftnlen)4);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSHAPE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad reference frame TFRAME", (ftnlen)26);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, "EME2000", (ftnlen)32, (ftnlen)7);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Blank reference frame TFRAME for ellipsoidal target", (ftnlen)51);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, " ", (ftnlen)32, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad shape: ray.", (ftnlen)15);
    s_copy(tshape, "RAY", (ftnlen)9, (ftnlen)3);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "XS", (ftnlen)200, (ftnlen)2);
    s_copy(tframe, " ", (ftnlen)32, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame TFRAME not centered on target when TSHAPE requir"
	    "es it.", (ftnlen)70);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-existent reference frame.", (ftnlen)29);
    s_copy(target, "beta", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(tframe, "EME2000", (ftnlen)32, (ftnlen)7);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target radii in kernel pool", (ftnlen)30);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dvpool_("BODY399_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii count", (ftnlen)22);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pdpool_("BODY399_RADII", &c__2, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii values", (ftnlen)23);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, svradi, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b34, &c_b36, &c_b202, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    vpack_(&c_b36, &c_b210, &c_b202, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    vpack_(&c_b36, &c_b36, &c_b220, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, svradi, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gdpool_("BODY399_RADII", &c__1, &c__3, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ID code for instrument.", (ftnlen)26);
    s_copy(inst, "ALPHA_ELLIPSE", (ftnlen)36, (ftnlen)13);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Instrument parameters missing from kernel pool.", (ftnlen)47);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    s_copy(kvname, "INS#_FOV_SHAPE", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);

/*     Delete the instrument shape from the kernel pool. */

    dvpool_(kvname, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SHAPEMISSING)", ok, (ftnlen)19);

/*     Restore the instrument shape value. */

    pcpool_(kvname, &c__1, "ELLIPSE", (ftnlen)32, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Degenerate FOV ellipse.", (ftnlen)23);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Fetch the instrument boresight vector from the kernel pool. */

    s_copy(kvname, "INS#_BORESIGHT", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__3, &n, bsite, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the instrument boundary vectors from the kernel pool. */

    s_copy(kvname, "INS#_FOV_BOUNDARY", (ftnlen)32, (ftnlen)17);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__30000, &n, fovbnd, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the first boundary vector equal to the boresight vector. */

    moved_(fovbnd, &n, badbnd);
    moved_(bsite, &c__3, badbnd);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore the first boundary vector and set the second */
/*     equal to the boresight vector. */

    moved_(fovbnd, &c__3, badbnd);
    moved_(bsite, &c__3, &badbnd[3]);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore the instrument FOV boundary vectors. */

    pdpool_(kvname, &n, fovbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("FOV boundary vector has excessive angular separation from boresi"
	    "ght.", (ftnlen)68);
    s_copy(inst, "ALPHA_RECTANGLE_NONE", (ftnlen)36, (ftnlen)20);
    bods2c_(inst, &instid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Fetch the instrument boresight vector from the kernel pool. */

    s_copy(kvname, "INS#_BORESIGHT", (ftnlen)32, (ftnlen)14);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__3, &n, bsite, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the instrument boundary vectors from the kernel pool. */

    s_copy(kvname, "INS#_FOV_BOUNDARY", (ftnlen)32, (ftnlen)17);
    repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    gdpool_(kvname, &c__1, &c__30000, &n, fovbnd, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the third boundary vector equal to the *negative* */
/*     of the boresight vector. */

    moved_(fovbnd, &n, badbnd);
    vminus_(bsite, &badbnd[6]);
    pdpool_(kvname, &n, badbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    vpack_(&c_b34, &c_b34, &c_b36, raydir);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(FACENOTFOUND)", ok, (ftnlen)19);

/*     Restore the instrument FOV boundary vectors. */

    pdpool_(kvname, &n, fovbnd, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

/*     At this point, we need an actual, non-empty confinement */
/*     window. */

    d__1 = spd_();
    wninsd_(&c_b34, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(inst, "ALPHA_ELLIPSE_NONE", (ftnlen)36, (ftnlen)18);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(tframe, "ITRF93", (ftnlen)32, (ftnlen)6);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    d__1 = spd_();
    wninsd_(&c_b34, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(tframe, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    d__1 = spd_();
    wninsd_(&c_b34, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(tframe, "IAU_SUN", (ftnlen)32, (ftnlen)7);
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No TFRAME orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    d__1 = spd_();
    wninsd_(&c_b34, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(tshape, "ellipsoid", (ftnlen)9, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(tframe, "ITRF93", (ftnlen)32, (ftnlen)6);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Non-error exceptional cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     We'll start out with cases using an ellipsoidal target and */
/*     geometric states. We'll use the four instruments defined */
/*     in nat.ti: */

/*        ALPHA_CIRCLE_NONE */
/*        ALPHA_ELLIPSE_NONE */
/*        ALPHA_RECTANGLE_NONE */
/*        ALPHA_DIAMOND_NONE */

/*     These have the FOV shapes */

/*        ELLIPSE */
/*        CIRCLE */
/*        RECTANGLE */
/*        POLYGON */

/*     and track body Alpha so body Beta's FOV entry and */
/*     exit times match the start and stop times of Beta's */
/*     transit across Alpha. */

/*     Initialize inputs for the search. */

    scardd_(&c__0, result);
    scardd_(&c__0, cnfine);

/*     Use a 3-day span that covers 3 events. */

    d__1 = spd_() * -.5;
    d__2 = spd_() * 2.5;
    wninsd_(&d__1, &d__2, cnfine);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tshape, "ELLIPSOID", (ftnlen)9, (ftnlen)9);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 300.;
    for (ins = 1; ins <= 4; ++ins) {
	s_copy(inst, instrs + ((i__1 = ins - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("instrs", i__1, "f_gftfov__", (ftnlen)1043)) * 36, (
		ftnlen)36, (ftnlen)36);
	for (nc = 1; nc <= 5; ++nc) {

/* --- Case: ------------------------------------------------------ */


	    s_copy(abcorr, abcors + ((i__1 = nc - 1) < 5 && 0 <= i__1 ? i__1 :
		     s_rnge("abcors", i__1, "f_gftfov__", (ftnlen)1050)) * 5, 
		    (ftnlen)200, (ftnlen)5);
	    zzprscor_(abcorr, attblk, (ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    uselt = attblk[1];
	    xmit = attblk[4];
	    if (! attblk[0]) {

/*              We have some type of aberration correction. */

/*              We're going to change the instrument frame dynamically */
/*              so as to keep the frame definition compatible with the */
/*              aberration correction we're using. We'll fetch the */
/*              instrument ID and use the aberration correction to */
/*              construct the name of the appropriate */
/*              aberration-corrected frame. */
		bodn2c_(inst, &instid, &found, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("INSTID found", &found, &c_true, ok, (ftnlen)12);
		s_copy(kvname, "INS#_FOV_FRAME", (ftnlen)32, (ftnlen)14);
		repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		gcpool_(kvname, &c__1, &c__1, &n, oldfrm, &found, (ftnlen)32, 
			(ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("frame found", &found, &c_true, ok, (ftnlen)11);
		s_copy(newfrm, oldfrm, (ftnlen)32, (ftnlen)32);
		suffix_("_#", &c__0, newfrm, (ftnlen)2, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Substitute the aberration correction flag into */
/*              the frame name. */

		repmc_(newfrm, "#", abcorr, newfrm, (ftnlen)32, (ftnlen)1, (
			ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              At this point we have a frame name such as */

/*                 ALPHA_VIEW_XY_CN+S */

/*              We must remove the '+' symbol from the name. */

		cmprss_("+", &c__0, newfrm, newfrm, (ftnlen)1, (ftnlen)32, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Insert the new instrument frame association into */
/*              the kernel pool. */

		pcpool_(kvname, &c__1, newfrm, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    } else {

/*              Set NEWFRM, since we reference it in the test case */
/*              title. */

		s_copy(newfrm, oldfrm, (ftnlen)32, (ftnlen)32);
	    }

/*           Set up the TCASE call. */

	    s_copy(title, "Target #; target shape #; Inst. #; #; inst frame #"
		    , (ftnlen)200, (ftnlen)50);
	    repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    36, (ftnlen)200);
	    repmc_(title, "#", tshape, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    9, (ftnlen)200);
	    repmc_(title, "#", inst, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    36, (ftnlen)200);
	    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    repmc_(title, "#", newfrm, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)200);

/*           Perform the search. */

	    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, 
		    cnfine, result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (
		    ftnlen)32, (ftnlen)200, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);

/*           We're expecting 3 events. */

	    chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    if (n > 0) {
		i__1 = n;
		for (i__ = 1; i__ <= i__1; ++i__) {
		    wnfetd_(result, &i__, endpt, &endpt[1]);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check the interval endpoints. The event should */
/*                 start at one noon+delta and stop 10 minutes later, */
/*                 where delta depends on the aberration correction: */

/*                    CN             delta = +1 */
/*                    CN+S           delta = +1 */
/*                    XCN            delta = -1 */
/*                    XCN+S          delta = -1 */

		    if (uselt) {
			if (xmit) {
			    delta = -1.;
			} else {
			    delta = 1.;
			}
		    } else {
			delta = 0.;
		    }
		    s_copy(qname, "Start time @", (ftnlen)40, (ftnlen)12);
		    repmi_(qname, "@", &i__, qname, (ftnlen)40, (ftnlen)1, (
			    ftnlen)40);
		    xtime = (i__ - 1) * spd_() + delta;
		    chcksd_(qname, endpt, "~", &xtime, &c_b459, ok, (ftnlen)
			    40, (ftnlen)1);
		    s_copy(qname, "Stop time @", (ftnlen)40, (ftnlen)11);
		    repmi_(qname, "@", &i__, qname, (ftnlen)40, (ftnlen)1, (
			    ftnlen)40);
		    xtime = (i__ - 1) * spd_() + 600. + delta;
		    chcksd_(qname, &endpt[1], "~", &xtime, &c_b459, ok, (
			    ftnlen)40, (ftnlen)1);
		}

/*              End of result interval check loop. */

	    }

/*           End of non-empty result window block. */

	    if (! attblk[0]) {

/*              Restore the old instrument frame association in */
/*              the kernel pool. */

		pcpool_(kvname, &c__1, oldfrm, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}

/*        End of aberration correction loop. */

    }

/*     End of instrument loop. */


/*     For the next set of cases, we keep the inputs as before, except */
/*     for the target shape: we treat the target as a single point. */
/*     This should cause each FOV entry to start 1 minute late and end */
/*     1 minute early. */

    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tshape, "POINT", (ftnlen)9, (ftnlen)5);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 300.;
    for (ins = 1; ins <= 4; ++ins) {
	s_copy(inst, instrs + ((i__1 = ins - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("instrs", i__1, "f_gftfov__", (ftnlen)1242)) * 36, (
		ftnlen)36, (ftnlen)36);
	for (nc = 1; nc <= 5; ++nc) {

/* --- Case: ------------------------------------------------------ */


	    s_copy(abcorr, abcors + ((i__1 = nc - 1) < 5 && 0 <= i__1 ? i__1 :
		     s_rnge("abcors", i__1, "f_gftfov__", (ftnlen)1249)) * 5, 
		    (ftnlen)200, (ftnlen)5);
	    zzprscor_(abcorr, attblk, (ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    uselt = attblk[1];
	    xmit = attblk[4];
	    if (! attblk[0]) {

/*              We have some type of aberration correction. */

/*              We're going to change the instrument frame dynamically */
/*              so as to keep the frame definition compatible with the */
/*              aberration correction we're using. We'll fetch the */
/*              instrument ID and use the aberration correction to */
/*              construct the name of the appropriate */
/*              aberration-corrected frame. */
		bodn2c_(inst, &instid, &found, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("INSTID found", &found, &c_true, ok, (ftnlen)12);
		s_copy(kvname, "INS#_FOV_FRAME", (ftnlen)32, (ftnlen)14);
		repmi_(kvname, "#", &instid, kvname, (ftnlen)32, (ftnlen)1, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		gcpool_(kvname, &c__1, &c__1, &n, oldfrm, &found, (ftnlen)32, 
			(ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("frame found", &found, &c_true, ok, (ftnlen)11);
		s_copy(newfrm, oldfrm, (ftnlen)32, (ftnlen)32);
		suffix_("_#", &c__0, newfrm, (ftnlen)2, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Substitute the aberration correction flag into */
/*              the frame name. */

		repmc_(newfrm, "#", abcorr, newfrm, (ftnlen)32, (ftnlen)1, (
			ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              At this point we have a frame name such as */

/*                 ALPHA_VIEW_XY_CN+S */

/*              We must remove the '+' symbol from the name. */

		cmprss_("+", &c__0, newfrm, newfrm, (ftnlen)1, (ftnlen)32, (
			ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Insert the new instrument frame association into */
/*              the kernel pool. */

		pcpool_(kvname, &c__1, newfrm, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    } else {

/*              Set NEWFRM, since we reference it in the test case */
/*              title. */

		s_copy(newfrm, oldfrm, (ftnlen)32, (ftnlen)32);
	    }

/*           Set up the TCASE call. */

	    s_copy(title, "Target #; target shape #; Inst. #; #; inst frame #"
		    , (ftnlen)200, (ftnlen)50);
	    repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    36, (ftnlen)200);
	    repmc_(title, "#", tshape, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    9, (ftnlen)200);
	    repmc_(title, "#", inst, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    36, (ftnlen)200);
	    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    repmc_(title, "#", newfrm, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)200);

/*           Perform the search. */

	    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, 
		    cnfine, result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (
		    ftnlen)32, (ftnlen)200, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);

/*           We're expecting 3 events. */

	    chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    if (n > 0) {
		i__1 = n;
		for (i__ = 1; i__ <= i__1; ++i__) {
		    wnfetd_(result, &i__, endpt, &endpt[1]);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check the interval endpoints. The event should */
/*                 start at one noon+delta and stop 10 minutes later, */
/*                 where delta depends on the aberration correction: */

/*                    CN             delta = +1 */
/*                    CN+S           delta = +1 */
/*                    XCN            delta = -1 */
/*                    XCN+S          delta = -1 */

		    if (uselt) {
			if (xmit) {
			    delta = -1.;
			} else {
			    delta = 1.;
			}
		    } else {
			delta = 0.;
		    }
		    s_copy(qname, "Start time @", (ftnlen)40, (ftnlen)12);
		    repmi_(qname, "@", &i__, qname, (ftnlen)40, (ftnlen)1, (
			    ftnlen)40);
		    xtime = (i__ - 1) * spd_() + delta + 60.;
		    chcksd_(qname, endpt, "~", &xtime, &c_b459, ok, (ftnlen)
			    40, (ftnlen)1);
		    s_copy(qname, "Stop time @", (ftnlen)40, (ftnlen)11);
		    repmi_(qname, "@", &i__, qname, (ftnlen)40, (ftnlen)1, (
			    ftnlen)40);
		    xtime = (i__ - 1) * spd_() + 600. + delta - 60.;
		    chcksd_(qname, &endpt[1], "~", &xtime, &c_b459, ok, (
			    ftnlen)40, (ftnlen)1);
		}

/*              End of result interval check loop. */

	    }

/*           End of non-empty result window block. */

	    if (! attblk[0]) {

/*              Restore the old instrument frame association in */
/*              the kernel pool. */

		pcpool_(kvname, &c__1, oldfrm, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}

/*        End of aberration correction loop. */

    }

/*     End of instrument loop. */


/*     Case */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)200, (
	    ftnlen)39);
    tcase_(title, (ftnlen)200);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    i__ = 1;
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(tshape, "POINT", (ftnlen)9, (ftnlen)5);
    s_copy(tframe, "BETAFIXED", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    step = 1.;
    s_copy(inst, instrs + ((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
	    "instrs", i__1, "f_gftfov__", (ftnlen)1445)) * 36, (ftnlen)36, (
	    ftnlen)36);
    s_copy(abcorr, abcors + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : 
	    s_rnge("abcors", i__1, "f_gftfov__", (ftnlen)1446)) * 5, (ftnlen)
	    200, (ftnlen)5);
    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use a 3-day span that covers 3 events. */

    d__1 = spd_() * -.5;
    d__2 = spd_() * 2.5;
    wninsd_(&d__1, &d__2, cnfine);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    n = wncard_(result);
    chcksi_("COUNT", &n, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b559);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gftfov_(inst, target, tshape, tframe, abcorr, obsrvr, &step, cnfine, 
	    result, (ftnlen)36, (ftnlen)36, (ftnlen)9, (ftnlen)32, (ftnlen)
	    200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = 0;
    n = wncard_(result);
    chcksi_("COUNT", &n, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b34, ok, (ftnlen)200, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b573);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgffvu.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gftfov__ */

