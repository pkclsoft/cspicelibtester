/* f_gfudb.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__50 = 50;
static integer c__2 = 2;
static doublereal c_b20 = -5.;
static doublereal c_b21 = 105.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b35 = 1e-6;
static integer c__1 = 1;
static doublereal c_b60 = 1e-4;
static doublereal c_b73 = 0.;

/* $Procedure F_GFUDB ( GFUDB family tests ) */
/* Subroutine */ int f_gfudb__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal left, step;
    static integer i__, j;
    extern /* Subroutine */ int gfudb_(U_fp, U_fp, doublereal *, doublereal *,
	     doublereal *), tcase_(char *, ftnlen), repmi_(char *, char *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    static doublereal right;
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *);
    static doublereal cnfine[8];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static integer badsiz[3];
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int wnfetd_(doublereal *, integer *, doublereal *,
	     doublereal *), ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), gfstol_(doublereal *);
    static doublereal result[56], beg;
    extern /* Subroutine */ int gfb_();
    static doublereal end;
    extern /* Subroutine */ int udf_();
    extern doublereal spd_(void);
    static doublereal lhs[3], rhs[3];
    static char msg1[80], msg2[80];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFUDB */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB geometry */
/*     routine GFUDB. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.0.0, 08-APR-2014 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything */


/*     Begin every test family with an open call. */

    topen_("F_GFUDB", (ftnlen)7);
    badsiz[0] = 0;
    badsiz[1] = 1;
    badsiz[2] = 3;

/*     The event boundaries as defined in GFB. */

    lhs[0] = 20.;
    lhs[1] = 55.;
    lhs[2] = 92.;
    rhs[0] = 30.;
    rhs[1] = 57.;
    rhs[2] = 100.;

/*     Error cases */


/*     Case 1: */

/*     Confirm bad window sizes cause the expected error. */

    step = spd_();
    scardd_(&c__0, result);
    scardd_(&c__0, cnfine);
    s_copy(msg1, "Invalid result window size #", (ftnlen)80, (ftnlen)28);
    for (i__ = 1; i__ <= 3; ++i__) {
	repmi_(msg1, "#", &badsiz[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("badsiz", i__1, "f_gfudb__", (ftnlen)209)], msg1, (
		ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(msg1, (ftnlen)80);
	ssized_(&badsiz[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"badsiz", i__1, "f_gfudb__", (ftnlen)213)], result);
	gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
	chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    }

/*     Setp appropriate sizes for the windows. */

    ssized_(&c__50, result);
    ssized_(&c__2, cnfine);

/*     Case 2: */

/*     Confirm non-positive step causes the expected error. */

    tcase_("Non-positive step size.", (ftnlen)23);
    step = -1.;
    gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = 0.;
    gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Standard cases. */


/*     Case 3: */

/*     Test output from use of GFB */


/*     Store the time bounds of our search interval in */
/*     the confinement window. */

    wninsd_(&c_b20, &c_b21, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = 1.9;
    gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 3 intervals in RESULT. */

    i__1 = wncard_(result);
    chcksi_("WNCARD( RESULT )", &i__1, "=", &c__3, &c__0, ok, (ftnlen)16, (
	    ftnlen)1);
    if (*ok) {
	i__1 = wncard_(result);
	for (j = 1; j <= i__1; ++j) {

/*           Fetch the endpoints of the Jth interval of the result */
/*           window. The window interval values should equal the */
/*           expected values (LHS, RHS) to within CNVTOL. */

	    wnfetd_(result, &j, &left, &right);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(msg1, "GFB use output, LHS #", (ftnlen)80, (ftnlen)21);
	    repmi_(msg1, "#", &j, msg1, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    tcase_(msg1, (ftnlen)80);
	    chcksd_(msg1, &left, "~", &lhs[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("lhs", i__2, "f_gfudb__", (ftnlen)291)], &
		    c_b35, ok, (ftnlen)80, (ftnlen)1);
	    s_copy(msg2, "GFB use output, RHS #", (ftnlen)80, (ftnlen)21);
	    repmi_(msg2, "#", &j, msg2, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    tcase_(msg2, (ftnlen)80);
	    chcksd_(msg2, &right, "~", &rhs[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("rhs", i__2, "f_gfudb__", (ftnlen)298)], &
		    c_b35, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/*     Case 4 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    step = 1.;
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfudb_((U_fp)udf_, (U_fp)gfb_, &step, cnfine, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(result, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b73, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b35);
    return 0;
} /* f_gfudb__ */

/* ---    The user defined functions required by GFUDB. */

/*      gfb    for udfunb */

/* $Procedure GFB ( Time dependent boolean function ) */
/* Subroutine */ int gfb_(U_fp udfuns, doublereal *t, logical *xbool)
{
/* $ Abstract */

/*     User defined boolean quantity function. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     None. */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     None. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 02-NOV-2011 (EDW) */

/* -& */

/*     Return false unless otherwise assigned. */

    *xbool = FALSE_;

/*     An arbitrary boolean function with known boundaries */
/*     at step event. */

/*           -----           -           ---- */
/*          |     |         | |         |    | */
/*     -----|     |--- ~ ---| |--- ~ ---|    |---- */
/*         20     30       55 57       92   100 */

    if (*t > 20. && *t < 30.) {
	*xbool = TRUE_;
    } else if (*t > 55. && *t < 57.) {
	*xbool = TRUE_;
    } else if (*t > 92. && *t < 100.) {
	*xbool = TRUE_;
    }
    return 0;
} /* gfb_ */

