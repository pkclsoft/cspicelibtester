/*

-Procedure f_gfudb_c ( Test gfudb_c )


-Abstract

   Perform tests on the CSPICE wrapper gfudb_c.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   void    gfb    ( void ( * udfuns ) ( SpiceDouble    et,
                                        SpiceDouble  * value ),
                    SpiceDouble    t, 
                    SpiceBoolean * xbool );


   void f_gfudb_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrappers for gfudb_c.

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   E.D. Wright     (JPL)

-Version

   -tspice_c Version 1.2.0, 10-FEB-2017 (EDW) (NJB)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.

       Removed unneeded declarations.       

   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 09-DEC-2011 (EDW)

-Index_Entries

   test gfudb_c

-&
*/

   { /* Begin f_gfudb_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   #define string_copy(src, dest)      strncpy( dest, src,  strlen(src) ); \
                                       dest[strlen(src)] = '\0';
   /*
   Constants
   */
   #define MAXWIN          50
   #define LNSIZE          80

   /*
   Local variables
   */

   SPICEDOUBLE_CELL      ( cnfine, 2      );
   SPICEDOUBLE_CELL      ( result, MAXWIN );

   SpiceChar               msg1[LNSIZE];
   SpiceChar               msg2[LNSIZE];

   SpiceInt                badsiz [] = { 0, 1, 3};
   SpiceInt                i;
   SpiceInt                j;

   SpiceDouble             left;
   SpiceDouble             right;
   SpiceDouble             step;

   /*
   The event boundaries as defined in gfb.
   */

   SpiceDouble             lhs []    = { 20., 55., 92. };
   SpiceDouble             rhs []    = { 30., 57., 100.};


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfudb_c" );


   /*
   Error cases
   */


   /*
   Case 1:
   */

   step   = spd_c();

   scard_c ( 0,      &result );
   scard_c ( 0,      &cnfine );

   ssize_c ( 2,         &cnfine );

   for ( i=0; i<3; i++ )
      {

      repmi_c ( "Invalid result window size #",  "#", badsiz[i], LNSIZE, msg1);
      tcase_c ( msg1 );

      ssize_c ( badsiz[i], &result );

      gfudb_c ( udf_c,
                gfb,
                step,
                &cnfine,
                &result );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDDIMENSION)", ok );

      }


   /*
   Setp appropriate sizes for the windows.
   */
   ssize_c ( MAXWIN,    &result );



   /*
   Case 2:

   Confirm non-positive step causes the expected error.
   */

   tcase_c ( "Non-positive step size." );

   step = -1.0;

   gfudb_c ( udf_c,
             gfb,
             step,
             &cnfine,
             &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );

   step = 0.;

   gfudb_c ( udf_c,
             gfb,
             step,
             &cnfine,
             &result );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );




   /*
   Standard cases.
   */


   /*
   Case 3:

   Test output from use of GFB
   */

   /*
   Store the time bounds of our search interval in
   the confinement window.
   */

   wninsd_c ( -5.0, 105.0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   step = 1.9;

   gfudb_c ( udf_c,
             gfb,
             step,
             &cnfine,
             &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   We expect 3 intervals in RESULT.
   */
   chcksi_c ( "COUNT", wncard_c( &result), "=", 3, 0,  ok );

   if (*ok)
      {

      for( j=0; j < wncard_c( &result); j++ )
         {

         /*
         Fetch the endpoints of the Jth interval of the result
         window. The window interval values should equal the
         expected values (lhs, rhs) to within SPICE_GF_CNVTOL.
         */
         wnfetd_c( &result, j, &left, &right );
         chckxc_c ( SPICEFALSE, " ", ok );

         repmi_c ( "GFB use output, LHS #", "#", j, LNSIZE, msg1);
         tcase_c ( msg1 );

         chcksd_c ( msg1, left, "~", lhs[j], SPICE_GF_CNVTOL, ok );


         repmi_c ( "GFB use output, RHS #", "#", j, LNSIZE, msg2);
         tcase_c ( msg2 );

         chcksd_c ( msg2, right, "~", rhs[j], SPICE_GF_CNVTOL, ok );

         }

      }




   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );

   } /* End f_gfudb_c */



   /*
   The user defined functions required by GFUDB.

      gfb    for udfunb
   */



   /*
   -Procedure Procedure gfb
   */

   void gfb ( void ( * udfuns ) ( SpiceDouble    et,
                                  SpiceDouble  * value ),
              SpiceDouble t, 
              SpiceBoolean * xbool )

   /*
   -Abstract

      User defined boolean quantity function.

   */
      {

      /*
      Return false unless otherwise assigned.
      */
      *xbool = SPICEFALSE;


      /*
      An arbitrary boolean function with known boundaries
      at step event.

            -----           -           ----
           |     |         | |         |    |
      -----|     |--- ~ ---| |--- ~ ---|    |----
          20     30       55 57       92    100
      */

      if ( ( t > 20.) && (t < 30.) )
         {
         *xbool = SPICETRUE;
         }
      else if ( ( t > 55.) && (t < 57.) )
         {
         *xbool = SPICETRUE;
         }
      else if ( ( t > 92.) && (t < 100.) )
         {
         *xbool = SPICETRUE;
         }

      return;
      }

