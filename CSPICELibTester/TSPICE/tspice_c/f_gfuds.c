/* f_gfuds.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__20000 = 20000;
static integer c__2 = 2;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__5 = 5;
static doublereal c_b122 = 1e-10;
static doublereal c_b203 = 1e-4;
static doublereal c_b218 = 0.;
static doublereal c_b219 = 1e-6;

/* $Procedure F_GFUDS ( GFUDS family tests ) */
/* Subroutine */ int f_gfuds__(logical *ok)
{
    /* Initialized data */

    static char relats[36*7] = "=                                   " "<    "
	    "                               " ">                             "
	    "      " "LOCMIN                              " "ABSMIN          "
	    "                    " "LOCMAX                              " 
	    "ABSMAX                              ";

    /* System generated locals */
    integer i__1, i__2;
    logical L__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal left;
    extern /* Subroutine */ int gfrr_(char *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    static doublereal step, work[100030]	/* was [20006][5] */, posr[6],
	     posu[6];
    static integer i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), gfuds_(U_fp, 
	    U_fp, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen);
    static doublereal right, drdtr;
    static char title[80];
    static doublereal drdtu;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer count, handl1, handl2;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal strtr, strtu;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    extern /* Subroutine */ int gfdecr_();
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int scardd_(integer *, doublereal *);
    static doublereal cnfine[20006];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer mw;
    extern /* Subroutine */ int gfrrdc_();
    static char abcorr[36];
    static integer nw;
    static char relate[36];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    extern integer wncard_(doublereal *);
    static doublereal refval;
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), kclear_(void);
    static char target[36];
    static doublereal fnishr, fnishu, adjust;
    extern doublereal dvnorm_(doublereal *);
    static char obsrvr[36];
    static doublereal reslud[20006], reslrr[20006];
    extern /* Subroutine */ int tstlsk_(void), furnsh_(char *, ftnlen), 
	    natspk_(char *, logical *, integer *, ftnlen), ssized_(integer *, 
	    doublereal *), tstspk_(char *, logical *, integer *, ftnlen), 
	    wninsd_(doublereal *, doublereal *, doublereal *), wnfetd_(
	    doublereal *, integer *, doublereal *, doublereal *), spkezr_(
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), gfstol_(doublereal 
	    *), delfil_(char *, ftnlen);
    static doublereal beg, end;
    extern /* Subroutine */ int gfq_();
    extern doublereal spd_(void);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        GFUDS */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB geometry routine */
/*     GFUDS. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.2.0, 28-JAN-2017 (EDW) */

/*        Modified GFSTOL test case to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.1.0, 27-NOV-2012 (EDW) */

/*        Added test on GFSTOL use. */

/* -    TSPICE Version 1.0.0, 16-FEB-2010 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved everything. */


/*     Begin every test family with an open call. */

    topen_("F_GFUDS", (ftnlen)7);
    tcase_("Setup: create and load SPK and LSK files.", (ftnlen)41);

/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load an SPK file as well. */

    tstspk_("gfuds.bsp", &c_false, &handl1, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfuds.bsp", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.bsp", &c_false, &handl2, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__20000, reslud);
    ssized_(&c__20000, reslrr);
    ssized_(&c__2, cnfine);
    scardd_(&c__0, cnfine);

/*     Create a confinement window. */

    str2et_("2007 JAN 1", &left, (ftnlen)10);
    str2et_("2007 MAY 1", &right, (ftnlen)10);
    wninsd_(&left, &right, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 1 */

    tcase_("Invalid result window size", (ftnlen)26);
    ssized_(&c__1, reslud);
    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);
    ssized_(&c__0, reslud);
    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 2 */

    tcase_("Non-positive step size", (ftnlen)22);
    ssized_(&c__20000, reslud);
    step = -1.;
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);
    step = 0.;
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSTEP)", ok, (ftnlen)18);

/*     Case 3 */

    tcase_("Invalid relations operator", (ftnlen)26);
    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "!=", (ftnlen)36, (ftnlen)2);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*     Case 4 */

    tcase_("Negative adjustment value", (ftnlen)25);
    step = spd_();
    refval = .3365;
    adjust = -1.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Case 5 */

    tcase_("Invalid value for MW, NW", (ftnlen)24);
    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    mw = 0;
    nw = 5;
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &mw, &nw, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Usable size of WORK windows is positive but below limit. */

    mw = 1;
    nw = 5;
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &mw, &nw, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Usable size of WORK windows is positive but is too small */
/*     to hold all intervals found across CNFINE. CNFINE spans */
/*     120 days, the constraint occurs approximately every */
/*     15 days. */


    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    mw = 6;
    nw = 5;
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &mw, &nw, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);

/*     Work window count below limit - NW = 4 */

    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(relate, "=", (ftnlen)36, (ftnlen)1);
    mw = 20000;
    nw = 4;
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &mw, &nw, work, reslud, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Case 6 */

    step = spd_();
    refval = .3365;
    adjust = 0.;
    s_copy(target, "301", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "10", (ftnlen)36, (ftnlen)2);
    for (i__ = 1; i__ <= 7; ++i__) {
	s_copy(relate, relats + ((i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("relats", i__1, "f_gfuds__", (ftnlen)451)) * 36, (
		ftnlen)36, (ftnlen)36);
	repmc_("GFRR vs GFUDS numerical: #", "#", relate, title, (ftnlen)26, (
		ftnlen)1, (ftnlen)36, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	scardd_(&c__0, cnfine);
	wninsd_(&left, &right, cnfine);
	gfrr_(target, abcorr, obsrvr, relate, &refval, &adjust, &step, cnfine,
		 &c__20000, &c__5, work, reslrr, (ftnlen)36, (ftnlen)36, (
		ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	scardd_(&c__0, cnfine);
	wninsd_(&left, &right, cnfine);

/*        GFDECR - wholly numerical derivative */

	gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, 
		cnfine, &c__20000, &c__5, work, reslud, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The cardinality of RESULD and RESLRR better match... */

	L__1 = (doublereal) wncard_(reslud) != 0.;
	chcksl_("COUNT != 0", &L__1, &c_true, ok, (ftnlen)10);
	i__1 = wncard_(reslud);
	i__2 = wncard_(reslrr);
	chcksi_("COUNT", &i__1, "=", &i__2, &c__0, ok, (ftnlen)5, (ftnlen)1);
	if (*ok) {
	    i__1 = wncard_(reslud);
	    for (j = 1; j <= i__1; ++j) {

/*              Fetch the endpoints of the Jth interval */
/*              of the result windows. */

		wnfetd_(reslrr, &j, &strtr, &fnishr);
		wnfetd_(reslud, &j, &strtu, &fnishu);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezr_("301", &strtu, "J2000", "NONE", "10", posu, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		spkezr_("301", &strtr, "J2000", "NONE", "10", posr, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		drdtu = dvnorm_(posu);
		drdtr = dvnorm_(posr);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_("Start", &drdtu, "~", &drdtr, &c_b122, ok, (ftnlen)5, 
			(ftnlen)1);
		spkezr_("301", &fnishu, "J2000", "NONE", "10", posu, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		spkezr_("301", &fnishr, "J2000", "NONE", "10", posr, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		drdtu = dvnorm_(posu);
		drdtr = dvnorm_(posr);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_("Finish", &drdtu, "~", &drdtr, &c_b122, ok, (ftnlen)6,
			 (ftnlen)1);
	    }
	}

/*        GFRRDC - analytic/numerical derivative */

	repmc_("GFRR vs GFUDS analytic: #", "#", relate, title, (ftnlen)25, (
		ftnlen)1, (ftnlen)36, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	gfuds_((U_fp)gfq_, (U_fp)gfrrdc_, relate, &refval, &adjust, &step, 
		cnfine, &c__20000, &c__5, work, reslud, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The cardinality of RESULD and RESLRR better match... */

	L__1 = (doublereal) wncard_(reslud) != 0.;
	chcksl_("COUNT != 0", &L__1, &c_true, ok, (ftnlen)10);
	i__1 = wncard_(reslud);
	i__2 = wncard_(reslrr);
	chcksi_("COUNT", &i__1, "=", &i__2, &c__0, ok, (ftnlen)5, (ftnlen)1);
	if (*ok) {
	    i__1 = wncard_(reslud);
	    for (j = 1; j <= i__1; ++j) {

/*              Fetch the endpoints of the Jth interval */
/*              of the result windows. The values of the range rate */
/*              should equal to within VTIGHT. */

		wnfetd_(reslrr, &j, &strtr, &fnishr);
		wnfetd_(reslud, &j, &strtu, &fnishu);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezr_("301", &strtu, "J2000", "NONE", "10", posu, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		drdtu = dvnorm_(posu);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezr_("301", &strtr, "J2000", "NONE", "10", posr, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		drdtr = dvnorm_(posr);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_("Start", &drdtu, "~", &drdtr, &c_b122, ok, (ftnlen)5, 
			(ftnlen)1);
		spkezr_("301", &fnishu, "J2000", "NONE", "10", posu, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		drdtu = dvnorm_(posu);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezr_("301", &fnishr, "J2000", "NONE", "10", posr, &lt, (
			ftnlen)3, (ftnlen)5, (ftnlen)4, (ftnlen)2);
		drdtr = dvnorm_(posr);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksd_("Finish", &drdtu, "~", &drdtr, &c_b122, ok, (ftnlen)6,
			 (ftnlen)1);
	    }
	}
    }

/*     Case 7 */

    s_copy(title, "Check the GF call uses the GFSTOL value", (ftnlen)80, (
	    ftnlen)39);
    tcase_(title, (ftnlen)80);

/*     Re-run a valid search after using GFSTOL to set the convergence */
/*     tolerance to a value that should cause a numerical error signal. */

    step = spd_();
    refval = 0.;
    adjust = 0.;
    s_copy(target, "301", (ftnlen)36, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "10", (ftnlen)36, (ftnlen)2);
    s_copy(relate, "ABSMIN", (ftnlen)36, (ftnlen)6);
    scardd_(&c__0, reslud);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(reslud);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(reslud, &c__1, &beg, &end);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scardd_(&c__0, reslud);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Reset tol. */

    gfstol_(&c_b203);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gfuds_((U_fp)gfq_, (U_fp)gfdecr_, relate, &refval, &adjust, &step, cnfine,
	     &c__20000, &c__5, work, reslud, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    count = 0;
    count = wncard_(reslud);
    chcksi_("COUNT", &count, "!=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)2);
    wnfetd_(reslud, &c__1, &left, &right);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The values in the time window should not match */
/*     as the search used different tolerances. Check */
/*     the first value in the first interval. */

    chcksd_(title, &beg, "!=", &left, &c_b218, ok, (ftnlen)80, (ftnlen)2);

/*     Reset the convergence tolerance. */

    gfstol_(&c_b219);

/*     Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("gfuds.bsp", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_gfuds__ */


/* ---    The user defined functions required by GFUDS. */

/*      gfq    for udfunc */
/*      gfdecr for udqdec */
/*      gfrrdc for udqdec */

/* $Procedure GFQ ( Scalar function - range rate ) */
/* Subroutine */ int gfq_(doublereal *et, doublereal *value)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    integer targ;
    doublereal state[6];
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal lt;
    char abcorr[12];
    extern doublereal dvnorm_(doublereal *);
    char ref[12];
    integer obs;

/* $ Abstract */

/*     User defined geometric quantity function. In this case, */
/*     the range from the sun to the Moon at TDB time ET. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None. */

/* $ Keywords */

/*   None. */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */

/* $ Detailed_Input */

/*   None. */

/* $ Detailed_Output */

/*   None. */

/* $ Parameters */

/*   None. */

/* $ Exceptions */

/*   None. */

/* $ Files */

/*   None. */

/* $ Particulars */

/*   None. */

/* $ Examples */

/*   None. */

/* $ Restrictions */

/*   None. */

/* $ Literature_References */

/*   None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 04-OCT-2009 (EDW) */

/* -& */

/*     Local variables. */


/*     Initialization. Retrieve the vector from the Sun to */
/*     the Moon in the J2000 frame, without aberration */
/*     correction. */

    targ = 301;
    s_copy(ref, "J2000", (ftnlen)12, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)12, (ftnlen)4);
    obs = 10;
    spkez_(&targ, et, ref, abcorr, &obs, state, &lt, (ftnlen)12, (ftnlen)12);

/*     Calculate the scalar range rate corresponding the */
/*     STATE vector. */

    *value = dvnorm_(state);
    return 0;
} /* gfq_ */

/* $Procedure GFDECR ( Derivative numeric ) */
/* Subroutine */ int gfdecr_(U_fp udfunc, doublereal *et, logical *bool)
{
    extern /* Subroutine */ int uddc_(U_fp, doublereal *, doublereal *, 
	    logical *);
    doublereal dt;

/* $ Abstract */

/*     User defined function to detect if the function derivative */
/*     is negative (the function is decreasing) at TDB time ET. */
/*     Numeric. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None. */

/* $ Keywords */

/*   None. */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */

/* $ Detailed_Input */

/*   None. */

/* $ Detailed_Output */

/*   None. */

/* $ Parameters */

/*   None. */

/* $ Exceptions */

/*   None. */

/* $ Files */

/*   None. */

/* $ Particulars */

/*   None. */

/* $ Examples */

/*   None. */

/* $ Restrictions */

/*   None. */

/* $ Literature_References */

/*   None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 04-OCT-2009 (EDW) */

/* -& */
    dt = 1.;

/*     Determine if GFQ is increasing or decreasing at ET. */

/*     UDDC - the SPICE function to determine if */
/*            the derivative of the user defined */
/*            function is negative at ET. */

/*     UDFUNC - the user defined scalar quantity function. */

    uddc_((U_fp)udfunc, et, &dt, bool);
    return 0;
} /* gfdecr_ */

/* $Procedure GFRRDC ( Derivative analytic/numeric ) */
/* Subroutine */ int gfrrdc_(U_fp udfunc, doublereal *et, logical *decres)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    doublereal dfdt[6];
    integer targ;
    extern doublereal vdot_(doublereal *, doublereal *);
    integer n;
    extern /* Subroutine */ int dvhat_(doublereal *, doublereal *);
    doublereal drvel, state[6], srhat[6];
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal state1[6], state2[6], dt, lt;
    char abcorr[12];
    extern /* Subroutine */ int qderiv_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *);
    char ref[12];
    integer obs;

/* $ Abstract */

/*     User defined function to detect if the function derivative */
/*     is negative (the function is decreasing) at TDB time ET. */
/*     Analytic/numeric. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None. */

/* $ Keywords */

/*   None. */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */

/* $ Detailed_Input */

/*   None. */

/* $ Detailed_Output */

/*   None. */

/* $ Parameters */

/*   None. */

/* $ Exceptions */

/*   None. */

/* $ Files */

/*   None. */

/* $ Particulars */

/*   None. */

/* $ Examples */

/*   None. */

/* $ Restrictions */

/*   None. */

/* $ Literature_References */

/*   None. */

/* $ Author_and_Institution */

/*     E. D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 04-OCT-2009 (EDW) */

/* -& */

/*     SPICELIB functions. */

    dt = 1.;
    n = 6;
    targ = 301;
    s_copy(ref, "J2000", (ftnlen)12, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)12, (ftnlen)4);
    obs = 10;
    d__1 = *et - dt;
    spkez_(&targ, &d__1, ref, abcorr, &obs, state1, &lt, (ftnlen)12, (ftnlen)
	    12);
    d__1 = *et + dt;
    spkez_(&targ, &d__1, ref, abcorr, &obs, state2, &lt, (ftnlen)12, (ftnlen)
	    12);

/*     Approximate the derivative of the position and velocity by */
/*     finding the derivative of a quadratic approximating function. */

/*        DFDT(1) = Vx */
/*        DFDT(2) = Vy */
/*        DFDT(3) = Vz */
/*        DFDT(4) = Ax */
/*        DFDT(5) = Ay */
/*        DFDT(6) = Az */

    qderiv_(&n, state1, state2, &dt, dfdt);
    spkez_(&targ, et, ref, abcorr, &obs, state, &lt, (ftnlen)12, (ftnlen)12);

/*        d ||r||     ^ */
/*        ------- = < r, v > */
/*        dt */

/*         2            ^          ^ */
/*        d ||r||   < d r, v > + < r, d v > */
/*        ------- =   ---             --- */
/*          2 */
/*        dt          dt              dt */

    dvhat_(state, srhat);
    drvel = vdot_(srhat, &dfdt[3]) + vdot_(&srhat[3], &state[3]);
    *decres = drvel < 0.;
    return 0;
} /* gfrrdc_ */

