/*

-Procedure f_gfuds_c ( Test gfuds_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper gfuds_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   void    gfq    ( SpiceDouble et, SpiceDouble * value );

   void    gfdecr ( void ( * udfunc ) ( SpiceDouble    et,
                                        SpiceDouble  * value ),
                    SpiceDouble    et, 
                    SpiceBoolean * xbool );

   void    gfrrdc ( void ( * udfunc ) ( SpiceDouble    et,
                                        SpiceDouble  * value ),
                    SpiceDouble    et, 
                    SpiceBoolean * xbool );

   void f_gfuds_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for gfudb_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution

   E.D. Wright     (JPL)
 
-Version

   -tspice_c Version 1.2.0, 08-FEB-2017 (EDW)

       Removed gfstol_c test case to correspond to change
       in ZZGFSOLVX.
 
   -tspice_c Version 1.1.0, 20-NOV-2012 (EDW)

        Added test on GFSTOL use.

   -tspice_c Version 1.0.0 16-FEB-2010 (EDW)

-Index_Entries

   test gfuds_c

-&
*/

{ /* Begin f_gfuds_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   #define string_copy(src, dest)      strncpy( dest, src,  strlen(src) ); \
                                       dest[strlen(src)] = '\0';
   /*
   Constants
   */
   #define SPK             "gfuds.bsp" 
   #define LSK             "gfuds.tls"
   #define SPK1            "nat.bsp" 
   #define PCK1            "nat.pck"
   #define VTIGHT          1.e-10
   #define LNSIZE          80 
   #define MAXWIN          20000
   #define NLOOPS          7 

   /*
   Local variables
   */

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN );
   SPICEDOUBLE_CELL      ( reslud,   MAXWIN );
   SPICEDOUBLE_CELL      ( reslrr,   MAXWIN );

   SpiceDouble             drdtr;
   SpiceDouble             drdtu;
   SpiceDouble             lt;
   SpiceDouble             adjust;
   SpiceDouble             refval;
   SpiceDouble             step;
   SpiceDouble             left;
   SpiceDouble             right;
   SpiceDouble             strtr;
   SpiceDouble             strtu;
   SpiceDouble             fnishr;
   SpiceDouble             fnishu;
   SpiceDouble             posu [6];
   SpiceDouble             posr [6];

   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                nintvls;
   SpiceInt                han1;

   SpiceChar               title  [LNSIZE*2];
   SpiceChar               relate [LNSIZE];
   SpiceChar               target [LNSIZE];
   SpiceChar               abcorr [LNSIZE];
   SpiceChar               obsrvr [LNSIZE];

   SpiceChar             * relates[] = { "=", 
                                        "<", 
                                        ">",
                                        "LOCMIN",
                                        "ABSMIN",
                                        "LOCMAX",  
                                        "ABSMAX"
                                        };

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_gfuds_c" );


   tcase_c ( "Setup: create and load SPK and LSK files." );
   
   /*
   Leapseconds, load using furnsh_c.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create the PCK for Nat's Solar System.
   */
   natpck_( PCK1, (logical *) &false_, 
                  (logical *) &true_, 
                  (ftnlen) strlen(PCK1) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create the SPK for Nat's Solar System.
   */
   natspk_( SPK1, (logical *) &false_, 
                  &han1, 
                  (ftnlen) strlen(SPK1) ); 
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create a confinement window.
   */
   str2et_c( "2007 JAN 1", &left );
   str2et_c( "2007 MAY 1", &right );
   wninsd_c ( left, right, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   Error cases
   */


   /*
   Case 1
   */
   tcase_c ( "Invalid result window size" );
 
   ssize_c( 1, &reslud);

   step    = spd_c();
   refval  = 0.33650;
   adjust  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "=",  relate);
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDDIMENSION)", ok );
 

   /*
   Case 2
   */
   tcase_c ( "Non-positive step size" );
 
   ssize_c( MAXWIN, &reslud);

   step    = -1.;
   refval  = 0.33650;
   adjust  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "=",  relate);
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );
  
   step    = -1.;

   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(INVALIDSTEP)", ok );
   
   

   /*
   Case 3
   */
   tcase_c ( "Invalid relations operator" );
 
   step    = spd_c();
   refval  = 0.33650;
   adjust  = 0.;
   nintvls = MAXWIN/2;
   string_copy( "!=",  relate);
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(NOTRECOGNIZED)", ok );


   /*
   Case 4
   */
   tcase_c ( "Negative adjustment value" );
 
   step    = spd_c();
   refval  = 0.33650;
   adjust  = -1.;
   nintvls = MAXWIN/2;
   string_copy( "=",  relate);
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );
   


   /*
   Case 5
   */
   tcase_c ( "Invalid value for nintvls" );
 
   step    = spd_c();
   refval  = 0.33650;
   adjust  = 0.;
   nintvls = 0;
   string_copy( "=",  relate);
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /*
   Usable size of work windows is positive but is too small
   to hold all intervals found across 'cnfine'. 'cnfine' spans 
   360 days, the local maximums occur approximately every 
   28 days.
   */

   nintvls = 3;
   
   gfuds_c ( gfq, 
             gfdecr,
             relate,
             refval,
             adjust,
             step,
             nintvls,
             &cnfine,
             &reslud );

   chckxc_c ( SPICETRUE, "SPICE(WINDOWEXCESS)", ok );


   /*
   Case 5
   */
 
   step    = spd_c();
   refval  = 0.33650;
   adjust  = 0.;
   nintvls = MAXWIN/2.;
   string_copy( "301",  target );
   string_copy( "NONE", abcorr );
   string_copy( "10",   obsrvr );
   
   for (i=0; i<NLOOPS; i++ )
      {
      string_copy( relates[i],  relate);

      scard_c  ( 0, &cnfine );
      wninsd_c ( left, right, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      gfrr_c ( target,
               abcorr,
               obsrvr,
               relate,
               refval,
               adjust,
               step,
               nintvls,
               &cnfine,
               &reslrr );

      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      GFDECR - wholly numerical derivative
      */
      repmc_c ( "GFRR vs GFUDS numerical: #", "#", relate, LNSIZE, title );
      tcase_c ( title );

      scard_c  ( 0, &cnfine );
      wninsd_c ( left, right, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      gfuds_c ( gfq, 
                gfdecr,
                relate,
                refval,
                adjust,
                step,
                nintvls,
               &cnfine,
               &reslud );

      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      The cardinality of RESULD and RESLRR better match...
      */
      chcksl_c ( "count != 0", (SpiceBoolean)( wncard_c(&reslrr) != 0),  
                               SPICETRUE, ok );

      chcksi_c ( "count", wncard_c(&reslrr), "=", 
                          wncard_c(&reslud), 0, ok );
                          
      if ( ok )
         {
         for (j=0; j < wncard_c( &reslud ); j++) 
            {
            
            /*
            Fetch the endpoints of the Jth interval
            of the result windows. The values of the range rate
            should equal to within VTIGHT.
            */

            wnfetd_c ( &reslrr, j, &strtr, &fnishr);
            wnfetd_c ( &reslud, j, &strtu, &fnishu);
            chckxc_c ( SPICEFALSE, " ", ok );

            spkezr_c ( target, strtr, "J2000", abcorr, obsrvr, posr, &lt );
            spkezr_c ( target, strtu, "J2000", abcorr, obsrvr, posu, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );
            
            drdtr = dvnorm_c( posr );
            drdtu = dvnorm_c( posu );
            chckxc_c ( SPICEFALSE, " ", ok );

            chcksd_c ( "Start", drdtr, "~", drdtu, VTIGHT, ok );


            spkezr_c ( target, fnishr, "J2000", abcorr, obsrvr, posr, &lt );
            spkezr_c ( target, fnishu, "J2000", abcorr, obsrvr, posu, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );
            
            drdtr = dvnorm_c( posr );
            drdtu = dvnorm_c( posu );
            chckxc_c ( SPICEFALSE, " ", ok );

            chcksd_c ( "Finish", drdtr, "~", drdtu, VTIGHT, ok );

            }
         
         
         }



      /*
      GFRRDC - analytic/numerical derivative
      */
      string_copy( relates[i],  relate);
      repmc_c ( "GFRR vs GFUDS analytic: #", "#", relate, LNSIZE, title );
      tcase_c ( title );

      scard_c  ( 0, &cnfine );
      wninsd_c ( left, right, &cnfine );
      chckxc_c ( SPICEFALSE, " ", ok );

      gfuds_c ( gfq, 
                gfrrdc,
                relate,
                refval,
                adjust,
                step,
                nintvls,
               &cnfine,
               &reslud );

      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      The cardinality of RESULD and RESLRR better match...
      */
      chcksl_c ( "count != 0", (SpiceBoolean)( wncard_c(&reslrr) != 0),  
                               SPICETRUE, ok );

      chcksi_c ( "count", wncard_c(&reslrr), "=", 
                          wncard_c(&reslud), 0, ok );
                          
      if ( ok )
         {
         for (j=0; j < wncard_c( &reslud ); j++) 
            {
            
            /*
            Fetch the endpoints of the Jth interval
            of the result windows. The values of the range rate
            should equal to within VTIGHT.
            */

            wnfetd_c ( &reslrr, j, &strtr, &fnishr);
            wnfetd_c ( &reslud, j, &strtu, &fnishu);
            chckxc_c ( SPICEFALSE, " ", ok );

            spkezr_c ( target, strtr, "J2000", abcorr, obsrvr, posr, &lt );
            spkezr_c ( target, strtu, "J2000", abcorr, obsrvr, posu, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );
            
            drdtr = dvnorm_c( posr );
            drdtu = dvnorm_c( posu );
            chckxc_c ( SPICEFALSE, " ", ok );

            chcksd_c ( "Start", drdtr, "~", drdtu, VTIGHT, ok );


            spkezr_c ( target, fnishr, "J2000", abcorr, obsrvr, posr, &lt );
            spkezr_c ( target, fnishu, "J2000", abcorr, obsrvr, posu, &lt );
            chckxc_c ( SPICEFALSE, " ", ok );
            
            drdtr = dvnorm_c( posr );
            drdtu = dvnorm_c( posu );
            chckxc_c ( SPICEFALSE, " ", ok );

            chcksd_c ( "Finish", drdtr, "~", drdtu, VTIGHT, ok );

            }

         }

      }




   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK1 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_gfuds_c */



   /*
   --- The user defined functions required by GFUDS.
      
      gfq    for udfunc
      gfdecr for udqdec
      gfrrdc for udqdec
   */



   /*
   -Procedure Procedure gfq
   */

   void gfq ( SpiceDouble et, SpiceDouble * value )

   /*
   -Abstract

      User defined geometric quantity function. In this case,
      the range from the sun to the Moon at TDB time 'et'.
   
   */
      {
      
      /* Initialization */
      SpiceInt             targ   = 301;
      SpiceInt             obs    = 10;

      SpiceChar          * ref    = "J2000";
      SpiceChar          * abcorr = "NONE";

      SpiceDouble          state [6];
      SpiceDouble          lt;

      /*
      Retrieve the vector from the Sun to the Moon in the J2000 
      frame, without aberration correction.
      */
      spkez_c ( targ, et, ref, abcorr, obs, state, &lt );

      /*
      Calculate the scalar range rate corresponding the
     'state' vector.   
      */
      *value = dvnorm_c( state );

      return;
      }



   /*
   -Procedure gfdecr
   */
   
   void gfdecr ( void ( * udfunc ) ( SpiceDouble    et,
                                     SpiceDouble  * value ),
                 SpiceDouble    et, 
                 SpiceBoolean * xbool )

   /*
   -Abstract

      User defined function to detect if the function derivative
      is negative (the function is decreasing) at TDB time 'et'.
      Calculation of the range rate derivative use a numerical function. 
   */
      {
         
      SpiceDouble         dt = 1.;

      /*
      Determine if "gfq" is decreasing at 'et'.

      uddc_c - the default GF function to determine if
               the derivative of the user defined
               function is negative at 'et'.

      gfq  - the user defined scalar quantity function.
      */
      uddc_c( udfunc, et, dt, xbool );

      return;
      }




   /*
   -Procedure gfrrdc ( Derivative analytic/numeric )
   */
   void gfrrdc ( void ( * udfunc ) ( SpiceDouble    et,
                                     SpiceDouble  * value ),
                 SpiceDouble    et, 
                 SpiceBoolean * xbool )

   /*
   -Abstract

      User defined function to detect if the function derivative
      is negative (the function is decreasing) at TDB time 'et'.
      Calculation of the range rate derivative use an analytic function. 
      Calculation of the observer to target acceleration magnitude 
      uses a numerical function.
      
      Note that this function does not use 'udfunc'.
   */
      {
      
      /* Initialization */
      SpiceInt             targ   = 301;
      SpiceInt             obs    = 10;
      SpiceInt             n      = 6;

      SpiceChar          * ref    = "J2000";
      SpiceChar          * abcorr = "NONE";

      SpiceDouble          lt;
      SpiceDouble          drvel;
      SpiceDouble          dt     = 1.;
      SpiceDouble          state   [6];
      SpiceDouble          states1 [6];
      SpiceDouble          states2 [6];
      SpiceDouble          dfdt    [6];
      SpiceDouble          srhat   [6];

      spkez_c ( targ, et-dt, ref, abcorr, obs, states1, &lt );
      spkez_c ( targ, et+dt, ref, abcorr, obs, states2, &lt );

      /*
      Approximate the derivative of the position and velocity by 
      finding the derivative of a quadratic approximating function.

         DFDT[0] = Vx
         DFDT[1] = Vy
         DFDT[2] = Vz
         DFDT[3] = Ax
         DFDT[4] = Ay
         DFDT[5] = Az
      */
      (void) qderiv_( (integer    *) &n, 
                      (doublereal *) states1, 
                      (doublereal *) states2, 
                      (doublereal *) &dt, 
                      (doublereal *) dfdt );

      spkez_c ( targ, et, ref, abcorr, obs, state, &lt );

      /*
         d ||r||     ^
         ------- = < r, v >
         dt

          2            ^          ^
         d ||r||   < d r, v > + < r, d v >
         ------- =   ---             ---
           2         
         dt          dt              dt
      */

      dvhat_c ( state, srhat );
      drvel  = vdot_c ( srhat, &(dfdt[3]) ) + vdot_c ( &(srhat[3]), 
                                                       &(state[3]) );

      *xbool = drvel < 0; 

      return;
      }




