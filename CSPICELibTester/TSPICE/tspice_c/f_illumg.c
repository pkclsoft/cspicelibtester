/* f_illumg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c_n499 = -499;
static integer c__499 = 499;
static integer c__1 = 1;
static integer c_n666 = -666;
static integer c__3 = 3;
static doublereal c_b69 = 2.;
static doublereal c_b101 = 3.;
static integer c__4 = 4;
static integer c__14 = 14;
static doublereal c_b223 = 0.;
static integer c__599 = 599;
static integer c__399 = 399;
static integer c__10 = 10;
static integer c__9 = 9;
static integer c__8 = 8;
static doublereal c_b550 = -1.;

/* $Procedure      F_ILLUMG ( ILLUMG family tests ) */
/* Subroutine */ int f_illumg__(logical *ok)
{
    /* Initialized data */

    static char abcs[10*5] = "None      " "Lt        " "Lt+s      " "XCn    "
	    "   " "Cn+s      ";
    static char srcs[32*5] = "10                              " "Venus baryc"
	    "enter                " "Jupiter barycenter              " "Satur"
	    "n barycenter               " "Uranus barycenter               ";
    static char refs[32*1*2] = "IAU_MARS                        " "IAU_PHOBO"
	    "S                      ";
    static char obsnms[32*2] = "Earth                           " "MARS_ORBI"
	    "TER                    ";
    static char trgnms[32*2] = "Mars                            " "PHOBOS   "
	    "                       ";
    static char methds[500*4] = "ELLIPSOID                                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "         " "dsk/unprioritized/surfaces=\"high-res\"             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "  " "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                           " 
	    "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                       ";

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static doublereal dlat, dlon;
    static integer nlat, nlon;
    static doublereal elts[8];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    extern doublereal vsep_(doublereal *, doublereal *);
    static integer npts;
    extern /* Subroutine */ int zzcorepc_(char *, doublereal *, doublereal *, 
	    doublereal *, ftnlen);
    static integer i__, j, k, n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal phase;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found, usecn;
    static doublereal state[6];
    static char title[240];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer ptidx;
    static doublereal ilult;
    extern /* Subroutine */ int spkw05_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    static logical uselt;
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *);
    static doublereal state0[6], badrad[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen), boddef_(char *, integer *,
	     ftnlen);
    extern doublereal pi_(void);
    static doublereal et;
    static integer abcidx, handle[2];
    static doublereal incdnc, lt;
    static integer obscde;
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     delfil_(char *, ftnlen);
    static doublereal altrad[3];
    static char abcorr[10];
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    static doublereal xincdc;
    static integer bodyid, trgcde;
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen);
    static char kvname[32], fixref[32], ilusrc[32], method[500], obsrvr[32];
    extern logical exists_(char *, ftnlen);
    static char srfnms[32*4], target[32], trgfrm[32];
    static doublereal emissn, et0, ilusta[6], lonlat[200]	/* was [2][
	    100] */, normal[3], obspos[3], spoint[3], srfvec[3], trgepc, 
	    xemisn, xepoch, xphase, xsrfvc[3];
    static integer nsflat, nsflon, obsidx, refidx, srfbod[4], srfids[4], 
	    surfid, trgidx;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     tstlsk_(void), spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), conics_(doublereal *, doublereal *, doublereal *)
	    , spkcls_(integer *), spklef_(char *, integer *, ftnlen), pcpool_(
	    char *, integer *, char *, ftnlen, ftnlen), pipool_(char *, 
	    integer *, integer *, ftnlen), furnsh_(char *, ftnlen), gdpool_(
	    char *, integer *, integer *, integer *, doublereal *, logical *, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen),
	     pdpool_(char *, integer *, doublereal *, ftnlen), bodvar_(
	    integer *, char *, integer *, doublereal *, ftnlen), latsrf_(char 
	    *, char *, doublereal *, char *, integer *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen), srfnrm_(char *, char *, 
	    doublereal *, char *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen), illumg_(char *, char *, char *, 
	    doublereal *, char *, char *, char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), spkcpt_(doublereal *, 
	    char *, char *, doublereal *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, doublereal 
	    *, doublereal *, logical *, ftnlen, ftnlen), vminus_(doublereal *,
	     doublereal *), spkcpo_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), unload_(char *, 
	    ftnlen), spkuef_(integer *), clpool_(void);
    static doublereal lat;
    extern /* Subroutine */ int ldpool_(char *, ftnlen), dvpool_(char *, 
	    ftnlen), bodvcd_(integer *, char *, integer *, integer *, 
	    doublereal *, ftnlen);
    extern doublereal dpr_(void), rpd_(void);
    static doublereal lon;
    static char utc[50];
    static integer mix;
    static doublereal tol, xte;

/* $ Abstract */

/*     Exercise the higher-level SPICELIB geometry routine ILLUMG. */
/*     Use DSK-based and ellipsoidal target shape models. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine ILLUMG. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 3.0.0 15-AUG-2016 (NJB) */

/*        Updated to test DSK usage. Also more thoroughly */
/*        tests ellipsoid target cases. */

/* -    TSPICE Version 2.1.0, 27-AUG-2013 (NJB) */

/*        Loosened TIGHT tolerance 1.D-11. */

/* -    TSPICE Version 2.0.0, 22-FEB-2011 (NJB) */

/*        Loosened MED tolerance parameter to 1.D-8. This */
/*        parameter is used for tests involving 'LT' */
/*        light time corrections. */

/* -    TSPICE Version 1.0.0, 27-FEB-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     The illumination sources are going to piggyback */
/*     on the aberration corrections: we'll use a */
/*     different source object for each correction */
/*     in the main loop. We'll avoid an extra nesting */
/*     level in this way. */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     REFS is a two-dimensional array. There's a set of */
/*     ray reference  frames for each target. Currently */
/*     there are only two targets: Mars and Phobos. */


/*     Note that the last two method strings are identical. This */
/*     is done to test the logic that uses saved values obtained */
/*     by parsing method string. */


/*     Begin every test family with an open call. */

    topen_("F_ILLUMG", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create SPK, PCK file.", (ftnlen)29);
    tstspk_("illumg_spk.bsp", &c_true, handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK file, and load it. Do not delete it. */

    t_pck08__("test_0008.tpc", &c_true, &c_true, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create LSK, load it, and delete it. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set initial time. */

    s_copy(utc, "2004 FEB 17", (ftnlen)50, (ftnlen)11);
    str2et_(utc, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = et0;

/*     Create a Mars orbiter SPK file. */

    spkopn_("orbiter.bsp", "orbiter.bsp", &c__0, &handle[1], (ftnlen)11, (
	    ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up elements defining a state.  The elements expected */
/*     by CONICS are: */

/*        RP      Perifocal distance. */
/*        ECC     Eccentricity. */
/*        INC     Inclination. */
/*        LNODE   Longitude of the ascending node. */
/*        ARGP    Argument of periapse. */
/*        M0      Mean anomaly at epoch. */
/*        T0      Epoch. */
/*        MU      Gravitational parameter. */

    elts[0] = 3800.;
    elts[1] = .1;
    elts[2] = rpd_() * 80.;
    elts[3] = 0.;
    elts[4] = rpd_() * 90.;
    elts[5] = 0.;
    elts[6] = et;
    elts[7] = 42828.314;
    conics_(elts, &et, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = jyear_() * -10;
    d__2 = jyear_() * 10;
    spkw05_(&handle[1], &c_n499, &c__499, "MARSIAU", &d__1, &d__2, "Mars orb"
	    "iter", &elts[7], &c__1, state0, &et, (ftnlen)7, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the new SPK file. */

    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add the orbiter's name/ID mapping to the kernel pool. */

    pcpool_("NAIF_BODY_NAME", &c__1, obsnms + 32, (ftnlen)14, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_BODY_CODE", &c__1, &c_n499, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add an incomplete frame definition to the kernel pool; */
/*     we'll need this later. */

    pipool_("FRAME_BAD_NAME", &c__1, &c_n666, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 200;
    nlat = 100;
    if (exists_("illumg_dsk0.bds", (ftnlen)15)) {
	delfil_("illumg_dsk0.bds", (ftnlen)15);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "illumg_dsk0.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars is very low-res. We also use a */
/*     different scale for the Mars radii used to create */
/*     the tessellated shape model. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 40;
    nlat = 20;
    if (exists_("illumg_dsk1.bds", (ftnlen)15)) {
	delfil_("illumg_dsk1.bds", (ftnlen)15);
    }
    s_copy(kvname, "BODY499_RADII", (ftnlen)32, (ftnlen)13);
    gdpool_(kvname, &c__1, &c__3, &n, radii, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("499 radii FOUND", &found, &c_true, ok, (ftnlen)15);
    vscl_(&c_b69, radii, altrad);
    pdpool_(kvname, &c__3, altrad, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "illumg_dsk1.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("illumg_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore normal Mars radii. */

    pdpool_(kvname, &c__3, radii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is low-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("illumg_dsk2.bds", (ftnlen)15)) {
	delfil_("illumg_dsk2.bds", (ftnlen)15);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "illumg_dsk2.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("illumg_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. We also use a */
/*     different scale for the Mars radii used to create */
/*     the tessellated shape model. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("illumg_dsk3.bds", (ftnlen)15)) {
	delfil_("illumg_dsk3.bds", (ftnlen)15);
    }
    s_copy(kvname, "BODY401_RADII", (ftnlen)32, (ftnlen)13);
    gdpool_(kvname, &c__1, &c__3, &n, radii, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("401 radii FOUND", &found, &c_true, ok, (ftnlen)15);
    vscl_(&c_b101, radii, altrad);
    pdpool_(kvname, &c__3, altrad, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "illumg_dsk3.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore normal Phobos radii. */

    pdpool_(kvname, &c__3, radii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the DSK. */

    furnsh_("illumg_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create surface map.", (ftnlen)26);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 401;
    srfids[2] = 1;
    s_copy(srfnms + 64, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[3] = 401;
    srfids[3] = 2;
    s_copy(srfnms + 96, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__4, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__4, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate a grid of planetocentric longitude/latitude */
/*     coordinate pairs. These, combined with surface models, */
/*     will yield a grid of surface points at which to */
/*     compute illumination angles. */

    nsflon = 4;
    nsflat = 5;
    npts = nsflon * nsflat;
    dlon = pi_() * 2 / nsflon;
    dlat = pi_() / (nsflat - 1);
    k = 0;
    i__1 = nsflon;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        We shift the coordinates away from possible plate */
/*        edges because we can't expect the normal vectors */
/*        to match our computed values at those locations. */

	lon = (i__ - 1) * dlon + .001;
	i__2 = nsflat;
	for (j = 1; j <= i__2; ++j) {
	    d__1 = halfpi_() - (j - 1) * dlat;
	    d__2 = -halfpi_();
	    d__3 = halfpi_();
	    lat = brcktd_(&d__1, &d__2, &d__3);
	    if (j == 1) {
		lat += -.001;
	    } else if (j == nsflat) {
		lat += .001;
	    }
	    ++k;
	    lonlat[(i__3 = (k << 1) - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge(
		    "lonlat", i__3, "f_illumg__", (ftnlen)662)] = lon;
	    lonlat[(i__3 = (k << 1) - 1) < 200 && 0 <= i__3 ? i__3 : s_rnge(
		    "lonlat", i__3, "f_illumg__", (ftnlen)663)] = lat;
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     Main test loop follows. */


/*     Loop over every choice of observer. */

    for (obsidx = 1; obsidx <= 2; ++obsidx) {
	s_copy(obsrvr, obsnms + (((i__1 = obsidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("obsnms", i__1, "f_illumg__", (ftnlen)684)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the observer ID code. */

	bodn2c_(obsrvr, &obscde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over every choice of target. */

	for (trgidx = 1; trgidx <= 2; ++trgidx) {
	    s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? 
		    i__1 : s_rnge("trgnms", i__1, "f_illumg__", (ftnlen)696)) 
		    << 5), (ftnlen)32, (ftnlen)32);

/*           Set the target ID code. */

	    bodn2c_(target, &trgcde, &found, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Get target radii. */

	    bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over the surface point sequence. */

	    i__1 = npts;
	    for (ptidx = 1; ptidx <= i__1; ++ptidx) {
		lon = lonlat[(i__2 = (ptidx << 1) - 2) < 200 && 0 <= i__2 ? 
			i__2 : s_rnge("lonlat", i__2, "f_illumg__", (ftnlen)
			715)];
		lat = lonlat[(i__2 = (ptidx << 1) - 1) < 200 && 0 <= i__2 ? 
			i__2 : s_rnge("lonlat", i__2, "f_illumg__", (ftnlen)
			716)];

/*              Loop over every aberration correction choice. */

		for (abcidx = 1; abcidx <= 5; ++abcidx) {
		    s_copy(abcorr, abcs + ((i__2 = abcidx - 1) < 5 && 0 <= 
			    i__2 ? i__2 : s_rnge("abcs", i__2, "f_illumg__", (
			    ftnlen)723)) * 10, (ftnlen)10, (ftnlen)10);

/*                 Set up some logical variables describing the */
/*                 attributes of the selected correction. */

		    uselt = s_cmp(abcorr, "None", (ftnlen)10, (ftnlen)4) != 0;
		    usecn = s_cmp(abcorr, "Cn", (ftnlen)2, (ftnlen)2) == 0;

/*                 Here's where the illumination source is set. */

		    s_copy(ilusrc, srcs + (((i__2 = abcidx - 1) < 5 && 0 <= 
			    i__2 ? i__2 : s_rnge("srcs", i__2, "f_illumg__", (
			    ftnlen)735)) << 5), (ftnlen)32, (ftnlen)32);

/*                 Loop over every target body-fixed frame choice. */

		    for (refidx = 1; refidx <= 1; ++refidx) {
			s_copy(trgfrm, refs + (((i__2 = refidx + trgidx - 2) <
				 2 && 0 <= i__2 ? i__2 : s_rnge("refs", i__2, 
				"f_illumg__", (ftnlen)743)) << 5), (ftnlen)32,
				 (ftnlen)32);

/*                    Loop over all method choices. */

			for (mix = 1; mix <= 4; ++mix) {
			    s_copy(method, methds + ((i__2 = mix - 1) < 4 && 
				    0 <= i__2 ? i__2 : s_rnge("methds", i__2, 
				    "f_illumg__", (ftnlen)751)) * 500, (
				    ftnlen)500, (ftnlen)500);

/* --- Case: ------------------------------------------------------ */

			    s_copy(title, "Observer = #; Target = #; ILUSRC "
				    "= #; ABCORR = #; TRGFRM = #; METHOD = #;"
				    " Longitude (deg) = #; Latitude (deg) = #"
				    "; ET = #.", (ftnlen)240, (ftnlen)122);
			    repmc_(title, "#", obsrvr, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", target, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", ilusrc, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", abcorr, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)10, (ftnlen)240);
			    repmc_(title, "#", trgfrm, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", method, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)500, (ftnlen)240);
			    d__1 = lon * dpr_();
			    repmd_(title, "#", &d__1, &c__14, title, (ftnlen)
				    240, (ftnlen)1, (ftnlen)240);
			    d__1 = lat * dpr_();
			    repmd_(title, "#", &d__1, &c__14, title, (ftnlen)
				    240, (ftnlen)1, (ftnlen)240);
			    repmd_(title, "#", &et, &c__14, title, (ftnlen)
				    240, (ftnlen)1, (ftnlen)240);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    tcase_(title, (ftnlen)240);

/*                       Generate the surface point we're going to work */
/*                       with. We do this here because we need the */
/*                       current method in order to generate a point on */
/*                       the surface. */

			    latsrf_(method, target, &et, trgfrm, &c__1, &
				    lonlat[(i__2 = (ptidx << 1) - 2) < 200 && 
				    0 <= i__2 ? i__2 : s_rnge("lonlat", i__2, 
				    "f_illumg__", (ftnlen)780)], spoint, (
				    ftnlen)500, (ftnlen)32, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*                       Get the outward surface normal vector at */
/*                       SPOINT. */

			    srfnrm_(method, target, &et, trgfrm, &c__1, 
				    spoint, normal, (ftnlen)500, (ftnlen)32, (
				    ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Start off by computing the illumination angles. */
/*                       We'll then check the results. */

			    illumg_(method, target, ilusrc, &et, trgfrm, 
				    abcorr, obsrvr, spoint, &trgepc, srfvec, &
				    phase, &incdnc, &emissn, (ftnlen)500, (
				    ftnlen)32, (ftnlen)32, (ftnlen)32, (
				    ftnlen)10, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       We'll treat the input surface point an */
/*                       ephemeris object and find its position relative */
/*                       to the observer. */

			    spkcpt_(spoint, target, trgfrm, &et, trgfrm, 
				    "TARGET", abcorr, obsrvr, state, &lt, (
				    ftnlen)32, (ftnlen)32, (ftnlen)32, (
				    ftnlen)6, (ftnlen)10, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       If SRFVEC is correct, then the position of */
/*                       SPOINT relative to the observer should be equal */
/*                       to SRFVEC. The light time obtained from SPKCPT */
/*                       should match that implied by TRGEPC. */

			    tol = 1e-12;
			    zzcorepc_(abcorr, &et, &lt, &xte, (ftnlen)10);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    chcksd_("TRGEPC", &trgepc, "~/", &xte, &tol, ok, (
				    ftnlen)6, (ftnlen)2);
			    if (uselt) {
				if (usecn) {
				    tol = 1e-10;
				} else {
				    tol = 5e-6;
				}
			    } else {
				tol = 1e-14;
			    }
			    chckad_("SRFVEC", srfvec, "~~/", state, &c__3, &
				    tol, ok, (ftnlen)6, (ftnlen)3);

/*                       We've checked the consistency of SPOINT, */
/*                       SRFVEC, and TRGEPC, but we haven't done */
/*                       anything to test the illumination angles. */
/*                       Do that now. */

/*                       We need the vectors used to define the */
/*                       illumination angles. */

/*                       Negate the observer-surface point vector. */

			    vminus_(srfvec, obspos);

/*                       Get the apparent position of the source as seen */
/*                       from the surface point at TRGEPC. */

			    spkcpo_(ilusrc, &trgepc, trgfrm, "OBSERVER", 
				    abcorr, spoint, target, trgfrm, ilusta, &
				    ilult, (ftnlen)32, (ftnlen)32, (ftnlen)8, 
				    (ftnlen)10, (ftnlen)32, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Compute the expected illumination angles. */

			    xphase = vsep_(ilusta, obspos);
			    xincdc = vsep_(ilusta, normal);
			    xemisn = vsep_(obspos, normal);

/*                       Since we're doing a consistency check, we */
/*                       expect to get very close agreement with */
/*                       ILLUMG. */

			    tol = 1e-14;
			    chcksd_("PHASE", &phase, "~/", &xphase, &tol, ok, 
				    (ftnlen)5, (ftnlen)2);
			    chcksd_("INCDNC", &incdnc, "~/", &xincdc, &tol, 
				    ok, (ftnlen)6, (ftnlen)2);
			    chcksd_("EMISSN", &emissn, "~/", &xemisn, &tol, 
				    ok, (ftnlen)6, (ftnlen)2);
			}

/*                    End of the method loop. */

		    }

/*                 End of the reference frame loop. */

		}

/*              End of the aberration correction loop. */

	    }

/*           End of the surface point loop. */

	}

/*        End of the target loop. */

    }

/*     End of the observer loop. */

/* *********************************************************************** */

/*     Normal case: input handling */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Input handling tests:  make sure target and observer */
/*     can be identified using integer "names." */

    tcase_("Use integer observer and target names.", (ftnlen)38);

/*     Set target and target-fixed frame. */

    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "ellipsoid", (ftnlen)500, (ftnlen)9);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__1 = (npts / 2 << 
	    1) - 2) < 200 && 0 <= i__1 ? i__1 : s_rnge("lonlat", i__1, "f_il"
	    "lumg__", (ftnlen)942)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    xepoch, xsrfvc, &xphase, &xincdc, &xemisn, (ftnlen)500, (ftnlen)
	    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, "499", ilusrc, &et, fixref, abcorr, "399", spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)3, 
	    (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);
/* *********************************************************************** */

/*     Normal case: state change detection */

/* *********************************************************************** */

/*     Certain subsystem state changes must be detected and responded to */
/*     by SINCPT. The subsystems (or structures) having states that must */
/*     be monitored are: */

/*        - Target name-ID mapping */

/*        - Observer name-ID mapping */

/*        - Surface name-ID mapping */

/*        - Target body-fixed frame definition */

/*        - ZZDSKBSR state */


/* --- Case: ------------------------------------------------------ */

    tcase_("Target name changed to JUPITER for ID code 499.", (ftnlen)47);

/*     First, get expected intercept. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(ilusrc, "9", (ftnlen)32, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    xepoch, xsrfvc, &xphase, &xincdc, &xemisn, (ftnlen)500, (ftnlen)
	    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    boddef_("JUPITER", &c__499, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);
/*     Restore original mapping. */

    boddef_("JUPITER", &c__599, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer name changed to SUN for ID code 399.", (ftnlen)45);
    boddef_("SUN", &c__399, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Illumination source name changed to NEPTUNE for ID code 9.", (
	    ftnlen)58);
    boddef_("NEPTUNE", &c__9, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Restore original mapping. */

    boddef_("NEPTUNE", &c__8, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars high-res surface name changed to AAAbbb.", (ftnlen)45);

/*     Get expected results first. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "dsk/unprioritized/surfaces = 1", (ftnlen)500, (ftnlen)30);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__1 = (npts / 2 << 
	    1) - 2) < 200 && 0 <= i__1 ? i__1 : s_rnge("lonlat", i__1, "f_il"
	    "lumg__", (ftnlen)1137)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    xepoch, xsrfvc, &xphase, &xincdc, &xemisn, (ftnlen)500, (ftnlen)
	    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(srfnms, "AAAbbb", (ftnlen)32, (ftnlen)6);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = AAAbbb", (ftnlen)500, (
	    ftnlen)35);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore original mapping. */

    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars high-res DSK.", (ftnlen)25);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "dsk/unprioritized/surfaces = low-res", (ftnlen)500, (
	    ftnlen)36);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__1 = (npts / 2 << 
	    1) - 2) < 200 && 0 <= i__1 ? i__1 : s_rnge("lonlat", i__1, "f_il"
	    "lumg__", (ftnlen)1212)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    xepoch, xsrfvc, &xphase, &xincdc, &xemisn, (ftnlen)500, (ftnlen)
	    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unload the high-res DSK; set METHOD to remove */
/*     surface specification. */

    unload_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars low-res DSK; reload Mars high-res DSK.", (ftnlen)50);

/*     Restore DSK, unload low-res DSK, and repeat computation. */

    furnsh_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__1 = (npts / 2 << 
	    1) - 2) < 200 && 0 <= i__1 ? i__1 : s_rnge("lonlat", i__1, "f_il"
	    "lumg__", (ftnlen)1277)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    xepoch, xsrfvc, &xphase, &xincdc, &xemisn, (ftnlen)500, (ftnlen)
	    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the result matches that obtained with the */
/*     high-res DSK specified. */

    s_copy(method, "dsk/unprioritized/ SURFACES = \"HIGH-RES\" ", (ftnlen)500,
	     (ftnlen)41);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b223, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b223, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("PHASE", &phase, "=", &xphase, &tol, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("INCDNC", &incdnc, "=", &xincdc, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("EMISSN", &emissn, "=", &xemisn, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */

/*     Error handling tests follow. */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid method.", (ftnlen)15);
    s_copy(target, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "SUN", (ftnlen)32, (ftnlen)3);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "ELLIPSOID", (ftnlen)500, (ftnlen)9);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__1 = (npts / 2 << 
	    1) - 2) < 200 && 0 <= i__1 ? i__1 : s_rnge("lonlat", i__1, "f_il"
	    "lumg__", (ftnlen)1342)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_("ELLIPSID", "EARTH", ilusrc, &et, "IAU_EARTH", "NONE", "SUN", 
	    spoint, &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)8, (
	    ftnlen)5, (ftnlen)32, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    illumg_("INTERCEPT ELLIPSOID", "EARTH", ilusrc, &et, "IAU_EARTH", "NONE", 
	    "SUN", spoint, &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)
	    19, (ftnlen)5, (ftnlen)32, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    illumg_("/DSK", "EARTH", ilusrc, &et, "IAU_EARTH", "NONE", "SUN", spoint, 
	    &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)4, (ftnlen)5, (
	    ftnlen)32, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    illumg_("/DSK/UPRIORTIZED", "EARTH", ilusrc, &et, "IAU_EARTH", "NONE", 
	    "SUN", spoint, &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)
	    16, (ftnlen)5, (ftnlen)32, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    illumg_("DSK", "EARTH", ilusrc, &et, "IAU_EARTH", "NONE", "SUN", spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)3, (ftnlen)5, (
	    ftnlen)32, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid observer name.", (ftnlen)22);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, "erth", spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)4);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid target name.", (ftnlen)20);
    illumg_(method, "su", ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)2, 
	    (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer is target.", (ftnlen)19);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, target, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid reference frame center", (ftnlen)30);
    illumg_(method, target, ilusrc, &et, "IAU_MOON", abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)8, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid aberration correction", (ftnlen)29);
    illumg_(method, target, ilusrc, &et, fixref, "LTT", obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*     Test SAVE logic by repeating the call. */

    illumg_(method, target, ilusrc, &et, fixref, "LTT", obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Relativistic aberration correction", (ftnlen)34);
    illumg_(method, target, ilusrc, &et, fixref, "RL", obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)2, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stellar aberration correction w/o light time", (ftnlen)44);
    illumg_(method, target, ilusrc, &et, fixref, "S", obsrvr, spoint, &trgepc,
	     srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32, (
	    ftnlen)32, (ftnlen)32, (ftnlen)1, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded SPK files", (ftnlen)19);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOLOADEDFILES)", ok, (ftnlen)20);
    spklef_("illumg_spk.bsp", handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for observer", (ftnlen)30);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, "1000", spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for target", (ftnlen)28);
    illumg_(method, "gaspra", ilusrc, &et, "IAU_GASPRA", abcorr, obsrvr, 
	    spoint, &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (
	    ftnlen)6, (ftnlen)32, (ftnlen)10, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No orientation data for target", (ftnlen)30);
    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No radius data for target", (ftnlen)25);
    dvpool_("BODY399_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_("ELLIPSOID", target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, 
	    &trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)9, (ftnlen)32, 
	    (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad radius data for target", (ftnlen)26);

/*     Fetch original radii. */

    bodvcd_(&c__399, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Overwrite good radii with bad in the kernel pool. */

    vpack_(&c_b550, &c_b223, &c_b101, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/*     Replace original radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded DSKs.", (ftnlen)15);
    unload_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    illumg_(method, target, ilusrc, &et, fixref, abcorr, obsrvr, spoint, &
	    trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)500, (ftnlen)32,
	     (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Clean up. */

    delfil_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("illumg_spk.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("orbiter.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("illumg_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("illumg_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("illumg_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("illumg_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("illumg_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_illumg__ */

