/*

-Procedure f_illumg_c ( illumg_c tests )

 
-Abstract
 
   Exercise the CSPICE wrapper illumg_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_illumg_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrapper 

      illumg_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Corrected call to dascls_c: wrong handle was 
      used previously.

   16-AUG-2016 (NJB)

      Original version.


-Index_Entries

   test illumg_c

-&
*/

{ /* Begin f_illumg_c */

 
   /*
   Constants
   */
   #define DSK0            "illumg_test0.bds"
   #define PCK0            "illumg_test.tpc"
   #define SPK0            "illumg_test.bsp"
   #define MAXN            10000
   #define METLEN          101
   #define TITLEN          321
   #define TIGHT           1.e-12
   /* #define VTIGHT          1.e-14 */
   #define NCORR           3
   #define NTIMES          2
   #define NOBS            2
   #define NSRC            2
   #define NTARGS          2
   #define NSHAPE          2

   /*
   Other macros 
   */

   /*
   The following macros are used to provide compact type cast
   expressions in order to suppress gcc's insane pointer type  
   compatibility warnings.  :/
   */
   #define PDP3            ( SpiceDouble (*) [3] )
   #define PI3             ( SpiceInt    (*) [3] )

   /*
   Local variables
   */
   SpiceBoolean            found;

   SpiceDLADescr           dladsc;
   SpiceDLADescr           nxtdsc;

   SpiceChar             * abcorr;
   SpiceChar             * fixref;
   SpiceChar             * ilusrc;
   SpiceChar               method [ METLEN ];
   SpiceChar             * obsrvr;
   SpiceChar             * shape;
   SpiceChar             * target;
   SpiceChar               title  [ TITLEN ];

   SpiceDouble             emissn;
   static SpiceDouble      pltctr  [ MAXN ][3];
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             et;
   SpiceDouble             et0;
   SpiceDouble             incdnc;
   SpiceDouble             lat;
   SpiceDouble             lon;
   SpiceDouble             lt;
   SpiceDouble             normal [3];
   SpiceDouble             obspos [3];
   SpiceDouble             phase;
   SpiceDouble             point  [3];
   SpiceDouble             s;
   SpiceDouble             state  [6];
   SpiceDouble             srclt;
   SpiceDouble             srcsta [6];
   SpiceDouble             srfvec [3];
   SpiceDouble             tdelta;
   static SpiceDouble      tilctr [MAXN][3];
   SpiceDouble             tol;
   SpiceDouble             trgepc;
   SpiceDouble             verts  [3][3];
   SpiceDouble             xepoch;   
   SpiceDouble             xphase;
   SpiceDouble             xemssn;
   SpiceDouble             xncdnc;

   SpiceInt                bodyid;
   SpiceInt                corix;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                np;
   SpiceInt                npts;
   SpiceInt                nv;
   SpiceInt                obsix;
   SpiceInt                plate  [3];
   SpiceInt                pntix;
   SpiceInt                shapix;
   SpiceInt                spkhan;
   SpiceInt                srcix;
   SpiceInt                surfid;
   SpiceInt                targix;
   SpiceInt                timix;
   SpiceInt                trgcde;


   /*
   Saved values 
   */
   static SpiceChar     * corrs  [NCORR ] = 
                          { "None",     "CN",         "CN+S" };

   static SpiceChar     * frames [NTARGS] = 
                          { "IAU_MARS", "IAU_PHOBOS"         };

   static SpiceChar     * obs    [NOBS  ] = 
                          { "Earth",    "Jupiter"            };

   static SpiceChar     * targs  [NTARGS] = 
                          { "Mars",     "Phobos"             };

   static SpiceChar     * shapes [NSHAPE] = 
                          { "DSK",      "ELLIPSOID"          };

   static SpiceChar     * srcs   [NSRC  ] = 
                          { "Sun",      "Moon"               };


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_illumg_c" );
   

   /*
   *********************************************************************
   *
   *
   *   setup
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a text PCK and a default SPK." );
   
   if ( exists_c(PCK0) ) 
   {
      removeFile(PCK0);
   }

   /*
   Don't load the PCK; do save it. 
   */
   tstpck_c ( PCK0, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load via furnsh_c to avoid later complexities. 
   */
   furnsh_c ( PCK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create and load the SPK. 
   */
   tstspk_c ( SPK0, SPICEFALSE, &spkhan );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c ( SPK0 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a DSK containing target shapes." );
 

   if ( exists_c(DSK0) )
   {
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile( DSK0 );
   }
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create the DSK. 
   */

   /*
   Start out by creating a segment for Mars. We'll create  
   a very low-resolution tessellated ellipsoid.
   */

   bodyid = 499;
   surfid = 1;
   fixref = "IAU_MARS";
   nlon   = 20;
   nlat   = 10;

   t_elds2z_c ( bodyid, surfid, fixref, nlon, nlat, DSK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compute lon/lat deltas for later use. 
   */
   dlon = twopi_c() / nlon;
   dlat = pi_c()    / nlat;


   /*
   Add a segment for Phobos. We'll create a non-convex,
   unconnected shape that will give rise to self-occultations.
   (The shape is an extremely crude representation of the shape
   of comet Churyumov-Gerasimenko.)
   */

   bodyid = 401;
   surfid = 2;
   fixref = "IAU_PHOBOS";
       
   t_cg_c ( bodyid, surfid, fixref, DSK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

 
   /*
   Load the DSK. 
   */
   furnsh_c ( DSK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Also open the DSK for a segment traversal. 
   */
   dasopr_c ( DSK0, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   *********************************************************************
   *
   *
   *   illumg_c normal cases
   *
   *
   *********************************************************************
   */
 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Top of illumg_c normal case loop" );


   /*
   Set an initial time and time delta.
   */
   et0    = 10 * jyear_c();
   tdelta = 3600.0;

   /*
   Loop over targets. 
   */
   for ( targix = 0;  targix < NTARGS;  targix++  )
   {
      target = targs [targix];
      fixref = frames[targix];

      bods2c_c ( target, &trgcde, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "bods2c_c found", found, SPICETRUE, ok );


      /*
      Set up the array of surface points at which we'll find illumination
      angles. 

      For the DSK cases, we'll use the centroids of the plates as the
      input surface points passed to illumg_c.

      For the ellipsoid cases, we'll use centers of lon/lat tiles.
 
      Start by getting the DLA descriptor for the segment associated with
      the current target.
      */
      if ( targix == 0 )
      {
         dlabfs_c ( handle, &dladsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dlabfs_c found", found, SPICETRUE, ok );
      }
      else
      {
         dlafns_c ( handle, &dladsc, &nxtdsc, &found );
         chckxc_c ( SPICEFALSE, " ", ok );

         chcksl_c ( "dlafns_c found", found, SPICETRUE, ok );

         dladsc = nxtdsc;
      }
      /*
      Get the vertex and plate counts for the current segment. 
      */
      dskz02_c ( handle, &dladsc, &nv, &np );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Create a surface point at the centroid of each plate. 

      Note plate and vertex indices are 1-based in all languages.
      */
      for ( i = 1;  i <= np;  i++ )
      {
         dskp02_c ( handle, &dladsc, i, 1, &n, PI3(plate) );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         Fetch the vertices for the ith plate. 
         */     
         for ( j = 0;  j < 3;  j++ )
         {
            k = plate[j];

            dskv02_c( handle, &dladsc, k, 1, &n, PDP3(verts[j]) );
            chckxc_c ( SPICEFALSE, " ", ok );
         }

         s = 1.0/3.0;

         /*
         Caution: the plate index of pltctr is 0-based.
         */
         vlcom3_c ( s, verts[0],  s, verts[1],  s, verts[2],  pltctr[i-1] );
         chckxc_c ( SPICEFALSE, " ", ok );

      }
      
      /*
      Now create points on the target's reference ellipsoid, each one at
      the center of a lon/lat tile. Note `dlon' and `dlat' were assigned
      in the setup portion of this routine.
      */
  
      k = 0;
      
      for ( i = 0;  i < nlon;  i++ )
      {
         lon =  (i + 0.5) * dlon;

         for ( j = 0;  j < nlat;  j++ )
         {
            lat = halfpi_c() - ( (j+0.5)*dlat );

            srfrec_c ( trgcde, lon, lat, tilctr[k] );
            chckxc_c ( SPICEFALSE, " ", ok );


            ++k;
         }
      }



      /*
      Loop over observers. 
      */
      for ( obsix = 0;  obsix < NOBS;  obsix++  )
      {
         obsrvr = obs[obsix];

         /*
         Loop over illumination sources. 
         */
         for ( srcix = 0;  srcix < NSRC;  srcix++  )
         {
            ilusrc = srcs[srcix];

            /*
            Loop over aberration corrections. 
            */
            for ( corix = 0;  corix < NCORR;  corix++  )
            {
               abcorr = corrs[corix];


               /*
               Loop over observation times. 
               */
               for ( timix = 0;  timix < NTIMES;  timix++  )
               {
                  et = et0  +  (timix * tdelta);

                  /*
                  Loop over target shapes. 
                  */
                  for ( shapix = 0;  shapix < NSHAPE;  shapix++  )
                  {
                     shape = shapes[shapix];

                     /*
                     Put together a method string. 
                     */

                     if ( shapix == 0 )
                     {
                        strncpy ( method, "Ellipsoid", METLEN );

                        npts = nlon * nlat;

                     }
                     else
                     {
                        /*
                        This is the DSK case. 

                        The target index, plus 1, is the surface ID.
                        */
                        strncpy ( method,
                                  "dsk/unprioritized/surfaces = @",
                                  METLEN                              );
                        repmi_c ( method, "@", targix+1, METLEN, method );
                        chckxc_c ( SPICEFALSE, " ", ok );


                        npts = np;

                     }
                        

                     /*
                     Loop over the surface points.
                     */

                     for ( pntix = 0;  pntix < npts;  pntix++ )
                     {
                        if ( shapix == 0 ) 
                        {
                           /*
                           This is the ellipsoid case. 
                           */
                           vequ_c ( tilctr[pntix], point );
                        }
                        else
                        {
                           /*
                           This is the DSK case. 
                           */
                           vequ_c ( pltctr[pntix], point );
                        }


                        /*
                        Finally, compute the illumination angles at the 
                        current surface point.
                        */


                        /* 
                        ---- Case --------------------------------------
                        */
                        sprintf ( title,
                                  "illumg_c: method = %s; "
                                  "target = %s; source = "
                                  "%s; et = %23.17e; fixref = "
                                  "%s; obsrvr = %s; "
                                  "point = (%f, %f, %f)",
                                  method,
                                  target,
                                  ilusrc,
                                  et,
                                  fixref,
                                  obsrvr,
                                  point[0], point[1], point[2] );
   
                        
                        /* printf ( "%s\n", title ); */
                        

                        tcase_c ( title );


                        illumg_c ( method,  target,  ilusrc, et,
                                   fixref,  abcorr,  obsrvr, point, 
                                   &trgepc, srfvec,  &phase, &incdnc,
                                   &emissn                          );

                        chckxc_c ( SPICEFALSE, " ", ok );

                        /*
                        We can now check the results. Start out by 
                        computing the expected target epoch and vector
                        from observer to surface point. The easy way to
                        do this is to treat the surface point as an
                        ephemeris object. 
                        */

                        spkcpt_c ( point,  target,   fixref, et,
                                   fixref, "TARGET", abcorr, obsrvr,
                                   state,  &lt                      );
                        chckxc_c ( SPICEFALSE, " ", ok );

                   
                        if ( eqstr_c( abcorr, "NONE" ) )
                        {
                           xepoch = et;
                        }
                        else
                        {
                           xepoch = et - lt;
                        }

                        /*
                        Check epoch and surface vector. 
                        */
                        tol = TIGHT;

                        chcksd_c ( "trgepc", trgepc, "~/", 
                                             xepoch, tol, ok );

                        chckad_c ( "srfvec", srfvec, "~~/",
                                   state,    3,      tol,   ok );

                        /*
                        Compute expected angles. We'll need the surface
                        point-illumination source position vector and
                        the outward normal vector at the surface point.
                        */
                        spkcpo_c ( ilusrc, trgepc, fixref, "OBSERVER",
                                   abcorr, point,  target, fixref,
                                   srcsta, &srclt                     );
                        chckxc_c ( SPICEFALSE, " ", ok );

                        srfnrm_c ( method,     target, et,       
                                   fixref,     1,      PDP3 point,  
                                   PDP3 normal                    );
                        chckxc_c ( SPICEFALSE, " ", ok );

                        vminus_c ( srfvec, obspos );


                        xphase = vsep_c ( obspos, srcsta );
                
                        xemssn = vsep_c ( obspos, normal );

                        xncdnc = vsep_c ( srcsta, normal );
                        

                        /*
                        Now check the angles. 
                        */
                        chcksd_c ( "phase",  phase, "~/", 
                                             xphase, tol, ok );

                        chcksd_c ( "emissn", emissn, "~/", 
                                             xemssn, tol, ok );

                        chcksd_c ( "incdnc", incdnc, "~/", 
                                             xncdnc, tol, ok );
                        
 
                     }
                     /*
                     End of the surface point loop. 
                     */

                  }
                  /*
                  End of shape loop. 
                  */

               }
               /*
               End of time loop. 
               */

            }
            /*
            End of aberration correction loop. 
            */
         }
         /*
         End of illumination source loop. 
         */
      }
      /*
      End of observer loop.
      */
   }
   /*
   End of target loop. 
   */


   /*
   *********************************************************************
   *
   *
   *   illumg_c error cases
   *
   *
   *********************************************************************
   */

   /*
   We need not test all of the error cases, but we must test at least
   one; this will exercise the error handling logic in the wrapper.
   */

   /*
   Use the last values of all the inputs. We'll create an error by 
   unloading the SPK file. 
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "illumg error: no loaded SPK." );


   unload_c ( SPK0 );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   illumg_c ( method,  target,  ilusrc, et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                           );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );


   /*
   For CSPICE, we must check handling of bad input strings.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "illumg error: empty input string." );

   illumg_c ( "",      target,  ilusrc, et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illumg_c ( method,  "",      ilusrc, et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   illumg_c ( method,  target,  "",     et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              "",      abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              fixref,  "",      obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              fixref,  abcorr,  "",     point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "illumg error: null input string." );

   illumg_c ( NULL,    target,  ilusrc, et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illumg_c ( method,  NULL,    ilusrc, et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   illumg_c ( method,  target,  NULL,   et,
              fixref,  abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              NULL,    abcorr,  obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              fixref,  NULL,    obsrvr, point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   illumg_c ( method,  target,  ilusrc, et,
              fixref,  abcorr,  NULL,   point, 
              &trgepc, srfvec,  &phase, &incdnc,
              &emissn                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   




   /*
   *********************************************************************
   *
   *
   *   Clean up
   *
   *
   *********************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );


   /*
   Undo the dasopr_c call we made earlier.
   */
   dascls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );    


   kclear_c();

   removeFile( PCK0 );
   removeFile( SPK0 );
   removeFile( DSK0 );

 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_illumg_c */

