/* f_insang.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 10.;
static doublereal c_b7 = 4.;
static doublereal c_b13 = 1.;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b34 = 5.;
static integer c__2 = 2;
static doublereal c_b46 = 2.;
static doublereal c_b59 = 3.;
static doublereal c_b118 = .5;
static doublereal c_b120 = .001;
static doublereal c_b145 = -.001;

/* $Procedure F_INSANG ( INSANG tests ) */
/* Subroutine */ int f_insang__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };
    static doublereal zvec[3] = { 0.,0.,1. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6;
    doublereal d__1;

    /* Builtin functions */
    double pow_di(doublereal *, integer *), pow_dd(doublereal *, doublereal *)
	    ;
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_stop(char *
	    , ftnlen);

    /* Local variables */
    static doublereal beta, dlat;
    static integer nrad;
    static doublereal dlon;
    static integer nlat, nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vsub_(doublereal *, doublereal *, doublereal *), vequ_(
	    doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int eul2m_(doublereal *, doublereal *, doublereal 
	    *, integer *, integer *, integer *, doublereal *);
    static integer i__, j, k, l, m, n;
    static doublereal gamma, r__, s, alpha, v[3], scale, plane[4];
    extern /* Subroutine */ int tcase_(char *, ftnlen), ident_(doublereal *), 
	    vpack_(doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal sides[9]	/* was [3][3] */;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal e1[3], e2[3], e3[3], vtemp[3], xform[9]	/* was [3][3] 
	    */;
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *);
    static integer nxpts;
    extern /* Subroutine */ int vlcom3_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), nvc2pl_(doublereal *, doublereal *, doublereal *), 
	    chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen), psv2pl_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer nscale;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), latrec_(doublereal *, doublereal *,
	     doublereal *, doublereal *), insang_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *, doublereal *);
    static doublereal plates[27]	/* was [3][3][3] */, sclplt[27]	/* 
	    was [3][3][3] */, vertex[3];
    extern /* Subroutine */ int inrypl_(doublereal *, doublereal *, 
	    doublereal *, integer *, doublereal *);
    static doublereal sclvtx[3], lat;
    extern doublereal rpd_(void);
    static doublereal lon, tol;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;
    static doublereal xpt[3];

/* $ Abstract */

/*     Exercise the ray-voxel grid intersection routine INSANG. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKLIB type 2 ray-voxel grid */
/*     intersection routine INSANG. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-JUN-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_INSANG", (ftnlen)8);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple intersection case.", (ftnlen)25);
    vpack_(&c_b4, &c_b4, &c_b6, e1);
    vpack_(&c_b7, &c_b4, &c_b6, e2);
    vpack_(&c_b4, &c_b7, &c_b6, e3);
    vpack_(&c_b13, &c_b13, &c_b6, v);
    insang_(v, e1, e2, e3, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vscl_(&scale, v, xpt);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Create a SPICELIB plane containing the plate defined */
/*     by E1, E2, E3. */

    nvc2pl_(zvec, &c_b6, plane);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Find the expected intercept. */

    inrypl_(origin, v, plane, &nxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple non-intersection case.", (ftnlen)29);

/*     Use the edge vectors from the previous case. Update V. */

    vpack_(&c_b4, &c_b34, &c_b6, v);
    insang_(v, e1, e2, e3, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */



/*     The following set of cases covers a variety of plate shapes, */
/*     vertex positions, and scales. */



/*     Create a rotation to be applied to all inputs to INSANG. */

    alpha = rpd_() * 30.;
    beta = rpd_() * -10.;
    gamma = rpd_() * 70.;
    eul2m_(&gamma, &beta, &alpha, &c__3, &c__2, &c__3, xform);
    ident_(xform);

/*     Create the plate shapes to be used in the following tests. */


/*        Plate 1 is an acute triangle. */

    vpack_(&c_b4, &c_b4, &c_b4, plates);
    vpack_(&c_b46, &c_b4, &c_b4, &plates[3]);
    vpack_(&c_b13, &c_b7, &c_b4, &plates[6]);

/*        Plate 2 is a right triangle. */

    vpack_(&c_b4, &c_b4, &c_b4, &plates[9]);
    vpack_(&c_b7, &c_b4, &c_b4, &plates[12]);
    vpack_(&c_b7, &c_b59, &c_b4, &plates[15]);

/*        Plate 3 is an obtuse triangle. */

    vpack_(&c_b4, &c_b4, &c_b4, &plates[18]);
    vpack_(&c_b7, &c_b4, &c_b4, &plates[21]);
    vpack_(&c_b46, &c_b13, &c_b4, &plates[24]);

/*     Set parameters used for vertex placement. */

    nlon = 5;
    dlon = twopi_() / nlon;
    nlat = 10;
    dlat = pi_() / (nlat - 1);
    nrad = 5;

/*     Set parameters used to determine the scale of the inputs. */

    nscale = 7;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    i__3 = nrad;
	    for (k = 1; k <= i__3; ++k) {
		i__4 = k - 2;
		r__ = pow_di(&c_b6, &i__4);
		i__4 = nscale;
		for (l = 1; l <= i__4; ++l) {
		    d__1 = (doublereal) (l - nscale / 2) * 20.;
		    scale = pow_dd(&c_b6, &d__1);
/*                  SCALE = 1.D0 */

/*                 Create the vertex. */

		    latrec_(&r__, &lon, &lat, vertex);

/*                 Scale the vertex. */

		    vscl_(&scale, vertex, sclvtx);

/*                 Loop over the shapes. */

		    for (m = 1; m <= 3; ++m) {
			for (n = 1; n <= 3; ++n) {
			    vscl_(&scale, &plates[(i__5 = (n + m * 3) * 3 - 
				    12) < 27 && 0 <= i__5 ? i__5 : s_rnge(
				    "plates", i__5, "f_insang__", (ftnlen)360)
				    ], &sclplt[(i__6 = (n + m * 3) * 3 - 12) <
				     27 && 0 <= i__6 ? i__6 : s_rnge("sclplt",
				     i__6, "f_insang__", (ftnlen)360)]);
			}

/*                    Transform vertex and plate using XFORM. */

			mxv_(xform, vertex, vtemp);
			vequ_(vtemp, vertex);
			for (n = 1; n <= 3; ++n) {
			    mxv_(xform, &plates[(i__5 = (n + m * 3) * 3 - 12) 
				    < 27 && 0 <= i__5 ? i__5 : s_rnge("plates"
				    , i__5, "f_insang__", (ftnlen)372)], 
				    vtemp);
			    vequ_(vtemp, &plates[(i__5 = (n + m * 3) * 3 - 12)
				     < 27 && 0 <= i__5 ? i__5 : s_rnge("plat"
				    "es", i__5, "f_insang__", (ftnlen)373)]);
			}

/*                    Create the edges of the pyramid for the current */
/*                    plate. */

			vsub_(&sclplt[(i__5 = (m * 3 + 1) * 3 - 12) < 27 && 0 
				<= i__5 ? i__5 : s_rnge("sclplt", i__5, "f_i"
				"nsang__", (ftnlen)380)], sclvtx, e1);
			vsub_(&sclplt[(i__5 = (m * 3 + 2) * 3 - 12) < 27 && 0 
				<= i__5 ? i__5 : s_rnge("sclplt", i__5, "f_i"
				"nsang__", (ftnlen)381)], sclvtx, e2);
			vsub_(&sclplt[(i__5 = (m * 3 + 3) * 3 - 12) < 27 && 0 
				<= i__5 ? i__5 : s_rnge("sclplt", i__5, "f_i"
				"nsang__", (ftnlen)382)], sclvtx, e3);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at plate's centroid.", (ftnlen)320, (ftnlen)
				59);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b13, e1, &c_b13, e2, &c_b13, e3, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
			vscl_(&s, v, xpt);

/*                    Create a SPICELIB plane containing the plate */
/*                    defined by E1, E2, E3. */

			vsub_(e2, e1, sides);
			vsub_(e3, e1, &sides[6]);
			psv2pl_(e1, sides, &sides[6], plane);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Find the expected intercept. */

			inrypl_(origin, v, plane, &nxpts, xxpt);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)5, (ftnlen)1);
			tol = 1e-9;
			chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at interior point close to the E1-E2 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e1, &c_b118, e2, &c_b120, e3, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
			vscl_(&s, v, xpt);

/*                    Create a SPICELIB plane containing the plate */
/*                    defined by E1, E2, E3. */

			vsub_(e2, e1, sides);
			vsub_(e3, e1, &sides[6]);
			psv2pl_(e1, sides, &sides[6], plane);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Find the expected intercept. */

			inrypl_(origin, v, plane, &nxpts, xxpt);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)5, (ftnlen)1);
			if (! (*ok)) {
			    s_stop("", (ftnlen)0);
			}
			tol = 1e-9;
			chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at exterior point close to the E1-E2 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e1, &c_b118, e2, &c_b145, e3, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at interior point close to the E1-E3 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e1, &c_b118, e3, &c_b120, e2, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
			vscl_(&s, v, xpt);

/*                    Create a SPICELIB plane containing the plate */
/*                    defined by E1, E2, E3. */

			vsub_(e2, e1, sides);
			vsub_(e3, e1, &sides[6]);
			psv2pl_(e1, sides, &sides[6], plane);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Find the expected intercept. */

			inrypl_(origin, v, plane, &nxpts, xxpt);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)5, (ftnlen)1);
			if (! (*ok)) {
			    s_stop("", (ftnlen)0);
			}
			tol = 1e-9;
			chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at exterior point close to the E1-E3 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e1, &c_b118, e3, &c_b145, e2, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at interior point close to the E2-E3 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e2, &c_b118, e3, &c_b120, e1, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
			vscl_(&s, v, xpt);

/*                    Create a SPICELIB plane containing the plate */
/*                    defined by E1, E2, E3. */

			vsub_(e2, e1, sides);
			vsub_(e3, e1, &sides[6]);
			psv2pl_(e1, sides, &sides[6], plane);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Find the expected intercept. */

			inrypl_(origin, v, plane, &nxpts, xxpt);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)5, (ftnlen)1);
			if (! (*ok)) {
			    s_stop("", (ftnlen)0);
			}
			tol = 1e-9;
			chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, "I = #; J = #, K = #, L = #, M = #. Ai"
				"m at exterior point close to the E2-E3 side.",
				 (ftnlen)320, (ftnlen)81);
			repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)
				1, (ftnlen)320);
			repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &l, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			repmi_(title, "#", &m, title, (ftnlen)320, (ftnlen)1, 
				(ftnlen)320);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tcase_(title, (ftnlen)320);
			vlcom3_(&c_b118, e2, &c_b118, e3, &c_b145, e1, v);
			insang_(v, e1, e2, e3, &found, &s);
			chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
		    }
		}
	    }
	}
    }
/* ********************************************************************** */

/*     Exceptional cases */

/* ********************************************************************** */

/*     INSANG is error-free. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Ray direction is zero vector.", (ftnlen)29);
    vpack_(&c_b4, &c_b4, &c_b6, e1);
    vpack_(&c_b7, &c_b4, &c_b6, e2);
    vpack_(&c_b4, &c_b7, &c_b6, e3);
    cleard_(&c__3, v);
    insang_(v, e1, e2, e3, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case: ------------------------------------------------------ */

    tcase_("Edge vectors are linearly dependent.", (ftnlen)36);

/*     Create independent edge vectors, but pass dependent */
/*     selections of the them to INSANG. */

    vpack_(&c_b4, &c_b4, &c_b6, e1);
    vpack_(&c_b7, &c_b4, &c_b6, e2);
    vpack_(&c_b4, &c_b7, &c_b6, e3);
    vpack_(&c_b13, &c_b13, &c_b6, v);

/*     E2 = E1: */

    insang_(v, e1, e1, e3, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    chcksd_("SCALE", &scale, "=", &c_b4, &c_b4, ok, (ftnlen)5, (ftnlen)1);

/*     E3 = E1: */

    insang_(v, e1, e2, e1, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    chcksd_("SCALE", &scale, "=", &c_b4, &c_b4, ok, (ftnlen)5, (ftnlen)1);

/*     E3 = E2: */

    insang_(v, e1, e2, e2, &found, &scale);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    chcksd_("SCALE", &scale, "=", &c_b4, &c_b4, ok, (ftnlen)5, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_insang__ */

