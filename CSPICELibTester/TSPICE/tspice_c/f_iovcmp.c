/* f_iovcmp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b46 = 0.;

/* $Procedure F_IOVCMP ( IOVCMP tests ) */
/* Subroutine */ int f_iovcmp__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal dval;
    static integer ndim;
    static doublereal dset[106];
    static integer i__, j, k;
    extern integer cardd_(doublereal *);
    extern /* Subroutine */ int tcase_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    static doublereal darry2[100], darry3[100];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static integer to;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), validd_(integer *, integer *, doublereal *), 
	    shelld_(integer *, doublereal *), reordd_(integer *, integer *, 
	    doublereal *);
    static doublereal darray[100];
    static integer iorder[100], rngmax;
    extern /* Subroutine */ int iovcmp_(doublereal *, integer *, integer *, 
	    integer *, integer *);
    static integer invord[100], xorder[100];

/* $ Abstract */

/*     Exercise the compressed inverse order vector routine IOVCMP. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKBRIEF supporting routine IOVCMP. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 08-FEB-2017 (NJB) */

/*        Original version 02-SEP-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_IOVCMP", (ftnlen)8);
/* ********************************************************************** */

/*     IOVCMP normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: ordered input set.", (ftnlen)320, (ftnlen)32)
	    ;
    tcase_(title, (ftnlen)320);
    ndim = 100;
    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	darray[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("darray", 
		i__2, "f_iovcmp__", (ftnlen)178)] = (doublereal) i__;
    }
    iovcmp_(darray, &ndim, iorder, invord, &rngmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The order vector and its inverse should both be */
/*     the identity permutation. */

    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xorder[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("xorder", 
		i__2, "f_iovcmp__", (ftnlen)189)] = i__;
    }
    chckai_("IORDER", iorder, "=", xorder, &ndim, ok, (ftnlen)6, (ftnlen)1);
    chckai_("INVORD", invord, "=", xorder, &ndim, ok, (ftnlen)6, (ftnlen)1);

/*     The maximum element of the inverse order vector is */
/*     supposed to be NDIM. */

    chcksi_("RNGMAX", &rngmax, "=", &ndim, &c__0, ok, (ftnlen)6, (ftnlen)1);
    ndim = 100;

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Reverse-ordered input set.", (ftnlen)320, (ftnlen)26);
    tcase_(title, (ftnlen)320);
    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	darray[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("darray", 
		i__2, "f_iovcmp__", (ftnlen)211)] = -((doublereal) i__);
    }
    iovcmp_(darray, &ndim, iorder, invord, &rngmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The order vector and its inverse should both be */
/*     the reverse permutation. */

    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xorder[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("xorder", 
		i__2, "f_iovcmp__", (ftnlen)222)] = ndim + 1 - i__;
    }
    chckai_("IORDER", iorder, "=", xorder, &ndim, ok, (ftnlen)6, (ftnlen)1);
    chckai_("INVORD", invord, "=", xorder, &ndim, ok, (ftnlen)6, (ftnlen)1);

/*     The maximum element of the inverse order vector is */
/*     supposed to be NDIM. */

    chcksi_("RNGMAX", &rngmax, "=", &ndim, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Ordered input set with paired, duplicate elements.", (
	    ftnlen)320, (ftnlen)50);
    tcase_(title, (ftnlen)320);
    ndim = 100;
    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	darray[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("darray", 
		i__2, "f_iovcmp__", (ftnlen)245)] = (doublereal) i__;
	darray[(i__2 = i__) < 100 && 0 <= i__2 ? i__2 : s_rnge("darray", i__2,
		 "f_iovcmp__", (ftnlen)246)] = darray[(i__3 = i__ - 1) < 100 
		&& 0 <= i__3 ? i__3 : s_rnge("darray", i__3, "f_iovcmp__", (
		ftnlen)246)];
    }
    iovcmp_(darray, &ndim, iorder, invord, &rngmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Because the shell sort isn't stable, we don't know what */
/*     the expected order vector is. But we do know that we */
/*     should be able to re-order the d.p. array using the */
/*     order vector and obtain an ordered set. */

    moved_(darray, &ndim, darry2);
    reordd_(iorder, &ndim, darry2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the expected set. */

    moved_(darray, &ndim, darry3);
    shelld_(&ndim, darry3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This test actually checks IORDER. */

    chckad_("DARRY2/IORDER", darry2, "=", darry3, &ndim, &c_b46, ok, (ftnlen)
	    13, (ftnlen)1);

/*     Now check the compressed inverse order vector. */

/*     Make a set out of the original array. */

    moved_(darray, &ndim, &dset[6]);
    validd_(&ndim, &ndim, dset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The compressed inverse order vector should map each index of */
/*     DARRAY to the index of an element in DSET containing the */
/*     same value as that located at the original index. */

    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dval = dset[(i__3 = invord[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 
		: s_rnge("invord", i__2, "f_iovcmp__", (ftnlen)292)] + 5) < 
		106 && 0 <= i__3 ? i__3 : s_rnge("dset", i__3, "f_iovcmp__", (
		ftnlen)292)];
	chcksd_("DVAL", &dval, "=", &darray[(i__2 = i__ - 1) < 100 && 0 <= 
		i__2 ? i__2 : s_rnge("darray", i__2, "f_iovcmp__", (ftnlen)
		294)], &c_b46, ok, (ftnlen)4, (ftnlen)1);
    }

/*     The maximum element of the inverse order vector is */
/*     supposed to be the cardinality of DSET. */

    i__1 = cardd_(dset);
    chcksi_("RNGMAX", &rngmax, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Ordered input set with varying numbers of duplicate eleme"
	    "nts.", (ftnlen)320, (ftnlen)61);
    tcase_(title, (ftnlen)320);
    ndim = 30;
    i__ = 1;
    to = 1;
    while(to <= ndim) {

/*        Let the number of duplicates range from 1 : 7. */

/* Computing MIN */
	i__1 = ndim + 1 - i__, i__2 = (i__ - 1) % 7 + 1;
	j = min(i__1,i__2);

/*        Fill in array elements TO : TO+J-1. */

	i__1 = j;
	for (k = 1; k <= i__1; ++k) {
	    darray[(i__2 = to + k - 2) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "darray", i__2, "f_iovcmp__", (ftnlen)328)] = (doublereal)
		     i__;
	}
	to += j;
	++i__;
    }
    iovcmp_(darray, &ndim, iorder, invord, &rngmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Because the shell sort isn't stable, we don't know what */
/*     the expected order vector is. But we do know that we */
/*     should be able to re-order the d.p. array using the */
/*     order vector and obtain an ordered set. */

    moved_(darray, &ndim, darry2);
    reordd_(iorder, &ndim, darry2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the expected set. */

    moved_(darray, &ndim, darry3);
    shelld_(&ndim, darry3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This test actually checks IORDER. */

    chckad_("DARRY2/IORDER", darry2, "=", darry3, &ndim, &c_b46, ok, (ftnlen)
	    13, (ftnlen)1);

/*     Now check the compressed inverse order vector. */

/*     Make a set out of the original array. */

    moved_(darray, &ndim, &dset[6]);
    validd_(&ndim, &ndim, dset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The compressed inverse order vector should map each index of */
/*     DARRAY to the index of an element in DSET containing the */
/*     same value as that located at the original index. */

    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dval = dset[(i__3 = invord[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 
		: s_rnge("invord", i__2, "f_iovcmp__", (ftnlen)380)] + 5) < 
		106 && 0 <= i__3 ? i__3 : s_rnge("dset", i__3, "f_iovcmp__", (
		ftnlen)380)];
	chcksd_("DVAL", &dval, "=", &darray[(i__2 = i__ - 1) < 100 && 0 <= 
		i__2 ? i__2 : s_rnge("darray", i__2, "f_iovcmp__", (ftnlen)
		382)], &c_b46, ok, (ftnlen)4, (ftnlen)1);
    }

/*     The maximum element of the inverse order vector is */
/*     supposed to be the cardinality of DSET. */

    i__1 = cardd_(dset);
    chcksi_("RNGMAX", &rngmax, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Ordered input set with varying numbers of duplicate eleme"
	    "nts. Negate the input set values from the previous case.", (
	    ftnlen)320, (ftnlen)113);
    tcase_(title, (ftnlen)320);
    ndim = 30;
    i__ = 1;
    to = 1;
    while(to <= ndim) {

/*        Let the number of duplicates range from 1 : 7. */

/* Computing MIN */
	i__1 = ndim + 1 - i__, i__2 = (i__ - 1) % 7 + 1;
	j = min(i__1,i__2);

/*        Fill in array elements TO : TO+J-1. */

	i__1 = j;
	for (k = 1; k <= i__1; ++k) {
	    darray[(i__2 = to + k - 2) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "darray", i__2, "f_iovcmp__", (ftnlen)417)] = -((
		    doublereal) i__);
	}
	to += j;
	++i__;
    }
    iovcmp_(darray, &ndim, iorder, invord, &rngmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Because the shell sort isn't stable, we don't know what */
/*     the expected order vector is. But we do know that we */
/*     should be able to re-order the d.p. array using the */
/*     order vector and obtain an ordered set. */

    moved_(darray, &ndim, darry2);
    reordd_(iorder, &ndim, darry2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the expected set. */

    moved_(darray, &ndim, darry3);
    shelld_(&ndim, darry3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This test actually checks IORDER. */

    chckad_("DARRY2/IORDER", darry2, "=", darry3, &ndim, &c_b46, ok, (ftnlen)
	    13, (ftnlen)1);

/*     Now check the compressed inverse order vector. */

/*     Make a set out of the original array. */

    moved_(darray, &ndim, &dset[6]);
    validd_(&ndim, &ndim, dset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The compressed inverse order vector should map each index of */
/*     DARRAY to the index of an element in DSET containing the */
/*     same value as that located at the original index. */

    i__1 = ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dval = dset[(i__3 = invord[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 
		: s_rnge("invord", i__2, "f_iovcmp__", (ftnlen)469)] + 5) < 
		106 && 0 <= i__3 ? i__3 : s_rnge("dset", i__3, "f_iovcmp__", (
		ftnlen)469)];
	chcksd_("DVAL", &dval, "=", &darray[(i__2 = i__ - 1) < 100 && 0 <= 
		i__2 ? i__2 : s_rnge("darray", i__2, "f_iovcmp__", (ftnlen)
		471)], &c_b46, ok, (ftnlen)4, (ftnlen)1);
    }

/*     The maximum element of the inverse order vector is */
/*     supposed to be the cardinality of DSET. */

    i__1 = cardd_(dset);
    chcksi_("RNGMAX", &rngmax, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
/* ********************************************************************** */

/*     IOVCMP error cases */

/* ********************************************************************** */

/*     None so far. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_iovcmp__ */

