/* f_kpbug.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;

/* $Procedure F_KPBUG ( UNLOAD bug demonstration ) */
/* Subroutine */ int f_kpbug__(logical *ok)
{
    static char ksrc[4];
    extern /* Subroutine */ int tcase_(char *, ftnlen), kinfo_(char *, char *,
	     char *, integer *, logical *, ftnlen, ftnlen, ftnlen), topen_(
	    char *, ftnlen);
    static char ktype[255];
    extern /* Subroutine */ int t_success__(logical *);
    static logical found0, found1, found2, found3;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen), unload_(char *, ftnlen),
	     spkuef_(integer *), furnsh_(char *, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int tstlsk_(void), tstspk_(char *, logical *, 
	    integer *, ftnlen);
    static integer han0, han1, han2, han3;

/* $ Abstract */

/*     Demonstrate the HANDLE bug in keeper */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the handle-changing bug in the following */
/*     call sequence to FURNSH and UNLOAD */
/*     type 20 SPK data: */

/*     CALL FURNSH(SPK0) */
/*     CALL KINFO(SPK0, KTYPE, SSRC, HAN0, FOUND0) */
/*     CALL FURNSH(SPK0) */
/*     CALL KINFO(SPK0, KTYPE, SSRC, HAN1, FOUND1) */
/*     CALL UNLOAD(SPK0) */
/*     CALL KINFO(SPK0, KTYPE, SSRC, HAN2, FOUND2) */

/*   Now HAN2 != HAN0 and HAN1 but FOUND<n> are */
/*   all TRUE. */




/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     G.M. Hockney     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 27-JAN-2017 (NJB) (GMH) */

/*        This routine has been updated so that, prior to exit, */
/*        it deletes the SPK it creates. */

/*        Deleted declarations of unused functions. */

/*        Renamed George's routine from FF_KEEPBUG */
/*        to F_KPBUG. */

/*     Original Version 1.0.0, 19-MAY-2015 (GMH) */


/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved variables */


/*     Save all local variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_KPBUG", (ftnlen)7);

/*     Open a new SPK file for writing. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create kernels.", (ftnlen)22);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create SPK file.", (ftnlen)16);

/*     Create a small SPK file containing 1 record. We'll */
/*     use the moon as the target and the earth-moon barycenter */
/*     as the center. */


/*     Load test SPK to provide data. */

    if (exists_("test.bsp", (ftnlen)8)) {
	delfil_("test.bsp", (ftnlen)8);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tstspk_("test.bsp", &c_true, &han0, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han0);

/* --- Case: ------------------------------------------------------ */

    tcase_("Demonstrate multiple load bug.", (ftnlen)30);
    furnsh_("test.bsp", (ftnlen)8);
    kinfo_("test.bsp", ktype, ksrc, &han0, &found0, (ftnlen)8, (ftnlen)255, (
	    ftnlen)4);
    chcksl_("KEEPBUG 0", &found0, &c_true, ok, (ftnlen)9);
    furnsh_("test.bsp", (ftnlen)8);
    kinfo_("test.bsp", ktype, ksrc, &han1, &found1, (ftnlen)8, (ftnlen)255, (
	    ftnlen)4);
    chcksl_("KEEPBUG 1", &found1, &c_true, ok, (ftnlen)9);
    chcksi_("KEEPBUG 2", &han0, "=", &han1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    unload_("test.bsp", (ftnlen)8);
    kinfo_("test.bsp", ktype, ksrc, &han2, &found2, (ftnlen)8, (ftnlen)255, (
	    ftnlen)4);
    chcksl_("KEEPBUG 3", &found2, &c_true, ok, (ftnlen)9);
    chcksi_("KEEPBUG 4", &han0, "=", &han2, &c__0, ok, (ftnlen)9, (ftnlen)1);
    unload_("test.bsp", (ftnlen)8);
    kinfo_("test.bsp", ktype, ksrc, &han3, &found3, (ftnlen)8, (ftnlen)255, (
	    ftnlen)4);
    chcksl_("KEEPBUG 5", &found3, &c_false, ok, (ftnlen)9);
    if (exists_("test.bsp", (ftnlen)8)) {
	delfil_("test.bsp", (ftnlen)8);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_kpbug__ */

