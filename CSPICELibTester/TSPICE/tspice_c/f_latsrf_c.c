/*

-Procedure f_latsrf_c ( latsrf_c tests )

 
-Abstract
 
   Exercise the CSPICE wrapper latsrf_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_latsrf_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrapper 

      latsrf_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Removed unnecessary call to dascls_c.

   01-AUG-2016 (NJB)

      Original version.


-Index_Entries

   test latsrf_c

-&
*/

{ /* Begin f_latsrf_c */

 
   /*
   Constants
   */
   #define PCK0            "latsrf_test.tpc"
   #define TITLEN          321
   #define TIGHT           1.e-12
   #define MAXN            10000

   /*
   Local macros 
   */
   #define DP              (SpiceDouble *)

   /*
   Local variables
   */
   static SpiceBoolean     fndarr [MAXN];
   SpiceBoolean            found;
   SpiceBoolean            pri;

   SpiceChar             * dsk;
   SpiceChar             * fixref;
   SpiceChar             * frame;
   SpiceChar             * method;
   SpiceChar             * target;
   SpiceChar               title  [ TITLEN ];

   SpiceDouble             a;
   SpiceDouble             b;
   SpiceDouble             c;
   static SpiceDouble      dirarr [MAXN][3];
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             et;
   SpiceDouble             lat;
   SpiceDouble             lon;
   static SpiceDouble      lonlat [MAXN][2];
   SpiceDouble             r;
   SpiceDouble             radii  [3];
   static SpiceDouble      srfpts [MAXN][3];
   SpiceDouble             tol;
   static SpiceDouble      vrtarr [MAXN][3];
   static SpiceDouble      xptarr [MAXN][3];
   SpiceDouble             xxpt   [3];

   SpiceInt                bodyid;
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                npts;
   SpiceInt                nslat;
   SpiceInt                nslon;
   SpiceInt                nsurf;
   SpiceInt                surfid;
   SpiceInt                srflst  [ SPICE_SRF_MAXSRF ];


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_latsrf_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a text PCK." );
   
   if ( exists_c(PCK0) ) 
   {
      removeFile(PCK0);
   }

   /*
   Don't load the PCK; do save it. 
   */
   tstpck_c ( PCK0, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load via furnsh_c to avoid later complexities. 
   */
   furnsh_c ( PCK0 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a DSK file containing segments for "
             "Mars and Saturn."                                 );
 

   /*
   We'll use a test utility that creates a tessellated plate model DSK. 
   */
   bodyid = 499;
   surfid = 1;
   frame  = "IAU_MARS";
   nlon   = 80;
   nlat   = 40;

   dsk    = "latsrf_test_0.bds";

   if ( exists_c(dsk) )
   {
      removeFile( dsk );
   }

   /*
   Create the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Add a Saturn segment. 
   */
   bodyid = 699;
   surfid = 2;
   frame  = "IAU_SATURN";
   nlon   = 60;
   nlat   = 30;

   /*
   Append to the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 


   /*
   Load the dsk for later use. 
   */
   furnsh_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 

 
  
   /* 
   ---- Case ---------------------------------------------------------
   */

   /*

   Find surface points corresponding to a lon/lat grid.

   Compare results against those obtained using dskxv_c.
   For each surface point, we'll use a ray pointing inward
   toward the target (Mars) and find the surface intercept of 
   this ray.
   */

   /*
   The surface list consists of one surface ID (for the only Mars
   segment).
   */
   target    = "Mars";
   fixref    = "IAU_MARS";

   bodvrd_c ( target, "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   a         = radii[0];
   b         = radii[1];
   c         = radii[2];

   nsurf     = 1;
   srflst[0] = 1;

   /*
   Pick our longitude and latitude band counts so we don't 
   end up sampling from plate boundaries. This simplifies
   our checks on the outputs.
   */
   nslon = 37;
   nslat = 23;

   dlon  = twopi_c() / nslon;
   dlat  = pi_c()    / nslat;

   /*
   Pick a magnitude for the ray's vertex. 
   */
   r = 1.0e6;

   /*
   Pick an evaluation epoch. 
   */
   et = 10 * jyear_c();

 


   /*
    ********************************************************************

   MARS VECTORIZED CASE

   Start out by creating arrays of lon/lat coordinates, 
   ray vertices and direction  vectors.

    ********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create input arrays." );
   
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         
         lat = halfpi_c() - j*dlat;

         /*
         Store the lon/lat values for the current surface point. 
         */
         lonlat[k][0] = lon;
         lonlat[k][1] = lat;


         /*
         Create the test ray's vertex and direction vector.
         */
         latrec_c ( r,    lon,  lat, vrtarr[k] );
         vminus_c ( vrtarr[k],       dirarr[k] );

         ++k;
      }
   }

   npts      = k;

   nsurf     = 1;
   srflst[0] = 1;




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Mars ellipsoid test." );

   /*
   Ellipsoid case.
   */
   method = "Ellipsoid";


   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   /*
   Find ray-surface intercepts for all the lon/lat pairs. We don't have
   a vectorized routine that does this for ellipsoids (other than
   latsrf_c itself), so use surfpt.
   */
   for ( k = 0;  k < npts;  k++ )
   {
      strncpy ( title, "Mars ellipsoid point #", TITLEN );

      repmi_c ( title, "#", k, TITLEN, title );
      chckxc_c ( SPICEFALSE, " ", ok );

      /* 
      ---- Case ------------------------------------------------------
      */
      tcase_c ( title );


      surfpt_c ( vrtarr[k], dirarr[k], a, b, c, xxpt, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "surfpt_c found", found, SPICETRUE, ok );


      tol = TIGHT;

      /*
      Check the kth point. 
      */ 
      chckad_c ( "srfpts[*]", srfpts[k], "~~/", xxpt, 3, tol, ok );
   }

   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Mars DSK test. Surface list is non-empty." );

   /*
   All latsrf_c inputs but the method string are already set.
   */

   method = "DSK/ unprioritized / surfaces = 1";
 
   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
  
   /*
   In this case we can use the vectorized routine dskxv_c to check
   the results. A few additional inputs are needed:
   */
   pri       = SPICEFALSE;
   nsurf     = 1;
   srflst[0] = 1;


   dskxv_c ( pri,  target, nsurf,  srflst, et,    fixref,
             npts, vrtarr, dirarr, xptarr, fndarr        );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results in one shot.
   */ 

   tol = TIGHT;

   chckad_c ( "srfpts", DP srfpts, "~~/", DP xptarr, 3*npts, tol, ok );


   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Mars DSK test. Surface list is empty." );

   /*
   All latsrf_c inputs but the method string are already set.
   */

   method = "Unprioritized/dsk";
 
   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
  
   /*
   In this case we can use the vectorized routine dskxv_c to check
   the results. A few additional inputs are needed:
   */
   pri       = SPICEFALSE;
   nsurf     = 0;


   dskxv_c ( pri,  target, nsurf,  srflst, et,    fixref,
             npts, vrtarr, dirarr, xptarr, fndarr        );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results in one shot.
   */ 

   tol = TIGHT;

   chckad_c ( "srfpts", DP srfpts, "~~/", DP xptarr, 3*npts, tol, ok );





   /*
    ********************************************************************

   SATURN VECTORIZED CASE

   Change target and time.

    ********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn ellipsoid test." );


   target    = "Saturn";
   fixref    = "IAU_SATURN";

   bodvrd_c ( target, "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   a         = radii[0];
   b         = radii[1];
   c         = radii[2];

   nsurf     = 1;
   srflst[0] = 2;


   et        = -10 * jyear_c();

   /*
   Ellipsoid case.
   */
   method = "Ellipsoid";


   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   /*
   Find ray-surface intercepts for all the lon/lat pairs. We don't have
   a vectorized routine that does this for ellipsoids (other than
   latsrf_c itself), so use surfpt.
   */
   for ( k = 0;  k < npts;  k++ )
   {
      strncpy ( title, "Saturn ellipsoid point #", TITLEN );

      repmi_c ( title, "#", k, TITLEN, title );
      chckxc_c ( SPICEFALSE, " ", ok );

      /* 
      ---- Case ------------------------------------------------------
      */
      tcase_c ( title );


      surfpt_c ( vrtarr[k], dirarr[k], a, b, c, xxpt, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "surfpt_c found", found, SPICETRUE, ok );


      tol = TIGHT;

      /*
      Check the kth point. 
      */ 
      chckad_c ( "srfpts[*]", srfpts[k], "~~/", xxpt, 3, tol, ok );
   }

   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn DSK test. Surface list is non-empty." );

   /*
   All latsrf_c inputs but the method string are already set.
   */

   method = "DSK/ Unprioritized / Surfaces = 2";
 
   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
  
   /*
   In this case we can use the vectorized routine dskxv_c to check
   the results. A few additional inputs are needed:
   */
   pri       = SPICEFALSE;
   nsurf     = 1;
   srflst[0] = 2;


   dskxv_c ( pri,  target, nsurf,  srflst, et,    fixref,
             npts, vrtarr, dirarr, xptarr, fndarr        );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results in one shot.
   */ 

   tol = TIGHT;

   chckad_c ( "srfpts", DP srfpts, "~~/", DP xptarr, 3*npts, tol, ok );


   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn DSK test. Surface list is empty." );

   /*
   All latsrf_c inputs but the method string are already set.
   */

   method = "Unprioritized/dsk";
 
   /*
   Find surface points for all the lon/lat pairs.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );
  
   /*
   In this case we can use the vectorized routine dskxv_c to check
   the results. A few additional inputs are needed:
   */
   pri       = SPICEFALSE;
   nsurf     = 0;


   dskxv_c ( pri,  target, nsurf,  srflst, et,    fixref,
             npts, vrtarr, dirarr, xptarr, fndarr        );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the results in one shot.
   */ 

   tol = TIGHT;

   chckad_c ( "srfpts", DP srfpts, "~~/", DP xptarr, 3*npts, tol, ok );


   /*
   *********************************************************************
   *
   *
   *   latsrf_c error cases
   *
   *
   *********************************************************************
   */

   /*
   We need not test all of the error cases, but we must test at least
   one; this will exercise the error handling logic in the wrapper.
   */
   
   latsrf_c ( method, target, et, "XXX", npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(NOFRAME)", ok );

 
   /*
   For CSPICE, we must check handling of bad input strings.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "latsrf_c error: empty input string." );


   /*
   Use the last values of all the inputs. 
   */
   latsrf_c ( "", target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   latsrf_c ( method, "", et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   latsrf_c ( method, target, et, "", npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "latsrf_c error: null input string." );


   /*
   Use the last values of all the inputs. 
   */
   latsrf_c ( NULL, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   latsrf_c ( method, NULL, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   latsrf_c ( method, target, et, NULL, npts, lonlat, srfpts );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );


   kclear_c();

   removeFile( dsk );

 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_latsrf_c */

