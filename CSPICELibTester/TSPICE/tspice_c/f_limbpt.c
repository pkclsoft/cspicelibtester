/* f_limbpt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c_n499 = -499;
static integer c__499 = 499;
static integer c__1 = 1;
static integer c_n666 = -666;
static integer c__4 = 4;
static doublereal c_b161 = 0.;
static doublereal c_b163 = 1.;
static doublereal c_b217 = .005;
static doublereal c_b230 = 1e-6;
static doublereal c_b247 = 10.;
static integer c__1000 = 1000;
static integer c__100 = 100;
static integer c__400 = 400;
static integer c__14 = 14;
static doublereal c_b378 = 5e-6;
static doublereal c_b387 = .1;
static integer c__599 = 599;
static integer c__399 = 399;
static integer c__10 = 10;
static integer c__6 = 6;
static integer c_n1 = -1;
static doublereal c_b599 = -1.;
static integer c__2 = 2;
static doublereal c_b694 = 3.;

/* $Procedure      F_LIMBPT ( LIMBPT family tests ) */
/* Subroutine */ int f_limbpt__(logical *ok)
{
    /* Initialized data */

    static char abcs[10*5] = "None      " "Cn        " "Cn+s      " "Lt     "
	    "   " "Lt+s      ";
    static char corlcs[32*2] = "ELLIPSOID LIMB                  " "CENTER   "
	    "                       ";
    static char refs[32*1*2] = "IAU_MARS                        " "IAU_PHOBO"
	    "S                      ";
    static char obsnms[32*2] = "Earth                           " "MARS_ORBI"
	    "TER                    ";
    static char trgnms[32*2] = "Mars                            " "PHOBOS   "
	    "                       ";
    static char methds[500*7] = "ELLIPSOID / tangent                        "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "         " "tangent/ellipsoid                                   "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                " 
	    "tangent/dsk/unprioritized/surfaces=\"high-res\"                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                       " "tangen"
	    "t/dsk/unprioritized/surfaces=\"LOW-RES\"                        "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                " "guided/ellips"
	    "oid                                                             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                       " "dsk/ guided /unpriorit"
	    "ized/surfaces=\"high-res\"                                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                " "dsk/ guided /unprioritized/su"
	    "rfaces=\"LOW-RES\"                                              "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                         ";

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen), vadd_(doublereal *,
	     doublereal *, doublereal *);
    static doublereal limb[9];
    static logical ishi;
    static integer nlat;
    static doublereal dist, axis[3];
    static integer maxn;
    static doublereal htol, elts[8], utan[3];
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *), vsub_(
	    doublereal *, doublereal *, doublereal *), vequ_(doublereal *, 
	    doublereal *);
    static integer npts[1000];
    extern /* Subroutine */ int mtxv_(doublereal *, doublereal *, doublereal *
	    ), zzcorepc_(char *, doublereal *, doublereal *, doublereal *, 
	    ftnlen);
    static doublereal d__;
    static integer i__, j, k;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static char label[32];
    static doublereal r__, radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal pnear[3], theta;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static logical isell;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found, isdsk;
    static doublereal state[6];
    static char title[320];
    static logical istan, usecn;
    static doublereal lproj[3];
    extern /* Subroutine */ int topen_(char *, ftnlen), repmi_(char *, char *,
	     integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    static doublereal epnts[60000]	/* was [3][20000] */, xform[9]	/* 
	    was [3][3] */, trglt;
    static integer ncuts;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    static integer nsurf;
    static logical uselt;
    extern doublereal vnorm_(doublereal *), twopi_(void);
    extern /* Subroutine */ int spkw05_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), dskxv_(logical *, 
	    char *, integer *, integer *, doublereal *, char *, integer *, 
	    doublereal *, doublereal *, doublereal *, logical *, ftnlen, 
	    ftnlen), ucrss_(doublereal *, doublereal *, doublereal *), vprjp_(
	    doublereal *, doublereal *, doublereal *), bodn2c_(char *, 
	    integer *, logical *, ftnlen), vlcom_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), t_success__(logical *),
	     vrotv_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    el2cgv_(doublereal *, doublereal *, doublereal *, doublereal *);
    static char methd2[500];
    static doublereal state0[6], badrad[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen), boddef_(char *, integer *,
	     ftnlen), psv2pl_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static integer ub;
    static doublereal et;
    static integer abcidx, handle[3];
    extern /* Subroutine */ int edlimb_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static integer obscde;
    static doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    extern logical matchi_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksd_(char *, doublereal 
	    *, char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static char abcorr[10];
    static doublereal pmcoef[3], lplane[4], refvec[3], tdelta;
    static char corloc[32];
    extern logical exists_(char *, ftnlen);
    static char fixref[32], method[500], obsrvr[32], spkloc[32], srfnms[32*4],
	     target[32], trgfrm[32];
    static doublereal center[3], cutnml[3], epochs[1000], et0, etngts[60000]	
	    /* was [3][20000] */, istate[6], itanvc[3], lpoint[3], lptepc, 
	    normal[3], plnvec[3], points[60000]	/* was [3][20000] */, raydir[
	    3];
    extern logical odd_(integer *);
    static doublereal rcross, rolstp, schstp, smajor[3], sminor[3], soltol, 
	    tangts[60000]	/* was [3][20000] */, tanvec[3], trgpos[3], 
	    alt, vertex[3], xepoch[20000], xisrfv[3], xpoint[60000]	/* 
	    was [3][20000] */;
    extern doublereal rpd_(void);
    static doublereal xsrfvc[3];
    static integer bodyid, locidx, obsidx, refidx;
    static char utc[50];
    static integer srfbod[4], mix;
    static doublereal tol;
    static integer srfids[4];
    static doublereal xte;
    static integer surfid, timidx, trgcde, trgidx;
    static logical attblk[6];
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     t_pck08__(char *, logical *, logical *, ftnlen), bodvrd_(char *, 
	    char *, integer *, integer *, doublereal *, ftnlen, ftnlen), 
	    pdpool_(char *, integer *, doublereal *, ftnlen), tstlsk_(void);
    static doublereal xpt[3];
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), conics_(doublereal *, doublereal *, doublereal *)
	    , spkcls_(integer *), spklef_(char *, integer *, ftnlen), pcpool_(
	    char *, integer *, char *, ftnlen, ftnlen), pipool_(char *, 
	    integer *, integer *, ftnlen), furnsh_(char *, ftnlen), bodvar_(
	    integer *, char *, integer *, doublereal *, ftnlen), spkpos_(char 
	    *, doublereal *, char *, char *, char *, doublereal *, doublereal 
	    *, ftnlen, ftnlen, ftnlen, ftnlen), limbpt_(char *, char *, 
	    doublereal *, char *, char *, char *, char *, doublereal *, 
	    doublereal *, integer *, doublereal *, doublereal *, integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), pxform_(char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), spkcpt_(doublereal *,
	     char *, char *, doublereal *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), nearpt_(doublereal *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *), vminus_(doublereal *, 
	    doublereal *), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen), surfnm_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), natspk_(char *, logical *, integer *,
	     ftnlen), natpck_(char *, logical *, logical *, ftnlen), srfnrm_(
	    char *, char *, doublereal *, char *, integer *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen), unload_(char *, ftnlen), 
	    cleard_(integer *, doublereal *), spkuef_(integer *), bodvcd_(
	    integer *, char *, integer *, integer *, doublereal *, ftnlen), 
	    clpool_(void), dvpool_(char *, ftnlen), kclear_(void), t_torus__(
	    integer *, integer *, char *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, char *, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the higher-level SPICELIB geometry routine LIMBPT. */
/*     Use DSK-based and ellipsoidal target shape models. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine LIMBPT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0 29-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */

/*      DOUBLE PRECISION      VSEP */

/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     This array is two-dimensional. The second dimension */
/*     is indexed by the input METHOD. This makes it simpler */
/*     to control the combinations of methods and locus */
/*     values. */


/*     Note that the last two method strings are identical. This */
/*     is done to test the logic that uses saved values obtained */
/*     by parsing method string. */


/*     Begin every test family with an open call.q */

    topen_("F_LIMBPT", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create SPK, PCK file.", (ftnlen)29);
    if (exists_("limbpt_spk.bsp", (ftnlen)14)) {
	delfil_("limbpt_spk.bsp", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tstspk_("limbpt_spk.bsp", &c_true, handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK file, and load it. Do not delete it. */

    if (exists_("test_0008.tpc", (ftnlen)13)) {
	delfil_("test_0008.tpc", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_pck08__("test_0008.tpc", &c_true, &c_true, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Optionally scale Mars rotation. */

    bodvrd_("MARS", "PM", &c__3, &n, pmcoef, (ftnlen)4, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Leave as is for delivery. */

    pmcoef[1] = pmcoef[1];
    pdpool_("BODY499_PM", &c__3, pmcoef, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create LSK, load it, and delete it. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set initial time. */

    s_copy(utc, "2004 FEB 17", (ftnlen)50, (ftnlen)11);
    str2et_(utc, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = et0;

/*     Create a Mars orbiter SPK file. */

    if (exists_("orbiter.bsp", (ftnlen)11)) {
	delfil_("orbiter.bsp", (ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("orbiter.bsp", "orbiter.bsp", &c__0, &handle[1], (ftnlen)11, (
	    ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up elements defining a state.  The elements expected */
/*     by CONICS are: */

/*        RP      Perifocal distance. */
/*        ECC     Eccentricity. */
/*        INC     Inclination. */
/*        LNODE   Longitude of the ascending node. */
/*        ARGP    Argument of periapse. */
/*        M0      Mean anomaly at epoch. */
/*        T0      Epoch. */
/*        MU      Gravitational parameter. */

    elts[0] = 3800.;
    elts[1] = .1;
    elts[2] = rpd_() * 80.;
    elts[3] = 0.;
    elts[4] = rpd_() * 90.;
    elts[5] = 0.;
    elts[6] = et;
    elts[7] = 42828.314;
    conics_(elts, &et, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = jyear_() * -10;
    d__2 = jyear_() * 10;
    spkw05_(&handle[1], &c_n499, &c__499, "MARSIAU", &d__1, &d__2, "Mars orb"
	    "iter", &elts[7], &c__1, state0, &et, (ftnlen)7, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the new SPK file. */

    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add the orbiter's name/ID mapping to the kernel pool. */

    pcpool_("NAIF_BODY_NAME", &c__1, obsnms + 32, (ftnlen)14, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_BODY_CODE", &c__1, &c_n499, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add an incomplete frame definition to the kernel pool; */
/*     we'll need this later. */

    pipool_("FRAME_BAD_NAME", &c__1, &c_n666, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 220;
    nlat = 110;
    if (exists_("limbpt_dsk0.bds", (ftnlen)15)) {
	delfil_("limbpt_dsk0.bds", (ftnlen)15);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "limbpt_dsk0.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars has slightly lower resolution. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 190;
    nlat = 95;
    if (exists_("limbpt_dsk1.bds", (ftnlen)15)) {
	delfil_("limbpt_dsk1.bds", (ftnlen)15);
    }

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "limbpt_dsk1.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("limbpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is high-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("limbpt_dsk2.bds", (ftnlen)15)) {
	delfil_("limbpt_dsk2.bds", (ftnlen)15);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "limbpt_dsk2.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("limbpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("limbpt_dsk3.bds", (ftnlen)15)) {
	delfil_("limbpt_dsk3.bds", (ftnlen)15);
    }

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "limbpt_dsk3.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("limbpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 401;
    srfids[2] = 1;
    s_copy(srfnms + 64, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[3] = 401;
    srfids[3] = 2;
    s_copy(srfnms + 96, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__4, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__4, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Main test loop follows. */


/*     This loop uses a single epoch. The set of tests using */
/*     nested tori, located further down in this file, does */
/*     vary the input times. */

/*     Loop over every choice of observer. */

    for (obsidx = 1; obsidx <= 2; ++obsidx) {
	s_copy(obsrvr, obsnms + (((i__1 = obsidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("obsnms", i__1, "f_limbpt__", (ftnlen)675)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the observer ID code. */

	bodn2c_(obsrvr, &obscde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over every choice of target. */

	for (trgidx = 1; trgidx <= 2; ++trgidx) {
	    s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? 
		    i__1 : s_rnge("trgnms", i__1, "f_limbpt__", (ftnlen)687)) 
		    << 5), (ftnlen)32, (ftnlen)32);

/*           Set the target ID code. */

	    bodn2c_(target, &trgcde, &found, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Get target radii. */

	    bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over every aberration correction locus. */

	    for (locidx = 1; locidx <= 2; ++locidx) {
		s_copy(corloc, corlcs + (((i__1 = locidx - 1) < 2 && 0 <= 
			i__1 ? i__1 : s_rnge("corlcs", i__1, "f_limbpt__", (
			ftnlen)705)) << 5), (ftnlen)32, (ftnlen)32);

/*              Loop over every aberration correction choice. */

		for (abcidx = 1; abcidx <= 5; ++abcidx) {
		    s_copy(abcorr, abcs + ((i__1 = abcidx - 1) < 5 && 0 <= 
			    i__1 ? i__1 : s_rnge("abcs", i__1, "f_limbpt__", (
			    ftnlen)711)) * 10, (ftnlen)10, (ftnlen)10);
		    zzprscor_(abcorr, attblk, (ftnlen)10);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Set up some logical variables describing the */
/*                 attributes of the selected correction. */

		    uselt = attblk[1];
		    usecn = attblk[3];

/*                 Loop over every target body-fixed frame choice. */

		    for (refidx = 1; refidx <= 1; ++refidx) {
			s_copy(trgfrm, refs + (((i__1 = refidx + trgidx - 2) <
				 2 && 0 <= i__1 ? i__1 : s_rnge("refs", i__1, 
				"f_limbpt__", (ftnlen)728)) << 5), (ftnlen)32,
				 (ftnlen)32);

/*                    Loop over all method choices that are */
/*                    compatible with the choice of CORLOC. */

/*                    The "center" locus is required  in order */
/*                    to use the "guided" methods. */

			if (eqstr_(corloc, "CENTER", (ftnlen)32, (ftnlen)6)) {
			    ub = 7;
			} else {

/*                       The locus is 'ELLIPSOID LIMB'. Only */
/*                       the "tangent" methods can be used. */

			    ub = 4;
			}
			i__1 = ub;
			for (mix = 1; mix <= i__1; ++mix) {
			    s_copy(method, methds + ((i__2 = mix - 1) < 7 && 
				    0 <= i__2 ? i__2 : s_rnge("methds", i__2, 
				    "f_limbpt__", (ftnlen)752)) * 500, (
				    ftnlen)500, (ftnlen)500);
			    isell = matchi_(method, "*ELLIPSOID*", "*", "?", (
				    ftnlen)500, (ftnlen)11, (ftnlen)1, (
				    ftnlen)1);
			    isdsk = ! isell;
			    istan = matchi_(method, "*TANGENT*", "*", "?", (
				    ftnlen)500, (ftnlen)9, (ftnlen)1, (ftnlen)
				    1);
			    ishi = matchi_(method, "*HIGH*", "*", "?", (
				    ftnlen)500, (ftnlen)6, (ftnlen)1, (ftnlen)
				    1);

/* --- Case: ------------------------------------------------------ */

			    s_copy(title, "Observer = #; Target = #; ABCORR "
				    "= #; TRGFRM = #; METHOD = #; CORLOC = #.",
				     (ftnlen)320, (ftnlen)73);
			    repmc_(title, "#", obsrvr, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)32, (ftnlen)320);
			    repmc_(title, "#", target, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)32, (ftnlen)320);
			    repmc_(title, "#", abcorr, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)10, (ftnlen)320);
			    repmc_(title, "#", trgfrm, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)32, (ftnlen)320);
			    repmc_(title, "#", method, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)500, (ftnlen)320);
			    repmc_(title, "#", corloc, title, (ftnlen)320, (
				    ftnlen)1, (ftnlen)32, (ftnlen)320);
			    tcase_(title, (ftnlen)320);

/*                       Start off by computing the set of limb points */
/*                       We'll then check the results. */

			    ncuts = 4;
			    rolstp = twopi_() / ncuts;
			    vpack_(&c_b161, &c_b161, &c_b163, refvec);

/*                       We expect a single limb point in each half */
/*                       plane, so we can use a large step. */

			    schstp = 4.;

/*                       Derive the solution tolerance from the height */
/*                       error tolerance and the observer-target center */
/*                       distance. */

			    spkpos_(target, &et, trgfrm, abcorr, obsrvr, 
				    trgpos, &trglt, (ftnlen)32, (ftnlen)32, (
				    ftnlen)10, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    dist = vnorm_(trgpos);
			    htol = 1e-6;
			    soltol = htol / dist;
			    maxn = 20000;

/*                       If the shape is DSK and the locus is ELLIPSOID */
/*                       limb, make a preliminary call to obtain the */
/*                       expected epochs. */

			    if (isdsk && eqstr_(corloc, "ELLIPSOID LIMB", (
				    ftnlen)32, (ftnlen)14)) {

/*                          Create a version of the method string with */
/*                          an ellipsoid shape specification. */

				s_copy(methd2, "ELLIPSOID/TANGENT", (ftnlen)
					500, (ftnlen)17);

/*                          Get the expected epochs for the call below */
/*                          that uses a DSK target shape. */

				limbpt_(methd2, target, &et, trgfrm, abcorr, 
					corloc, obsrvr, refvec, &rolstp, &
					ncuts, &schstp, &soltol, &maxn, npts, 
					epnts, xepoch, etngts, (ftnlen)500, (
					ftnlen)32, (ftnlen)32, (ftnlen)10, (
					ftnlen)32, (ftnlen)32);
			    }
			    limbpt_(method, target, &et, trgfrm, abcorr, 
				    corloc, obsrvr, refvec, &rolstp, &ncuts, &
				    schstp, &soltol, &maxn, npts, points, 
				    epochs, tangts, (ftnlen)500, (ftnlen)32, (
				    ftnlen)32, (ftnlen)10, (ftnlen)32, (
				    ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Check the outputs. */

			    k = 1;
			    i__2 = ncuts;
			    for (i__ = 1; i__ <= i__2; ++i__) {

/*                          For convex shapes that include the origin, */
/*                          there should be just one limb point per */
/*                          cutting half-plane. */

				s_copy(label, "NPTS(#)", (ftnlen)32, (ftnlen)
					7);
				repmi_(label, "#", &i__, label, (ftnlen)32, (
					ftnlen)1, (ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				chcksi_(label, &npts[(i__3 = i__ - 1) < 1000 
					&& 0 <= i__3 ? i__3 : s_rnge("npts", 
					i__3, "f_limbpt__", (ftnlen)859)], 
					"=", &c__1, &c__0, ok, (ftnlen)32, (
					ftnlen)1);
				i__4 = npts[(i__3 = i__ - 1) < 1000 && 0 <= 
					i__3 ? i__3 : s_rnge("npts", i__3, 
					"f_limbpt__", (ftnlen)862)];
				for (j = 1; j <= i__4; ++j) {

/*                             We'll treat the Jth limb point as an */
/*                             ephemeris object and find its position */
/*                             relative to the observer. */

				    vequ_(&points[(i__3 = k * 3 - 3) < 60000 
					    && 0 <= i__3 ? i__3 : s_rnge(
					    "points", i__3, "f_limbpt__", (
					    ftnlen)868)], lpoint);
				    vequ_(&tangts[(i__3 = k * 3 - 3) < 60000 
					    && 0 <= i__3 ? i__3 : s_rnge(
					    "tangts", i__3, "f_limbpt__", (
					    ftnlen)869)], tanvec);
				    lptepc = epochs[(i__3 = i__ - 1) < 1000 &&
					     0 <= i__3 ? i__3 : s_rnge("epoc"
					    "hs", i__3, "f_limbpt__", (ftnlen)
					    870)];

/*                             Get an inertially referenced version */
/*                             of the tangent vector. */

				    pxform_("J2000", trgfrm, &lptepc, xform, (
					    ftnlen)5, (ftnlen)32);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    mtxv_(xform, tanvec, itanvc);

/*                             Set the SPK lookup locus to be compatible */
/*                             with the aberration correction locus used */
/*                             with the limb point. */

				    if (s_cmp(corloc, "CENTER", (ftnlen)32, (
					    ftnlen)6) == 0) {

/*                                In this case we're using aberration */
/*                                corrections for the target body */
/*                                center. We must compute the expected */
/*                                state of the limb point "manually." */

					spkpos_(target, &et, trgfrm, abcorr, 
						obsrvr, trgpos, &trglt, (
						ftnlen)32, (ftnlen)32, (
						ftnlen)10, (ftnlen)32);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					vadd_(trgpos, lpoint, xsrfvc);

/*                                For the 'CENTER' locus, epochs */
/*                                associated with limb points be the */
/*                                epoch associated with the target */
/*                                body's center. */

					lt = trglt;

/*                                Get an inertially referenced version */
/*                                of the expected tangent vector. */

					pxform_("J2000", trgfrm, &lptepc, 
						xform, (ftnlen)5, (ftnlen)32);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					mtxv_(xform, xsrfvc, xisrfv);
				    } else {

/*                                This is the 'ellipsoid limb' case. */

					s_copy(spkloc, "TARGET", (ftnlen)32, (
						ftnlen)6);
					spkcpt_(lpoint, target, trgfrm, &et, 
						trgfrm, spkloc, abcorr, 
						obsrvr, state, &lt, (ftnlen)
						32, (ftnlen)32, (ftnlen)32, (
						ftnlen)32, (ftnlen)10, (
						ftnlen)32);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					vequ_(state, xsrfvc);
					spkcpt_(lpoint, target, trgfrm, &et, 
						"J2000", spkloc, abcorr, 
						obsrvr, istate, &lt, (ftnlen)
						32, (ftnlen)32, (ftnlen)5, (
						ftnlen)32, (ftnlen)10, (
						ftnlen)32);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					vequ_(istate, xisrfv);
				    }

/*                             If LPOINT is correct, then the position */
/*                             of LPOINT relative to the observer should */
/*                             be equal to TANVEC. The light time */
/*                             obtained from SPKCPT or computed manually */
/*                             should match that implied by LPTEPC. */

				    tol = 1e-14;

/*                             If the light time correction is */
/*                             non-converged, we can't expect tight */
/*                             agreement. */

				    if (uselt && ! usecn) {
					tol *= 100.;
				    }
				    if (isdsk && s_cmp(corloc, "ELLIPSOID LI"
					    "MB", (ftnlen)32, (ftnlen)14) == 0)
					     {

/*                                Set the expected epoch using that */
/*                                obtained using the ellipsoid target */
/*                                shape. */

					xte = xepoch[(i__3 = i__ - 1) < 20000 
						&& 0 <= i__3 ? i__3 : s_rnge(
						"xepoch", i__3, "f_limbpt__", 
						(ftnlen)971)];
				    } else {

/*                                Compute the expected epoch from the */
/*                                SPKCPT or SPKPOS call. */

					zzcorepc_(abcorr, &et, &lt, &xte, (
						ftnlen)10);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
				    }
				    s_copy(label, "LPTEPC PLANE #, PT #", (
					    ftnlen)32, (ftnlen)20);
				    repmi_(label, "#", &i__, label, (ftnlen)
					    32, (ftnlen)1, (ftnlen)32);
				    repmi_(label, "#", &j, label, (ftnlen)32, 
					    (ftnlen)1, (ftnlen)32);
				    chcksd_(label, &lptepc, "~/", &xte, &tol, 
					    ok, (ftnlen)32, (ftnlen)2);
				    if (uselt) {
					if (usecn) {
					    tol = 1e-10;
					} else {
					    tol = 5e-6;
					}
				    } else {
					tol = 1e-14;
				    }
				    if (isdsk) {

/*                                Use looser tolerances, since the */
/*                                aberration correction performed by */
/*                                LIMBPT is for the corresponding */
/*                                ellipsoid limb point. */

					tol *= 1e3;
				    }
				    s_copy(label, "TANVEC PLANE #, PT #", (
					    ftnlen)32, (ftnlen)20);
				    repmi_(label, "#", &i__, label, (ftnlen)
					    32, (ftnlen)1, (ftnlen)32);
				    repmi_(label, "#", &j, label, (ftnlen)32, 
					    (ftnlen)1, (ftnlen)32);
				    if (eqstr_(obsrvr, "MARS_ORBITER", (
					    ftnlen)32, (ftnlen)12)) {

/*                                Check the absolute distance error for */
/*                                this case. Use a 5m tolerance. */

					chckad_(label, tanvec, "~~", xsrfvc, &
						c__3, &c_b217, ok, (ftnlen)32,
						 (ftnlen)2);
				    } else {

/*                                Compare the inertially referenced */
/*                                tangent vectors. */

					s_copy(label, "ITANVC PLANE #, PT #", 
						(ftnlen)32, (ftnlen)20);
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);

/*                                Use a tighter tolerance for the */
/*                                inertial vector test. */

					d__1 = tol / 10;
					chckad_(label, itanvc, "~~/", xisrfv, 
						&c__3, &d__1, ok, (ftnlen)32, 
						(ftnlen)3);

/*                                Check the relative error for the */
/*                                body-fixed vectors. */

					chckad_(label, tanvec, "~~/", xsrfvc, 
						&c__3, &tol, ok, (ftnlen)32, (
						ftnlen)3);
				    }
				    if (! (*ok)) {

/*                                Perform the following test just to */
/*                                display the absolute magnitude of */
/*                                the error. */

					s_copy(label, "TANVEC PLANE #, PT #", 
						(ftnlen)32, (ftnlen)20);
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);
					chckad_(label, tanvec, "~~", xsrfvc, &
						c__3, &c_b230, ok, (ftnlen)32,
						 (ftnlen)2);
					s_copy(label, "ITANVC PLANE #, PT #", 
						(ftnlen)32, (ftnlen)20);
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);
					chckad_(label, itanvc, "~~", xisrfv, &
						c__3, &c_b230, ok, (ftnlen)32,
						 (ftnlen)2);
				    }
				    if (abcidx <= 3 && isell) {

/*                                The correction is 'NONE', 'CN', or */
/*                                'CN+S'. The shape is 'ELLIPSOID'. Look */
/*                                for millimeter-level agreement. */
					chckad_(label, tanvec, "~~", xsrfvc, &
						c__3, &c_b230, ok, (ftnlen)32,
						 (ftnlen)2);
				    }

/*                             We've checked the consistency of LPOINT, */
/*                             TANVEC, and LPTEPC, but we haven't done */
/*                             anything to show that LPOINT is a limb */
/*                             point. Do this now. */

/*                             We need to verify that */

/*                                1) The limb point is on the target */
/*                                   body's surface. */

/*                                2) For the "tangent" methods, the */
/*                                   limb point is a point of tangency */
/*                                   on the target of a ray emanating */
/*                                   from the observer. */

/*                                   For the "guided" methods, the limb */
/*                                   point lies on a ray emanating from */
/*                                   the center of the limb of the */
/*                                   reference ellipsoid. */

/*                                   When the target shape is an */
/*                                   ellipsoid, there should be no */
/*                                   difference between results obtained */
/*                                   using the tangent and guided */
/*                                   methods. */

/*                                3) The limb point lies in the */
/*                                   correct half-plane. */


/*                             Check the limb point's distance from the */
/*                             target surface. */

				    if (isell) {

/*                                Find the altitude of the limb point */
/*                                with respect to the reference */
/*                                ellipsoid. */

					nearpt_(lpoint, radii, &radii[1], &
						radii[2], pnear, &alt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                For ellipsoidal targets, one micron */
/*                                is a generous tolerance. */

					tol = 1e-9;
					s_copy(label, "LIMBPT ALT, PLANE #, "
						"PT #", (ftnlen)32, (ftnlen)25)
						;
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);
					chcksd_(label, &alt, "~", &c_b161, &
						tol, ok, (ftnlen)32, (ftnlen)
						1);
				    } else {

/*                                This is the DSK case. */

/*                                We'll create an inward-pointing ray */
/*                                that ideally passes through the limb */
/*                                point, and we'll find the ray-surface */
/*                                intercept. This intercept should be */
/*                                close to the original limb point. */
					vscl_(&c_b247, lpoint, vertex);
					vminus_(vertex, raydir);
					nsurf = 1;
					if (ishi) {
					    surfid = 1;
					} else {
					    surfid = 2;
					}
					dskxv_(&c_false, target, &nsurf, &
						surfid, &et, trgfrm, &c__1, 
						vertex, raydir, xpt, &found, (
						ftnlen)32, (ftnlen)32);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					chcksl_("DSKXV FOUND", &found, &
						c_true, ok, (ftnlen)11);

/*                                Our ray should hit the DSK surface very */
/*                                close to the limb point. One micron is */
/*                                enough margin. */

					tol = 1e-9;
					dist = vdist_(xpt, lpoint);
					s_copy(label, "LIMBPT DIST, PLANE #,"
						" PT #", (ftnlen)32, (ftnlen)
						26);
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);
					chcksd_(label, &dist, "~", &c_b161, &
						tol, ok, (ftnlen)32, (ftnlen)
						1);
				    }

/*                             Verify that the putative limb point is */
/*                             really on the limb. */

/*                             Create the central axis and normal to the */
/*                             current cutting half-plane. We'll use */
/*                             these for the DSK case below, and for the */
/*                             next set of tests, in which the limb */
/*                             point is tested for inclusion in the */
/*                             cutting half- plane. */
				    vsub_(lpoint, tanvec, axis);
				    d__1 = (i__ - 1) * rolstp;
				    vrotv_(refvec, axis, &d__1, plnvec);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    ucrss_(axis, plnvec, cutnml);

				    if (isell) {

/*                                This is the ellipsoid case. */

/*                                If LPOINT is all it's claimed to be, */
/*                                the outward normal at LPOINT should be */
/*                                orthogonal to the observer-limb point */
/*                                vector. */

					surfnm_(radii, &radii[1], &radii[2], 
						lpoint, normal);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					vhat_(tanvec, utan);
					d__ = vdot_(normal, utan);
					tol = 1e-14;
					s_copy(label, "<LIMBPT, TANGNT>, PLA"
						"NE #, PT #", (ftnlen)32, (
						ftnlen)31);
					repmi_(label, "#", &i__, label, (
						ftnlen)32, (ftnlen)1, (ftnlen)
						32);
					repmi_(label, "#", &j, label, (ftnlen)
						32, (ftnlen)1, (ftnlen)32);
					chcksd_(label, &d__, "~", &c_b161, &
						tol, ok, (ftnlen)32, (ftnlen)
						1);
				    } else {

/*                                This is the DSK case. */

/*                                We could check the relative orientation */
/*                                of the outward normal and the */
/*                                associated tangent vector, but we */
/*                                can't expect these vectors to be */
/*                                orthogonal. */

/*                                We can perform a more accurate check */
/*                                by determining whether a small */
/*                                rotation of the ray defined by the */
/*                                observer and the tangent vector, */
/*                                performed within the cutting */
/*                                half-plane, will move the ray off of */
/*                                the target. */
					if (istan) {

/*                                   This is the DSK tangent case. */

/*                                   Rotate the tangent ray away from */
/*                                   the axis by twice the angular */
/*                                   search tolerance. Presuming the ray */
/*                                   was nearly tangential, it should */
/*                                   now point off the target. */

					    theta = soltol * -2;
					    vrotv_(tanvec, cutnml, &theta, 
						    raydir);
					    nsurf = 1;
					    if (ishi) {
			  surfid = 1;
					    } else {
			  surfid = 2;
					    }
					    dskxv_(&c_false, target, &nsurf, &
						    surfid, &et, trgfrm, &
						    c__1, axis, raydir, xpt, &
						    found, (ftnlen)32, (
						    ftnlen)32);
					    chckxc_(&c_false, " ", ok, (
						    ftnlen)1);
					    s_copy(label, "Ray hit PLANE #, "
						    "PT #", (ftnlen)32, (
						    ftnlen)21);
					    repmi_(label, "#", &i__, label, (
						    ftnlen)32, (ftnlen)1, (
						    ftnlen)32);
					    repmi_(label, "#", &j, label, (
						    ftnlen)32, (ftnlen)1, (
						    ftnlen)32);
					    chcksl_(label, &found, &c_false, 
						    ok, (ftnlen)32);
					} else {

/*                                   This is the DSK "guided" case. */

/*                                   We've already checked that the limb */
/*                                   point is on the DSK surface; we */
/*                                   need to make sure it's in the */
/*                                   reference ellipsoid's limb plane. */
/*                                   The last check we'll do will ensure */
/*                                   the point is in the correct cutting */
/*                                   half-plane. */

/*                                   Create the ellipsoid limb and */
/*                                   the limb plane. */

					    edlimb_(radii, &radii[1], &radii[
						    2], axis, limb);
					    chckxc_(&c_false, " ", ok, (
						    ftnlen)1);
					    el2cgv_(limb, center, smajor, 
						    sminor);
					    chckxc_(&c_false, " ", ok, (
						    ftnlen)1);
					    psv2pl_(center, smajor, sminor, 
						    lplane);

/*                                   Project the guided DSK limb point */
/*                                   onto the limb plane. */

					    vprjp_(lpoint, lplane, lproj);
					    dist = vdist_(lpoint, lproj);

/*                                   One micron is a generous tolerance */
/*                                   for the distance of the limb point */
/*                                   from the ellipsoid limb plane. */

					    tol = 1e-9;
					    s_copy(label, "LIMBPT ERROR, PLA"
						    "NE #, PT #", (ftnlen)32, (
						    ftnlen)27);
					    repmi_(label, "#", &i__, label, (
						    ftnlen)32, (ftnlen)1, (
						    ftnlen)32);
					    repmi_(label, "#", &j, label, (
						    ftnlen)32, (ftnlen)1, (
						    ftnlen)32);
					    chcksd_(label, &alt, "~", &c_b161,
						     &tol, ok, (ftnlen)32, (
						    ftnlen)1);
					}
				    }

/*                             Verify that the limb point is in the */
/*                             cutting half-plane. This test applies to */
/*                             both ellipsoids and DSKs. */

/*                             The plane containing the cutting */
/*                             half-plane contains the origin, so the */
/*                             distance from the plane of a point is */
/*                             given by the dot product of the point */
/*                             with the plane's unit normal vector. */

				    d__ = vdot_(cutnml, lpoint);
				    tol = 1e-6;
				    s_copy(label, "<LIMBPT, CUTNML>, PLANE #"
					    ", PT #", (ftnlen)32, (ftnlen)31);
				    repmi_(label, "#", &i__, label, (ftnlen)
					    32, (ftnlen)1, (ftnlen)32);
				    repmi_(label, "#", &j, label, (ftnlen)32, 
					    (ftnlen)1, (ftnlen)32);
				    chcksd_(label, &d__, "~", &c_b161, &tol, 
					    ok, (ftnlen)32, (ftnlen)1);

/*                             Update K to point to the next limb point. */

				    ++k;
				}
			    }

/*                       We're finished with the consistency checks. */

			}

/*                    End of the method loop. */

		    }

/*                 End of the reference frame loop. */

		}

/*              End of the aberration correction loop. */

	    }

/*           End of the correction locus loop. */

	}

/*        End of the target loop. */

    }

/*     End of the observer loop. */

/* *********************************************************************** */

/*     Normal case: nested tori as target shape */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Torus test setup", (ftnlen)16);

/*     Create and load Nat's SPK and PCK. */

    natspk_("nat.bsp", &c_true, &handle[2], (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_true, (ftnlen)7);

/*     Create DSK containing two nested tori. */

    if (exists_("limbpt_dsk4.bds", (ftnlen)15)) {
	delfil_("limbpt_dsk4.bds", (ftnlen)15);
    }
    surfid = 3;
    s_copy(trgfrm, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    r__ = 7e4;
    rcross = 1e4;
    vpack_(&c_b161, &c_b161, &c_b161, center);
    vpack_(&c_b163, &c_b161, &c_b161, normal);
    t_torus__(&c__1000, &surfid, trgfrm, &c__100, &c__400, &r__, &rcross, 
	    center, normal, "limbpt_dsk4.bds", (ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = 4e4;
    vpack_(&c_b161, &c_b161, &c_b161, center);
    vpack_(&c_b163, &c_b161, &c_b161, normal);
    t_torus__(&c__1000, &surfid, trgfrm, &c__100, &c__400, &r__, &rcross, 
	    center, normal, "limbpt_dsk4.bds", (ftnlen)32, (ftnlen)15);
    furnsh_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare LIMBPT call. */

    s_copy(obsrvr, "SUN", (ftnlen)32, (ftnlen)3);
    s_copy(target, "ALPHA", (ftnlen)32, (ftnlen)5);
    s_copy(corloc, "CENTER", (ftnlen)32, (ftnlen)6);
    s_copy(method, "DSK/TANGENT/UNPRIORITIZED", (ftnlen)500, (ftnlen)25);
    et0 = 0.;
    tdelta = 1e5;

/*     Loop over epochs. */

    for (timidx = 1; timidx <= 5; ++timidx) {
	et = et0 + (timidx - 1) * tdelta;

/*        Loop over the aberration correction choices. For this */
/*        test, we use only 'NONE' and 'CN'. */

	for (abcidx = 1; abcidx <= 2; ++abcidx) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(abcorr, abcs + ((i__1 = abcidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("abcs", i__1, "f_limbpt__", (ftnlen)1504)) *
		     10, (ftnlen)10, (ftnlen)10);
	    s_copy(title, "Observer = #; Target = #; ABCORR = #; TRGFRM = #;"
		    " METHOD = #; CORLOC = #; ET = #.", (ftnlen)320, (ftnlen)
		    81);
	    repmc_(title, "#", obsrvr, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    32, (ftnlen)320);
	    repmc_(title, "#", target, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    32, (ftnlen)320);
	    repmc_(title, "#", abcorr, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    10, (ftnlen)320);
	    repmc_(title, "#", trgfrm, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    32, (ftnlen)320);
	    repmc_(title, "#", method, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    500, (ftnlen)320);
	    repmc_(title, "#", corloc, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    32, (ftnlen)320);
	    repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1, (
		    ftnlen)320);
	    tcase_(title, (ftnlen)320);

/*           Start off by computing the set of limb points */
/*           We'll then check the results. */

	    ncuts = 1;
	    rolstp = twopi_() / ncuts;
	    vpack_(&c_b161, &c_b161, &c_b163, refvec);

/*           We expect a multiple limb points in each half */
/*           plane, so we must use a small step. */

	    schstp = 1e-4;

/*           Derive the solution tolerance from the height */
/*           error tolerance and the observer-target center */
/*           distance. */

	    spkpos_(target, &et, trgfrm, abcorr, obsrvr, trgpos, &trglt, (
		    ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dist = vnorm_(trgpos);
	    htol = 1e-6;
	    soltol = htol / dist;
	    maxn = 20000;
	    limbpt_(method, target, &et, trgfrm, abcorr, corloc, obsrvr, 
		    refvec, &rolstp, &ncuts, &schstp, &soltol, &maxn, npts, 
		    points, epochs, tangts, (ftnlen)500, (ftnlen)32, (ftnlen)
		    32, (ftnlen)10, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the outputs. */

	    k = 1;
	    i__1 = ncuts;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              There should be 4 limb points in each cutting */
/*              half-plane. */

		s_copy(label, "NPTS HALF-PLANE #,", (ftnlen)32, (ftnlen)18);
		repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			ftnlen)32);
		chcksi_(label, &npts[(i__2 = i__ - 1) < 1000 && 0 <= i__2 ? 
			i__2 : s_rnge("npts", i__2, "f_limbpt__", (ftnlen)
			1570)], "=", &c__4, &c__0, ok, (ftnlen)32, (ftnlen)1);
		i__4 = npts[(i__2 = i__ - 1) < 1000 && 0 <= i__2 ? i__2 : 
			s_rnge("npts", i__2, "f_limbpt__", (ftnlen)1573)];
		for (j = 1; j <= i__4; ++j) {

/*                 We'll treat the Jth limb point as an */
/*                 ephemeris object and find its position */
/*                 relative to the observer. */

		    vequ_(&points[(i__2 = k * 3 - 3) < 60000 && 0 <= i__2 ? 
			    i__2 : s_rnge("points", i__2, "f_limbpt__", (
			    ftnlen)1579)], lpoint);
		    vequ_(&tangts[(i__2 = k * 3 - 3) < 60000 && 0 <= i__2 ? 
			    i__2 : s_rnge("tangts", i__2, "f_limbpt__", (
			    ftnlen)1580)], tanvec);
		    lptepc = epochs[(i__2 = i__ - 1) < 1000 && 0 <= i__2 ? 
			    i__2 : s_rnge("epochs", i__2, "f_limbpt__", (
			    ftnlen)1581)];
		    s_copy(spkloc, "TARGET", (ftnlen)32, (ftnlen)6);
		    spkcpt_(lpoint, target, trgfrm, &et, trgfrm, spkloc, 
			    abcorr, obsrvr, state, &lt, (ftnlen)32, (ftnlen)
			    32, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
			    32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    vequ_(state, xsrfvc);

/*                 Since we're using the 'CENTER' locus, the light time */
/*                 obtained from SPKPOS should match that implied by */
/*                 LPTEPC. */

		    tol = 1e-14;
		    zzcorepc_(abcorr, &et, &trglt, &xte, (ftnlen)10);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_copy(label, "LPTEPC PLANE #, PT #", (ftnlen)32, (ftnlen)
			    20);
		    repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    chcksd_(label, &lptepc, "~/", &xte, &tol, ok, (ftnlen)32, 
			    (ftnlen)2);

/*                 If LPOINT is correct, then the position */
/*                 of LPOINT relative to the observer should */
/*                 be equal to TANVEC. */

		    if (s_cmp(abcorr, "NONE", (ftnlen)10, (ftnlen)4) == 0) {
			tol = 1e-14;
		    } else {

/*                    Use looser tolerances, since the aberration */
/*                    correction performed by LIMBPT is for the */
/*                    target center. */
			tol = 1e-12;
		    }
		    s_copy(label, "TANVEC PLANE #, PT #", (ftnlen)32, (ftnlen)
			    20);
		    repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);

/*                 Check the absolute distance error for */
/*                 this case. Use a 5mm tolerance. */

		    chckad_(label, tanvec, "~~", xsrfvc, &c__3, &c_b378, ok, (
			    ftnlen)32, (ftnlen)2);

/*                 Check the relative error for this case. */

		    chckad_(label, tanvec, "~~/", xsrfvc, &c__3, &tol, ok, (
			    ftnlen)32, (ftnlen)3);
		    if (! (*ok)) {

/*                    Perform the following test just to */
/*                    display the absolute magnitude of */
/*                    the error. */

			chckad_(label, tanvec, "~~", xsrfvc, &c__3, &c_b230, 
				ok, (ftnlen)32, (ftnlen)2);
		    }

/*                 We've checked the consistency of LPOINT, TANVEC, and */
/*                 LPTEPC, but we haven't done anything to show that */
/*                 LPOINT is a limb point. Do this now. */

/*                 Check the limb point's distance from the target */
/*                 surface. */

/*                 We'll create an inward-pointing ray that ideally */
/*                 passes through the limb point, and we'll find the */
/*                 ray-surface intercept. This intercept should be close */
/*                 to the original limb point. */

/*                 Create the ray's vertex using the outward normal at */
/*                 the surface point. The ray's direction will be the */
/*                 negative of the normal. */
		    srfnrm_("DSK/UNPRIORITIZED", target, &et, trgfrm, &c__1, 
			    lpoint, normal, (ftnlen)17, (ftnlen)32, (ftnlen)
			    32);
		    vlcom_(&c_b163, lpoint, &c_b387, normal, vertex);
		    vminus_(normal, raydir);
		    nsurf = 1;
		    dskxv_(&c_false, target, &nsurf, &surfid, &et, trgfrm, &
			    c__1, vertex, raydir, xpt, &found, (ftnlen)32, (
			    ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("DSKXV FOUND", &found, &c_true, ok, (ftnlen)11);

/*                 Our ray should hit the DSK surface very */
/*                 close to the limb point. One micron is */
/*                 enough margin. */

		    tol = 1e-9;
		    dist = vdist_(xpt, lpoint);
		    s_copy(label, "LIMBPT DIST, PLANE #, PT #", (ftnlen)32, (
			    ftnlen)26);
		    repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    chcksd_(label, &dist, "~", &c_b161, &tol, ok, (ftnlen)32, 
			    (ftnlen)1);

/*                 Verify that the putative limb point is */
/*                 really on the limb. */

/*                 Create the central axis and normal to the */
/*                 current cutting half-plane. We'll use these */
/*                 for the DSK case below, and for the next */
/*                 set of tests, in which the limb point is */
/*                 tested for inclusion in the cutting half- */
/*                 plane. */

		    vsub_(lpoint, tanvec, axis);
		    d__1 = (i__ - 1) * rolstp;
		    vrotv_(refvec, axis, &d__1, plnvec);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    ucrss_(axis, plnvec, cutnml);

/*                 This is the DSK tangent case. */

/*                 Rotate the tangent ray to move it off of the target */
/*                 surface. The rotation is within the cutting */
/*                 half-plane. The direction of rotation depends on the */
/*                 placement of the surface point: for points having */
/*                 outward normals pointing away from the axis, the */
/*                 rotation is away from the axis; for the other points, */
/*                 the direction is inward. For the nested tori, the */
/*                 points having and odd index are the ones for which */
/*                 the rotation is outward. */

/*                 Outward rotation about CUTNML corresponds to a */
/*                 negative rotation angle. */

/*                 The rotation angle is set to twice the angular search */
/*                 tolerance. Presuming the ray was nearly tangential, */
/*                 it should now point off the target. */

		    if (odd_(&j)) {
			theta = soltol * -2;
		    } else {
			theta = soltol * 2;
		    }
		    vrotv_(tanvec, cutnml, &theta, raydir);
		    nsurf = 1;
		    dskxv_(&c_false, target, &nsurf, &surfid, &et, trgfrm, &
			    c__1, axis, raydir, xpt, &found, (ftnlen)32, (
			    ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_copy(label, "Ray hit PLANE #, PT #", (ftnlen)32, (
			    ftnlen)21);
		    repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    chcksl_(label, &found, &c_false, ok, (ftnlen)32);

/*                 Verify that the limb point is in the cutting */
/*                 half-plane. This test applies to both ellipsoids and */
/*                 DSKs. */

/*                 The plane containing the cutting half-plane contains */
/*                 the origin, so the distance from the plane of a point */
/*                 is given by the dot product of the point with the */
/*                 plane's unit normal vector. */

		    d__ = vdot_(cutnml, lpoint);
		    tol = 1e-6;
		    s_copy(label, "<LIMBPT, CUTNML>, PLANE #, PT #", (ftnlen)
			    32, (ftnlen)31);
		    repmi_(label, "#", &i__, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (
			    ftnlen)32);
		    chcksd_(label, &d__, "~", &c_b161, &tol, ok, (ftnlen)32, (
			    ftnlen)1);

/*                 Update K to point to the next limb point. */

		    ++k;
		}
	    }

/*           We're finished with the consistency checks. */

	}

/*        End of the aberration correction loop. */

    }

/*     End of the time loop. */


/*     Unload the torus DSK. Leaving it loaded would interfere */
/*     with some of the following tests. */

    unload_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */

/*     Normal case: input handling */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Input handling tests:  make sure target and observer */
/*     can be identified using integer "names." */

/* *********************************************************************** */

/*     Normal case: state change detection */

/* *********************************************************************** */

/*     Certain subsystem state changes must be detected and responded to */
/*     by SINCPT. The subsystems (or structures) having states that must */
/*     be monitored are: */

/*        - Target name-ID mapping */

/*        - Observer name-ID mapping */

/*        - Surface name-ID mapping */

/*        - Target body-fixed frame definition */

/*        - ZZDSKBSR state */


/* --- Case: ------------------------------------------------------ */

    tcase_("Target name changed to JUPITER for ID code 499.", (ftnlen)47);

/*     First, get expected intercept. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    ncuts = 1;
    s_copy(method, "DSK/UNPRIORITIZED/GUIDED", (ftnlen)500, (ftnlen)24);
    s_copy(corloc, "CENTER", (ftnlen)32, (ftnlen)6);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, xpoint, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    boddef_("JUPITER", &c__499, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("POINTS", points, "=", xpoint, &c__3, &c_b161, ok, (ftnlen)6, (
	    ftnlen)1);
/*     Restore original mapping. */

    boddef_("JUPITER", &c__599, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer name changed to SUN for ID code 399.", (ftnlen)45);
    boddef_("SUN", &c__399, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("POINTS", points, "=", xpoint, &c__3, &c_b161, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars high-res surface name changed to AAAbbb.", (ftnlen)45);

/*     Get expected results first. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "DSK/UNPRIORITIZED/GUIDED/surfaces = 1", (ftnlen)500, (
	    ftnlen)37);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, xpoint, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(srfnms, "AAAbbb", (ftnlen)32, (ftnlen)6);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "guided/dsk/unprioritized/surfaces = AAAbbb", (ftnlen)500, 
	    (ftnlen)42);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("POINTS", points, "=", xpoint, &c__3, &c_b161, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars high-res DSK.", (ftnlen)25);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "guided/dsk/unprioritized/surfaces = low-res", (ftnlen)500,
	     (ftnlen)43);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, xpoint, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unload the high-res DSK; set METHOD to remove */
/*     surface specification. */

    unload_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "guided/dsk/unprioritized", (ftnlen)500, (ftnlen)24);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("POINTS", points, "=", xpoint, &c__3, &c_b161, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars low-res DSK; reload Mars high-res DSK.", (ftnlen)50);

/*     Restore DSK, unload low-res DSK, and repeat computation. */

    furnsh_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "guided/dsk/unprioritized", (ftnlen)500, (ftnlen)24);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the result matches that obtained with the */
/*     high-res DSK specified. */

    s_copy(method, "guided/dsk/unprioritized/ SURFACES = \"HIGH-RES\" ", (
	    ftnlen)500, (ftnlen)48);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, xpoint, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("POINTS", points, "=", xpoint, &c__3, &c_b161, ok, (ftnlen)6, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Error handling tests follow. */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid method.", (ftnlen)15);
    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "ELLIPSOID TANGENT", (ftnlen)500, (ftnlen)17);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    s_copy(method, "DSK/GUIDED", (ftnlen)500, (ftnlen)10);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);
    s_copy(method, "DSK/UNPRIORITIZED", (ftnlen)500, (ftnlen)17);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDLIMBTYPE)", ok, (ftnlen)22);
    s_copy(method, "UNPRIORITIZED /DSK/guided/INTERCEPT", (ftnlen)500, (
	    ftnlen)35);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);
    s_copy(method, "UNPRIORITIZED/umbral /DSK/guided/", (ftnlen)500, (ftnlen)
	    33);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);

/*     Restore a valid method. We'll use the tangent limb */
/*     type because it requires the search step and */
/*     tolerance. */

    s_copy(method, "TANGENT/DSK/UNPRIORITIZED", (ftnlen)500, (ftnlen)25);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid target name.", (ftnlen)20);
    limbpt_(method, "marr", &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)4, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid observer name.", (ftnlen)22);
    limbpt_(method, target, &et, fixref, abcorr, corloc, "sn", refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)2);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer is target.", (ftnlen)19);
    limbpt_(method, target, &et, fixref, abcorr, corloc, target, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid reference frame center", (ftnlen)30);
    limbpt_(method, target, &et, "IAU_MOON", abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)8, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame not found", (ftnlen)25);
    limbpt_(method, target, &et, "IAU_M", abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)5, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid aberration correction", (ftnlen)29);
    limbpt_(method, target, &et, fixref, "L", corloc, obsrvr, refvec, &rolstp,
	     &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, tangts, (
	    ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)1, (ftnlen)32, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*     Test SAVE logic by repeating the call. */

    limbpt_(method, target, &et, fixref, "L", corloc, obsrvr, refvec, &rolstp,
	     &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, tangts, (
	    ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)1, (ftnlen)32, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Transmission aberration correction", (ftnlen)34);
    limbpt_(method, target, &et, fixref, "XCN", corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)3, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Relativistic aberration correction", (ftnlen)34);
    limbpt_(method, target, &et, fixref, "RL", corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)2, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stellar aberration correction w/o light time", (ftnlen)44);
    limbpt_(method, target, &et, fixref, "S", corloc, obsrvr, refvec, &rolstp,
	     &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, tangts, (
	    ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)1, (ftnlen)32, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid aberration correction locus", (ftnlen)35);
    limbpt_(method, target, &et, fixref, abcorr, "dsk", obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    3, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDLOCUS)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid reference vector.", (ftnlen)25);
    cleard_(&c__6, state);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, state, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid cut count.", (ftnlen)18);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &c__0, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &c_n1, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    i__1 = maxn + 1;
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &i__1, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid maximum point count.", (ftnlen)28);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &c__0, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &c_n1, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid search tolerance.", (ftnlen)25);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &c_b599, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);
    limbpt_("ellipsoid/tangent", target, &et, fixref, abcorr, corloc, obsrvr, 
	    refvec, &rolstp, &ncuts, &c_b599, &soltol, &maxn, npts, points, 
	    epochs, tangts, (ftnlen)17, (ftnlen)32, (ftnlen)32, (ftnlen)10, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid search step.", (ftnlen)20);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &c_b599, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDSEARCHSTEP)", ok, (ftnlen)24);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &c_b161, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDSEARCHSTEP)", ok, (ftnlen)24);
    limbpt_("ellipsoid/tangent", target, &et, fixref, abcorr, corloc, obsrvr, 
	    refvec, &rolstp, &ncuts, &c_b599, &soltol, &maxn, npts, points, 
	    epochs, tangts, (ftnlen)17, (ftnlen)32, (ftnlen)32, (ftnlen)10, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid roll step.", (ftnlen)18);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    c_b161, &c__2, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDROLLSTEP)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded SPK files", (ftnlen)19);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOLOADEDFILES)", ok, (ftnlen)20);
    spklef_("limbpt_spk.bsp", handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spklef_("nat.bsp", &handle[2], (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for observer", (ftnlen)30);
    limbpt_(method, target, &et, fixref, abcorr, corloc, "GASPRA", refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for target", (ftnlen)28);
    furnsh_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, "ALPHA", &et, "ALPHAFIXED", abcorr, corloc, obsrvr, 
	    refvec, &rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, 
	    epochs, tangts, (ftnlen)500, (ftnlen)5, (ftnlen)10, (ftnlen)10, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);
    unload_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No orientation data for target", (ftnlen)30);

/*     Fetch target radii. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Clear the kernel pool. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore target radii so we can get to the error */
/*     condition we're looking for. */

    pdpool_("BODY499_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No radius data for target", (ftnlen)25);

/*     We need a method that uses an ellipsoid for this one. */

    s_copy(method, "DSK / UNPRIORITIZED / GUIDED", (ftnlen)500, (ftnlen)28);
    dvpool_("BODY499_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    furnsh_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad radius data for target", (ftnlen)26);

/*     Fetch original radii. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Overwrite good radii with bad in the kernel pool. */

    vpack_(&c_b599, &c_b161, &c_b694, badrad);
    pdpool_("BODY499_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);

/*     Replace original radii. */

    pdpool_("BODY499_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad radius count for target", (ftnlen)27);

/*     Fetch original radii. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Overwrite good radii with short array */
/*     in the kernel pool. */

    pdpool_("BODY499_RADII", &c__2, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADRADIUSCOUNT)", ok, (ftnlen)21);

/*     Replace original radii. */

    pdpool_("BODY499_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Not enough room in output arrays.", (ftnlen)33);

/*     The checks on NCUTS and MAXN prevent this situation from */
/*     occurring in the usual case, in which there is one limb */
/*     point per cutting half-plane. The torus case can present */
/*     problems. */

/*     Prepare LIMBPT call. */

    s_copy(obsrvr, "SUN", (ftnlen)32, (ftnlen)3);
    s_copy(target, "ALPHA", (ftnlen)32, (ftnlen)5);
    s_copy(fixref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
    s_copy(corloc, "CENTER", (ftnlen)32, (ftnlen)6);
    s_copy(method, "DSK/TANGENT/UNPRIORITIZED", (ftnlen)500, (ftnlen)25);
    et = 0.;
    ncuts = 2;
    maxn = 2;
    furnsh_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.tpc", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spklef_("nat.bsp", &handle[2], (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(OUTOFROOM)", ok, (ftnlen)16);
    unload_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("GUIDED limb definition w/ ELLIPSOID LIMB correction locus.", (
	    ftnlen)58);
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(corloc, "ELLIPSOID LIMB", (ftnlen)32, (ftnlen)14);
    s_copy(method, "GUIDED/DSK/UNPRIORITIZED", (ftnlen)500, (ftnlen)24);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADLIMBLOCUSMIX)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded DSKs.", (ftnlen)15);
    unload_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "guided/dsk/unprioritized", (ftnlen)500, (ftnlen)24);
    limbpt_(method, target, &et, fixref, abcorr, corloc, obsrvr, refvec, &
	    rolstp, &ncuts, &schstp, &soltol, &maxn, npts, points, epochs, 
	    tangts, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Clean up. */

    kclear_();
    delfil_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.tpc", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_spk.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("orbiter.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("limbpt_dsk4.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_limbpt__ */

