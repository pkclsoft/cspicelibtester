/* f_matrix3.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b9 = -1.;
static doublereal c_b10 = 1.;
static integer c__9 = 9;
static doublereal c_b21 = 0.;
static doublereal c_b54 = 1e-15;
static integer c__3 = 3;
static doublereal c_b80 = 1e-14;

/* $Procedure F_MATRIX3 (Family of tests on 3x3-matrix operations) */
/* Subroutine */ int f_matrix3__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer seed;
    doublereal eval, mran[9]	/* was [3][3] */;
    integer indx;
    doublereal vran[3];
    extern /* Subroutine */ int mequ_(doublereal *, doublereal *);
    extern doublereal vdot_(doublereal *, doublereal *);
    doublereal expt, mout[9]	/* was [3][3] */;
    extern /* Subroutine */ int mtxm_(doublereal *, doublereal *, doublereal *
	    ), mxmt_(doublereal *, doublereal *, doublereal *);
    extern doublereal vtmv_(doublereal *, doublereal *, doublereal *);
    doublereal vout[3];
    extern /* Subroutine */ int mtxv_(doublereal *, doublereal *, doublereal *
	    );
    integer i__, j;
    extern doublereal trace_(doublereal *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal mindx[9]	/* was [3][3] */;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal vindx[3], vtemp[3], mzero[9]	/* was [3][3] */;
    extern /* Subroutine */ int xpose_(doublereal *, doublereal *);
    doublereal vexpt[3];
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal det_(doublereal *), t_randd__(doublereal *, doublereal *
	    , integer *);
    extern /* Subroutine */ int mxm_(doublereal *, doublereal *, doublereal *)
	    , mxv_(doublereal *, doublereal *, doublereal *);

/* $ Abstract */

/*     This routine performs rudimentary tests on a collection of */
/*     3x3-matrix operation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     M.C. Kim      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 24-AUG-2016 (MCK) (BVS) */

/*        Initial code to test */

/*           DET */
/*           MEQU */
/*           MTXM */
/*           MTXV */
/*           MXM */
/*           MXMT */
/*           MXV */
/*           TRACE */
/*           VTMV */
/*           XPOSE */

/* -& */
/* $ Index_Entries */

/*     test 3x3 matrix operation routines */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Initial seed number for pseudo-random number generation */


/*     Dimension of 3-by-3 square matrices */


/*     Number of elements in an N-by-N matrix. */


/*     Local Variables */


/*     indices */


/*     Seed number for pseudo-random number generation */


/*     3-component vectors */


/*     3-by-3 matrices */


/*     Evaluated number from a library routine. */


/*     Expected value of EVAL */


/*     Begin every test family with an open call. */

    topen_("F_MATRIX3", (ftnlen)9);

/*     Form arrays to be used for more than one test case. */


/*     A zero matrix */

    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    mzero[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		    "mzero", i__1, "f_matrix3__", (ftnlen)223)] = 0.;
	}
    }

/*     An index vector, whose components are their indices */

    for (i__ = 1; i__ <= 3; ++i__) {
	vindx[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vindx", i__1,
		 "f_matrix3__", (ftnlen)231)] = (doublereal) i__;
    }

/*     An index matrix, whose elements are their column-wise */
/*     storage indices */

    indx = 0;
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    ++indx;
	    mindx[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		    "mindx", i__1, "f_matrix3__", (ftnlen)242)] = (doublereal)
		     indx;
	}
    }

/*     Initialize the seed number of random number generation. */

    seed = 19920810;

/*     Form a random vector, whose components are */
/*     uniformly distributed on the interval [-1.0D0, +1.0D0]. */

    for (i__ = 1; i__ <= 3; ++i__) {
	vran[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vran", i__1, 
		"f_matrix3__", (ftnlen)257)] = t_randd__(&c_b9, &c_b10, &seed)
		;
    }

/*     Form a random matrix, whose elements are */
/*     uniformly distributed on the interval [-1.0D0, +1.0D0]. */

    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    mran[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		    "mran", i__1, "f_matrix3__", (ftnlen)266)] = t_randd__(&
		    c_b9, &c_b10, &seed);
	}
    }

/*     Cases to test MEQU */

    tcase_("MEQU: copying a 3x3 zero matrix and checking strict equality bet"
	    "ween the zero matrix and the copied matrix", (ftnlen)106);
    mequ_(mzero, mout);
    chckad_("MOUT", mout, "=", mzero, &c__9, &c_b21, ok, (ftnlen)4, (ftnlen)1)
	    ;
    tcase_("MEQU: copying a 3x3 random matrix and checking strict equality b"
	    "etween the random matrix and the copied matrix", (ftnlen)110);
    mequ_(mran, mout);
    chckad_("MOUT", mout, "=", mran, &c__9, &c_b21, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test XPOSE */

    tcase_("XPOSE: generating the transpose of an index matrix, and checking"
	    " equality between original and transposed elements", (ftnlen)114);
    xpose_(mindx, mout);
    indx = 0;
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    ++indx;
	    expt = (doublereal) indx;
	    chcksd_("MOUT(J,I)", &mout[(i__1 = j + i__ * 3 - 4) < 9 && 0 <= 
		    i__1 ? i__1 : s_rnge("mout", i__1, "f_matrix3__", (ftnlen)
		    305)], "=", &expt, &c_b21, ok, (ftnlen)9, (ftnlen)1);
	}
    }

    tcase_("XPOSE: generating the transpose of a random matrix, and checking"
	    " equality between original and transposed elements", (ftnlen)114);
    xpose_(mran, mout);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    chcksd_("MOUT(J,I)", &mout[(i__1 = j + i__ * 3 - 4) < 9 && 0 <= 
		    i__1 ? i__1 : s_rnge("mout", i__1, "f_matrix3__", (ftnlen)
		    320)], "=", &mran[(i__2 = i__ + j * 3 - 4) < 9 && 0 <= 
		    i__2 ? i__2 : s_rnge("mran", i__2, "f_matrix3__", (ftnlen)
		    320)], &c_b21, ok, (ftnlen)9, (ftnlen)1);
	}
    }

/*     Cases to test TRACE */

    tcase_("TRACE: an index matrix, whose trace is 15.0D0", (ftnlen)45);
    eval = trace_(mindx);
    expt = 15.;
    chcksd_("EVAL", &eval, "=", &expt, &c_b21, ok, (ftnlen)4, (ftnlen)1);
    tcase_("TRACE: random matrix", (ftnlen)20);
    eval = trace_(mran);
    expt = 0.;
    for (i__ = 1; i__ <= 3; ++i__) {
	expt += mran[(i__1 = i__ + i__ * 3 - 4) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("mran", i__1, "f_matrix3__", (ftnlen)345)];
    }
    chcksd_("EVAL", &eval, "=", &expt, &c_b21, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test DET */

    tcase_("DET: random matrix", (ftnlen)18);
    eval = det_(mran);

/*     The determinant of a 3x3x matrix can be computed by adding three */
/*     up diagonal followed by subtracting three down-diagonal products. */

    expt = mran[0] * mran[4] * mran[8] + mran[3] * mran[7] * mran[2] + mran[6]
	     * mran[1] * mran[5] - mran[0] * mran[7] * mran[5] - mran[3] * 
	    mran[1] * mran[8] - mran[6] * mran[4] * mran[2];
    chcksd_("EVAL", &eval, "~", &expt, &c_b54, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test MXV */

    tcase_("MXV: random matrix and random vector", (ftnlen)36);
    mxv_(mran, vran, vout);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    vtemp[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vtemp", 
		    i__1, "f_matrix3__", (ftnlen)380)] = mran[(i__2 = i__ + j 
		    * 3 - 4) < 9 && 0 <= i__2 ? i__2 : s_rnge("mran", i__2, 
		    "f_matrix3__", (ftnlen)380)];
	}
	vexpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vexpt", i__1,
		 "f_matrix3__", (ftnlen)382)] = vdot_(vtemp, vran);
    }
    chckad_("VOUT", vout, "~", vexpt, &c__3, &c_b54, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MTXV */

    tcase_("MTXV: random matrix and random vector", (ftnlen)37);
    mtxv_(mran, vran, vout);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    vtemp[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vtemp", 
		    i__1, "f_matrix3__", (ftnlen)396)] = mran[(i__2 = j + i__ 
		    * 3 - 4) < 9 && 0 <= i__2 ? i__2 : s_rnge("mran", i__2, 
		    "f_matrix3__", (ftnlen)396)];
	}
	vexpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vexpt", i__1,
		 "f_matrix3__", (ftnlen)398)] = vdot_(vtemp, vran);
    }
    chckad_("VOUT", vout, "=", vexpt, &c__3, &c_b21, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test VTMV */

    tcase_("VTMV: random vector, random matrix, index vector ", (ftnlen)49);
    eval = vtmv_(vran, mran, vindx);

/*     First compute the product of MRAN and VINDX. */

    mxv_(mran, vindx, vtemp);

/*     Then, compute the expected value using the routine VDOT. */

    expt = vdot_(vran, vtemp);
    chcksd_("EVAL", &eval, "~", &expt, &c_b80, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test MXM */

    tcase_("MXM: Freivald's Algorithm", (ftnlen)25);

/*     Compute the product MOUT = MRAN * MINDX. */

    mxm_(mran, mindx, mout);

/*     Check MRAN * ( MINDX * VRAN ) = MOUT * VRAN, where VRAN is */
/*     an arbitrary vector. */

    mxv_(mindx, vran, vout);
    mxv_(mran, vout, vtemp);
    mxv_(mout, vran, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__3, &c_b80, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MTXM */

    tcase_("MTXM: Freivald's Algorithm", (ftnlen)26);

/*     Compute the product MOUT = Transpose(MRAN) * MINDX. */

    mtxm_(mran, mindx, mout);

/*     Check Transpose(MRAN) * ( MINDX * VRAN ) = MOUT * VRAN, */
/*     where VRAN is an arbitrary vector. */

    mxv_(mindx, vran, vout);
    mtxv_(mran, vout, vtemp);
    mxv_(mout, vran, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__3, &c_b80, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MXMT */

    tcase_("MXMT: Freivald's Algorithm", (ftnlen)26);

/*     Compute the product MOUT = MRAN * Transpose(MINDX). */

    mxmt_(mran, mindx, mout);

/*     Check MRAN * ( Transpose(MINDX) * VRAN ) = MOUT * VRAN, */
/*     where VRAN is an arbitrary vector. */

    mtxv_(mindx, vran, vout);
    mxv_(mran, vout, vtemp);
    mxv_(mout, vran, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__3, &c_b80, ok, (ftnlen)4, (ftnlen)1)
	    ;
    t_success__(ok);
    return 0;
} /* f_matrix3__ */

