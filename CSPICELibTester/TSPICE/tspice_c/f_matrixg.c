/* f_matrixg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b3 = -1.;
static doublereal c_b4 = 1.;
static integer c__7 = 7;
static integer c__6 = 6;
static integer c__42 = 42;
static doublereal c_b41 = 0.;
static integer c__1 = 1;
static doublereal c_b98 = 1e-14;

/* $Procedure F_MATRIXG (Family of tests on general matrix operations) */
/* Subroutine */ int f_matrixg__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer seed;
    doublereal eval, mnnc[42]	/* was [7][6] */, mncn[42]	/* was [6][7] 
	    */, mran[42]	/* was [7][6] */, vcol[6];
    extern /* Subroutine */ int mxmg_(doublereal *, doublereal *, integer *, 
	    integer *, integer *, doublereal *);
    doublereal mnrn[49]	/* was [7][7] */, mnnr[49]	/* was [7][7] */, 
	    expt;
    extern /* Subroutine */ int mxvg_(doublereal *, doublereal *, integer *, 
	    integer *, doublereal *);
    doublereal mout[42]	/* was [7][6] */, vrow[7], vout[7];
    integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), mequg_(doublereal *, 
	    integer *, integer *, doublereal *);
    extern doublereal vdotg_(doublereal *, doublereal *, integer *);
    doublereal matsq[49]	/* was [7][7] */;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal vtemp[7];
    extern /* Subroutine */ int mtxmg_(doublereal *, doublereal *, integer *, 
	    integer *, integer *, doublereal *), mxmtg_(doublereal *, 
	    doublereal *, integer *, integer *, integer *, doublereal *);
    extern doublereal vtmvg_(doublereal *, doublereal *, doublereal *, 
	    integer *, integer *);
    extern /* Subroutine */ int mtxvg_(doublereal *, doublereal *, integer *, 
	    integer *, doublereal *);
    doublereal vexpt[7];
    extern /* Subroutine */ int t_success__(logical *);
    integer ic;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    integer ir;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal traceg_(doublereal *, integer *);
    extern /* Subroutine */ int xposeg_(doublereal *, integer *, integer *, 
	    doublereal *);
    doublereal xposem[42]	/* was [6][7] */;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    doublereal mnx1[7]	/* was [7][1] */, m1xn[7]	/* was [1][7] */;

/* $ Abstract */

/*     This routine performs rudimentary tests on a collection of */
/*     general matrix operation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     M.C. Kim      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 11-FEB-2017 (MCK) (BVS) (NJB) */

/*        Removed unneeded declarations. */

/* -    TSPICE Version 1.0.0, 27-JUL-2016 (MCK) (BVS) */

/*        Initial code to test */

/*           MEQUG */
/*           XPOSEG */
/*           TRACEG */
/*           MXVG */
/*           MTXVG */
/*           VTMVG */
/*           MXMG */
/*           MTXMG */
/*           MXMTG */

/* -& */
/* $ Index_Entries */

/*     test general matrix operation routines */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Initial seed number for pseudo-random number generation */


/*     Number of rows in an arbitrary matrix */


/*     Number of columns in an arbitrary matrix */


/*     Number of rows (or columns) of a square matrix. */
/*     Should be set to MAX( NR, NC ). */


/*     Number of elements in an NR-by-NC matrix */


/*     Local Variables */


/*     An NR-by-NC pseudo-random matrix */


/*     A single-column matrix */


/*     A single-row matrix */


/*     Matrices having the same dimension as that of MRAN */


/*     Transpose of MRAN */


/*     An N-by-N matrix */


/*     Some other work space matrices */


/*     A column vector */


/*     A row vector */


/*     Some other work space vectors */


/*     seed number for pseudo-random number generation */


/*     An index */


/*     A row index */


/*     A column index */


/*     Some double precision numbers */


/*     Begin every test family with an open call. */

    topen_("F_MATRIXG", (ftnlen)9);

/*     Form arrays to be used for more than one test case. */


/*     Arbitrary matrices, whose components are pseudo-random */
/*     numbers uniformly distributed on the interval [-1.0D0,+1.0D0]. */

    seed = 20140416;
    for (ic = 1; ic <= 6; ++ic) {
	for (ir = 1; ir <= 7; ++ir) {
	    mran[(i__1 = ir + ic * 7 - 8) < 42 && 0 <= i__1 ? i__1 : s_rnge(
		    "mran", i__1, "f_matrixg__", (ftnlen)282)] = t_randd__(&
		    c_b3, &c_b4, &seed);
	}
    }
    for (i__ = 1; i__ <= 7; ++i__) {
	for (ir = 1; ir <= 7; ++ir) {
	    mnrn[(i__1 = ir + i__ * 7 - 8) < 49 && 0 <= i__1 ? i__1 : s_rnge(
		    "mnrn", i__1, "f_matrixg__", (ftnlen)288)] = t_randd__(&
		    c_b3, &c_b4, &seed);
	}
    }
    for (ic = 1; ic <= 6; ++ic) {
	for (i__ = 1; i__ <= 7; ++i__) {
	    mnnc[(i__1 = i__ + ic * 7 - 8) < 42 && 0 <= i__1 ? i__1 : s_rnge(
		    "mnnc", i__1, "f_matrixg__", (ftnlen)294)] = t_randd__(&
		    c_b3, &c_b4, &seed);
	}
    }
    for (i__ = 1; i__ <= 7; ++i__) {
	for (ir = 1; ir <= 7; ++ir) {
	    mnnr[(i__1 = ir + i__ * 7 - 8) < 49 && 0 <= i__1 ? i__1 : s_rnge(
		    "mnnr", i__1, "f_matrixg__", (ftnlen)300)] = t_randd__(&
		    c_b3, &c_b4, &seed);
	}
    }
    for (ic = 1; ic <= 7; ++ic) {
	for (i__ = 1; i__ <= 6; ++i__) {
	    mncn[(i__1 = i__ + ic * 6 - 7) < 42 && 0 <= i__1 ? i__1 : s_rnge(
		    "mncn", i__1, "f_matrixg__", (ftnlen)306)] = t_randd__(&
		    c_b3, &c_b4, &seed);
	}
    }

/*     A single-row matrix, whose elements are their column index */

    for (i__ = 1; i__ <= 7; ++i__) {
	m1xn[(i__1 = i__ - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("m1xn", i__1, 
		"f_matrixg__", (ftnlen)314)] = (doublereal) i__;
    }

/*     A square index matrix, whose components are storage indices */

    eval = 0.;
    for (ic = 1; ic <= 7; ++ic) {
	for (ir = 1; ir <= 7; ++ir) {
	    eval += 1.;
	    matsq[(i__1 = ir + ic * 7 - 8) < 49 && 0 <= i__1 ? i__1 : s_rnge(
		    "matsq", i__1, "f_matrixg__", (ftnlen)325)] = eval;
	}
    }

/*     Arbitrary vectors, whose components are pseudo-random */
/*     numbers uniformly distributed on the interval [-1.0D0,+1.0D0] */

    for (ic = 1; ic <= 6; ++ic) {
	vcol[(i__1 = ic - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("vcol", i__1, 
		"f_matrixg__", (ftnlen)334)] = t_randd__(&c_b3, &c_b4, &seed);
    }
    for (ir = 1; ir <= 7; ++ir) {
	vrow[(i__1 = ir - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("vrow", i__1, 
		"f_matrixg__", (ftnlen)338)] = t_randd__(&c_b3, &c_b4, &seed);
    }

/*     Cases to test MEGUG */

    tcase_("MEQUG: arbitrary matrix", (ftnlen)23);
    mequg_(mran, &c__7, &c__6, mout);

/*     Numerical tolerance for this test should be zero. */

    chckad_("MOUT", mout, "~", mran, &c__42, &c_b41, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test XPOSEG */

    tcase_("XPOSEG: 1-by-N matrix", (ftnlen)21);
    xposeg_(m1xn, &c__1, &c__7, mnx1);

/*     Numerical tolerance for this test should be zero. */

    chckad_("MNX1", mnx1, "~", m1xn, &c__7, &c_b41, ok, (ftnlen)4, (ftnlen)1);
    tcase_("XPOSEG: N-by-1 matrix", (ftnlen)21);
    xposeg_(mnx1, &c__7, &c__1, m1xn);

/*     Numerical tolerance for this test should be zero. */

    chckad_("M1XN", m1xn, "~", mnx1, &c__7, &c_b41, ok, (ftnlen)4, (ftnlen)1);
    tcase_("XPOSEG: arbitrary matrix", (ftnlen)24);
    xposeg_(mran, &c__7, &c__6, xposem);

/*     Check the equality of M = ( M + Transpose( Transpose( M ) ) ) / 2 */
/*     for an arbitrary matrix M ( that is MRAN herein). */

    for (ic = 1; ic <= 6; ++ic) {
	for (ir = 1; ir <= 7; ++ir) {
	    mout[(i__1 = ir + ic * 7 - 8) < 42 && 0 <= i__1 ? i__1 : s_rnge(
		    "mout", i__1, "f_matrixg__", (ftnlen)388)] = (mran[(i__2 =
		     ir + ic * 7 - 8) < 42 && 0 <= i__2 ? i__2 : s_rnge("mran"
		    , i__2, "f_matrixg__", (ftnlen)388)] + xposem[(i__3 = ic 
		    + ir * 6 - 7) < 42 && 0 <= i__3 ? i__3 : s_rnge("xposem", 
		    i__3, "f_matrixg__", (ftnlen)388)]) / 2.;
	}
    }

/*     Numerical tolerance for this test should be zero. */

    chckad_("MOUT", mout, "~", mran, &c__42, &c_b41, ok, (ftnlen)4, (ftnlen)1)
	    ;
    tcase_("XPOSEG: applying twice.", (ftnlen)23);
    xposeg_(mran, &c__7, &c__6, xposem);
    xposeg_(xposem, &c__6, &c__7, mout);

/*     Numerical tolerance for this test should be zero. */

    chckad_("MOUT", mout, "~", mran, &c__42, &c_b41, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test TRACEG */

    tcase_("TRACEG: square integer matrix", (ftnlen)29);
    eval = traceg_(matsq, &c__7);
    expt = 0.;
    for (i__ = 1; i__ <= 7; ++i__) {
	expt += matsq[(i__1 = i__ + i__ * 7 - 8) < 49 && 0 <= i__1 ? i__1 : 
		s_rnge("matsq", i__1, "f_matrixg__", (ftnlen)418)];
    }

/*     Being MATSQ an integer matrix, numerical tolerance for this test */
/*     should be zero. */

    chcksd_("EVAL", &eval, "~", &expt, &c_b41, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test MXVG */

    tcase_("MXVG: multiplying an arbitrary matrix and an arbitrary column ve"
	    "ctor.", (ftnlen)69);
    mxvg_(mran, vcol, &c__7, &c__6, vout);
    for (ir = 1; ir <= 7; ++ir) {
	for (ic = 1; ic <= 6; ++ic) {
	    vtemp[(i__1 = ic - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("vtemp", 
		    i__1, "f_matrixg__", (ftnlen)437)] = mran[(i__2 = ir + ic 
		    * 7 - 8) < 42 && 0 <= i__2 ? i__2 : s_rnge("mran", i__2, 
		    "f_matrixg__", (ftnlen)437)];
	}
	vexpt[(i__1 = ir - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("vexpt", i__1, 
		"f_matrixg__", (ftnlen)439)] = vdotg_(vtemp, vcol, &c__6);
    }
    chckad_("VOUT", vout, "~", vexpt, &c__7, &c_b98, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MTXVG */

    tcase_("MTXVG: multiplying the transpose of an arbitrary matrix and an a"
	    "rbitrary vector", (ftnlen)79);
    mtxvg_(mran, vrow, &c__6, &c__7, vout);
    for (ic = 1; ic <= 6; ++ic) {
	for (ir = 1; ir <= 7; ++ir) {
	    vtemp[(i__1 = ir - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("vtemp", 
		    i__1, "f_matrixg__", (ftnlen)454)] = mran[(i__2 = ir + ic 
		    * 7 - 8) < 42 && 0 <= i__2 ? i__2 : s_rnge("mran", i__2, 
		    "f_matrixg__", (ftnlen)454)];
	}
	vexpt[(i__1 = ic - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge("vexpt", i__1, 
		"f_matrixg__", (ftnlen)456)] = vdotg_(vtemp, vrow, &c__7);
    }
    chckad_("VOUT", vout, "~", vexpt, &c__6, &c_b98, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test VTMVG */

    tcase_("VTMVG: ", (ftnlen)7);
    eval = vtmvg_(vrow, mran, vcol, &c__7, &c__6);
    expt = 0.;
    for (ic = 1; ic <= 6; ++ic) {
	for (ir = 1; ir <= 7; ++ir) {
	    expt += vrow[(i__1 = ir - 1) < 7 && 0 <= i__1 ? i__1 : s_rnge(
		    "vrow", i__1, "f_matrixg__", (ftnlen)471)] * mran[(i__2 = 
		    ir + ic * 7 - 8) < 42 && 0 <= i__2 ? i__2 : s_rnge("mran",
		     i__2, "f_matrixg__", (ftnlen)471)] * vcol[(i__3 = ic - 1)
		     < 6 && 0 <= i__3 ? i__3 : s_rnge("vcol", i__3, "f_matri"
		    "xg__", (ftnlen)471)];
	}
    }
    chcksd_("EVAL", &eval, "~", &expt, &c_b98, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test MXMG */

    tcase_("MXMG: Freivald's Algorithm", (ftnlen)26);

/*     Compute the product MOUT = MNRN * MNNC. */

    mxmg_(mnrn, mnnc, &c__7, &c__7, &c__6, mout);

/*     Check MNRN * ( MNNC * VCOL ) = MOUT * VCOL, where VCOL is */
/*     an arbitrary vector. */

    mxvg_(mnnc, vcol, &c__7, &c__6, vout);
    mxvg_(mnrn, vout, &c__7, &c__7, vtemp);
    mxvg_(mout, vcol, &c__7, &c__6, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__7, &c_b98, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MTXMG */

    tcase_("MTXMG: Freivald's Algorithm", (ftnlen)27);

/*     Compute the product MOUT = Transpose(MNNR) * MNNC. */

    mtxmg_(mnnr, mnnc, &c__7, &c__7, &c__6, mout);

/*     Check Transpose(MNNR) * ( MNNC * VCOL ) = MOUT * VCOL, */
/*     where VCOL is an arbitrary vector. */

    mxvg_(mnnc, vcol, &c__7, &c__6, vout);
    mtxvg_(mnnr, vout, &c__7, &c__7, vtemp);
    mxvg_(mout, vcol, &c__7, &c__6, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__7, &c_b98, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Cases to test MXMTG */

    tcase_("MXMTG: Freivald's Algorithm", (ftnlen)27);

/*     Compute the product MOUT = MNRN * Transpose(MNCN). */

    mxmtg_(mnrn, mncn, &c__7, &c__7, &c__6, mout);

/*     Check MNRN * ( Transpose(MNCN) * VCOL ) = MOUT * VCOL, */
/*     where VCOL is an arbitrary vector. */

    mtxvg_(mncn, vcol, &c__7, &c__6, vout);
    mxvg_(mnrn, vout, &c__7, &c__7, vtemp);
    mxvg_(mout, vcol, &c__7, &c__6, vout);
    chckad_("VOUT", vout, "~", vtemp, &c__7, &c_b98, ok, (ftnlen)4, (ftnlen)1)
	    ;
    t_success__(ok);

/*     Tests for zero/negative NR/NC inputs. */

/*     THERE CAN BE NO SUCH TESTS BECAUSE OF THE WAY INPUT AND */
/*     OUTPUT MATRICES ARE DECLARED IN THESE ROUTINES. */

    return 0;
} /* f_matrixg__ */

