/* f_npsgpt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 1.;
static doublereal c_b5 = 0.;
static doublereal c_b7 = 2.;
static doublereal c_b10 = -1.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b61 = 3.;
static doublereal c_b71 = 10.;
static doublereal c_b74 = -7.;
static doublereal c_b75 = 5.;
static doublereal c_b76 = -13.;
static doublereal c_b77 = -.5;
static doublereal c_b111 = .5;

/* $Procedure F_NPSGPT ( NPSGPT tests ) */
/* Subroutine */ int f_npsgpt__(logical *ok)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal dist, udir[3], perp[3];
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static integer i__;
    static doublereal p[3], scale, x[3], y[3], z__[3];
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), tcase_(char *, ftnlen), vpack_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal pnear[3], small;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), vlcom_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static char title[160];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    static doublereal xdist;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *), vlcom3_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    ;
    static doublereal dscale;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    static integer nscale;
    static doublereal minscl, maxscl, xpnear[3];
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *);
    static doublereal ep1[3], ep2[3];
    extern /* Subroutine */ int npsgpt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal dir[3], tol;

/* $ Abstract */

/*     Exercise the SPICELIB routine NPSGPT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine NPSGPT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 18-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_NPSGPT", (ftnlen)8);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment lies on X-axis between X = 1 and X = 2. Point has negati"
	    "ve X.", (ftnlen)69);
    vpack_(&c_b4, &c_b5, &c_b5, ep1);
    vpack_(&c_b7, &c_b5, &c_b5, ep2);
    vpack_(&c_b10, &c_b5, &c_b5, p);
    vequ_(ep1, xpnear);
    xdist = 2.;
    npsgpt_(ep1, ep2, p, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment lies on X-axis between X = 1 and X = 2. Point has X = 1.",
	     (ftnlen)64);
    vpack_(&c_b4, &c_b5, &c_b5, ep1);
    vpack_(&c_b7, &c_b5, &c_b5, ep2);
    vpack_(&c_b4, &c_b5, &c_b5, p);
    vequ_(ep1, xpnear);
    xdist = 0.;
    npsgpt_(ep1, ep2, p, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment lies on X-axis between X = 1 and X = 2. Point has X = 2.",
	     (ftnlen)64);
    vpack_(&c_b4, &c_b5, &c_b5, ep1);
    vpack_(&c_b7, &c_b5, &c_b5, ep2);
    vpack_(&c_b7, &c_b5, &c_b5, p);
    vequ_(ep2, xpnear);
    xdist = 0.;
    npsgpt_(ep1, ep2, p, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment lies on X-axis between X = 1 and X = 2. Point has X > 2.",
	     (ftnlen)64);
    vpack_(&c_b4, &c_b5, &c_b5, ep1);
    vpack_(&c_b7, &c_b5, &c_b5, ep2);
    vpack_(&c_b61, &c_b5, &c_b5, p);
    vequ_(ep2, xpnear);
    xdist = 1.;
    npsgpt_(ep1, ep2, p, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Now try some more general cases. */

    minscl = -100.;
    maxscl = 100.;
    nscale = 30;
    dscale = (maxscl - minscl) / nscale;
    i__1 = nscale;
    for (i__ = 0; i__ <= i__1; ++i__) {

/*        Generate endpoints of the segment. */

	d__1 = minscl + i__ * dscale;
	scale = pow_dd(&c_b71, &d__1);
	vpack_(&c_b4, &c_b61, &c_b74, ep1);
	vpack_(&c_b75, &c_b76, &c_b77, ep2);
	vsclip_(&scale, ep1);
	vsclip_(&scale, ep2);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Point projection is off EP1 end; I = #.", (ftnlen)160, 
		(ftnlen)39);
	repmi_(title, "#", &i__, title, (ftnlen)160, (ftnlen)1, (ftnlen)160);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)160);

/*        Let DIR be the vector from EP1 to EP2. */

	vsub_(ep2, ep1, dir);
	vhat_(dir, udir);
	vequ_(udir, x);

/*        Get some vectors orthogonal to DIR; generate */
/*        a perpendicular component vector for P. */

	frame_(x, y, z__);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = scale * 2.;
	d__2 = scale * 5.;
	vlcom_(&d__1, y, &d__2, z__, perp);

/*        Generate an input point. */

	small = 1e-13;
	d__1 = -small;
	vlcom3_(&c_b4, ep1, &d__1, dir, &c_b4, perp, p);
	tol = 1e-14;
	vequ_(ep1, xpnear);
	xdist = vdist_(p, ep1);
	npsgpt_(ep1, ep2, p, pnear, &dist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
		ftnlen)3);
	chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Point projection is EP1; I = #.", (ftnlen)160, (ftnlen)
		31);
	repmi_(title, "#", &i__, title, (ftnlen)160, (ftnlen)1, (ftnlen)160);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)160);

/*        Generate an input point. */

	vlcom3_(&c_b4, ep1, &c_b5, dir, &c_b4, perp, p);
	tol = 1e-14;
	vequ_(ep1, xpnear);
	xdist = vdist_(p, ep1);
	npsgpt_(ep1, ep2, p, pnear, &dist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
		ftnlen)3);
	chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Point projection is between EP1 and EP2; I = #.", (
		ftnlen)160, (ftnlen)47);
	repmi_(title, "#", &i__, title, (ftnlen)160, (ftnlen)1, (ftnlen)160);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)160);

/*        Generate an input point. */

	vlcom3_(&c_b111, ep1, &c_b111, ep2, &c_b4, perp, p);
	tol = 1e-14;
	vlcom_(&c_b111, ep1, &c_b111, ep2, xpnear);
	xdist = vnorm_(perp);
	npsgpt_(ep1, ep2, p, pnear, &dist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
		ftnlen)3);
	chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Point projection is EP2; I = #.", (ftnlen)160, (ftnlen)
		31);
	repmi_(title, "#", &i__, title, (ftnlen)160, (ftnlen)1, (ftnlen)160);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)160);

/*        Generate an input point. */

	vlcom3_(&c_b4, ep2, &c_b5, dir, &c_b4, perp, p);
	tol = 1e-14;
	vequ_(ep2, xpnear);
	xdist = vdist_(p, ep2);
	npsgpt_(ep1, ep2, p, pnear, &dist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
		ftnlen)3);
	chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Point projection is off EP2 end; I = #.", (ftnlen)160, 
		(ftnlen)39);
	repmi_(title, "#", &i__, title, (ftnlen)160, (ftnlen)1, (ftnlen)160);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)160);

/*        Generate an input point. */

	small = 1e-13;
	vlcom3_(&c_b4, ep2, &small, dir, &c_b4, perp, p);
	tol = 1e-14;
	vequ_(ep2, xpnear);
	xdist = vdist_(p, ep2);
	npsgpt_(ep1, ep2, p, pnear, &dist);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
		ftnlen)3);
	chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    }
/* ********************************************************************* */
/* * */
/* *    Non-error exceptional case */
/* * */
/* ********************************************************************* */
    tcase_("EP1 == EP2", (ftnlen)10);
    vlcom_(&c_b4, ep2, &c_b4, dir, p);
    npsgpt_(ep1, ep1, p, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(ep1, xpnear);
    xdist = vdist_(p, ep1);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     None. */


/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_npsgpt__ */

