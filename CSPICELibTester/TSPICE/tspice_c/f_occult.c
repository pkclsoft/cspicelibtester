/* f_occult.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__3 = 3;
static doublereal c_b269 = 250.;
static doublereal c_b295 = 62.;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c__10 = 10;
static doublereal c_b447 = 0.;
static integer c_n2 = -2;
static integer c_n3 = -3;

/* $Procedure      F_OCCULT ( OCCULT family tests ) */
/* Subroutine */ int f_occult__(logical *ok)
{
    /* Initialized data */

    static char times[20*5] = "2011-JAN-02 19:00:00" "2011-JAN-02 21:00:00" 
	    "2011-JAN-03 00:00:00" "2011-JAN-03 09:00:00" "2011-JAN-03 11:00"
	    ":00";
    static integer result[5] = { 0,-1,-3,1,2 };

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal arad[3];
    integer nlat;
    doublereal last;
    integer nlon;
    char targ1[20], targ2[20];
    doublereal c__;
    integer i__, j, n;
    char frame[20];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    extern doublereal jyear_(void);
    logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal first;
    extern /* Subroutine */ int t_success__(logical *);
    char frame1[20], frame2[20], shape1[20], shape2[20];
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    doublereal et;
    char aframe[20], bframe[20];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int bodvcd_(integer *, char *, integer *, integer 
	    *, doublereal *, ftnlen), chckxc_(logical *, char *, logical *, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen);
    integer mltfac;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    char abcorr[20];
    doublereal pmcoef[3];
    integer bodyid, ocltid;
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen);
    logical usepad;
    extern /* Subroutine */ int unload_(char *, ftnlen), gdpool_(char *, 
	    integer *, integer *, integer *, doublereal *, logical *, ftnlen),
	     natdsk_(char *, char *, integer *, integer *, char *, integer *, 
	    integer *, ftnlen, ftnlen, ftnlen);
    doublereal corpar[10], bounds[4]	/* was [2][2] */;
    integer surfid;
    logical makvtl;
    extern /* Subroutine */ int natspk_(char *, logical *, integer *, ftnlen),
	     occult_(char *, char *, char *, char *, char *, char *, char *, 
	    char *, doublereal *, integer *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), dvpool_(char *, ftnlen), pdpool_(
	    char *, integer *, doublereal *, ftnlen), spkuef_(integer *), 
	    tstpck_(char *, logical *, logical *, ftnlen), furnsh_(char *, 
	    ftnlen);
    char obsrvr[20];
    extern logical exists_(char *, ftnlen);
    integer corsys;
    extern /* Subroutine */ int tstlsk_(void), tstspk_(char *, logical *, 
	    integer *, ftnlen), spkpos_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    extern doublereal rpd_(void);
    doublereal pos[3];
    integer han1, han2;
    extern /* Subroutine */ int t_secds2__(integer *, integer *, char *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    logical *, logical *, char *, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        OCCULT */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB occultation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     ELLIPSOID */
/*     GEOMETRY */
/*     OCCULTATION */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     S.C. Krening      (JPL) */
/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 24-JAN-2012 (SCK) (NJB) */

/* -& */

/*     The following integer codes indicate the geometric relationship */
/*     of the three bodies. */

/*     The meaning of the sign of each code is given below. */

/*                    Code sign          Meaning */
/*                    ---------          ------------------------------ */
/*                       > 0             The second ellipsoid is */
/*                                       partially or fully occulted */
/*                                       by the first. */

/*                       < 0             The first ellipsoid is */
/*                                       partially of fully */
/*                                       occulted by the second. */

/*                       = 0             No occultation. */

/*     The meanings of the codes are given below. The variable names */
/*     indicate the type of occultation and which target is in the back. */
/*     For example, TOTAL1 represents a total occultation in which the */
/*     first target is in the back (or occulted by) the second target. */

/*                    Name      Code     Meaning */
/*                    ------    -----    ------------------------------ */
/*                    TOTAL1     -3      Total occultation of first */
/*                                       target by second. */

/*                    ANNLR1     -2      Annular occultation of first */
/*                                       target by second.  The second */
/*                                       target does not block the limb */
/*                                       of the first. */

/*                    PARTL1     -1      Partial occultation of first */
/*                                       target by second target. */

/*                    NOOCC       0      No occultation or transit:  both */
/*                                       objects are completely visible */
/*                                       to the observer. */

/*                    PARTL2      1      Partial occultation of second */
/*                                       target by first target. */

/*                    ANNLR2      2      Annular occultation of second */
/*                                       target by first. */

/*                    TOTAL2      3      Total occultation of second */
/*                                       target by first. */


/*     End include file occult.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine OCCULT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     S.C. Krening     (JPL) */
/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 23-MAY-2016 (SCK) (NJB) */

/*        Updated to test support for DSK-based target */
/*        shape models. */

/* -    TSPICE Version 1.0.0, 17-JAN-2012 (SCK) (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save variables */


/*     Initialize */


/*     Begin every test family with an open call. */

    topen_("F_OCCULT", (ftnlen)8);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("generic.bsp", &c_true, &han2, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000-JAN-01 00:00:00 (TDB)", &et, (ftnlen)26);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid first shape", (ftnlen)19);
    occult_("ALPHA", "PNT", "ALPHAFIXED", "BETA", "POINT", "BETAFIXED", "NONE"
	    , "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)3, (ftnlen)10, (ftnlen)
	    4, (ftnlen)5, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/*     Missing "unprioritized" keyword: */

    occult_("ALPHA", "DSK", "ALPHAFIXED", "BETA", "POINT", "BETAFIXED", "NONE"
	    , "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)3, (ftnlen)10, (ftnlen)
	    4, (ftnlen)5, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid second shape", (ftnlen)20);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "PNT", "BETAFIXED", "NONE"
	    , "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)
	    4, (ftnlen)3, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/*     Missing "unprioritized" keyword: */

    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "DSK", "BETAFIXED", "NONE"
	    , "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)
	    4, (ftnlen)3, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify both targets cannot be points", (ftnlen)36);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "POINT", "BETAFIXED", 
	    "NONE", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)5, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDSHAPECOMBO)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify both targets cannot be identical", (ftnlen)39);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "ALPHA", "ELLIPSOID", "BETAFIXED",
	     "NONE", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)5, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify the first target and observer cannot be identical.", (
	    ftnlen)57);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "NONE", "ALPHA", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify the second target and observer cannot be identical.", (
	    ftnlen)58);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "NONE", "BETA", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

/*     Note:  This is a test for three cases: */
/*     1)  If an unrecognized target is elliptical and the frame */
/*         cannot be found. */
/*     2)  If an unrecognized target is a point and the ID code is */
/*         not found in the call tree of ZZGFOCIN. */
/*     3)  If an observer is unrecognized. */

    tcase_("Verify an error if the targets or observer are unrecognized", (
	    ftnlen)59);
    occult_("ALPH", "ellipsoid", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "NONE", "SUN", &et, &ocltid, (ftnlen)4, (ftnlen)9, (ftnlen)
	    10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BTA", "ELLIPSOID", "BETAFIXED", 
	    "NONE", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)3, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "NONE", "SUNN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)4);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);


/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify aberration correction inputs", (ftnlen)35);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "S", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)1, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "XS", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "RLT", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "XRLT", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "z", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)1, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);


/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for observer", (ftnlen)24);
    occult_("ALPHA", "POINT", "ALPHAFIXED", "BETA", "ELLIPSOID", "BETAFIXED", 
	    "LT", "GASPRA", &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for first target", (ftnlen)28);
    occult_("GASPRA", "POINT", "IAU_GASPRA", "BETA", "ELLIPSOID", "BETAFIXED",
	     "LT", "SUN", &et, &ocltid, (ftnlen)6, (ftnlen)5, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for second target", (ftnlen)29);
    occult_("ALPHA", "ELLIPSOID", "ALPHAFIXED", "GASPRA", "POINT", "IAU_GASP"
	    "RA", "LT", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)9, (ftnlen)10, 
	    (ftnlen)6, (ftnlen)5, (ftnlen)10, (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for first target; both targets are ellip"
	    "soids.", (ftnlen)70);
    occult_("EARTH", "ellipsoid", "ITRF93", "ALPHA", "ELLIPSOID", "ALPHAFIXED"
	    , "LT", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)9, (ftnlen)6, (
	    ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for second target; both targets are elli"
	    "psoids.", (ftnlen)71);
    occult_("ALPHA", "ELLIPSOID", "ALPHAFIXED", "EARTH", "ellipsoid", "ITRF93"
	    , "LT", "SUN", &et, &ocltid, (ftnlen)5, (ftnlen)9, (ftnlen)10, (
	    ftnlen)5, (ftnlen)9, (ftnlen)6, (ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for front target; front target is ellips"
	    "oid; back is point.", (ftnlen)83);

/*     Capture Beta's prime meridian data; delete it from */
/*     the kernel pool. */

    gdpool_("BODY2000_PM", &c__1, &c__3, &n, pmcoef, &found, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dvpool_("BODY2000_PM", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("BETA", "ELLIPSOID", "BETAFIXED", "ALPHA", "point", " ", "NONE", 
	    "SUN", &c_b269, &ocltid, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)
	    5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Restore the PM data. */

    pdpool_("BODY2000_PM", &c__3, pmcoef, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for back target; front target is point; "
	    "back is ellipsoid.", (ftnlen)82);

/*     Because the frame ALPHAFIXED is a TK frame defined */
/*     relative to the PCK frame BETAAFIXED, we can make */
/*     the SINCPT call, which accesses the ALPHAFIXED frame, */
/*     fail by damaging the data for the for BETAFIXED frame. */

/*     This scheme depends on the Nat's solar system PCK */
/*     implementation. */

/*     Capture Beta's prime meridian data; delete it from */
/*     the kernel pool. */

    gdpool_("BODY2000_PM", &c__1, &c__3, &n, pmcoef, &found, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dvpool_("BODY2000_PM", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("BETA", "point", " ", "ALPHA", "ellipsoid", "alphafixed", "NONE", 
	    "SUN", &c_b295, &ocltid, (ftnlen)4, (ftnlen)5, (ftnlen)1, (ftnlen)
	    5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Restore the PM data. */

    pdpool_("BODY2000_PM", &c__3, pmcoef, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     When TARG1 is GAMMA (abcorr = none): */

/* 2000-JAN-02 19 GAMMA  not occulted by        ALPHA  as seen by SUN   0 */
/* 2000-JAN-02 21 GAMMA  partially occulted by  ALPHA  as seen by SUN  -1 */
/* 2000-JAN-03 00 GAMMA  totally occulted by    ALPHA  as seen by SUN  -3 */
/* 2000-JAN-03 07 GAMMA  not occulted by        ALPHA  as seen by SUN   0 */
/* 2000-JAN-03 09 ALPHA  partially occulted by  GAMMA  as seen by SUN   1 */
/* 2000-JAN-03 11 ALPHA  transited by           GAMMA  as seen by SUN   2 */
/* 2000-JAN-03 16 GAMMA  not occulted by        ALPHA  as seen by SUN   0 */

/*     When TARG1 is ALPHA (abcorr = none): */

/* 2000-JAN-02 19 ALPHA not occulted by         GAMMA  as seen by SUN   0 */
/* 2000-JAN-02 21 GAMMA partially occulted by   ALPHA  as seen by SUN   1 */
/* 2000-JAN-03 00 GAMMA totally occulted by     ALPHA  as seen by SUN   3 */
/* 2000-JAN-03 07 ALPHA not occulted by         GAMMA  as seen by SUN   0 */
/* 2000-JAN-03 09 ALPHA partially occulted by   GAMMA  as seen by SUN  -1 */
/* 2000-JAN-03 11 ALPHA transited by            GAMMA  as seen by SUN  -2 */
/* 2000-JAN-03 16 ALPHA not occulted by         GAMMA  as seen by SUN   0 */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (none), */
/*                  occultation type (any) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 20:41:15 */
/*                     Stop time:  2000-JAN-03 03:51:58 */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (none), */
/*                  occultation type (partial) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 20:41:15 */
/*                     Stop time:  2000-JAN-02 22:52:53 */
/*                  Interval            2 */
/*                     Start time: 2000-JAN-03 02:28:28 */
/*                     Stop time:  2000-JAN-03 03:51:58 */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (none), */
/*                  occultation type (annular) */

/*                  No occultation was found. */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (none), */
/*                  occultation type (full, total) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 22:52:53 */
/*                     Stop time:  2000-JAN-03 02:28:28 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (none), */
/*                  occultation type (any) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 08:24:46 */
/*                     Stop time:  2000-JAN-03 14:57:23 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (none), */
/*                  occultation type (partial) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 08:24:46 */
/*                     Stop time:  2000-JAN-03 09:58:08 */
/*                  Interval            2 */
/*                     Start time: 2000-JAN-03 12:34:35 */
/*                     Stop time:  2000-JAN-03 14:57:23 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (none), */
/*                  occultation type (annular) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 09:58:08 */
/*                     Stop time:  2000-JAN-03 12:34:35 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (none), */
/*                  occultation type (full, total) */

/*                  No occultation was found. */

/*           -   -   -   -   -   -   -   -   -   -   -   - */
/*     From GFOCLT, Front (gamma), back (alpha), abcorr (xcn), */
/*                  occultation type (any) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 08:24:39 */
/*                     Stop time:  2000-JAN-03 14:57:17 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (xcn), */
/*                  occultation type (partial) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 08:24:39 */
/*                     Stop time:  2000-JAN-03 09:57:59 */
/*                  Interval            2 */
/*                     Start time: 2000-JAN-03 12:34:32 */
/*                     Stop time:  2000-JAN-03 14:57:17 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (xcn), */
/*                  occultation type (annular transit) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-03 09:57:59 */
/*                     Stop time:  2000-JAN-03 12:34:32 */

/*     From GFOCLT, Front (gamma), back (alpha), abcorr (xcn), */
/*                  occultation type (full) */

/*                  No occultation was found. */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (xcn), */
/*                  occultation type (any) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 20:41:09 */
/*                     Stop time:  2000-JAN-03 03:51:51 */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (xcn), */
/*                  occultation type (partial) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 20:41:09 */
/*                     Stop time:  2000-JAN-02 22:52:49 */
/*                  Interval            2 */
/*                     Start time: 2000-JAN-03 02:28:19 */
/*                     Stop time:  2000-JAN-03 03:51:51 */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (xcn), */
/*                  occultation type (annular) */

/*                  No occultation was found. */

/*     From GFOCLT, Front (alpha), back (gamma), abcorr (xcn), */
/*                  occultation type (full) */

/*                  Interval            1 */
/*                     Start time: 2000-JAN-02 22:52:49 */
/*                     Stop time:  2000-JAN-03 02:28:19 */


/* ---- Case ------------------------------------------------------------- */

    tcase_("Verify the occultation types for specific times are correct.", (
	    ftnlen)60);

/*     In order to test all occultation ID codes, first make GAMMA the */
/*     first target, and then make ALPHA the first target.  The same */
/*     configuration will have an occultation ID of -1*result if the */
/*     target 1 and target 2 bodies are reversed.  This is why MLTFAC */
/*     exists. */

    for (i__ = 1; i__ <= 2; ++i__) {
	if (i__ == 1) {
	    s_copy(targ1, "GAMMA", (ftnlen)20, (ftnlen)5);
	    s_copy(shape1, "ELLIPSOID", (ftnlen)20, (ftnlen)9);
	    s_copy(frame1, "GAMMAFIXED", (ftnlen)20, (ftnlen)10);
	    s_copy(targ2, "ALPHA", (ftnlen)20, (ftnlen)5);
	    s_copy(shape2, "ELLIPSOID", (ftnlen)20, (ftnlen)9);
	    s_copy(frame2, "ALPHAFIXED", (ftnlen)20, (ftnlen)10);
	    s_copy(obsrvr, "SUN", (ftnlen)20, (ftnlen)3);
	    s_copy(abcorr, "NONE", (ftnlen)20, (ftnlen)4);
	    mltfac = 1;
	} else {
	    s_copy(targ1, "ALPHA", (ftnlen)20, (ftnlen)5);
	    s_copy(frame1, "ALPHAFIXED", (ftnlen)20, (ftnlen)10);
	    s_copy(targ2, "GAMMA", (ftnlen)20, (ftnlen)5);
	    s_copy(frame2, "GAMMAFIXED", (ftnlen)20, (ftnlen)10);
	    mltfac = -1;
	}

/*        For each test, convert the time to ET, call OCCULT, and verify */
/*        the OCLTID matches with the desired result. */

	for (j = 1; j <= 5; ++j) {
	    str2et_(times + ((i__1 = j - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		    "times", i__1, "f_occult__", (ftnlen)707)) * 20, &et, (
		    ftnlen)20);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    occult_(targ1, shape1, frame1, targ2, shape2, frame2, abcorr, 
		    obsrvr, &et, &ocltid, (ftnlen)20, (ftnlen)20, (ftnlen)20, 
		    (ftnlen)20, (ftnlen)20, (ftnlen)20, (ftnlen)20, (ftnlen)
		    20);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    i__2 = mltfac * result[(i__1 = j - 1) < 5 && 0 <= i__1 ? i__1 : 
		    s_rnge("result", i__1, "f_occult__", (ftnlen)716)];
	    chcksi_("OCLTID", &ocltid, "=", &i__2, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	    i__2 = mltfac * result[(i__1 = j - 1) < 5 && 0 <= i__1 ? i__1 : 
		    s_rnge("result", i__1, "f_occult__", (ftnlen)718)];
	    chcksi_("OCLTID", &ocltid, "=", &i__2, &c__0, ok, (ftnlen)6, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

/*     At TIMES(3) with Gamma as the first target (point), the */
/*     occultation ID should be -3, representing that Gamma is completely */
/*     occulted by Alpha. */

    tcase_("Point case:  Point totally occulted by ellipsoid", (ftnlen)48);
    str2et_(times + 40, &et, (ftnlen)20);
    occult_("gamma", "point", " ", "alpha", "ellipsoid", "ALPHAFIXED", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)5,
	     (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &result[2], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    occult_("alpha", "ellipsoid", "ALPHAFIXED", "gamma", "point", " ", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)
	    5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = -result[2];
    chcksi_("OCLTID", &ocltid, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

/*     At TIMES(5) with Gamma as the first target (point), the */
/*     occultation ID should be 2 (alpha transited by gamma). */

    tcase_("Point case: Point transiting ellipsoid", (ftnlen)38);
    str2et_(times + 80, &et, (ftnlen)20);
    occult_("GAMMA", "POINT", " ", "ALPHA", "ELLIPSOID", "ALPHAFIXED", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)5,
	     (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &result[4], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);
    occult_("ALPHA", "ELLIPSOID", "ALPHAFIXED", "GAMMA", "POINT", " ", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)
	    5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = -result[4];
    chcksi_("OCLTID", &ocltid, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

/*     At the time below, if the abcorr of 'none' is used, a partial */
/*     occultation is reported.  If 'xcn' is used, an annular transit */
/*     is reported.  The time and occultation types were calculated */
/*     using GFOCLT. */

    tcase_("Verify results with different abcorr", (ftnlen)36);
    str2et_("2000-JAN-03 09:58:02 (TDB)", &et, (ftnlen)26);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Calculate the occultation type with no aberration correction. */
/*     Check that the result is 'partial'. */

    occult_("GAMMA", "ELLIPSOID", "GAMMAFIXED", "ALPHA", "ELLIPSOID", "ALPHA"
	    "FIXED", "NONE", obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)9, (
	    ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)
	    20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Calculate the occultation type with the aberration correction set */
/*     to 'xcn'.  Check that the result is 'annular'. */

    occult_("GAMMA", "ELLIPSOID", "GAMMAFIXED", "ALPHA", "ELLIPSOID", "ALPHA"
	    "FIXED", "XCN", obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)9, (
	    ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)
	    20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);
/* *********************************************************************** */

/*     DSK tests */

/* *********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Create a DSK containing shape models for bodies Alpha and Beta.", 
	    (ftnlen)63);

/*     Make sure the generic SPK is unloaded! We need the sun ephemeris */
/*     from Nat's SPK. */

    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/*     This block is suitable for use in F_GFOCLT. We don't actually */
/*     have to have the high-resolution DSK patches to test OCCULT. */

/*     We'll enhance the generic DSK models for Alpha and Beta by */
/*     appending segments containing small patches of high-resolution */
/*     data for the surface regions that participate in computation of */
/*     accurate occultation ingress and egress times. We use small */
/*     patches because the size and run time needed to create full */
/*     models at this resolution would be prohibitive. */

/*     Start by creating the basic DSK. */

    if (exists_("occult_nat.bds", (ftnlen)14)) {
	unload_("occult_nat.bds", (ftnlen)14);
	delfil_("occult_nat.bds", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Use low resolution tessellations for the main models. */

    nlon = 20;
    nlat = 10;
    s_copy(aframe, "ALPHAFIXED", (ftnlen)20, (ftnlen)10);
    s_copy(bframe, "BETAFIXED", (ftnlen)20, (ftnlen)9);
    natdsk_("occult_nat.bds", aframe, &nlon, &nlat, bframe, &nlon, &nlat, (
	    ftnlen)14, (ftnlen)20, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the -Y patch for body Alpha. The patch covers */
/*     the lon/lat rectangle */

/*        -92 deg. <= lon <= -88 deg. */
/*          0 deg. <= lat <=   4 deg. */

/*     Note that Alpha' body-fixed Z axis lies in Alpha's orbital */
/*     plane. */

    s_copy(frame, aframe, (ftnlen)20, (ftnlen)20);
    bodyid = 1000;
    surfid = 2;
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, arad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make the patch spherical, using the ellipsoid's Z */
/*     semi-axis length. */

    c__ = arad[2];
    corsys = 1;
    cleard_(&c__10, corpar);
    makvtl = FALSE_;
    bounds[0] = rpd_() * -92.;
    bounds[1] = rpd_() * -88.;
    bounds[2] = rpd_() * 0.;
    bounds[3] = rpd_() * 4.;
    nlat = 200;
    nlon = 200;
    usepad = TRUE_;

/*     Append the patch segment to the existing DSK. */

    t_secds2__(&bodyid, &surfid, frame, &first, &last, &corsys, corpar, 
	    bounds, &c__, &c__, &c__, &nlon, &nlat, &makvtl, &usepad, "occul"
	    "t_nat.bds", (ftnlen)20, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the +Y patch for body Alpha. The patch covers */
/*     the lon/lat rectangle */

/*         88 deg. <= lon <=  92 deg. */
/*          0 deg. <= lat <=   4 deg. */

    surfid = 3;
    bounds[0] = rpd_() * 88.;
    bounds[1] = rpd_() * 92.;
    bounds[2] = rpd_() * 0.;
    bounds[3] = rpd_() * 4.;
    nlat = 200;
    nlon = 200;
    usepad = TRUE_;

/*     Append the patch segment to the existing DSK. */

    t_secds2__(&bodyid, &surfid, frame, &first, &last, &corsys, corpar, 
	    bounds, &c__, &c__, &c__, &nlon, &nlat, &makvtl, &usepad, "occul"
	    "t_nat.bds", (ftnlen)20, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for non-occultation just prior to transit start, for point"
	    " BETA and DSK ALPHA. BETA is body 1.", (ftnlen)100);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("occult_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_("ALPHA", &c_b447, "J2000", "NONE", "sun", pos, &lt, (ftnlen)5, (
	    ftnlen)5, (ftnlen)4, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use a time 2 microseconds before the nominal start of occultation. */

    str2et_("2000 JAN 1 12:00:59.999998 TDB", &et, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("BETA", "POINT", " ", "ALPHA", "DSK/UNPRIORITIZED", frame, "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)4, (ftnlen)5, (ftnlen)1, (ftnlen)5,
	     (ftnlen)17, (ftnlen)20, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for non-occultation just prior to transit start, for point"
	    " BETA and DSK ALPHA. BETA is body 2.", (ftnlen)100);
    occult_("ALPHA", "DSK/UNPRIORITIZED", frame, "BETA", "POINT", " ", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)17, (ftnlen)20, (ftnlen)
	    4, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for annular transit just after transit start, for point BE"
	    "TA and DSK ALPHA. BETA is body 1.", (ftnlen)97);

/*     Use a time 2 microseconds after the nominal start of occultation. */

    str2et_("2000 JAN 1 12:01:00.000002 TDB", &et, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("BETA", "POINT", " ", "ALPHA", "DSK/UNPRIORITIZED", frame, "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)4, (ftnlen)5, (ftnlen)1, (ftnlen)5,
	     (ftnlen)17, (ftnlen)20, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for annular transit just after transit start, for point BE"
	    "TA and DSK ALPHA. BETA is body 2.", (ftnlen)97);
    occult_("ALPHA", "DSK/UNPRIORITIZED", frame, "BETA", "POINT", " ", "NONE",
	     obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)17, (ftnlen)20, (ftnlen)
	    4, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c_n2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     In the tests below, BETA is modeled by a DSK and ALPHA is */
/*     treated as a point. */


/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for full occultation at the midpoint of the occultation in"
	    "terval, for DSK BETA and point ALPHA. BETA is body 1.", (ftnlen)
	    117);
    str2et_("2000 JAN 1 12:05:00 TDB", &et, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("BETA", "DSK/UNPRIORITIZED", bframe, "ALPHA", "POINT", " ", "NONE"
	    , obsrvr, &et, &ocltid, (ftnlen)4, (ftnlen)17, (ftnlen)20, (
	    ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check for full occultation at the midpoint of the occultation in"
	    "terval, for DSK BETA and point ALPHA. BETA is body 2.", (ftnlen)
	    117);
    str2et_("2000 JAN 1 12:05:00 TDB", &et, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    occult_("ALPHA", "POINT", " ", "BETA", "DSK/UNPRIORITIZED", bframe, "NONE"
	    , obsrvr, &et, &ocltid, (ftnlen)5, (ftnlen)5, (ftnlen)1, (ftnlen)
	    4, (ftnlen)17, (ftnlen)20, (ftnlen)4, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("OCLTID", &ocltid, "=", &c_n3, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK and DSK files. */

    spkuef_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("occult_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("occult_nat.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_occult__ */

