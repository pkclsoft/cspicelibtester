/*

-Procedure f_occult_c ( Test occult_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper occult_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_occult_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for occult_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   S.C. Krening    (JPL)
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 25-JAN-2012 (SCK) (NJB) 

-Index_Entries

   test occult_c

-&
*/

{ /* Begin f_occult_c */


   /*
   Prototypes 
   */
   
   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );
   /*
   Constants
   */
   #define PCK             "nat.tpc"   
   #define SPK             "nat.bsp"   
   
   /*
   Local variables
   */
   
   integer                 handle;

   static logical          keeppc  = SPICEFALSE;
   static logical          loadpc  = SPICETRUE;
   static logical          loadspk = SPICETRUE;
   
   SpiceDouble             et;
   
   SpiceInt                occult_code;
   
   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_occult_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Create and load a PCK file. Delete the file afterward.
   */
   natpck_ ( ( char      * ) PCK,
             ( logical   * ) &loadpc, 
             ( logical   * ) &keeppc, 
             ( ftnlen      ) strlen(PCK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Load an SPK file as well.
   */
   natspk_ ( ( char      * ) SPK,
             ( logical   * ) &loadspk, 
             ( integer   * ) &handle, 
             ( ftnlen      ) strlen(SPK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   str2et_c ( "2000 jan 1 TDB", &et );
   chckxc_c ( SPICEFALSE, " ", ok   );
   
   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Bad input string pointers" );

   occult_c ( NULLCPTR,  "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",     NULLCPTR,    "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  NULLCPTR, 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              NULLCPTR,  "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",      NULLCPTR,    "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",   NULLCPTR, 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              NULLCPTR,  "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      NULLCPTR, et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Empty input strings" );
   
   occult_c ( "",        "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",     "",          "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "",        "ellipsoid",  "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",      "",          "iau_sun", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",   "", 
              "lt",      "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "",        "earth", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   occult_c ( "moon",    "ellipsoid",  "iau_moon", 
              "sun",     "ellipsoid",  "iau_sun", 
              "lt",      "", et, &occult_code );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   
   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   
   str2et_c ( "2011 jan 2 21:00:00 TDB", &et );
   chckxc_c ( SPICEFALSE, " ", ok   );
   
   occult_c ( "gamma", "ellipsoid", "gammafixed", 
              "alpha", "ellipsoid", "alphafixed",
              "none",  "sun", et, &occult_code );
   chckxc_c ( SPICEFALSE, " ", ok );
   chcksi_c ( "occult_code", occult_code, "=", -1, 0, ok );
   
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK file.
   */
   spkuef_c ( (SpiceInt) handle );
   TRASH   ( SPK    );
   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_occult_c */

