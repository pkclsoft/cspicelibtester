/* f_oscltx.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b8 = 100.;
static doublereal c_b9 = 0.;
static logical c_true = TRUE_;
static doublereal c_b16 = 1e10;
static doublereal c_b39 = 1.;
static doublereal c_b40 = 1e-10;
static integer c__14 = 14;
static logical c_false = FALSE_;
static doublereal c_b70 = 1e-13;
static integer c__3 = 3;
static doublereal c_b79 = 1e-12;
static integer c__1 = 1;
static integer c__2 = 2;

/* $Procedure      F_OSCLTX ( OSCLTX routine tests ) */
/* Subroutine */ int f_oscltx__(logical *ok)
{
    /* Initialized data */

    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double sin(doublereal), cos(doublereal), sqrt(doublereal), atan2(
	    doublereal, doublereal), tan(doublereal), sinh(doublereal);

    /* Local variables */
    static integer iecc;
    static doublereal xecc[6];
    static integer iinc;
    static doublereal node[3], xinc[7], elts[20], xtau, a, d__, e, f, h__[3];
    static integer i__;
    static doublereal r__, cosea, sinea;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal coshf;
    static integer iargp;
    extern doublereal exact_(doublereal *, doublereal *, doublereal *);
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmd_(char *, char *, doublereal *, integer *, 
	    char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmax_(void);
    static doublereal perim[9]	/* was [3][3] */, perip[3], state[6], xargp[8]
	    ;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal perix[3], periy[3], xelts[20];
    extern /* Subroutine */ int vcrss_(doublereal *, doublereal *, doublereal 
	    *);
    extern doublereal vnorm_(doublereal *), twopi_(void);
    extern /* Subroutine */ int ucrss_(doublereal *, doublereal *, doublereal 
	    *), t_success__(logical *), vrotv_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal state2[6], ea;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    static doublereal et, xa;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), recrad_(
	    doublereal *, doublereal *, doublereal *, doublereal *);
    extern doublereal dacosh_(doublereal *);
    static doublereal mu, nu;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal xm;
    static integer ilnode;
    extern /* Subroutine */ int conics_(doublereal *, doublereal *, 
	    doublereal *);
    static logical succes;
    static doublereal xlnode[4], inelts[8];
    static char tstdsc[400];
    static integer im0;
    extern /* Subroutine */ int twovec_(doublereal *, integer *, doublereal *,
	     integer *, doublereal *), oscltx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal xm0[8], ecc, lat, tau, tol, xrp[1], xnu;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        OSCLTX */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file oscltx.inc */

/* $ Abstract */

/*     This file declares public parameters related to */
/*     the SPICELIB routine OSCLTX. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 25-JAN-2017 (NJB) */

/* -& */

/*     Size of the OSCLTX output argument array ELTS. */

/*     The output array ELTS is intended to contain unused space to hold */
/*     additional elements that may be added in a later version of this */
/*     routine. In order to maintain forward compatibility, user */
/*     applications should declare ELTS as follows: */

/*     DOUBLE PRECISION   ELTS( OSCXSZ ) */


/*     End of include file oscltx.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the low-level SPICELIB routine OSCLTX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 02-FEB-2017 (NJB) */

/*        Previous version 11-NOV-2014 */

/*           Based on TSPICE Version 2.0.0, 04-APR-2009 (NJB) (EDW) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     The value of CLOSE must match that used in OSCLTX. */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_OSCLTX", (ftnlen)8);

/*     Error tests: */

    for (i__ = 1; i__ <= 3; ++i__) {
	state[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("state", i__1,
		 "f_oscltx__", (ftnlen)253)] = i__ * 1e3;
    }
    for (i__ = 4; i__ <= 6; ++i__) {
	state[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("state", i__1,
		 "f_oscltx__", (ftnlen)257)] = i__ * -10.;
    }
    tcase_("Invalid GM.", (ftnlen)11);
    oscltx_(state, &c_b8, &c_b9, elts);
    chckxc_(&c_true, "SPICE(NONPOSITIVEMASS)", ok, (ftnlen)22);
    tcase_("Specific angular momentum == 0", (ftnlen)30);
    for (i__ = 4; i__ <= 6; ++i__) {
	state[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("state", i__1,
		 "f_oscltx__", (ftnlen)272)] = 0.;
    }
    oscltx_(state, &c_b8, &c_b16, elts);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Normal cases: */


/*     Set up the expected element values. */


/*     Expected argument of periapse: */

    for (iargp = 1; iargp <= 8; ++iargp) {
	xargp[(i__1 = iargp - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge("xargp", 
		i__1, "f_oscltx__", (ftnlen)290)] = (iargp - 1) * twopi_() / 
		8;
    }

/*     Expected eccentricity: */

    xecc[0] = 0.;
    xecc[1] = .5;
    xecc[2] = .999;
    xecc[3] = 1.;
    xecc[4] = 1.001;
    xecc[5] = 1.5;

/*     Expected inclination: */

    xinc[0] = 0.;
    xinc[1] = 5.0000000000000002e-11;
    xinc[2] = pi_() * .25;
    xinc[3] = pi_() * .5;
    xinc[4] = pi_() * .75;
    xinc[5] = pi_() - 5.0000000000000002e-11;
    xinc[6] = pi_();

/*     Expected longitude of the ascending node: */

    for (ilnode = 1; ilnode <= 4; ++ilnode) {
	xlnode[(i__1 = ilnode - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge("xlnode", 
		i__1, "f_oscltx__", (ftnlen)318)] = (ilnode - 1) * twopi_() / 
		4;
    }

/*     Expected mean anomaly: */

    for (im0 = 1; im0 <= 8; ++im0) {
	xm0[(i__1 = im0 - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge("xm0", i__1, 
		"f_oscltx__", (ftnlen)325)] = (im0 - 1) * twopi_() / 8;
    }

/*     Epoch: */

    et = 1e8;

/*     Expected perifocal distance: */

    xrp[0] = 262144.;

/*     GM of central body (we make this the cube of RP so we may */
/*     recover an eccentricity of zero when we have a circular orbit): */

    mu = 18014398509481984.;

/*     For a variety of element sets, we'll use CONICS to produce */
/*     the equivalent state vector.  We'll then try to recover */
/*     the elements from the state vector. */

    for (iargp = 1; iargp <= 8; ++iargp) {
	for (iecc = 1; iecc <= 6; ++iecc) {
	    for (iinc = 1; iinc <= 7; ++iinc) {
		for (ilnode = 1; ilnode <= 4; ++ilnode) {
		    for (im0 = 1; im0 <= 8; ++im0) {

/*                    Assign the input elements for this test case. */

			inelts[0] = xrp[0];
			inelts[1] = xecc[(i__1 = iecc - 1) < 6 && 0 <= i__1 ? 
				i__1 : s_rnge("xecc", i__1, "f_oscltx__", (
				ftnlen)362)];
			inelts[2] = xinc[(i__1 = iinc - 1) < 7 && 0 <= i__1 ? 
				i__1 : s_rnge("xinc", i__1, "f_oscltx__", (
				ftnlen)363)];
			inelts[3] = xlnode[(i__1 = ilnode - 1) < 4 && 0 <= 
				i__1 ? i__1 : s_rnge("xlnode", i__1, "f_oscl"
				"tx__", (ftnlen)364)];
			inelts[4] = xargp[(i__1 = iargp - 1) < 8 && 0 <= i__1 
				? i__1 : s_rnge("xargp", i__1, "f_oscltx__", (
				ftnlen)365)];
			inelts[5] = xm0[(i__1 = im0 - 1) < 8 && 0 <= i__1 ? 
				i__1 : s_rnge("xm0", i__1, "f_oscltx__", (
				ftnlen)366)];
			inelts[6] = et;
			inelts[7] = mu;

/*                    Set the expected elements. */

			for (i__ = 1; i__ <= 8; ++i__) {
			    xelts[(i__1 = i__ - 1) < 20 && 0 <= i__1 ? i__1 : 
				    s_rnge("xelts", i__1, "f_oscltx__", (
				    ftnlen)375)] = inelts[(i__2 = i__ - 1) < 
				    8 && 0 <= i__2 ? i__2 : s_rnge("inelts", 
				    i__2, "f_oscltx__", (ftnlen)375)];
			}
/*                    In some cases, these are not the input */
/*                    elements, so adjust accordingly: */

/*                    If the input inclination is close to 0 or pi, */
/*                    the output inclination will be rounded.  The */
/*                    longitude of the ascending node becomes 0. */

			if ((d__1 = inelts[2] + 0., abs(d__1)) < 1e-10) {
			    xelts[2] = 0.;
			    xelts[3] = 0.;
			} else if ((d__1 = inelts[2] - pi_(), abs(d__1)) < 
				1e-10) {
			    xelts[2] = pi_();
			    xelts[3] = 0.;
			}

/*                    If the eccentricity is "close" to 1, make it 1. */

			xelts[1] = exact_(&xelts[1], &c_b39, &c_b40);

/*                    Set up the test description for the TCASE call. */

			s_copy(tstdsc, "RP = #; ECC = #; INC = #; LNODE = #;"
				" ARGP = #; M0 = #; ET = #;, MU = #", (ftnlen)
				400, (ftnlen)70);
			for (i__ = 1; i__ <= 8; ++i__) {
			    repmd_(tstdsc, "#", &inelts[(i__1 = i__ - 1) < 8 
				    && 0 <= i__1 ? i__1 : s_rnge("inelts", 
				    i__1, "f_oscltx__", (ftnlen)409)], &c__14,
				     tstdsc, (ftnlen)400, (ftnlen)1, (ftnlen)
				    400);
			}
			tcase_(tstdsc, (ftnlen)400);

/*                    Obtain the equivalent state vector, then call */
/*                    OSCLTX. */

			conics_(inelts, &et, state);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			oscltx_(state, &et, &mu, elts);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
/*                     WRITE (*,*) 'ELTS(2) = ', ELTS(2) */
/*                     WRITE (*,*) 'ELTS(3) = ', ELTS(3) */
			succes = TRUE_;

/*                    See how well we did. */

/*                    Set the tolerance to use for RP; acknowledge */
/*                    that we can't recover RP for nearly */
/*                    parabolic orbits. */

			if (inelts[1] == 1.) {
			    tol = 1e-12;
			} else if ((d__1 = inelts[1] - 1., abs(d__1)) < 1e-10)
				 {
			    tol = .001;
			} else if ((d__1 = inelts[1] - 1., abs(d__1)) < 1e-4) 
				{
			    tol = 1e-8;
			} else {
			    tol = 1e-10;
			}
			chcksd_("RP", elts, "~/", xelts, &tol, ok, (ftnlen)2, 
				(ftnlen)2);
			succes = succes && *ok;

/*                    We use the same tolerances for eccentricity */
/*                    as for inclination. */

			chcksd_("ECC", &elts[1], "~", &xelts[1], &tol, ok, (
				ftnlen)3, (ftnlen)1);
			succes = succes && *ok;

/*                    We should always be able to recover inclination */
/*                    reasonably accurately. */

			tol = 1e-10;
			chcksd_("INC", &elts[2], "~", &xelts[2], &tol, ok, (
				ftnlen)3, (ftnlen)1);
			succes = succes && *ok;

/*                    When the inclination is not too close to 0 */
/*                    or pi, we should be able to recover it */
/*                    reasonably accurately. */

			if (sin(xelts[3]) > 1e-10) {
			    tol = 1e-10;
			} else {
			    tol = 1e-8;
			}
			if ((d__1 = elts[3] - xelts[3], abs(d__1)) <= pi_()) {
			    chcksd_("LNODE", &elts[3], "~", &xelts[3], &tol, 
				    ok, (ftnlen)5, (ftnlen)1);
			} else {
			    if (elts[3] > xelts[3]) {
				xelts[3] += twopi_();
			    } else {
				xelts[3] -= twopi_();
			    }
			    chcksd_("LNODE", &elts[3], "~", &xelts[3], &tol, 
				    ok, (ftnlen)5, (ftnlen)1);
			}
			succes = succes && *ok;

/*                    Check ARGP: */

/*                    When the eccentricity is not too close to */
/*                    zero and the inclination is not too close to */
/*                    zero or pi, we can use a reasonably tight tolerance */
/*                    for ARGP.  When the eccentricity is determined */
/*                    by OSCLTX to be zero, we know exactly what */
/*                    ARGP is supposed to be.  When the eccentricity */
/*                    is very close to but not equal to zero, ARGP */
/*                    can be almost anything.  For these cases all */
/*                    we can do is check ARGP+M0. */

/*                    Note that our path is governed by the eccentricity */
/*                    found by OSCLTX, since an input eccentricity of */
/*                    zero may not be recovered as such. */

			if (inelts[2] > 1e-10 && inelts[2] < pi_() - 1e-10) {

/*                       These are the normal inclination cases. */

			    if ((d__1 = elts[1] - 1., abs(d__1)) <= 1e-10) {
				tol = dpmax_();
			    } else if (elts[1] > 1e-5) {
				tol = 1e-12;
			    } else if (elts[1] > 1e-6) {
				tol = 1e-10;
			    } else if (elts[1] == 0.) {
				tol = 1e-12;
			    } else {
				tol = dpmax_();
			    }
			} else if (inelts[2] == 0. || inelts[2] == pi_()) {

/*                       These are exceptional, but reasonably */
/*                       well-behaved inclination cases. */

			    if ((d__1 = elts[1] - 1., abs(d__1)) <= 1e-10) {
				tol = dpmax_();
			    } else if ((d__1 = inelts[1] - 1., abs(d__1)) <= 
				    .01) {
				tol = 1e-8;
			    } else if (elts[1] > 1e-6) {
				tol = 1e-12;
			    } else if (elts[1] == 0.) {
				tol = 1e-12;
			    } else {
				tol = dpmax_();
			    }
			} else {
			    if ((d__1 = elts[1] - 1., abs(d__1)) <= 1e-10) {
				tol = dpmax_();
			    } else if (elts[1] > 1e-10 || elts[1] == 0.) {
				tol = 1e-10;
			    } else {
				tol = dpmax_();
			    }
			}

/*                    When the eccentricity is zero, the argument of */
/*                    periapse is absorbed into the mean anomaly.  This */
/*                    only happens if OSCLTX is able to determine that */
/*                    the eccentricity is zero, so test ELTS(2). */

			if (elts[1] == 0.) {
			    xelts[5] = xelts[4] + xelts[5];
			    xelts[4] = 0.;
			}

/*                    Adjust our expected value of ARGP if */
/*                    LNODE has been set to zero and if the */
/*                    eccentricity is non-zero.  The sign of */
/*                    the adjustment depends on INC. */

			if (elts[3] == 0. && inelts[3] != 0. && elts[1] != 0. 
				&& elts[2] == 0.) {
			    xelts[4] += inelts[3] - elts[3];
			} else if (elts[3] == 0. && inelts[3] != 0. && elts[1]
				 != 0. && elts[2] == pi_()) {
			    xelts[4] += elts[3] - inelts[3];
			}
			if (xelts[4] >= twopi_()) {
			    xelts[4] -= twopi_();
			} else if (xelts[4] < 0.) {
			    xelts[4] += twopi_();
			}
			if ((d__1 = elts[4] - xelts[4], abs(d__1)) <= pi_()) {
			    chcksd_("ARGP", &elts[4], "~", &xelts[4], &tol, 
				    ok, (ftnlen)4, (ftnlen)1);
			} else {
			    if (elts[4] > xelts[4]) {
				xelts[4] += twopi_();
			    } else {
				xelts[4] -= twopi_();
			    }
			    chcksd_("ARGP", &elts[4], "~", &xelts[4], &tol, 
				    ok, (ftnlen)4, (ftnlen)1);
			}
			succes = succes && *ok;

/*                    Check M0: */

			if ((d__1 = elts[1] - 1., abs(d__1)) <= 1e-10) {
			    tol = dpmax_();
			} else if ((d__1 = elts[1] - 1., abs(d__1)) <= .01) {
			    tol = .001;
			} else if (elts[1] > 1e-10) {
			    tol = 1e-12;
			} else if (elts[1] == 0.) {
			    tol = 1e-12;
			} else {
			    tol = dpmax_();
			}
			succes = succes && *ok;

/*                    If the eccentricity is zero and the inclination */
/*                    is zero or pi, the original */
/*                    longitude of the ascending node is absorbed into */
/*                    M0. */

			if (elts[1] == 0. && elts[2] == 0.) {

/*                       Eccentricity and inclination were both */
/*                       found to be zero by OSCLTX.  The input */
/*                       value of the argument of periapse is going */
/*                       to be added onto the mean anomaly. */

			    xelts[5] += inelts[3];
			} else if (elts[1] == 0. && elts[2] == pi_()) {

/*                       Eccentricity was found to be zero and */
/*                       inclination was found to be pi by OSCLTX. */
/*                       The input value of the argument of periapse */
/*                       is going to be subtracted from the mean anomaly. */

			    xelts[5] -= inelts[3];
			}
			if ((d__1 = elts[5] - xelts[5], abs(d__1)) <= pi_()) {
			    chcksd_("M0", &elts[5], "~", &xelts[5], &tol, ok, 
				    (ftnlen)2, (ftnlen)1);
			} else {
			    if (elts[5] > xelts[5]) {
				xelts[5] += twopi_();
			    } else {
				xelts[5] -= twopi_();
			    }
			    chcksd_("M0", &elts[5], "~", &xelts[5], &tol, ok, 
				    (ftnlen)2, (ftnlen)1);
			}
			succes = succes && *ok;
			chcksd_("MU", &elts[7], "=", &xelts[7], &c_b70, ok, (
				ftnlen)2, (ftnlen)1);

/*                    Now that we've obtained elements, see whether */
/*                    we can use them to obtain the state we got */
/*                    back from CONICS the first time. */

			conics_(elts, &et, state2);
			if ((d__1 = inelts[1] - 1., abs(d__1)) <= .01) {
			    tol = 1e-6;
			} else if (inelts[2] == 0. || inelts[2] >= pi_()) {
			    tol = 1e-12;
			} else if (inelts[2] <= 1e-10 || inelts[2] >= pi_() - 
				1e-10) {
			    tol = 1e-10;
			} else {
			    tol = 1e-12;
			}
			chckad_("Position", state2, "~~/", state, &c__3, &tol,
				 ok, (ftnlen)8, (ftnlen)3);
			chckad_("Velocity", &state2[3], "~~/", &state[3], &
				c__3, &tol, ok, (ftnlen)8, (ftnlen)3);

/*                    Check NU: */

			nu = elts[8];
			if (inelts[1] == 0.) {

/*                       The orbit is circular. We expect NU to */
/*                       be equal to M0. */

			    xnu = elts[5];
			    if (abs(nu) > 1.) {
				chcksd_("NU", &nu, "~/", &xnu, &c_b79, ok, (
					ftnlen)2, (ftnlen)2);
			    } else {
				chcksd_("NU", &nu, "~", &xnu, &c_b79, ok, (
					ftnlen)2, (ftnlen)1);
			    }
			} else {

/*                       This is a non-circular orbit. */

/*                       We'll use the state and elements we already */
/*                       computed to find the basis vectors of the */
/*                       perifocal frame. */


/*                       Start with the specific angular momentum vector: */

			    vcrss_(state, &state[3], h__);

/*                       The ascending node: */

			    if ((d__1 = sin(inelts[2]), abs(d__1)) < 1e-10) {

/*                          Handle the case of inclination equal to zero */
/*                          or pi. */

				vpack_(&c_b39, &c_b9, &c_b9, node);
			    } else {
				ucrss_(z__, h__, node);
			    }

/*                       Rotate the node vector about H by the argument */
/*                       of periapse to obtain the perifocal X vector: */

			    vrotv_(node, h__, &elts[4], perix);
			    ucrss_(h__, perix, periy);

/*                       Create a matrix to transform vectors from the */
/*                       base frame to the perifocal frame: */

			    twovec_(perix, &c__1, periy, &c__2, perim);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    if (*ok) {

/*                          Map the position to the perifocal frame. */
/*                          The longitude of the transformed vector is */
/*                          the expected true anomaly. */

				mxv_(perim, state, perip);
				recrad_(perip, &r__, &xnu, &lat);
				if (xnu < nu - pi_()) {
				    xnu += twopi_();
				} else if (xnu > nu + pi_()) {
				    xnu -= twopi_();
				}
			    }
			    if (abs(xnu) > .01) {
				chcksd_("NU", &nu, "~/", &xnu, &c_b40, ok, (
					ftnlen)2, (ftnlen)2);
			    } else {
				chcksd_("NU", &nu, "~", &xnu, &c_b40, ok, (
					ftnlen)2, (ftnlen)1);
			    }
			}

/*                    Check the compatibility of NU and M0: derive one of */

/*                       - the eccentric anomaly */
/*                       - the parabolic anomaly */
/*                       - the hyperbolic anomaly */

/*                    from NU, then compare to M0. */

			ecc = elts[1];
			if (ecc < 1.) {

/*                       This is the elliptical case. */

/*                       The mean anomaly can be computed as shown below. */

/*                                     e + cos(nu) */
/*                          cos(E)  = ---------------         (ellipse) */
/*                                     1 + e cos(nu) */

/*                          M0      = E - e sin(E) */


			    cosea = (ecc + cos(nu)) / (ecc * cos(nu) + 1.);

/*                       From the definition of the eccentric anomaly, */
/*                       we have */

/*                          a sin(EA) = (a/b) * r * sin(nu) */

/*                       Here b is the semi-minor axis, so */

/*                           2    2    2    2 */
/*                          a  - b  = a  ecc */


/*                       and */

/*                           2    2        2 */
/*                          b  = a (1 - ecc ) */

/*                             = [a(1 - ecc)] * a * (1 + ecc) */

/*                                 2          2 */
/*                             = [a  (1 - ecc) ] * (1 + ecc)/(1 - ecc) */

/*                                  2 */
/*                             = r_p  * (1 + ecc) / (1 - ecc) */

/*                       so */

/*                          1/b = 1/r_p  * sqrt( (1 - ecc)/(1 + ecc) ) */

/*                       Then */

/*                          sin(EA) = (1/b) * r *sin(nu) */

/*                                  = (r/r_p) * sin(nu) */
/*                                            * sqrt((1-ecc)/(1+ecc)) */


			    sinea = vnorm_(state) / elts[0] * sin(nu) * sqrt((
				    1. - ecc) / (ecc + 1.));
			    ea = atan2(sinea, cosea);
			    xm = ea - ecc * sinea;
			    if (xm < 0.) {
				xm += twopi_();
			    }
			    if (xm < elts[5] - pi_()) {
				xm += twopi_();
			    } else if (xm > elts[5] + pi_()) {
				xm -= twopi_();
			    }
			    if (abs(xm) > .01) {
				chcksd_("M", &elts[5], "~/", &xm, &c_b40, ok, 
					(ftnlen)1, (ftnlen)2);
			    } else {
				chcksd_("M", &elts[5], "~", &xm, &c_b40, ok, (
					ftnlen)1, (ftnlen)1);
			    }
			} else if (ecc == 1.) {

/*                       This is the parabolic case. */

/*                       The mean anomaly can be computed as shown */
/*                       below. */

/*                          D   = tan(nu/2) */

/*                                       3 */
/*                          M0  = D  +  D / 3 */

			    d__ = tan(nu / 2);
/* Computing 3rd power */
			    d__1 = d__;
			    xm = d__ + d__1 * (d__1 * d__1) / 3;
			    if (abs(xm) > .01) {
				chcksd_("M", &elts[5], "~/", &xm, &c_b40, ok, 
					(ftnlen)1, (ftnlen)2);
			    } else {
				chcksd_("M", &elts[5], "~", &xm, &c_b40, ok, (
					ftnlen)1, (ftnlen)1);
			    }
			} else {

/*                       ECC > 1 */


/*                       This is the hyperbolic case. */

/*                       The mean anomaly can be computed as shown */
/*                       below. */


/*                                      e + cos(nu) */
/*                          cosh(F) = ---------------         (hyperbola) */
/*                                      1 + e cos(nu) */

/*                          M       = e sinh(F) - F */

			    coshf = (ecc + cos(nu)) / (ecc * cos(nu) + 1.);

/*                       Make sure we have a valid value of COSHF. */

			    coshf = max(1.,coshf);
			    f = dacosh_(&coshf);

/*                       Note that DACOSH is a SPICELIB function, not a */
/*                       Fortran intrinsic. DACOSH can signal an error. */

			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    xm = ecc * sinh(f) - f;
			    if (nu < 0.) {
				xm = -xm;
			    }
			    if (abs(xm) > .01) {
				chcksd_("M", &elts[5], "~/", &xm, &c_b40, ok, 
					(ftnlen)1, (ftnlen)2);
			    } else {
				chcksd_("M", &elts[5], "~", &xm, &c_b40, ok, (
					ftnlen)1, (ftnlen)1);
			    }
			}

/*                    Check A: */

			a = elts[9];
			if (elts[1] < 1.) {

/*                       For elliptical orbits, A = r_p / (1-ecc): */

			    xa = elts[0] / (1. - elts[1]);
			    tol = 1e-12;
			    chcksd_("A (ecc<1)", &a, "~/", &xa, &tol, ok, (
				    ftnlen)9, (ftnlen)2);
			} else if (elts[1] == 1.) {

/*                       For parabolic orbits, A should be set to zero. */

			    xa = 0.;
			    chcksd_("A (ecc==1)", &a, "=", &xa, &tol, ok, (
				    ftnlen)10, (ftnlen)1);
			}

/*                    Check of A for all non-parabolic orbits: compute */
/*                    the energy E; then let */

/*                       E = - mu / (2A) */

/*                    and solve for A: */

/*                       A = - mu / (2E) */


/*                    To compute E, we note that E is constant */
/*                    over the orbit, so it suffices to compute */
/*                    E at periapse: */

/*                                2 */
/*                       E = ( v_p / 2 )  -  mu / r_p */

/*                    where v_p and r_p are the speed and distance */
/*                    of the orbiting object, relative to the center */
/*                    of motion, at periapse. */

/*                    We also know that the semi-latus rectum p */
/*                    is equal to both */

/*                        2             2      2 */
/*                       h / mu  =  (r_p  * v_p ) / mu */


/*                    and to */

/*                       r_p ( 1 + ecc ) */

/*                    Then */

/*                          2 */
/*                       v_p  = mu * ( 1 + ecc ) / r_p */

/*                    So */

/*                       E    = mu * ( ecc - 1 ) / ( 2 * r_p ) */


			e = mu * (elts[1] - 1.) / (elts[0] * 2.);
			if (e != 0.) {
			    xa = -mu / (e * 2);
			    tol = 1e-10;
			    chcksd_("A (ecc!=1)", &a, "~/", &xa, &tol, ok, (
				    ftnlen)10, (ftnlen)2);
			}
			succes = succes && *ok;

/*                    Check TAU: */

/*                    If we have an elliptical orbit, compute the */
/*                    expected value of TAU. Otherwise, TAU should */
/*                    be set to zero. */

			tau = elts[10];
			if (elts[1] < 1.) {

/*                       For elliptical orbits, A = r_p / (1-ecc): */

			    xa = elts[0] / (1. - elts[1]);
/* Computing 3rd power */
			    d__1 = xa;
			    xtau = twopi_() * sqrt(d__1 * (d__1 * d__1) / mu);
			    tol = 1e-12;
			    chcksd_("TAU", &tau, "~/", &xtau, &tol, ok, (
				    ftnlen)3, (ftnlen)2);
			} else {
			    tol = 0.;
			    xtau = 0.;
			    chcksd_("TAU", &tau, "=", &xtau, &tol, ok, (
				    ftnlen)3, (ftnlen)1);
			}
			succes = succes && *ok;
			if (! succes) {
/*                        WRITE (*, *) 'ELTS: ' */
/*                        WRITE (*, '(8(1X,E25.16))') ELTS */
			    succes = TRUE_;
			}
		    }
		}
	    }
	}
    }
    t_success__(ok);
    return 0;
} /* f_oscltx__ */

