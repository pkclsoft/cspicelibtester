/* f_pck20.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c_b18 = 1980000;
static logical c_true = TRUE_;
static integer c__51 = 51;
static integer c_n1 = -1;
static doublereal c_b57 = 0.;
static doublereal c_b60 = -1.;
static integer c__3 = 3;
static integer c__1 = 1;
static integer c__9 = 9;
static doublereal c_b270 = 1e-12;

/* $Procedure F_PCK20 ( PCK data type 20 tests ) */
/* Subroutine */ int f_pck20__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double d_int(doublereal *);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static integer body;
    static doublereal last;
    extern /* Subroutine */ int t_spkchb__(char *, char *, char *, doublereal 
	    *, doublereal *, integer *, integer *, doublereal *, doublereal *,
	     doublereal *, ftnlen, ftnlen, ftnlen);
    static doublereal work[51];
    static integer i__, j, k;
    static doublereal cdata[1980000];
    static integer n;
    static char label[80];
    static doublereal delta;
    static char frame[32], segid[80];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal rbuff[900009]	/* was [3][3][100001] */;
    static integer class__, recno;
    extern /* Subroutine */ int pckw20_(integer *, integer *, char *, 
	    doublereal *, doublereal *, char *, doublereal *, integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, doublereal *,
	     doublereal *, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static doublereal midpt;
    static integer nsamp;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static doublereal first, xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int spkw09_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    extern doublereal twopi_(void);
    extern /* Subroutine */ int bodc2n_(integer *, char *, logical *, ftnlen),
	     t_success__(logical *), xf2eul_(doublereal *, integer *, integer 
	    *, integer *, doublereal *, logical *), xf2rav_(doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    ;
    static doublereal ascale, et;
    static integer degree, handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int dafcls_(integer *);
    static integer frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     kclear_(void), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen);
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    static doublereal avbuff[300003]	/* was [3][100001] */, tscale, velcof[
	    153];
    static integer center;
    static doublereal epochs[100001], initjd;
    static char fixref[32], centnm[36], srcref[32];
    static integer clssid, frcent;
    static char bodynm[36];
    static doublereal intlen, poscof[153], initfr, eulsta[6], xavbuf[300003]	
	    /* was [3][100001] */, xrbuff[900009]	/* was [3][3][100001] 
	    */;
    static integer polydg;
    static logical unique;
    extern /* Subroutine */ int pckopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    static integer nterms;
    static doublereal et0, et1, xstbuf[600006]	/* was [6][100001] */;
    extern /* Subroutine */ int pckcls_(integer *), namfrm_(char *, integer *,
	     ftnlen), frinfo_(integer *, integer *, integer *, integer *, 
	    logical *);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     sxform_(char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), spkopn_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), spkcls_(integer *), tstlsk_(void), furnsh_(char *, 
	    ftnlen), spkezp_(integer *, doublereal *, char *, char *, integer 
	    *, doublereal *, doublereal *, ftnlen, ftnlen), vsclip_(
	    doublereal *, doublereal *), unload_(char *, ftnlen);
    extern doublereal j2000_(void), dpr_(void), rpd_(void), spd_(void);
    static integer han2;
    static doublereal day0, pos0[3];

/* $ Abstract */

/*     Exercise routines associated with PCK data type 20. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare SPK data record size.  This record is declared in */
/*     SPKPVN and is passed to SPK reader (SPKRxx) and evaluator */
/*     (SPKExx) routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     1) If new SPK types are added, it may be necessary to */
/*        increase the size of this record.  The header of SPKPVN */
/*        should be updated as well to show the record size */
/*        requirement for each data type. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 05-OCT-2012 (NJB) */

/*        Updated to support increase of maximum degree to 27 for types */
/*        2, 3, 8, 9, 12, 13, 18, and 19. See SPKPVN for a list */
/*        of record size requirements as a function of data type. */

/* -    SPICELIB Version 1.0.0, 16-AUG-2002 (NJB) */

/* -& */

/*     End include file spkrec.inc */

/* $ Abstract */

/*     Declare parameters specific to SPK type 20. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 30-DEC-2013 (NJB) */

/* -& */
/*     MAXDEG         is the maximum allowed degree of the input */
/*                    Chebyshev expansions. If the value of MAXDEG is */
/*                    increased, the SPICELIB routine SPKPVN must be */
/*                    changed accordingly. In particular, the size of */
/*                    the record passed to SPKRnn and SPKEnn must be */
/*                    increased, and comments describing the record size */
/*                    must be changed. */

/*                    The record size requirement is */

/*                       MAXREC = 3 * ( MAXDEG + 3 ) */



/*     TOLSCL         is a tolerance scale factor (also called a */
/*                    "relative tolerance") used for time coverage */
/*                    bound checking. TOLSCL is unitless. TOLSCL */
/*                    produces a tolerance value via the formula */

/*                       TOL = TOLSCL * MAX( ABS(FIRST), ABS(LAST) ) */

/*                    where FIRST and LAST are the coverage time bounds */
/*                    of a type 20 segment, expressed as seconds past */
/*                    J2000 TDB. */

/*                    The resulting parameter TOL is used as a tolerance */
/*                    for comparing the input segment descriptor time */
/*                    bounds to the first and last epoch covered by the */
/*                    sequence of time intervals defined by the inputs */
/*                    to SPKW20: */

/*                       INITJD */
/*                       INITFR */
/*                       INTLEN */
/*                       N */

/*     Tolerance scale for coverage gap at the endpoints */
/*     of the segment coverage interval: */


/*     End of include file spk20.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the following routines that write and read */
/*     type 20 PCK data: */

/*        PCKE20 */
/*        PCKR20 */
/*        PCKW20 */

/*     The higher level PCK routine */

/*        PCKMAT */

/*     is also exercised by this test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-MAR-2014 (NJB) */


/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Note: SEGID is declared longer than SIDLEN because of the need to */
/*     hold a long string for testing error handling. */


/*     Saved variables */


/*     Save all local variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_PCK20", (ftnlen)7);

/*     Open a new PCK file for writing. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create kernels.", (ftnlen)22);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);

/*     Initialize the data buffer with values that are recognizable but */
/*     otherwise bogus. */

    for (i__ = 1; i__ <= 1980000; ++i__) {
	cdata[(i__1 = i__ - 1) < 1980000 && 0 <= i__1 ? i__1 : s_rnge("cdata",
		 i__1, "f_pck20__", (ftnlen)287)] = (doublereal) i__;
    }

/*     Pick class ID and frame. */

    clssid = 3;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     Initial record count. */

    n = 100;

/*     Polynomial degree. */

    polydg = 16;

/*     Record interval length and start time. Units */
/*     are Julian ephemeris days. */

    intlen = 5.;
    initjd = 2451545.;
    initfr = .25;

/*     Pick nominal time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/*     Initialize segment identifier. */

    s_copy(segid, "pckw20 test segment", (ftnlen)80, (ftnlen)19);

/* ***************************************************************** */
/* * */
/* *    PCKW20 error cases: */
/* * */
/* ***************************************************************** */


/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid frame.", (ftnlen)14);
    polydg = 1;
    ascale = rpd_();
    tscale = spd_();
    cleard_(&c_b18, cdata);
    pckw20_(&handle, &clssid, "XXX", &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)3, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree too high.", (ftnlen)27);

/*     POLYDG = MAXDEG + 1 */

    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    c__51, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree too low.", (ftnlen)26);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &c_n1,
	     cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (ftnlen)
	    80);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID too long.", (ftnlen)15);
    s_copy(segid, "1234567890123456789012345678912345678901234567890", (
	    ftnlen)80, (ftnlen)49);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(IDSTRINGTOOLONG)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID contains non-printable characters.", (ftnlen)40);
    s_copy(segid, "pckw20 test segment", (ftnlen)80, (ftnlen)19);
    *(unsigned char *)&segid[4] = '\a';
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid coefficient count", (ftnlen)25);
    s_copy(segid, "pckw20 test segment", (ftnlen)80, (ftnlen)19);
    n = 0;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = -1;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = 100;

/* --- Case: ------------------------------------------------------ */

    tcase_("Descriptor times out of order", (ftnlen)29);
    first = last + 1.;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);
    first = (initjd - j2000_() + initfr) * spd_();
    last = first - 1.;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/*     Restore original descriptor time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/* --- Case: ------------------------------------------------------ */

    tcase_("Gap following last epoch", (ftnlen)24);
    last += 2e-4;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();
    first += -2e-4;
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/*     Restore original descriptor time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive interval length", (ftnlen)28);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &c_b57, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INTLENNOTPOS)", ok, (ftnlen)19);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &c_b60, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INTLENNOTPOS)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive distance or time scale", (ftnlen)35);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &c_b57, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &c_b60, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &c_b57, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &c_b60, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);

/*     Delete PCK used for error test cases. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    PCKW20 non-error exception cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with coverage gaps at each end.", (ftnlen)46);

/*     The following segment will have a two-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     9 ms in each direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    clssid = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 2.;
    initjd = j2000_() - 1.;
    initfr = 0.;
    first = -spd_() - 1e-9;
    last = spd_() + 1e-9;
    n = 1;
    polydg = 20;

/*     The angle scale is radians; the time scale is Julian days. */

    ascale = 1.;
    tscale = spd_();
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and coverage gaps at "
	    "each end.", (ftnlen)73);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     40 ms in each direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    clssid = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_() - .001;
    last = (initjd + intlen - j2000_()) * spd_() + .001;
    n = 1;
    polydg = 20;

/*     The distance scale is radians; the time scale is Julian days. */

    ascale = 1.;
    tscale = spd_();
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     And one more error case: */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and excessive coverag"
	    "e gaps at the left end.", (ftnlen)87);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     100 ms in the negative direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    namfrm_("IAU_MOON", &frcode, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    frinfo_(&frcode, &frcent, &class__, &clssid, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_() - .1;
    last = (initjd + intlen - j2000_()) * spd_();
    n = 1;
    polydg = 20;

/*     The distance scale is degrees; the time scale is Julian days. */

    ascale = dpr_();
    tscale = spd_();
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and excessive coverag"
	    "e gaps at the right end.", (ftnlen)88);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     100 ms in the negative direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    clssid = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_();
    last = (initjd + intlen - j2000_()) * spd_() + .1;
    n = 1;
    polydg = 20;

/*     The angle scale is degrees; the time scale is Julian days. */

    ascale = rpd_();
    tscale = spd_();
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    PCKW20, PCKR20, PCKE20 normal cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create PCK with one small type 20 segment.The PCK provides IAU_M"
	    "OON orientation.", (ftnlen)80);

/*     Create a small PCK file containing 1 record. We'll represent */
/*     orientation of the IAU_MOON frame relative to J2000. */

/*     We're going to do something dirty here: we'll create a type */
/*     9 SPK file containing Euler angle states. This will enable */
/*     us to fit Chebys to the states using existing test utility */
/*     code. */


/*     Create and load a test PCK to provide orientation data. */
/*     Keep the file after loading. */

    tstpck_("test.bpc", &c_true, &c_true, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample orientation from the PCK and convert to */
/*     Euler states. */

    first = -spd_();
    last = spd_();
    nsamp = 10000;
    delta = (last - first) / (nsamp - 1);
    s_copy(srcref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	epochs[(i__2 = i__ - 1) < 100001 && 0 <= i__2 ? i__2 : s_rnge("epochs"
		, i__2, "f_pck20__", (ftnlen)874)] = et;
	sxform_("J2000", srcref, &et, xform, (ftnlen)5, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2eul_(xform, &c__3, &c__1, &c__3, eulsta, &unique);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Reorder the state as we pack it into the state buffer. */
/*        The rightmost angle comes first. */

	for (j = 1; j <= 3; ++j) {
	    xstbuf[(i__2 = 4 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)888)] = 
		    eulsta[(i__3 = j - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)888)];
	    xstbuf[(i__2 = 7 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)889)] = 
		    eulsta[(i__3 = j + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)889)];
	}
    }

/*     Create a type 9 SPK from the state data. */

    if (exists_("source_data.bsp", (ftnlen)15)) {
	delfil_("source_data.bsp", (ftnlen)15);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("source_data.bsp", " ", &c__0, &handle, (ftnlen)15, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 301;
    center = 3;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(segid, "Source Euler states", (ftnlen)80, (ftnlen)19);
    degree = 3;
    spkw09_(&handle, &body, &center, frame, &first, &last, segid, &degree, &
	    nsamp, xstbuf, epochs, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load test SPK to provide data. */

    furnsh_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate angle and angular rate coefficients for the segment. */
/*     We'll store only the rate coefficients. */

    bodc2n_(&center, centnm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodc2n_(&body, bodynm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nterms = polydg + 1;
    t_spkchb__(bodynm, centnm, frame, &first, &last, &nterms, &nterms, work, 
	    poscof, velcof, (ftnlen)36, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the coefficients to represent positions in units */
/*     of ASCALE and velocities in units of ASCALE/TSCALE. */

    ascale = rpd_();
    tscale = spd_();
    i__1 = nterms * 3;
    for (i__ = 1; i__ <= i__1; ++i__) {
	poscof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("poscof", 
		i__2, "f_pck20__", (ftnlen)952)] = poscof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("poscof", i__3, "f_pck20__", 
		(ftnlen)952)] / ascale;
	velcof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("velcof", 
		i__2, "f_pck20__", (ftnlen)954)] = velcof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("velcof", i__3, "f_pck20__", 
		(ftnlen)954)] / ascale * tscale;
    }

/*     Get the state at the interval midpoint. */

    midpt = (first + last) / 2;
    spkezp_(&body, &midpt, frame, "NONE", &center, pos0, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the midpoint position to units of ASCALE. */

    d__1 = 1. / ascale;
    vsclip_(&d__1, pos0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Pack the coefficient array. */

    k = 1;
    for (i__ = 1; i__ <= 3; ++i__) {
	j = (i__ - 1) * nterms + 1;
	moved_(&velcof[(i__1 = j - 1) < 153 && 0 <= i__1 ? i__1 : s_rnge(
		"velcof", i__1, "f_pck20__", (ftnlen)981)], &nterms, &cdata[(
		i__2 = k - 1) < 1980000 && 0 <= i__2 ? i__2 : s_rnge("cdata", 
		i__2, "f_pck20__", (ftnlen)981)]);
	cdata[(i__1 = k + nterms - 1) < 1980000 && 0 <= i__1 ? i__1 : s_rnge(
		"cdata", i__1, "f_pck20__", (ftnlen)982)] = pos0[(i__2 = i__ 
		- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("pos0", i__2, "f_pck20"
		"__", (ftnlen)982)];
	k = k + nterms + 1;
    }

/*     Get class ID of "to" frame. We're going to pretend this */
/*     is the IAU_JUPITER frame. All we need is an IAU frame */
/*     distinct from IAU_MOON. */

    s_copy(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    namfrm_(fixref, &frcode, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    frinfo_(&frcode, &frcent, &class__, &clssid, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     The record covers two days. */

    first = -spd_();
    last = spd_();
    n = 1;
    polydg = 20;

/*     The interval length has units of Julian days. */

    intlen = (last - first) / spd_();
    day0 = j2000_() + first / spd_();
    initjd = d_int(&day0);
    initfr = day0 - initjd;
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sample orientation from small IAU_MOON PCK.", (ftnlen)43);
    furnsh_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check orientation from the new type 20 PCK file. */

    nsamp = 10001;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, fixref, &et, xform, (ftnlen)32, (ftnlen)32);
	xf2rav_(xform, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1067)], &avbuff[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("avbuff", i__3, "f_pck20__", (ftnlen)1067)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, srcref, &et, xform, (ftnlen)32, (ftnlen)32);
	xf2rav_(xform, &xrbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 
		<= i__2 ? i__2 : s_rnge("xrbuff", i__2, "f_pck20__", (ftnlen)
		1079)], &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1079)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Test orientation results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "R(@)", (ftnlen)80, (ftnlen)4);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1095)], "~~/", &xrbuff[(i__3 = (i__ * 3 + 1) * 3 - 12) < 
		900009 && 0 <= i__3 ? i__3 : s_rnge("xrbuff", i__3, "f_pck20"
		"__", (ftnlen)1095)], &c__9, &c_b270, ok, (ftnlen)80, (ftnlen)
		3);
    }

/*     Test angular velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "AV(@)", (ftnlen)80, (ftnlen)5);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &avbuff[(i__2 = i__ * 3 - 3) < 300003 && 0 <= i__2 ? 
		i__2 : s_rnge("avbuff", i__2, "f_pck20__", (ftnlen)1109)], 
		"~~/", &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1109)], &
		c__3, &c_b270, ok, (ftnlen)80, (ftnlen)3);
    }
    unload_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create PCK with one small type 20 segment. The PCK provides IAU_"
	    "EARTH orientation.", (ftnlen)82);

/*     Create a small PCK file containing 1 record. We'll represent */
/*     orientation of the IAU_EARTH frame relative to ECLIPJ2000 */

/*     We're going to do something dirty here: we'll create a type */
/*     9 SPK file containing Euler angle states. This will enable */
/*     us to fit Chebys to the states using existing test utility */
/*     code. */

    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test.bpc", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample orientation from the PCK and convert to */
/*     Euler states. */

    first = -spd_();
    last = first + spd_() * 2;
    nsamp = 10000;
    delta = (last - first) / (nsamp - 1);
    s_copy(frame, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    s_copy(srcref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	epochs[(i__2 = i__ - 1) < 100001 && 0 <= i__2 ? i__2 : s_rnge("epochs"
		, i__2, "f_pck20__", (ftnlen)1161)] = et;
	sxform_(frame, srcref, &et, xform, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2eul_(xform, &c__3, &c__1, &c__3, eulsta, &unique);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Adjust the angle corresponding to RA+pi/2 */
/*        so it remains continuous. */

	if (eulsta[2] < 0.) {
	    eulsta[2] += twopi_();
	}

/*        Reorder the state as we pack it into the state buffer. */
/*        The rightmost angle comes first. */

	for (j = 1; j <= 3; ++j) {
	    xstbuf[(i__2 = 4 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)1181)] = 
		    eulsta[(i__3 = j - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)1181)];
	    xstbuf[(i__2 = 7 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)1182)] = 
		    eulsta[(i__3 = j + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)1182)];
	}
    }

/*     Create a type 9 SPK from the state data. */

    if (exists_("source_data.bsp", (ftnlen)15)) {
	delfil_("source_data.bsp", (ftnlen)15);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("source_data.bsp", " ", &c__0, &handle, (ftnlen)15, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 399;
    center = 3;
    s_copy(segid, "Source Euler states", (ftnlen)80, (ftnlen)19);
    degree = 7;
    spkw09_(&handle, &body, &center, frame, &first, &last, segid, &degree, &
	    nsamp, xstbuf, epochs, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load test SPK to provide data. */

    furnsh_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate angle and angular rate coefficients for the segment. */
/*     We'll store only the rate coefficients. */

    bodc2n_(&center, centnm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodc2n_(&body, bodynm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nterms = polydg + 1;
    t_spkchb__(bodynm, centnm, frame, &first, &last, &nterms, &nterms, work, 
	    poscof, velcof, (ftnlen)36, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the coefficients to represent positions in units */
/*     of ASCALE and velocities in units of ASCALE/TSCALE. */

    ascale = rpd_();
    tscale = spd_();
    i__1 = nterms * 3;
    for (i__ = 1; i__ <= i__1; ++i__) {
	poscof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("poscof", 
		i__2, "f_pck20__", (ftnlen)1244)] = poscof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("poscof", i__3, "f_pck20__", 
		(ftnlen)1244)] / ascale;
	velcof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("velcof", 
		i__2, "f_pck20__", (ftnlen)1246)] = velcof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("velcof", i__3, "f_pck20__", 
		(ftnlen)1246)] / ascale * tscale;
    }

/*     Get the state at the interval midpoint. */

    midpt = (first + last) / 2;

/*     The position information from the SPK is garbage; we must */
/*     compute POS0 directly. */

    spkezp_(&body, &midpt, frame, "NONE", &center, pos0, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the midpoint position to units of ASCALE. */

    d__1 = 1. / ascale;
    vsclip_(&d__1, pos0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Pack the coefficient array. */

    k = 1;
    for (i__ = 1; i__ <= 3; ++i__) {
	j = (i__ - 1) * nterms + 1;
	moved_(&velcof[(i__1 = j - 1) < 153 && 0 <= i__1 ? i__1 : s_rnge(
		"velcof", i__1, "f_pck20__", (ftnlen)1278)], &nterms, &cdata[(
		i__2 = k - 1) < 1980000 && 0 <= i__2 ? i__2 : s_rnge("cdata", 
		i__2, "f_pck20__", (ftnlen)1278)]);
	cdata[(i__1 = k + nterms - 1) < 1980000 && 0 <= i__1 ? i__1 : s_rnge(
		"cdata", i__1, "f_pck20__", (ftnlen)1279)] = pos0[(i__2 = i__ 
		- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("pos0", i__2, "f_pck20"
		"__", (ftnlen)1279)];
	k = k + nterms + 1;
    }

/*     Get class ID of "to" frame. We're going to pretend this */
/*     is the IAU_JUPITER frame. All we need is an IAU frame */
/*     distinct from IAU_EARTH. */

    s_copy(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    namfrm_(fixref, &frcode, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    frinfo_(&frcode, &frcent, &class__, &clssid, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    n = 1;
    polydg = 20;

/*     The interval length has units of Julian days. */

    intlen = (last - first) / spd_();
    day0 = j2000_() + first / spd_();
    initjd = d_int(&day0);
    initfr = day0 - initjd;
    if (exists_("pck_test_20_v1.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v1.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v1.bpc", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckw20_(&handle, &clssid, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sample orientation from small IAU_EARTH PCK.", (ftnlen)44);
    furnsh_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check orientation from the new type 20 PCK file. */

    nsamp = 10001;
    nsamp = 10;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, fixref, &et, xform, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2rav_(xform, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1361)], &avbuff[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("avbuff", i__3, "f_pck20__", (ftnlen)1361)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, srcref, &et, xform, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2rav_(xform, &xrbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 
		<= i__2 ? i__2 : s_rnge("xrbuff", i__2, "f_pck20__", (ftnlen)
		1375)], &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1375)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Test orientation results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "R(@)", (ftnlen)80, (ftnlen)4);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1390)], "~~/", &xrbuff[(i__3 = (i__ * 3 + 1) * 3 - 12) < 
		900009 && 0 <= i__3 ? i__3 : s_rnge("xrbuff", i__3, "f_pck20"
		"__", (ftnlen)1390)], &c__9, &c_b270, ok, (ftnlen)80, (ftnlen)
		3);
	if (! (*ok)) {
	    s_stop("", (ftnlen)0);
	}
    }

/*     Test angular velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "AV(@)", (ftnlen)80, (ftnlen)5);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &avbuff[(i__2 = i__ * 3 - 3) < 300003 && 0 <= i__2 ? 
		i__2 : s_rnge("avbuff", i__2, "f_pck20__", (ftnlen)1408)], 
		"~~/", &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1408)], &
		c__3, &c_b270, ok, (ftnlen)80, (ftnlen)3);
	if (! (*ok)) {
	    s_stop("", (ftnlen)0);
	}
    }
    unload_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a PCK with a large type 20 segment having multiple record"
	    "s. The PCK provides IAU_MOON orientation.", (ftnlen)105);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    s_copy(srcref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    clssid = 599;
    body = 301;
    center = 3;
    bodc2n_(&center, centnm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodc2n_(&body, bodynm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     N is the record count. */

    n = 1000;

/*     The interval length has units of Julian days. */

/*     Each record covers 1/8 day. */

    intlen = .125;
    first = jyear_() * -10;
    last = first + n * intlen * spd_();
    polydg = 20;
    day0 = j2000_() + first / spd_();
    initjd = d_int(&day0);
    initfr = day0 - initjd;

/*     Sample orientation from the PCK and convert to */
/*     Euler states. */

    nsamp = 100000;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	epochs[(i__2 = i__ - 1) < 100001 && 0 <= i__2 ? i__2 : s_rnge("epochs"
		, i__2, "f_pck20__", (ftnlen)1478)] = et;
	sxform_(frame, srcref, &et, xform, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xf2eul_(xform, &c__3, &c__1, &c__3, eulsta, &unique);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Reorder the state as we pack it into the state buffer. */
/*        The rightmost angle comes first. */

	for (j = 1; j <= 3; ++j) {
	    xstbuf[(i__2 = 4 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)1492)] = 
		    eulsta[(i__3 = j - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)1492)];
	    xstbuf[(i__2 = 7 - j + i__ * 6 - 7) < 600006 && 0 <= i__2 ? i__2 :
		     s_rnge("xstbuf", i__2, "f_pck20__", (ftnlen)1493)] = 
		    eulsta[(i__3 = j + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "eulsta", i__3, "f_pck20__", (ftnlen)1493)];
	}
    }

/*     Create a type 9 SPK from the state data. */

    if (exists_("source_data1.bsp", (ftnlen)16)) {
	delfil_("source_data1.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("source_data1.bsp", " ", &c__0, &handle, (ftnlen)16, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 301;
    center = 3;
    s_copy(segid, "Source Euler states", (ftnlen)80, (ftnlen)19);
    degree = 3;
    spkw09_(&handle, &body, &center, frame, &first, &last, segid, &degree, &
	    nsamp, xstbuf, epochs, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now create the PCK file. */


/*     Load test SPK to provide data. */

    furnsh_("source_data1.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The interval length has units of Julian days. */

/*     Each record covers 1/8 day. */


/*     Create N data records. */

    nterms = polydg + 1;
    k = 1;
    i__1 = n;
    for (recno = 1; recno <= i__1; ++recno) {
	et0 = first + (recno - 1) * intlen * spd_();
	et1 = et0 + intlen * spd_();
	t_spkchb__(bodynm, centnm, frame, &et0, &et1, &nterms, &nterms, work, 
		poscof, velcof, (ftnlen)36, (ftnlen)36, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Scale the coefficients to represent positions in units */
/*        of ASCALE and velocities in units of ASCALE/TSCALE. */

	i__2 = nterms * 3;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    poscof[(i__3 = i__ - 1) < 153 && 0 <= i__3 ? i__3 : s_rnge("posc"
		    "of", i__3, "f_pck20__", (ftnlen)1562)] = poscof[(i__4 = 
		    i__ - 1) < 153 && 0 <= i__4 ? i__4 : s_rnge("poscof", 
		    i__4, "f_pck20__", (ftnlen)1562)] / ascale;
	    velcof[(i__3 = i__ - 1) < 153 && 0 <= i__3 ? i__3 : s_rnge("velc"
		    "of", i__3, "f_pck20__", (ftnlen)1564)] = velcof[(i__4 = 
		    i__ - 1) < 153 && 0 <= i__4 ? i__4 : s_rnge("velcof", 
		    i__4, "f_pck20__", (ftnlen)1564)] / ascale * tscale;
	}

/*        Get the state at the interval midpoint. */

	midpt = (et0 + et1) / 2;
	spkezp_(&body, &midpt, frame, "NONE", &center, pos0, &lt, (ftnlen)32, 
		(ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Scale the midpoint position to units of ASCALE. */

	d__1 = 1. / ascale;
	vsclip_(&d__1, pos0);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Pack the coefficient array. */

	for (i__ = 1; i__ <= 3; ++i__) {
	    j = (i__ - 1) * nterms + 1;
	    moved_(&velcof[(i__2 = j - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge(
		    "velcof", i__2, "f_pck20__", (ftnlen)1590)], &nterms, &
		    cdata[(i__3 = k - 1) < 1980000 && 0 <= i__3 ? i__3 : 
		    s_rnge("cdata", i__3, "f_pck20__", (ftnlen)1590)]);
	    cdata[(i__2 = k + nterms - 1) < 1980000 && 0 <= i__2 ? i__2 : 
		    s_rnge("cdata", i__2, "f_pck20__", (ftnlen)1591)] = pos0[(
		    i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("pos0", 
		    i__3, "f_pck20__", (ftnlen)1591)];
	    k = k + nterms + 1;
	}
    }
    if (exists_("pck_test_20_v2.bpc", (ftnlen)18)) {
	delfil_("pck_test_20_v2.bpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    pckopn_("pck_test_20_v2.bpc", " ", &c__0, &han2, (ftnlen)18, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckw20_(&han2, &clssid, frame, &first, &last, segid, &intlen, &n, &polydg,
	     cdata, &ascale, &tscale, &initjd, &initfr, (ftnlen)32, (ftnlen)
	    80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("source_data1.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sample orientation from large PCK.", (ftnlen)34);
    furnsh_("pck_test_20_v2.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check orientation from the new type 20 PCK file. */

    nsamp = 100001;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, fixref, &et, xform, (ftnlen)32, (ftnlen)32);
	xf2rav_(xform, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1646)], &avbuff[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("avbuff", i__3, "f_pck20__", (ftnlen)1646)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	sxform_(frame, srcref, &et, xform, (ftnlen)32, (ftnlen)32);
	xf2rav_(xform, &xrbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 
		<= i__2 ? i__2 : s_rnge("xrbuff", i__2, "f_pck20__", (ftnlen)
		1658)], &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1658)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Test orientation results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "R(@)", (ftnlen)80, (ftnlen)4);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &rbuff[(i__2 = (i__ * 3 + 1) * 3 - 12) < 900009 && 0 <=
		 i__2 ? i__2 : s_rnge("rbuff", i__2, "f_pck20__", (ftnlen)
		1674)], "~~/", &xrbuff[(i__3 = (i__ * 3 + 1) * 3 - 12) < 
		900009 && 0 <= i__3 ? i__3 : s_rnge("xrbuff", i__3, "f_pck20"
		"__", (ftnlen)1674)], &c__9, &c_b270, ok, (ftnlen)80, (ftnlen)
		3);
    }

/*     Test angular velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "AV(@)", (ftnlen)80, (ftnlen)5);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &avbuff[(i__2 = i__ * 3 - 3) < 300003 && 0 <= i__2 ? 
		i__2 : s_rnge("avbuff", i__2, "f_pck20__", (ftnlen)1688)], 
		"~~/", &xavbuf[(i__3 = i__ * 3 - 3) < 300003 && 0 <= i__3 ? 
		i__3 : s_rnge("xavbuf", i__3, "f_pck20__", (ftnlen)1688)], &
		c__3, &c_b270, ok, (ftnlen)80, (ftnlen)3);
    }
    unload_("pck_test_20_v2.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Close and delete the PCK files. */

    tcase_("Clean up.", (ftnlen)9);
    delfil_("pck_test_20_v1.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pck_test_20_v2.bpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("source_data.bsp", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("source_data1.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("source_data1.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_pck20__ */

