/* f_pckbuf.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__200 = 200;
static integer c__3 = 3;
static integer c__1 = 1;
static logical c_false = FALSE_;
static integer c__9 = 9;
static doublereal c_b51 = 1e-12;
static logical c_true = TRUE_;
static integer c__2 = 2;

/* $Procedure      F_PCKBUF ( Text PCK reader buffering tests ) */
/* Subroutine */ int f_pckbuf__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal mbod[9]	/* was [3][3] */;
    char line[80];
    doublereal meul[9]	/* was [3][3] */, mtip[9]	/* was [3][3] */, 
	    mtis[9]	/* was [3][3] */;
    extern integer zzbodbry_(integer *);
    extern /* Subroutine */ int eul2m_(doublereal *, doublereal *, doublereal 
	    *, integer *, integer *, integer *, doublereal *);
    integer i__, j;
    doublereal w;
    integer refid;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     tcase_(char *, ftnlen);
    integer frmid;
    doublereal dvals[200];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), t_success__(
	    logical *), chckad_(char *, doublereal *, char *, doublereal *, 
	    integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal lambda, ra, et;
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     bodmat_(integer *, doublereal *, doublereal *);
    integer bodyid;
    extern /* Subroutine */ int bodeul_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *);
    integer nangls;
    extern /* Subroutine */ int tipbod_(char *, integer *, doublereal *, 
	    doublereal *, ftnlen), tisbod_(char *, integer *, doublereal *, 
	    doublereal *, ftnlen), clpool_(void), pdpool_(char *, integer *, 
	    doublereal *, ftnlen), pipool_(char *, integer *, integer *, 
	    ftnlen), dvpool_(char *, ftnlen);
    extern doublereal j2000_(void);
    doublereal dec;
    extern doublereal spd_(void);
    doublereal m6x6[36]	/* was [6][6] */;

/* $ Abstract */

/*     This family tests buffering in the PCK natural body rotation */
/*     routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     PCK */

/* $ Keywords */

/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   Test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK             is a boolean flag indicating test status. OK is */
/*                    returned .TRUE. only if all tests conducted by */
/*                    this family have passed. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     Not applicable. */

/* $ Files */

/*     Not applicable. */

/* $ Particulars */

/*     This routine tests buffering in the PCK natural body rotation */
/*     routines introduced in N0066. */

/*     Orientation data are checked by means of calls to the SPICELIB */
/*     routines */

/*        BODMAT */
/*        TIPBOD */
/*        TISBOD */

/*     that were updated to do buffering against */

/*        BODEUL */

/*     that was not. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     B.V. Semenov      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 02-MAR-2016 (BVS) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters. */

/*     MXPOLY and MAXANG should be the same as TISBOD values. */

/*     MAXBOD should be greater than TISBOD's MAXBOD to exercise reset */
/*     on fill up. */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_PCKBUF", (ftnlen)8);

/*     Clear pool and fill it with bogus PCK data. */

    clpool_();
    for (bodyid = 1000; bodyid <= 1300; ++bodyid) {
	refid = zzbodbry_(&bodyid);
	frmid = bodyid % 15 + 1;
	nangls = bodyid % 100 + 1;
	d__1 = (doublereal) bodyid / 1e3;
	filld_(&d__1, &c__200, dvals);
	s_copy(line, "BODY#_POLE_RA", (ftnlen)80, (ftnlen)13);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &c__3, dvals, (ftnlen)80);
	s_copy(line, "BODY#_POLE_DEC", (ftnlen)80, (ftnlen)14);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &c__3, dvals, (ftnlen)80);
	s_copy(line, "BODY#_PM", (ftnlen)80, (ftnlen)8);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &c__3, dvals, (ftnlen)80);
	s_copy(line, "BODY#_NUT_PREC_RA", (ftnlen)80, (ftnlen)17);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &nangls, dvals, (ftnlen)80);
	s_copy(line, "BODY#_NUT_PREC_DEC", (ftnlen)80, (ftnlen)18);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &nangls, dvals, (ftnlen)80);
	s_copy(line, "BODY#_NUT_PREC_PM", (ftnlen)80, (ftnlen)17);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pdpool_(line, &nangls, dvals, (ftnlen)80);
	s_copy(line, "BODY#_CONSTANTS_JED_EPOCH", (ftnlen)80, (ftnlen)25);
	repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	d__1 = dvals[0] + j2000_();
	pdpool_(line, &c__1, &d__1, (ftnlen)80);
	s_copy(line, "BODY#_CONSTANTS_REF_FRAME", (ftnlen)80, (ftnlen)25);
	repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	pipool_(line, &c__1, &frmid, (ftnlen)80);
	s_copy(line, "BODY#_NUT_PREC_ANGLES", (ftnlen)80, (ftnlen)21);
	repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	i__1 = nangls << 1;
	pdpool_(line, &i__1, dvals, (ftnlen)80);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Check PCK rotations with un-disturbed POOL", (ftnlen)42);
    et = spd_() * 300;
    for (bodyid = 1000; bodyid <= 1300; ++bodyid) {

/*        Get rotations using all four PCK routines. */

	bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = halfpi_() - dec;
	d__2 = halfpi_() + ra;
	eul2m_(&w, &d__1, &d__2, &c__3, &c__1, &c__3, meul);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	bodmat_(&bodyid, &et, mbod);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (i__ = 1; i__ <= 3; ++i__) {
	    for (j = 1; j <= 3; ++j) {
		mtis[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : 
			s_rnge("mtis", i__1, "f_pckbuf__", (ftnlen)265)] = 
			m6x6[(i__2 = i__ + j * 6 - 7) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("m6x6", i__2, "f_pckbuf__", (ftnlen)265)
			];
	    }
	}

/*        Check all matrices against BODEUL matrix. */

	s_copy(line, "BODMAT M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mbod, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TIPBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtip, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TISBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtis, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Check PCK rotations with disturbed POOL", (ftnlen)39);
    et = spd_() * 300;
    for (bodyid = 1000; bodyid <= 1300; ++bodyid) {

/*        Get rotations using all four PCK routines with "touching" */
/*        before all of them but BODEUL. */

	bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = halfpi_() - dec;
	d__2 = halfpi_() + ra;
	eul2m_(&w, &d__1, &d__2, &c__3, &c__1, &c__3, meul);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	bodmat_(&bodyid, &et, mbod);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (i__ = 1; i__ <= 3; ++i__) {
	    for (j = 1; j <= 3; ++j) {
		mtis[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : 
			s_rnge("mtis", i__1, "f_pckbuf__", (ftnlen)322)] = 
			m6x6[(i__2 = i__ + j * 6 - 7) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("m6x6", i__2, "f_pckbuf__", (ftnlen)322)
			];
	    }
	}

/*        Check all matrices against BODEUL matrix. */

	s_copy(line, "BODMAT M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mbod, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TIPBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtip, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TISBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtis, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
    }

/* --- Some success cases with partial data ------------------------ */

    tcase_("Check PCK rotations with partial data", (ftnlen)37);
    et = spd_() * 300;
    for (bodyid = 1295; bodyid <= 1300; ++bodyid) {

/*        Remove some optional values from the POOL. */

	refid = zzbodbry_(&bodyid);
	if (bodyid == 1295) {
	    s_copy(line, "BODY#_NUT_PREC_RA", (ftnlen)80, (ftnlen)17);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	} else if (bodyid == 1296) {
	    s_copy(line, "BODY#_NUT_PREC_DEC", (ftnlen)80, (ftnlen)18);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	} else if (bodyid == 1297) {
	    s_copy(line, "BODY#_NUT_PREC_PM", (ftnlen)80, (ftnlen)17);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	} else if (bodyid == 1298) {
	    s_copy(line, "BODY#_CONSTANTS_JED_EPOCH", (ftnlen)80, (ftnlen)25);
	    repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    dvpool_(line, (ftnlen)80);
	} else if (bodyid == 1299) {
	    s_copy(line, "BODY#_CONSTANTS_REF_FRAME", (ftnlen)80, (ftnlen)25);
	    repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    dvpool_(line, (ftnlen)80);
	} else if (bodyid == 1300) {
	    s_copy(line, "BODY#_NUT_PREC_RA", (ftnlen)80, (ftnlen)17);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	    s_copy(line, "BODY#_NUT_PREC_DEC", (ftnlen)80, (ftnlen)18);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	    s_copy(line, "BODY#_NUT_PREC_PM", (ftnlen)80, (ftnlen)17);
	    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    dvpool_(line, (ftnlen)80);
	    s_copy(line, "BODY#_NUT_PREC_ANGLES", (ftnlen)80, (ftnlen)21);
	    repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    dvpool_(line, (ftnlen)80);
	}

/*        Get rotations using all four PCK routines. */

	bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = halfpi_() - dec;
	d__2 = halfpi_() + ra;
	eul2m_(&w, &d__1, &d__2, &c__3, &c__1, &c__3, meul);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	bodmat_(&bodyid, &et, mbod);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pipool_("I", &c__1, &c__1, (ftnlen)1);
	tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (i__ = 1; i__ <= 3; ++i__) {
	    for (j = 1; j <= 3; ++j) {
		mtis[(i__1 = i__ + j * 3 - 4) < 9 && 0 <= i__1 ? i__1 : 
			s_rnge("mtis", i__1, "f_pckbuf__", (ftnlen)434)] = 
			m6x6[(i__2 = i__ + j * 6 - 7) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("m6x6", i__2, "f_pckbuf__", (ftnlen)434)
			];
	    }
	}

/*        Check all matrices against BODEUL matrix. */

	s_copy(line, "BODMAT M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mbod, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TIPBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtip, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
	s_copy(line, "TISBOD M vs BODEUL M for body #", (ftnlen)80, (ftnlen)
		31);
	repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(line, mtis, "~", meul, &c__9, &c_b51, ok, (ftnlen)80, (ftnlen)
		1);
    }

/* --- Some failure cases ------------------------------------------ */


/*     Since we have the POOL conveniently filled with whole bunch of */
/*     PCK data, let's do some error checks. */


/*     Try without _PM keyword. */

    bodyid = 1000;
    s_copy(line, "BODY#_PM", (ftnlen)80, (ftnlen)8);
    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    dvpool_(line, (ftnlen)80);
    tcase_("Check for missing _PM, BODEUL", (ftnlen)29);
    bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _PM, BODMAT", (ftnlen)29);
    bodmat_(&bodyid, &et, mbod);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _PM, TIPBOD", (ftnlen)29);
    tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _PM, TISBOD", (ftnlen)29);
    tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Try without _POLE_RA keyword. */

    bodyid = 1001;
    s_copy(line, "BODY#_POLE_RA", (ftnlen)80, (ftnlen)13);
    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    dvpool_(line, (ftnlen)80);
    tcase_("Check for missing _POLE_RA, BODEUL", (ftnlen)34);
    bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_RA, BODMAT", (ftnlen)34);
    bodmat_(&bodyid, &et, mbod);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_RA, TIPBOD", (ftnlen)34);
    tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_RA, TISBOD", (ftnlen)34);
    tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/*     Try without _POLE_DEC keyword. */

    bodyid = 1002;
    s_copy(line, "BODY#_POLE_DEC", (ftnlen)80, (ftnlen)14);
    repmi_(line, "#", &bodyid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    dvpool_(line, (ftnlen)80);
    tcase_("Check for missing _POLE_DEC, BODEUL", (ftnlen)35);
    bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_DEC, BODMAT", (ftnlen)35);
    bodmat_(&bodyid, &et, mbod);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_DEC, TIPBOD", (ftnlen)35);
    tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for missing _POLE_DEC, TISBOD", (ftnlen)35);
    tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/*     Try with _NUT_PREC_ANGLES that has too few values. */

    bodyid = 1003;
    refid = zzbodbry_(&bodyid);
    s_copy(line, "BODY#_NUT_PREC_ANGLES", (ftnlen)80, (ftnlen)21);
    repmi_(line, "#", &refid, line, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    pdpool_(line, &c__2, dvals, (ftnlen)80);
    tcase_("Check for too small _NUT_PREC_ANGLES, BODEUL", (ftnlen)44);
    bodeul_(&bodyid, &et, &ra, &dec, &w, &lambda);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    tcase_("Check for too small _NUT_PREC_ANGLES, BODMAT", (ftnlen)44);
    bodmat_(&bodyid, &et, mbod);
    chckxc_(&c_true, "SPICE(INSUFFICIENTANGLES)", ok, (ftnlen)25);
    tcase_("Check for too small _NUT_PREC_ANGLES, TIPBOD", (ftnlen)44);
    tipbod_("J2000", &bodyid, &et, mtip, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INSUFFICIENTANGLES)", ok, (ftnlen)25);
    tcase_("Check for too small _NUT_PREC_ANGLES, TISBOD", (ftnlen)44);
    tisbod_("J2000", &bodyid, &et, m6x6, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INSUFFICIENTANGLES)", ok, (ftnlen)25);

/*     Clear POOL before leaving. */

    clpool_();

/*     All done. */

    t_success__(ok);
    return 0;
} /* f_pckbuf__ */

