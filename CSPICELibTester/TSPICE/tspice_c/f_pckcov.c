/* f_pckcov.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static logical c_false = FALSE_;
static integer c__1000 = 1000;
static integer c__6 = 6;
static integer c__1 = 1;
static doublereal c_b46 = 0.;
static doublereal c_b55 = 1e6;
static doublereal c_b56 = 1e7;
static logical c_true = TRUE_;
static integer c__20 = 20;
static integer c__3 = 3;
static integer c__4 = 4;
static integer c_b141 = -1000000;

/* $Procedure      F_PCKCOV ( PCKCOV tests ) */
/* Subroutine */ int f_pckcov__(logical *ok)
{
    /* Initialized data */

    static integer frame[3] = { 4,5,6 };
    static integer nseg[3] = { 10,20,30 };

    /* System generated locals */
    integer i__1, i__2, i__3;
    cllist cl__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer f_clos(cllist *);

    /* Local variables */
    doublereal last;
    integer xids[10], i__, j;
    extern integer cardd_(doublereal *);
    extern /* Subroutine */ int dafbt_(char *, integer *, ftnlen);
    extern integer cardi_(integer *);
    extern /* Subroutine */ int tcase_(char *, ftnlen), pckw02_(integer *, 
	    integer *, char *, doublereal *, doublereal *, char *, doublereal 
	    *, integer *, integer *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), repmi_(char *, char *, integer *, char *, ftnlen, ftnlen,
	     ftnlen);
    doublereal cover[1006];
    char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal first;
    extern /* Subroutine */ int tstek_(char *, integer *, integer *, logical *
	    , integer *, ftnlen);
    integer xunit;
    extern /* Subroutine */ int t_success__(logical *), tstck3_(char *, char *
	    , logical *, logical *, logical *, integer *, ftnlen, ftnlen), 
	    chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen), chckai_(char *, integer 
	    *, char *, integer *, integer *, logical *, ftnlen, ftnlen);
    integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen), scardd_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen);
    doublereal coeffs[6]	/* was [2][3] */;
    extern /* Subroutine */ int pckcls_(integer *), pckfrm_(char *, integer *,
	     ftnlen), pckcov_(char *, integer *, doublereal *, ftnlen);
    doublereal intlen;
    extern /* Subroutine */ int pckopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *);
    doublereal xcover[3018]	/* was [1006][3] */;
    extern /* Subroutine */ int ssizei_(integer *, integer *), insrti_(
	    integer *, integer *), tstspk_(char *, logical *, integer *, 
	    ftnlen), txtopn_(char *, integer *, ftnlen);
    integer ids[10];

/* $ Abstract */

/*     This routine tests the SPICELIB routines */

/*        PCKCOV */
/*        PCKFRM */

/* $ Version */

/* -    TSPICE Version 1.0.0, 26-NOV-2007 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_PCKCOV", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create PCK file.", (ftnlen)24);

/*     Create an PCK file with data for three frames. */

    pckopn_("pckcov.bpc", "pckcov.bpc", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the expected coverage windows. */

    for (i__ = 1; i__ <= 3; ++i__) {
	ssized_(&c__1000, &xcover[(i__1 = i__ * 1006 - 1006) < 3018 && 0 <= 
		i__1 ? i__1 : s_rnge("xcover", i__1, "f_pckcov__", (ftnlen)
		123)]);
    }
    cleard_(&c__6, coeffs);
    for (i__ = 1; i__ <= 3; ++i__) {
	i__2 = nseg[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
		i__1, "f_pckcov__", (ftnlen)130)];
	for (j = 1; j <= i__2; ++j) {

/*           Create segments for frame I. */

	    if (i__ == 1) {

/*              Create NSEG(1) segments, each one separated */
/*              by a 1 second gap. */

		first = (j - 1) * 11.;
		last = first + 10.;
	    } else if (i__ == 2) {

/*              Create NSEG(2) segments, each one separated */
/*              by a 1 second gap.  This time, create the */
/*              segments in decreasing time order. */

		first = (nseg[1] - j) * 101.;
		last = first + 100.;
	    } else {

/*              I equals 3. */

/*              Create NSEG(3) segments with no gaps. */

		first = (j - 1) * 1e3;
		last = first + 1e3;
	    }

/*           Add to the expected coverage window for this frame. */

	    wninsd_(&first, &last, &xcover[(i__1 = i__ * 1006 - 1006) < 3018 
		    && 0 <= i__1 ? i__1 : s_rnge("xcover", i__1, "f_pckcov__",
		     (ftnlen)165)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           There's no need for the data to be complicated. Create */
/*           just one record per segment. */

	    coeffs[0] = 11.;
	    coeffs[1] = 21.;
	    coeffs[2] = 12.;
	    coeffs[3] = 22.;
	    coeffs[4] = 13.;
	    coeffs[5] = 23.;
	    intlen = last - first;
	    pckw02_(&handle, &frame[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 :
		     s_rnge("frame", i__1, "f_pckcov__", (ftnlen)184)], "J20"
		    "00", &first, &last, "TEST", &intlen, &c__1, &c__1, coeffs,
		     &first, (ftnlen)5, (ftnlen)4);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    pckcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Loop through the canned cases. */

    for (i__ = 1; i__ <= 3; ++i__) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Check coverage for frame #.", (ftnlen)80, (ftnlen)27);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	ssized_(&c__1000, cover);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pckcov_("pckcov.bpc", &frame[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 
		: s_rnge("frame", i__2, "f_pckcov__", (ftnlen)213)], cover, (
		ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check cardinality of coverage window. */

	i__1 = cardd_(cover);
	i__3 = cardd_(&xcover[(i__2 = i__ * 1006 - 1006) < 3018 && 0 <= i__2 ?
		 i__2 : s_rnge("xcover", i__2, "f_pckcov__", (ftnlen)219)]);
	chcksi_("CARDD(COVER)", &i__1, "=", &i__3, &c__0, ok, (ftnlen)12, (
		ftnlen)1);

/*        Check coverage window. */

	i__1 = cardd_(cover);
	chckad_("COVER", &cover[6], "=", &xcover[(i__2 = i__ * 1006 - 1000) < 
		3018 && 0 <= i__2 ? i__2 : s_rnge("xcover", i__2, "f_pckcov__"
		, (ftnlen)226)], &i__1, &c_b46, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Loop through the canned cases.  This time, use a coverage */
/*     window that already contains data. */

    scardd_(&c__0, cover);
    for (i__ = 1; i__ <= 3; ++i__) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Check coverage for frame #; COVER starts out non-empt"
		"y.", (ftnlen)80, (ftnlen)55);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	ssized_(&c__1000, cover);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	wninsd_(&c_b55, &c_b56, cover);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	pckcov_("pckcov.bpc", &frame[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 
		: s_rnge("frame", i__2, "f_pckcov__", (ftnlen)256)], cover, (
		ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check cardinality of coverage window. */

	wninsd_(&c_b55, &c_b56, &xcover[(i__2 = i__ * 1006 - 1006) < 3018 && 
		0 <= i__2 ? i__2 : s_rnge("xcover", i__2, "f_pckcov__", (
		ftnlen)262)]);
	i__1 = cardd_(cover);
	i__3 = cardd_(&xcover[(i__2 = i__ * 1006 - 1006) < 3018 && 0 <= i__2 ?
		 i__2 : s_rnge("xcover", i__2, "f_pckcov__", (ftnlen)264)]);
	chcksi_("CARDD(COVER)", &i__1, "=", &i__3, &c__0, ok, (ftnlen)12, (
		ftnlen)1);

/*        Check coverage window. */

	i__1 = cardd_(cover);
	chckad_("COVER", &cover[6], "=", &xcover[(i__2 = i__ * 1006 - 1000) < 
		3018 && 0 <= i__2 ? i__2 : s_rnge("xcover", i__2, "f_pckcov__"
		, (ftnlen)271)], &i__1, &c_b46, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Error cases: */


/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find coverage for a transfer PCK.", (ftnlen)40);
    txtopn_("pckcov.xsp", &xunit, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbt_("pckcov.bpc", &xunit, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cl__1.cerr = 0;
    cl__1.cunit = xunit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    pckcov_("pckcov.xsp", frame, cover, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDFORMAT)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find coverage for a CK.", (ftnlen)30);
    tstck3_("pckcov.bc", "pckcov.tsc", &c_false, &c_false, &c_false, &handle, 
	    (ftnlen)9, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcov_("pckcov.bc", frame, cover, (ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find coverage for an SPK.", (ftnlen)32);
    tstspk_("pckcov.bsp", &c_false, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcov_("pckcov.bsp", frame, cover, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find coverage for an EK.", (ftnlen)31);
    tstek_("pckcov.bes", &c__1, &c__20, &c_false, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckcov_("pckcov.bes", frame, cover, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDARCHTYPE)", ok, (ftnlen)22);
/* ****************************************************** */
/* ****************************************************** */
/* ****************************************************** */
/*     PCKFRM tests */
/* ****************************************************** */
/* ****************************************************** */
/* ****************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Find frames in our test PCK.", (ftnlen)28);
    ssizei_(&c__3, ids);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__3, xids);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	insrti_(&frame[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		"frame", i__2, "f_pckcov__", (ftnlen)358)], xids);
    }
    pckfrm_("pckcov.bpc", ids, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check cardinality of frame set. */

    i__2 = cardi_(ids);
    i__1 = cardi_(xids);
    chcksi_("CARDI(IDS)", &i__2, "=", &i__1, &c__0, ok, (ftnlen)10, (ftnlen)1)
	    ;

/*     Check frame set. */

    i__2 = cardi_(xids);
    chckai_("IDS", &ids[6], "=", &xids[6], &i__2, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find frames in our test PCK.  Start with non-empty ID set.", (
	    ftnlen)58);
    ssizei_(&c__4, ids);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__4, xids);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    insrti_(&c_b141, xids);
    for (i__ = 1; i__ <= 3; ++i__) {
	insrti_(&frame[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		"frame", i__2, "f_pckcov__", (ftnlen)393)], xids);
    }
    insrti_(&c_b141, ids);
    pckfrm_("pckcov.bpc", ids, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check cardinality of frame set. */

    i__2 = cardi_(ids);
    i__1 = cardi_(xids);
    chcksi_("CARDI(IDS)", &i__2, "=", &i__1, &c__0, ok, (ftnlen)10, (ftnlen)1)
	    ;

/*     Check frame set. */

    i__2 = cardi_(xids);
    chckai_("IDS", &ids[6], "=", &xids[6], &i__2, ok, (ftnlen)3, (ftnlen)1);

/*     Error cases: */


/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find frames for a transfer PCK.", (ftnlen)38);

/*     Initialize the IDS set. */

    ssizei_(&c__3, ids);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pckfrm_("pckcov.xsp", ids, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDFORMAT)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find frames for a CK.", (ftnlen)28);
    pckfrm_("pckcov.bc", ids, (ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find frames for an EK.", (ftnlen)29);
    pckfrm_("pckcov.bes", ids, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDARCHTYPE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Try to find frames for an SPK.", (ftnlen)30);
    pckfrm_("pckcov.bsp", ids, (ftnlen)10);
    chckxc_(&c_true, "SPICE(INVALIDFILETYPE)", ok, (ftnlen)22);

/*     Clean up. */

    delfil_("pckcov.bpc", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pckcov.xsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pckcov.bc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pckcov.bes", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("pckcov.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_pckcov__ */

