/*

-Procedure f_pckcov_c ( Test wrappers for PCK coverage routines )


-Abstract

   Perform tests on CSPICE wrappers for the PCK
   coverage routines.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <stdio.h>
   #include <string.h>
   #include <math.h>
   #include "SpiceUsr.h"
   #include "SpiceZmc.h"
   #include "SpiceZfc.h"
   #include "tutils_c.h"
   
   void f_pckcov_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This test family exercises the PCK coverage API wrappers

      pckcov_c
      pckfrm_c

   and the supporting PCK API wrappers

      pckcls_c
      pckopn_c
      pckw02_

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   N.J. Bachman    (JPL)

-Version

   -tspice_c Version 1.0.0 16-FEB-2017 (NJB)

       Previous version 1.0.0 16-DEC-2016 (NJB)

   
       Updated to use the CSPICE wrappers 
 
          pckcls_c
          pckopn_c
          pckw02_c
   

   -tspice_c Version 1.0.0 04-JAN-2004 (NJB)

-Index_Entries

  test PCK coverage routines

-&
*/

{ /* Begin f_pckcov_c */

   /*
   Local constants
   */
   #define CK              "pckcov.bc"
   #define EK              "pckcov.bes"
   #define PCK             "pckcov.bpc"
   #define SCLK            "pckcov.tsc"
   #define SPK             "pckcov.bsp"
   #define XFRPCK          "pckcov.xsp"

   #define DELTA            ( 1.e-6  )
   #define BIGTOL           ( 1.e-12 )
   #define MEDTOL           ( 1.e-14 )

   #define MAXCOV           10000
   #define WINSIZ           ( 2 * MAXCOV )

   #define FILSIZ           256
   #define LNSIZE           81
   #define NFRM             3
  

   /*
   Local variables
   */
   SPICEDOUBLE_CELL      ( cover,   WINSIZ );
   SPICEDOUBLE_CELL      ( xcover0, WINSIZ );
   SPICEDOUBLE_CELL      ( xcover1, WINSIZ );
   SPICEDOUBLE_CELL      ( xcover2, WINSIZ );

   SPICEINT_CELL         ( ids,     (NFRM+1) );
   SPICEINT_CELL         ( xids,    (NFRM+1) );

   static SpiceCell      * xcov     [ NFRM ] = { &xcover0,
                                                 &xcover1,
                                                 &xcover2 };
   SpiceChar               title    [ LNSIZE ];

   SpiceDouble             coeffs   [3][2];

   SpiceDouble             states   [2][6];
   SpiceDouble             first;
   SpiceDouble             intlen;
   SpiceDouble             last;

   static SpiceInt         frame     [ NFRM ]  =  { 4, 5, 6 };

   SpiceInt                badhan;
   SpiceInt                degree;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                j;
   static SpiceInt         nseg     [ NFRM ]  =  { 10, 20, 30 };
   SpiceInt                unit;
   SpiceInt                ncomc;
   SpiceInt                nrec;



   /*
   Local macros 
   */




   topen_c ( "f_pckcov_c" );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Setup:  create PCK file." );

   /*
   Create an PCK file with data for three bodies. 
   */
   ncomc = 0;
   pckopn_c ( PCK, PCK, ncomc, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


   j = 12;
   cleard_ ( &j, (SpiceDouble *)states );


   for ( i = 0;  i < NFRM;  i++  )
   {
      for ( j = 0;  j < nseg[i];  j++  )
      {
         /*
         Create segments for frame I.
         */
         if ( i == 0  )
         {
            /*
            Create nseg[0] segments, each one separated by a 1 second gap.
            */
            first = (j-1) * 11.0; 
            last  = first + 10.0; 
         } 

         else if ( i == 1 )
         {
            /*
            Create nseg[1] segments, each one separated
            by a 1 second gap.  This time, create the 
            segments in decreasing time order.
            */
            first = ( nseg[1] - j ) * 101.0;
            last  = first + 100.0;
         }

         else
         {
            /*
            i == 3 

            Create nseg[2] segments with no gaps.
            */
            first = (j-1) * 1000.0;
            last  = first + 1000.0;
         }

         /*
         Add to the expected coverage window for this frame.
         */
         wninsd_c ( first, last, xcov[i] );
         chckxc_c ( SPICEFALSE, " ", ok );


         /*
         There's no need for the data to be complicated. Create 
         just one record per segment.
         */
         coeffs[0][0] = 11.0;
         coeffs[0][1] = 21.0;

         coeffs[1][0] = 12.0;
         coeffs[1][1] = 22.0;

         coeffs[1][0] = 13.0;
         coeffs[1][1] = 23.0;


         nrec   = 1;
         degree = 1;
         intlen = last-first;

         pckw02_c ( handle, frame[i], "J2000",
                    first,  last,     "TEST",               intlen,
                    nrec,    degree,  (doublereal*) coeffs, first  );
         chckxc_c ( SPICEFALSE, " ", ok );

      }

   }

   pckcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   Find the available coverage in PCK for the frames in the
   frame array.

   Loop through the canned cases.
   */
   for ( i = 0;  i < NFRM;  i++  )
   {
      /*
      --- Case: ------------------------------------------------------
      */
   
      sprintf ( title, "Check coverage for frame %d", (int)i );
      tcase_c ( title );

      /*
      In this test, we empty out cover before using it. 
      */
      scard_c  ( 0,            &cover );
      pckcov_c ( PCK, frame[i], &cover );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check cardinality of coverage window. 
      */
      chcksi_c ( "card_c(&cover)",   card_c(&cover),   "=",
                 card_c(xcov[i]),    0,                ok   );

      /*
      Check coverage window. 
      */

      chckad_c ( "cover", 
                 (SpiceDouble *)cover.base, 
                 "=",
                 (SpiceDouble *)( (xcov[i])->base ),
                 card_c ( &cover ), 
                 0.0,
                 ok                                  );

   }



   /*
   Find the available coverage in PCK for the frames in the
   frame array.  This time, start with a non-empty coverage window.

   Loop through the canned cases.
   */
   for ( i = 0;  i < NFRM;  i++  )
   {
      /*
      --- Case: ------------------------------------------------------
      */
   
      sprintf ( title, "Check coverage for frame %d; cover "
                "starts out non-empty.",  (int)i            );
      tcase_c ( title );

      /*
      In this test, we put an interval into cover before using it. 
      */
      scard_c  ( 0,            &cover );
      wninsd_c ( 1.e6,  1.e7,  &cover );
      chckxc_c ( SPICEFALSE, " ", ok );

      pckcov_c ( PCK, frame[i], &cover );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Check cardinality of coverage window. 
      */
      wninsd_c ( 1.e6,  1.e7,  xcov[i] );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksi_c ( "card_c(&cover)",   card_c(&cover),   "=",
                 card_c(xcov[i]),    0,                ok   );

      /*
      Check coverage window. 
      */
      chckad_c ( "cover", 
                 (SpiceDouble *)cover.base, 
                 "=",
                 (SpiceDouble *)( (xcov[i])->base ),
                 card_c ( &cover ), 
                 0.0,
                 ok                                  );
   }


   /*
   Error cases 
   */

   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Error:  pckcov_c empty PCK name" );

   pckcov_c ( "", 1, &cover );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Error:  pckcov_c null PCK name" );

   pckcov_c ( NULLCPTR, 1, &cover );

   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find coverage for a transfer PCK." );

   txtopn_ ( (char    *) XFRPCK, 
             (integer *) &unit, 
             (ftnlen   ) strlen(XFRPCK) );

   chckxc_c ( SPICEFALSE, " ", ok );


   dafbt_  ( (char    *) PCK,
             (integer *) &unit,
             (ftnlen   ) strlen(PCK) );

   chckxc_c ( SPICEFALSE, " ", ok );

   ftncls_c ( unit );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckcov_c ( XFRPCK, 1, &cover );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFORMAT)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find coverage for a CK." );

   tstck3_c ( CK, SCLK, SPICEFALSE, SPICEFALSE, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckcov_c ( CK, frame[0], &cover );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFILETYPE)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find coverage for an EK." );

   tstek_c  ( EK, 0, 20, SPICEFALSE, &handle, ok );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckcov_c ( EK, frame[0], &cover );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARCHTYPE)", ok );


   
   /*
   ******************************************************
   ******************************************************
   ******************************************************
       PCKFRM tests
   ******************************************************
   ******************************************************
   ******************************************************
   */



   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Find frames in our test PCK." );

   for ( i = 0;  i < NFRM;  i++  )
   {
      insrti_c ( frame[i], &xids );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   pckfrm_c ( PCK, &ids );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check cardinality of coverage window. 
   */
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "card_c(&ids )",   card_c(&ids),   "=",
               card_c(&xids),    0,              ok   );

   /*
   Check coverage window. 
   */

   chckai_c ( "ids", 
              (SpiceInt *)( ids.base ), 
              "=",
              (SpiceInt *)( xids.base ),
              card_c ( &ids ), 
              ok                                  );


   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Find frames in our test PCK. Start with "
             "a non-empty ID set."                       );


   insrti_c ( -1.e6, &xids );

   for ( i = 0;  i < NFRM;  i++  )
   {
      insrti_c ( frame[i], &xids );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   insrti_c ( -1.e6, &ids );

   pckfrm_c ( PCK, &ids );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check cardinality of coverage window. 
   */
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "card_c(&ids )",   card_c(&ids),   "=",
               card_c(&xids),    0,              ok   );

   /*
   Check coverage window. 
   */

   chckai_c ( "ids", 
              (SpiceInt *)( ids.base ), 
              "=",
              (SpiceInt *)( xids.base ),
              card_c ( &ids ), 
              ok                                  );



   /*
   Error cases 
   */

   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Error:  pckfrm_c empty PCK name" );

   pckfrm_c ( "", &ids );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Error:  pckfrm_c null PCK name" );

   pckfrm_c ( NULLCPTR, &ids );

   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find IDS in a transfer format PCK." );
   pckfrm_c ( XFRPCK, &ids );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFORMAT)", ok ); 


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find IDS in a CK." );

   tstck3_c ( CK, SCLK, SPICEFALSE, SPICEFALSE, SPICEFALSE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckfrm_c ( CK, &ids );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDFILETYPE)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Try to find IDS in an EK." );

   pckfrm_c ( EK, &ids );
   chckxc_c ( SPICETRUE, "SPICE(INVALIDARCHTYPE)", ok );


   /*
   --- Case: ------------------------------------------------------
   */

   tcase_c ( "Error: pckcls_c: no segments in new PCK file." );

   removeFile ( PCK );
   pckopn_c ( PCK, PCK, 0, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckcls_c ( handle );
   
   chckxc_c ( SPICETRUE, "SPICE(NOSEGMENTSFOUND)", ok );
   
   /*
   Close file at DAF level. 
   */
   dafcls_c ( handle );

   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckopn_c: file exists." );

   pckopn_c ( PCK, PCK, 0, &badhan );
   chckxc_c ( SPICETRUE, "SPICE(FILEOPENFAIL)", ok );

   removeFile ( PCK );

   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckopn_c: empty input strings." );

   pckopn_c ( "", PCK, 0, &badhan );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   pckopn_c ( PCK, "", 0, &badhan );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckopn_c: null input strings." );

   pckopn_c ( NULL, PCK, 0, &badhan );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   pckopn_c ( PCK, NULL, 0, &badhan );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );



   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckw02_c: invalid handle." );

 
   pckw02_c ( 0,      frame[i], "J2000",
              first,  last,     "TEST",               intlen,
              nrec,   degree,   (doublereal*) coeffs, first  );


   chckxc_c ( SPICETRUE, "SPICE(DAFNOSUCHHANDLE)", ok );


   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckw02_c: empty input strings." );


   pckopn_c ( PCK, PCK, 0, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   pckw02_c ( handle, frame[i], "",
              first,  last,     "TEST",               intlen,
              nrec,   degree,   (doublereal*) coeffs, first  );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   pckw02_c ( handle, frame[i], "J2000",
              first,  last,     "",                   intlen,
              nrec,   degree,   (doublereal*) coeffs, first  );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /*
   --- Case: ------------------------------------------------------
   */
   tcase_c ( "Error: pckw02_c: null input strings." );


   pckw02_c ( handle, frame[i], NULL,
              first,  last,     "TEST",               intlen,
              nrec,   degree,   (doublereal*) coeffs, first  );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   pckw02_c ( handle, frame[i], "J2000",
              first,  last,     NULL,                 intlen,
              nrec,   degree,   (doublereal*) coeffs, first  );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   dafcls_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );





   /*
   Clean up. 
   */
   removeFile ( PCK );
   removeFile ( CK  );
   removeFile ( EK  );
   removeFile ( XFRPCK );

   t_success_c ( ok );
}
