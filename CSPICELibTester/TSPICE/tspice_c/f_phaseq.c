/* f_phaseq.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__100 = 100;
static integer c__0 = 0;
static integer c__5 = 5;
static integer c__1 = 1;
static doublereal c_b93 = 1e-9;
static doublereal c_b137 = 0.;
static doublereal c_b143 = 9.;
static doublereal c_b144 = 10.;

/* $Procedure F_PHASEQ ( PHASEQ tests ) */
/* Subroutine */ int f_phaseq__(logical *ok)
{
    /* Initialized data */

    static char rcorr[10*5] = "  nOne    " " lT       " "  Cn      " " Lt + "
	    "s   " "cN + S    ";
    static char xcorr[10*4] = " xlT      " "  xCn     " " XLt + s  " "XcN + "
	    "S   ";
    static char target[25*6] = "ALPHA                    " "ALPHA           "
	    "         " "X                        " "ALPHA                    "
	     "SUN                      " "ALPHA                    ";
    static char illumn[25*6] = "SUN                      " "X               "
	    "         " "SUN                      " "ALPHA                    "
	     "SUN                      " "SUN                      ";
    static char obsrvr[25*6] = "X                        " "BETA            "
	    "         " "BETA                     " "BETA                     "
	     "ALPHA                    " "SUN                      ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    doublereal tbeg, tend, phas;
    char targ[25];
    doublereal posa[3], posb[3], rlog, step;
    extern doublereal vsep_(doublereal *, doublereal *);
    doublereal work[530]	/* was [106][5] */;
    integer seed1, i__, j;
    extern /* Subroutine */ int zztstlsk_(char *, ftnlen), tcase_(char *, 
	    ftnlen), gfsep_(char *, char *, char *, char *, char *, char *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen), repmc_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen), repmi_(char *, char *, integer *, char *, ftnlen,
	     ftnlen, ftnlen);
    char illum[25];
    doublereal xphas;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    integer count;
    doublereal t1;
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    doublereal et;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int kclear_(void);
    doublereal cnfine[106];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     scardd_(integer *, doublereal *);
    char abcorr[25];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), delfil_(char *, ftnlen);
    extern integer wncard_(doublereal *);
    extern doublereal phaseq_(doublereal *, char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    doublereal v_phas__[5], adjust, et0, et1, refval, result[106];
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     furnsh_(char *, ftnlen), natspk_(char *, logical *, integer *, 
	    ftnlen), ssized_(integer *, doublereal *), wninsd_(doublereal *, 
	    doublereal *, doublereal *), wnfetd_(doublereal *, integer *, 
	    doublereal *, doublereal *), spkpos_(char *, doublereal *, char *,
	     char *, char *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    integer han;
    char obs[25];
    extern doublereal spd_(void), t_randd__(doublereal *, doublereal *, 
	    integer *);
    doublereal tol;
    char txt[124];

/* $ Abstract */

/*     This routine tests the SPICELIB routines */

/*        PHASEQ */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 09-AUG-2012 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Saved variables */


/*     Indices 1:3 for Invalid body name test, 4:6 for not distinct */
/*     body names test. */


/*     Begin every test family with an open call. */

    topen_("F_PHASEQ", (ftnlen)8);
    kclear_();
    seed1 = -54290018;

/*     Case 1: Create test kernels. */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.spk", &c_true, &han, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.spk", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an LSK, load using FURNSH. */

    zztstlsk_("phaseq.tls", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("phaseq.tls", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 2: Invalid body names. */


/*     Set appropriate values for ET, ABCORR. */

    et = 0.;
    s_copy(abcorr, rcorr, (ftnlen)25, (ftnlen)10);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(targ, target + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("target", i__1, "f_phaseq__", (ftnlen)283)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(illum, illumn + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("illumn", i__1, "f_phaseq__", (ftnlen)284)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_phaseq__", (ftnlen)285)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(txt, "Invalid body name test. TARG = #, ILLUM = #, OBS = #", (
		ftnlen)124, (ftnlen)52);
	repmc_(txt, "#", targ, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	repmc_(txt, "#", illum, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	repmc_(txt, "#", obs, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	tcase_(txt, (ftnlen)124);
	phas = phaseq_(&et, targ, illum, obs, abcorr, (ftnlen)25, (ftnlen)25, 
		(ftnlen)25, (ftnlen)25);
	chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    }

/*     Case 3: Invalid aberration corrections. */


/*     Set appropriate values for TARGET, ILLUMN, OBSRVR. */

    s_copy(targ, target, (ftnlen)25, (ftnlen)25);
    s_copy(illum, illumn, (ftnlen)25, (ftnlen)25);
    s_copy(obs, obsrvr + 25, (ftnlen)25, (ftnlen)25);
    for (i__ = 1; i__ <= 4; ++i__) {
	s_copy(abcorr, xcorr + ((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("xcorr", i__1, "f_phaseq__", (ftnlen)315)) * 10, (
		ftnlen)25, (ftnlen)10);
	s_copy(txt, "Invalid aberration correction. ABCOR = #", (ftnlen)124, (
		ftnlen)40);
	repmc_(txt, "#", abcorr, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	tcase_(txt, (ftnlen)124);
	phas = phaseq_(&et, targ, illum, obs, abcorr, (ftnlen)25, (ftnlen)25, 
		(ftnlen)25, (ftnlen)25);
	chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    }

/*     Case 4: Not distinct body names. */


/*     Set appropriate values for ABCORR. */

    s_copy(abcorr, rcorr, (ftnlen)25, (ftnlen)10);
    for (i__ = 4; i__ <= 6; ++i__) {
	s_copy(targ, target + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("target", i__1, "f_phaseq__", (ftnlen)341)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(illum, illumn + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("illumn", i__1, "f_phaseq__", (ftnlen)342)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_phaseq__", (ftnlen)343)) * 25, (
		ftnlen)25, (ftnlen)25);
	s_copy(txt, "Not distinct body name test. TARG = #, ILLUM = #, OBS ="
		" #", (ftnlen)124, (ftnlen)57);
	repmc_(txt, "#", targ, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	repmc_(txt, "#", illum, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	repmc_(txt, "#", obs, txt, (ftnlen)124, (ftnlen)1, (ftnlen)25, (
		ftnlen)124);
	tcase_(txt, (ftnlen)124);
	phas = phaseq_(&et, targ, illum, obs, abcorr, (ftnlen)25, (ftnlen)25, 
		(ftnlen)25, (ftnlen)25);
	chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    }

/*     Case 5: */

    tcase_("Separation angle vs phase angle - 1", (ftnlen)35);

/*     Calculate the phase angle for geometry with BETA as */
/*     the observer with ALPHA as the target. We perform */
/*     an angular separtion search for a geometry with an */
/*     identical result. This test requires use of NONE */
/*     correction. */


/*     Perform a simple search using ALPHA and BETA for times */
/*     of maximum angular separation as seen from the sun. */
/*     Recall ALPHA - BETA occultation occurs every day */
/*     at 12:00 PM TDB with correction NONE. */

    step = 60.;
    adjust = 0.;
    refval = 0.;
    ssized_(&c__100, cnfine);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Store the time bounds of our search interval in */
/*     the CNFINE confinement window. */

    str2et_("1999 DEC 31 21:00:00 TDB", &et0, (ftnlen)24);
    str2et_("2000 JAN 02 03:00:00 TDB", &et1, (ftnlen)24);
    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    gfsep_("SUN", "POINT", "IAU_SUN", "BETA", "POINT", "BETAFIXED", abcorr, 
	    "ALPHA", "ABSMAX", &refval, &adjust, &step, cnfine, &c__100, &
	    c__5, work, result, (ftnlen)3, (ftnlen)5, (ftnlen)7, (ftnlen)4, (
	    ftnlen)5, (ftnlen)9, (ftnlen)25, (ftnlen)5, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals in the result window. */

    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    wnfetd_(result, &c__1, &tbeg, &tend);
    spkpos_("SUN", &tbeg, "J2000", abcorr, "ALPHA", posa, &lt, (ftnlen)3, (
	    ftnlen)5, (ftnlen)25, (ftnlen)5);
    spkpos_("BETA", &tbeg, "J2000", abcorr, "ALPHA", posb, &lt, (ftnlen)4, (
	    ftnlen)5, (ftnlen)25, (ftnlen)5);
    s_copy(targ, "ALPHA", (ftnlen)25, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)25, (ftnlen)3);
    s_copy(obs, "BETA", (ftnlen)25, (ftnlen)4);
    phas = phaseq_(&tbeg, targ, illum, obs, abcorr, (ftnlen)25, (ftnlen)25, (
	    ftnlen)25, (ftnlen)25);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The angular separation should be within the ANGTOL */
/*     tolerance of the phase angle. */

    d__1 = vsep_(posa, posb);
    chcksd_("SEP vs PHASE", &d__1, "~", &phas, &c_b93, ok, (ftnlen)12, (
	    ftnlen)1);

/*     Case 6: */

    tcase_("Separation angle vs phase angle - 2", (ftnlen)35);

/*     Use ALPHA as the observer with BETA as the target. */
/*     The maximum value for this geometry phase angle is PI. */
/*     As before, perform an angular separtion search for a */
/*     geometry with an identical result. This test requires */
/*     use of NONE correction. */

    scardd_(&c__0, cnfine);
    wninsd_(&et0, &et1, cnfine);
    gfsep_("SUN", "POINT", "IAU_SUN", "ALPHA", "POINT", "ALPHAFIXED", abcorr, 
	    "BETA", "ABSMAX", &refval, &adjust, &step, cnfine, &c__100, &c__5,
	     work, result, (ftnlen)3, (ftnlen)5, (ftnlen)7, (ftnlen)5, (
	    ftnlen)5, (ftnlen)10, (ftnlen)25, (ftnlen)4, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals in the result window. */

    count = 0;
    count = wncard_(result);
    chcksi_("COUNT", &count, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    wnfetd_(result, &c__1, &tbeg, &tend);
    spkpos_("BETA", &tbeg, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)4, (
	    ftnlen)5, (ftnlen)25, (ftnlen)3);
    spkpos_("BETA", &tbeg, "J2000", abcorr, "ALPHA", posb, &lt, (ftnlen)4, (
	    ftnlen)5, (ftnlen)25, (ftnlen)5);
    s_copy(targ, "BETA", (ftnlen)25, (ftnlen)4);
    s_copy(illum, "SUN", (ftnlen)25, (ftnlen)3);
    s_copy(obs, "ALPHA", (ftnlen)25, (ftnlen)5);
    phas = phaseq_(&tbeg, targ, illum, obs, abcorr, (ftnlen)25, (ftnlen)25, (
	    ftnlen)25, (ftnlen)25);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The angular separation should be within the ANGTOL */
/*     tolerance of the phase angle, in this case, PI. */

    d__1 = vsep_(posa, posb);
    chcksd_("SEP vs PHASE", &d__1, "~", &phas, &c_b93, ok, (ftnlen)12, (
	    ftnlen)1);

/*     Case 7: */

/*     Specifically confirm the PHASEQ calculation returns numerically */
/*     different results for different valid aberration corrections with */
/*     all other arguments held constant. */

    for (i__ = 1; i__ <= 5; ++i__) {
	v_phas__[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge("v_phas", 
		i__1, "f_phaseq__", (ftnlen)505)] = phaseq_(&tbeg, targ, 
		illum, obs, rcorr + ((i__2 = i__ - 1) < 5 && 0 <= i__2 ? i__2 
		: s_rnge("rcorr", i__2, "f_phaseq__", (ftnlen)505)) * 10, (
		ftnlen)25, (ftnlen)25, (ftnlen)25, (ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 4; ++i__) {
	for (j = i__ - 1; j <= 5; ++j) {
	    if (i__ < j) {
		s_copy(txt, "Diff phase on diff abcorr, I = #, J = #", (
			ftnlen)124, (ftnlen)39);
		repmi_(txt, "#", &i__, txt, (ftnlen)124, (ftnlen)1, (ftnlen)
			124);
		repmi_(txt, "#", &j, txt, (ftnlen)124, (ftnlen)1, (ftnlen)124)
			;
		tcase_(txt, (ftnlen)124);
		chcksd_(txt, &v_phas__[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? 
			i__1 : s_rnge("v_phas", i__1, "f_phaseq__", (ftnlen)
			522)], "!=", &v_phas__[(i__2 = j - 1) < 5 && 0 <= 
			i__2 ? i__2 : s_rnge("v_phas", i__2, "f_phaseq__", (
			ftnlen)522)], &c_b137, ok, (ftnlen)124, (ftnlen)2);
	    }
	}
    }

/*     Case 8: */

/*     Confirm the expected property PHASE(t) = PHASE(t + tau*n) */
/*     for the orbit of BETA relative to ALPHA for all allowed */
/*     aberration corrections. */

/*     Recall ALPHA and BETA are in circular orbits. */

/*     In this case with BETA orbit period of 21 hours, and */
/*     ALPHA orbit period of 7 days, we can derive the period of */
/*     BETA with respect to ALPHA. */

/*        omega = 2pi/21hours.tosecs - 2pi/7days.tosecs */
/*        tau   = 2pi/omega */

/*     This gives tau = 86400secs. */

    for (i__ = 1; i__ <= 5; ++i__) {

/*        The time coverage for nat.spk is 1899 DEC 31 12:00:00.000 TDB */
/*        to 2100 JAN 01 12:00:00.000 TDB. This corresponds to */
/*        approximately -3.1*10^9 TDB seconds from J2000 to 3.1*10^9 */
/*        TDB seconds from J2000. Randomly select a positive time within */
/*        this interval as the test time. */

/*        10^9.477121254719663 ~ 3*10^9 */

	rlog = t_randd__(&c_b137, &c_b143, &seed1);
	t1 = pow_dd(&c_b144, &rlog);

/*        At T1 near 10^8, we loose of accuracy due to the use of */
/*        double precision evaluations, so reduce the tolerance by */
/*        an order of magnitude. T1 will never exceed 10^9 in this test. */

	if (t1 > 1e8) {
	    tol = 1e-8;
	} else {
	    tol = 1e-9;
	}
	xphas = phaseq_(&t1, targ, illum, obs, rcorr + ((i__1 = i__ - 1) < 5 
		&& 0 <= i__1 ? i__1 : s_rnge("rcorr", i__1, "f_phaseq__", (
		ftnlen)577)) * 10, (ftnlen)25, (ftnlen)25, (ftnlen)25, (
		ftnlen)10);
	for (j = 1; j <= 5; ++j) {
	    s_copy(txt, "PHASE(t) = PHASE(t + # * tau), CORR = #", (ftnlen)
		    124, (ftnlen)39);
	    i__1 = j * 10;
	    repmi_(txt, "#", &i__1, txt, (ftnlen)124, (ftnlen)1, (ftnlen)124);
	    repmc_(txt, "#", rcorr + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("rcorr", i__1, "f_phaseq__", (ftnlen)583)) *
		     10, txt, (ftnlen)124, (ftnlen)1, (ftnlen)10, (ftnlen)124)
		    ;
	    tcase_(txt, (ftnlen)124);
	    d__1 = t1 + j * spd_() * 10;
	    phas = phaseq_(&d__1, targ, illum, obs, rcorr + ((i__1 = i__ - 1) 
		    < 5 && 0 <= i__1 ? i__1 : s_rnge("rcorr", i__1, "f_phase"
		    "q__", (ftnlen)587)) * 10, (ftnlen)25, (ftnlen)25, (ftnlen)
		    25, (ftnlen)10);
	    chcksd_(txt, &phas, "~", &xphas, &tol, ok, (ftnlen)124, (ftnlen)1)
		    ;
	}
    }

/*     Case N: */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("phaseq.tls", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.spk", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_phaseq__ */

