/*

-Procedure f_phaseq_c ( Test phaseq_c )

-Abstract

   Perform tests on the CSPICE wrapper phaseq_c.

-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading

   None.

-Keywords

   TESTING

*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   doublereal t_randd__(doublereal *lower, doublereal *upper, integer *seed);

   void f_phaseq_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION
   --------  ---  --------------------------------------------------
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise..

-Detailed_Input

   None.

-Detailed_Output

   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.

-Parameters

   None.

-Exceptions

   Error free.

-Files

   None.

-Particulars

   This routine tests the wrapper for phaseq_c.

-Examples

   None.

-Restrictions

   None.

-Literature_References

   None.

-Author_and_Institution

   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)

-Version

   -tspice_c Version 1.1.0, 10-FEB-2017 (NJB)
 
       Removed unneeded declarations.       

   -tspice_c Version 1.0.0 09-MAY-2012 (EDW)

-Index_Entries

   test phaseq_c

-&
*/

{ /* Begin f_phaseq_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Constants
   */
   #define LSK             "phaseq.tls"
   #define SPK             "nat.bsp"
   #define PCK             "nat.pck"
   #define ANGTOL          1.e-9
   #define MAXWIN          100
   #define NRCORR          5
   #define NXCORR          4
   #define ABCLEN          25
   #define STRLEN          124

   SpiceInt                count;
   SpiceInt                han;
   SpiceInt                i     = 0;
   SpiceInt                j     = 0;
   SpiceInt                seed1 = -54290018;

   SpiceChar               abcorr [ABCLEN];
   SpiceChar               targ   [ABCLEN];
   SpiceChar               illum  [ABCLEN];
   SpiceChar               obs    [ABCLEN];
   SpiceChar               txt    [STRLEN];

   SpiceChar             * RCORR  [] = { "  nOne ",
                                        " lT",
                                        "  Cn",
                                        " Lt + s",
                                        "cN + S" };

   SpiceChar             * XCORR  [] = { " xlT",
                                        "  xCn",
                                        " XLt + s",
                                        "XcN + S"  };


   /*
   Indices 0:2 for Invalid body name test, 3:5 for not distinct
   body names test.
   */
   SpiceChar             * TARGET [] = { "ALPHA", "ALPHA", "X",
                                         "ALPHA", "SUN", "ALPHA"  };

   SpiceChar             * ILLUMN [] = { "SUN", "X",  "SUN",
                                         "ALPHA", "SUN", "SUN"  };

   SpiceChar             * OBSRVR [] = { "X", "BETA", "BETA",
                                         "BETA", "ALPHA", "SUN"  };

   SpiceDouble             adjust;
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             et;
   SpiceDouble             lt;
   SpiceDouble             phas;
   SpiceDouble             posa [3];
   SpiceDouble             posb [3];
   SpiceDouble             refval;
   SpiceDouble             rlog;
   SpiceDouble             step;
   SpiceDouble             t1;
   SpiceDouble             tbeg;
   SpiceDouble             tend;
   SpiceDouble             tol;
   SpiceDouble             v_phas [NRCORR];
   SpiceDouble             xphas;
   SpiceDouble             llim  = 0.;
   SpiceDouble             ulim  = 9.;

   SPICEDOUBLE_CELL      ( cnfine,   MAXWIN*2 );
   SPICEDOUBLE_CELL      ( result,   MAXWIN*2 );

   /*
   Local constants
   */
   logical      true_  = SPICETRUE;
   logical      false_ = SPICEFALSE;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_phaseq_c" );

   kclear_c();




   /*
   Case 1: Create test kernels.
   */
   tcase_c ( "Setup: create and load SPK, PCK, LSK files." );

   /*
   Leapseconds, load using FURNSH.
   */
   zztstlsk_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create the PCK for Nat"s Solar System.
   */
   natpck_( PCK, (logical *) &false_,
                  (logical *) &true_,
                  (ftnlen) strlen(PCK) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Create the SPK for Nat"s Solar System.
   */
   natspk_( SPK, (logical *) &false_, &han,
                  (ftnlen) strlen(SPK) );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   Case 2: Invalid body names.
   */

   /*
   Set appropriate values for ET, ABCORR.
   */
   et     = 0.0;
   repmc_c( "#", "#", RCORR[0], ABCLEN, abcorr  );

   for ( i=0; i<3 ; i++ )
      {

      repmc_c( "#", "#", TARGET[i], ABCLEN, targ  );
      repmc_c( "#", "#", ILLUMN[i], ABCLEN, illum );
      repmc_c( "#", "#", OBSRVR[i], ABCLEN, obs   );

      repmc_c( "$", "$",
               "Invalid body name test. TARG = #, ILLUM = #, OBS = #",
               STRLEN, txt  );

      repmc_c ( txt, "#", targ,  STRLEN, txt );
      repmc_c ( txt, "#", illum, STRLEN, txt );
      repmc_c ( txt, "#", obs,   STRLEN, txt );

      tcase_c (txt);

      phas = phaseq_c ( et, targ, illum, obs, abcorr );
      chckxc_c ( SPICETRUE, "SPICE(IDCODENOTFOUND)", ok );

      }




   /*
   Case 3: Invalid aberration corrections.
   */

   /*
   Set appropriate values for TARGET, ILLUMN, OBSRVR.
   */
   repmc_c( "#", "#", TARGET[0], ABCLEN, targ  );
   repmc_c( "#", "#", ILLUMN[0], ABCLEN, illum );
   repmc_c( "#", "#", OBSRVR[1], ABCLEN, obs   );

   for ( i=0; i<NXCORR ; i++ )
      {
      repmc_c( "#", "#", XCORR[i], ABCLEN, abcorr  );

      repmc_c( "$", "$",
               "Invalid aberration correction. ABCOR = #",
               STRLEN, txt  );

      repmc_c ( txt, "#", abcorr, STRLEN, txt );

      tcase_c (txt);

      phas = phaseq_c ( et, targ, illum, obs, abcorr );
      chckxc_c ( SPICETRUE, "SPICE(INVALIDOPTION)", ok );

      }




   /*
   Case 4: Not distinct body names.
   */

   /*
   Set appropriate values for ABCORR.
   */
   repmc_c( "#", "#", RCORR[0], ABCLEN, abcorr  );

   for ( i=3; i<6 ; i++ )
      {

      repmc_c( "#", "#", TARGET[i], ABCLEN, targ  );
      repmc_c( "#", "#", ILLUMN[i], ABCLEN, illum );
      repmc_c( "#", "#", OBSRVR[i], ABCLEN, obs   );

      repmc_c( "$", "$",
               "Not distinct body name test. TARG = #, ILLUM = #, OBS = #",
               STRLEN, txt  );

      repmc_c ( txt, "#", targ,  STRLEN, txt );
      repmc_c ( txt, "#", illum, STRLEN, txt );
      repmc_c ( txt, "#", obs,   STRLEN, txt );

      tcase_c (txt);

      phas = phaseq_c ( et, targ, illum, obs, abcorr );
      chckxc_c ( SPICETRUE, "SPICE(BODIESNOTDISTINCT)", ok );

      }




   /*
   Case 5:
   */
   tcase_c ( "Separation angle vs phase angle - 1" );

   /*
   Calculate the phase angle for geometry with BETA as
   the observer with ALPHA as the target. We perform
   an angular separtion search for a geometry with an
   identical result. This test requires use of NONE
   correction.
   */

   /*
   Perform a simple search using ALPHA and BETA for times
   of maximum angular separation as seen from the sun.
   Recall ALPHA - BETA occultation occurs every day
   at 12:00 PM TDB with correction NONE.
   */

   step   = 60.;
   adjust = 0.;
   refval = 0.;

   ssize_c ( MAXWIN, &cnfine );
   ssize_c ( MAXWIN, &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Store the time bounds of our search interval in
   the CNFINE confinement window.
   */
   str2et_c ( "1999 DEC 31 21:00:00 TDB", &et0 );
   str2et_c ( "2000 JAN 02 03:00:00 TDB", &et1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   scard_c ( 0, &cnfine);
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfsep_c ( "SUN",   "POINT", "IAU_SUN",
             "BETA",  "POINT", "BETAFIXED",
             abcorr,  "ALPHA", "ABSMAX",
             refval,  adjust, step, MAXWIN,
             &cnfine,  &result );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the number of intervals in the result window.
   */
   count = 0;
   count = wncard_c(&result);
   chcksi_c ( "count", count, "=", 1, 0, ok );

   wnfetd_c ( &result, 0, &tbeg, &tend  );

   spkpos_c ( "SUN",  tbeg, "J2000", abcorr, "ALPHA",
                                                    posa, &lt);
   spkpos_c ( "BETA", tbeg, "J2000", abcorr, "ALPHA",
                                                    posb, &lt);

   repmc_c( "#", "#", "ALPHA", ABCLEN, targ  );
   repmc_c( "#", "#", "SUN",   ABCLEN, illum );
   repmc_c( "#", "#", "BETA",  ABCLEN, obs   );

   phas = phaseq_c( tbeg, targ, illum, obs, abcorr );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   The angular separation should be within the ANGTOL
   tolerance of the phase angle.
   */
   chcksd_c ( "SEP vs PHASE", vsep_c(posa, posb), "~",
                                      phas, ANGTOL, ok );




   /*
   Case 6:
   */
   tcase_c ( "Separation angle vs phase angle - 2" );

   /*
   Use ALPHA as the observer with BETA as the target.
   The maximum value for this geometry phase angle is PI.
   As before, perform an angular separtion search for a
   geometry with an identical result. This test requires
   use of NONE correction.
   */

   scard_c ( 0, &cnfine);
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   gfsep_c ( "SUN",   "POINT", "IAU_SUN",
             "ALPHA", "POINT", "BETAFIXED",
             abcorr,  "BETA", "ABSMAX",
             refval,  adjust, step, MAXWIN,
             &cnfine,  &result );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the number of intervals in the result window.
   */
   count = 0;
   count = wncard_c(&result);
   chcksi_c ( "count", count, "=", 1, 0, ok );

   wnfetd_c ( &result, 0, &tbeg, &tend  );

   spkpos_c ( "BETA",  tbeg, "J2000", abcorr, "SUN",
                                                    posa, &lt);
   spkpos_c ( "BETA", tbeg, "J2000", abcorr, "ALPHA",
                                                    posb, &lt);

   repmc_c( "#", "#", "ALPHA", ABCLEN, targ  );
   repmc_c( "#", "#", "SUN",   ABCLEN, illum );
   repmc_c( "#", "#", "BETA",  ABCLEN, obs   );

   phas = phaseq_c( tbeg, targ, illum, obs, abcorr );
   chckxc_c ( SPICEFALSE, " ", ok );

   repmc_c( "#", "#", "BETA", ABCLEN, targ  );
   repmc_c( "#", "#", "SUN",   ABCLEN, illum );
   repmc_c( "#", "#", "ALPHA",  ABCLEN, obs   );

   phas = phaseq_c( tbeg, targ, illum, obs, abcorr );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   The angular separation should be within the ANGTOL
   tolerance of the phase angle, in this case, PI.
   */
   chcksd_c ( "SEP vs PHASE", vsep_c(posa, posb), "~",
                                      phas, ANGTOL, ok );

   chcksd_c ( "PHASE = PI", pi_c(), "~",
                                      phas, ANGTOL, ok );




   /*
   Case 7:

   Specifically confirm the phaseq_c calculation returns numerically
   different results for different valid aberration corrections with
   all other arguments held constant.
   */
   for ( i=0; i<NRCORR ; i++ )
      {
      v_phas[i] = phaseq_c ( tbeg, targ, illum, obs, RCORR[i] );
      chckxc_c ( SPICEFALSE, " ", ok );
      }

   for ( i=0; i<(NRCORR-1) ; i++ )
      {

      for ( j=i+1; j<NRCORR ; j++ )
         {

         if ( i < j )
            {
            repmc_c( "$", "$",
               "Diff phase on diff abcorr, I = #, J = #",
               STRLEN, txt  );

            repmi_c ( txt, "#", i, STRLEN, txt );
            repmi_c ( txt, "#", j, STRLEN, txt );

            tcase_c (txt);

            chcksd_c ( txt, v_phas[i], "!=", v_phas[j], 0., ok );
            }

         }

      }




   /*
   Case 8:

   Confirm the expected property PHASE(t) = PHASE(t + tau*n)
   for the orbit of BETA relative to ALPHA for all allowed
   aberration corrections.

   Recall ALPHA and BETA are in circular orbits.

   In this case with BETA orbit period of 21 hours, and
   ALPHA orbit period of 7 days, we can derive the period of
   BETA with respect to ALPHA.

      omega = 2pi/21hours.tosecs - 2pi/7days.tosecs
      tau   = 2pi/omega

   This gives tau = 86400secs.
   */
   for ( i=0; i<NRCORR ; i++ )
      {

      /*
      The time coverage for nat.spk is 1899 DEC 31 12:00:00.000 TDB
      to 2100 JAN 01 12:00:00.000 TDB. This corresponds to
      approximately -3.1*10^9 TDB seconds from J2000 to 3.1*10^9
      TDB seconds from J2000. Randomly select a positive time within
      this interval as the test time.

      10^9.477121254719663 ~ 3*10^9
      */
      rlog   = t_randd__( &llim, &ulim, &seed1);
      t1     = pow(10., rlog );

      /*
      At 't1' near 10^8, we loose accuracy due to the use of
      double precision evaluations, so reduce the tolerance by
      an order of magnitude. T1 will never exceed 10^9 in this test.
      */
      if ( t1 >  pow(10., 8) )
         {
         tol = ANGTOL *10.;
         }
      else
         {
         tol = ANGTOL;
         }


      xphas = phaseq_c ( t1, targ, illum, obs, RCORR[i] );
      chckxc_c ( SPICEFALSE, " ", ok );

      for ( j=0; j<5 ; j++ )
         {

         repmc_c( "$", "$",
                  "PHASE(t) = PHASE(t + # * tau), CORR = #",
                  STRLEN, txt  );

         repmi_c ( txt, "#", j*10,     STRLEN, txt );
         repmc_c ( txt, "#", RCORR[i], STRLEN, txt );

         tcase_c (txt);

         phas = phaseq_c ( t1 + j*spd_c()*10, targ,
                                            illum, obs, RCORR[i] );
         chcksd_c ( txt, phas, "~", xphas, tol, ok );

         }

      }




   /*
   Case n
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c();

   TRASH( LSK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   TRASH( SPK );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Retrieve the current test status.
   */
   t_success_c ( ok );


   } /* End f_phaseq_c */



