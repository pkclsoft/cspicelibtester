/* f_plt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__3000 = 3000;
static logical c_false = FALSE_;
static integer c__6000 = 6000;
static logical c_true = TRUE_;
static doublereal c_b45 = 1e-14;
static doublereal c_b53 = 100.;
static doublereal c_b54 = 200.;
static doublereal c_b55 = -400.;
static doublereal c_b124 = -.5;
static doublereal c_b125 = 0.;
static doublereal c_b127 = 1.;
static integer c__3 = 3;
static doublereal c_b147 = -1.;
static doublereal c_b149 = 3.;
static doublereal c_b150 = 2.;
static doublereal c_b154 = 4.;
static integer c__9 = 9;

/* $Procedure F_PLT ( Plate routine tests ) */
/* Subroutine */ int f_plt__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal);

    /* Local variables */
    static doublereal area;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal edge1[3], edge2[3];
    static integer pout[6006];
    static doublereal xvol, vout[3006], a, b, c__;
    static integer pout1[6006], i__, j;
    extern integer cardd_(doublereal *);
    static doublereal vout1[3006];
    extern /* Subroutine */ int zzpsxlat_(doublereal *, doublereal *, 
	    doublereal *);
    extern integer cardi_(integer *);
    static doublereal s, delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal xarea;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), movei_(integer *, integer *, integer *), vlcom_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static char title[255];
    extern doublereal pltar_(integer *, doublereal *, integer *, integer *);
    extern /* Subroutine */ int topen_(char *, ftnlen), vcrss_(doublereal *, 
	    doublereal *, doublereal *);
    static doublereal v1[3], v2[3], v3[3];
    extern /* Subroutine */ int t_success__(logical *), vlcom3_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static integer plats2[6000]	/* was [3][2000] */;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     cleard_(integer *, doublereal *);
    static integer np;
    extern /* Subroutine */ int cleari_(integer *, integer *), chcksd_(char *,
	     doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    static integer nv;
    static doublereal center[3], offset[3], normal[3];
    static integer plates[6000]	/* was [3][2000] */;
    extern /* Subroutine */ int vhatip_(doublereal *), ssized_(integer *, 
	    doublereal *);
    static doublereal vrtces[3000]	/* was [3][1000] */;
    extern /* Subroutine */ int ssizei_(integer *, integer *);
    static doublereal iverts[9]	/* was [3][3] */;
    extern /* Subroutine */ int pltnrm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), pltexp_(doublereal *, doublereal *, 
	    doublereal *);
    static doublereal xnorml[3];
    extern doublereal pltvol_(integer *, doublereal *, integer *, integer *);
    static doublereal overts[9]	/* was [3][3] */, xverts[9]	/* was [3][3] 
	    */, vol;
    extern /* Subroutine */ int zzpsbox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *);

/* $ Abstract */

/*     Exercise plate routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB plate routines that */
/*     are not covered by their own test families. These are: */

/*        PLTAR */
/*        PLTNRM */
/*        PLTVOL */
/*        PLTEXP */

/*     The plate routine */

/*        PLTNP */

/*     has its own, separate test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 24-OCT-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */

/*      DOUBLE PRECISION      TIGHT */
/*      PARAMETER           ( TIGHT  = 1.D-12 ) */

/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_PLT", (ftnlen)5);
/* ********************************************************************** */

/*     PLTAR tests */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTAR Error: too few plates", (ftnlen)255, (ftnlen)27);
    tcase_(title, (ftnlen)255);
    cleard_(&c__3000, vrtces);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__6000, plates);
    nv = 3;
    np = -1;
    area = pltar_(&nv, vrtces, &np, plates);
    chckxc_(&c_true, "SPICE(BADPLATECOUNT)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTAR Error: too few vertices", (ftnlen)255, (ftnlen)29);
    tcase_(title, (ftnlen)255);
    nv = 2;
    np = 1;
    area = pltar_(&nv, vrtces, &np, plates);
    chckxc_(&c_true, "SPICE(TOOFEWVERTICES)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTAR Error: bad vertex index in plate", (ftnlen)255, (
	    ftnlen)38);
    tcase_(title, (ftnlen)255);

/*     Initialize cells. */

    ssized_(&c__3000, vout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a plate set for a box. */

    a = 10.;
    b = 20.;
    c__ = 30.;
    zzpsbox_(&a, &b, &c__, vout, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    np = cardi_(pout) / 3;
    nv = cardd_(vout) / 3;

/*     Create a bad plate: first vertex index is 0. */

    i__1 = np * 3;
    movei_(&pout[6], &i__1, plats2);
    plats2[0] = 0;
    area = pltar_(&nv, vrtces, &np, plats2);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);

/*     Restore the original plate. It's simplest to copy the */
/*     plate set. Set another invalid vertex index. */

    i__1 = np * 3;
    movei_(&pout[6], &i__1, plats2);
    plats2[1] = 0;
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    i__2 = np * 3;
	    movei_(&pout[6], &i__2, plats2);
	    plats2[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("plats2", i__2, "f_plt__", (ftnlen)294)] = 0;
	    area = pltar_(&nv, vrtces, &np, plats2);
	    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
	    i__2 = np * 3;
	    movei_(&pout[6], &i__2, plats2);
	    plats2[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("plats2", i__2, "f_plt__", (ftnlen)300)] = nv + 1;
	    area = pltar_(&nv, vrtces, &np, plats2);
	    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTAR: Find area of a box centered at the origin.", (
	    ftnlen)255, (ftnlen)49);
    tcase_(title, (ftnlen)255);
    ssized_(&c__3000, vout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = 10.;
    b = 20.;
    c__ = 30.;
    zzpsbox_(&a, &b, &c__, vout, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nv = cardd_(vout) / 3;
    np = cardi_(pout) / 3;
    area = pltar_(&nv, &vout[6], &np, &pout[6]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xarea = (a * b + b * c__ + a * c__) * 2;
    chcksd_("AREA", &area, "~/", &xarea, &c_b45, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTAR: Find area of a box that excludes the origin.", (
	    ftnlen)255, (ftnlen)51);
    tcase_(title, (ftnlen)255);
    ssized_(&c__3000, vout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a vector by which to translate the vertices of the box. */

    vpack_(&c_b53, &c_b54, &c_b55, offset);

/*     Create a translated box. The plate set doesn't change. */

    zzpsxlat_(vout, offset, vout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compute the area of the translated box. */

    area = pltar_(&nv, &vout1[6], &np, &pout[6]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("AREA", &area, "~/", &xarea, &c_b45, ok, (ftnlen)4, (ftnlen)2);
/* ********************************************************************** */

/*     PLTVOL tests */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTVOL Error: too few plates", (ftnlen)255, (ftnlen)28);
    tcase_(title, (ftnlen)255);
    cleard_(&c__3000, vrtces);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__6000, plates);
    nv = 4;
    np = 3;
    area = pltvol_(&nv, vrtces, &np, plates);
    chckxc_(&c_true, "SPICE(TOOFEWPLATES)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTVOL Error: too few vertices", (ftnlen)255, (ftnlen)30);
    tcase_(title, (ftnlen)255);
    nv = 3;
    np = 4;
    area = pltvol_(&nv, vrtces, &np, plates);
    chckxc_(&c_true, "SPICE(TOOFEWVERTICES)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLVOL Error: bad vertex index in plate", (ftnlen)255, (
	    ftnlen)38);
    tcase_(title, (ftnlen)255);

/*     Initialize cells. */

    ssized_(&c__3000, vout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a plate set for a box. */

    a = 10.;
    b = 20.;
    c__ = 30.;
    zzpsbox_(&a, &b, &c__, vout, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    np = cardi_(pout) / 3;
    nv = cardd_(vout) / 3;

/*     Create a bad plate: first vertex index is 0. */

    i__1 = np * 3;
    movei_(&pout[6], &i__1, plats2);
    plats2[0] = 0;
    vol = pltvol_(&nv, vrtces, &np, plats2);
    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);

/*     Restore the original plate. It's simplest to copy the */
/*     plate set. Set another invalid vertex index. */

    i__1 = np * 3;
    movei_(&pout[6], &i__1, plats2);
    plats2[1] = 0;
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    i__2 = np * 3;
	    movei_(&pout[6], &i__2, plats2);
	    plats2[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("plats2", i__2, "f_plt__", (ftnlen)467)] = 0;
	    vol = pltvol_(&nv, vrtces, &np, plats2);
	    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
	    i__2 = np * 3;
	    movei_(&pout[6], &i__2, plats2);
	    plats2[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("plats2", i__2, "f_plt__", (ftnlen)473)] = nv + 1;
	    vol = pltvol_(&nv, vrtces, &np, plats2);
	    chckxc_(&c_true, "SPICE(INDEXOUTOFRANGE)", ok, (ftnlen)22);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTVOL: Find volume of a box centered at the origin.", (
	    ftnlen)255, (ftnlen)52);
    tcase_(title, (ftnlen)255);
    ssized_(&c__3000, vout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = 10.;
    b = 20.;
    c__ = 30.;
    zzpsbox_(&a, &b, &c__, vout, pout);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nv = cardd_(vout) / 3;
    np = cardi_(pout) / 3;
    vol = pltvol_(&nv, &vout[6], &np, &pout[6]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xvol = a * b * c__;
    chcksd_("VOL", &vol, "~/", &xvol, &c_b45, ok, (ftnlen)3, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTVOL: Find volume of a box that excludes the origin.", (
	    ftnlen)255, (ftnlen)54);
    ssized_(&c__3000, vout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssizei_(&c__6000, pout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a vector by which to translate the vertices of the box. */

    vpack_(&c_b53, &c_b54, &c_b55, offset);

/*     Create a translated box. The plate set doesn't change. */

    zzpsxlat_(vout, offset, vout1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compute the volume of the translated box. */

    vol = pltvol_(&nv, &vout[6], &np, &pout[6]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xvol = a * b * c__;
    chcksd_("VOL", &vol, "~/", &xvol, &c_b45, ok, (ftnlen)3, (ftnlen)2);
/* ********************************************************************** */

/*     PLTNRM tests */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "PLTNRM: Compute an upward normal of an equilateral triang"
	    "le lying in the X-Y plane and centered at the origin.", (ftnlen)
	    255, (ftnlen)110);
    tcase_(title, (ftnlen)255);
    s = sqrt(3.) / 2;
    vpack_(&s, &c_b124, &c_b125, v1);
    vpack_(&c_b125, &c_b127, &c_b125, v2);
    d__1 = -s;
    vpack_(&d__1, &c_b124, &c_b125, v3);
    pltnrm_(v1, v2, v3, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compute an expected normal vector. */

    vsub_(v2, v1, edge1);
    vsub_(v3, v2, edge2);
    vcrss_(edge1, edge2, xnorml);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("NORMAL", normal, "~~/", xnorml, &c__3, &c_b45, ok, (ftnlen)6, (
	    ftnlen)3);

/*     Convert normal to unit length and compare to a known vector. */

    vpack_(&c_b125, &c_b125, &c_b127, xnorml);
    vhatip_(normal);
    chckad_("NORMAL (unit)", normal, "~~/", xnorml, &c__3, &c_b45, ok, (
	    ftnlen)13, (ftnlen)3);
/* ********************************************************************** */

/*     PLTEXP tests */

/* ********************************************************************** */
    s_copy(title, "PLTEXP: expand a plate that is parallel to, but not conta"
	    "ined in, the X-Y plane.", (ftnlen)255, (ftnlen)80);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b147, &c_b147, &c_b149, iverts);
    vpack_(&c_b150, &c_b147, &c_b149, &iverts[3]);
    vpack_(&c_b150, &c_b154, &c_b149, &iverts[6]);
    delta = 1.;
    pltexp_(iverts, &delta, overts);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare the expected output. */

    s = .33333333333333331;
    vlcom3_(&s, iverts, &s, &iverts[3], &s, &iverts[6], center);
    for (i__ = 1; i__ <= 3; ++i__) {
	vsub_(&iverts[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"iverts", i__1, "f_plt__", (ftnlen)633)], center, offset);
	vlcom_(&c_b127, center, &c_b150, offset, &xverts[(i__1 = i__ * 3 - 3) 
		< 9 && 0 <= i__1 ? i__1 : s_rnge("xverts", i__1, "f_plt__", (
		ftnlen)635)]);
    }
    chckad_("OVERTS", overts, "~~/", xverts, &c__9, &c_b45, ok, (ftnlen)6, (
	    ftnlen)3);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_plt__ */

