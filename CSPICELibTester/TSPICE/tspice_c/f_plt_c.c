/*

-Procedure f_plt_c ( Plate utility tests )

 
-Abstract
 
   Exercise the CSPICE wrappers for the plate utilities.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_plt_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrappers

      pltar_c
      pltexp_c
      pltnp_c
      pltnrm_c
      pltvol_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

-Index_Entries

   test plate utility routines

-&
*/

{ /* Begin f_plt_c */

 
   /*
   Constants
   */
   #define LNSIZE          256
   #define MAXV            1000
   #define MAXP            ( 2 * MAXV )
   #define VTIGHT          1.0e-14

   /*
   Local variables
   */
   SPICEDOUBLE_CELL        ( vout,  3*MAXV );
   SPICEDOUBLE_CELL        ( vout1, 3*MAXV );
   SPICEINT_CELL           ( pout,  3*MAXP );

   SpiceChar               title  [ LNSIZE ];

   SpiceDouble             a;
   SpiceDouble             area;
   SpiceDouble             b;
   SpiceDouble             c;
   SpiceDouble             center [3];
   SpiceDouble             delta;
   SpiceDouble             dist;
   SpiceDouble             edge1  [3];
   SpiceDouble             edge2  [3];
   SpiceDouble             iverts [3][3];
   SpiceDouble             normal [3];
   SpiceDouble             offset [3];
   SpiceDouble             overts [3][3];
   SpiceDouble             point  [3];
   SpiceDouble             pnear  [3];
   SpiceDouble             s;
   SpiceDouble             v1     [3];
   SpiceDouble             v2     [3];
   SpiceDouble             v3     [3];
   SpiceDouble             vol;
   SpiceDouble             vrtces [MAXV][3];
   SpiceDouble             xarea;
   SpiceDouble             xdist;   
   SpiceDouble             xnorml [3];
   SpiceDouble             xpnear [3];
   SpiceDouble             xverts [3][3];
   SpiceDouble             xvol;

   SpiceInt                i;
   SpiceInt                np;
   SpiceInt                nv;
   SpiceInt                plates [MAXP][3];

   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_plt_c" );
   

   /*
   *********************************************************************
   *
   *
   *   pltar_c tests
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "pltar_c error: too few plates." );

   nv =  3;
   np = -1;

   area = pltar_c( nv, vrtces, np, plates );
   chckxc_c ( SPICETRUE, "SPICE(BADPLATECOUNT)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "pltar_c error: too few vertices." );

   nv =  2;
   np =  1;


   area = pltar_c( nv, vrtces, np, plates );
   chckxc_c ( SPICETRUE, "SPICE(TOOFEWVERTICES)", ok );

 

   /* 
   ---- Case ---------------------------------------------------------
   */ 
   tcase_c ( "pltar_c:  Find area of a box centered at the origin." );


   a = 10.0;
   b = 20.0;
   c = 30.0;

   zzpsbox_c ( a, b, c, &vout, &pout );
   chckxc_c ( SPICEFALSE, " ", ok );

   nv   = card_c(&vout) / 3;
   np   = card_c(&pout) / 3;

   area = pltar_c ( nv, vout.data, np, pout.data );
   chckxc_c ( SPICEFALSE, " ", ok );

   xarea = 2 * (  ( a * b ) + ( b * c ) + ( a * c )  );

   chcksd_c ( "area", area, "~/", xarea, VTIGHT, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */ 
   tcase_c ( "pltar_c:  Find area of a box that excludes the origin." );


   /*
   Create a vector by which to translate the vertices of the box.
   */
   vpack_c ( 100.0, 200.0, -400.0, offset );

   /*
   Create a translated box. The plate set doesn't change. 
   */
   zzpsxlat_c ( &vout, offset, &vout1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compute the area of the translated box.   
   */
   area = pltar_c ( nv, vout1.data, np, pout.data );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "area", area, "~/", xarea, VTIGHT, ok );
   

   /*
   *********************************************************************
   *
   *
   *   pltvol_c tests
   *
   *
   *********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "pltvol_c error: too few plates." );

   nv =  4;
   np =  3;

   area = pltvol_c( nv, vrtces, np, plates );
   chckxc_c ( SPICETRUE, "SPICE(TOOFEWPLATES)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "pltvol_c error: too few vertices." );

   nv =  3;
   np =  4;

   area = pltvol_c( nv, vrtces, np, plates );
   chckxc_c ( SPICETRUE, "SPICE(TOOFEWVERTICES)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Find volume of a box centered at the origin." );

   a = 10.0;
   b = 20.0;
   c = 30.0;

   zzpsbox_c ( a, b, c, &vout, &pout );
   chckxc_c  ( SPICEFALSE, " ", ok );

   nv  = card_c(&vout) / 3;
   np  = card_c(&pout) / 3;

   vol = pltvol_c ( nv, vout.data, np, pout.data );
   chckxc_c  ( SPICEFALSE, " ", ok );

   xvol = a * b *c;

   chcksd_c ( "vol", vol, "~/", xvol, VTIGHT, ok );

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Find volume of a box that excludes the origin." );
   

   /*
   Create a vector by which to translate the vertices of the box.
   */
   vpack_c ( 100.0, 200.0, -400.0, offset );

   /*
   Create a translated box. The plate set doesn't change. 
   */
   zzpsxlat_c ( &vout, offset, &vout1 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compute the volume of the translated box.   
   */
   vol = pltvol_c ( nv, vout1.data, np, pout.data );
   chckxc_c ( SPICEFALSE, " ", ok );

   xvol = a * b * c;
   
   chcksd_c ( "vol", vol, "~/", xvol, VTIGHT, ok );




   /*
   *********************************************************************
   *
   *
   *   pltnrm_c tests
   *
   *
   *********************************************************************
   */

   strncpy ( title, 
             "Compute an upward normal of an equilateral "
             "triangle lying in the X-Y plane and centered "
             "at the origin.",
             LNSIZE                                         );

   tcase_c ( title );


   s = sqrt(3.0) / 2;

   vpack_c (   s,  -0.5,  0.0,  v1 );
   vpack_c ( 0.0,   1.0,  0.0,  v2 );
   vpack_c (  -s,  -0.5,  0.0,  v3 );

   pltnrm_c ( v1, v2, v3, normal );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compute an expected normal vector. 
   */
   vsub_c ( v2, v1, edge1 );
   vsub_c ( v3, v2, edge2 );

   vcrss_c ( edge1, edge2, xnorml );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "normal", normal, "~~/", xnorml, 3, VTIGHT, ok );

   /*
   Convert normal to unit length and compare to a known vector.
   */
   vpack_c ( 0.0, 0.0, 1.0, xnorml );

   vhat_c ( normal, normal );

   chckad_c ( "normal (unit)", normal, "~~/", xnorml, 3, VTIGHT, ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 

   /*
   Note that pltnrm_c is error-free. 
   */



   /*
   *********************************************************************
   *
   *
   *   pltexp_c tests
   *
   *
   *********************************************************************
   */

   tcase_c ( "pltexp_c: expand a plate that is parallel to, but not "
             "contained in, the X-Y plane."                          );


   vpack_c ( -1.0, -1.0, 3.0, iverts[0] );
   vpack_c (  2.0, -1.0, 3.0, iverts[1] );
   vpack_c (  2.0,  4.0, 3.0, iverts[2] );

   delta = 1.0;

   pltexp_c ( iverts, delta, overts );
   /*
   Check for an error, for safety. No error is supposed to ever
   be signaled by pltexp_c.
   */
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Prepare the expected output. 
   */
   s = 1.0 / 3.0;

   vlcom3_c ( s, iverts[0], s, iverts[1], s, iverts[2], center );

   for ( i = 0;  i < 3;  i++ )
   {
      vsub_c ( iverts[i], center, offset );

      vlcom_c ( 1.0, center, 2.0, offset, xverts[i] );
   }

 
   chckad_c ( "overts", (SpiceDouble *)overts, "~~/", 
                        (SpiceDouble *)xverts, 9,     VTIGHT, ok );


   /*
   Note that pltexp_c is error-free. 
   */


   /*
   *********************************************************************
   *
   *
   *   pltnp_c tests
   *
   *
   *********************************************************************
   */



   tcase_c ( "pltnp_c: find the nearest point to the origin on "
             "a plate that is parallel to, but not "
             "contained in, the X-Y plane."                          );


   cleard_c ( 3, point );

   vpack_c ( -1.0, -1.0, 3.0, iverts[0] );
   vpack_c (  2.0, -1.0, 3.0, iverts[1] );
   vpack_c (  2.0,  4.0, 3.0, iverts[2] );


   pltnp_c ( point, iverts[0], iverts[1], iverts[2], pnear, &dist );

   /*
   Check for an error, for safety. No error is supposed to ever
   be signaled by pltnp_c.
   */
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Prepare the expected output. 
   */
   vpack_c ( 0.0, 0.0, 3.0, xpnear );

   xdist = 3.0;

 
   chckad_c ( "pnear", (SpiceDouble *)pnear,  "~~/", 
                       (SpiceDouble *)xpnear, 3,     VTIGHT, ok );

   chcksd_c ( "dist", dist, "~/", xdist, VTIGHT, ok );
   

   t_success_c( ok );
   
} /* End f_plt_c */

