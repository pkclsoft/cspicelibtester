/* f_pltnp.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 3.;
static doublereal c_b5 = 4.;
static doublereal c_b6 = 5.;
static doublereal c_b7 = 0.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b32 = 6.;
static doublereal c_b33 = 8.;
static doublereal c_b34 = 10.;
static doublereal c_b35 = .5;
static doublereal c_b138 = -1.;
static doublereal c_b141 = 1.;
static doublereal c_b162 = -2.;
static doublereal c_b186 = 2.;
static doublereal c_b197 = -4.;
static doublereal c_b219 = -3.;
static doublereal c_b258 = -.5;
static doublereal c_b339 = .9;
static doublereal c_b349 = .1;
static doublereal c_b360 = -.1;
static doublereal c_b405 = 1e4;
static doublereal c_b407 = -1e4;
static doublereal c_b416 = -17.;
static doublereal c_b417 = 13.;
static doublereal c_b418 = 23.;
static integer c__2 = 2;
static integer c__14 = 14;

/* $Procedure F_PLTNP ( PLTNP tests ) */
/* Subroutine */ int f_pltnp__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };
    static integer pred[3] = { 3,1,2 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double sqrt(doublereal);
    integer s_rnge(char *, integer, char *, integer);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    static doublereal edge[9]	/* was [3][3] */;
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    ), vhat_(doublereal *, doublereal *);
    static doublereal dist;
    extern doublereal vsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), eul2m_(doublereal *, 
	    doublereal *, doublereal *, integer *, integer *, integer *, 
	    doublereal *);
    static integer i__;
    static doublereal l, r__, s, angle, scale, uedge[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal pnear[3];
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer 
	    *, char *, ftnlen, ftnlen, ftnlen), vlcom_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static char title[255];
    static integer altix;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    static doublereal point[3], vtemp[3], xdist, xform[9]	/* was [3][3] 
	    */;
    extern /* Subroutine */ int pltnp_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, doublereal *), ucrss_(doublereal *,
	     doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *);
    static doublereal v1[3], v2[3], v3[3];
    extern /* Subroutine */ int t_success__(logical *), vrotv_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), vlcom3_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    , chcksd_(char *, doublereal *, char *, doublereal *, doublereal *
	    , logical *, ftnlen, ftnlen), chckxc_(logical *, char *, logical *
	    , ftnlen);
    static doublereal edgnml[9]	/* was [3][3] */, shapes[45]	/* was [3][3][
	    5] */, offset[3], normal[3], xshape[9]	/* was [3][3] */;
    static integer scalix, shapix;
    static doublereal xpnear[3];
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *), nplnpt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal dir[3], alt;
    extern doublereal rpd_(void);
    static doublereal sep, tol;
    static integer rix, uix, vix;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     Exercise the plate-point near point routine PLTNP. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKLIB type 2 point-plate distance */
/*     routine PLTNP. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 18-AUG-2015 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_PLTNP", (ftnlen)7);
/* ********************************************************************** */

/*     Non-error exceptional cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a single point.", (ftnlen)255, (
	    ftnlen)41);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vequ_(v1, v2);
    vequ_(v1, v3);
    vpack_(&c_b7, &c_b7, &c_b6, point);
    vequ_(v1, xpnear);
    xdist = 5.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a single point. Input point coi"
	    "ncides with plate.", (ftnlen)255, (ftnlen)75);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vequ_(v1, v2);
    vequ_(v1, v3);
    vequ_(v1, point);
    vequ_(v1, xpnear);
    xdist = 0.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);
    chckad_("PNEAR", pnear, "~~", xpnear, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V1 an"
	    "d V2.", (ftnlen)255, (ftnlen)62);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vpack_(&c_b32, &c_b33, &c_b34, v2);
    vlcom_(&c_b35, v1, &c_b35, v2, v3);
    vpack_(&c_b7, &c_b7, &c_b6, point);
    vequ_(v1, xpnear);
    xdist = 5.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V1 an"
	    "d V2. Input point is interior of segment. ", (ftnlen)255, (ftnlen)
	    99);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vpack_(&c_b32, &c_b33, &c_b34, v2);
    vequ_(v2, v3);
    vlcom_(&c_b35, v1, &c_b35, v2, point);
    vequ_(point, xpnear);
    xdist = 0.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V1 an"
	    "d V3.", (ftnlen)255, (ftnlen)62);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vpack_(&c_b32, &c_b33, &c_b34, v3);
    vlcom_(&c_b35, v1, &c_b35, v3, v2);
    vpack_(&c_b7, &c_b7, &c_b6, point);
    vequ_(v1, xpnear);
    xdist = 5.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V1 an"
	    "d V3. Input point is interior of segment. ", (ftnlen)255, (ftnlen)
	    99);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v1);
    vpack_(&c_b32, &c_b33, &c_b34, v3);
    vlcom_(&c_b35, v1, &c_b35, v3, v2);
    vlcom_(&c_b35, v1, &c_b35, v2, point);
    vequ_(point, xpnear);
    xdist = 0.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V2 an"
	    "d V3.", (ftnlen)255, (ftnlen)62);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v2);
    vpack_(&c_b32, &c_b33, &c_b34, v3);
    vlcom_(&c_b35, v2, &c_b35, v3, v1);
    vpack_(&c_b7, &c_b7, &c_b6, point);
    vequ_(v2, xpnear);
    xdist = 5.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate case: plate is a line segment bounded by V2 an"
	    "d V3. Input point is interior of segment. ", (ftnlen)255, (ftnlen)
	    99);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b5, &c_b6, v2);
    vpack_(&c_b32, &c_b33, &c_b34, v3);
    vlcom_(&c_b35, v2, &c_b35, v3, v1);
    vlcom_(&c_b35, v2, &c_b35, v3, point);
    vequ_(point, xpnear);
    xdist = 0.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */


/*     Simple tests with easily verified results: */



/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: plate is in X-Y plane; point is on +Z axis.",
	     (ftnlen)255, (ftnlen)57);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b138, &c_b138, &c_b7, v1);
    vpack_(&c_b141, &c_b138, &c_b7, v2);
    vpack_(&c_b7, &c_b141, &c_b7, v3);
    vpack_(&c_b7, &c_b7, &c_b4, point);
    vpack_(&c_b7, &c_b7, &c_b7, xpnear);
    xdist = 3.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     The following cases use the triangle defined above. */


/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is below (in the -Y sense) the bottom edge of the pla"
	    "te.", (ftnlen)255, (ftnlen)124);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b7, &c_b162, &c_b4, point);
    vpack_(&c_b7, &c_b138, &c_b7, xpnear);
    xdist = sqrt(10.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is below the bottom edge of the plate and shifted in "
	    "the -X direction.", (ftnlen)255, (ftnlen)138);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b162, &c_b162, &c_b4, point);
    vequ_(v1, xpnear);
    xdist = sqrt(11.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is below the bottom edge of the plate and shifted in "
	    "the +X direction.", (ftnlen)255, (ftnlen)138);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b186, &c_b162, &c_b4, point);
    vequ_(v2, xpnear);
    xdist = sqrt(11.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense) the bottom edge of the pla"
	    "te but below outward normal to the left edge emanating from the "
	    "lower left corner.", (ftnlen)255, (ftnlen)203);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b197, &c_b7, &c_b4, point);
    vequ_(v1, xpnear);
    xdist = sqrt(19.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense) the bottom edge of the pla"
	    "te but below outward normal to the right edge emanating from the"
	    " lower right corner.", (ftnlen)255, (ftnlen)205);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b5, &c_b7, &c_b4, point);
    vequ_(v2, xpnear);
    xdist = sqrt(19.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is on the outward normal to the left edge emanating f"
	    "rom the lower left corner.", (ftnlen)255, (ftnlen)147);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b219, &c_b7, &c_b4, point);
    vequ_(v1, xpnear);
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is on the outward normal to the right edge emanating "
	    "from the lower right corner.", (ftnlen)255, (ftnlen)149);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b4, &c_b7, &c_b4, point);
    vequ_(v2, xpnear);
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense) the outward normal to the "
	    "left edge emanating from the lower left corner.", (ftnlen)255, (
	    ftnlen)168);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b35, &c_b141, &c_b7, offset);
    vadd_(v1, offset, xpnear);
    vpack_(&c_b138, &c_b35, &c_b7, normal);
    vlcom3_(&c_b141, v1, &c_b141, offset, &c_b186, normal, point);
    point[2] = 3.;
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense) the outward normal to the "
	    "right edge emanating from the lower right corner.", (ftnlen)255, (
	    ftnlen)170);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b258, &c_b141, &c_b7, offset);
    vadd_(v2, offset, xpnear);
    vpack_(&c_b141, &c_b35, &c_b7, normal);
    vlcom3_(&c_b141, v2, &c_b141, offset, &c_b186, normal, point);
    point[2] = 3.;
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is on the outward normal to the left edge emanating f"
	    "rom the top (in the +Y sense) vertex.", (ftnlen)255, (ftnlen)158);
    tcase_(title, (ftnlen)255);
    vequ_(v3, xpnear);
    vpack_(&c_b138, &c_b35, &c_b7, normal);
    vlcom_(&c_b141, v3, &c_b186, normal, point);
    point[2] = 3.;
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is on the outward normal to the right edge emanating "
	    "from the top (in the +Y sense) vertex.", (ftnlen)255, (ftnlen)159)
	    ;
    tcase_(title, (ftnlen)255);
    vequ_(v3, xpnear);
    vpack_(&c_b141, &c_b35, &c_b7, normal);
    vlcom_(&c_b141, v3, &c_b186, normal, point);
    point[2] = 3.;
    xdist = sqrt(14.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense)the outward normal to the l"
	    "eft edge emanating from the top (in the +Y sense) vertex.", (
	    ftnlen)255, (ftnlen)178);
    tcase_(title, (ftnlen)255);
    vequ_(v3, xpnear);
    vpack_(&c_b138, &c_b35, &c_b7, normal);
    vlcom_(&c_b141, v3, &c_b186, normal, point);
    point[1] += 1.;
    point[2] = 3.;
    xdist = sqrt(17.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense)the outward normal to the r"
	    "ight edge emanating from the top (in the +Y sense) vertex.", (
	    ftnlen)255, (ftnlen)179);
    tcase_(title, (ftnlen)255);
    vequ_(v3, xpnear);
    vpack_(&c_b141, &c_b35, &c_b7, normal);
    vlcom_(&c_b141, v3, &c_b186, normal, point);
    point[1] += 1.;
    point[2] = 3.;
    xdist = sqrt(17.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Exterior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the +Y sense)the top vertex.", (ftnlen)
	    255, (ftnlen)109);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b7, &c_b4, &c_b4, point);
    vequ_(v3, xpnear);
    xdist = sqrt(13.);
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     "Interior" cases follow. For these cases, the projection */
/*     of the point onto the plane containing the plate is a point */
/*     on the plate. */


/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Interior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is below (in the -Y sense)the top vertex.", (ftnlen)
	    255, (ftnlen)109);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b7, &c_b339, &c_b4, point);
    vequ_(point, xpnear);
    xpnear[2] = 0.;
    xdist = 3.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Interior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the -Y sense)and to the right of the low"
	    "er left vertex.", (ftnlen)255, (ftnlen)136);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b349, &c_b349, &c_b4, offset);
    vadd_(v1, offset, point);
    vequ_(point, xpnear);
    xpnear[2] = 0.;
    xdist = 3.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Interior case: plate is in X-Y plane; X-Y plane projectio"
	    "n of point is above (in the -Y sense)and to the left of the lowe"
	    "r right vertex.", (ftnlen)255, (ftnlen)136);
    tcase_(title, (ftnlen)255);
    vpack_(&c_b360, &c_b349, &c_b4, offset);
    vadd_(v2, offset, point);
    vequ_(point, xpnear);
    xpnear[2] = 0.;
    xdist = 3.;
    pltnp_(point, v1, v2, v3, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);



/*     Systematic tests: */




/* --- Case -------------------------------------------------------- */

    s_copy(title, "Setup", (ftnlen)255, (ftnlen)5);
    tcase_(title, (ftnlen)255);

/*     Create basic shapes. All shapes are variants of triangles. */

/*        Shape 1 is an acute triangle. */

    vpack_(&c_b138, &c_b138, &c_b7, shapes);
    vpack_(&c_b141, &c_b138, &c_b7, &shapes[3]);
    vpack_(&c_b7, &c_b141, &c_b7, &shapes[6]);

/*        Shape 2 is a right triangle. */

    vpack_(&c_b138, &c_b138, &c_b7, &shapes[9]);
    vpack_(&c_b141, &c_b138, &c_b7, &shapes[12]);
    vpack_(&c_b138, &c_b141, &c_b7, &shapes[15]);

/*        Shape 3 is an obtuse triangle. */

    vpack_(&c_b197, &c_b138, &c_b7, &shapes[18]);
    vpack_(&c_b5, &c_b138, &c_b7, &shapes[21]);
    vpack_(&c_b7, &c_b141, &c_b7, &shapes[24]);

/*        Shape 4 is a needle-like, acute triangle. */

    vpack_(&c_b138, &c_b138, &c_b7, &shapes[27]);
    vpack_(&c_b141, &c_b138, &c_b7, &shapes[30]);
    vpack_(&c_b7, &c_b405, &c_b7, &shapes[33]);

/*        Shape 5 is an almost flat, obtuse triangle. */

    vpack_(&c_b407, &c_b138, &c_b7, &shapes[36]);
    vpack_(&c_b405, &c_b138, &c_b7, &shapes[39]);
    vpack_(&c_b7, &c_b141, &c_b7, &shapes[42]);

/*     Create an offset vector and a transformation; we'll use */
/*     these to move the shapes away from the origin and from */
/*     the X-Y plane. */

    vpack_(&c_b416, &c_b417, &c_b418, offset);
    d__1 = rpd_() * 30.;
    d__2 = rpd_() * -10.;
    d__3 = rpd_() * 70.;
    eul2m_(&d__1, &d__2, &d__3, &c__3, &c__2, &c__3, xform);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */


/*     Loop over the triangles. */

    for (shapix = 1; shapix <= 5; ++shapix) {

/*        Loop over the scales. */

	for (scalix = 1; scalix <= 8; ++scalix) {

/*           Translate and transform the triangle. */

	    for (i__ = 1; i__ <= 3; ++i__) {
		vadd_(offset, &shapes[(i__1 = (i__ + shapix * 3) * 3 - 12) < 
			45 && 0 <= i__1 ? i__1 : s_rnge("shapes", i__1, "f_p"
			"ltnp__", (ftnlen)1021)], vtemp);
		mxv_(xform, vtemp, &xshape[(i__1 = i__ * 3 - 3) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("xshape", i__1, "f_pltnp__", (
			ftnlen)1022)]);
	    }
	    d__1 = (scalix - 4) * 4.;
	    scale = pow_dd(&c_b34, &d__1);

/*           Scale the triangle. */

	    for (i__ = 1; i__ <= 3; ++i__) {
		vsclip_(&scale, &xshape[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 
			? i__1 : s_rnge("xshape", i__1, "f_pltnp__", (ftnlen)
			1033)]);
	    }

/*           Compute the triangle's edges. */

	    vsub_(&xshape[3], xshape, edge);
	    vsub_(&xshape[6], &xshape[3], &edge[3]);
	    vsub_(xshape, &xshape[6], &edge[6]);

/*           Compute a normal to the plane of the triangle. Pick the */
/*           normal so the vertices are ordered in the positive sense */
/*           about the normal. */

	    ucrss_(edge, &edge[3], normal);

/*           Compute the outward normal vectors for the edges, where the */
/*           normal vectors lie in the plane of the triangle. */

	    ucrss_(edge, normal, edgnml);
	    ucrss_(&edge[3], normal, &edgnml[3]);
	    ucrss_(&edge[6], normal, &edgnml[6]);

/*           We're going to create points, the distance of which from */
/*           the current triangle is to be computed. */

/*           Loop over the point's height values. */

	    for (altix = 1; altix <= 5; ++altix) {
		d__1 = (doublereal) (altix - 2);
		alt = scale * pow_dd(&c_b34, &d__1);

/*              Loop over the triangles' vertices. */

		for (vix = 1; vix <= 3; ++vix) {

/*                 Compute the angular separation between the */
/*                 rays emanating from vertex VIX in the directions */
/*                 of the outward normal to edge VIX and the */
/*                 normal to edge PRED(VIX). We'll use this later. */

		    sep = vsep_(&edgnml[(i__1 = vix * 3 - 3) < 9 && 0 <= i__1 
			    ? i__1 : s_rnge("edgnml", i__1, "f_pltnp__", (
			    ftnlen)1078)], &edgnml[(i__3 = pred[(i__2 = vix - 
			    1) < 3 && 0 <= i__2 ? i__2 : s_rnge("pred", i__2, 
			    "f_pltnp__", (ftnlen)1078)] * 3 - 3) < 9 && 0 <= 
			    i__3 ? i__3 : s_rnge("edgnml", i__3, "f_pltnp__", 
			    (ftnlen)1078)]);

/*                 Compute the length of the edge between vertex */
/*                 VIX and vertex NEXT(VIX). We'll use this later. */

		    l = vnorm_(&edge[(i__1 = vix * 3 - 3) < 9 && 0 <= i__1 ? 
			    i__1 : s_rnge("edge", i__1, "f_pltnp__", (ftnlen)
			    1083)]);
		    for (uix = 0; uix <= 5; ++uix) {

/*                    UIX controls the angular separation of the point */
/*                    from the normal rays emanating from vertex VIX. */

			angle = sep * ((doublereal) uix / 5.);
			vrotv_(&edgnml[(i__2 = pred[(i__1 = vix - 1) < 3 && 0 
				<= i__1 ? i__1 : s_rnge("pred", i__1, "f_plt"
				"np__", (ftnlen)1093)] * 3 - 3) < 9 && 0 <= 
				i__2 ? i__2 : s_rnge("edgnml", i__2, "f_pltn"
				"p__", (ftnlen)1093)], normal, &angle, dir);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			for (rix = 1; rix <= 6; ++rix) {

/* --- Case -------------------------------------------------------- */


/*                       Create a point in the plane of the triangle, */
/*                       lying in the sector having apex at vertex VIX */
/*                       and bounded by rays emanating from the apex in */
/*                       directions EDGNML(1,VIX) and */
/*                       EDGNML(1,PRED(VIX)). For such a point, the */
/*                       nearest point on the triangle is the vertex */
/*                       itself. We'll then add an "altitude" to the */
/*                       point. */

/*                       In the case immediately below, RIX controls the */
/*                       distance of the point from the vertex. */

			    d__1 = (rix - 3) * 2.;
			    r__ = scale * pow_dd(&c_b34, &d__1);
			    s_copy(title, "Near point should be vertex #: SH"
				    "APIX = #; SCALIX = #; ALTIX = #; VIX = #"
				    "; UIX = #, RIX = #; R   = #; ALT = #.", (
				    ftnlen)255, (ftnlen)110);
			    repmi_(title, "#", &vix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &shapix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &scalix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &altix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &vix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &uix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &rix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &r__, &c__14, title, (ftnlen)
				    255, (ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &alt, &c__14, title, (ftnlen)
				    255, (ftnlen)1, (ftnlen)255);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    tcase_(title, (ftnlen)255);

/*                       Create the point. */

			    vlcom3_(&c_b141, &xshape[(i__1 = vix * 3 - 3) < 9 
				    && 0 <= i__1 ? i__1 : s_rnge("xshape", 
				    i__1, "f_pltnp__", (ftnlen)1141)], &r__, 
				    dir, &alt, normal, point);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Compute the near point on the triangle and */
/*                       the distance between this point and POINT. */

			    pltnp_(point, xshape, &xshape[3], &xshape[6], 
				    pnear, &dist);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Set tolerances based on the numerical */
/*                       difficulty of the case. */

			    if (shapix <= 3) {
				tol = 1e-12;
			    } else {
				tol = 1e-5;
			    }
			    xdist = vdist_(&xshape[(i__1 = vix * 3 - 3) < 9 &&
				     0 <= i__1 ? i__1 : s_rnge("xshape", i__1,
				     "f_pltnp__", (ftnlen)1167)], point);
			    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (
				    ftnlen)4, (ftnlen)2);
			    if (shapix <= 3) {
				if (rix <= 3) {
				    tol = 1e-12;
				} else {
				    tol = 1e-9;
				}
			    } else {
				tol = 1e-5;
			    }
			    chckad_("PNEAR", pnear, "~~/", &xshape[(i__1 = 
				    vix * 3 - 3) < 9 && 0 <= i__1 ? i__1 : 
				    s_rnge("xshape", i__1, "f_pltnp__", (
				    ftnlen)1187)], &c__3, &tol, ok, (ftnlen)5,
				     (ftnlen)3);

/* --- Case -------------------------------------------------------- */


/*                       Test points whose near points lie on the */
/*                       edge between vertices VIX and NEXT(VIX). */

/*                       We can use the values of ALT and R we've */
/*                       already computed. R will be the distance */
/*                       from the edge of the projection of the */
/*                       point onto the plane of the triangle. */

/*                       S will replace ANGLE in this computation: S */
/*                       will be the distance from the outward normal */
/*                       ray of the edge, emanating from vertex VIX, */
/*                       of the projection of the point onto the plane */
/*                       of the triangle. */

			    s = l * ((doublereal) uix / 5.);
			    s_copy(title, "Near point should be on the edge:"
				    " SHAPIX = #; SCALIX = #; ALTIX = #; VIX "
				    "= #; UIX = #, RIX = #; R   = #; S = #; A"
				    "LT = #.", (ftnlen)255, (ftnlen)120);
			    repmi_(title, "#", &shapix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &scalix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &altix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &vix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &uix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &rix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &r__, &c__14, title, (ftnlen)
				    255, (ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &s, &c__14, title, (ftnlen)255,
				     (ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &alt, &c__14, title, (ftnlen)
				    255, (ftnlen)1, (ftnlen)255);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    tcase_(title, (ftnlen)255);

/*                       Generate the point. */

/*                       We'll need the unit vector in the direction */
/*                       of the edge. */

			    vhat_(&edge[(i__1 = vix * 3 - 3) < 9 && 0 <= i__1 
				    ? i__1 : s_rnge("edge", i__1, "f_pltnp__",
				     (ftnlen)1235)], uedge);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Create the point. */

			    vlcom3_(&c_b141, &xshape[(i__1 = vix * 3 - 3) < 9 
				    && 0 <= i__1 ? i__1 : s_rnge("xshape", 
				    i__1, "f_pltnp__", (ftnlen)1241)], &r__, &
				    edgnml[(i__2 = vix * 3 - 3) < 9 && 0 <= 
				    i__2 ? i__2 : s_rnge("edgnml", i__2, 
				    "f_pltnp__", (ftnlen)1241)], &s, uedge, 
				    vtemp);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    vlcom_(&c_b141, vtemp, &alt, normal, point);

/*                       Compute the near point on the triangle and */
/*                       the distance between this point and POINT. */

			    pltnp_(point, xshape, &xshape[3], &xshape[6], 
				    pnear, &dist);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       The expected near point lies on the line */
/*                       containing the edge. */

			    nplnpt_(&xshape[(i__1 = vix * 3 - 3) < 9 && 0 <= 
				    i__1 ? i__1 : s_rnge("xshape", i__1, 
				    "f_pltnp__", (ftnlen)1263)], uedge, point,
				     xpnear, &xdist);

/*                       Set tolerances based on the numerical */
/*                       difficulty of the case. */

			    if (shapix <= 3) {
				if (rix <= 3 && altix <= 2) {
				    tol = 1e-9;
				} else {
				    tol = 1e-5;
				}
			    } else {
				tol = 1e-5;
			    }
			    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (
				    ftnlen)4, (ftnlen)2);
			    if (shapix <= 3) {
				if (rix <= 3 && altix <= 2) {
				    tol = 1e-12;
				} else {
				    tol = 1e-5;
				}
			    } else {
				tol = 1e-5;
			    }
			    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &
				    tol, ok, (ftnlen)5, (ftnlen)3);

/* --- Case -------------------------------------------------------- */


/*                       Test points whose near points lie in the */
/*                       interior of the triangle. */

/*                       We can use the values of ALT we've already */
/*                       computed. We'll create the projection of the */
/*                       point using linear combinations of the vertices. */

			    s_copy(title, "Near point should be in the inter"
				    "ior of the triangle: SHAPIX = #; SCALIX "
				    "= #; ALTIX = #; VIX = #; UIX = #, RIX = "
				    "#; ALT = #. ", (ftnlen)255, (ftnlen)125);
			    repmi_(title, "#", &shapix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &scalix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &altix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &vix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &uix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmi_(title, "#", &rix, title, (ftnlen)255, (
				    ftnlen)1, (ftnlen)255);
			    repmd_(title, "#", &alt, &c__14, title, (ftnlen)
				    255, (ftnlen)1, (ftnlen)255);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    tcase_(title, (ftnlen)255);

/*                       S ranges from 0 to 1. */

			    s = (doublereal) uix / 5.;

/*                       Make R range from 0 to 1. */

			    r__ = (doublereal) (rix - 1) / 5.;

/*                       Generate the projection of the point. This */
/*                       is the expected near point. Use a convex */
/*                       combination of the vertices. */

			    d__1 = 1. - s;
			    d__2 = s * (1. - r__);
			    d__3 = s * r__;
			    vlcom3_(&d__1, &xshape[(i__1 = vix * 3 - 3) < 9 &&
				     0 <= i__1 ? i__1 : s_rnge("xshape", i__1,
				     "f_pltnp__", (ftnlen)1359)], &d__2, &
				    xshape[(i__3 = next[(i__2 = vix - 1) < 3 
				    && 0 <= i__2 ? i__2 : s_rnge("next", i__2,
				     "f_pltnp__", (ftnlen)1359)] * 3 - 3) < 9 
				    && 0 <= i__3 ? i__3 : s_rnge("xshape", 
				    i__3, "f_pltnp__", (ftnlen)1359)], &d__3, 
				    &xshape[(i__5 = pred[(i__4 = vix - 1) < 3 
				    && 0 <= i__4 ? i__4 : s_rnge("pred", i__4,
				     "f_pltnp__", (ftnlen)1359)] * 3 - 3) < 9 
				    && 0 <= i__5 ? i__5 : s_rnge("xshape", 
				    i__5, "f_pltnp__", (ftnlen)1359)], xpnear)
				    ;

/*                       Generate the point. */

			    vlcom_(&c_b141, xpnear, &alt, normal, point);

/*                       Compute the near point on the triangle and */
/*                       the distance between this point and POINT. */

			    pltnp_(point, xshape, &xshape[3], &xshape[6], 
				    pnear, &dist);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       The expected distance is just the altitude */
/*                       of the point with respect to the plane */
/*                       containing the triangle. */

			    xdist = alt;

/*                       Set tolerances based on the numerical */
/*                       difficulty of the case. */

			    if (shapix <= 3) {
				if (rix <= 3 && altix <= 2) {
				    tol = 1e-9;
				} else {
				    tol = 1e-5;
				}
			    } else {
				tol = 1e-5;
			    }
			    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (
				    ftnlen)4, (ftnlen)2);
			    if (shapix <= 3) {
				if (rix <= 3 && altix <= 2) {
				    tol = 1e-12;
				} else {
				    tol = 1e-5;
				}
			    } else {
				tol = 1e-5;
			    }
			    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &
				    tol, ok, (ftnlen)5, (ftnlen)3);
			}

/*                    End of "RIX" loop. */

		    }

/*                 End of "UIX" loop. */

		}

/*              End of vertex loop. */

	    }

/*           End of altitude loop. */

	}

/*        End of scale loop. */

    }

/*     End of shape loop. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_pltnp__ */

