/*

-Procedure f_pxfrm2_c ( Test pxfrm2_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper pxfrm2_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"

   void f_pxfrm2_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for pxfrm2_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   S.C. Krening    (JPL)
 
-Version
 
   -tspice_c Version 1.1.0, 10-FEB-2017 (NJB)
 
       Removed unneeded declarations.       

   -test_pxfrm2 Version 1.0.0 13-OCT-2011 (SCK) 

-Index_Entries

   test pxfrm2_c

-&
*/

{ /* Begin f_pxfrm2_c */

   /*
   Constants
   */
   #define TXTPCK          "test.pck"

   /*
   Local variables
   */

   SpiceBoolean            found_earth ;
   SpiceBoolean            found_mars ;

   SpiceDouble             etfrom ;
   SpiceDouble             etto ;
   SpiceDouble             JF      [3][3] ;
   SpiceDouble             TJ      [3][3] ;
   SpiceDouble             TF      [3][3] ;
   SpiceDouble             temp    [3][3] ;
   SpiceDouble             xform   [3][3] ;

   SpiceInt                code_earth ;
   SpiceInt                code_mars  ;

   

   /*
   Begin every test family with an open call.
   */ 
   topen_c ( "f_pxfrm2_c" );


   /*
   ------ Case -------------------------------------------------------
   */

   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Create and load a PCK file. Delete the file afterward.
   */
   tstpck_c ( TXTPCK, SPICETRUE, SPICETRUE );

   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test pxfrm2.  Apply to the IAU Mars and Earth frames "
             "through J2000; compare with tipbod results."   );

   etfrom  = - 100000000.;
   etto    = - 100000010.; 

   bodn2c_c ( "MARS",  &code_mars,  &found_mars);
   bodn2c_c ( "EARTH", &code_earth, &found_earth );

   pxfrm2_c ( "IAU_MARS", "IAU_EARTH", etfrom, etto, xform );
   
   /*
     `tipbod_c' returns the rotation from inertial to body
     coordinates.  We want JF to be from body to J2000 coordinates.
   */
   tipbod_c ( "J2000", code_mars, etfrom, temp );
   xpose_c  ( temp, JF );

   tipbod_c ( "J2000", code_earth, etto, TJ );

   mxm_c    ( TJ, JF, TF );
   chckxc_c ( SPICEFALSE, " ", ok );

   chckad_c ( "tipm", 
              (SpiceDouble *)xform,  
              "~",  
              (SpiceDouble *)TF,  
              9,  
              1.e-14, 
              ok                     );

   /*
   Check pxfrm2_c string error cases:
   
      1) Null from string.
      2) Empty from string.
      3) Null to string.
      4) Empty to string.
      
   */
   pxfrm2_c ( NULLCPTR, "IAU_EARTH", etfrom, etto, xform );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   pxfrm2_c ( "", "IAU_EARTH", etfrom, etto, xform );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
            
   pxfrm2_c ( "J2000", NULLCPTR, etfrom, etto, xform );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   pxfrm2_c ( "J2000", "", etfrom, etto, xform );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );
   
   
} /* End f_pxfrm2_c */


