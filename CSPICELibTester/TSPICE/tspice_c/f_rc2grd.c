/* f_rc2grd.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c_b4 = 1000000;
static integer c__2000 = 2000;
static logical c_false = FALSE_;
static integer c__0 = 0;
static real c_b32 = 0.f;
static logical c_true = TRUE_;
static integer c__2 = 2;
static integer c_n1 = -1;

/* $Procedure F_RC2GRD ( RC2GRD tests ) */
/* Subroutine */ int f_rc2grd__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer seed;
    static logical grid[1000000];
    static integer nrec, vset[1000006];
    static doublereal bnds1[20002]	/* was [2][10001] */, bnds2[20002]	
	    /* was [2][10001] */;
    static integer xmap1[2000], xmap2[2000];
    static doublereal h__;
    static integer i__, j, k, l;
    extern integer cardd_(doublereal *);
    static integer m;
    static char label[80];
    static doublereal w;
    extern /* Subroutine */ int tcase_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *);
    static logical value;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer ncols;
    static char title[320];
    static integer c1, c2;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer n1, n2;
    static doublereal mapbd1, mapbd2;
    static integer nrows;
    extern /* Subroutine */ int t_success__(logical *);
    static integer mapco1, mapco2;
    extern /* Subroutine */ int rc2grd_(integer *, doublereal *, doublereal *,
	     integer *, integer *, logical *, integer *, integer *, integer *,
	     integer *, integer *, integer *, integer *, integer *, logical *)
	    ;
    static doublereal coset1[2006], coset2[2006];
    static integer civor1[2000], civor2[2000], pxmap1[2000], pxmap2[2000], 
	    xcivo1[2000], xcivo2[2000];
    static doublereal pxmin1, pxmin2, pxmax1, pxmax2;
    extern /* Subroutine */ int chckai_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksd_(char *, doublereal 
	    *, char *, doublereal *, real *, logical *, ftnlen, ftnlen), 
	    chckxc_(logical *, char *, logical *, ftnlen), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), validd_(integer *, integer *, doublereal *), chcksl_(
	    char *, logical *, logical *, logical *, ftnlen);
    static logical includ;
    static integer ncover, mrkset[1000006], xncols;
    static logical xvalue;
    extern /* Subroutine */ int ssizei_(integer *, integer *);
    static integer tmpset[1000006], xnrows, col;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static integer row;
    static doublereal mid1, mid2;
    static integer ord1[2000], ord2[2000];

/* $ Abstract */

/*     Exercise the compressed inverse order vector routine IOVCMP. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKBRIEF supporting routine RC2GRD. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 31-JAN-2017 (NJB) */

/* -& */

/*     TESTUTIL functions */


/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_RC2GRD", (ftnlen)8);
/* ********************************************************************** */

/*     RC2GRD normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Simple case: two disjoint rectangles, common Y bounds", (
	    ftnlen)320, (ftnlen)53);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);

/*     Note that the output grid should have a column corresponding to */
/*     the horizontal gap between the rectangles. */

    xnrows = 1;
    xncols = 3;
    bnds1[0] = 1.;
    bnds1[1] = 3.;
    bnds1[2] = 4.;
    bnds1[3] = 5.;
    bnds2[0] = -1.;
    bnds2[1] = 1.;
    bnds2[2] = -1.;
    bnds2[3] = 1.;
    nrec = 2;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output grid dimensions. We're looking for a 1x3 grid. */

    chcksi_("NROWS", &nrows, "=", &xnrows, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOLS", &ncols, "=", &xncols, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the inverse order vectors. First, create the */
/*     expected vectors. */

    i__ = 1;
    i__1 = xncols + 1;
    for (col = 1; col <= i__1; ++col) {
	k = (col - 1) / 2 + 1;
	j = col - (k - 1 << 1);
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    xcivo1[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo1", i__3, "f_rc2grd__", (ftnlen)291)] = col;
	    ++i__;
	}
    }
    i__1 = xncols + 1;
    chckai_("CIVOR1", civor1, "=", xcivo1, &i__1, ok, (ftnlen)6, (ftnlen)1);
    i__ = 1;
    i__1 = xncols + 1;
    for (col = 1; col <= i__1; ++col) {
	k = (col - 1) / 2 + 1;
	j = col - (k - 1 << 1);
	i__2 = xnrows + 1;
	for (row = 1; row <= i__2; ++row) {
	    xcivo2[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo2", i__3, "f_rc2grd__", (ftnlen)312)] = row;
	    ++i__;
	}
    }
    i__1 = xnrows + 1;
    chckai_("CIVOR2", civor2, "=", xcivo2, &i__1, ok, (ftnlen)6, (ftnlen)1);

/*     Check the pixel maps. */

    i__1 = xncols + 1;
    for (col = 1; col <= i__1; ++col) {
	s_copy(label, "Grid(1,*) coord 1", (ftnlen)80, (ftnlen)17);
	repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	mapco1 = pxmap1[(i__2 = col - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"pxmap1", i__2, "f_rc2grd__", (ftnlen)332)];
	j = (mapco1 - 1) / 2 + 1;
	i__ = mapco1 - (j - 1 << 1);
	mapbd1 = bnds1[(i__2 = i__ + (j << 1) - 3) < 20002 && 0 <= i__2 ? 
		i__2 : s_rnge("bnds1", i__2, "f_rc2grd__", (ftnlen)338)];
	j = (col - 1) / 2 + 1;
	i__ = col - (j - 1 << 1);
	chcksd_(label, &mapbd1, "=", &bnds1[(i__2 = i__ + (j << 1) - 3) < 
		20002 && 0 <= i__2 ? i__2 : s_rnge("bnds1", i__2, "f_rc2grd__"
		, (ftnlen)344)], &c_b32, ok, (ftnlen)80, (ftnlen)1);
    }
    i__1 = xnrows + 1;
    for (row = 1; row <= i__1; ++row) {
	s_copy(label, "Grid(*,1) coord 2", (ftnlen)80, (ftnlen)17);
	repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	mapco2 = pxmap2[(i__2 = row - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"pxmap2", i__2, "f_rc2grd__", (ftnlen)353)];
	j = (mapco2 - 1) / 2 + 1;
	i__ = mapco2 - (j - 1 << 1);
	mapbd2 = bnds2[(i__2 = i__ + (j << 1) - 3) < 20002 && 0 <= i__2 ? 
		i__2 : s_rnge("bnds2", i__2, "f_rc2grd__", (ftnlen)359)];
	j = (row - 1) / 2 + 1;
	i__ = row - (j - 1 << 1);
	chcksd_(label, &mapbd2, "=", &bnds2[(i__2 = i__ + (j << 1) - 3) < 
		20002 && 0 <= i__2 ? i__2 : s_rnge("bnds2", i__2, "f_rc2grd__"
		, (ftnlen)365)], &c_b32, ok, (ftnlen)80, (ftnlen)1);
    }

/*     Check the grid. Marked pixels are included in the */
/*     rectangle set. */

    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    i__ = row + (col - 1) * nrows;
	    xvalue = col != 2;
	    chcksl_(label, &grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? 
		    i__3 : s_rnge("grid", i__3, "f_rc2grd__", (ftnlen)389)], &
		    xvalue, ok, (ftnlen)80);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Simple case: two overlapping rectangles, both X and Y bou"
	    "nds overlap.", (ftnlen)320, (ftnlen)69);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);

/*     Note that the output grid should have a column corresponding to */
/*     the horizontal gap between the rectangles. */

    xnrows = 3;
    xncols = 3;
    bnds1[0] = 1.;
    bnds1[1] = 3.;
    bnds1[2] = 2.;
    bnds1[3] = 4.;
    bnds2[0] = -1.;
    bnds2[1] = 1.;
    bnds2[2] = 0.;
    bnds2[3] = 2.;
    nrec = 2;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output grid dimensions. We're looking for a 3x3 grid. */

    chcksi_("NROWS", &nrows, "=", &xnrows, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOLS", &ncols, "=", &xncols, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the inverse order vectors. First, create the */
/*     expected vectors. */

    xcivo1[0] = 1;
    xcivo1[1] = 3;
    xcivo1[2] = 2;
    xcivo1[3] = 4;
    i__1 = xncols + 1;
    chckai_("CIVOR1", civor1, "=", xcivo1, &i__1, ok, (ftnlen)6, (ftnlen)1);
    xcivo2[0] = 1;
    xcivo2[1] = 3;
    xcivo2[2] = 2;
    xcivo2[3] = 4;
    i__1 = xnrows + 1;
    chckai_("CIVOR2", civor2, "=", xcivo2, &i__1, ok, (ftnlen)6, (ftnlen)1);

/*     Check the pixel maps. */

    xmap1[0] = 1;
    xmap1[1] = 3;
    xmap1[2] = 2;
    xmap1[3] = 4;
    i__1 = xncols + 1;
    chckai_("PXMAP1", pxmap1, "=", xmap1, &i__1, ok, (ftnlen)6, (ftnlen)1);
    xmap2[0] = 1;
    xmap2[1] = 3;
    xmap2[2] = 2;
    xmap2[3] = 4;
    i__1 = xnrows + 1;
    chckai_("PXMAP2", pxmap2, "=", xmap2, &i__1, ok, (ftnlen)6, (ftnlen)1);

/*     Check the grid. Marked pixels are included in the */
/*     rectangle set. */

    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    xvalue = TRUE_;
	    if (row == 3 && col == 1) {
		xvalue = FALSE_;
	    } else if (row == 1 && col == 3) {
		xvalue = FALSE_;
	    }
	    i__ = row + (col - 1) * nrows;
	    chcksl_(label, &grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? 
		    i__3 : s_rnge("grid", i__3, "f_rc2grd__", (ftnlen)516)], &
		    xvalue, ok, (ftnlen)80);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Simple case: 3x5 grid, no gaps.", (ftnlen)320, (ftnlen)31);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    includ = ! value;
    xnrows = 3;
    xncols = 5;
    i__ = 1;
    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    bnds1[(i__3 = (i__ << 1) - 2) < 20002 && 0 <= i__3 ? i__3 : 
		    s_rnge("bnds1", i__3, "f_rc2grd__", (ftnlen)554)] = (
		    doublereal) col;
	    bnds1[(i__3 = (i__ << 1) - 1) < 20002 && 0 <= i__3 ? i__3 : 
		    s_rnge("bnds1", i__3, "f_rc2grd__", (ftnlen)555)] = (
		    doublereal) (col + 1);
	    bnds2[(i__3 = (i__ << 1) - 2) < 20002 && 0 <= i__3 ? i__3 : 
		    s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)556)] = (
		    doublereal) row;
	    bnds2[(i__3 = (i__ << 1) - 1) < 20002 && 0 <= i__3 ? i__3 : 
		    s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)557)] = (
		    doublereal) (row + 1);
	    ++i__;
	}
    }
    nrec = xnrows * xncols;

/*     Find the coverage of the grid. */

    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output grid dimensions. We're looking for a 3x5 grid. */

    chcksi_("NROWS", &nrows, "=", &xnrows, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOLS", &ncols, "=", &xncols, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the inverse order vectors. First, create the */
/*     expected vectors. */

    i__ = 1;
    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    xcivo1[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo1", i__3, "f_rc2grd__", (ftnlen)592)] = col;
	    xcivo1[(i__3 = i__) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xcivo1", 
		    i__3, "f_rc2grd__", (ftnlen)593)] = col + 1;
	    i__ += 2;
	}
    }
    i__1 = xncols << 1;
    chckai_("CIVOR1", civor1, "=", xcivo1, &i__1, ok, (ftnlen)6, (ftnlen)1);
    i__ = 1;
    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    xcivo2[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo2", i__3, "f_rc2grd__", (ftnlen)611)] = row;
	    xcivo2[(i__3 = i__) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xcivo2", 
		    i__3, "f_rc2grd__", (ftnlen)612)] = row + 1;
	    i__ += 2;
	}
    }
    i__1 = xnrows << 1;
    chckai_("CIVOR2", civor2, "=", xcivo2, &i__1, ok, (ftnlen)6, (ftnlen)1);

/*     Check the pixel maps. */

    i__1 = xncols + 1;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows + 1;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*) coord 1", (ftnlen)80, (ftnlen)17);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    mapco1 = pxmap1[(i__3 = col - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("pxmap1", i__3, "f_rc2grd__", (ftnlen)634)];
	    mapco2 = pxmap2[(i__3 = row - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("pxmap2", i__3, "f_rc2grd__", (ftnlen)635)];
	    j = (mapco1 - 1) / 2 + 1;
	    i__ = mapco1 - (j - 1 << 1);
	    mapbd1 = bnds1[(i__3 = i__ + (j << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds1", i__3, "f_rc2grd__", (ftnlen)641)];
	    if (col <= xncols) {

/*              Pick the left X-bound of the rectangle in the first row */
/*              of column COL. */

		k = nrows * (col - 1) + 1;
		chcksd_(label, &mapbd1, "=", &bnds1[(i__3 = (k << 1) - 2) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds1", i__3, 
			"f_rc2grd__", (ftnlen)651)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    } else {

/*              Pick the right X-bound of the rectangle in the first row */
/*              of column XNCOLS. */

		k = nrows * (col - 2) + 1;
		chcksd_(label, &mapbd1, "=", &bnds1[(i__3 = (k << 1) - 1) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds1", i__3, 
			"f_rc2grd__", (ftnlen)660)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    }
	    s_copy(label, "Grid(*,*) coord 2", (ftnlen)80, (ftnlen)17);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    j = (mapco2 - 1) / 2 + 1;
	    i__ = mapco2 - (j - 1 << 1);
	    mapbd2 = bnds2[(i__3 = i__ + (j << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)673)];
	    if (row <= xnrows) {

/*              Pick the lower Y-bound of the rectangle in the first */
/*              column of row ROW. */

		k = row;
		chcksd_(label, &mapbd2, "=", &bnds2[(i__3 = (k << 1) - 2) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds2", i__3, 
			"f_rc2grd__", (ftnlen)683)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    } else {

/*              Pick the right X-bound of the rectangle in the first row */
/*              of column XNCOLS. */

		k = nrows;
		chcksd_(label, &mapbd2, "=", &bnds2[(i__3 = (k << 1) - 1) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds2", i__3, 
			"f_rc2grd__", (ftnlen)692)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    }
	}
    }

/*     Check the grid. Marked pixels are included in the */
/*     rectangle set. */

    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    i__ = row + (col - 1) * nrows;
	    chcksl_(label, &grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? 
		    i__3 : s_rnge("grid", i__3, "f_rc2grd__", (ftnlen)716)], &
		    includ, ok, (ftnlen)80);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Simple case: 3x5 grid, no gaps. Reverse the order of the "
	    "rectangle inputs.", (ftnlen)320, (ftnlen)74);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);

/*     VALUE is the logical value indicating a gap. */

    value = FALSE_;
    includ = ! value;
    xnrows = 3;
    xncols = 5;
    i__ = 1;
    for (col = xncols; col >= 1; --col) {
	for (row = xnrows; row >= 1; --row) {
	    bnds1[(i__1 = (i__ << 1) - 2) < 20002 && 0 <= i__1 ? i__1 : 
		    s_rnge("bnds1", i__1, "f_rc2grd__", (ftnlen)756)] = (
		    doublereal) col;
	    bnds1[(i__1 = (i__ << 1) - 1) < 20002 && 0 <= i__1 ? i__1 : 
		    s_rnge("bnds1", i__1, "f_rc2grd__", (ftnlen)757)] = (
		    doublereal) (col + 1);
	    bnds2[(i__1 = (i__ << 1) - 2) < 20002 && 0 <= i__1 ? i__1 : 
		    s_rnge("bnds2", i__1, "f_rc2grd__", (ftnlen)758)] = (
		    doublereal) row;
	    bnds2[(i__1 = (i__ << 1) - 1) < 20002 && 0 <= i__1 ? i__1 : 
		    s_rnge("bnds2", i__1, "f_rc2grd__", (ftnlen)759)] = (
		    doublereal) (row + 1);
	    ++i__;
	}
    }
    nrec = xnrows * xncols;

/*     Find the coverage of the grid. */

    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output grid dimensions. We're looking for a 3x5 grid. */

    chcksi_("NROWS", &nrows, "=", &xnrows, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOLS", &ncols, "=", &xncols, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the inverse order vectors. First, create the */
/*     expected vectors. */

    i__ = 1;
    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    xcivo1[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo1", i__3, "f_rc2grd__", (ftnlen)794)] = xncols + 1 - 
		    col;
	    xcivo1[(i__3 = i__) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xcivo1", 
		    i__3, "f_rc2grd__", (ftnlen)795)] = xncols + 2 - col;
	    i__ += 2;
	}
    }
    i__1 = xncols << 1;
    chckai_("CIVOR1", civor1, "=", xcivo1, &i__1, ok, (ftnlen)6, (ftnlen)1);
    i__ = 1;
    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    xcivo2[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xci"
		    "vo2", i__3, "f_rc2grd__", (ftnlen)813)] = xnrows + 1 - 
		    row;
	    xcivo2[(i__3 = i__) < 2000 && 0 <= i__3 ? i__3 : s_rnge("xcivo2", 
		    i__3, "f_rc2grd__", (ftnlen)814)] = xnrows + 2 - row;
	    i__ += 2;
	}
    }
    i__1 = xnrows << 1;
    chckai_("CIVOR2", civor2, "=", xcivo2, &i__1, ok, (ftnlen)6, (ftnlen)1);

/*     Check the pixel maps. */

    i__1 = xncols + 1;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows + 1;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*) coord 1", (ftnlen)80, (ftnlen)17);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    mapco1 = pxmap1[(i__3 = col - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("pxmap1", i__3, "f_rc2grd__", (ftnlen)835)];
	    mapco2 = pxmap2[(i__3 = row - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("pxmap2", i__3, "f_rc2grd__", (ftnlen)836)];
	    j = (mapco1 - 1) / 2 + 1;
	    i__ = mapco1 - (j - 1 << 1);
	    mapbd1 = bnds1[(i__3 = i__ + (j << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds1", i__3, "f_rc2grd__", (ftnlen)842)];
	    if (col <= xncols) {

/*              Pick the left X-bound of the rectangle in the first row */
/*              of column COL. */

		k = nrows * (col - 1) + 1;
/*               CALL CHCKSD ( LABEL, MAPBD1, '=', BNDS1(1,K), 0.0, OK ) */
	    } else {

/*              Pick the right X-bound of the rectangle in the first row */
/*              of column XNCOLS. */

		k = nrows * (col - 2) + 1;
/*               CALL CHCKSD ( LABEL, MAPBD1, '=', BNDS1(2,K), 0.0, OK ) */
	    }
	    s_copy(label, "Grid(*,*) coord 2", (ftnlen)80, (ftnlen)17);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    j = (mapco2 - 1) / 2 + 1;
	    i__ = mapco2 - (j - 1 << 1);
	    mapbd2 = bnds2[(i__3 = i__ + (j << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)874)];
	    if (row <= xnrows) {

/*              Pick the lower Y-bound of the rectangle in the first */
/*              column of row ROW. */

		k = xnrows + 1 - row;
		chcksd_(label, &mapbd2, "=", &bnds2[(i__3 = (k << 1) - 2) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds2", i__3, 
			"f_rc2grd__", (ftnlen)884)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    } else {

/*              Pick the right X-bound of the rectangle in the first row */
/*              of column XNCOLS. */

		k = 1;
		chcksd_(label, &mapbd2, "=", &bnds2[(i__3 = (k << 1) - 1) < 
			20002 && 0 <= i__3 ? i__3 : s_rnge("bnds2", i__3, 
			"f_rc2grd__", (ftnlen)893)], &c_b32, ok, (ftnlen)80, (
			ftnlen)1);
	    }
	}
    }

/*     Check the grid. Marked pixels are included in the */
/*     rectangle set. */

    i__1 = xncols;
    for (col = 1; col <= i__1; ++col) {
	i__2 = xnrows;
	for (row = 1; row <= i__2; ++row) {
	    s_copy(label, "Grid(*,*)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "*", &row, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "*", &col, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    i__ = row + (col - 1) * nrows;
	    chcksl_(label, &grid[(i__3 = i__ - 1) < 1000000 && 0 <= i__3 ? 
		    i__3 : s_rnge("grid", i__3, "f_rc2grd__", (ftnlen)918)], &
		    includ, ok, (ftnlen)80);
	}
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Test a random set of rectangles.", (ftnlen)320, (ftnlen)32)
	    ;
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);
    nrec = 100;
    h__ = 20.;
    w = 30.;
    seed = -1;
    i__1 = nrec;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the bounds of the Ith rectangle. */

	d__1 = -w / 2;
	d__2 = w / 2;
	bnds1[(i__2 = (i__ << 1) - 2) < 20002 && 0 <= i__2 ? i__2 : s_rnge(
		"bnds1", i__2, "f_rc2grd__", (ftnlen)953)] = t_randd__(&d__1, 
		&d__2, &seed);
	d__1 = w / 2;
	bnds1[(i__2 = (i__ << 1) - 1) < 20002 && 0 <= i__2 ? i__2 : s_rnge(
		"bnds1", i__2, "f_rc2grd__", (ftnlen)954)] = t_randd__(&bnds1[
		(i__3 = (i__ << 1) - 2) < 20002 && 0 <= i__3 ? i__3 : s_rnge(
		"bnds1", i__3, "f_rc2grd__", (ftnlen)954)], &d__1, &seed);
	d__1 = -h__ / 2;
	d__2 = h__ / 2;
	bnds2[(i__2 = (i__ << 1) - 2) < 20002 && 0 <= i__2 ? i__2 : s_rnge(
		"bnds2", i__2, "f_rc2grd__", (ftnlen)956)] = t_randd__(&d__1, 
		&d__2, &seed);
	d__1 = h__ / 2;
	bnds2[(i__2 = (i__ << 1) - 1) < 20002 && 0 <= i__2 ? i__2 : s_rnge(
		"bnds2", i__2, "f_rc2grd__", (ftnlen)957)] = t_randd__(&bnds2[
		(i__3 = (i__ << 1) - 2) < 20002 && 0 <= i__3 ? i__3 : s_rnge(
		"bnds2", i__3, "f_rc2grd__", (ftnlen)957)], &d__1, &seed);
/*         WRITE (*,*) 'corners: ', BNDS1(1,I),BNDS2(1,I), */
/*     .                            BNDS1(2,I),BNDS2(2,I) */
    }

/*     Make sets of the X bounds and Y bounds. The cardinalities of */
/*     these sets will enable us to compute the expected grid */
/*     dimensions. */

    n1 = nrec << 1;
    moved_(bnds1, &n1, &coset1[6]);
    validd_(&c__2000, &n1, coset1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    c1 = cardd_(coset1);
    xncols = c1 - 1;
    n2 = nrec << 1;
    moved_(bnds1, &n1, &coset2[6]);
    validd_(&c__2000, &n2, coset2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    c2 = cardd_(coset2);
    xnrows = c2 - 1;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the grid dimensions. */

    chcksi_("NROWS", &nrows, "=", &xnrows, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOLS", &ncols, "=", &xncols, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the pixels. Count the covered pixels as we go. */

    ncover = 0;
    i__1 = ncols;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Find the first coordinates of the boundaries corresponding */
/*        to the pixel at coordinates (J,I) in the pixel grid. */

	m = (pxmap1[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"pxmap1", i__2, "f_rc2grd__", (ftnlen)1013)] - 1) / 2 + 1;
	l = pxmap1[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("pxm"
		"ap1", i__2, "f_rc2grd__", (ftnlen)1015)] - (m - 1 << 1);
	pxmin1 = bnds1[(i__2 = l + (m << 1) - 3) < 20002 && 0 <= i__2 ? i__2 :
		 s_rnge("bnds1", i__2, "f_rc2grd__", (ftnlen)1017)];
	m = (pxmap1[(i__2 = i__) < 2000 && 0 <= i__2 ? i__2 : s_rnge("pxmap1",
		 i__2, "f_rc2grd__", (ftnlen)1019)] - 1) / 2 + 1;
	l = pxmap1[(i__2 = i__) < 2000 && 0 <= i__2 ? i__2 : s_rnge("pxmap1", 
		i__2, "f_rc2grd__", (ftnlen)1021)] - (m - 1 << 1);
	pxmax1 = bnds1[(i__2 = l + (m << 1) - 3) < 20002 && 0 <= i__2 ? i__2 :
		 s_rnge("bnds1", i__2, "f_rc2grd__", (ftnlen)1024)];
	mid1 = (pxmin1 + pxmax1) / 2;
	i__2 = nrows;
	for (j = 1; j <= i__2; ++j) {

/*           Find the second coordinates of the boundaries corresponding */
/*           to the pixel at coordinates (J,I) in the pixel grid. */

	    m = (pxmap2[(i__3 = j - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pxmap2", i__3, "f_rc2grd__", (ftnlen)1035)] - 1) / 2 + 1;
	    l = pxmap2[(i__3 = j - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pxmap2", i__3, "f_rc2grd__", (ftnlen)1037)] - (m - 1 << 
		    1);
	    pxmin2 = bnds2[(i__3 = l + (m << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)1039)];
	    m = (pxmap2[(i__3 = j) < 2000 && 0 <= i__3 ? i__3 : s_rnge("pxma"
		    "p2", i__3, "f_rc2grd__", (ftnlen)1041)] - 1) / 2 + 1;
	    l = pxmap2[(i__3 = j) < 2000 && 0 <= i__3 ? i__3 : s_rnge("pxmap2"
		    , i__3, "f_rc2grd__", (ftnlen)1043)] - (m - 1 << 1);
	    pxmax2 = bnds2[(i__3 = l + (m << 1) - 3) < 20002 && 0 <= i__3 ? 
		    i__3 : s_rnge("bnds2", i__3, "f_rc2grd__", (ftnlen)1045)];

/*           Check whether this pixel is inside any rectangle. */
/*           It suffices to check the center of the pixel. */

	    mid2 = (pxmin2 + pxmax2) / 2;
	    xvalue = FALSE_;
	    i__3 = nrec;
	    for (k = 1; k <= i__3; ++k) {
		if (bnds1[(i__4 = (k << 1) - 2) < 20002 && 0 <= i__4 ? i__4 : 
			s_rnge("bnds1", i__4, "f_rc2grd__", (ftnlen)1058)] < 
			mid1 && bnds1[(i__5 = (k << 1) - 1) < 20002 && 0 <= 
			i__5 ? i__5 : s_rnge("bnds1", i__5, "f_rc2grd__", (
			ftnlen)1058)] > mid1 && bnds2[(i__6 = (k << 1) - 2) < 
			20002 && 0 <= i__6 ? i__6 : s_rnge("bnds2", i__6, 
			"f_rc2grd__", (ftnlen)1058)] < mid2 && bnds2[(i__7 = (
			k << 1) - 1) < 20002 && 0 <= i__7 ? i__7 : s_rnge(
			"bnds2", i__7, "f_rc2grd__", (ftnlen)1058)] > mid2) {
		    xvalue = TRUE_;
		}
	    }
	    if (xvalue) {
		++ncover;
	    }

/*           If the current pixel is in one of the original rectangles, */
/*           the XVALUE is .TRUE. Otherwise XVALUE is .FALSE. */

	    s_copy(label, "Pixel(*,*)", (ftnlen)80, (ftnlen)10);
	    repmi_(label, "*", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    k = j + (i__ - 1) * nrows;
	    chcksl_(label, &grid[(i__3 = k - 1) < 1000000 && 0 <= i__3 ? i__3 
		    : s_rnge("grid", i__3, "f_rc2grd__", (ftnlen)1085)], &
		    xvalue, ok, (ftnlen)80);
	}
    }
/* ********************************************************************** */

/*     RC2GRD error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Invalid value of NREC.", (ftnlen)320, (ftnlen)22);
    tcase_(title, (ftnlen)320);

/*     Initialize all sets. */

    ssizei_(&c_b4, mrkset);
    ssizei_(&c_b4, tmpset);
    ssizei_(&c_b4, vset);

/*     Note that the output grid should have a column corresponding to */
/*     the horizontal gap between the rectangles. */

    xnrows = 1;
    xncols = 3;
    bnds1[0] = 1.;
    bnds1[1] = 3.;
    bnds1[2] = 4.;
    bnds1[3] = 5.;
    bnds2[0] = -1.;
    bnds2[1] = 1.;
    bnds2[2] = -1.;
    bnds2[3] = 1.;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    nrec = 0;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    nrec = -1;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "MAXGRD is too small.", (ftnlen)320, (ftnlen)20);
    tcase_(title, (ftnlen)320);
    nrec = 2;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c__2, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    rc2grd_(&nrec, bnds1, bnds2, &c_n1, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "MAXORD is too small.", (ftnlen)320, (ftnlen)20);
    tcase_(title, (ftnlen)320);
    nrec = 2;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__0, &includ, ord1, ord2, civor1, 
	    civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c_n1, &includ, ord1, ord2, civor1, 
	    civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Invalid edge length.", (ftnlen)320, (ftnlen)20);
    tcase_(title, (ftnlen)320);
    nrec = 2;
    xnrows = 1;
    xncols = 3;
    bnds1[0] = 1.;
    bnds1[1] = 1.;
    bnds1[2] = 4.;
    bnds1[3] = 5.;
    bnds2[0] = -1.;
    bnds2[1] = 1.;
    bnds2[2] = -1.;
    bnds2[3] = 1.;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(INVALIDBOUNDS)", ok, (ftnlen)20);
    bnds1[0] = 1.;
    bnds1[1] = 2.;
    bnds1[2] = 4.;
    bnds1[3] = 5.;
    bnds2[0] = -1.;
    bnds2[1] = 1.;
    bnds2[2] = -1.;
    bnds2[3] = -2.;

/*     Find the coverage of the grid. */

    includ = TRUE_;
    rc2grd_(&nrec, bnds1, bnds2, &c_b4, &c__2000, &includ, ord1, ord2, civor1,
	     civor2, pxmap1, pxmap2, &nrows, &ncols, grid);
    chckxc_(&c_true, "SPICE(INVALIDBOUNDS)", ok, (ftnlen)20);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_rc2grd__ */

