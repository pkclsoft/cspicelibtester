/* f_rdpck.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1000 = 1000;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c_n10 = -10;
static integer c__14 = 14;
static doublereal c_b72 = 1e-11;
static doublereal c_b78 = 1e-10;
static doublereal c_b81 = 0.;
static integer c__3 = 3;
static integer c__9 = 9;
static integer c__6 = 6;
static integer c__36 = 36;
static integer c__10 = 10;

/* $Procedure      F_RDPCK ( Text PCK reader tests ) */
/* Subroutine */ int f_rdpck__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;
    logical L__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_indx(char *, char *, 
	    ftnlen, ftnlen);
    double d_mod(doublereal *, doublereal *);

    /* Local variables */
    integer code;
    doublereal xdec;
    char line[200];
    doublereal xrad[3];
    extern /* Subroutine */ int t_pckang__(integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), t_pckrad__(integer *, 
	    doublereal *), mxmg_(doublereal *, doublereal *, integer *, 
	    integer *, integer *, doublereal *);
    doublereal tipm[9]	/* was [3][3] */, zmat[9]	/* was [3][3] */;
    integer xset[1006];
    extern /* Subroutine */ int t_pckast__(integer *), t_pckrst__(integer *), 
	    eul2m_(doublereal *, doublereal *, doublereal *, integer *, 
	    integer *, integer *, doublereal *);
    integer i__, j, k;
    extern integer cardc_(char *, ftnlen);
    integer n;
    extern integer cardi_(integer *);
    integer t;
    doublereal w, radii[3], delta;
    extern logical elemi_(integer *, integer *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    char qname[200];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    char xname[32];
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), moved_(doublereal *, integer *,
	     doublereal *);
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    char title[200];
    extern /* Subroutine */ int copyi_(integer *, integer *);
    char idstr[32];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal tsipm[36]	/* was [6][6] */, tmpxf[36]	/* was [6][6] 
	    */, xtipm[9]	/* was [3][3] */;
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *), eul2xf_(doublereal *, 
	    integer *, integer *, integer *, doublereal *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    doublereal tsipm2[36]	/* was [6][6] */, lambda, ra, et;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen), chcksd_(char *, doublereal *, char *, doublereal 
	    *, doublereal *, logical *, ftnlen, ftnlen), t_pck10__(char *, 
	    logical *, logical *, ftnlen), validc_(integer *, integer *, char 
	    *, ftnlen);
    extern doublereal halfpi_(void);
    extern integer bsrchc_(char *, integer *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    doublereal xlmbda;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), bodmat_(integer *, doublereal *, doublereal *), 
	    bodeul_(integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), bodvcd_(integer *, char *, integer *,
	     integer *, doublereal *, ftnlen);
    doublereal xw;
    integer angset[1006], radset[1006];
    extern /* Subroutine */ int tipbod_(char *, integer *, doublereal *, 
	    doublereal *, ftnlen), tisbod_(char *, integer *, doublereal *, 
	    doublereal *, ftnlen), qderiv_(integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), removc_(char *, char *,
	     ftnlen, ftnlen);
    doublereal eulsta[6];
    char kvnams[32*1006];
    doublereal b2j[9]	/* was [3][3] */, tmpmat[9]	/* was [3][3] */, 
	    xdtipm[9]	/* was [3][3] */;
    extern /* Subroutine */ int ssizei_(integer *, integer *), gnpool_(char *,
	     integer *, integer *, integer *, char *, logical *, ftnlen, 
	    ftnlen), removi_(integer *, integer *), pxform_(char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), sxform_(char *, char 
	    *, doublereal *, doublereal *, ftnlen, ftnlen), prsint_(char *, 
	    integer *, ftnlen);
    doublereal xtsipm[36]	/* was [6][6] */;
    extern doublereal j2000_(void);
    doublereal dec;
    extern doublereal b1950_(void);
    char pck[255];
    extern doublereal rpd_(void), spd_(void);
    doublereal xra;
    extern /* Subroutine */ int mxm_(doublereal *, doublereal *, doublereal *)
	    ;
    doublereal rj2e[9]	/* was [3][3] */, tj2e[36]	/* was [6][6] */, 
	    eul0[3], eul2[3];

/* $ Abstract */

/*     This family tests the current NAIF text PCK and the PCK */
/*     reader software that makes use of it. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     PCK */

/* $ Keywords */

/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   Test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK             is a boolean flag indicating test status. OK is */
/*                    returned .TRUE. only if all tests conducted by */
/*                    this family have passed. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     Not applicable. */

/* $ Files */

/*     The data provided by this routine are meant to be consistent with */
/*     the current IAU report [1] and should be consistent with the NAIF */
/*     text PCK */

/*        pck00010.tpc */

/*     This routine must be updated along with each NAIF text PCK update. */

/* $ Particulars */

/*     This routine tests a version of the current NAIF text PCK file */
/*     which is generated at run time by the test utility T_PCKnn. */

/*     Orientation data are checked by means of calls to the SPICELIB */
/*     routines */

/*        BODEUL */
/*        BODMAT */
/*        TIPBOD */
/*        TISBOD */

/*     Results of computations done by these routines are compared */
/*     with results of computations implemented in-line in the */
/*     test utility routine T_PCKEQ.  This testing approach attempts */
/*     to validate orientation computations done using text PCK data */
/*     by comparing their results to those obtained via an alternate */
/*     computational approach. */

/*     This routine also tests radii data provided by the PCK, and */
/*     checks that the sets of bodies for which orientation and radii */
/*     data are provided by the PCK match the corresponding sets for */
/*     the IAU report [1]. */

/*     These tests also serve to minimize the chance of transcription */
/*     error when a new PCK version is created. Entering the PCK */
/*     constants both in the new PCK file and in T_PCKEQ enables */
/*     double-checking of the constants. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     1) This routine must be kept consistent with the both current IAU */
/*        report [1] and the current NAIF text PCK. Currency is achieved */
/*        by calling a current version of T_PCKEQ and the current PCK */
/*        generation utility T_PCKnn. */


/* $ Literature_References */

/*     [1]   Seidelmann, P.K., Archinal, B.A., A'Hearn, M.F., */
/*           Conrad, A., Consolmagno, G.J., Hestroffer, D., */
/*           Hilton, J.L., Krasinsky, G.A., Neumann, G., */
/*           Oberst, J., Stooke, P., Tedesco, E.F., Tholen, D.J., */
/*           and Thomas, P.C. "Report of the IAU/IAG Working Group */
/*           on cartographic coordinates and rotational elements: 2006." */

/* $ Author_and_Institution */

/*     N.J. Bachman       (JPL) */

/* $ Version */

/* -    TSPICE Version 4.0.0, 29-JAN-2011 (NJB) */

/*        Updated to test pck00010.tpc. */

/*        Length of parameter NUMLEN was increased to suppress */
/*        string truncation warning issued by gfortran. */

/* -    TSPICE Version 3.0.0, 11-FEB-2010 (NJB) */

/*        Tests have been added for the sets of bodies for which radii */
/*        and orientation data are provided. */

/* -    TSPICE Version 2.0.0, 14-DEC-2005 (NJB) */

/*        Upgraded tests: */

/*          - Full tests for TISBOD have been added.  Previously, */
/*            just a sanity check was done on the upper left block */
/*            of TISBOD's output matrix.  (The previous version of */
/*            this routine did not claim to test TISBOD at all.) */

/*          - Tests of TIPBOD and TISBOD for non-native input frames */
/*            have been added. */

/*          - Test for different epochs have been condensed using loops. */


/* -    TSPICE Version 1.1.0, 27-SEP-2005 (NJB) */

/*        Updated to remove non-standard use of duplicate arguments */
/*        in MXM calls. */

/* -    TSPICE Version 1.0.0, 13-FEB-2004 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Begin every test family with an open call. */

    topen_("F_RDPCK", (ftnlen)7);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create full text PCK file.", (ftnlen)34);
    s_copy(pck, "test_0010.tpc", (ftnlen)255, (ftnlen)13);

/*     Create the PCK file, load it, and delete it. */

    t_pck10__(pck, &c_true, &c_true, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(pck, (ftnlen)255);
/* *************************************************************** */

/*     Rotation tests */

/* *************************************************************** */

/*     Check the set of bodies for which rotation data are provided. */
/*     Find the set of kernel variables for prime meridian coefficients; */
/*     check the corresponding set of ID codes against the set */
/*     from the test utility T_PCKAST. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check the set of bodies for which rotation data are provided.", (
	    ftnlen)61);
    ssizei_(&c__1000, xset);
    ssizei_(&c__1000, angset);
    t_pckast__(xset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gnpool_("BODY*_PM", &c__1, &c__1000, &n, kvnams + 192, &found, (ftnlen)8, 
	    (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    validc_(&c__1000, &n, kvnams, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Remove all of the nutation precession PM names. */

    i__ = 1;
    while(i__ <= cardc_(kvnams, (ftnlen)32)) {
	if (i_indx(kvnams + (((i__1 = i__ + 5) < 1006 && 0 <= i__1 ? i__1 : 
		s_rnge("kvnams", i__1, "f_rdpck__", (ftnlen)336)) << 5), 
		"NUT_PREC", (ftnlen)32, (ftnlen)8) > 0) {
	    removc_(kvnams + (((i__1 = i__ + 5) < 1006 && 0 <= i__1 ? i__1 : 
		    s_rnge("kvnams", i__1, "f_rdpck__", (ftnlen)338)) << 5), 
		    kvnams, (ftnlen)32, (ftnlen)32);
	} else {
	    ++i__;
	}
    }
    chcksl_("PM names found", &found, &c_true, ok, (ftnlen)14);
    n = cardc_(kvnams, (ftnlen)32);
    i__1 = cardi_(xset);
    chcksi_("Number of PM names", &n, "=", &i__1, &c__0, ok, (ftnlen)18, (
	    ftnlen)1);

/*     Check each name in the expected set for presence in the */
/*     actual name set. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check bodies in set expected to have rotation data against those"
	    " present in the kernel.", (ftnlen)87);
    i__1 = cardi_(xset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(xname, "BODY#_PM", (ftnlen)32, (ftnlen)8);
	repmi_(xname, "#", &xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 :
		 s_rnge("xset", i__2, "f_rdpck__", (ftnlen)369)], xname, (
		ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Make sure the expected name is in the kernel */
/*           variable list. */

	j = bsrchc_(xname, &n, kvnams + 192, (ftnlen)32, (ftnlen)32);
	s_copy(qname, "# is present in kernel?", (ftnlen)200, (ftnlen)23);
	repmc_(qname, "#", xname, qname, (ftnlen)200, (ftnlen)1, (ftnlen)32, (
		ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	L__1 = j > 0;
	chcksl_(qname, &L__1, &c_true, ok, (ftnlen)200);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Check bodies having rotation data in kernel against those in the"
	    " expected set.", (ftnlen)78);

/*     Check each name in the actual set for presence in the */
/*     expected name set. */

    i__1 = cardc_(kvnams, (ftnlen)32);
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get the ID code of the current name. */

	s_copy(idstr, kvnams + ((((i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 
		: s_rnge("kvnams", i__2, "f_rdpck__", (ftnlen)402)) << 5) + 4)
		, (ftnlen)32, (ftnlen)28);
	j = i_indx(idstr, "_", (ftnlen)32, (ftnlen)1);
	prsint_(idstr, &code, j - 1);
	s_copy(qname, "# is present in expected set?", (ftnlen)200, (ftnlen)
		29);
	repmi_(qname, "#", &code, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	L__1 = elemi_(&code, xset);
	chcksl_(qname, &L__1, &c_true, ok, (ftnlen)200);
    }

/*     Since we've checked the set of bodies, let the actual */
/*     set be the expected set. */

    copyi_(xset, angset);

/*     Delete body -10 from the set used for the normal test cases. */

    removi_(&c_n10, angset);

/*     Perform BODEUL tests. */

/*     A note concerning the looser tolerance value MEDIUM used */
/*     in these tests:  converting Euler angles to rotation matrices */
/*     involves applying the SIN and COS functions to the angles. */
/*     In the case of the prime meridian angle W, applying trig */
/*     functions increases the relative error, since W may start */
/*     out with magnitude much larger than 2*pi.  The error in W */
/*     is the largest source of error in the matrix TIPM. */

    i__1 = cardi_(angset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (t = 1; t <= 2; ++t) {

/* --- Case: ------------------------------------------------------ */

	    if (t == 1) {
		et = 0.;
	    } else {
		et = -3.15576e8;
	    }
	    s_copy(line, "BODEUL test: Check Euler angles for # at ET #.", (
		    ftnlen)200, (ftnlen)46);
	    repmi_(line, "#", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)453)], 
		    line, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	    repmd_(line, "#", &et, &c__14, line, (ftnlen)200, (ftnlen)1, (
		    ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(line, (ftnlen)200);
	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)460)], &et, &
		    xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = twopi_();
	    xw = d_mod(&xw, &d__1);
	    if (xw < 0.) {
		xw += twopi_();
	    }
	    bodeul_(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)469)], &et, &
		    ra, &dec, &w, &lambda);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("RA", &ra, "~", &xra, &c_b72, ok, (ftnlen)2, (ftnlen)1);
	    chcksd_("DEC", &dec, "~", &xdec, &c_b72, ok, (ftnlen)3, (ftnlen)1)
		    ;
	    chcksd_("W", &w, "~", &xw, &c_b78, ok, (ftnlen)1, (ftnlen)1);
	}
    }

/*     Perform analogous tests on both BODMAT and TIPBOD. In order to */
/*     compare results against those from T_PCKANG, we represent results */
/*     from the latter routine as rotation matrices. */

/*     Obtain the position and state transformations from the J2000 */
/*     to the ECLIPJ2000 frame.  These will be used later. */

    pxform_("J2000", "ECLIPJ2000", &c_b81, rj2e, (ftnlen)5, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sxform_("J2000", "ECLIPJ2000", &c_b81, tj2e, (ftnlen)5, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = cardi_(angset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (t = 1; t <= 2; ++t) {

/* --- Case: ------------------------------------------------------ */

	    if (t == 1) {
		et = 0.;
	    } else {
		et = -3.15576e8;
	    }
	    s_copy(line, "BODMAT test: Check attitude matrix for # at ET #.", 
		    (ftnlen)200, (ftnlen)49);
	    repmi_(line, "#", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)511)], 
		    line, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	    repmd_(line, "#", &et, &c__14, line, (ftnlen)200, (ftnlen)1, (
		    ftnlen)200);
	    tcase_(line, (ftnlen)200);
	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)515)], &et, &
		    xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = halfpi_() - xdec;
	    d__2 = halfpi_() + xra;
	    eul2m_(&xw, &d__1, &d__2, &c__3, &c__1, &c__3, xtipm);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Test BODMAT. */

	    bodmat_(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)525)], &et, 
		    tipm);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (
		    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*              Test TIPBOD. */

	    s_copy(line, "TIPBOD test: Check attitude matrix for # at ET #.", 
		    (ftnlen)200, (ftnlen)49);
	    repmi_(line, "#", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)539)], 
		    line, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	    repmd_(line, "#", &et, &c__14, line, (ftnlen)200, (ftnlen)1, (
		    ftnlen)200);
	    tcase_(line, (ftnlen)200);
	    tipbod_("J2000", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)543)], 
		    &et, tipm, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (
		    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*              Repeat the TIPBOD test using a non-native inertial */
/*              frame. Right-multiply the matrix obtained from TIPBOD by */
/*              the mapping from J2000 to the non-native frame; this */
/*              should give us the same transformation we'd have had if */
/*              we had looked up the mapping from J2000 to the */
/*              body-fixed frame directly. */

	    tipbod_("ECLIPJ2000", &angset[(i__2 = i__ + 5) < 1006 && 0 <= 
		    i__2 ? i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)
		    559)], &et, tipm, (ftnlen)10);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    mxm_(tipm, rj2e, tmpmat);
	    moved_(tmpmat, &c__9, tipm);
	    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (
		    ftnlen)1);
	}
    }

/*     Perform analogous tests on TISBOD.  In order to compare */
/*     results against those from T_PCKANG, we represent results */
/*     from the latter routine as rotation matrices. */

/*     We use discrete derivatives of the Euler angles to examine the */
/*     derivative portion of the state transformation matrix. */

    cleard_(&c__9, zmat);
    i__1 = cardi_(angset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (t = 1; t <= 2; ++t) {

/* --- Case: ------------------------------------------------------ */

	    if (t == 1) {
		et = 0.;
	    } else {
		et = -3.15576e8;
	    }
	    s_copy(line, "TISBOD: check state transformation matrix for # at"
		    " ET #.", (ftnlen)200, (ftnlen)56);
	    repmi_(line, "#", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)600)], 
		    line, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	    repmd_(line, "#", &et, &c__14, line, (ftnlen)200, (ftnlen)1, (
		    ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(line, (ftnlen)200);
	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)607)], &et, &
		    xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = halfpi_() - xdec;
	    d__2 = halfpi_() + xra;
	    eul2m_(&xw, &d__1, &d__2, &c__3, &c__1, &c__3, xtipm);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tisbod_("J2000", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)614)], 
		    &et, tsipm, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              We test the blocks of the state transformation matrix */
/*              separately, since we don't use identical tolerances for */
/*              all blocks. */

	    for (j = 1; j <= 3; ++j) {
		for (k = 1; k <= 3; ++k) {
		    tipm[(i__2 = j + k * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			    s_rnge("tipm", i__2, "f_rdpck__", (ftnlen)624)] = 
			    tsipm[(i__3 = j + k * 6 - 7) < 36 && 0 <= i__3 ? 
			    i__3 : s_rnge("tsipm", i__3, "f_rdpck__", (ftnlen)
			    624)];
		}
	    }
	    chckad_("TSIPM (upper left block)", tipm, "~", xtipm, &c__9, &
		    c_b78, ok, (ftnlen)24, (ftnlen)1);
	    for (j = 1; j <= 3; ++j) {
		for (k = 1; k <= 3; ++k) {
		    tipm[(i__2 = j + k * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			    s_rnge("tipm", i__2, "f_rdpck__", (ftnlen)633)] = 
			    tsipm[(i__3 = j + 3 + (k + 3) * 6 - 7) < 36 && 0 
			    <= i__3 ? i__3 : s_rnge("tsipm", i__3, "f_rdpck__"
			    , (ftnlen)633)];
		}
	    }
	    chckad_("TSIPM (lower right block)", tipm, "~", xtipm, &c__9, &
		    c_b78, ok, (ftnlen)25, (ftnlen)1);
	    for (j = 1; j <= 3; ++j) {
		for (k = 1; k <= 3; ++k) {
		    tmpmat[(i__2 = j + k * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			    s_rnge("tmpmat", i__2, "f_rdpck__", (ftnlen)642)] 
			    = tsipm[(i__3 = j + (k + 3) * 6 - 7) < 36 && 0 <= 
			    i__3 ? i__3 : s_rnge("tsipm", i__3, "f_rdpck__", (
			    ftnlen)642)];
		}
	    }
	    chckad_("TSIPM (upper right block)", tmpmat, "~", zmat, &c__9, &
		    c_b78, ok, (ftnlen)25, (ftnlen)1);

/*              To test the derivative block, we'll first estimate the */
/*              derivatives of the Euler angles using a quadratic */
/*              approximation.  We'll sample the angles at +/- 1 second */
/*              from ET. */

	    delta = 1.;

/*              Get the left side angles... */

	    d__1 = et - delta;
	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)660)], &d__1, 
		    &xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    eul0[0] = xw;
	    eul0[1] = halfpi_() - xdec;
	    eul0[2] = halfpi_() + xra;

/*              The right side angles... */

	    d__1 = et + delta;
	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)670)], &d__1, 
		    &xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    eul2[0] = xw;
	    eul2[1] = halfpi_() - xdec;
	    eul2[2] = halfpi_() + xra;

/*              Find the derivatives. */

	    qderiv_(&c__3, eul0, eul2, &delta, &eulsta[3]);

/*              Complete the Euler angle state vector by filling in the */
/*              first three components with the angles at ET. */

	    t_pckang__(&angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : 
		    s_rnge("angset", i__2, "f_rdpck__", (ftnlen)686)], &et, &
		    xra, &xdec, &xw);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = halfpi_() - xdec;
	    d__2 = halfpi_() + xra;
	    vpack_(&xw, &d__1, &d__2, eulsta);

/*              Now find the expected state transformation at ET. */

	    eul2xf_(eulsta, &c__3, &c__1, &c__3, xtsipm);

/*              Extract the derivative block from the expected state */
/*              transformation. */

	    for (j = 1; j <= 3; ++j) {
		for (k = 1; k <= 3; ++k) {
		    xdtipm[(i__2 = j + k * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			    s_rnge("xdtipm", i__2, "f_rdpck__", (ftnlen)702)] 
			    = xtsipm[(i__3 = j + 3 + k * 6 - 7) < 36 && 0 <= 
			    i__3 ? i__3 : s_rnge("xtsipm", i__3, "f_rdpck__", 
			    (ftnlen)702)];
		}
	    }

/*              Do the same for the transformation returned from TISBOD. */

	    for (j = 1; j <= 3; ++j) {
		for (k = 1; k <= 3; ++k) {
		    tmpmat[(i__2 = j + k * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			    s_rnge("tmpmat", i__2, "f_rdpck__", (ftnlen)711)] 
			    = tsipm[(i__3 = j + 3 + k * 6 - 7) < 36 && 0 <= 
			    i__3 ? i__3 : s_rnge("tsipm", i__3, "f_rdpck__", (
			    ftnlen)711)];
		}
	    }
	    chckad_("TSIPM derivative (lower left block)", tmpmat, "~", 
		    xdtipm, &c__9, &c_b78, ok, (ftnlen)35, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*              Repeat the TISBOD test using a non-native input frame. */

	    s_copy(line, "TISBOD: check state transformation matrix for # at"
		    " ET #.  Use ECLIPJ2000 as input frame.", (ftnlen)200, (
		    ftnlen)88);
	    repmi_(line, "#", &angset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? 
		    i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)728)], 
		    line, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	    repmd_(line, "#", &et, &c__14, line, (ftnlen)200, (ftnlen)1, (
		    ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(line, (ftnlen)200);
	    tisbod_("ECLIPJ2000", &angset[(i__2 = i__ + 5) < 1006 && 0 <= 
		    i__2 ? i__2 : s_rnge("angset", i__2, "f_rdpck__", (ftnlen)
		    733)], &et, tsipm2, (ftnlen)10);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    mxmg_(tsipm2, tj2e, &c__6, &c__6, &c__6, tmpxf);
	    moved_(tmpxf, &c__36, tsipm2);

/*              Compare against TSIPM from the previous test. */

	    chckad_("TSIPM2", tsipm2, "~", tsipm, &c__9, &c_b78, ok, (ftnlen)
		    6, (ftnlen)1);
	}

/*           We've tested TISBOD for the current time value. */


/*        We've tested TISBOD for the current body. */

    }

/* --- Case: ------------------------------------------------------ */


/*     Now check TIPBOD'S ability to deal with non-standard epochs and */
/*     frames.  Look up data for body -10 (these data are created by the */
/*     test utility T_PCK09) at the J1950 epoch in the B1950 frame. */
/*     Object -10 uses the same constants as those for the sun, but for */
/*     body -10, the native frame is B1950 and the reference epoch is */
/*     J1950.  The output should match the standard numbers for the sun. */

    tcase_("TIPBOD lookup of matrix for body -10", (ftnlen)36);
    et = (b1950_() - j2000_()) * spd_();
    bodmat_(&c__10, &c_b81, xtipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tipbod_("B1950", &c_n10, &et, tipm, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (ftnlen)1)
	    ;

/* --- Case: ------------------------------------------------------ */


/*     Repeat the test for TISBOD. */

    tcase_("TISBOD lookup of matrix for body -10", (ftnlen)36);
    tisbod_("B1950", &c_n10, &et, tsipm, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (j = 1; j <= 3; ++j) {
	for (k = 1; k <= 3; ++k) {
	    tipm[(i__1 = j + k * 3 - 4) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		    "tipm", i__1, "f_rdpck__", (ftnlen)794)] = tsipm[(i__2 = 
		    j + k * 6 - 7) < 36 && 0 <= i__2 ? i__2 : s_rnge("tsipm", 
		    i__2, "f_rdpck__", (ftnlen)794)];
	}
    }
    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Now check BODMAT's ability to deal with non-standard epochs and */
/*     frames.  Look up data for body -10 at the B1950 epoch in the */
/*     B1950 frame.  Since the numbers in the PCK match the standard */
/*     numbers for the sun, applying the B1950-to-J2000 transformation */
/*     to the output via right multiplication */
/*     should yield the standard matrix for the sun. */
    tcase_("BODMAT lookup of matrix for body -10", (ftnlen)36);
    et = (b1950_() - j2000_()) * spd_();
    bodmat_(&c__10, &c_b81, xtipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodmat_(&c_n10, &et, tipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pxform_("B1950", "J2000", &c_b81, b2j, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mxm_(tipm, b2j, tmpmat);
    moved_(tmpmat, &c__9, tipm);
    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Now check BODEUL's ability to deal with non-standard epochs and */
/*     frames.  Look up data for body -10 at the B1950 epoch in the */
/*     B1950 frame.  Convert the resulting Euler angles to a matrix. */

/*     Since the numbers in the PCK match the standard */
/*     numbers for the sun, applying the B1950-to-J2000 transformation */
/*     to the output via right multiplication */
/*     should yield the standard matrix for the sun. */

    tcase_("BODEUL lookup of angles for body -10, epoch B9150", (ftnlen)49);
    et = (b1950_() - j2000_()) * spd_();
    bodmat_(&c__10, &c_b81, xtipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodeul_(&c_n10, &et, &ra, &dec, &w, &lambda);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = halfpi_() - dec;
    d__2 = halfpi_() + ra;
    eul2m_(&w, &d__1, &d__2, &c__3, &c__1, &c__3, tipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pxform_("B1950", "J2000", &c_b81, b2j, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mxm_(tipm, b2j, tmpmat);
    moved_(tmpmat, &c__9, tipm);
    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Repeat test for epoch J2000. */

    tcase_("BODEUL lookup of angles for body -10, epoch J2000", (ftnlen)49);
    et = (j2000_() - b1950_()) * spd_();
    bodmat_(&c__10, &et, xtipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodeul_(&c_n10, &c_b81, &ra, &dec, &w, &lambda);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = halfpi_() - dec;
    d__2 = halfpi_() + ra;
    eul2m_(&w, &d__1, &d__2, &c__3, &c__1, &c__3, tipm);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pxform_("B1950", "J2000", &c_b81, b2j, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mxm_(tipm, b2j, tmpmat);
    moved_(tmpmat, &c__9, tipm);
    chckad_("TIPM", tipm, "~", xtipm, &c__9, &c_b78, ok, (ftnlen)4, (ftnlen)1)
	    ;

/*     Check the LAMBDA value associated with body -10.  We */
/*     expect the value to be 99 degrees *RPD().  (The value */
/*     459 degrees is set by T_PCK09, which writes the PCK file */
/*     we're using.  We expect BODEUL to mod this value by 2*pi.) */

    xlmbda = rpd_() * 99.;
    chcksd_("LAMBDA", &lambda, "~", &xlmbda, &c_b78, ok, (ftnlen)6, (ftnlen)1)
	    ;
/* *************************************************************** */

/*     Radius tests */

/* *************************************************************** */

/*     Check the set of bodies for which radius data are provided. */
/*     Find the set of kernel variables for triaxial radii; */
/*     check the corresponding set of ID codes against the set */
/*     from the test utility T_PCKRST. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check the set of bodies for which radius data are provided.", (
	    ftnlen)59);
    ssizei_(&c__1000, xset);
    ssizei_(&c__1000, radset);
    t_pckrst__(xset);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    gnpool_("BODY*_RADII", &c__1, &c__1000, &n, kvnams + 192, &found, (ftnlen)
	    11, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    validc_(&c__1000, &n, kvnams, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("RADII names found", &found, &c_true, ok, (ftnlen)17);
    n = cardc_(kvnams, (ftnlen)32);
    i__1 = cardi_(xset);
    chcksi_("Number of RADII names", &n, "=", &i__1, &c__0, ok, (ftnlen)21, (
	    ftnlen)1);

/*     Check each name in the expected set for presence in the */
/*     actual name set. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check bodies in set expected to have radius data against those p"
	    "resent in the kernel.", (ftnlen)85);
    i__1 = cardi_(xset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(xname, "BODY#_RADII", (ftnlen)32, (ftnlen)11);
	repmi_(xname, "#", &xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 :
		 s_rnge("xset", i__2, "f_rdpck__", (ftnlen)950)], xname, (
		ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Make sure the expected name is in the kernel */
/*           variable list. */

	j = bsrchc_(xname, &n, kvnams + 192, (ftnlen)32, (ftnlen)32);
	s_copy(qname, "# is present in kernel?", (ftnlen)200, (ftnlen)23);
	repmc_(qname, "#", xname, qname, (ftnlen)200, (ftnlen)1, (ftnlen)32, (
		ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	L__1 = j > 0;
	chcksl_(qname, &L__1, &c_true, ok, (ftnlen)200);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Check bodies having radius data in kernel against those in the e"
	    "xpected set.", (ftnlen)76);

/*     Check each name in the actual set for presence in the */
/*     expected name set. */

    i__1 = cardc_(kvnams, (ftnlen)32);
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get the ID code of the current name. */

	s_copy(idstr, kvnams + ((((i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 
		: s_rnge("kvnams", i__2, "f_rdpck__", (ftnlen)983)) << 5) + 4)
		, (ftnlen)32, (ftnlen)28);
	j = i_indx(idstr, "_", (ftnlen)32, (ftnlen)1);
	prsint_(idstr, &code, j - 1);
	s_copy(qname, "# is present in expected set?", (ftnlen)200, (ftnlen)
		29);
	repmi_(qname, "#", &code, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	L__1 = elemi_(&code, xset);
	chcksl_(qname, &L__1, &c_true, ok, (ftnlen)200);
    }

/*     Since we've checked the set of bodies, let the actual */
/*     set be the expected set. */

    copyi_(xset, angset);

/* --- Case: ------------------------------------------------------ */


/*     Check radii of each body in the expected set. */

    i__1 = cardi_(xset);
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Check radii of body #", (ftnlen)200, (ftnlen)21);
	repmi_(title, "#", &xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 :
		 s_rnge("xset", i__2, "f_rdpck__", (ftnlen)1015)], title, (
		ftnlen)200, (ftnlen)1, (ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

	tcase_(title, (ftnlen)200);
	t_pckrad__(&xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : s_rnge(
		"xset", i__2, "f_rdpck__", (ftnlen)1024)], xrad);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	bodvcd_(&xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 : s_rnge(
		"xset", i__2, "f_rdpck__", (ftnlen)1028)], "RADII", &c__3, &j,
		 radii, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Number of radii of body #", (ftnlen)200, (ftnlen)25);
	repmi_(qname, "#", &xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 :
		 s_rnge("xset", i__2, "f_rdpck__", (ftnlen)1033)], qname, (
		ftnlen)200, (ftnlen)1, (ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(qname, &j, "=", &c__3, &c__0, ok, (ftnlen)200, (ftnlen)1);
	s_copy(qname, "Radii of body #", (ftnlen)200, (ftnlen)15);
	repmi_(qname, "#", &xset[(i__2 = i__ + 5) < 1006 && 0 <= i__2 ? i__2 :
		 s_rnge("xset", i__2, "f_rdpck__", (ftnlen)1040)], qname, (
		ftnlen)200, (ftnlen)1, (ftnlen)200);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(qname, radii, "=", xrad, &c__3, &c_b72, ok, (ftnlen)200, (
		ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_rdpck__ */

