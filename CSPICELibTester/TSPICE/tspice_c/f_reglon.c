/* f_reglon.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b8 = 0.;
static doublereal c_b9 = 1e-14;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__2 = 2;
static logical c_true = TRUE_;

/* $Procedure F_REGLON ( REGLON tests ) */
/* Subroutine */ int f_reglon__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static doublereal xbds[200]	/* was [2][100] */;
    static integer maxn, size, srcs[100], nout;
    extern /* Subroutine */ int tcase_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal twopi_(void);
    static integer xsrcs[100];
    extern /* Subroutine */ int t_success__(logical *);
    static integer xnout;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen), reglon_(
	    integer *, doublereal *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, integer *);
    static doublereal bounds[200]	/* was [2][100] */, minlon;
    static integer nivals;
    static doublereal maxlon, outbds[200]	/* was [2][100] */, tol;

/* $ Abstract */

/*     Exercise the longitude "regularization" routine REGLON. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKBRIEF supporting routine REGLON. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 21-FEB-2017 (NJB) */

/*        Updated for consistency with new behavior of REGLON. */
/*        REGLON now tries to avoid splitting longitude ranges */
/*        into two intervals. */

/*        Previous version 12-OCT-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_REGLON", (ftnlen)8);
/* ********************************************************************** */

/*     REGLON normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: one interval in [0,2pi], no change needed.", 
	    (ftnlen)320, (ftnlen)56);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = 0.;
    bounds[1] = pi_();
    maxn = 1;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    size = nout << 1;
    moved_(bounds, &size, xbds);
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: one interval in [-pi,pi], no change needed.",
	     (ftnlen)320, (ftnlen)57);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = -pi_() / 2;
    bounds[1] = pi_() / 2;
    maxn = 1;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    size = nout << 1;
    moved_(bounds, &size, xbds);
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "-pi, pi], bounds have mixed signs.", (ftnlen)320, (ftnlen)91);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = pi_() / 2;
    bounds[1] = -pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_() / 2;
    xbds[1] = pi_() * 1.5;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "-pi, pi], bounds are both negative.", (ftnlen)320, (ftnlen)92);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = -pi_() / 4;
    bounds[1] = -pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = -pi_();
    xbds[1] = -pi_() / 2;
    xbds[2] = -pi_() / 4;
    xbds[3] = pi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__2, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "-pi, pi], only interval on right of lower bound is produced.", (
	    ftnlen)320, (ftnlen)117);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = pi_() / 2;
    bounds[1] = -pi_();
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. We expect it to have been */
/*     adjusted. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_() / 2;
    xbds[1] = pi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals, one interval has endpoints in reverse orde"
	    "r, range is [-pi, pi]. For reversed interval, only interval on r"
	    "ight of lower bound is produced.", (ftnlen)320, (ftnlen)153);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = pi_() / 2;
    bounds[1] = -pi_();
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_() / 2;
    xbds[1] = pi_();
    xbds[2] = -pi_() / 2;
    xbds[3] = pi_() / 2;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "-pi, pi], only interval on left of upper bound is produced.", (
	    ftnlen)320, (ftnlen)116);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = pi_();
    bounds[1] = -pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_();
    xbds[1] = pi_() * 1.5;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals, one interval has endpoints in reverse orde"
	    "r, range is [-pi, pi]. For reversed interval, only interval on l"
	    "eft of upper bound is produced.", (ftnlen)320, (ftnlen)152);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = pi_();
    bounds[1] = -pi_() / 2;
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);



    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = -pi_();
    xbds[1] = -pi_() / 2;
    xbds[2] = -pi_() / 2;
    xbds[3] = pi_() / 2;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "0, 2*pi].", (ftnlen)320, (ftnlen)66);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = pi_() / 2;
    bounds[1] = pi_() / 4;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = -pi_();
    xbds[1] = pi_() / 4;
    xbds[2] = pi_() / 2;
    xbds[3] = pi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__2, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "0, 2*pi], only interval on right of lower bound is produced.", (
	    ftnlen)320, (ftnlen)117);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = pi_() / 2;
    bounds[1] = 0.;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_() / 2;
    xbds[1] = twopi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals, one interval has endpoints in reverse orde"
	    "r, range is [0, 2*pi]. For reversed interval, only interval on r"
	    "ight of lower bound is produced.", (ftnlen)320, (ftnlen)153);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = pi_() * 1.5;
    bounds[1] = 0.;
    bounds[2] = pi_();
    bounds[3] = pi_() * 1.5;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = pi_() * 1.5;
    xbds[1] = twopi_();
    xbds[2] = pi_();
    xbds[3] = pi_() * 1.5;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "One interval, endpoints are in reverse order, range is  ["
	    "0, 2*pi], only interval on left of upper bound is produced.", (
	    ftnlen)320, (ftnlen)116);
    tcase_(title, (ftnlen)320);
    nivals = 1;
    bounds[0] = twopi_();
    bounds[1] = pi_() / 2;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 1;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = 0.;
    xbds[1] = pi_() / 2;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    chckai_("SRCS", srcs, "=", xsrcs, &c__1, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals, one interval has endpoints in reverse orde"
	    "r, range is [0, 2*pi]. For reversed interval, only interval on l"
	    "eft of upper bound is produced.", (ftnlen)320, (ftnlen)152);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = twopi_();
    bounds[1] = pi_() / 2;
    bounds[2] = pi_();
    bounds[3] = pi_() * 1.5;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    tol = 1e-14;
    xbds[0] = 0.;
    xbds[1] = pi_() / 2;
    xbds[2] = -pi_();
    xbds[3] = -pi_() / 2;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "~~", xbds, &size, &c_b9, ok, (ftnlen)6, (
	    ftnlen)2);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [0,2pi], no change needed.", (ftnlen)320, 
	    (ftnlen)43);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = pi_();
    bounds[2] = pi_() / 4;
    bounds[3] = pi_() / 3;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    chcksd_("MINLON", &minlon, "~", &c_b8, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = twopi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    size = nout << 1;
    moved_(bounds, &size, xbds);
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [0,2pi], one interval has reversed bound"
	    "s.", (ftnlen)320, (ftnlen)59);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = pi_();
    bounds[2] = pi_() / 3;
    bounds[3] = pi_() / 4;
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 3;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = 0.;
    xbds[1] = pi_();
    xbds[2] = -pi_();
    xbds[3] = pi_() / 4;
    xbds[4] = pi_() / 3;
    xbds[5] = pi_();
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    xsrcs[2] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [0,2pi], both intervals have reversed bo"
	    "unds.", (ftnlen)320, (ftnlen)62);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 8;
    bounds[2] = pi_() / 3;
    bounds[3] = pi_() / 4;
    maxn = 4;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 4;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = -pi_();
    xbds[1] = pi_() / 8;
    xbds[2] = pi_() / 4;
    xbds[3] = pi_();
    xbds[4] = -pi_();
    xbds[5] = pi_() / 4;
    xbds[6] = pi_() / 3;
    xbds[7] = pi_();
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 1;
    xsrcs[2] = 2;
    xsrcs[3] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [-pi,pi], no change needed.", (ftnlen)320,
	     (ftnlen)44);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    size = nout << 1;
    moved_(bounds, &size, xbds);
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [-2pi, 2pi], one interval must be moved "
	    "into the range [-pi, pi].", (ftnlen)320, (ftnlen)82);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = pi_();
    bounds[2] = pi_() * -3 / 2;
    bounds[3] = -pi_();
    maxn = 2;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = 0.;
    xbds[1] = pi_();
    xbds[2] = pi_() / 2;
    xbds[3] = pi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [-2pi, 2pi], one interval has reversed b"
	    "ounds and must be moved into the range [-pi, pi].", (ftnlen)320, (
	    ftnlen)106);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = -pi_() / 2;
    bounds[2] = pi_() * -3 / 2;
    bounds[3] = -pi_();
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 3;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = -pi_();
    xbds[1] = -pi_() / 2;
    xbds[2] = 0.;
    xbds[3] = pi_();
    xbds[4] = pi_() / 2;
    xbds[5] = pi_();
    size = nout << 1;
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 1;
    xsrcs[2] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [-2pi, 2pi], both intervals have reverse"
	    "d bounds and must be moved into the range [-pi, pi].", (ftnlen)
	    320, (ftnlen)109);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = -pi_() / 2;
    bounds[2] = pi_() * -3 / 2;
    bounds[3] = pi_() * -7 / 4;
    maxn = 4;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 4;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = -pi_();
    xbds[1] = -pi_() / 2;
    xbds[2] = 0.;
    xbds[3] = pi_();
    xbds[4] = -pi_();
    xbds[5] = pi_() / 4;
    xbds[6] = pi_() / 2;
    xbds[7] = pi_();
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 1;
    xsrcs[2] = 2;
    xsrcs[3] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Two intervals in [-2pi, 2pi], both intervals have reverse"
	    "d bounds and must be moved into the range [-pi, pi]. For the fir"
	    "st input interval, only the interval to the right of the lower b"
	    "ound is produced. For the second input interval, only the interv"
	    "al to the left of the upper bound is produced.", (ftnlen)320, (
	    ftnlen)295);
    tcase_(title, (ftnlen)320);
    nivals = 2;
    bounds[0] = pi_() * 3 / 2;
    bounds[1] = pi_();
    bounds[2] = -pi_();
    bounds[3] = pi_() * -7 / 4;
    maxn = 4;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the output range. */

    d__1 = -pi_();
    chcksd_("MINLON", &minlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);
    d__1 = pi_();
    chcksd_("MAXLON", &maxlon, "~", &d__1, &c_b9, ok, (ftnlen)6, (ftnlen)1);

/*     Check number of output intervals. */

    xnout = 2;
    chcksi_("NOUT", &nout, "=", &xnout, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Check output intervals. */

    xbds[0] = -pi_() / 2;
    xbds[1] = pi_();
    xbds[2] = -pi_();
    xbds[3] = pi_() / 4;
    size = nout << 1;
    chckad_("OUTBDS", outbds, "=", xbds, &size, &c_b8, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check source intervals. */

    xsrcs[0] = 1;
    xsrcs[1] = 2;
    chckai_("SRCS", srcs, "=", xsrcs, &nout, ok, (ftnlen)4, (ftnlen)1);
/* ********************************************************************** */

/*     REGLON error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Output array is too small.", (ftnlen)26);
    nivals = 2;
    bounds[0] = 0.;
    bounds[1] = pi_();
    bounds[2] = pi_() / 2;
    bounds[3] = -pi_() / 2;
    maxn = 1;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_true, "SPICE(ARRAYTOOSMALL)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Longitudes are out of range.", (ftnlen)28);
    nivals = 2;
    bounds[0] = pi_() * -3;
    bounds[1] = pi_();
    bounds[2] = pi_() / 2;
    bounds[3] = -pi_() / 2;
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    bounds[0] = pi_() * -2;
    bounds[1] = pi_() * 3;
    bounds[2] = pi_() / 2;
    bounds[3] = -pi_() / 2;
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Longitude bounds are equal.", (ftnlen)27);
    nivals = 2;
    bounds[0] = pi_() * -2;
    bounds[1] = pi_() * -2;
    bounds[2] = pi_() / 2;
    bounds[3] = -pi_() / 2;
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);
    bounds[0] = pi_() * -2;
    bounds[1] = pi_() * 2;
    bounds[2] = pi_() / 2;
    bounds[3] = pi_() / 2;
    maxn = 3;
    reglon_(&nivals, bounds, &maxn, &nout, &minlon, &maxlon, outbds, srcs);
    chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_reglon__ */

