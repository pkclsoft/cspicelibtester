/* f_sbf.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__24 = 24;
static logical c_false = FALSE_;
static integer c__8 = 8;
static integer c__3 = 3;
static integer c__10000 = 10000;
static integer c__100 = 100;
static integer c__0 = 0;
static integer c_b51 = 240000;
static doublereal c_b52 = 0.;
static integer c__80000 = 80000;
static integer c__1 = 1;
static integer c__30000 = 30000;
static integer c__6 = 6;
static integer c__499 = 499;
static integer c__2 = 2;
static logical c_true = TRUE_;

/* $Procedure F_SBF ( SBF tests ) */
/* Subroutine */ int f_sbf__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7, i__8, i__9, i__10;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_stop(char *
	    , ftnlen);
    integer i_dnnt(doublereal *);

    /* Local variables */
    static integer bids[100], xbod[100];
    static doublereal xrad;
    static integer xhan, axes[9]	/* was [3][3] */;
    static doublereal xoff[3];
    static char dsks[255*100];
    static doublereal last, xctr[3];
    extern /* Subroutine */ int t_smldsk__(integer *, integer *, char *, char 
	    *, ftnlen, ftnlen), t_topker__(char *, char *, char *, char *, 
	    integer *, integer *, char *, doublereal *, char *, integer *, 
	    doublereal *, doublereal *, integer *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), zzdsksba_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *, 
	    integer *, doublereal *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *), zzdsksbf_(integer *, integer *, 
	    integer *, doublereal *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *,
	     doublereal *, integer *, logical *, doublereal *), zzdsksbi_(
	    integer *, integer *, integer *, integer *, integer *, integer *, 
	    integer *, doublereal *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *), zzsegbox_(doublereal *, doublereal *,
	     doublereal *), zzdsksbr_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, doublereal 
	    *, integer *, integer *, doublereal *, doublereal *, doublereal *)
	    ;
    static integer i__, j, k;
    static char label[25];
    static integer p;
    static char frame[32];
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     dskgd_(integer *, integer *, doublereal *), tcase_(char *, 
	    ftnlen), filli_(integer *, integer *, integer *);
    static integer class__;
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer sthan[10000];
    static doublereal strad[10000], xdpar[240000], stoff[30000]	/* was [3][
	    10000] */;
    static char topfk[255*3];
    extern /* Subroutine */ int movei_(integer *, integer *, integer *), 
	    topen_(char *, ftnlen);
    static doublereal first, point[3], xform[9]	/* was [3][3] */;
    static integer nsurf;
    static doublereal stctr[30000]	/* was [3][10000] */;
    extern /* Subroutine */ int bodc2n_(integer *, char *, logical *, ftnlen),
	     t_success__(logical *);
    static doublereal dc[1];
    static integer ic[1];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static integer needed, ub;
    extern /* Subroutine */ int dlabbs_(integer *, integer *, logical *);
    extern doublereal pi_(void);
    static doublereal et, handle;
    static integer dladsc[8];
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *);
    static integer btcard;
    static doublereal lt;
    static integer segfid;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     cleari_(integer *, integer *), chcksi_(char *, integer *, char *,
	     integer *, integer *, logical *, ftnlen, ftnlen), delfil_(char *,
	     ftnlen), cleard_(integer *, doublereal *);
    static integer btnbod;
    static doublereal angles[9]	/* was [3][3] */;
    static integer stdlad[80000]	/* was [8][10000] */;
    static char frames[32*100];
    static integer btbody[100], btsegp[100], stfree;
    static doublereal stdscr[240000]	/* was [24][10000] */;
    extern logical exists_(char *, ftnlen);
    static char segfrm[32];
    static integer btstsz[100];
    static char sitfnm[32*3], sitnms[32*3], target[32], topspk[255*3];
    static doublereal dskdsc[24], normal[3], raydir[3], sitpos[9]	/* 
	    was [3][3] */, vertex[3], xdskds[24];
    static integer bodyid, clssid, fixfid, frmctr, handls[100], maxbod, 
	    prvdsc[8], segctr, sitfid[3], sitids[3], srflst[100], stcard, 
	    surfid, xdlads[8], xintar[80000];
    extern /* Subroutine */ int furnsh_(char *, ftnlen), dasopr_(char *, 
	    integer *, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), tstpck_(
	    char *, logical *, logical *, ftnlen), srfrec_(integer *, 
	    doublereal *, doublereal *, doublereal *), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen), unload_(char *, ftnlen),
	     dlafps_(integer *, integer *, integer *, logical *), pxform_(
	    char *, char *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    frinfo_(integer *, integer *, integer *, integer *, logical *), 
	    frmnam_(integer *, char *, ftnlen), chcksc_(char *, char *, char *
	    , char *, logical *, ftnlen, ftnlen, ftnlen, ftnlen), spkgps_(
	    integer *, doublereal *, char *, integer *, doublereal *, 
	    doublereal *, ftnlen);
    static doublereal lat;
    extern /* Subroutine */ int dascls_(integer *), kclear_(void);
    extern doublereal rpd_(void);
    static doublereal lon, tol;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;
    static doublereal xpt[3];

/* $ Abstract */

/*     Exercise the SPICELIB DSK "SBF" buffering subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB SBF routines: */

/*        ZZDSKSBA */
/*        ZZDSKSBF */
/*        ZZDSKSBI */
/*        ZZDSKSBR */
/*        ZZSBFXR */
/*        ZZSBFXRI */
/*        ZZSBFNRM */

/*     The routines */

/*        ZZSBFXR */
/*        ZZSBFXRI */
/*        ZZSBFNRM */

/*     are supported by routines having complex functionality: */

/*        ZZDSKBUN */
/*        ZZDSKBUX */

/*     Tests of those routines are carried out in families */
/*     dedicated to those routines. Tests performed here */
/*     of the higher-level surface intercept and surface normal */
/*     APIs are relatively superficial. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Body table variables */
/*     -------------------- */

/*        BTNBOD  is the number of bodies in the body table. */

/*        BTBODY  is an array of body ID codes. */

/*        BTSEGP  is an array of pointers (start indices) to entries in */
/*                the segment table. The Ith pointer indicates the start */
/*                index for entries for the Ith body. */

/*        BTSTSZ  is an array of segment table entry counts. The Ith */
/*                element of BTSTSZ is the number of entries in the */
/*                segment table for the Ith body. */



/*     Segment table variables */


/*     Miniature buffer sizes */


/*     Local Parameters */

/*      INTEGER               TITLEN */
/*      PARAMETER           ( TITLEN = 160 ) */

/*     Local Variables */

/*      CHARACTER*(TITLEN)    TITLE */

/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_SBF", (ftnlen)5);
/* *********************************************************************** */

/*     Test ZZDSKSBI */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBI initialization call", (ftnlen)28);
    maxbod = 100;

/*     Give the data structure arrays some initial values, so we */
/*     can verify the initialization procedure. */

    btnbod = 100;
    for (i__ = 1; i__ <= 100; ++i__) {
	btbody[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge("btbody", 
		i__1, "f_sbf__", (ftnlen)351)] = -i__;
	btstsz[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge("btstsz", 
		i__1, "f_sbf__", (ftnlen)352)] = i__;
    }
    for (i__ = 1; i__ <= 10000; ++i__) {
	sthan[(i__1 = i__ - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("sthan", 
		i__1, "f_sbf__", (ftnlen)359)] = -i__;
	d__1 = (doublereal) i__;
	filld_(&d__1, &c__24, &stdscr[(i__1 = i__ * 24 - 24) < 240000 && 0 <= 
		i__1 ? i__1 : s_rnge("stdscr", i__1, "f_sbf__", (ftnlen)361)])
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	filli_(&i__, &c__8, &stdlad[(i__1 = (i__ << 3) - 8) < 80000 && 0 <= 
		i__1 ? i__1 : s_rnge("stdlad", i__1, "f_sbf__", (ftnlen)364)])
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = (doublereal) i__;
	filld_(&d__1, &c__3, &stoff[(i__1 = i__ * 3 - 3) < 30000 && 0 <= i__1 
		? i__1 : s_rnge("stoff", i__1, "f_sbf__", (ftnlen)367)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = (doublereal) i__;
	filld_(&d__1, &c__3, &stctr[(i__1 = i__ * 3 - 3) < 30000 && 0 <= i__1 
		? i__1 : s_rnge("stctr", i__1, "f_sbf__", (ftnlen)370)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	strad[(i__1 = i__ - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("strad", 
		i__1, "f_sbf__", (ftnlen)373)] = (doublereal) i__;
    }
    stfree = 0;

/*     The call: */

    zzdsksbi_(&maxbod, &c__10000, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the body ID table. */

    cleari_(&c__100, xintar);
    chckai_("BTBODY", btbody, "=", xintar, &c__100, ok, (ftnlen)6, (ftnlen)1);

/*     Check the body table count. */

    chcksi_("BTNBOD", &btnbod, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the body table segment list sizes. */

    chckai_("BTSTSZ", btstsz, "=", xintar, &c__100, ok, (ftnlen)6, (ftnlen)1);

/*     Check the segment table handle array. */

    chckai_("STHAN", sthan, "=", xintar, &c__10000, ok, (ftnlen)5, (ftnlen)1);

/*     Check the segment table DSK descriptor array. */

    chckad_("STDSCR", stdscr, "=", xdpar, &c_b51, &c_b52, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the segment table DLA descriptor array. */

    chckai_("STDLAD", stdlad, "=", xintar, &c__80000, ok, (ftnlen)6, (ftnlen)
	    1);

/*     Check the segment table free element index. The first */
/*     free element is at index 1. */

    chcksi_("STFREE", &stfree, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the segment table bounding sphere center array. */

    chckad_("STCTR", stctr, "=", xdpar, &c__30000, &c_b52, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Check the segment table bounding sphere radius array. */

    chckad_("STRAD", strad, "=", xdpar, &c__10000, &c_b52, ok, (ftnlen)5, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Test ZZDSKSBA */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple case: add data for a body having one segment.", (ftnlen)52)
	    ;
    s_copy(dsks, "zzdsksbf_test_1.bds", (ftnlen)255, (ftnlen)19);
    if (exists_(dsks, (ftnlen)255)) {
	delfil_(dsks, (ftnlen)255);
    }

/*     Create a trivial DSK for Mars. */

    bodyid = 499;
    surfid = 1;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    t_smldsk__(&bodyid, &surfid, frame, dsks, (ftnlen)32, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the DSK. */

    furnsh_(dsks, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the SBF data structures. Use the miniature buffers. */

    zzdsksbi_(&c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add data for Mars to the buffer. */

    zzdsksba_(&bodyid, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now check the buffer contents. */

/*     The body table should have just one entry. */

    chcksi_("BTNBOD", &btnbod, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("BTBODY(1)", btbody, "=", &c__499, &c__0, ok, (ftnlen)9, (ftnlen)
	    1);

/*     The segment table should contain one entry for Mars. */

    chcksi_("BTSTSZ(1)", btstsz, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     The segment table pointer should point to the first */
/*     element of the segment table. */

    chcksi_("BTSEGP(1)", btsegp, "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Get the expected handle and descriptors for the Mars segment. */

    dasopr_(dsks, handls, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xhan = handls[0];
    dlabfs_(&xhan, xdlads, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskgd_(&xhan, xdlads, xdskds);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check these items in the buffers. */

    chcksi_("STHAN(1)", sthan, "=", &xhan, &c__0, ok, (ftnlen)8, (ftnlen)1);
    chckai_("STDLAD(*,1)", stdlad, "=", xdlads, &c__8, ok, (ftnlen)11, (
	    ftnlen)1);
    chckad_("STDSKD(*,1)", stdscr, "=", xdskds, &c__24, &c_b52, ok, (ftnlen)
	    11, (ftnlen)1);

/*     Check the segment table "free" index. */

    chcksi_("STFREE", &stfree, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the segment frame center offset. The body and frame */
/*     center coincide. */

    cleard_(&c__3, xoff);
    chckad_("STOFF(*,1)", stoff, "=", xoff, &c__3, &c_b52, ok, (ftnlen)10, (
	    ftnlen)1);

/*     Get the expected bounding sphere's center and radius. Check the */
/*     buffered values. */

    zzsegbox_(xdskds, xctr, &xrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STCTR(*,1)", stctr, "=", xctr, &c__3, &c_b52, ok, (ftnlen)10, (
	    ftnlen)1);
    chcksd_("STRAD(1)", strad, "=", &xrad, &c_b52, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: try to buffer Mars data again, without re-initializi"
	    "ng first.", (ftnlen)73);
    zzdsksba_(&bodyid, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(INVALIDADD)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Remove the Mars entry from the buffers.", (ftnlen)39);

/*     The removal is accomplished by "making room" for the */
/*     maximum number of segment table entries. */

    needed = 6;
    zzdsksbr_(&needed, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The body table should be empty. */

    chcksi_("BTNBOD", &btnbod, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the segment table "free" index. */

    chcksi_("STFREE", &stfree, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create DSKs for Venus, Jupiter and Saturn.", (ftnlen)42);

/*     We need to have a DSK for a body other Mars loaded */
/*     in order to call ZZDSKSBA in the next test. */


/*     These files will contain two segments each. */

    bids[1] = 299;
    bids[2] = 599;
    bids[3] = 699;
    s_copy(frames + 32, "IAU_VENUS", (ftnlen)32, (ftnlen)9);
    s_copy(frames + 64, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    s_copy(frames + 96, "IAU_SATURN", (ftnlen)32, (ftnlen)10);

/*     We'll use a topocentric frame for one segment for each */
/*     body; the other segments will use body-centered frames. */

/*     We'll need a generic PCK: */

    if (exists_("sbf_test0.tpc", (ftnlen)13)) {
	delfil_("sbf_test0.tpc", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tstpck_("sbf_test0.tpc", &c_true, &c_true, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     Create the FKs and SPKs needed to support topocentric frames. */

    sitids[0] = 299001;
    sitids[1] = 599001;
    sitids[2] = 699001;
    s_copy(sitnms, "VENUS_SURFACE_SITE", (ftnlen)32, (ftnlen)18);
    s_copy(sitnms + 32, "JUPITER_SURFACE_SITE", (ftnlen)32, (ftnlen)20);
    s_copy(sitnms + 64, "SATURN_SURFACE_SITE", (ftnlen)32, (ftnlen)19);
    for (i__ = 1; i__ <= 3; ++i__) {
	lon = ((i__ - 1) * 3 + 30.) * rpd_();
	lat = ((i__ - 1 << 1) + 45.) * rpd_();
	srfrec_(&bids[(i__1 = i__) < 100 && 0 <= i__1 ? i__1 : s_rnge("bids", 
		i__1, "f_sbf__", (ftnlen)663)], &lon, &lat, &sitpos[(i__2 = 
		i__ * 3 - 3) < 9 && 0 <= i__2 ? i__2 : s_rnge("sitpos", i__2, 
		"f_sbf__", (ftnlen)663)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    s_copy(sitfnm, "VENUS_TOPO", (ftnlen)32, (ftnlen)10);
    s_copy(sitfnm + 32, "JUPITER_TOPO", (ftnlen)32, (ftnlen)12);
    s_copy(sitfnm + 64, "SATURN_TOPO", (ftnlen)32, (ftnlen)11);
    for (i__ = 1; i__ <= 3; ++i__) {
	sitfid[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("sitfid", 
		i__1, "f_sbf__", (ftnlen)673)] = sitids[(i__2 = i__ - 1) < 3 
		&& 0 <= i__2 ? i__2 : s_rnge("sitids", i__2, "f_sbf__", (
		ftnlen)673)];
    }
    first = jyear_() * -100;
    last = jyear_() * 100;
    for (i__ = 1; i__ <= 3; ++i__) {
	axes[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_sbf__", (ftnlen)681)] = 3;
	axes[(i__1 = i__ * 3 - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_sbf__", (ftnlen)682)] = 2;
	axes[(i__1 = i__ * 3 - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_sbf__", (ftnlen)683)] = 3;
	lon = ((i__ - 1) * 3 + 30.) * rpd_();
	lat = ((i__ - 1 << 1) + 45.) * rpd_();
	angles[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_sbf__", (ftnlen)688)] = -lon;
	angles[(i__1 = i__ * 3 - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_sbf__", (ftnlen)689)] = lat - pi_() / 2;
	angles[(i__1 = i__ * 3 - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_sbf__", (ftnlen)690)] = pi_();
    }

/*     Create topocentric kernels. We'll need to create */
/*     separate kernels for the different target bodies. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topfk", i__1, "f_sbf__", (ftnlen)700)) * 255, "sbf_test_top"
		"o_#.tf", (ftnlen)255, (ftnlen)18);
	repmi_(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topfk", i__1, "f_sbf__", (ftnlen)701)) * 255, "#", &i__, 
		topfk + ((i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		"topfk", i__2, "f_sbf__", (ftnlen)701)) * 255, (ftnlen)255, (
		ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topspk", i__1, "f_sbf__", (ftnlen)704)) * 255, "sbf_test_to"
		"po_#.bsp", (ftnlen)255, (ftnlen)19);
	repmi_(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topspk", i__1, "f_sbf__", (ftnlen)705)) * 255, "#", &i__, 
		topspk + ((i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		"topspk", i__2, "f_sbf__", (ftnlen)705)) * 255, (ftnlen)255, (
		ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	bodc2n_(&bids[(i__1 = i__) < 100 && 0 <= i__1 ? i__1 : s_rnge("bids", 
		i__1, "f_sbf__", (ftnlen)708)], target, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("body name found", &found, &c_true, ok, (ftnlen)15);
	if (exists_(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("topfk", i__1, "f_sbf__", (ftnlen)712)) * 255, (ftnlen)
		255)) {
	    delfil_(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("topfk", i__1, "f_sbf__", (ftnlen)713)) * 255, (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	if (exists_(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("topspk", i__1, "f_sbf__", (ftnlen)717)) * 255, (
		ftnlen)255)) {
	    delfil_(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("topspk", i__1, "f_sbf__", (ftnlen)718)) * 255, (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	t_topker__(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topfk", i__1, "f_sbf__", (ftnlen)723)) * 255, topspk + ((
		i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("topspk", 
		i__2, "f_sbf__", (ftnlen)723)) * 255, target, frames + (((
		i__3 = i__) < 100 && 0 <= i__3 ? i__3 : s_rnge("frames", i__3,
		 "f_sbf__", (ftnlen)723)) << 5), &c__1, &sitids[(i__4 = i__ - 
		1) < 3 && 0 <= i__4 ? i__4 : s_rnge("sitids", i__4, "f_sbf__",
		 (ftnlen)723)], sitnms + (((i__5 = i__ - 1) < 3 && 0 <= i__5 ?
		 i__5 : s_rnge("sitnms", i__5, "f_sbf__", (ftnlen)723)) << 5),
		 &sitpos[(i__6 = i__ * 3 - 3) < 9 && 0 <= i__6 ? i__6 : 
		s_rnge("sitpos", i__6, "f_sbf__", (ftnlen)723)], sitfnm + (((
		i__7 = i__ - 1) < 3 && 0 <= i__7 ? i__7 : s_rnge("sitfnm", 
		i__7, "f_sbf__", (ftnlen)723)) << 5), &sitfid[(i__8 = i__ - 1)
		 < 3 && 0 <= i__8 ? i__8 : s_rnge("sitfid", i__8, "f_sbf__", (
		ftnlen)723)], &first, &last, &axes[(i__9 = i__ * 3 - 3) < 9 &&
		 0 <= i__9 ? i__9 : s_rnge("axes", i__9, "f_sbf__", (ftnlen)
		723)], &angles[(i__10 = i__ * 3 - 3) < 9 && 0 <= i__10 ? 
		i__10 : s_rnge("angles", i__10, "f_sbf__", (ftnlen)723)], (
		ftnlen)255, (ftnlen)255, (ftnlen)32, (ftnlen)32, (ftnlen)32, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Load the kernels. */

	furnsh_(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topfk", i__1, "f_sbf__", (ftnlen)733)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	furnsh_(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topspk", i__1, "f_sbf__", (ftnlen)736)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Now create the DSK files. */

    for (i__ = 2; i__ <= 4; ++i__) {
	s_copy(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)746)) * 255, "zzdsksbf_test"
		"_#.bds", (ftnlen)255, (ftnlen)19);
	repmi_(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)747)) * 255, "#", &i__, dsks 
		+ ((i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("dsks",
		 i__2, "f_sbf__", (ftnlen)747)) * 255, (ftnlen)255, (ftnlen)1,
		 (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_sbf__", (ftnlen)750)) * 255, (ftnlen)
		255)) {
	    delfil_(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		    s_rnge("dsks", i__1, "f_sbf__", (ftnlen)751)) * 255, (
		    ftnlen)255);
	}
	for (j = 1; j <= 2; ++j) {
	    surfid = j;
	    bodyid = bids[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		    "bids", i__1, "f_sbf__", (ftnlen)757)];
	    if (j == 1) {
		s_copy(frame, frames + (((i__1 = i__ - 1) < 100 && 0 <= i__1 ?
			 i__1 : s_rnge("frames", i__1, "f_sbf__", (ftnlen)760)
			) << 5), (ftnlen)32, (ftnlen)32);
	    } else {
		s_copy(frame, sitfnm + (((i__1 = i__ - 2) < 3 && 0 <= i__1 ? 
			i__1 : s_rnge("sitfnm", i__1, "f_sbf__", (ftnlen)762))
			 << 5), (ftnlen)32, (ftnlen)32);
	    }
	    t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__1 = i__ - 1) < 
		    100 && 0 <= i__1 ? i__1 : s_rnge("dsks", i__1, "f_sbf__", 
		    (ftnlen)765)) * 255, (ftnlen)32, (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload the Mars DSK. Try to populate buffers. This is not an err"
	    "or, but there should be no Mars data in the buffer.", (ftnlen)115)
	    ;
    unload_(dsks, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a Jupiter DSK so the BSR subsystem will agree to function. */

    furnsh_(dsks + 255, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksba_(&bodyid, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The body table should contain an entry for Mars, but */
/*     there should be no segment data. */

    chcksi_("BTNBOD", &btnbod, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("BTSTSZ", btstsz, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the segment table "free" index. */

    chcksi_("STFREE", &stfree, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Re-initialize, then Buffer data for Venus, Jupiter, and Saturn.", 
	    (ftnlen)63);

/*     Load kernels. */

    for (i__ = 2; i__ <= 4; ++i__) {
	furnsh_(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)825)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Reinitialize the buffers. */

    bids[1] = 299;
    bids[2] = 599;
    bids[3] = 699;
    zzdsksbi_(&c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 2; i__ <= 4; ++i__) {
	zzdsksba_(&bids[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"bids", i__1, "f_sbf__", (ftnlen)845)], &c__3, &c__6, btbody, 
		&btnbod, btsegp, btstsz, sthan, stdscr, stdlad, &stfree, 
		stoff, stctr, strad);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     The body table should have three entries. */

    chcksi_("BTNBOD", &btnbod, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "BTBODY(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &btbody[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("btbody", i__1, "f_sbf__", (ftnlen)866)], "=", &bids[(
		i__2 = i__) < 100 && 0 <= i__2 ? i__2 : s_rnge("bids", i__2, 
		"f_sbf__", (ftnlen)866)], &c__0, ok, (ftnlen)25, (ftnlen)1);
    }

/*     The segment table should contain two entries for each body. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "BTSTSZ(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &btstsz[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("btstsz", i__1, "f_sbf__", (ftnlen)879)], "=", &c__2, &
		c__0, ok, (ftnlen)25, (ftnlen)1);
    }

/*     The segment table pointers should point to the successive */
/*     odd-indexed elements of the table. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "BTSEGP(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = (i__ << 1) - 1;
	chcksi_("BTSEGP", &btsegp[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 :
		 s_rnge("btsegp", i__1, "f_sbf__", (ftnlen)894)], "=", &i__2, 
		&c__0, ok, (ftnlen)6, (ftnlen)1);
    }

/*     Check the segment table. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Check segment data for body I. Open the DSK for this body. */

	k = i__ + 1;
	dasopr_(dsks + ((i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)907)) * 255, &handls[(i__2 = 
		k - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
		"f_sbf__", (ftnlen)907)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 2; ++j) {

/*           Check the entry for the Jth segment. */

/*           Get the expected DLA descriptor from the DSK file. */

	    if (j == 1) {
		dlabbs_(&handls[(i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : 
			s_rnge("handls", i__1, "f_sbf__", (ftnlen)919)], 
			xdlads, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    } else {
		dlafps_(&handls[(i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : 
			s_rnge("handls", i__1, "f_sbf__", (ftnlen)926)], 
			xdlads, prvdsc, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		movei_(prvdsc, &c__8, xdlads);
	    }

/*           Let P be the start index of the current entry in the */
/*           segment table. */

	    p = btsegp[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		    "btsegp", i__1, "f_sbf__", (ftnlen)939)] - 1 + j;

/*           Check the file handle in the segment table. */

	    chcksi_("HANDLE", &sthan[(i__1 = p - 1) < 10000 && 0 <= i__1 ? 
		    i__1 : s_rnge("sthan", i__1, "f_sbf__", (ftnlen)944)], 
		    "=", &handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : 
		    s_rnge("handls", i__2, "f_sbf__", (ftnlen)944)], &c__0, 
		    ok, (ftnlen)6, (ftnlen)1);

/*           Get the expected DSK descriptor. */

	    dskgd_(&handls[(i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		    "handls", i__1, "f_sbf__", (ftnlen)949)], xdlads, xdskds);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the buffered DLA descriptor for the current segment. */

	    chckai_("DLADSC", &stdlad[(i__1 = (p << 3) - 8) < 80000 && 0 <= 
		    i__1 ? i__1 : s_rnge("stdlad", i__1, "f_sbf__", (ftnlen)
		    956)], "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)1);

/*           Check the buffered DSK descriptor for the current segment. */

	    chckad_("DSKDSC", &stdscr[(i__1 = p * 24 - 24) < 240000 && 0 <= 
		    i__1 ? i__1 : s_rnge("stdscr", i__1, "f_sbf__", (ftnlen)
		    962)], "=", xdskds, &c__24, &c_b52, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the segment frame center offset. */

	    if (j == 1) {

/*              The segment uses a topocentric frame. We must */
/*              convert the site position into this frame in */
/*              order to generate the buffered vector. */

/*              The rotation between the frames is time-independent. */

		pxform_(frames + (((i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : 
			s_rnge("frames", i__1, "f_sbf__", (ftnlen)976)) << 5),
			 sitfnm + (((i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 :
			 s_rnge("sitfnm", i__2, "f_sbf__", (ftnlen)976)) << 5)
			, &c_b52, xform, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		mxv_(xform, &sitpos[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? 
			i__1 : s_rnge("sitpos", i__1, "f_sbf__", (ftnlen)979)]
			, xoff);
	    } else {

/*              The body and frame center coincide. */

		cleard_(&c__3, xoff);
	    }
	    tol = 1e-12;
	    chckad_("STOFF(*,P)", &stoff[(i__1 = p * 3 - 3) < 30000 && 0 <= 
		    i__1 ? i__1 : s_rnge("stoff", i__1, "f_sbf__", (ftnlen)
		    991)], "~~/", xoff, &c__3, &tol, ok, (ftnlen)10, (ftnlen)
		    3);

/*           Get the expected bounding sphere's center and radius. Check */
/*           the buffered values. */

	    zzsegbox_(xdskds, xctr, &xrad);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("STCTR(*,P)", &stctr[(i__1 = p * 3 - 3) < 30000 && 0 <= 
		    i__1 ? i__1 : s_rnge("stctr", i__1, "f_sbf__", (ftnlen)
		    1001)], "=", xctr, &c__3, &c_b52, ok, (ftnlen)10, (ftnlen)
		    1);
	    chcksd_("STRAD(P)", &strad[(i__1 = p - 1) < 10000 && 0 <= i__1 ? 
		    i__1 : s_rnge("strad", i__1, "f_sbf__", (ftnlen)1004)], 
		    "=", &xrad, &c_b52, ok, (ftnlen)8, (ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Buffer data for Mars. This should cause data for Venus to be del"
	    "eted.", (ftnlen)69);
    furnsh_(dsks, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bids[0] = 499;
    zzdsksba_(bids, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The body table should have three entries. */

    chcksi_("BTNBOD", &btnbod, "=", &c__3, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the body IDs in the body table. */

    xbod[0] = 599;
    xbod[1] = 699;
    xbod[2] = 499;
    i__1 = btnbod;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "BTBODY(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &btbody[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : 
		s_rnge("btbody", i__2, "f_sbf__", (ftnlen)1050)], "=", &xbod[(
		i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge("xbod", 
		i__3, "f_sbf__", (ftnlen)1050)], &c__0, ok, (ftnlen)25, (
		ftnlen)1);
    }

/*     The segment table should contain two entries for each body */
/*     except for Mars. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "BTSTSZ(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (i__ == 3) {
	    j = 1;
	} else {
	    j = 2;
	}
	chcksi_(label, &btstsz[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("btstsz", i__1, "f_sbf__", (ftnlen)1071)], "=", &j, &
		c__0, ok, (ftnlen)25, (ftnlen)1);
    }

/*     The segment table pointers should point to the successive */
/*     odd-indexed elements of the table. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "BTSEGP(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = (i__ << 1) - 1;
	chcksi_("BTSEGP", &btsegp[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 :
		 s_rnge("btsegp", i__1, "f_sbf__", (ftnlen)1085)], "=", &i__2,
		 &c__0, ok, (ftnlen)6, (ftnlen)1);
    }

/*     Check the segment table. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Check segment data for body I. Open the DSK for this body. */

	if (i__ == 3) {
	    k = 1;
	} else {
	    k = i__ + 2;
	}
	dasopr_(dsks + ((i__1 = k - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)1104)) * 255, &handls[(i__2 =
		 k - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
		"f_sbf__", (ftnlen)1104)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Let UB be the upper bound of the segment loop. */

	if (i__ == 3) {
	    ub = 1;
	} else {
	    ub = 2;
	}
	i__1 = ub;
	for (j = 1; j <= i__1; ++j) {

/*           Check the entry for the Jth segment. */

/*           Get the expected DLA descriptor from the DSK file. */

	    if (j == 1) {
		dlabbs_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : 
			s_rnge("handls", i__2, "f_sbf__", (ftnlen)1124)], 
			xdlads, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    } else {
		dlafps_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : 
			s_rnge("handls", i__2, "f_sbf__", (ftnlen)1131)], 
			xdlads, prvdsc, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		movei_(prvdsc, &c__8, xdlads);
	    }

/*           Let P be the start index of the current entry in the */
/*           segment table. */

	    p = btsegp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "btsegp", i__2, "f_sbf__", (ftnlen)1144)] - 1 + j;

/*           Check the file handle in the segment table. */

	    chcksi_("HANDLE", &sthan[(i__2 = p - 1) < 10000 && 0 <= i__2 ? 
		    i__2 : s_rnge("sthan", i__2, "f_sbf__", (ftnlen)1150)], 
		    "=", &handls[(i__3 = k - 1) < 100 && 0 <= i__3 ? i__3 : 
		    s_rnge("handls", i__3, "f_sbf__", (ftnlen)1150)], &c__0, 
		    ok, (ftnlen)6, (ftnlen)1);
	    if (! (*ok)) {
		s_stop("", (ftnlen)0);
	    }

/*           Get the expected DSK descriptor. */

	    dskgd_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "handls", i__2, "f_sbf__", (ftnlen)1157)], xdlads, xdskds)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the buffered DLA descriptor for the current segment. */

	    chckai_("DLADSC", &stdlad[(i__2 = (p << 3) - 8) < 80000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stdlad", i__2, "f_sbf__", (ftnlen)
		    1164)], "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)1);

/*           Check the buffered DSK descriptor for the current segment. */

	    chckad_("DSKDSC", &stdscr[(i__2 = p * 24 - 24) < 240000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stdscr", i__2, "f_sbf__", (ftnlen)
		    1170)], "=", xdskds, &c__24, &c_b52, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the segment frame center offset. Get the expected */
/*           offset first. */

	    segctr = i_dnnt(&xdskds[1]);
	    segfid = i_dnnt(&xdskds[4]);
	    frinfo_(&segfid, &frmctr, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("info found", &found, &c_true, ok, (ftnlen)10);
	    if (frmctr != segctr) {
		frmnam_(&segfid, segfrm, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_("SEGFRM", segfrm, "!=", " ", ok, (ftnlen)6, (ftnlen)
			32, (ftnlen)2, (ftnlen)1);
		spkgps_(&frmctr, &c_b52, segfrm, &segctr, xoff, &lt, (ftnlen)
			32);
		chckad_("STOFF(*,P)", &stoff[(i__2 = p * 3 - 3) < 30000 && 0 
			<= i__2 ? i__2 : s_rnge("stoff", i__2, "f_sbf__", (
			ftnlen)1197)], "=", xoff, &c__3, &c_b52, ok, (ftnlen)
			10, (ftnlen)1);
	    } else {

/*              The frame center and segment's central body match. */

		cleard_(&c__3, xoff);
	    }

/*           Get the expected bounding sphere's center and radius. Check */
/*           the buffered values. */

	    zzsegbox_(xdskds, xctr, &xrad);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("STCTR(*,P)", &stctr[(i__2 = p * 3 - 3) < 30000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stctr", i__2, "f_sbf__", (ftnlen)
		    1214)], "=", xctr, &c__3, &c_b52, ok, (ftnlen)10, (ftnlen)
		    1);
	    chcksd_("STRAD(P)", &strad[(i__2 = p - 1) < 10000 && 0 <= i__2 ? 
		    i__2 : s_rnge("strad", i__2, "f_sbf__", (ftnlen)1217)], 
		    "=", &xrad, &c_b52, ok, (ftnlen)8, (ftnlen)1);
	}
    }

/*     Close files opened by DASOPR. */

    for (i__ = 1; i__ <= 4; ++i__) {
	dascls_(&handls[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"handls", i__1, "f_sbf__", (ftnlen)1229)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* ********************************************************************* */
/* * */
/* *    ZZDSKSBA Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBA: insufficient room in segment table.", (ftnlen)45);
    zzdsksbi_(&c__3, &c__1, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bids[0] = 599;
    zzdsksba_(bids, &c__3, &c__1, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(SEGMENTTABLEFULL)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBA: missing SPK data; cannot create offsets.", (ftnlen)50);
    unload_(topspk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksbi_(&c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bids[0] = 299;
    zzdsksba_(bids, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/*     Restore SPK file. */

    furnsh_(topspk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBA: missing FK data; cannot create offsets.", (ftnlen)49);
    unload_(topfk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unloading the file has the side effect of unloading the */
/*     test PCK as well. Restore this file. */

    furnsh_("sbf_test0.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksbi_(&c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bids[0] = 299;
    zzdsksba_(bids, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(NOFRAMEINFO)", ok, (ftnlen)18);

/*     Restore FK file. */

    furnsh_(topfk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBA: missing PCK data; cannot create offsets.", (ftnlen)50);
    unload_("sbf_test0.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unloading the file has the side effect of unloading the */
/*     FK as well. Restore this file. */

    furnsh_(topfk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksbi_(&c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, stdscr, 
	    stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bids[0] = 299;
    zzdsksba_(bids, &c__3, &c__6, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Restore FK file. */

    furnsh_("sbf_test0.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */

/*     Test ZZDSKSBR */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */
    tcase_("Store 7 segments; make room for 2.", (ftnlen)34);

/*     Re-initialize. */

    btcard = 4;
    stcard = 7;
    zzdsksbi_(&btcard, &stcard, btbody, &btnbod, btsegp, btstsz, sthan, 
	    stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Insert segment data for bodies 499, 299, 599, and 699. */

    bids[0] = 499;
    bids[1] = 299;
    bids[2] = 599;
    bids[3] = 699;
    for (i__ = 1; i__ <= 4; ++i__) {
	zzdsksba_(&bids[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"bids", i__1, "f_sbf__", (ftnlen)1416)], &btcard, &stcard, 
		btbody, &btnbod, btsegp, btstsz, sthan, stdscr, stdlad, &
		stfree, stoff, stctr, strad);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Make room for 2 segments. */

    needed = 2;
    zzdsksbr_(&needed, &btcard, &stcard, btbody, &btnbod, btsegp, btstsz, 
	    sthan, stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The tables should contain data only for 599 and 699. */


/*     The body table should have two entries. */

    chcksi_("BTNBOD", &btnbod, "=", &c__2, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the body IDs in the body table. */

    xbod[0] = 599;
    xbod[1] = 699;
    i__1 = btnbod;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "BTBODY(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &btbody[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : 
		s_rnge("btbody", i__2, "f_sbf__", (ftnlen)1456)], "=", &xbod[(
		i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge("xbod", 
		i__3, "f_sbf__", (ftnlen)1456)], &c__0, ok, (ftnlen)25, (
		ftnlen)1);
    }

/*     The segment table should contain two entries for each body. */

    for (i__ = 1; i__ <= 2; ++i__) {
	s_copy(label, "BTSTSZ(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &btstsz[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : 
		s_rnge("btstsz", i__1, "f_sbf__", (ftnlen)1469)], "=", &c__2, 
		&c__0, ok, (ftnlen)25, (ftnlen)1);
    }

/*     The segment table pointers should point to the successive */
/*     odd-indexed elements of the table. */

    for (i__ = 1; i__ <= 2; ++i__) {
	s_copy(label, "BTSEGP(@)", (ftnlen)25, (ftnlen)9);
	repmi_(label, "@", &i__, label, (ftnlen)25, (ftnlen)1, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__2 = (i__ << 1) - 1;
	chcksi_("BTSEGP", &btsegp[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 :
		 s_rnge("btsegp", i__1, "f_sbf__", (ftnlen)1483)], "=", &i__2,
		 &c__0, ok, (ftnlen)6, (ftnlen)1);
    }

/*     Check the segment table. */

    i__1 = btnbod;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Check segment data for body I. Open the DSK for this body. */

	k = i__ + 2;
	dasopr_(dsks + ((i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_sbf__", (ftnlen)1497)) * 255, &handls[(i__3 =
		 k - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge("handls", i__3, 
		"f_sbf__", (ftnlen)1497)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 2; ++j) {

/*           Check the entry for the Jth segment. */

/*           Get the expected DLA descriptor from the DSK file. */

	    if (j == 1) {
		dlabbs_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : 
			s_rnge("handls", i__2, "f_sbf__", (ftnlen)1508)], 
			xdlads, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    } else {
		dlafps_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : 
			s_rnge("handls", i__2, "f_sbf__", (ftnlen)1515)], 
			xdlads, prvdsc, &found);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
		movei_(prvdsc, &c__8, xdlads);
	    }

/*           Let P be the start index of the current entry in the */
/*           segment table. */

	    p = btsegp[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "btsegp", i__2, "f_sbf__", (ftnlen)1528)] - 1 + j;

/*           Check the file handle in the segment table. */

	    chcksi_("HANDLE", &sthan[(i__2 = p - 1) < 10000 && 0 <= i__2 ? 
		    i__2 : s_rnge("sthan", i__2, "f_sbf__", (ftnlen)1533)], 
		    "=", &handls[(i__3 = k - 1) < 100 && 0 <= i__3 ? i__3 : 
		    s_rnge("handls", i__3, "f_sbf__", (ftnlen)1533)], &c__0, 
		    ok, (ftnlen)6, (ftnlen)1);
	    if (! (*ok)) {
		s_stop("", (ftnlen)0);
	    }

/*           Get the expected DSK descriptor. */

	    dskgd_(&handls[(i__2 = k - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		    "handls", i__2, "f_sbf__", (ftnlen)1540)], xdlads, xdskds)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the buffered DLA descriptor for the current segment. */

	    chckai_("DLADSC", &stdlad[(i__2 = (p << 3) - 8) < 80000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stdlad", i__2, "f_sbf__", (ftnlen)
		    1547)], "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)1);

/*           Check the buffered DSK descriptor for the current segment. */

	    chckad_("DSKDSC", &stdscr[(i__2 = p * 24 - 24) < 240000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stdscr", i__2, "f_sbf__", (ftnlen)
		    1553)], "=", xdskds, &c__24, &c_b52, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the segment frame center offset. Get the expected */
/*           offset first. */

	    segctr = i_dnnt(&xdskds[1]);
	    segfid = i_dnnt(&xdskds[4]);
	    frinfo_(&segfid, &frmctr, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("info found", &found, &c_true, ok, (ftnlen)10);
	    if (frmctr != segctr) {
		frmnam_(&segfid, segfrm, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_("SEGFRM", segfrm, "!=", " ", ok, (ftnlen)6, (ftnlen)
			32, (ftnlen)2, (ftnlen)1);
		spkgps_(&frmctr, &c_b52, segfrm, &segctr, xoff, &lt, (ftnlen)
			32);
		chckad_("STOFF(*,P)", &stoff[(i__2 = p * 3 - 3) < 30000 && 0 
			<= i__2 ? i__2 : s_rnge("stoff", i__2, "f_sbf__", (
			ftnlen)1581)], "=", xoff, &c__3, &c_b52, ok, (ftnlen)
			10, (ftnlen)1);
	    } else {

/*              The frame center and segment's central body match. */

		cleard_(&c__3, xoff);
	    }

/*           Check the segment frame center offset. The body and frame */
/*           center coincide. */

	    chckad_("STOFF(*,P)", &stoff[(i__2 = p * 3 - 3) < 30000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stoff", i__2, "f_sbf__", (ftnlen)
		    1595)], "=", xoff, &c__3, &c_b52, ok, (ftnlen)10, (ftnlen)
		    1);

/*           Get the expected bounding sphere's center and radius. Check */
/*           the buffered values. */

	    zzsegbox_(xdskds, xctr, &xrad);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("STCTR(*,P)", &stctr[(i__2 = p * 3 - 3) < 30000 && 0 <= 
		    i__2 ? i__2 : s_rnge("stctr", i__2, "f_sbf__", (ftnlen)
		    1605)], "=", xctr, &c__3, &c_b52, ok, (ftnlen)10, (ftnlen)
		    1);
	    chcksd_("STRAD(P)", &strad[(i__2 = p - 1) < 10000 && 0 <= i__2 ? 
		    i__2 : s_rnge("strad", i__2, "f_sbf__", (ftnlen)1608)], 
		    "=", &xrad, &c_b52, ok, (ftnlen)8, (ftnlen)1);
	}
    }

/*     Close files opened by DASOPR. */

    for (i__ = 1; i__ <= 4; ++i__) {
	dascls_(&handls[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"handls", i__1, "f_sbf__", (ftnlen)1620)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* *********************************************************************** */

/*     ZZDSKSBR error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Ask for more room than the segment table has.", (ftnlen)45);
    needed = stcard + 1;
    zzdsksbr_(&needed, &btcard, &stcard, btbody, &btnbod, btsegp, btstsz, 
	    sthan, stdscr, stdlad, &stfree, stoff, stctr, strad);
    chckxc_(&c_true, "SPICE(SEGTABLETOOSMALL)", ok, (ftnlen)23);
/* ********************************************************************* */
/* * */
/* *    ZZDSKSBF Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Direct ZZDSKSBF call.", (ftnlen)21);
    zzdsksbf_(&bodyid, &nsurf, srflst, &et, &fixfid, vertex, raydir, point, 
	    xpt, &handle, dladsc, dskdsc, dc, ic, &found, normal);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
/* ********************************************************************* */
/* * */
/* *    Clean up */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    kclear_();
    delfil_("sbf_test0.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	dascls_(&handls[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"handls", i__1, "f_sbf__", (ftnlen)1690)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(dsks + ((i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_sbf__", (ftnlen)1693)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 3; ++i__) {
	delfil_(topfk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topfk", i__1, "f_sbf__", (ftnlen)1700)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(topspk + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"topspk", i__1, "f_sbf__", (ftnlen)1703)) * 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_sbf__ */

