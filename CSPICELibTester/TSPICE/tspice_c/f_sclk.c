/* f_sclk.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static doublereal c_b68 = 0.;
static doublereal c_b255 = 1e-14;
static integer c__4 = 4;
static doublereal c_b377 = 1e-9;

/* $Procedure F_SCLK ( SCLK tests ) */
/* Subroutine */ int f_sclk__(logical *ok)
{
    /* Initialized data */

    static char dlchrs[5] = ".:-, ";

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static char coms[80*9], fstr[25*4];
    static doublereal xtdt;
    extern /* Subroutine */ int sce2c_(integer *, doublereal *, doublereal *),
	     scs2e_(integer *, char *, doublereal *, ftnlen), sce2s_(integer *
	    , doublereal *, char *, ftnlen), sce2t_(integer *, doublereal *, 
	    doublereal *), sct2e_(integer *, doublereal *, doublereal *);
    static integer i__, j, k, n, q, field[4], clkid, w;
    static doublereal delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal etoff, major;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), scfmt_(integer *, doublereal *, char *, 
	    ftnlen);
    static char title[240];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal divsr[4];
    static logical error;
    extern /* Subroutine */ int ljust_(char *, char *, ftnlen, ftnlen);
    static doublereal pstop[9999];
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal xstop[9999];
    extern /* Subroutine */ int tstck3_(char *, char *, logical *, logical *, 
	    logical *, integer *, ftnlen, ftnlen), chckad_(char *, doublereal 
	    *, char *, doublereal *, integer *, doublereal *, logical *, 
	    ftnlen, ftnlen);
    static integer delcde;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen), 
	    scdecd_(integer *, doublereal *, char *, ftnlen);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), scencd_(integer *, 
	    char *, doublereal *, ftnlen);
    static integer ncoeff, nfield;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal clkoff, coeffs[150000]	/* was [3][50000] */;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksd_(char *, doublereal 
	    *, char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal divdnd;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), delfil_(char *, ftnlen), replch_(char *, char *, char 
	    *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal sclkdp;
    extern /* Subroutine */ int rmaini_(integer *, integer *, integer *, 
	    integer *);
    extern integer lastnb_(char *, ftnlen);
    static doublereal offset[10], xclkdp, moduli[10];
    extern /* Subroutine */ int scpars_(integer *, char *, logical *, char *, 
	    doublereal *, ftnlen, ftnlen), scpart_(integer *, integer *, 
	    doublereal *, doublereal *), gipool_(char *, integer *, integer *,
	     integer *, integer *, logical *, ftnlen), ldpool_(char *, ftnlen)
	    ;
    static char errmsg[240];
    extern /* Subroutine */ int sctiks_(integer *, char *, doublereal *, 
	    ftnlen);
    static char clkstr[40];
    static integer partno;
    extern doublereal unitim_(doublereal *, char *, char *, ftnlen, ftnlen);
    extern integer sctype_(integer *);
    static char xclkst[40];
    static doublereal et0;
    static integer clktyp, nparts;
    static doublereal pstart[9999];
    extern /* Subroutine */ int pipool_(char *, integer *, integer *, ftnlen),
	     ssizec_(integer *, char *, ftnlen), suffix_(char *, integer *, 
	    char *, ftnlen, ftnlen), prefix_(char *, integer *, char *, 
	    ftnlen, ftnlen), intstr_(integer *, char *, ftnlen), tstlsk_(void)
	    ;
    static doublereal xstart[9999];
    static integer rem;
    static doublereal tdt, xet;
    extern /* Subroutine */ int t_wclk01__(char *, char *, integer *, char *, 
	    char *, integer *, doublereal *, doublereal *, char *, integer *, 
	    doublereal *, doublereal *, integer *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the SCLK time conversion and kernel pool access routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Include file sclk.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define sizes and limits used by */
/*     the SCLK system. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     See the declaration section below. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 24-MAY-2010 (NJB) */

/*        Increased value of maximum coefficient record count */
/*        parameter MXCOEF from 10K to 50K. */

/* -    SPICELIB Version 1.0.0, 11-FEB-2008 (NJB) */

/* -& */

/*     Number of supported SCLK field delimiters: */


/*     Supported SCLK string field delimiters: */


/*     Maximum number of partitions: */


/*     Partition string length. */

/*     Since the maximum number of partitions is given by MXPART is */
/*     9999, PRTSTR needs at most 4 characters for the partition number */
/*     and one character for the slash. */


/*     Maximum number of coefficient records: */


/*     Maximum number of fields in an SCLK string: */


/*     Length of strings used to represent D.P. */
/*     numbers: */


/*     Maximum number of supported parallel time systems: */


/*     End of include file sclk.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SCLK routines */

/*        SC01 */
/*        SCDECD */
/*        SCE2C */
/*        SCE2S */
/*        SCE2T */
/*        SCENCD */
/*        SCFMT */
/*        SCLU01 */
/*        SCPARS */
/*        SCPART */
/*        SCS2E */
/*        SCT2E */
/*        SCTIKS */
/*        SCTYPE */

/*     The entry points of SC01 and SCLU01 are exercised by tests of */
/*     the API-level routines. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.1.0 26-OCT-2010 (EDW) */

/*        All variables to SAVE for f2c'd version compiled with Borland. */

/* -    TSPICE Version 2.0.0, 03-MAR-2009 (NJB) */

/*        Added test cases for watcher-related error */
/*        handling. Routines that encounter failures */
/*        when looking up kernel data must not assume */
/*        they have the data, even if the watcher */
/*        system says no update is needed; formerly some */
/*        SCLK routines had this error. */

/* -    TSPICE Version 1.0.0, 21-FEB-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     We use a longer, non-standard length for ABCORR because we */
/*     want to include embedded blanks for testing. */


/*     Saved variables */


/*     Initial values */


/*     Open the test family. */

    topen_("F_SCLK", (ftnlen)6);
/* ***************************************************************** */

/*     Error cases */

/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCPART.", (ftnlen)43);

/*     We should see a lookup failure error on two successive calls */
/*     to SCPART if no data are available for a given clock. */

    clkid = -888;
    scpart_(&clkid, &n, pstart, pstop);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    scpart_(&clkid, &n, pstart, pstop);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCTYPE.", (ftnlen)43);

/*     We should see a lookup failure error on two successive calls */
/*     to SCTYPE if no data are available for a given clock. */

    clkid = -888;
    clktyp = sctype_(&clkid);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    clktyp = sctype_(&clkid);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCENCD.", (ftnlen)43);

/*     We should see a lookup failure error on two successive calls */
/*     to SCENCD if no data are available for a given clock. */

    clkid = -888;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCDECD.", (ftnlen)43);

/*     We should see a lookup failure error on two successive calls */
/*     to SCENCD if no data are available for a given clock. */

    clkid = -888;
    sclkdp = 1e3;
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCS2E.", (ftnlen)42);

/*     We should see a lookup failure error on two successive calls */
/*     to SCS2E if no data are available for a given clock. */

    clkid = -888;
    sclkdp = 1e3;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scs2e_(&clkid, clkstr, &et, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    scs2e_(&clkid, clkstr, &et, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCE2S.", (ftnlen)42);

/*     We should see a lookup failure error on two successive calls */
/*     to SCS if no data are available for a given clock. */

    clkid = -888;
    sclkdp = 1e3;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    sce2s_(&clkid, &et, clkstr, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    sce2s_(&clkid, &et, clkstr, (ftnlen)40);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check handling of lookup failure in SCE2C.", (ftnlen)42);

/*     We should see a lookup failure error on two successive calls */
/*     to SCS if no data are available for a given clock. */

    clkid = -888;
    sclkdp = 1e3;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    sce2c_(&clkid, &et, &sclkdp);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    sce2c_(&clkid, &et, &sclkdp);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
/* ***************************************************************** */

/*     Normal cases */

/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  load kernels.", (ftnlen)21);

/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load an SCLK kernel. Keep the file; we'll be */
/*     re-loading it later. We'll delete it at the end of */
/*     the routine. W */

    tstck3_("test.bc", "test.tsc", &c_false, &c_true, &c_true, &handle, (
	    ftnlen)7, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The following tests that use SCLK ID -9 presume the SCLK kernel */
/*     we just created contains the following assignments: */

/*        SCLK_KERNEL_ID                = ( @28-OCT-1994        ) */

/*        SCLK_DATA_TYPE_9              = ( 1 ) */

/*        SCLK01_TIME_SYSTEM_9          = ( 1 ) */
/*        SCLK01_N_FIELDS_9             = ( 2 ) */
/*        SCLK01_MODULI_9               = ( 1000000000     10000 ) */
/*        SCLK01_OFFSETS_9              = ( 0         0 ) */
/*        SCLK01_OUTPUT_DELIM_9         = ( 1 ) */

/*        SCLK_PARTITION_START_9        = ( 0.0000000000000E+00 ) */
/*        SCLK_PARTITION_END_9          = ( 1.00000000E+14      ) */
/*        SCLK01_COEFFICIENTS_9         = ( 0.00000000E+00 */
/*        @01-JAN-1980-00:00:00.000 */
/*        1  ) */


/*        DELTET/DELTA_T_A       =   32.184 */
/*        DELTET/K               =    1.657D-3 */
/*        DELTET/EB              =    1.671D-2 */
/*        DELTET/M               = (  6.239996D0 1.99096871D-7 ) */

/*        CK_-9999_SCLK          =   -9 */
/*        CK_-9999_SPK           =   -9 */

/*        CK_-10000_SCLK         =   -9 */
/*        CK_-10000_SPK          =   -9 */

/*        CK_-10001_SCLK         =   -9 */
/*        CK_-10001_SPK          =   -9 */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check clock type via SCTYPE", (ftnlen)27);
    clkid = -9;
    i__ = sctype_(&clkid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("Clock type", &i__, "=", &c__1, &c__0, ok, (ftnlen)10, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Fetch partition boundaries via SCPART", (ftnlen)37);
    clkid = -9;
    scpart_(&clkid, &nparts, pstart, pstop);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NPARTS", &nparts, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     These values come from the SCLK kernel described above. */

    xstart[0] = 0.;
    xstop[0] = 1e14;
    chckad_("PSTART", pstart, "=", xstart, &nparts, &c_b68, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("PSTOP", pstop, "=", xstop, &nparts, &c_b68, ok, (ftnlen)5, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Encode SCLK strings via SCENCD", (ftnlen)30);
    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 9876543214321.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time excluding the partition */
/*     number. */

    s_copy(clkstr, "987654321.4321", (ftnlen)40, (ftnlen)14);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time excluding the partition */
/*     number and using a smaller count in the leftmost field. */

    s_copy(clkstr, "4321.4321", (ftnlen)40, (ftnlen)9);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 43214321.;
    chcksd_("3) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Encode SCLK strings via SCENCD using all supported delimiters.", (
	    ftnlen)62);
    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 9876543214321.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);
    s_copy(clkstr, "1/987654321:4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);
    s_copy(clkstr, "1/987654321-4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("3) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);
    s_copy(clkstr, "1/987654321,4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("4) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);
    s_copy(clkstr, "1/987654321 4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("5) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Encode SCLK strings via SCPARS", (ftnlen)30);
    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scpars_(&clkid, clkstr, &error, errmsg, &sclkdp, (ftnlen)40, (ftnlen)240);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ERROR", &error, &c_false, ok, (ftnlen)5);
    chcksc_("ERRMSG", errmsg, "=", " ", ok, (ftnlen)6, (ftnlen)240, (ftnlen)1,
	     (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 9876543214321.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time excluding the partition */
/*     number. */

    s_copy(clkstr, "987654321.4321", (ftnlen)40, (ftnlen)14);
    scpars_(&clkid, clkstr, &error, errmsg, &sclkdp, (ftnlen)40, (ftnlen)240);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ERROR", &error, &c_false, ok, (ftnlen)5);
    chcksc_("ERRMSG", errmsg, "=", " ", ok, (ftnlen)6, (ftnlen)240, (ftnlen)1,
	     (ftnlen)1);
    xclkdp = 9876543214321.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time excluding the partition */
/*     number and using a smaller count in the leftmost field. */

    s_copy(clkstr, "4321.4321", (ftnlen)40, (ftnlen)9);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 43214321.;
    chcksd_("3) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Encode an invalid SCLK string via SCPARS", (ftnlen)40);
    clkid = -9;
    s_copy(clkstr, "1/987654321.XXXX", (ftnlen)40, (ftnlen)16);
    scpars_(&clkid, clkstr, &error, errmsg, &sclkdp, (ftnlen)40, (ftnlen)240);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ERROR", &error, &c_true, ok, (ftnlen)5);
    chcksc_("ERRMSG", errmsg, "=", "Could not parse SCLK component XXXX from"
	    " 987654321.XXXX as a number.", ok, (ftnlen)6, (ftnlen)240, (
	    ftnlen)1, (ftnlen)68);

/* --- Case: ------------------------------------------------------ */

    tcase_("Encode SCLK duration strings via SCTIKS", (ftnlen)39);
    clkid = -9;
    s_copy(clkstr, "987654321.4321", (ftnlen)40, (ftnlen)14);
    sctiks_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 9876543214321.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time using a smaller count */
/*     in the leftmost field. */

    s_copy(clkstr, "4321.4321", (ftnlen)40, (ftnlen)9);
    sctiks_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 43214321.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Decode SCLK tick values via SCDECD", (ftnlen)34);
    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're expecting exact agreement. */

    s_copy(xclkst, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    chcksc_("1) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Now repeat the test, this time using smaller */
/*     tick value. */

    s_copy(clkstr, "4321.4321", (ftnlen)40, (ftnlen)9);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're expecting exact agreement. */

    s_copy(xclkst, "1/000004321.4321", (ftnlen)40, (ftnlen)16);
    chcksc_("2) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/* --- Case: ------------------------------------------------------ */

    tcase_("Decode SCLK tick values via SCDECD using each supported output d"
	    "elimiter", (ftnlen)72);
    clkid = -9;

/*     Fetch the current output delimiter code. */

    gipool_("SCLK01_OUTPUT_DELIM_9", &c__1, &c__1, &n, &delcde, &found, (
	    ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {

/*        The delimiter array declared in sclk.inc has been */
/*        ordered so that the Ith character has code I. */
/*        Insert this code into the kernel pool. */

	pipool_("SCLK01_OUTPUT_DELIM_9", &c__1, &i__, (ftnlen)21);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create the expected output string. */

	s_copy(xclkst, "1/987654321#4321", (ftnlen)40, (ftnlen)16);
	replch_(xclkst, "#", dlchrs + (i__ - 1), xclkst, (ftnlen)40, (ftnlen)
		1, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We're expecting exact agreement. */

	s_copy(title, "#) CLKSTR", (ftnlen)240, (ftnlen)9);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	chcksc_(title, clkstr, "=", xclkst, ok, (ftnlen)240, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);
    }

/*     Restore the original output delimiter code. */

    pipool_("SCLK01_OUTPUT_DELIM_9", &c__1, &delcde, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Decode SCLK tick values via SCFMT", (ftnlen)33);
    clkid = -9;
    s_copy(clkstr, "987654321.4321", (ftnlen)40, (ftnlen)14);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scfmt_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're expecting exact agreement. */

    s_copy(xclkst, "987654321.4321", (ftnlen)40, (ftnlen)14);
    chcksc_("CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)6, (ftnlen)40, (ftnlen)
	    1, (ftnlen)40);

/*     Now repeat the test, this time using smaller */
/*     tick value. */

    s_copy(clkstr, "4321.4321", (ftnlen)40, (ftnlen)9);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scfmt_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're expecting exact agreement. */

    s_copy(xclkst, "000004321.4321", (ftnlen)40, (ftnlen)14);
    chcksc_("2) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert ET values to integer ticks via SCE2T", (ftnlen)44);
    clkid = -9;
    str2et_("1980 JAN 1 00:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     First try the ET value corresponding to clock start. */

    sce2t_(&clkid, &et0, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 0.;
    chcksd_("SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert ET values to continuous ticks via SCE2C", (ftnlen)47);
    clkid = -9;
    str2et_("1980 JAN 1 00:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     First try the ET value corresponding to clock start. */

    sce2c_(&clkid, &et0, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 0.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now a greater ET value. */

    delta = 100000000.33329999;
    et = et0 + delta;
    sce2c_(&clkid, &et, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = delta * 1e4;

/*     We should get some round-off here, so don't require */
/*     an exact match. */

    chcksd_("2) SCLKDP", &sclkdp, "~/", &xclkdp, &c_b255, ok, (ftnlen)9, (
	    ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert continuous ticks to ET via SCT2E", (ftnlen)40);
    clkid = -9;
    str2et_("1980 JAN 1 00:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use an ET value that corresponds to a fractional tick count. */

    delta = 100000000.33329999;
    xet = et0 + delta;
    sce2c_(&clkid, &xet, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sct2e_(&clkid, &sclkdp, &et);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should get some round-off here, so don't require */
/*     an exact match. */

    chcksd_("2) ET", &et, "~/", &xet, &c_b255, ok, (ftnlen)5, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert an SCLK string to ET via SCS2E", (ftnlen)38);
    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);

/*     Compute the expected value using SCENCD and */
/*     SCT2E. */

    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sct2e_(&clkid, &sclkdp, &xet);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now try the conversion with SCS2E. */

    scs2e_(&clkid, clkstr, &et, (ftnlen)40);

/*     We're expecting exact agreement. */

    chcksd_("ET", &et, "=", &xet, &c_b68, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Convert an ET value to an SCLK string via SCE2S.", (ftnlen)48);

/*     Generate the ET value. */

    clkid = -9;
    s_copy(clkstr, "1/987654321.4321", (ftnlen)40, (ftnlen)16);
    scs2e_(&clkid, clkstr, &et, (ftnlen)40);

/*     Compute the expected value using SCE2T and SCDECD. */

    sce2t_(&clkid, &et, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, xclkst, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now try the conversion with SCE2S. */

    sce2s_(&clkid, &et, clkstr, (ftnlen)40);

/*     We're expecting exact agreement. */

    chcksc_("CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)6, (ftnlen)40, (ftnlen)
	    1, (ftnlen)40);

/* --- Case: ------------------------------------------------------ */


/*     At this point we're going to create a new kernel to */
/*     exercise other aspects of the SCLK system. The kernel */
/*     contents will be as follows: */


/*     KPL/SCLK */

/*     \begindata */

/*        SCLK_KERNEL_ID                  = ( @01-JAN-2000/12:00:00 ) */

/*        SCLK_DATA_TYPE_-99              = ( 1 ) */

/*        SCLK01_TIME_SYSTEM_-99          = ( 1 ) */
/*        SCLK01_N_FIELDS_-99             = ( 4 ) */
/*        SCLK01_MODULI_-99               = ( 10000000 10 9 8 ) */
/*        SCLK01_OFFSETS_-99              = ( 0 0 0 0 ) */
/*        SCLK01_OUTPUT_DELIM_-99         = ( 2 ) */

/*        SCLK_PARTITION_START_-99        = ( 1.0, */
/*                                            2.0, */
/*                                            3.0, */
/*                                            ... */
/*                                            1001.0 ) */

/*        SCLK_PARTITION_END_-99          = ( 720001.0, */
/*                                            720002.0, */
/*                                            ... */
/*                                            721001.0 ) */

/*        SCLK01_COEFFICIENTS_-99         = ( */

/*                0.0    0.0000000000000000E+00    0.2000000001000000E+01 */
/*           180000.0    0.5000000000000000E+03    0.2000000002000000E+01 */
/*           360000.0    0.1000000000000000E+04    0.2000000003000000E+01 */
/*                                ... */
/*        720000000.0    0.2000000000000000E+07    0.2000004001000000E+01 */

/*        ) */

/*     \begintext */

/*     The above kernel has 1001 partitions and 4001 coefficient records. */

    tcase_("Create second SCLK kernel with 1001 partitions.", (ftnlen)47);
    ssizec_(&c__0, coms, (ftnlen)80);
    clkid = -99;
    nparts = 1001;
    ncoeff = 4001;
    nfield = 4;
    moduli[0] = 1e7;
    moduli[1] = 10.;
    moduli[2] = 9.;
    moduli[3] = 8.;
    major = 1.;
    for (i__ = 2; i__ <= 4; ++i__) {
	major *= moduli[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"moduli", i__1, "f_sclk__", (ftnlen)1067)];
    }
    offset[0] = 0.;
    offset[1] = 0.;
    offset[2] = 0.;
    offset[3] = 0.;
    i__1 = nparts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xstart[(i__2 = i__ - 1) < 9999 && 0 <= i__2 ? i__2 : s_rnge("xstart", 
		i__2, "f_sclk__", (ftnlen)1076)] = (doublereal) i__;
	xstop[(i__2 = i__ - 1) < 9999 && 0 <= i__2 ? i__2 : s_rnge("xstop", 
		i__2, "f_sclk__", (ftnlen)1077)] = (doublereal) (i__ + 720000)
		;
    }
    delta = 1e-9;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1084)] = (i__ - 1) * 
		1.8e5;
	coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1086)] = coeffs[(i__3 = 
		i__ * 3 - 3) < 150000 && 0 <= i__3 ? i__3 : s_rnge("coeffs", 
		i__3, "f_sclk__", (ftnlen)1086)] * 2 / major;
	coeffs[(i__2 = i__ * 3 - 1) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1088)] = i__ * delta + 2.;
    }
    t_wclk01__("test2.tsc", coms, &clkid, "@01-JAN-2000/12:00:00", "TDB", &
	    nfield, moduli, offset, ":", &nparts, xstart, xstop, &ncoeff, 
	    coeffs, (ftnlen)9, (ftnlen)80, (ftnlen)21, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ldpool_("test2.tsc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Recover partition bounds via SCPART", (ftnlen)46);
    clkid = -99;
    scpart_(&clkid, &n, pstart, pstop);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &nparts, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chckad_("PSTART", pstart, "=", xstart, &nparts, &c_b68, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("PSTOP", pstop, "=", xstop, &nparts, &c_b68, ok, (ftnlen)5, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert tick coefficients to SCLK strings via SCDECD."
	    " Invert the conversion via SCENCD.", (ftnlen)98);
    clkid = -99;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	scdecd_(&clkid, &coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? 
		i__2 : s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1139)], 
		clkstr, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Derive the expected string based on the SCLK kernel */
/*        structure: every record whose index is equivalent */
/*        to 1 mod 4 is at the start of a new partition. Each */
/*        record is 180000 ticks ahead of the previous one. */

	i__2 = i__ - 1;
	rmaini_(&i__2, &c__4, &q, &rem);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The partition number is simply Q+1. */

	partno = q + 1;
	intstr_(&partno, xclkst, (ftnlen)40);
	suffix_("/", &c__0, xclkst, (ftnlen)1, (ftnlen)40);

/*        Let J be the number of ticks past the */
/*        partition start reading. Let SCLKDP */
/*        be the actual clock reading; we obtain */
/*        SCLKDP by adding the partition start reading */
/*        to J. */

	d__1 = rem * 1.8e5;
	j = i_dnnt(&d__1);
	sclkdp = j + xstart[(i__2 = partno - 1) < 9999 && 0 <= i__2 ? i__2 : 
		s_rnge("xstart", i__2, "f_sclk__", (ftnlen)1167)];

/*        Convert SCLKDP to an SCLK string. */

	divsr[0] = 720.;
	divsr[1] = 72.;
	divsr[2] = 8.;
	divdnd = sclkdp;
	for (k = 1; k <= 3; ++k) {
	    field[(i__2 = k - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("field", 
		    i__2, "f_sclk__", (ftnlen)1179)] = i_dnnt(&divdnd) / 
		    i_dnnt(&divsr[(i__3 = k - 1) < 4 && 0 <= i__3 ? i__3 : 
		    s_rnge("divsr", i__3, "f_sclk__", (ftnlen)1179)]);
	    intstr_(&field[(i__2 = k - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		    "field", i__2, "f_sclk__", (ftnlen)1181)], fstr + ((i__3 =
		     k - 1) < 4 && 0 <= i__3 ? i__3 : s_rnge("fstr", i__3, 
		    "f_sclk__", (ftnlen)1181)) * 25, (ftnlen)25);
	    ljust_(fstr + ((i__2 = k - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		    "fstr", i__2, "f_sclk__", (ftnlen)1182)) * 25, fstr + ((
		    i__3 = k - 1) < 4 && 0 <= i__3 ? i__3 : s_rnge("fstr", 
		    i__3, "f_sclk__", (ftnlen)1182)) * 25, (ftnlen)25, (
		    ftnlen)25);
	    divdnd -= field[(i__2 = k - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		    "field", i__2, "f_sclk__", (ftnlen)1184)] * divsr[(i__3 = 
		    k - 1) < 4 && 0 <= i__3 ? i__3 : s_rnge("divsr", i__3, 
		    "f_sclk__", (ftnlen)1184)];
	}
	field[3] = i_dnnt(&divdnd);
	intstr_(&field[3], fstr + 75, (ftnlen)25);

/*        Pad the first field on the left with zeros. */

	w = lastnb_(fstr, (ftnlen)25);
	i__2 = 7 - w;
	for (k = 1; k <= i__2; ++k) {
	    prefix_("0", &c__0, fstr, (ftnlen)1, (ftnlen)25);
	}

/*        Append the field and delimiters to the expected */
/*        string. */

	for (k = 1; k <= 3; ++k) {
	    suffix_(fstr + ((i__2 = k - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		    "fstr", i__2, "f_sclk__", (ftnlen)1205)) * 25, &c__0, 
		    xclkst, (ftnlen)25, (ftnlen)40);
	    suffix_(":", &c__0, xclkst, (ftnlen)1, (ftnlen)40);
	}
	suffix_(fstr + 75, &c__0, xclkst, (ftnlen)25, (ftnlen)40);
	s_copy(title, "CLKSTR(#)", (ftnlen)240, (ftnlen)9);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Look for an exact match. */

	chcksc_(title, clkstr, "=", xclkst, ok, (ftnlen)240, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);

/*        Now convert the string to a tick value. */

	scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Compare the tick value to the tick coefficient. */

	chcksd_("SCLKDP", &sclkdp, "=", &coeffs[(i__2 = i__ * 3 - 3) < 150000 
		&& 0 <= i__2 ? i__2 : s_rnge("coeffs", i__2, "f_sclk__", (
		ftnlen)1228)], &c_b68, ok, (ftnlen)6, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert tick coefficients to ET via SCT2E", (ftnlen)52)
	    ;
    clkid = -99;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sct2e_(&clkid, &coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? 
		i__2 : s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1244)], &et)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xet = coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1247)];
	s_copy(title, "ET(#)", (ftnlen)240, (ftnlen)5);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Don't look for an exact match, since NPARSD, which is */
/*        used by the text kernel reader routines, can introduce */
/*        round-off error in integral inputs (!) depending on */
/*        their format. */

	chcksd_(title, &et, "~", &xet, &c_b377, ok, (ftnlen)240, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert tick coefficient midpoints to ET via SCT2E", (
	    ftnlen)61);
    clkid = -99;

/*     The tick offset value is constant, given the SCLK kernel */
/*     we've created. */

    clkoff = (coeffs[0] + coeffs[3]) / 2;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1278)] + clkoff;
	sct2e_(&clkid, &sclkdp, &et);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The ET offset is based on the rate for the previous */
/*        (Ith) coefficient record. */

	xet = coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1287)] + clkoff / 
		major * coeffs[(i__3 = i__ * 3 - 1) < 150000 && 0 <= i__3 ? 
		i__3 : s_rnge("coeffs", i__3, "f_sclk__", (ftnlen)1287)];
	s_copy(title, "ET(#)", (ftnlen)240, (ftnlen)5);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Look for tight, but not exact, match. */

	chcksd_(title, &et, "~/", &xet, &c_b255, ok, (ftnlen)240, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert TDB coefficients to ticks via SCE2T", (ftnlen)
	    54);
    clkid = -99;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sce2t_(&clkid, &coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? 
		i__2 : s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1310)], &
		sclkdp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xclkdp = coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1313)];
	s_copy(title, "SCLKDP(#)", (ftnlen)240, (ftnlen)9);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Look for an exact match. */

	chcksd_(title, &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)240, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert tick coefficient midpoints to ET via SCT2E", (
	    ftnlen)61);
    clkid = -99;

/*     The tick offset value is constant, given the SCLK kernel */
/*     we've created. */

    clkoff = (coeffs[0] + coeffs[3]) / 2;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1342)] + clkoff;
	sct2e_(&clkid, &sclkdp, &et);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The ET offset is based on the rate for the previous */
/*        (Ith) coefficient record. */

	xet = coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1351)] + clkoff / 
		major * coeffs[(i__3 = i__ * 3 - 1) < 150000 && 0 <= i__3 ? 
		i__3 : s_rnge("coeffs", i__3, "f_sclk__", (ftnlen)1351)];
	s_copy(title, "ET(#)", (ftnlen)240, (ftnlen)5);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Look for tight, but not exact, match. */

	chcksd_(title, &et, "~/", &xet, &c_b255, ok, (ftnlen)240, (ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -99: Convert ET coefficient midpoints to continuous ticks "
	    "via SCE2C", (ftnlen)73);
    clkid = -99;

/*     The ET offset value is constant, given the SCLK kernel */
/*     we've created. */

    etoff = (coeffs[1] + coeffs[4]) / 2;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1379)] + etoff;
	sce2c_(&clkid, &et, &sclkdp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The tick offset is based on the rate for the previous */
/*        (Ith) coefficient record. */

/*        The expected tick offset is the ET offset divided */
/*        by the applicable rate, which has units of seconds */
/*        per major field, times the ticks per major field. */

	xclkdp = coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : 
		s_rnge("coeffs", i__2, "f_sclk__", (ftnlen)1392)] + major * 
		etoff / coeffs[(i__3 = i__ * 3 - 1) < 150000 && 0 <= i__3 ? 
		i__3 : s_rnge("coeffs", i__3, "f_sclk__", (ftnlen)1392)];
	s_copy(title, "SCLKDP (#)", (ftnlen)240, (ftnlen)10);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);

/*        Look for tight, but not exact, match. */

	chcksd_(title, &sclkdp, "~/", &xclkdp, &c_b255, ok, (ftnlen)240, (
		ftnlen)2);
    }

/* --- Case: ------------------------------------------------------ */


/*     At this point we're going to create another new kernel to */
/*     exercise other aspects of the SCLK system. This kernel */
/*     uses TDT as the parallel time system. The kernel */
/*     contents will be as follows: */


/*     KPL/SCLK */

/*     \begindata */

/*        SCLK_KERNEL_ID                   = ( @01-JAN-2001/12:00:00 ) */

/*        SCLK_DATA_TYPE_-999              = ( 1 ) */

/*        SCLK01_TIME_SYSTEM_-999          = ( 2 ) */
/*        SCLK01_N_FIELDS_-999             = ( 4 ) */
/*        SCLK01_MODULI_-999               = ( 10000000 10 9 8 ) */
/*        SCLK01_OFFSETS_-999              = ( 4 3 2 1 ) */
/*        SCLK01_OUTPUT_DELIM_-999         = ( 2 ) */

/*        SCLK_PARTITION_START_-999        = ( 1.0, */
/*                                             1000002.0, */
/*                                             3.0, */
/*                                             4.0  ) */

/*        SCLK_PARTITION_END_-999          = ( 720001.0, */
/*                                             1720002.0, */
/*                                             720003.0, */
/*                                             720004.0 ) */

/*        SCLK01_COEFFICIENTS_-999         = ( */


/*                0.0    0.0000000000000000E+00    0.2000000001000000E+01 */
/*           180000.0    0.5000000000000000E+03    0.2000000002000000E+01 */
/*           360000.0    0.1000000000000000E+04    0.2000000003000000E+01 */
/*                                ... */
/*          2700000.0    0.7500000000000000E+04    0.2000000016000000E+01 */

/*        ) */

/*     \begintext */

/*     The above kernel has 4 partitions and 16 coefficient records. */

    tcase_("Create third SCLK kernel with 4 partitions. Time system is TDT.", 
	    (ftnlen)63);
    ssizec_(&c__0, coms, (ftnlen)80);
    clkid = -999;
    nparts = 4;
    ncoeff = 16;
    nfield = 4;
    moduli[0] = 1e7;
    moduli[1] = 10.;
    moduli[2] = 9.;
    moduli[3] = 8.;
    major = 1.;
    for (i__ = 2; i__ <= 4; ++i__) {
	major *= moduli[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"moduli", i__1, "f_sclk__", (ftnlen)1474)];
    }
    offset[0] = 4.;
    offset[1] = 3.;
    offset[2] = 2.;
    offset[3] = 1.;
    i__1 = nparts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xstart[(i__2 = i__ - 1) < 9999 && 0 <= i__2 ? i__2 : s_rnge("xstart", 
		i__2, "f_sclk__", (ftnlen)1483)] = (doublereal) i__;
	xstop[(i__2 = i__ - 1) < 9999 && 0 <= i__2 ? i__2 : s_rnge("xstop", 
		i__2, "f_sclk__", (ftnlen)1484)] = (doublereal) (i__ + 720000)
		;
    }

/*     Make the second partition start with a forward jump. */

    xstart[1] += 1e6;
    xstop[1] += 1e6;
    delta = 1e-9;
    i__1 = ncoeff;
    for (i__ = 1; i__ <= i__1; ++i__) {
	coeffs[(i__2 = i__ * 3 - 3) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1497)] = (i__ - 1) * 
		1.8e5;
	coeffs[(i__2 = i__ * 3 - 2) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1499)] = coeffs[(i__3 = 
		i__ * 3 - 3) < 150000 && 0 <= i__3 ? i__3 : s_rnge("coeffs", 
		i__3, "f_sclk__", (ftnlen)1499)] * 2 / major;
	coeffs[(i__2 = i__ * 3 - 1) < 150000 && 0 <= i__2 ? i__2 : s_rnge(
		"coeffs", i__2, "f_sclk__", (ftnlen)1501)] = i__ * delta + 2.;
    }
    t_wclk01__("test3.tsc", coms, &clkid, "@01-JAN-2001/12:00:00", "TDT", &
	    nfield, moduli, offset, ":", &nparts, xstart, xstop, &ncoeff, 
	    coeffs, (ftnlen)9, (ftnlen)80, (ftnlen)21, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ldpool_("test3.tsc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Encode SCLK strings via SCENCD.", (ftnlen)43);
    clkid = -999;
    s_copy(clkstr, "1/4:03:02:2", (ftnlen)40, (ftnlen)11);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 0.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now repeat the test, this time excluding the partition */
/*     number. */

    s_copy(clkstr, "4:03:02:2", (ftnlen)40, (ftnlen)9);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 0.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Use a larger input value. */

    s_copy(clkstr, "1/5:03:02:2", (ftnlen)40, (ftnlen)11);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 720.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Use an input value in the second partition. */

    s_copy(clkstr, "2/1393:11:10:3", (ftnlen)40, (ftnlen)14);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 720720.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Encode SCLK string with overflow field values via SC"
	    "ENCD.", (ftnlen)69);
    clkid = -999;
    s_copy(clkstr, "1/4:03:02:11", (ftnlen)40, (ftnlen)12);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 9.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);
    s_copy(clkstr, "1/4:13:02:11", (ftnlen)40, (ftnlen)12);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 729.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Overflow in the second and fourth fields: */

    s_copy(clkstr, "1/4:13:02:11", (ftnlen)40, (ftnlen)12);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 729.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Overflow in the second, third and fourth fields: */

    s_copy(clkstr, "1/4:13:12:11", (ftnlen)40, (ftnlen)12);
    scencd_(&clkid, clkstr, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that we're expecting exact agreement. */

    xclkdp = 809.;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Decode SCLK tick values via SCDECD.", (ftnlen)47);
    clkid = -999;
    s_copy(xclkst, "1/00000004:03:02:2", (ftnlen)40, (ftnlen)18);
    scencd_(&clkid, xclkst, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("1) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Use a larger input value. */

    s_copy(xclkst, "1/00000005:03:02:2", (ftnlen)40, (ftnlen)18);
    scencd_(&clkid, xclkst, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("2) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Use an input value in the second partition. */

    s_copy(xclkst, "2/00001393:11:10:3", (ftnlen)40, (ftnlen)18);
    scencd_(&clkid, xclkst, &sclkdp, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    scdecd_(&clkid, &sclkdp, clkstr, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("3) CLKSTR", clkstr, "=", xclkst, ok, (ftnlen)9, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Convert TDT values to integer ticks via SCE2T", (
	    ftnlen)57);
    clkid = -999;
    str2et_("2000 JAN 1 12:00:00 TDT", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     First try the TDT value corresponding to clock start. */

    sce2t_(&clkid, &et0, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 0.;
    chcksd_("1) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/*     First now try a greater TDT value: 1000 seconds past clock */
/*     start. */

    str2et_("2000 JAN 1 12:16:40 TDT", &et, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sce2t_(&clkid, &et, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = 3.6e5;
    chcksd_("2) SCLKDP", &sclkdp, "=", &xclkdp, &c_b68, ok, (ftnlen)9, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Convert TDT values to continuous ticks via SCE2C", (
	    ftnlen)60);
    clkid = -999;
    str2et_("2000 JAN 1 12:00:00.125 TDT", &et, (ftnlen)27);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     First try the TDT value corresponding to 1/8 second */
/*     past clock start. */

    sce2c_(&clkid, &et, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = .125 / coeffs[2] * 720.;
    chcksd_("1) SCLKDP", &sclkdp, "~", &xclkdp, &c_b255, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Now a greater TDT value. */

    tdt = 1000.3333;
    et = unitim_(&tdt, "TDT", "TDB", (ftnlen)3, (ftnlen)3);
    sce2c_(&clkid, &et, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xclkdp = coeffs[6] + (tdt - coeffs[7]) / coeffs[8] * 720.;

/*     We should get some round-off here, so don't require */
/*     an exact match. */

    chcksd_("2) SCLKDP", &sclkdp, "~/", &xclkdp, &c_b255, ok, (ftnlen)9, (
	    ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clock -999: Convert continuous ticks to TDT via SCT2E", (ftnlen)
	    53);
    clkid = -999;
    str2et_("2000 JAN 1 12:00:00 TDT", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use a TDT value that corresponds to a fractional */
/*     tick count. */

    delta = 1000.3333;
    xtdt = unitim_(&et0, "TDB", "TDT", (ftnlen)3, (ftnlen)3) + delta;
    xet = unitim_(&xtdt, "TDT", "TDB", (ftnlen)3, (ftnlen)3);
    sce2c_(&clkid, &xet, &sclkdp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sct2e_(&clkid, &sclkdp, &et);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tdt = unitim_(&et, "TDB", "TDT", (ftnlen)3, (ftnlen)3);

/*     We should get some round-off here, so don't require */
/*     an exact match. */

    chcksd_("2) TDT", &xtdt, "~/", &xtdt, &c_b255, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    delfil_("test.bc", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*      CALL BYEBYE (' ' ) */
    delfil_("test.tsc", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test2.tsc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test3.tsc", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_sclk__ */

