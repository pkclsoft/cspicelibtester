/* f_sincpt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c_n499 = -499;
static integer c__499 = 499;
static integer c__1 = 1;
static integer c_n666 = -666;
static integer c__4 = 4;
static doublereal c_b160 = 1.;
static doublereal c_b185 = 0.;
static doublereal c_b186 = .001;
static integer c__3 = 3;
static integer c__599 = 599;
static integer c__399 = 399;
static integer c__10 = 10;

/* $Procedure      F_SINCPT ( SINCPT family tests ) */
/* Subroutine */ int f_sincpt__(logical *ok)
{
    /* Initialized data */

    static char abcs[10*9] = "None      " "Lt        " "Lt+s      " "Cn     "
	    "   " "Cn+s      " "Xlt       " "Xlt+s     " "Xcn       " "Xcn+s "
	    "    ";
    static char refs[32*4*2] = "J2000                           " "J2000    "
	    "                       " "ECLIPJ2000                      " "ECL"
	    "IPJ2000                      " "IAU_MARS                        " 
	    "IAU_PHOBOS                      " "IAU_EARTH                   "
	    "    " "IAU_EARTH                       ";
    static char geoms[160*4] = "POINT_AT_CENTER                             "
	    "                                                                "
	    "                                                    " "MISS_BACK"
	    "WARD                                                            "
	    "                                                                "
	    "                       " "LIMB_INSIDE_NEAR                      "
	    "                                                                "
	    "                                                          " "MIS"
	    "S_LIMB_NEAR                                                     "
	    "                                                                "
	    "                             ";
    static char obsnms[32*2] = "Earth                           " "MARS_ORBI"
	    "TER                    ";
    static char trgnms[32*2] = "Mars                            " "PHOBOS   "
	    "                       ";
    static char methds[500*4] = "ELLIPSOID                                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "         " "dsk/unprioritized/surfaces=\"high-res\"             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "  " "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                           " 
	    "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                       ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen), i_indx(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static doublereal frac;
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    static char dref[32];
    static doublereal dvec[3], limb[9];
    static char geom[160];
    static integer nlat;
    static doublereal dist, etol;
    static integer nlon;
    static doublereal elts[8], axlt, tipm[9]	/* was [3][3] */;
    extern doublereal vsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static doublereal xray[3];
    extern /* Subroutine */ int mtxv_(doublereal *, doublereal *, doublereal *
	    ), zzcorepc_(char *, doublereal *, doublereal *, doublereal *, 
	    ftnlen);
    static doublereal j2obs[3];
    static integer n;
    static doublereal radii[3], delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found, usecn;
    static char title[160];
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *), topen_(char *, ftnlen), spkw05_(
	    integer *, integer *, integer *, char *, doublereal *, doublereal 
	    *, char *, doublereal *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen);
    static doublereal tipfx[3], xform[9]	/* was [3][3] */;
    static logical uselt;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int dskxv_(logical *, char *, integer *, integer *
	    , doublereal *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *);
    static doublereal xspnt[3], dvecj2[3];
    extern /* Subroutine */ int el2cgv_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal state0[6], trgj2m[9]	/* was [3][3] */, srfvj2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen), boddef_(char *, integer *,
	     ftnlen);
    static doublereal et, te;
    static integer abcidx, drefid, handle[2], obscde;
    static doublereal depoch;
    static integer frcode;
    extern logical matchi_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal lmboff[3], negvec[3];
    static char abcorr[10];
    static integer trgcde;
    extern doublereal clight_(void);
    static integer bodyid;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     t_pck08__(char *, logical *, logical *, ftnlen);
    static char fixref[32], method[500], srfnms[32*4], target[32], trgfrm[32],
	     obsrvr[32];
    extern logical exists_(char *, ftnlen);
    static doublereal axista[6], dj2[3], dvecfx[3], lcentr[3], obspos[3], 
	    obsvec[3], raydir[3], raysta[6], refpos[3], smajor[3], sminor[3], 
	    spoint[3], spntlt, srfvec[3], ssbobs[6], ssbtrg[6], tmpvec[3], 
	    trgepc, xepoch, xobsps[3], xsrfvc[3];
    static integer clssid, geomix, obsidx, refctr, refidx, srfbod[4], srfids[
	    4], surfid, srflst[100], trgidx;
    static logical usestl;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     tstlsk_(void), spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), conics_(doublereal *, doublereal *, doublereal *)
	    , spkcls_(integer *), spklef_(char *, integer *, ftnlen);
    static logical fnd;
    extern /* Subroutine */ int pcpool_(char *, integer *, char *, ftnlen, 
	    ftnlen), pipool_(char *, integer *, integer *, ftnlen), delfil_(
	    char *, ftnlen), furnsh_(char *, ftnlen), cnmfrm_(char *, integer 
	    *, char *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), bodvar_(integer *, char *, integer 
	    *, doublereal *, ftnlen), namfrm_(char *, integer *, ftnlen);
    static integer cls;
    extern /* Subroutine */ int frinfo_(integer *, integer *, integer *, 
	    integer *, logical *);
    static doublereal dlt;
    extern /* Subroutine */ int spkezp_(integer *, doublereal *, char *, char 
	    *, integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    extern doublereal rpd_(void);
    extern /* Subroutine */ int pxform_(char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen);
    static doublereal sep;
    extern /* Subroutine */ int spkpos_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), vminus_(doublereal *, doublereal *), edlimb_(doublereal *
	    , doublereal *, doublereal *, doublereal *, doublereal *);
    static char utc[50];
    extern /* Subroutine */ int spkcpt_(doublereal *, char *, char *, 
	    doublereal *, char *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer mix;
    static doublereal tgt[3], tol, xte, rlt;
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    static doublereal tlt;
    extern /* Subroutine */ int sincpt_(char *, char *, doublereal *, char *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen), chcksd_(char *, doublereal *, char *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen), spkssb_(integer *, 
	    doublereal *, char *, doublereal *, ftnlen);
    static doublereal xlt;
    extern /* Subroutine */ int unload_(char *, ftnlen), cleard_(integer *, 
	    doublereal *), mxv_(doublereal *, doublereal *, doublereal *);
    static doublereal xpt[3];
    extern /* Subroutine */ int spkuef_(integer *);
    static doublereal dj2m[9]	/* was [3][3] */;

/* $ Abstract */

/*     Exercise the higher-level SPICELIB geometry routine SINCPT. */
/*     Use both DSK-based and ellipsoidal target shape models. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine SINCPT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 3.0.0 02-APR-2016 (NJB) */

/*        Updated to test DSK usage. Also more thoroughly */
/*        tests ellipsoid target cases. */

/* -    TSPICE Version 2.0.0, 02-APR-2012 (NJB) */

/*           Added cases for bad aberration correction values. */

/* -    TSPICE Version 1.0.0, 22-FEB-2008 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local parameters */

/*      DOUBLE PRECISION      VTIGHT */
/*      PARAMETER           ( VTIGHT  = 1.D-14 ) */

/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     REFS is a two-dimensional array. There's a set of */
/*     ray reference  frames for each target. Currently */
/*     there are only two targets: Mars and Phobos. */


/*     Note that the last two method strings are identical. This */
/*     is done to test the logic that uses saved values obtained */
/*     by parsing method string. */


/*     Begin every test family with an open call. */

    topen_("F_SINCPT", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create SPK, PCK file.", (ftnlen)29);
    tstspk_("sincpt_spk.bsp", &c_true, handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK file, load it, and delete it. */

    t_pck08__("test_0008.tpc", &c_true, &c_false, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create LSK, load it, and delete it. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set time. */

    s_copy(utc, "2004 FEB 17", (ftnlen)50, (ftnlen)11);
    str2et_(utc, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a Mars orbiter SPK file. */

    spkopn_("orbiter.bsp", "orbiter.bsp", &c__0, &handle[1], (ftnlen)11, (
	    ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up elements defining a state.  The elements expected */
/*     by CONICS are: */

/*        RP      Perifocal distance. */
/*        ECC     Eccentricity. */
/*        INC     Inclination. */
/*        LNODE   Longitude of the ascending node. */
/*        ARGP    Argument of periapse. */
/*        M0      Mean anomaly at epoch. */
/*        T0      Epoch. */
/*        MU      Gravitational parameter. */

    elts[0] = 3800.;
    elts[1] = .1;
    elts[2] = rpd_() * 80.;
    elts[3] = 0.;
    elts[4] = rpd_() * 90.;
    elts[5] = 0.;
    elts[6] = et;
    elts[7] = 42828.314;
    conics_(elts, &et, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = jyear_() * -10;
    d__2 = jyear_() * 10;
    spkw05_(&handle[1], &c_n499, &c__499, "MARSIAU", &d__1, &d__2, "Mars orb"
	    "iter", &elts[7], &c__1, state0, &et, (ftnlen)7, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the new SPK file. */

    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add the orbiter's name/ID mapping to the kernel pool. */

    pcpool_("NAIF_BODY_NAME", &c__1, obsnms + 32, (ftnlen)14, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_BODY_CODE", &c__1, &c_n499, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add an incomplete frame definition to the kernel pool; */
/*     we'll need this later. */

    pipool_("FRAME_BAD_NAME", &c__1, &c_n666, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 1200;
    nlat = 600;
    if (exists_("sincpt_dsk0.bds", (ftnlen)15)) {
	delfil_("sincpt_dsk0.bds", (ftnlen)15);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "sincpt_dsk0.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars is very low-res. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 40;
    nlat = 20;
    if (exists_("sincpt_dsk1.bds", (ftnlen)15)) {
	delfil_("sincpt_dsk1.bds", (ftnlen)15);
    }

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "sincpt_dsk1.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("sincpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is low-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("sincpt_dsk2.bds", (ftnlen)15)) {
	delfil_("sincpt_dsk2.bds", (ftnlen)15);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "sincpt_dsk2.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("sincpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("sincpt_dsk3.bds", (ftnlen)15)) {
	delfil_("sincpt_dsk3.bds", (ftnlen)15);
    }

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "sincpt_dsk3.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("sincpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 401;
    srfids[2] = 1;
    s_copy(srfnms + 64, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[3] = 401;
    srfids[3] = 2;
    s_copy(srfnms + 96, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__4, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__4, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Main test loop follows. */


/*     Loop over every choice of observer. */

    for (obsidx = 1; obsidx <= 2; ++obsidx) {
	s_copy(obsrvr, obsnms + (((i__1 = obsidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("obsnms", i__1, "f_sincpt__", (ftnlen)610)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the observer ID code. */

	bodn2c_(obsrvr, &obscde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over every choice of target. */

	for (trgidx = 1; trgidx <= 2; ++trgidx) {
	    s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? 
		    i__1 : s_rnge("trgnms", i__1, "f_sincpt__", (ftnlen)622)) 
		    << 5), (ftnlen)32, (ftnlen)32);

/*           Set the target ID code. */

	    bodn2c_(target, &trgcde, &found, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Get target body-fixed frame name. */

	    cnmfrm_(target, &frcode, trgfrm, &found, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*           Get target radii. */

	    bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over every viewing geometry case. */

	    for (geomix = 1; geomix <= 4; ++geomix) {
		s_copy(geom, geoms + ((i__1 = geomix - 1) < 4 && 0 <= i__1 ? 
			i__1 : s_rnge("geoms", i__1, "f_sincpt__", (ftnlen)
			648)) * 160, (ftnlen)160, (ftnlen)160);

/*              Loop over every aberration correction choice. */

		for (abcidx = 1; abcidx <= 9; ++abcidx) {
		    s_copy(abcorr, abcs + ((i__1 = abcidx - 1) < 9 && 0 <= 
			    i__1 ? i__1 : s_rnge("abcs", i__1, "f_sincpt__", (
			    ftnlen)655)) * 10, (ftnlen)10, (ftnlen)10);

/*                 Set up some logical variables describing the */
/*                 attributes of the selected correction. */

		    uselt = s_cmp(abcorr, "None", (ftnlen)10, (ftnlen)4) != 0;
		    usecn = s_cmp(abcorr, "Cn", (ftnlen)2, (ftnlen)2) == 0 || 
			    s_cmp(abcorr, "Xcn", (ftnlen)3, (ftnlen)3) == 0;
		    usestl = i_indx(abcorr, "+s", (ftnlen)10, (ftnlen)2) != 0;

/*                 Loop over every direction vector frame choice. */

		    for (refidx = 1; refidx <= 4; ++refidx) {
			s_copy(dref, refs + (((i__1 = refidx + (trgidx << 2) 
				- 5) < 8 && 0 <= i__1 ? i__1 : s_rnge("refs", 
				i__1, "f_sincpt__", (ftnlen)673)) << 5), (
				ftnlen)32, (ftnlen)32);

/*                    Set light time RLT from observer to center of */
/*                    frame for the direction vector. */

			namfrm_(dref, &drefid, (ftnlen)32);
			frinfo_(&drefid, &refctr, &cls, &clssid, &fnd);
			spkezp_(&refctr, &et, "J2000", abcorr, &obscde, 
				refpos, &rlt, (ftnlen)5, (ftnlen)10);

/*                    We'll need the epoch DEPOCH associated */
/*                    with the center of DREF.  RLT is the */
/*                    light time from DREF's center to the observer. */

			zzcorepc_(abcorr, &et, &rlt, &depoch, (ftnlen)10);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Look up the transformation from frame DREF to */
/*                    J2000. We don't need this right away, but we'll */
/*                    have occasion to use it later. */

			pxform_(dref, "J2000", &depoch, dj2m, (ftnlen)32, (
				ftnlen)5);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Loop over all method choices. */

			for (mix = 1; mix <= 4; ++mix) {
			    s_copy(method, methds + ((i__1 = mix - 1) < 4 && 
				    0 <= i__1 ? i__1 : s_rnge("methds", i__1, 
				    "f_sincpt__", (ftnlen)705)) * 500, (
				    ftnlen)500, (ftnlen)500);

/* --- Case: ------------------------------------------------------ */

			    s_copy(title, "Observer = #; Target = #; Geometr"
				    "y = #; ABCORR = #; DREF = #; METHOD = #.",
				     (ftnlen)160, (ftnlen)73);
			    repmc_(title, "#", obsrvr, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)32, (ftnlen)160);
			    repmc_(title, "#", target, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)32, (ftnlen)160);
			    repmc_(title, "#", geom, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)160, (ftnlen)160);
			    repmc_(title, "#", abcorr, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)10, (ftnlen)160);
			    repmc_(title, "#", dref, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)32, (ftnlen)160);
			    repmc_(title, "#", method, title, (ftnlen)160, (
				    ftnlen)1, (ftnlen)500, (ftnlen)160);
			    tcase_(title, (ftnlen)160);
			    if (s_cmp(geom, "POINT_AT_CENTER", (ftnlen)160, (
				    ftnlen)15) == 0) {

/*                          Look up direction vector using current frame */
/*                          and aberration correction.  The direction */
/*                          vector is going to point to the target's */
/*                          center, so we should hit the target. */

				spkpos_(target, &et, dref, abcorr, obsrvr, 
					dvec, &dlt, (ftnlen)32, (ftnlen)32, (
					ftnlen)10, (ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
			    } else if (s_cmp(geom, "MISS_BACKWARD", (ftnlen)
				    160, (ftnlen)13) == 0) {

/*                          Set the pointing direction to the inverse of */
/*                          that obtained in the 'POINT_AT_CENTER' case. */

				spkpos_(target, &et, dref, abcorr, obsrvr, 
					negvec, &dlt, (ftnlen)32, (ftnlen)32, 
					(ftnlen)10, (ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				vminus_(negvec, dvec);
			    } else if (s_cmp(geom, "LIMB_INSIDE_NEAR", (
				    ftnlen)160, (ftnlen)16) == 0 || s_cmp(
				    geom, "MISS_LIMB_NEAR", (ftnlen)160, (
				    ftnlen)14) == 0) {

/*                          Find the limb of the target based on the */
/*                          aberration-corrected target center position. */
/*                          Select ray to hit limb plane along major */
/*                          axis, slightly inside or slightly outside */
/*                          the ellipse, depending on the geometry case. */

/*                          Note we're looking up the target state in */
/*                          the target's body-fixed frame, not in the */
/*                          DREF frame. */

				spkpos_(target, &et, trgfrm, abcorr, obsrvr, 
					negvec, &tlt, (ftnlen)32, (ftnlen)32, 
					(ftnlen)10, (ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				vminus_(negvec, obsvec);

/*                          Get the limb's center and semi-axis vectors. */

				edlimb_(radii, &radii[1], &radii[2], obsvec, 
					limb);
				el2cgv_(limb, lcentr, smajor, sminor);

/*                          Improve the limb if we're using aberration */
/*                          corrections. */

/*                          We'll treat one endpoint of the limb's major */
/*                          axis as an ephemeris object, and we'll */
/*                          compute the aberration-corrected position of */
/*                          this object. */

/*                          To get an accurate limb, we'll find the */
/*                          light time from observer to tip of */
/*                          semi-major axis and get an improved light */
/*                          time estimate. */
				if (uselt) {
				    vadd_(lcentr, smajor, tipfx);

/*                             Find the aberration-corrected position of */
/*                             the tip of the major axis. Compute the */
/*                             offset from the original position. */

				    spkcpt_(tipfx, target, trgfrm, &et, 
					    trgfrm, "TARGET", abcorr, obsrvr, 
					    axista, &axlt, (ftnlen)32, (
					    ftnlen)32, (ftnlen)32, (ftnlen)6, 
					    (ftnlen)10, (ftnlen)32);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    vsub_(axista, tipfx, lmboff);

/*                             Shift the limb center by the offset we */
/*                             just computed. */

				    vadd_(lcentr, lmboff, tmpvec);
				    vequ_(tmpvec, lcentr);

/*                             We've compute our improved limb estimate. */

				}

/*                          Pick our target point near the limb. The */
/*                          point is 1+/- DELTA of the semi-major axis */
/*                          length out from the center, along one of the */
/*                          semi- major axes. */

/*                          A challenge that occurs here, since our */
/*                          target is a DSK-based shape, is that the */
/*                          actual limb is a polygon inscribed in the */
/*                          ellipsoid limb. Depending on the orientation */
/*                          of the polygon, the semi-major axis of the */
/*                          ellipsoid limb may extend outside of the DSK */
/*                          shape, and our target point may define a ray */
/*                          that will miss the ellipsoid even for cases */
/*                          where an intersection should occur. */

/*                          We'll mitigate this problem for the */
/*                          geometric case by modifying the target */
/*                          point. We'll try to find a surface point on */
/*                          the DSK shape "below" the target point, and */
/*                          if we find it, we'll use it instead of the */
/*                          original target. */

				if (usecn) {
				    delta = 1e-5;
				} else if (uselt) {
				    delta = .001;
				} else {

/*                             Value used for ellipsoidal targets is */

/*                                DELTA = 1.D-9 */

/*                             We can still use this value if we're */
/*                             using the high-resolution DSK for Mars. */
/*                             Otherwise, we need a point deeper inside */
/*                             the ellipsoid, which corresponds to a */
/*                             larger value of DELTA. */

				    if (matchi_(method, "*HIGH*", "*", "?", (
					    ftnlen)500, (ftnlen)6, (ftnlen)1, 
					    (ftnlen)1)) {
					if (s_cmp(target, "MARS", (ftnlen)32, 
						(ftnlen)4) == 0) {
					    delta = 1e-9;
					} else {
					    delta = .001;
					}
				    } else {
					delta = .01;
				    }
				}
				if (s_cmp(geom, "MISS", (ftnlen)4, (ftnlen)4) 
					== 0) {
				    frac = delta + 1.;
				} else {
				    frac = 1. - delta;
				}
				vlcom_(&c_b160, lcentr, &frac, smajor, tgt);
				if (! uselt) {

/*                             Find the surface point "under" TGT. This */
/*                             can be done simply only for the geometric */
/*                             case, because the limb has not been */
/*                             translated to account for aberration */
/*                             corrections. */

				    vminus_(tgt, raydir);
				    dskxv_(&c_false, target, &c__0, srflst, &
					    et, trgfrm, &c__1, tgt, raydir, 
					    xpt, &fnd, (ftnlen)32, (ftnlen)32)
					    ;
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    if (fnd) {

/*                                If an intercept was found, use it */
/*                                instead of TGT. If an intercept was */
/*                                not found, TGT is already inside the */
/*                                tessellated ellipsoid. */

					vequ_(xpt, tgt);
				    }
				}

/*                          Our ray extends from the observer through */
/*                          the target point. */

				vsub_(tgt, obsvec, dvecfx);
				sep = vsep_(negvec, dvecfx);

/*                          Convert the ray from the target body fixed */
/*                          frame to J2000, then from J2000 to the DREF */
/*                          frame. We need the target frame epoch TE to */
/*                          find the first transformation matrix TIPM. */

				zzcorepc_(abcorr, &et, &tlt, &te, (ftnlen)10);
				pxform_("J2000", trgfrm, &te, tipm, (ftnlen)5,
					 (ftnlen)32);
				mtxv_(tipm, dvecfx, dvecj2);

/*                          The matrix DJ2M maps from DREF to J2000, so */
/*                          apply the transpose of this matrix to obtain */
/*                          DVEC. */

				mtxv_(dj2m, dvecj2, dvec);
			    } else {

/*                          Oops!  Name mismatch. */

				sigerr_("SPICE(BUG)", (ftnlen)10);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
			    }

/*                       Find the surface intercept point. */

			    sincpt_(method, target, &et, trgfrm, abcorr, 
				    obsrvr, dref, dvec, spoint, &trgepc, 
				    srfvec, &found, (ftnlen)500, (ftnlen)32, (
				    ftnlen)32, (ftnlen)10, (ftnlen)32, (
				    ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       Check the results. */

			    if (! found) {
				if (s_cmp(geom, "MISS", (ftnlen)4, (ftnlen)4) 
					== 0) {

/*                             FOUND should be .FALSE.; the other outputs */
/*                             are undefined. */

				    chcksl_("FOUND", &found, &c_false, ok, (
					    ftnlen)5);
				} else {

/*                             We're supposed to have an intercept. */
/*                             Force an error signal. */

				    chcksl_("FOUND", &found, &c_true, ok, (
					    ftnlen)5);
				}
			    } else {

/*                          FOUND is true. */

/*                          Compute the observer position relative to */
/*                          the target center, consistent with the */
/*                          aberration corrections applicable to the */
/*                          apparent intercept point. Also compute the */
/*                          distance of between the observer and the */
/*                          apparent intercept point. */
				vsub_(spoint, srfvec, obspos);

/*                          Let DIST be the length of SRFVEC. */

				dist = vnorm_(srfvec);

/*                          The target epoch had better be consistent */
/*                          with DIST and ABCORR. */

				xlt = dist / clight_();
				zzcorepc_(abcorr, &et, &xlt, &xepoch, (ftnlen)
					10);

/*                          This is a relative error check. */

				if (usecn) {
				    etol = 1e-12;
				} else {
				    etol = 1e-8;
				}
				chcksd_("TRGEPC", &trgepc, "~/", &xepoch, &
					etol, ok, (ftnlen)6, (ftnlen)2);

/*                          Get the transformation from the target frame */
/*                          to J2000. */
				pxform_(trgfrm, "J2000", &trgepc, trgj2m, (
					ftnlen)32, (ftnlen)5);
				chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                          Now transform DVEC to the J2000 frame. */

				mxv_(dj2m, dvec, dj2);

/*                          The following check applies only to the case */
/*                          in which the pointing direction is toward */
/*                          the target's center. */

				if (s_cmp(geom, "POINT_AT_CENTER", (ftnlen)
					160, (ftnlen)15) == 0) {

/*                             The angular separation of SRFVEC and DVEC */
/*                             should be pretty small when these vectors */
/*                             are compared in compatible reference */
/*                             frames. We don't expect these vectors to */
/*                             be identical (even theoretically) because */
/*                             they've been computed with different */
/*                             target epochs. */

/*                             First step: get SRFVEC in the J2000 */
/*                             frame. */

				    mxv_(trgj2m, srfvec, srfvj2);

/*                             Find the angular separation and make sure */
/*                             it's not too large. */

				    sep = vsep_(srfvj2, dj2);
				    chcksd_("DJ2 vs NEG2 SEP", &sep, "~", &
					    c_b185, &c_b186, ok, (ftnlen)15, (
					    ftnlen)1);
				}

/*                          End of sanity check test for the */
/*                          POINT_AT_CENTER case. */

/*                          Having made it this far, we're ready for */
/*                          some more rigorous tests. In particular, */
/*                          we're going treat SPOINT as an ephemeris */
/*                          object and find its aberration-corrected */
/*                          position relative to the observer in J2000 */
/*                          coordinates. This computation will allow us */
/*                          to derive expected values of TRGEPC, OBSPOS, */
/*                          the transformation from the J2000 frame to */
/*                          the target body-fixed frame at TRGEPC. We */
/*                          will verify that the aberration-corrected */
/*                          location of SPOINT, lies on the ray DVEC: */
/*                          this is the criterion we used to define */
/*                          SPOINT. */

/*                          These tests are primarily of interest when */
/*                          aberration corrections are used, but they */
/*                          still serve as a consistency check for the */
/*                          geometric case. */

/*                          We're expecting to get good agreement */
/*                          between all of these items and their */
/*                          counterparts obtained from SINCPT, */
/*                          especially when we use converged Newtonian */
/*                          corrections. */
				if (eqstr_(obsrvr, "EARTH", (ftnlen)32, (
					ftnlen)5)) {
				    if (usecn) {
					tol = 1e-12;
				    } else if (uselt) {
					tol = 1e-8;
				    } else {
					tol = 1e-12;
				    }
				} else {

/*                             Use looser tolerances for the Mars */
/*                             orbiter. For the orbiter, small errors in */
/*                             SPOINT lead to larger relative errors in */
/*                             DIST and SEP. */
				    if (usecn) {
					tol = 1e-10;
				    } else if (uselt) {
					tol = 5e-6;
				    } else {
					tol = 1e-10;
				    }
				    if (s_cmp(geom, "LIMB_INSIDE_NEAR", (
					    ftnlen)160, (ftnlen)16) == 0) {

/*                                Loosen the tolerance so the */
/*                                MAC/OSX/Native C platform can handle */
/*                                these tests. */

					tol *= 100.;
				    }
				}

/*                          Find the aberration-corrected location of */
/*                          SPOINT. */

/*                          Prior to N0065, we had to do this manually. */
/*                          Now we can call a SPICE routine to do the */
/*                          job. */

				spkcpt_(spoint, target, trgfrm, &et, "J2000", 
					"TARGET", abcorr, obsrvr, raysta, &
					spntlt, (ftnlen)32, (ftnlen)32, (
					ftnlen)5, (ftnlen)6, (ftnlen)10, (
					ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                          XRAY is our expected result. */

				vequ_(raysta, xray);

/*                          Compute the expected target epoch. */

				zzcorepc_(abcorr, &et, &spntlt, &xte, (ftnlen)
					10);
				chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                          Moment of truth: XRAY is the J2000 vector */
/*                          from the observer to the */
/*                          aberration-corrected position of our */
/*                          "ephemeris object" located on the target */
/*                          surface at location SPOINT. If SPOINT were */
/*                          correct in the first place, then XRAY should */
/*                          be lined up with our boresight direction */
/*                          DVEC, when DVEC is rotated to the J2000 */
/*                          frame. */

/*                          Actually, we computed DVEC in the J2000 */
/*                          frame long ago: this vector is called DJ2. */

				chcksd_("TRGEPC vs XTE", &trgepc, "~/", &xte, 
					&tol, ok, (ftnlen)13, (ftnlen)2);
				sep = vsep_(dj2, xray);
				chcksd_("XRAY vs DJ2 sep", &sep, "~", &c_b185,
					 &tol, ok, (ftnlen)15, (ftnlen)1);

/*                          Check DIST against its predicted value. */

				d__1 = vnorm_(xray);
				chcksd_("DIST", &dist, "~/", &d__1, &tol, ok, 
					(ftnlen)4, (ftnlen)2);

/*                          Create a predicted value for SRFVEC: convert */
/*                          XRAY to the target frame using XFORM. */

				pxform_("J2000", trgfrm, &xte, xform, (ftnlen)
					5, (ftnlen)32);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				mxv_(xform, xray, xsrfvc);
				chckad_("SRFVEC", srfvec, "~~/", xsrfvc, &
					c__3, &tol, ok, (ftnlen)6, (ftnlen)3);

/*                          The following test only makes sense when */
/*                          stellar aberration is not used. (When */
/*                          stellar aberration IS used, SPOINT does not */
/*                          lie on the ray; the image of SPOINT under */
/*                          the stellar aberration correction lies on */
/*                          the ray. */

				if (! usestl) {

/*                             Create a predicted value for SPOINT: */
/*                             convert the (optionally light-time */
/*                             corrected) target-observer vector to the */
/*                             target body-fixed frame at epoch XTE and */
/*                             add to SRFVEC to form XSPNT. */

				    spkssb_(&obscde, &et, "J2000", ssbobs, (
					    ftnlen)5);
				    spkssb_(&trgcde, &xte, "J2000", ssbtrg, (
					    ftnlen)5);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    vsub_(ssbobs, ssbtrg, j2obs);
				    mxv_(xform, j2obs, xobsps);
				    vadd_(xobsps, srfvec, xspnt);

/*                             Use absolute tolerances for this check: */
/*                             1cm for converged light time; 1km for */
/*                             non-converged. */

				    if (usecn) {
					tol = 1e-5;
				    } else {
					tol = 1.;
				    }
				    chckad_("SPOINT", spoint, "~~", xspnt, &
					    c__3, &tol, ok, (ftnlen)6, (
					    ftnlen)2);
				}
			    }

/*                       We're finished with the consistency checks for */
/*                       the intercept cases. */

			}

/*                    End of the method loop. */

		    }

/*                 End of the reference frame loop. */

		}

/*              End of the aberration correction loop. */

	    }

/*           End of the geometry case loop. */

	}

/*        End of the target loop. */

    }

/*     End of the observer loop. */

/* *********************************************************************** */

/*     Normal case: input handling */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Input handling tests:  make sure target and observer */
/*     can be identified using integer "names." */

    tcase_("Use integer observer and target names.", (ftnlen)38);

/*     Set up the ray first. */

    spkpos_("499", &et, dref, abcorr, "399", dvec, &dlt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     Set target and target-fixed frame. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    sincpt_("Ellipsoid", "MARS", &et, fixref, abcorr, "Earth", dref, dvec, 
	    xspnt, &xepoch, xsrfvc, &found, (ftnlen)9, (ftnlen)4, (ftnlen)32, 
	    (ftnlen)10, (ftnlen)5, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    sincpt_("Ellipsoid", "499", &et, fixref, abcorr, "399", dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)3, (ftnlen)32,
	     (ftnlen)10, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b185, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("DIST", &dist, "=", &dist, &c_b185, ok, (ftnlen)4, (ftnlen)1);
/* *********************************************************************** */

/*     Normal case: state change detection */

/* *********************************************************************** */

/*     Certain subsystem state changes must be detected and responded to */
/*     by SINCPT. The subsystems (or structures) having states that must */
/*     be monitored are: */

/*        - Target name-ID mapping */

/*        - Observer name-ID mapping */

/*        - Surface name-ID mapping */

/*        - Ray direction frame definition */

/*        - Target body-fixed frame definition */

/*        - ZZDSKBSR state */


/* --- Case: ------------------------------------------------------ */

    tcase_("Target name changed to JUPITER for ID code 499.", (ftnlen)47);

/*     First, get expected intercept. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Set up the ray first. */

    spkpos_("499", &et, dref, abcorr, "399", dvec, &dlt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sincpt_("Ellipsoid", target, &et, fixref, abcorr, obsrvr, dref, dvec, 
	    xspnt, &xepoch, xsrfvc, &found, (ftnlen)9, (ftnlen)32, (ftnlen)32,
	     (ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);
    boddef_("JUPITER", &c__499, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sincpt_("Ellipsoid", "JUPITER", &et, fixref, abcorr, obsrvr, dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)7, (ftnlen)32,
	     (ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(1) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);
/*     Restore original mapping. */

    boddef_("JUPITER", &c__599, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer name changed to SUN for ID code 399.", (ftnlen)45);
    boddef_("SUN", &c__399, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sincpt_("Ellipsoid", target, &et, fixref, abcorr, "SUN", dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)32, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars high-res surface name changed to AAAbbb.", (ftnlen)45);

/*     Get expected results first. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    spkpos_("499", &et, dref, abcorr, "399", dvec, &dlt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = 1", (ftnlen)500, (ftnlen)30);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, xspnt, &
	    xepoch, xsrfvc, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);
    s_copy(srfnms, "AAAbbb", (ftnlen)32, (ftnlen)6);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = AAAbbb", (ftnlen)500, (
	    ftnlen)35);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, spoint, &
	    trgepc, srfvec, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(1) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars high-res DSK.", (ftnlen)25);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    spkpos_("499", &et, dref, abcorr, "399", dvec, &dlt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = low-res", (ftnlen)500, (
	    ftnlen)36);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, xspnt, &
	    xepoch, xsrfvc, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     Unload the high-res DSK; set METHOD to remove */
/*     surface specification. */

    unload_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, spoint, &
	    trgepc, srfvec, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(1) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars low-res DSK; reload Mars high-res DSK.", (ftnlen)50);

/*     Restore DSK, unload low-res DSK, and repeat computation. */

    furnsh_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, spoint, &
	    trgepc, srfvec, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(1) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     Make sure the result matches that obtained with the */
/*     high-res DSK specified. */

    s_copy(method, "dsk/unprioritized/ SURFACES = \"HIGH-RES\" ", (ftnlen)500,
	     (ftnlen)41);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, xspnt, &
	    xepoch, xsrfvc, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("(0) FOUND", &found, &c_true, ok, (ftnlen)9);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xspnt, &c__3, &c_b185, ok, (ftnlen)6, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Error handling tests follow. */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Target name not translated", (ftnlen)26);

/*     Find the surface intercept point. */

    sincpt_("Ellipsoid", "xyz", &et, fixref, abcorr, obsrvr, dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)3, (ftnlen)32,
	     (ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer name not translated", (ftnlen)28);

/*     Find the surface intercept point. */

    sincpt_("Ellipsoid", target, &et, fixref, abcorr, "xyz", dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)32, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer coincides with target", (ftnlen)30);

/*     Find the surface intercept point. */

    sincpt_("Ellipsoid", target, &et, fixref, abcorr, target, dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)32, (ftnlen)
	    32, (ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unsupported computation method", (ftnlen)30);

/*     Find the surface intercept point. */

    sincpt_("xyz", target, &et, fixref, abcorr, obsrvr, dref, dvec, spoint, &
	    trgepc, srfvec, &found, (ftnlen)3, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Body-fixed frame is not centered on target. ", (ftnlen)44);

/*     Find the surface intercept point.  Use reference frame 'BAD' */
/*     for direction vector. */

    sincpt_("Ellipsoid", "Mars_orbiter", &et, fixref, abcorr, "EARTH", "BAD", 
	    dvec, spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)12, (
	    ftnlen)32, (ftnlen)10, (ftnlen)5, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction. ", (ftnlen)27);

/*     Find the surface intercept point. Use unrecognized aberration */
/*     correction. */

    sincpt_("Ellipsoid", "Mars_orbiter", &et, fixref, "ZZZ", "EARTH", "IAU_E"
	    "ARTH", dvec, spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)
	    12, (ftnlen)32, (ftnlen)3, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction: relativistic ", (ftnlen)40);

/*     Find the surface intercept point. Use relativistic aberration */
/*     correction. */

    sincpt_("Ellipsoid", "Mars", &et, fixref, "RL", "EARTH", "IAU_EARTH", 
	    dvec, spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)4, (
	    ftnlen)32, (ftnlen)2, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    tcase_("Bad aberration correction: stellar aberration only", (ftnlen)50);

/*     Find the surface intercept point. Use stellar aberration */
/*     correction w/o light time correction. */

    sincpt_("Ellipsoid", "MARS", &et, fixref, "XS", "EARTH", "IAU_EARTH", 
	    dvec, spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)4, (
	    ftnlen)32, (ftnlen)2, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Zero direction vector", (ftnlen)21);
    cleard_(&c__3, dvec);
    sincpt_("Ellipsoid", target, &et, fixref, abcorr, obsrvr, dref, dvec, 
	    spoint, &trgepc, srfvec, &found, (ftnlen)9, (ftnlen)32, (ftnlen)
	    32, (ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded DSKs.", (ftnlen)15);
    unload_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    spkpos_("499", &et, dref, abcorr, "399", dvec, &dlt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = low-res", (ftnlen)500, (
	    ftnlen)36);
    sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, dvec, xspnt, &
	    xepoch, xsrfvc, &found, (ftnlen)500, (ftnlen)32, (ftnlen)32, (
	    ftnlen)10, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Clean up. */

    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sincpt_spk.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("orbiter.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sincpt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sincpt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sincpt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sincpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sincpt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_sincpt__ */

