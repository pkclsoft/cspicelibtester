/* f_spk01.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__6 = 6;
static doublereal c_b95 = 0.;
static integer c__71 = 71;
static integer c__14 = 14;
static integer c__15 = 15;
static integer c__3 = 3;
static doublereal c_b223 = 5e-12;

/* $Procedure F_SPK01 ( SPK data type 01 tests ) */
/* Subroutine */ int f_spk01__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer body;
    static doublereal last, step;
    static integer i__, j, n;
    static char label[80];
    extern /* Subroutine */ int dafgs_(doublereal *);
    static char frame[32], segid[80];
    static doublereal t01rec[71];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5], t13rec[198], t21rec[112];
    extern /* Subroutine */ int dafus_(doublereal *, integer *, integer *, 
	    doublereal *, integer *);
    static doublereal tbuff[10000];
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), moved_(doublereal *, integer *, 
	    doublereal *);
    static doublereal state[6];
    extern /* Subroutine */ int spkr01_(integer *, doublereal *, doublereal *,
	     doublereal *), topen_(char *, ftnlen), spkw01_(integer *, 
	    integer *, integer *, char *, doublereal *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    t_success__(logical *);
    static doublereal dc[2];
    static integer ic[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     daffna_(logical *), dafbfs_(integer *);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), dafcls_(
	    integer *), delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal dlbuff[710000]	/* was [71][10000] */;
    static integer frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    extern /* Subroutine */ int dafopr_(char *, integer *, ftnlen);
    static integer center;
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen), unload_(
	    char *, ftnlen), spkopa_(char *, integer *, ftnlen), spkcls_(
	    integer *), furnsh_(char *, ftnlen);
    static doublereal xstate[6];
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen);
    extern doublereal spd_(void);
    extern /* Subroutine */ int t_t13xmd__(doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    static integer han0, han2;

/* $ Abstract */

/*     Exercise routines associated with SPK data type 1. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare SPK data record size.  This record is declared in */
/*     SPKPVN and is passed to SPK reader (SPKRxx) and evaluator */
/*     (SPKExx) routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     1) If new SPK types are added, it may be necessary to */
/*        increase the size of this record.  The header of SPKPVN */
/*        should be updated as well to show the record size */
/*        requirement for each data type. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 05-OCT-2012 (NJB) */

/*        Updated to support increase of maximum degree to 27 for types */
/*        2, 3, 8, 9, 12, 13, 18, and 19. See SPKPVN for a list */
/*        of record size requirements as a function of data type. */

/* -    SPICELIB Version 1.0.0, 16-AUG-2002 (NJB) */

/* -& */

/*     End include file spkrec.inc */

/* $ Abstract */

/*     Declare parameters specific to SPK type 21. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 25-DEC-2013 (NJB) */

/* -& */

/*     MAXTRM      is the maximum number of terms allowed in each */
/*                 component of the difference table contained in a type */
/*                 21 SPK difference line. MAXTRM replaces the fixed */
/*                 table parameter value of 15 used in SPK type 1 */
/*                 segments. */

/*                 Type 21 segments have variable size. Let MAXDIM be */
/*                 the dimension of each component of the difference */
/*                 table within each difference line. Then the size */
/*                 DLSIZE of the difference line is */

/*                    ( 4 * MAXDIM ) + 11 */

/*                 MAXTRM is the largest allowed value of MAXDIM. */



/*     End of include file spk21.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the following routines that write and read */
/*     type 01 SPK data: */

/*        SPKE01 */
/*        SPKR01 */
/*        SPKW01 */

/*     The SPK type 01 subsetter has its own test family. */

/*     The higher level SPK routine */

/*        SPKPVN */

/*     is also exercised by this test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 27-MAR-2014 (NJB) */

/*        Previous delivery date: 20-OCT-2012 */


/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Note: SEGID is declared longer than SIDLEN because of the need to */
/*     hold a long string for testing error handling. */


/*     Saved variables */


/*     Save all local variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_SPK01", (ftnlen)7);

/*     Open a new SPK file for writing. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  open a new SPK file for writing.", (ftnlen)40);
    if (exists_("spk_test_01_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_01_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_01_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);

/*     Initialize the time and data buffers with values that are */
/*     recognizable but otherwise bogus. */

    for (i__ = 1; i__ <= 10000; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk01__", (ftnlen)261)] = i__ * 1e3;
	for (j = 1; j <= 71; ++j) {
	    dlbuff[(i__1 = j + i__ * 71 - 72) < 710000 && 0 <= i__1 ? i__1 : 
		    s_rnge("dlbuff", i__1, "f_spk01__", (ftnlen)265)] = tbuff[
		    (i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge(
		    "tbuff", i__2, "f_spk01__", (ftnlen)265)] + j - 1;
	}
    }

/*     Pick body, center, and frame. */

    body = 3;
    center = 10;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     Initial difference line count. */

    n = 100;

/*     Pick nominal time bounds. */

    first = 0.;
    last = tbuff[(i__1 = n - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk01__", (ftnlen)287)];

/*     Initialize segment identifier. */

    s_copy(segid, "spkw01 test segment", (ftnlen)80, (ftnlen)19);

/* ***************************************************************** */
/* * */
/* *    SPKW01 error cases: */
/* * */
/* ***************************************************************** */


/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid frame.", (ftnlen)14);
    spkw01_(&handle, &body, &center, "XXX", &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)3, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID too long.", (ftnlen)15);
    s_copy(segid, "1234567890123456789012345678912345678901234567890", (
	    ftnlen)80, (ftnlen)49);
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SEGIDTOOLONG)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID contains non-printable characters.", (ftnlen)40);
    s_copy(segid, "spkw01 test segment", (ftnlen)80, (ftnlen)19);
    *(unsigned char *)&segid[4] = '\a';
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid difference line count", (ftnlen)29);
    s_copy(segid, "spkw01 test segment", (ftnlen)80, (ftnlen)19);
    n = 0;
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = -1;
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = 100;

/* --- Case: ------------------------------------------------------ */

    tcase_("Descriptor times out of order", (ftnlen)29);
    first = last + 1.;
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);
    first = 0.;
    et = last;
    last = first - 1.;
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);
    last = et;

/* --- Case: ------------------------------------------------------ */

    tcase_("epochs out of order", (ftnlen)19);
    et = tbuff[2];
    tbuff[2] = tbuff[1];
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(TIMESOUTOFORDER)", ok, (ftnlen)22);
    tbuff[2] = et;

/* --- Case: ------------------------------------------------------ */

    tcase_("Gap following last epoch", (ftnlen)24);
    et = tbuff[(i__1 = n - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk01__", (ftnlen)417)];
    tbuff[(i__1 = n - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", i__1, 
	    "f_spk01__", (ftnlen)418)] = tbuff[(i__2 = n - 1) < 10000 && 0 <= 
	    i__2 ? i__2 : s_rnge("tbuff", i__2, "f_spk01__", (ftnlen)418)] - 
	    1e-6;
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);
    tbuff[(i__1 = n - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", i__1, 
	    "f_spk01__", (ftnlen)425)] = et;
/* ***************************************************************** */
/* * */
/* *    SPKR01 normal cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Make sure segment containing one difference line is readable.", (
	    ftnlen)61);
    last = tbuff[0];
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &c__1, 
	    dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafopr_("spk_test_01_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    chcksd_("Start time", dc, "=", &first, &c_b95, ok, (ftnlen)10, (ftnlen)1);
    chcksd_("Stop time", &dc[1], "=", &last, &c_b95, ok, (ftnlen)9, (ftnlen)1)
	    ;

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    spkr01_(&handle, descr, tbuff, t01rec);
    chckad_("Diff line", t01rec, "=", dlbuff, &c__71, &c_b95, ok, (ftnlen)9, (
	    ftnlen)1);
    spkcls_(&handle);

/* --- Case: ------------------------------------------------------ */


/*     Repeat the test with MXNREC records.  The new segment will mask */
/*     the previous one. */

    tcase_("Create and read segment with multiple difference lines.", (ftnlen)
	    55);
    spkopa_("spk_test_01_v1.bsp", &handle, (ftnlen)18);
    n = 10000;
    last = tbuff[(i__1 = n - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk01__", (ftnlen)509)];
    spkw01_(&handle, &body, &center, frame, &first, &last, segid, &n, dlbuff, 
	    tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open the file for read access; find the 2nd descriptor. */

    dafopr_("spk_test_01_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    chcksd_("Start time", dc, "=", &first, &c_b95, ok, (ftnlen)10, (ftnlen)1);
    chcksd_("Stop time", &dc[1], "=", &last, &c_b95, ok, (ftnlen)9, (ftnlen)1)
	    ;

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Difference line number *, time *", (ftnlen)80, (ftnlen)
		32);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk01__", (ftnlen)566)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	spkr01_(&handle, descr, &tbuff[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ?
		 i__2 : s_rnge("tbuff", i__2, "f_spk01__", (ftnlen)568)], 
		t01rec);
	chckad_(label, t01rec, "=", &dlbuff[(i__2 = i__ * 71 - 71) < 710000 &&
		 0 <= i__2 ? i__2 : s_rnge("dlbuff", i__2, "f_spk01__", (
		ftnlen)570)], &c__71, &c_b95, ok, (ftnlen)80, (ftnlen)1);
    }
    spkcls_(&handle);
/* ***************************************************************** */
/* * */
/* *    SPKR01/SPKE01/SPKPVN normal cases: */
/* * */
/* ***************************************************************** */

/*     We'll now create a type 1 segment containing some more or */
/*     less realistic data. We'll start states sampled from a */
/*     Jupiter barycenter segment created by TSTSPK. We'll use */
/*     these to create a type 13 record, which we'll then convert */
/*     to a type 1 record. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create realistic data for a type 01 record.", (ftnlen)43);

/*     Start out with a generic SPK file. */

    tstspk_("test.bsp", &c_false, &han0, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a type 13 record from a sequence of */
/*     epochs and states. Use a polynomial degree of 15. */
/*     The corresponding difference line will have */
/*     14 terms in each component of the difference table. */

    body = 5;
    center = 10;
    n = 8;
    step = spd_();
    first = 0.;
    last = first + (n - 1) * step;
    s_copy(segid, "Type 01 Jup Barycenter", (ftnlen)80, (ftnlen)22);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    furnsh_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t13rec[0] = (doublereal) n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * step;
	spkez_(&body, &et, frame, "NONE", &center, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 6 + 2;
	moved_(state, &c__6, &t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 
		: s_rnge("t13rec", i__2, "f_spk01__", (ftnlen)640)]);
	j = n * 6 + 1 + i__;
	t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 : s_rnge("t13rec", 
		i__2, "f_spk01__", (ftnlen)644)] = et;
    }
    unload_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a type 01 record from a type 13 record.", (ftnlen)46);
/*      CALL T13MDA ( T13REC, FIRST, LAST, T01REC, FOUND ) */
    t_t13xmd__(t13rec, &first, &last, t21rec, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a type 01 record from the type 21 record we just */
/*     created. */

    cleard_(&c__71, t01rec);

/*     Copy the reference epoch. */

    t01rec[0] = t21rec[1];

/*     Copy the stepsize vector. */

    moved_(&t21rec[2], &c__15, &t01rec[1]);

/*     Copy the reference position and velocity. */

    moved_(&t21rec[27], &c__6, &t01rec[16]);

/*     Copy the difference table. Since each component of the type 21 */
/*     difference table has a difference size from the corresponding */
/*     type 1 component, we must copy the components individually. */

    moved_(&t21rec[33], &c__15, &t01rec[22]);
    moved_(&t21rec[58], &c__15, &t01rec[37]);
    moved_(&t21rec[83], &c__15, &t01rec[52]);

/*     Don't copy the order parameters; set the parameters for */
/*     type 1 */

    t01rec[67] = 15.;
    t01rec[68] = 14.;
    t01rec[69] = 14.;
    t01rec[70] = 14.;
    chcksl_("Type 01 record created", &found, &c_true, ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a new SPK from a type 01 record.", (ftnlen)39);
    if (exists_("spk_test_01_v2.bsp", (ftnlen)18)) {
	delfil_("spk_test_01_v2.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_01_v2.bsp", "spk_test_01_v2.bsp", &c__0, &han2, (ftnlen)
	    18, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that our time buffer contains a single element. */

    spkw01_(&han2, &body, &center, frame, &first, &last, segid, &c__1, t01rec,
	     &last, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Here's the main event: test the states obtained from the */
/*     type 01 segment. */

    tcase_("Recover states from the type 01 segment.", (ftnlen)40);
    furnsh_("spk_test_01_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * step;
	et = brcktd_(&d__1, &first, &last);
	spkez_(&body, &et, frame, "NONE", &center, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 6 + 2;
	moved_(&t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 : s_rnge(
		"t13rec", i__2, "f_spk01__", (ftnlen)753)], &c__6, xstate);
	s_copy(label, "Position number *, time *", (ftnlen)80, (ftnlen)25);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &et, &c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	chckad_(label, state, "~~/", xstate, &c__3, &c_b223, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(label, "Velocity number *, time *", (ftnlen)80, (ftnlen)25);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &et, &c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	chckad_(label, &state[3], "~~/", &xstate[3], &c__3, &c_b223, ok, (
		ftnlen)80, (ftnlen)3);
    }
    unload_("spk_test_01_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Close and delete the SPK file. */

    tcase_("Clean up.", (ftnlen)9);
    dafcls_(&han0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_01_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_01_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_spk01__ */

