/* f_spk10.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__1950 = 1950;
static integer c__1000 = 1000;
static integer c__399 = 399;
static integer c__1 = 1;
static logical c_true = TRUE_;
static integer c__8 = 8;
static doublereal c_b31 = 0.;
static integer c__10 = 10;
static integer c__9 = 9;
static integer c__6 = 6;
static doublereal c_b107 = 1e-14;
static integer c__29 = 29;
static integer c__0 = 0;

/* $Procedure F_SPK10 ( Family of tests for types 10 ) */
/* Subroutine */ int f_spk10__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    integer body;
    doublereal dwdt, last;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    integer type__;
    extern /* Subroutine */ int mxvg_(doublereal *, doublereal *, integer *, 
	    integer *, doublereal *);
    char hi2ln[80*2];
    doublereal part1[6], part2[6];
    integer i__, n;
    doublereal begin, w;
    integer frame;
    doublereal epoch[29];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal descr[6];
    char ident[40];
    doublereal denom, elems[290], precm[36]	/* was [6][6] */;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    logical found;
    doublereal state[6];
    extern /* Subroutine */ int spkr10_(integer *, doublereal *, doublereal *,
	     doublereal *), vlcom_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), topen_(char *, ftnlen);
    doublereal numer, first;
    extern /* Subroutine */ int spkw10_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal tempv[3];
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal expst[6];
    extern /* Subroutine */ int t_success__(logical *);
    char low2ln[80*20];
    integer id;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    doublereal et;
    integer handle;
    doublereal lt;
    extern /* Subroutine */ int kclear_(void);
    doublereal dargdt;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen), kilfil_(char *, ftnlen), getelm_(integer *, 
	    char *, doublereal *, doublereal *, ftnlen);
    doublereal record[50];
    integer center;
    extern /* Subroutine */ int spklef_(char *, integer *, ftnlen);
    doublereal offset[29];
    extern /* Subroutine */ int vlcomg_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *), spkuef_(integer *), 
	    spkcls_(integer *);
    doublereal invprc[36]	/* was [6][6] */, expsta[174]	/* was [6][29]
	     */;
    extern /* Subroutine */ int furnsh_(char *, ftnlen);
    doublereal tmpsta[6], consts[8];
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), spksfs_(integer *, doublereal *, integer *, 
	    doublereal *, char *, logical *, ftnlen), spkuds_(doublereal *, 
	    integer *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), zzteme_(doublereal *, 
	    doublereal *), invstm_(doublereal *, doublereal *), tstlsk_(void);
    char ele[80*58];
    doublereal end, arg;
    extern /* Subroutine */ int xxsgp4e_(doublereal *, doublereal *), 
	    xxsgp4i_(doublereal *, doublereal *, integer *);
    extern doublereal spd_(void);
    char msg[24*29];
    doublereal vel[3];
    logical err[29];
    doublereal tol[29];
    integer spk1, spk2, spk3;

/* $ Abstract */

/*     This routine performs tests on SPK type 10 to make sure that */
/*     the correct routines are called and that subsetting occurs */
/*     correctly. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None. */

/* $ Keywords */

/*   None. */

/* $ Declarations */
/* $Procedure ZZSGP4 ( SGP4 parameters ) */

/* $ Abstract */

/*      Parameter assignments for SGP4 algorithm as expressed */
/*      by Vallado [2]. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     None. */

/* $ Declarations */

/*     None. */

/* $ Brief_I/O */

/*     None. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     None. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     J2    = GEOPHS(K_J2) */
/*     J3    = GEOPHS(K_J3) */
/*     J4    = GEOPHS(K_J4) */
/*     ER    = GEOPHS(K_ER) */
/*     XKE   = GEOPHS(K_KE) */

/*     TUMIN = 1.D0/XKE */
/*     J3OJ2 = J3/J2 */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*   [1] Hoots, F. R., and Roehrich, R. L. 1980. "Models for */
/*       Propagation of the NORAD Element Sets." Spacetrack Report #3. */
/*       U.S. Air Force: Aerospace Defense Command. */

/*   [2] Vallado, David, Crawford, Paul, Hujsak, Richard, and Kelso, T.S. */
/*       2006. Revisiting Spacetrack Report #3. Paper AIAA 2006-6753 */
/*       presented at the AIAA/AAS Astrodynamics Specialist Conference, */
/*       August 21-24, 2006. Keystone, CO. */

/* $ Author_and_Institution */

/*     E. D. Wright    (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0 22-JUL-2014 (EDW) */

/* -& */
/* $ Index_Entries */

/*  SGP4 */

/* -& */

/*      WGS gravitational constants IDs. */


/*      Gravitational constant indices. */


/*     The following parameters give the location in the GEOPHS */
/*     array of the various geophysical parameters needed for */
/*     the two line element sets. */

/*     K_J2  --- location of J2 */
/*     K_J3  --- location of J3 */
/*     K_J4  --- location if J4 */
/*     K_KE  --- location of KE = sqrt(GM) in earth-radii**1.5/MIN */
/*     K_QO  --- upper bound of atmospheric model in KM */
/*     K_SO  --- lower bound of atmospheric model in KM */
/*     K_ER  --- earth equatorial radius in KM. */
/*     K_AE  --- distance units/earth radius */


/*     Operation mode values, OPMODE. */


/*     An enumeration of the various components of the */
/*     elements array---ELEMS */

/*     KNDT20  --- location of NDT20 */
/*     KNDD60  --- location of NDD60 */
/*     KBSTAR  --- location of BSTAR */
/*     KINCL   --- location of INCL */
/*     KNODE0  --- location of NODE0 */
/*     KECC    --- location of ECC */
/*     KOMEGA  --- location of OMEGA */
/*     KMO     --- location of MO */
/*     KNO     --- location of NO */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   Indicates success or failure of test fmaily. */

/* $ Detailed_Input */

/*   None. */

/* $ Detailed_Output */

/*   OK           Boolean indicating if all test family cases passed. */

/* $ Parameters */

/*   None. */

/* $ Exceptions */

/*   None. */

/* $ Files */

/*   None. */

/* $ Particulars */

/*   None. */

/* $ Examples */

/*   None. */

/* $ Restrictions */

/*   None. */

/* $ Literature_References */

/*   None. */

/* $ Author_and_Institution */

/*      W.L. Taber      (JPL) */

/* $ Version */

/* -    SPICELIB Version 3.0.0, 24-AUG-2016 (EDW) (BVS) */

/*        Tests reimplemented for new TLE code. */
/*        Eliminated unneeded test for EV2LIN; the new code */
/*        logic unifies low and high orbit propagation */
/*        calculations. */

/* -    SPICELIB Version 2.0.0, 11-OCT-2013 (EDW) */

/*        Recoded to use new ZZTEME routine in SPKE10 and */
/*        corresponding logic. */

/*        Added a proper SPICE header. */

/*        Replace equality checks, "=", with tolerance check, "~". */

/* -    SPICELIB Version 1.1.0, 20-OCT-1999 (WLT) */

/*        Declared PI to be an EXTERNAL function. */

/* -& */
/* $ Index_Entries */

/*     SPK type 10 (two line elements) test cases */

/* -& */

/*     SPICELIB Functions */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_SPK10", (ftnlen)7);
    tcase_("Preliminaries --- load a leapseconds kernel ", (ftnlen)44);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Make sure we can create an SPK type 10 segment from a single set"
	    " of two-line elements. ", (ftnlen)87);
    kilfil_("type10.bsp", (ftnlen)10);

/*        We'll use the two-line elments for Topex given in the */
/*        header to GETELM. */

    s_copy(low2ln, "1 22076U 92052A   97173.53461370 -.00000038  00000-0  10"
	    "000-3 0   594", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 80, "2 22076  66.0378 163.4372 0008359 278.7732  81.2337"
	    " 12.80930736227550", (ftnlen)80, (ftnlen)69);

/*        Set the geophysical constants used by SPACE COMMAND in */
/*        the distributed code. */

    consts[0] = .001082616;
    consts[1] = -2.53881e-6;
    consts[2] = -1.65597e-6;
    consts[3] = .0743669161;
    consts[4] = 120.;
    consts[5] = 78.;
    consts[6] = 6378.135;
    consts[7] = 1.;
    getelm_(&c__1950, low2ln, epoch, elems, (ftnlen)80);
    first = epoch[0] - spd_() * 100.;
    last = epoch[0] + spd_() * 100.;
    id = -122076;
    spkopn_("type10.bsp", "TEST_FILE", &c__1000, &handle, (ftnlen)10, (ftnlen)
	    9);
    spkw10_(&handle, &id, &c__399, "J2000", &first, &last, "Test TOPEX", 
	    consts, &c__1, elems, epoch, (ftnlen)5, (ftnlen)10);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Make sure we can read read out of the file the data we just inse"
	    "rted. ", (ftnlen)70);
    et = epoch[0];
    spklef_("type10.bsp", &spk1, (ftnlen)10);
    spksfs_(&id, &et, &handle, descr, ident, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Unpack the descriptor and make sure it has the correct */
/*        data in it. */

    spkuds_(descr, &body, &center, &frame, &type__, &first, &last, &begin, &
	    end);
    spkr10_(&handle, descr, &et, record);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("CONSTS", consts, "=", record, &c__8, &c_b31, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("ELEM1", elems, "=", &record[8], &c__10, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chckad_("ELEM2", elems, "=", &record[22], &c__10, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    spkuef_(&spk1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Construct a longer file and make sure that we get the correct st"
	    "ates from SPKEZ. ", (ftnlen)81);
    kilfil_("type10_2.bsp", (ftnlen)12);
    s_copy(low2ln, "1 18123U 87 53  A 87324.61041692 -.00000023  00000-0 -75"
	    "103-5 0 00675", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 80, "2 18123  98.8296 152.0074 0014950 168.7820 191.3688"
	    " 14.12912554 21686", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 160, "1 18123U 87 53  A 87326.73487726  .00000045  00000"
	    "-0  28709-4 0 00684", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 240, "2 18123  98.8335 154.1103 0015643 163.5445 196.623"
	    "5 14.12912902 21988", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 320, "1 18123U 87 53  A 87331.40868801  .00000104  00000"
	    "-0  60183-4 0 00690", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 400, "2 18123  98.8311 158.7160 0015481 149.9848 210.222"
	    "0 14.12914624 22644", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 480, "1 18123U 87 53  A 87334.24129978  .00000086  00000"
	    "-0  51111-4 0 00702", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 560, "2 18123  98.8296 161.5054 0015372 142.4159 217.808"
	    "9 14.12914879 23045", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 640, "1 18123U 87 53  A 87336.93227900 -.00000107  00000"
	    "-0 -52860-4 0 00713", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 720, "2 18123  98.8317 164.1627 0014570 135.9191 224.232"
	    "1 14.12910572 23425", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 800, "1 18123U 87 53  A 87337.28635487  .00000173  00000"
	    "-0  10226-3 0 00726", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 880, "2 18123  98.8284 164.5113 0015289 133.5979 226.643"
	    "8 14.12916140 23475", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 960, "1 18123U 87 53  A 87339.05673569  .00000079  00000"
	    "-0  47069-4 0 00738", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 1040, "2 18123  98.8288 166.2585 0015281 127.9985 232.25"
	    "67 14.12916010 24908", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 1120, "1 18123U 87 53  A 87345.43010859  .00000022  0000"
	    "0-0  16481-4 0 00758", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 1200, "2 18123  98.8241 172.5226 0015362 109.1515 251.13"
	    "23 14.12915487 24626", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 1280, "1 18123U 87 53  A 87349.04167543  .00000042  0000"
	    "0-0  27370-4 0 00764", (ftnlen)80, (ftnlen)69);
    s_copy(low2ln + 1360, "2 18123  98.8301 176.1010 0015565 100.0881 260.20"
	    "47 14.12916361 25138", (ftnlen)80, (ftnlen)69);

/*        Parse the element lines to arrays or orbit parameters. */

    for (i__ = 0; i__ <= 8; ++i__) {
	getelm_(&c__1950, low2ln + ((i__1 = i__ << 1) < 20 && 0 <= i__1 ? 
		i__1 : s_rnge("low2ln", i__1, "f_spk10__", (ftnlen)338)) * 80,
		 &epoch[(i__2 = i__) < 29 && 0 <= i__2 ? i__2 : s_rnge("epoch"
		, i__2, "f_spk10__", (ftnlen)338)], &elems[(i__3 = i__ * 10) <
		 290 && 0 <= i__3 ? i__3 : s_rnge("elems", i__3, "f_spk10__", 
		(ftnlen)338)], (ftnlen)80);
    }
    first = epoch[0] - spd_() * .5;
    last = epoch[8] + spd_() * .5;

/*        Create the TLE based SPK. */

    id = -118123;
    spkopn_("type10_2.bsp", "TEST_FILE", &c__1000, &handle, (ftnlen)12, (
	    ftnlen)9);
    spkw10_(&handle, &id, &c__399, "J2000", &first, &last, "DMSP F8", consts, 
	    &c__9, elems, epoch, (ftnlen)5, (ftnlen)7);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        EPOCH(1) corresponds to ELEMS(1) */
/*        EPOCH(2) corresponds to ELEMS(11) */
/*        EPOCH(3) corresponds to ELEMS(21) */
/*        EPOCH(4) corresponds to ELEMS(13) */
/*        EPOCH(5) corresponds to ELEMS(41) */
/*        EPOCH(6) corresponds to ELEMS(51) */
/*        EPOCH(7) corresponds to ELEMS(61) */
/*        EPOCH(8) corresponds to ELEMS(71) */
/*        EPOCH(9) corresponds to ELEMS(81) */

    et = epoch[4] * .6 + epoch[5] * .4;
    spklef_("type10_2.bsp", &spk2, (ftnlen)12);
    xxsgp4i_(consts, &elems[40], &c__1);
    d__1 = (et - epoch[4]) / 60.;
    xxsgp4e_(&d__1, part1);
    xxsgp4i_(consts, &elems[50], &c__1);
    d__1 = (et - epoch[5]) / 60.;
    xxsgp4e_(&d__1, part2);
    numer = et - epoch[4];
    denom = epoch[5] - epoch[4];
    arg = numer * pi_() / denom;
    dargdt = pi_() / denom;
    w = cos(arg) * .5 + .5;
    dwdt = sin(arg) * -.5 * dargdt;
    d__1 = 1. - w;
    vlcomg_(&c__6, &w, part1, &d__1, part2, expst);
    d__1 = -dwdt;
    vlcom_(&dwdt, part1, &d__1, part2, vel);
    vadd_(vel, &expst[3], tempv);
    vequ_(tempv, &expst[3]);
    zzteme_(&et, precm);
    invstm_(precm, invprc);
    mxvg_(invprc, expst, &c__6, &c__6, tmpsta);
    moved_(tmpsta, &c__6, expst);
    spkez_(&id, &et, "J2000", "NONE", &c__399, state, &lt, (ftnlen)5, (ftnlen)
	    4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "=", expst, &c__6, &c_b31, ok, (ftnlen)5, (ftnlen)
	    1);
    tcase_("Using the previous file, make sure we get the correct state at o"
	    "ne second after the beginning of the segment.", (ftnlen)109);
    et = first + 1.;
    xxsgp4i_(consts, elems, &c__1);
    d__1 = (et - epoch[0]) / 60.;
    xxsgp4e_(&d__1, expst);
    zzteme_(&et, precm);
    invstm_(precm, invprc);
    mxvg_(invprc, expst, &c__6, &c__6, tmpsta);
    moved_(tmpsta, &c__6, expst);
    spkez_(&id, &et, "J2000", "NONE", &c__399, state, &lt, (ftnlen)5, (ftnlen)
	    4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~", expst, &c__6, &c_b107, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Using the same file make sure we get the correct state at one se"
	    "cond before the end of the segment. ", (ftnlen)100);
    et = last - 1.;
    xxsgp4i_(consts, &elems[80], &c__1);
    d__1 = (et - epoch[8]) / 60.;
    xxsgp4e_(&d__1, expst);
    zzteme_(&et, precm);
    invstm_(precm, invprc);
    mxvg_(invprc, expst, &c__6, &c__6, tmpsta);
    moved_(tmpsta, &c__6, expst);
    spkez_(&id, &et, "J2000", "NONE", &c__399, state, &lt, (ftnlen)5, (ftnlen)
	    4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~", expst, &c__6, &c_b107, ok, (ftnlen)5, (
	    ftnlen)1);
    spkuef_(&spk2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Make sure we can perform the same experiments using a deep space"
	    " satellite. ", (ftnlen)76);
    kilfil_("type10_3.bsp", (ftnlen)12);
    s_copy(hi2ln, "1 24846U 97031A   97179.08162378 -.00000182  00000-0  000"
	    "00+0 0   129", (ftnlen)80, (ftnlen)69);
    s_copy(hi2ln + 80, "2 24846   4.5222  86.7012 6052628 178.7924 183.5048 "
	    " 2.04105068    52", (ftnlen)80, (ftnlen)69);
    getelm_(&c__1950, hi2ln, epoch, elems, (ftnlen)80);
    first = epoch[0] - spd_() * 10.;
    last = epoch[0] + spd_() * 10.;
    id = -124846;
    spkopn_("type10_3.bsp", "TEST_FILE", &c__1000, &handle, (ftnlen)12, (
	    ftnlen)9);
    spkw10_(&handle, &id, &c__399, "J2000", &first, &last, "Test INTELSAT", 
	    consts, &c__1, elems, epoch, (ftnlen)5, (ftnlen)13);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = epoch[0];
    xxsgp4i_(consts, elems, &c__1);
    d__1 = (et - epoch[0]) / 60.;
    xxsgp4e_(&d__1, expst);
    zzteme_(&et, precm);
    invstm_(precm, invprc);
    mxvg_(invprc, expst, &c__6, &c__6, tmpsta);
    moved_(tmpsta, &c__6, expst);
    spklef_("type10_3.bsp", &spk3, (ftnlen)12);
    spkez_(&id, &et, "J2000", "NONE", &c__399, state, &lt, (ftnlen)5, (ftnlen)
	    4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~", expst, &c__6, &c_b107, ok, (ftnlen)5, (
	    ftnlen)1);
    spkuef_(&spk3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("TLE set mixing low and high orbit elements.", (ftnlen)43);

/*        Test TLE evaluations using SPICE code, compare results */
/*        to values calculated from non SPICE routines. */

/*        EXPSTA data values taken from output of tle_t_ref.f. */
/*        That program uses only Vallado 2006 code for TLE */
/*        propagation. */

/*        Evaluate each TLE at epoch, or at an epoch + offset */
/*        that causes an error signal. */

/*        ERR, boolean indicating if expected SPICE error. */
/*        MSG, expected SPICE error message, if any. */
/*        OFFSET, from TLE epoch, in TDB seconds */

    n = 1;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)518)) * 80, "1 00005U 58002B  "
	    " 00179.78495062  .00000023  00000-0  28098-4 0  4753", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)520)) * 80, "2 00005  34.2682 "
	    "348.7242 1859667 331.7664  19.3264 10.82419157413667", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)522)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)523)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)524)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)526)] = 7022.4652905749172;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)527)] = -1400.0829671389206;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)528)] = .039951552899067093;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)529)] = 1.8938410139509001;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)530)] = 6.4058937573052042;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)531)] = 4.5348072490078408;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)533)] = 4.9999999999999995e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)538)) * 80, "1 04632U 70093B  "
	    " 04031.91070959 -.00000084  00000-0  10000-3 0  9955", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)540)) * 80, "2 04632  11.4628 "
	    "273.1101 1450506 207.6000 143.9350  1.20231981 44145", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)542)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)543)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)544)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)546)] = 2334.114500153129;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)547)] = -41920.440341023626;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)548)] = -.038674373804744179;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)549)] = 2.8263210311692482;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)550)] = -.065091663977403971;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)551)] = .57093605288508975;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)553)] = 9.9999999999999991e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)558)) * 80, "1 06251U 62025E  "
	    " 06176.82412014  .00008885  00000-0  12808-3 0  3985", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)560)) * 80, "2 06251  58.0579 "
	    " 54.0425 0030035 139.1568 221.1854 15.56387291  6774", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)562)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)563)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)564)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)566)] = 3988.3102258099252;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)567)] = 5498.9665707163922;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)568)] = .90055878311511461;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)569)] = -3.2900327369587359;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)570)] = 2.3576528189349908;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)571)] = 6.4966234730266708;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)573)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)578)) * 80, "1 08195U 75081A  "
	    " 06176.33215444  .00000099  00000-0  11873-3 0   813", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)580)) * 80, "2 08195  64.1586 "
	    "279.0717 6877146 264.7651  20.2257  2.00491383225656", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)582)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)583)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)584)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)586)] = 2349.8948327983148;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)587)] = -14785.938111219008;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)588)] = .021193776981972199;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)589)] = 2.7214880947491822;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)590)] = -3.2568116536917593;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)591)] = 4.4984166710329783;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)593)] = 4.9999999999999995e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)598)) * 80, "1 09880U 77021A  "
	    " 06176.56157475  .00000421  00000-0  10000-3 0  9814", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)600)) * 80, "2 09880  64.5968 "
	    "349.3786 7069051 270.0229  16.3320  2.00813614112380", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)602)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)603)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)604)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)606)] = 13020.067503971142;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)607)] = -2449.0719342705006;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)608)] = 1.1589602950956694;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)609)] = 4.2473639336004894;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)610)] = 1.5971785003727861;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)611)] = 4.9567086099164372;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)613)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)618)) * 80, "1 09998U 74033F  "
	    " 05148.79417928 -.00000112  00000-0  00000+0 0  4480", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)620)) * 80, "2 09998   9.4958 "
	    "313.1750 0270971 327.5225  30.8097  1.16186785 45878", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)622)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)623)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)624)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)626)] = 25532.9894650758;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)627)] = -27244.263271425338;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)628)] = -1.1157242099297304;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)629)] = 2.4102838843371428;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)630)] = 2.1941756819375415;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)631)] = .54588852559427548;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)633)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)638)) * 80, "1 11801U         "
	    " 80230.29629788  .01431103  00000-0  14311-1      13", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)640)) * 80, "2 11801  46.7916 "
	    "230.4354 7318036  47.4722  10.4117  2.28537848    13", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)642)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)643)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)644)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)646)] = 7473.3710226929679;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)647)] = 428.94748300162695;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)648)] = 5828.748466090221;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)649)] = 5.1071553893435429;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)650)] = 6.4446803027110429;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)651)] = -.18613329728878689;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)653)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)658)) * 80, "1 14128U 83058A  "
	    " 06176.02844893 -.00000158  00000-0  10000-3 0  9627", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)660)) * 80, "2 14128  11.4384 "
	    " 35.2134 0011562  26.4582 333.5652  0.98870114 46093", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)662)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)663)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)664)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)666)] = 34747.579316629301;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)667)] = 24502.371133501172;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)668)] = -1.3283298582193905;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)669)] = -1.73164266139195;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)670)] = 2.4527726147067073;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)671)] = .60851008051124345;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)673)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)678)) * 80, "1 16925U 86065D  "
	    " 06151.67415771  .02550794 -30915-6  18784-3 0  4486", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)680)) * 80, "2 16925  62.0906 "
	    "295.0239 5596327 245.1593  47.9690  4.88511875148616", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)682)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)683)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)684)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)686)] = 5559.1168667023858;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)687)] = -11941.04090426173;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)688)] = -19.412352061843578;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)689)] = 3.392116760624535;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)690)] = -1.9469851236560267;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)691)] = 4.2507558511834462;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)693)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)698)) * 80, "1 20413U 83020D  "
	    " 05363.79166667  .00000000  00000-0  00000+0 0  7041", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)700)) * 80, "2 20413  12.3514 "
	    "187.4253 7864447 196.3027 356.5478  0.24690082  7978", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)702)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)703)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)704)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)706)] = 25123.292899943484;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)707)] = -13225.499658932509;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)708)] = 3249.4035177273227;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)709)] = .48868341891562678;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)710)] = 4.7978975920330251;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)711)] = -.96111969247696638;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)713)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)718)) * 80, "1 21897U 92011A  "
	    " 06176.02341244 -.00001273  00000-0 -13525-3 0  3044", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)720)) * 80, "2 21897  62.1749 "
	    "198.0096 7421690 253.0462  20.1561  2.01269994104880", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)722)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)723)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)724)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)726)] = -14464.721347520543;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)727)] = -4699.1951744705957;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)728)] = .066816848095162931;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)729)] = -3.2493120125354;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)730)] = -3.281032705977549;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)731)] = 4.0070469384187533;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)733)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)738)) * 80, "1 22312U 93002D  "
	    " 06094.46235912  .99999999  81888-5  49949-3 0  3953", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)740)) * 80, "2 22312  62.1486 "
	    " 77.4698 0308723 267.9229  88.7392 15.95744531 98783", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)742)] = TRUE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)743)) * 24, "SPICE(BADMECCENTRICITY)", 
	    (ftnlen)24, (ftnlen)23);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)744)] = 29400.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)746)] = 0.;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)747)] = 0.;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)748)] = 0.;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)749)] = 0.;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)750)] = 0.;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)751)] = 0.;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)753)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)758)) * 80, "1 22674U 93035D  "
	    " 06176.55909107  .00002121  00000-0  29868-3 0  6569", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)760)) * 80, "2 22674  63.5035 "
	    "354.4452 7541712 253.3264  18.7754  1.96679808 93877", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)762)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)763)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)764)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)766)] = 14712.220228426777;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)767)] = -1443.8106180803009;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)768)] = .83497887031521567;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)769)] = 4.4189654690537532;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)770)] = 1.6295920970275477;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)771)] = 4.1155318005100501;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)773)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)778)) * 80, "1 23177U 94040C  "
	    " 06175.45752052  .00000386  00000-0  76590-3 0    95", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)780)) * 80, "2 23177   7.0496 "
	    "179.8238 7258491 296.0482   8.3061  2.25906668 97438", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)782)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)783)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)784)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)786)] = -8801.6004644471086;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)787)] = -.033575572814206908;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)788)] = -.44522742636108958;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)789)] = -3.8352790996615878;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)790)] = -7.662552173174574;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)791)] = .94456132286685801;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)793)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)798)) * 80, "1 23333U 94071A  "
	    " 94305.49999999 -.00172956  26967-3  10000-3 0    15", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)800)) * 80, "2 23333  28.7490 "
	    "  2.3720 9728298  30.4360   1.3500  0.07309491    70", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)802)] = TRUE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)803)) * 24, "SPICE(ORBITDECAY)", (
	    ftnlen)24, (ftnlen)17);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)804)] = -1200.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)806)] = 0.;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)807)] = 0.;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)808)] = 0.;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)809)] = 0.;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)810)] = 0.;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)811)] = 0.;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)813)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)818)) * 80, "1 23599U 95029B  "
	    " 06171.76535463  .00085586  12891-6  12956-2 0  2905", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)820)) * 80, "2 23599   6.9327 "
	    "  0.2849 5782022 274.4436  25.2425  4.47796565123555", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)822)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)823)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)824)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)826)] = 9892.6379404641721;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)827)] = 35.761449679428672;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)828)] = -1.0822883762430016;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)829)] = 3.5566432356568418;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)830)] = 6.4560093731824457;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)831)] = .783610889617117;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)833)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)838)) * 80, "1 24208U 96044A  "
	    " 06177.04061740 -.00000094  00000-0  10000-3 0  1600", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)840)) * 80, "2 24208   3.8536 "
	    " 80.0121 0026640 311.0977  48.3000  1.00778054 36119", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)842)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)843)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)844)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)846)] = 7534.1098696537065;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)847)] = 41266.392656156131;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)848)] = -.10801028478177664;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)849)] = -3.0271680074579521;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)850)] = .55884899599332005;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)851)] = .20798275541008424;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)853)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)858)) * 80, "1 25954U 99060A  "
	    " 04039.68057285 -.00000108  00000-0  00000-0 0  6847", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)860)) * 80, "2 25954   0.0004 "
	    "243.8136 0001765  15.5294  22.7134  1.00271289 15615", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)862)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)863)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)864)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)866)] = 8827.1566020954251;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)867)] = -41223.009700114068;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)868)] = 3.6348296275007628;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)869)] = 3.0070873176244128;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)870)] = .64370132294004578;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)871)] = 9.4166299972929057e-4;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)873)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)878)) * 80, "1 26900U 01039A  "
	    " 06106.74503247  .00000045  00000-0  10000-3 0  8290", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)880)) * 80, "2 26900   0.0164 "
	    "266.5378 0003319  86.1794 182.2590  1.00273847 16981", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)882)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)883)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)884)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)886)] = -42014.837945374689;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)887)] = 3702.3435766153675;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)888)] = -26.675002565785363;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)889)] = -.26977524684110693;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)890)] = -3.0618543924537232;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)891)] = 3.3672573774080308e-4;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)893)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)898)) * 80, "1 26975U 78066F  "
	    " 06174.85818871  .00000620  00000-0  10000-3 0  6809", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)900)) * 80, "2 26975  68.4714 "
	    "236.1303 5602877 123.7484 302.5767  2.05657553 67521", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)902)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)903)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)904)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)906)] = -14506.923133367129;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)907)] = -21613.56042638596;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)908)] = 10.050188926590833;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)909)] = 2.2129433074605691;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)910)] = 1.1599708913588078;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)911)] = 3.0206002010544735;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)913)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)918)) * 80, "1 28057U 03049A  "
	    " 06177.78615833  .00000060  00000-0  35940-4 0  1836", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)920)) * 80, "2 28057  98.4283 "
	    "247.6961 0000884  88.1964 271.9322 14.35478080140550", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)922)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)923)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)924)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)926)] = -2715.2823740490348;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)927)] = -6619.2643669242079;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)928)] = -.013414434536107979;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)929)] = -1.0085872729754288;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)930)] = .42278200265497323;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)931)] = 7.3852729394058771;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)933)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)938)) * 80, "1 28129U 03058A  "
	    " 06175.57071136 -.00000104  00000-0  10000-3 0   459", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)940)) * 80, "2 28129  54.7298 "
	    "324.8098 0048506 266.2640  93.1663  2.00562768 18443", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)942)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)943)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)944)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)946)] = 21707.464117055755;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)947)] = -15318.617519348343;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)948)] = .13551151963463845;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)949)] = 1.3040292138648395;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)950)] = 1.816904973704573;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)951)] = 3.1619199752770473;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)953)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)958)) * 80, "1 28350U 04020A  "
	    " 06167.21788666  .16154492  76267-5  18678-3 0  8894", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)960)) * 80, "2 28350  64.9977 "
	    "345.6130 0024870 260.7578  99.9590 16.47856722116490", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)962)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)963)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)964)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)966)] = 6333.0812293998079;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)967)] = -1580.8285227911904;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)968)] = 90.693557173272907;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)969)] = .71463442323171589;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)970)] = 3.2242465486030065;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)971)] = 7.0831281301836881;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)973)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)978)) * 80, "1 28623U 05006B  "
	    " 06177.81079184  .00637644  69054-6  96390-3 0  6000", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)980)) * 80, "2 28623  28.5200 "
	    "114.9834 6249053 170.2550 212.8965  3.79477162 12753", (ftnlen)80,
	     (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)982)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)983)) * 24, " ", (ftnlen)24, (ftnlen)1)
	    ;
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)984)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)986)] = -11665.709019766888;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)987)] = 24943.614326156436;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)988)] = 25.805436322352136;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)989)] = -1.5962286209749481;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)990)] = -1.4761279607723508;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)991)] = 1.1260597533138914;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)993)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)998)) * 80, "1 28626U 05008A  "
	    " 06176.46683397 -.00000205  00000-0  10000-3 0  2190", (ftnlen)80,
	     (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1000)) * 80, "2 28626   0.0019"
	    " 286.9433 0000335  13.7918  55.6504  1.00270176  4891", (ftnlen)
	    80, (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)1002)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)1003)) * 24, " ", (ftnlen)24, (ftnlen)
	    1);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)1004)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1006)] = 42080.71850961155;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1007)] = -2646.8638735693594;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1008)] = .81851293889159726;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1009)] = .19310517730917121;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1010)] = 3.0686882496601733;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1011)] = 4.3844943135621002e-4;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)1013)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1018)) * 80, "1 28872U 05037B "
	    "  05333.02012661  .25992681  00000-0  24476-3 0  1534", (ftnlen)
	    80, (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1020)) * 80, "2 28872  96.4736"
	    " 157.9986 0303955 244.0492 110.6523 16.46015938 10708", (ftnlen)
	    80, (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)1022)] = TRUE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)1023)) * 24, "SPICE(SUBORBITAL)", (
	    ftnlen)24, (ftnlen)17);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)1024)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1026)] = 0.;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1027)] = 0.;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1028)] = 0.;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1029)] = 0.;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1030)] = 0.;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1031)] = 0.;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)1033)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1038)) * 80, "1 29141U 85108AA"
	    "  06170.26783845  .99999999  00000-0  13519-0 0   718", (ftnlen)
	    80, (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1040)) * 80, "2 29141  82.4288"
	    " 273.4882 0015848 277.2124  83.9133 15.93343074  6828", (ftnlen)
	    80, (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)1042)] = TRUE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)1043)) * 24, "SPICE(BADMSEMIMAJOR)", (
	    ftnlen)24, (ftnlen)20);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)1044)] = -8.4e4;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1046)] = 0.;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1047)] = 0.;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1048)] = 0.;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1049)] = 0.;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1050)] = 0.;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1051)] = 0.;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)1053)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1058)) * 80, "1 29238U 06022G "
	    "  06177.28732010  .00766286  10823-4  13334-2 0   101", (ftnlen)
	    80, (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1060)) * 80, "2 29238  51.5595"
	    " 213.7903 0202579  95.2503 267.9010 15.73823839  1061", (ftnlen)
	    80, (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)1062)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)1063)) * 24, " ", (ftnlen)24, (ftnlen)
	    1);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)1064)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1066)] = -5566.5951265376598;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1067)] = -3789.7599104569354;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1068)] = 67.60382242983637;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1069)] = 2.8737593660919392;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1070)] = -3.8253405215249763;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1071)] = 6.023253923747367;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)1073)] = 1e-11;
    ++n;
    s_copy(ele + ((i__1 = (n << 1) - 2) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1078)) * 80, "1 88888U        "
	    "  80275.98708465  .00073094  13844-3  66816-4 0    87", (ftnlen)
	    80, (ftnlen)69);
    s_copy(ele + ((i__1 = (n << 1) - 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
	    "ele", i__1, "f_spk10__", (ftnlen)1080)) * 80, "2 88888  72.8435"
	    " 115.9689 0086731  52.6988 110.5714 16.05824518  1058", (ftnlen)
	    80, (ftnlen)69);
    err[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("err", i__1, "f_spk"
	    "10__", (ftnlen)1082)] = FALSE_;
    s_copy(msg + ((i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("msg", 
	    i__1, "f_spk10__", (ftnlen)1083)) * 24, " ", (ftnlen)24, (ftnlen)
	    1);
    offset[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("offset", i__1, 
	    "f_spk10__", (ftnlen)1084)] = 0.;
    expsta[(i__1 = n * 6 - 6) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1086)] = 2328.9697519299193;
    expsta[(i__1 = n * 6 - 5) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1087)] = -5995.2205115974202;
    expsta[(i__1 = n * 6 - 4) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1088)] = 1719.9729714014379;
    expsta[(i__1 = n * 6 - 3) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1089)] = 2.9120732803862603;
    expsta[(i__1 = n * 6 - 2) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1090)] = -.98341795550286237;
    expsta[(i__1 = n * 6 - 1) < 174 && 0 <= i__1 ? i__1 : s_rnge("expsta", 
	    i__1, "f_spk10__", (ftnlen)1091)] = -7.0908162079545889;
    tol[(i__1 = n - 1) < 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_spk"
	    "10__", (ftnlen)1093)] = 1e-11;

/*        Check the number of mixe element sets equals the expected */
/*        number of mix element sets. */

    chcksi_("NMIX", &c__29, "=", &n, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*        Open a new SPK to contain the ELE array. */

    spkopn_("spk10mix.bsp", "TEST_FILE", &c__1000, &handle, (ftnlen)12, (
	    ftnlen)9);

/*        Create an SPK using the TLE set included in the 2006 Vallado */
/*        paper. This TLE set includes low and high orbit vehicles. */

    for (i__ = 0; i__ <= 28; ++i__) {
	getelm_(&c__1950, ele + ((i__1 = i__ << 1) < 58 && 0 <= i__1 ? i__1 : 
		s_rnge("ele", i__1, "f_spk10__", (ftnlen)1112)) * 80, &epoch[(
		i__2 = i__) < 29 && 0 <= i__2 ? i__2 : s_rnge("epoch", i__2, 
		"f_spk10__", (ftnlen)1112)], &elems[(i__3 = i__ * 10) < 290 &&
		 0 <= i__3 ? i__3 : s_rnge("elems", i__3, "f_spk10__", (
		ftnlen)1112)], (ftnlen)80);
	first = epoch[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge("epoch", 
		i__1, "f_spk10__", (ftnlen)1114)] - spd_();
	last = epoch[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge("epoch", 
		i__1, "f_spk10__", (ftnlen)1115)] + spd_();
	i__3 = -i__ - 10000;
	spkw10_(&handle, &i__3, &c__399, "J2000", &first, &last, "MIX", 
		consts, &c__1, &elems[(i__1 = i__ * 10) < 290 && 0 <= i__1 ? 
		i__1 : s_rnge("elems", i__1, "f_spk10__", (ftnlen)1117)], &
		epoch[(i__2 = i__) < 29 && 0 <= i__2 ? i__2 : s_rnge("epoch", 
		i__2, "f_spk10__", (ftnlen)1117)], (ftnlen)5, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk10mix.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Continue tests if successful creation of mix kernel. */

    if (*ok) {

/*           Confirm evaluation at epoch of each TLE set. If */
/*           the evaluation fails, confirm expected error. */

	for (i__ = 0; i__ <= 28; ++i__) {
	    tcase_(ele + ((i__1 = i__ << 1) < 58 && 0 <= i__1 ? i__1 : s_rnge(
		    "ele", i__1, "f_spk10__", (ftnlen)1144)) * 80, (ftnlen)80)
		    ;
	    i__3 = -i__ - 10000;
	    d__1 = epoch[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge("epo"
		    "ch", i__1, "f_spk10__", (ftnlen)1146)] + offset[(i__2 = 
		    i__) < 29 && 0 <= i__2 ? i__2 : s_rnge("offset", i__2, 
		    "f_spk10__", (ftnlen)1146)];
	    spkez_(&i__3, &d__1, "J2000", "NONE", &c__399, state, &lt, (
		    ftnlen)5, (ftnlen)4);
	    chckxc_(&err[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge("err",
		     i__1, "f_spk10__", (ftnlen)1149)], msg + ((i__2 = i__) < 
		    29 && 0 <= i__2 ? i__2 : s_rnge("msg", i__2, "f_spk10__", 
		    (ftnlen)1149)) * 24, ok, (ftnlen)24);

/*              If no error and expected result, compare the STATE */
/*              vector against an expected state. */

	    if (*ok && ! err[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge(
		    "err", i__1, "f_spk10__", (ftnlen)1155)]) {

/*                 Map EXPSTA from TEME to J2000 frame. */

		d__1 = epoch[(i__1 = i__) < 29 && 0 <= i__1 ? i__1 : s_rnge(
			"epoch", i__1, "f_spk10__", (ftnlen)1160)] + offset[(
			i__2 = i__) < 29 && 0 <= i__2 ? i__2 : s_rnge("offset"
			, i__2, "f_spk10__", (ftnlen)1160)];
		zzteme_(&d__1, precm);
		invstm_(precm, invprc);
		mxvg_(invprc, &expsta[(i__1 = (i__ + 1) * 6 - 6) < 174 && 0 <=
			 i__1 ? i__1 : s_rnge("expsta", i__1, "f_spk10__", (
			ftnlen)1163)], &c__6, &c__6, expst);

/*                 Confirm SPK evaluation equals to the case-specific */
/*                 tolerance the output from the Vallado based routine. */

		chckad_("EXPST", expst, "~/", state, &c__6, &tol[(i__1 = i__) 
			< 29 && 0 <= i__1 ? i__1 : s_rnge("tol", i__1, "f_sp"
			"k10__", (ftnlen)1169)], ok, (ftnlen)5, (ftnlen)2);
	    }
	}
    }

/*     Unload all files, then delete. */

    kclear_();
    kilfil_("type10.bsp", (ftnlen)10);
    kilfil_("type10_2.bsp", (ftnlen)12);
    kilfil_("type10_3.bsp", (ftnlen)12);
    kilfil_("spk10mix.bsp", (ftnlen)12);
    t_success__(ok);
    return 0;
} /* f_spk10__ */

