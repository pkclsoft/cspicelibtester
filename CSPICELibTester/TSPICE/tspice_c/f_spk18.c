/* f_spk18.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__4 = 4;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__9 = 9;
static logical c_true = TRUE_;
static integer c__25 = 25;
static integer c__0 = 0;
static integer c__6 = 6;
static doublereal c_b106 = 1e-14;
static doublereal c_b122 = 10.;
static integer c_b167 = -10000;
static integer c__5 = 5;
static integer c__10101 = 10101;
static integer c__2 = 2;
static doublereal c_b319 = 0.;

/* $Procedure F_SPK18 ( SPK data type 18 tests ) */
/* Subroutine */ int f_spk18__(logical *ok)
{
    /* Initialized data */

    static doublereal dscepc[9] = { 100.,200.,300.,400.,500.,600.,700.,800.,
	    900. };
    static doublereal dscsts[54]	/* was [6][9] */ = { 101.,201.,301.,
	    401.,501.,601.,102.,202.,302.,402.,502.,602.,103.,203.,303.,403.,
	    503.,603.,104.,204.,304.,404.,504.,604.,105.,205.,305.,405.,505.,
	    605.,106.,206.,306.,406.,506.,606.,107.,207.,307.,407.,507.,607.,
	    108.,208.,308.,408.,508.,608.,109.,209.,309.,409.,509.,609. };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    static integer newh;
    static char xref[32];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static integer i__, j, eaddr, n;
    extern /* Subroutine */ int dafgn_(char *, ftnlen), dafgs_(doublereal *);
    static char segid[60];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5];
    extern /* Subroutine */ int dafus_(doublereal *, integer *, integer *, 
	    doublereal *, integer *);
    static doublereal dpval;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int vsclg_(doublereal *, doublereal *, integer *, 
	    doublereal *);
    static doublereal state[6];
    static integer xbody;
    extern /* Subroutine */ int topen_(char *, ftnlen), spkw18_(integer *, 
	    integer *, integer *, integer *, char *, doublereal *, doublereal 
	    *, char *, integer *, integer *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), t_success__(logical *);
    static char segid2[60];
    extern /* Subroutine */ int dafgda_(integer *, integer *, integer *, 
	    doublereal *);
    static doublereal dc[2], bt0lst[121212]	/* was [12][10101] */, bt1lst[
	    60606]	/* was [6][10101] */;
    static integer ic[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     daffna_(logical *), dafbfs_(integer *);
    static integer degree, handle;
    extern /* Subroutine */ int dafcls_(integer *), delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen);
    static doublereal dscpak[108]	/* was [12][9] */;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), dafopr_(char *, integer *, ftnlen), spklef_(char *, 
	    integer *, ftnlen);
    static doublereal beplst[10101];
    extern /* Subroutine */ int spkgeo_(integer *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen), spkuef_(integer *)
	    , spkcls_(integer *);
    static integer xcentr;
    extern /* Subroutine */ int spksub_(integer *, doublereal *, char *, 
	    doublereal *, doublereal *, integer *, ftnlen);
    static doublereal xstate[6];
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    extern logical exists_(char *, ftnlen);

/* $ Abstract */

/*     Exercise routines associated with SPK data type 18. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 18. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-AUG-2002 (NJB) */

/* -& */

/*     SPK type 18 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 12-element packets, order */
/*                 reduction at boundaries to preceding number */
/*                 equivalent to 3 mod 4. */


/*     Subtype 1:  Lagrange interpolation, 6-element packets, order */
/*                 reduction at boundaries to preceding odd number. */


/*     Packet sizes associated with the various subtypes: */


/*     End of include file spk18.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests routines that write and read type 18 SPK */
/*     data. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 21-DEC-2012 (NJB) (EDW) */

/*        Bug fix: now tests for correct number of states in output */
/*        segment for small segments. A new SPK file is created for */
/*        use as an input file by SPKS18; this file contains small */
/*        segments suitable for testing pad creation logic for small */
/*        segments. */

/*        An SPKW18 error test was added for the case of the packet */
/*        count being 1. */

/*        Unused variable and parameter declarations were deleted. */

/*        References to SPKS03 in test case titles have been corrected. */

/*        All variables to SAVE for f2c'd version compiled with Borland. */
/*        Implemented a proper SPICE header. */

/* -    TSPICE Version 1.0.0, 07-OCT-2005 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Epochs and states: */


/*     Open the test family. */

    topen_("F_SPK18", (ftnlen)7);

/*     Test SPKW18:  start out with error handling. */


/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: bad frame name.", (ftnlen)34);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 18 test segment", (ftnlen)60, (ftnlen)24);

/*     Open a new SPK file. */

    if (exists_("test18err.bsp", (ftnlen)13)) {
	delfil_("test18err.bsp", (ftnlen)13);
    }
    spkopn_("test18err.bsp", "Type 18 SPK internal file name", &c__4, &handle,
	     (ftnlen)13, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkw18_(&handle, &c__1, &xbody, &xcentr, "SPUD", dscepc, &dscepc[8], 
	    segid, &c__3, &c__9, dscsts, dscepc, (ftnlen)4, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: SEGID too long.", (ftnlen)34);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], "X   "
	    "                                            X", &c__3, &c__9, 
	    dscsts, dscepc, (ftnlen)32, (ftnlen)49);
    chckxc_(&c_true, "SPICE(SEGIDTOOLONG)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: unprintable SEGID characters.", (ftnlen)48);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], "\a", &
	    c__3, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: polynomial degree too high.", (ftnlen)46);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &c__25, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: polynomial degree too low.", (ftnlen)45);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &c__0, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case:  odd window size.", (ftnlen)36);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &c__4, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: too few states", (ftnlen)33);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, dscepc, segid, &
	    c__3, &c__0, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TOOFEWSTATES)", ok, (ftnlen)19);
    tcase_("SPKW18 error case: too few states", (ftnlen)33);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, dscepc, segid, &
	    c__3, &c__1, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TOOFEWSTATES)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: descriptor times swapped.", (ftnlen)44);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, &dscepc[8], dscepc, segid, 
	    &c__3, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: descriptor start time is too early.", (ftnlen)
	    54);
    d__1 = dscepc[0] - 1.;
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, &d__1, &dscepc[8], segid, &
	    c__3, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 error case: descriptor end time is too late.", (ftnlen)51);
    d__1 = dscepc[8] + 1.;
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &d__1, segid, &
	    c__3, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/*     Close the SPK file at the DAF level; SPKCLS won't close */
/*     a file without segments. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Enough with the error cases; write a segment already. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create a type 18 subtype 1 segment.", (ftnlen)35);
    spkopn_("sp18t1.bsp", "Type 18 SPK internal file name", &c__4, &handle, (
	    ftnlen)10, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &c__3, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the SPK file. */

    spklef_("sp18t1.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up states for each epoch in our list.  Compare. */

    for (i__ = 1; i__ <= 9; ++i__) {
	spkgeo_(&xbody, &dscepc[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("dscepc", i__1, "f_spk18__", (ftnlen)574)], xref, &
		xcentr, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("type 18 state", state, "~", &dscsts[(i__1 = i__ * 6 - 6) < 
		54 && 0 <= i__1 ? i__1 : s_rnge("dscsts", i__1, "f_spk18__", (
		ftnlen)577)], &c__6, &c_b106, ok, (ftnlen)13, (ftnlen)1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Repeat this test using subtype 0. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create a type 18 subtype 0 segment.", (ftnlen)35);
    spkopn_("sp18t0.bsp", "Type 18 SPK internal file name", &c__4, &handle, (
	    ftnlen)10, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fill in the packets using the discrete state set.  Make the */
/*     velocity/accleration data 10x the position/velocity data.  Note */
/*     that these two data sets are independent; realistic correspondence */
/*     between them is not required. */

    for (i__ = 1; i__ <= 9; ++i__) {
	moved_(&dscsts[(i__1 = i__ * 6 - 6) < 54 && 0 <= i__1 ? i__1 : s_rnge(
		"dscsts", i__1, "f_spk18__", (ftnlen)617)], &c__6, &dscpak[(
		i__2 = i__ * 12 - 12) < 108 && 0 <= i__2 ? i__2 : s_rnge(
		"dscpak", i__2, "f_spk18__", (ftnlen)617)]);
	vsclg_(&c_b122, &dscsts[(i__1 = i__ * 6 - 3) < 54 && 0 <= i__1 ? i__1 
		: s_rnge("dscsts", i__1, "f_spk18__", (ftnlen)619)], &c__6, &
		dscpak[(i__2 = i__ * 12 - 6) < 108 && 0 <= i__2 ? i__2 : 
		s_rnge("dscpak", i__2, "f_spk18__", (ftnlen)619)]);
    }
    spkw18_(&handle, &c__0, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &c__3, &c__9, dscpak, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the SPK file. */

    spklef_("sp18t0.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up states for each epoch in our list.  Compare. */

    for (i__ = 1; i__ <= 9; ++i__) {
	spkgeo_(&xbody, &dscepc[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("dscepc", i__1, "f_spk18__", (ftnlen)655)], xref, &
		xcentr, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(&dscsts[(i__1 = i__ * 6 - 6) < 54 && 0 <= i__1 ? i__1 : s_rnge(
		"dscsts", i__1, "f_spk18__", (ftnlen)659)], &c__3, xstate);
	vscl_(&c_b122, &dscsts[(i__1 = i__ * 6 - 3) < 54 && 0 <= i__1 ? i__1 :
		 s_rnge("dscsts", i__1, "f_spk18__", (ftnlen)660)], &xstate[3]
		);
	chckad_("type 18 state", state, "~", xstate, &c__6, &c_b106, ok, (
		ftnlen)13, (ftnlen)1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 test:  create a large subtype 1 segment with multiple dir"
	    "ectories.", (ftnlen)73);

/*     Create the state and epoch values we'll use. */

    for (i__ = 1; i__ <= 10101; ++i__) {
	for (j = 1; j <= 6; ++j) {
	    bt1lst[(i__1 = j + i__ * 6 - 7) < 60606 && 0 <= i__1 ? i__1 : 
		    s_rnge("bt1lst", i__1, "f_spk18__", (ftnlen)695)] = i__ * 
		    10. + j;
	}
	beplst[(i__1 = i__ - 1) < 10101 && 0 <= i__1 ? i__1 : s_rnge("beplst",
		 i__1, "f_spk18__", (ftnlen)699)] = i__ * 10.;
    }

/*     Open a new type 18 SPK file. */

    if (exists_("sp18big1.bsp", (ftnlen)12)) {
	delfil_("sp18big1.bsp", (ftnlen)12);
    }
    spkopn_("sp18big1.bsp", "Type 18 SPK internal file name", &c__0, &handle, 
	    (ftnlen)12, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkw18_(&handle, &c__1, &c_b167, &c__5, xref, beplst, &beplst[10100], 
	    segid, &c__3, &c__10101, bt1lst, beplst, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the SPK file. */

    spklef_("sp18big1.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up states for each midpoint of adjacent epochs in our list. */
/*     Compare. */

    for (i__ = 1; i__ <= 10100; ++i__) {
	d__1 = beplst[(i__1 = i__ - 1) < 10101 && 0 <= i__1 ? i__1 : s_rnge(
		"beplst", i__1, "f_spk18__", (ftnlen)748)] + 5.;
	spkgeo_(&c_b167, &d__1, xref, &c__5, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Set up the expected state vector. */

	moved_(&bt1lst[(i__1 = i__ * 6 - 6) < 60606 && 0 <= i__1 ? i__1 : 
		s_rnge("bt1lst", i__1, "f_spk18__", (ftnlen)761)], &c__6, 
		xstate);
	for (j = 1; j <= 6; ++j) {
	    xstate[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("xstate", 
		    i__1, "f_spk18__", (ftnlen)765)] = xstate[(i__2 = j - 1) <
		     6 && 0 <= i__2 ? i__2 : s_rnge("xstate", i__2, "f_spk18"
		    "__", (ftnlen)765)] + 5.;
	}
	chckad_("type 18 position", state, "~", xstate, &c__3, &c_b106, ok, (
		ftnlen)16, (ftnlen)1);
	chckad_("type 18 velocity", &state[3], "~", &xstate[3], &c__3, &
		c_b106, ok, (ftnlen)16, (ftnlen)1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW18 test:  create a large subtype 0 segment with multiple dir"
	    "ectories.", (ftnlen)73);

/*     Create the state and epoch values we'll use. We're going to set */
/*     all velocities to zero to create a rounded stair-step sort of */
/*     pattern in the position components. This will ensure that the */
/*     correct states cannot be obtained without selecting the correct */
/*     window of states in the reader. */

/*     For velocity and acceleration, we'll use the same idea, but we'll */
/*     scale the values to distinguish them. */

    for (i__ = 1; i__ <= 10101; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    bt0lst[(i__1 = j + i__ * 12 - 13) < 121212 && 0 <= i__1 ? i__1 : 
		    s_rnge("bt0lst", i__1, "f_spk18__", (ftnlen)814)] = i__ * 
		    10. + j;
	    bt0lst[(i__1 = j + 3 + i__ * 12 - 13) < 121212 && 0 <= i__1 ? 
		    i__1 : s_rnge("bt0lst", i__1, "f_spk18__", (ftnlen)815)] =
		     0.;
	    bt0lst[(i__1 = j + 6 + i__ * 12 - 13) < 121212 && 0 <= i__1 ? 
		    i__1 : s_rnge("bt0lst", i__1, "f_spk18__", (ftnlen)816)] =
		     bt0lst[(i__2 = j + i__ * 12 - 13) < 121212 && 0 <= i__2 ?
		     i__2 : s_rnge("bt0lst", i__2, "f_spk18__", (ftnlen)816)] 
		    * 1e6;
	    bt0lst[(i__1 = j + 9 + i__ * 12 - 13) < 121212 && 0 <= i__1 ? 
		    i__1 : s_rnge("bt0lst", i__1, "f_spk18__", (ftnlen)817)] =
		     0.;
	}
	beplst[(i__1 = i__ - 1) < 10101 && 0 <= i__1 ? i__1 : s_rnge("beplst",
		 i__1, "f_spk18__", (ftnlen)821)] = i__ * 10.;
    }

/*     Open a new type 18 SPK file. */

    if (exists_("sp18big0.bsp", (ftnlen)12)) {
	delfil_("sp18big0.bsp", (ftnlen)12);
    }
    spkopn_("sp18big0.bsp", "Type 18 SPK internal file name", &c__0, &handle, 
	    (ftnlen)12, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkw18_(&handle, &c__0, &c_b167, &c__5, xref, beplst, &beplst[10100], 
	    segid, &c__3, &c__10101, bt0lst, beplst, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the SPK file. */

    spklef_("sp18big0.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up states for each midpoint of adjacent epochs in our list. */
/*     Compare. */

    for (i__ = 1; i__ <= 10100; ++i__) {
	d__1 = beplst[(i__1 = i__ - 1) < 10101 && 0 <= i__1 ? i__1 : s_rnge(
		"beplst", i__1, "f_spk18__", (ftnlen)870)] + 5.;
	spkgeo_(&c_b167, &d__1, xref, &c__5, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Set up the expected state vector. */

	moved_(&bt0lst[(i__1 = i__ * 12 - 12) < 121212 && 0 <= i__1 ? i__1 : 
		s_rnge("bt0lst", i__1, "f_spk18__", (ftnlen)883)], &c__3, 
		xstate);
	for (j = 1; j <= 3; ++j) {
	    xstate[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("xstate", 
		    i__1, "f_spk18__", (ftnlen)887)] = xstate[(i__2 = j - 1) <
		     6 && 0 <= i__2 ? i__2 : s_rnge("xstate", i__2, "f_spk18"
		    "__", (ftnlen)887)] + 5.;
	    xstate[(i__1 = j + 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("xstate", 
		    i__1, "f_spk18__", (ftnlen)888)] = xstate[(i__2 = j - 1) <
		     6 && 0 <= i__2 ? i__2 : s_rnge("xstate", i__2, "f_spk18"
		    "__", (ftnlen)888)] * 1e6;
	}
	chckad_("type 18 position", state, "~", xstate, &c__3, &c_b106, ok, (
		ftnlen)16, (ftnlen)1);
	chckad_("type 18 velocity", &state[3], "~", &xstate[3], &c__3, &
		c_b106, ok, (ftnlen)16, (ftnlen)1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Create an SPK file suitable for use as an input file */
/*     for SPKS18. This file will contain segments of subtypes */
/*     0 and 1. */

    tcase_("Create a type 18 SPK for subset testing. This file contains segm"
	    "ents of subtypes 0 and 1.", (ftnlen)89);
    spkopn_("sp18small.bsp", "Type 18 SPK internal file name", &c__4, &handle,
	     (ftnlen)13, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add first segment, which has subtype 1 and degree 15. */

    degree = 15;
    spkw18_(&handle, &c__1, &xbody, &xcentr, xref, dscepc, &dscepc[8], segid, 
	    &degree, &c__9, dscsts, dscepc, (ftnlen)32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add second segment, which has subtype 0 and degree 15. */
/*     This file contains only 5 packets. */

/*     We must use a different body for this segment. */

/*     Note the use of DSCPAK rather than DSKSTS. DSKPAK */
/*     was initialized earlier. */

    xbody = 4;
    spkw18_(&handle, &c__0, &xbody, &xcentr, xref, &dscepc[4], &dscepc[6], 
	    segid, &degree, &c__5, &dscpak[36], &dscepc[3], (ftnlen)32, (
	    ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: write new file having a subtype 1 segment created b"
	    "y subsetting small segment from SP18SM.", (ftnlen)103);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 18 test subset segment", (ftnlen)60, (ftnlen)31);

/*     Open a new SPK file. */

    if (exists_("sp18sub1.bsp", (ftnlen)12)) {
	delfil_("sp18sub1.bsp", (ftnlen)12);
    }
    spkopn_("sp18sub1.bsp", "Type 18 SPK internal file name", &c__0, &newh, (
	    ftnlen)12, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open SP18T1 and extract segment descriptor and ID of first */
/*     segment. */

    dafopr_("sp18small.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid2, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a type 18 segment in new file.  Shorten the time */
/*     coverage by knocking off the coverage contributed by */
/*     the first and last packets of the source segment. */

    spksub_(&handle, descr, segid, &dscepc[1], &dscepc[7], &newh, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the new SPK file. */

    spkcls_(&newh);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the old SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: check descriptor bounds on subsetted file (subtype "
	    "1).", (ftnlen)67);

/*     Open SP18S1 and extract segment descriptor and ID of first */
/*     segment. */

    dafopr_("sp18sub1.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the time bounds. */

    i__ = 8;
    chcksd_("Segment start", dc, "=", &dscepc[1], &c_b319, ok, (ftnlen)13, (
	    ftnlen)1);
    chcksd_("Segment end", &dc[1], "=", &dscepc[(i__1 = i__ - 1) < 9 && 0 <= 
	    i__1 ? i__1 : s_rnge("dscepc", i__1, "f_spk18__", (ftnlen)1089)], 
	    &c_b319, ok, (ftnlen)11, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: check state count in subsetted file (subtype 1).", (
	    ftnlen)61);

/*     Load the SPK file. */

    spklef_("sp18sub1.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get descriptor of first segment. */

    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    eaddr = ic[5];
    dafgda_(&handle, &eaddr, &eaddr, &dpval);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = i_dnnt(&dpval);
    chcksi_("N", &n, "=", &c__9, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: read states from subsetted file (subtype 1).", (
	    ftnlen)57);

/*     Load the SPK file. */

    spklef_("sp18sub1.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Look up states for each epoch in our list.  Compare. */

    for (i__ = 4; i__ <= 8; ++i__) {
	spkgeo_(&xbody, &dscepc[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("dscepc", i__1, "f_spk18__", (ftnlen)1160)], xref, &
		xcentr, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("type 18 position", state, "~", &dscsts[(i__1 = i__ * 6 - 6) <
		 54 && 0 <= i__1 ? i__1 : s_rnge("dscsts", i__1, "f_spk18__", 
		(ftnlen)1163)], &c__3, &c_b106, ok, (ftnlen)16, (ftnlen)1);
	chckad_("type 18 velocity", &state[3], "~", &dscsts[(i__1 = i__ * 6 - 
		3) < 54 && 0 <= i__1 ? i__1 : s_rnge("dscsts", i__1, "f_spk1"
		"8__", (ftnlen)1171)], &c__3, &c_b106, ok, (ftnlen)16, (ftnlen)
		1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Repeat subsetting tests for a subtype 0 segment. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: write new file having a segment created by subsetti"
	    "ng small segment from SP18SM.The output segment has subtype 0.", (
	    ftnlen)126);
    xbody = 4;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 18 test subset segment", (ftnlen)60, (ftnlen)31);

/*     Open a new SPK file. */

    if (exists_("sp18sub0.bsp", (ftnlen)12)) {
	delfil_("sp18sub0.bsp", (ftnlen)12);
    }
    spkopn_("sp18sub0.bsp", "Type 18 SPK internal file name", &c__0, &newh, (
	    ftnlen)12, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open SP18SM and extract segment descriptor and ID of second */
/*     segment. */

    dafopr_("sp18small.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 2; ++i__) {
	daffna_(&found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid2, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a type 18 segment in new file.  Shorten the time */
/*     coverage by knocking off the coverage contributed by */
/*     the first and last packets of the source segment. */

    spksub_(&handle, descr, segid, &dscepc[4], &dscepc[6], &newh, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the new SPK file. */

    spkcls_(&newh);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the old SPK file. */

    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: check descriptor bounds on subsetted file (subtype "
	    "0).", (ftnlen)67);

/*     Open SP18S0 and extract segment descriptor and ID of first */
/*     segment. */

    dafopr_("sp18sub0.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the time bounds. */

    chcksd_("Segment start", dc, "=", &dscepc[4], &c_b319, ok, (ftnlen)13, (
	    ftnlen)1);
    chcksd_("Segment end", &dc[1], "=", &dscepc[6], &c_b319, ok, (ftnlen)11, (
	    ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: check state count in subsetted file (subtype 0).", (
	    ftnlen)61);

/*     Load the SPK file. */

    spklef_("sp18sub0.bsp", &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get descriptor of first segment. */

    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    eaddr = ic[5];
    dafgda_(&handle, &eaddr, &eaddr, &dpval);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = i_dnnt(&dpval);
    chcksi_("N", &n, "=", &c__5, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS18: read states from subsetted file (subtype 0).", (
	    ftnlen)57);

/*     Load the SPK file. */

    spklef_("sp18small.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 4;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Look up states for each epoch in our list.  Compare. */

    for (i__ = 5; i__ <= 7; ++i__) {
	spkgeo_(&xbody, &dscepc[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("dscepc", i__1, "f_spk18__", (ftnlen)1368)], xref, &
		xcentr, state, &lt, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("type 18 position", state, "~", &dscpak[(i__1 = i__ * 12 - 12)
		 < 108 && 0 <= i__1 ? i__1 : s_rnge("dscpak", i__1, "f_spk18"
		"__", (ftnlen)1371)], &c__3, &c_b106, ok, (ftnlen)16, (ftnlen)
		1);
	chckad_("type 18 velocity", &state[3], "~", &dscpak[(i__1 = i__ * 12 
		- 6) < 108 && 0 <= i__1 ? i__1 : s_rnge("dscpak", i__1, "f_s"
		"pk18__", (ftnlen)1379)], &c__3, &c_b106, ok, (ftnlen)16, (
		ftnlen)1);
    }

/*     Unload the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete SPK files.", (ftnlen)28);
    delfil_("test18err.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18big0.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18big1.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18sub0.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18sub1.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp18small.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_spk18__ */

