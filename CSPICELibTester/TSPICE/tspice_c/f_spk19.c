/* f_spk19.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__4 = 4;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;
static integer c__6 = 6;
static doublereal c_b124 = 1e-14;
static integer c__3 = 3;
static doublereal c_b375 = 10.;
static integer c__28 = 28;

/* $Procedure F_SPK19 ( SPK data type 19 tests ) */
/* Subroutine */ int f_spk19__(logical *ok)
{
    /* Initialized data */

    static doublereal dscepc[9] = { 100.,200.,300.,400.,500.,600.,700.,800.,
	    900. };
    static doublereal dscsts[54]	/* was [6][9] */ = { 101.,201.,301.,
	    401.,501.,601.,102.,202.,302.,402.,502.,602.,103.,203.,303.,403.,
	    503.,603.,104.,204.,304.,404.,504.,604.,105.,205.,305.,405.,505.,
	    605.,106.,206.,306.,406.,506.,606.,107.,207.,307.,407.,507.,607.,
	    108.,208.,308.,408.,508.,608.,109.,209.,309.,409.,509.,609. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double pow_di(doublereal *, integer *), exp(doublereal);

    /* Local variables */
    static char name__[80];
    static doublereal last;
    static char xref[32];
    static doublereal work[28];
    static integer b, e, i__, j, k;
    static char segid[60];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal midet;
    static integer epcto;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), swapd_(doublereal *, doublereal *);
    static doublereal state[6];
    static char title[800];
    static integer xbody;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    static doublereal xvals[28], yvals[28];
    static integer npkts[21000];
    extern /* Subroutine */ int spkw19_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    static integer pktto;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    static integer lb, ub;
    static doublereal et;
    static integer handle;
    static doublereal escale;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal endepc;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int dafcls_(integer *), chckxc_(logical *, char *,
	     logical *, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static integer degres[21000];
    static doublereal epochs[2100000], midsta[6], ivlbds[21001];
    extern /* Subroutine */ int unload_(char *, ftnlen);
    static doublereal packts[25200000];
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    extern doublereal lgrint_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static integer rectot;
    extern /* Subroutine */ int setmsg_(char *, ftnlen);
    static integer xcentr;
    static logical sellst;
    extern /* Subroutine */ int errint_(char *, integer *, ftnlen);
    static doublereal xstate[6];
    extern /* Subroutine */ int spkcls_(integer *);
    static integer nintvl;
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), furnsh_(char *, ftnlen);
    extern logical exists_(char *, ftnlen);
    static integer subtps[21000], winsiz, pktsiz, pkttot;
    extern logical odd_(integer *);

/* $ Abstract */

/*     Exercise routines associated with SPK data type 19. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 19. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 11-MAY-2015 (NJB) */

/*        Updated to support subtype 2. */

/* -    SPICELIB Version 1.0.0, 07-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this SPK type. */

/*     The degree is compatible with the maximum degrees */
/*     supported by types 13 and 21. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     SPK type 19 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 12-element packets. */


/*     Subtype 1:  Lagrange interpolation, 6-element packets. */


/*     Subtype 2:  Hermite interpolation, 6-element packets. */


/*     Packet sizes associated with the various subtypes: */


/*     Number of subtypes: */


/*     Maximum packet size for type 19: */


/*     Minimum packet size for type 19: */


/*     The SPKPVN record size declared in spkrec.inc must be at least as */
/*     large as the maximum possible size of an SPK type 19 record. */

/*     The largest possible SPK type 19 record has subtype 1 (note that */
/*     records of subtype 0 have half as many epochs as those of subtype */
/*     1, for a given polynomial degree). A type 1 record contains */

/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size S19PS1 */
/*        - MAXDEG+1 time tags */


/*     End of include file spk19.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the following routines that write and read */
/*     type 19 SPK data: */

/*        SPKE19 */
/*        SPKR19 */
/*        SPKW19 */

/*     The SPK type 19 subsetter has its own test family. */

/*     The higher level SPK routine */

/*        SPKPVN */

/*     is also exercised by this test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 21-DEC-2015 (NJB) */

/*        Updated to support subtype 2. */

/* -    TSPICE Version 1.0.0, 24-DEC-2013 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Epochs and states: */


/*     Open the test family. */

    topen_("F_SPK19", (ftnlen)7);
/* ***************************************************************** */
/* * */
/* *    SPKW19 error cases: */
/* * */
/* ***************************************************************** */

/*     Test SPKW19:  start out with error handling. */


/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: bad frame name.", (ftnlen)34);

/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Open a new SPK file. */

    if (exists_("test19err.bsp", (ftnlen)13)) {
	delfil_("test19err.bsp", (ftnlen)13);
    }
    spkopn_("test19err.bsp", "Type 19 SPK internal file name", &c__4, &handle,
	     (ftnlen)13, (ftnlen)30);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nintvl = 1;
    npkts[0] = 2;
    subtps[0] = 1;
    degres[0] = 1;
    rectot = 0;
    pkttot = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Update total counts of records and packet elements. */
/*        The latter depends on the packet size of the current */
/*        mini-segment. */

	if (subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"subtps", i__2, "f_spk19__", (ftnlen)312)] == 0) {
	    pktsiz = 12;
	} else if (subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
		s_rnge("subtps", i__2, "f_spk19__", (ftnlen)316)] == 1) {
	    pktsiz = 6;
	} else {
	    setmsg_("Unexpected SPK type 19 subtype # found in mini-segment "
		    "#.", (ftnlen)57);
	    errint_("#", &subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 
		    : s_rnge("subtps", i__2, "f_spk19__", (ftnlen)324)], (
		    ftnlen)1);
	    errint_("#", &i__, (ftnlen)1);
	    sigerr_("SPICE(INVALIDSUBTYPE)", (ftnlen)21);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	pkttot += npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)333)] * pktsiz;
	rectot += npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)335)];
    }
    cleard_(&pkttot, packts);
    i__1 = rectot;
    for (i__ = 1; i__ <= i__1; ++i__) {
	epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epoc"
		"hs", i__2, "f_spk19__", (ftnlen)343)] = (doublereal) i__;
    }
    first = epochs[0];
    last = epochs[1];
    ivlbds[0] = first;
    ivlbds[1] = last;
    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, "SPUD", &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)4,
	     (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: SEGID too long.", (ftnlen)34);
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, "X               "
	    "                                X", &nintvl, npkts, subtps, 
	    degres, packts, epochs, ivlbds, &sellst, (ftnlen)32, (ftnlen)49);
    chckxc_(&c_true, "SPICE(SEGIDTOOLONG)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: unprintable SEGID characters.", (ftnlen)48);
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, "\a", &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: non-positive interval count", (ftnlen)46);
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &c__0, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    nintvl = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: non-increasing interval bounds", (ftnlen)49);
    ivlbds[1] = ivlbds[0];
    first = 1.5;
    last = 1.6;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);
    ivlbds[0] = 1.;
    ivlbds[1] = 2.;
    first = ivlbds[0];
    last = ivlbds[1];

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: polynomial degree too high.", (ftnlen)46);
    degres[0] = 40;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: polynomial degree too low.", (ftnlen)45);
    degres[0] = 0;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case:  odd window size.", (ftnlen)36);
    degres[0] = 2;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADWINDOWSIZE)", ok, (ftnlen)20);
    subtps[0] = 0;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADWINDOWSIZE)", ok, (ftnlen)20);
    subtps[0] = 1;
    degres[0] = 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: too few states", (ftnlen)33);
    npkts[0] = 1;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TOOFEWPACKETS)", ok, (ftnlen)20);
    npkts[0] = 2;

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: descriptor times swapped.", (ftnlen)44);
    spkw19_(&handle, &xbody, &xcentr, xref, &last, &first, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: descriptor start time is too early.", (ftnlen)
	    54);
    d__1 = first - 1.;
    spkw19_(&handle, &xbody, &xcentr, xref, &d__1, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: descriptor end time is too late.", (ftnlen)51);
    d__1 = last + 1.;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &d__1, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: epochs for first mini-segment are out of orde"
	    "r.", (ftnlen)66);
    nintvl = 1;
    npkts[0] = 4;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epoc"
		"hs", i__2, "f_spk19__", (ftnlen)709)] = (doublereal) i__;
    }
    first = epochs[0];
    last = epochs[(i__1 = npkts[0] - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spk19__", (ftnlen)713)];
    ivlbds[0] = first;
    ivlbds[1] = last;
    swapd_(&epochs[1], &epochs[2]);
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(TIMESOUTOFORDER)", ok, (ftnlen)22);
    swapd_(&epochs[1], &epochs[2]);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: epochs for first mini-segment don't cover fir"
	    "st interpolation interval.", (ftnlen)90);
    nintvl = 1;
    npkts[0] = 4;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epoc"
		"hs", i__2, "f_spk19__", (ftnlen)753)] = (doublereal) i__;
    }
    first = epochs[0];
    last = epochs[(i__1 = npkts[0] - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spk19__", (ftnlen)757)];
    ivlbds[0] = first;
    ivlbds[1] = last;
    epochs[0] += .5;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSDISAGREE)", ok, (ftnlen)21);
    epochs[0] = 1.;
    epochs[3] += -.5;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_true, "SPICE(BOUNDSDISAGREE)", ok, (ftnlen)21);
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epoc"
		"hs", i__2, "f_spk19__", (ftnlen)805)] = (doublereal) i__;
    }

/*     End of SPKW19 error cases. */


/*     Close the SPK file at the DAF level; SPKCLS won't close */
/*     a file without segments. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     SPKW19 Non-error cases: */

/* ***************************************************************** */
/* * */
/* *    Trivial case: write an SPK containing a small subtype 1 */
/* *    segment. */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Write a 1-interval subtype 1 segment.", (ftnlen)37);

/*     This file lacks both interval directories and mini-segment */
/*     epoch directories. */


/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);
    spkopn_("sp19t1.bsp", "sp19t1.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nintvl = 1;
    npkts[0] = 9;
    subtps[0] = 1;
    degres[0] = 3;
    first = dscepc[2];
    last = dscepc[(i__1 = npkts[0] - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spk19__", (ftnlen)866)];
    ivlbds[0] = dscepc[1];
    ivlbds[1] = dscepc[(i__1 = npkts[0] - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spk19__", (ftnlen)869)];
    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, dscsts, dscepc, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs match input epoch"
	    "s.", (ftnlen)66);
    furnsh_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);
    i__1 = npkts[0] - 2;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = dscepc[(i__2 = i__ - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("dscepc"
		, i__2, "f_spk19__", (ftnlen)911)];
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(name__, state, "~~", &dscsts[(i__2 = i__ * 6 - 6) < 54 && 
		    0 <= i__2 ? i__2 : s_rnge("dscsts", i__2, "f_spk19__", (
		    ftnlen)922)], &c__6, &c_b124, ok, (ftnlen)80, (ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs are at midpoints "
	    "between adjacent input epochs.", (ftnlen)94);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);
    i__1 = npkts[0] - 3;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = (dscepc[(i__2 = i__ - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("dsce"
		"pc", i__2, "f_spk19__", (ftnlen)945)] + dscepc[(i__3 = i__) < 
		9 && 0 <= i__3 ? i__3 : s_rnge("dscepc", i__3, "f_spk19__", (
		ftnlen)945)]) / 2;
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (j = 1; j <= 6; ++j) {
		xstate[(i__2 = j - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge("xsta"
			"te", i__2, "f_spk19__", (ftnlen)957)] = (dscsts[(i__3 
			= j + i__ * 6 - 7) < 54 && 0 <= i__3 ? i__3 : s_rnge(
			"dscsts", i__3, "f_spk19__", (ftnlen)957)] + dscsts[(
			i__4 = j + (i__ + 1) * 6 - 7) < 54 && 0 <= i__4 ? 
			i__4 : s_rnge("dscsts", i__4, "f_spk19__", (ftnlen)
			957)]) / 2;
	    }
	    chckad_(name__, state, "~~", xstate, &c__6, &c_b124, ok, (ftnlen)
		    80, (ftnlen)2);
	}
    }
    unload_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Remove the trivial SPK we created; we'll create a new */
/*     one with more complexity. */

    delfil_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    Test subtype 2: write an SPK containing a small subtype 2 */
/* *    mini-segment. */
/* * */
/* ***************************************************************** */

/*     Below, it will be convenient to use cubic interpolation and a */
/*     window size of 2. */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create an SPK with subtype 2 mini-segments.", (ftnlen)43);
    spkopn_("sp19t2.bsp", "sp19t2.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the interval count. */

    nintvl = 10;

/*     Set the packet counts, subtypes, and degrees. */

/*     Since we're concentrating on interval selection */
/*     logic, we'll use small packet counts. */

    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("subtps",
		 i__2, "f_spk19__", (ftnlen)1011)] = 2;
	degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("degres",
		 i__2, "f_spk19__", (ftnlen)1012)] = 3;
	if (odd_(&i__)) {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1016)] = 3;
	} else {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1018)] = 4;
	}
    }

/*     Generate epochs and packets. */

    epcto = 0;

/*     The end epoch of each interval must be the start epoch of */
/*     the next. */

    endepc = 0.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the interval start time. */

	ivlbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge("ivlbds",
		 i__2, "f_spk19__", (ftnlen)1037)] = endepc;

/*        Set the separation of epochs. */

	if (odd_(&i__)) {
	    escale = 10.;
	} else {
	    escale = 20.;
	}
	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)1050)];
	for (j = 1; j <= i__3; ++j) {
	    ++epcto;
	    epochs[(i__2 = epcto - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge(
		    "epochs", i__2, "f_spk19__", (ftnlen)1054)] = endepc + 
		    escale * (j - 1);
	}

/*        Save the last epoch of this interval. */

	endepc = epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : 
		s_rnge("epochs", i__3, "f_spk19__", (ftnlen)1061)];
    }

/*     Save the stop time of the last interval. */

    ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds", 
	    i__1, "f_spk19__", (ftnlen)1068)] = endepc;

/*     Set the descriptor bounds. We don't need to do anything */
/*     fancy here. */

    first = ivlbds[0];
    last = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlb"
	    "ds", i__1, "f_spk19__", (ftnlen)1075)];

/*     Now assign the packets. Since we're using two-point */
/*     interpolation, selecting the wrong packets to interpolate */
/*     should yield an obviously wrong answer. */

/*     We also want to have large packet discontinuities at */
/*     interval boundaries, so the consequence of selecting */
/*     the wrong interval is obvious. */

    pktto = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Assign the packets for the Ith interval. */

	i__2 = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_spk19__", (ftnlen)1093)];
	for (j = 1; j <= i__2; ++j) {

/*           Position components: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)1101)] = 
			i__ * 1e9 + j * 100. + k;
	    }

/*           Velocities associated with positions: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)1113)] = 
			0.;
	    }
	}
    }

/*     For this segment, we'll select the last applicable interval */
/*     when the request time lies on an interval boundary. */

    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from type 2 file. SELLST = .TRUE.", (ftnlen)45);
    furnsh_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Read states corresponding to epochs. */

	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (odd_(&i__)) {
		escale = 10.;
	    } else {
		escale = 20.;
	    }

/*           Note that the last epoch of each interval, */
/*           except the last, is a special case. The */
/*           state returned for that epoch is computed */
/*           from data in the following interval, if any. */

	    if (i__ < nintvl) {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)1182)] - 1;
	    } else {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)1184)];
	    }
	    i__2 = ub;
	    for (j = 1; j <= i__2; ++j) {

/* --- Case: ------------------------------------------------------ */

		et = ivlbds[(i__3 = i__ - 1) < 21001 && 0 <= i__3 ? i__3 : 
			s_rnge("ivlbds", i__3, "f_spk19__", (ftnlen)1192)] + 
			escale * (j - 1);
		s_copy(title, "State at interval #; packet index #", (ftnlen)
			800, (ftnlen)35);
		repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			ftnlen)800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)
			800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)800);
		spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (
			ftnlen)32, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		for (k = 1; k <= 3; ++k) {
		    xstate[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)1212)] = i__ 
			    * 1e9 + j * 100. + k;
		    xstate[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)1216)] = 0.;

/*                 Also compute the state at the midpoint */
/*                 between the Jth epoch and the next. Because */
/*                 the cubics are fit to endpoints with */
/*                 zero derivatives, the function value */
/*                 at the midpoint is just the average of */
/*                 the values at the endpoints. */

/*                 The midpoint velocity should be 3/2 * the difference */
/*                 between the Jth and (J+1)st positions, divided by */
/*                 the epoch difference. This can be derived from the */
/*                 simple case */

/*                              3       2 */
/*                    p(x) = a x  +  a x  +  a x  +  a */
/*                            3       2       1       0 */


/*                    p(0)      = alpha */
/*                    p(1)      = beta */
/*                    dp/dx (0) = 0 */
/*                    dp/dx (1) = 0 */



		    midsta[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)1242)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)1242)]
			     + 50.;
		    midsta[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)1243)] = 1.5 
			    / escale * 100.;
		}

/*              Check position at the Jth epoch. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b124, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity at the Jth epoch. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b124, ok, (ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*              Perform this test only if the packet index */
/*              is not the last of the interval. */

		if (j < npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : 
			s_rnge("npkts", i__3, "f_spk19__", (ftnlen)1267)]) {
		    s_copy(title, "State in interval #; epoch midway between"
			    " that of packet index # and packet index #.", (
			    ftnlen)800, (ftnlen)84);
		    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    i__3 = j + 1;
		    repmi_(title, "#", &i__3, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)800);
		    midet = et + escale * .5;
		    spkez_(&xbody, &midet, xref, "NONE", &xcentr, state, &lt, 
			    (ftnlen)32, (ftnlen)4);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check position midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid pos", state, "~~/", midsta, &c__3, &c_b124, 
			    ok, (ftnlen)7, (ftnlen)3);

/*                 Check velocity midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid vel", &state[3], "~~/", &midsta[3], &c__3, &
			    c_b124, ok, (ftnlen)7, (ftnlen)3);
		}
	    }
	}
    }
    unload_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the file we created. */

    delfil_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    Test subtype 2: write an SPK containing a small subtype 2 */
/* *    mini-segment. This time use different velocities for each */
/* *    component. */
/* * */
/* ***************************************************************** */

/*     Below, we'll use linear functions for each position component. */
/*     The rates for each component will be distinct. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create an SPK with subtype 2 mini-segments. Use linear position "
	    "functions with different rates for each component.", (ftnlen)114);
    spkopn_("sp19t2.bsp", "sp19t2.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the interval count. */

    nintvl = 10;

/*     Set the packet counts, subtypes, and degrees. */

/*     Since we're concentrating on interval selection */
/*     logic, we'll use small packet counts. */

    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("subtps",
		 i__2, "f_spk19__", (ftnlen)1366)] = 2;
	degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("degres",
		 i__2, "f_spk19__", (ftnlen)1367)] = 3;
	if (odd_(&i__)) {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1371)] = 3;
	} else {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1373)] = 4;
	}
    }

/*     Generate epochs and packets. */

    epcto = 0;

/*     The end epoch of each interval must be the start epoch of */
/*     the next. */

    endepc = 0.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the interval start time. */

	ivlbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge("ivlbds",
		 i__2, "f_spk19__", (ftnlen)1392)] = endepc;

/*        Set the separation of epochs. */

	if (odd_(&i__)) {
	    escale = 10.;
	} else {
	    escale = 20.;
	}
	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)1405)];
	for (j = 1; j <= i__3; ++j) {
	    ++epcto;
	    epochs[(i__2 = epcto - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge(
		    "epochs", i__2, "f_spk19__", (ftnlen)1409)] = endepc + 
		    escale * (j - 1);
	}

/*        Save the last epoch of this interval. */

	endepc = epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : 
		s_rnge("epochs", i__3, "f_spk19__", (ftnlen)1416)];
    }

/*     Save the stop time of the last interval. */

    ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds", 
	    i__1, "f_spk19__", (ftnlen)1423)] = endepc;

/*     Set the descriptor bounds. We don't need to do anything */
/*     fancy here. */

    first = ivlbds[0];
    last = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlb"
	    "ds", i__1, "f_spk19__", (ftnlen)1430)];

/*     Now assign the packets. Since we're using two-point */
/*     interpolation, selecting the wrong packets to interpolate */
/*     should yield an obviously wrong answer. */

/*     We also want to have large packet discontinuities at */
/*     interval boundaries, so the consequence of selecting */
/*     the wrong interval is obvious. */

    pktto = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (odd_(&i__)) {
	    escale = 10.;
	} else {
	    escale = 20.;
	}

/*        Assign the packets for the Ith interval. */

	i__2 = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_spk19__", (ftnlen)1456)];
	for (j = 1; j <= i__2; ++j) {

/*           Position components: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)1464)] = 
			i__ * 1e9 + j * 100. * k + k;
	    }

/*           Velocities associated with positions: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)1476)] = 
			k * 100. / escale;
	    }
	}
    }

/*     For this segment, we'll select the last applicable interval */
/*     when the request time lies on an interval boundary. */

    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from constant rate type 2 file. SELLST = .TRUE.", (
	    ftnlen)59);
    furnsh_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Read states corresponding to epochs. */

	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (odd_(&i__)) {
		escale = 10.;
	    } else {
		escale = 20.;
	    }

/*           Note that the last epoch of each interval, */
/*           except the last, is a special case. The */
/*           state returned for that epoch is computed */
/*           from data in the following interval, if any. */

	    if (i__ < nintvl) {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)1545)] - 1;
	    } else {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)1547)];
	    }
	    i__2 = ub;
	    for (j = 1; j <= i__2; ++j) {

/* --- Case: ------------------------------------------------------ */

		et = ivlbds[(i__3 = i__ - 1) < 21001 && 0 <= i__3 ? i__3 : 
			s_rnge("ivlbds", i__3, "f_spk19__", (ftnlen)1555)] + 
			escale * (j - 1);
		s_copy(title, "State at interval #; packet index #; subtype "
			"2; linear case", (ftnlen)800, (ftnlen)59);
		repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			ftnlen)800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)
			800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)800);
		spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (
			ftnlen)32, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		for (k = 1; k <= 3; ++k) {
		    xstate[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)1576)] = i__ 
			    * 1e9 + j * 100. * k + k;
		    xstate[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)1580)] = k * 
			    100. / escale;

/*                 Also compute the state at the midpoint */
/*                 between the Jth epoch and the next. */

		    midsta[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)1586)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)1586)]
			     + k * 100. * .5;
		    midsta[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)1587)] = k * 
			    100. / escale;
		}

/*              Check position at the Jth epoch. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b124, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity at the Jth epoch. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b124, ok, (ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*              Perform this test only if the packet index */
/*              is not the last of the interval. */

		if (j < npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : 
			s_rnge("npkts", i__3, "f_spk19__", (ftnlen)1611)]) {
		    s_copy(title, "State in interval #; epoch midway between"
			    " that of packet index # and packet index #. Subt"
			    "ype 2; linear case.", (ftnlen)800, (ftnlen)108);
		    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    i__3 = j + 1;
		    repmi_(title, "#", &i__3, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)800);
		    midet = et + escale * .5;
		    spkez_(&xbody, &midet, xref, "NONE", &xcentr, state, &lt, 
			    (ftnlen)32, (ftnlen)4);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check position midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid pos", state, "~~/", midsta, &c__3, &c_b124, 
			    ok, (ftnlen)7, (ftnlen)3);

/*                 Check velocity midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid vel", &state[3], "~~/", &midsta[3], &c__3, &
			    c_b124, ok, (ftnlen)7, (ftnlen)3);
		}
	    }
	}
    }
    unload_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the file we created. */

    delfil_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKR19 window selection logic cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a subtype 1 segment suitable for testing interpolation wi"
	    "ndow packet selection.", (ftnlen)86);

/*     The position and velocity data in this segment are defined by */
/*     exponential functions of time. */

    spkopn_("sp19t1.bsp", "sp19t1.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);
    nintvl = 1;
    npkts[0] = 56;
    subtps[0] = 1;
    degres[0] = 27;

/*     Assign the packet epochs. */

    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epoc"
		"hs", i__2, "f_spk19__", (ftnlen)1710)] = (doublereal) i__;
    }
    first = epochs[0];
    last = epochs[(i__1 = npkts[0] - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spk19__", (ftnlen)1715)];
    ivlbds[0] = first;
    ivlbds[1] = last;

/*     Assign the packet values. */

    pktto = 0;
    i__1 = npkts[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 6; ++j) {
	    ++pktto;
	    packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : s_rnge(
		    "packts", i__2, "f_spk19__", (ftnlen)1731)] = pow_di(&
		    c_b375, &j) * exp(i__ / 100.);
	}
    }
    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up states at midpoints between packet epochs of segment we "
	    "just created.", (ftnlen)77);

/*     Touch array WORK to make f2c happy. WORK is simply a workspace */
/*     array. */

    cleard_(&c__28, work);
    furnsh_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    winsiz = 28;
    i__1 = npkts[0] - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = (epochs[(i__2 = i__ - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge(
		"epochs", i__2, "f_spk19__", (ftnlen)1785)] + epochs[(i__3 = 
		i__) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, 
		"f_spk19__", (ftnlen)1785)]) / 2;
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Generate expected state components at ET. */


/*        Let B and E be the begin and end indices of the */
/*        packets comprising the interpolation window. */

/* Computing MAX */
	i__2 = 1, i__3 = i__ - (winsiz / 2 - 1);
	b = max(i__2,i__3);
/* Computing MIN */
	i__2 = npkts[0], i__3 = i__ + (winsiz / 2 - 1) + 1;
	e = min(i__2,i__3);
	for (j = 1; j <= 6; ++j) {
	    i__2 = e;
	    for (k = b; k <= i__2; ++k) {
		xvals[(i__3 = k - b) < 28 && 0 <= i__3 ? i__3 : s_rnge("xvals"
			, i__3, "f_spk19__", (ftnlen)1805)] = epochs[(i__4 = 
			k - 1) < 2100000 && 0 <= i__4 ? i__4 : s_rnge("epochs"
			, i__4, "f_spk19__", (ftnlen)1805)];
		yvals[(i__3 = k - b) < 28 && 0 <= i__3 ? i__3 : s_rnge("yvals"
			, i__3, "f_spk19__", (ftnlen)1807)] = pow_di(&c_b375, 
			&j) * exp(k / 100.);
	    }
	    i__3 = e - b + 1;
	    xstate[(i__2 = j - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge("xstate", 
		    i__2, "f_spk19__", (ftnlen)1811)] = lgrint_(&i__3, xvals, 
		    yvals, work, &et);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Test the state at ET. */

	for (j = 1; j <= 6; ++j) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "State # component #", (ftnlen)800, (ftnlen)19);
	    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    800);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)800)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)800);
	    chcksd_("State", &state[(i__2 = j - 1) < 6 && 0 <= i__2 ? i__2 : 
		    s_rnge("state", i__2, "f_spk19__", (ftnlen)1833)], "~/", &
		    xstate[(i__3 = j - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
		    "xstate", i__3, "f_spk19__", (ftnlen)1833)], &c_b124, ok, 
		    (ftnlen)5, (ftnlen)2);
	}
    }

/*     Delete the file we created so we can start over. */

    unload_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKR19 interval re-use logic cases: */
/* * */
/* ***************************************************************** */
/* ***************************************************************** */
/* * */
/* *    SPKR19 subtype logic cases: */
/* * */
/* ***************************************************************** */
/* ***************************************************************** */
/* * */
/* *    SPKR19 interval selection logic cases: */
/* * */
/* ***************************************************************** */

/*     Below, we'll use type 0 segments because it will */
/*     be convenient to use cubic interpolation and */
/*     a window size of 2. */

/*     The velocity/acceleration data will not be compatible with the */
/*     position/velocity data. */


/* --- Case: ------------------------------------------------------ */


/*     We need a segment containing enough interval directories */
/*     so that multiple buffers full of directory entries must */
/*     be read. This implies we need more than DIRSIZ**2 intervals */
/*     in the segment. */

    tcase_("Create an SPK with interval directories.", (ftnlen)40);
    spkopn_("sp19t0.bsp", "sp19t0.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the interval count. */

    nintvl = 21000;

/*     Set the packet counts, subtypes, and degrees. */

/*     Since we're concentrating on interval selection */
/*     logic, we'll use small packet counts. */

    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("subtps",
		 i__2, "f_spk19__", (ftnlen)1922)] = 0;
	degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("degres",
		 i__2, "f_spk19__", (ftnlen)1923)] = 3;
	if (odd_(&i__)) {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1927)] = 3;
	} else {
	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spk19__", (ftnlen)1929)] = 4;
	}
    }

/*     Generate epochs and packets. */

    epcto = 0;

/*     The end epoch of each interval must be the start epoch of */
/*     the next. */

    endepc = 0.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the interval start time. */

	ivlbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : s_rnge("ivlbds",
		 i__2, "f_spk19__", (ftnlen)1948)] = endepc;

/*        Set the separation of epochs. */

	if (odd_(&i__)) {
	    escale = 10.;
	} else {
	    escale = 20.;
	}
	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)1961)];
	for (j = 1; j <= i__3; ++j) {
	    ++epcto;
	    epochs[(i__2 = epcto - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge(
		    "epochs", i__2, "f_spk19__", (ftnlen)1965)] = endepc + 
		    escale * (j - 1);
	}

/*        Save the last epoch of this interval. */

	endepc = epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : 
		s_rnge("epochs", i__3, "f_spk19__", (ftnlen)1972)];
    }

/*     Save the stop time of the last interval. */

    ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds", 
	    i__1, "f_spk19__", (ftnlen)1979)] = endepc;

/*     Set the descriptor bounds. We don't need to do anything */
/*     fancy here. */

    first = ivlbds[0];
    last = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlb"
	    "ds", i__1, "f_spk19__", (ftnlen)1986)];

/*     Now assign the packets. Since we're using two-point */
/*     interpolation, selecting the wrong packets to interpolate */
/*     should yield an obviously wrong answer. */

/*     We also want to have large packet discontinuities at */
/*     interval boundaries, so the consequence of selecting */
/*     the wrong interval is obvious. */

    pktto = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Assign the packets for the Ith interval. */

	i__2 = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_spk19__", (ftnlen)2004)];
	for (j = 1; j <= i__2; ++j) {

/*           Position components: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)2013)] = 
			i__ * 1e9 + j * 100. + k;
	    }

/*           Velocities associated with positions: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)2025)] = 
			0.;
	    }

/*           Velocity/acceleration packets: */

	    for (k = 1; k <= 6; ++k) {
		++pktto;
		packts[(i__3 = pktto - 1) < 25200000 && 0 <= i__3 ? i__3 : 
			s_rnge("packts", i__3, "f_spk19__", (ftnlen)2036)] = 
			packts[(i__4 = pktto - 7) < 25200000 && 0 <= i__4 ? 
			i__4 : s_rnge("packts", i__4, "f_spk19__", (ftnlen)
			2036)] * 10.;
	    }
	}
    }

/*     For this segment, we'll select the last applicable interval */
/*     when the request time lies on an interval boundary. */

    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from large type 0 file. SELLST = .TRUE.", (ftnlen)51);
    furnsh_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Read states corresponding to epochs. */

	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (odd_(&i__)) {
		escale = 10.;
	    } else {
		escale = 20.;
	    }

/*           Note that the last epoch of each interval, */
/*           except the last, is a special case. The */
/*           state returned for that epoch is computed */
/*           from data in the following interval, if any. */

	    if (i__ < nintvl) {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)2106)] - 1;
	    } else {
		ub = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)2108)];
	    }
	    i__2 = ub;
	    for (j = 1; j <= i__2; ++j) {

/* --- Case: ------------------------------------------------------ */

		et = ivlbds[(i__3 = i__ - 1) < 21001 && 0 <= i__3 ? i__3 : 
			s_rnge("ivlbds", i__3, "f_spk19__", (ftnlen)2116)] + 
			escale * (j - 1);
		s_copy(title, "State at interval #; packet index #", (ftnlen)
			800, (ftnlen)35);
		repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			ftnlen)800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)
			800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)800);
		spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (
			ftnlen)32, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		for (k = 1; k <= 3; ++k) {
		    xstate[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)2136)] = i__ 
			    * 1e9 + j * 100. + k;
		    xstate[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "xstate", i__3, "f_spk19__", (ftnlen)2140)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2140)]
			     * 10.;

/*                 Also compute the state at the midpoint */
/*                 between the Jth epoch and the next. Because */
/*                 the cubics are fit to endpoints with */
/*                 zero derivatives, the function value */
/*                 at the midpoint is just the average of */
/*                 the values at the endpoints. */

		    midsta[(i__3 = k - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)2150)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2150)]
			     + 50.;
		    midsta[(i__3 = k + 2) < 6 && 0 <= i__3 ? i__3 : s_rnge(
			    "midsta", i__3, "f_spk19__", (ftnlen)2151)] = 
			    midsta[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("midsta", i__4, "f_spk19__", (ftnlen)2151)]
			     * 10.;
		}

/*              Check position at the Jth epoch. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b124, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity at the Jth epoch. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b124, ok, (ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*              Perform this test only if the packet index */
/*              is not the last of the interval. */

		if (j < npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : 
			s_rnge("npkts", i__3, "f_spk19__", (ftnlen)2176)]) {
		    s_copy(title, "State in interval #; epoch midway between"
			    " that of packet index # and packet index #.", (
			    ftnlen)800, (ftnlen)84);
		    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    i__3 = j + 1;
		    repmi_(title, "#", &i__3, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)800);
		    midet = et + escale * .5;
		    spkez_(&xbody, &midet, xref, "NONE", &xcentr, state, &lt, 
			    (ftnlen)32, (ftnlen)4);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check position midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid pos", state, "~~/", midsta, &c__3, &c_b124, 
			    ok, (ftnlen)7, (ftnlen)3);

/*                 Check velocity midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid vel", &state[3], "~~/", &midsta[3], &c__3, &
			    c_b124, ok, (ftnlen)7, (ftnlen)3);
		}
	    }
	}
    }
    unload_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the file we created so we can start over. */

    delfil_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */



/*     Repeat the previous cases using a segment for which the */
/*     first applicable interval is picked. */

    spkopn_("sp19t0.bsp", "sp19t0.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sellst = FALSE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from large type 0 file. SELLST = .FALSE.", (ftnlen)52)
	    ;
    furnsh_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Read states corresponding to epochs. */

	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (odd_(&i__)) {
		escale = 10.;
	    } else {
		escale = 20.;
	    }

/*           Note that the first epoch of each interval, */
/*           except the first, is a special case. The */
/*           state returned for that epoch is computed */
/*           from data in the preceding interval, if any. */

	    if (i__ > 1) {
		lb = 2;
	    } else {
		lb = 1;
	    }
	    i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
		    s_rnge("npkts", i__2, "f_spk19__", (ftnlen)2308)];
	    for (j = lb; j <= i__3; ++j) {

/* --- Case: ------------------------------------------------------ */

		et = ivlbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : 
			s_rnge("ivlbds", i__2, "f_spk19__", (ftnlen)2312)] + 
			escale * (j - 1);
		s_copy(title, "State at interval #; packet index #", (ftnlen)
			800, (ftnlen)35);
		repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			ftnlen)800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)
			800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)800);
		spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (
			ftnlen)32, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		for (k = 1; k <= 3; ++k) {
		    xstate[(i__2 = k - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "xstate", i__2, "f_spk19__", (ftnlen)2332)] = i__ 
			    * 1e9 + j * 100. + k;
		    xstate[(i__2 = k + 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "xstate", i__2, "f_spk19__", (ftnlen)2336)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2336)]
			     * 10.;

/*                 Also compute the state at the midpoint */
/*                 between the Jth epoch and the J-1st. Because */
/*                 the cubics are fit to endpoints with */
/*                 zero derivatives, the function value */
/*                 at the midpoint is just the average of */
/*                 the values at the endpoints. */

		    midsta[(i__2 = k - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "midsta", i__2, "f_spk19__", (ftnlen)2346)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2346)]
			     - 50.;
		    midsta[(i__2 = k + 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "midsta", i__2, "f_spk19__", (ftnlen)2347)] = 
			    midsta[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("midsta", i__4, "f_spk19__", (ftnlen)2347)]
			     * 10.;
		}

/*              Check position at the Jth epoch. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b124, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity at the Jth epoch. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b124, ok, (ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*              Perform the following test only if a preceding */
/*              index exists. */

		if (j > 1) {
		    s_copy(title, "State in interval #; epoch midway between"
			    " that of packet index # and packet index #.", (
			    ftnlen)800, (ftnlen)84);
		    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    i__2 = j - 1;
		    repmi_(title, "#", &i__2, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)800);
		    midet = et - escale * .5;
		    spkez_(&xbody, &midet, xref, "NONE", &xcentr, state, &lt, 
			    (ftnlen)32, (ftnlen)4);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check position midway between the Jth and J-1st */
/*                 epochs. */

		    chckad_("Mid pos", state, "~~/", midsta, &c__3, &c_b124, 
			    ok, (ftnlen)7, (ftnlen)3);

/*                 Check velocity midway between the Jth and J-1st */
/*                 epochs. */

		    chckad_("Mid vel", &state[3], "~~/", &midsta[3], &c__3, &
			    c_b124, ok, (ftnlen)7, (ftnlen)3);
		}
	    }
	}
    }
    unload_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the SPK file. */

    delfil_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKR19 packet selection logic cases: */
/* * */
/* ***************************************************************** */

/*     Below, we'll use type 0 segments because it will */
/*     be convenient to use cubic interpolation and */
/*     a window size of 2. */

/*     The velocity/acceleration data will not be compatible with the */
/*     position/velocity data. */


/* --- Case: ------------------------------------------------------ */


/*     We must now test the packet selection logic. The first */
/*     aspect to test is use of packet directories. We must have */
/*     at least one interval that contains enough directories so */
/*     that they can't all be buffered. It's all right to have */
/*     a small number of intervals for this test. */

    tcase_("Create an SPK with interval directories.", (ftnlen)40);
    spkopn_("sp19t0.bsp", "sp19t0.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the interval count. */

    nintvl = 2;

/*     Set the packet counts, subtypes, and degrees. */

    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("subtps",
		 i__3, "f_spk19__", (ftnlen)2475)] = 0;
	degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("degres",
		 i__3, "f_spk19__", (ftnlen)2476)] = 3;
	if (odd_(&i__)) {
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spk19__", (ftnlen)2480)] = 20900;
	} else {
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spk19__", (ftnlen)2482)] = 21000;
	}
    }

/*     Generate epochs and packets. */

    epcto = 0;

/*     The end epoch of each interval must be the start epoch of */
/*     the next. */

    endepc = 0.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the interval start time. */

	ivlbds[(i__3 = i__ - 1) < 21001 && 0 <= i__3 ? i__3 : s_rnge("ivlbds",
		 i__3, "f_spk19__", (ftnlen)2501)] = endepc;

/*        Set the separation of epochs. */

	if (odd_(&i__)) {
	    escale = 1.;
	} else {
	    escale = 2.;
	}
	i__2 = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_spk19__", (ftnlen)2514)];
	for (j = 1; j <= i__2; ++j) {
	    ++epcto;
	    epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge(
		    "epochs", i__3, "f_spk19__", (ftnlen)2518)] = endepc + 
		    escale * (j - 1);
	}

/*        Save the last epoch of this interval. */

	endepc = epochs[(i__2 = epcto - 1) < 2100000 && 0 <= i__2 ? i__2 : 
		s_rnge("epochs", i__2, "f_spk19__", (ftnlen)2525)];
    }

/*     Save the stop time of the last interval. */

    ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds", 
	    i__1, "f_spk19__", (ftnlen)2532)] = endepc;

/*     Set the descriptor bounds. We don't need to do anything */
/*     fancy here. */

    first = ivlbds[0];
    last = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlb"
	    "ds", i__1, "f_spk19__", (ftnlen)2539)];

/*     Now assign the packets. Since we're using two-point */
/*     interpolation, selecting the wrong packets to interpolate */
/*     should yield an obviously wrong answer. */

/*     We also want to have large packet discontinuities at */
/*     interval boundaries, so the consequence of selecting */
/*     the wrong interval is obvious. */

    pktto = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Assign the packets for the Ith interval. */

	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spk19__", (ftnlen)2557)];
	for (j = 1; j <= i__3; ++j) {

/*           Position components: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spk19__", (ftnlen)2566)] = 
			i__ * 1e9 + j * 100. + k;
	    }

/*           Velocities associated with positions: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spk19__", (ftnlen)2578)] = 
			0.;
	    }

/*           Velocity/acceleration packets: */

	    for (k = 1; k <= 6; ++k) {
		++pktto;
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spk19__", (ftnlen)2589)] = 
			packts[(i__4 = pktto - 7) < 25200000 && 0 <= i__4 ? 
			i__4 : s_rnge("packts", i__4, "f_spk19__", (ftnlen)
			2589)] * 10.;
	    }
	}
    }

/*     For this segment, we'll select the last applicable interval */
/*     when the request time lies on an interval boundary. */

    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from large type 0 file. Packet selection test. SELLS"
	    "T = .TRUE.", (ftnlen)74);
    furnsh_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {

/*        Read states corresponding to epochs. */

	i__1 = nintvl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (odd_(&i__)) {
		escale = 1.;
	    } else {
		escale = 2.;
	    }

/*           Note that the last epoch of each interval, */
/*           except the last, is a special case. The */
/*           state returned for that epoch is computed */
/*           from data in the following interval, if any. */

	    if (i__ < nintvl) {
		ub = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : 
			s_rnge("npkts", i__3, "f_spk19__", (ftnlen)2660)] - 1;
	    } else {
		ub = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : 
			s_rnge("npkts", i__3, "f_spk19__", (ftnlen)2662)];
	    }
	    i__3 = ub;
	    for (j = 1; j <= i__3; ++j) {

/* --- Case: ------------------------------------------------------ */

		et = ivlbds[(i__2 = i__ - 1) < 21001 && 0 <= i__2 ? i__2 : 
			s_rnge("ivlbds", i__2, "f_spk19__", (ftnlen)2670)] + 
			escale * (j - 1);
		s_copy(title, "State at interval #; packet index #", (ftnlen)
			800, (ftnlen)35);
		repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			ftnlen)800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (ftnlen)
			800);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)800);
		spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (
			ftnlen)32, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		for (k = 1; k <= 3; ++k) {
		    xstate[(i__2 = k - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "xstate", i__2, "f_spk19__", (ftnlen)2690)] = i__ 
			    * 1e9 + j * 100. + k;
		    xstate[(i__2 = k + 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "xstate", i__2, "f_spk19__", (ftnlen)2694)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2694)]
			     * 10.;

/*                 Also compute the state at the midpoint */
/*                 between the Jth epoch and the next. Because */
/*                 the cubics are fit to endpoints with */
/*                 zero derivatives, the function value */
/*                 at the midpoint is just the average of */
/*                 the values at the endpoints. */

		    midsta[(i__2 = k - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "midsta", i__2, "f_spk19__", (ftnlen)2704)] = 
			    xstate[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("xstate", i__4, "f_spk19__", (ftnlen)2704)]
			     + 50.;
		    midsta[(i__2 = k + 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			    "midsta", i__2, "f_spk19__", (ftnlen)2705)] = 
			    midsta[(i__4 = k - 1) < 6 && 0 <= i__4 ? i__4 : 
			    s_rnge("midsta", i__4, "f_spk19__", (ftnlen)2705)]
			     * 10.;
		}

/*              Check position at the Jth epoch. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b124, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity at the Jth epoch. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b124, ok, (ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*              Perform this test only if the packet index */
/*              is not the last of the interval. */

		if (j < npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : 
			s_rnge("npkts", i__2, "f_spk19__", (ftnlen)2730)]) {
		    s_copy(title, "State in interval #; epoch midway between"
			    " that of packet index # and packet index #.", (
			    ftnlen)800, (ftnlen)84);
		    repmi_(title, "#", &i__, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(title, "#", &j, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    i__2 = j + 1;
		    repmi_(title, "#", &i__2, title, (ftnlen)800, (ftnlen)1, (
			    ftnlen)800);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)800);
		    midet = et + escale * .5;
		    spkez_(&xbody, &midet, xref, "NONE", &xcentr, state, &lt, 
			    (ftnlen)32, (ftnlen)4);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Check position midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid pos", state, "~~/", midsta, &c__3, &c_b124, 
			    ok, (ftnlen)7, (ftnlen)3);

/*                 Check velocity midway between the Jth and J+1st */
/*                 epochs. */

		    chckad_("Mid vel", &state[3], "~~/", &midsta[3], &c__3, &
			    c_b124, ok, (ftnlen)7, (ftnlen)3);
		}
	    }
	}
    }
    unload_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the file we created so we can start over. */

    delfil_("sp19t0.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete SPK files.", (ftnlen)28);
    if (exists_("test19err.bsp", (ftnlen)13)) {
	delfil_("test19err.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("sp19t0.bsp", (ftnlen)10)) {
	delfil_("sp19t0.bsp", (ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("sp19t1.bsp", (ftnlen)10)) {
	delfil_("sp19t1.bsp", (ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_spk19__ */

