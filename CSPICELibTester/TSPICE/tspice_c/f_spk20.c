/* f_spk20.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b18 = 1.;
static integer c_b23 = 1980000;
static logical c_true = TRUE_;
static integer c__51 = 51;
static integer c_n1 = -1;
static doublereal c_b61 = 0.;
static doublereal c_b64 = -1.;
static integer c__3 = 3;
static doublereal c_b250 = 1e-11;
static doublereal c_b338 = 5e-11;
static integer c__2 = 2;
static integer c__6 = 6;
static doublereal c_b440 = 1e-13;

/* $Procedure F_SPK20 ( SPK data type 20 tests ) */
/* Subroutine */ int f_spk20__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double d_int(doublereal *);

    /* Local variables */
    static integer hans, body;
    static doublereal last;
    extern /* Subroutine */ int t_spkchb__(char *, char *, char *, doublereal 
	    *, doublereal *, integer *, integer *, doublereal *, doublereal *,
	     doublereal *, ftnlen, ftnlen, ftnlen);
    static doublereal work[51];
    static integer i__, j, k;
    static doublereal cdata[1980000];
    static integer n;
    static char label[80];
    extern /* Subroutine */ int dafgn_(char *, ftnlen), dafgs_(doublereal *);
    static doublereal delta;
    static char frame[32], segid[80];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5];
    extern /* Subroutine */ int dafus_(doublereal *, integer *, integer *, 
	    doublereal *, integer *);
    static integer recno;
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static doublereal midpt;
    static integer nsamp;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), spkw20_(integer *
	    , integer *, integer *, char *, doublereal *, doublereal *, char *
	    , doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen), bodc2n_(
	    integer *, char *, logical *, ftnlen), t_success__(logical *);
    static doublereal dc[2];
    static integer ic[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     daffna_(logical *), dafbfs_(integer *);
    static doublereal au, et, dscale;
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), delfil_(
	    char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int dafcls_(integer *), chckxc_(logical *, char *,
	     logical *, ftnlen), chcksl_(char *, logical *, logical *, 
	    logical *, ftnlen);
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    static doublereal tscale;
    extern /* Subroutine */ int dafopr_(char *, integer *, ftnlen);
    static doublereal velcof[153];
    static integer center;
    static doublereal initjd;
    extern /* Subroutine */ int unload_(char *, ftnlen);
    static char centnm[36];
    static doublereal stabuf[600006]	/* was [6][100001] */;
    static char bodynm[36];
    static doublereal intlen, poscof[153], initfr;
    static integer polydg;
    extern /* Subroutine */ int spkcls_(integer *), vsclip_(doublereal *, 
	    doublereal *), furnsh_(char *, ftnlen);
    static integer nterms;
    static doublereal et0, et1, xstbuf[600006]	/* was [6][100001] */;
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), convrt_(doublereal *, char *, char *, doublereal 
	    *, ftnlen, ftnlen), spkezp_(integer *, doublereal *, char *, char 
	    *, integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int spksub_(integer *, doublereal *, char *, 
	    doublereal *, doublereal *, integer *, ftnlen), tstlsk_(void), 
	    tstspk_(char *, logical *, integer *, ftnlen);
    extern doublereal j2000_(void), spd_(void);
    static integer han0, han2;
    static doublereal day0, pos0[3];

/* $ Abstract */

/*     Exercise routines associated with SPK data type 20. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare SPK data record size.  This record is declared in */
/*     SPKPVN and is passed to SPK reader (SPKRxx) and evaluator */
/*     (SPKExx) routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     1) If new SPK types are added, it may be necessary to */
/*        increase the size of this record.  The header of SPKPVN */
/*        should be updated as well to show the record size */
/*        requirement for each data type. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 05-OCT-2012 (NJB) */

/*        Updated to support increase of maximum degree to 27 for types */
/*        2, 3, 8, 9, 12, 13, 18, and 19. See SPKPVN for a list */
/*        of record size requirements as a function of data type. */

/* -    SPICELIB Version 1.0.0, 16-AUG-2002 (NJB) */

/* -& */

/*     End include file spkrec.inc */

/* $ Abstract */

/*     Declare parameters specific to SPK type 20. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 30-DEC-2013 (NJB) */

/* -& */
/*     MAXDEG         is the maximum allowed degree of the input */
/*                    Chebyshev expansions. If the value of MAXDEG is */
/*                    increased, the SPICELIB routine SPKPVN must be */
/*                    changed accordingly. In particular, the size of */
/*                    the record passed to SPKRnn and SPKEnn must be */
/*                    increased, and comments describing the record size */
/*                    must be changed. */

/*                    The record size requirement is */

/*                       MAXREC = 3 * ( MAXDEG + 3 ) */



/*     TOLSCL         is a tolerance scale factor (also called a */
/*                    "relative tolerance") used for time coverage */
/*                    bound checking. TOLSCL is unitless. TOLSCL */
/*                    produces a tolerance value via the formula */

/*                       TOL = TOLSCL * MAX( ABS(FIRST), ABS(LAST) ) */

/*                    where FIRST and LAST are the coverage time bounds */
/*                    of a type 20 segment, expressed as seconds past */
/*                    J2000 TDB. */

/*                    The resulting parameter TOL is used as a tolerance */
/*                    for comparing the input segment descriptor time */
/*                    bounds to the first and last epoch covered by the */
/*                    sequence of time intervals defined by the inputs */
/*                    to SPKW20: */

/*                       INITJD */
/*                       INITFR */
/*                       INTLEN */
/*                       N */

/*     Tolerance scale for coverage gap at the endpoints */
/*     of the segment coverage interval: */


/*     End of include file spk20.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the following routines that write and read */
/*     type 20 SPK data: */

/*        SPKE20 */
/*        SPKR20 */
/*        SPKW20 */

/*     The SPK type 20 subsetter has its own test family. */

/*     The higher level SPK routine */

/*        SPKPVN */

/*     is also exercised by this test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 16-AUG-2016 (NJB) */

/*        Bug fix: this routine no longer calls TSTSPK with the "load" */
/*        option set to .TRUE., nor does it call DAFCLS to unload the */
/*        file (which left the loaded file in the SPKBSR subsystem). */
/*        Instead, this routine calls FURNSH to load the SPK. */

/* -    TSPICE Version 1.0.0, 20-MAR-2014 (NJB) */


/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Note: SEGID is declared longer than SIDLEN because of the need to */
/*     hold a long string for testing error handling. */


/*     Saved variables */


/*     Save all local variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_SPK20", (ftnlen)7);

/*     Open a new SPK file for writing. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create kernels.", (ftnlen)22);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);

/*     Initialize the data buffer with values that are recognizable but */
/*     otherwise bogus. */

    for (i__ = 1; i__ <= 1980000; ++i__) {
	cdata[(i__1 = i__ - 1) < 1980000 && 0 <= i__1 ? i__1 : s_rnge("cdata",
		 i__1, "f_spk20__", (ftnlen)297)] = (doublereal) i__;
    }

/*     Pick body, center, and frame. */

    body = 3;
    center = 10;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     Initial record count. */

    n = 100;

/*     Polynomial degree. */

    polydg = 16;

/*     Record interval length and start time. Units */
/*     are Julian ephemeris days. */

    intlen = 5.;
    initjd = 2451545.;
    initfr = .25;

/*     Pick nominal time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/*     Initialize segment identifier. */

    s_copy(segid, "spkw20 test segment", (ftnlen)80, (ftnlen)19);

/* ***************************************************************** */
/* * */
/* *    SPKW20 error cases: */
/* * */
/* ***************************************************************** */


/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid frame.", (ftnlen)14);
    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    polydg = 1;
    dscale = au;
    tscale = spd_();
    cleard_(&c_b23, cdata);
    spkw20_(&handle, &body, &center, "XXX", &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)3, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree too high.", (ftnlen)27);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     POLYDG = MAXDEG + 1 */

    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &c__51, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Polynomial degree too low.", (ftnlen)26);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &c_n1, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDEGREE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID too long.", (ftnlen)15);
    s_copy(segid, "1234567890123456789012345678912345678901234567890", (
	    ftnlen)80, (ftnlen)49);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(IDSTRINGTOOLONG)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID contains non-printable characters.", (ftnlen)40);
    s_copy(segid, "spkw20 test segment", (ftnlen)80, (ftnlen)19);
    *(unsigned char *)&segid[4] = '\a';
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid coefficient count", (ftnlen)25);
    s_copy(segid, "spkw20 test segment", (ftnlen)80, (ftnlen)19);
    n = 0;
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = -1;
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = 100;

/* --- Case: ------------------------------------------------------ */

    tcase_("Descriptor times out of order", (ftnlen)29);
    first = last + 1.;
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);

/*     Restore original descriptor time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/* --- Case: ------------------------------------------------------ */

    tcase_("Gap following last epoch", (ftnlen)24);
    last += 2e-4;
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();
    first += -2e-4;
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);

/*     Restore original descriptor time bounds. */

    first = (initjd - j2000_() + initfr) * spd_();
    last = (initjd - j2000_() + initfr + n * intlen) * spd_();

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive interval length", (ftnlen)28);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &c_b61, &n, 
	    &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INTLENNOTPOS)", ok, (ftnlen)19);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &c_b64, &n, 
	    &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INTLENNOTPOS)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive distance or time scale", (ftnlen)35);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &c_b61, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &c_b64, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &c_b61, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &c_b64, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPOSITIVESCALE)", ok, (ftnlen)23);

/*     Delete SPK used for error test cases. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ***************************************************************** */
/* * */
/* *    SPKW20 non-error exception cases: */
/* * */
/* ***************************************************************** */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with coverage gaps at each end.", (ftnlen)46);

/*     The following segment will have a two-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     9 ms in each direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 2.;
    initjd = j2000_() - 1.;
    initfr = 0.;
    first = -spd_() - 8e-9;
    last = spd_() + 8e-9;
    n = 1;
    polydg = 20;

/*     The distance scale is AU; the time scale is Julian days. */

    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dscale = au;
    tscale = spd_();
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and coverage gaps at "
	    "each end.", (ftnlen)73);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     40 ms in each direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_() - .004;
    last = (initjd + intlen - j2000_()) * spd_() + .004;
    n = 1;
    polydg = 20;

/*     The distance scale is AU; the time scale is Julian days. */

    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dscale = au;
    tscale = spd_();
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     And one more error case: */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and excessive coverag"
	    "e gaps at the left end.", (ftnlen)87);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     100 ms in the negative direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_() - .1;
    last = (initjd + intlen - j2000_()) * spd_();
    n = 1;
    polydg = 20;

/*     The distance scale is AU; the time scale is Julian days. */

    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dscale = au;
    tscale = spd_();
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create segment with long coverage interval and excessive coverag"
	    "e gaps at the right end.", (ftnlen)88);

/*     For this segment, the gap tolerance will be based on */
/*     a relative scale. */


/*     The following segment will have a 1000000-day record coverage */
/*     interval, and the descriptor coverage will extend an extra */
/*     100 ms in the negative direction. */

    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    intlen = 1e6;
    initjd = j2000_() - intlen / 2;
    initfr = 0.;
    first = (initjd - j2000_()) * spd_();
    last = (initjd + intlen - j2000_()) * spd_() + .1;
    n = 1;
    polydg = 20;

/*     The distance scale is AU; the time scale is Julian days. */

    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dscale = au;
    tscale = spd_();
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKW20, SPKR20, SPKE20, SPKS20 normal cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create SPK with one small type 20 segment.", (ftnlen)42);

/*     Create a small SPK file containing 1 record. We'll */
/*     use the moon as the target and the earth-moon barycenter */
/*     as the center. */


/*     Load test SPK to provide data. */

    if (exists_("test.bsp", (ftnlen)8)) {
	delfil_("test.bsp", (ftnlen)8);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create the SPK but don't ask TSTSPK to load it. Use FURNSH */
/*     instead to simplify cleanup. */

    tstspk_("test.bsp", &c_false, &han0, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     The record covers two days. */

    first = -spd_();
    last = spd_();
    n = 1;
    polydg = 20;

/*     The interval length has units of Julian days. */

    intlen = (last - first) / spd_();
    day0 = j2000_() + first / spd_();
    initjd = d_int(&day0);
    initfr = day0 - initjd;

/*     The distance scale is AU; the time scale is Julian days. */

    convrt_(&c_b18, "AU", "KM", &au, (ftnlen)2, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dscale = au;
    tscale = spd_();
/*      DSCALE = 1.D0 */
/*      TSCALE = 1.D0 */

/*     Generate position and velocity coefficients for the segment. */
/*     We'll store only the velocity coefficients. */

    bodc2n_(&center, centnm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodc2n_(&body, bodynm, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nterms = polydg + 1;
    t_spkchb__(bodynm, centnm, frame, &first, &last, &nterms, &nterms, work, 
	    poscof, velcof, (ftnlen)36, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the coefficients to represent positions in units */
/*     of DSCALE and velocities in units of DSCALE/TSCALE. */

    i__1 = nterms * 3;
    for (i__ = 1; i__ <= i__1; ++i__) {
	poscof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("poscof", 
		i__2, "f_spk20__", (ftnlen)925)] = poscof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("poscof", i__3, "f_spk20__", 
		(ftnlen)925)] / dscale;
	velcof[(i__2 = i__ - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge("velcof", 
		i__2, "f_spk20__", (ftnlen)927)] = velcof[(i__3 = i__ - 1) < 
		153 && 0 <= i__3 ? i__3 : s_rnge("velcof", i__3, "f_spk20__", 
		(ftnlen)927)] / dscale * tscale;
    }

/*     Get the state at the interval midpoint. */

    midpt = (first + last) / 2;
    spkezp_(&body, &midpt, frame, "NONE", &center, pos0, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the midpoint position to units of DSCALE. */

    d__1 = 1. / dscale;
    vsclip_(&d__1, pos0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Pack the coefficient array. */

    k = 1;
    for (i__ = 1; i__ <= 3; ++i__) {
	j = (i__ - 1) * nterms + 1;
	moved_(&velcof[(i__1 = j - 1) < 153 && 0 <= i__1 ? i__1 : s_rnge(
		"velcof", i__1, "f_spk20__", (ftnlen)954)], &nterms, &cdata[(
		i__2 = k - 1) < 1980000 && 0 <= i__2 ? i__2 : s_rnge("cdata", 
		i__2, "f_spk20__", (ftnlen)954)]);
	cdata[(i__1 = k + nterms - 1) < 1980000 && 0 <= i__1 ? i__1 : s_rnge(
		"cdata", i__1, "f_spk20__", (ftnlen)955)] = pos0[(i__2 = i__ 
		- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("pos0", i__2, "f_spk20"
		"__", (ftnlen)955)];
	k = k + nterms + 1;
    }
    if (exists_("spk_test_20_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);
    spkw20_(&handle, &body, &center, frame, &first, &last, segid, &intlen, &n,
	     &polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sample states from small SPK.", (ftnlen)29);

/*     Sample and check states from the new type 20 SPK file. */

    nsamp = 10001;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	spkez_(&body, &et, frame, "NONE", &center, &xstbuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("xstbuf", i__2, 
		"f_spk20__", (ftnlen)1001)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	spkez_(&body, &et, frame, "NONE", &center, &stabuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("stabuf", i__2, 
		"f_spk20__", (ftnlen)1019)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Test position results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Pos(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 6) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1034)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 6) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1034)], &
		c__3, &c_b250, ok, (ftnlen)80, (ftnlen)3);
    }

/*     Test velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Vel(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 3) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1048)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 3) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1048)], &
		c__3, &c_b250, ok, (ftnlen)80, (ftnlen)3);
    }
    unload_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create an SPK with a large type 20 segment having multiple recor"
	    "ds.", (ftnlen)67);
    n = 1000;

/*     Load test SPK to provide data. */

    furnsh_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(segid, "Small test segment: type 20", (ftnlen)80, (ftnlen)27);
    center = 3;
    body = 301;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     The interval length has units of Julian days. */

/*     Each record covers 1/8 day. */

    intlen = .125;
    first = jyear_() * -10;
    last = first + n * intlen * spd_();
    polydg = 20;
    day0 = j2000_() + first / spd_();
    initjd = d_int(&day0);
    initfr = day0 - initjd;
/*      WRITE (*,*) 'INITJD = ', INITJD */
/*      WRITE (*,*) 'INITFR = ', INITFR */

/*     Create N data records. */

    nterms = polydg + 1;
    k = 1;
    i__1 = n;
    for (recno = 1; recno <= i__1; ++recno) {
	et0 = first + (recno - 1) * intlen * spd_();
	et1 = et0 + intlen * spd_();
	t_spkchb__(bodynm, centnm, frame, &et0, &et1, &nterms, &nterms, work, 
		poscof, velcof, (ftnlen)36, (ftnlen)36, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Scale the coefficients to represent positions in units */
/*        of DSCALE and velocities in units of DSCALE/TSCALE. */

	i__2 = nterms * 3;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    poscof[(i__3 = i__ - 1) < 153 && 0 <= i__3 ? i__3 : s_rnge("posc"
		    "of", i__3, "f_spk20__", (ftnlen)1123)] = poscof[(i__4 = 
		    i__ - 1) < 153 && 0 <= i__4 ? i__4 : s_rnge("poscof", 
		    i__4, "f_spk20__", (ftnlen)1123)] / dscale;
	    velcof[(i__3 = i__ - 1) < 153 && 0 <= i__3 ? i__3 : s_rnge("velc"
		    "of", i__3, "f_spk20__", (ftnlen)1125)] = velcof[(i__4 = 
		    i__ - 1) < 153 && 0 <= i__4 ? i__4 : s_rnge("velcof", 
		    i__4, "f_spk20__", (ftnlen)1125)] / dscale * tscale;
	}

/*        Get the state at the interval midpoint. */

	midpt = (et0 + et1) / 2;
	spkezp_(&body, &midpt, frame, "NONE", &center, pos0, &lt, (ftnlen)32, 
		(ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Scale the midpoint position to units of DSCALE. */

	d__1 = 1. / dscale;
	vsclip_(&d__1, pos0);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Pack the coefficient array. */

	for (i__ = 1; i__ <= 3; ++i__) {
	    j = (i__ - 1) * nterms + 1;
	    moved_(&velcof[(i__2 = j - 1) < 153 && 0 <= i__2 ? i__2 : s_rnge(
		    "velcof", i__2, "f_spk20__", (ftnlen)1151)], &nterms, &
		    cdata[(i__3 = k - 1) < 1980000 && 0 <= i__3 ? i__3 : 
		    s_rnge("cdata", i__3, "f_spk20__", (ftnlen)1151)]);
	    cdata[(i__2 = k + nterms - 1) < 1980000 && 0 <= i__2 ? i__2 : 
		    s_rnge("cdata", i__2, "f_spk20__", (ftnlen)1152)] = pos0[(
		    i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("pos0", 
		    i__3, "f_spk20__", (ftnlen)1152)];
	    k = k + nterms + 1;
	}
    }
    unload_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("spk_test_20_v2.bsp", (ftnlen)18)) {
	delfil_("spk_test_20_v2.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_20_v2.bsp", " ", &c__0, &han2, (ftnlen)18, (ftnlen)1);
    spkw20_(&han2, &body, &center, frame, &first, &last, segid, &intlen, &n, &
	    polydg, cdata, &dscale, &tscale, &initjd, &initfr, (ftnlen)32, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sample states from the large SPK.", (ftnlen)33);
    furnsh_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 20 SPK file. */

    nsamp = 20001;
    delta = (last - first) / (nsamp - 1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &xstbuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("xstbuf", i__2, 
		"f_spk20__", (ftnlen)1206)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &stabuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("stabuf", i__2, 
		"f_spk20__", (ftnlen)1224)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Test position results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Pos(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 6) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1239)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 6) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1239)], &
		c__3, &c_b338, ok, (ftnlen)80, (ftnlen)3);
    }

/*     Test velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Vel(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 3) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1253)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 3) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1253)], &
		c__3, &c_b338, ok, (ftnlen)80, (ftnlen)3);
    }
    unload_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKS20 test: subset small segment.", (ftnlen)34);
    dafopr_("spk_test_20_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*      WRITE (*,*) 'START = ', DC(1) */
/*      WRITE (*,*) 'SPAN  = ', DC(2)-DC(1) */
/*      WRITE (*,*) 'STOP  = ', DC(2) */
    first = dc[0] + 6400.;
    last = dc[1] - 6400.;
    if (exists_("test20sub.bsp", (ftnlen)13)) {
	delfil_("test20sub.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("test20sub.bsp", " ", &c__0, &hans, (ftnlen)13, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spksub_(&handle, descr, segid, &first, &last, &hans, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK. */

    spkcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close source SPK. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for DAF search. */

    dafopr_("test20sub.bsp", &hans, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK and re-open for sampling. */

    dafcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected states from source SPK. */

    nsamp = 100001;
    delta = (last - first) / (nsamp - 1);
    furnsh_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 20 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &xstbuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("xstbuf", i__2, 
		"f_spk20__", (ftnlen)1368)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for sampling. */

    furnsh_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 20 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	spkez_(&body, &et, frame, "NONE", &center, &stabuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("stabuf", i__2, 
		"f_spk20__", (ftnlen)1389)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test position results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Pos(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 6) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1408)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 6) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1408)], &
		c__3, &c_b440, ok, (ftnlen)80, (ftnlen)3);
    }

/*     Test velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Vel(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 3) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1422)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 3) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1422)], &
		c__3, &c_b440, ok, (ftnlen)80, (ftnlen)3);
    }
/*      WRITE (*,*) 'START = ', DC(1) */
/*      WRITE (*,*) 'STOP  = ', DC(2) */

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKS20 test: subset large segment.", (ftnlen)34);

/*     This time, knock off 10 day's worth of data from */
/*     both ends of the coverage interval. */

    unload_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafopr_("spk_test_20_v2.bsp", &han2, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    first = dc[0] + spd_() * 10;
    last = dc[1] - spd_() * 10;
    if (exists_("test20sub.bsp", (ftnlen)13)) {
	delfil_("test20sub.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("test20sub.bsp", " ", &c__0, &hans, (ftnlen)13, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spksub_(&han2, descr, segid, &first, &last, &hans, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK. */

    spkcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close source SPK. */

    dafcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for DAF search. */

    dafopr_("test20sub.bsp", &hans, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK and re-open for sampling. */

    dafcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected states from source SPK. */

    nsamp = 10001;
    delta = (last - first) / (nsamp - 1);
    furnsh_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 20 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &xstbuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("xstbuf", i__2, 
		"f_spk20__", (ftnlen)1545)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for sampling. */

    furnsh_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 20 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = first + (i__ - 1) * delta;
	et = brcktd_(&d__1, &first, &last);
	spkez_(&body, &et, frame, "NONE", &center, &stabuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("stabuf", i__2, 
		"f_spk20__", (ftnlen)1566)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test position results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Pos(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 6) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1583)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 6) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1583)], &
		c__3, &c_b440, ok, (ftnlen)80, (ftnlen)3);
    }

/*     Test velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Vel(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 3) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk20__", (ftnlen)1597)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 3) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk20__", (ftnlen)1597)], &
		c__3, &c_b440, ok, (ftnlen)80, (ftnlen)3);
    }
/*      WRITE (*,*) 'START = ', DC(1) */
/*      WRITE (*,*) 'STOP  = ', DC(2) */

/* --- Case: ------------------------------------------------------ */


/*     Close and delete the SPK files. */

    tcase_("Clean up.", (ftnlen)9);

/*     SPK0 has already been unloaded. */

    delfil_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_20_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test20sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_spk20__ */

