/* f_spk21.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__111 = 111;
static logical c_true = TRUE_;
static integer c__200 = 200;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__6 = 6;
static integer c__21 = 21;
static doublereal c_b157 = 0.;
static integer c__14 = 14;
static integer c__3 = 3;
static doublereal c_b360 = 5e-12;
static doublereal c_b454 = 1e-13;

/* $Procedure F_SPK21 ( SPK data type 21 tests ) */
/* Subroutine */ int f_spk21__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer i_dnnt(doublereal *);

    /* Local variables */
    static integer hans, body;
    static doublereal last, step;
    static integer i__, j, k, n;
    static char label[80];
    extern /* Subroutine */ int dafgn_(char *, ftnlen), dafgs_(doublereal *);
    static doublereal delta;
    static char frame[32], segid[80];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5], t13rec[198], t21rec[300];
    extern /* Subroutine */ int dafus_(doublereal *, integer *, integer *, 
	    doublereal *, integer *);
    static doublereal tbuff[10099];
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), moved_(doublereal *, integer *,
	     doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer nsamp;
    static doublereal state[6];
    extern /* Subroutine */ int spkr21_(integer *, doublereal *, doublereal *,
	     doublereal *), topen_(char *, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int spkw21_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), spkez_(integer *, 
	    doublereal *, char *, char *, integer *, doublereal *, doublereal 
	    *, ftnlen, ftnlen), t_success__(logical *);
    static doublereal dc[2];
    static integer ic[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     daffna_(logical *), dafbfs_(integer *);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), dafcls_(
	    integer *), delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal dlbuff[1120989]	/* was [111][10099] */;
    static integer frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal dpbuff[1120990];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), chcksi_(char *, integer *, char *, integer *, integer 
	    *, logical *, ftnlen, ftnlen), dafopr_(char *, integer *, ftnlen);
    static integer center, maxdim;
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen), unload_(
	    char *, ftnlen);
    static doublereal stabuf[600006]	/* was [6][100001] */;
    static integer mioidx, dlsize;
    extern /* Subroutine */ int spkopa_(char *, integer *, ftnlen), spkcls_(
	    integer *), furnsh_(char *, ftnlen);
    static doublereal xstate[6];
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    static doublereal xstbuf[600006]	/* was [6][100001] */;
    extern /* Subroutine */ int spksub_(integer *, doublereal *, char *, 
	    doublereal *, doublereal *, integer *, ftnlen);
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen);
    extern doublereal spd_(void);
    extern /* Subroutine */ int t_t13xmd__(doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    static integer han0, han2;

/* $ Abstract */

/*     Exercise routines associated with SPK data type 21. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 21. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 25-DEC-2013 (NJB) */

/* -& */

/*     MAXTRM      is the maximum number of terms allowed in each */
/*                 component of the difference table contained in a type */
/*                 21 SPK difference line. MAXTRM replaces the fixed */
/*                 table parameter value of 15 used in SPK type 1 */
/*                 segments. */

/*                 Type 21 segments have variable size. Let MAXDIM be */
/*                 the dimension of each component of the difference */
/*                 table within each difference line. Then the size */
/*                 DLSIZE of the difference line is */

/*                    ( 4 * MAXDIM ) + 11 */

/*                 MAXTRM is the largest allowed value of MAXDIM. */



/*     End of include file spk21.inc. */

/* $ Abstract */

/*     Declare SPK data record size.  This record is declared in */
/*     SPKPVN and is passed to SPK reader (SPKRxx) and evaluator */
/*     (SPKExx) routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     1) If new SPK types are added, it may be necessary to */
/*        increase the size of this record.  The header of SPKPVN */
/*        should be updated as well to show the record size */
/*        requirement for each data type. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 05-OCT-2012 (NJB) */

/*        Updated to support increase of maximum degree to 27 for types */
/*        2, 3, 8, 9, 12, 13, 18, and 19. See SPKPVN for a list */
/*        of record size requirements as a function of data type. */

/* -    SPICELIB Version 1.0.0, 16-AUG-2002 (NJB) */

/* -& */

/*     End include file spkrec.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the following routines that write and read */
/*     type 21 SPK data: */

/*        SPKE21 */
/*        SPKR21 */
/*        SPKW21 */

/*     The SPK type 21 subsetter has its own test family. */

/*     The higher level SPK routine */

/*        SPKPVN */

/*     is also exercised by this test family. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 06-JUN-2014 (NJB) */

/*        Updated to accommodate new step size error */
/*        check in SPKW21. Now the difference line element */
/*        representing the maximum integration order is used */
/*        by this routine, so arbitrary, bogus values can't */
/*        be used for this element in test data. */

/*        A check has been added to ensure that zero step size */
/*        values are allowed past index KQMAX1-2 in the step size */
/*        table. */

/* -    TSPICE Version 1.0.0, 17-JAN-2014 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Note: SEGID is declared longer than SIDLEN because of the need to */
/*     hold a long string for testing error handling. */


/*     Saved variables */


/*     Save all local variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_SPK21", (ftnlen)7);

/*     Open a new SPK file for writing. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  open a new SPK file for writing.", (ftnlen)40);
    if (exists_("spk_test_21_v1.bsp", (ftnlen)18)) {
	delfil_("spk_test_21_v1.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_21_v1.bsp", " ", &c__0, &handle, (ftnlen)18, (ftnlen)1);

/*     Initialize the time and data buffers with values that are */
/*     recognizable but otherwise bogus. */

    maxdim = 25;
    mioidx = (maxdim << 2) + 8;
    for (i__ = 1; i__ <= 10099; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk21__", (ftnlen)286)] = i__ * 1e3;
	for (j = 1; j <= 111; ++j) {
	    dlbuff[(i__1 = j + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? i__1 
		    : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)290)] = 
		    tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? i__2 : 
		    s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)290)] + j - 1;
	}
	dlbuff[(i__1 = mioidx + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? 
		i__1 : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)294)] = (
		doublereal) maxdim;
    }

/*     Pick body, center, and frame. */

    body = 3;
    center = 10;
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);

/*     Initial difference line count. */

    n = 100;

/*     Pick nominal time bounds. */

    first = 0.;
    last = tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk21__", (ftnlen)314)];

/*     Initialize segment identifier. */

    s_copy(segid, "spkw21 test segment", (ftnlen)80, (ftnlen)19);

/* ***************************************************************** */
/* * */
/* *    SPKW21 error cases: */
/* * */
/* ***************************************************************** */


/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid frame.", (ftnlen)14);
    spkw21_(&handle, &body, &center, "XXX", &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)3, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDREFFRAME)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID too long.", (ftnlen)15);
    s_copy(segid, "1234567890123456789012345678912345678901234567890", (
	    ftnlen)80, (ftnlen)49);
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SEGIDTOOLONG)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SEGID contains non-printable characters.", (ftnlen)40);
    s_copy(segid, "spkw21 test segment", (ftnlen)80, (ftnlen)19);
    *(unsigned char *)&segid[4] = '\a';
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(NONPRINTABLECHARS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid difference line count", (ftnlen)29);
    s_copy(segid, "spkw21 test segment", (ftnlen)80, (ftnlen)19);
    n = 0;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = -1;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
    n = 100;

/* --- Case: ------------------------------------------------------ */

    tcase_("Descriptor times out of order", (ftnlen)29);
    first = last + 1.;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADDESCRTIMES)", ok, (ftnlen)20);
    first = 0.;

/* --- Case: ------------------------------------------------------ */

    tcase_("epochs out of order", (ftnlen)19);
    et = tbuff[2];
    tbuff[2] = tbuff[1];
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(TIMESOUTOFORDER)", ok, (ftnlen)22);
    tbuff[2] = et;

/* --- Case: ------------------------------------------------------ */

    tcase_("Gap following last epoch", (ftnlen)24);
    et = tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk21__", (ftnlen)434)];
    tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", i__1, 
	    "f_spk21__", (ftnlen)435)] = tbuff[(i__2 = n - 1) < 10099 && 0 <= 
	    i__2 ? i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)435)] - 
	    1e-6;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(COVERAGEGAP)", ok, (ftnlen)18);
    tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", i__1, 
	    "f_spk21__", (ftnlen)442)] = et;

/* --- Case: ------------------------------------------------------ */

    tcase_("DLMAX is out of range.", (ftnlen)22);
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__200,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(DIFFLINETOOLARGE)", ok, (ftnlen)23);
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__0, 
	    dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(DIFFLINETOOSMALL)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Zero step in step size array", (ftnlen)28);

/*     Initialize the time and data buffers with values that are */
/*     recognizable but otherwise bogus. */

    maxdim = 25;
    for (i__ = 1; i__ <= 10099; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk21__", (ftnlen)478)] = i__ * 1e3;
	for (j = 1; j <= 111; ++j) {
	    dlbuff[(i__1 = j + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? i__1 
		    : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)482)] = 
		    tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? i__2 : 
		    s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)482)] + j - 1;
	}
	dlbuff[(i__1 = mioidx + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? 
		i__1 : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)486)] = (
		doublereal) maxdim;
    }
    dlbuff[223] = 0.;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(ZEROSTEP)", ok, (ftnlen)15);

/*     Restore original step value. */

    dlbuff[223] = tbuff[2] + 1;

/* --- Case: ------------------------------------------------------ */

    tcase_("Valid zero step in step size array", (ftnlen)34);

/*     This is a non-error exceptional case. */

    n = 1;
    maxdim = 15;
    j = (maxdim << 2) + 11;
    dlbuff[0] = (doublereal) j;
    mioidx = (maxdim << 2) + 8;
    dlbuff[(i__1 = mioidx - 1) < 1120989 && 0 <= i__1 ? i__1 : s_rnge("dlbuff"
	    , i__1, "f_spk21__", (ftnlen)518)] = 4.;
    dlbuff[(i__1 = mioidx) < 1120989 && 0 <= i__1 ? i__1 : s_rnge("dlbuff", 
	    i__1, "f_spk21__", (ftnlen)519)] = 3.;
    dlbuff[(i__1 = mioidx + 1) < 1120989 && 0 <= i__1 ? i__1 : s_rnge("dlbuff"
	    , i__1, "f_spk21__", (ftnlen)520)] = 2.;
    dlbuff[(i__1 = mioidx + 2) < 1120989 && 0 <= i__1 ? i__1 : s_rnge("dlbuff"
	    , i__1, "f_spk21__", (ftnlen)521)] = 1.;

/*     I is the index of the last non-zero step in the step size */
/*     table. I corresponds to MQ2 (== KQMAX1-2) in SPKE21. */

    d__1 = dlbuff[(i__1 = mioidx - 1) < 1120989 && 0 <= i__1 ? i__1 : s_rnge(
	    "dlbuff", i__1, "f_spk21__", (ftnlen)527)] - 2;
    i__ = i_dnnt(&d__1);
    i__2 = maxdim - i__;
    cleard_(&i__2, &dlbuff[(i__1 = i__ + 1) < 1120989 && 0 <= i__1 ? i__1 : 
	    s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)529)]);
/*      DO I = 1, J */
/*         WRITE (*,*) I, ' ', DLBUFF(I,1) */
/*      END DO */
    spkw21_(&handle, &body, &center, frame, &first, tbuff, segid, &n, &j, 
	    dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case clean-up.", (ftnlen)20);

/*     Delete SPK1. */

    delfil_("spk_test_21_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKR21 normal cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Make sure segment containing one difference line is readable.", (
	    ftnlen)61);

/*     Re-create SPK1. */

    spkopn_("spk_test_21_v1.bsp", "spk_test_21_v1.bsp", &c__0, &handle, (
	    ftnlen)18, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the time and data buffers with values that are */
/*     recognizable but otherwise bogus. */

    maxdim = 25;
    mioidx = (maxdim << 2) + 8;
    for (i__ = 1; i__ <= 10099; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk21__", (ftnlen)583)] = i__ * 1e3;
	for (j = 1; j <= 111; ++j) {
	    dlbuff[(i__1 = j + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? i__1 
		    : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)587)] = 
		    tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? i__2 : 
		    s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)587)] + j - 1;
	}
	dlbuff[(i__1 = mioidx + i__ * 111 - 112) < 1120989 && 0 <= i__1 ? 
		i__1 : s_rnge("dlbuff", i__1, "f_spk21__", (ftnlen)591)] = (
		doublereal) maxdim;
    }
    last = tbuff[0];
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &c__1, &
	    c__111, dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafopr_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__21, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    chcksd_("Start time", dc, "=", &first, &c_b157, ok, (ftnlen)10, (ftnlen)1)
	    ;
    chcksd_("Stop time", &dc[1], "=", &last, &c_b157, ok, (ftnlen)9, (ftnlen)
	    1);

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    spkr21_(&handle, descr, tbuff, t21rec);
    chckad_("Diff line", &t21rec[1], "=", dlbuff, &c__111, &c_b157, ok, (
	    ftnlen)9, (ftnlen)1);
    s_copy(label, "MAXDIM", (ftnlen)80, (ftnlen)6);
    d__1 = (doublereal) maxdim;
    chcksd_(label, t21rec, "=", &d__1, &c_b157, ok, (ftnlen)80, (ftnlen)1);
    spkcls_(&handle);

/* --- Case: ------------------------------------------------------ */


/*     Repeat the test with MXNREC records.  The new segment will mask */
/*     the previous one. */

    tcase_("Create and read segment with multiple difference lines.", (ftnlen)
	    55);
    spkopa_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    n = 10099;
    last = tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk21__", (ftnlen)668)];
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &c__111,
	     dlbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open the file for read access; find the 2nd descriptor. */

    dafopr_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__21, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    chcksd_("Start time", dc, "=", &first, &c_b157, ok, (ftnlen)10, (ftnlen)1)
	    ;
    chcksd_("Stop time", &dc[1], "=", &last, &c_b157, ok, (ftnlen)9, (ftnlen)
	    1);

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Difference line number *, time *", (ftnlen)80, (ftnlen)
		32);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)725)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	spkr21_(&handle, descr, &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ?
		 i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)727)], 
		t21rec);
	chckad_(label, &t21rec[1], "=", &dlbuff[(i__2 = i__ * 111 - 111) < 
		1120989 && 0 <= i__2 ? i__2 : s_rnge("dlbuff", i__2, "f_spk2"
		"1__", (ftnlen)729)], &c__111, &c_b157, ok, (ftnlen)80, (
		ftnlen)1);
	s_copy(label, "MAXDIM for difference line number *, time *", (ftnlen)
		80, (ftnlen)43);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)736)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	d__1 = (doublereal) maxdim;
	chcksd_(label, t21rec, "=", &d__1, &c_b157, ok, (ftnlen)80, (ftnlen)1)
		;
    }
    spkcls_(&handle);

/* --- Case: ------------------------------------------------------ */


/*     Repeat the test with a new segment having a smaller value */
/*     of MAXDIM.  The new segment will mask the previous one. */

    tcase_("Create and read segment with MAXDIM set to 15.", (ftnlen)46);
    spkopa_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    n = 10099;
    last = tbuff[(i__1 = n - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
	    i__1, "f_spk21__", (ftnlen)758)];
    maxdim = 15;
    dlsize = (maxdim << 2) + 11;
    mioidx = (maxdim << 2) + 8;
    for (i__ = 1; i__ <= 10099; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk21__", (ftnlen)768)] = i__ * 1e3;
	i__1 = dlsize;
	for (j = 1; j <= i__1; ++j) {

/*           K is the offset of the (J,I) entry of DPBUFF. */

	    k = (i__ - 1) * dlsize + j;
	    dpbuff[(i__2 = k - 1) < 1120990 && 0 <= i__2 ? i__2 : s_rnge(
		    "dpbuff", i__2, "f_spk21__", (ftnlen)775)] = tbuff[(i__3 =
		     i__ - 1) < 10099 && 0 <= i__3 ? i__3 : s_rnge("tbuff", 
		    i__3, "f_spk21__", (ftnlen)775)] + j - 1;
	}
	k = (i__ - 1) * dlsize + mioidx;
	dpbuff[(i__1 = k - 1) < 1120990 && 0 <= i__1 ? i__1 : s_rnge("dpbuff",
		 i__1, "f_spk21__", (ftnlen)781)] = (doublereal) maxdim;
    }

/*     We need to use a different data buffer here; we'll call it */
/*     DPBUFF. */

    body = 5;
    spkw21_(&handle, &body, &center, frame, &first, &last, segid, &n, &dlsize,
	     dpbuff, tbuff, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open the file for read access; find the 3rd descriptor. */

    dafopr_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	daffna_(&found);
	chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    }
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__21, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    chcksd_("Start time", dc, "=", &first, &c_b157, ok, (ftnlen)10, (ftnlen)1)
	    ;
    chcksd_("Stop time", &dc[1], "=", &last, &c_b157, ok, (ftnlen)9, (ftnlen)
	    1);

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Difference line number *, time *", (ftnlen)80, (ftnlen)
		32);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)840)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	spkr21_(&handle, descr, &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ?
		 i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)842)], 
		t21rec);
	k = (i__ - 1) * dlsize + 1;
	chckad_(label, &t21rec[1], "=", &dpbuff[(i__2 = k - 1) < 1120990 && 0 
		<= i__2 ? i__2 : s_rnge("dpbuff", i__2, "f_spk21__", (ftnlen)
		846)], &dlsize, &c_b157, ok, (ftnlen)80, (ftnlen)1);
	s_copy(label, "MAXDIM for difference line number *, time *", (ftnlen)
		80, (ftnlen)43);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)853)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	d__1 = (doublereal) maxdim;
	chcksd_(label, t21rec, "=", &d__1, &c_b157, ok, (ftnlen)80, (ftnlen)1)
		;
    }
    spkcls_(&handle);
/* ***************************************************************** */
/* * */
/* *    SPKR21/SPKE21/SPKPVN normal cases: */
/* * */
/* ***************************************************************** */

/*     We'll now create a type 21 segment containing some more or */
/*     less realistic data. We'll start states sampled from a */
/*     Jupiter barycenter segment created by TSTSPK. We'll use */
/*     these to create a type 13 record, which we'll then convert */
/*     to a type 21 record. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create realistic data for a type 21 record.", (ftnlen)43);

/*     Start out with a generic SPK file. */

    tstspk_("test.bsp", &c_false, &han0, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a type 13 record from a sequence of */
/*     epochs and states. */

    body = 5;
    center = 10;
    n = 10;
    step = spd_();
    first = 0.;
    last = first + (n - 1) * step;
    s_copy(segid, "Type 21 Jup Barycenter MAXDIM = 21", (ftnlen)80, (ftnlen)
	    34);
    s_copy(frame, "J2000", (ftnlen)32, (ftnlen)5);
    furnsh_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t13rec[0] = (doublereal) n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * step;
	spkez_(&body, &et, frame, "NONE", &center, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 6 + 2;
	moved_(state, &c__6, &t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 
		: s_rnge("t13rec", i__2, "f_spk21__", (ftnlen)924)]);
	j = n * 6 + 1 + i__;
	t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 : s_rnge("t13rec", 
		i__2, "f_spk21__", (ftnlen)928)] = et;
    }
    unload_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a type 21 record from a type 13 record.", (ftnlen)46);
    t_t13xmd__(t13rec, &first, &last, t21rec, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Type 21 record created", &found, &c_true, ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a new SPK from a type 21 record.", (ftnlen)39);
    if (exists_("spk_test_21_v2.bsp", (ftnlen)18)) {
	delfil_("spk_test_21_v2.bsp", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("spk_test_21_v2.bsp", "spk_test_21_v2.bsp", &c__0, &han2, (ftnlen)
	    18, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that our time buffer contains a single element. */

    spkw21_(&han2, &body, &center, frame, &first, &last, segid, &c__1, &
	    c__111, &t21rec[1], &last, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Here's the main event: test the states obtained from the */
/*     type 21 segment. */

    tcase_("Recover states from the type 21 segment.", (ftnlen)40);
    furnsh_("spk_test_21_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * step;
	spkez_(&body, &et, frame, "NONE", &center, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	j = (i__ - 1) * 6 + 2;
	moved_(&t13rec[(i__2 = j - 1) < 198 && 0 <= i__2 ? i__2 : s_rnge(
		"t13rec", i__2, "f_spk21__", (ftnlen)996)], &c__6, xstate);
	s_copy(label, "Position number *, time *", (ftnlen)80, (ftnlen)25);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &et, &c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	chckad_(label, state, "~~/", xstate, &c__3, &c_b360, ok, (ftnlen)80, (
		ftnlen)3);
	s_copy(label, "Velocity number *, time *", (ftnlen)80, (ftnlen)25);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &et, &c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	chckad_(label, &state[3], "~~/", &xstate[3], &c__3, &c_b360, ok, (
		ftnlen)80, (ftnlen)3);
    }
    unload_("spk_test_21_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    SPKS21 normal cases: */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKS21 test: subset small segment.", (ftnlen)34);

/*     SPK2 has realistic MDA data. */

    dafopr_("spk_test_21_v2.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    first = dc[0] + 6400.;
    last = dc[1] - 6400.;
    if (exists_("test21sub.bsp", (ftnlen)13)) {
	delfil_("test21sub.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("test21sub.bsp", " ", &c__0, &hans, (ftnlen)13, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spksub_(&handle, descr, segid, &first, &last, &hans, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK. */

    spkcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close source SPK. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for DAF search. */

    dafopr_("test21sub.bsp", &hans, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = ic[0];
    center = ic[1];

/*     Close subsetted SPK and re-open for sampling. */

    dafcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("test21sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected states from source SPK. */

    nsamp = 100001;
    delta = (last - first) / (nsamp - 1);
    furnsh_("spk_test_21_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 21 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &xstbuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("xstbuf", i__2, 
		"f_spk21__", (ftnlen)1136)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("spk_test_21_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Re-open subsetted SPK for sampling. */

    furnsh_("test21sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Sample and check states from the new type 21 SPK file. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	et = first + (i__ - 1) * delta;
	spkez_(&body, &et, frame, "NONE", &center, &stabuf[(i__2 = i__ * 6 - 
		6) < 600006 && 0 <= i__2 ? i__2 : s_rnge("stabuf", i__2, 
		"f_spk21__", (ftnlen)1157)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("test21sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Test position results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Pos(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 6) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk21__", (ftnlen)1175)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 6) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk21__", (ftnlen)1175)], &
		c__3, &c_b454, ok, (ftnlen)80, (ftnlen)3);
    }

/*     Test velocity results. */

    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "Vel(@)", (ftnlen)80, (ftnlen)6);
	repmi_(label, "@", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, &stabuf[(i__2 = i__ * 6 - 3) < 600006 && 0 <= i__2 ? 
		i__2 : s_rnge("stabuf", i__2, "f_spk21__", (ftnlen)1189)], 
		"~~/", &xstbuf[(i__3 = i__ * 6 - 3) < 600006 && 0 <= i__3 ? 
		i__3 : s_rnge("xstbuf", i__3, "f_spk21__", (ftnlen)1189)], &
		c__3, &c_b454, ok, (ftnlen)80, (ftnlen)3);
    }
    unload_("test21sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKS21 test: subset large segment.", (ftnlen)34);

/*     We'll use an input segment that has unrealistic data */
/*     but a large number of records. */

    dafopr_("spk_test_21_v1.bsp", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Subset the third segment. */

    for (i__ = 1; i__ <= 3; ++i__) {
	daffna_(&found);
	chcksl_("Segment found", &found, &c_true, ok, (ftnlen)13);
    }
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = ic[0];
    center = ic[1];
    frcode = ic[2];

/*     This time chop off 10 days at both ends of the segment. */

    first = dc[0] + spd_() * 10;
    last = dc[1] - spd_() * 10;
    if (exists_("test21sub.bsp", (ftnlen)13)) {
	delfil_("test21sub.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    spkopn_("test21sub.bsp", " ", &c__0, &hans, (ftnlen)13, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spksub_(&handle, descr, segid, &first, &last, &hans, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close subsetted SPK. */

    spkcls_(&hans);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close source SPK. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open the subsetted SPK. */

    dafopr_("test21sub.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafus_(descr, &c__2, &c__6, dc, ic);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the descriptor. */

    namfrm_(frame, &frcode, (ftnlen)32);
    chcksi_("Body", ic, "=", &body, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("Center", &ic[1], "=", &center, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("Frame", &ic[2], "=", &frcode, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("Data type", &ic[3], "=", &c__21, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    chcksd_("Start time", dc, "=", &first, &c_b157, ok, (ftnlen)10, (ftnlen)1)
	    ;
    chcksd_("Stop time", &dc[1], "=", &last, &c_b157, ok, (ftnlen)9, (ftnlen)
	    1);

/*     Look up the data and compare it to what we put in.  We */
/*     expect an exact match. */

    maxdim = 15;
    dlsize = (maxdim << 2) + 11;
    mioidx = (maxdim << 2) + 8;

/*     Re-create the test data for the third segment of SPK1. */

    for (i__ = 1; i__ <= 10099; ++i__) {
	tbuff[(i__1 = i__ - 1) < 10099 && 0 <= i__1 ? i__1 : s_rnge("tbuff", 
		i__1, "f_spk21__", (ftnlen)1309)] = i__ * 1e3;
	i__1 = dlsize;
	for (j = 1; j <= i__1; ++j) {

/*           K is the offset of the (J,I) entry of DPBUFF. */

	    k = (i__ - 1) * dlsize + j;
	    dpbuff[(i__2 = k - 1) < 1120990 && 0 <= i__2 ? i__2 : s_rnge(
		    "dpbuff", i__2, "f_spk21__", (ftnlen)1316)] = tbuff[(i__3 
		    = i__ - 1) < 10099 && 0 <= i__3 ? i__3 : s_rnge("tbuff", 
		    i__3, "f_spk21__", (ftnlen)1316)] + j - 1;
	}
	k = (i__ - 1) * dlsize + mioidx;
	dpbuff[(i__1 = k - 1) < 1120990 && 0 <= i__1 ? i__1 : s_rnge("dpbuff",
		 i__1, "f_spk21__", (ftnlen)1322)] = (doublereal) maxdim;
    }
    d__1 = first / 1000;
    j = i_dnnt(&d__1);
    d__1 = last / 1000;
    k = i_dnnt(&d__1);
    i__1 = k;
    for (i__ = j; i__ <= i__1; ++i__) {
	s_copy(label, "Difference line number *, time *", (ftnlen)80, (ftnlen)
		32);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)1335)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	spkr21_(&handle, descr, &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ?
		 i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)1337)], 
		t21rec);
	k = (i__ - 1) * dlsize + 1;
	chckad_(label, &t21rec[1], "=", &dpbuff[(i__2 = k - 1) < 1120990 && 0 
		<= i__2 ? i__2 : s_rnge("dpbuff", i__2, "f_spk21__", (ftnlen)
		1341)], &dlsize, &c_b157, ok, (ftnlen)80, (ftnlen)1);
	s_copy(label, "MAXDIM for difference line number *, time *", (ftnlen)
		80, (ftnlen)43);
	repmi_(label, "*", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(label, "*", &tbuff[(i__2 = i__ - 1) < 10099 && 0 <= i__2 ? 
		i__2 : s_rnge("tbuff", i__2, "f_spk21__", (ftnlen)1348)], &
		c__14, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	d__1 = (doublereal) maxdim;
	chcksd_(label, t21rec, "=", &d__1, &c_b157, ok, (ftnlen)80, (ftnlen)1)
		;
    }
    spkcls_(&handle);

/* --- Case: ------------------------------------------------------ */


/*     Close and delete the SPK files. */

    tcase_("Clean up.", (ftnlen)9);
    dafcls_(&han0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_21_v1.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafcls_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("spk_test_21_v2.bsp", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test21sub.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_spk21__ */

