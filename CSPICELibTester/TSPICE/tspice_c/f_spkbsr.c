/* f_spkbsr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;
static integer c__4 = 4;
static doublereal c_b31 = 0.;
static integer c__2 = 2;

/* $Procedure  F_SPKBSR ( Family of tests for T_SSFS ) */
/* Subroutine */ int f_spkbsr__(logical *ok)
{
    /* Initialized data */

    static char spks[255*10] = "sfs1.bsp                                    "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                   " "sfs2.bsp                                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                     " "sfs3.bsp                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                       " "sfs4.bsp                              "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                         " "sfs5.bsp                            "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                           " "sfs6.bsp                          "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                             " "sfs7.bsp                        "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                               " "sfs8.bsp                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                 " "sfs9.bsp                    "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                   " "sfs10.bsp                 "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                     ";
    static integer nseg[10] = { 1,500,500,1000,1010,100,23,23,4000,3988 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer body;
    extern /* Subroutine */ int t_crdesc__(char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen);
    static char smsg[25];
    static integer i__, j;
    static doublereal t;
    static char segid[40];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5], tbegs[16000];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static integer segno;
    static doublereal tends[16000];
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static integer spkno;
    extern /* Subroutine */ int t_success__(logical *);
    static integer handle;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), delfil_(char *, 
	    ftnlen), chckxc_(logical *, char *, logical *, ftnlen), chcksi_(
	    char *, integer *, char *, integer *, integer *, logical *, 
	    ftnlen, ftnlen), t_chds__(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    t_slef__(char *, integer *, ftnlen);
    static integer hndles[10], cpyhan[500];
    static char xsegid[40*16000];
    extern /* Subroutine */ int t_suef__(integer *);
    static doublereal xdescr[80000]	/* was [5][16000] */;
    extern /* Subroutine */ int sigerr_(char *, ftnlen), t_sbsr__(char *, 
	    integer *, integer *, doublereal *, doublereal *, char *, logical 
	    *, ftnlen, ftnlen), t_ssfs__(integer *, doublereal *, integer *, 
	    doublereal *, char *, logical *, ftnlen);
    static char spkcpy[255*500];
    extern logical return_(void);
    static integer ids[16000];
    extern /* Subroutine */ int t_crdaf__(char *, char *, integer *, integer *
	    , doublereal *, doublereal *, char *, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPK segment selection and buffering system, */
/*     which is implemented by T_SSFS. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     None. */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*    N. J. Bachman  (JPL) */

/* $ Version */

/* -   TSPICE Version 2.0.0, 17-MAR-2014 (NJB) */

/*       Table size parameters */

/*          FTSIZE */
/*          ITSIZE */
/*          STSIZE */

/*       were modified for compatibility with */

/*          T_SPKBSR */

/* -   TSPICE Version 1.1.0, 13-DEC-2011 (EDW) */

/*       All variables to SAVE for f2c'd version compiled with Borland. */
/*       Implemented a proper SPICE header. */

/* -   TSPICE Version 1.0.0, 28-NOV-2001 (NJB) */

/* -& */
/* $ Index_Entries */

/*   Test SPK segment buffering system */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     The number of segments in the respective SPK files: */


/*     Other parameters: */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_SPKBSR", (ftnlen)8);
    tcase_("The first SPK file contains 1 segment for body 1. Make sure we c"
	    "an look up data from this file.", (ftnlen)95);

/*     Create the first SPK file. */

    body = 1;
    tbegs[0] = 1e4;
    tends[0] = 10001.;
    spkno = 1;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, (
	    ftnlen)40);
    repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_crdaf__("SPK", spks, nseg, &body, tbegs, tends, xsegid, (ftnlen)3, (
	    ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", hndles, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1,
	     (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &c__1, &body, tbegs, tends, xdescr, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Try to look up data for a different body in SPK 1.  Also look up"
	    " data for body 1 for a time which is not covered.", (ftnlen)113);
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&c__2, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should not be found. */

    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    d__1 = tbegs[0] + 10;
    t_ssfs__(&c__1, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should not be found. */

    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    tcase_("Create a second SPK containing data for body 1 and body 2.  Load"
	    " this SPK, then look up a state covered by the new file.", (
	    ftnlen)120);
    body = 1;
    spkno = 2;
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)359)];
    for (i__ = 1; i__ <= i__2; ++i__) {
	if (i__ <= nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"nseg", i__1, "f_spkbsr__", (ftnlen)361)] / 2) {
	    ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", 
		    i__1, "f_spkbsr__", (ftnlen)362)] = 2;
	} else {
	    ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", 
		    i__1, "f_spkbsr__", (ftnlen)364)] = 1;
	}
	tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
		i__1, "f_spkbsr__", (ftnlen)367)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", 
		i__1, "f_spkbsr__", (ftnlen)368)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)368)] + 1;
	s_copy(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)370)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)372)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)372)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)372)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)373)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)373)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : 
	    s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)379)) * 255, &nseg[(
	    i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", i__1, 
	    "f_spkbsr__", (ftnlen)379)], ids, tbegs, tends, xsegid, (ftnlen)3,
	     (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "spks", i__2, "f_spkbsr__", (ftnlen)384)) * 255, &hndles[(i__1 = 
	    spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("hndles", i__1, 
	    "f_spkbsr__", (ftnlen)384)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)387)];
    d__1 = tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tbegs", i__2, "f_spkbsr__", (ftnlen)389)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 <= 
	    i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)398)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 0 <= 
	    i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)399)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 0 <=
	     i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)405)], 
	    &tends[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tends", i__1, "f_spkbsr__", (ftnlen)405)], &xdescr[(i__3 = segno 
	    * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", i__3, 
	    "f_spkbsr__", (ftnlen)405)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 && 0 
	    <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (ftnlen)409)
	    ], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Look up data for body 2.  This should cause an OLD FILES search.",
	     (ftnlen)64);
    body = 2;
    spkno = 2;
    segno = 1;
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 <= 
	    i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)431)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 0 <= 
	    i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)432)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 0 <=
	     i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)438)], 
	    &tends[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tends", i__1, "f_spkbsr__", (ftnlen)438)], &xdescr[(i__3 = segno 
	    * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", i__3, 
	    "f_spkbsr__", (ftnlen)438)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 && 0 
	    <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (ftnlen)442)
	    ], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Create a third SPK containing data for body 3. Load this SPK, th"
	    "en look up a state covered by the new file. This should cause th"
	    "e segment list for body 1 to get dumped.", (ftnlen)168);
    body = 3;
    spkno = 3;
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)455)];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2,
		 "f_spkbsr__", (ftnlen)457)] = body;
	tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
		i__2, "f_spkbsr__", (ftnlen)459)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tends", 
		i__2, "f_spkbsr__", (ftnlen)460)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)460)] + 1;
	s_copy(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)462)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)464)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)464)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)464)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)465)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)465)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)471)) * 255, &nseg[(
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", i__2, 
	    "f_spkbsr__", (ftnlen)471)], ids, tbegs, tends, xsegid, (ftnlen)3,
	     (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)476)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)476)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)479)];
    d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tbegs", i__1, "f_spkbsr__", (ftnlen)481)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)490)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)491)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)497)], 
	    &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)497)], &xdescr[(i__3 = segno 
	    * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", i__3, 
	    "f_spkbsr__", (ftnlen)497)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)501)
	    ], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Create another SPK for body 1 and load it. The segment count in "
	    "this file is such that all other body lists must be dumped to ma"
	    "ke room. Then make a request that is satisfied by SPK 1. The seg"
	    "ment in SPK 1 cannot be added to the segment table.", (ftnlen)243)
	    ;
    body = 1;
    spkno = 4;
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)519)];
    for (i__ = 1; i__ <= i__2; ++i__) {
	ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", i__1,
		 "f_spkbsr__", (ftnlen)521)] = body;
	tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
		i__1, "f_spkbsr__", (ftnlen)523)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", 
		i__1, "f_spkbsr__", (ftnlen)524)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)524)] + 1;
	s_copy(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)526)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)528)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)528)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)528)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)529)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)529)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : 
	    s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)534)) * 255, &nseg[(
	    i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", i__1, 
	    "f_spkbsr__", (ftnlen)534)], ids, tbegs, tends, xsegid, (ftnlen)3,
	     (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "spks", i__2, "f_spkbsr__", (ftnlen)539)) * 255, &hndles[(i__1 = 
	    spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("hndles", i__1, 
	    "f_spkbsr__", (ftnlen)539)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkno = 1;
    segno = 1;
    tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
	    i__2, "f_spkbsr__", (ftnlen)545)] = (doublereal) (spkno * 10000 + 
	    segno - 1);
    tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tends", 
	    i__2, "f_spkbsr__", (ftnlen)546)] = (doublereal) (spkno * 10000 + 
	    segno);
    d__1 = tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tbegs", i__2, "f_spkbsr__", (ftnlen)548)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    s_copy(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)557)) * 40, "File: # Segno"
	    ": #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)558)) * 40, "#", spks + ((
	    i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("spks", i__1, 
	    "f_spkbsr__", (ftnlen)558)) * 255, xsegid + ((i__3 = segno - 1) < 
	    16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (
	    ftnlen)558)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40)
	    ;
    repmi_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)559)) * 40, "#", &c__1, 
	    xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)559)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 <= 
	    i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)563)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 0 <= 
	    i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)564)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 0 <=
	     i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)570)], 
	    &tends[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tends", i__1, "f_spkbsr__", (ftnlen)570)], &xdescr[(i__3 = segno 
	    * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", i__3, 
	    "f_spkbsr__", (ftnlen)570)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 && 0 
	    <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (ftnlen)574)
	    ], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Start a segment list for body 1 by making a request that is sati"
	    "sfied by SPK 1.  Then build a file (SPK 5) with too many segment"
	    "s for body 1 to be buffered.  Make a request that is satisfied b"
	    "y SPK 5. This tests the logic for searching the subset of a segm"
	    "ent list that must be dumped due to lack of room.", (ftnlen)305);

/*     Set up by making a request that will be satisfied by the segment */
/*     in SPK 1.  This builds up the segment list for body 1. */

    body = 1;
    tbegs[0] = 1e4;
    tends[0] = 10001.;
    spkno = 1;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, (
	    ftnlen)40);
    repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Go ahead and make the new file. */

    body = 1;
    spkno = 5;
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)617)];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2,
		 "f_spkbsr__", (ftnlen)619)] = body;
	if (i__ == 10 || i__ == 1001) {

/*           We want the lower bound of the re-use interval to */
/*           match the right endpoint of the segment's coverage */
/*           interval. */

	    tbegs[(i__2 = i__ - 2) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)627)] = (doublereal) (
		    spkno * 10000 + i__);
	    tends[(i__2 = i__ - 2) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)628)] = tbegs[(i__3 = 
		    i__ - 2) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)628)] + 1.;
	    tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)630)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)631)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)631)] + 1;
	    tbegs[(i__2 = i__) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
		    i__2, "f_spkbsr__", (ftnlen)633)] = tbegs[(i__3 = i__ - 1)
		     < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, 
		    "f_spkbsr__", (ftnlen)633)];
	    tends[(i__2 = i__) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tends", 
		    i__2, "f_spkbsr__", (ftnlen)634)] = tends[(i__3 = i__ - 1)
		     < 16000 && 0 <= i__3 ? i__3 : s_rnge("tends", i__3, 
		    "f_spkbsr__", (ftnlen)634)];
	    tbegs[(i__2 = i__ + 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)636)] = tends[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tends", 
		    i__3, "f_spkbsr__", (ftnlen)636)] + 1;
	    tends[(i__2 = i__ + 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)637)] = tbegs[(i__3 = 
		    i__ + 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)637)] + 1;
	} else if (i__ == 1006) {

/*           Create a singleton segment. */

	    tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)644)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)645)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)645)];
	} else if (i__ == 1007) {

/*           Create an invisible segment. */

	    tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)651)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)652)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)652)] - 1;
	} else if (i__ < 9 || i__ > 12 && i__ < 1000 || i__ > 1003) {
	    tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbe"
		    "gs", i__2, "f_spkbsr__", (ftnlen)658)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
		    "ds", i__2, "f_spkbsr__", (ftnlen)659)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)659)] + 1;
	}
	s_copy(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)663)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)665)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)665)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)665)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)666)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)666)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)671)) * 255, &nseg[(
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", i__2, 
	    "f_spkbsr__", (ftnlen)671)], ids, tbegs, tends, xsegid, (ftnlen)3,
	     (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)676)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)676)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = 1;
    d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tbegs", i__1, "f_spkbsr__", (ftnlen)681)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)690)) * 40, "File: # Segno"
	    ": #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)691)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)691)) * 255, xsegid + ((i__3 = segno - 1) < 
	    16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (
	    ftnlen)691)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40)
	    ;
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)692)) * 40, "#", &c__1, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)692)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)696)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)697)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)703)], 
	    &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)703)], &xdescr[(i__3 = segno 
	    * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", i__3, 
	    "f_spkbsr__", (ftnlen)703)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)707)
	    ], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Create an SPK containing data for BTSIZE new bodies. Look up dat"
	    "a for each.", (ftnlen)75);

/*     Unload all SPKs. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)723)]);
    }
    spkno = 6;
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)728)];
    for (i__ = 1; i__ <= i__2; ++i__) {
	ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", i__1,
		 "f_spkbsr__", (ftnlen)730)] = i__ + 100;
	tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
		i__1, "f_spkbsr__", (ftnlen)732)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", 
		i__1, "f_spkbsr__", (ftnlen)733)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)733)] + 1;
	s_copy(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)735)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)737)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)737)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)737)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)738)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)738)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : 
	    s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)743)) * 255, &nseg[(
	    i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", i__1, 
	    "f_spkbsr__", (ftnlen)743)], ids, tbegs, tends, xsegid, (ftnlen)3,
	     (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "spks", i__2, "f_spkbsr__", (ftnlen)748)) * 255, &hndles[(i__1 = 
	    spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("hndles", i__1, 
	    "f_spkbsr__", (ftnlen)748)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)752)];
    for (i__ = 1; i__ <= i__1; ++i__) {
	body = ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
		"ids", i__2, "f_spkbsr__", (ftnlen)754)];
	segno = i__;
	d__1 = tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
		"tbegs", i__2, "f_spkbsr__", (ftnlen)757)] + .5f;
	t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        In this case, the segment should be found.  Make sure we get */
/*        back the right handle and segment identifier. */

	s_copy(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)765)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)766)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)766)) * 255, 
		xsegid + ((i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)766)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)767)) * 40, 
		"#", &segno, xsegid + ((i__3 = segno - 1) < 16000 && 0 <= 
		i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)
		767)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 
		<= i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)
		771)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (
		ftnlen)772)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (
		ftnlen)40);

/*        Check the descriptor as well.  However, don't check the */
/*        segment addresses. */

	t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (
		ftnlen)778)], &tends[(i__3 = segno - 1) < 16000 && 0 <= i__3 ?
		 i__3 : s_rnge("tends", i__3, "f_spkbsr__", (ftnlen)778)], &
		xdescr[(i__4 = segno * 5 - 5) < 80000 && 0 <= i__4 ? i__4 : 
		s_rnge("xdescr", i__4, "f_spkbsr__", (ftnlen)778)], (ftnlen)3)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 
		&& 0 <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (
		ftnlen)782)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    }
    tcase_("The body table should be full now; the segment table should have"
	    " room.  Cause a body list to be dumped to make room in the body "
	    "table.", (ftnlen)134);

/*     Create a list for body 1 more expensive than those for the */
/*     bodies in SPK 6.  Body 1's list will be placed at the head of */
/*     the body table. */

    body = 1;
    spkno = 2;
    segno = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)805)];
    i__ = segno;
    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)807)] = (doublereal) (spkno * 10000 + i__ 
	    - 1);
    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)808)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    808)] + 1;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)811)) * 255, xsegid, (
	    ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
    repmi_(xsegid, "#", &segno, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + 255, &hndles[1], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs"
	    , i__1, "f_spkbsr__", (ftnlen)819)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)828)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1,
	     (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = i__ - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)835)], &
	    tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ten"
	    "ds", i__2, "f_spkbsr__", (ftnlen)835)], xdescr, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Now do a look up for body 2.  This will require dumping lists */
/*     from SPK 6. */

    body = 2;
    spkno = 2;
    segno = 1;
    i__ = segno;
    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)850)] = (doublereal) (spkno * 10000 + i__ 
	    - 1);
    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)851)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    851)] + 1;
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)855)) * 40, "File: # Segno"
	    ": #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)856)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)856)) * 255, xsegid + ((i__3 = segno - 1) < 
	    16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (
	    ftnlen)856)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40)
	    ;
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)857)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)857)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs"
	    , i__1, "f_spkbsr__", (ftnlen)860)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)869)], 
	    &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)870)) *
	     40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &c__1, &body, tbegs, tends, xdescr, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Look up data from a representative subset of the segments in SPK"
	    " 5.", (ftnlen)67);
    spkno = 5;
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)892)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)892)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)896)];
    for (i__ = 1; i__ <= i__2; ++i__) {
	ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", i__1,
		 "f_spkbsr__", (ftnlen)898)] = body;
	if (i__ == 10 || i__ == 1001) {

/*           We want the lower bound of the re-use interval to */
/*           match the right endpoint of the segment's coverage */
/*           interval. */

	    tbegs[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)906)] = (doublereal) (
		    spkno * 10000 + i__);
	    tends[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)907)] = tbegs[(i__3 = 
		    i__ - 2) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)907)] + 1.;
	    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)909)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)910)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)910)] + 1;
	    tbegs[(i__1 = i__) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
		    i__1, "f_spkbsr__", (ftnlen)912)] = tbegs[(i__3 = i__ - 1)
		     < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, 
		    "f_spkbsr__", (ftnlen)912)];
	    tends[(i__1 = i__) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", 
		    i__1, "f_spkbsr__", (ftnlen)913)] = tends[(i__3 = i__ - 1)
		     < 16000 && 0 <= i__3 ? i__3 : s_rnge("tends", i__3, 
		    "f_spkbsr__", (ftnlen)913)];
	    tbegs[(i__1 = i__ + 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)915)] = tends[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tends", 
		    i__3, "f_spkbsr__", (ftnlen)915)] + 1;
	    tends[(i__1 = i__ + 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)916)] = tbegs[(i__3 = 
		    i__ + 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)916)] + 1;
	} else if (i__ == 1006) {

/*           Create a singleton segment. */

	    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)922)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)923)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)923)];
	} else if (i__ == 1007) {

/*           Create an invisible segment. */

	    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)929)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)930)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)930)] - 1;
	} else if (i__ < 10 || i__ > 12 && i__ < 1000 || i__ > 1003) {
	    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbe"
		    "gs", i__1, "f_spkbsr__", (ftnlen)936)] = (doublereal) (
		    spkno * 10000 + i__ - 1);
	    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ten"
		    "ds", i__1, "f_spkbsr__", (ftnlen)937)] = tbegs[(i__3 = 
		    i__ - 1) < 16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", 
		    i__3, "f_spkbsr__", (ftnlen)937)] + 1;
	}
	s_copy(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)941)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)943)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)943)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)943)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)944)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)944)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__ = 1;
    while(i__ <= nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "nseg", i__2, "f_spkbsr__", (ftnlen)952)]) {
	body = 1;
	segno = i__;
	d__1 = tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
		"tbegs", i__2, "f_spkbsr__", (ftnlen)957)] + .5f;
	t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        In this case, the segment should be found.  Make sure we get */
/*        back the right handle and segment identifier. */

	s_copy(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)965)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)966)) * 40, 
		"#", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
		s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)966)) * 255, 
		xsegid + ((i__3 = segno - 1) < 16000 && 0 <= i__3 ? i__3 : 
		s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)966)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)967)) * 40, 
		"#", &segno, xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
		i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)
		967)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 
		<= i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)
		971)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (
		ftnlen)972)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (
		ftnlen)40);

/*        Check the descriptor as well.  However, don't check the */
/*        segment addresses. */

	t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (
		ftnlen)978)], &tends[(i__1 = segno - 1) < 16000 && 0 <= i__1 ?
		 i__1 : s_rnge("tends", i__1, "f_spkbsr__", (ftnlen)978)], &
		xdescr[(i__3 = segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : 
		s_rnge("xdescr", i__3, "f_spkbsr__", (ftnlen)978)], (ftnlen)3)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 
		&& 0 <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (
		ftnlen)982)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);

/*        Skip some tests that are unlikely to reveal bugs, as well as */
/*        those which would give anomalous results due to the structure */
/*        of SPK 5. */

	if (i__ == 3) {
	    i__ = 498;
	} else if (i__ == 503) {
	    i__ = 998;
	} else if (i__ == 1000) {
	    i__ = 1005;
	} else if (i__ == 1005) {
	    i__ = 1008;
	} else {
	    ++i__;
	}
    }

/*     Try a search w/o buffering case where no segment is found. */

    tcase_("Search w/o buffering, no segment should be found.", (ftnlen)49);
    spkno = 5;
    body = 1;
    t = tends[(i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : 
	    s_rnge("nseg", i__2, "f_spkbsr__", (ftnlen)1011)] - 1) < 16000 && 
	    0 <= i__1 ? i__1 : s_rnge("tends", i__1, "f_spkbsr__", (ftnlen)
	    1011)] * 2;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Return on entry in RETURN mode, if the error status is set. */

    tcase_("Make sure T_SSFS returns on entry when RETURN()is .TRUE.", (
	    ftnlen)56);
    s_copy(smsg, "Return on entry", (ftnlen)25, (ftnlen)15);
    sigerr_(smsg, (ftnlen)25);
    t_ssfs__(&c__1, &c_b31, &handle, descr, segid, &found, (ftnlen)40);

/*     Depending on whether we're calling a version of T_SBSR that does */
/*     coverage checking, the error status may be reset. */

    if (return_()) {
	chckxc_(&c_true, smsg, ok, (ftnlen)25);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Unload the SPK files. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		"hndles", i__2, "f_spkbsr__", (ftnlen)1045)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Make sure an error is signaled if no SPKs are loaded. */

    tcase_("Make sure an error is signaled if no SPKs are loaded.", (ftnlen)
	    53);
    t_ssfs__(&c__1, &c_b31, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOLOADEDFILES)", ok, (ftnlen)20);

/*     Load SPK1 and look up a state from it to create a cheap list. */
/*     Make the cheap list the second list by looking up data from */
/*     it after looking up data for body BTSIZE+1. */

    tcase_("Test removal of cheap list when adding a new body; cheap list is"
	    " 2nd.", (ftnlen)69);
    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now load the SPK containing 100 bodies.  Look up data for */
/*     each one.  The last one will cause the list for body 1 to */
/*     be dumped. */

    spkno = 6;
    t_slef__(spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "spks", i__2, "f_spkbsr__", (ftnlen)1076)) * 255, &hndles[(i__1 = 
	    spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("hndles", i__1, 
	    "f_spkbsr__", (ftnlen)1076)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)1079)];
    for (i__ = 1; i__ <= i__1; ++i__) {
	ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2,
		 "f_spkbsr__", (ftnlen)1081)] = i__ + 100;
	tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
		i__2, "f_spkbsr__", (ftnlen)1083)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tends", 
		i__2, "f_spkbsr__", (ftnlen)1084)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)1084)] + 1;
	s_copy(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1086)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1088)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)1088)) * 255, 
		xsegid + ((i__4 = i__ - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)1088)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1089)) * 40, 
		"#", &i__, xsegid + ((i__3 = i__ - 1) < 16000 && 0 <= i__3 ? 
		i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)1089)) * 
		40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)1095)];
    for (i__ = 1; i__ <= i__2; ++i__) {
	body = ids[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
		"ids", i__1, "f_spkbsr__", (ftnlen)1097)];
	segno = i__;
	d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
		"tbegs", i__1, "f_spkbsr__", (ftnlen)1100)] + .5f;
	t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        In this case, the segment should be found.  Make sure we get */
/*        back the right handle and segment identifier. */

	s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1108)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1109)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)1109)) * 255, 
		xsegid + ((i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)1109)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1110)) * 40, 
		"#", &segno, xsegid + ((i__3 = segno - 1) < 16000 && 0 <= 
		i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)
		1110)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1114)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 
		0 <= i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (
		ftnlen)1115)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (
		ftnlen)40);

/*        Check the descriptor as well.  However, don't check the */
/*        segment addresses. */

	t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 
		0 <= i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (
		ftnlen)1121)], &tends[(i__3 = segno - 1) < 16000 && 0 <= i__3 
		? i__3 : s_rnge("tends", i__3, "f_spkbsr__", (ftnlen)1121)], &
		xdescr[(i__4 = segno * 5 - 5) < 80000 && 0 <= i__4 ? i__4 : 
		s_rnge("xdescr", i__4, "f_spkbsr__", (ftnlen)1121)], (ftnlen)
		3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 
		&& 0 <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (
		ftnlen)1125)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
	if (i__ == 1) {

/*           Create a cheap list for body 1. */

	    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
		    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)1132)) * 255, &
		    hndles[(i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		    s_rnge("hndles", i__3, "f_spkbsr__", (ftnlen)1132)], (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tbegs[0] = 1e4;
	    d__1 = tbegs[0] + .5f;
	    t_ssfs__(&c__1, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    tcase_("Test ability to make room by deleting a body table entry with an"
	    " empty list.", (ftnlen)76);

/*     Create an example of the list in question by forcing a search */
/*     without buffering on body 1, where the highest priority file */
/*     contains too many segments to buffer.  However, we want this */
/*     list to have a high expense, so load an SPK with many segments */
/*     for this body and search it first. */

    spkno = 5;
    t_slef__(spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
	    "spks", i__2, "f_spkbsr__", (ftnlen)1156)) * 255, &hndles[(i__1 = 
	    spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("hndles", i__1, 
	    "f_spkbsr__", (ftnlen)1156)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 1;
    t = spkno * 10000 + 1000 + .5;
    t_ssfs__(&c__1, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Now look up data for the first NSEG-1 bodies in SPK 6.  This */
/*     should fill up the body table. */

    spkno = 6;
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)1173)] - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2,
		 "f_spkbsr__", (ftnlen)1175)] = i__ + 100;
	tbegs[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
		i__2, "f_spkbsr__", (ftnlen)1177)] = (doublereal) (spkno * 
		10000 + i__ - 1);
	tends[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tends", 
		i__2, "f_spkbsr__", (ftnlen)1178)] = tbegs[(i__3 = i__ - 1) < 
		16000 && 0 <= i__3 ? i__3 : s_rnge("tbegs", i__3, "f_spkbsr__"
		, (ftnlen)1178)] + 1;
	body = ids[(i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
		"ids", i__2, "f_spkbsr__", (ftnlen)1180)];
	segno = i__;
	d__1 = tbegs[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
		"tbegs", i__2, "f_spkbsr__", (ftnlen)1183)] + .5f;
	t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        In this case, the segment should be found.  Make sure we get */
/*        back the right handle and segment identifier. */

	s_copy(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1191)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1192)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)1192)) * 255, 
		xsegid + ((i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)1192)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1193)) * 40, 
		"#", &segno, xsegid + ((i__3 = segno - 1) < 16000 && 0 <= 
		i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)
		1193)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("HANDLE", &handle, "=", &hndles[(i__2 = spkno - 1) < 10 && 0 
		<= i__2 ? i__2 : s_rnge("hndles", i__2, "f_spkbsr__", (ftnlen)
		1197)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + ((i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (
		ftnlen)1198)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (
		ftnlen)40);

/*        Check the descriptor as well.  However, don't check the */
/*        segment addresses. */

	t_crdesc__("SPK", &segno, &body, &tbegs[(i__2 = segno - 1) < 16000 && 
		0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (
		ftnlen)1204)], &tends[(i__3 = segno - 1) < 16000 && 0 <= i__3 
		? i__3 : s_rnge("tends", i__3, "f_spkbsr__", (ftnlen)1204)], &
		xdescr[(i__4 = segno * 5 - 5) < 80000 && 0 <= i__4 ? i__4 : 
		s_rnge("xdescr", i__4, "f_spkbsr__", (ftnlen)1204)], (ftnlen)
		3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[(i__2 = segno * 5 - 5) < 80000 
		&& 0 <= i__2 ? i__2 : s_rnge("xdescr", i__2, "f_spkbsr__", (
		ftnlen)1208)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Try some cases where the re-use interval matches the selected */
/*     segment's coverage interval. */

    tcase_("Search w/o buffering case, selected segment is in dumped list, c"
	    "overage interval matches re-use interval, request time is in cen"
	    "ter of re-use interval.", (ftnlen)151);

/*     Set up the case by unloading the currently loaded SPKs.  Load */
/*     SPK 1 and look up a state from it. */


/*     Unload the SPK files. */

    for (i__ = 1; i__ <= 9; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)1232)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Load SPK 1 and look up a state from this file. */

    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 1;
    tbegs[0] = 1e4;
    tends[0] = 10001.;
    spkno = 1;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, (
	    ftnlen)40);
    repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now load SPK 5.  Look up a state from segment 9, where the */
/*     request time is to the right of a segment whose right endpoint */
/*     is at the left endpoint of the re-use interval. */

    t_slef__(spks + 1020, &hndles[4], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkno = 5;
    body = 1;
    segno = 9;
    tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
	    i__1, "f_spkbsr__", (ftnlen)1269)] = (doublereal) (spkno * 10000 
	    + segno + 1);
    tends[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", 
	    i__1, "f_spkbsr__", (ftnlen)1270)] = tbegs[(i__2 = segno - 1) < 
	    16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (
	    ftnlen)1270)] + 1;
    t = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs",
	     i__1, "f_spkbsr__", (ftnlen)1272)] + .25;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, segment 9 should match. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    segno = 9;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks + 1020, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)
	    255, (ftnlen)40);
    repmi_(xsegid, "#", &segno, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)1289)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1,
	     (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)1296)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)1296)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)1296)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    1300)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);

/*     Create a situation where the segment list for body 1 contributed */
/*     by SPK 5 gets dumped, and where the request is satisfied by */
/*     a segment in SPK 1. */

    tcase_("Dump segment list from SPK 5; find segment for body 1 in SPK 1.", 
	    (ftnlen)63);
    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + 1020, &hndles[4], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 1;
    tbegs[0] = 1e4;
    tends[0] = 10001.;
    t = (tbegs[0] + tends[0]) * .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check handle, segment descriptor and ID. */

    chcksi_("HANDLE", &handle, "=", hndles, &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_crdesc__("SPK", &c__1, &body, tbegs, tends, xdescr, (ftnlen)3);
    t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, (
	    ftnlen)40);
    repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1,
	     (ftnlen)40);
    tcase_("Dump segment list from SPK 5.  While searching list for segment "
	    "for body 1, make lower bound of re-use interval match lower boun"
	    "d of segment descriptor.", (ftnlen)152);

/*     Make SPK 1 higher priority than SPK 5. */

    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Place request time in the "hole" between segments STSIZE+1 and */
/*     STSIZE+3. */

    i__ = 1001;
    tbegs[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)1365)] = (doublereal) (spkno * 10000 + i__)
	    ;
    tends[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)1366)] = tbegs[(i__2 = i__ - 2) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    1366)] + 1.;
    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)1368)] = (doublereal) (spkno * 10000 + i__ 
	    - 1);
    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)1369)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    1369)] + 1;
    tbegs[(i__1 = i__) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1, 
	    "f_spkbsr__", (ftnlen)1371)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    1371)];
    tends[(i__1 = i__) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1, 
	    "f_spkbsr__", (ftnlen)1372)] = tends[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tends", i__2, "f_spkbsr__", (ftnlen)
	    1372)];
    tbegs[(i__1 = i__ + 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)1374)] = tends[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tends", i__2, "f_spkbsr__", (ftnlen)
	    1374)] + 1;
    tends[(i__1 = i__ + 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)1375)] = tbegs[(i__2 = i__ + 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    1375)] + 1;
    t = tbegs[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
	    i__1, "f_spkbsr__", (ftnlen)1377)] + .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, segment STSIZE should match. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    segno = 1000;
    s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid, "#", spks + 1020, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)
	    255, (ftnlen)40);
    repmi_(xsegid, "#", &segno, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)1395)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1,
	     (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    i__ = segno + 1;
    tbegs[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)1403)] = (doublereal) (spkno * 10000 + i__)
	    ;
    tends[(i__1 = i__ - 2) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)1404)] = tbegs[(i__2 = i__ - 2) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    1404)] + 1.;
    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)1406)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)1406)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)1406)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    1410)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);

/*     Check correct handling of re-use intervals.  Create a new */
/*     SPK file that contains coverage that exemplifies the various */
/*     masking possibilities that may occur. */

    tcase_("Check re-use for a 1-body segment list.", (ftnlen)39);
    spkno = 7;

/*     Segment 1: */

    body = 1;
    ids[0] = body;
    tbegs[0] = (doublereal) (spkno * 10000);
    tends[0] = tbegs[0] + 1.;

/*     Segments 2-3: */

    body = 2;
    ids[1] = body;
    ids[2] = body;
    tbegs[2] = (doublereal) (spkno * 10000);
    tends[2] = tbegs[2] + 1.;
    tbegs[1] = tends[2] + 1.;
    tends[1] = tbegs[1] + 1.;

/*     Segments 4-6: */

    body = 3;
    ids[3] = body;
    ids[4] = body;
    ids[5] = body;
    tbegs[5] = (doublereal) (spkno * 10000);
    tends[5] = tbegs[5] + 3.;
    tbegs[4] = tends[5] - 1.;
    tends[4] = tbegs[4] + 3.;
    tbegs[3] = tbegs[4] + 1.;
    tends[3] = tends[4] - 1.;

/*     Segments 7-9: */

    body = 4;
    ids[6] = body;
    ids[7] = body;
    ids[8] = body;
    tbegs[8] = (doublereal) (spkno * 10000);
    tends[8] = tbegs[8] + 3.;
    tbegs[7] = tbegs[8];
    tends[7] = tends[8];
    tbegs[6] = tbegs[8] - 2.;
    tends[6] = tbegs[8] + 3.;

/*     Segments 10-12: */

    body = 5;
    ids[9] = body;
    ids[10] = body;
    ids[11] = body;
    tbegs[11] = (doublereal) (spkno * 10000);
    tends[11] = tbegs[11] + 3.;
    tbegs[10] = tbegs[11] - 2.;
    tends[10] = tbegs[10] + 3.;
    tbegs[9] = tbegs[10] - 2.;
    tends[9] = tends[11] + 1.;

/*     Segments 13-14: */

    body = 6;
    ids[12] = body;
    ids[13] = body;

/*     Singleton segment: */

    tbegs[12] = (doublereal) (spkno * 10000);
    tends[12] = tbegs[12];

/*     Invisible segment: */

    tbegs[13] = tends[12] + 3.;
    tends[13] = tbegs[13] - 1.;

/*     Three more segments for body 4: */

    ids[14] = 4;
    ids[15] = 4;
    ids[16] = 4;
    tbegs[14] = spkno * 10000 + 10.;
    tends[14] = tbegs[14] + 3.;
    tbegs[15] = tbegs[14] + 1.;
    tends[15] = tends[14] - 1.;
    tbegs[16] = tbegs[15];
    tends[16] = tends[15];

/*     Three more segments for body 5: */

    body = 5;
    ids[17] = body;
    ids[18] = body;
    ids[19] = body;
    tbegs[19] = spkno * 10000 + 10.;
    tends[19] = tbegs[19] + 3.;
    tbegs[18] = tbegs[19] - 2.;
    tends[18] = tbegs[18] + 3.;
    tbegs[17] = tbegs[18] - 2.;
    tends[17] = tends[19] + 1.;

/*     Create a segment for body 6 with the following topology: */


/*              +++++++           segment 21 */
/*                    +++++++             22 */
/*        +++++++                         23 */


    body = 6;
    ids[20] = body;
    ids[21] = body;
    ids[22] = body;
    tbegs[20] = spkno * 10000 + 10.;
    tends[20] = tbegs[20] + 3.;
    tbegs[21] = tends[20];
    tends[21] = tbegs[20] + 3.;
    tbegs[22] = tbegs[20] - 3.;
    tends[22] = tbegs[20];

/*     Create the eighth SPK, which is just a copy of the 7th, except */
/*     for descriptors and segment IDs. */

    spkno = 8;
    i__2 = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)1585)];
    for (segno = 1; segno <= i__2; ++segno) {
	t_crdesc__("SPK", &segno, &ids[(i__1 = segno - 1) < 16000 && 0 <= 
		i__1 ? i__1 : s_rnge("ids", i__1, "f_spkbsr__", (ftnlen)1587)]
		, &tbegs[(i__3 = segno - 1) < 16000 && 0 <= i__3 ? i__3 : 
		s_rnge("tbegs", i__3, "f_spkbsr__", (ftnlen)1587)], &tends[(
		i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : s_rnge("tends"
		, i__4, "f_spkbsr__", (ftnlen)1587)], &xdescr[(i__5 = segno * 
		5 - 5) < 80000 && 0 <= i__5 ? i__5 : s_rnge("xdescr", i__5, 
		"f_spkbsr__", (ftnlen)1587)], (ftnlen)3);
	s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1590)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1591)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)1591)) * 255, 
		xsegid + ((i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)1591)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : 
		s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)1592)) * 40, 
		"#", &segno, xsegid + ((i__3 = segno - 1) < 16000 && 0 <= 
		i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)
		1592)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : 
	    s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)1597)) * 255, &nseg[(
	    i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", i__1, 
	    "f_spkbsr__", (ftnlen)1597)], ids, tbegs, tends, xsegid, (ftnlen)
	    3, (ftnlen)255, (ftnlen)40);

/*     Create the segment descriptors and segment identifiers for */
/*     this SPK file. */

    spkno = 7;
    i__1 = nseg[(i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", 
	    i__2, "f_spkbsr__", (ftnlen)1608)];
    for (segno = 1; segno <= i__1; ++segno) {
	t_crdesc__("SPK", &segno, &ids[(i__2 = segno - 1) < 16000 && 0 <= 
		i__2 ? i__2 : s_rnge("ids", i__2, "f_spkbsr__", (ftnlen)1610)]
		, &tbegs[(i__3 = segno - 1) < 16000 && 0 <= i__3 ? i__3 : 
		s_rnge("tbegs", i__3, "f_spkbsr__", (ftnlen)1610)], &tends[(
		i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : s_rnge("tends"
		, i__4, "f_spkbsr__", (ftnlen)1610)], &xdescr[(i__5 = segno * 
		5 - 5) < 80000 && 0 <= i__5 ? i__5 : s_rnge("xdescr", i__5, 
		"f_spkbsr__", (ftnlen)1610)], (ftnlen)3);
	s_copy(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1613)) * 40, 
		"File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1614)) * 40, 
		"#", spks + ((i__3 = spkno - 1) < 10 && 0 <= i__3 ? i__3 : 
		s_rnge("spks", i__3, "f_spkbsr__", (ftnlen)1614)) * 255, 
		xsegid + ((i__4 = segno - 1) < 16000 && 0 <= i__4 ? i__4 : 
		s_rnge("xsegid", i__4, "f_spkbsr__", (ftnlen)1614)) * 40, (
		ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : 
		s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)1615)) * 40, 
		"#", &segno, xsegid + ((i__3 = segno - 1) < 16000 && 0 <= 
		i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)
		1615)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Unload the other SPK files.  Create and load the SPK file. */


/*     Unload the SPK files.  Again. */

    i__1 = spkno - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	t_suef__(&hndles[(i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge(
		"hndles", i__2, "f_spkbsr__", (ftnlen)1627)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_crdaf__("SPK", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)1631)) * 255, &nseg[(
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", i__2, 
	    "f_spkbsr__", (ftnlen)1631)], ids, tbegs, tends, xsegid, (ftnlen)
	    3, (ftnlen)255, (ftnlen)40);
/*      CALL BYEBYE ( 'SUCCESS' ) */
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)1640)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)1640)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Time for tests. */


/*     Make sure we can re-use data from the first segment for body 1. */

    spkno = 7;
    body = ids[0];
    t = (tbegs[0] + tends[0]) * .5;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1664)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
		ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);
    }
    t = tbegs[0] - 1.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    t = tends[0] + 1.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    t = tbegs[0];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    t = tends[0];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check out behavior for coverage consisting of two non-overlapping */
/*     segments.  The coverage topology is as follows: */


/*                      ++++++++++    segment 2 */
/*        +++++++++++                         3 */



    tcase_("Coverage is union of two disjoint intervals. Test re-use of each."
	    , (ftnlen)65);
    body = ids[1];
    t = (tbegs[1] + tends[1]) * .5;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1733)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[5], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 40, ok, (ftnlen)5, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);
    }
    t = (tbegs[2] + tends[2]) * .5;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1755)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[10], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 80, ok, (ftnlen)5, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);
    }

/*     Hit the endpoints of the left interval. */

    t = tbegs[2];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check handle, segment descriptor and ID. */

    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)1777)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[10], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 80, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    t = tends[2];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check handle, segment descriptor and ID. */

    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)1795)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[10], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 80, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Segments 4-6: */


/*     Check out behavior for coverage consisting of three segments */
/*     whose coverage is as shown: */


/*                 +++++++          segment 4 */
/*              +++++++++++++               5 */
/*        +++++++++++                       6 */


    tcase_("Segments 4-6:  three-segment overlapping case #1.", (ftnlen)49);
    body = ids[4];
    t = tends[5] + .25f;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1835)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[20], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 160, ok, (ftnlen)5, (ftnlen)40, 
		(ftnlen)1, (ftnlen)40);
    }
    body = ids[3];
    t = tbegs[5] + .25f;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1858)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[25], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 200, ok, (ftnlen)5, (ftnlen)40, 
		(ftnlen)1, (ftnlen)40);
    }
    t = tbegs[4] + .25f;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    t = tbegs[5] - .25f;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*     Segments 7-9: */


/*     Check out behavior for coverage consisting of three segments */
/*     whose coverage is as shown: */

/*        +++++++++++           segment 7 */
/*             +++++++++++              8 */
/*             +++++++++++              9 */

    tcase_("Segments 7-9:  three-segment overlapping case #2.", (ftnlen)49);

/*     Get the right side of the re-use interval to coincide with */
/*     the left endpoint of a descriptor, where ET lies to the left */
/*     of the segment, in the CHECK LIST state: */

    body = ids[6];
    t = tbegs[6] + .25f;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)1918)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[30], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 240, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Check out behavior for coverage consisting of three segments */
/*     whose coverage is as shown: */


/*       ++++++++++++++++++        segment 10 */
/*           +++++++                       11 */
/*               ++++++++                  12 */


    tcase_("Three-segment overlapping case #2.", (ftnlen)34);
    body = ids[9];
    t = tends[11] + .25f;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1956)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[45], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 360, ok, (ftnlen)5, (ftnlen)40, 
		(ftnlen)1, (ftnlen)40);
    }
    t = tends[9] + 1.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    t = tbegs[9] + .25f;
    for (i__ = 1; i__ <= 3; ++i__) {
	t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Check handle, segment descriptor and ID. */

	chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 
		<= i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)
		1987)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	t_chds__("DESCR", descr, "=", &xdescr[45], &c__4, &c_b31, ok, (ftnlen)
		5, (ftnlen)1);
	chcksc_("SEGID", segid, "=", xsegid + 360, ok, (ftnlen)5, (ftnlen)40, 
		(ftnlen)1, (ftnlen)40);
    }
    t = tbegs[10] - .25f;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2005)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[45], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 360, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Check out behavior for coverage consisting of three segments */
/*     whose coverage is as shown: */


/*       ++++++++++++++++++        segment 15 */
/*            +++++++                      16 */
/*            +++++++                      17 */


    tcase_("ET > segment uppper bound.  Lower bound of re-use interval = upp"
	    "er bound of segment.", (ftnlen)84);
    body = ids[14];
    t = tends[16] + .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check handle, segment descriptor and ID. */

    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2041)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[70], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 560, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Check out behavior for coverage consisting of three segments */
/*     whose coverage is as shown: */


/*              +++++++           segment 21 */
/*                    +++++++             22 */
/*        +++++++                         23 */


    tcase_("ET is in segment.  Lower bound of re-use interval = lower bound "
	    "of segment.", (ftnlen)75);
    body = 6;
    ids[20] = body;
    ids[21] = body;
    ids[22] = body;
    tbegs[20] = spkno * 10000 + 10.;
    tends[20] = tbegs[20] + 3.;
    tbegs[21] = tends[20];
    tends[21] = tbegs[20] + 3.;
    tbegs[22] = tbegs[20] - 3.;
    tends[22] = tbegs[20];
    body = ids[20];
    t = tbegs[20] + .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check handle, segment descriptor and ID. */

    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2093)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[100], &c__4, &c_b31, ok, (ftnlen)5, 
	    (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 800, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Check out behavior for coverage consisting singleton and */
/*     invisible segments. */


    tcase_("Look up data from a singleton segment.", (ftnlen)38);
    t = tbegs[12];
    body = ids[12];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2121)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[60], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 480, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);

/*     Exercise the logic for handling singleton and invisible */
/*     segments during a NEW BODY search. */

    tcase_("Look up data from a singleton segment, this time in a NEW SEGMEN"
	    "TS search.", (ftnlen)74);
    spkno = 8;
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)2138)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)2138)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = 13;
    t_crdesc__("SPK", &segno, &ids[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? 
	    i__1 : s_rnge("ids", i__1, "f_spkbsr__", (ftnlen)2144)], &tbegs[(
	    i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", 
	    i__2, "f_spkbsr__", (ftnlen)2144)], &tends[(i__3 = segno - 1) < 
	    16000 && 0 <= i__3 ? i__3 : s_rnge("tends", i__3, "f_spkbsr__", (
	    ftnlen)2144)], &xdescr[(i__4 = segno * 5 - 5) < 80000 && 0 <= 
	    i__4 ? i__4 : s_rnge("xdescr", i__4, "f_spkbsr__", (ftnlen)2144)],
	     (ftnlen)3);
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2147)) * 40, "File: # Segn"
	    "o: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2148)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2148)) * 255, xsegid + ((i__3 = segno - 1) <
	     16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", 
	    (ftnlen)2148)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2149)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2149)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t = tbegs[12];
    body = ids[12];
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2162)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[60], &c__4, &c_b31, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + 480, ok, (ftnlen)5, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    tcase_("Prepare for search w/o buffering tests: create an SPK with STSIZ"
	    "E segments for bodies 1-NBODY.", (ftnlen)94);

/*     Create an SPK file with STSIZE segments for bodies 1-NBODY. */

    spkno = 9;
    for (body = 1; body <= 4; ++body) {
	for (i__ = 1; i__ <= 1000; ++i__) {
	    j = (body - 1) * 1000 + i__;
	    ids[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", 
		    i__1, "f_spkbsr__", (ftnlen)2187)] = body;
	    tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs",
		     i__1, "f_spkbsr__", (ftnlen)2189)] = (doublereal) (spkno 
		    * 10000 + i__ - 1);
	    tends[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends",
		     i__1, "f_spkbsr__", (ftnlen)2190)] = tbegs[(i__2 = j - 1)
		     < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, 
		    "f_spkbsr__", (ftnlen)2190)] + 1;
	    s_copy(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2192)) * 40, 
		    "File: # Segno: #  Body:  #", (ftnlen)40, (ftnlen)26);
	    repmc_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2194)) * 40, 
		    "#", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 :
		     s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)2194)) * 255, 
		    xsegid + ((i__3 = j - 1) < 16000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)2194)) * 40, 
		    (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2195)) * 40, 
		    "#", &j, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 ? 
		    i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)2195))
		     * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2196)) * 40, 
		    "#", &body, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)
		    2196)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_crdaf__("SPK", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)2203)) * 255, &nseg[(
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", i__2, 
	    "f_spkbsr__", (ftnlen)2203)], ids, tbegs, tends, xsegid, (ftnlen)
	    3, (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Prepare for search w/o buffering tests: create an SPK with STSIZ"
	    "E segments for bodies 1-NBODY.", (ftnlen)94);

/*     Create an SPK file with STSIZE segments for bodies 1-NBODY. */

    spkno = 10;
    for (body = 1; body <= 4; ++body) {
	for (i__ = 1; i__ <= 997; ++i__) {
	    j = (body - 1) * 997 + i__;
	    ids[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("ids", 
		    i__1, "f_spkbsr__", (ftnlen)2225)] = body;
	    tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs",
		     i__1, "f_spkbsr__", (ftnlen)2227)] = (doublereal) (spkno 
		    * 10000 + i__ - 1);
	    tends[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends",
		     i__1, "f_spkbsr__", (ftnlen)2228)] = tbegs[(i__2 = j - 1)
		     < 16000 && 0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, 
		    "f_spkbsr__", (ftnlen)2228)] + 1;
	    s_copy(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2230)) * 40, 
		    "File: # Segno: #  Body:  #", (ftnlen)40, (ftnlen)26);
	    repmc_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2232)) * 40, 
		    "#", spks + ((i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 :
		     s_rnge("spks", i__2, "f_spkbsr__", (ftnlen)2232)) * 255, 
		    xsegid + ((i__3 = j - 1) < 16000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)2232)) * 40, 
		    (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2233)) * 40, 
		    "#", &j, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 ? 
		    i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)2233))
		     * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2234)) * 40, 
		    "#", &body, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)
		    2234)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    t_crdaf__("SPK", spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : 
	    s_rnge("spks", i__1, "f_spkbsr__", (ftnlen)2241)) * 255, &nseg[(
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("nseg", i__2, 
	    "f_spkbsr__", (ftnlen)2241)], ids, tbegs, tends, xsegid, (ftnlen)
	    3, (ftnlen)255, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_("Search w/o buffering, ET < segment begin, re-use interval right "
	    "endpoint < segment begin.", (ftnlen)89);

/*     Unload the SPK files.  Again. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)2258)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Load SPKs 7 and 9. */

    t_slef__(spks + 1530, &hndles[6], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + 2040, &hndles[8], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The request time should precede the coverage of segment 3 in */
/*     SPK 7. */

    body = 2;
    t = 69999.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    tcase_("Search w/o buffering, ET within segment, re-use interval, left e"
	    "ndpoint > segment begin.", (ftnlen)88);

/*     The request time should precede the coverage of segment 3 in */
/*     SPK 7. */

    body = 3;
    segno = 5;
    spkno = 7;
    tbegs[5] = (doublereal) (spkno * 10000);
    tends[5] = tbegs[5] + 3.;
    tbegs[4] = tends[5] - 1.;
    tends[4] = tbegs[4] + 3.;
    t = spkno * 10000 + 4.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2314)) * 40, "File: # Segn"
	    "o: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2315)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2315)) * 255, xsegid + ((i__3 = segno - 1) <
	     16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", 
	    (ftnlen)2315)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2316)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2316)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2319)) 
	    * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)2325)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)2325)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)2325)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    2330)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Search w/o buffering, ET < segment begin, re-use interval right "
	    "endpoint = segment begin.", (ftnlen)89);
    body = 4;
    segno = 7;
    spkno = 7;
    ids[6] = body;
    ids[7] = body;
    ids[8] = body;
    tbegs[8] = (doublereal) (spkno * 10000);
    tends[8] = tbegs[8] + 3.;
    tbegs[7] = tbegs[8];
    tends[7] = tends[8];
    tbegs[6] = tbegs[8] - 2.;
    tends[6] = tbegs[8] + 3.;
    t = tbegs[7] - 1.;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2364)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2366)) * 40, "File: # Segn"
	    "o: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2367)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2367)) * 255, xsegid + ((i__3 = segno - 1) <
	     16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", 
	    (ftnlen)2367)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2368)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2368)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2371)) 
	    * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)2377)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)2377)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)2377)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    2382)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);

/*     Some cases where a partial list must be dumped: */

    tcase_("Dump segment list from SPK 10.  While searching list for segment"
	    " for body 4, make upper bound of re-use interval < upper bound o"
	    "f segment descriptor.", (ftnlen)149);

/*     Unload SPK 9; load SPK 10. */

    t_suef__(&hndles[8]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_slef__(spks + 2295, &hndles[9], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Our request time should be in the interior of segment 15 in */
/*     SPK 7. */

    spkno = 7;
    segno = 15;
    ids[14] = 4;
    ids[15] = 4;
    ids[16] = 4;
    tbegs[14] = spkno * 10000 + 10.;
    tends[14] = tbegs[14] + 3.;
    tbegs[15] = tbegs[14] + 1.;
    tends[15] = tends[14] - 1.;
    tbegs[16] = tbegs[15];
    tends[16] = tbegs[16];
    t = tbegs[14] + .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2430)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2432)) * 40, "File: # Segn"
	    "o: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2433)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2433)) * 255, xsegid + ((i__3 = segno - 1) <
	     16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", 
	    (ftnlen)2433)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2434)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2434)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2437)) 
	    * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)2443)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)2443)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)2443)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    2448)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Dump segment list from SPK 10.  While searching list for segment"
	    " for body 4, make lower bound of re-use interval = upper bound o"
	    "f segment descriptor.", (ftnlen)149);
    spkno = 7;
    body = 4;
    tbegs[8] = (doublereal) (spkno * 10000);
    tends[8] = tbegs[8] + 3.;
    t = tends[8] + .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    tcase_("Dump segment list from SPK 10.  While searching list for segment"
	    " for body 5, make lower bound of re-use interval > lower bound o"
	    "f segment descriptor.", (ftnlen)149);
    spkno = 7;
    body = 5;
    ids[17] = body;
    ids[18] = body;
    ids[19] = body;
    tbegs[19] = spkno * 10000 + 10.;
    tends[19] = tbegs[19] + 3.;
    tbegs[18] = tbegs[19] - 2.;
    tends[18] = tbegs[18] + 3.;
    tbegs[17] = tbegs[18] - 2.;
    tends[17] = tends[19] + 1.;
    t = tends[17] - .5;
    t_ssfs__(&body, &t, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2501)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    segno = 18;
    s_copy(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2505)) * 40, "File: # Segn"
	    "o: #", (ftnlen)40, (ftnlen)16);
    repmc_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2506)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2506)) * 255, xsegid + ((i__3 = segno - 1) <
	     16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", 
	    (ftnlen)2506)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2507)) * 40, "#", &segno, 
	    xsegid + ((i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2507)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = segno - 1) < 16000 && 0 <= 
	    i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2510)) 
	    * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, (ftnlen)40);

/*     Check the descriptor as well.  However, don't check the */
/*     segment addresses. */

    t_crdesc__("SPK", &segno, &body, &tbegs[(i__1 = segno - 1) < 16000 && 0 <=
	     i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (ftnlen)2516)],
	     &tends[(i__2 = segno - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "tends", i__2, "f_spkbsr__", (ftnlen)2516)], &xdescr[(i__3 = 
	    segno * 5 - 5) < 80000 && 0 <= i__3 ? i__3 : s_rnge("xdescr", 
	    i__3, "f_spkbsr__", (ftnlen)2516)], (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = segno * 5 - 5) < 80000 && 0 
	    <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__", (ftnlen)
	    2521)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Create a situation where room is needed in the body table, and t"
	    "he second body list has expense greater than the first.", (ftnlen)
	    119);

/*     Unload SPKs 7 and 10. */

    t_suef__(&hndles[6]);
    t_suef__(&hndles[9]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fill up (nearly) the segment table with a cheap list for body 2 */
/*     and an expensive list for body 1. */

    spkno = 7;
    t_slef__(spks + ((i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
	    "spks", i__1, "f_spkbsr__", (ftnlen)2542)) * 255, &hndles[(i__2 = 
	    spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", i__2, 
	    "f_spkbsr__", (ftnlen)2542)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 2;
    ids[1] = body;
    ids[2] = body;
    tbegs[2] = (doublereal) (spkno * 10000);
    tends[2] = tbegs[2] + 1.;
    body = 2;
    segno = 3;
    d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tbegs", i__1, "f_spkbsr__", (ftnlen)2556)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the segment should be found.  Make sure we get */
/*     back the right handle and segment identifier. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2565)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);
    t_slef__(spks + 2295, &hndles[9], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    body = 1;
    spkno = 10;
    segno = 1;
    i__ = 1;
    tbegs[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1,
	     "f_spkbsr__", (ftnlen)2575)] = (doublereal) (spkno * 10000 + i__ 
	    - 1);
    tends[(i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1,
	     "f_spkbsr__", (ftnlen)2576)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    2576)] + 1;
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2583)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Now do a look up for body 3.  This should cause the segment */
/*     lists for bodies 2 and 1 to get dumped. */

    body = 3;
    spkno = 10;
    i__ = 1;
    j = (body - 1) * 997 + i__;
    tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", i__1, 
	    "f_spkbsr__", (ftnlen)2595)] = (doublereal) (spkno * 10000 + i__ 
	    - 1);
    tends[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends", i__1, 
	    "f_spkbsr__", (ftnlen)2596)] = tbegs[(i__2 = i__ - 1) < 16000 && 
	    0 <= i__2 ? i__2 : s_rnge("tbegs", i__2, "f_spkbsr__", (ftnlen)
	    2596)] + 1;
    s_copy(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2598)) * 40, "File: # Segn"
	    "o: #  Body:  #", (ftnlen)40, (ftnlen)26);
    repmc_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2600)) * 40, "#", spks + ((
	    i__2 = spkno - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("spks", i__2, 
	    "f_spkbsr__", (ftnlen)2600)) * 255, xsegid + ((i__3 = i__ - 1) < 
	    16000 && 0 <= i__3 ? i__3 : s_rnge("xsegid", i__3, "f_spkbsr__", (
	    ftnlen)2600)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)
	    40);
    repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2601)) * 40, "#", &j, 
	    xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2601)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    repmi_(xsegid + ((i__1 = i__ - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "xsegid", i__1, "f_spkbsr__", (ftnlen)2602)) * 40, "#", &body, 
	    xsegid + ((i__2 = i__ - 1) < 16000 && 0 <= i__2 ? i__2 : s_rnge(
	    "xsegid", i__2, "f_spkbsr__", (ftnlen)2602)) * 40, (ftnlen)40, (
	    ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    segno = j;
    d__1 = tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
	    i__1, "f_spkbsr__", (ftnlen)2607)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    chcksi_("HANDLE", &handle, "=", &hndles[(i__1 = spkno - 1) < 10 && 0 <= 
	    i__1 ? i__1 : s_rnge("hndles", i__1, "f_spkbsr__", (ftnlen)2612)],
	     &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Return on entry in RETURN mode, if the error status is set. */

    tcase_("Make sure all T_SBSR entry points return on entry when RETURN() "
	    "is .TRUE.", (ftnlen)73);

/*     Depending on whether we're calling a version of T_SBSR that does */
/*     coverage checking, the error status may be reset. */
    s_copy(smsg, "Return on entry", (ftnlen)25, (ftnlen)15);
    sigerr_(smsg, (ftnlen)25);
    t_sbsr__(" ", &c__1, &c__1, &c_b31, descr, segid, &found, (ftnlen)1, (
	    ftnlen)40);
    if (return_()) {
	chckxc_(&c_true, smsg, ok, (ftnlen)25);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    sigerr_(smsg, (ftnlen)25);
    t_slef__(" ", &handle, (ftnlen)1);
    if (return_()) {
	chckxc_(&c_true, smsg, ok, (ftnlen)25);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    sigerr_(smsg, (ftnlen)25);
    t_suef__(&handle);
    if (return_()) {
	chckxc_(&c_true, smsg, ok, (ftnlen)25);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    sigerr_(smsg, (ftnlen)25);
    t_ssfs__(&c__1, &c_b31, &handle, descr, segid, &found, (ftnlen)40);
    if (return_()) {
	chckxc_(&c_true, smsg, ok, (ftnlen)25);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tcase_("Make sure an error is signaled if T_SBSR is called directly and "
	    "RETURN() is .FALSE.", (ftnlen)83);
    t_sbsr__(" ", &c__1, &c__1, &c_b31, descr, segid, &found, (ftnlen)1, (
	    ftnlen)40);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
    tcase_("Try DAFOPR error handling.", (ftnlen)26);
    t_slef__("ThisFileDoesNotExist", &handle, (ftnlen)20);
    if (return_()) {
	chckxc_(&c_true, "SPICE(FILENOTFOUND)", ok, (ftnlen)19);
    } else {
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tcase_("Test partial deletion of a segment list when a file is unloaded.",
	     (ftnlen)64);

/*     Unload the SPK files.  The load files 1 and 2. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)2705)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 2; ++i__) {
	t_slef__(spks + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"spks", i__1, "f_spkbsr__", (ftnlen)2711)) * 255, &hndles[(
		i__2 = i__ - 1) < 10 && 0 <= i__2 ? i__2 : s_rnge("hndles", 
		i__2, "f_spkbsr__", (ftnlen)2711)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Do lookups for body 1 that hit both files. */

    body = 1;
    tbegs[0] = 1e4;
    d__1 = tbegs[0] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    body = 1;
    spkno = 2;
    segno = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)2729)] / 2 + 1;
    tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
	    i__1, "f_spkbsr__", (ftnlen)2731)] = (doublereal) (spkno * 10000 
	    + segno - 1);
    d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tbegs", i__1, "f_spkbsr__", (ftnlen)2733)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Do a lookup for body 2 to create a segment list for that */
/*     body. */

    body = 2;
    spkno = 2;
    segno = nseg[(i__1 = spkno - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge("nseg", 
	    i__1, "f_spkbsr__", (ftnlen)2745)] / 2;
    tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs", 
	    i__1, "f_spkbsr__", (ftnlen)2747)] = (doublereal) (spkno * 10000 
	    + segno - 1);
    d__1 = tbegs[(i__1 = segno - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tbegs", i__1, "f_spkbsr__", (ftnlen)2749)] + .5f;
    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Reload file 1, removing the portion of body 1's segment list */
/*     that came from file 1, as part of the unload process that */
/*     precedes re-loading file 1. */

    t_slef__(spks, hndles, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create FTSIZE copies of SPK 1 and load FTSIZE-1 of them.  We */
/*     should get a file table overflow error. */

    tcase_("File table overflow error.", (ftnlen)26);
    for (i__ = 1; i__ <= 500; ++i__) {
	s_copy(spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		"spkcpy", i__1, "f_spkbsr__", (ftnlen)2774)) * 255, "copy#.b"
		"sp", (ftnlen)255, (ftnlen)9);
	repmi_(spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		"spkcpy", i__1, "f_spkbsr__", (ftnlen)2775)) * 255, "#", &i__,
		 spkcpy + ((i__2 = i__ - 1) < 500 && 0 <= i__2 ? i__2 : 
		s_rnge("spkcpy", i__2, "f_spkbsr__", (ftnlen)2775)) * 255, (
		ftnlen)255, (ftnlen)1, (ftnlen)255);
	body = 1;
	tbegs[0] = 1e4;
	tends[0] = 10001.;
	spkno = 1;
	s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid, "#", spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? 
		i__1 : s_rnge("spkcpy", i__1, "f_spkbsr__", (ftnlen)2783)) * 
		255, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_crdaf__("SPK", spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 
		: s_rnge("spkcpy", i__1, "f_spkbsr__", (ftnlen)2787)) * 255, 
		nseg, &body, tbegs, tends, xsegid, (ftnlen)3, (ftnlen)255, (
		ftnlen)40);
    }
    for (i__ = 1; i__ <= 498; ++i__) {
	t_slef__(spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : 
		s_rnge("spkcpy", i__1, "f_spkbsr__", (ftnlen)2794)) * 255, &
		cpyhan[(i__2 = i__ - 1) < 500 && 0 <= i__2 ? i__2 : s_rnge(
		"cpyhan", i__2, "f_spkbsr__", (ftnlen)2794)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_slef__(spkcpy + 126990, &cpyhan[498], (ftnlen)255);

/*     Note:  if FTSIZE were >= the handle manager file table's size, */
/*     we would expect the short error message */

/*        SPICE(FTFULL) */

    chckxc_(&c_true, "SPICE(SPKFILETABLEFULL)", ok, (ftnlen)23);

/*     Loading, unloading, and priority checks: */

    tcase_("Load all copies of SPK 1, looking up the same state from each.  "
	    "Unload the files in reverse order.  Repeat 3 times.", (ftnlen)115)
	    ;

/*     First, make sure all files are unloaded. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)2821)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 499; ++i__) {
	t_suef__(&cpyhan[(i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		"cpyhan", i__1, "f_spkbsr__", (ftnlen)2828)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    body = 1;
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 500; ++j) {
	    tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs",
		     i__1, "f_spkbsr__", (ftnlen)2839)] = 1e4;
	    tends[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends",
		     i__1, "f_spkbsr__", (ftnlen)2840)] = 10001.;
	    t_slef__(spkcpy + ((i__1 = j - 1) < 500 && 0 <= i__1 ? i__1 : 
		    s_rnge("spkcpy", i__1, "f_spkbsr__", (ftnlen)2842)) * 255,
		     &cpyhan[(i__2 = j - 1) < 500 && 0 <= i__2 ? i__2 : 
		    s_rnge("cpyhan", i__2, "f_spkbsr__", (ftnlen)2842)], (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2845)) * 40, 
		    "File: # Segno: #", (ftnlen)40, (ftnlen)16);
	    repmc_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2846)) * 40, 
		    "#", spkcpy + ((i__2 = j - 1) < 500 && 0 <= i__2 ? i__2 : 
		    s_rnge("spkcpy", i__2, "f_spkbsr__", (ftnlen)2846)) * 255,
		     xsegid + ((i__3 = j - 1) < 16000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)2846)) * 40, 
		    (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2847)) * 40, 
		    "#", &c__1, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)
		    2847)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
		    "tbegs", i__1, "f_spkbsr__", (ftnlen)2850)] + .5f;
	    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           In this case, the segment should be found.  Make sure */
/*           we get back the right handle and segment identifier. */

	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    chcksi_("HANDLE", &handle, "=", &cpyhan[(i__1 = j - 1) < 500 && 0 
		    <= i__1 ? i__1 : s_rnge("cpyhan", i__1, "f_spkbsr__", (
		    ftnlen)2859)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = j - 1) < 16000 && 
		    0 <= i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (
		    ftnlen)2860)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, 
		    (ftnlen)40);

/*           Check the descriptor as well.  However, don't check the */
/*           segment addresses. */

	    t_crdesc__("SPK", &c__1, &body, &tbegs[(i__1 = j - 1) < 16000 && 
		    0 <= i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (
		    ftnlen)2866)], &tends[(i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("tends", i__2, "f_spkbsr__", (ftnlen)2866)
		    ], &xdescr[(i__3 = j * 5 - 5) < 80000 && 0 <= i__3 ? i__3 
		    : s_rnge("xdescr", i__3, "f_spkbsr__", (ftnlen)2866)], (
		    ftnlen)3);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = j * 5 - 5) < 80000 
		    && 0 <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__",
		     (ftnlen)2870)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
	}

/*        Now unload files, looking up states as we go. */

	for (j = 499; j >= 1; --j) {
	    t_suef__(&cpyhan[(i__1 = j) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		    "cpyhan", i__1, "f_spkbsr__", (ftnlen)2880)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tbegs",
		     i__1, "f_spkbsr__", (ftnlen)2883)] = 1e4;
	    tends[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge("tends",
		     i__1, "f_spkbsr__", (ftnlen)2884)] = 10001.;
	    s_copy(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2886)) * 40, 
		    "File: # Segno: #", (ftnlen)40, (ftnlen)16);
	    repmc_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2887)) * 40, 
		    "#", spkcpy + ((i__2 = j - 1) < 500 && 0 <= i__2 ? i__2 : 
		    s_rnge("spkcpy", i__2, "f_spkbsr__", (ftnlen)2887)) * 255,
		     xsegid + ((i__3 = j - 1) < 16000 && 0 <= i__3 ? i__3 : 
		    s_rnge("xsegid", i__3, "f_spkbsr__", (ftnlen)2887)) * 40, 
		    (ftnlen)40, (ftnlen)1, (ftnlen)255, (ftnlen)40);
	    repmi_(xsegid + ((i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : 
		    s_rnge("xsegid", i__1, "f_spkbsr__", (ftnlen)2888)) * 40, 
		    "#", &c__1, xsegid + ((i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("xsegid", i__2, "f_spkbsr__", (ftnlen)
		    2888)) * 40, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = tbegs[(i__1 = j - 1) < 16000 && 0 <= i__1 ? i__1 : s_rnge(
		    "tbegs", i__1, "f_spkbsr__", (ftnlen)2891)] + .5f;
	    t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           In this case, the segment should be found.  Make sure */
/*           we get back the right handle and segment identifier. */

	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    chcksi_("HANDLE", &handle, "=", &cpyhan[(i__1 = j - 1) < 500 && 0 
		    <= i__1 ? i__1 : s_rnge("cpyhan", i__1, "f_spkbsr__", (
		    ftnlen)2900)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	    chcksc_("SEGID", segid, "=", xsegid + ((i__1 = j - 1) < 16000 && 
		    0 <= i__1 ? i__1 : s_rnge("xsegid", i__1, "f_spkbsr__", (
		    ftnlen)2901)) * 40, ok, (ftnlen)5, (ftnlen)40, (ftnlen)1, 
		    (ftnlen)40);

/*           Check the descriptor as well.  However, don't check the */
/*           segment addresses. */

	    t_crdesc__("SPK", &c__1, &body, &tbegs[(i__1 = j - 1) < 16000 && 
		    0 <= i__1 ? i__1 : s_rnge("tbegs", i__1, "f_spkbsr__", (
		    ftnlen)2907)], &tends[(i__2 = j - 1) < 16000 && 0 <= i__2 
		    ? i__2 : s_rnge("tends", i__2, "f_spkbsr__", (ftnlen)2907)
		    ], &xdescr[(i__3 = j * 5 - 5) < 80000 && 0 <= i__3 ? i__3 
		    : s_rnge("xdescr", i__3, "f_spkbsr__", (ftnlen)2907)], (
		    ftnlen)3);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_chds__("DESCR", descr, "=", &xdescr[(i__1 = j * 5 - 5) < 80000 
		    && 0 <= i__1 ? i__1 : s_rnge("xdescr", i__1, "f_spkbsr__",
		     (ftnlen)2911)], &c__4, &c_b31, ok, (ftnlen)5, (ftnlen)1);
	}
    }

/*     Make sure we don't accumulate DAF links by re-loading a file. */

    tcase_("Load the first SPK file 2*FTSIZE times.", (ftnlen)39);
    for (i__ = 1; i__ <= 1000; ++i__) {
	tbegs[0] = 1e4;
	tends[0] = 10001.;
	t_slef__(spks, hndles, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(xsegid, "File: # Segno: #", (ftnlen)40, (ftnlen)16);
	repmc_(xsegid, "#", spks, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)255, 
		(ftnlen)40);
	repmi_(xsegid, "#", &c__1, xsegid, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = tbegs[0] + .5f;
	t_ssfs__(&body, &d__1, &handle, descr, segid, &found, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        In this case, the segment should be found.  Make sure */
/*        we get back the right handle and segment identifier. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("HANDLE", &handle, "=", hndles, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
	chcksc_("SEGID", segid, "=", xsegid, ok, (ftnlen)5, (ftnlen)40, (
		ftnlen)1, (ftnlen)40);

/*        Check the descriptor as well.  However, don't check the */
/*        segment addresses. */

	t_crdesc__("SPK", &c__1, &body, tbegs, tends, xdescr, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_chds__("DESCR", descr, "=", xdescr, &c__4, &c_b31, ok, (ftnlen)5, (
		ftnlen)1);
    }

/*     Last step:  delete all of the SPK files we created. */

    for (i__ = 1; i__ <= 10; ++i__) {
	t_suef__(&hndles[(i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"hndles", i__1, "f_spkbsr__", (ftnlen)2966)]);
	delfil_(spks + ((i__1 = i__ - 1) < 10 && 0 <= i__1 ? i__1 : s_rnge(
		"spks", i__1, "f_spkbsr__", (ftnlen)2967)) * 255, (ftnlen)255)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    for (i__ = 1; i__ <= 500; ++i__) {
	t_suef__(&cpyhan[(i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		"cpyhan", i__1, "f_spkbsr__", (ftnlen)2974)]);
	delfil_(spkcpy + ((i__1 = i__ - 1) < 500 && 0 <= i__1 ? i__1 : s_rnge(
		"spkcpy", i__1, "f_spkbsr__", (ftnlen)2975)) * 255, (ftnlen)
		255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_spkbsr__ */

