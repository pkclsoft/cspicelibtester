/* f_spkcor.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__6 = 6;
static doublereal c_b33 = 0.;
static integer c__13 = 13;
static integer c__1 = 1;
static doublereal c_b52 = 1e-13;
static doublereal c_b61 = 1e-6;
static integer c__3 = 3;
static doublereal c_b184 = 1.;
static integer c__0 = 0;
static doublereal c_b260 = 1e-14;
static doublereal c_b270 = 1e-10;
static doublereal c_b274 = 1e-4;
static doublereal c_b304 = 1e-8;

/* $Procedure      F_SPKCOR ( Tests for SPK aberration corrections ) */
/* Subroutine */ int f_spkcor__(logical *ok)
{
    /* Initialized data */

    static char badcor[5*6] = "S    " "RL   " "RL+S " "XS   " "XRL  " "XRL+S";
    static char corlst[5*9] = "NONE " "LT   " "LT+S " "CN   " "CN+S " "XLT  " 
	    "XLT+S" "XCN  " "XCN+S";
    static char frmlst[32*3] = "J2000                           " "ECLIPJ200"
	    "0                      " "IAU_MOON                        ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int zzspkac0_(integer *, doublereal *, char *, 
	    char *, integer *, doublereal *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), zzspkac1_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen);
    doublereal left;
    integer maxn;
    extern /* Subroutine */ int zzspkas0_(integer *, doublereal *, char *, 
	    char *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen);
    doublereal dpos[3];
    extern /* Subroutine */ int zzspkas1_(integer *, doublereal *, char *, 
	    char *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), zzspkez0_(integer *, doublereal *, 
	    char *, char *, integer *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), zzspkez1_(integer *, doublereal *, char *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen, ftnlen);
    logical xmit;
    doublereal work[13];
    extern /* Subroutine */ int zzspklt0_(integer *, doublereal *, char *, 
	    char *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), zzspklt1_(integer *, doublereal *, char *, char *
	    , doublereal *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), vsub_(doublereal *, doublereal *, doublereal *);
    extern doublereal t_stcini__(integer *, char *, char *, integer *, ftnlen,
	     ftnlen);
    extern /* Subroutine */ int zzstelab_(logical *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, doublereal *);
    integer i__, j;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    doublereal t;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen);
    doublereal delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    doublereal right;
    integer nsamp;
    doublereal state[6];
    char title[80];
    doublereal pcorr[3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal ltssb, stobs[6];
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    t_success__(logical *);
    doublereal dpcor2[3], state0[6], state1[6];
    integer ac;
    doublereal pcorr2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chbder_(doublereal *, integer *, doublereal *, doublereal *, 
	    integer *, doublereal *, doublereal *);
    integer fr;
    doublereal et;
    integer degree, handle;
    extern /* Subroutine */ int chbfit_(U_fp, doublereal *, doublereal *, 
	    integer *, doublereal *, doublereal *);
    doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), delfil_(
	    char *, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    doublereal coeffs[13];
    char abcorr[5];
    extern /* Subroutine */ int kilfil_(char *, ftnlen), stelab_(doublereal *,
	     doublereal *, doublereal *);
    doublereal tdelta, halfwn;
    logical attblk[15];
    integer target;
    doublereal dpcorr[3];
    extern /* Subroutine */ int t_getx__(), t_gety__(), t_getz__();
    doublereal derivs[3], partdp[39]	/* was [3][13] */, retval, ssbobs[12]	
	    /* was [6][2] */, corpos[3], et0;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     spkssb_(integer *, doublereal *, char *, doublereal *, ftnlen);
    integer obsrvr;
    extern /* Subroutine */ int spkltc_(integer *, doublereal *, char *, char 
	    *, doublereal *, doublereal *, doublereal *, doublereal *, ftnlen,
	     ftnlen);
    doublereal lt0, lt1;
    char timstr[40];
    extern /* Subroutine */ int spkacs_(integer *, doublereal *, char *, char 
	    *, integer *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), qderiv_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), spkaps_(integer *, doublereal *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), spkgeo_(integer *, 
	    doublereal *, char *, integer *, doublereal *, doublereal *, 
	    ftnlen), stlabx_(doublereal *, doublereal *, doublereal *), 
	    tstspk_(char *, logical *, integer *, ftnlen), spkuef_(integer *);
    doublereal x2s[2];
    extern /* Subroutine */ int t_zzstelab__(logical *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal acc[3];
    extern /* Subroutine */ int t_zzstlabn__(logical *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    char ref[32];
    doublereal dlt, pos[3];
    extern /* Subroutine */ int t_getlt__(), t_getsx__(), t_getsy__(), 
	    t_getsz__();
    doublereal dlt0, dlt1;

/* $ Abstract */

/*     This routine performs a series of tests on the SPICELIB */
/*     SPK API routines, as well as their frame system "mirror" */
/*     routines, to make their aberration correction features work */
/*     as intended. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB SPK API routines, as well as */
/*     their frame system "mirror" routines, to make their aberration */
/*     correction features work as intended. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.5.0, 10-MAR-2014 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-INTEL. */

/* -    TSPICE Version 2.4.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-LINUX-64BIT-IFORT. */

/* -    TSPICE Version 2.3.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-GFORTRAN. */

/* -    TSPICE Version 2.2.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GFORTRAN. */

/* -    TSPICE Version 2.1.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GCC_C. */

/* -    TSPICE Version 2.0.0, 02-MAY-2012 (EDW) */

/*        Edited the SPKEZ invalid aberration test case */

/*           SPKEZ: reject S, XS, relativistic ABCORR values */

/*        to expect error signal SPICE(INVALIDOPTION) from */
/*        ZZVALCOR. Previous version of test case expected */
/*        SPICE(NOTSUPPORTED) signal from SPKAPS. */

/*        Edited the SPKLTC reject aberration test case */

/*           SPKLTC: reject S, XS, relativistic ABCORR values */

/*        to expect error signal SPICE(INVALIDOPTION) from */
/*        ZZVALCOR. Previous version of test case expected */
/*        SPICE(NOTSUPPORTED) signal from ZZPRSCOR. */

/* -    TSPICE Version 1.12.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL. */

/* -    TSPICE Version 1.11.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-CC_C. */

/* -    TSPICE Version 1.10.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-64BIT-CC_C. */

/* -    TSPICE Version 1.9.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-NATIVE_C. */

/* -    TSPICE Version 1.8.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-WINDOWS-64BIT-IFORT. */

/* -    TSPICE Version 1.7.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-LINUX-64BIT-GFORTRAN. */

/* -    TSPICE Version 1.6.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-64BIT-MS_C. */

/* -    TSPICE Version 1.5.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-INTEL_C. */

/* -    TSPICE Version 1.4.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-IFORT. */

/* -    TSPICE Version 1.3.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-GFORTRAN. */

/* -    TSPICE Version 1.2.0, 18-MAR-2009 (BVS) */

/*        Updated for PC-LINUX-GFORTRAN. */

/* -    TSPICE Version 1.1.0, 18-MAR-2009 (BVS) */

/*        Updated for MAC-OSX-GFORTRAN. */

/* -    TSPICE Version 1.0.0, 25-FEB-2008 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     External functions */


/*     Local parameters */


/*     Tolerance level that can be used with DE-418 on PC/Linux/g77 */
/*     platform: 5.D-8 */


/*     Tolerance level that can be used with DE-418 on PC/Linux/g77 */
/*     platform: 5.D-8 */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_SPKCOR", (ftnlen)8);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Create kernel files.", (ftnlen)20);
    kilfil_("phoenix.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kilfil_("phoenix.tpc", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("phoenix.bsp", &c_true, &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("phoenix.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For improved numeric performance, use a DE-based SPK. */

/*      CALL FURNSH ( 'de418.bsp' ) */


/*     The first set of tests covers the routine SPKEZ and */
/*     its frame system mirror routines. */

    for (fr = 1; fr <= 3; ++fr) {
	for (ac = 1; ac <= 9; ++ac) {

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "SPKEZ: check Earth-Mars states; frame #, abcorr ="
		    " #.", (ftnlen)80, (ftnlen)52);
	    obsrvr = 399;
	    target = 499;
	    s_copy(ref, frmlst + (((i__1 = fr - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("frmlst", i__1, "f_spkcor__", (ftnlen)410)) << 5), 
		    (ftnlen)32, (ftnlen)32);
	    s_copy(abcorr, corlst + ((i__1 = ac - 1) < 9 && 0 <= i__1 ? i__1 :
		     s_rnge("corlst", i__1, "f_spkcor__", (ftnlen)411)) * 5, (
		    ftnlen)5, (ftnlen)5);
	    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, 
		    (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    5, (ftnlen)80);
	    tcase_(title, (ftnlen)80);

/*           Set up our ephemeris utilities to enable Cheby fitting. */

	    retval = t_stcini__(&target, ref, abcorr, &obsrvr, (ftnlen)32, (
		    ftnlen)5);

/*           Take four years' worth of samples, spaced 2 months apart. */

	    tdelta = jyear_() / 6;
	    nsamp = 24;
	    et0 = 0.;
	    halfwn = 300.;
	    degree = 12;
	    maxn = degree + 1;
	    i__1 = nsamp;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		et = et0 + (i__ - 1) * tdelta;
		spkez_(&target, &et, ref, abcorr, &obsrvr, state, &lt, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check ZZSPKEZ0: state should match that */
/*              from SPKEZ. */

		zzspkez0_(&target, &et, ref, abcorr, &obsrvr, state0, &lt, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("STATE0", state0, "=", state, &c__6, &c_b33, ok, (
			ftnlen)6, (ftnlen)1);

/*              Check ZZSPKEZ1: state should match that */
/*              from SPKEZ. */

		zzspkez1_(&target, &et, ref, abcorr, &obsrvr, state1, &lt, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("STATE1", state1, "=", state, &c__6, &c_b33, ok, (
			ftnlen)6, (ftnlen)1);

/*              We're going to test each position component of our state */
/*              vector to make sure our fitting approach is working. */

		for (j = 1; j <= 3; ++j) {

/*                 Fit a Cheby expansion of degree MAXDEG to the Jth */
/*                 position component over a 2*HALFWN time span centered */
/*                 at ET. We'll retain an expansion of degree DEGREE. */

		    if (j == 1) {
			left = et - halfwn;
			right = et + halfwn;
			chbfit_((U_fp)t_getx__, &left, &right, &c__13, work, 
				coeffs);
		    } else if (j == 2) {
			chbfit_((U_fp)t_gety__, &left, &right, &c__13, work, 
				coeffs);
		    } else {
			chbfit_((U_fp)t_getz__, &left, &right, &c__13, work, 
				coeffs);
		    }
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Find the derivatives of the current Cheby expansion. */

		    x2s[0] = et;
		    x2s[1] = halfwn;
		    i__2 = maxn - 1;
		    chbder_(coeffs, &i__2, x2s, &et, &c__1, partdp, derivs);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    pos[0] = derivs[0];
		    dpos[0] = derivs[1];

/*                 Note: to compute acceleration, change the argument */
/*                 "1" above to "2" and capture acceleration from */

/*                    DERIVS(2) */


/*                 Compare the expansion to the position from SPKEZ at */
/*                 ET. */

		    etcal_(&et, timstr, (ftnlen)40);
		    s_copy(title, "Position component # at ET #.", (ftnlen)80,
			     (ftnlen)29);
		    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)40, (ftnlen)80);
		    chcksd_(title, &state[(i__2 = j - 1) < 6 && 0 <= i__2 ? 
			    i__2 : s_rnge("state", i__2, "f_spkcor__", (
			    ftnlen)524)], "~/", pos, &c_b52, ok, (ftnlen)80, (
			    ftnlen)2);

/*                 Compare the derivative of the expansion to the */
/*                 velocity from SPKEZ at ET. */

		    etcal_(&et, timstr, (ftnlen)40);
		    s_copy(title, "Velocity component # at ET #.", (ftnlen)80,
			     (ftnlen)29);
		    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)40, (ftnlen)80);

/*                 For velocity components under 1 km/s, check */
/*                 the absolute error. We're looking for */
/*                 1 mm/s agreement. */

		    if ((d__1 = state[(i__2 = j + 2) < 6 && 0 <= i__2 ? i__2 :
			     s_rnge("state", i__2, "f_spkcor__", (ftnlen)542)]
			    , abs(d__1)) < 1.) {
			chcksd_(title, &state[(i__2 = j + 2) < 6 && 0 <= i__2 
				? i__2 : s_rnge("state", i__2, "f_spkcor__", (
				ftnlen)544)], "~", dpos, &c_b61, ok, (ftnlen)
				80, (ftnlen)1);

/*                    For velocity components over 1 km/s, check */
/*                    the relative error. */

		    } else {
			chcksd_(title, &state[(i__2 = j + 2) < 6 && 0 <= i__2 
				? i__2 : s_rnge("state", i__2, "f_spkcor__", (
				ftnlen)552)], "~/", dpos, &c_b61, ok, (ftnlen)
				80, (ftnlen)2);
		    }
		}

/*              End of state component loop. */

	    }

/*           End of epoch loop. */

	}

/*        End of aberration correction loop. */

    }

/*     End of frame loop. */


/*     The second set of tests covers the routine SPKLTC and its frame */
/*     system mirror routines. This routine requires an inertial input */
/*     frame, so we omit the cases for the last frame in the list. */

    for (fr = 1; fr <= 2; ++fr) {
	for (ac = 1; ac <= 9; ++ac) {

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "SPKLTC:  Earth-Mars light time/light time rate; f"
		    "rame #, abcorr = #.", (ftnlen)80, (ftnlen)68);
	    obsrvr = 399;
	    target = 499;
	    s_copy(ref, frmlst + (((i__1 = fr - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("frmlst", i__1, "f_spkcor__", (ftnlen)591)) << 5), 
		    (ftnlen)32, (ftnlen)32);
	    s_copy(abcorr, corlst + ((i__1 = ac - 1) < 9 && 0 <= i__1 ? i__1 :
		     s_rnge("corlst", i__1, "f_spkcor__", (ftnlen)592)) * 5, (
		    ftnlen)5, (ftnlen)5);
	    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, 
		    (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    5, (ftnlen)80);
	    tcase_(title, (ftnlen)80);

/*           Set up our ephemeris utilities to enable Cheby fitting. */

	    retval = t_stcini__(&target, ref, abcorr, &obsrvr, (ftnlen)32, (
		    ftnlen)5);

/*           Take four years' worth of samples, spaced 2 months apart. */

	    tdelta = jyear_() / 6;
	    nsamp = 24;
	    et0 = 0.;
	    halfwn = 300.;
	    degree = 12;
	    maxn = degree + 1;
	    i__1 = nsamp;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		et = et0 + (i__ - 1) * tdelta;
		spkssb_(&obsrvr, &et, ref, stobs, (ftnlen)32);
		spkltc_(&target, &et, ref, abcorr, stobs, state, &lt, &dlt, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check SPKEZ: light time (but not state) should */
/*              match that from SPKLTC. */

		spkez_(&target, &et, ref, abcorr, &obsrvr, state0, &lt0, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "SPKEZ light time at ET #.", (ftnlen)80, (
			ftnlen)25);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt0, "~/", &lt, &c_b52, ok, (ftnlen)80, (
			ftnlen)2);

/*              Check ZZSPKLT0: state, light time, light time rate */
/*              all should match those from SPKLTC. */

		zzspklt0_(&target, &et, ref, abcorr, stobs, state0, &lt0, &
			dlt0, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "ZZSPKLT0 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state0, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);
		s_copy(title, "ZZSPKLT0 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt0, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKLT0 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt0, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
/*              Check ZZSPKLT1: state, light time, light time rate */
/*              all should match those from SPKLTC. */

		zzspklt1_(&target, &et, ref, abcorr, stobs, state1, &lt1, &
			dlt1, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "ZZSPKLT1 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state1, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);
		s_copy(title, "ZZSPKLT1 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt1, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKLT1 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt1, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);

/*              Fit a Cheby expansion of degree MAXDEG to the light time */
/*              over a 2*HALFWN time span centered at ET. We'll retain */
/*              an expansion of degree DEGREE. */

		left = et - halfwn;
		right = et + halfwn;
		chbfit_((U_fp)t_getlt__, &left, &right, &c__13, work, coeffs);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Find the derivatives of the current Cheby expansion. */

		x2s[0] = et;
		x2s[1] = halfwn;
		i__2 = maxn - 1;
		chbder_(coeffs, &i__2, x2s, &et, &c__1, partdp, derivs);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		pos[0] = derivs[0];
		dpos[0] = derivs[1];

/*              Note: to compute acceleration, change the argument */
/*              "1" above to "2" and capture acceleration from */

/*                 DERIVS(2) */


/*              Compare the expansion to the light time from SPKLTC at */
/*              ET. */

		s_copy(title, "SPKLTC Light time at ET #.", (ftnlen)80, (
			ftnlen)26);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt, "~/", pos, &c_b52, ok, (ftnlen)80, (
			ftnlen)2);

/*              Compare the derivative of the expansion to the */
/*              light time from SPKLTC at ET. */

		s_copy(title, "SPKLTC Light time rate at ET #.", (ftnlen)80, (
			ftnlen)31);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt, "~/", dpos, &c_b61, ok, (ftnlen)80, (
			ftnlen)2);
	    }

/*           End of epoch loop. */

	}

/*        End of aberration correction loop. */

    }

/*     End of frame loop. */


/*     The third set of tests covers the routines SPKACS, SPKAPS, and */
/*     their frame system mirror routines. These routines require an */
/*     inertial input frame, so we omit the cases for the last frame in */
/*     the list. */

    for (fr = 1; fr <= 2; ++fr) {
	for (ac = 1; ac <= 9; ++ac) {

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "SPKACS:  Earth-Mars state, light time/rate; frame"
		    " #, abcorr = #.", (ftnlen)80, (ftnlen)64);
	    obsrvr = 399;
	    target = 499;
	    s_copy(ref, frmlst + (((i__1 = fr - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("frmlst", i__1, "f_spkcor__", (ftnlen)760)) << 5), 
		    (ftnlen)32, (ftnlen)32);
	    s_copy(abcorr, corlst + ((i__1 = ac - 1) < 9 && 0 <= i__1 ? i__1 :
		     s_rnge("corlst", i__1, "f_spkcor__", (ftnlen)761)) * 5, (
		    ftnlen)5, (ftnlen)5);
	    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, 
		    (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    5, (ftnlen)80);
	    tcase_(title, (ftnlen)80);

/*           Take four years' worth of samples, spaced 2 months apart. */

	    tdelta = jyear_() / 6;
	    nsamp = 24;
	    et0 = 0.;
	    i__1 = nsamp;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		et = et0 + (i__ - 1) * tdelta;

/*              Test SPKACS and its mirror routines: */

		spkacs_(&target, &et, ref, abcorr, &obsrvr, state, &lt, &dlt, 
			(ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkssb_(&obsrvr, &et, ref, stobs, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkltc_(&target, &et, ref, abcorr, stobs, state0, &lt0, &dlt0,
			 (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "SPKACS light time at ET #.", (ftnlen)80, (
			ftnlen)26);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt, "=", &lt0, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "SPKACS light time rate at ET #.", (ftnlen)80, (
			ftnlen)31);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt, "=", &dlt0, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);

/*              Check against SPKEZ: state should match that from SPKACS. */

		spkez_(&target, &et, ref, abcorr, &obsrvr, state1, &lt1, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "SPKACS state at ET #.", (ftnlen)80, (ftnlen)21)
			;
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state, "~~/", state1, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);

/*              Check against ZZSPKAC0: state, light time, and light */
/*              time rate should match those from SPKACS. */

		zzspkac0_(&target, &et, ref, abcorr, &obsrvr, state0, &lt0, &
			dlt0, (ftnlen)32, (ftnlen)5);
		s_copy(title, "ZZSPKAC0 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt0, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKAC0 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt0, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);

/*              Check state from ZZSPKAC0: should match that from SPKACS. */

		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "ZZSPKAC0 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state0, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);

/*              Check against ZZSPKAC1: state, light time, and light */
/*              time rate should match those from SPKACS. */

		zzspkac1_(&target, &et, ref, abcorr, &obsrvr, state1, &lt1, &
			dlt1, (ftnlen)32, (ftnlen)5);
		s_copy(title, "ZZSPKAC1 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt1, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKAC1 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt1, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);

/*              Check state from ZZSPKAC1: should match that from SPKACS. */

		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "ZZSPKAC1 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state1, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);

/*              Test SPKAPS and its mirror routines: */


/*              First look up the state and acceleration of the */
/*              observer relative to the solar system barycenter. */

		spkssb_(&obsrvr, &et, ref, stobs, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		d__1 = et - 1;
		spkssb_(&obsrvr, &d__1, ref, state0, (ftnlen)32);
		d__1 = et + 1;
		spkssb_(&obsrvr, &d__1, ref, state1, (ftnlen)32);
		qderiv_(&c__3, &state0[3], &state1[3], &c_b184, acc);

/*              Call SPKAPS; make sure its outputs are consistent with */
/*              those of SPKACS. */

		spkaps_(&target, &et, ref, abcorr, stobs, acc, state, &lt, &
			dlt, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkacs_(&target, &et, ref, abcorr, &obsrvr, state0, &lt0, &
			dlt0, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "SPKAPS light time at ET #.", (ftnlen)80, (
			ftnlen)26);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt, "~/", &lt0, &c_b52, ok, (ftnlen)80, (
			ftnlen)2);
		s_copy(title, "SPKAPS light time rate at ET #.", (ftnlen)80, (
			ftnlen)31);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt, "~/", &dlt0, &c_b52, ok, (ftnlen)80, (
			ftnlen)2);
		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "SPKAPS state at ET #.", (ftnlen)80, (ftnlen)21)
			;
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state, "~~/", state0, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);

/*              Check against ZZSPKAS0: state, light time, and light */
/*              time rate should match those from SPKAPS. */

		zzspkas0_(&target, &et, ref, abcorr, stobs, acc, state, &lt, &
			dlt, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "ZZSPKAS0 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt0, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKAS0 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt0, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "ZZSPKAS0 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state0, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);

/*              Check against ZZSPKAS1: state, light time, and light */
/*              time rate should match those from SPKAPS. */

		zzspkas1_(&target, &et, ref, abcorr, stobs, acc, state, &lt, &
			dlt, (ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(title, "ZZSPKAS1 light time at ET #.", (ftnlen)80, (
			ftnlen)28);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &lt0, "=", &lt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		s_copy(title, "ZZSPKAS1 light time rate at ET #.", (ftnlen)80,
			 (ftnlen)33);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chcksd_(title, &dlt0, "=", &dlt, &c_b33, ok, (ftnlen)80, (
			ftnlen)1);
		etcal_(&et, timstr, (ftnlen)40);
		s_copy(title, "ZZSPKAS1 state at ET #.", (ftnlen)80, (ftnlen)
			23);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)40, (ftnlen)80);
		chckad_(title, state0, "~~/", state, &c__6, &c_b52, ok, (
			ftnlen)80, (ftnlen)3);
	    }

/*           End of epoch loop. */

	}

/*        End of aberration correction loop. */

    }

/*     End of frame loop. */


/*     The fourth set of tests covers the routine ZZSTELAB and its frame */
/*     system mirror routines. This routine requires an inertial input */
/*     frame, so we omit the cases for the last frame in the list. */

    for (fr = 1; fr <= 2; ++fr) {
	for (ac = 1; ac <= 9; ++ac) {

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "ZZSTELAB: Earth-Mars stellar aberration/velocity;"
		    " frame #, abcorr #.", (ftnlen)80, (ftnlen)68);
	    obsrvr = 399;
	    target = 499;
	    s_copy(ref, frmlst + (((i__1 = fr - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("frmlst", i__1, "f_spkcor__", (ftnlen)981)) << 5), 
		    (ftnlen)32, (ftnlen)32);
	    s_copy(abcorr, corlst + ((i__1 = ac - 1) < 9 && 0 <= i__1 ? i__1 :
		     s_rnge("corlst", i__1, "f_spkcor__", (ftnlen)982)) * 5, (
		    ftnlen)5, (ftnlen)5);
	    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, 
		    (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    5, (ftnlen)80);
	    tcase_(title, (ftnlen)80);

/*           Parse the aberration correction flag; determine whether we */
/*           have a specification for reception or transmission */
/*           corrections. */

	    zzprscor_(abcorr, attblk, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xmit = attblk[4];

/*           Set up our ephemeris utilities to enable Cheby fitting. */

	    retval = t_stcini__(&target, ref, abcorr, &obsrvr, (ftnlen)32, (
		    ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Take four years' worth of samples, spaced 2 months apart. */

	    tdelta = jyear_() / 6;
	    nsamp = 24;
	    et0 = 0.;
	    halfwn = 300.;
	    degree = 12;
	    maxn = degree + 1;
	    delta = 1.;
	    i__1 = nsamp;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		et = et0 + (i__ - 1) * tdelta;

/*              Get the observer-target state without stellar */
/*              aberration correction. Also get the state of */
/*              the observer relative to the solar system barycenter. */

		spkssb_(&obsrvr, &et, ref, stobs, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkltc_(&target, &et, ref, abcorr, stobs, state, &lt, &dlt, (
			ftnlen)32, (ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute the acceleration of the observer with respect */
/*              to the solar system barycenter. */

		for (j = 1; j <= 2; ++j) {
		    t = et + ((j << 1) - 3) * delta;
		    spkgeo_(&obsrvr, &t, ref, &c__0, &ssbobs[(i__2 = j * 6 - 
			    6) < 12 && 0 <= i__2 ? i__2 : s_rnge("ssbobs", 
			    i__2, "f_spkcor__", (ftnlen)1037)], &ltssb, (
			    ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
		qderiv_(&c__3, &ssbobs[3], &ssbobs[9], &delta, acc);

/*              Get the stellar aberration correction and its time */
/*              derivative. Note that the input observer state relative */
/*              to the solar system barycenter was obtained from SPKLTC. */

		zzstelab_(&xmit, acc, &stobs[3], state, pcorr, dpcorr);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the stellar aberration offset and its time */
/*              derivative against those from an alternate */
/*              implementation. We expect a very close match. */

		t_zzstelab__(&xmit, acc, &stobs[3], state, pcorr2, dpcor2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("PCORR -0", pcorr, "~~/", pcorr2, &c__3, &c_b260, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("DPCORR -0", dpcorr, "~~/", dpcor2, &c__3, &c_b260, 
			ok, (ftnlen)9, (ftnlen)3);

/*              Check the stellar aberration offset and its time */
/*              derivative against those from a numeric */
/*              implementation. We expect a very good match */
/*              for the position correction and a near-single */
/*              precision match for the velocity correction. */

		t_zzstlabn__(&xmit, acc, &stobs[3], state, pcorr2, dpcor2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("PCORR -1", pcorr, "~~/", pcorr2, &c__3, &c_b270, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("DPCORR -1", dpcorr, "~~/", dpcor2, &c__3, &c_b274, 
			ok, (ftnlen)9, (ftnlen)3);

/*              Check the stellar aberration position offset against */
/*              that obtained from either of the legacy routines STELAB */
/*              and STLABX. We expect close agreement. */

		if (xmit) {
		    stlabx_(state, &stobs[3], corpos);
		    vsub_(corpos, state, pcorr2);
		} else {
		    stelab_(state, &stobs[3], corpos);
		    vsub_(corpos, state, pcorr2);
		}
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("PCORR -2", pcorr, "~~/", pcorr2, &c__3, &c_b270, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Fit a Cheby expansion of degree MAXDEG to the each */
/*              component of the stellar aberration correction over a */
/*              2*HALFWN time span centered at ET. We'll retain an */
/*              expansion of degree DEGREE. */

		left = et - halfwn;
		right = et + halfwn;
		for (j = 1; j <= 3; ++j) {
		    if (j == 1) {
			chbfit_((U_fp)t_getsx__, &left, &right, &c__13, work, 
				coeffs);
		    } else if (j == 2) {
			chbfit_((U_fp)t_getsy__, &left, &right, &c__13, work, 
				coeffs);
		    } else {
			chbfit_((U_fp)t_getsz__, &left, &right, &c__13, work, 
				coeffs);
		    }
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Find the derivatives of the current Cheby expansion. */

		    x2s[0] = et;
		    x2s[1] = halfwn;
		    i__2 = maxn - 1;
		    chbder_(coeffs, &i__2, x2s, &et, &c__1, partdp, derivs);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    pos[0] = derivs[0];
		    dpos[0] = derivs[1];

/*                 Note: to compute acceleration, change the argument */
/*                 "1" above to "2" and capture acceleration from */

/*                    DERIVS(2) */


/*                 Compare the expansion to the position correction */
/*                 from ZZSTELAB at ET. */

		    etcal_(&et, timstr, (ftnlen)40);
		    s_copy(title, "Position component # at ET #.", (ftnlen)80,
			     (ftnlen)29);
		    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)40, (ftnlen)80);
		    chcksd_(title, &pcorr[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("pcorr", i__2, "f_spkcor__", (
			    ftnlen)1166)], "~/", pos, &c_b270, ok, (ftnlen)80,
			     (ftnlen)2);

/*                 Compare the derivative of the expansion to the */
/*                 velocity from ZZSTELAB at ET. */

		    etcal_(&et, timstr, (ftnlen)40);
		    s_copy(title, "Velocity component # at ET #.", (ftnlen)80,
			     (ftnlen)29);
		    repmi_(title, "#", &j, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)80);
		    repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			    ftnlen)40, (ftnlen)80);

/*                 The relative error should be less than */
/*                 1.D-4. */

		    chcksd_(title, &dpcorr[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("dpcorr", i__2, "f_spkcor__", (
			    ftnlen)1183)], "~/", dpos, &c_b274, ok, (ftnlen)
			    80, (ftnlen)2);
/*                  IF ( .NOT. OK ) THEN */
/*                     WRITE (*,*) 'PCORR  = ', PCORR */
/*                     WRITE (*,*) 'DPCORR = ', DPCORR */
/*                  END IF */

/*                 Make sure the absolute velocity error is less */
/*                 than 10 microns/sec. */

		    chcksd_(title, &dpcorr[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("dpcorr", i__2, "f_spkcor__", (
			    ftnlen)1195)], "~", dpos, &c_b304, ok, (ftnlen)80,
			     (ftnlen)1);
		}

/*              End of correction component loop. */

	    }

/*           End of epoch loop. */

	}

/*        End of aberration correction loop. */

    }

/*     End of frame loop. */


/*     Error cases: */


/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKLTC, ZZSPKLT0, ZZSPKLT1: reject non-inertial frames", (
	    ftnlen)80, (ftnlen)54);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(abcorr, "LT", (ftnlen)5, (ftnlen)2);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkssb_(&obsrvr, &et, ref, stobs, (ftnlen)32);
    spkltc_(&target, &et, ref, abcorr, stobs, state, &lt, &dlt, (ftnlen)32, (
	    ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspklt0_(&target, &et, ref, abcorr, stobs, state, &lt, &dlt, (ftnlen)32, 
	    (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspklt1_(&target, &et, ref, abcorr, stobs, state, &lt, &dlt, (ftnlen)32, 
	    (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKACS, ZZSPKAC0, ZZSPKAC1: reject non-inertial frames", (
	    ftnlen)80, (ftnlen)54);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(abcorr, "LT", (ftnlen)5, (ftnlen)2);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkacs_(&target, &et, ref, abcorr, &obsrvr, state, &lt, &dlt, (ftnlen)32, 
	    (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspkac0_(&target, &et, ref, abcorr, &obsrvr, state, &lt, &dlt, (ftnlen)
	    32, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspkac1_(&target, &et, ref, abcorr, &obsrvr, state, &lt, &dlt, (ftnlen)
	    32, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKAPS, ZZSPKAS0, ZZSPKAS1: reject non-inertial frames", (
	    ftnlen)80, (ftnlen)54);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(abcorr, "LT", (ftnlen)5, (ftnlen)2);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkaps_(&target, &et, ref, abcorr, stobs, state, acc, &lt, &dlt, (ftnlen)
	    32, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspkas0_(&target, &et, ref, abcorr, stobs, state, acc, &lt, &dlt, (
	    ftnlen)32, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);
    zzspkas1_(&target, &et, ref, abcorr, stobs, state, acc, &lt, &dlt, (
	    ftnlen)32, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKAPS: reject S, XS, relativistic ABCORR values", (ftnlen)
	    80, (ftnlen)48);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(abcorr, "LT", (ftnlen)5, (ftnlen)2);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    for (i__ = 1; i__ <= 6; ++i__) {
	spkaps_(&target, &et, ref, badcor + ((i__1 = i__ - 1) < 6 && 0 <= 
		i__1 ? i__1 : s_rnge("badcor", i__1, "f_spkcor__", (ftnlen)
		1320)) * 5, stobs, state, acc, &lt, &dlt, (ftnlen)32, (ftnlen)
		5);
	chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    }

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKAPS: reject random, invalid ABCORR value", (ftnlen)80, (
	    ftnlen)43);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkaps_(&target, &et, ref, "XYZ", stobs, state, acc, &lt, &dlt, (ftnlen)
	    32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKACS: reject S, XS, relativistic ABCORR values", (ftnlen)
	    80, (ftnlen)48);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    for (i__ = 1; i__ <= 6; ++i__) {
	spkacs_(&target, &et, ref, badcor + ((i__1 = i__ - 1) < 6 && 0 <= 
		i__1 ? i__1 : s_rnge("badcor", i__1, "f_spkcor__", (ftnlen)
		1360)) * 5, &obsrvr, state, &lt, &dlt, (ftnlen)32, (ftnlen)5);
	chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    }

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKACS: reject random, invalid ABCORR value", (ftnlen)80, (
	    ftnlen)43);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkacs_(&target, &et, ref, "XYZ", &obsrvr, state, &lt, &dlt, (ftnlen)32, (
	    ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKLTC: reject S, XS, relativistic ABCORR values", (ftnlen)
	    80, (ftnlen)48);
    target = 499;
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    for (i__ = 1; i__ <= 6; ++i__) {
	spkltc_(&target, &et, ref, badcor + ((i__1 = i__ - 1) < 6 && 0 <= 
		i__1 ? i__1 : s_rnge("badcor", i__1, "f_spkcor__", (ftnlen)
		1399)) * 5, stobs, state, &lt, &dlt, (ftnlen)32, (ftnlen)5);
	chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    }

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKLTC: reject random, invalid ABCORR value", (ftnlen)80, (
	    ftnlen)43);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkltc_(&target, &et, ref, "XYZ", stobs, state, &lt, &dlt, (ftnlen)32, (
	    ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKEZ: reject S, XS, relativistic ABCORR values", (ftnlen)
	    80, (ftnlen)47);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    for (i__ = 1; i__ <= 6; ++i__) {
	spkez_(&target, &et, ref, badcor + ((i__1 = i__ - 1) < 6 && 0 <= i__1 
		? i__1 : s_rnge("badcor", i__1, "f_spkcor__", (ftnlen)1439)) *
		 5, &obsrvr, state, &lt, (ftnlen)32, (ftnlen)5);
	chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    }

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKEZ: reject random, invalid ABCORR value", (ftnlen)80, (
	    ftnlen)42);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkez_(&target, &et, ref, "XYZ", &obsrvr, state, &lt, (ftnlen)32, (ftnlen)
	    3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKEZ: bad frame name", (ftnlen)80, (ftnlen)21);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "IAU_MOOON", (ftnlen)32, (ftnlen)9);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkez_(&target, &et, ref, "LT+S", &obsrvr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "SPKEZ: REFCHG failure", (ftnlen)80, (ftnlen)21);
    obsrvr = 399;
    target = 499;
    s_copy(ref, "ITRF93", (ftnlen)32, (ftnlen)6);
    repmc_(title, "#", ref, title, (ftnlen)80, (ftnlen)1, (ftnlen)32, (ftnlen)
	    80);
    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)5, (
	    ftnlen)80);
    tcase_(title, (ftnlen)80);
    spkez_(&target, &et, ref, "LT+S", &obsrvr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/*     Clean up the kernels. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("phoenix.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_spkcor__ */

