/* f_spkcpv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b21 = 0.;
static doublereal c_b23 = 1.;
static integer c__0 = 0;
static integer c__6 = 6;
static integer c__3 = 3;
static doublereal c_b40 = 2.;
static doublereal c_b67 = 1e-15;
static integer c__14 = 14;
static doublereal c_b220 = 1e-12;
static doublereal c_b224 = 1e-10;
static integer c__36 = 36;

/* $Procedure F_SPKCPV ( Test constant velocity state routines ) */
/* Subroutine */ int f_spkcpv__(logical *ok)
{
    /* Initialized data */

    static char abcorr[25*9] = " nOne                    " " lT             "
	    "         " " xlT                     " "  Cn                     "
	     "  xCn                    " " Lt + s                  " " XLt +"
	    " s                 " "cN + S                   " "XcN + S       "
	    "           ";
    static char frames[32*5] = "J2000                           " "MARSIAU  "
	    "                       " "IAU_EARTH                       " "IAU"
	    "_MARS                        " "IAU_JUPITER                     ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal last;
    char corr[25];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    doublereal step, xdlt;
    logical xmit;
    extern /* Subroutine */ int mxvg_(doublereal *, doublereal *, integer *, 
	    integer *, doublereal *), zzcorepc_(char *, doublereal *, 
	    doublereal *, doublereal *, ftnlen), zzcvssta_(doublereal *, 
	    integer *, doublereal *, char *, ftnlen), zzcorsxf_(logical *, 
	    doublereal *, doublereal *, doublereal *);
    integer i__;
    extern /* Subroutine */ int zzcvxsta_(doublereal *, char *, integer *, 
	    doublereal *, ftnlen), zzprscor_(char *, logical *, ftnlen);
    integer n;
    doublereal y[3];
    char frame[32], segid[40];
    doublereal z__[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmc_(char *, char *, 
	    char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), repmd_(char *, 
	    char *, doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *),
	     repmi_(char *, char *, integer *, char *, ftnlen, ftnlen, ftnlen)
	    ;
    doublereal state[6];
    char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen), vlcom_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal first;
    char error[320];
    doublereal xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int spkw08_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen), spkez_(
	    integer *, doublereal *, char *, char *, integer *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), ucrss_(doublereal *, doublereal *, 
	    doublereal *), t_success__(logical *);
    doublereal epoch1, state0[6];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal et;
    char badcor[25];
    integer degree, handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    integer obscde;
    doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), delfil_(
	    char *, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    integer ctrcde, srfcde;
    char refloc[15], center[36], obsref[32];
    doublereal obsepc, trgepc;
    char target[36], trgref[32];
    doublereal ettarg, velmag;
    integer coridx, fixhan, frmidx;
    doublereal states[12]	/* was [6][2] */, tstate[6], xstate[6], 
	    tmpxfm[36]	/* was [6][6] */;
    integer srfhan;
    char obsrvr[36];
    integer timidx, outctr, trgcde;
    logical attblk[15];
    extern /* Subroutine */ int kilfil_(char *, ftnlen), tstpck_(char *, 
	    logical *, logical *, ftnlen), tparse_(char *, doublereal *, char 
	    *, ftnlen, ftnlen), srfrec_(integer *, doublereal *, doublereal *,
	     doublereal *), spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), tstspk_(char *, logical *, integer *, ftnlen), 
	    vsclip_(doublereal *, doublereal *), spkcls_(integer *), furnsh_(
	    char *, ftnlen), spkgeo_(integer *, doublereal *, char *, integer 
	    *, doublereal *, doublereal *, ftnlen), chcksi_(char *, integer *,
	     char *, integer *, integer *, logical *, ftnlen, ftnlen), 
	    unload_(char *, ftnlen), spkcvt_(doublereal *, doublereal *, char 
	    *, char *, doublereal *, char *, char *, char *, char *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), spkacs_(integer *, doublereal *, char *, char *, 
	    integer *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen), sxform_(char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), spkcvo_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    spkcpo_(char *, doublereal *, char *, char *, char *, doublereal *
	    , char *, char *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), spkcpt_(doublereal *, char *, 
	    char *, doublereal *, char *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    char ref[32];
    doublereal lat;
    extern doublereal rpd_(void);
    doublereal lon, tol, xlt;

/* $ Abstract */

/*     This routine performs a series of tests on the public and private */
/*     routines that find states of ephemeris objects relative to */
/*     objects having constant velocity or constant position relative to */
/*     a given center of motion in a specified reference frame. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routines */

/*        SPKCPO */
/*        SPKCPT */
/*        SPKCVO */
/*        SPKCVT */
/*        ZZCVSTAT (umbrella routine) */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     B.V. Semenov     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 02-MAY-2012 (NJB) (BVS) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_SPKCPV", (ftnlen)8);
/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    Set-up                                                     * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Create and load generic test SPK and PCK. */

    tcase_("Create generic test kernels.", (ftnlen)28);
    kilfil_("test.bsp", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("test.bsp", &c_true, &handle, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("test.tpc", &c_true, &c_false, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tparse_("1 JAN 1995", &et, error, (ftnlen)10, (ftnlen)320);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create pinpoint-style SPK file containing data for */
/*     a fast-moving "surface point" on the earth. This */
/*     point has constant velocity in the IAU_EARTH frame. */

    ctrcde = 399;
    lon = rpd_() * 60.;
    lat = rpd_() * 30.;
    srfrec_(&ctrcde, &lon, &lat, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b21, &c_b21, &c_b23, z__);
    ucrss_(z__, state0, y);

/*     Sharpen the Z component of the Y basis vector. */

    y[2] = 0.;

/*     Our object is moving---rather fast. */

    velmag = 5.;
    vscl_(&velmag, y, &state0[3]);
    state0[4] = state0[3] * 1.5f;
    state0[5] = state0[4] * 1.5f;
    kilfil_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkopn_("zzcvstat.bsp", "zzcvstat.bsp", &c__0, &srfhan, (ftnlen)12, (
	    ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfcde = 777;
    s_copy(frame, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    first = jyear_() * -10;
    last = jyear_() * 10;
    s_copy(segid, "Surface object, constant velocity", (ftnlen)40, (ftnlen)33)
	    ;
    degree = 1;
    n = 2;
    epoch1 = first;
    step = last - first;
    last = min(last,step);
    moved_(state0, &c__6, states);
    moved_(&state0[3], &c__3, &states[9]);
    vlcom_(&c_b23, state0, &step, &state0[3], &states[6]);
/*      WRITE (*,*) 'STATES = ', STATES */
    spkw08_(&srfhan, &srfcde, &ctrcde, frame, &first, &last, segid, &degree, &
	    n, states, &epoch1, &step, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a second segment for object 888. */

    srfcde = 888;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    ctrcde = 499;
    for (i__ = 1; i__ <= 2; ++i__) {
	vsclip_(&c_b40, &states[(i__1 = i__ * 6 - 6) < 12 && 0 <= i__1 ? i__1 
		: s_rnge("states", i__1, "f_spkcpv__", (ftnlen)387)]);
	vsclip_(&c_b40, &states[(i__1 = i__ * 6 - 3) < 12 && 0 <= i__1 ? i__1 
		: s_rnge("states", i__1, "f_spkcpv__", (ftnlen)388)]);
    }
    spkw08_(&srfhan, &srfcde, &ctrcde, frame, &first, &last, segid, &degree, &
	    n, states, &epoch1, &step, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&srfhan);
/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZCVSTAT tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Exercise the linear state extrapolation routines */
/*     contained in ZZCVSTAT. */

    tcase_("Exercise ZZCVSSTA, ZZCVXSTA. REF = IAU_EARTH", (ftnlen)44);
    furnsh_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    trgcde = 777;
    ctrcde = 399;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    first = jyear_() * -10;
    spkgeo_(&trgcde, &first, ref, &ctrcde, state0, &xlt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzcvssta_(state0, &ctrcde, &first, ref, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = jyear_() * 10.;
    spkgeo_(&trgcde, &et, ref, &ctrcde, xstate, &xlt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzcvxsta_(&et, ref, &outctr, state, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the correct center of motion is returned. */

    chcksi_("Center", &outctr, "=", &ctrcde, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the ouput state. We expect near equality. */

    chckad_("Position", state, "~~/", xstate, &c__3, &c_b67, ok, (ftnlen)8, (
	    ftnlen)3);
    chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &c_b67, ok, (
	    ftnlen)8, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*     Exercise the linear state extrapolation routines */
/*     contained in ZZCVSTAT. */

    tcase_("Exercise ZZCVSSTA, ZZCVXSTA. REF = J2000", (ftnlen)40);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    spkgeo_(&trgcde, &et, ref, &ctrcde, xstate, &xlt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzcvxsta_(&et, ref, &outctr, state, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the correct center of motion is returned. */

    chcksi_("Center", &outctr, "=", &ctrcde, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the ouput state. We expect near equality. */

    chckad_("Position", state, "~~/", xstate, &c__3, &c_b67, ok, (ftnlen)8, (
	    ftnlen)3);
    chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &c_b67, ok, (
	    ftnlen)8, (ftnlen)3);
    unload_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    SPKCVT tests                                               * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad observer ID.", (ftnlen)35);
    furnsh_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "NONE", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, trgref, refloc, corr, 
	    obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
	    15, (ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad center ID.", (ftnlen)33);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "NONE", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, trgref, refloc, corr, 
	    obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
	    15, (ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad frame locus", (ftnlen)34);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "3", (ftnlen)36, (ftnlen)1);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "NONE", (ftnlen)25, (ftnlen)4);
    spkcvt_(state0, &trgepc, center, trgref, &et, trgref, "XYZ", corr, obsrvr,
	     state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)3, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad aberration correction.", (ftnlen)45);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(badcor, "L+S", (ftnlen)25, (ftnlen)3);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, trgref, refloc, badcor, 
	    obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
	    15, (ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad target frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EART", (ftnlen)32, (ftnlen)8);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, trgref, refloc, corr, 
	    obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
	    15, (ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: bad output frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)15, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: no target center data.", (ftnlen)41);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(center, "333", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)15, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVT error case: no observer data.", (ftnlen)36);
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "666", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)15, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */



/*     SPKCVT normal cases: */


    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)780)) 
		    << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)805)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)817)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)822)) *
			 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                  WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                  WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)889)) 
		    << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)914)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)925)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)930)) *
			 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                  WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                  WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)994)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1000))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1025)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)1045)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)1054)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1079)) 
			* 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                  WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                  WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1141)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1147))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1172)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)1192)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)1201)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1226)) 
			* 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                  WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                  WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1287)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1293))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #", (ftnlen)
			320, (ftnlen)85);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1318)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)1337)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Transform the state in to frame REF at the observer */
/*              epoch. */

		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1351)) 
			* 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                  WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                  WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1410)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1416))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1441)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)1460)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Transform the state in to frame REF at the observer */
/*              epoch. */

		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvt_(state0, &trgepc, center, trgref, &et, ref, refloc, 
			abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1474)) 
			* 25, obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    SPKCVO tests                                               * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad target name.", (ftnlen)35);
    s_copy(target, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBERVER", (ftnlen)15, (ftnlen)7);
    spkcvo_(target, &et, ref, refloc, corr, state0, &obsepc, center, obsref, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)15, (ftnlen)25, (
	    ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad center name.", (ftnlen)35);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    s_copy(refloc, "OBERVER", (ftnlen)15, (ftnlen)7);
    spkcvo_(target, &et, ref, refloc, corr, state0, &obsepc, center, obsref, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)15, (ftnlen)25, (
	    ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad locus", (ftnlen)28);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcvo_(target, &et, ref, "XYZ", corr, state0, &obsepc, center, obsref, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)3, (ftnlen)25, (
	    ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad aberration correction.", (ftnlen)45);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(badcor, "L+S", (ftnlen)25, (ftnlen)3);
    spkcvo_(target, &et, ref, "OBSERVER", badcor, state0, &obsepc, center, 
	    obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25,
	     (ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad observer frame name", (ftnlen)42);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EART", (ftnlen)32, (ftnlen)8);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcvo_(target, &et, ref, "OBSERVER", corr, state0, &obsepc, center, 
	    obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25,
	     (ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: bad output frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcvo_(target, &et, ref, "OBSERVER", corr, state0, &obsepc, center, 
	    obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25,
	     (ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: no target data.", (ftnlen)34);
    s_copy(target, "666", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcvo_(target, &et, ref, "OBSERVER", corr, state0, &obsepc, center, 
	    obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25,
	     (ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCVO error case: no observer center data.", (ftnlen)43);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(center, "333", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcvo_(target, &et, ref, "OBSERVER", corr, state0, &obsepc, center, 
	    obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25,
	     (ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     SPKCVO normal cases: */


    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1758)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1763))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #", (ftnlen)
			320, (ftnlen)85);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1788)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)1799)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)1804)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1874)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1880))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)1905)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)1918)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)1923)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &tol, ok, (ftnlen)10, (
			ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)1992)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)1998))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)2023)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)2044)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)2053)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)2078)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)2148)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)2154))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #", (ftnlen)
			320, (ftnlen)85);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)2179)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)2200)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)2209)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)2234)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)2304)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)2310))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #", (ftnlen)
			320, (ftnlen)85);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)2335)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */
		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)2354)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Since the output frame evaluation epoch is the */
/*              same as the observation epoch, there's no need */
/*              to adjust the frame transformation matrix for the */
/*              rate of change of light time. */

		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)2372)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)2441)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)2447))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCVO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)2472)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */
		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)2492)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Since the output frame evaluation epoch is the */
/*              same as the observation epoch, there's no need */
/*              to adjust the frame transformation matrix for the */
/*              rate of change of light time. */

		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcvo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)2510)) * 25, state0, &obsepc, 
			center, obsref, state, &lt, (ftnlen)36, (ftnlen)32, (
			ftnlen)15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Because of the very large observer speed in the J2000 */
/*              frame, cases with stellar aberration correction are */
/*              going to have sloppy velocity. Make allowances. */

		if (attblk[2]) {
		    tol = 1e-7;
		} else {
		    tol = 1e-10;
		}
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &tol,
			 ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    SPKCPO tests                                               * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create a fixed-point SPK for CP* testing.", (ftnlen)41);

/*     Create pinpoint-style SPK file containing data for */
/*     a fixed "surface point" on the earth. This */
/*     point has constant position in the IAU_EARTH frame. */

    ctrcde = 399;
    lon = rpd_() * 60.;
    lat = rpd_() * 30.;
    srfrec_(&ctrcde, &lon, &lat, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, &state0[3]);
    kilfil_("zzcvstat2.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkopn_("zzcvstat2.bsp", "zzcvstat2.bsp", &c__0, &fixhan, (ftnlen)13, (
	    ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfcde = 777;
    s_copy(frame, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    first = jyear_() * -10;
    last = jyear_() * 10;
    s_copy(segid, "Surface object, constant velocity", (ftnlen)40, (ftnlen)33)
	    ;
    degree = 1;
    n = 2;
    epoch1 = first;
    step = last - first;
    last = min(last,step);
    moved_(state0, &c__6, states);
    moved_(state0, &c__6, &states[6]);
    spkw08_(&fixhan, &srfcde, &ctrcde, frame, &first, &last, segid, &degree, &
	    n, states, &epoch1, &step, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a second segment for object 888. */

    srfcde = 888;
    s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    ctrcde = 499;
    for (i__ = 1; i__ <= 2; ++i__) {
	vsclip_(&c_b40, &states[(i__1 = i__ * 6 - 6) < 12 && 0 <= i__1 ? i__1 
		: s_rnge("states", i__1, "f_spkcpv__", (ftnlen)2625)]);
	vsclip_(&c_b40, &states[(i__1 = i__ * 6 - 3) < 12 && 0 <= i__1 ? i__1 
		: s_rnge("states", i__1, "f_spkcpv__", (ftnlen)2626)]);
    }
    spkw08_(&fixhan, &srfcde, &ctrcde, frame, &first, &last, segid, &degree, &
	    n, states, &epoch1, &step, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&fixhan);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad target name", (ftnlen)34);
    furnsh_("zzcvstat2.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(target, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad center name", (ftnlen)34);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad locus", (ftnlen)28);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "xyz", corr, state0, center, obsref, state, &lt,
	     (ftnlen)36, (ftnlen)32, (ftnlen)3, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad aberration correction.", (ftnlen)45);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(badcor, "L+S", (ftnlen)25, (ftnlen)3);
    spkcpo_(target, &et, ref, "OBSERVER", badcor, state0, center, obsref, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (
	    ftnlen)36, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad observer frame name", (ftnlen)42);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EART", (ftnlen)32, (ftnlen)8);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: bad output frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: no target data.", (ftnlen)34);
    s_copy(target, "666", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPO error case: no observer center data.", (ftnlen)43);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(center, "333", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    obsepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpo_(target, &et, ref, "OBSERVER", corr, state0, center, obsref, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)8, (ftnlen)25, (ftnlen)36, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     SPKCPO normal cases: */



/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)2890)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)2895))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)2920)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)2934)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)2939)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)2999)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3005))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3030)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)3044)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)3049)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b224, ok, (ftnlen)8, (ftnlen)3);
		if (! (*ok)) {
/*                 WRITE (*,*) 'SPKEZ: XSTATE = ', XSTATE */
/*                 WRITE (*,*) 'SPKCVT: STATE = ', STATE */
		}
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */
    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)3115)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3121))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3146)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)3169)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)3178)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)3203)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)3261)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3267))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3292)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)3315)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)3324)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)3349)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    obscde = 777;
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    trgcde = 301;
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)3410)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3416))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3441)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */
		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)3461)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Since the output frame evaluation epoch is the */
/*              same as the observation epoch, there's no need */
/*              to adjust the frame transformation matrix for the */
/*              rate of change of light time. */

		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)3479)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    obscde = 888;
    s_copy(obsrvr, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    trgcde = 6;
    s_copy(target, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    obsepc = jyear_() * -9;
    s_copy(obsref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&obscde, &obsepc, obsref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)3539)) * 25, 
		attblk, (ftnlen)25);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3545))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPO: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3570)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */
		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)3590)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Since the output frame evaluation epoch is the */
/*              same as the observation epoch, there's no need */
/*              to adjust the frame transformation matrix for the */
/*              rate of change of light time. */

		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpo_(target, &et, ref, refloc, abcorr + ((i__1 = coridx - 
			1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, 
			"f_spkcpv__", (ftnlen)3608)) * 25, state0, center, 
			obsref, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			15, (ftnlen)25, (ftnlen)36, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    SPKCPT tests                                               * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad observer name", (ftnlen)36);
    cleard_(&c__6, state0);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "XYZ", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, trgref, "OBSERVER", corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad center name", (ftnlen)34);
    cleard_(&c__6, state0);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "xyz", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, trgref, "OBSERVER", corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad locus", (ftnlen)28);
    cleard_(&c__6, state0);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "earth", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, trgref, "XYZ", corr, obsrvr, state, &
	    lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)3, (ftnlen)25, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad aberration correction.", (ftnlen)45);
    cleard_(&c__6, state0);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(badcor, "L+S", (ftnlen)25, (ftnlen)3);
    spkcpt_(state0, center, trgref, &et, trgref, "OBSERVER", badcor, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad target frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EART", (ftnlen)32, (ftnlen)8);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, trgref, "OBSERVER", corr, obsrvr, 
	    state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (
	    ftnlen)25, (ftnlen)36);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: bad output frame name", (ftnlen)40);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, ref, "OBSERVER", corr, obsrvr, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (ftnlen)25, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: no target center data.", (ftnlen)41);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(center, "333", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "777", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, ref, "OBSERVER", corr, obsrvr, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (ftnlen)25, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SPKCPT error case: no observer data.", (ftnlen)36);
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "666", (ftnlen)36, (ftnlen)3);
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    trgepc = jyear_() * -9;
    s_copy(corr, "LT+S", (ftnlen)25, (ftnlen)4);
    spkcpt_(state0, center, trgref, &et, ref, "OBSERVER", corr, obsrvr, state,
	     &lt, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)8, (ftnlen)25, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     SPKCPT normal cases: */



/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
/*      OBSCDE = 499 */
/*      OBSRVR = 'MARS' */
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)3919))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)3944)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)3958)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)3963)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "CENTER", (ftnlen)15, (ftnlen)6);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)4025))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)4050)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv"
			"__", (ftnlen)4062)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)4067)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)4125)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)4131))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)4156)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)4177)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)4186)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)4211)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b224, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "TARGET", (ftnlen)15, (ftnlen)6);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)4270)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)4276))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)4301)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame, and to get the */
/*              light time rate and applicable target epoch. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)4321)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the epoch only if light time corrections are used. */

		if (attblk[1]) {
		    zzcorepc_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ?
			     i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			    ftnlen)4330)) * 25, &et, &xlt, &ettarg, (ftnlen)
			    25);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    ettarg = et;
		}
		sxform_("J2000", ref, &ettarg, tmpxfm, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Adjust the rotation derivative only if light time */
/*              corrections are used. */

		if (attblk[1]) {
		    zzcorsxf_(&xmit, &xdlt, tmpxfm, xform);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    moved_(tmpxfm, &c__36, xform);
		}
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)4355)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    trgcde = 777;
    s_copy(target, "777", (ftnlen)36, (ftnlen)3);
    ctrcde = 399;
    s_copy(center, "EARTH", (ftnlen)36, (ftnlen)5);
    obscde = 301;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)4412)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)4418))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)4443)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)4462)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Transform the state in to frame REF at the observer */
/*              epoch. */

		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)4476)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    s_copy(refloc, "OBSERVER", (ftnlen)15, (ftnlen)8);
    trgcde = 888;
    s_copy(target, "888", (ftnlen)36, (ftnlen)3);
    ctrcde = 499;
    s_copy(center, "MARS", (ftnlen)36, (ftnlen)4);
    obscde = 6;
    s_copy(obsrvr, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    trgepc = jyear_() * -9;
    s_copy(trgref, "IAU_MARS", (ftnlen)32, (ftnlen)8);

/*     Look up the initial state from our SPK file. */

    spkgeo_(&trgcde, &trgepc, trgref, &ctrcde, state0, &lt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	zzprscor_(abcorr + ((i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("abcorr", i__1, "f_spkcpv__", (ftnlen)4532)) * 25, 
		attblk, (ftnlen)25);
	xmit = attblk[4];
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_spkcpv__", (ftnlen)4538))
		     << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 5; ++timidx) {
		et = (timidx - 10) * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "SPKCPT: Target = #; Observer = #; Ref = #; Ce"
			"nter = #; Abcorr = #; ET = #; REFLOC = #.", (ftnlen)
			320, (ftnlen)86);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_spkcpv__", (
			ftnlen)4563)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", refloc, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)15, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Creating the expected state takes a bit of work now, */
/*              since the SPK method of choosing the output frame's */
/*              evaluation epoch isn't sufficient. We'll use SPKACS to */
/*              obtain the state in an inertial frame. Then we'll */
/*              transform the state to the desired output frame */
/*              manually. */

		spkacs_(&trgcde, &et, "J2000", abcorr + ((i__1 = coridx - 1) <
			 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_s"
			"pkcpv__", (ftnlen)4582)) * 25, &obscde, tstate, &xlt, 
			&xdlt, (ftnlen)5, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Transform the state in to frame REF at the observer */
/*              epoch. */

		sxform_("J2000", ref, &et, xform, (ftnlen)5, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		mxvg_(xform, tstate, &c__6, &c__6, xstate);
		spkcpt_(state0, center, trgref, &et, ref, refloc, abcorr + ((
			i__1 = coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"abcorr", i__1, "f_spkcpv__", (ftnlen)4596)) * 25, 
			obsrvr, state, &lt, (ftnlen)36, (ftnlen)32, (ftnlen)
			32, (ftnlen)15, (ftnlen)25, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b220, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b220, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b220, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up SPK files", (ftnlen)18);

/*     GENSPK is supposed to be cleaned up automatically. */
/*     If this fails, use the following: */

/*      CALL SPKUEF ( HANDLE ) */
/*      CALL CHCKXC ( .FALSE., ' ', OK ) */

/*      CALL DELFIL ( GENSPK ) */
/*      CALL CHCKXC ( .FALSE., ' ', OK ) */
    unload_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzcvstat.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzcvstat2.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzcvstat2.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_spkcpv__ */

