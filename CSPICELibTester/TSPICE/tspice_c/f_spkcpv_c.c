/*

-Procedure f_spkcpv_c ( Test wrappers for SPKCV*, SPKCP* )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the SPK constant 
   velocity and constant position routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_spkcpv_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for the SPK routines

      spkcpo_c {SPK, Constant position observer} 
      spkcpt_c {SPK, Constant position target} 
      spkcvo_c {SPK, Constant velocity observer} 
      spkcvt_c {SPK, Constant velocity target} 
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   B.V. Semenov    (JPL)
 
-Version 

   -tspice_c Version 1.0.0 27-MAR-2012 (NJB) (BVS)

-Index_Entries

   test SPK constant velocity and position wrappers

-&
*/

{ /* Begin f_spkcpv_c */

 
   /*
   Constants
   */
   #define BDNMLN          37
   #define FRNMLN          33
   #define LNSIZE          81
   #define MSGLEN          501
   #define NCORR           9
   #define NEVL            3
   #define NLINES          15
   #define NOBJ            2
   #define NREF            3
   #define NTIMES          20
   #define TIMFMT          "YYYY MON DD HR:MN:SC.###### TDB::TDB"
   #define TIMLEN          41
   #define PCK             "test_spkcpv.tpc"
   #define SPK             "test_spkcpv.bsp"
   #define SPK1            "site_spkcpv.bsp"

   #define TIGHT           5.0e-13
   #define MED             5.0e-12
   #define LOOSE           5.0e-11

   /*
   Local variables
   */
   SpiceBoolean            cond0;
   SpiceBoolean            cond1;
   SpiceBoolean            cond2;
   SpiceBoolean            cond3;


   SpiceChar             * abcorr;
   SpiceChar             * evlref;
   SpiceChar               title  [ MSGLEN ];
   SpiceChar               title1 [ MSGLEN ];
   SpiceChar             * obsctr;
   SpiceChar             * obsref;
   SpiceChar             * obsrvr;
   SpiceChar             * outref;
   SpiceChar             * segid;
   SpiceChar             * srfnam;
   SpiceChar             * target;
   SpiceChar             * templt;
   SpiceChar               timstr [ TIMLEN ];
   SpiceChar             * trgctr;
   SpiceChar             * trgref;

   SpiceChar        txtbuf  [ NLINES ] [LNSIZE ] = 
      {
         "FRAME_DSS-14_TOPO                     = 1399014",
         "FRAME_1399014_NAME                    = 'DSS-14_TOPO'",
         "FRAME_1399014_CLASS                   = 4",
         "FRAME_1399014_CLASS_ID                = 1399014",
         "FRAME_1399014_CENTER                  = 399014",
         " ",
         "OBJECT_399014_FRAME                   = 'DSS-14_TOPO'",
         " ",
         "TKFRAME_DSS-14_TOPO_RELATIVE          = 'EARTH_FIXED'",
         "TKFRAME_DSS-14_TOPO_SPEC              = 'ANGLES'",
         "TKFRAME_DSS-14_TOPO_UNITS             = 'DEGREES'",
         "TKFRAME_DSS-14_TOPO_AXES              = ( 3, 2, 3)",
         "TKFRAME_DSS-14_TOPO_ANGLES            = ( -243.1104612607222,",
         "                                          -54.5740991182250,",
         "                                          180.0000000000000  )"
      };


   static SpiceChar      * corlst[] = 
                           { "NONE", 
                             "CN",
                             "CN+S",
                             "XCN",
                             "XCN+S",
                             "LT",
                             "LT+S",
                             "XLT",
                             "XLT+S" };

   static SpiceChar      * evllst[] = 
                           { "observer", "target", "center" };

   static SpiceChar      * obslst[] = 
                           { "DSS-14", "moon" };

   static SpiceChar      * reflst[] = 
                           { "J2000", "IAU_EARTH", "IAU_MOON" };

   static SpiceChar      * trglst[] = 
                           { "moon", "DSS-14" };


   SpiceDouble             delta;
   SpiceDouble             et;
   SpiceDouble             et0;
   SpiceDouble             first;
   SpiceDouble             last;
   SpiceDouble             lt;
   SpiceDouble             obsepc;
   SpiceDouble             obspos [ 3 ];
   SpiceDouble             obssta [ 6 ];
   SpiceDouble             state  [ 6 ];


   /*
   These states were extracted from 
 
      earthstns_itrf93_050714.bsp

   using Spy. The correspond to the epochs

      1950 JAN 01 00:00:00.000 TDB
      2050 JAN 01 00:00:00.000 TDB
   */

   SpiceDouble             states [ 2 ][ 6 ] = {

                        { -0.23536204649238493e+04,
                          -0.46413418153895900e+04,
                           0.36770525213592941e+04,
                          -0.57085700433493031e-12,
                           0.20549069003980022e-12,
                          -0.12170732248333206e-12 },
                        
                        { -0.23536222664115494e+04,
                          -0.46413411669102898e+04,
                           0.36770521372801941e+04,
                          -0.57085700433493031e-12,
                           0.20549069003980022e-12,
                          -0.12170732248333206e-12 }  };
                        

   SpiceDouble             step;
   SpiceDouble             tol;
   SpiceDouble             trgepc;
   SpiceDouble             trgpos [ 3 ];
   SpiceDouble             trgsta [ 6 ];
   SpiceDouble             xlt;
   SpiceDouble             xstate [ 6 ];

   SpiceInt                center;
   SpiceInt                coridx;
   SpiceInt                evidx;
   SpiceInt                objidx;
   SpiceInt                refidx;
   SpiceInt                srfid;
   SpiceInt                timidx;

   SpiceInt                handle;
   SpiceInt                siteHan;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_spkcpv_c" );
   


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Setup: create kernels." );  

   /*
   Create and load the generic test kernels. 
   */
   tstlsk_c();
   chckxc_c( SPICEFALSE, " ", ok );

   tstspk_c ( SPK, SPICETRUE, &handle );   
   chckxc_c( SPICEFALSE, " ", ok );

   tstpck_c ( PCK, SPICETRUE, SPICEFALSE );
   chckxc_c( SPICEFALSE, " ", ok );


   /*
   Create the site (surface point) SPK file. 

   The segment bounds

      1950 JAN 01 00:00:00.000 TDB
      2050 JAN 01 00:00:00.000 TDB

   are from the earth station SPK cited above.
   */

   first  = -0.15778800000000000e+10;
   last   =  0.15778800000000000e+10;
   
   step   = last - first + 1e-3;

   srfnam = "DSS-14";
   srfid  = 399014;
   center = 399;
   trgref = "IAU_EARTH";

   if ( exists_c(SPK1) )
   {
      removeFile ( SPK1 );
   }

   spkopn_c ( SPK1, SPK1, 0, &siteHan );
   chckxc_c( SPICEFALSE, " ", ok );
   
   segid  = "DSS-14 test segment";

   spkw08_c ( siteHan, srfid, center, trgref, first, last,
              segid,   1,     2,      states, first, step  );
   chckxc_c( SPICEFALSE, " ", ok );

   spkcls_c ( siteHan );
   chckxc_c( SPICEFALSE, " ", ok );

   /*
   Load the site SPK file. 
   */
   furnsh_c ( SPK1 );
   chckxc_c( SPICEFALSE, " ", ok );


   /*
   Load the DSS-14_TOPO frame definition.

   We'll do this through the LMPOOL API---no kernels are needed.
   */

   
   lmpool_c ( txtbuf, LNSIZE, NLINES );
   chckxc_c( SPICEFALSE, " ", ok );
   



   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */

   /*
   chckxc_c( SPICEFALSE, " ", ok );

       chcksd_c ( qname, endpts[1], "~", xtime, TIMTOL, ok );
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Normal case setup." );  


   /*   
   Test each API, comparing results to those obtainable 
   from spkezr_c.

   Loop over

      evlref values
         
        target-illumination source pairs

           aberration corrections

              output frames 

                 times
   */

   trgepc    =  0.0;
   obsepc    =  trgepc;
   trgref    = "IAU_EARTH";
   obsref    = trgref;
   trgctr    = "EARTH";
   obsctr    = trgctr;
   
   /*
   Initialize the surface point state. 
   */
   spkezr_c ( srfnam, trgepc, trgref, 
              "NONE", trgctr, trgsta, &lt );

   chckxc_c( SPICEFALSE, " ", ok );

   /*
   The observer state is the surface point state as well. 
   */
   memmove ( obssta, trgsta, 6*sizeof(SpiceDouble) );

   


   et0   = 0.0;
   delta = jyear_c() / 4;
      

   for ( evidx = 0;  evidx < NEVL;  evidx++ )
   {
      evlref = evllst[ evidx ];


      for ( objidx = 0;  objidx < NOBJ;  objidx++ )
      {
         obsrvr = obslst[ objidx ];
         target = trglst[ objidx ];


         for ( coridx = 0;  coridx < NCORR;  coridx++ ) 
         {          
            abcorr = corlst[ coridx ];


            for ( refidx = 0;  refidx < NREF;  refidx++ )
            {
               outref = reflst[ refidx ];


               for ( timidx = 0;  timidx < NTIMES;  timidx++ )
               {
                  et = et0  + ( timidx * delta );

                  timout_c ( et, TIMFMT, TIMLEN, timstr );


                  /*
                  Create a test case title message. 
                  */
                  templt = "RNAME test: evlref = #; obsrvr = #; "
                           "target = #; abcorr = #; outref = #; "
                           "time = #";

                  strncpy ( title1, templt, MSGLEN );

                  repmc_c ( title1, "#", evlref, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );

                  repmc_c ( title1, "#", obsrvr, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );

                  repmc_c ( title1, "#", target, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );

                  repmc_c ( title1, "#", abcorr, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );

                  repmc_c ( title1, "#", outref, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );

                  repmc_c ( title1, "#", timstr, MSGLEN, title1 );
                  chckxc_c( SPICEFALSE, " ", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
                  /*
                  Test spkcvt_c. 
                  */

                  /*
                  This case is applicable only if:

                     - The target is the surface point
                   
                     - One of the following is true:

                        > evlref is "OBSERVER" and outref is
                          "J2000" or "IAU_MOON"

                        > evlref is "CENTER"

                        > evlref is "TARGET" and outref is "J2000"
                          or "DSS-14_TOPO"
                       
                  */

                  cond0 =  eqstr_c( target, srfnam );


                  cond1 =  eqstr_c( evlref, "OBSERVER" ) &&

                           (    eqstr_c( outref, "J2000"     ) 
                             || eqstr_c( outref, "IAU_MOON"  )  );


                  cond2 = eqstr_c( evlref, "CENTER" );


                  cond3 =  eqstr_c( evlref, "TARGET" ) &&

                           (    eqstr_c( outref, "J2000"       ) 
                             || eqstr_c( outref, "DSS-14_TOPO" )  );
                  

                  if (   cond0  &&  ( cond1 || cond2 || cond3 )   )
                  {
                     strncpy ( title, title1, MSGLEN );           

                     repmc_c ( title, "RNAME", "spkcvt_c", MSGLEN, title );
                     chckxc_c( SPICEFALSE, " ", ok );

                     tcase_c ( title );  

                     /*
                     Look up the state using spkcvt_c.
                     */
                     spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
                                outref, evlref, abcorr, obsrvr, state, &lt );

                     chckxc_c( SPICEFALSE, " ", ok );


                     if ( *ok ) 
                     {
                        /*
                        Look up the expected state and light time. 
                        */
                        spkezr_c ( srfnam, et,     outref, 
                                   abcorr, obsrvr, xstate, &xlt );

                        chckxc_c( SPICEFALSE, " ", ok );
                   
                        /*
                        Set position tolerance. Use looser tolerance for LT
                        corrections.
                        */

                        if ( coridx > 4 ) 
                        {
                           tol = MED;
                        }
                        else
                        {
                           tol = TIGHT;
                        }

                        /*
                        Check the position vector. 
                        */
                        chckad_c ( "Position", state,    "~~/", 
                                               xstate,   3, tol, ok );

                        /*
                        Check the velocity vector. 
                        */
                        chckad_c ( "Velocity", state+3,  "~~/", 
                                               xstate+3, 3, MED, ok );

                        /*
                        Check light time. 
                        */
                        chcksd_c ( "LT", lt, "~/", xlt, tol, ok );

                     }
                     

                  }

   /* 
   ---- Case ---------------------------------------------------------
   */

                  /*
                  Test spkcpt_c. 
                  */


                  /*
                  This case is applicable only if:

                     - The target is the surface point.
                   
                     - One of the following is true:

                        > evlref is "OBSERVER" and outref is
                          "J2000" or "IAU_MOON"

                        > evlref is "CENTER"

                        > evlref is "TARGET" and outref is "J2000"
                          or "DSS-14_TOPO"
                       
                  */

                  cond0 =  eqstr_c( target, srfnam );


                  cond1 =  eqstr_c( evlref, "OBSERVER" ) &&

                           (    eqstr_c( outref, "J2000"     ) 
                             || eqstr_c( outref, "IAU_MOON"  )  );


                  cond2 = eqstr_c( evlref, "CENTER" );


                  cond3 =  eqstr_c( evlref, "TARGET" ) &&

                           (    eqstr_c( outref, "J2000"       ) 
                             || eqstr_c( outref, "DSS-14_TOPO" )  );
                  

                  if (   cond0  &&  ( cond1 || cond2 || cond3 )   )
                  {
                     strncpy ( title, title1, MSGLEN );           

                     repmc_c ( title, "RNAME", "spkcpt_c", MSGLEN, title );
                     chckxc_c( SPICEFALSE, " ", ok );

                     tcase_c ( title );  


                     /*
                     Set the surface point position. 
                     */
                     spkpos_c ( srfnam, et,     trgref, 
                                "NONE", trgctr, trgpos, &lt );

                     chckxc_c( SPICEFALSE, " ", ok );


                     /*
                     Look up the state using spkcpt_c.
                     */
                     spkcpt_c ( trgpos, trgctr, trgref, et,
                                outref, evlref, abcorr, obsrvr, state, &lt );

                     chckxc_c( SPICEFALSE, " ", ok );


                     if ( *ok ) 
                     {
                        /*
                        Look up the expected state and light time. 
                        */
                        spkezr_c ( srfnam, et,     outref, 
                                   abcorr, obsrvr, xstate, &xlt );

                        chckxc_c( SPICEFALSE, " ", ok );
                   
                        /*
                        Set position tolerance. Use looser tolerance for LT
                        corrections.
                        */

                        if ( coridx > 4 ) 
                        {
                           tol = MED;
                        }
                        else
                        {
                           tol = TIGHT;
                        }

                        /*
                        Check the position vector. 
                        */
                        chckad_c ( "Position", state,    "~~/", 
                                               xstate,   3, tol, ok );

                        /*
                        Check the velocity vector. 
                        */
                        chckad_c ( "Velocity", state+3,  "~~/", 
                                               xstate+3, 3, LOOSE, ok );

                        /*
                        Check light time. 
                        */
                        chcksd_c ( "LT", lt, "~/", xlt, tol, ok );

                     }
                     

                  }

 


   /* 
   ---- Case ---------------------------------------------------------
   */

                  /*
                  Test spkcvo_c. 
                  */

                  /*
                  This case is applicable only if:

                     - The target is the moon.
                   
                     - One of the following is true:

                        > evlref is "OBSERVER" and outref is
                          "J2000" or "DSS-14_TOPO"

                        > evlref is "CENTER"

                        > evlref is "TARGET" and outref is "J2000"
                          or "IAU_MOON"
                       
                  */

                  cond0 =  eqstr_c( target, "moon" );


                  cond1 =  eqstr_c( evlref, "OBSERVER" ) &&

                           (    eqstr_c( outref, "J2000"        ) 
                             || eqstr_c( outref, "DSS-14_TOPO"  )  );


                  cond2 = eqstr_c( evlref, "CENTER" );


                  cond3 =  eqstr_c( evlref, "TARGET" ) &&

                           (    eqstr_c( outref, "J2000"    ) 
                             || eqstr_c( outref, "IAU_MOON" )  );
                 

                  if (   cond0  &&  ( cond1 || cond2 || cond3 )   )
                  {
                     strncpy ( title, title1, MSGLEN );           

                     repmc_c ( title, "RNAME", "spkcvo_c", MSGLEN, title );
                     chckxc_c( SPICEFALSE, " ", ok );

                     tcase_c ( title );  

                     /*
                     Look up the surface point-target state using spkcvo_c.
                     */
                     spkcvo_c ( target, et,     outref, evlref, abcorr, 
                                obssta, obsepc, obsctr, obsref, state, &lt );

                     chckxc_c( SPICEFALSE, " ", ok );


                     if ( *ok ) 
                     {
                        /*
                        Look up the expected state and light time. 
                        */
                        spkezr_c ( target, et,     outref, 
                                   abcorr, srfnam, xstate, &xlt );

                        chckxc_c( SPICEFALSE, " ", ok );
                   
                        /*
                        Set position tolerance. Use looser tolerance for LT
                        corrections.
                        */

                        if ( coridx > 4 ) 
                        {
                           tol = MED;
                        }
                        else
                        {
                           tol = TIGHT;
                        }

                        /*
                        Check the position vector. 
                        */
                        chckad_c ( "Position", state,    "~~/", 
                                               xstate,   3, tol, ok );


                        /*
                        if ( !*ok ) 
                        {
                           printf ( "pos:  %f %f %f \n",
                                    state[0],state[1],state[2] );

                           printf ( "xpos: %f %f %f \n", 
                                     xstate[0], xstate[1], xstate[2] );

                           printf ( "obs pos:  %f %f %f \n", 
                                    obssta[0],obssta[1],obssta[2] );
                        }
                        */


                        /*
                        Check the velocity vector. 
                        */
                        chckad_c ( "Velocity", state+3,  "~~/", 
                                               xstate+3, 3, MED, ok );

                        /*
                        Check light time. 
                        */
                        chcksd_c ( "LT", lt, "~/", xlt, tol, ok );

                     }              
                  }



   /* 
   ---- Case ---------------------------------------------------------
   */

                  /*
                  Test spkcpo_c. 
                  */
                  /*
                  This case is applicable only if:

                     - The target is the moon.
                   
                     - One of the following is true:

                        > evlref is "OBSERVER" and outref is
                          "J2000" or "DSS-14_TOPO"

                        > evlref is "CENTER"

                        > evlref is "TARGET" and outref is "J2000"
                          or "IAU_MOON"
                       
                  */

                  cond0 =  eqstr_c( target, "moon" );


                  cond1 =  eqstr_c( evlref, "OBSERVER" ) &&

                           (    eqstr_c( outref, "J2000"        ) 
                             || eqstr_c( outref, "DSS-14_TOPO"  )  );


                  cond2 = eqstr_c( evlref, "CENTER" );


                  cond3 =  eqstr_c( evlref, "TARGET" ) &&

                           (    eqstr_c( outref, "J2000"    ) 
                             || eqstr_c( outref, "IAU_MOON" )  );
                 

                  if (   cond0  &&  ( cond1 || cond2 || cond3 )   )
                  {
                     strncpy ( title, title1, MSGLEN );           

                     repmc_c ( title, "RNAME", "spkcvp_c", MSGLEN, title );
                     chckxc_c( SPICEFALSE, " ", ok );

                     tcase_c ( title );  



                     /*
                     Set the surface point position. 
                     */
                     spkpos_c ( srfnam, et,     obsref, 
                                "NONE", obsctr, obspos, &lt );

                     chckxc_c( SPICEFALSE, " ", ok );



                     /*
                     Look up the surface point-target state using spkcpo_c.
                     */

                     spkcpo_c ( target, et,     outref, evlref, abcorr, 
                                obspos, obsctr, obsref, state, &lt      );

                     chckxc_c( SPICEFALSE, " ", ok );


                     if ( *ok ) 
                     {
                        /*
                        Look up the expected state and light time. 
                        */
                        spkezr_c ( target, et,     outref, 
                                   abcorr, srfnam, xstate, &xlt );

                        chckxc_c( SPICEFALSE, " ", ok );
                   
                        /*
                        Set position tolerance. Use looser tolerance for LT
                        corrections.
                        */

                        if ( coridx > 4 ) 
                        {
                           tol = MED;
                        }
                        else
                        {
                           tol = TIGHT;
                        }

                        /*
                        Check the position vector. 
                        */
                        chckad_c ( "Position", state,    "~~/", 
                                               xstate,   3, tol, ok );


                        /*
                        if ( !*ok ) 
                        {
                           printf ( "pos:  %f %f %f \n", 
                                    state[0],state[1],state[2] );

                           printf ( "xpos: %f %f %f \n", 
                                    xstate[0], xstate[1], xstate[2] );

                           printf ( "obs pos:  %f %f %f \n", 
                                    obssta[0],obssta[1],obssta[2] );
                        }
                        */


                        /*
                        Check the velocity vector. 
                        */
                        chckad_c ( "Velocity", state+3,  "~~/", 
                                               xstate+3, 3, MED, ok );

                        /*
                        Check light time. 
                        */
                        chcksd_c ( "LT", lt, "~/", xlt, tol, ok );

                     }              
                  }

               }
               /*
               End of event time loop. 
               */
            }
            /*
            End of reference frame loop.
            */
         }
         /*
         End of aberration correction loop. 
         */
      }
      /*
      End of object loop. 
      */
   }
   /*
   End of locus loop. 
   */


   

   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */   


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcvt_c: null input/output pointers" );  


   spkcvt_c ( NULL,     trgepc, trgctr, trgref, et,
              outref,   evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   spkcvt_c ( trgsta, trgepc, NULL,   trgref, et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, NULL,   et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              NULL,   evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, NULL,   abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, NULL,   obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, abcorr, NULL,   state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, abcorr, obsrvr, NULL, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, abcorr, obsrvr, state, NULL );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcvt_c: empty input strings" );  



   spkcvt_c ( trgsta, trgepc, "",     trgref, et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, "",     et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              "",     evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, "",     abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, "",     obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvt_c ( trgsta, trgepc, trgctr, trgref, et,
              outref, evlref, abcorr, "",     state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcpt_c: null input/output pointers" );  


   spkcpt_c ( NULL,     trgctr, trgref, et,
              outref,   evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   spkcpt_c ( trgpos, NULL,   trgref, et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, NULL,   et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              NULL,   evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, NULL,   abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, NULL,   obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, abcorr, NULL,   state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, abcorr, obsrvr, NULL, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, abcorr, obsrvr, state, NULL );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcpt_c: empty input strings" );  
 

   spkcpt_c ( trgpos, "",     trgref, et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpt_c ( trgpos, trgctr, "",     et,
              outref, evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              "",     evlref, abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, "",     abcorr, obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, "",     obsrvr, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpt_c ( trgpos, trgctr, trgref, et,
              outref, evlref, abcorr, "",     state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "spkcvo_c: null input/output pointers" );  


   spkcvo_c ( target, et,     NULL,   evlref, abcorr, 
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, NULL,   abcorr, 
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, NULL, 
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              NULL,   obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, NULL,   obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, obsctr, NULL,   state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, obsctr, obsref, NULL, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, obsctr, obsref, state, NULL );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "spkcvo_c: empty input strings" );  


   spkcvo_c ( target, et,     "",     evlref, abcorr, 
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvo_c ( target, et,     outref, "",     abcorr, 
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvo_c ( target, et,     outref, evlref, "",   
              obssta, obsepc, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, "",     obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcvo_c ( target, et,     outref, evlref, abcorr, 
              obssta, obsepc, obsctr, "",     state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcpo_c: null input/output pointers" );  


   spkcpo_c ( target, et,     NULL,   evlref, abcorr, 
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, NULL,   abcorr, 
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, NULL, 
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr,
              NULL,   obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, NULL,   obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, obsctr, NULL,   state, &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, obsctr, obsref, NULL,   &lt );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, obsctr, obsref, state,  NULL  );
   chckxc_c( SPICETRUE, "SPICE(NULLPOINTER)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "spkcpo_c: empty input strings" );  


   spkcpo_c ( target, et,     "",     evlref, abcorr, 
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpo_c ( target, et,     outref, "",     abcorr, 
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpo_c ( target, et,     outref, evlref, "",   
              obspos, obsctr, obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, "",     obsref, state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   spkcpo_c ( target, et,     outref, evlref, abcorr, 
              obspos, obsctr, "",     state, &lt );
   chckxc_c( SPICETRUE, "SPICE(EMPTYSTRING)", ok );






   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Clean up kernels." );  

   
   /*
   Unload and delete our test SPK files. 
   */
   spkuef_c ( handle );
   chckxc_c( SPICEFALSE, " ", ok );
   removeFile ( SPK );

   unload_c ( SPK1 );
   chckxc_c( SPICEFALSE, " ", ok );
   removeFile ( SPK1 );


      
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_spkcpv_c */

