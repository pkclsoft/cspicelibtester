/*

-Procedure f_spklow_c ( Test wrappers for low-level SPK routines )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the low-level SPK API
   routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_spklow_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for a subset of the CSPICE SPK
   subsystem routines. 
   
   The subset is:
      
      spksfs_c
      spkpvn_c
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version

   -tspice_c Version 1.0.1 04-OCT-2012 (NJB)  

      Updated header comments.
   
   -tspice_c Version 1.0.0 26-APR-2012 (NJB)  

-Index_Entries

   test wrappers of low-level SPK routines

-&
*/

{ /* Begin f_spklow_c */

 
   /*
   Constants
   */
   #define FRNMLN          33
   #define SIDLEN          41
   #define DSCSIZ           5
   #define ND               2
   #define NI               6   

   #define SPK             "spklow.bsp"

   /*
   Local variables
   */

   SpiceBoolean            found;
   
   SpiceChar               frname [ FRNMLN ];
   SpiceChar               segid  [ SIDLEN ];

   SpiceDouble             dc     [ ND ];
   SpiceDouble             descr  [ DSCSIZ ];
   SpiceDouble             et;
   SpiceDouble             lt;
   SpiceDouble             state  [ 6 ];
   SpiceDouble             xstart;
   SpiceDouble             xstate [ 6 ];
   SpiceDouble             xstop;

   SpiceInt                body;
   SpiceInt                center;
   SpiceInt                dtype;
   SpiceInt                frcode;
   SpiceInt                handle;
   SpiceInt                ic     [ NI ];
   SpiceInt                refID;
   SpiceInt                xbody;
   SpiceInt                xcenter;
   SpiceInt                xdtype;
   SpiceInt                xfrcode;
   SpiceInt                xhandle;
   SpiceInt                xrefID;


   
   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_spklow_c" );
   
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Set-up: create and load kernels." );
   
   tstlsk_c ();
   chckxc_c ( SPICEFALSE, " ", ok );

   tstspk_c ( SPK, SPICETRUE, &xhandle );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
    ******************************************************************
    *
    *  spksfs_c  tests
    *
    ******************************************************************
    */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test spksfs_c; obtain a descriptor for a segment " 
             "for the earth."                                   );

   xbody = 399;
   et    = 0.0;

   spksfs_c ( xbody, et, SIDLEN, &handle, descr, segid, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the found flag. 
   */
   chcksl_c ( "found", found, SPICETRUE, ok );

   /*
   Check the DAF handle returned by spksfs_c.
   */
   chcksi_c ( "handle", handle, "=", xhandle, 0, ok );

   /*
   Check the DAF segment descriptor returned by spksfs_c.
   */ 
   dafus_c ( descr, ND, NI, dc, ic );
   chckxc_c ( SPICEFALSE, " ", ok );

   xcenter = 3;
   xdtype  = 8;
   
   namfrm_c ( "FK4", &xfrcode );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the integer component of the descriptor. 
   */

   body   = ic[0];
   center = ic[1];
   frcode = ic[2];
   dtype  = ic[3];

   chcksi_c ( "body",   body,   "=", xbody,   0, ok );
   chcksi_c ( "center", center, "=", xcenter, 0, ok );
   chcksi_c ( "frcode", frcode, "=", xfrcode, 0, ok );
   chcksi_c ( "dtype",  dtype,  "=", xdtype,  0, ok );

   /*
   Check the double precision component of the descriptor. 
   */
   str2et_c ( "1984-FEB-27 11:06:40.000000 TDB", &xstart );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2015-NOV-05 12:53:20.000000 TDB", &xstop );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "start", dc[0], "=", xstart, 0.0, ok );
   chcksd_c ( "stop",  dc[1], "=", xstop,  0.0, ok );
   

   /*
   Check the segment ID. 
   */
   chcksc_c ( "segid", segid, "=", "EARTH", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test spksfs_c; null pointer for output string." );
 
   body = 399;
   et   = 0.0;
   
   spksfs_c ( body, et, SIDLEN, &handle, descr, NULL, &found );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test spksfs_c; output string too short." ); 

   spksfs_c ( body, et, 1, &handle, descr, segid, &found );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );


   /*
    ******************************************************************
    *
    *  spkpvn_c  tests
    *
    ******************************************************************
    */

   tcase_c ( "Test spkpvn_c: look up state of Jupiter barycenter." );

   body    = 5;
   xcenter = 0;
   et      = 0.0;

   spksfs_c ( body, et, SIDLEN, &handle, descr, segid, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the found flag. 
   */
   chcksl_c ( "found", found, SPICETRUE, ok );

   if ( ok )
   {
      /*
      Get the expected center and frame IDs from the
      segment descriptor. 
      */
      dafus_c ( descr, ND, NI, dc, ic );
      chckxc_c ( SPICEFALSE, " ", ok );

      xcenter = ic[1];
      xrefID  = ic[2];

      frmnam_c ( xrefID, FRNMLN, frname );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Get the expected state vector. 
      */
      spkez_c  ( body, et, frname, "NONE", xcenter, xstate, &lt );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      Get outputs from spkpnv_c. 
      */
      spkpvn_c ( handle, descr, et, &refID, state, &center );
      chckxc_c ( SPICEFALSE, " ", ok );
      
      /*
      Check the outputs for spkpvn_c. 
      */
      chcksi_c ( "center", center, "=", xcenter, 0, ok );
      chcksi_c ( "refID",  refID,  "=", xrefID,  0, ok );

      chckad_c ( "position", state,   "=", xstate,   3, 0.0, ok );
      chckad_c ( "velocity", state+3, "=", xstate+3, 3, 0.0, ok );
   }
   


   /* 
   ---- Case ---------------------------------------------------------
   */
   
   tcase_c ( "Clean up: unload and delete kernels." );

   spkuef_c ( xhandle );
   removeFile   ( SPK     );
      
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_spklow_c */

