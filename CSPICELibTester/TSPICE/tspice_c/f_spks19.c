/* f_spks19.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__6 = 6;
static doublereal c_b95 = 1e-14;
static integer c_b300 = 2100001;
static integer c__3 = 3;
static doublereal c_b474 = 0.;

/* $Procedure F_SPKS19 ( SPK data type 19 subsetter tests ) */
/* Subroutine */ int f_spks19__(logical *ok)
{
    /* Initialized data */

    static doublereal dscepc[9] = { 100.,200.,300.,400.,500.,600.,700.,800.,
	    900. };
    static doublereal dscsts[54]	/* was [6][9] */ = { 101.,201.,301.,
	    401.,501.,601.,102.,202.,302.,402.,502.,602.,103.,203.,303.,403.,
	    503.,603.,104.,204.,304.,404.,504.,604.,105.,205.,305.,405.,505.,
	    605.,106.,206.,306.,406.,506.,606.,107.,207.,307.,407.,507.,607.,
	    108.,208.,308.,408.,508.,608.,109.,209.,309.,409.,509.,609. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer cpad;
    static char name__[80];
    static integer npad;
    static char xref[32];
    static doublereal last;
    static integer i__, j, k;
    extern /* Subroutine */ int dafgn_(char *, ftnlen);
    static doublereal begin;
    extern /* Subroutine */ int dafgs_(doublereal *);
    static char segid[60];
    static integer ivbeg;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal descr[5];
    static integer epcto;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), moved_(doublereal *, integer *, 
	    doublereal *);
    static doublereal state[6];
    extern /* Subroutine */ int movei_(integer *, integer *, integer *);
    static integer xbody;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen), spkw19_(
	    integer *, integer *, integer *, char *, doublereal *, doublereal 
	    *, char *, integer *, integer *, integer *, integer *, doublereal 
	    *, doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static integer npkts[21000], pktto;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), daffna_(logical *), dafbfs_(integer *)
	    ;
    static doublereal et;
    static integer handle;
    static doublereal escale;
    extern /* Subroutine */ int dafcls_(integer *);
    static doublereal endepc;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int cleari_(integer *, integer *);
    static integer to;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer degres[21000];
    extern /* Subroutine */ int dafopr_(char *, integer *, ftnlen);
    static integer pktbeg, subhan;
    static doublereal epochs[2100000];
    extern /* Subroutine */ int unload_(char *, ftnlen);
    static doublereal ivlbds[21001], packts[25200000];
    static integer pktoff;
    static doublereal tmpivb[21001];
    static integer rectot, xcentr;
    static logical sellst;
    static integer ltrunc;
    static doublereal xstate[6];
    static integer tmpnpk[21000];
    static doublereal xstbuf[108]	/* was [6][18] */;
    static integer nintvl;
    static doublereal tmpfst;
    static integer rtrunc;
    extern logical exists_(char *, ftnlen);
    static integer subtps[21000], tmpniv;
    extern /* Subroutine */ int spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen);
    static doublereal tmplst;
    static integer pktsiz, pktsum[2100001], tmptot;
    extern /* Subroutine */ int spkcls_(integer *), spksub_(integer *, 
	    doublereal *, char *, doublereal *, doublereal *, integer *, 
	    ftnlen), furnsh_(char *, ftnlen);
    static integer pktszs[21000];
    extern logical odd_(integer *);
    static doublereal end, xlt;
    extern /* Subroutine */ int t_spks19__(integer *, doublereal *, char *, 
	    doublereal *, doublereal *, char *, integer *, integer *, char *, 
	    doublereal *, doublereal *, integer *, integer *, integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, logical *, 
	    logical *, logical *, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the SPK data type 19 subsetter SPKS19. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 19. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 11-MAY-2015 (NJB) */

/*        Updated to support subtype 2. */

/* -    SPICELIB Version 1.0.0, 07-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this SPK type. */

/*     The degree is compatible with the maximum degrees */
/*     supported by types 13 and 21. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     SPK type 19 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 12-element packets. */


/*     Subtype 1:  Lagrange interpolation, 6-element packets. */


/*     Subtype 2:  Hermite interpolation, 6-element packets. */


/*     Packet sizes associated with the various subtypes: */


/*     Number of subtypes: */


/*     Maximum packet size for type 19: */


/*     Minimum packet size for type 19: */


/*     The SPKPVN record size declared in spkrec.inc must be at least as */
/*     large as the maximum possible size of an SPK type 19 record. */

/*     The largest possible SPK type 19 record has subtype 1 (note that */
/*     records of subtype 0 have half as many epochs as those of subtype */
/*     1, for a given polynomial degree). A type 1 record contains */

/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size S19PS1 */
/*        - MAXDEG+1 time tags */


/*     End of include file spk19.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests SPKS19. */

/*     Tests for this routine are provided in a dedicated test family */
/*     in order to enhance readability of both this source file and */
/*     that of f_spk19.for. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 21-DEC-2015 (NJB) */

/*        Updated to support subtype 2. */

/*        Deleted debugging comments. */

/* -    TSPICE Version 1.0.0, 02-MAY-2012 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Epochs and states: */


/*     Open the test family. */

    topen_("F_SPKS19", (ftnlen)8);
/* ***************************************************************** */
/* * */
/* *    SPKS19 error cases: */
/* * */
/* ***************************************************************** */

/*     Test SPKS19:  start out with error handling. */


/* --- Case: ------------------------------------------------------ */

    tcase_("SPKW19 error case: bad frame name.", (ftnlen)34);

/*     End of SPKS19 error cases. */


/*     Close the SPK file at the DAF level; SPKCLS won't close */
/*     a file without segments. */

/*      CALL DAFCLS ( HANDLE ) */
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     SPKW19 Non-error cases: */

/* ***************************************************************** */
/* * */
/* *    Trivial case: write an SPK containing a small subtype 1 */
/* *    segment. */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Write a 1-interval subtype 1 segment.", (ftnlen)37);

/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     This file lacks both interval directories and mini-segment */
/*     epoch directories. */

    spkopn_("sp19t1.bsp", "sp19t1.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nintvl = 1;
    npkts[0] = 9;
    subtps[0] = 1;
    degres[0] = 3;
    first = dscepc[2];
    last = dscepc[(i__1 = npkts[0] - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spks19__", (ftnlen)341)];
    ivlbds[0] = dscepc[1];
    ivlbds[1] = dscepc[(i__1 = npkts[0] - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spks19__", (ftnlen)344)];
    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, dscsts, dscepc, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS19's ability to copy the segment we just created.", (
	    ftnlen)58);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19t1.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tmpfst = dscepc[2];
    tmplst = dscepc[(i__1 = npkts[0] - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spks19__", (ftnlen)399)];
    begin = tmpfst;
    end = tmplst;

/*     Create a new file containing the specified subset of the */
/*     input file. Write a new segment from scratch that */
/*     contains the data we expect to have in the subset file. */
/*     Compare the segments; verify the contents match. */

/*     In order to compare the segments, we need to supply the */
/*     SPK type 19 writer with the proper inputs. Specifically, */
/*     the input arrays corresponding to mini-segments must */
/*     have the correct start indices. Otherwise the writer will */
/*     create a segment that contains unused data at the start. */

/*     Let IVBEG be the start index of the arrays that */
/*     have a one-to-one correspondence with intervals. */

/*     Let PKTBEG be the start index of the arrays that */
/*     have a one-to-one correspondence with packets. */

/*     Let RTRUNC be the amount by which the final mini-segment's */
/*     packet count is truncated. */

/*     In this case, the window size associated with the */
/*     first (and only) mini-segment is 4, so there needs */
/*     to be one pad epoch to the left of the first */
/*     interval start time. The packet start is therefore */
/*     at index 2. */

    pktbeg = 2;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;
    tmpniv = nintvl;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The first mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 1;

/*     Adjust the packet count. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1) - rtrunc;

/*     Adjust the interval bounds. */

    tmpivb[0] = dscepc[(i__1 = pktbeg - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
	    "dscepc", i__1, "f_spks19__", (ftnlen)460)];
    tmpivb[1] = dscepc[(i__1 = pktbeg + tmpnpk[0] - 2) < 9 && 0 <= i__1 ? 
	    i__1 : s_rnge("dscepc", i__1, "f_spks19__", (ftnlen)461)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &tmpfst, &tmplst, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)464)], &subtps[(i__2 = ivbeg - 1) < 21000 &&
	     0 <= i__2 ? i__2 : s_rnge("subtps", i__2, "f_spks19__", (ftnlen)
	    464)], &degres[(i__3 = ivbeg - 1) < 21000 && 0 <= i__3 ? i__3 : 
	    s_rnge("degres", i__3, "f_spks19__", (ftnlen)464)], &dscsts[(i__4 
	    = pktbeg * 6 - 6) < 54 && 0 <= i__4 ? i__4 : s_rnge("dscsts", 
	    i__4, "f_spks19__", (ftnlen)464)], &dscepc[(i__5 = pktbeg - 1) < 
	    9 && 0 <= i__5 ? i__5 : s_rnge("dscepc", i__5, "f_spks19__", (
	    ftnlen)464)], &tmpivb[(i__6 = ivbeg - 1) < 21001 && 0 <= i__6 ? 
	    i__6 : s_rnge("tmpivb", i__6, "f_spks19__", (ftnlen)464)], &
	    sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The test routine T_SPKS19 deletes the subset SPK file, so */
/*     we don't do it here. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create a new SPK containing a copy of the segment that was just "
	    "created.", (ftnlen)72);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19t1.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkopn_("sp19subset.bsp", "sp19subset.bsp", &c__0, &subhan, (ftnlen)14, (
	    ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create subset. */

    spksub_(&handle, descr, segid, &first, &last, &subhan, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the subsetted file and the input file. */

    spkcls_(&subhan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs match input epoch"
	    "s.", (ftnlen)66);
    furnsh_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);
    i__1 = npkts[0] - 2;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = dscepc[(i__2 = i__ - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("dscepc"
		, i__2, "f_spks19__", (ftnlen)555)];
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(name__, state, "~~", &dscsts[(i__2 = i__ * 6 - 6) < 54 && 
		    0 <= i__2 ? i__2 : s_rnge("dscsts", i__2, "f_spks19__", (
		    ftnlen)566)], &c__6, &c_b95, ok, (ftnlen)80, (ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs are at midpoints "
	    "between adjacent input epochs.", (ftnlen)94);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);
    i__1 = npkts[0] - 3;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = (dscepc[(i__2 = i__ - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("dsce"
		"pc", i__2, "f_spks19__", (ftnlen)588)] + dscepc[(i__3 = i__) <
		 9 && 0 <= i__3 ? i__3 : s_rnge("dscepc", i__3, "f_spks19__", 
		(ftnlen)588)]) / 2;
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (j = 1; j <= 6; ++j) {
		xstate[(i__2 = j - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge("xsta"
			"te", i__2, "f_spks19__", (ftnlen)600)] = (dscsts[(
			i__3 = j + i__ * 6 - 7) < 54 && 0 <= i__3 ? i__3 : 
			s_rnge("dscsts", i__3, "f_spks19__", (ftnlen)600)] + 
			dscsts[(i__4 = j + (i__ + 1) * 6 - 7) < 54 && 0 <= 
			i__4 ? i__4 : s_rnge("dscsts", i__4, "f_spks19__", (
			ftnlen)600)]) / 2;
	    }
	    chckad_(name__, state, "~~", xstate, &c__6, &c_b95, ok, (ftnlen)
		    80, (ftnlen)2);
	}
    }
    unload_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Remove the trivial SPK we created; we'll create a new */
/*     one with more complexity. */

    delfil_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the input file as well. */

    delfil_("sp19t1.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    Write an SPK containing a small subtype 2 segment */
/* *    with two intervals. This file is needed to test packet size */
/* *    detection logic for subtype 2. */
/* * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Write a 2-interval subtype 2 segment.", (ftnlen)37);

/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     This file lacks both interval directories and mini-segment */
/*     epoch directories. */

    spkopn_("sp19t2.bsp", "sp19t2.bsp", &c__0, &handle, (ftnlen)10, (ftnlen)
	    10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nintvl = 2;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npkts", 
		i__2, "f_spks19__", (ftnlen)665)] = 9;
	subtps[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("subtps",
		 i__2, "f_spks19__", (ftnlen)666)] = 2;

/*        Use degree 7 to make window size match that of subtype 1 case */
/*        above. */

	degres[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("degres",
		 i__2, "f_spks19__", (ftnlen)672)] = 7;
    }
    to = 1;
    pktto = 1;
    endepc = 100.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spks19__", (ftnlen)682)];
	for (j = 1; j <= i__3; ++j) {
	    epochs[(i__2 = to - 1) < 2100000 && 0 <= i__2 ? i__2 : s_rnge(
		    "epochs", i__2, "f_spks19__", (ftnlen)684)] = endepc + (j 
		    - 1) * 100.;
	    for (k = 1; k <= 6; ++k) {
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spks19__", (ftnlen)688)] = 
			epochs[(i__4 = to - 1) < 2100000 && 0 <= i__4 ? i__4 :
			 s_rnge("epochs", i__4, "f_spks19__", (ftnlen)688)] + 
			k;
		++pktto;
	    }
	    ++to;
	}
	endepc = epochs[(i__3 = to - 2) < 2100000 && 0 <= i__3 ? i__3 : 
		s_rnge("epochs", i__3, "f_spks19__", (ftnlen)697)];
    }
    rectot = to - 1;
    first = epochs[2];
    ivlbds[0] = epochs[1];
    ivlbds[1] = epochs[(i__1 = npkts[0] - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)706)];
    ivlbds[2] = epochs[(i__1 = rectot - 2) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)707)];
    last = epochs[(i__1 = rectot - 3) < 2100000 && 0 <= i__1 ? i__1 : s_rnge(
	    "epochs", i__1, "f_spks19__", (ftnlen)709)];
    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test SPKS19's ability to copy the segment we just created; subty"
	    "pe 2 case.", (ftnlen)74);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19t2.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tmpfst = epochs[2];
    tmplst = epochs[(i__1 = rectot - 3) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)766)];
    begin = tmpfst;
    end = tmplst;
    pktsiz = 6;

/*     Create a new file containing the specified subset of the */
/*     input file. Write a new segment from scratch that */
/*     contains the data we expect to have in the subset file. */
/*     Compare the segments; verify the contents match. */

/*     In order to compare the segments, we need to supply the */
/*     SPK type 19 writer with the proper inputs. Specifically, */
/*     the input arrays corresponding to mini-segments must */
/*     have the correct start indices. Otherwise the writer will */
/*     create a segment that contains unused data at the start. */

/*     Let IVBEG be the start index of the arrays that */
/*     have a one-to-one correspondence with intervals. */

/*     Let PKTBEG be the start index of the arrays that */
/*     have a one-to-one correspondence with packets. */

/*     Let RTRUNC be the amount by which the final mini-segment's */
/*     packet count is truncated. */

/*     In this case, the window size associated with the */
/*     both mini-segments is 4, so there needs */
/*     to be one pad epoch to the left of the first */
/*     interval start time. The packet start is therefore */
/*     at index 2. */

    pktbeg = 2;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;
    tmpniv = nintvl;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The second mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 1;

/*     Adjust the packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1);
    tmpnpk[1] = npkts[1] - rtrunc;

/*     Adjust the interval bounds. */

    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)829)];
    tmpivb[1] = epochs[(i__1 = npkts[0] - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)830)];
    tmpivb[2] = epochs[(i__1 = rectot - rtrunc - 1) < 2100000 && 0 <= i__1 ? 
	    i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)831)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &tmpfst, &tmplst, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)833)], &subtps[(i__3 = ivbeg - 1) < 21000 &&
	     0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (ftnlen)
	    833)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? i__2 : 
	    s_rnge("degres", i__2, "f_spks19__", (ftnlen)833)], &packts[(i__4 
	    = (pktbeg - 1) * pktsiz) < 25200000 && 0 <= i__4 ? i__4 : s_rnge(
	    "packts", i__4, "f_spks19__", (ftnlen)833)], &epochs[(i__5 = 
	    pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs", i__5,
	     "f_spks19__", (ftnlen)833)], &tmpivb[(i__6 = ivbeg - 1) < 21001 
	    && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", (
	    ftnlen)833)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The test routine T_SPKS19 deletes the subset SPK file, so */
/*     we don't do it here. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Create a new SPK containing a copy of the segment that was just "
	    "created. Subtype 2 case.", (ftnlen)88);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19t2.bsp", &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkopn_("sp19subset.bsp", "sp19subset.bsp", &c__0, &subhan, (ftnlen)14, (
	    ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create subset. */

    spksub_(&handle, descr, segid, &first, &last, &subhan, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close the subsetted file and the input file. */

    spkcls_(&subhan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs match input epoch"
	    "s. Subtype 2 case.", (ftnlen)82);
    furnsh_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);
    i__1 = rectot - 2;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = epochs[(i__3 = i__ - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge(
		"epochs", i__3, "f_spks19__", (ftnlen)930)];
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    j = (i__ - 1) * pktsiz + 1;
	    chckad_(name__, state, "~~", &packts[(i__3 = j - 1) < 25200000 && 
		    0 <= i__3 ? i__3 : s_rnge("packts", i__3, "f_spks19__", (
		    ftnlen)943)], &c__6, &c_b95, ok, (ftnlen)80, (ftnlen)2);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Read states from the file just created. Epochs are at midpoints "
	    "between adjacent input epochs. Subtype 2 case.", (ftnlen)110);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Buffer expected states. */

    unload_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = rectot - 3;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = (epochs[(i__3 = i__ - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge(
		"epochs", i__3, "f_spks19__", (ftnlen)974)] + epochs[(i__2 = 
		i__) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epochs", i__2, 
		"f_spks19__", (ftnlen)974)]) / 2;
	spkez_(&xbody, &et, xref, "NONE", &xcentr, &xstbuf[(i__3 = i__ * 6 - 
		6) < 108 && 0 <= i__3 ? i__3 : s_rnge("xstbuf", i__3, "f_spk"
		"s19__", (ftnlen)976)], &lt, (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = rectot - 3;
    for (i__ = 3; i__ <= i__1; ++i__) {
	et = (epochs[(i__3 = i__ - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge(
		"epochs", i__3, "f_spks19__", (ftnlen)991)] + epochs[(i__2 = 
		i__) < 2100000 && 0 <= i__2 ? i__2 : s_rnge("epochs", i__2, 
		"f_spks19__", (ftnlen)991)]) / 2;
	spkez_(&xbody, &et, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*ok) {
	    s_copy(name__, "State #.", (ftnlen)80, (ftnlen)8);
	    repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    moved_(&xstbuf[(i__3 = i__ * 6 - 6) < 108 && 0 <= i__3 ? i__3 : 
		    s_rnge("xstbuf", i__3, "f_spks19__", (ftnlen)1003)], &
		    c__6, xstate);
	    chckad_(name__, state, "~~", xstate, &c__6, &c_b95, ok, (ftnlen)
		    80, (ftnlen)2);
	}
    }
    unload_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Remove the trivial SPK we created; we'll create a new */
/*     one with more complexity. */

    delfil_("sp19subset.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Delete the input file as well. */

    delfil_("sp19t2.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ***************************************************************** */
/* * */
/* *    Large segment subsetting cases: */
/* * */
/* ***************************************************************** */

/*     Below, we'll use type 0 segments because it will be convenient to */
/*     use cubic interpolation and a window size of 2. */

/*     The velocity/acceleration data will not be compatible with the */
/*     position/velocity data. */

/*     We need a segment containing enough interval directories */
/*     so that multiple buffers full of directory entries must */
/*     be read. This implies we need more than DIRSIZ**2 intervals */
/*     in the segment. */

/*     Note that these cases do not exercise padding logic. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Create an SPK with interval directories.", (ftnlen)40);
    spkopn_("sp19mixed.bsp", "sp19mixed.bsp", &c__0, &handle, (ftnlen)13, (
	    ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the inputs to SPKW19. */

    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    nintvl = 21000;

/*     PKTSUM is an array whose Ith element contains the sum of */
/*     counts of elements in the first through Ith packets. */
/*     Initialize PKTSUM here. */

    cleari_(&c_b300, pktsum);

/*     Set the packet counts, subtypes, and degrees. */

/*     Since we're concentrating on interval selection */
/*     logic, we'll mostly use small packet counts. However, */
/*     we'll create larger mini-segments at the beginning */
/*     and end of the segment. */

    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (i__ <= 2) {
	    subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "subtps", i__3, "f_spks19__", (ftnlen)1098)] = 0;
	    pktszs[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktszs", i__3, "f_spks19__", (ftnlen)1099)] = 12;
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spks19__", (ftnlen)1100)] = 10001;
	    degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "degres", i__3, "f_spks19__", (ftnlen)1101)] = 11;
	} else if (i__ >= nintvl - 1) {

/*           Vary the subtype; use subtype 1. */
/*           Adjust the degree to keep the window */
/*           size equal to 6. */

	    subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "subtps", i__3, "f_spks19__", (ftnlen)1109)] = 1;
	    pktszs[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktszs", i__3, "f_spks19__", (ftnlen)1110)] = 6;
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spks19__", (ftnlen)1111)] = 10001;
	    degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "degres", i__3, "f_spks19__", (ftnlen)1112)] = 5;
	} else if (i__ % 3 == 0) {

/*           Vary the subtype, based on equivalence class of the interval */
/*           index. */

	    subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "subtps", i__3, "f_spks19__", (ftnlen)1119)] = 1;
	    pktszs[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktszs", i__3, "f_spks19__", (ftnlen)1120)] = 6;
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spks19__", (ftnlen)1121)] = 3;
	    degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "degres", i__3, "f_spks19__", (ftnlen)1122)] = 1;
	} else if (i__ % 3 == 1) {
	    subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "subtps", i__3, "f_spks19__", (ftnlen)1126)] = 0;
	    pktszs[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktszs", i__3, "f_spks19__", (ftnlen)1127)] = 12;
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spks19__", (ftnlen)1128)] = 4;
	    degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "degres", i__3, "f_spks19__", (ftnlen)1129)] = 3;
	} else {
	    subtps[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "subtps", i__3, "f_spks19__", (ftnlen)1133)] = 2;
	    pktszs[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktszs", i__3, "f_spks19__", (ftnlen)1134)] = 6;
	    npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npk"
		    "ts", i__3, "f_spks19__", (ftnlen)1135)] = 4;
	    degres[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		    "degres", i__3, "f_spks19__", (ftnlen)1136)] = 3;
	}
    }

/*     Set the pad size for degree 11 Hermite interpolation. */

/*     Create padding at the boundaries of the first two and */
/*     last two intervals. */

    npad = 2;

/*     Generate epochs and packets. */

    epcto = 0;

/*     The end epoch of each interval must be the start epoch of */
/*     the next. */

    endepc = 0.;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the interval start time. */

	ivlbds[(i__3 = i__ - 1) < 21001 && 0 <= i__3 ? i__3 : s_rnge("ivlbds",
		 i__3, "f_spks19__", (ftnlen)1167)] = endepc;

/*        Set the separation of epochs. */

	if (odd_(&i__)) {
	    escale = 10.;
	} else {
	    escale = 20.;
	}

/*        Create left-side padding for the first two and last */
/*        two intervals. */

	if (i__ <= 2 || i__ >= nintvl - 1) {
	    for (j = 1 - npad; j <= 0; ++j) {
		++epcto;
		epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : 
			s_rnge("epochs", i__3, "f_spks19__", (ftnlen)1189)] = 
			endepc + escale * (j - 1);
		pktsum[(i__3 = epcto) < 2100001 && 0 <= i__3 ? i__3 : s_rnge(
			"pktsum", i__3, "f_spks19__", (ftnlen)1191)] = pktsum[
			(i__2 = epcto - 1) < 2100001 && 0 <= i__2 ? i__2 : 
			s_rnge("pktsum", i__2, "f_spks19__", (ftnlen)1191)] + 
			pktszs[(i__4 = i__ - 1) < 21000 && 0 <= i__4 ? i__4 : 
			s_rnge("pktszs", i__4, "f_spks19__", (ftnlen)1191)];
	    }
	}

/*        This is the common epoch-generating loop. */

	i__2 = npkts[(i__3 = i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge(
		"npkts", i__3, "f_spks19__", (ftnlen)1200)];
	for (j = 1; j <= i__2; ++j) {
	    ++epcto;
	    epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge(
		    "epochs", i__3, "f_spks19__", (ftnlen)1204)] = endepc + 
		    escale * (j - 1);
	    pktsum[(i__3 = epcto) < 2100001 && 0 <= i__3 ? i__3 : s_rnge(
		    "pktsum", i__3, "f_spks19__", (ftnlen)1206)] = pktsum[(
		    i__4 = epcto - 1) < 2100001 && 0 <= i__4 ? i__4 : s_rnge(
		    "pktsum", i__4, "f_spks19__", (ftnlen)1206)] + pktszs[(
		    i__5 = i__ - 1) < 21000 && 0 <= i__5 ? i__5 : s_rnge(
		    "pktszs", i__5, "f_spks19__", (ftnlen)1206)];
	}

/*        Save the last epoch of this interval. */

	endepc = epochs[(i__2 = epcto - 1) < 2100000 && 0 <= i__2 ? i__2 : 
		s_rnge("epochs", i__2, "f_spks19__", (ftnlen)1213)];

/*        Create right-side padding for the first two intervals. */

	if (i__ <= 2 || i__ >= nintvl - 1) {
	    i__2 = npad;
	    for (j = 1; j <= i__2; ++j) {
		++epcto;
		epochs[(i__3 = epcto - 1) < 2100000 && 0 <= i__3 ? i__3 : 
			s_rnge("epochs", i__3, "f_spks19__", (ftnlen)1224)] = 
			endepc + escale * j;
		pktsum[(i__3 = epcto) < 2100001 && 0 <= i__3 ? i__3 : s_rnge(
			"pktsum", i__3, "f_spks19__", (ftnlen)1226)] = pktsum[
			(i__4 = epcto - 1) < 2100001 && 0 <= i__4 ? i__4 : 
			s_rnge("pktsum", i__4, "f_spks19__", (ftnlen)1226)] + 
			pktszs[(i__5 = i__ - 1) < 21000 && 0 <= i__5 ? i__5 : 
			s_rnge("pktszs", i__5, "f_spks19__", (ftnlen)1226)];
	    }

/*           Update the packet count for the current interval. */

	    npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge("npk"
		    "ts", i__2, "f_spks19__", (ftnlen)1233)] = npkts[(i__3 = 
		    i__ - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", 
		    i__3, "f_spks19__", (ftnlen)1233)] + (npad << 1);
	}
    }

/*     Let RECTOT be the total number of records in the segment. */

    rectot = epcto;

/*     Save the stop time of the last interval. */

    ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds", 
	    i__1, "f_spks19__", (ftnlen)1248)] = endepc;

/*     Set the descriptor bounds. We don't need to do anything */
/*     fancy here. */

    first = ivlbds[0];
    last = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlb"
	    "ds", i__1, "f_spks19__", (ftnlen)1255)];

/*     Now assign the packets. Since we're using two-point */
/*     interpolation for the middle segments, */
/*     selecting the wrong packets to interpolate */
/*     should yield an obviously wrong answer. */

/*     We also want to have large packet discontinuities at */
/*     interval boundaries, so the consequence of selecting */
/*     the wrong interval is obvious. */

    pktto = 0;
    i__1 = nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Assign the packets for the Ith interval. */

	i__3 = npkts[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		"npkts", i__2, "f_spks19__", (ftnlen)1274)];
	for (j = 1; j <= i__3; ++j) {

/*           Position components: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spks19__", (ftnlen)1283)] = 
			i__ * 1e9 + j * 100. + k;
	    }

/*           Velocities associated with positions: */

	    for (k = 1; k <= 3; ++k) {
		++pktto;
		packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 : 
			s_rnge("packts", i__2, "f_spks19__", (ftnlen)1295)] = 
			0.;
	    }
	    if (pktszs[(i__2 = i__ - 1) < 21000 && 0 <= i__2 ? i__2 : s_rnge(
		    "pktszs", i__2, "f_spks19__", (ftnlen)1299)] == 12) {

/*              Velocity/acceleration packets: */

		for (k = 1; k <= 6; ++k) {
		    ++pktto;
		    packts[(i__2 = pktto - 1) < 25200000 && 0 <= i__2 ? i__2 :
			     s_rnge("packts", i__2, "f_spks19__", (ftnlen)
			    1307)] = packts[(i__4 = pktto - 7) < 25200000 && 
			    0 <= i__4 ? i__4 : s_rnge("packts", i__4, "f_spk"
			    "s19__", (ftnlen)1307)] * 10.;
		}
	    }
	}
    }

/*     For this segment, we'll select the last applicable interval */
/*     when the request time lies on an interval boundary. */

    sellst = TRUE_;
    spkw19_(&handle, &xbody, &xcentr, xref, &first, &last, segid, &nintvl, 
	    npkts, subtps, degres, packts, epochs, ivlbds, &sellst, (ftnlen)
	    32, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Trivial case: subset is whole file.", (ftnlen)35);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = nintvl;
    begin = ivlbds[0];
    end = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds"
	    , i__1, "f_spks19__", (ftnlen)1388)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &nintvl, npkts, subtps, 
	    degres, packts, epochs, ivlbds, &sellst, &c_true, ok, (ftnlen)60, 
	    (ftnlen)16, (ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Coverage is for the singleton interval located at the"
	    " first interval start time.", (ftnlen)155);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)1471)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = (npad << 1) + 1;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[0] = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? 
	    i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)1494)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)1495)] = epochs[(i__3 = pktbeg + (
	    npad << 1) - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", 
	    i__3, "f_spks19__", (ftnlen)1495)];
    begin = ivlbds[0];
    end = begin;

/*     Save the subset file created by the following call. */

    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)1503)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)1503)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)1503)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)1503)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)1503)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)1503)], &sellst, &c_false, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare states looked up from the input file and the subset */
/*     file. */

    furnsh_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, xstate, &xlt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("Position: ", state, "=", xstate, &c__3, &c_b474, ok, (ftnlen)10, 
	    (ftnlen)1);
    chckad_("Velocity: ", &state[3], "=", &xstate[3], &c__3, &c_b474, ok, (
	    ftnlen)10, (ftnlen)1);

/*     Clean up the subset file. */

    if (exists_("spk19tempsub.bsp", (ftnlen)16)) {
	delfil_("spk19tempsub.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Coverage is for the singleton interval located at the"
	    " first interval end time.", (ftnlen)153);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     In this case, since the segment time coincides with the end */
/*     time of the first interval, SPKS19 will append a second, */
/*     small interval to the first output interval. */

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    pktbeg = npkts[0] - (npad << 1);

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)1625)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = (npad << 1) + 1;
    tmpnpk[1] = (npad << 1) + 1;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)1650)];
    tmpivb[1] = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? 
	    i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)1651)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)1652)] = epochs[(i__3 = pktbeg + (
	    npad << 2)) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)1652)];
    begin = ivlbds[1];
    end = begin;

/*     Save the subset file created by the following call. */

    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)1660)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)1660)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)1660)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)1660)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)1660)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)1660)], &sellst, &c_false, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare states looked up from the input file and the subset */
/*     file. */

    furnsh_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, xstate, &xlt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("Position: ", state, "=", xstate, &c__3, &c_b474, ok, (ftnlen)10, 
	    (ftnlen)1);
    chckad_("Velocity: ", &state[3], "=", &xstate[3], &c__3, &c_b474, ok, (
	    ftnlen)10, (ftnlen)1);
    if (exists_("spk19tempsub.bsp", (ftnlen)16)) {
	delfil_("spk19tempsub.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Coverage is for the singleton interval located at the"
	    " last interval end time.", (ftnlen)152);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = rectot - (npad << 1);

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)1774)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = nintvl;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the last packet count. */

    tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)1791)] = (npad << 1) + 1;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[(i__1 = ivbeg - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)1798)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)1798)];
    tmpivb[(i__1 = ivbeg) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", i__1,
	     "f_spks19__", (ftnlen)1799)] = ivlbds[(i__3 = nintvl) < 21001 && 
	    0 <= i__3 ? i__3 : s_rnge("ivlbds", i__3, "f_spks19__", (ftnlen)
	    1799)];
    begin = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivl"
	    "bds", i__1, "f_spks19__", (ftnlen)1801)];
    end = begin;

/*     Save the subset file created by the following call. */

    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)1807)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)1807)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)1807)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)1807)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)1807)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)1807)], &sellst, &c_false, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare states looked up from the input file and the subset */
/*     file. */

    furnsh_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, xstate, &xlt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("Position: ", state, "=", xstate, &c__3, &c_b474, ok, (ftnlen)10, 
	    (ftnlen)1);
    chckad_("Velocity: ", &state[3], "=", &xstate[3], &c__3, &c_b474, ok, (
	    ftnlen)10, (ftnlen)1);
    if (exists_("spk19tempsub.bsp", (ftnlen)16)) {
	delfil_("spk19tempsub.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Coverage is for the singleton interval located betwee"
	    "n two interior epochs.", (ftnlen)150);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = 500;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)1922)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first packet count. */

    tmpnpk[0] = (npad << 1) + 2;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[(i__1 = ivbeg - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)1946)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)1946)];
    tmpivb[(i__1 = ivbeg) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", i__1,
	     "f_spks19__", (ftnlen)1947)] = epochs[(i__3 = pktbeg + (npad << 
	    1)) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_sp"
	    "ks19__", (ftnlen)1947)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)1950)] + 1.;
    end = begin;

/*     Save the subset file created by the following call. */

    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)1956)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)1956)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)1956)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)1956)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)1956)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)1956)], &sellst, &c_false, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare states looked up from the input file and the subset */
/*     file. */

    furnsh_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, xstate, &xlt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("Position: ", state, "=", xstate, &c__3, &c_b474, ok, (ftnlen)10, 
	    (ftnlen)1);
    chckad_("Velocity: ", &state[3], "=", &xstate[3], &c__3, &c_b474, ok, (
	    ftnlen)10, (ftnlen)1);
    if (exists_("spk19tempsub.bsp", (ftnlen)16)) {
	delfil_("spk19tempsub.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Coverage is for the singleton interval located at the"
	    " end of the third interpolation interval.", (ftnlen)169);

/*     This case exercises epoch selection logic for the final */
/*     output mini-segment. In this case, the input mini-segments */
/*     from which the output mini-segments are taken contain no */
/*     no padding. */


/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    cpad = 1;
    pktbeg = 0;
    for (i__ = 1; i__ <= 3; ++i__) {
	pktbeg = npkts[(i__1 = i__ - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
		"npkts", i__1, "f_spks19__", (ftnlen)2069)] + pktbeg;
    }
    pktbeg -= cpad;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2082)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the third interval of the */
/*     input file. */

    ivbeg = 3;

/*     Copy the packet counts. */

    movei_(&npkts[(i__1 = ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "npkts", i__1, "f_spks19__", (ftnlen)2093)], &tmpniv, tmpnpk);

/*     Adjust the packet counts. */

    tmpnpk[(i__1 = ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)2098)] = cpad + 1;
    tmpnpk[(i__1 = ivbeg) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1,
	     "f_spks19__", (ftnlen)2099)] = cpad + 1;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[(i__1 = ivbeg - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2106)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)2106)];
    tmpivb[(i__1 = ivbeg) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", i__1,
	     "f_spks19__", (ftnlen)2107)] = ivlbds[(i__3 = ivbeg) < 21001 && 
	    0 <= i__3 ? i__3 : s_rnge("ivlbds", i__3, "f_spks19__", (ftnlen)
	    2107)];

/*     Note that the epochs at indices */

/*        PKTBEG + CPAD */
/*        PKTBEG + 2*CPAD */

/*     are, respectively, the last epoch of interval 3 and the */
/*     first of interval 4; hence they coincide. The next */
/*     distinct epoch is at index */

/*        PKTBEG + 3*CPAD */

    tmpivb[(i__1 = ivbeg + 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2120)] = epochs[(i__3 = pktbeg + cpad 
	    * 3 - 1) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, 
	    "f_spks19__", (ftnlen)2120)];
    begin = ivlbds[3];
    end = ivlbds[3];

/*     Save the subset file created by the following call. */

    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2130)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2130)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2130)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2130)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2130)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2130)], &sellst, &c_false, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compare states looked up from the input file and the subset */
/*     file. */

    furnsh_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, xstate, &xlt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("sp19mixed.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkez_(&xbody, &begin, xref, "NONE", &xcentr, state, &lt, (ftnlen)32, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("spk19tempsub.bsp", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("Position: ", state, "=", xstate, &c__3, &c_b474, ok, (ftnlen)10, 
	    (ftnlen)1);
    chckad_("Velocity: ", &state[3], "=", &xstate[3], &c__3, &c_b474, ok, (
	    ftnlen)10, (ftnlen)1);
    if (exists_("spk19tempsub.bsp", (ftnlen)16)) {
	delfil_("spk19tempsub.bsp", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Use the entire first mini-segment. Inset the segment "
	    "end time slightly to prevent addition of a small interval on the"
	    " right.", (ftnlen)199);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

/*     Note that PKTSUM has a 0th element which is set to 0. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2243)] + 1;

/*     The first interval in the subset segment is the */
/*     first interval of the input file. */

    ivbeg = 1;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Copy the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmptot = tmpnpk[0];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2264)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2265)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2269)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2269)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2269)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2269)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2269)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2269)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Use the entire first mini-segment.", (ftnlen)109);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. The output segment will contain */
/*     a second, small interval on the right to support */
/*     correct interpolation when the request time is the */
/*     end time of the first interval and the interval */
/*     selection method is "select last interval." */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    pktbeg = 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2346)] + 1;

/*     The first interval in the subset segment is the */
/*     first interval of the input file. */

    ivbeg = 1;

/*     Copy and adjust the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);
    tmpnpk[1] = (npad << 1) + 1;

/*     Copy and adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[2] = epochs[(i__1 = npkts[0] + 1 + (npad << 1) - 1) < 2100000 && 0 
	    <= i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)
	    2367)];
    tmptot = tmpnpk[0] + tmpnpk[1];
    begin = ivlbds[0];
    end = ivlbds[1];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2376)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2376)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2376)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2376)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2376)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2376)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Create a file with one interpolation interval. Skip o"
	    "ver the first 500 packets of the first interval, and truncate th"
	    "e last 500 packets of the interval. Start and stop times are ins"
	    "et and lie between members of the epoch list.", (ftnlen)301);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2457)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The first mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1) - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);

/*     Since we have only one interval, the total epoch */
/*     count is obtained by subtracting the sizes of */
/*     the initial and final truncated portions from the */
/*     initial size of the first mini-segment. */

    tmptot = npkts[0] - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2499)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2500)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)2500)];

/*     Inset BEGIN and END by the pad size. */

    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2505)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2506)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2508)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2508)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2508)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2508)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2508)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2508)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Create a file with one interpolation interval. Skip o"
	    "ver the first 500 packets of the first interval, and truncate th"
	    "e last 500 packets of the interval. Start and stop times coincid"
	    "e with members of the epoch list.", (ftnlen)289);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2593)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The first mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1) - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);

/*     Since we have only one interval, the total epoch */
/*     count is obtained by subtracting the sizes of */
/*     the initial and final truncated portions from the */
/*     initial size of the first mini-segment. */

    tmptot = npkts[0] - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2636)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2637)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)2637)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2639)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2640)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2643)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2643)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2643)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2643)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2643)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2643)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Create a file with two interpolation intervals. Skip "
	    "over the first 500 packets of the first interval, and truncate t"
	    "he last 500 packets of the second interval. The output segment h"
	    "as no \"middle group.\" Start and stop times are inset; they lie"
	    " between members of the epoch list.", (ftnlen)353);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2730)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The second mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1);
    tmpnpk[1] = npkts[1] - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);

/*     Since we have two intervals, the total epoch */
/*     count is obtained by subtracting the sizes of */
/*     the initial and final truncated portions from the */
/*     sum of the sizes of the first two mini-segments. */

    tmptot = npkts[0] + npkts[1] - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2773)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2774)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)2774)];

/*     Inset BEGIN and END by the pad size. */

    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2779)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2780)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2783)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2783)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2783)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2783)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2783)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2783)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Create a file with two interpolation interval. Skip o"
	    "ver the first 500 packets of the first interval, and truncate th"
	    "e last 500 packets of the second interval. The output segment ha"
	    "s no \"middle group.\" Start and stop times coincide with member"
	    "s of the epoch list.", (ftnlen)338);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)2865)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The second mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1);
    tmpnpk[1] = npkts[1] - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);

/*     Since we have only one interval, the total epoch */
/*     count is obtained by subtracting the sizes of */
/*     the initial and final truncated portions from the */
/*     initial size of the first mini-segment. */

    tmptot = npkts[0] + npkts[1] - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2908)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)2909)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)2909)];
    begin = tmpivb[0];
    end = tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb"
	    , i__1, "f_spks19__", (ftnlen)2912)];

/*     Inset BEGIN and END by the pad size. */

    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2917)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)2918)];
    tmpfst = begin;
    tmplst = end;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &tmpfst, &tmplst, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)2924)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)2924)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)2924)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)2924)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)2924)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)2924)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the first interval"
	    ", and truncate the last 500 packets of the last. Start and stop "
	    "times are inset and lie between members of the epoch list.", (
	    ftnlen)250);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = nintvl;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3003)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The first mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1);
    tmpnpk[(i__1 = tmpniv - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3031)] = npkts[(i__3 = tmpniv - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3031)] - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmptot = rectot - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3040)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3041)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3041)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3044)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3045)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3049)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3049)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3049)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3049)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3049)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3049)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the first interval"
	    ", and truncate the last 500 packets of the last. Start and stop "
	    "times coincide with members of the epoch list.", (ftnlen)238);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = nintvl;

/*     Set the index of the first packet we'll use. */

    pktbeg = 501;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3132)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the first interval of the */
/*     input file. We don't actually lose any intervals */
/*     in this case. So the interval index doesn't change. */

    ivbeg = 1;

/*     The packet count and time bounds of the first */
/*     interval do change. */

/*     The first mini-segment ends one epoch after the */
/*     last epoch contained in the mini-segment's coverage */
/*     interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &tmpniv, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[0] = npkts[0] - (pktbeg - 1);
    tmpnpk[(i__1 = tmpniv - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3160)] = npkts[(i__3 = tmpniv - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3160)] - rtrunc;

/*     Adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmptot = rectot - (pktbeg - 1) - rtrunc;
    tmpivb[0] = epochs[(i__1 = pktbeg - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3169)];
    tmpivb[(i__1 = tmpniv) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3170)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3170)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3172)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3173)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3176)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3176)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3176)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3176)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3176)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3176)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the penultimate in"
	    "terval, and truncate the last 500 packets of the last. Start and"
	    " stop times are inset and lie between members of the epoch list.",
	     (ftnlen)256);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    ltrunc = 500;
    pktbeg = rectot - npkts[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : 
	    s_rnge("npkts", i__1, "f_spks19__", (ftnlen)3251)] - npkts[(i__3 =
	     nintvl - 2) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, 
	    "f_spks19__", (ftnlen)3251)] + ltrunc + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl - 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3268)] + 1;


/*     The second mini-segment ends 500 epochs before */
/*     the end of the mini-segment's coverage interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[(i__1 = nintvl - 2) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3285)] = npkts[(i__3 = nintvl - 2) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3285)] - ltrunc;
    tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3286)] = npkts[(i__3 = nintvl - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3286)] - rtrunc;

/*     Adjust the interval bounds. */

    moved_(ivlbds, &nintvl, tmpivb);
    tmptot = tmpnpk[(i__1 = nintvl - 2) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3294)] + tmpnpk[(i__3 = 
	    nintvl - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("tmpnpk", i__3, 
	    "f_spks19__", (ftnlen)3294)];

/*     Note that the first output interval ends at the end */
/*     time of the corresponding interval of the input segment, */
/*     so this time is unchanged. */

    tmpivb[(i__1 = nintvl - 2) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3301)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)3301)];
    tmpivb[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3302)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3302)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3305)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3306)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3308)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3308)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3308)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3308)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3308)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3308)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the penultimate in"
	    "terval, and truncate the last 500 packets of the last. Start and"
	    " stop times coincide with members of the epoch list.", (ftnlen)
	    244);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 2;

/*     Set the index of the first packet we'll use. */

    ltrunc = 500;
    pktbeg = rectot - npkts[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : 
	    s_rnge("npkts", i__1, "f_spks19__", (ftnlen)3381)] - npkts[(i__3 =
	     nintvl - 2) < 21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, 
	    "f_spks19__", (ftnlen)3381)] + ltrunc + 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3391)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl - 1;


/*     The second mini-segment ends 500 epochs before */
/*     the end of the mini-segment's coverage interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[(i__1 = nintvl - 2) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3414)] = npkts[(i__3 = nintvl - 2) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3414)] - ltrunc;
    tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3415)] = npkts[(i__3 = nintvl - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3415)] - rtrunc;

/*     Adjust the interval bounds. */

    moved_(ivlbds, &nintvl, tmpivb);
    tmptot = tmpnpk[(i__1 = nintvl - 2) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3423)] + tmpnpk[(i__3 = 
	    nintvl - 1) < 21000 && 0 <= i__3 ? i__3 : s_rnge("tmpnpk", i__3, 
	    "f_spks19__", (ftnlen)3423)];

/*     Note that the first output interval ends at the end */
/*     time of the corresponding interval of the input segment, */
/*     so this time is unchanged. */

    tmpivb[(i__1 = nintvl - 2) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3431)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)3431)];
    tmpivb[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3432)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3432)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3436)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3437)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3440)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3440)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3440)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3440)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3440)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3440)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the last interval,"
	    " and truncate the last 500 packets of the last. Start and stop t"
	    "imes are inset and lie between members of the epoch list.", (
	    ftnlen)249);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    ltrunc = 500;
    pktbeg = rectot - npkts[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : 
	    s_rnge("npkts", i__1, "f_spks19__", (ftnlen)3512)] + ltrunc + 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3522)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl;


/*     The second mini-segment ends 500 epochs before */
/*     the end of the mini-segment's coverage interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3545)] = npkts[(i__3 = nintvl - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3545)] - rtrunc - ltrunc;

/*     Adjust the interval bounds. */

    moved_(ivlbds, &nintvl, tmpivb);
    tmptot = tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3552)];
    tmpivb[(i__1 = nintvl - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3554)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)3554)];
    tmpivb[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3555)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3555)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3558)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3559)] 
	    - 1.;
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3562)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3562)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3562)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3562)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3562)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3562)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Skip over the first 500 packets of the last interval,"
	    " and truncate the last 500 packets of the last. Start and stop t"
	    "imes are members of the epoch list.", (ftnlen)227);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    ltrunc = 500;
    pktbeg = rectot - npkts[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : 
	    s_rnge("npkts", i__1, "f_spks19__", (ftnlen)3635)] + ltrunc + 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3645)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl;


/*     The second mini-segment ends 500 epochs before */
/*     the end of the mini-segment's coverage interval. */

    rtrunc = 500;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Adjust the first and last packet counts. */

    tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3668)] = npkts[(i__3 = nintvl - 1) < 
	    21000 && 0 <= i__3 ? i__3 : s_rnge("npkts", i__3, "f_spks19__", (
	    ftnlen)3668)] - rtrunc - ltrunc;

/*     Adjust the interval bounds. */

    moved_(ivlbds, &nintvl, tmpivb);
    tmptot = tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3675)];
    tmpivb[(i__1 = nintvl - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3678)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)3678)];
    tmpivb[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3679)] = epochs[(i__3 = pktbeg + 
	    tmptot - 2) < 2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3,
	     "f_spks19__", (ftnlen)3679)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3682)];
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3683)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3685)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3685)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3685)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3685)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3685)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3685)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Use the entire final mini-segment. Inset the segment "
	    "start time slightly to prevent addition of a small interval on t"
	    "he left.", (ftnlen)200);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. */

    tmpniv = 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = rectot - npkts[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : 
	    s_rnge("npkts", i__1, "f_spks19__", (ftnlen)3753)] + 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3762)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl;

/*     Copy the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);

/*     Copy the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmptot = tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3781)];
    begin = epochs[(i__1 = pktbeg + npad - 1) < 2100000 && 0 <= i__1 ? i__1 : 
	    s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3784)] + 1.;
    end = epochs[(i__1 = pktbeg + tmptot - 1 - npad - 1) < 2100000 && 0 <= 
	    i__1 ? i__1 : s_rnge("epochs", i__1, "f_spks19__", (ftnlen)3785)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3787)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3787)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3787)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3787)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3787)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3787)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Test the structure of a subsetted segment created from the large"
	    " SPK file. Use the entire final mini-segment.", (ftnlen)109);

/*     Search the input file for the segment we'll copy from. */

    dafopr_("sp19mixed.bsp", &handle, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafbfs_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    daffna_(&found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up segment descriptor and segment ID. */

    dafgs_(descr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dafgn_(segid, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xbody = 3;
    xcentr = 10;
    s_copy(xref, "J2000", (ftnlen)32, (ftnlen)5);

/*     Create a segment identifier. */

    s_copy(segid, "SPK type 19 test segment", (ftnlen)60, (ftnlen)24);

/*     Set the interval count. Note that a zero-length */
/*     interval will be added on the left side of the */
/*     output segment to accommodate correct interpolation */
/*     when the request time is at the start of the */
/*     final interval and the interval selection method */
/*     is "select first." */

    tmpniv = 2;

/*     Copy and adjust the packet counts. */

    movei_(npkts, &nintvl, tmpnpk);
    tmpnpk[(i__1 = nintvl - 2) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", 
	    i__1, "f_spks19__", (ftnlen)3862)] = (npad << 1) + 1;

/*     Set the index of the first packet we'll use. */

    pktbeg = rectot - tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 :
	     s_rnge("tmpnpk", i__1, "f_spks19__", (ftnlen)3868)] - tmpnpk[(
	    i__3 = nintvl - 2) < 21000 && 0 <= i__3 ? i__3 : s_rnge("tmpnpk", 
	    i__3, "f_spks19__", (ftnlen)3868)] + 1;

/*     PKTOFF is the offset of the first packet to be */
/*     used. Since PACKTS is a one-dimensional array, */
/*     we must skip over the total number of addresses */
/*     occupied by the packets that precede the first */
/*     of the subset. */

    pktoff = pktsum[(i__1 = pktbeg - 1) < 2100001 && 0 <= i__1 ? i__1 : 
	    s_rnge("pktsum", i__1, "f_spks19__", (ftnlen)3877)] + 1;

/*     The first interval in the subset segment is a */
/*     truncated version of the penultimate interval of the */
/*     input file. */

    ivbeg = nintvl - 1;

/*     Copy and adjust the interval bounds. */

    i__1 = nintvl + 1;
    moved_(ivlbds, &i__1, tmpivb);
    tmpivb[(i__1 = nintvl - 2) < 21001 && 0 <= i__1 ? i__1 : s_rnge("tmpivb", 
	    i__1, "f_spks19__", (ftnlen)3892)] = epochs[(i__3 = pktbeg - 1) < 
	    2100000 && 0 <= i__3 ? i__3 : s_rnge("epochs", i__3, "f_spks19__",
	     (ftnlen)3892)];
    tmptot = tmpnpk[(i__1 = nintvl - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge(
	    "tmpnpk", i__1, "f_spks19__", (ftnlen)3894)] + tmpnpk[(i__3 = 
	    nintvl - 2) < 21000 && 0 <= i__3 ? i__3 : s_rnge("tmpnpk", i__3, 
	    "f_spks19__", (ftnlen)3894)];
    begin = ivlbds[(i__1 = nintvl - 1) < 21001 && 0 <= i__1 ? i__1 : s_rnge(
	    "ivlbds", i__1, "f_spks19__", (ftnlen)3897)];
    end = ivlbds[(i__1 = nintvl) < 21001 && 0 <= i__1 ? i__1 : s_rnge("ivlbds"
	    , i__1, "f_spks19__", (ftnlen)3898)];
    t_spks19__(&handle, descr, segid, &begin, &end, "spk19tempsub.bsp", &
	    xbody, &xcentr, xref, &begin, &end, &tmpniv, &tmpnpk[(i__1 = 
	    ivbeg - 1) < 21000 && 0 <= i__1 ? i__1 : s_rnge("tmpnpk", i__1, 
	    "f_spks19__", (ftnlen)3902)], &subtps[(i__3 = ivbeg - 1) < 21000 
	    && 0 <= i__3 ? i__3 : s_rnge("subtps", i__3, "f_spks19__", (
	    ftnlen)3902)], &degres[(i__2 = ivbeg - 1) < 21000 && 0 <= i__2 ? 
	    i__2 : s_rnge("degres", i__2, "f_spks19__", (ftnlen)3902)], &
	    packts[(i__4 = pktoff - 1) < 25200000 && 0 <= i__4 ? i__4 : 
	    s_rnge("packts", i__4, "f_spks19__", (ftnlen)3902)], &epochs[(
	    i__5 = pktbeg - 1) < 2100000 && 0 <= i__5 ? i__5 : s_rnge("epochs"
	    , i__5, "f_spks19__", (ftnlen)3902)], &tmpivb[(i__6 = ivbeg - 1) <
	     21001 && 0 <= i__6 ? i__6 : s_rnge("tmpivb", i__6, "f_spks19__", 
	    (ftnlen)3902)], &sellst, &c_true, ok, (ftnlen)60, (ftnlen)16, (
	    ftnlen)32);

/*     Close the input file from which we created the subset segment. */

    dafcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete SPK files.", (ftnlen)28);
    if (exists_("test19err.bsp", (ftnlen)13)) {
	delfil_("test19err.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("sp19mixed.bsp", (ftnlen)13)) {
	delfil_("sp19mixed.bsp", (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("sp19t1.bsp", (ftnlen)10)) {
	delfil_("sp19t1.bsp", (ftnlen)10);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("sp19subset.bsp", (ftnlen)14)) {
	delfil_("sp19subset.bsp", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_spks19__ */

