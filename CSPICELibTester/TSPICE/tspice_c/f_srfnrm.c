/* f_srfnrm.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c_n666 = -666;
static integer c__10 = 10;
static integer c__3 = 3;
static doublereal c_b51 = 2.;
static doublereal c_b83 = 3.;
static integer c__5 = 5;
static integer c__14 = 14;
static integer c__100 = 100;
static doublereal c_b265 = 0.;
static integer c__499 = 499;
static integer c__599 = 599;
static doublereal c_b470 = -1.;

/* $Procedure      F_SRFNRM ( SRFNRM family tests ) */
/* Subroutine */ int f_srfnrm__(logical *ok)
{
    /* Initialized data */

    static char refs[32*2*2] = "IAU_MARS                        " "MARS_FIXE"
	    "D                      " "IAU_PHOBOS                      " "   "
	    "                             ";
    static integer nrefs[2] = { 2,1 };
    static char trgnms[32*2] = "Mars                            " "PHOBOS   "
	    "                       ";
    static char methds[500*4] = "ELLIPSOID                                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "         " "dsk/unprioritized/surfaces=\"high-res\"             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "  " "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                           " 
	    "UNPRIORITIZED/ dsk /SURFACES =\"LOW-RES\"                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                       ";

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int srfnrm_(char *, char *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen), 
	    latrec_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    vminus_(doublereal *, doublereal *), dskxsi_(logical *, char *, 
	    integer *, integer *, doublereal *, char *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, integer *, 
	    integer *, doublereal *, doublereal *, integer *, logical *, 
	    ftnlen, ftnlen), mxv_(doublereal *, doublereal *, doublereal *), 
	    t_elds2z__(integer *, integer *, char *, integer *, integer *, 
	    char *, ftnlen, ftnlen), surfnm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), unload_(char *, ftnlen)
	    , clpool_(void), ldpool_(char *, ftnlen), dvpool_(char *, ftnlen),
	     bodvcd_(integer *, char *, integer *, integer *, doublereal *, 
	    ftnlen);
    static doublereal dlat;
    static integer maxd;
    static doublereal dlon;
    static integer maxi, nlat, nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static integer npts;
    extern /* Subroutine */ int zzmaxrad_(doublereal *), zzsudski_(integer *, 
	    integer *, integer *, integer *);
    static integer i__, j, k;
    extern /* Subroutine */ int zzprsmet_(integer *, char *, integer *, char *
	    , char *, logical *, integer *, integer *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen, ftnlen);
    static integer n;
    static char label[50];
    static doublereal r__, radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char shape[32];
    extern /* Subroutine */ int dskn02_(integer *, integer *, integer *, 
	    doublereal *), vpack_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found, isdsk;
    static char title[320];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static integer ptidx;
    static doublereal xform[9]	/* was [3][3] */;
    static integer nsurf;
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *);
    static doublereal dc[1], badrad[3];
    static integer ic[1];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen), boddef_(char *, integer *,
	     ftnlen);
    extern doublereal pi_(void);
    static doublereal et;
    static integer dladsc[8], handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen), refchg_(integer *, 
	    integer *, doublereal *, doublereal *);
    extern doublereal halfpi_(void);
    extern logical matchi_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    static doublereal altrad[3];
    static integer segfid;
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *), dpr_(
	    void);
    extern logical exists_(char *, ftnlen);
    static char fixref[32], frmtxt[320*10], kvname[32], method[500], pntdef[
	    32], srfnms[32*5], subtyp[32], target[32], trmtyp[32], trgfrm[32],
	     utc[50];
    static doublereal dskdsc[24], et0, lat, lon, lonlat[200]	/* was [2][
	    100] */, maxrad, normal[3], normls[300]	/* was [3][100] */, 
	    raydir[3], spoint[3], srfpts[300]	/* was [3][100] */, tmpvec[3],
	     tol, vertex[3], xnorml[300]	/* was [3][100] */, xpt[3];
    static integer bodyid, fixfid, mix, nsflat, nsflon, refidx, srfbod[5], 
	    srfids[5], srflst[100], surfid, trgcde, trgidx;
    static logical pri;
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen), chckxc_(logical *, char *, logical *, ftnlen), tstlsk_(
	    void), pipool_(char *, integer *, integer *, ftnlen), lmpool_(
	    char *, integer *, ftnlen), furnsh_(char *, ftnlen), gdpool_(char 
	    *, integer *, integer *, integer *, doublereal *, logical *, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen),
	     pdpool_(char *, integer *, doublereal *, ftnlen), pcpool_(char *,
	     integer *, char *, ftnlen, ftnlen), bodvar_(integer *, char *, 
	    integer *, doublereal *, ftnlen), namfrm_(char *, integer *, 
	    ftnlen), latsrf_(char *, char *, doublereal *, char *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the higher-level SPICELIB geometry routine SRFNRM. */
/*     Use DSK-based and ellipsoidal target shape models. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine SRFNRM. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0 13-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     REFS is a two-dimensional array. There's a set of */
/*     ray reference  frames for each target. Currently */
/*     there are only two targets: Mars and Phobos. */


/*     Note that the last two method strings are identical. This */
/*     is done to test the logic that uses saved values obtained */
/*     by parsing method string. */


/*     Begin every test family with an open call. */

    topen_("F_SRFNRM", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create PCK file.", (ftnlen)24);

/*     Create the PCK file, and load it. Do not delete it. */

    t_pck08__("test_0008.tpc", &c_true, &c_true, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create LSK, load it, and delete it. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set initial time. */

    s_copy(utc, "2004 FEB 17", (ftnlen)50, (ftnlen)11);
    str2et_(utc, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = et0;

/*     Add an incomplete frame definition to the kernel pool; */
/*     we'll need this later. */

    pipool_("FRAME_BAD_NAME", &c__1, &c_n666, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create alternate Mars-fixed frame.", (ftnlen)41);
    s_copy(frmtxt, "FRAME_MARS_FIXED         = 1499000", (ftnlen)320, (ftnlen)
	    34);
    s_copy(frmtxt + 320, "FRAME_1499000_NAME       = 'MARS_FIXED' ", (ftnlen)
	    320, (ftnlen)40);
    s_copy(frmtxt + 640, "FRAME_1499000_CLASS      = 4", (ftnlen)320, (ftnlen)
	    28);
    s_copy(frmtxt + 960, "FRAME_1499000_CLASS_ID   = 1499000", (ftnlen)320, (
	    ftnlen)34);
    s_copy(frmtxt + 1280, "FRAME_1499000_CENTER     = 499", (ftnlen)320, (
	    ftnlen)30);
    s_copy(frmtxt + 1600, "TKFRAME_1499000_RELATIVE = 'IAU_MARS' ", (ftnlen)
	    320, (ftnlen)38);
    s_copy(frmtxt + 1920, "TKFRAME_1499000_SPEC     = 'MATRIX' ", (ftnlen)320,
	     (ftnlen)36);
    s_copy(frmtxt + 2240, "TKFRAME_1499000_MATRIX   = ( 0, 1, 0,", (ftnlen)
	    320, (ftnlen)37);
    s_copy(frmtxt + 2560, "                           0, 0, 1,", (ftnlen)320, 
	    (ftnlen)35);
    s_copy(frmtxt + 2880, "                           1, 0, 0, )", (ftnlen)
	    320, (ftnlen)37);
    lmpool_(frmtxt, &c__10, (ftnlen)320);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 200;
    nlat = 100;
    if (exists_("latsrf_dsk0.bds", (ftnlen)15)) {
	delfil_("latsrf_dsk0.bds", (ftnlen)15);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "latsrf_dsk0.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars is very low-res. We also use a */
/*     different scale for the Mars radii used to create */
/*     the tessellated shape model. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 40;
    nlat = 20;
    if (exists_("latsrf_dsk1.bds", (ftnlen)15)) {
	delfil_("latsrf_dsk1.bds", (ftnlen)15);
    }
    s_copy(kvname, "BODY499_RADII", (ftnlen)32, (ftnlen)13);
    gdpool_(kvname, &c__1, &c__3, &n, radii, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("499 radii FOUND", &found, &c_true, ok, (ftnlen)15);
    vscl_(&c_b51, radii, altrad);
    pdpool_(kvname, &c__3, altrad, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "latsrf_dsk1.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("latsrf_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore normal Mars radii. */

    pdpool_(kvname, &c__3, radii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is low-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("latsrf_dsk2.bds", (ftnlen)15)) {
	delfil_("latsrf_dsk2.bds", (ftnlen)15);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "latsrf_dsk2.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("latsrf_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. We also use a */
/*     different scale for the Mars radii used to create */
/*     the tessellated shape model. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("latsrf_dsk3.bds", (ftnlen)15)) {
	delfil_("latsrf_dsk3.bds", (ftnlen)15);
    }
    s_copy(kvname, "BODY401_RADII", (ftnlen)32, (ftnlen)13);
    gdpool_(kvname, &c__1, &c__3, &n, radii, &found, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("401 radii FOUND", &found, &c_true, ok, (ftnlen)15);
    vscl_(&c_b83, radii, altrad);
    pdpool_(kvname, &c__3, altrad, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "latsrf_dsk3.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore normal Phobos radii. */

    pdpool_(kvname, &c__3, radii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the DSK. */

    furnsh_("latsrf_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create surface map.", (ftnlen)26);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 499;
    srfids[2] = 3;
    s_copy(srfnms + 64, "c-g", (ftnlen)32, (ftnlen)3);
    srfbod[3] = 401;
    srfids[3] = 1;
    s_copy(srfnms + 96, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[4] = 401;
    srfids[4] = 2;
    s_copy(srfnms + 128, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__5, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__5, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__5, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate a grid of planetocentric longitude/latitude coordinate */
/*     pairs. These, combined with surface models, will yield a grid of */
/*     coordinates at which to compute surface points. */

    nsflon = 4;
    nsflat = 5;
    npts = nsflon * nsflat;
    dlon = pi_() * 2 / nsflon;
    dlat = pi_() / (nsflat - 1);
    k = 0;
    i__1 = nsflon;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        We shift the coordinates away from possible plate */
/*        edges because we can't expect the normal vectors */
/*        to match our computed values at those locations. */

	lon = (i__ - 1) * dlon + .001;
	i__2 = nsflat;
	for (j = 1; j <= i__2; ++j) {
	    d__1 = halfpi_() - (j - 1) * dlat;
	    d__2 = -halfpi_();
	    d__3 = halfpi_();
	    lat = brcktd_(&d__1, &d__2, &d__3);
	    if (j <= nsflat / 2) {
		lat += -.001;
	    } else {
		lat += .001;
	    }
	    ++k;
	    lonlat[(i__3 = (k << 1) - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge(
		    "lonlat", i__3, "f_srfnrm__", (ftnlen)577)] = lon;
	    lonlat[(i__3 = (k << 1) - 1) < 200 && 0 <= i__3 ? i__3 : s_rnge(
		    "lonlat", i__3, "f_srfnrm__", (ftnlen)578)] = lat;
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     The first test loop follows. In this loop, we call SRFNRM */
/*     once for each coordinate pair. */


/*     Loop over every choice of target. */

    for (trgidx = 1; trgidx <= 2; ++trgidx) {
	s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("trgnms", i__1, "f_srfnrm__", (ftnlen)597)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the target ID code. */

	bodn2c_(target, &trgcde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get target radii. */

	bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over the surface point sequence. */

	i__1 = npts;
	for (ptidx = 1; ptidx <= i__1; ++ptidx) {
	    lon = lonlat[(i__2 = (ptidx << 1) - 2) < 200 && 0 <= i__2 ? i__2 :
		     s_rnge("lonlat", i__2, "f_srfnrm__", (ftnlen)616)];
	    lat = lonlat[(i__2 = (ptidx << 1) - 1) < 200 && 0 <= i__2 ? i__2 :
		     s_rnge("lonlat", i__2, "f_srfnrm__", (ftnlen)617)];

/*           Loop over every target body-fixed frame choice. */

	    i__3 = nrefs[(i__2 = trgidx - 1) < 2 && 0 <= i__2 ? i__2 : s_rnge(
		    "nrefs", i__2, "f_srfnrm__", (ftnlen)622)];
	    for (refidx = 1; refidx <= i__3; ++refidx) {
		s_copy(trgfrm, refs + (((i__2 = refidx + (trgidx << 1) - 3) < 
			4 && 0 <= i__2 ? i__2 : s_rnge("refs", i__2, "f_srfn"
			"rm__", (ftnlen)624)) << 5), (ftnlen)32, (ftnlen)32);
		namfrm_(trgfrm, &fixfid, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Loop over all method choices. */

		for (mix = 1; mix <= 4; ++mix) {
		    s_copy(method, methds + ((i__2 = mix - 1) < 4 && 0 <= 
			    i__2 ? i__2 : s_rnge("methds", i__2, "f_srfnrm__",
			     (ftnlen)634)) * 500, (ftnlen)500, (ftnlen)500);
		    isdsk = matchi_(method, "*DSK*", "*", "?", (ftnlen)500, (
			    ftnlen)5, (ftnlen)1, (ftnlen)1);

/* - Case: ------------------------------------------------------ */

		    s_copy(title, "Target = #; TRGFRM = #; METHOD = #; Longi"
			    "tude (deg) = #; Latitude (deg) = #; ET = #.", (
			    ftnlen)320, (ftnlen)84);
		    repmc_(title, "#", target, title, (ftnlen)320, (ftnlen)1, 
			    (ftnlen)32, (ftnlen)320);
		    repmc_(title, "#", trgfrm, title, (ftnlen)320, (ftnlen)1, 
			    (ftnlen)32, (ftnlen)320);
		    repmc_(title, "#", method, title, (ftnlen)320, (ftnlen)1, 
			    (ftnlen)500, (ftnlen)320);
		    d__1 = lon * dpr_();
		    repmd_(title, "#", &d__1, &c__14, title, (ftnlen)320, (
			    ftnlen)1, (ftnlen)320);
		    d__1 = lat * dpr_();
		    repmd_(title, "#", &d__1, &c__14, title, (ftnlen)320, (
			    ftnlen)1, (ftnlen)320);
		    repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (
			    ftnlen)1, (ftnlen)320);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)320);

/*                 Extract the surface list from the method string. */

		    zzprsmet_(&trgcde, method, &c__100, shape, subtyp, &pri, &
			    nsurf, srflst, pntdef, trmtyp, (ftnlen)500, (
			    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Obtain a surface point corresponding to the input */
/*                 coordinates. */

		    latsrf_(method, target, &et, trgfrm, &c__1, &lonlat[(i__2 
			    = (ptidx << 1) - 2) < 200 && 0 <= i__2 ? i__2 : 
			    s_rnge("lonlat", i__2, "f_srfnrm__", (ftnlen)667)]
			    , spoint, (ftnlen)500, (ftnlen)32, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Call SRFNRM to produce a normal vector for the */
/*                 input coordinates. */

		    srfnrm_(method, target, &et, trgfrm, &c__1, spoint, 
			    normal, (ftnlen)500, (ftnlen)32, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Prepare to check the result. If the target shape is */
/*                 given by DSK data, we'll need to find the plate on */
/*                 which the surface point lies. */

/*                 Get an outer bounding radius for the target. */

		    if (isdsk) {

/*                    Perform initialization to enable generation */
/*                    of a bounding radius for the current surface */
/*                    list. */

			zzsudski_(&trgcde, &nsurf, srflst, &fixfid);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			zzmaxrad_(&maxrad);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Create an inward-pointing ray. The surface */
/*                    intercept of this ray is our expected result from */
/*                    SRFNRM. */

			r__ = maxrad * 2.;
			latrec_(&r__, &lon, &lat, vertex);
			vminus_(vertex, raydir);
/*                    Call DSKXSI to produce a surface point for the */
/*                    input coordinates, and to identify the plate */
/*                    on which this point lies. */

			maxd = 1;
			maxi = 1;
			dskxsi_(&c_false, target, &nsurf, srflst, &et, trgfrm,
				 vertex, raydir, &maxd, &maxi, xpt, &handle, 
				dladsc, dskdsc, dc, ic, &found, (ftnlen)32, (
				ftnlen)32);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*                    Get the normal vector for the indicated plate. */

			dskn02_(&handle, dladsc, ic, xnorml);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    The vector returned by DSKN02 is in the frame */
/*                    of the designated segment. Transform the vector */
/*                    to the SRFNRM request frame, if necessary. */

			segfid = i_dnnt(&dskdsc[4]);
			if (segfid != fixfid) {
			    refchg_(&segfid, &fixfid, &et, xform);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    mxv_(xform, xnorml, tmpvec);
			    vequ_(tmpvec, xnorml);
			}
		    } else {

/*                    This is the ellipsoid case. Compute the */
/*                    surface normal directly. (Recall SURFNM */
/*                    is the low-level ellipsoid routine.) */

			surfnm_(radii, &radii[1], &radii[2], spoint, xnorml);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }

/*                 We expect near-perfect agreement between the */
/*                 actual and expected normal vector. */

		    tol = 1e-14;
		    chckad_("NORMAL", normal, "~~/", xnorml, &c__3, &tol, ok, 
			    (ftnlen)6, (ftnlen)3);
		}

/*              End of the method loop. */

	    }

/*           End of the reference frame loop. */

	}

/*        End of the surface point loop. */

    }

/*     End of the target loop. */


/* --- Case: ------------------------------------------------------ */


/*     The second test loop follows. In this loop, we call SRFNRM */
/*     once for the full set of coordinate pairs. */


/*     Loop over every choice of target. */

    for (trgidx = 1; trgidx <= 2; ++trgidx) {
	s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("trgnms", i__1, "f_srfnrm__", (ftnlen)803)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the target ID code. */

	bodn2c_(target, &trgcde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get target radii. */

	bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over every target body-fixed frame choice. */

	i__3 = nrefs[(i__1 = trgidx - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
		"nrefs", i__1, "f_srfnrm__", (ftnlen)818)];
	for (refidx = 1; refidx <= i__3; ++refidx) {
	    s_copy(trgfrm, refs + (((i__1 = refidx + (trgidx << 1) - 3) < 4 &&
		     0 <= i__1 ? i__1 : s_rnge("refs", i__1, "f_srfnrm__", (
		    ftnlen)820)) << 5), (ftnlen)32, (ftnlen)32);
	    namfrm_(trgfrm, &fixfid, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over all method choices. */

	    for (mix = 1; mix <= 4; ++mix) {
		s_copy(method, methds + ((i__1 = mix - 1) < 4 && 0 <= i__1 ? 
			i__1 : s_rnge("methds", i__1, "f_srfnrm__", (ftnlen)
			830)) * 500, (ftnlen)500, (ftnlen)500);
		isdsk = matchi_(method, "*DSK*", "*", "?", (ftnlen)500, (
			ftnlen)5, (ftnlen)1, (ftnlen)1);

/* - Case: ------------------------------------------------------ */

		s_copy(title, "Target = #; TRGFRM = #; METHOD = #; ET = #.", (
			ftnlen)320, (ftnlen)43);
		repmc_(title, "#", target, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		repmc_(title, "#", trgfrm, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		repmc_(title, "#", method, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)500, (ftnlen)320);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              Check the output point array. In order to do this, we'll */
/*              extract the surface list from the method string. */
		zzprsmet_(&trgcde, method, &c__100, shape, subtyp, &pri, &
			nsurf, srflst, pntdef, trmtyp, (ftnlen)500, (ftnlen)
			32, (ftnlen)32, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Generate the array of surface points. */

		latsrf_(method, target, &et, trgfrm, &npts, lonlat, srfpts, (
			ftnlen)500, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Call SRFNRM to produce a normal vector for each */
/*              surface point. */

		srfnrm_(method, target, &et, trgfrm, &npts, srfpts, normls, (
			ftnlen)500, (ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (isdsk) {

/*                 To check the normal vectors, we'll find the plates */
/*                 with which the vectors are associated. We'll use */
/*                 DSKXSI for this. */

/*                 Get an outer bounding radius for the target. */

/*                 Perform initialization to enable generation */
/*                 of a bounding radius for the current surface */
/*                 list. */

		    zzsudski_(&trgcde, &nsurf, srflst, &fixfid);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    zzmaxrad_(&maxrad);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Create an array of inward-pointing rays. The */
/*                 respective surface intercepts of these rays are our */
/*                 expected results from SRFNRM. */

		    r__ = maxrad * 2.;
		    i__1 = npts;
		    for (ptidx = 1; ptidx <= i__1; ++ptidx) {
			lon = lonlat[(i__2 = (ptidx << 1) - 2) < 200 && 0 <= 
				i__2 ? i__2 : s_rnge("lonlat", i__2, "f_srfn"
				"rm__", (ftnlen)898)];
			lat = lonlat[(i__2 = (ptidx << 1) - 1) < 200 && 0 <= 
				i__2 ? i__2 : s_rnge("lonlat", i__2, "f_srfn"
				"rm__", (ftnlen)899)];
			latrec_(&r__, &lon, &lat, vertex);
			vminus_(vertex, raydir);
			maxd = 1;
			maxi = 1;
			dskxsi_(&c_false, target, &nsurf, srflst, &et, trgfrm,
				 vertex, raydir, &maxd, &maxi, xpt, &handle, 
				dladsc, dskdsc, dc, ic, &found, (ftnlen)32, (
				ftnlen)32);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Get the normal vector for the current surface */
/*                    point. */

			dskn02_(&handle, dladsc, ic, &xnorml[(i__2 = ptidx * 
				3 - 3) < 300 && 0 <= i__2 ? i__2 : s_rnge(
				"xnorml", i__2, "f_srfnrm__", (ftnlen)918)]);

/*                    The vector returned by DSKN02 is in the frame */
/*                    of the designated segment. Transform the vector */
/*                    to the SRFNRM request frame, if necessary. */

			segfid = i_dnnt(&dskdsc[4]);
			if (segfid != fixfid) {
			    refchg_(&segfid, &fixfid, &et, xform);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    mxv_(xform, &xnorml[(i__2 = ptidx * 3 - 3) < 300 
				    && 0 <= i__2 ? i__2 : s_rnge("xnorml", 
				    i__2, "f_srfnrm__", (ftnlen)933)], tmpvec)
				    ;
			    vequ_(tmpvec, &xnorml[(i__2 = ptidx * 3 - 3) < 
				    300 && 0 <= i__2 ? i__2 : s_rnge("xnorml",
				     i__2, "f_srfnrm__", (ftnlen)934)]);
			}
		    }
		} else {

/*                 Compute normal vectors on the reference ellipsoid */
/*                 for the current target. */

		    i__1 = npts;
		    for (ptidx = 1; ptidx <= i__1; ++ptidx) {
			surfnm_(radii, &radii[1], &radii[2], &srfpts[(i__2 = 
				ptidx * 3 - 3) < 300 && 0 <= i__2 ? i__2 : 
				s_rnge("srfpts", i__2, "f_srfnrm__", (ftnlen)
				949)], &xnorml[(i__4 = ptidx * 3 - 3) < 300 &&
				 0 <= i__4 ? i__4 : s_rnge("xnorml", i__4, 
				"f_srfnrm__", (ftnlen)949)]);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }
		}

/*              Check the normal vectors. */

/*              We should get extremely good agreement. */

		i__1 = npts;
		for (ptidx = 1; ptidx <= i__1; ++ptidx) {
		    s_copy(label, "FOUND #", (ftnlen)50, (ftnlen)7);
		    repmi_(label, "#", &ptidx, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 We expect near-perfect agreement between the */
/*                 actual and expected normal vector. */

		    tol = 1e-14;
		    i__2 = npts * 3;
		    chckad_("NORMLS", normls, "~~/", xnorml, &i__2, &tol, ok, 
			    (ftnlen)6, (ftnlen)3);
		    if (! (*ok)) {
			s_stop("", (ftnlen)0);
		    }
		}
	    }

/*           End of the method loop. */

	}

/*        End of the reference frame loop. */

    }

/*     End of the target loop. */

/* *********************************************************************** */

/*     Normal case: input handling */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Input handling tests:  make sure target */
/*     can be identified using integer "name." */

    tcase_("Use integer target name.", (ftnlen)24);

/*     Set target and target-fixed frame. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "ellipsoid", (ftnlen)500, (ftnlen)9);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__3 = (ptidx << 1) 
	    - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge("lonlat", i__3, "f_srfnr"
	    "m__", (ftnlen)1020)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, xnorml, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, "499", &et, fixref, &c__1, spoint, normal, (ftnlen)500, (
	    ftnlen)3, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("NORMAL", normal, "=", xnorml, &c__3, &c_b265, ok, (ftnlen)6, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Normal case: state change detection */

/* *********************************************************************** */

/*     Certain subsystem state changes must be detected and responded to */
/*     by SINCPT. The subsystems (or structures) having states that must */
/*     be monitored are: */

/*        - Target name-ID mapping */

/*        - Observer name-ID mapping */

/*        - Surface name-ID mapping */

/*        - Target body-fixed frame definition */

/*        - ZZDSKBSR state */


/* --- Case: ------------------------------------------------------ */

    tcase_("Target name changed to JUPITER for ID code 499.", (ftnlen)47);

/*     First, get expected intercept. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, xnorml, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    boddef_("JUPITER", &c__499, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, normal, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("NORMAL", normal, "=", xnorml, &c__3, &c_b265, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    boddef_("JUPITER", &c__599, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars high-res surface name changed to AAAbbb.", (ftnlen)45);

/*     Get expected results first. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "dsk/unprioritized/surfaces = 1", (ftnlen)500, (ftnlen)30);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__3 = (ptidx << 1) 
	    - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge("lonlat", i__3, "f_srfnr"
	    "m__", (ftnlen)1114)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, xnorml, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(srfnms, "AAAbbb", (ftnlen)32, (ftnlen)6);
    pcpool_("NAIF_SURFACE_NAME", &c__5, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized/surfaces = AAAbbb", (ftnlen)500, (
	    ftnlen)35);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, normal, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("NORMAL", normal, "=", xnorml, &c__3, &c_b265, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    pcpool_("NAIF_SURFACE_NAME", &c__5, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars high-res DSK.", (ftnlen)25);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "dsk/unprioritized/surfaces = low-res", (ftnlen)500, (
	    ftnlen)36);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__3 = (ptidx << 1) 
	    - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge("lonlat", i__3, "f_srfnr"
	    "m__", (ftnlen)1166)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, xnorml, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unload the high-res DSK; set METHOD to remove */
/*     surface specification. */

    unload_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, normal, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("NORMAL", normal, "=", xnorml, &c__3, &c_b265, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars low-res DSK; reload Mars high-res DSK.", (ftnlen)50);

/*     Restore DSK, unload low-res DSK, and repeat computation. */

    furnsh_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    latsrf_(method, target, &et, fixref, &c__1, &lonlat[(i__3 = (ptidx << 1) 
	    - 2) < 200 && 0 <= i__3 ? i__3 : s_rnge("lonlat", i__3, "f_srfnr"
	    "m__", (ftnlen)1217)], spoint, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, xnorml, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the result matches that obtained with the */
/*     high-res DSK specified. */

    s_copy(method, "dsk/unprioritized/ SURFACES = \"HIGH-RES\" ", (ftnlen)500,
	     (ftnlen)41);
    srfnrm_(method, target, &et, fixref, &c__1, spoint, normal, (ftnlen)500, (
	    ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact matches here. */

    chckad_("NORMAL", normal, "=", xnorml, &c__3, &c_b265, ok, (ftnlen)6, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Error handling tests follow. */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid method.", (ftnlen)15);
    et = 0.;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "ELLIPSOD", (ftnlen)500, (ftnlen)8);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1261)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    s_copy(method, "ELLIPSOID/UNPRIORITIZED", (ftnlen)500, (ftnlen)23);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1268)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    s_copy(method, "DSK/unprioritized/intercept", (ftnlen)500, (ftnlen)27);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1276)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);
    s_copy(method, "DSK/nadir/unprioritized", (ftnlen)500, (ftnlen)23);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1283)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);
    s_copy(method, "DSK", (ftnlen)500, (ftnlen)3);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1290)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADPRIORITYSPEC)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid target name.", (ftnlen)20);
    s_copy(method, "DSK/UNPRIORITIZED", (ftnlen)500, (ftnlen)17);
    srfnrm_(method, "XXX", &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 - 
	    3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnrm__"
	    , (ftnlen)1302)], normal, (ftnlen)500, (ftnlen)3, (ftnlen)32);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid reference frame center", (ftnlen)30);
    srfnrm_(method, target, &et, "IAU_EARTH", &c__1, &srfpts[(i__3 = npts / 2 
	    * 3 - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_s"
	    "rfnrm__", (ftnlen)1313)], normal, (ftnlen)500, (ftnlen)32, (
	    ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unrecognized reference frame", (ftnlen)28);
    srfnrm_(method, target, &et, "ZZZ", &c__1, &srfpts[(i__3 = npts / 2 * 3 - 
	    3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnrm__"
	    , (ftnlen)1324)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("No orientation data for target", (ftnlen)30);

/*     This error applies only to the DSK case. */

    s_copy(method, "UNPRIORITIZED  /  DSK", (ftnlen)500, (ftnlen)21);
    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a class 2 (PCK) frame with no orientation data. */

    s_copy(frmtxt, "FRAME_MARS_FIXED_PCK     = 1499001", (ftnlen)320, (ftnlen)
	    34);
    s_copy(frmtxt + 320, "FRAME_1499001_NAME       = 'MARS_FIXED_PCK' ", (
	    ftnlen)320, (ftnlen)44);
    s_copy(frmtxt + 640, "FRAME_1499001_CLASS      = 2", (ftnlen)320, (ftnlen)
	    28);
    s_copy(frmtxt + 960, "FRAME_1499001_CLASS_ID   = 1499001", (ftnlen)320, (
	    ftnlen)34);
    s_copy(frmtxt + 1280, "FRAME_1499001_CENTER     = 499", (ftnlen)320, (
	    ftnlen)30);
    lmpool_(frmtxt, &c__5, (ftnlen)320);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    lmpool_(frmtxt, &c__10, (ftnlen)320);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, "MARS_FIXED_PCK", &c__1, &srfpts[(i__3 = 
	    npts / 2 * 3 - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", 
	    i__3, "f_srfnrm__", (ftnlen)1359)], normal, (ftnlen)500, (ftnlen)
	    32, (ftnlen)14);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No radius data for target", (ftnlen)25);
    s_copy(method, "ELLIPSOID", (ftnlen)500, (ftnlen)9);
    dvpool_("BODY499_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1380)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad radius data for target", (ftnlen)26);
    s_copy(method, "ELLIPSOID", (ftnlen)500, (ftnlen)9);

/*     Fetch original radii. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Overwrite good radii with bad in the kernel pool. */

    vpack_(&c_b470, &c_b265, &c_b83, badrad);
    pdpool_("BODY499_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1408)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/*     Replace original radii. */

    pdpool_("BODY499_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded DSKs.", (ftnlen)15);
    s_copy(method, "UNPRIORITIZED  /  DSK", (ftnlen)500, (ftnlen)21);
    unload_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "dsk/unprioritized", (ftnlen)500, (ftnlen)17);
    srfnrm_(method, target, &et, fixref, &c__1, &srfpts[(i__3 = npts / 2 * 3 
	    - 3) < 300 && 0 <= i__3 ? i__3 : s_rnge("srfpts", i__3, "f_srfnr"
	    "m__", (ftnlen)1445)], normal, (ftnlen)500, (ftnlen)32, (ftnlen)32)
	    ;
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Clean up. */

    delfil_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("latsrf_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("latsrf_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("latsrf_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("latsrf_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("latsrf_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_srfnrm__ */

