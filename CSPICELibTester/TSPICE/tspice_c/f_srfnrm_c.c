/*

-Procedure f_srfnrm_c ( srfnrm_c tests )

 
-Abstract
 
   Exercise the CSPICE wrapper srfnrm_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_srfnrm_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrapper 

      srfnrm_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Removed unnecessary call to dascls_c.

   01-AUG-2016 (NJB)

      Original version.

-Index_Entries

   test srfnrm_c

-&
*/

{ /* Begin f_srfnrm_c */

 
   /*
   Constants
   */
   #define PCK0            "srfnrm_test.tpc"
   #define MAXD            SPICE_DSKXSI_DCSIZE
   #define MAXI            SPICE_DSKXSI_ICSIZE
   #define MAXN            10000
   #define TITLEN          321
   #define LABLEN          51
   #define TIGHT           1.e-12

   /*
   Local variables
   */
   SpiceBoolean            found;
   SpiceBoolean            pri;

   SpiceDLADescr           dladsc;
   SpiceDSKDescr           dskdsc;

   SpiceChar             * dsk;
   SpiceChar             * fixref;
   SpiceChar             * frame;
   SpiceChar               label  [ LABLEN ];
   SpiceChar             * method;
   SpiceChar             * target;
   SpiceChar               title  [ TITLEN ];

   SpiceDouble             a;
   SpiceDouble             b;
   SpiceDouble             c;
   SpiceDouble             dc      [SPICE_DSKXSI_DCSIZE];
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             et;
   SpiceDouble             lat;
   SpiceDouble             lon;
   SpiceDouble             lonlat [MAXN][2];
   SpiceDouble             normal [3];
   static SpiceDouble      normls [MAXN][3];
   SpiceDouble             r;
   SpiceDouble             radii  [3];
   SpiceDouble             raydir [3];
   static SpiceDouble      srfpts [MAXN][3];
   SpiceDouble             tol;
   SpiceDouble             vertex [3];
   SpiceDouble             xpt    [3];
   SpiceDouble             xnorml [3];

   SpiceInt                bodyid;
   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                ic     [SPICE_DSKXSI_ICSIZE];
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                n;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                npts;
   SpiceInt                nslat;
   SpiceInt                nslon;
   SpiceInt                nsurf;
   SpiceInt                plid;
   SpiceInt                surfid;
   SpiceInt                srflst  [ SPICE_SRF_MAXSRF ];


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_srfnrm_c" );
   

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a text PCK." );
   
   if ( exists_c(PCK0) ) 
   {
      removeFile(PCK0);
   }

   /*
   Don't load the PCK; do save it. 
   */
   tstpck_c ( PCK0, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load via furnsh_c to avoid later complexities. 
   */
   furnsh_c ( PCK0 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a DSK file containing segments for "
             "Mars and Saturn."                                 );
 

   /*
   We'll use a test utility that creates a tessellated plate model DSK. 
   */
   bodyid = 499;
   surfid = 1;
   frame  = "IAU_MARS";
   nlon   = 80;
   nlat   = 40;

   dsk    = "srfnrm_test_0.bds";

   if ( exists_c(dsk) )
   {
      removeFile( dsk );
   }

   /*
   Create the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Add a Saturn segment. 
   */
   bodyid = 699;
   surfid = 2;
   frame  = "IAU_SATURN";
   nlon   = 60;
   nlat   = 30;

   /*
   Append to the DSK. 
   */
   t_elds2z_c ( bodyid, surfid, frame, nlon, nlat, dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 


   /*
   Load the dsk for later use. 
   */
   furnsh_c ( dsk );
   chckxc_c ( SPICEFALSE, " ", ok ); 


 


   /*
    ********************************************************************


    MARS CASES


    ********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Mars normal vector test. DSK target. Surface list is "
             "non-empty."                                           );

   /*
   We'll compute outward surface normals at a set of points for which
   the correct normals are easily obtained.

   We'll create a set of rays pointing inward toward the target (Mars)
   and find the surface intercepts of these rays. We'll obtain the IDs
   of the plates on which the intercepts occur and get the outward
   normals of those plates.
   */

   /*
   The surface list consists of one surface ID (for the only Mars
   segment).
   */
   target    = "Mars";
   fixref    = "IAU_MARS";
   nsurf     = 1;
   srflst[0] = 1;

   /*
   Get the target radii for later use. 
   */
   bodvrd_c ( target, "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );

   a = radii[0];
   b = radii[1];
   c = radii[2];

   /*
   Pick our longitude and latitude band counts so we don't 
   end up sampling from plate boundaries. This simplifies
   our checks on the outputs.
   */
   nslon = 37;
   nslat = 23;

   dlon  = twopi_c() / nslon;
   dlat  = pi_c()    / nslat;

   /*
   Pick a magnitude for the rays' vertices. 
   */
   r = 1.0e6;

   /*
   Pick an evaluation epoch. 
   */
   et = 10 * jyear_c();

   /*
   The prioritization flag is always "false" for the N0066 version
   of SPICE. 
   */
   pri = SPICEFALSE;


   /*
   Generate coodinates of surface points at which to find outward
   normal vectors.
   */



   method = "DSK/UNPRIORITIZED/SURFACES = 1";


   /*
   Create an array of lon/lat coordinates. 
   */

   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         /*
         Choose sample latitudes so that the poles are not hit. 
         */
         lat          = halfpi_c() - j*dlat;

         if ( j == 0 )
         {
            lat -= dlat/2;
         }
         else if ( j == nslat )
         {
            lat += dlat/2;
         }

         lonlat[k][0] = lon;
         lonlat[k][1] = lat;

         ++k;
      }
   }

   npts = k;

   /*
   Find the normal vectors at the surface points corresponding to the
   surface coordinates.

   First, we must find the Cartesian coordinates of the surface points.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the normals using dskxsi_c. 
   */
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      for ( j = 0;  j <= nslat;  j++ )
      {
         lon = lonlat[k][0];
         lat = lonlat[k][1];

         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Mars normal vector test for lon # (deg), lat #(deg).",
                   TITLEN                                                );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );
         chckxc_c ( SPICEFALSE, " ", ok );

         tcase_c ( title );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxsi_c ( pri,     target,  nsurf, srflst, et,    fixref,
                    vertex,  raydir,  MAXD,  MAXI,   xpt,   &handle,
                    &dladsc, &dskdsc, dc,    ic,     &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "srfnrm_c found", found, SPICETRUE, ok );

         /*
         Get the outward normal of the plate that was hit. The 
         plate ID is stored in the integer information component
         returned by dskxsi_c.
         */

         plid = ic[0];

         dskn02_c ( handle, &dladsc, plid, normal );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         /*
         Check the kth normal vector from dskxsi_c.
         */

         tol = TIGHT;

         chckad_c ( "kth normal", normls[k], "~~/", normal, 3, tol, ok );

         /*
         Onward. 
         */

         ++k;
      }
   }

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Mars normal vector test. DSK target. Surface list is "
             "empty."                                               );


   /*
   Change the method and epoch, then repeat the previous test. 
   */

   method = "DSK /Unprioritized";

   et     = -10 * jyear_c();


   /*
   We already have the array of lon/lat coordinates. 
   */
 

   /*
   Find the normal vectors at the surface points corresponding to the
   surface coordinates.

   First, we must find the Cartesian coordinates of the surface points.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the normals using dskxsi_c. 
   */
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      for ( j = 0;  j <= nslat;  j++ )
      {
         lon = lonlat[k][0];
         lat = lonlat[k][1];

         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Mars normal vector test for lon # (deg), lat #(deg).",
                   TITLEN                                                );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );
         chckxc_c ( SPICEFALSE, " ", ok );

         tcase_c ( title );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxsi_c ( pri,     target,  nsurf, srflst, et,    fixref,
                    vertex,  raydir,  MAXD,  MAXI,   xpt,   &handle,
                    &dladsc, &dskdsc, dc,    ic,     &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "srfnrm_c found", found, SPICETRUE, ok );

         /*
         Get the outward normal of the plate that was hit. The 
         plate ID is stored in the integer information component
         returned by dskxsi_c.
         */

         plid = ic[0];

         dskn02_c ( handle, &dladsc, plid, normal );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         /*
         Check the kth normal vector from dskxsi_c.
         */

         tol = TIGHT;

         chckad_c ( "kth normal", normls[k], "~~/", normal, 3, tol, ok );

         /*
         Onward. 
         */

         ++k;
      }
   }



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Mars normal vector test. Ellipsoidal target." );


  
   method = "ellipsoid";

   /*
   Generate surface points on the reference ellipsoid. Use the existing
   lon/lat coordinate array.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the results. 
   */

   for ( k = 0;  k < npts;  k++ )
   {

      surfnm_c ( a, b, c, srfpts[k], xnorml );
      chckxc_c ( SPICEFALSE, " ", ok );

      strncpy ( label, "normal vector @", LABLEN );

      repmi_c ( label, "@", k, LABLEN, label );
      chckxc_c ( SPICEFALSE, " ", ok );
         
      tol = TIGHT;

      chckad_c ( label, normls[k], "~~/", xnorml, 3, tol, ok );
   }




   /*
    ********************************************************************


    SATURN CASES


    ********************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Saturn normal vector test setup" );

   /*
   We'll do what we did for Mars, but we'll make a few minor changes.
   We'll change the set of surface points, and we'll change the
   surface ID.
   */

   /*
   The surface list consists of one surface ID (for the only Saturn
   segment).
   */
   target    = "Saturn";
   fixref    = "IAU_SATURN";
   nsurf     = 1;
   srflst[0] = 2;

   /*
   Get the target radii for later use. 
   */
   bodvrd_c ( target, "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );

   a = radii[0];
   b = radii[1];
   c = radii[2];

   /*
   Pick our longitude and latitude band counts so we don't 
   end up sampling from plate boundaries. This simplifies
   our checks on the outputs.
   */
   nslon = 29;
   nslat = 19;

   dlon  = twopi_c() / nslon;
   dlat  = pi_c()    / nslat;

   /*
   Pick a magnitude for the ray's vertex. 
   */
   r = 1.0e7;

   /*
   Pick an evaluation epoch. 
   */
   et = -10 * jyear_c();

   /*
   The prioritization flag is always "false" for the N0066 version
   of SPICE. 
   */
   pri = SPICEFALSE;

 

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Saturn normal vector test. DSK target. Surface list is "
             "non-empty."                                           );



   method = "DSK/UNPRIORITIZED/SURFACES = 2";

   /*
   Generate coodinates of surface points at which to find outward
   normal vectors.
   */


   /*
   Create an array of lon/lat coordinates. 
   */

   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      /*
      Choose sample longitudes so that plate boundaries are not hit. 
      */
      lon = 0.5 + (i * dlon);

      for ( j = 0;  j <= nslat;  j++ )
      {
         /*
         Choose sample latitudes so that the poles are not hit. 
         */
         lat          = halfpi_c() - j*dlat;

         if ( j == 0 )
         {
            lat -= dlat/2;
         }
         else if ( j == nslat )
         {
            lat += dlat/2;
         }

         lonlat[k][0] = lon;
         lonlat[k][1] = lat;

         ++k;
      }
   }

   npts = k;

   /*
   Find the normal vectors at the surface points corresponding to the
   surface coordinates.

   First, we must find the Cartesian coordinates of the surface points.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the normals using dskxsi_c. 
   */
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      for ( j = 0;  j <= nslat;  j++ )
      {
         lon = lonlat[k][0];
         lat = lonlat[k][1];

         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Saturn normal vector test for lon # (deg), lat #(deg).",
                   TITLEN                                                );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );
         chckxc_c ( SPICEFALSE, " ", ok );

         tcase_c ( title );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxsi_c ( pri,     target,  nsurf, srflst, et,    fixref,
                    vertex,  raydir,  MAXD,  MAXI,   xpt,   &handle,
                    &dladsc, &dskdsc, dc,    ic,     &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "srfnrm_c found", found, SPICETRUE, ok );

         /*
         Get the outward normal of the plate that was hit. The 
         plate ID is stored in the integer information component
         returned by dskxsi_c.
         */

         plid = ic[0];

         dskn02_c ( handle, &dladsc, plid, normal );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         /*
         Check the kth normal vector from dskxsi_c.
         */

         tol = TIGHT;

         chckad_c ( "kth normal", normls[k], "~~/", normal, 3, tol, ok );

         /*
         Onward. 
         */

         ++k;
      }
   }



   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Saturn normal vector test. DSK target. Surface list is "
             "empty."                                               );


   /*
   Change the method and epoch, then repeat the previous test. 
   */

   method = "DSK /Unprioritized";

   et     = -10 * jyear_c();


   /*
   We already have the array of lon/lat coordinates. 
   */
 

   /*
   Find the normal vectors at the surface points corresponding to the
   surface coordinates.

   First, we must find the Cartesian coordinates of the surface points.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the normals using dskxsi_c. 
   */
   k = 0;

   for ( i = 0;  i < nslon;  i++ )
   {
      for ( j = 0;  j <= nslat;  j++ )
      {
         lon = lonlat[k][0];
         lat = lonlat[k][1];

         latrec_c ( r, lon, lat, vertex );
         vminus_c ( vertex,      raydir );

         /* 
         ---- Case ---------------------------------------------------------
         */
         strncpy ( title, 
                   "Saturn normal vector test for lon # (deg), lat #(deg).",
                   TITLEN                                                );

         repmf_c ( title, "#", lon*dpr_c(), 6, 'F', TITLEN, title );
         repmf_c ( title, "#", lat*dpr_c(), 6, 'F', TITLEN, title );
         chckxc_c ( SPICEFALSE, " ", ok );

         tcase_c ( title );

         /*
         Find the ray-surface intercept for the current ray.
         */
         dskxsi_c ( pri,     target,  nsurf, srflst, et,    fixref,
                    vertex,  raydir,  MAXD,  MAXI,   xpt,   &handle,
                    &dladsc, &dskdsc, dc,    ic,     &found         );
         chckxc_c ( SPICEFALSE, " ", ok );

         /*
         We expect to find an intercept every time. 
         */
         chcksl_c ( "srfnrm_c found", found, SPICETRUE, ok );

         /*
         Get the outward normal of the plate that was hit. The 
         plate ID is stored in the integer information component
         returned by dskxsi_c.
         */

         plid = ic[0];

         dskn02_c ( handle, &dladsc, plid, normal );
         chckxc_c ( SPICEFALSE, " ", ok );
         
         /*
         Check the kth normal vector from dskxsi_c.
         */

         tol = TIGHT;

         chckad_c ( "kth normal", normls[k], "~~/", normal, 3, tol, ok );

         /*
         Onward. 
         */

         ++k;
      }
   }


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Saturn normal vector test. Ellipsoidal target." );


  
   method = "ellipsoid";

   /*
   Generate surface points on the reference ellipsoid. Use the existing
   lon/lat coordinate array.
   */
   latsrf_c ( method, target, et, fixref, npts, lonlat, srfpts );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Now get the normal vectors. 
   */
   srfnrm_c ( method, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the results. 
   */

   for ( k = 0;  k < npts;  k++ )
   {

      surfnm_c ( a, b, c, srfpts[k], xnorml );
      chckxc_c ( SPICEFALSE, " ", ok );

      strncpy ( label, "normal vector @", LABLEN );

      repmi_c ( label, "@", k, LABLEN, label );
      chckxc_c ( SPICEFALSE, " ", ok );
         
      tol = TIGHT;

      chckad_c ( label, normls[k], "~~/", xnorml, 3, tol, ok );
   }





   /*
   *********************************************************************
   *
   *
   *   srfnrm_c error cases
   *
   *
   *********************************************************************
   */

   /*
   We need not test all of the error cases, but we must test at least
   one; this will exercise the error handling logic in the wrapper.
   */
   
   srfnrm_c ( method, target, et, "XXX", npts, srfpts, normls );
   chckxc_c ( SPICETRUE, "SPICE(NOFRAME)", ok );

 
   /*
   For CSPICE, we must check handling of bad input strings.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfnrm_c error: empty input string." );


   /*
   Use the last values of all the inputs. 
   */
   srfnrm_c ( "", target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   srfnrm_c ( method, "", et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfnrm_c error: null input string." );


   /*
   Use the last values of all the inputs. 
   */
   srfnrm_c ( NULL, target, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   srfnrm_c ( method, NULL, et, fixref, npts, srfpts, normls );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   kclear_c();

   removeFile( dsk );

 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_srfnrm_c */

