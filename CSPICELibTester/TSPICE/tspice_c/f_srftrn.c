/* f_srftrn.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__10 = 10;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c_n1 = -1;

/* $Procedure      F_SRFTRN ( Test surface name/ID mapping routines ) */
/* Subroutine */ int f_srftrn__(logical *ok)
{
    /* Initialized data */

    static char bodlst[36*3] = "Mars                                " "Phobo"
	    "s                              " "Moon                          "
	    "      ";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3, i__4[2];
    icilist ici__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int zzsrfn2c_(char *, integer *, integer *, 
	    logical *, ftnlen), zzsrfc2n_(integer *, integer *, char *, 
	    logical *, ftnlen), zzsrfker_(char *, char *, integer *, integer *
	    , logical *, integer *, integer *, integer *, integer *, integer *
	    , integer *, integer *, ftnlen, ftnlen), zzctruin_(integer *);
    static integer i__, j, k, n;
    static char label[80];
    extern /* Subroutine */ int zzsrftrk_(integer *, logical *);
    static integer ncase;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer nkvar;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static char bdnam2[36*2003];
    extern /* Subroutine */ int t_success__(logical *), bods2c_(char *, 
	    integer *, logical *, ftnlen);
    static integer krbid2[2003];
    static char krnam2[36*2003];
    static integer krsid2[2003];
    extern /* Subroutine */ int srfs2c_(char *, char *, integer *, logical *, 
	    ftnlen, ftnlen), srfc2s_(integer *, integer *, char *, logical *, 
	    ftnlen), chcksc_(char *, char *, char *, char *, logical *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    static char bodnam[36*2000];
    static integer kerbid[2000];
    extern /* Subroutine */ int kclear_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen);
    static integer bodyid;
    static logical isname;
    static char kernam[36*2000];
    static integer kersid[2000];
    static logical update;
    static integer sididx[2003];
    extern /* Subroutine */ int srfscc_(char *, integer *, integer *, logical 
	    *, ftnlen);
    static integer sidhls[2003];
    static char nornam[36*2000];
    static integer sidpol[2009];
    static char bodstr[36];
    static integer surfid;
    extern /* Subroutine */ int pcpool_(char *, integer *, char *, ftnlen, 
	    ftnlen);
    static integer snmidx[2003];
    static logical extker;
    static integer snmhls[2003];
    extern /* Subroutine */ int pipool_(char *, integer *, integer *, ftnlen),
	     srfcss_(integer *, char *, char *, logical *, ftnlen, ftnlen);
    static integer snmpol[2009], usrctr[2];
    static char srfstr[36];
    extern /* Subroutine */ int intstr_(integer *, char *, ftnlen);
    static char expstr[36], numstr[11];

/* $ Abstract */

/*     Test the SPICELIB surface name/ID mapping routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare public surface name/ID mapping parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     NAIF_IDS */

/* $ Keywords */

/*     CONVERSION */
/*     NAME */
/*     STRING */
/*     SURFACE */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 02-DEC-2015 (NJB) */

/* -& */

/*     Maximum number of surface name/ID mapping entries: */


/*     Maximum length of a surface name string: */


/*     End of file srftrn.inc. */

/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. User software should not include this file */
/*     due to the volatile nature of this file. */

/*     Declare private surface name/ID mapping parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     NAIF_IDS */

/* $ Keywords */

/*     CONVERSION */
/*     NAME */
/*     STRING */
/*     SURFACE */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 04-FEB-2017 (NJB) */

/*        Original version 03-DEC-2015 (NJB) */

/* -& */

/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing, this size is set to the */
/*     first prime number greater than MXNSRF defined in the public */
/*     include file */

/*        srftrn.inc. */


/*     Singly-linked list pool lower bound: */


/*     End of file zzsrftrn.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the surface name/ID mapping routines */

/*        SRFC2S */
/*        SRFCSS */
/*        SRFS2C */
/*        SRFSCC */
/*        ZZSRFC2N */
/*        ZZSRFINI */
/*        ZZSRFKER */
/*        ZZSRFN2C */
/*        ZZSRFTRK */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 04-DEC-2015 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_SRFTRN", (ftnlen)8);

/* --- Case -------------------------------------------------------- */

    tcase_("Setup: create kernel variables", (ftnlen)30);

/*     Create a set of variables with multiple bodies for a given */
/*     surface name. */

    nkvar = 1998;
    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {
	for (j = 1; j <= 3; ++j) {
	    k = i__ - 1 + j;
	    kersid[(i__2 = k - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("kersid"
		    , i__2, "f_srftrn__", (ftnlen)227)] = k;
	    s_copy(bodnam + ((i__2 = k - 1) < 2000 && 0 <= i__2 ? i__2 : 
		    s_rnge("bodnam", i__2, "f_srftrn__", (ftnlen)229)) * 36, 
		    bodlst + ((i__3 = j - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		    "bodlst", i__3, "f_srftrn__", (ftnlen)229)) * 36, (ftnlen)
		    36, (ftnlen)36);
	    bods2c_(bodnam + ((i__2 = k - 1) < 2000 && 0 <= i__2 ? i__2 : 
		    s_rnge("bodnam", i__2, "f_srftrn__", (ftnlen)231)) * 36, &
		    kerbid[(i__3 = k - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge(
		    "kerbid", i__3, "f_srftrn__", (ftnlen)231)], &found, (
		    ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = 11;
	    ici__1.iciunit = numstr;
	    ici__1.icifmt = "(I4.4)";
	    s_wsfi(&ici__1);
	    do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	    e_wsfi();
	    s_copy(kernam + ((i__2 = k - 1) < 2000 && 0 <= i__2 ? i__2 : 
		    s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)238)) * 36, 
		    "Surface # name", (ftnlen)36, (ftnlen)14);
	    repmc_(kernam + ((i__2 = k - 1) < 2000 && 0 <= i__2 ? i__2 : 
		    s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)240)) * 36, 
		    "#", numstr, kernam + ((i__3 = k - 1) < 2000 && 0 <= i__3 
		    ? i__3 : s_rnge("kernam", i__3, "f_srftrn__", (ftnlen)240)
		    ) * 36, (ftnlen)36, (ftnlen)1, (ftnlen)11, (ftnlen)36);
	}
    }

/*     >>> We'll avoid overwriting */

/*            KERNAM */
/*            KERBID */
/*            KERSID */
/*            BODNAM */

/*         in the rest of this routine. We'll use the variables */

/*            KRNAM2 */
/*            KRBID2 */
/*            KRSID2 */
/*            BDNAM2 */

/*         for temporary kernel variable values. */

/* *********************************************************************** */

/*     ZZSRFTRK tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFTRK: check update status before any other ZZSRFTRN entry po"
	    "int has been initialized.", (ftnlen)89);
    zzctruin_(usrctr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrftrk_(usrctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE 0", &update, &c_true, ok, (ftnlen)8);

/*     Make a second check. */

    zzsrftrk_(usrctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE 1", &update, &c_false, ok, (ftnlen)8);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFTRK: check update status after a map update.", (ftnlen)49);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrftrk_(usrctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE 0", &update, &c_true, ok, (ftnlen)8);

/*     Make a second check. */

    zzsrftrk_(usrctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE 1", &update, &c_false, ok, (ftnlen)8);
/* *********************************************************************** */

/*     ZZSRFKER tests */

/* *********************************************************************** */

/*     Most of the functionality of ZZSRFKER will be exercised by */
/*     tests of higher-level routines. We'll test the error handling */
/*     here. */


/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFKER: check EXTKER when map is not present", (ftnlen)46);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(kernam, nornam, kersid, kerbid, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("EXTKER", &extker, &c_false, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFKER: array size mismatch", (ftnlen)29);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &c__10, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(kernam, nornam, kersid, kerbid, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(ARRAYSIZEMISMATCH)", ok, (ftnlen)24);
    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__10, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(kernam, nornam, kersid, kerbid, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(ARRAYSIZEMISMATCH)", ok, (ftnlen)24);
    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__10, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(kernam, nornam, kersid, kerbid, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(ARRAYSIZEMISMATCH)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFKER: bad kernel variable data type", (ftnlen)39);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_BODY", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pcpool_("NAIF_SURFACE_CODE", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_NAME", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(kernam, nornam, kersid, kerbid, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BADVARIABLETYPE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFKER: arrays too large", (ftnlen)26);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(krnam2 + ((i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"krnam2", i__2, "f_srftrn__", (ftnlen)457)) * 36, kernam + ((
		i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kernam", 
		i__3, "f_srftrn__", (ftnlen)457)) * 36, (ftnlen)36, (ftnlen)
		36);
	krbid2[(i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krbid2", 
		i__2, "f_srftrn__", (ftnlen)458)] = kerbid[(i__3 = i__ - 1) < 
		2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, "f_srftrn__"
		, (ftnlen)458)];
	krsid2[(i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krsid2", 
		i__2, "f_srftrn__", (ftnlen)459)] = kersid[(i__3 = i__ - 1) < 
		2000 && 0 <= i__3 ? i__3 : s_rnge("kersid", i__3, "f_srftrn__"
		, (ftnlen)459)];
    }
    s_copy(krnam2 + ((i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krn"
	    "am2", i__1, "f_srftrn__", (ftnlen)463)) * 36, "EXTRA 1", (ftnlen)
	    36, (ftnlen)7);
    s_copy(krnam2 + ((i__1 = nkvar + 1) < 2003 && 0 <= i__1 ? i__1 : s_rnge(
	    "krnam2", i__1, "f_srftrn__", (ftnlen)464)) * 36, "EXTRA 2", (
	    ftnlen)36, (ftnlen)7);
    s_copy(krnam2 + ((i__1 = nkvar + 2) < 2003 && 0 <= i__1 ? i__1 : s_rnge(
	    "krnam2", i__1, "f_srftrn__", (ftnlen)465)) * 36, "EXTRA 3", (
	    ftnlen)36, (ftnlen)7);
    krsid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krsid2", i__1, 
	    "f_srftrn__", (ftnlen)467)] = 1;
    krsid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krsid2", i__1, 
	    "f_srftrn__", (ftnlen)468)] = 2;
    krsid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krsid2", i__1, 
	    "f_srftrn__", (ftnlen)469)] = 3;
    krbid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krbid2", i__1, 
	    "f_srftrn__", (ftnlen)471)] = -1;
    krbid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krbid2", i__1, 
	    "f_srftrn__", (ftnlen)472)] = -2;
    krbid2[(i__1 = nkvar) < 2003 && 0 <= i__1 ? i__1 : s_rnge("krbid2", i__1, 
	    "f_srftrn__", (ftnlen)473)] = -3;

/*     Install variables in the kernel pool. */

    i__1 = nkvar + 3;
    pcpool_("NAIF_SURFACE_NAME", &i__1, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nkvar + 3;
    pipool_("NAIF_SURFACE_BODY", &i__1, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nkvar + 3;
    pipool_("NAIF_SURFACE_CODE", &i__1, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(krnam2, nornam, krsid2, krbid2, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(TOOMANYSURFACES)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZSRFKER: blank surface name", (ftnlen)28);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Install variables in the kernel pool. */

    s_copy(krnam2 + 35964, " ", (ftnlen)36, (ftnlen)1);
    pcpool_("NAIF_SURFACE_NAME", &nkvar, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsrfker_(krnam2, nornam, krsid2, krbid2, &extker, &n, snmhls, snmpol, 
	    snmidx, sidhls, sidpol, sididx, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BLANKNAMEASSIGNED)", ok, (ftnlen)24);
/* *********************************************************************** */

/*     SRFS2C tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: map names to codes", (ftnlen)26);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfs2c_(kernam + ((i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : 
		s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)556)) * 36, 
		bodnam + ((i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : 
		s_rnge("bodnam", i__3, "f_srftrn__", (ftnlen)556)) * 36, &
		surfid, &found, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &kersid[(i__2 = i__ - 1) < 2000 && 0 
		    <= i__2 ? i__2 : s_rnge("kersid", i__2, "f_srftrn__", (
		    ftnlen)571)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: map integer surface and body strings to codes", (ftnlen)
	    53);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	intstr_(&kersid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kersid", i__2, "f_srftrn__", (ftnlen)589)], srfstr, (ftnlen)
		36);
	intstr_(&kerbid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kerbid", i__2, "f_srftrn__", (ftnlen)590)], bodstr, (ftnlen)
		36);
	srfs2c_(srfstr, bodstr, &surfid, &found, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &kersid[(i__2 = i__ - 1) < 2000 && 0 
		    <= i__2 ? i__2 : s_rnge("kersid", i__2, "f_srftrn__", (
		    ftnlen)607)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: surface name not valid; body name valid", (ftnlen)47);
    s_copy(srfstr, "ZZZ", (ftnlen)36, (ftnlen)3);
    intstr_(kerbid, bodstr, (ftnlen)36);
    srfs2c_(srfstr, bodstr, &surfid, &found, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &found, &c_false, ok, (ftnlen)80);

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: surface name valid; body name not valid", (ftnlen)47);
    s_copy(bodstr, "ZZZ", (ftnlen)36, (ftnlen)3);
    srfs2c_(kernam, bodstr, &surfid, &found, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &found, &c_false, ok, (ftnlen)80);

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: clear pool, make sure translations are not performed.", (
	    ftnlen)61);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfs2c_(kernam + ((i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : 
		s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)654)) * 36, 
		bodnam + ((i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : 
		s_rnge("bodnam", i__3, "f_srftrn__", (ftnlen)654)) * 36, &
		surfid, &found, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_false, ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFS2C: map names to codes (change variables)", (ftnlen)45);

/*     Create variables having reversed order of elements; also */
/*     negate surface IDs. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = nkvar + 1 - i__;
/* Writing concatenation */
	i__4[0] = 34, a__1[0] = kernam + ((i__3 = i__ - 1) < 2000 && 0 <= 
		i__3 ? i__3 : s_rnge("kernam", i__3, "f_srftrn__", (ftnlen)
		682)) * 36;
	i__4[1] = 2, a__1[1] = "ZZ";
	s_cat(krnam2 + ((i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"krnam2", i__2, "f_srftrn__", (ftnlen)682)) * 36, a__1, i__4, 
		&c__2, (ftnlen)36);
	krbid2[(i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krbid2", 
		i__2, "f_srftrn__", (ftnlen)683)] = kerbid[(i__3 = i__ - 1) < 
		2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, "f_srftrn__"
		, (ftnlen)683)];
	s_copy(bdnam2 + ((i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"bdnam2", i__2, "f_srftrn__", (ftnlen)684)) * 36, bodnam + ((
		i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("bodnam", 
		i__3, "f_srftrn__", (ftnlen)684)) * 36, (ftnlen)36, (ftnlen)
		36);
	krsid2[(i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krsid2", 
		i__2, "f_srftrn__", (ftnlen)685)] = -kersid[(i__3 = i__ - 1) <
		 2000 && 0 <= i__3 ? i__3 : s_rnge("kersid", i__3, "f_srftrn"
		"__", (ftnlen)685)];
    }

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfs2c_(krnam2 + ((i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : 
		s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)707)) * 36, 
		bdnam2 + ((i__3 = i__ - 1) < 2003 && 0 <= i__3 ? i__3 : 
		s_rnge("bdnam2", i__3, "f_srftrn__", (ftnlen)707)) * 36, &
		surfid, &found, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &krsid2[(i__2 = i__ - 1) < 2003 && 0 
		    <= i__2 ? i__2 : s_rnge("krsid2", i__2, "f_srftrn__", (
		    ftnlen)722)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }
/* *********************************************************************** */

/*     SRFSCC tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("SRFSCC: map names to codes", (ftnlen)26);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfscc_(kernam + ((i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : 
		s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)761)) * 36, &
		kerbid[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge(
		"kerbid", i__3, "f_srftrn__", (ftnlen)761)], &surfid, &found, 
		(ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &kersid[(i__2 = i__ - 1) < 2000 && 0 
		    <= i__2 ? i__2 : s_rnge("kersid", i__2, "f_srftrn__", (
		    ftnlen)776)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFSCC: map integer surface strings to codes", (ftnlen)44);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	intstr_(&kersid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kersid", i__2, "f_srftrn__", (ftnlen)795)], srfstr, (ftnlen)
		36);
	srfscc_(srfstr, &kerbid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : 
		s_rnge("kerbid", i__2, "f_srftrn__", (ftnlen)797)], &surfid, &
		found, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &kersid[(i__2 = i__ - 1) < 2000 && 0 
		    <= i__2 ? i__2 : s_rnge("kersid", i__2, "f_srftrn__", (
		    ftnlen)812)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFSCC: surface name not valid; body code valid", (ftnlen)47);
    s_copy(srfstr, "ZZZ", (ftnlen)36, (ftnlen)3);
    srfscc_(srfstr, kerbid, &surfid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &found, &c_false, ok, (ftnlen)80);

/* --- Case -------------------------------------------------------- */

    tcase_("SRFSCC: surface name valid; body code not valid", (ftnlen)47);
    srfscc_(kernam, &c_n1, &surfid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &found, &c_false, ok, (ftnlen)80);

/* --- Case -------------------------------------------------------- */

    tcase_("SRFSCC: clear pool, make sure translations are not performed.", (
	    ftnlen)61);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfscc_(kernam + ((i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : 
		s_rnge("kernam", i__2, "f_srftrn__", (ftnlen)856)) * 36, &
		kerbid[(i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge(
		"kerbid", i__3, "f_srftrn__", (ftnlen)856)], &surfid, &found, 
		(ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_false, ok, (ftnlen)80);
    }

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */

/*     SRFC2S tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("SRFC2S: map codes to names", (ftnlen)26);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfc2s_(&kersid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kersid", i__2, "f_srftrn__", (ftnlen)897)], &kerbid[(i__3 = 
		i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, 
		"f_srftrn__", (ftnlen)897)], srfstr, &isname, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "ISNAME #", (ftnlen)80, (ftnlen)8);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &isname, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksc_(label, srfstr, "=", kernam + ((i__2 = i__ - 1) < 2000 && 
		    0 <= i__2 ? i__2 : s_rnge("kernam", i__2, "f_srftrn__", (
		    ftnlen)912)) * 36, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1, 
		    (ftnlen)36);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFCSS: surface code valid; body code not valid", (ftnlen)47);
    bodyid = -3;
    srfc2s_(kersid, &bodyid, srfstr, &isname, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &isname, &c_false, ok, (ftnlen)80);
    intstr_(kersid, expstr, (ftnlen)36);
    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_(label, srfstr, "=", expstr, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1,
	     (ftnlen)36);

/* --- Case -------------------------------------------------------- */

    tcase_("SRFC2S: map names to codes (change variables)", (ftnlen)45);

/*     Create variables having reversed order of elements; also */
/*     negate surface IDs. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = nkvar + 1 - i__;
/* Writing concatenation */
	i__4[0] = 34, a__1[0] = kernam + ((i__3 = i__ - 1) < 2000 && 0 <= 
		i__3 ? i__3 : s_rnge("kernam", i__3, "f_srftrn__", (ftnlen)
		954)) * 36;
	i__4[1] = 2, a__1[1] = "ZZ";
	s_cat(krnam2 + ((i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"krnam2", i__2, "f_srftrn__", (ftnlen)954)) * 36, a__1, i__4, 
		&c__2, (ftnlen)36);
	krbid2[(i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krbid2", 
		i__2, "f_srftrn__", (ftnlen)955)] = kerbid[(i__3 = i__ - 1) < 
		2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, "f_srftrn__"
		, (ftnlen)955)];
	s_copy(bdnam2 + ((i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"bdnam2", i__2, "f_srftrn__", (ftnlen)956)) * 36, bodnam + ((
		i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("bodnam", 
		i__3, "f_srftrn__", (ftnlen)956)) * 36, (ftnlen)36, (ftnlen)
		36);
	krsid2[(i__2 = j - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krsid2", 
		i__2, "f_srftrn__", (ftnlen)957)] = -kersid[(i__3 = i__ - 1) <
		 2000 && 0 <= i__3 ? i__3 : s_rnge("kersid", i__3, "f_srftrn"
		"__", (ftnlen)957)];
    }

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfc2s_(&krsid2[(i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge(
		"krsid2", i__2, "f_srftrn__", (ftnlen)976)], &krbid2[(i__3 = 
		i__ - 1) < 2003 && 0 <= i__3 ? i__3 : s_rnge("krbid2", i__3, 
		"f_srftrn__", (ftnlen)976)], srfstr, &isname, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "ISNAME #", (ftnlen)80, (ftnlen)8);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &isname, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksc_(label, srfstr, "=", krnam2 + ((i__2 = i__ - 1) < 2003 && 
		    0 <= i__2 ? i__2 : s_rnge("krnam2", i__2, "f_srftrn__", (
		    ftnlen)991)) * 36, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1, 
		    (ftnlen)36);
	}
    }
/* *********************************************************************** */

/*     SRFCSS tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("SRFCSS: map codes to names", (ftnlen)26);

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, kernam, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, kerbid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, kersid, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	srfcss_(&kersid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kersid", i__2, "f_srftrn__", (ftnlen)1028)], bodnam + ((i__3 
		= i__ - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("bodnam", i__3,
		 "f_srftrn__", (ftnlen)1028)) * 36, srfstr, &isname, (ftnlen)
		36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "ISNAME #", (ftnlen)80, (ftnlen)8);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &isname, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksc_(label, srfstr, "=", kernam + ((i__2 = i__ - 1) < 2000 && 
		    0 <= i__2 ? i__2 : s_rnge("kernam", i__2, "f_srftrn__", (
		    ftnlen)1043)) * 36, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1,
		     (ftnlen)36);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFCSS: map codes to names using body strings", (ftnlen)45);
    ncase = nkvar;
    i__1 = ncase;
    for (i__ = 1; i__ <= i__1; ++i__) {
	intstr_(&kerbid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kerbid", i__2, "f_srftrn__", (ftnlen)1060)], bodstr, (ftnlen)
		36);
	srfcss_(&kersid[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge(
		"kersid", i__2, "f_srftrn__", (ftnlen)1062)], bodstr, srfstr, 
		&isname, (ftnlen)36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "ISNAME #", (ftnlen)80, (ftnlen)8);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &isname, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksc_(label, srfstr, "=", kernam + ((i__2 = i__ - 1) < 2000 && 
		    0 <= i__2 ? i__2 : s_rnge("kernam", i__2, "f_srftrn__", (
		    ftnlen)1077)) * 36, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1,
		     (ftnlen)36);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("SRFCSS: surface code valid; body string not valid", (ftnlen)49);
    s_copy(bodstr, "ZZZ", (ftnlen)36, (ftnlen)3);
    srfcss_(kersid, bodstr, srfstr, &isname, (ftnlen)36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_(label, &isname, &c_false, ok, (ftnlen)80);
    intstr_(kersid, expstr, (ftnlen)36);
    s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_(label, srfstr, "=", expstr, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1,
	     (ftnlen)36);
/* *********************************************************************** */

/*     ZZSRFN2C, ZZSRFC2N tests */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Check mapping of alias surface names to codes", (ftnlen)45);

/*     Create a mapping such that, for each surface ID and body, */
/*     there are three names. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {
	for (j = 1; j <= 3; ++j) {
	    k = i__ - 1 + j;
	    s_copy(krnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1130)) * 36, 
		    kernam + ((i__3 = k - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("kernam", i__3, "f_srftrn__", (ftnlen)1130)) * 36, 
		    (ftnlen)36, (ftnlen)36);
	    repmc_(krnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1132)) * 36, 
		    "name", "name #", krnam2 + ((i__3 = k - 1) < 2003 && 0 <= 
		    i__3 ? i__3 : s_rnge("krnam2", i__3, "f_srftrn__", (
		    ftnlen)1132)) * 36, (ftnlen)36, (ftnlen)4, (ftnlen)6, (
		    ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(krnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1134)) * 36, 
		    "#", &j, krnam2 + ((i__3 = k - 1) < 2003 && 0 <= i__3 ? 
		    i__3 : s_rnge("krnam2", i__3, "f_srftrn__", (ftnlen)1134))
		     * 36, (ftnlen)36, (ftnlen)1, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    krbid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krbid2"
		    , i__2, "f_srftrn__", (ftnlen)1137)] = kerbid[(i__3 = i__ 
		    - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, 
		    "f_srftrn__", (ftnlen)1137)];
	    krsid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krsid2"
		    , i__2, "f_srftrn__", (ftnlen)1138)] = kersid[(i__3 = i__ 
		    - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kersid", i__3, 
		    "f_srftrn__", (ftnlen)1138)];
	    s_copy(bdnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("bdnam2", i__2, "f_srftrn__", (ftnlen)1139)) * 36, 
		    bodnam + ((i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("bodnam", i__3, "f_srftrn__", (ftnlen)1139)) * 36, 
		    (ftnlen)36, (ftnlen)36);
	}
    }

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; ++i__) {
	zzsrfn2c_(krnam2 + ((i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : 
		s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1160)) * 36, &
		krbid2[(i__3 = i__ - 1) < 2003 && 0 <= i__3 ? i__3 : s_rnge(
		"krbid2", i__3, "f_srftrn__", (ftnlen)1160)], &surfid, &found,
		 (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &surfid, "=", &krsid2[(i__2 = i__ - 1) < 2003 && 0 
		    <= i__2 ? i__2 : s_rnge("krsid2", i__2, "f_srftrn__", (
		    ftnlen)1175)], &c__0, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check mapping of codes to aliased surface names", (ftnlen)47);

/*     Use the mapping from the previous test case. */

/*     Make sure the highest-priority name is selected. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {
	zzsrfc2n_(&krsid2[(i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : 
		s_rnge("krsid2", i__2, "f_srftrn__", (ftnlen)1195)], &krbid2[(
		i__3 = i__ - 1) < 2003 && 0 <= i__3 ? i__3 : s_rnge("krbid2", 
		i__3, "f_srftrn__", (ftnlen)1195)], srfstr, &found, (ftnlen)
		36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	if (found) {
	    s_copy(label, "Surf string #", (ftnlen)80, (ftnlen)13);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note the index used in KRNAM2... */

	    chcksc_(label, srfstr, "=", krnam2 + ((i__2 = i__ + 1) < 2003 && 
		    0 <= i__2 ? i__2 : s_rnge("krnam2", i__2, "f_srftrn__", (
		    ftnlen)1213)) * 36, ok, (ftnlen)80, (ftnlen)36, (ftnlen)1,
		     (ftnlen)36);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check masking of surface names associated with multiple codes wh"
	    "en mapping names to codes", (ftnlen)89);

/*     Create a mapping such that, for each surface name and body, */
/*     there are three surface IDs. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {
	for (j = 1; j <= 3; ++j) {
	    k = i__ - 1 + j;
	    s_copy(krnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1237)) * 36, 
		    kernam + ((i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("kernam", i__3, "f_srftrn__", (ftnlen)1237)) * 36, 
		    (ftnlen)36, (ftnlen)36);
	    krbid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krbid2"
		    , i__2, "f_srftrn__", (ftnlen)1238)] = kerbid[(i__3 = i__ 
		    - 1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kerbid", i__3, 
		    "f_srftrn__", (ftnlen)1238)];
	    krsid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : s_rnge("krsid2"
		    , i__2, "f_srftrn__", (ftnlen)1239)] = kersid[(i__3 = k - 
		    1) < 2000 && 0 <= i__3 ? i__3 : s_rnge("kersid", i__3, 
		    "f_srftrn__", (ftnlen)1239)];
	    s_copy(bdnam2 + ((i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("bdnam2", i__2, "f_srftrn__", (ftnlen)1240)) * 36, 
		    bodnam + ((i__3 = i__ - 1) < 2000 && 0 <= i__3 ? i__3 : 
		    s_rnge("bodnam", i__3, "f_srftrn__", (ftnlen)1240)) * 36, 
		    (ftnlen)36, (ftnlen)36);
	}
    }

/*     Install variables in the kernel pool. */

    pcpool_("NAIF_SURFACE_NAME", &nkvar, krnam2, (ftnlen)17, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &nkvar, krbid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &nkvar, krsid2, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {

/*        The mapping in this direction need be performed for */
/*        only one value of J. */

	for (j = 1; j <= 1; ++j) {
	    k = i__ - 1 + j;
	    zzsrfn2c_(krnam2 + ((i__2 = i__ - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krnam2", i__2, "f_srftrn__", (ftnlen)1267)) * 36, 
		    &krbid2[(i__3 = i__ - 1) < 2003 && 0 <= i__3 ? i__3 : 
		    s_rnge("krbid2", i__3, "f_srftrn__", (ftnlen)1267)], &
		    surfid, &found, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	    if (found) {
		s_copy(label, "Surf ID #", (ftnlen)80, (ftnlen)9);
		repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Note index of KRSID2. */

		chcksi_(label, &surfid, "=", &krsid2[(i__2 = i__ + 1) < 2003 
			&& 0 <= i__2 ? i__2 : s_rnge("krsid2", i__2, "f_srft"
			"rn__", (ftnlen)1284)], &c__0, ok, (ftnlen)80, (ftnlen)
			1);
	    }
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check masking of surface names associated with multiple codes wh"
	    "en mapping codes to names", (ftnlen)89);

/*     Use the mapping defined in the previous test case. */

    i__1 = nkvar;
    for (i__ = 1; i__ <= i__1; i__ += 3) {

/*        The mapping in this direction can be performed for */
/*        only J = 3. */

	for (j = 1; j <= 2; ++j) {
	    k = i__ - 1 + j;
	    zzsrfc2n_(&krsid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krsid2", i__2, "f_srftrn__", (ftnlen)1311)], &
		    krbid2[(i__3 = k - 1) < 2003 && 0 <= i__3 ? i__3 : s_rnge(
		    "krbid2", i__3, "f_srftrn__", (ftnlen)1311)], srfstr, &
		    found, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(label, &found, &c_false, ok, (ftnlen)80);
	}
	for (j = 3; j <= 3; ++j) {
	    k = i__ - 1 + j;
	    zzsrfc2n_(&krsid2[(i__2 = k - 1) < 2003 && 0 <= i__2 ? i__2 : 
		    s_rnge("krsid2", i__2, "f_srftrn__", (ftnlen)1327)], &
		    krbid2[(i__3 = k - 1) < 2003 && 0 <= i__3 ? i__3 : s_rnge(
		    "krbid2", i__3, "f_srftrn__", (ftnlen)1327)], srfstr, &
		    found, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND #", (ftnlen)80, (ftnlen)7);
	    repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(label, &found, &c_true, ok, (ftnlen)80);
	    if (found) {
		s_copy(label, "Surf name #", (ftnlen)80, (ftnlen)11);
		repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_(label, srfstr, "=", krnam2 + ((i__2 = k - 1) < 2003 &&
			 0 <= i__2 ? i__2 : s_rnge("krnam2", i__2, "f_srftrn"
			"__", (ftnlen)1342)) * 36, ok, (ftnlen)80, (ftnlen)36, 
			(ftnlen)1, (ftnlen)36);
	    }
	}
    }
    t_success__(ok);
    return 0;
} /* f_srftrn__ */

