/*

-Procedure f_srftrn_c ( Test wrappers for surface name/ID functions )

 
-Abstract
 
   Perform tests on CSPICE wrappers for surface name/ID translation
   functions. 
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_srftrn_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the surface name-code routines.  The
   covered set of routines is:
   
      srfs2c_c
      srfscc_c
      srfc2s_c
      srfcss_c

                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Previous version 22-JAN-2016 (NJB)

-Index_Entries

   test surface translation routines

-&
*/

{ /* Begin f_srftrn_c */

 
   /*
   Constants
   */
   #define KVNNAM          "NAIF_SURFACE_NAME"
   #define KVBNAM          "NAIF_SURFACE_BODY"
   #define KVCNAM          "NAIF_SURFACE_CODE"

   #define BDNMLN          36
   #define NBDNAM          3
   #define MXNSRF          2000
   #define NUMLEN          12
   #define SFNMLN          SPICE_SRF_SFNMLN
   #define LNSIZE          81

   /*
   Local variables
   */
   SpiceBoolean            found;
   SpiceBoolean            isname;
   
   static SpiceChar      * bodlst[ NBDNAM ] = { "Mars",
                                                "Phobos",
                                                "Moon" };

   static SpiceChar        bodnam[ MXNSRF ][ BDNMLN ];
   static SpiceChar        kernam[ MXNSRF ][ SFNMLN ];
   static SpiceChar        label [ LNSIZE ];
   SpiceChar               numstr[ NUMLEN ];
   SpiceChar               srfnam[ SFNMLN ];


   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   static SpiceInt         kerbid[ MXNSRF ];
   static SpiceInt         kersid[ MXNSRF ];
   SpiceInt                ncase;
   SpiceInt                nkvar;
   SpiceInt                surfid;

   


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_srftrn_c" );
   

   
   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "Setup: create kernel variables." );

   /*
   Create a set of variables with multiple bodies for a given
   surface name.
   */
   nkvar = 1998;

   for ( i = 0;  i < nkvar;  i += 3 )
   {
      for ( j = 0;  j < 3;  j++ )
      {
         k = i + j;

         kersid[k] = k + 1;

         strncpy ( bodnam[k], bodlst[j], BDNMLN );

         bods2c_c ( bodnam[k], kerbid+k, &found );
         chckxc_c ( SPICEFALSE, " ", ok );
            
         chcksl_c ( "found", found, SPICETRUE, ok );

         sprintf ( numstr, "%04d", (int)(i+1) );

         strncpy ( kernam[k], "Surface # name", SFNMLN );

         repmc_c ( kernam[k], "#", numstr, SFNMLN, kernam[k] );

         /*
         printf ( "%s, surface ID = %d, body ID = %d \n", 
                  kernam[k], (int)kersid[k], (int)kerbid[k] );
         */

      }
   }


/*
**************************************************************

  SRFS2C tests
   
**************************************************************
*/

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "srfs2c_c: map surface names to codes." );
   
   kclear_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Install variables in the kernel pool.
   */
   pcpool_c ( KVNNAM, nkvar, SFNMLN, kernam );
   chckxc_c ( SPICEFALSE, " ", ok );

   pipool_c ( KVBNAM, nkvar, kerbid );
   chckxc_c ( SPICEFALSE, " ", ok );

   pipool_c ( KVCNAM, nkvar, kersid );
   chckxc_c ( SPICEFALSE, " ", ok );


   ncase = nkvar;

   for ( i = 0;  i < ncase;  i++ )
   {
      srfs2c_c ( kernam[i], bodnam[i], &surfid, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      sprintf  ( label, "found %d", (int)i );

      chcksl_c ( label, found, SPICETRUE, ok );
      
      sprintf  ( label, "surfid %d", (int)i );

      chcksi_c ( label, surfid, "=", kersid[i], 0, ok );
      /*
      printf ( "%d\n", (int)kersid[i] );
      */
   }

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfs2c_c: no ID associated with surface name" );

   srfs2c_c ( "zzz", "Mars", &surfid, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICEFALSE, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfs2c_c: empty surface name" );

   srfs2c_c ( "", "Mars", &surfid, &found );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfs2c_c: null surface name" );

   srfs2c_c ( NULL, "Mars", &surfid, &found );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfs2c_c: empty body name" );

   srfs2c_c ( kernam[0], "", &surfid, &found );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfs2c_c: null body name" );

   srfs2c_c ( kernam[0], NULL, &surfid, &found );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );




/*
**************************************************************

  SRFSCC tests
   
**************************************************************
*/

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "srfscc_c: map surface names to codes." );
 
   /*
   Use kernel pool contents from previous tests. 
   */

   ncase = nkvar;

   for ( i = 0;  i < ncase;  i++ )
   {
      srfscc_c ( kernam[i], kerbid[i], &surfid, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      sprintf  ( label, "found %d", (int)i );

      chcksl_c ( label, found, SPICETRUE, ok );
      
      sprintf  ( label, "surfid %d", (int)i );

      chcksi_c ( label, surfid, "=", kersid[i], 0, ok );
   }

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfscc_c: no ID associated with surface name" );

   srfscc_c ( "zzz", 499, &surfid, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICEFALSE, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfscc_c: no ID associated with surface name" );

   srfscc_c ( "zzz", 499, &surfid, &found );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "found", found, SPICEFALSE, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfscc_c: empty surface name" );

   srfscc_c ( "", 499, &surfid, &found );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );



/*
**************************************************************

  SRFC2S tests
   
**************************************************************
*/

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "srfc2s_c: map surface codes to names." );
 
   /*
   Use kernel pool contents from previous tests. 
   */

   ncase = nkvar;

   for ( i = 0;  i < ncase;  i++ )
   {
      srfc2s_c ( kersid[i], kerbid[i], SFNMLN, srfnam, &isname );
      chckxc_c ( SPICEFALSE, " ", ok );

      sprintf  ( label, "isname %d", (int)i );

      chcksl_c ( label, isname, SPICETRUE, ok );
      
      sprintf  ( label, "srfnam %d", (int)i );

      chcksc_c ( label, srfnam, "=", kernam[i], ok );
   }

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfc2s_c: no name associated with surface ID" );

   srfc2s_c ( -1, 499, SFNMLN, srfnam, &isname );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "isname", isname, SPICEFALSE, ok );

   chcksc_c ( "srfnam", srfnam, "=", "-1", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfc2s_c: no mapping for input body ID" );

   srfc2s_c ( -2, -1, SFNMLN, srfnam, &isname );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "isname", isname, SPICEFALSE, ok );

   chcksc_c ( "srfnam", srfnam, "=", "-2", ok );


/*
**************************************************************

  SRFCSS tests
   
**************************************************************
*/

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "srfcss_c: map surface codes to names." );
 
   /*
   Use kernel pool contents from previous tests. 
   */

   ncase = nkvar;

   for ( i = 0;  i < ncase;  i++ )
   {
      srfcss_c ( kersid[i], bodnam[i], SFNMLN, srfnam, &isname );
      chckxc_c ( SPICEFALSE, " ", ok );

      sprintf  ( label, "isname %d", (int)i );

      chcksl_c ( label, isname, SPICETRUE, ok );
      
      sprintf  ( label, "srfnam %d", (int)i );

      chcksc_c ( label, srfnam, "=", kernam[i], ok );
   }

   /*
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfcss_c: no name associated with surface ID" );

   srfcss_c ( -1, "Mars", SFNMLN, srfnam, &isname );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "isname", isname, SPICEFALSE, ok );

   chcksc_c ( "srfnam", srfnam, "=", "-1", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "srfc2s_c: no mapping for input body name" );

   srfcss_c ( -2, "ZZZ", SFNMLN, srfnam, &isname );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksl_c ( "isname", isname, SPICEFALSE, ok );

   chcksc_c ( "srfnam", srfnam, "=", "-2", ok );




   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_srftrn_c */

