/* f_stmp03.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__14 = 14;
static logical c_false = FALSE_;
static doublereal c_b15 = 1e-12;

/* $Procedure F_STMP03 ( STMP03 tests ) */
/* Subroutine */ int f_stmp03__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double log(doublereal);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double sqrt(doublereal), cosh(doublereal), sinh(doublereal), cos(
	    doublereal), sin(doublereal);

    /* Local variables */
    integer i__;
    doublereal x, z__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmd_(char *, char *,
	     doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmax_(void);
    char title[240];
    doublereal c0, c1, c2, c3;
    extern /* Subroutine */ int topen_(char *, ftnlen), stmp03_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), 
	    t_success__(logical *), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    doublereal lbound, ubound, xc0, xc1, xc2, xc3;

/* $ Abstract */

/*     Exercise the SPICELIB Stumpff function routine STMP03. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine */

/*        STMP03 */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-APR-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Open the test family. */

    topen_("F_STMP03", (ftnlen)8);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Argument too large.", (ftnlen)19);

/*     LBOUND is an approximate lower bound on the input */
/*     values for which the Stumpff functions can be computed. */

/* Computing 2nd power */
    d__1 = log(2.) + log(dpmax_());
    lbound = -(d__1 * d__1);
    x = lbound * 2;
    stmp03_(&x, &c0, &c1, &c2, &c3);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/*     Test STMP03 for values of X less than -1. */

    x = -1.0000000000000011;
    while(x > lbound) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "X = #", (ftnlen)240, (ftnlen)5);
	repmd_(title, "#", &x, &c__14, title, (ftnlen)240, (ftnlen)1, (ftnlen)
		240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	stmp03_(&x, &c0, &c1, &c2, &c3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Generate expected results. */

	z__ = sqrt(-x);
	xc0 = cosh(z__);
	xc1 = sinh(z__) / z__;
	xc2 = (1. - xc0) / x;
	xc3 = (1. - xc1) / x;
	chcksd_("C0", &c0, "~/", &xc0, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C1", &c1, "~/", &xc1, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	x *= 1.5;
    }

/*     Test STMP03 for values of X between -1 and 0. */

    x = -1.;
    while(x < -1e-15) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "X = #", (ftnlen)240, (ftnlen)5);
	repmd_(title, "#", &x, &c__14, title, (ftnlen)240, (ftnlen)1, (ftnlen)
		240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	stmp03_(&x, &c0, &c1, &c2, &c3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Generate expected results. */

	z__ = sqrt(-x);
	xc0 = cosh(z__);
	xc1 = sinh(z__) / z__;
	chcksd_("C0", &c0, "~/", &xc0, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C1", &c1, "~/", &xc1, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	if (x < -.01) {
	    xc2 = (1. - xc0) / x;
	    xc3 = (1. - xc1) / x;
	    chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	    chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	} else {

/*           Compute the C3 and C2 series directly. Group terms to avoid */
/*           loss of precision. */

	    xc3 = 1. - x / 342;
	    for (i__ = 1; i__ <= 7; ++i__) {
		xc3 = 1. - x / ((18 - (i__ << 1)) * (19 - (i__ << 1))) * xc3;
	    }
	    xc3 /= 6;
	    chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	    xc2 = 1. - x / 306;
	    for (i__ = 1; i__ <= 7; ++i__) {
		xc2 = 1. - x / ((17 - (i__ << 1)) * (18 - (i__ << 1))) * xc2;
	    }
	    xc2 /= 2;
	    chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	}
	x *= .3;
    }

/*     Test STMP03 for values of X greater than 1. */

    ubound = dpmax_() / 10;
    x = 1.0000000000000011;
    while(x < ubound) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "X = #", (ftnlen)240, (ftnlen)5);
	repmd_(title, "#", &x, &c__14, title, (ftnlen)240, (ftnlen)1, (ftnlen)
		240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	stmp03_(&x, &c0, &c1, &c2, &c3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Generate expected results. */

	z__ = sqrt(x);
	xc0 = cos(z__);
	xc1 = sin(z__) / z__;
	xc2 = (1. - xc0) / x;
	xc3 = (1. - xc1) / x;
	chcksd_("C0", &c0, "~/", &xc0, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C1", &c1, "~/", &xc1, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	x *= 7;
    }

/*     Test STMP03 for values of X between 0 and 1. */

    ubound = dpmax_() / 10;
    x = 1e-15;
    while(x <= 1.) {

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "X = #", (ftnlen)240, (ftnlen)5);
	repmd_(title, "#", &x, &c__14, title, (ftnlen)240, (ftnlen)1, (ftnlen)
		240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	stmp03_(&x, &c0, &c1, &c2, &c3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Generate expected results. */

	z__ = sqrt(x);
	xc0 = cos(z__);
	xc1 = sin(z__) / z__;
	chcksd_("C0", &c0, "~/", &xc0, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	chcksd_("C1", &c1, "~/", &xc1, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	if (x > .01) {
	    xc2 = (1. - xc0) / x;
	    xc3 = (1. - xc1) / x;
	    chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	    chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	} else {

/*           Compute the C3 and C2 series directly. Group terms to avoid */
/*           loss of precision. */

	    xc3 = 1. - x / 342;
	    for (i__ = 1; i__ <= 7; ++i__) {
		xc3 = 1. - x / ((18 - (i__ << 1)) * (19 - (i__ << 1))) * xc3;
	    }
	    xc3 /= 6;
	    chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	    xc2 = 1. - x / 306;
	    for (i__ = 1; i__ <= 7; ++i__) {
		xc2 = 1. - x / ((17 - (i__ << 1)) * (18 - (i__ << 1))) * xc2;
	    }
	    xc2 /= 2;
	    chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
	}
	x *= 3;
    }

/*     Test STMP03 for values of X = 0. */

    x = 0.;

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "X = #", (ftnlen)240, (ftnlen)5);
    repmd_(title, "#", &x, &c__14, title, (ftnlen)240, (ftnlen)1, (ftnlen)240)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tcase_(title, (ftnlen)240);
    stmp03_(&x, &c0, &c1, &c2, &c3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected results. */

    xc0 = 1.;
    xc1 = 1.;
    xc2 = .5;
    xc3 = .16666666666666666;
    chcksd_("C0", &c0, "~/", &xc0, &c_b15, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("C1", &c1, "~/", &xc1, &c_b15, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("C2", &c2, "~/", &xc2, &c_b15, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("C3", &c3, "~/", &xc3, &c_b15, ok, (ftnlen)2, (ftnlen)2);

/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_stmp03__ */

