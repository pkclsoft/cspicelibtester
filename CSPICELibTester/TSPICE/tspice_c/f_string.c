/* f_string.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__9 = 9;
static integer c__17 = 17;

/* $Procedure F_STRING ( Family of tests for string routines ) */
/* Subroutine */ int f_string__(logical *ok)
{
    /* Initialized data */

    static char sarray[8*13] = "a A a A " " b   B  " "  c C c " "   d   D" 
	    "    e E " "     f  " "      g " "       h" "i-I-i-I " " j---J  " 
	    "  k-K-k " "   l---L" "    m-M ";
    static char slucr1[8*13] = "A A A A " "B B     " "C C C   " "D D     " 
	    "E E     " "F       " "G       " "H       " "I-I-I-I " "J---J   " 
	    "K-K-K   " "L---L   " "M-M     ";
    static char slucr2[8*13] = "A A A A " "B  B    " "C C C   " "D  D    " 
	    "E E     " "F       " "G       " "H       " "I-I-I-I " "J---J   " 
	    "K-K-K   " "L---L   " "M-M     ";
    static char larray[16*13] = "N n N n N n N n " " O+++o+++O+++o  " "  P p"
	    " P p P p P " "   Q   q   Q   q" "    R r R r R r " "     S   s  "
	    " S  " "      T t T t T " "       U   u   U" "        V/v/V/v " 
	    "         W   w  " "          X x X " "           Y   y" "      "
	    "      Z z ";
    static char lljust[16*13] = "N n N n N n N n " "O+++o+++O+++o   " "P p P"
	    " p P p P   " "Q   q   Q   q   " "R r R r R r     " "S   s   S   "
	    "    " "T t T t T       " "U   u   U       " "V/v/V/v         " 
	    "W   w           " "X x X           " "Y   y           " "Z z   "
	    "          ";
    static char lucase[16*13] = "N N N N N N N N " " O+++O+++O+++O  " "  P P"
	    " P P P P P " "   Q   Q   Q   Q" "    R R R R R R " "     S   S  "
	    " S  " "      T T T T T " "       U   U   U" "        V/V/V/V " 
	    "         W   W  " "          X X X " "           Y   Y" "      "
	    "      Z Z ";
    static char llcase[16*13] = "n n n n n n n n " " o+++o+++o+++o  " "  p p"
	    " p p p p p " "   q   q   q   q" "    r r r r r r " "     s   s  "
	    " s  " "      t t t t t " "       u   u   u" "        v/v/v/v " 
	    "         w   w  " "          x x x " "           y   y" "      "
	    "      z z ";
    static char lljstu[16*13] = "N N N N N N N N " "O+++O+++O+++O   " "P P P"
	    " P P P P   " "Q   Q   Q   Q   " "R R R R R R     " "S   S   S   "
	    "    " "T T T T T       " "U   U   U       " "V/V/V/V         " 
	    "W   W           " "X X X           " "Y   Y           " "Z Z   "
	    "          ";
    static char lcmpr0[16*13] = "NnNnNnNn        " "O+++o+++O+++o   " "PpPpP"
	    "pP         " "QqQq            " "RrRrRr          " "SsS         "
	    "    " "TtTtT           " "UuU             " "V/v/V/v         " 
	    "Ww              " "XxX             " "Yy              " "Zz    "
	    "          ";
    static char lcmpr1[16*13] = "N n N n N n N n " " O+++o+++O+++o  " " P p "
	    "P p P p P  " " Q q Q q        " " R r R r R r    " " S s S      "
	    "    " " T t T t T      " " U u U          " " V/v/V/v        " 
	    " W w            " " X x X          " " Y y            " " Z z  "
	    "          ";
    static char lcmpr2[16*13] = "N n N n N n N n " " O+++o+++O+++o  " "  P p"
	    " P p P p P " "  Q  q  Q  q    " "  R r R r R r   " "  S  s  S   "
	    "    " "  T t T t T     " "  U  u  U       " "  V/v/V/v       " 
	    "  W  w          " "  X x X         " "  Y  y          " "  Z z "
	    "          ";
    static char sljust[8*13] = "a A a A " "b   B   " "c C c   " "d   D   " 
	    "e E     " "f       " "g       " "h       " "i-I-i-I " "j---J   " 
	    "k-K-k   " "l---L   " "m-M     ";
    static char llucr0[16*13] = "NNNNNNNN        " "O+++O+++O+++O   " "PPPPP"
	    "PP         " "QQQQ            " "RRRRRR          " "SSS         "
	    "    " "TTTTT           " "UUU             " "V/V/V/V         " 
	    "WW              " "XXX             " "YY              " "ZZ    "
	    "          ";
    static char llucr1[16*13] = "N N N N N N N N " "O+++O+++O+++O   " "P P P"
	    " P P P P   " "Q Q Q Q         " "R R R R R R     " "S S S       "
	    "    " "T T T T T       " "U U U           " "V/V/V/V         " 
	    "W W             " "X X X           " "Y Y             " "Z Z   "
	    "          ";
    static char llucr2[16*13] = "N N N N N N N N " "O+++O+++O+++O   " "P P P"
	    " P P P P   " "Q  Q  Q  Q      " "R R R R R R     " "S  S  S     "
	    "    " "T T T T T       " "U  U  U         " "V/V/V/V         " 
	    "W  W            " "X X X           " "Y  Y            " "Z Z   "
	    "          ";
    static char sucase[8*13] = "A A A A " " B   B  " "  C C C " "   D   D" 
	    "    E E " "     F  " "      G " "       H" "I-I-I-I " " J---J  " 
	    "  K-K-K " "   L---L" "    M-M ";
    static char slcase[8*13] = "a a a a " " b   b  " "  c c c " "   d   d" 
	    "    e e " "     f  " "      g " "       h" "i-i-i-i " " j---j  " 
	    "  k-k-k " "   l---l" "    m-m ";
    static char sljstu[8*13] = "A A A A " "B   B   " "C C C   " "D   D   " 
	    "E E     " "F       " "G       " "H       " "I-I-I-I " "J---J   " 
	    "K-K-K   " "L---L   " "M-M     ";
    static char scmpr0[8*13] = "aAaA    " "bB      " "cCc     " "dD      " 
	    "eE      " "f       " "g       " "h       " "i-I-i-I " "j---J   " 
	    "k-K-k   " "l---L   " "m-M     ";
    static char scmpr1[8*13] = "a A a A " " b B    " " c C c  " " d D    " 
	    " e E    " " f      " " g      " " h      " "i-I-i-I " " j---J  " 
	    " k-K-k  " " l---L  " " m-M    ";
    static char scmpr2[8*13] = "a A a A " " b  B   " "  c C c " "  d  D  " 
	    "  e E   " "  f     " "  g     " "  h     " "i-I-i-I " " j---J  " 
	    "  k-K-k " "  l---L " "  m-M   ";
    static char slucr0[8*13] = "AAAA    " "BB      " "CCC     " "DD      " 
	    "EE      " "F       " "G       " "H       " "I-I-I-I " "J---J   " 
	    "K-K-K   " "L---L   " "M-M     ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer i_len(char *, ftnlen);

    /* Local variables */
    static char lstr[16], sstr[8], lstr1[16];
    static integer e, i__;
    static char sstr1[8];
    static integer r__;
    extern /* Subroutine */ int lcase_(char *, char *, ftnlen, ftnlen), 
	    tcase_(char *, ftnlen), ucase_(char *, char *, ftnlen, ftnlen), 
	    topen_(char *, ftnlen), ljust_(char *, char *, ftnlen, ftnlen), 
	    t_success__(logical *), chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern integer lastnb_(char *, ftnlen);
    extern /* Subroutine */ int ljucrs_(integer *, char *, char *, ftnlen, 
	    ftnlen), cmprss_(char *, integer *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests string manipulation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     None */

/* $ Version */

/* -    Version 1.0.0  29-JUL-2013 (BVS) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Save all */


/*     Initial values */


/*     Short string array. */


/*     Short string array, left-justified. */


/*     Short string array, upper-cased. */


/*     Short string array, lower-cased. */


/*     Short string array, left-justified and upper-cased. */


/*     Short string array, compressed, 0 spaces. */


/*     Short string array, compressed, 1 space. */


/*     Short string array, compressed, 2 spaces. */


/*     Short string array, left-justified, upper-cased, compressed, 0 */
/*     spaces. */


/*     Short string array, left-justified, upper-cased, compressed, 1 */
/*     space. */


/*     Short string array, left-justified, upper-cased, compressed, 2 */
/*     spaces. */


/*     Long string array. */


/*     Long string array, left-justified. */


/*     Long string array, upper-cased, */


/*     Long string array, lower-cased. */


/*     Long string array, left-justified, upper-cased. */


/*     Long string array, compressed, 0 spaces. */


/*     Long string array, compressed, 1 space. */


/*     Long string array, compressed, 2 spaces. */


/*     Long string array, left-justified, upper-cased, compressed, 0 */
/*     spaces. */


/*     Long string array, left-justified, upper-cased, compressed, 1 */
/*     space. */


/*     Long string array, left-justified, upper-cased, compressed, 2 */
/*     spaces. */


/*     Begin every test family with an open call. */

    topen_("F_string", (ftnlen)8);

/*     LJUST tests. */

    tcase_("LJUST: short to short", (ftnlen)21);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljust_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)407)) << 3), sstr, (
		ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sljust + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sljust", i__1, "f_string__", (ftnlen)
		408)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUST: short to short, same I/O", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)413)) << 3), (
		ftnlen)8, (ftnlen)8);
	ljust_(sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sljust + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sljust", i__1, "f_string__", (ftnlen)
		415)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUST: long to long", (ftnlen)19);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljust_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)420)) << 4), lstr, (
		ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lljust + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lljust", i__1, "f_string__", (ftnlen)
		421)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LJUST: short to long", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(sljust + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sljust", i__1, "f_string__", (ftnlen)427)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, sljust + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sljust", i__1, "f_string__", (ftnlen)429)) << 3), e, 
		e);
	ljust_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)430)) << 3), lstr, (
		ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LJUST: long to short", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lljust + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lljust", i__1, "f_string__", (ftnlen)437)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lljust + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lljust", i__1, "f_string__", (ftnlen)439)) << 4), e, 
		e);
	ljust_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)440)) << 4), sstr, (
		ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUST: blank", (ftnlen)12);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    ljust_(sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);

/*     UCASE tests. */

    tcase_("UCASE: short to short", (ftnlen)21);
    for (i__ = 1; i__ <= 13; ++i__) {
	ucase_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)457)) << 3), sstr, (
		ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sucase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sucase", i__1, "f_string__", (ftnlen)
		458)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("UCASE: short to short, same I/O", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)463)) << 3), (
		ftnlen)8, (ftnlen)8);
	ucase_(sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sucase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sucase", i__1, "f_string__", (ftnlen)
		465)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("UCASE: long to long", (ftnlen)19);
    for (i__ = 1; i__ <= 13; ++i__) {
	ucase_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)470)) << 4), lstr, (
		ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lucase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lucase", i__1, "f_string__", (ftnlen)
		471)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("UCASE: short to long", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(sucase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sucase", i__1, "f_string__", (ftnlen)477)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, sucase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sucase", i__1, "f_string__", (ftnlen)479)) << 3), e, 
		e);
	ucase_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)480)) << 3), lstr, (
		ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("UCASE: long to short", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lucase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lucase", i__1, "f_string__", (ftnlen)487)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lucase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lucase", i__1, "f_string__", (ftnlen)489)) << 4), e, 
		e);
	ucase_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)490)) << 4), sstr, (
		ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("UCASE: blank", (ftnlen)12);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    ucase_(sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);

/*     LCASE tests. */

    tcase_("LCASE: short to short", (ftnlen)21);
    for (i__ = 1; i__ <= 13; ++i__) {
	lcase_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)506)) << 3), sstr, (
		ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slcase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slcase", i__1, "f_string__", (ftnlen)
		507)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LCASE: short to short, same I/O", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)512)) << 3), (
		ftnlen)8, (ftnlen)8);
	lcase_(sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slcase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slcase", i__1, "f_string__", (ftnlen)
		514)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LCASE: long to long", (ftnlen)19);
    for (i__ = 1; i__ <= 13; ++i__) {
	lcase_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)519)) << 4), lstr, (
		ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", llcase + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("llcase", i__1, "f_string__", (ftnlen)
		520)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LCASE: short to long", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(slcase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slcase", i__1, "f_string__", (ftnlen)526)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, slcase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slcase", i__1, "f_string__", (ftnlen)528)) << 3), e, 
		e);
	lcase_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"sarray", i__1, "f_string__", (ftnlen)529)) << 3), lstr, (
		ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LCASE: long to short", (ftnlen)20);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(llcase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llcase", i__1, "f_string__", (ftnlen)536)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, llcase + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llcase", i__1, "f_string__", (ftnlen)538)) << 4), e, 
		e);
	lcase_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : s_rnge(
		"larray", i__1, "f_string__", (ftnlen)539)) << 4), sstr, (
		ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LCASE: blank", (ftnlen)12);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    lcase_(sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);

/*     CMPRSS tests. */

    tcase_("CMPRSS: short to short, 0", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__0, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)556)) << 
		3), sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr0", i__1, "f_string__", (ftnlen)
		557)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, 1", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__1, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)562)) << 
		3), sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr1", i__1, "f_string__", (ftnlen)
		563)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, 2", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__2, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)568)) << 
		3), sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr2", i__1, "f_string__", (ftnlen)
		569)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, too many", (ftnlen)32);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__9, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)574)) << 
		3), sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sarray + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)
		575)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, same I/O, 0", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)580)) << 3), (
		ftnlen)8, (ftnlen)8);
	cmprss_(" ", &c__0, sstr, sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr0", i__1, "f_string__", (ftnlen)
		582)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, same I/O, 1", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)587)) << 3), (
		ftnlen)8, (ftnlen)8);
	cmprss_(" ", &c__1, sstr, sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr1", i__1, "f_string__", (ftnlen)
		589)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, same I/O, 2", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)594)) << 3), (
		ftnlen)8, (ftnlen)8);
	cmprss_(" ", &c__2, sstr, sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", scmpr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("scmpr2", i__1, "f_string__", (ftnlen)
		596)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: short to short, same I/O, too many", (ftnlen)42);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)601)) << 3), (
		ftnlen)8, (ftnlen)8);
	cmprss_(" ", &c__9, sstr, sstr, (ftnlen)1, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sarray + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)
		603)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: long to long, 0", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__0, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)608)) << 
		4), lstr, (ftnlen)1, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lcmpr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lcmpr0", i__1, "f_string__", (ftnlen)
		609)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("CMPRSS: long to long, 1", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__1, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)614)) << 
		4), lstr, (ftnlen)1, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lcmpr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lcmpr1", i__1, "f_string__", (ftnlen)
		615)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("CMPRSS: long to long, 2", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__2, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)620)) << 
		4), lstr, (ftnlen)1, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lcmpr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lcmpr2", i__1, "f_string__", (ftnlen)
		621)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("CMPRSS: long to long, too many", (ftnlen)30);
    for (i__ = 1; i__ <= 13; ++i__) {
	cmprss_(" ", &c__17, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)626)) << 
		4), lstr, (ftnlen)1, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", larray + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)
		627)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("CMPRSS: short to long, 0", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(scmpr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr0", i__1, "f_string__", (ftnlen)633)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, scmpr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr0", i__1, "f_string__", (ftnlen)635)) << 3), e, 
		e);
	cmprss_(" ", &c__0, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)636)) << 
		3), lstr, (ftnlen)1, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("CMPRSS: short to long, 1", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(scmpr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr1", i__1, "f_string__", (ftnlen)643)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, scmpr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr1", i__1, "f_string__", (ftnlen)645)) << 3), e, 
		e);
	cmprss_(" ", &c__1, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)646)) << 
		3), lstr, (ftnlen)1, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("CMPRSS: short to long, 2", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(scmpr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr2", i__1, "f_string__", (ftnlen)653)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, scmpr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("scmpr2", i__1, "f_string__", (ftnlen)655)) << 3), e, 
		e);
	cmprss_(" ", &c__2, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)656)) << 
		3), lstr, (ftnlen)1, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("CMPRSS: short to long, too many", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)663)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)665)) << 3), e, 
		e);
	cmprss_(" ", &c__9, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("sarray", i__1, "f_string__", (ftnlen)666)) << 
		3), lstr, (ftnlen)1, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("CMPRSS: long to short, 0", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lcmpr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr0", i__1, "f_string__", (ftnlen)673)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lcmpr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr0", i__1, "f_string__", (ftnlen)675)) << 4), e, 
		e);
	cmprss_(" ", &c__0, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)676)) << 
		4), sstr, (ftnlen)1, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: long to short, 1", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lcmpr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr1", i__1, "f_string__", (ftnlen)683)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lcmpr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr1", i__1, "f_string__", (ftnlen)685)) << 4), e, 
		e);
	cmprss_(" ", &c__1, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)686)) << 
		4), sstr, (ftnlen)1, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: long to short, 2", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lcmpr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr2", i__1, "f_string__", (ftnlen)693)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lcmpr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lcmpr2", i__1, "f_string__", (ftnlen)695)) << 4), e, 
		e);
	cmprss_(" ", &c__2, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)696)) << 
		4), sstr, (ftnlen)1, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: long to short, too many", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)703)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)705)) << 4), e, 
		e);
	cmprss_(" ", &c__17, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? 
		i__1 : s_rnge("larray", i__1, "f_string__", (ftnlen)706)) << 
		4), sstr, (ftnlen)1, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("CMPRSS: blank, 0", (ftnlen)16);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    cmprss_(" ", &c__0, sstr, sstr1, (ftnlen)1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);
    tcase_("CMPRSS: blank, 1", (ftnlen)16);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    cmprss_(" ", &c__1, sstr, sstr1, (ftnlen)1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);
    tcase_("CMPRSS: blank, too many", (ftnlen)23);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    cmprss_(" ", &c__9, sstr, sstr1, (ftnlen)1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);

/*     LJUCRS tests. */

    tcase_("LJUCRS: short to short, 0", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__0, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)737)) << 3), 
		sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr0", i__1, "f_string__", (ftnlen)
		738)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, 1", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__1, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)743)) << 3), 
		sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr1", i__1, "f_string__", (ftnlen)
		744)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, 2", (ftnlen)25);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__2, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)749)) << 3), 
		sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr2", i__1, "f_string__", (ftnlen)
		750)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, too many", (ftnlen)32);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__9, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)755)) << 3), 
		sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sljstu + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sljstu", i__1, "f_string__", (ftnlen)
		756)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, same I/O, 0", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)761)) << 3), (
		ftnlen)8, (ftnlen)8);
	ljucrs_(&c__0, sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr0", i__1, "f_string__", (ftnlen)
		763)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, same I/O, 1", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)768)) << 3), (
		ftnlen)8, (ftnlen)8);
	ljucrs_(&c__1, sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr1", i__1, "f_string__", (ftnlen)
		770)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, same I/O, 2", (ftnlen)35);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)775)) << 3), (
		ftnlen)8, (ftnlen)8);
	ljucrs_(&c__2, sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", slucr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("slucr2", i__1, "f_string__", (ftnlen)
		777)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: short to short, same I/O, too many", (ftnlen)42);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)782)) << 3), (
		ftnlen)8, (ftnlen)8);
	ljucrs_(&c__9, sstr, sstr, (ftnlen)8, (ftnlen)8);
	chcksc_("SARRAY(I)", sstr, "=", sljstu + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("sljstu", i__1, "f_string__", (ftnlen)
		784)) << 3), ok, (ftnlen)9, (ftnlen)8, (ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: long to long, 0", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__0, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)789)) << 4), 
		lstr, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", llucr0 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("llucr0", i__1, "f_string__", (ftnlen)
		790)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LJUCRS: long to long, 1", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__1, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)795)) << 4), 
		lstr, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", llucr1 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("llucr1", i__1, "f_string__", (ftnlen)
		796)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LJUCRS: long to long, 2", (ftnlen)23);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__2, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)801)) << 4), 
		lstr, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", llucr2 + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("llucr2", i__1, "f_string__", (ftnlen)
		802)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LJUCRS: long to long, too many", (ftnlen)30);
    for (i__ = 1; i__ <= 13; ++i__) {
	ljucrs_(&c__17, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 :
		 s_rnge("larray", i__1, "f_string__", (ftnlen)807)) << 4), 
		lstr, (ftnlen)16, (ftnlen)16);
	chcksc_("LARRAY(I)", lstr, "=", lljstu + (((i__1 = i__ - 1) < 13 && 0 
		<= i__1 ? i__1 : s_rnge("lljstu", i__1, "f_string__", (ftnlen)
		808)) << 4), ok, (ftnlen)9, (ftnlen)16, (ftnlen)1, (ftnlen)16)
		;
    }
    tcase_("LJUCRS: short to long, 0", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(slucr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr0", i__1, "f_string__", (ftnlen)814)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, slucr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr0", i__1, "f_string__", (ftnlen)816)) << 3), e, 
		e);
	ljucrs_(&c__0, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)817)) << 3), 
		lstr, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LJUCRS: short to long, 1", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(slucr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr1", i__1, "f_string__", (ftnlen)824)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, slucr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr1", i__1, "f_string__", (ftnlen)826)) << 3), e, 
		e);
	ljucrs_(&c__1, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)827)) << 3), 
		lstr, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LJUCRS: short to long, 2", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(slucr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr2", i__1, "f_string__", (ftnlen)834)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, slucr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("slucr2", i__1, "f_string__", (ftnlen)836)) << 3), e, 
		e);
	ljucrs_(&c__2, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)837)) << 3), 
		lstr, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LJUCRS: short to long, too many", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(lstr1, " ", (ftnlen)16, (ftnlen)1);
	r__ = lastnb_(sljstu + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sljstu", i__1, "f_string__", (ftnlen)844)) << 3), (
		ftnlen)8);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(lstr1, (ftnlen)16);
	e = min(i__1,i__2);
	s_copy(lstr1, sljstu + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sljstu", i__1, "f_string__", (ftnlen)846)) << 3), e, 
		e);
	ljucrs_(&c__9, sarray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("sarray", i__1, "f_string__", (ftnlen)847)) << 3), 
		lstr, (ftnlen)8, (ftnlen)16);
	chcksc_("SARRAY(I)", lstr, "=", lstr1, ok, (ftnlen)9, (ftnlen)16, (
		ftnlen)1, (ftnlen)16);
    }
    tcase_("LJUCRS: long to short, 0", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(llucr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr0", i__1, "f_string__", (ftnlen)854)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, llucr0 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr0", i__1, "f_string__", (ftnlen)856)) << 4), e, 
		e);
	ljucrs_(&c__0, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)857)) << 4), 
		sstr, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: long to short, 1", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(llucr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr1", i__1, "f_string__", (ftnlen)864)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, llucr1 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr1", i__1, "f_string__", (ftnlen)866)) << 4), e, 
		e);
	ljucrs_(&c__1, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)867)) << 4), 
		sstr, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: long to short, 2", (ftnlen)24);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(llucr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr2", i__1, "f_string__", (ftnlen)874)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, llucr2 + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("llucr2", i__1, "f_string__", (ftnlen)876)) << 4), e, 
		e);
	ljucrs_(&c__2, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("larray", i__1, "f_string__", (ftnlen)877)) << 4), 
		sstr, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: long to short, too many", (ftnlen)31);
    for (i__ = 1; i__ <= 13; ++i__) {
	s_copy(sstr1, " ", (ftnlen)8, (ftnlen)1);
	r__ = lastnb_(lljstu + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lljstu", i__1, "f_string__", (ftnlen)884)) << 4), (
		ftnlen)16);
/* Computing MIN */
	i__1 = r__, i__2 = i_len(sstr1, (ftnlen)8);
	e = min(i__1,i__2);
	s_copy(sstr1, lljstu + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 : 
		s_rnge("lljstu", i__1, "f_string__", (ftnlen)886)) << 4), e, 
		e);
	ljucrs_(&c__17, larray + (((i__1 = i__ - 1) < 13 && 0 <= i__1 ? i__1 :
		 s_rnge("larray", i__1, "f_string__", (ftnlen)887)) << 4), 
		sstr, (ftnlen)16, (ftnlen)8);
	chcksc_("LARRAY(I)", sstr, "=", sstr1, ok, (ftnlen)9, (ftnlen)8, (
		ftnlen)1, (ftnlen)8);
    }
    tcase_("LJUCRS: blank, 0", (ftnlen)16);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    ljucrs_(&c__0, sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);
    tcase_("LJUCRS: blank, 1", (ftnlen)16);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    ljucrs_(&c__1, sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);
    tcase_("LJUCRS: blank, too many", (ftnlen)23);
    s_copy(sstr, " ", (ftnlen)8, (ftnlen)1);
    s_copy(sstr1, "boo", (ftnlen)8, (ftnlen)3);
    ljucrs_(&c__9, sstr, sstr1, (ftnlen)8, (ftnlen)8);
    chcksc_("SLMST1", sstr1, "=", " ", ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, (
	    ftnlen)1);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_string__ */

