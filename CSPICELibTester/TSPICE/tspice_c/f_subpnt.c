/* f_subpnt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c_n499 = -499;
static integer c__499 = 499;
static integer c__1 = 1;
static integer c_n666 = -666;
static integer c__4 = 4;
static integer c__14 = 14;
static integer c__3 = 3;
static doublereal c_b195 = 0.;
static integer c__599 = 599;
static integer c__399 = 399;
static integer c__10 = 10;
static doublereal c_b475 = -1.;
static doublereal c_b477 = 3.;

/* $Procedure      F_SUBPNT ( SUBPNT family tests ) */
/* Subroutine */ int f_subpnt__(logical *ok)
{
    /* Initialized data */

    static char abcs[10*9] = "None      " "Lt        " "Lt+s      " "Cn     "
	    "   " "Cn+s      " "Xlt       " "Xlt+s     " "Xcn       " "Xcn+s "
	    "    ";
    static char refs[32*1*2] = "IAU_MARS                        " "IAU_PHOBO"
	    "S                      ";
    static char obsnms[32*2] = "Earth                           " "MARS_ORBI"
	    "TER                    ";
    static char trgnms[32*2] = "Mars                            " "PHOBOS   "
	    "                       ";
    static char methds[500*8] = "ELLIPSOID / intercept                      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "         " "intercept: ellipsoid                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                " 
	    "near point: ellipsoid                                          "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                     " "near poi"
	    "nt /ellipsoid                                                   "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                            " "nadir/dsk/unprior"
	    "itized/surfaces=\"high-res\"                                    "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                     " "dsk/ nadir /unprioritize"
	    "d/surfaces=\"high-res\"                                         "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                              " "intercept/ UNPRIORITIZED/ dsk /"
	    "SURFACES =\"LOW-RES\"                                           "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                       " "intercept/UNPRIORITIZED/ dsk /SURFACES"
	    " =\"LOW-RES\"                                                   "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static integer nlat, nlon;
    static doublereal elts[8];
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), zzcorepc_(char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen);
    static integer n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmc_(char *, char *, 
	    char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), repmd_(char *, 
	    char *, doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    static logical found, usecn;
    static doublereal state[6];
    static char title[240];
    extern /* Subroutine */ int topen_(char *, ftnlen), spkw05_(integer *, 
	    integer *, integer *, char *, doublereal *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, doublereal *, ftnlen, 
	    ftnlen);
    static logical uselt;
    extern /* Subroutine */ int dskxv_(logical *, char *, integer *, integer *
	    , doublereal *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen), bodn2c_(char *, integer 
	    *, logical *, ftnlen), t_success__(logical *);
    static doublereal state0[6], badrad[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     str2et_(char *, doublereal *, ftnlen), boddef_(char *, integer *,
	     ftnlen);
    static doublereal et;
    static integer abcidx, handle[2], obscde;
    static doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    extern logical matchi_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chcksl_(
	    char *, logical *, logical *, logical *, ftnlen);
    static char abcorr[10];
    static integer trgcde, bodyid;
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen);
    static doublereal tdelta;
    extern /* Subroutine */ int conics_(doublereal *, doublereal *, 
	    doublereal *);
    static char method[500], fixref[32], srfnms[32*4], target[32], obsrvr[32];
    extern logical exists_(char *, ftnlen);
    static char trgfrm[32];
    static doublereal esubpt[3], et0, obspos[3], spoint[3], srfvec[3], subdir[
	    3], trgepc, xepoch, xsubpt[3], xsrfvc[3];
    static integer obsidx, refidx, srfbod[4], srfids[4], surfid, srflst[100], 
	    timidx, trgidx;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     tstlsk_(void), spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), spkcls_(integer *), spklef_(char *, integer *, 
	    ftnlen), pcpool_(char *, integer *, char *, ftnlen, ftnlen), 
	    pipool_(char *, integer *, integer *, ftnlen), furnsh_(char *, 
	    ftnlen), bodvar_(integer *, char *, integer *, doublereal *, 
	    ftnlen), subpnt_(char *, char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), spkcpt_(doublereal *, char *, char *, 
	    doublereal *, char *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    surfpt_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *), nearpt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), unload_(char *, ftnlen), spkuef_(integer *), 
	    clpool_(void), ldpool_(char *, ftnlen), dvpool_(char *, ftnlen), 
	    bodvcd_(integer *, char *, integer *, integer *, doublereal *, 
	    ftnlen), pdpool_(char *, integer *, doublereal *, ftnlen);
    static doublereal alt;
    extern doublereal rpd_(void);
    static char utc[50];
    static integer mix;
    static doublereal tol, xte;

/* $ Abstract */

/*     Exercise the higher-level SPICELIB geometry routine SUBPNT. */
/*     Use DSK-based and ellipsoidal target shape models. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine SUBPNT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 3.0.0 06-APR-2016 (NJB) */

/*        Updated to test DSK usage. Also more thoroughly */
/*        tests ellipsoid target cases. */

/* -    TSPICE Version 2.0.0, 02-APR-2012 (NJB) */

/*           Added cases for bad aberration correction values. */

/* -    TSPICE Version 1.0.0, 22-FEB-2008 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     REFS is a two-dimensional array. There's a set of */
/*     ray reference  frames for each target. Currently */
/*     there are only two targets: Mars and Phobos. */


/*     Note that the last two method strings are identical. This */
/*     is done to test the logic that uses saved values obtained */
/*     by parsing method string. */


/*     Begin every test family with an open call. */

    topen_("F_SUBPNT", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup:  create SPK, PCK file.", (ftnlen)29);
    tstspk_("subpnt_spk.bsp", &c_true, handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK file, and load it. Do not delete it. */

    t_pck08__("test_0008.tpc", &c_true, &c_true, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create LSK, load it, and delete it. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set initial time. */

    s_copy(utc, "2004 FEB 17", (ftnlen)50, (ftnlen)11);
    str2et_(utc, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = et0;
    tdelta = jyear_();

/*     Create a Mars orbiter SPK file. */

    spkopn_("orbiter.bsp", "orbiter.bsp", &c__0, &handle[1], (ftnlen)11, (
	    ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up elements defining a state.  The elements expected */
/*     by CONICS are: */

/*        RP      Perifocal distance. */
/*        ECC     Eccentricity. */
/*        INC     Inclination. */
/*        LNODE   Longitude of the ascending node. */
/*        ARGP    Argument of periapse. */
/*        M0      Mean anomaly at epoch. */
/*        T0      Epoch. */
/*        MU      Gravitational parameter. */

    elts[0] = 3800.;
    elts[1] = .1;
    elts[2] = rpd_() * 80.;
    elts[3] = 0.;
    elts[4] = rpd_() * 90.;
    elts[5] = 0.;
    elts[6] = et;
    elts[7] = 42828.314;
    conics_(elts, &et, state0);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = jyear_() * -10;
    d__2 = jyear_() * 10;
    spkw05_(&handle[1], &c_n499, &c__499, "MARSIAU", &d__1, &d__2, "Mars orb"
	    "iter", &elts[7], &c__1, state0, &et, (ftnlen)7, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the new SPK file. */

    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add the orbiter's name/ID mapping to the kernel pool. */

    pcpool_("NAIF_BODY_NAME", &c__1, obsnms + 32, (ftnlen)14, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_BODY_CODE", &c__1, &c_n499, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add an incomplete frame definition to the kernel pool; */
/*     we'll need this later. */

    pipool_("FRAME_BAD_NAME", &c__1, &c_n666, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 200;
    nlat = 100;
    if (exists_("subpnt_dsk0.bds", (ftnlen)15)) {
	delfil_("subpnt_dsk0.bds", (ftnlen)15);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "subpnt_dsk0.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars is very low-res. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 40;
    nlat = 20;
    if (exists_("subpnt_dsk1.bds", (ftnlen)15)) {
	delfil_("subpnt_dsk1.bds", (ftnlen)15);
    }

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "subpnt_dsk1.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("subpnt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is low-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("subpnt_dsk2.bds", (ftnlen)15)) {
	delfil_("subpnt_dsk2.bds", (ftnlen)15);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "subpnt_dsk2.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("subpnt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("subpnt_dsk3.bds", (ftnlen)15)) {
	delfil_("subpnt_dsk3.bds", (ftnlen)15);
    }

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "subpnt_dsk3.bds", (
	    ftnlen)32, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("subpnt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 401;
    srfids[2] = 1;
    s_copy(srfnms + 64, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[3] = 401;
    srfids[3] = 2;
    s_copy(srfnms + 96, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__4, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__4, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Main test loop follows. */


/*     Loop over every choice of observer. */

    for (obsidx = 1; obsidx <= 2; ++obsidx) {
	s_copy(obsrvr, obsnms + (((i__1 = obsidx - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("obsnms", i__1, "f_subpnt__", (ftnlen)552)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Set the observer ID code. */

	bodn2c_(obsrvr, &obscde, &found, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Loop over every choice of target. */

	for (trgidx = 1; trgidx <= 2; ++trgidx) {
	    s_copy(target, trgnms + (((i__1 = trgidx - 1) < 2 && 0 <= i__1 ? 
		    i__1 : s_rnge("trgnms", i__1, "f_subpnt__", (ftnlen)564)) 
		    << 5), (ftnlen)32, (ftnlen)32);

/*           Set the target ID code. */

	    bodn2c_(target, &trgcde, &found, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Get target radii. */

	    bodvar_(&trgcde, "RADII", &n, radii, (ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over the time sequence. */

	    for (timidx = 1; timidx <= 3; ++timidx) {
		et = et0 + (timidx - 1) * tdelta;

/*              Loop over every aberration correction choice. */

		for (abcidx = 1; abcidx <= 9; ++abcidx) {
		    s_copy(abcorr, abcs + ((i__1 = abcidx - 1) < 9 && 0 <= 
			    i__1 ? i__1 : s_rnge("abcs", i__1, "f_subpnt__", (
			    ftnlen)590)) * 10, (ftnlen)10, (ftnlen)10);

/*                 Set up some logical variables describing the */
/*                 attributes of the selected correction. */

		    uselt = s_cmp(abcorr, "None", (ftnlen)10, (ftnlen)4) != 0;
		    usecn = s_cmp(abcorr, "Cn", (ftnlen)2, (ftnlen)2) == 0 || 
			    s_cmp(abcorr, "Xcn", (ftnlen)3, (ftnlen)3) == 0;

/*                 Loop over every target body-fixed frame choice. */

		    for (refidx = 1; refidx <= 1; ++refidx) {
			s_copy(trgfrm, refs + (((i__1 = refidx + trgidx - 2) <
				 2 && 0 <= i__1 ? i__1 : s_rnge("refs", i__1, 
				"f_subpnt__", (ftnlen)605)) << 5), (ftnlen)32,
				 (ftnlen)32);

/*                    Loop over all method choices. */

			for (mix = 1; mix <= 8; ++mix) {
			    s_copy(method, methds + ((i__1 = mix - 1) < 8 && 
				    0 <= i__1 ? i__1 : s_rnge("methds", i__1, 
				    "f_subpnt__", (ftnlen)613)) * 500, (
				    ftnlen)500, (ftnlen)500);

/* --- Case: ------------------------------------------------------ */

			    s_copy(title, "Observer = #; Target = #; ABCORR "
				    "= #; TRGFRM = #; METHOD = #; ET = #.", (
				    ftnlen)240, (ftnlen)69);
			    repmc_(title, "#", obsrvr, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", target, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", abcorr, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)10, (ftnlen)240);
			    repmc_(title, "#", trgfrm, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)32, (ftnlen)240);
			    repmc_(title, "#", method, title, (ftnlen)240, (
				    ftnlen)1, (ftnlen)500, (ftnlen)240);
			    repmd_(title, "#", &et, &c__14, title, (ftnlen)
				    240, (ftnlen)1, (ftnlen)240);
			    tcase_(title, (ftnlen)240);

/*                       Start off by computing the sub-observer point. */
/*                       We'll then check the results. */

			    subpnt_(method, target, &et, trgfrm, abcorr, 
				    obsrvr, spoint, &trgepc, srfvec, (ftnlen)
				    500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (
				    ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       We'll treat the computed sub-observer point as */
/*                       an ephemeris object and find its position */
/*                       relative to the observer. */

			    spkcpt_(spoint, target, trgfrm, &et, trgfrm, 
				    "TARGET", abcorr, obsrvr, state, &lt, (
				    ftnlen)32, (ftnlen)32, (ftnlen)32, (
				    ftnlen)6, (ftnlen)10, (ftnlen)32);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                       If SPOINT is correct, then the position of */
/*                       SPOINT relative to the observer should be equal */
/*                       to SRFVEC. The light time obtained from SPKCPT */
/*                       should match that implied by TRGEPC. */

			    tol = 1e-12;
			    zzcorepc_(abcorr, &et, &lt, &xte, (ftnlen)10);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    chcksd_("TRGEPC", &trgepc, "~/", &xte, &tol, ok, (
				    ftnlen)6, (ftnlen)2);
			    if (uselt) {
				if (usecn) {
				    tol = 1e-10;
				} else {
				    tol = 5e-6;
				}
			    } else {
				tol = 1e-14;
			    }
			    chckad_("SRFVEC", srfvec, "~~/", state, &c__3, &
				    tol, ok, (ftnlen)6, (ftnlen)3);

/*                       We've checked the consistency of SPOINT, */
/*                       SRFVEC, and TRGEPC, but we haven't done */
/*                       anything to show that SPOINT is a sub-observer */
/*                       point. Do that now. */

/*                       Compute the position of the observer relative to */
/*                       the target center. */

			    vsub_(spoint, state, obspos);
			    found = FALSE_;
			    if (matchi_(method, "*INTERCEPT*", "*", "?", (
				    ftnlen)500, (ftnlen)11, (ftnlen)1, (
				    ftnlen)1)) {
				if (matchi_(method, "*ELLIPSOID*", "*", "?", (
					ftnlen)500, (ftnlen)11, (ftnlen)1, (
					ftnlen)1)) {
				    surfpt_(obspos, state, radii, &radii[1], &
					    radii[2], xsubpt, &found);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				} else {

/*                             This is the DSK case. Note that we're */
/*                             working with the target epoch (although */
/*                             in this case, it's only used for DSK */
/*                             segment selection). */

/*                             Set the surface list according to the */
/*                             value of the surface list in METHOD. */

				    if (matchi_(method, "*HIGH-RES*", "*", 
					    "?", (ftnlen)500, (ftnlen)10, (
					    ftnlen)1, (ftnlen)1)) {
					srflst[0] = 1;
				    } else {
					srflst[0] = 2;
				    }
				    dskxv_(&c_false, target, &c__1, srflst, &
					    trgepc, trgfrm, &c__1, obspos, 
					    state, xsubpt, &found, (ftnlen)32,
					     (ftnlen)32);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				}
				chcksl_("FOUND", &found, &c_true, ok, (ftnlen)
					5);

/*                          Use the tolerance we set for the */
/*                          SRFVEC test. */

/*                          To simplify the comparison, we'll convert */
/*                          the expected sub-observer point to an */
/*                          expected observer-surface vector. This will */
/*                          give us a vector having the scale on which */
/*                          we based our tolerance magnitude. */

				vsub_(xsubpt, obspos, xsrfvc);
				chckad_("SRFVEC (2)", srfvec, "~~/", xsrfvc, &
					c__3, &tol, ok, (ftnlen)10, (ftnlen)3)
					;
			    } else {

/*                          This is the "near point" or "nadir" case. */

/*                          We need the ellipsoid near point for both */
/*                          the ellipsoid and DSK target shape cases. */

				nearpt_(obspos, radii, &radii[1], &radii[2], 
					esubpt, &alt);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				if (matchi_(method, "*ELLIPSOID*", "*", "?", (
					ftnlen)500, (ftnlen)11, (ftnlen)1, (
					ftnlen)1)) {

/*                             This is the ellipsoid case. */

				    vequ_(esubpt, xsubpt);
				} else {

/*                             This is the DSK case. */

/*                             Compute the direction vector of a ray */
/*                             pointing from the observer to the */
/*                             ellipsoid near point. */

				    vsub_(esubpt, obspos, subdir);

/*                             Set the surface list according to the */
/*                             value of the surface list in METHOD. */

				    if (matchi_(method, "*HIGH-RES*", "*", 
					    "?", (ftnlen)500, (ftnlen)10, (
					    ftnlen)1, (ftnlen)1)) {
					srflst[0] = 1;
				    } else {
					srflst[0] = 2;
				    }

/*                             Update XSUBPT to be the DSK surface */
/*                             intercept of the ray from the observer to */
/*                             the ellipsoid near point. */

				    dskxv_(&c_false, target, &c__1, srflst, &
					    trgepc, trgfrm, &c__1, obspos, 
					    subdir, xsubpt, &found, (ftnlen)
					    32, (ftnlen)32);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    chcksl_("FOUND", &found, &c_true, ok, (
					    ftnlen)5);
				}

/*                          Update XSRFVC based on the expected near */
/*                          point or nadir point. */

				vsub_(xsubpt, obspos, xsrfvc);
				chckad_("SRFVEC (2)", srfvec, "~~/", xsrfvc, &
					c__3, &tol, ok, (ftnlen)10, (ftnlen)3)
					;
			    }

/*                       We're finished with the consistency checks for */
/*                       the intercept and near point/nadir cases. */

			}

/*                    End of the method loop. */

		    }

/*                 End of the reference frame loop. */

		}

/*              End of the aberration correction loop. */

	    }

/*           End of the time loop. */

	}

/*        End of the target loop. */

    }

/*     End of the observer loop. */

/* *********************************************************************** */

/*     Normal case: input handling */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Input handling tests:  make sure target and observer */
/*     can be identified using integer "names." */

    tcase_("Use integer observer and target names.", (ftnlen)38);

/*     Set target and target-fixed frame. */

    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(method, "ellipsoid/intercept", (ftnlen)500, (ftnlen)19);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, xsubpt, &xepoch, 
	    xsrfvc, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_(method, "499", &et, fixref, abcorr, "399", spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)3, (ftnlen)32, (ftnlen)10, (ftnlen)3)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);
    chckad_("SRFVEC", srfvec, "=", xsrfvc, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);
    chcksd_("TRGEPC", &trgepc, "=", &xepoch, &c_b195, ok, (ftnlen)6, (ftnlen)
	    1);
/* *********************************************************************** */

/*     Normal case: state change detection */

/* *********************************************************************** */

/*     Certain subsystem state changes must be detected and responded to */
/*     by SINCPT. The subsystems (or structures) having states that must */
/*     be monitored are: */

/*        - Target name-ID mapping */

/*        - Observer name-ID mapping */

/*        - Surface name-ID mapping */

/*        - Target body-fixed frame definition */

/*        - ZZDSKBSR state */


/* --- Case: ------------------------------------------------------ */

    tcase_("Target name changed to JUPITER for ID code 499.", (ftnlen)47);

/*     First, get expected intercept. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, xsubpt, &xepoch, 
	    xsrfvc, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    boddef_("JUPITER", &c__499, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);
/*     Restore original mapping. */

    boddef_("JUPITER", &c__599, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer name changed to SUN for ID code 399.", (ftnlen)45);
    boddef_("SUN", &c__399, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    boddef_("SUN", &c__10, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars high-res surface name changed to AAAbbb.", (ftnlen)45);

/*     Get expected results first. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "intercept/dsk/unprioritized/surfaces = 1", (ftnlen)500, (
	    ftnlen)40);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, xsubpt, &xepoch, 
	    xsrfvc, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(srfnms, "AAAbbb", (ftnlen)32, (ftnlen)6);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "intercept/dsk/unprioritized/surfaces = AAAbbb", (ftnlen)
	    500, (ftnlen)45);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Restore original mapping. */

    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars high-res DSK.", (ftnlen)25);

/*     Get reference result using low-res Mars DSK. */

    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "nadir/dsk/unprioritized/surfaces = low-res", (ftnlen)500, 
	    (ftnlen)42);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, xsubpt, &xepoch, 
	    xsrfvc, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Unload the high-res DSK; set METHOD to remove */
/*     surface specification. */

    unload_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "nadir/dsk/unprioritized", (ftnlen)500, (ftnlen)23);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars low-res DSK; reload Mars high-res DSK.", (ftnlen)50);

/*     Restore DSK, unload low-res DSK, and repeat computation. */

    furnsh_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "nadir/dsk/unprioritized", (ftnlen)500, (ftnlen)23);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the result matches that obtained with the */
/*     high-res DSK specified. */

    s_copy(method, "nadir/dsk/unprioritized/ SURFACES = \"HIGH-RES\" ", (
	    ftnlen)500, (ftnlen)47);
    subpnt_(method, target, &et, fixref, abcorr, obsrvr, xsubpt, &xepoch, 
	    xsrfvc, (ftnlen)500, (ftnlen)32, (ftnlen)32, (ftnlen)10, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an exact match here. */

    chckad_("SPOINT", spoint, "=", xsubpt, &c__3, &c_b195, ok, (ftnlen)6, (
	    ftnlen)1);
/* *********************************************************************** */

/*     Error handling tests follow. */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid method.", (ftnlen)15);
    subpnt_("INTERCPT/ ELLIPSOID", "EARTH", &et, "IAU_EARTH", "NONE", "SUN", 
	    spoint, &trgepc, srfvec, (ftnlen)19, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    subpnt_("INTERCEPT: ELLIPSOIDD", "EARTH", &et, "IAU_EARTH", "NONE", "SUN",
	     spoint, &trgepc, srfvec, (ftnlen)21, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    subpnt_("ellipsoid / INTERCEPT /", "EARTH", &et, "IAU_EARTH", "NONE", 
	    "SUN", spoint, &trgepc, srfvec, (ftnlen)23, (ftnlen)5, (ftnlen)9, 
	    (ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    subpnt_("ELLIPSOID", "EARTH", &et, "IAU_EARTH", "NONE", "SUN", spoint, &
	    trgepc, srfvec, (ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)4, (
	    ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDSUBTYPE)", ok, (ftnlen)21);
    subpnt_("INTERCEPT ELLIPSOID", "EARTH", &et, "IAU_EARTH", "NONE", "SUN", 
	    spoint, &trgepc, srfvec, (ftnlen)19, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid target name.", (ftnlen)20);
    subpnt_("INTERCEPT: ELLIPSOID", "ERTH", &et, "IAU_EARTH", "NONE", "SUN", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)4, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid observer name.", (ftnlen)22);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "NONE", "SN", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer is target.", (ftnlen)19);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "NONE", "earth"
	    , spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)5);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid reference frame center", (ftnlen)30);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_MOON", "NONE", "SUN", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)8, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame not found", (ftnlen)25);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EART", "NONE", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)8, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid aberration correction", (ftnlen)29);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "LTT", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/*     Test SAVE logic by repeating the call. */

    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "LTT", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Relativistic aberration correction", (ftnlen)34);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "RL", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)2, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stellar aberration correction w/o light time", (ftnlen)44);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "S", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)1, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded SPK files", (ftnlen)19);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "LT+S", "sun", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)3);
    chckxc_(&c_true, "SPICE(NOLOADEDFILES)", ok, (ftnlen)20);
    spklef_("subpnt_spk.bsp", handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spklef_("orbiter.bsp", &handle[1], (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for observer", (ftnlen)30);
    subpnt_("INTERCEPT: ELLIPSOID", "EARTH", &et, "IAU_EARTH", "LT+S", "1000",
	     spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No ephemeris data for target", (ftnlen)28);
    subpnt_("INTERCEPT: ELLIPSOID", "gaspra", &et, "IAU_GASPRA", "LT+S", 
	    "10", spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)6, (ftnlen)10, 
	    (ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No orientation data for target", (ftnlen)30);
    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_("INTERCEPT: ELLIPSOID", "earth", &et, "IAU_EARTH", "LT+S", "10", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No radius data for target", (ftnlen)25);
    dvpool_("BODY399_RADII", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_("INTERCEPT: ELLIPSOID", "earth", &et, "IAU_EARTH", "LT+S", "10", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    ldpool_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad radius data for target", (ftnlen)26);

/*     Fetch original radii. */

    bodvcd_(&c__399, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Overwrite good radii with bad in the kernel pool. */

    vpack_(&c_b475, &c_b195, &c_b477, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subpnt_("INTERCEPT: ELLIPSOID", "earth", &et, "IAU_EARTH", "LT+S", "10", 
	    spoint, &trgepc, srfvec, (ftnlen)20, (ftnlen)5, (ftnlen)9, (
	    ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/*     Replace original radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No loaded DSKs.", (ftnlen)15);
    unload_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = jyear_() * 8;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(obsrvr, "EARTH", (ftnlen)32, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)10, (ftnlen)4);
    s_copy(method, "nadir/dsk/unprioritized", (ftnlen)500, (ftnlen)23);
    subpnt_(method, "499", &et, fixref, abcorr, "399", spoint, &trgepc, 
	    srfvec, (ftnlen)500, (ftnlen)3, (ftnlen)32, (ftnlen)10, (ftnlen)3)
	    ;
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Clean up. */

    delfil_("test_0008.tpc", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("subpnt_spk.bsp", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("orbiter.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("subpnt_dsk0.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("subpnt_dsk1.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("subpnt_dsk2.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("subpnt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("subpnt_dsk3.bds", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_subpnt__ */

