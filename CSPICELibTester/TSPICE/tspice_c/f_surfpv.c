/* f_surfpv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 10.;
static integer c__6 = 6;
static integer c__3 = 3;
static logical c_true = TRUE_;
static doublereal c_b12 = -1.;
static doublereal c_b14 = 0.;
static doublereal c_b15 = 1.;
static logical c_false = FALSE_;
static doublereal c_b103 = 1e-10;
static doublereal c_b108 = -290.;
static doublereal c_b109 = 290.;
static doublereal c_b112 = 2.;
static integer c__14 = 14;

/* $Procedure F_SURFPV ( SURFPV tests ) */
/* Subroutine */ int f_surfpv__(logical *ok)
{
    /* Initialized data */

    static char descr[400*8] = "Vertex moves; direction vector has zero velo"
	    "city.                                                           "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                    " "Direction vector scans up"
	    "ward; vertex has zero velocity.                                 "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                       " "No int"
	    "ersection; ray passes above target.                             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "          " "No intersection; ray points away from target.      "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                             " "Velocity not computable: ray sca"
	    "ns upward too fast.                                             "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                " "Velocity not "
	    "computable: vertex moves upward too fast.                       "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "   " "Interior vertex; vertex not at origin.                    "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                      " "Interior vertex; vertex is at origin.  "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                                                "
	    "                                         ";
    static doublereal smpa[8] = { 100.,100.,100.,100.,1.,1.,100.,100. };
    static doublereal smpb[8] = { 100.,200.,200.,200.,2.,2.,200.,200. };
    static doublereal smpc[8] = { 100.,300.,300.,300.,3.,3.,300.,300. };
    static doublereal smpvst[48]	/* was [6][8] */ = { 200.,0.,0.,0.,0.,
	    1.,200.,0.,0.,0.,0.,0.,200.,0.,310.,0.,0.,0.,200.,0.,0.,0.,0.,0.,
	    2.,0.,0.,0.,0.,0.,2.,0.,2.,0.,0.,1.5e308,10.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0. };
    static doublereal smpdst[48]	/* was [6][8] */ = { -1.,0.,0.,0.,0.,
	    0.,-1.,0.,0.,0.,0.,1.,-1.,0.,0.,0.,0.,1.,1.,0.,0.,0.,0.,1.,-1.,0.,
	    0.,0.,0.,1e308,-1.,0.,0.,0.,0.,0.,1.,0.,0.,0.,0.,1.,1.,0.,0.,0.,
	    0.,1. };
    static logical smpfnd[8] = { TRUE_,TRUE_,FALSE_,FALSE_,FALSE_,FALSE_,
	    TRUE_,TRUE_ };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double pow_dd(doublereal *, doublereal *), sqrt(doublereal);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer seed;
    static doublereal xstx[6];
    extern /* Subroutine */ int t_surfpv__(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *)
	    ;
    static doublereal a, b, c__;
    static integer i__, j;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     tcase_(char *, ftnlen), vpack_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal level;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[400];
    static doublereal stdir[6];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen), cleard_(integer *, 
	    doublereal *), chckxc_(logical *, char *, logical *, ftnlen), 
	    chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static doublereal sfactr;
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *);
    static logical xfound;
    extern /* Subroutine */ int surfpv_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *)
	    ;
    static doublereal stvrtx[6];
    extern logical odd_(integer *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static doublereal stx[6];

/* $ Abstract */

/*     Exercise the SPICELIB routine SURFPV. */
/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine SURFPV. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 01-MAY-2012 (NJB) */

/*        Loosened "tight" tolerance to 1.e-10. */

/* -    TSPICE Version 1.1.0, 06-JAN-2009 (NJB) */

/*        Bug fix: regrouped terms of computation of */
/*        LEVEL to avoid overflow. */

/* -    TSPICE Version 1.0.0, 30-MAY-2008 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved everything. */


/*     Initial values */


/*     The simple test cases. */


/*     Open the test family. */

    topen_("F_SURFPV", (ftnlen)8);

/*     SURFPV error cases: */


/* --- Case: ------------------------------------------------------ */

    tcase_("SURFPV: zero direction vector,", (ftnlen)30);
    filld_(&c_b4, &c__6, stvrtx);
    cleard_(&c__3, stdir);
    surfpv_(stvrtx, stdir, &a, &b, &c__, stx, &found);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("SURFPV: ellipsoid has one zero-length axis.", (ftnlen)43);
    filld_(&c_b4, &c__6, stvrtx);
    filld_(&c_b12, &c__6, stdir);
    surfpv_(stvrtx, stdir, &c_b14, &c_b15, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    surfpv_(stvrtx, stdir, &c_b15, &c_b14, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    surfpv_(stvrtx, stdir, &c_b15, &c_b15, &c_b14, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SURFPV: ellipsoid has one negative axis.", (ftnlen)40);
    filld_(&c_b4, &c__6, stvrtx);
    filld_(&c_b12, &c__6, stdir);
    surfpv_(stvrtx, stdir, &c_b12, &c_b15, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    surfpv_(stvrtx, stdir, &c_b15, &c_b12, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);
    surfpv_(stvrtx, stdir, &c_b15, &c_b15, &c_b12, stx, &found);
    chckxc_(&c_true, "SPICE(BADAXISLENGTH)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("SURFPV: ray's vertex is on the ellipsoid", (ftnlen)40);
    filld_(&c_b14, &c__3, stvrtx);
    stvrtx[0] = 1.;
    filld_(&c_b15, &c__3, &stvrtx[3]);
    filld_(&c_b12, &c__6, stdir);
    surfpv_(stvrtx, stdir, &c_b15, &c_b15, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(INVALIDVERTEX)", ok, (ftnlen)20);
    t_surfpv__(stvrtx, stdir, &c_b15, &c_b15, &c_b15, stx, &found);
    chckxc_(&c_true, "SPICE(INVALIDVERTEX)", ok, (ftnlen)20);

/*     Run some simple tests where the correct results can be */
/*     determined by inspection. */

    for (i__ = 1; i__ <= 8; ++i__) {

/* --- Case: ------------------------------------------------------ */

	tcase_(descr + ((i__1 = i__ - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge(
		"descr", i__1, "f_surfpv__", (ftnlen)365)) * 400, (ftnlen)400)
		;
	surfpv_(&smpvst[(i__1 = i__ * 6 - 6) < 48 && 0 <= i__1 ? i__1 : 
		s_rnge("smpvst", i__1, "f_surfpv__", (ftnlen)368)], &smpdst[(
		i__2 = i__ * 6 - 6) < 48 && 0 <= i__2 ? i__2 : s_rnge("smpdst"
		, i__2, "f_surfpv__", (ftnlen)368)], &smpa[(i__3 = i__ - 1) < 
		8 && 0 <= i__3 ? i__3 : s_rnge("smpa", i__3, "f_surfpv__", (
		ftnlen)368)], &smpb[(i__4 = i__ - 1) < 8 && 0 <= i__4 ? i__4 :
		 s_rnge("smpb", i__4, "f_surfpv__", (ftnlen)368)], &smpc[(
		i__5 = i__ - 1) < 8 && 0 <= i__5 ? i__5 : s_rnge("smpc", i__5,
		 "f_surfpv__", (ftnlen)368)], stx, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND (0)", &found, &smpfnd[(i__1 = i__ - 1) < 8 && 0 <= 
		i__1 ? i__1 : s_rnge("smpfnd", i__1, "f_surfpv__", (ftnlen)
		373)], ok, (ftnlen)9);
/*         IF ( FOUND ) THEN */
/*            WRITE (*,*) 'STX from SURFPV: ', STX */
/*         END IF */

/*        Repeat the computation with T_SURFPV. */

	t_surfpv__(&smpvst[(i__1 = i__ * 6 - 6) < 48 && 0 <= i__1 ? i__1 : 
		s_rnge("smpvst", i__1, "f_surfpv__", (ftnlen)382)], &smpdst[(
		i__2 = i__ * 6 - 6) < 48 && 0 <= i__2 ? i__2 : s_rnge("smpdst"
		, i__2, "f_surfpv__", (ftnlen)382)], &smpa[(i__3 = i__ - 1) < 
		8 && 0 <= i__3 ? i__3 : s_rnge("smpa", i__3, "f_surfpv__", (
		ftnlen)382)], &smpb[(i__4 = i__ - 1) < 8 && 0 <= i__4 ? i__4 :
		 s_rnge("smpb", i__4, "f_surfpv__", (ftnlen)382)], &smpc[(
		i__5 = i__ - 1) < 8 && 0 <= i__5 ? i__5 : s_rnge("smpc", i__5,
		 "f_surfpv__", (ftnlen)382)], xstx, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND (1)", &found, &smpfnd[(i__1 = i__ - 1) < 8 && 0 <= 
		i__1 ? i__1 : s_rnge("smpfnd", i__1, "f_surfpv__", (ftnlen)
		387)], ok, (ftnlen)9);
	if (smpfnd[(i__1 = i__ - 1) < 8 && 0 <= i__1 ? i__1 : s_rnge("smpfnd",
		 i__1, "f_surfpv__", (ftnlen)389)]) {

/*           Check the intercept state against that found */
/*           by T_SURFPV. */

	    chckad_("STX(1:3)", stx, "~~/", xstx, &c__3, &c_b103, ok, (ftnlen)
		    8, (ftnlen)3);
	    chckad_("STX(4:6)", &stx[3], "~~/", &xstx[3], &c__3, &c_b103, ok, 
		    (ftnlen)8, (ftnlen)3);
	}
    }

/*     Now for some more difficult cases.  We'll generate the ellipsoids */
/*     and lines using random numbers.  There are 16 components to */
/*     generate: */

/*        - random ray vertices and vertex velocities */
/*        - random ray directions and direction velocities */
/*        - random ellipsoid axis lengths */
/*        - random scale factors for the ellipsoid and ray */

    seed = -1;
    for (i__ = 1; i__ <= 5000; ++i__) {

/* --- Case: ------------------------------------------------------ */


/*        Get a scale factor. */

	d__1 = t_randd__(&c_b108, &c_b109, &seed);
	sfactr = pow_dd(&c_b4, &d__1);

/*        Make up ellipsoid axis lengths. */

	a = sfactr * t_randd__(&c_b15, &c_b112, &seed);
	b = sfactr * t_randd__(&c_b15, &c_b112, &seed);
	c__ = sfactr * t_randd__(&c_b15, &c_b112, &seed);

/*        We gotta have a ray vertex and vertex velocity. */

	for (j = 1; j <= 6; ++j) {
	    stvrtx[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("stvrtx", 
		    i__1, "f_surfpv__", (ftnlen)442)] = sfactr * 10 * 
		    t_randd__(&c_b12, &c_b15, &seed);
	}

/*        Compute the level surface parameter of the vertex. */

/* Computing 2nd power */
	d__1 = stvrtx[0] / a;
/* Computing 2nd power */
	d__2 = stvrtx[1] / b;
/* Computing 2nd power */
	d__3 = stvrtx[2] / c__;
	level = d__1 * d__1 + d__2 * d__2 + d__3 * d__3;

/*        Alternate between exterior and interior cases. */

	if (odd_(&i__)) {

/*           If necessary, scale up the vertex to take it outside the */
/*           ellipsoid. */

	    if (level < 1.) {
		if (level > 0.) {
		    d__1 = 2. / sqrt(level);
		    vsclip_(&d__1, stvrtx);
		} else {

/*                 It's unlikely that we get here: the vertex is at */
/*                 the origin. Pick an exterior vertex. */

		    d__1 = a * 2.;
		    d__2 = b * 2.;
		    d__3 = c__ * 2.;
		    vpack_(&d__1, &d__2, &d__3, stvrtx);
		}
	    }
	} else {

/*           Ensure the vertex is inside the ellipsoid. */

	    if (level >= 1.) {
		d__1 = .5 / sqrt(level);
		vsclip_(&d__1, stvrtx);
	    }
	}

/*        Generate the ray's direction vector by perturbing the */
/*        inverse of the ray's vertex. */

	for (j = 1; j <= 3; ++j) {
	    stdir[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("stdir", 
		    i__1, "f_surfpv__", (ftnlen)493)] = -stvrtx[(i__2 = j - 1)
		     < 6 && 0 <= i__2 ? i__2 : s_rnge("stvrtx", i__2, "f_sur"
		    "fpv__", (ftnlen)493)] + sfactr * t_randd__(&c_b12, &c_b15,
		     &seed);
	}
	for (j = 4; j <= 6; ++j) {
	    stdir[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("stdir", 
		    i__1, "f_surfpv__", (ftnlen)498)] = sfactr * t_randd__(&
		    c_b12, &c_b15, &seed);
	}
	s_copy(title, "SURFPV Random case #.  A, B, C = # # #; STVRTX = (#, "
		"#, #, #, #, #) STDIR  = (#, #, #, #, #, #)", (ftnlen)400, (
		ftnlen)95);
	repmi_(title, "#", &i__, title, (ftnlen)400, (ftnlen)1, (ftnlen)400);
	repmd_(title, "#", &a, &c__14, title, (ftnlen)400, (ftnlen)1, (ftnlen)
		400);
	repmd_(title, "#", &b, &c__14, title, (ftnlen)400, (ftnlen)1, (ftnlen)
		400);
	repmd_(title, "#", &c__, &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", stvrtx, &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stvrtx[1], &c__14, title, (ftnlen)400, (ftnlen)1, 
		(ftnlen)400);
	repmd_(title, "#", &stvrtx[2], &c__14, title, (ftnlen)400, (ftnlen)1, 
		(ftnlen)400);
	repmd_(title, "#", &stvrtx[3], &c__14, title, (ftnlen)400, (ftnlen)1, 
		(ftnlen)400);
	repmd_(title, "#", &stvrtx[4], &c__14, title, (ftnlen)400, (ftnlen)1, 
		(ftnlen)400);
	repmd_(title, "#", &stvrtx[5], &c__14, title, (ftnlen)400, (ftnlen)1, 
		(ftnlen)400);
	repmd_(title, "#", stdir, &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stdir[1], &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stdir[2], &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stdir[3], &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stdir[4], &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	repmd_(title, "#", &stdir[5], &c__14, title, (ftnlen)400, (ftnlen)1, (
		ftnlen)400);
	tcase_(title, (ftnlen)400);

/*        Cross our fingers and toes and let 'er rip. */

	surfpv_(stvrtx, stdir, &a, &b, &c__, stx, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_surfpv__(stvrtx, stdir, &a, &b, &c__, xstx, &xfound);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND (0)", &found, &xfound, ok, (ftnlen)9);
	if (xfound) {
/*           Check the intercept state against that found */
/*           by T_SURFPV. */

	    chckad_("STX(1:3)", stx, "~~/", xstx, &c__3, &c_b103, ok, (ftnlen)
		    8, (ftnlen)3);
	    chckad_("STX(4:6)", &stx[3], "~~/", &xstx[3], &c__3, &c_b103, ok, 
		    (ftnlen)8, (ftnlen)3);
/*            WRITE (*,*) '-----------------' */
/*            WRITE (*,*) 'Case ', I */
/*            WRITE (*,*) 'STX:  ', STX */
/*            WRITE (*,*) 'XSTX: ', XSTX */
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_surfpv__ */

