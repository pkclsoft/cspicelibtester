/*

-Procedure f_surfpv_c ( Test wrapper for surfpv_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper for the surfpv_c, which finds
   the nearest point on an ellipsoid to a specified line.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_surfpv_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrapper for the CSPICE routine surfpv_c.
             
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 2.0.0 10-OCT-2013 (NJB)  

      Loosened value of TIGHT tolerance to 1.e-10.

   -tspice_c Version 1.0.0 09-MAR-2009 (NJB)  

-Index_Entries

   test surfpv_c

-&
*/

{ /* Begin f_surfpv_c */

 
   /*
   Prototypes 
   */


   /*
   Constants
   */
   #define TIGHT         ( 1.e-10 )
   #define LINELN          81
   #define MSGLEN          400
   #define NUMSMP          3
   #define NUMTOL          3
   #define NRANDM          5000
   
   /*
   Local variables
   */
   logical                 xfound;
   integer                 seed;

   SpiceBoolean            found;

   SpiceChar               title  [ MSGLEN ];

   SpiceDouble             a;
   SpiceDouble             b;
   SpiceDouble             c;
   
   SpiceDouble             lb;
   SpiceDouble             level;
   SpiceDouble             sfactr;
   SpiceDouble             stdir  [6];
   SpiceDouble             stvrtx [6];
   SpiceDouble             stx    [6];
   SpiceDouble             ub;
   SpiceDouble             xstx   [6];
   SpiceDouble             z;


   SpiceInt                i;
   SpiceInt                j;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_surfpv_c" );

     
   /*
   Execute the random cases from F_SURFPV. 
   */

   seed = -1;
    
   for ( i = 0;  i < NRANDM;  i++ )
   {
      /*
      --- Case: ------------------------------------------------------
      */

      /*
      Get a scale factor.
      */
      
      lb     = -290.0;
      ub     =  290.0;
      z      =  t_randd__ ( &lb, &ub, &seed );
      sfactr =  pow ( 10.0, z );
 
      /*
      Make up ellipsoid axis lengths.
      */
      
      lb     = 1.0;
      ub     = 2.0;

      a      =  sfactr  *  t_randd__ ( &lb, &ub, &seed );
      b      =  sfactr  *  t_randd__ ( &lb, &ub, &seed );
      c      =  sfactr  *  t_randd__ ( &lb, &ub, &seed );
 
      /*
      We gotta have a ray vertex and vertex velocity.
      */
      lb     = -1.0;
      ub     =  1.0;
      
      for ( j = 0;  j < 6;  j++ )
      {
         z         =  t_randd__ ( &lb, &ub, &seed );
         stvrtx[j] =  10.0 * sfactr  * z;

      }
 
      /*
      Compute the level surface parameter of the vertex.
      */
      level =    pow( stvrtx[0]/a, 2 )
               + pow( stvrtx[1]/b, 2 )
               + pow( stvrtx[2]/c, 2 );

      /*
      Alternate between exterior and interior cases.

      Since we're using zero-based indexing, we use even_
      rather than odd_ here, for consistency with TSPICE.
      */
      if ( even_( &i ) )
      {
         /*
         If necessary, scale up the vertex to take it outside the
         ellipsoid.
         */
         if ( level < 1.0 )
         {
            if ( level > 0.0 )
            {
               vscl_c ( 2.0/sqrt(level), stvrtx, stvrtx );
            }
            else
            {
               /*
               It's unlikely that we get here: the vertex is at
               the origin. Pick an exterior vertex.
               */
               vpack_c ( 2.0*a, 2.0*b, 2.0*c, stvrtx );
            }
         }
      }
      else
      {
         /*
         Ensure the vertex is inside the ellipsoid.
         */
         if ( level >= 1.0 )
         {
            vscl_c ( 0.5/sqrt(level), stvrtx, stvrtx );
         }
      }


      /*
      Generate the ray's direction vector by perturbing the
      inverse of the ray's vertex.
      */
      lb =  -1.0;
      ub =   1.0;

      for ( j = 0;  j < 3;  j++ )
      {
         stdir[j] =  -  stvrtx[j]
                     +  sfactr  * t_randd__ ( &lb, &ub, &seed );
      }

      for ( j = 3;  j < 6;  j++ )
      {
         stdir[j] =     sfactr  * t_randd__ ( &lb, &ub, &seed );
      }

      title[0] = NULLCHAR;

      strncpy ( title, 
                "SURFPV Random case #.  A, B, C = # # #; "
                "STVRTX = (#, #, #, #, #, #) "
                "STDIR  = (#, #, #, #, #, #)",
                MSGLEN                                     );

      repmi_c ( title, "#", i+1,           MSGLEN, title );
      repmd_c ( title, "#", a,         14, MSGLEN, title );
      repmd_c ( title, "#", b,         14, MSGLEN, title );
      repmd_c ( title, "#", c,         14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[0], 14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[1], 14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[2], 14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[3], 14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[4], 14, MSGLEN, title );
      repmd_c ( title, "#", stvrtx[5], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [0], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [1], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [2], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [3], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [4], 14, MSGLEN, title );
      repmd_c ( title, "#", stdir [5], 14, MSGLEN, title );

      tcase_c ( title );
      

      /*
      Cross our fingers and toes and let 'er rip.    
      */       
      surfpv_c ( stvrtx, stdir,  a, b, c, stx, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      /*
      printf ( "found = %ld \n", (long)found  );
      */


      t_surfpv__ ( stvrtx, stdir,  &a, &b, &c, xstx, &xfound );
      chckxc_c ( SPICEFALSE, " ", ok );

      
      chcksl_c ( "found (0)", found, (SpiceBoolean)xfound, ok );
      

      if ( (SpiceBoolean)xfound == found )
      {
         /*
         Check the intercept state against that found
         by t_surfpv__.
         */
         chckad_c ( "stx(0:2)", stx,   "~~/", xstx,   3, TIGHT, ok );

         chckad_c ( "stx(3:5)", stx+3, "~~/", xstx+3, 3, TIGHT, ok );
      }
      else
      {
         /*
         printf ( "found = %ld; xfound = %ld\n", (long)found, (long)xfound  );


         printf ( "Case: %ld\n", i );
         printf ( "stx: %e %e %e %e %e %e \n", 
                  stx[0],
                  stx[1],
                  stx[2],
                  stx[3],
                  stx[4],
                  stx[5]                       );

         printf ( "xstx: %e %e %e %e %e %e \n", 
                  xstx[0],
                  xstx[1],
                  xstx[2],
                  xstx[3],
                  xstx[4],
                  xstx[5]                       );

         */
      }

   }
      
      
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_surfpv_c */


