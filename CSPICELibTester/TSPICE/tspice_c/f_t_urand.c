/* f_t_urand.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;

/* $Procedure F_T_URAND ( T_URAND tests ) */
/* Subroutine */ int f_t_urand__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    char name__[80];
    integer seed, seed2, i__, n;
    doublereal u;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmi_(char *, char *,
	     integer *, char *, ftnlen, ftnlen, ftnlen);
    char title[800];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal u2;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, doublereal *, char *, doublereal *, integer *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal t_urand__(integer *), t_urand2__(integer *);

/* $ Abstract */

/*     Exercise T_URAND, the SPICE test utility routine that generates */
/*     uniformly distributed random numbers between 0 and 1. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the test utility routine T_URAND. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 17-OCT-2011 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_T_URAND", (ftnlen)9);

/* --- Case: ------------------------------------------------------ */


    s_copy(title, "Generate a series of random numbers, starting with an ini"
	    "tializing seed.", (ftnlen)800, (ftnlen)72);
    tcase_(title, (ftnlen)800);
    n = 100000;
    seed = -1;
    seed2 = -1;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	u = t_urand__(&seed);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	u2 = t_urand2__(&seed2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(name__, "Case 1 iteration #.", (ftnlen)80, (ftnlen)19);
	repmi_(name__, "#", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(name__, &u, "=", &u2, &c__0, ok, (ftnlen)80, (ftnlen)1);
/*         IF ( U .LT. 1.D-3 ) THEN */
/*            WRITE (*,*) 'U  = ', U */
/*            WRITE (*,*) 'U2 = ', U2 */
/*            WRITE (*,*) ' ' */
/*         END IF */
    }
    return 0;
} /* f_t_urand__ */

