/* f_tabtxt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__21 = 21;
static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__0 = 0;
static doublereal c_b73 = 0.;

/* $Procedure F_TABTXT ( Text kernel tab processing tests ) */
/* Subroutine */ int f_tabtxt__(logical *ok)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2, i__3, i__4;
    char ch__1[20];
    cllist cl__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);
    integer s_rnge(char *, integer, char *, integer), f_clos(cllist *);

    /* Local variables */
    integer unit, i__;
    extern integer cardc_(char *, ftnlen);
    integer n;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    logical found;
    extern /* Subroutine */ int copyc_(char *, char *, ftnlen, ftnlen);
    char xcvar[60*3];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckac_(char *, char *, char *, char *, integer *, logical *, 
	    ftnlen, ftnlen, ftnlen, ftnlen), chckad_(char *, doublereal *, 
	    char *, doublereal *, integer *, doublereal *, logical *, ftnlen, 
	    ftnlen);
    extern /* Character */ VOID begdat_(char *, ftnlen);
    extern /* Subroutine */ int scardc_(integer *, char *, ftnlen), delfil_(
	    char *, ftnlen);
    char tabbuf[60*27];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    char filbuf[60*27];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), replch_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), gcpool_(char *, integer *, integer *, 
	    integer *, char *, logical *, ftnlen, ftnlen), gdpool_(char *, 
	    integer *, integer *, integer *, doublereal *, logical *, ftnlen),
	     clpool_(void), ldpool_(char *, ftnlen);
    extern /* Character */ VOID begtxt_(char *, ftnlen);
    extern /* Subroutine */ int ssizec_(integer *, char *, ftnlen);
    doublereal xdpvar[3];
    extern /* Subroutine */ int cmprss_(char *, integer *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), writln_(char *, integer *, ftnlen);
    char kv1[60*3];
    doublereal kv2[3], kv3[3];
    extern /* Subroutine */ int txtopn_(char *, integer *, ftnlen);

/* $ Abstract */

/*     Exercise tab processing in entry points of RDKER. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     This file creates both normal and a tab-filled text */
/*     kernels for testing. Both are deleted at the end */
/*     of execution. */

/* $ Particulars */

/*     This routine tests the ability of the RDKER entry points */

/*        RDKNEW */
/*        RDKDAT */

/*     to process kernels where either blanks or tab characters */
/*     are used, in any combination, as white space. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 21-FEB-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Non-SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Open the test family. */

    topen_("F_TABTXT", (ftnlen)8);

/* --- Case: ------------------------------------------------------ */

    tcase_("Init: create a normal text kernel file.", (ftnlen)39);

/*     Rather than create a text kernel using an existing utility */
/*     routine, we create the file in-line so that we can ensure */
/*     the file has an instance of each structural feature. */

    ssizec_(&c__21, filbuf, (ftnlen)60);
    s_copy(filbuf + 360, " ", (ftnlen)60, (ftnlen)1);
    s_copy(filbuf + 420, " KPL/PCK", (ftnlen)60, (ftnlen)8);
    s_copy(filbuf + 480, " ", (ftnlen)60, (ftnlen)1);
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = " ";
    begdat_(ch__1, (ftnlen)20);
    i__1[1] = 20, a__1[1] = ch__1;
    s_cat(filbuf + 540, a__1, i__1, &c__2, (ftnlen)60);
    s_copy(filbuf + 600, " ", (ftnlen)60, (ftnlen)1);
    s_copy(filbuf + 660, " KV1 = ( ' A ', ' B ', ' C ' )", (ftnlen)60, (
	    ftnlen)30);
    s_copy(filbuf + 720, " KV2 = ( 1.0, 2.0, 3.0 )", (ftnlen)60, (ftnlen)24);
    s_copy(filbuf + 780, " ", (ftnlen)60, (ftnlen)1);
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = " ";
    begtxt_(ch__1, (ftnlen)20);
    i__1[1] = 20, a__1[1] = ch__1;
    s_cat(filbuf + 840, a__1, i__1, &c__2, (ftnlen)60);
    s_copy(filbuf + 900, " ", (ftnlen)60, (ftnlen)1);
    s_copy(filbuf + 960, " Comment line 1", (ftnlen)60, (ftnlen)15);
    s_copy(filbuf + 1020, " Comment line 2", (ftnlen)60, (ftnlen)15);
    s_copy(filbuf + 1080, " Comment line 3", (ftnlen)60, (ftnlen)15);
    s_copy(filbuf + 1140, " ", (ftnlen)60, (ftnlen)1);
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = " ";
    begdat_(ch__1, (ftnlen)20);
    i__1[1] = 20, a__1[1] = ch__1;
    s_cat(filbuf + 1200, a__1, i__1, &c__2, (ftnlen)60);
    s_copy(filbuf + 1260, " KV3 = ( -1.0, -2.0, ", (ftnlen)60, (ftnlen)21);
    s_copy(filbuf + 1320, " ", (ftnlen)60, (ftnlen)1);
    s_copy(filbuf + 1380, " -3.0 )", (ftnlen)60, (ftnlen)7);
    s_copy(filbuf + 1440, " ", (ftnlen)60, (ftnlen)1);
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = " ";
    begtxt_(ch__1, (ftnlen)20);
    i__1[1] = 20, a__1[1] = ch__1;
    s_cat(filbuf + 1500, a__1, i__1, &c__2, (ftnlen)60);
    s_copy(filbuf + 1560, " ", (ftnlen)60, (ftnlen)1);
    scardc_(&c__21, filbuf, (ftnlen)60);

/*     Open and write the file. */

    txtopn_("test.ker", &unit, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	i__ = 1;
	while(*ok && i__ <= cardc_(filbuf, (ftnlen)60)) {
	    writln_(filbuf + ((i__2 = i__ + 5) < 27 && 0 <= i__2 ? i__2 : 
		    s_rnge("filbuf", i__2, "f_tabtxt__", (ftnlen)213)) * 60, &
		    unit, (ftnlen)60);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ++i__;
	}
    }
    cl__1.cerr = 0;
    cl__1.cunit = unit;
    cl__1.csta = 0;
    f_clos(&cl__1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Control case: load normal file and check.", (ftnlen)41);
    ldpool_("test.ker", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the string variable KV1. */

    gcpool_("KV1", &c__1, &c__3, &n, kv1, &found, (ftnlen)3, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	s_copy(xcvar, " A", (ftnlen)60, (ftnlen)2);
	s_copy(xcvar + 60, " B", (ftnlen)60, (ftnlen)2);
	s_copy(xcvar + 120, " C", (ftnlen)60, (ftnlen)2);
	chckac_("KV1", kv1, "=", xcvar, &c__3, ok, (ftnlen)3, (ftnlen)60, (
		ftnlen)1, (ftnlen)60);
    }

/*     Check the d.p. variable KV2. */

    gdpool_("KV2", &c__1, &c__3, &n, kv2, &found, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	xdpvar[0] = 1.;
	xdpvar[1] = 2.;
	xdpvar[2] = 3.;
	chckad_("KV2", kv2, "=", xdpvar, &c__3, &c_b73, ok, (ftnlen)3, (
		ftnlen)1);
    }

/*     Check the d.p. variable KV3. */

    gdpool_("KV3", &c__1, &c__3, &n, kv3, &found, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	xdpvar[0] = -1.;
	xdpvar[1] = -2.;
	xdpvar[2] = -3.;
	chckad_("KV3", kv3, "=", xdpvar, &c__3, &c_b73, ok, (ftnlen)3, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Money case: load tab-ridden file and check.", (ftnlen)43);

/*     TABBUF will contain the same string data as FILBUF, except */
/*     that every blank in FILBUF is replaced by a tab character. */

    ssizec_(&c__21, tabbuf, (ftnlen)60);
    copyc_(filbuf, tabbuf, (ftnlen)60, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__2 = cardc_(tabbuf, (ftnlen)60);
    for (i__ = 1; i__ <= i__2; ++i__) {
	replch_(tabbuf + ((i__3 = i__ + 5) < 27 && 0 <= i__3 ? i__3 : s_rnge(
		"tabbuf", i__3, "f_tabtxt__", (ftnlen)312)) * 60, " ", "\t", 
		tabbuf + ((i__4 = i__ + 5) < 27 && 0 <= i__4 ? i__4 : s_rnge(
		"tabbuf", i__4, "f_tabtxt__", (ftnlen)312)) * 60, (ftnlen)60, 
		(ftnlen)1, (ftnlen)1, (ftnlen)60);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        The following lines of code, which are intentionally commented */
/*        out, can be used to verify that tab characters have been */
/*        inserted where expected. This can be useful because */
/*        applications one might use to view the file we're creating */
/*        don't necessarily display all tab characters as a fixed set of */
/*        consecutive blank spaces. For example, the programs cat, more, */
/*        vi, and emacs, at least as configured on the author's */
/*        computer, all show a single blank space between the words */
/*        "Comment" and "line", even though a tab character is actually */
/*        present between them, while other tabs are shown as multiple */
/*        consecutive blanks. */

/*         CALL REPLCH ( TABBUF(I), CHAR(TABCDE), '@', TABBUF(I) ) */
/*         WRITE (*,*) TABBUF(I) */
/*         CALL REPLCH ( TABBUF(I), '@', CHAR(TABCDE), TABBUF(I) ) */


/*        The output created by these lines is shown below. */

/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @KPL/PCK@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @\begindata@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @KV1@=@(@'@A@',@'@B@',@'@C@'@)@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @KV2@=@(@1.0,@2.0,@3.0@)@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @\begintext@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @Comment@line@1@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @Comment@line@2@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @Comment@line@3@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @\begindata@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @KV3@=@(@-1.0,@-2.0,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @-3.0@)@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @\begintext@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

    }

/*     Empty the kernel pool. Delete the normal kernel. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test.ker", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open and write the new file containing tabs. */

    txtopn_("test.ker", &unit, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (*ok) {
	i__ = 1;
	while(*ok && i__ <= cardc_(tabbuf, (ftnlen)60)) {
	    writln_(tabbuf + ((i__2 = i__ + 5) < 27 && 0 <= i__2 ? i__2 : 
		    s_rnge("tabbuf", i__2, "f_tabtxt__", (ftnlen)382)) * 60, &
		    unit, (ftnlen)60);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ++i__;
	}
    }
    cl__1.cerr = 0;
    cl__1.cunit = unit;
    cl__1.csta = 0;
    f_clos(&cl__1);

/*     Load the new kernel. We don't expect to hit any */
/*     SPICE errors, even though the file contains tabs. */

    ldpool_("test.ker", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the string variable KV1. */

    gcpool_("KV1", &c__1, &c__3, &n, kv1, &found, (ftnlen)3, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	s_copy(xcvar, " A", (ftnlen)60, (ftnlen)2);
	s_copy(xcvar + 60, " B", (ftnlen)60, (ftnlen)2);
	s_copy(xcvar + 120, " C", (ftnlen)60, (ftnlen)2);

/*        On some systems, each tab in an input text line will be */
/*        replaced by one or more blanks on input, before RDKER has a */
/*        chance to process the line. This can cause the values */
/*        of the string variables to have extra leading blanks. */
/*        Compress out any such blanks. */

	for (i__ = 1; i__ <= 3; ++i__) {
	    cmprss_(" ", &c__1, kv1 + ((i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("kv1", i__2, "f_tabtxt__", (ftnlen)424)) * 
		    60, kv1 + ((i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : 
		    s_rnge("kv1", i__3, "f_tabtxt__", (ftnlen)424)) * 60, (
		    ftnlen)1, (ftnlen)60, (ftnlen)60);
	}
	chckac_("KV1", kv1, "=", xcvar, &c__3, ok, (ftnlen)3, (ftnlen)60, (
		ftnlen)1, (ftnlen)60);
    }

/*     Check the d.p. variable KV2. */

    gdpool_("KV2", &c__1, &c__3, &n, kv2, &found, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	xdpvar[0] = 1.;
	xdpvar[1] = 2.;
	xdpvar[2] = 3.;
	chckad_("KV2", kv2, "=", xdpvar, &c__3, &c_b73, ok, (ftnlen)3, (
		ftnlen)1);
    }

/*     Check the d.p. variable KV3. */

    gdpool_("KV3", &c__1, &c__3, &n, kv3, &found, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);
	xdpvar[0] = -1.;
	xdpvar[1] = -2.;
	xdpvar[2] = -3.;
	chckad_("KV3", kv3, "=", xdpvar, &c__3, &c_b73, ok, (ftnlen)3, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up: delete text kernel file.", (ftnlen)34);
    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("test.ker", (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_tabtxt__ */

