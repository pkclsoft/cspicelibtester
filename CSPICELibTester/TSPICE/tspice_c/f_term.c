/* f_term.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static integer c__1 = 1;
static doublereal c_b19 = 1e-13;
static logical c_true = TRUE_;
static doublereal c_b134 = 3.;
static doublereal c_b136 = 2.;
static doublereal c_b137 = 1.;
static doublereal c_b139 = 1.5;
static integer c__0 = 0;
static integer c_n1 = -1;
static doublereal c_b352 = 1e6;
static doublereal c_b379 = 1e-15;

/* $Procedure      F_TERM ( Test terminator routines ) */
/* Subroutine */ int f_term__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    integer npts;
    extern /* Subroutine */ int zzedterm_(char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *,
	     ftnlen);
    doublereal a, b, c__, d__;
    integer i__, n;
    doublereal r__, radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal xdiff;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    doublereal xgrid[3000]	/* was [3][1000] */;
    char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal trans[9]	/* was [3][3] */;
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *);
    doublereal badrad[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    doublereal et;
    integer handle;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    char abcorr[10];
    doublereal londif, sradii[3];
    extern /* Subroutine */ int reclat_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), edterm_(char *, char *, char *, 
	    doublereal *, char *, char *, char *, integer *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), bodvrd_(char *, char *, integer *, integer *, 
	    doublereal *, ftnlen, ftnlen);
    char fixref[32];
    doublereal trgepc;
    logical umbral;
    char target[36];
    doublereal radius, gridpt[3];
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), spkuef_(integer *);
    doublereal trmgrd[3000]	/* was [3][1000] */;
    char source[36];
    doublereal obspos[3];
    extern /* Subroutine */ int twovec_(doublereal *, integer *, doublereal *,
	     integer *, doublereal *), tstpck_(char *, logical *, logical *, 
	    ftnlen);
    doublereal srcpos[3];
    char obsrvr[36];
    doublereal trgpos[3], xobsps[3], prvlon;
    extern /* Subroutine */ int spkpos_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), vminus_(doublereal *, doublereal *), tstspk_(char *, 
	    logical *, integer *, ftnlen);
    doublereal lat, lon;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    , t_eterm__(char *, logical *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *,
	     ftnlen);

/* $ Abstract */

/*     Test the SPICELIB terminator routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     Currently this test suite is limited to routines that */
/*     deal with ellipsoids. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 07-MAY-2012 (NJB) */

/*        Added test cases for EDTERM. Updated header */
/*        to conform to standard format. */

/* -    TSPICE Version 1.0.0, 31-JAN-2007 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */

/*      DOUBLE PRECISION      XFORM  ( 3, 3 ) */

/*     Begin every test family with an open call. */

    topen_("F_TERM", (ftnlen)6);
/* ****************************************************************** */

/*     ZZEDTERM tests */

/* ****************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Spherical target, D/R = 2, on z-axis, umbral case", (
	    ftnlen)59);
    d__ = 2e6;
    vpack_(&c_b4, &c_b4, &d__, srcpos);
    a = 1e4;
    b = a;
    c__ = a;
    r__ = d__ / 2 + a;
    npts = 360;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    umbral = TRUE_;
    prvlon = 0.;
    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        Check that the grid point is a tangency point of */
/*        a plane that is also tangent to the light source. */
/*        Also check that the center of the source is on the */
/*        correct side of the plane. */

	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)251)], ok, (ftnlen)80);

/*        Validate spacing of grid points:  longitude deltas */
/*        should be constant.  To avoid problems with singularities, */
/*        we work in a frame where SRCPOS is on the +Z axis and */
/*        the component of the first grid point orthogonal to the */
/*        +Z axis lies on the +X axis. */

	if (i__ == 1) {
	    twovec_(srcpos, &c__3, trmgrd, &c__1, trans);
	}
	mxv_(trans, &trmgrd[(i__2 = i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : 
		s_rnge("trmgrd", i__2, "f_term__", (ftnlen)267)], gridpt);
	reclat_(gridpt, &radius, &lon, &lat);
	if (lon < 0.) {
	    lon += twopi_();
	}
	if (i__ == 2) {

/*           We don't know the expected sign of the difference, but we */
/*           know the expected magnitude. */

	    xdiff = lon - prvlon;
	    if (xdiff > pi_()) {
		xdiff -= twopi_();
	    }
	    d__1 = abs(xdiff);
	    d__2 = twopi_() / npts;
	    chcksd_("ABS(XDIFF)", &d__1, "~", &d__2, &c_b19, ok, (ftnlen)10, (
		    ftnlen)1);
	} else if (i__ > 2) {
	    londif = lon - prvlon;
	    chcksd_("LONDIF", &londif, "~", &xdiff, &c_b19, ok, (ftnlen)6, (
		    ftnlen)1);
	}
/*         WRITE (*,*) RADIUS,LON*DPR(), LAT*DPR() */
	prvlon = lon;
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Spherical target, D/R = 2, on z-axis, penumbral case", (
	    ftnlen)62);
    d__ = 2e6;
    vpack_(&c_b4, &c_b4, &d__, srcpos);
    a = 1e4;
    b = a;
    c__ = a;
    r__ = d__ / 2 + a;
    npts = 6;
    zzedterm_("PENUMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)
	    9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    umbral = FALSE_;
    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)339)], ok, (ftnlen)80);
	reclat_(&trmgrd[(i__2 = i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : 
		s_rnge("trmgrd", i__2, "f_term__", (ftnlen)342)], &radius, &
		lon, &lat);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Slightly prolate target, D/R = 2, on z-axis", (ftnlen)
	    53);
    d__ = 2e6;
    vpack_(&c_b4, &c_b4, &d__, srcpos);
    a = 1e4;
    b = a - 1e3;
    c__ = a - 1e3;
    r__ = a + 1e6;
    npts = 1000;
    umbral = TRUE_;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)377)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Geometry comparable to Sun-Earth, umbral case", (ftnlen)
	    55);
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = 6357.;
    r__ = 1e6;
    npts = 360;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    umbral = TRUE_;
    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)417)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Geometry comparable to Sun-Earth, penumbral case", (
	    ftnlen)58);
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = 6357.;
    r__ = 1e6;
    npts = 360;
    umbral = FALSE_;
    zzedterm_("PENUMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)
	    9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)455)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Source and target have equal size, umbral case", (
	    ftnlen)56);
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = a;
    r__ = a;
    npts = 360;
    umbral = TRUE_;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)492)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Source and target have equal size, penumbral case", (
	    ftnlen)59);
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = a;
    r__ = a;
    npts = 360;
    umbral = FALSE_;
    zzedterm_("PENUMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)
	    9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)529)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Source is smaller than target, umbral case", (ftnlen)52)
	    ;
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = a;
    r__ = a / 2;
    npts = 360;
    umbral = TRUE_;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)566)], ok, (ftnlen)80);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Source is smaller than target, penumbral case", (ftnlen)
	    55);
    d__ = 1.5e8;
    vpack_(&d__, &c_b4, &c_b4, srcpos);
    a = 6378.;
    b = a;
    c__ = a;
    r__ = a / 2;
    npts = 360;
    umbral = TRUE_;
    zzedterm_("UMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure each point found is a terminator point. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(title, "Point *,", (ftnlen)80, (ftnlen)8);
	repmi_(title, "*", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_eterm__(title, &umbral, srcpos, &r__, &a, &b, &c__, &trmgrd[(i__2 = 
		i__ * 3 - 3) < 3000 && 0 <= i__2 ? i__2 : s_rnge("trmgrd", 
		i__2, "f_term__", (ftnlen)604)], ok, (ftnlen)80);
    }


/*     Error handling tests follow. */



/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Bad terminator type.", (ftnlen)30);
    zzedterm_("PNUMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)
	    8);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Bad grid size", (ftnlen)23);
    npts = 0;
    zzedterm_("PENUMBRAL", &a, &b, &c__, &r__, srcpos, &npts, trmgrd, (ftnlen)
	    9);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Bad ellipsoid axis", (ftnlen)28);
    npts = 4;
    zzedterm_("PENUMBRAL", &c_b4, &b, &c__, &r__, srcpos, &npts, trmgrd, (
	    ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedterm_("PENUMBRAL", &a, &c_b4, &c__, &r__, srcpos, &npts, trmgrd, (
	    ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedterm_("PENUMBRAL", &a, &b, &c_b4, &r__, srcpos, &npts, trmgrd, (
	    ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Bad light source radius", (ftnlen)33);
    npts = 4;
    zzedterm_("PENUMBRAL", &a, &b, &c__, &c_b4, srcpos, &npts, trmgrd, (
	    ftnlen)9);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZEDTERM: Light source and target are too close", (ftnlen)47);
    npts = 4;
    vpack_(&c_b4, &c_b4, &c_b134, srcpos);
    zzedterm_("UMBRAL", &c_b136, &c_b137, &c_b137, &c_b139, srcpos, &npts, 
	    trmgrd, (ftnlen)6);
    chckxc_(&c_true, "SPICE(OBJECTSTOOCLOSE)", ok, (ftnlen)22);
/* ****************************************************************** */

/*     EDTERM tests */

/* ****************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: setup: create and load kernels.", (ftnlen)39);

/*     We'll need a generic test PCK and an SPK file. */

    tstpck_("edterm.tpc", &c_true, &c_true, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("edterm.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: observer = moon, target = earth, source = sun. Umbral ca"
	    "se.", (ftnlen)67);
    et = 1e8;
    s_copy(abcorr, "LT+S", (ftnlen)10, (ftnlen)4);
    s_copy(obsrvr, "Moon", (ftnlen)36, (ftnlen)4);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(source, "10", (ftnlen)36, (ftnlen)2);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    npts = 3;
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the observer-target vector and one-way light time */
/*     so we can check OBSPOS and TRGEPC. */

    spkpos_(target, &et, fixref, abcorr, obsrvr, trgpos, &lt, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et - lt;
    chcksd_("TRGEPC", &trgepc, "~/", &d__1, &c_b19, ok, (ftnlen)6, (ftnlen)2);
    vminus_(trgpos, xobsps);
    chckad_("OBSPOS", obspos, "~~/", xobsps, &c__3, &c_b19, ok, (ftnlen)6, (
	    ftnlen)3);

/*     In order the check the terminator points, we'll need */
/*     to express the target-source position in the body-fixed */
/*     frame at TRGEPC. */

    spkpos_(source, &trgepc, fixref, abcorr, target, srcpos, &lt, (ftnlen)36, 
	    (ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the target radii. */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the source radii. */

    bodvrd_(source, "RADII", &c__3, &n, sradii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = sradii[0];

/*     Get the expected terminator grid from ZZEDTERM. We */
/*     rely on ZZEDTERM to work correctly here. */

    zzedterm_("UMBRAL", radii, &radii[1], &radii[2], &r__, srcpos, &npts, 
	    xgrid, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the terminator points. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	chckad_("TRMPTS", &trmgrd[(i__2 = i__ * 3 - 3) < 3000 && 0 <= i__2 ? 
		i__2 : s_rnge("trmgrd", i__2, "f_term__", (ftnlen)765)], 
		"~~/", &xgrid[(i__3 = i__ * 3 - 3) < 3000 && 0 <= i__3 ? i__3 
		: s_rnge("xgrid", i__3, "f_term__", (ftnlen)765)], &c__3, &
		c_b19, ok, (ftnlen)6, (ftnlen)3);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: observer = earth, target = earth, source = sun. Umbral c"
	    "ase.", (ftnlen)68);
    et = 1e8;
    s_copy(abcorr, "LT+S", (ftnlen)10, (ftnlen)4);
    s_copy(obsrvr, "Earth", (ftnlen)36, (ftnlen)5);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(source, "10", (ftnlen)36, (ftnlen)2);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    npts = 3;
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the observer-target vector and one-way light time */
/*     so we can check OBSPOS and TRGEPC. */

    spkpos_(target, &et, fixref, abcorr, obsrvr, trgpos, &lt, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et - lt;
    chcksd_("TRGEPC", &trgepc, "~/", &d__1, &c_b19, ok, (ftnlen)6, (ftnlen)2);
    vminus_(trgpos, xobsps);
    chckad_("OBSPOS", obspos, "~~/", xobsps, &c__3, &c_b19, ok, (ftnlen)6, (
	    ftnlen)3);

/*     In order the check the terminator points, we'll need */
/*     to express the target-source position in the body-fixed */
/*     frame at TRGEPC. */

    spkpos_(source, &trgepc, fixref, abcorr, target, srcpos, &lt, (ftnlen)36, 
	    (ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the target radii. */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the source radii. */

    bodvrd_(source, "RADII", &c__3, &n, sradii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = sradii[0];

/*     Get the expected terminator grid from ZZEDTERM. We */
/*     rely on ZZEDTERM to work correctly here. */

    zzedterm_("UMBRAL", radii, &radii[1], &radii[2], &r__, srcpos, &npts, 
	    xgrid, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the terminator points. */

    i__1 = npts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	chckad_("TRMPTS", &trmgrd[(i__2 = i__ * 3 - 3) < 3000 && 0 <= i__2 ? 
		i__2 : s_rnge("trmgrd", i__2, "f_term__", (ftnlen)840)], 
		"~~/", &xgrid[(i__3 = i__ * 3 - 3) < 3000 && 0 <= i__3 ? i__3 
		: s_rnge("xgrid", i__3, "f_term__", (ftnlen)840)], &c__3, &
		c_b19, ok, (ftnlen)6, (ftnlen)3);
    }


/*     EDTERM error cases: */



/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: No frame name definition.", (ftnlen)33);
    et = 1e8;
    s_copy(abcorr, "LT+S", (ftnlen)10, (ftnlen)4);
    s_copy(obsrvr, "Moon", (ftnlen)36, (ftnlen)4);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(source, "10", (ftnlen)36, (ftnlen)2);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    npts = 3;
    edterm_("UMBRAL", source, target, &et, "X", abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)1, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRANSLATION)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: No target name definition.", (ftnlen)34);
    edterm_("UMBRAL", source, "X", &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)1, (ftnlen)
	    32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTRANSLATION)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: Target frame not centered at target.", (ftnlen)44);
    edterm_("UMBRAL", source, target, &et, "J2000", abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)5, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFIXREF)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: Bad terminator type.", (ftnlen)28);
    edterm_("UMB", source, target, &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)3, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: NPTS is non-positive", (ftnlen)28);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c_n1, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: target semi-axis length non-positive", (ftnlen)44);

/*     Fetch the target radii. */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create bad radii for the target and store these */
/*     in the pool. */

    vequ_(radii, badrad);
    badrad[0] = 0.;
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/*     Restore good radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: source semi-axis length non-positive", (ftnlen)44);

/*     Fetch the source radii. */

    bodvrd_(source, "RADII", &c__3, &n, sradii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create bad radii for the target and store these */
/*     in the pool. */

    vequ_(radii, badrad);
    badrad[0] = 0.;
    pdpool_("BODY10_RADII", &c__3, badrad, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/*     Restore good radii. */

    pdpool_("BODY10_RADII", &c__3, radii, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: target radius count != 3", (ftnlen)32);

/*     Fetch the target radii. */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the radius count to 1. */

    pdpool_("BODY399_RADII", &c__1, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/*     Set the radius count to 4. */

    pdpool_("BODY399_RADII", &c__1, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/*     Restore good radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: source radius count != 3", (ftnlen)32);

/*     Fetch the source radii. */

    bodvrd_(source, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the radius count to 1. */

    pdpool_("BODY10_RADII", &c__1, radii, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/*     Set the radius count to 4. */

    pdpool_("BODY10_RADII", &c__1, radii, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &c__0, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/*     Restore good radii. */

    pdpool_("BODY10_RADII", &c__3, radii, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: source is too close to target", (ftnlen)37);

/*     Fetch the source radii. */

    bodvrd_(source, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the target radii to some awfully large values. */

    vscl_(&c_b352, radii, badrad);
    pdpool_("BODY399_RADII", &c__3, badrad, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(OBJECTSTOOCLOSE)", ok, (ftnlen)22);

/*     Restore good radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: SPK lookup failure.", (ftnlen)27);
    et = 1e9;
    edterm_("UMBRAL", source, target, &et, fixref, abcorr, obsrvr, &npts, &
	    trgepc, obspos, trmgrd, (ftnlen)6, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("EDTERM: clean up: unload and delete kernels.", (ftnlen)44);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("edterm.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_term__ */



/*     Utility functions: */



/*     Check a terminator point on an ellipsoid. */

/* Subroutine */ int t_eterm__(char *title, logical *umbral, doublereal *
	srcpos, doublereal *srcrad, doublereal *a, doublereal *b, doublereal *
	c__, doublereal *x, logical *ok, ftnlen title_len)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    );
    doublereal d__, h__, n[3];
    char label[80];
    doublereal scale, plane[4], nearp[3];
    extern doublereal vdist_(doublereal *, doublereal *), vnorm_(doublereal *)
	    ;
    extern /* Subroutine */ int vprjp_(doublereal *, doublereal *, doublereal 
	    *), nvp2pl_(doublereal *, doublereal *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    doublereal offset[3];
    extern /* Subroutine */ int suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen), surfnm_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Get the ellipsoid's outward normal vector at X. */

    surfnm_(a, b, c__, x, n);

/*     Form the tangent plane at X. */

    nvp2pl_(n, x, plane);

/*     The center of the source should be below the tangent */
/*     plane in the umbral case and above the plane in the */
/*     penumbral case. */

    s_copy(label, title, (ftnlen)80, title_len);
    suffix_("<N,SRCPOS>", &c__1, label, (ftnlen)10, (ftnlen)80);
    vsub_(srcpos, x, offset);
    if (*umbral) {
	d__1 = vdot_(n, offset);
	chcksd_(label, &d__1, "<", &c_b4, &c_b379, ok, (ftnlen)80, (ftnlen)1);
    } else {
	d__1 = vdot_(n, offset);
	chcksd_(label, &d__1, ">", &c_b4, &c_b379, ok, (ftnlen)80, (ftnlen)1);
    }
    if (! (*ok)) {
	return 0;
    }

/*     Find the nearest point on the plane to the center */
/*     of the light source. */

    vprjp_(srcpos, plane, nearp);

/*     Find the plane's height relative to the source's */
/*     surface. */

    h__ = vdist_(nearp, srcpos) - *srcrad;

/*     Determine the scale that should be used for the */
/*     coming relative error computation. */

    d__ = vnorm_(srcpos);
/* Computing MAX */
    d__1 = max(d__,*srcrad), d__1 = max(d__1,*a), d__1 = max(d__1,*b);
    scale = max(d__1,*c__);
    s_copy(label, title, (ftnlen)80, title_len);
    suffix_("ABS(H)/SCALE", &c__1, label, (ftnlen)12, (ftnlen)80);
    d__1 = abs(h__) / scale;
    chcksd_(title, &d__1, "~", &c_b4, &c_b379, ok, title_len, (ftnlen)1);
    return 0;
} /* t_eterm__ */

