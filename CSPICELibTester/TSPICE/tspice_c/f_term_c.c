/*

-Procedure f_term_c ( Test wrappers for terminator routines )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the terminator
   functions. 
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_term_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the ellipsoid terminator routine 

      edterm_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
  
   -tspice_c Version 1.0.0 07-MAY-2012 (NJB)  

-Index_Entries

   test terminator routines

-&
*/

{ /* Begin f_term_c */

 
   /*
   Constants
   */
   #define PCK             "edterm.tpc"
   #define SPK             "edterm.bsp"

   #define MAXPT           1000

   #define TIGHT           1.e-13
      

   /*
   Static variables
   */
 
   /*
   Local variables
   */
   SpiceChar             * abcorr;
   SpiceChar             * fixref;
   SpiceChar             * obsrvr;
   SpiceChar             * source;
   SpiceChar             * target;

   SpiceDouble             et;
   SpiceDouble             lt;
   SpiceDouble             obspos  [3];
   SpiceDouble             r;
   SpiceDouble             radii   [3];
   SpiceDouble             sradii  [3];
   SpiceDouble             srclt;
   SpiceDouble             srcpos  [3];
   SpiceDouble             trgepc;
   SpiceDouble             trgpos  [3];
   SpiceDouble             trmgrid [MAXPT][3];
   SpiceDouble             xobspos [3];
   SpiceDouble             xgrid   [MAXPT][3];

   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                n;
   SpiceInt                npts;



   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_term_c" );
   

   
    
   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: setup: create and load kernels."  );


   /*
   We'll need a generic test PCK and an SPK file.
   */
   tstpck_c ( PCK, SPICETRUE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );
     
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   
   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: observer = moon, target = earth, "
             "source = sun. Umbral case."                );

   et     = 1.e8;

   abcorr = "lt+s";
   obsrvr = "moon";
   target = "399";
   source = "10";
   fixref = "iau_earth";
   npts   = 3;
      
   edterm_c ( "umbral", source, target, et,      
              fixref,   abcorr, obsrvr, npts, 
              &trgepc,  obspos, trmgrid      );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Get the observer-target vector and one-way light time
   so we can check obspos and trgepc.
   */
   spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "trgepc", trgepc, "~/", et-lt, TIGHT, ok );

   vminus_c ( trgpos, xobspos );

   chckad_c ( "obspos", obspos, "~~/", xobspos, 3, TIGHT, ok );

   /*
   In order the check the terminator points, we'll need
   to express the target-source position in the body-fixed
   frame at trgepc.
   */
   spkpos_c ( source, trgepc, fixref, abcorr, target, srcpos, &srclt );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Fetch the target radii.
   */
   bodvrd_c ( target, "RADII", 3, &n, radii );
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Fetch the source radii.
   */
   bodvrd_c ( source, "RADII", 3, &n, sradii );
   chckxc_c ( SPICEFALSE, " ", ok );

   r = sradii[0];
   
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   

   /*
   Get the expected terminator grid from zzedterm_. We
   rely on zzedterm_ to work correctly here.
   */
   zzedterm_ ( (char        *) "umbral",  
               (doublereal  *) radii,   
               (doublereal  *) radii+1,   
               (doublereal  *) radii+2,   
               (doublereal  *) &r,   
               (doublereal  *) srcpos,
               (integer     *) &npts,   
               (doublereal  *) xgrid, 
               (ftnlen       ) strlen("umbral")  );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the terminator points. 
   */
   for ( i = 0;  i < npts;  i++ )
   {
      chckad_c ( "trmpts", trmgrid[i], "~~/", xgrid[i], 3, TIGHT, ok );
   }





   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: observer = moon, target = earth, "
             "source = sun. Penumbral case."                );


   edterm_c ( "penumbral", source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Get the observer-target vector and one-way light time
   so we can check obspos and trgepc.
   */
   spkpos_c ( target, et, fixref, abcorr, obsrvr, trgpos, &lt );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksd_c ( "trgepc", trgepc, "~/", et-lt, TIGHT, ok );

   vminus_c ( trgpos, xobspos );

   chckad_c ( "obspos", obspos, "~~/", xobspos, 3, TIGHT, ok );
 

   /*
   Get the expected terminator grid from zzedterm_. We
   rely on zzedterm_ to work correctly here.
   */
   zzedterm_ ( (char        *) "penumbral",  
               (doublereal  *) radii,   
               (doublereal  *) radii+1,   
               (doublereal  *) radii+2,   
               (doublereal  *) &r,   
               (doublereal  *) srcpos,
               (integer     *) &npts,   
               (doublereal  *) xgrid, 
               (ftnlen       ) strlen("penumbral")  );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Check the terminator points. 
   */
   for ( i = 0;  i < npts;  i++ )
   {
      chckad_c ( "trmpts", trmgrid[i], "~~/", xgrid[i], 3, TIGHT, ok );
   }


   /*
    *
    * edterm_c:  Error cases
    *
   */

   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: insufficient SPK data" );

   /*
   Force at least one error to occur in the underlying f2c'd routine. 
   */
   et = 1.e10;

   edterm_c ( "umbral",    source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(SPKINSUFFDATA)", ok );



   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: null string pointers." );

   edterm_c ( NULL,        source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   edterm_c ( "UMBRAL",    NULL,   target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   edterm_c ( "UMBRAL",    source, NULL,   et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              NULL,        abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      NULL,   obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      abcorr, NULL,   npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );



   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: null output array pointers." );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     NULL,   trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, NULL         );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "edterm_c: empty input strings." );

   edterm_c ( "",          source, target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   edterm_c ( "UMBRAL",    "",     target, et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   edterm_c ( "UMBRAL",    source, "",     et,      
              fixref,      abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              "",          abcorr, obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      "",     obsrvr, npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   edterm_c ( "UMBRAL",    source, target, et,      
              fixref,      abcorr, "",     npts, 
              &trgepc,     obspos, trmgrid      );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );





   /*
   --- Case --------------------------------------------------------
   */
   tcase_c ( "clean up: unload and delete kernels." );

   spkuef_c ( handle );
   chckxc_c ( SPICEFALSE, " ", ok );

   removeFile   ( SPK );
 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 


   
} /* End f_term_c */

