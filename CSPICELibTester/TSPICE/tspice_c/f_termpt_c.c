/*

-Procedure f_termpt_c ( termpt_c tests )

 
-Abstract
 
   Exercise the CSPICE wrapper termpt_c.
 
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_termpt_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars

   This routine tests the CSPICE wrapper 

      termpt_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version
 
   -tspice_c Version 1.0.0 15-FEB-2017 (NJB)

      Removed unused code for DLA traversal.

   31-AUG-2016 (NJB)

      Original version.

-Index_Entries

   test termpt_c

-&
*/

{ /* Begin f_termpt_c */

 
   /*
   Constants
   */
   #define DSK0            "termpt_test0.bds"
   #define DSK1            "termpt_test1.bds"
   #define PCK0            "termpt_test.tpc"
   #define SPK0            "termpt_test.bsp"

   #define LBLLEN          81
   #define LNSIZE          81
   #define MAXCUT          1000
   #define MAXN            10000
   #define METLEN          101
   #define NCORR           3
   #define NLINES          19
   #define NLOC            2
   #define NOBS            2
   #define NSRC            2
   #define NSHAPE          2
   #define NTARGS          2
   #define NTIMES          2
   #define TITLEN          321

   #define LOOSE           1.e-6
   #define MED             1.e-8
   #define TIGHT           1.e-12
   #define VTIGHT          1.e-14 

   /*
   Other macros 
   */

   /*
   The following macros are used to provide compact type cast
   expressions in order to suppress gcc's insane pointer type  
   compatibility warnings.  :/
   */
   #define PDP3            ( SpiceDouble (*) [3] )
   #define PI3             ( SpiceInt    (*) [3] )

   /*
   Local variables
   */
   SpiceBoolean            found;
 
   SpiceChar             * abcorr;
   SpiceChar             * corloc;
   SpiceChar             * fixref;
   SpiceChar             * ilusrc;
   SpiceChar               label  [ LBLLEN ];
   SpiceChar               method [ METLEN ];
   SpiceChar             * obsrvr;
   SpiceChar             * sfnmth;
   SpiceChar             * shadow;
   SpiceChar             * shape;
   SpiceChar             * target;
   SpiceChar               title  [ TITLEN ];

   SpiceDouble             a;
   SpiceDouble             axis   [3];
   SpiceDouble             angle;
   SpiceDouble             b;
   SpiceDouble             c;
   SpiceDouble             center [3];
   SpiceDouble             dlat;
   SpiceDouble             dlon;
   SpiceDouble             dp;
   SpiceDouble             eepocs [ MAXN ];
   SpiceDouble             epnts  [MAXN][3];
   SpiceDouble             epochs [ MAXN ];
   SpiceDouble             et0;
   SpiceDouble             et;
   SpiceDouble             etagts [MAXN][3];
   SpiceDouble             lt;
   SpiceDouble             normal [3];
   SpiceDouble             plnnml [3];
   SpiceDouble             plnref [3];
   SpiceDouble             points [MAXN][3];
   SpiceDouble             r;
   SpiceDouble             raydir [3];
   SpiceDouble             rcross;
   SpiceDouble             refvec [3];
   SpiceDouble             roll;
   SpiceDouble             rolstp;
   SpiceDouble             schstp;
   SpiceDouble             soltol;              
   SpiceDouble             srcrad;
   SpiceDouble             tanvec [3];
   SpiceDouble             trmvcs [MAXN][3];
   SpiceDouble             tdelta;
   SpiceDouble             tol;
   SpiceDouble             tpoint [3];
   SpiceDouble             tproj  [3];
   SpiceDouble             trgpos [3];
   SpiceDouble             vertex [3];
   SpiceDouble             viewpt [3];
   SpiceDouble             vtemp  [3];
   SpiceDouble             xepoch;   
   SpiceDouble             xpt    [3];
   SpiceDouble             xpt1   [3];
   SpiceDouble             xpt2   [3];
   SpiceDouble             xtrmvc [6];

   SpiceEllipse            srclmb;

   SpiceInt                bodyid;
   SpiceInt                corix;
   SpiceInt                cutix;
   static SpiceInt         enpts  [MAXCUT];
   SpiceInt                i;
   SpiceInt                j;
   SpiceInt                k;
   SpiceInt                locix;
   SpiceInt                n;
   SpiceInt                ncross;
   SpiceInt                ncuts;
   SpiceInt                nlat;
   SpiceInt                nlon;
   SpiceInt                npolyv;
   static SpiceInt         npts   [MAXCUT];
   SpiceInt                nxpts;
   SpiceInt                obsix;
   SpiceDouble             radii  [3];
   SpiceInt                shapix;
   SpiceInt                spkhan;
   SpiceInt                srcix;
   SpiceInt                srflst [SPICE_SRF_MAXSRF];

   SpiceInt                surfid;
   SpiceInt                targix;
   SpiceInt                timix;
   SpiceInt                trgcde;

   SpicePlane              cutpln;



   /*
   Saved values 
   */
   static SpiceChar     * corrs  [NCORR ] = 
                          { "None",     "CN",         "CN+S" };

   static SpiceChar     * locs   [NLOC] = 
                          { "CENTER", "Ellipsoid terminator" };

   static SpiceChar     * frames [NTARGS] = 
                          { "IAU_MARS", "SUN_PHOBOS_VIEW_ZY" };

   static SpiceChar     * obs    [NOBS  ] = 
                          { "Earth",    "Sun"                  };

   static SpiceChar     * srcs   [NOBS  ] = 
                          { "Sun",      "Moon"                 };

   static SpiceChar     * targs  [NTARGS] = 
                          { "Mars",     "Phobos"               };

   static SpiceChar     * shapes [NSHAPE] = 
                          { "DSK",      "ELLIPSOID"            };


   static SpiceChar       frmbuf [NLINES][LNSIZE] = {

      "FRAME_SUN_PHOBOS_VIEW_ZY        = 401000",
      "FRAME_401000_NAME               = 'SUN_PHOBOS_VIEW_ZY' ",
      "FRAME_401000_CLASS              = 5",
      "FRAME_401000_CLASS_ID           = 401000",
      "FRAME_401000_CENTER             = 401",
      "FRAME_401000_RELATIVE           = 'J2000' ",
      "FRAME_401000_DEF_STYLE          = 'PARAMETERIZED' ",
      "FRAME_401000_FAMILY             = 'TWO-VECTOR' ",
      "FRAME_401000_PRI_AXIS           = 'Z' ",
      "FRAME_401000_PRI_VECTOR_DEF     = 'OBSERVER_TARGET_POSITION' ",
      "FRAME_401000_PRI_OBSERVER       = 'SUN' ",
      "FRAME_401000_PRI_TARGET         = 'PHOBOS' ",
      "FRAME_401000_PRI_ABCORR         = 'NONE' ",
      "FRAME_401000_SEC_AXIS           = 'Y' ",
      "FRAME_401000_SEC_VECTOR_DEF     = 'OBSERVER_TARGET_VELOCITY' ",
      "FRAME_401000_SEC_OBSERVER       = 'SUN' ",
      "FRAME_401000_SEC_TARGET         = 'PHOBOS' ",
      "FRAME_401000_SEC_ABCORR         = 'NONE' ",
      "FRAME_401000_SEC_FRAME          = 'J2000' " 
   };

   SpiceDouble             origin [3] = { 0.0, 0.0, 0.0 };




   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_termpt_c" );
   

   /*
   *********************************************************************
   *
   *
   *   setup
   *
   *
   *********************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create a text PCK and a default SPK." );
   
   if ( exists_c(PCK0) ) 
   {
      removeFile(PCK0);
   }

   /*
   Don't load the PCK; do save it. 
   */
   tstpck_c ( PCK0, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load via furnsh_c to avoid later complexities. 
   */
   furnsh_c ( PCK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create and load the SPK. 
   */
   tstspk_c ( SPK0, SPICEFALSE, &spkhan );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c ( SPK0 );
   chckxc_c ( SPICEFALSE, " ", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Setup: create two DSKs containing target shapes." );
 

   if ( exists_c(DSK0) )
   {
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile( DSK0 );
   }
   chckxc_c ( SPICEFALSE, " ", ok );

   if ( exists_c(DSK1) )
   {
      chckxc_c ( SPICEFALSE, " ", ok );

      removeFile( DSK1 );
   }
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Create the DSKs. 
   */

   /*
   Start out by creating a segment for Mars. We'll create  
   a very low-resolution tessellated ellipsoid.
   */

   bodyid = 499;
   surfid = 1;
   fixref = "IAU_MARS";
   nlon   = 1000;
   nlat   =  501;

   t_elds2z_c ( bodyid, surfid, fixref, nlon, nlat, DSK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Compute lon/lat deltas for later use. 
   */
   dlon = twopi_c() / nlon;
   dlat = pi_c()    / nlat;


   /*
   Add shape data for Phobos. We'll use a set of nested tori having
   a central axis that points from the sun toward Phobos. This will
   require creation of three new segments. 
   
   We'll need to load the frame definition first. The frame name is

      SUN_PHOBOS_VIEW_ZY

   */
   lmpool_c ( frmbuf, LNSIZE, NLINES );



   bodyid = 401;
   surfid = 2;
   fixref = "SUN_PHOBOS_VIEW_ZY";
   npolyv = 50;
   ncross = 100;
   r      = 100;
   rcross =  10;
   vpack_c ( 0.0, 0.0, 0.0, center );
   vpack_c ( 0.0, 0.0, 1.0, normal );

   t_torus_c ( bodyid, surfid, fixref, 
               npolyv, ncross, r,
               rcross, center, normal, DSK1 );
  
   r = 70;

   t_torus_c ( bodyid, surfid, fixref, 
               npolyv, ncross, r,
               rcross, center, normal, DSK1 );

   r = 40;

   t_torus_c ( bodyid, surfid, fixref, 
               npolyv, ncross, r,
               rcross, center, normal, DSK1 );

   /*
   exit(0);
   */

   /*
   Load the DSKs. 
   */
   furnsh_c ( DSK0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c ( DSK1 );
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   *********************************************************************
   *
   *
   *   termpt_c normal cases
   *
   *
   *********************************************************************
   */
 
   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Top of termpt_c normal case loop" );


   /*
   Set an initial time and time delta.
   */
   et0    = 10 * jyear_c();
   tdelta = 3600.0;

   /*
   Loop over targets. 
   */
   for ( targix = 0;  targix < NTARGS;  targix++  )
   {
      target = targs [targix];
      fixref = frames[targix];

      bods2c_c ( target, &trgcde, &found );
      chckxc_c ( SPICEFALSE, " ", ok );

      chcksl_c ( "bods2c_c found", found, SPICETRUE, ok );


      /*
      Loop over observers. 
      */
      for ( obsix = 0;  obsix < NOBS;  obsix++  )
      {
         obsrvr = obs[obsix];

         /*
         Loop over illumination sources.
         */
         for ( srcix = 0;  srcix < NSRC;  srcix++  )
         {
            ilusrc = srcs[srcix];

            /*
            Loop over aberration corrections. 
            */
            for ( corix = 0;  corix < NCORR;  corix++  )
            {
               abcorr = corrs[corix];

               /*
               Loop over aberration correction loci.
               */
               for ( locix = 0;  locix < NLOC;  locix++  )
               {
                  corloc = locs[locix];


                  /*
                  Loop over observation times. 
                  */
                  for ( timix = 0;  timix < NTIMES;  timix++  )
                  {
                     et = et0  +  (timix * tdelta);

                     /*
                     Loop over target shapes. 
                     */
                     for ( shapix = 0;  shapix < NSHAPE;  shapix++  )
                     {
                        shape = shapes[shapix];

                        /*
                        Put together a method string. 
                        */

                        if ( eqstr_c( shape, "ellipsoid") )
                        {
                           strncpy ( method, 
                                     "Ellipsoid/Tangent/Penumbral", METLEN );

                           shadow = "Penumbral";
                        }
                        else
                        {
                           /*
                           This is the DSK case. 

                           The target index, plus 1, is the surface ID.
                           */
                           strncpy ( method,
                                     "dsk/unprioritized/tangent/"
                                     "umbral/surfaces = @",
                                     METLEN                              );
                           repmi_c ( method, "@", targix+1, METLEN, method );
                           chckxc_c ( SPICEFALSE, " ", ok );


                           shadow = "Umbral";
                        }



                        /* 
                        ---- Case --------------------------------------
                        */
                        sprintf ( title,
                                  "termpt_c: method = %s; "
                                  "locus = %s; "
                                  "abcorr = %s; " 
                                  "target = %s;  "
                                  "source = %s;  "
                                  "et = %23.17e; fixref = "
                                  "%s; obsrvr = %s. ",
                                  method,
                                  corloc,
                                  abcorr,
                                  target,
                                  ilusrc,
                                  et,
                                  fixref,
                                  obsrvr );

                        tcase_c ( title );

                        if ( eqstr_c(shape, "DSK") )
                        {
                           /*
                           This is the DSK case. 
                           */
                           soltol = 1.e-9;

                           if ( eqstr_c(target,"MARS") )
                           {
                              schstp = 4.0;
                              vpack_c ( 0.0, 0.0, 1.0, refvec );
                           }
                           else
                           {
                              /*
                              The target is Phobos. 
                              */
                              schstp = 1.e-8;
                              vpack_c ( 1.0, 0.0, 0.0, refvec );
                           }
                        }
                        else
                        {
                           soltol = 0.0;
                           schstp = 0.0;
                        }

                        
                        rolstp = 15.0 * rpd_c();

                        ncuts  = 4;


                        termpt_c ( method,
                                   ilusrc,  target,  et,     fixref,
                                   abcorr,  corloc,  obsrvr, refvec, 
                                   rolstp,  ncuts,   schstp, soltol, 
                                   MAXN,    npts,    points, epochs, 
                                   trmvcs                            );

                        chckxc_c ( SPICEFALSE, " ", ok );

 
                        /*
                        We're going to focus on just a few combinations
                        of observer, target, shape, aberration correction,
                        and aberration correction locus.
                        */

                        if (     eqstr_c( ilusrc, "SUN"  )
                             &&  eqstr_c( target, "PHOBOS" )
                             &&  eqstr_c( shape,  "DSK"    )
                             &&  eqstr_c( abcorr, "NONE"   )
                             &&  eqstr_c( corloc, "CENTER" ) )
                        {
                           /*
                           This is the case where Phobos is represented
                           by a set of three nested tori. 

                           We expect that each torus will contribute 
                           an upper and lower terminator point in each cutting
                           half-plane.
                           */


                           i = 0;

                           for ( cutix = 0; cutix < ncuts; cutix++ )
                           {
                              /*
                              We cut through 3 tori in each half-plane.
                              This gives us 6 terminator points. 
                              */
                              sprintf ( label, "npts[%d]", (int)cutix );
                              
                              chcksi_c ( label, npts[cutix], "=", 6, 0, ok );
                              
            
                              /*
                              Check the epochs, points, and tangent vectors. 
                              */
                              for ( j = 0; j < npts[cutix]; j++ )
                              {
                                 /*
                                 Since the aberration correction is NONE, 
                                 each epoch should match the input epoch.
                                 There still is one epoch per terminator point;
                                 check them all.
                                 */
                                  
                                 k   = i + j;

                                 tol = TIGHT;

                                 sprintf ( label, "epoch[%d] for cut %d", 
                                           (int)j, (int)cutix            );

                                 chcksd_c ( label, epochs[k], "~",
                                            et,    tol,       ok  );

                                 /*
                                 Each tangent vector should be the sum
                                 of the observer-target vector and the
                                 corresponding terminator point. 
                                 */

                                 spkpos_c ( target, et,     fixref, abcorr,
                                            obsrvr, trgpos, &lt            );
                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 vadd_c ( trgpos, points[k], xtrmvc );

                                 tol = TIGHT;

                                 sprintf ( label, "trmvcs[%d] for cut %d", 
                                           (int)j, (int)cutix            );
                                 chckad_c ( label,  trmvcs[k], "~~/",
                                            xtrmvc, 3,         tol,   ok );
           
                              }

                              i += npts[cutix];
                           }


                        }

                        else if (    eqstr_c( target, "MARS"                 )
                                  && eqstr_c( shape,  "ELLIPSOID"            )
                                  && eqstr_c( corloc, "ELLIPSOID TERMINATOR" ) )
                        {
                           /*
                           This is a set of cases where the epochs
                           associated with the terminator points are expected
                           to be accurate, since they're computed
                           individually for each terminator point, and
                           because the reference ellipsoid is used for
                           the light time corrections.

                           In these cases, we should get accurate
                           results for any aberration correction.

                           We expect just one terminator point per cutting 
                           half-plane. 
                           */
                           for ( cutix = 0; cutix < ncuts; cutix++ )
                           {
                              sprintf ( label, "npts[%d]", (int)cutix );
                              
                              chcksi_c ( label, npts[cutix], "=", 1, 0, ok );

                              /*
                              Find the state of the terminator point as seen by
                              the observer. 
                              */
                              spkcpt_c ( points[cutix], target, fixref, 
                                         et,            fixref, "target",  
                                         abcorr,        obsrvr, xtrmvc,  &lt );
                              chckxc_c ( SPICEFALSE, " ", ok );


                              /*
                              Check the epoch. 
                              */
                              if ( eqstr_c(abcorr, "NONE") )
                              {
                                 xepoch = et;
                              }
                              else
                              {
                                 xepoch = et - lt;
                              }

                              sprintf ( label, "epoch for cut %d", 
                                        (int)cutix                );

                              tol = TIGHT;

                              chcksd_c ( label,  epochs[cutix], "~",
                                         xepoch, tol,           ok  );


                              /*
                              Check the terminator point and tangent vector. 
                              */

                              tol = TIGHT;

                              sprintf ( label, "trmvcs[0] for cut %d", 
                                        (int)cutix                    );
                              chckad_c ( label,  trmvcs[cutix], "~~/",
                                         xtrmvc, 3,             tol,   ok );
                           }
                        }

                        else if (    eqstr_c( target, "MARS"                 )
                                  && eqstr_c( shape,  "DSK"                  )
                                  && eqstr_c( corloc, "ELLIPSOID TERMINATOR" ) )
                        {
                           /*
                           This is a set of cases where the epochs
                           associated with the terminator points are
                           expected to be less accurate, since, while
                           they're computed individually for each
                           terminator point, the computations are done
                           for terminator points on the reference
                           ellipsoid rather than on the DSK surface.
                         
                           Compute the ellipsoid terminator points just to
                           obtain the associated epochs. 
                           */
                           termpt_c ( "ellipsoid/tangent/umbral",  
                                      ilusrc,  target,  et,      fixref,
                                      abcorr,  corloc,  obsrvr, refvec, 
                                      rolstp,  ncuts,   schstp, soltol, 
                                      MAXN,    enpts,   epnts,  eepocs, 
                                      etagts                            );

                           /*
                           We expect just one terminator point per cutting 
                           half-plane. 
                           */

                           for ( cutix = 0; cutix < ncuts; cutix++ )
                           {
                              sprintf ( label, "npts[%d]", (int)cutix );
                              
                              chcksi_c ( label, npts[cutix], "=", 1, 0, ok );



                              /*
                              Find the state of the terminator point as seen by
                              the observer. 
                              */
                              spkcpt_c ( points[cutix], target, fixref, 
                                         et,            fixref, "target",  
                                         abcorr,        obsrvr, xtrmvc,  &lt );
                              chckxc_c ( SPICEFALSE, " ", ok );



                              /*
                              Check the epoch against that obtained by
                              treating the terminator point as an ephemeris
                              object. 
                              */
                              if ( eqstr_c(abcorr, "NONE") )
                              {
                                 xepoch = et;
                              }
                              else
                              {
                                 xepoch = et - lt;
                              }

                              sprintf ( label, "(1) epoch for cut %d", 
                                        (int)cutix                );


                              /*
                              We don't expect tight agreement. 
                              */
                              tol = 1.e-3;

                              chcksd_c ( label,  epochs[cutix], "~",
                                         xepoch, tol,           ok  );



                              /*
                              Check the epoch against that obtained for
                              an ellipsoidal target shape. This match
                              should be very good.
                              */
                              xepoch = eepocs[cutix];

                              sprintf ( label, "(2) epoch for cut %d", 
                                        (int)cutix                );

                              tol = VTIGHT;

                              chcksd_c ( label,  epochs[cutix], "~",
                                         xepoch, tol,           ok  );


                              /*
                              Check the terminator point and tangent vector. 
                              */

                              tol = LOOSE;

                              sprintf ( label, "trmvcs[0] for cut %d", 
                                        (int)cutix                    );
                              chckad_c ( label,  trmvcs[cutix], "~~/",
                                         xtrmvc, 3,             tol,   ok );
 
                           }
                        }



                        /*
                        The following tests apply to all combinations
                        of targets, observers, and shapes.
                        */
                        if ( eqstr_c( abcorr, "NONE" ) )
                        {
                           /*
                           The geometric case is relatively simple,
                           since the target frame is evaluated at the
                           input epoch `et'.
 
                           We've done some checks on the consistency of
                           the terminator points and tangent vectors,
                           but we haven't done anything to verify that
                           the terminator points returned by termpt_c
                           are really on the terminator, and that they
                           lie in the correct half-planes. Do that now.
                           */

                           j = 0;

                           for ( cutix = 0;  cutix < ncuts;  cutix++ )
                           {
                              /*
                              Check the points in the current
                              half-plane.
                              */
                              for ( i = 0;  i < npts[cutix];  i++ )
                              {            
                                 k = j + i;

                                 /*
                                 Use a friendly alias for the
                                 terminator point.
                                 */
                                 vequ_c ( points[k], tpoint );


                                 /*
                                 Make sure the terminator point is on
                                 the target surface. To do this, we'll
                                 get an outward normal vector at the
                                 point, create a vertex some distance
                                 from the surface in the normal
                                 direction, and then find the surface
                                 intercept of a ray emanating from this
                                 vertex and pointing in the direction
                                 opposite to the normal.
 
                                 If the original terminator point is on
                                 the surface, then the intercept found
                                 by this process should be very close
                                 to the terminator point. Of course, if
                                 the terminator point is too far off
                                 the surface, the attempt to find an
                                 outward surface normal will fail.
                                 */

                                 if ( eqstr_c( shape, "DSK" ) )
                                 {                          
                                    sfnmth = "DSK/UNPRIORITIZED";
                                 }
                                 else
                                 {
                                    sfnmth = "ELLIPSOID";
                                 }

                                 srfnrm_c ( sfnmth, target, et,  fixref,
                                            1,      tpoint, PDP3 normal );

                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 vminus_c ( normal, raydir );

                                 vadd_c ( tpoint, normal, vertex );

                                 if ( eqstr_c( shape, "DSK" ) )
                                 {                                  
                                    dskxv_c( SPICEFALSE, target,   0, srflst,
                                             et,         fixref,   1, vertex,
                                             raydir,     PDP3 xpt,    &found  );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                 }
                                 else
                                 {
                                    bodvrd_c ( target, "RADII", 3, &n, radii );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                    a = radii[0];
                                    b = radii[1];
                                    c = radii[2];

                                    surfpt_c ( vertex, raydir, a, 
                                               b,      c,      xpt, &found );
                                    chckxc_c ( SPICEFALSE, " ", ok );
                                 }

                                 /*
                                 We expect the intercept to be very close
                                 to the original terminator point. 
                                 */                              
                                 tol = TIGHT;

                                 sprintf ( label, 
                                           "terminator point %d "
                                           "in half plane %d",
                                           (int)i, (int)cutix    );

                                 chckad_c ( label, tpoint, "~~/", 
                                                   xpt,    3,     tol, ok );


                                 /*
                                 Now verify that the terminator point
                                 is in the correct cutting half-plane.
 
                                 Start out by getting the axis vector,
                                 which is the target center-illumination
                                 source vector.

                                 Because this case is geometric, we can
                                 use `et' as the epoch.
                                 */
                                 spkpos_c ( ilusrc, et,   fixref, abcorr,
                                            target, axis, &lt            );
                                 chckxc_c ( SPICEFALSE, " ", ok );


                                 /*
                                 Let plnref be a vector in the
                                 half-plane. Let vtemp be the result of
                                 rotating refvec about axis by the roll
                                 angle, and let plnref be the component
                                 of vtemp orthogonal to axis.
                                 */                              
                                 roll = cutix * rolstp;

                                 vrotv_c ( refvec, axis,  roll, vtemp );

                                 ucrss_c ( axis,   vtemp, plnnml );

                                 ucrss_c ( plnnml, axis,  plnref );


                                 /*
                                 Create a SPICE plane containing the target
                                 center and normal to plnnml. 
                                 */
                                 nvp2pl_c ( plnnml, origin, &cutpln );
                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 /*
                                 Project the terminator point orthogonally onto
                                 the plane containing the cutting half-plane. 

                                 The projection should be very close to the
                                 original terminator point. However, since the 
                                 observer may be far from the target, the
                                 round-off error in the axis may be fairly
                                 large. So we use a tolerance a bit looser
                                 than TIGHT.
                                 */
                                 vprjp_c ( tpoint, &cutpln, tproj );
                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 tol = MED;

                                 sprintf ( label, 
                                           "terminator point %d projection "
                                           "in half plane %d",
                                           (int)i, (int)cutix              );


                                 chckad_c ( label,  tproj,  "~~/", 
                                            tpoint, 3,      tol,   ok );

                                 /*
                                 We've verified the point is in the correct
                                 plane; we must ensure it's in the correct
                                 half-plane. 
                                 */
                                 dp = vdot_c( tpoint, plnref );

                                 sprintf ( label, 
                                           "terminator point %d inner product "
                                           "with reference vector "
                                           "in half plane %d",
                                           (int)i, (int)cutix              );

                                 chcksd_c ( label, dp, ">", 0.0, 0.0, ok );


                                 /*
                                 Programmer's note:

                                 This next step is not necessary to verify
                                 the correctness of the CSPICE wrapper. It
                                 need not be replicated in ticy and tmice
                                 test families. (But it doesn't hurt to 
                                 include it.)                   
                                 */

                                 /*
                                 We'll produce a vector in the current
                                 half-plane that's tangent to both the
                                 illumination source and to the target.
                                 
                                 Start out by finding the limb on the 
                                 illumination source as seen from 
                                 current terminator point. The point
                                 of tangency on the source must lie on
                                 the limb.

                                 The source's shape is considered to 
                                 be a sphere by termpt_c, so we treat
                                 it as a sphere here, as well.
                                 */ 
                                 bodvrd_c ( ilusrc, "RADII", 3, &n, radii );
                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 srcrad = maxd_c( 3,        radii[0], 
                                                  radii[1], radii[2]  );

                                 vsub_c ( tpoint, axis, viewpt );

                                 edlimb_c ( srcrad, srcrad, 
                                            srcrad, viewpt, &srclmb );

                                 chckxc_c ( SPICEFALSE, " ", ok );

                                 /*
                                 Now chop the limb using the current cutting  
                                 half-plane. The tangency point we seek is
                                 one of the points of intersection.
                                 */
                                 inelpl_c ( &srclmb, &cutpln, &nxpts,
                                            xpt1,    xpt2            );

                                 chcksi_c ( "nxpts", nxpts, "=", 2, 0, ok );

                                 /*
                                 The intersection points are offsets from
                                 the center of the illumination source.
                                 Translate them so that they're relative to
                                 the target center. 
                                 */
                                 vadd_c ( xpt1, axis, xpt1 );
                                 vadd_c ( xpt2, axis, xpt2 );

                                 /*
                                 If the shadow type is "umbral," then
                                 the tangent point on the source is on
                                 the same side of the axis as the
                                 reference vector. Otherwise, it's on
                                 the opposite side.
                                 */
                                 if ( eqstr_c( shadow, "Umbral" ) )
                                 {
                                    if ( vdot_c( xpt1, plnref ) > 0.0 )
                                    {
                                       vequ_c ( xpt1, vertex );
                                    }
                                    else
                                    {
                                       vequ_c ( xpt2, vertex );
                                    }                               
                                 }
                                 else
                                 {
                                    if ( vdot_c( xpt1, plnref ) < 0.0 )
                                    {
                                       vequ_c ( xpt1, vertex );
                                    }
                                    else
                                    {
                                       vequ_c ( xpt2, vertex );
                                    }                               
                                 }

                                 vsub_c ( tpoint, vertex, tanvec );

                                 /*
                                 Make sure that this tangent vector, if
                                 rotated slightly away from the target
                                 surface, defines the direction of a
                                 ray that misses the target.

                                 We need to be careful with the nested
                                 tori shape: for this one, "away from
                                 the surface" does not coincide with
                                 "away from the observer- target center
                                 vector."
                                 */

                                 if ( npts[cutix] == 1 )
                                 {
                                    /*
                                    We have a convex surface.

                                    "Away from the surface" corresponds to
                                    a negative rotation about plnnml.
                                    */ 
                                    angle = -1.e-8;
                                 }
                                 else
                                 {
                                    if ( i%2 == 0 )
                                    {
                                       /*
                                       This is an "outer" terminator point.
                                       */
                                       angle = -1.e-8;
                                    }
                                    else
                                    {
                                       angle =  1.e-8;
                                    }
                                 }


                                 vrotv_c ( tanvec, plnnml, angle, raydir );
                                 chckxc_c ( SPICEFALSE, " ", ok );


                                 if ( eqstr_c( shape, "DSK" ) )
                                 {
                                    dskxv_c( SPICEFALSE, target,   0, srflst,
                                             et,         fixref,   1, vertex,
                                             raydir,     PDP3 xpt,    &found  );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                 }
                                 else
                                 {
                                    bodvrd_c ( target, "RADII", 3, &n, radii );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                    a = radii[0];
                                    b = radii[1];
                                    c = radii[2];

                                    surfpt_c ( vertex, raydir, a, 
                                               b,      c,      xpt, &found );
                                    chckxc_c ( SPICEFALSE, " ", ok );
                                 }


                                 /*
                                 No intercept should be found. 
                                 */
                                 sprintf ( label, 
                                        "terminator point %d off-surface "
                                        "ray intercept found "
                                        "in half plane %d",
                                        (int)i, (int)cutix              );

                                 chcksl_c ( label, found, SPICEFALSE, ok );



                                 /*
                                 Now check that rotating the tangent vector
                                 towards the target results in an intercept. 
                                 */
                                 angle *= -1;


                                 vrotv_c ( tanvec, plnnml, angle, raydir );
                                 chckxc_c ( SPICEFALSE, " ", ok );


                                 if ( eqstr_c( shape, "DSK" ) )
                                 {
                                    dskxv_c( SPICEFALSE, target,   0, srflst,
                                             et,         fixref,   1, vertex,
                                             raydir,     PDP3 xpt,    &found  );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                 }
                                 else
                                 {
                                    bodvrd_c ( target, "RADII", 3, &n, radii );
                                    chckxc_c ( SPICEFALSE, " ", ok );

                                    a = radii[0];
                                    b = radii[1];
                                    c = radii[2];

                                    surfpt_c ( vertex, raydir, a, 
                                               b,      c,      xpt, &found );
                                    chckxc_c ( SPICEFALSE, " ", ok );
                                 }


                                 /*
                                 An intercept should be found. 
                                 */
                                 sprintf ( label, 
                                        "terminator point %d off-surface "
                                        "ray intercept found "
                                        "in half plane %d",
                                        (int)i, (int)cutix              );

                                 chcksl_c ( label, found, SPICETRUE, ok );

                              }
                              /*
                              End of loop for current half-plane. 
                              */

                              j += npts[cutix];
                           }
                           /*
                           End of loop over all half-planes.
                           */
                        }
                        /*
                        End of geometric case. 
                        */
                     }
                     /*
                     End of shape loop. 
                     */
                  }
                  /*
                  End of time loop. 
                  */
               }
               /*
               End of aberration correction locus loop. 
               */
            }
            /*
            End of aberration correction loop. 
            */
         }
         /*
         End of illumination source loop.
         */
      }
      /*
      End of observer loop.
      */
   }
   /*
   End of target loop. 
   */


   /*
   *********************************************************************
   *
   *
   *   termpt_c error cases
   *
   *
   *********************************************************************
   */

   /*
   We need not test all of the error cases, but we must test at least
   one; this will exercise the error handling logic in the wrapper.
   */

   /*
   Use the last values of all the inputs. We'll create an error by 
   unloading the SPK file. 
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "termpt error: no loaded SPK." );

   rolstp = 10.0 * rpd_c();
   schstp = 1.e-4;
   soltol = 1.e-9;
   ncuts  = 1;
   corloc = "center";
   vpack_c ( 0.0, 0.0, 1.0, refvec );

   unload_c ( SPK0 );
   chckxc_c ( SPICEFALSE, " ", ok );
 
   strncpy ( method, "ellipsoid/tangent/penumbral", METLEN );

   termpt_c ( method,  ilusrc,  target,  et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                           );
   chckxc_c ( SPICETRUE, "SPICE(NOLOADEDFILES)", ok );
 
   /*
   For CSPICE, we must check handling of bad input strings.
   */

   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "termpt error: empty input string." );

   furnsh_c (SPK0);
   chckxc_c ( SPICEFALSE, " ", ok );

   termpt_c ( "",      ilusrc,  target,  et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   termpt_c ( method,  "",      target, et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   termpt_c ( method,  ilusrc,  "",     et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   termpt_c ( method,  ilusrc,  target, et,     "",
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   termpt_c ( method,  ilusrc,  target,  et,     fixref,
              "",      corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );

   termpt_c ( method,  ilusrc,  target,  et,     fixref,
              abcorr,  "",      obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );




   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "termpt error: null input string." );

   termpt_c ( NULL,    ilusrc,  target,  et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );


   termpt_c ( method,  NULL,    target, et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   termpt_c ( method,  ilusrc,  NULL,   et,     fixref,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   termpt_c ( method,  ilusrc,  target, et,     NULL,
              abcorr,  corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   termpt_c ( method,  ilusrc,  target,  et,     fixref,
              NULL,    corloc,  obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   termpt_c ( method,  ilusrc,  target,  et,     fixref,
              abcorr,  NULL,    obsrvr, refvec, 
              rolstp,  ncuts,   schstp, soltol, 
              MAXN,    npts,    points, epochs, 
              trmvcs                            );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

 
  



   /*
   *********************************************************************
   *
   *
   *   Clean up
   *
   *
   *********************************************************************
   */



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up." );


   kclear_c();
   chckxc_c ( SPICEFALSE, " ", ok );    

   removeFile( PCK0 );
   removeFile( SPK0 );
   removeFile( DSK0 );
   removeFile( DSK1 );

 

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_termpt_c */

