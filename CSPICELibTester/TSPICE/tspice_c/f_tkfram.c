/* f_tkfram.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__53 = 53;
static integer c__86 = 86;
static logical c_false = FALSE_;
static integer c__9 = 9;
static doublereal c_b117 = 1e-14;
static integer c__0 = 0;
static integer c__277 = 277;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__10013 = 10013;
static doublereal c_b427 = 1e-13;
static integer c_b431 = 1000003;
static integer c__14 = 14;
static integer c__1 = 1;
static doublereal c_b565 = 1e-12;

/* $Procedure      F_TKFRAM ( Family Test of  TKFRAM ) */
/* Subroutine */ int f_tkfram__(logical *ok)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3, i__4, i__5[2];
    doublereal d__1, d__2, d__3;
    char ch__1[16], ch__2[138];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static doublereal erot[9], xrot[9]	/* was [3][3] */;
    extern /* Subroutine */ int eul2m_(doublereal *, doublereal *, doublereal 
	    *, integer *, integer *, integer *, doublereal *);
    static integer i__, j;
    static doublereal r__[3645]	/* was [3][3][405] */;
    static integer frame;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[132];
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static char lines[132*300];
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static doublereal angle1[405], angle2[405], angle3[405];
    extern /* Subroutine */ int t_success__(logical *);
    static integer id;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern /* Character */ VOID begdat_(char *, ftnlen);
    extern doublereal pi_(void);
    static integer eframe;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static char buffer[132*30];
    extern /* Subroutine */ int kilfil_(char *, ftnlen), tkfram_(integer *, 
	    doublereal *, integer *, logical *);
    static integer nlines;
    extern /* Subroutine */ int clpool_(void);
    extern /* Character */ VOID begtxt_(char *, ftnlen);
    static integer maxvar;
    extern /* Subroutine */ int lmpool_(char *, integer *, ftnlen), szpool_(
	    char *, integer *, logical *, ftnlen), tsttxt_(char *, char *, 
	    integer *, logical *, logical *, ftnlen, ftnlen);
    extern doublereal rpd_(void);
    static doublereal rot[9]	/* was [3][3] */;

/* $ Abstract */

/*     Test the routine TKFRAM. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the frame system utility routine TKFRAM. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     B.V. Semenov     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 3.1.0, 08-JAN-2014 (BVS) */

/*        Added failure test case for a frame specified */
/*        relative to itself. */

/*        Renamed TKBFSZ to BUFSIZ and increased it from 20 to 200 to be */
/*        consistent with TKFRAM. */

/*        Changed to fetch MAXVAR via SZPOOL. */

/*        Replaced in-line numbers with BUFSIZ where applicable. */

/* -    TSPICE Version 3.0.0, 24-APR-2009 (NJB) */

/*        Added test cases to exercise watchers, in */
/*        particular to ensure that watchers are not */
/*        deleted improperly. */

/* -    TSPICE Version 2.0.0, 19-MAR-2009 (NJB) */

/*        Added test case to check watcher deletion. */

/* -    TSPICE Version 1.1.0, 20-OCT-1999 (WLT) */

/*        Declared PI to be an EXTERNAL Function. */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     BUFSIZ must be set to BUFSIZ used in TKFRAM. */


/*     Maximum size of angle, matrix lists. */


/*     Local variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_TKFRAM", (ftnlen)8);

/*     Clear the kernel pool so that we don't have to worry */
/*     about previous test cases contaminating the kernel pool. */

    clpool_();

/*     Get rid of any existing frame kernels. */

    kilfil_("phoenix.tk", (ftnlen)10);
    kilfil_("phoenix.prt", (ftnlen)11);

/*     Create an I-kernel that we can load later on. */

    s_copy(lines, "This is a test TK Frame kernel for the fictional instrume"
	    "nt TST_PHEONIX", (ftnlen)132, (ftnlen)71);
    s_copy(lines + 132, "on board the fictional spacecraft PHEONIX.  A C-ker"
	    "nel for", (ftnlen)132, (ftnlen)58);
    s_copy(lines + 264, "the platform on which TST_PHEONIX is mounted can be"
	    " generated", (ftnlen)132, (ftnlen)61);
    s_copy(lines + 396, "by calling the test utility TSTCK3.", (ftnlen)132, (
	    ftnlen)35);
    s_copy(lines + 528, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 660, "This kernel describes only the orientation attribut"
	    "es of the", (ftnlen)132, (ftnlen)60);
    s_copy(lines + 792, "TST_PHOENIX instrument.", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 924, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1056, "This kernel is intended only for test purposes.  I"
	    "t is primarily", (ftnlen)132, (ftnlen)64);
    s_copy(lines + 1188, "useful for testing the TKFRAM data fetching routin"
	    "e.", (ftnlen)132, (ftnlen)52);
    s_copy(lines + 1320, " ", (ftnlen)132, (ftnlen)1);
    begdat_(ch__1, (ftnlen)16);
    s_copy(lines + 1452, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 1584, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1716, "TKFRAME_-111111_SPEC      = 'MATRIX'", (ftnlen)132, 
	    (ftnlen)36);
    s_copy(lines + 1848, "TKFRAME_-111111_RELATIVE  = 'PHOENIX'", (ftnlen)132,
	     (ftnlen)37);
    s_copy(lines + 1980, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 2112, "TKFRAME_-111111_MATRIX    = ( 0.48", (ftnlen)132, (
	    ftnlen)34);
    s_copy(lines + 2244, " 0.60", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 2376, " 0.64", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 2508, "-0.8", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 2640, " 0.0", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 2772, " 0.6", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 2904, " 0.36", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 3036, "-0.80", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 3168, " 0.48 )", (ftnlen)132, (ftnlen)7);
    s_copy(lines + 3300, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 3432, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 3564, "TKFRAME_TST2-PHOENIX_SPEC      = 'ROTATION'", (
	    ftnlen)132, (ftnlen)43);
    s_copy(lines + 3696, "TKFRAME_TST2-PHOENIX_RELATIVE  = 'PHOENIX'", (
	    ftnlen)132, (ftnlen)42);
    s_copy(lines + 3828, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 3960, "TKFRAME_-111112_ROTATION  = ( 0.48", (ftnlen)132, (
	    ftnlen)34);
    s_copy(lines + 4092, " 0.60", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 4224, " 0.64", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 4356, "-0.8", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 4488, " 0.0", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 4620, " 0.6", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 4752, " 0.36", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 4884, "-0.80", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 5016, " 0.48 )", (ftnlen)132, (ftnlen)7);
    s_copy(lines + 5148, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 5280, " ", (ftnlen)132, (ftnlen)1);
    begtxt_(ch__1, (ftnlen)16);
    s_copy(lines + 5412, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 5544, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 5676, "Next we need to supply the various bits of frame i"
	    "dentification for", (ftnlen)132, (ftnlen)67);
    s_copy(lines + 5808, "this instrument.", (ftnlen)132, (ftnlen)16);
    s_copy(lines + 5940, " ", (ftnlen)132, (ftnlen)1);
    begdat_(ch__1, (ftnlen)16);
    s_copy(lines + 6072, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 6204, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 6336, "FRAME_-111111_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6468, "FRAME_-111111_CENTER   = -9", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6600, "FRAME_-111111_CLASS_ID = -111111", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 6732, "FRAME_-111111_NAME     = 'TST-PHOENIX'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 6864, "FRAME_TST-PHOENIX      = -111111", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 6996, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 7128, "FRAME_-9999_CLASS      =  3", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7260, "FRAME_-9999_CENTER     = -9", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7392, "FRAME_-9999_CLASS_ID   = -9999", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 7524, "FRAME_-9999_NAME       = 'PHOENIX'", (ftnlen)132, (
	    ftnlen)34);
    s_copy(lines + 7656, "FRAME_PHOENIX          = -9999", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 7788, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 7920, "FRAME_-111112_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 8052, "FRAME_-111112_CENTER   = -9", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 8184, "FRAME_-111112_CLASS_ID = -111112", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 8316, "FRAME_-111112_NAME     = 'TST2-PHOENIX'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 8448, "FRAME_TST2-PHOENIX     = -111112", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 8580, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 8712, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 8844, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 8976, "CK_-9999_SCLK          =  -9", (ftnlen)132, (ftnlen)
	    28);
    s_copy(lines + 9108, "CK_-9999_SPK           =  -9", (ftnlen)132, (ftnlen)
	    28);
    s_copy(lines + 9240, " ", (ftnlen)132, (ftnlen)1);
    begtxt_(ch__1, (ftnlen)16);
    s_copy(lines + 9372, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 9504, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 9636, "This frame is defined relative to itself. A no-no.",
	     (ftnlen)132, (ftnlen)50);
    s_copy(lines + 9768, " ", (ftnlen)132, (ftnlen)1);
    begdat_(ch__1, (ftnlen)16);
    s_copy(lines + 9900, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 10032, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 10164, "FRAME_-111113_NAME        = 'TST3-PHOENIX'", (
	    ftnlen)132, (ftnlen)42);
    s_copy(lines + 10296, "FRAME_TST3-PHOENIX        = -111113", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 10428, "FRAME_-111113_CLASS       =  4", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 10560, "FRAME_-111113_CENTER      = -9", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 10692, "FRAME_-111113_CLASS_ID    = -111113", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 10824, "TKFRAME_-111113_SPEC      = 'MATRIX'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 10956, "TKFRAME_-111113_RELATIVE  = 'TST3-PHOENIX'", (
	    ftnlen)132, (ftnlen)42);
    s_copy(lines + 11088, "TKFRAME_-111113_ROTATION  = (", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11220, "0.48 0.60 0.64 -0.8 0.0 0.6 0.36 -0.80 0.48 )", (
	    ftnlen)132, (ftnlen)45);
    tcase_("Check that a zero instrument ID causes an error to be signaled. ",
	     (ftnlen)64);
    id = 0;
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_true, "SPICE(ZEROFRAMEID)", ok, (ftnlen)18);
    tcase_("Check that a proper error is signaled if there is no frame data "
	    "for the requestedframe. ", (ftnlen)88);
    id = -10;
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_true, "SPICE(INCOMPLETFRAME)", ok, (ftnlen)21);
    tcase_("Load a TK frame that is missing the orientation information and "
	    "check that the deficiency is properly diagnosed.", (ftnlen)112);
    id = -111111;
    clpool_();
    tsttxt_("phoenix.prt", lines, &c__53, &c_true, &c_true, (ftnlen)11, (
	    ftnlen)132);
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_true, "SPICE(BADFRAMESPEC)", ok, (ftnlen)19);
    tcase_("Make sure that the proper error is signaled if the frame 'SPEC' "
	    "is not recognized. ", (ftnlen)83);
    id = -111112;
    clpool_();
    tsttxt_("phoenix.tk", lines, &c__86, &c_true, &c_true, (ftnlen)10, (
	    ftnlen)132);
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAMESPEC)", ok, (ftnlen)23);
    tcase_("Make sure that the proper error is signaled if the frame is defi"
	    "ned relative to itself. ", (ftnlen)88);
    id = -111113;
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_true, "SPICE(BADFRAMESPEC2)", ok, (ftnlen)20);
    tcase_("Determine the orientation of the instrument frame relative to it"
	    "s C-kernel frame. ", (ftnlen)82);
    id = -111111;
    tkfram_(&id, rot, &frame, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    erot[0] = .48;
    erot[1] = .6;
    erot[2] = .64;
    erot[3] = -.8;
    erot[4] = 0.;
    erot[5] = .6;
    erot[6] = .36;
    erot[7] = -.8;
    erot[8] = .48;
    eframe = -9999;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("ROT", rot, "~", erot, &c__9, &c_b117, ok, (ftnlen)3, (ftnlen)1);
    chcksi_("FRAME", &frame, "=", &eframe, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tcase_("Make sure that we can successfully retrieve rotations for 25 rea"
	    "listic frames.", (ftnlen)78);
    s_copy(lines, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 132, "This PCK file contains definitions for TOPOGRAPHIC "
	    "reference", (ftnlen)132, (ftnlen)60);
    s_copy(lines + 264, "frames at 25 different observatories around the wor"
	    "ld.  Note", (ftnlen)132, (ftnlen)60);
    s_copy(lines + 396, "that the definition of these frames is approximate "
	    "and that", (ftnlen)132, (ftnlen)59);
    s_copy(lines + 528, "they are accurate to only about 0.1 degrees.", (
	    ftnlen)132, (ftnlen)44);
    s_copy(lines + 660, " ", (ftnlen)132, (ftnlen)1);
    begdat_(ch__1, (ftnlen)16);
    s_copy(lines + 792, ch__1, (ftnlen)132, (ftnlen)16);
    s_copy(lines + 924, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1056, "FRAME_LAPLATA_TOPO     = 1000001", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1188, "FRAME_CANBERRA_TOPO    = 1000002", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1320, "FRAME_URANIA_TOPO      = 1000003", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1452, "FRAME_VALONGO_TOPO     = 1000004", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1584, "FRAME_LASCAMPANAS_TOPO = 1000005", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1716, "FRAME_YUNNAN_TOPO      = 1000006", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1848, "FRAME_QUITO_TOPO       = 1000007", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 1980, "FRAME_TUORLA_TOPO      = 1000008", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2112, "FRAME_GRENOBLE_TOPO    = 1000009", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2244, "FRAME_HAMBURG_TOPO     = 1000010", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2376, "FRAME_MUNICH_TOPO      = 1000011", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2508, "FRAME_KODAIKANAL_TOPO  = 1000012", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2640, "FRAME_DUNSINK_TOPO     = 1000013", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2772, "FRAME_TURIN_TOPO       = 1000014", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 2904, "FRAME_HIDA_TOPO        = 1000015", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3036, "FRAME_ARECIBO_TOPO     = 1000016", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3168, "FRAME_PULKOVO_TOPO     = 1000017", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3300, "FRAME_BOYDEN_TOPO      = 1000018", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3432, "FRAME_MADRID_TOPO      = 1000019", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3564, "FRAME_AROSA_TOPO       = 1000020", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3696, "FRAME_GREENWICH_TOPO   = 1000021", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3828, "FRAME_KITTPEAK_TOPO    = 1000022", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 3960, "FRAME_GOLDSTONE_TOPO   = 1000023", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 4092, "FRAME_PALOMAR_TOPO     = 1000024", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 4224, "FRAME_USNO_TOPO        = 1000025", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 4356, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 4488, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 4620, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 4752, "FRAME_1000001_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 4884, "FRAME_1000002_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5016, "FRAME_1000003_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5148, "FRAME_1000004_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5280, "FRAME_1000005_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5412, "FRAME_1000006_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5544, "FRAME_1000007_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5676, "FRAME_1000008_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5808, "FRAME_1000009_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 5940, "FRAME_1000010_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6072, "FRAME_1000011_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6204, "FRAME_1000012_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6336, "FRAME_1000013_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6468, "FRAME_1000014_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6600, "FRAME_1000015_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6732, "FRAME_1000016_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6864, "FRAME_1000017_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 6996, "FRAME_1000018_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7128, "FRAME_1000019_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7260, "FRAME_1000020_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7392, "FRAME_1000021_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7524, "FRAME_1000022_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7656, "FRAME_1000023_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7788, "FRAME_1000024_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 7920, "FRAME_1000025_CLASS    =  4", (ftnlen)132, (ftnlen)
	    27);
    s_copy(lines + 8052, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 8184, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 8316, "FRAME_1000001_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 8448, "FRAME_1000002_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 8580, "FRAME_1000003_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 8712, "FRAME_1000004_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 8844, "FRAME_1000005_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 8976, "FRAME_1000006_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9108, "FRAME_1000007_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9240, "FRAME_1000008_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9372, "FRAME_1000009_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9504, "FRAME_1000010_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9636, "FRAME_1000011_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9768, "FRAME_1000012_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 9900, "FRAME_1000013_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10032, "FRAME_1000014_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10164, "FRAME_1000015_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10296, "FRAME_1000016_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10428, "FRAME_1000017_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10560, "FRAME_1000018_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10692, "FRAME_1000019_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10824, "FRAME_1000020_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 10956, "FRAME_1000021_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11088, "FRAME_1000022_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11220, "FRAME_1000023_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11352, "FRAME_1000024_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11484, "FRAME_1000025_CENTER   =  399", (ftnlen)132, (
	    ftnlen)29);
    s_copy(lines + 11616, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 11748, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 11880, "FRAME_1000001_CLASS_ID = 1000001", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12012, "FRAME_1000002_CLASS_ID = 1000002", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12144, "FRAME_1000003_CLASS_ID = 1000003", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12276, "FRAME_1000004_CLASS_ID = 1000004", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12408, "FRAME_1000005_CLASS_ID = 1000005", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12540, "FRAME_1000006_CLASS_ID = 1000006", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12672, "FRAME_1000007_CLASS_ID = 1000007", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12804, "FRAME_1000008_CLASS_ID = 1000008", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 12936, "FRAME_1000009_CLASS_ID = 1000009", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13068, "FRAME_1000010_CLASS_ID = 1000010", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13200, "FRAME_1000011_CLASS_ID = 1000011", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13332, "FRAME_1000012_CLASS_ID = 1000012", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13464, "FRAME_1000013_CLASS_ID = 1000013", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13596, "FRAME_1000014_CLASS_ID = 1000014", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13728, "FRAME_1000015_CLASS_ID = 1000015", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13860, "FRAME_1000016_CLASS_ID = 1000016", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 13992, "FRAME_1000017_CLASS_ID = 1000017", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14124, "FRAME_1000018_CLASS_ID = 1000018", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14256, "FRAME_1000019_CLASS_ID = 1000019", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14388, "FRAME_1000020_CLASS_ID = 1000020", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14520, "FRAME_1000021_CLASS_ID = 1000021", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14652, "FRAME_1000022_CLASS_ID = 1000022", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14784, "FRAME_1000023_CLASS_ID = 1000023", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 14916, "FRAME_1000024_CLASS_ID = 1000024", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 15048, "FRAME_1000025_CLASS_ID = 1000025", (ftnlen)132, (
	    ftnlen)32);
    s_copy(lines + 15180, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 15312, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 15444, "FRAME_1000001_NAME     = 'LAPLATA_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 15576, "FRAME_1000002_NAME     = 'CANBERRA_TOPO'", (ftnlen)
	    132, (ftnlen)40);
    s_copy(lines + 15708, "FRAME_1000003_NAME     = 'URANIA_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 15840, "FRAME_1000004_NAME     = 'VALONGO_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 15972, "FRAME_1000005_NAME     = 'LASCAMPANAS_TOPO'", (
	    ftnlen)132, (ftnlen)43);
    s_copy(lines + 16104, "FRAME_1000006_NAME     = 'YUNNAN_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 16236, "FRAME_1000007_NAME     = 'QUITO_TOPO'", (ftnlen)
	    132, (ftnlen)37);
    s_copy(lines + 16368, "FRAME_1000008_NAME     = 'TUORLA_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 16500, "FRAME_1000009_NAME     = 'GRENOBLE_TOPO'", (ftnlen)
	    132, (ftnlen)40);
    s_copy(lines + 16632, "FRAME_1000010_NAME     = 'HAMBURG_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 16764, "FRAME_1000011_NAME     = 'MUNICH_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 16896, "FRAME_1000012_NAME     = 'KODAIKANAL_TOPO'", (
	    ftnlen)132, (ftnlen)42);
    s_copy(lines + 17028, "FRAME_1000013_NAME     = 'DUNSINK_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 17160, "FRAME_1000014_NAME     = 'TURIN_TOPO'", (ftnlen)
	    132, (ftnlen)37);
    s_copy(lines + 17292, "FRAME_1000015_NAME     = 'HIDA_TOPO'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 17424, "FRAME_1000016_NAME     = 'ARECIBO_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 17556, "FRAME_1000017_NAME     = 'PULKOVO_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 17688, "FRAME_1000018_NAME     = 'BOYDEN_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 17820, "FRAME_1000019_NAME     = 'MADRID_TOPO'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 17952, "FRAME_1000020_NAME     = 'AROSA_TOPO'", (ftnlen)
	    132, (ftnlen)37);
    s_copy(lines + 18084, "FRAME_1000021_NAME     = 'GREENWICH_TOPO'", (
	    ftnlen)132, (ftnlen)41);
    s_copy(lines + 18216, "FRAME_1000022_NAME     = 'KITTPEAK_TOPO'", (ftnlen)
	    132, (ftnlen)40);
    s_copy(lines + 18348, "FRAME_1000023_NAME     = 'GOLDSTONE_TOPO'", (
	    ftnlen)132, (ftnlen)41);
    s_copy(lines + 18480, "FRAME_1000024_NAME     = 'PALOMAR_TOPO'", (ftnlen)
	    132, (ftnlen)39);
    s_copy(lines + 18612, "FRAME_1000025_NAME     = 'USNO_TOPO'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 18744, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 18876, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 19008, "TKFRAME_1000001_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19140, "TKFRAME_1000002_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19272, "TKFRAME_1000003_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19404, "TKFRAME_1000004_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19536, "TKFRAME_1000005_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19668, "TKFRAME_1000006_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19800, "TKFRAME_1000007_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 19932, "TKFRAME_1000008_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20064, "TKFRAME_1000009_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20196, "TKFRAME_1000010_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20328, "TKFRAME_1000011_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20460, "TKFRAME_1000012_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20592, "TKFRAME_1000013_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20724, "TKFRAME_1000014_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20856, "TKFRAME_1000015_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 20988, "TKFRAME_1000016_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21120, "TKFRAME_1000017_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21252, "TKFRAME_1000018_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21384, "TKFRAME_1000019_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21516, "TKFRAME_AROSA_TOPO_SPEC  = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21648, "TKFRAME_1000021_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21780, "TKFRAME_1000022_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 21912, "TKFRAME_1000023_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 22044, "TKFRAME_1000024_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 22176, "TKFRAME_1000025_SPEC     = 'ANGLES'", (ftnlen)132, 
	    (ftnlen)35);
    s_copy(lines + 22308, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 22440, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 22572, "TKFRAME_1000001_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 22704, "TKFRAME_1000002_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 22836, "TKFRAME_1000003_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 22968, "TKFRAME_1000004_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23100, "TKFRAME_1000005_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23232, "TKFRAME_1000006_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23364, "TKFRAME_1000007_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23496, "TKFRAME_1000008_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23628, "TKFRAME_1000009_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23760, "TKFRAME_1000010_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 23892, "TKFRAME_1000011_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24024, "TKFRAME_1000012_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24156, "TKFRAME_1000013_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24288, "TKFRAME_1000014_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24420, "TKFRAME_1000015_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24552, "TKFRAME_1000016_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24684, "TKFRAME_1000017_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24816, "TKFRAME_1000018_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 24948, "TKFRAME_1000019_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25080, "TKFRAME_AROSA_TOPO_RELATIVE = 'IAU_EARTH'", (
	    ftnlen)132, (ftnlen)41);
    s_copy(lines + 25212, "TKFRAME_1000021_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25344, "TKFRAME_1000022_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25476, "TKFRAME_1000023_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25608, "TKFRAME_1000024_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25740, "TKFRAME_1000025_RELATIVE = 'IAU_EARTH'", (ftnlen)
	    132, (ftnlen)38);
    s_copy(lines + 25872, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 26004, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 26136, "TKFRAME_1000001_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26268, "TKFRAME_1000002_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26400, "TKFRAME_1000003_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26532, "TKFRAME_1000004_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26664, "TKFRAME_1000005_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26796, "TKFRAME_1000006_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 26928, "TKFRAME_1000007_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27060, "TKFRAME_1000008_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27192, "TKFRAME_1000009_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27324, "TKFRAME_1000010_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27456, "TKFRAME_1000011_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27588, "TKFRAME_1000012_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27720, "TKFRAME_1000013_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27852, "TKFRAME_1000014_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 27984, "TKFRAME_1000015_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28116, "TKFRAME_1000016_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28248, "TKFRAME_1000017_AXES =     (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28380, "TKFRAME_BOYDEN_TOPO_AXES = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28512, "TKFRAME_1000019_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28644, "TKFRAME_1000020_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28776, "TKFRAME_1000021_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 28908, "TKFRAME_1000022_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 29040, "TKFRAME_1000023_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 29172, "TKFRAME_1000024_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 29304, "TKFRAME_1000025_AXES     = (       3,        2,  "
	    " 3 )", (ftnlen)132, (ftnlen)53);
    s_copy(lines + 29436, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 29568, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 29700, "TKFRAME_1000001_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 29832, "TKFRAME_1000002_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 29964, "TKFRAME_1000003_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30096, "TKFRAME_1000004_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30228, "TKFRAME_1000005_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30360, "TKFRAME_1000006_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30492, "TKFRAME_1000007_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30624, "TKFRAME_1000008_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30756, "TKFRAME_1000009_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 30888, "TKFRAME_1000010_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31020, "TKFRAME_1000011_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31152, "TKFRAME_1000012_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31284, "TKFRAME_1000013_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31416, "TKFRAME_1000014_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31548, "TKFRAME_1000015_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31680, "TKFRAME_1000016_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31812, "TKFRAME_1000017_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 31944, "TKFRAME_BOYDEN_TOPO_UNITS = 'DEGREES'", (ftnlen)
	    132, (ftnlen)37);
    s_copy(lines + 32076, "TKFRAME_1000019_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32208, "TKFRAME_1000020_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32340, "TKFRAME_1000021_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32472, "TKFRAME_1000022_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32604, "TKFRAME_1000023_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32736, "TKFRAME_1000024_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 32868, "TKFRAME_1000025_UNITS    = 'DEGREES'", (ftnlen)132,
	     (ftnlen)36);
    s_copy(lines + 33000, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 33132, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 33264, "TKFRAME_1000001_ANGLES   = ( 57.9, -124.9, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 33396, "TKFRAME_1000002_ANGLES   = ( -149.0, -125.3, 180 )"
	    , (ftnlen)132, (ftnlen)50);
    s_copy(lines + 33528, "TKFRAME_1000003_ANGLES   = ( -16.4, -41.8, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 33660, "TKFRAME_1000004_ANGLES   = ( 43.2, -112.9, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 33792, "TKFRAME_1000005_ANGLES   = ( 70.7, -123.5, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 33924, "TKFRAME_1000006_ANGLES   = ( -102.8, -65.0, 180 )",
	     (ftnlen)132, (ftnlen)49);
    s_copy(lines + 34056, "TKFRAME_1000007_ANGLES   = ( 78.5, -90.2, 180 )", (
	    ftnlen)132, (ftnlen)47);
    s_copy(lines + 34188, "TKFRAME_1000008_ANGLES   = ( -22.4, -29.6, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 34320, "TKFRAME_1000009_ANGLES   = ( -05.9, -45.4, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 34452, "TKFRAME_1000010_ANGLES   = ( -10.2, -36.5, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 34584, "TKFRAME_1000011_ANGLES   = ( -11.6, -41.9, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 34716, "TKFRAME_1000012_ANGLES   = ( -77.5, -79.8, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 34848, "TKFRAME_1000013_ANGLES   = ( 06.3, -36.6, 180 )", (
	    ftnlen)132, (ftnlen)47);
    s_copy(lines + 34980, "TKFRAME_1000014_ANGLES   = ( -07.8, -45.0, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 35112, "TKFRAME_1000015_ANGLES   = ( -137.6, -53.7, 180 )",
	     (ftnlen)132, (ftnlen)49);
    s_copy(lines + 35244, "TKFRAME_1000016_ANGLES   = ( 66.8, -71.6, 180 )", (
	    ftnlen)132, (ftnlen)47);
    s_copy(lines + 35376, "TKFRAME_1000017_ANGLES   = ( -42.5, -46.3, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 35508, "TKFRAME_1000018_ANGLES   = ( -26.6, -119.0, 180 )",
	     (ftnlen)132, (ftnlen)49);
    s_copy(lines + 35640, "TKFRAME_1000019_ANGLES   = ( 03.7, -49.6, 180 )", (
	    ftnlen)132, (ftnlen)47);
    s_copy(lines + 35772, "TKFRAME_1000020_ANGLES   = ( -09.7, -43.2, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 35904, "TKFRAME_1000021_ANGLES   = ( -00.1, -37.8, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 36036, "TKFRAME_1000022_ANGLES   = ( 111.6, -58.0, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 36168, "TKFRAME_1000023_ANGLES   = ( 116.8, -54.6, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 36300, "TKFRAME_1000024_ANGLES   = ( 116.8, -56.7, 180 )", 
	    (ftnlen)132, (ftnlen)48);
    s_copy(lines + 36432, "TKFRAME_1000025_ANGLES   = ( 77.1, -51.1, 180 )", (
	    ftnlen)132, (ftnlen)47);
    kilfil_("topcentrc.frm", (ftnlen)13);
    tsttxt_("topcentrc.frm", lines, &c__277, &c_true, &c_true, (ftnlen)13, (
	    ftnlen)132);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    angle1[0] = rpd_() * 57.9;
    angle1[1] = rpd_() * -149.;
    angle1[2] = rpd_() * -16.4;
    angle1[3] = rpd_() * 43.2;
    angle1[4] = rpd_() * 70.7;
    angle1[5] = rpd_() * -102.8;
    angle1[6] = rpd_() * 78.5;
    angle1[7] = rpd_() * -22.4;
    angle1[8] = rpd_() * -5.9;
    angle1[9] = rpd_() * -10.2;
    angle1[10] = rpd_() * -11.6;
    angle1[11] = rpd_() * -77.5;
    angle1[12] = rpd_() * 6.3;
    angle1[13] = rpd_() * -7.8;
    angle1[14] = rpd_() * -137.6;
    angle1[15] = rpd_() * 66.8;
    angle1[16] = rpd_() * -42.5;
    angle1[17] = rpd_() * -26.6;
    angle1[18] = rpd_() * 3.7;
    angle1[19] = rpd_() * -9.7;
    angle1[20] = rpd_() * -.1;
    angle1[21] = rpd_() * 111.6;
    angle1[22] = rpd_() * 116.8;
    angle1[23] = rpd_() * 116.8;
    angle1[24] = rpd_() * 77.1;
    angle2[0] = rpd_() * -124.9;
    angle2[1] = rpd_() * -125.3;
    angle2[2] = rpd_() * -41.8;
    angle2[3] = rpd_() * -112.9;
    angle2[4] = rpd_() * -123.5;
    angle2[5] = rpd_() * -65.;
    angle2[6] = rpd_() * -90.2;
    angle2[7] = rpd_() * -29.6;
    angle2[8] = rpd_() * -45.4;
    angle2[9] = rpd_() * -36.5;
    angle2[10] = rpd_() * -41.9;
    angle2[11] = rpd_() * -79.8;
    angle2[12] = rpd_() * -36.6;
    angle2[13] = rpd_() * -45.;
    angle2[14] = rpd_() * -53.7;
    angle2[15] = rpd_() * -71.6;
    angle2[16] = rpd_() * -46.3;
    angle2[17] = rpd_() * -119.;
    angle2[18] = rpd_() * -49.6;
    angle2[19] = rpd_() * -43.2;
    angle2[20] = rpd_() * -37.8;
    angle2[21] = rpd_() * -58.;
    angle2[22] = rpd_() * -54.6;
    angle2[23] = rpd_() * -56.7;
    angle2[24] = rpd_() * -51.1;
    for (i__ = 1; i__ <= 25; ++i__) {
	angle3[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle3", 
		i__1, "f_tkfram__", (ftnlen)804)] = pi_();
    }
    for (i__ = 1; i__ <= 25; ++i__) {
	eul2m_(&angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge(
		"angle1", i__1, "f_tkfram__", (ftnlen)809)], &angle2[(i__2 = 
		i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge("angle2", i__2, 
		"f_tkfram__", (ftnlen)809)], &angle3[(i__3 = i__ - 1) < 405 &&
		 0 <= i__3 ? i__3 : s_rnge("angle3", i__3, "f_tkfram__", (
		ftnlen)809)], &c__3, &c__2, &c__3, &r__[(i__4 = (i__ * 3 + 1) 
		* 3 - 12) < 3645 && 0 <= i__4 ? i__4 : s_rnge("r", i__4, 
		"f_tkfram__", (ftnlen)809)]);
    }
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 25; ++i__) {
	    id = i__ + 1000000;
	    tkfram_(&id, rot, &frame, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_("FRAME", &frame, "=", &c__10013, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    chckad_("ROT", rot, "~", &r__[(i__1 = (i__ * 3 + 1) * 3 - 12) < 
		    3645 && 0 <= i__1 ? i__1 : s_rnge("r", i__1, "f_tkfram__",
		     (ftnlen)821)], &c__9, &c_b427, ok, (ftnlen)3, (ftnlen)1);
	}
    }
    tcase_("Make sure we can get the right answer if we ask for the same fra"
	    "me several times in a row. ", (ftnlen)91);
    for (i__ = 1; i__ <= 3; ++i__) {
	tkfram_(&c_b431, rot, &frame, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("FRAME", &frame, "=", &c__10013, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	chckad_("ROT", rot, "~", &r__[18], &c__9, &c_b427, ok, (ftnlen)3, (
		ftnlen)1);
    }
    tcase_("Make sure we don't overflow the watcher system if we define a ve"
	    "ry large number of frames.", (ftnlen)90);

/*        Fetch the number of POOL variables. */

    szpool_("MAXVAR", &maxvar, &found, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SZPOOL FOUND", &found, &c_true, ok, (ftnlen)12);
    nlines = 17;
    s_copy(lines, "FRAME_#_NAME        = 'TKF_#'", (ftnlen)132, (ftnlen)29);
    s_copy(lines + 132, "FRAME_TKF_#         = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 264, "FRAME_#_CLASS       = 4", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 396, "FRAME_#_CENTER      = 9", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 528, "FRAME_#_CLASS_ID    = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 660, "TKFRAME_#_SPEC      = 'MATRIX'", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 792, "TKFRAME_#_RELATIVE  = 'J2000'", (ftnlen)132, (ftnlen)
	    29);
    s_copy(lines + 924, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1056, "TKFRAME_#_MATRIX    = ( 0.48", (ftnlen)132, (ftnlen)
	    28);
    s_copy(lines + 1188, " 0.60", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 1320, " 0.64", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 1452, "-0.8", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 1584, " 0.0", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 1716, " 0.6", (ftnlen)132, (ftnlen)4);
    s_copy(lines + 1848, " 0.36", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 1980, "-0.80", (ftnlen)132, (ftnlen)5);
    s_copy(lines + 2112, " 0.48 )", (ftnlen)132, (ftnlen)7);

/*        We can fit up to MAXVAR/8 TK frames in the POOL before */
/*        we run out POOL variables. */

    i__1 = maxvar / 8;
    for (i__ = 1; i__ <= i__1; ++i__) {
	id = i__ + 5000000;

/*           Create a frame definition by filling the frame */
/*           ID into the template. */

	i__2 = nlines;
	for (j = 1; j <= i__2; ++j) {
	    repmi_(lines + ((i__3 = j - 1) < 300 && 0 <= i__3 ? i__3 : s_rnge(
		    "lines", i__3, "f_tkfram__", (ftnlen)884)) * 132, "#", &
		    id, buffer + ((i__4 = j - 1) < 30 && 0 <= i__4 ? i__4 : 
		    s_rnge("buffer", i__4, "f_tkfram__", (ftnlen)884)) * 132, 
		    (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge(
		    "buffer", i__3, "f_tkfram__", (ftnlen)887)) * 132, "#", &
		    id, buffer + ((i__4 = j - 1) < 30 && 0 <= i__4 ? i__4 : 
		    s_rnge("buffer", i__4, "f_tkfram__", (ftnlen)887)) * 132, 
		    (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*           Load the frame specification into the kernel pool. */

	lmpool_(buffer, &nlines, (ftnlen)132);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Look up frame data for the current frame. */

	tkfram_(&id, rot, &frame, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (i__ % 200 == 0) {

/*              Clear the kernel pool so we don't run out of */
/*              room for kernel variables before we run out */
/*              of watchers. */

	    clpool_();
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    tcase_("Verify that buffered frame data, for the number of frames equal "
	    "to the TKFRAM buffer size, are updated when watchers indicate a "
	    "kernel pool update ", (ftnlen)147);

/*        Clear the kernel pool, then insert definitions */
/*        of BUFSIZ TK frames into the pool. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlines = 11;
    s_copy(lines, "FRAME_#_NAME        = 'TKF_#'", (ftnlen)132, (ftnlen)29);
    s_copy(lines + 132, "FRAME_TKF_#         = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 264, "FRAME_#_CLASS       = 4", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 396, "FRAME_#_CENTER      = 9", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 528, "FRAME_#_CLASS_ID    = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 660, "TKFRAME_#_SPEC      = 'ANGLES'", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 792, "TKFRAME_#_RELATIVE  = 'J2000'", (ftnlen)132, (ftnlen)
	    29);
    s_copy(lines + 924, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1056, "TKFRAME_#_AXES   = ( 3, 1, 3 )", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 1188, "TKFRAME_#_UNITS  = 'DEGREES'", (ftnlen)132, (ftnlen)
	    28);
    s_copy(lines + 1320, "TKFRAME_#_ANGLES = ( @ @ @ )", (ftnlen)132, (ftnlen)
	    28);
    for (i__ = 1; i__ <= 200; ++i__) {
	id = i__ + 5000000;

/*           Create a frame definition by filling the frame */
/*           ID into the template. */

	i__1 = nlines;
	for (j = 1; j <= i__1; ++j) {
	    repmi_(lines + ((i__2 = j - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge(
		    "lines", i__2, "f_tkfram__", (ftnlen)957)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)957)) * 132, 
		    (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(buffer + ((i__2 = j - 1) < 30 && 0 <= i__2 ? i__2 : s_rnge(
		    "buffer", i__2, "f_tkfram__", (ftnlen)960)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)960)) * 132, 
		    (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*           Give each frame a different orientation. */

	angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle1", 
		i__1, "f_tkfram__", (ftnlen)968)] = (i__ + 100) / 100.;
	angle2[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle2", 
		i__1, "f_tkfram__", (ftnlen)969)] = (i__ + 200) / 100.;
	angle3[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle3", 
		i__1, "f_tkfram__", (ftnlen)970)] = (i__ + 300) / 100.;
	j = 11;
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)973)) * 132, "@", &
		angle1[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle1", i__2, "f_tkfram__", (ftnlen)973)], &c__14, buffer + 
		((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)973)) * 132, (ftnlen)132, (ftnlen)
		1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)974)) * 132, "@", &
		angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle2", i__2, "f_tkfram__", (ftnlen)974)], &c__14, buffer + 
		((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)974)) * 132, (ftnlen)132, (ftnlen)
		1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)975)) * 132, "@", &
		angle3[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle3", i__2, "f_tkfram__", (ftnlen)975)], &c__14, buffer + 
		((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)975)) * 132, (ftnlen)132, (ftnlen)
		1, (ftnlen)132);

/*           Load the frame specification into the kernel pool. */

	lmpool_(buffer, &nlines, (ftnlen)132);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*        Verify that the expected frame definitions are */
/*        available. */

    for (i__ = 1; i__ <= 200; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*              Look up frame data for the current frame. */

	    id = i__ + 5000000;
	    tkfram_(&id, rot, &frame, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Create the expected matrix. */

	    d__1 = angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge(
		    "angle1", i__1, "f_tkfram__", (ftnlen)1001)] * rpd_();
	    d__2 = angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		    "angle2", i__2, "f_tkfram__", (ftnlen)1001)] * rpd_();
	    d__3 = angle3[(i__3 = i__ - 1) < 405 && 0 <= i__3 ? i__3 : s_rnge(
		    "angle3", i__3, "f_tkfram__", (ftnlen)1001)] * rpd_();
	    eul2m_(&d__1, &d__2, &d__3, &c__3, &c__1, &c__3, xrot);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "ROT(#,#) (pass 1)", (ftnlen)132, (ftnlen)17);
	    repmi_(qname, "#", &i__, qname, (ftnlen)132, (ftnlen)1, (ftnlen)
		    132);
	    repmi_(qname, "#", &j, qname, (ftnlen)132, (ftnlen)1, (ftnlen)132)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(qname, rot, "~", xrot, &c__9, &c_b565, ok, (ftnlen)132, (
		    ftnlen)1);
	}
    }

/*        Now update all of the frame definitions. */

    for (i__ = 1; i__ <= 200; ++i__) {
	id = i__ + 5000000;

/*           Create a frame definition by filling the frame */
/*           ID into the template. */

	i__1 = nlines;
	for (j = 1; j <= i__1; ++j) {
	    repmi_(lines + ((i__2 = j - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge(
		    "lines", i__2, "f_tkfram__", (ftnlen)1030)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1030)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(buffer + ((i__2 = j - 1) < 30 && 0 <= i__2 ? i__2 : s_rnge(
		    "buffer", i__2, "f_tkfram__", (ftnlen)1033)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1033)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*           Give each frame a different orientation. */

	angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle1", 
		i__1, "f_tkfram__", (ftnlen)1041)] = (i__ + 100) / 100.;
	angle2[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle2", 
		i__1, "f_tkfram__", (ftnlen)1042)] = (i__ + 400) / 100.;
	angle3[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle3", 
		i__1, "f_tkfram__", (ftnlen)1043)] = (i__ + 800) / 100.;
	j = 11;
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1046)) * 132, "@", &
		angle1[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle1", i__2, "f_tkfram__", (ftnlen)1046)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1046)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1047)) * 132, "@", &
		angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle2", i__2, "f_tkfram__", (ftnlen)1047)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1047)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1048)) * 132, "@", &
		angle3[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle3", i__2, "f_tkfram__", (ftnlen)1048)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1048)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);

/*           Load the frame specification into the kernel pool. */

	lmpool_(buffer, &nlines, (ftnlen)132);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*        Verify that the expected frame definitions are */
/*        available. At this point, we're relying on the */
/*        watchers set in TKFRAM to indicate the need to */
/*        fetch new data from the kernel pool. */

    for (i__ = 1; i__ <= 200; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*              Look up frame data for the current frame. */

	    id = i__ + 5000000;
	    tkfram_(&id, rot, &frame, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Create the expected matrix. */

	    d__1 = angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge(
		    "angle1", i__1, "f_tkfram__", (ftnlen)1077)] * rpd_();
	    d__2 = angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		    "angle2", i__2, "f_tkfram__", (ftnlen)1077)] * rpd_();
	    d__3 = angle3[(i__3 = i__ - 1) < 405 && 0 <= i__3 ? i__3 : s_rnge(
		    "angle3", i__3, "f_tkfram__", (ftnlen)1077)] * rpd_();
	    eul2m_(&d__1, &d__2, &d__3, &c__3, &c__1, &c__3, xrot);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "ROT(#,#) (pass 2)", (ftnlen)132, (ftnlen)17);
	    repmi_(qname, "#", &i__, qname, (ftnlen)132, (ftnlen)1, (ftnlen)
		    132);
	    repmi_(qname, "#", &j, qname, (ftnlen)132, (ftnlen)1, (ftnlen)132)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(qname, rot, "~", xrot, &c__9, &c_b565, ok, (ftnlen)132, (
		    ftnlen)1);
	}
    }
    tcase_("Repeat watcher test for the number of frames greater than the TK"
	    "FRAM buffer size", (ftnlen)80);

/*        Clear the kernel pool, then insert definitions */
/*        of MAXSIZ TK frames into the pool. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlines = 11;
    s_copy(lines, "FRAME_#_NAME        = 'TKF_#'", (ftnlen)132, (ftnlen)29);
    s_copy(lines + 132, "FRAME_TKF_#         = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 264, "FRAME_#_CLASS       = 4", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 396, "FRAME_#_CENTER      = 9", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 528, "FRAME_#_CLASS_ID    = #", (ftnlen)132, (ftnlen)23);
    s_copy(lines + 660, "TKFRAME_#_SPEC      = 'ANGLES'", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 792, "TKFRAME_#_RELATIVE  = 'J2000'", (ftnlen)132, (ftnlen)
	    29);
    s_copy(lines + 924, " ", (ftnlen)132, (ftnlen)1);
    s_copy(lines + 1056, "TKFRAME_#_AXES   = ( 3, 1, 3 )", (ftnlen)132, (
	    ftnlen)30);
    s_copy(lines + 1188, "TKFRAME_#_UNITS  = 'DEGREES'", (ftnlen)132, (ftnlen)
	    28);
    s_copy(lines + 1320, "TKFRAME_#_ANGLES = ( @ @ @ )", (ftnlen)132, (ftnlen)
	    28);
    for (i__ = 1; i__ <= 405; ++i__) {
	id = i__ + 5000000;

/*           Create a frame definition by filling the frame */
/*           ID into the template. */

	i__1 = nlines;
	for (j = 1; j <= i__1; ++j) {
	    repmi_(lines + ((i__2 = j - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge(
		    "lines", i__2, "f_tkfram__", (ftnlen)1129)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1129)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(buffer + ((i__2 = j - 1) < 30 && 0 <= i__2 ? i__2 : s_rnge(
		    "buffer", i__2, "f_tkfram__", (ftnlen)1132)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1132)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*           Give each frame a different orientation. */

	angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle1", 
		i__1, "f_tkfram__", (ftnlen)1140)] = (i__ + 100) / 100.;
	angle2[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle2", 
		i__1, "f_tkfram__", (ftnlen)1141)] = (i__ + 200) / 100.;
	angle3[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle3", 
		i__1, "f_tkfram__", (ftnlen)1142)] = (i__ + 300) / 100.;
	j = 11;
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1145)) * 132, "@", &
		angle1[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle1", i__2, "f_tkfram__", (ftnlen)1145)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1145)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1146)) * 132, "@", &
		angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle2", i__2, "f_tkfram__", (ftnlen)1146)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1146)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1147)) * 132, "@", &
		angle3[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle3", i__2, "f_tkfram__", (ftnlen)1147)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1147)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);

/*           Load the frame specification into the kernel pool. */

	lmpool_(buffer, &nlines, (ftnlen)132);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*        Verify that the expected frame definitions are */
/*        available. */

    for (i__ = 1; i__ <= 405; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*              Look up frame data for the current frame. */

	    id = i__ + 5000000;
	    tkfram_(&id, rot, &frame, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "ROT(#,#) (pass 1)", (ftnlen)132, (ftnlen)17);
	    repmi_(qname, "#", &i__, qname, (ftnlen)132, (ftnlen)1, (ftnlen)
		    132);
	    repmi_(qname, "#", &j, qname, (ftnlen)132, (ftnlen)1, (ftnlen)132)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Writing concatenation */
	    i__5[0] = 132, a__1[0] = qname;
	    i__5[1] = 6, a__1[1] = " FOUND";
	    s_cat(ch__2, a__1, i__5, &c__2, (ftnlen)138);
	    chcksl_(ch__2, &found, &c_true, ok, (ftnlen)138);
	    if (*ok) {

/*                 Create the expected matrix. */

		d__1 = angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : 
			s_rnge("angle1", i__1, "f_tkfram__", (ftnlen)1182)] * 
			rpd_();
		d__2 = angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : 
			s_rnge("angle2", i__2, "f_tkfram__", (ftnlen)1182)] * 
			rpd_();
		d__3 = angle3[(i__3 = i__ - 1) < 405 && 0 <= i__3 ? i__3 : 
			s_rnge("angle3", i__3, "f_tkfram__", (ftnlen)1182)] * 
			rpd_();
		eul2m_(&d__1, &d__2, &d__3, &c__3, &c__1, &c__3, xrot);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_(qname, rot, "~", xrot, &c__9, &c_b565, ok, (ftnlen)
			132, (ftnlen)1);
	    }
	}
    }

/*        Now update all of the frame definitions. */

    for (i__ = 1; i__ <= 405; ++i__) {
	id = i__ + 5000000;

/*           Create a frame definition by filling the frame */
/*           ID into the template. */

	i__1 = nlines;
	for (j = 1; j <= i__1; ++j) {
	    repmi_(lines + ((i__2 = j - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge(
		    "lines", i__2, "f_tkfram__", (ftnlen)1208)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1208)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(buffer + ((i__2 = j - 1) < 30 && 0 <= i__2 ? i__2 : s_rnge(
		    "buffer", i__2, "f_tkfram__", (ftnlen)1211)) * 132, "#", &
		    id, buffer + ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : 
		    s_rnge("buffer", i__3, "f_tkfram__", (ftnlen)1211)) * 132,
		     (ftnlen)132, (ftnlen)1, (ftnlen)132);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*           Give each frame a different orientation. */

	angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle1", 
		i__1, "f_tkfram__", (ftnlen)1219)] = (i__ + 100) / 100.;
	angle2[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle2", 
		i__1, "f_tkfram__", (ftnlen)1220)] = (i__ + 400) / 100.;
	angle3[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge("angle3", 
		i__1, "f_tkfram__", (ftnlen)1221)] = (i__ + 800) / 100.;
	j = 11;
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1224)) * 132, "@", &
		angle1[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle1", i__2, "f_tkfram__", (ftnlen)1224)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1224)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1225)) * 132, "@", &
		angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle2", i__2, "f_tkfram__", (ftnlen)1225)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1225)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);
	repmd_(buffer + ((i__1 = j - 1) < 30 && 0 <= i__1 ? i__1 : s_rnge(
		"buffer", i__1, "f_tkfram__", (ftnlen)1226)) * 132, "@", &
		angle3[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		"angle3", i__2, "f_tkfram__", (ftnlen)1226)], &c__14, buffer 
		+ ((i__3 = j - 1) < 30 && 0 <= i__3 ? i__3 : s_rnge("buffer", 
		i__3, "f_tkfram__", (ftnlen)1226)) * 132, (ftnlen)132, (
		ftnlen)1, (ftnlen)132);

/*           Load the frame specification into the kernel pool. */

	lmpool_(buffer, &nlines, (ftnlen)132);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*        Verify that the expected frame definitions are */
/*        available. At this point, we're relying on the */
/*        watchers set in TKFRAM to indicate the need to */
/*        fetch new data from the kernel pool. */

    for (i__ = 1; i__ <= 405; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*              Look up frame data for the current frame. */

	    id = i__ + 5000000;
	    tkfram_(&id, rot, &frame, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Create the expected matrix. */

	    d__1 = angle1[(i__1 = i__ - 1) < 405 && 0 <= i__1 ? i__1 : s_rnge(
		    "angle1", i__1, "f_tkfram__", (ftnlen)1255)] * rpd_();
	    d__2 = angle2[(i__2 = i__ - 1) < 405 && 0 <= i__2 ? i__2 : s_rnge(
		    "angle2", i__2, "f_tkfram__", (ftnlen)1255)] * rpd_();
	    d__3 = angle3[(i__3 = i__ - 1) < 405 && 0 <= i__3 ? i__3 : s_rnge(
		    "angle3", i__3, "f_tkfram__", (ftnlen)1255)] * rpd_();
	    eul2m_(&d__1, &d__2, &d__3, &c__3, &c__1, &c__3, xrot);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "ROT(#,#) (pass 2)", (ftnlen)132, (ftnlen)17);
	    repmi_(qname, "#", &i__, qname, (ftnlen)132, (ftnlen)1, (ftnlen)
		    132);
	    repmi_(qname, "#", &j, qname, (ftnlen)132, (ftnlen)1, (ftnlen)132)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(qname, rot, "~", xrot, &c__9, &c_b565, ok, (ftnlen)132, (
		    ftnlen)1);
	}
    }

/*     That's all.  Clean up the kernel files we created. */

    kilfil_("phoenix.tk", (ftnlen)10);
    kilfil_("phoenix.prt", (ftnlen)11);
    kilfil_("topcentrc.frm", (ftnlen)13);
    t_success__(ok);
    return 0;
} /* f_tkfram__ */

