/* f_tncnsg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__3 = 3;
static doublereal c_b5 = 0.;
static doublereal c_b8 = -1.;
static doublereal c_b12 = 2.;
static logical c_true = TRUE_;
static doublereal c_b19 = 1.;
static doublereal c_b43 = -3.;
static doublereal c_b46 = 3.;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static doublereal c_b77 = -2.;
static doublereal c_b133 = 100.;
static doublereal c_b134 = -200.;
static doublereal c_b135 = 300.;
static doublereal c_b136 = -7.;
static doublereal c_b137 = 5.;
static integer c__2 = 2;
static doublereal c_b323 = 4.;
static doublereal c_b330 = 6.;
static doublereal c_b361 = -4.;
static doublereal c_b415 = -.5;
static doublereal c_b421 = 1e-12;
static doublereal c_b460 = .5;
static doublereal c_b473 = -5.;
static doublereal c_b523 = 7.;
static doublereal c_b612 = 1.0009999999999999;

/* $Procedure F_TNCNSG ( T_INCNSG tests ) */
/* Subroutine */ int f_tncnsg__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt(doublereal), tan(doublereal);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double cos(doublereal), sin(doublereal);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal dlat, apex[3], targ[3];
    static integer nlat;
    static doublereal span;
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal dist, axis[3], xvec[3], yvec[3], conx[3];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal conz[3];
    extern doublereal vdot_(doublereal *, doublereal *), vsep_(doublereal *, 
	    doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), t_incnsg__(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     doublereal *, doublereal *), mtxv_(doublereal *, doublereal *, 
	    doublereal *);
    static doublereal h__;
    static integer i__, j, k;
    static doublereal r__, s, xxpt1[3], xxpt2[3], angle, z__;
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), tcase_(char *, ftnlen);
    static integer ncone;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static doublereal dspan, pnear[3], theta;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer nspan;
    static char title[320];
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *), topen_(char *, ftnlen);
    static doublereal targy, vtemp[3], xform[9]	/* was [3][3] */;
    extern doublereal vnorm_(doublereal *);
    static doublereal r2;
    extern /* Subroutine */ int t_success__(logical *);
    static integer nxpts;
    static doublereal ep1off[3], ep2off[3], endpt1[3], endpt2[3];
    extern /* Subroutine */ int vlcom3_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal conpt1[3], conpt2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal dp, dalpha;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static doublereal margin, condir[3], xz, offset[3], raydir[3];
    extern /* Subroutine */ int vhatip_(doublereal *);
    static doublereal ep1[3], ep2[3];
    extern /* Subroutine */ int twovec_(doublereal *, integer *, doublereal *,
	     integer *, doublereal *), nplnpt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal vertex[3], convtx[3];
    static integer xnxpts;
    static doublereal lat;
    extern doublereal rpd_(void);
    static doublereal sep, tol, xpt1[3], xpt2[3];

/* $ Abstract */

/*     Exercise the line segment-cone intersection routine T_INCNSG. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the line segment-cone intersection */
/*     routine T_INCNSG. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-AUG-2016 (NJB) */

/*        Added test cases. */

/*        18-MAR-2016 (NJB) */

/*           Removed unneeded declarations. */

/*        24-SEP-2014 (NJB) */

/*           Original version. */
/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_TNCNSG", (ftnlen)8);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Axis is zero vector.", (ftnlen)20);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b5, axis);
    angle = rpd_() * 30.;
    xz = sqrt(3.);
    d__1 = xz / 2;
    vpack_(&c_b8, &c_b5, &d__1, endpt1);
    vpack_(&c_b8, &c_b5, &c_b12, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Angular radius of cone is negative.", (ftnlen)35);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = -1e-12;
    xz = sqrt(3.);
    d__1 = xz / 2;
    vpack_(&c_b8, &c_b5, &d__1, endpt1);
    vpack_(&c_b8, &c_b5, &c_b12, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_true, "SPICE(INVALIDANGLE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Line segment has zero length.", (ftnlen)29);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 30.;
    xz = sqrt(3.);
    d__1 = xz / 2;
    vpack_(&c_b8, &c_b5, &d__1, endpt1);
    vequ_(endpt1, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_true, "SPICE(ENDPOINTSMATCH)", ok, (ftnlen)21);

/*     To be completed: */

/* --- Case: ------------------------------------------------------ */

/*      TITLE =  'Coefficients have absolute values that are too large.' */

/*      CALL TCASE ( TITLE ) */
/* ********************************************************************** */

/*     Non-error exceptional cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is the half-space {Z>=0}. Segment is vertical and intersect"
	    "s cone.", (ftnlen)71);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 90.;
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b46, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is the half-space {Z>=0}. Segment is vertical and misses co"
	    "ne.", (ftnlen)67);
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b77, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space {Z>=0}; cone angle i"
	    "s pi/2 - 1.e-9. Segment is vertical and intersects cone.", (
	    ftnlen)120);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 90. - 1e-9;
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b46, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-7;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-7;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space {Z>=0}; cone angle i"
	    "s pi/2 - 1.e-9. Segment is vertical and misses cone.", (ftnlen)
	    116);
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b77, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space { X: <X-APEX,AXIS>  "
	    ">  0}; cone angle is pi/2 - 1.e-9. Segment is parallel to AXIS a"
	    "nd intersects cone.", (ftnlen)147);
    vpack_(&c_b133, &c_b134, &c_b135, apex);
    vpack_(&c_b136, &c_b137, &c_b19, axis);
    frame_(axis, xvec, yvec);
    angle = rpd_() * 90. - 1e-9;
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &c_b43, axis, vtemp);
    vadd_(vtemp, apex, endpt1);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &c_b46, axis, vtemp);
    vadd_(vtemp, apex, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (up)", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)10, (ftnlen)
	    1);
    z__ = sqrt(5.) * tan(halfpi_() - angle);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (up)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)9, (
	    ftnlen)3);

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (down)", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)12, (
	    ftnlen)1);
    d__1 = -z__;
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &d__1, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (down)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)11, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space { X: <X-APEX,AXIS>  "
	    "<  0}; cone angle is pi/2 + 1.e-9. Segment is parallel to AXIS a"
	    "nd intersects cone.", (ftnlen)147);
    vpack_(&c_b133, &c_b134, &c_b135, apex);
    vpack_(&c_b136, &c_b137, &c_b19, axis);
    frame_(axis, xvec, yvec);
    angle = rpd_() * 90. + 1e-9;
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &c_b43, axis, vtemp);
    vadd_(vtemp, apex, endpt1);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &c_b46, axis, vtemp);
    vadd_(vtemp, apex, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (up)", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)10, (ftnlen)
	    1);
    z__ = sqrt(5.) * tan(halfpi_() - angle);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (up)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)9, (
	    ftnlen)3);

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (down)", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)12, (
	    ftnlen)1);
    d__1 = -z__;
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &d__1, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (down)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)11, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space { X: <X-APEX,AXIS>  "
	    "<  0}; cone angle is pi/2 - 1.e-9. Segment is orthogonal to cone"
	    "'s axis and intersects cone twice.", (ftnlen)162);
    vpack_(&c_b133, &c_b134, &c_b135, apex);
    vpack_(&c_b136, &c_b137, &c_b19, axis);
    frame_(axis, xvec, yvec);
    angle = rpd_() * 90. - 1e-9;

/*     Z is the magnitude of the projections of the endpoints */
/*     onto the cone's axis. */

    z__ = 1e-10;
    vlcom3_(&c_b19, xvec, &c_b77, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, endpt1);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (1)", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Compute expected value of XPT1. */

    r__ = sqrt(5.);
    z__ = 1e-10;
    s = z__ / tan((d__1 = halfpi_() - angle, abs(d__1)));
    d__1 = s * 1. / r__;
    d__2 = s * -2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (1)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);
    d__1 = s * -1. / r__;
    d__2 = s * 2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt2);
    tol = 1e-7;
    chckad_("XPT2 (1)", xpt2, "~~/", xxpt2, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (2)", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)9, (ftnlen)1);
    d__1 = s * -1. / r__;
    d__2 = s * 2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (2)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);
    d__1 = s * 1. / r__;
    d__2 = s * -2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt2);
    chckad_("XPT2 (2)", xpt2, "~~/", xxpt2, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Stretch case: cone is ALMOST the half-space { X: <X-APEX,AXIS>  "
	    "<  0}; cone angle is pi/2 + 1.e-9. Segment is orthogonal to cone"
	    "'s axis and intersects cone twice.", (ftnlen)162);
    vpack_(&c_b133, &c_b134, &c_b135, apex);
    vpack_(&c_b136, &c_b137, &c_b19, axis);
    frame_(axis, xvec, yvec);
    angle = rpd_() * 90. + 1e-9;

/*     Z is the magnitude of the projections of the endpoints */
/*     onto the cone's axis. */

    z__ = -1e-10;
    vlcom3_(&c_b19, xvec, &c_b77, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, endpt1);
    vlcom3_(&c_b8, xvec, &c_b12, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (1)", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)9, (ftnlen)1);

/*     Compute expected value of XPT1. */

    r__ = sqrt(5.);
    z__ = 1e-10;
    s = abs(z__) / tan((d__1 = halfpi_() - angle, abs(d__1)));
    d__1 = s * 1. / r__;
    d__2 = s * -2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (1)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);
    d__1 = s * -1. / r__;
    d__2 = s * 2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt2);
    tol = 1e-7;
    chckad_("XPT2 (1)", xpt2, "~~/", xxpt2, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS (2)", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)9, (ftnlen)1);
    d__1 = s * -1. / r__;
    d__2 = s * 2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt1);
    tol = 1e-7;
    chckad_("XPT1 (2)", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);
    d__1 = s * 1. / r__;
    d__2 = s * -2. / r__;
    vlcom3_(&d__1, xvec, &d__2, yvec, &z__, axis, vtemp);
    vadd_(vtemp, apex, xxpt2);
    chckad_("XPT2 (2)", xpt2, "~~/", xxpt2, &c__3, &tol, ok, (ftnlen)8, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is ALMOST the half-space {Z>=0}; cone angle is pi/2 - 1.e-1"
	    "5. Segment is vertical and intersects cone.", (ftnlen)107);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 90. - 1e-15;
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b46, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b8, &c_b12, &c_b5, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is ALMOST the half-space {Z>=0}; cone angle is pi/2 - 1.e-1"
	    "5. Segment is vertical and misses cone.", (ftnlen)103);
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b77, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is the half-space {X<=5}. Segment is parallel to X axis and"
	    " intersects cone.", (ftnlen)81);
    vpack_(&c_b137, &c_b46, &c_b323, apex);
    vpack_(&c_b8, &c_b5, &c_b5, axis);
    angle = rpd_() * 90.;
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b330, &c_b12, &c_b43, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b137, &c_b12, &c_b43, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    vpack_(&c_b137, &c_b12, &c_b43, xxpt1);
    tol = 1e-12;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is the half-space {X<=5}. Segment is parallel to X axis and"
	    " misses cone.", (ftnlen)77);
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b361, &c_b12, &c_b43, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Cone is ALMOST the half-space {Z>=0}; cone angle is pi/2 - 1.e-7"
	    ". Segment is vertical and intersects cone.", (ftnlen)106);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 90. - 1e-7;
    vpack_(&c_b8, &c_b12, &c_b43, endpt1);
    vpack_(&c_b8, &c_b12, &c_b46, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    d__1 = tan(halfpi_() - angle) * sqrt(5.);
    vpack_(&c_b8, &c_b12, &d__1, xxpt1);
    tol = 1e-8;
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     Repeat call with segment endpoints reversed. */

    t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: cone apex is origin; axis is +Z; angular ra"
	    "dius is 30 degrees; segment's first endpoint is (-1,0,sqrt(3)/2)"
	    "; second endpoint is (0,0,sqrt(3)/2).", (ftnlen)320, (ftnlen)158);
    tcase_(title, (ftnlen)320);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 30.;
    xz = sqrt(3.) / 2;
    vpack_(&c_b8, &c_b5, &xz, endpt1);
    vpack_(&c_b5, &c_b5, &xz, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect one point of intersection to be found. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	vpack_(&c_b415, &c_b5, &xz, xxpt1);
	cleard_(&c__3, xxpt2);
	chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: cone apex is origin; axis is +Z; angular ra"
	    "dius is 30 degrees; segment's first endpoint is (-1,0,sqrt(3)/2)"
	    "; second endpoint is (-1,0,2).", (ftnlen)320, (ftnlen)151);
    tcase_(title, (ftnlen)320);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 30.;
    xz = sqrt(3.);
    d__1 = xz / 2;
    vpack_(&c_b8, &c_b5, &d__1, endpt1);
    vpack_(&c_b8, &c_b5, &c_b12, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect one point of intersection to be found. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	vpack_(&c_b8, &c_b5, &xz, xxpt1);
	cleard_(&c__3, xxpt2);
	chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: cone apex is origin; axis is +Z; angular ra"
	    "dius is 30 degrees; segment's first endpoint is (-1,0,sqrt(3)/2)"
	    "; second endpoint is (1,0,sqrt(3)/2).", (ftnlen)320, (ftnlen)158);
    tcase_(title, (ftnlen)320);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 30.;
    xz = sqrt(3.) / 2;
    vpack_(&c_b8, &c_b5, &xz, endpt1);
    vpack_(&c_b19, &c_b5, &xz, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two points of intersection to be found. */

    xnxpts = 2;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	vpack_(&c_b415, &c_b5, &xz, xxpt1);
	vpack_(&c_b460, &c_b5, &xz, xxpt2);
	chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
	chckad_("XPT2", xpt2, "~~/", xxpt2, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Repeat previous case but offset both the apex and ray's v"
	    "ertex.", (ftnlen)320, (ftnlen)63);
    tcase_(title, (ftnlen)320);
    vpack_(&c_b46, &c_b323, &c_b473, apex);
    angle = rpd_() * 30.;
    xz = sqrt(3.) / 2;
    vpack_(&c_b8, &c_b5, &xz, endpt1);
    vadd_(endpt1, apex, vtemp);
    vequ_(vtemp, endpt1);
    vpack_(&c_b19, &c_b5, &xz, endpt2);
    vadd_(endpt2, apex, vtemp);
    vequ_(vtemp, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two points of intersection to be found. */

    xnxpts = 2;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	vpack_(&c_b415, &c_b5, &xz, xxpt1);
	vadd_(xxpt1, apex, vtemp);
	vequ_(vtemp, xxpt1);
	vpack_(&c_b460, &c_b5, &xz, xxpt2);
	vadd_(xxpt2, apex, vtemp);
	vequ_(vtemp, xxpt2);
	chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
	chckad_("XPT2", xpt2, "~~/", xxpt2, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Cone angle exceeds pi/2: cone apex is origin; axis is +Z;"
	    " angular radius is 150 degrees; segment's first endpoint is (-1,"
	    "0,-sqrt(3)/2); second endpoint is (1,0,-sqrt(3)/2).", (ftnlen)320,
	     (ftnlen)172);
    tcase_(title, (ftnlen)320);
    cleard_(&c__3, apex);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    angle = rpd_() * 150.;
    xz = -sqrt(3.) / 2;
    vpack_(&c_b8, &c_b5, &xz, endpt1);
    vpack_(&c_b19, &c_b5, &xz, endpt2);
    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two points of intersection to be found. */

    xnxpts = 2;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	vpack_(&c_b415, &c_b5, &xz, xxpt1);
	vpack_(&c_b460, &c_b5, &xz, xxpt2);
	chckad_("XPT1", xpt1, "~~/", xxpt1, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
	chckad_("XPT2", xpt2, "~~/", xxpt2, &c__3, &c_b421, ok, (ftnlen)4, (
		ftnlen)3);
    }
/* ********************************************************************** */

/*     Normal cases --- varied geometry */

/* ********************************************************************** */

/*     The following are intersection cases. */

/*     All intersection cases for lines that are not orthogonal to the */
/*     cone's axis can be characterized by: */

/*        1) The line's intersection with the plane containing the apex */
/*           and normal to the axis. This point of intersection is the */
/*           vertex of a ray contained in the line and intersecting the */
/*           cone. */

/*        2) The ray's angle relative to the plane described in (1). */

/*        3) The azimuth of the ray relative to the line segment starting */
/*           at the ray's vertex and ending at the cone's apex. */


/*     Pick an arbitrary cone axis and apex. */


    vpack_(&c_b46, &c_b77, &c_b523, apex);
    vpack_(&c_b12, &c_b46, &c_b77, axis);
    vpack_(&c_b5, &c_b5, &c_b19, axis);
    vhat_(axis, conz);

/*     Pick an arbitrary ray vertex in the X-Y plane. */

    theta = rpd_() * 27.;
    r__ = 1e3;
    d__1 = r__ * cos(theta);
    d__2 = r__ * sin(theta);
    vpack_(&d__1, &d__2, &c_b5, vtemp);

/*     Create the transformation matrix for mapping vectors */
/*     from the standard frame to the cone frame. */

    twovec_(conz, &c__3, vtemp, &c__1, xform);
    for (i__ = 1; i__ <= 3; ++i__) {
	conx[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("conx", i__1, 
		"f_tncnsg__", (ftnlen)1189)] = xform[(i__2 = i__ * 3 - 3) < 9 
		&& 0 <= i__2 ? i__2 : s_rnge("xform", i__2, "f_tncnsg__", (
		ftnlen)1189)];
    }
    vscl_(&r__, conx, convtx);

/*     Loop over cone angles. */

    ncone = 10;
    dalpha = halfpi_() / (ncone + 1);

/*     Note: NLAT can be increased to 100 without an excessive */
/*     increase in run time. However, the value 10 exercises a */
/*     difficult numerical case: that in which the ray is normal */
/*     to the cone at the first intercept. */

    nlat = 10;
    dlat = halfpi_() / (nlat + 1);

/*     Let the cone angles range from DALPHA to pi/2 - DALPHA. */

    i__1 = ncone;
    for (i__ = 1; i__ <= i__1; ++i__) {
	angle = i__ * dalpha;

/*        Let the ray "latitude" vary from DLAT to pi/2 - DLAT. Note */
/*        that this is an approximation unless the ray's direction */
/*        vector has a zero Y component. */

	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = j * dlat;

/*           Let H be the Z intercept of a ray emanating from CONVTX, */
/*           pointing toward the +Z axis, and making angle LAT relative */
/*           to the X-Y plane. */

	    h__ = r__ * tan(lat);

/*           Let SPAN be the length of a line segment parallel to the Y */
/*           axis and passing through the +Z axis at height H. */

	    span = h__ * 2. * tan(angle);

/*           We'll place a "target" on the segment from which SPAN was */
/*           derived. */

	    nspan = 11;
	    dspan = span / (nspan - 1);
	    i__3 = nspan;
	    for (k = 1; k <= i__3; ++k) {
		targy = -span / 2 + (k - 1) * dspan;
		if (k == 1 || k == nspan) {

/*                 Contract TARGY a bit, since these are formally */
/*                 tangent cases, and we want to ensure a hit. */

		    targy *= .999;
		}
		vpack_(&c_b5, &targy, &h__, targ);
		vsub_(targ, convtx, condir);
		vhatip_(condir);
		vequ_(convtx, conpt1);

/*              Let R2 be the distance of the second endpoint from */
/*              the vertex. */

		r2 = 1e5;
		vlcom_(&c_b19, conpt1, &r2, condir, conpt2);

/*              Shift the endpoints to make them relative to the origin */
/*              of the standard frame. Convert the endpoints back to the */
/*              standard basis. */

		mtxv_(xform, conpt1, vtemp);
		vadd_(vtemp, apex, endpt1);
		mtxv_(xform, conpt2, vtemp);
		vadd_(vtemp, apex, endpt2);
		mtxv_(xform, convtx, vtemp);
		vadd_(vtemp, apex, vertex);
		mtxv_(xform, targ, vtemp);
		vadd_(vtemp, apex, targ);
		mtxv_(xform, condir, vtemp);
		vequ_(vtemp, raydir);

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Intercept case: I = #; J = #; K = #", (ftnlen)
			320, (ftnlen)35);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		tcase_(title, (ftnlen)320);
		t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_("NXPTS", &nxpts, ">", &c__0, &c__0, ok, (ftnlen)5, (
			ftnlen)1);
		if (nxpts >= 1) {

/*                 Check XPT1. */

/*                 Make sure XPT1 is on the ray containing the */
/*                 line segment. */

		    nplnpt_(vertex, raydir, xpt1, pnear, &dist);
		    tol = 1e-10;
		    chckad_("PNEAR", pnear, "~~/", xpt1, &c__3, &tol, ok, (
			    ftnlen)5, (ftnlen)3);

/*                 Make sure XPT1 is on the correct side of the ray's */
/*                 vertex. */

		    vsub_(xpt1, vertex, ep1off);
		    dp = vdot_(ep1off, raydir);
		    chcksd_("DP 1", &dp, ">", &c_b5, &c_b5, ok, (ftnlen)4, (
			    ftnlen)1);

/*                 Make sure XPT1 is on the cone's surface. */

		    vsub_(xpt1, apex, offset);
		    sep = vsep_(offset, axis);
		    tol = 1e-10;
		    chcksd_("XPT1 SEP", &sep, "~", &angle, &tol, ok, (ftnlen)
			    8, (ftnlen)1);
		    if (nxpts == 2) {

/*                    Check XPT2. */

/*                    Make sure XPT2 is on the ray containing the */
/*                    line segment. */

			nplnpt_(vertex, raydir, xpt2, pnear, &dist);
			tol = 1e-10;
			chckad_("PNEAR", pnear, "~~/", xpt2, &c__3, &tol, ok, 
				(ftnlen)5, (ftnlen)3);

/*                    Make sure XPT2 is on the correct side of the ray's */
/*                    vertex. */

			vsub_(xpt2, vertex, ep2off);
			dp = vdot_(ep2off, raydir);
			chcksd_("DP 2", &dp, ">", &c_b5, &c_b5, ok, (ftnlen)4,
				 (ftnlen)1);

/*                    Check that XPT2 is farther from VERTEX than XPT1. */

			d__1 = vnorm_(ep2off);
			d__2 = vnorm_(ep1off);
			chcksd_("XPT2 offset", &d__1, ">", &d__2, &c_b5, ok, (
				ftnlen)11, (ftnlen)1);

/*                    Make sure XPT2 is on the cone's surface. */

			vsub_(xpt2, apex, offset);
			sep = vsep_(offset, axis);
			tol = 1e-10;
			chcksd_("XPT2 SEP", &sep, "~", &angle, &tol, ok, (
				ftnlen)8, (ftnlen)1);
		    }
		}

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Intercept case with reversed endpoints: I = #"
			"; J = #; K = #", (ftnlen)320, (ftnlen)59);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		tcase_(title, (ftnlen)320);
		t_incnsg__(apex, axis, &angle, endpt2, endpt1, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_("NXPTS", &nxpts, ">", &c__0, &c__0, ok, (ftnlen)5, (
			ftnlen)1);
		if (nxpts >= 1) {

/*                 Check XPT1. */

/*                 Make sure XPT1 is on the ray containing the */
/*                 line segment. */

		    nplnpt_(vertex, raydir, xpt1, pnear, &dist);
		    tol = 1e-9;
		    chckad_("PNEAR", pnear, "~~/", xpt1, &c__3, &tol, ok, (
			    ftnlen)5, (ftnlen)3);

/*                 Make sure XPT1 is on the correct side of the ray's */
/*                 vertex. */

		    vsub_(xpt1, vertex, ep1off);
		    dp = vdot_(ep1off, raydir);
		    chcksd_("DP 1", &dp, ">", &c_b5, &c_b5, ok, (ftnlen)4, (
			    ftnlen)1);

/*                 Make sure XPT1 is on the cone's surface. */

		    vsub_(xpt1, apex, offset);
		    sep = vsep_(offset, axis);
		    tol = 1e-9;
		    chcksd_("XPT1 SEP", &sep, "~", &angle, &tol, ok, (ftnlen)
			    8, (ftnlen)1);
		    if (nxpts == 2) {

/*                    Check XPT2. */

/*                    Make sure XPT2 is on the ray containing the */
/*                    line segment. */

			nplnpt_(vertex, raydir, xpt2, pnear, &dist);
			tol = 1e-10;
			chckad_("PNEAR", pnear, "~~/", xpt2, &c__3, &tol, ok, 
				(ftnlen)5, (ftnlen)3);

/*                    Make sure XPT2 is on the correct side of the ray's */
/*                    vertex. */

			vsub_(xpt2, vertex, ep2off);
			dp = vdot_(ep2off, raydir);
			chcksd_("DP 2", &dp, ">", &c_b5, &c_b5, ok, (ftnlen)4,
				 (ftnlen)1);

/*                    Check that XPT1 is farther from VERTEX than XPT2. */

			d__1 = vnorm_(ep1off);
			d__2 = vnorm_(ep2off);
			chcksd_("XPT1 offset", &d__1, ">", &d__2, &c_b5, ok, (
				ftnlen)11, (ftnlen)1);

/*                    Make sure XPT2 is on the cone's surface. */

			vsub_(xpt2, apex, offset);
			sep = vsep_(offset, axis);
			tol = 1e-9;
			chcksd_("XPT2 SEP", &sep, "~", &angle, &tol, ok, (
				ftnlen)8, (ftnlen)1);
		    }
		}

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Intercept case with second endpoint in cone's"
			" interior. I = #; J = #; K = #", (ftnlen)320, (ftnlen)
			75);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		tcase_(title, (ftnlen)320);

/*              Find the first intercept. Extend the segment slightly */
/*              to create the second endpoint. */

		t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_("(0) NXPTS", &nxpts, ">", &c__0, &c__0, ok, (ftnlen)9,
			 (ftnlen)1);
		vsub_(xpt1, endpt1, ep1off);
		vlcom_(&c_b19, endpt1, &c_b612, ep1off, ep2);

/*              On this second call to T_INCNSG, use the new value */
/*              of the second endpoint. */

		t_incnsg__(apex, axis, &angle, endpt1, ep2, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              We expect exactly one hit. */

		chcksi_("(1) NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)9,
			 (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Intercept case with first endpoint in cone's "
			"interior. The endpoints are reversed relative to tho"
			"se of the previous case. I = #; J = #; K = #.", (
			ftnlen)320, (ftnlen)142);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		tcase_(title, (ftnlen)320);

/*              Find the first intercept. Extend the segment slightly */
/*              to create the second endpoint. */

		t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_("(0) NXPTS", &nxpts, ">", &c__0, &c__0, ok, (ftnlen)9,
			 (ftnlen)1);
		vsub_(xpt1, endpt1, ep1off);
		vlcom_(&c_b19, endpt1, &c_b612, ep1off, ep2);

/*              On this second call to T_INCNSG, use the new value */
/*              of the second endpoint as the first endpoint. */

		t_incnsg__(apex, axis, &angle, ep2, endpt1, &nxpts, xpt1, 
			xpt2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              We expect exactly one hit. */

		chcksi_("(1) NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)9,
			 (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

		if (k == (nspan + 1) / 2) {

/*                 Perform this test only for rays contained in the */
/*                 plane containing ENDPT1 and AXIS; this simplifies */
/*                 the geometry. */

		    s_copy(title, "Intercept case with first endpoint in con"
			    "e's interior. I = #; J = #; K = #", (ftnlen)320, (
			    ftnlen)74);
		    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    tcase_(title, (ftnlen)320);

/*                 Find the first intercept. Extend the segment slightly */
/*                 to create the new value of the first endpoint. */

		    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, 
			    xpt1, xpt2);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksi_("(0) NXPTS", &nxpts, ">", &c__0, &c__0, ok, (
			    ftnlen)9, (ftnlen)1);
		    vsub_(xpt1, endpt1, ep1off);
		    vlcom_(&c_b19, endpt1, &c_b612, ep1off, ep1);

/*                 On this second call to T_INCNSG, use the new value of */
/*                 the first endpoint. */

		    t_incnsg__(apex, axis, &angle, ep1, endpt2, &nxpts, xpt1, 
			    xpt2);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    margin = 1e-10;
		    if (lat < halfpi_() - angle - margin) {

/*                    We expect exactly one hit. */

			chcksi_("(1) NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)9, (ftnlen)1);
		    } else if (lat > halfpi_() - angle + margin) {

/*                    There should be no hits; the ray rises more steeply */
/*                    than the cone. */

			chcksi_("(1) NXPTS", &nxpts, "=", &c__0, &c__0, ok, (
				ftnlen)9, (ftnlen)1);
		    }

/* --- Case: ------------------------------------------------------ */

/*                 Perform this test only for rays contained in the */
/*                 plane containing ENDPT1 and AXIS; this simplifies */
/*                 the geometry. */

		    s_copy(title, "Intercept case with second endpoint in co"
			    "ne's interior. This is a repeat of the previous "
			    "case, with endpoints switched. I = #; J = #; K ="
			    " #", (ftnlen)320, (ftnlen)139);
		    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    tcase_(title, (ftnlen)320);

/*                 On this second call to T_INCNSG, use the new value of */
/*                 the first endpoint. */

		    t_incnsg__(apex, axis, &angle, endpt2, ep1, &nxpts, xpt1, 
			    xpt2);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    margin = 1e-10;
		    if (lat < halfpi_() - angle - margin) {

/*                    We expect exactly one hit. */

			chcksi_("(1) NXPTS", &nxpts, "=", &c__1, &c__0, ok, (
				ftnlen)9, (ftnlen)1);
		    } else if (lat > halfpi_() - angle + margin) {

/*                    There should be no hits; the ray rises more steeply */
/*                    than the cone. */

			chcksi_("(1) NXPTS", &nxpts, "=", &c__0, &c__0, ok, (
				ftnlen)9, (ftnlen)1);
		    }
		}
	    }

/* --- Case: ------------------------------------------------------ */

/*           Miss case: make the target too far off in the Y direction */
/*           for an intercept to exist. */

	    k = 0;
	    targy = -span / 2 + (k - 1) * dspan;
	    s_copy(title, "Non-intercept case: Y < -SPAN/2. I = #; J = #; K "
		    "= #", (ftnlen)320, (ftnlen)52);
	    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    tcase_(title, (ftnlen)320);
	    vpack_(&c_b5, &targy, &h__, targ);
	    vsub_(targ, convtx, condir);
	    vhatip_(condir);
	    vequ_(convtx, conpt1);

/*           Let R2 be the distance of the second endpoint from */
/*           the vertex. */

	    r2 = 1e5;
	    vlcom_(&c_b19, conpt1, &r2, condir, conpt2);

/*           Shift the endpoints to make them relative to the origin */
/*           of the standard frame. Convert the endpoints back to the */
/*           standard basis. */

	    mtxv_(xform, conpt1, vtemp);
	    vadd_(vtemp, apex, endpt1);
	    mtxv_(xform, conpt2, vtemp);
	    vadd_(vtemp, apex, endpt2);
	    mtxv_(xform, targ, vtemp);
	    vadd_(vtemp, apex, targ);
	    mtxv_(xform, convtx, vtemp);
	    vadd_(vtemp, apex, vertex);
	    mtxv_(xform, raydir, vtemp);
	    vequ_(vtemp, raydir);
	    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect a miss. */

	    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

/*           Miss case: make the target too far off in the Y direction */
/*           for an intercept to exist. */

	    k = nspan + 1;
	    targy = (doublereal) (nspan / 2) * dspan;
	    s_copy(title, "Non-intercept case: Y > SPAN/2. I = #; J = #; K ="
		    " #", (ftnlen)320, (ftnlen)51);
	    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    tcase_(title, (ftnlen)320);
	    vpack_(&c_b5, &targy, &h__, targ);
	    vsub_(targ, convtx, raydir);
	    vhatip_(raydir);
	    vequ_(convtx, conpt1);

/*           Let R2 be the distance of the second endpoint from */
/*           the vertex. */

	    r2 = 1e5;
	    vlcom_(&c_b19, conpt1, &r2, condir, conpt2);

/*           Shift the endpoints to make them relative to the origin */
/*           of the standard frame. Convert the endpoints back to the */
/*           standard basis. */

	    mtxv_(xform, conpt1, vtemp);
	    vadd_(vtemp, apex, endpt1);
	    mtxv_(xform, conpt2, vtemp);
	    vadd_(vtemp, apex, endpt2);
	    mtxv_(xform, targ, vtemp);
	    vadd_(vtemp, apex, targ);
	    mtxv_(xform, convtx, vtemp);
	    vadd_(vtemp, apex, vertex);
	    mtxv_(xform, raydir, vtemp);
	    vequ_(vtemp, raydir);
	    t_incnsg__(apex, axis, &angle, endpt1, endpt2, &nxpts, xpt1, xpt2)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect a miss. */

	    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_tncnsg__ */

