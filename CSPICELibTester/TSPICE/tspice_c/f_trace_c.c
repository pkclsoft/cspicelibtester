/*

-Procedure f_trace_c ( Test wrappers for traceback routines )

 
-Abstract
 
   Perform tests on CSPICE wrappers for the traceback API
   routines.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include <string.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_trace_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for a subset of the CSPICE traceback
   subsystem routines. 
   
   The subset is:
      
      qcktrc_c
      trcdep_c
      trcnam_c
       
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
 
-Version

   -tspice_c Version 1.0.0 24-MAR-2014 (NJB)  

-Index_Entries

   test wrappers of traceback routines

-&
*/

{ /* Begin f_trace_c */

 
   /*
   Constants
   */
   #define                 MAXN      SPICE_ERROR_MAXMOD
   #define                 TLEN      33
   /*
   Local variables
   */   
   SpiceChar               modnam  [ SPICE_ERROR_MODLEN ];
   SpiceChar               names   [ MAXN ][ SPICE_ERROR_MODLEN ];
   SpiceChar               title   [ TLEN ];
   SpiceChar               trace   [ SPICE_ERROR_TRCLEN ];
   SpiceChar               xnames  [ MAXN ][ SPICE_ERROR_MODLEN ];
   SpiceChar               xtrace  [ SPICE_ERROR_TRCLEN ];
 
   SpiceInt                depth;
   SpiceInt                i;
   SpiceInt                inidep;
   SpiceInt                n;
   SpiceInt                xdepth;

   
   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_trace_c" );
 

   /*
    ******************************************************************
    *
    *  Normal cases
    *
    ******************************************************************
    */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Get the initial traceback depth; fetch the checked-in "
             "module names individually."                            );
 
   trcdep_c ( &inidep );

   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "depth", inidep, "=", 1, 0, ok );

   for ( i = 0; i < inidep; i++ )
   {
      trcnam_c ( i, SPICE_ERROR_MODLEN, modnam );
      chckxc_c ( SPICEFALSE, " ", ok );
 
      /*
      printf ( "%ld: %s\n", (long)i, modnam );
      */
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Stack (append) a set of names, then check the stack. "
             "Use trcnam_c to retrieve names."                       );

   n = MAXN-inidep;

   for ( i = 0; i < n; i++ )
   {      
      sprintf ( xnames[i], "module________________________%ld", (long)i );

      chkin_c ( xnames[i] );
   }

   /*
   Check the current trace depth. 
   */
   xdepth = inidep + n;
   
   trcdep_c ( &depth );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "depth", depth, "=", xdepth, 0, ok );

   /*
   Capture the names on the trace stack. 
   */
   for ( i = 0; i < n; i++ )
   {      
      trcnam_c ( i + inidep, SPICE_ERROR_MODLEN, names[i] );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Pop the names we added before proceeding. 
   */
   for ( i = n; i > 0; i-- )
   {
      chkout_c ( xnames[i-1] );
   }

   /*
   Check the names we retrieved from the trace stack. 
   */
   for ( i = 0; i < n; i++ )
   {      
      sprintf  ( title, "name %ld", (long)i );
      chcksc_c ( title, names[i], "=", xnames[i], ok );
 
      /*
      printf ( "%s\n", names[i] );
      */      
   }


   /*
   Make sure the stack has been returned to its initial state. 
   */
   trcdep_c ( &depth );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "post-test depth", depth, "=", inidep, 0, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Stack (append) a set of names, then check the stack. "
             "Use qcktrc_c to retrieve the trace."                   );



   n = MAXN-inidep;

   for ( i = 0; i < n; i++ )
   {      
      chkin_c ( xnames[i] );
   }

   qcktrc_c ( SPICE_ERROR_TRCLEN, trace );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Pop the names we added before proceeding. 
   */
   for ( i = n; i > 0; i-- )
   {
      chkout_c ( xnames[i-1] );
   }

   /*
   Check the traceback string. We'll construct an expected string
   first. 

   Note that any modules checked in before this routine was called 
   must be included in the expected traceback.
   */
   xtrace[0] = (char)0;

   for ( i = 0; i < inidep; i++ )
   {      
      trcnam_c ( i, SPICE_ERROR_MODLEN, modnam );
      chckxc_c ( SPICEFALSE, " ", ok );

      strncat ( xtrace, modnam, SPICE_ERROR_MODLEN );

      strncat ( xtrace, " --> ", 5 );
   }


   for ( i = 0; i < n-1; i++ )
   {      
      strncat ( xtrace, xnames[i], SPICE_ERROR_MODLEN );

      strncat ( xtrace, " --> ", 5 );
   }
   strncat ( xtrace, xnames[n-1], SPICE_ERROR_MODLEN );

   /*
   printf ( "%s\n", xtrace );
   */

   chcksc_c ( "full traceback", trace, "=", xtrace, ok );


   /*
    ******************************************************************
    *
    *  Error cases
    *
    ******************************************************************
    */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "trcnam_c: Invalid module index." );


   trcnam_c ( -1, SPICE_ERROR_MODLEN, modnam );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );


   n = MAXN-inidep;

   for ( i = 0; i < n; i++ )
   {      
      chkin_c ( xnames[i] );
   }

   trcnam_c ( SPICE_ERROR_MAXMOD, SPICE_ERROR_MODLEN, modnam );
   chckxc_c ( SPICETRUE, "SPICE(INDEXOUTOFRANGE)", ok );

   for ( i = n; i > 0; i-- )
   {      
      chkout_c ( xnames[i-1] );
   }


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "trcnam_c: null output string." );

   chkin_c ( xnames[0] );
   
   trcnam_c ( inidep, SPICE_ERROR_MODLEN, NULL );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   chkout_c ( xnames[0] );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "trcnam_c: zero-length output string." );

   chkin_c ( xnames[0] );
   
   trcnam_c ( inidep, 0, modnam );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );

   chkout_c ( xnames[0] );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "qcktrc_c: null output string." );

   chkin_c ( xnames[0] );
   
   qcktrc_c ( SPICE_ERROR_TRCLEN, NULL );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   chkout_c ( xnames[0] );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "qcktrc_c: zero-length output string." );

   chkin_c ( xnames[0] );
   
   qcktrc_c ( 0, trace );
   chckxc_c ( SPICETRUE, "SPICE(STRINGTOOSHORT)", ok );

   chkout_c ( xnames[0] );




   /*
    ******************************************************************
    *
    *  Clean-up
    *
    ******************************************************************
    */

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Make sure the stack has been returned to its initial state." );


   trcdep_c ( &depth );
   chckxc_c ( SPICEFALSE, " ", ok );

   chcksi_c ( "post-test depth", depth, "=", inidep, 0, ok );


   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_trace_c */

