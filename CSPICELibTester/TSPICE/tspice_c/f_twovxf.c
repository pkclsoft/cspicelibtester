/* f_twovxf.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 1.;
static doublereal c_b7 = -1.;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__9 = 9;
static doublereal c_b115 = 1e-14;
static integer c__3 = 3;
static integer c__36 = 36;
static doublereal c_b143 = 10.;

/* $Procedure F_TWOVXF ( TWOVXF tests ) */
/* Subroutine */ int f_twovxf__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };
    static integer prev[3] = { 3,1,2 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    integer seed;
    extern doublereal vsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    integer i__, j;
    doublereal r__[9]	/* was [3][3] */;
    integer ncase;
    extern /* Subroutine */ int tcase_(char *, ftnlen), zztwovxf_(doublereal *
	    , integer *, doublereal *, integer *, doublereal *), ident_(
	    doublereal *), vpack_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), dvhat_(doublereal *, doublereal *), repmi_(char *, 
	    char *, integer *, char *, ftnlen, ftnlen, ftnlen);
    char title[240];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int t_success__(logical *);
    doublereal pscal1, pscal2;
    integer index1;
    doublereal vscal1, vscal2;
    integer index2;
    doublereal state1[6], state2[6];
    extern /* Subroutine */ int xf2rav_(doublereal *, doublereal *, 
	    doublereal *), chckad_(char *, doublereal *, char *, doublereal *,
	     integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal lb, dr[9]	/* was [3][3] */, av[3], ub, uxsta2[6];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen);
    integer caseno;
    doublereal xr[9]	/* was [3][3] */;
    extern /* Subroutine */ int ducrss_(doublereal *, doublereal *, 
	    doublereal *);
    doublereal xformi[36]	/* was [6][6] */;
    extern /* Subroutine */ int twovec_(doublereal *, integer *, doublereal *,
	     integer *, doublereal *), invstm_(doublereal *, doublereal *);
    doublereal xxform[36]	/* was [6][6] */, uxstat[6];
    extern /* Subroutine */ int twovxf_(doublereal *, integer *, doublereal *,
	     integer *, doublereal *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    doublereal xav[3];

/* $ Abstract */

/*     Exercise the SPICELIB two-state frame transformation */
/*     routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routines */

/*        TWOVXF */
/*        ZZTWOVXF */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-APR-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */

/*      DOUBLE PRECISION      TIGHT */
/*      PARAMETER           ( TIGHT  = 1.D-12 ) */

/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Open the test family. */

    topen_("F_TWOVXF", (ftnlen)8);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("TWOVXF: Bad index", (ftnlen)17);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    vpack_(&c_b6, &c_b4, &c_b6, state2);
    vpack_(&c_b4, &c_b4, &c_b7, &state2[3]);
    index1 = 0;
    index2 = 2;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 4;
    index2 = 2;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 1;
    index2 = 0;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 1;
    index2 = 4;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("TWOVXF: indices match", (ftnlen)21);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    vpack_(&c_b6, &c_b4, &c_b6, state2);
    vpack_(&c_b4, &c_b4, &c_b7, &state2[3]);
    index1 = 1;
    index2 = 1;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(UNDEFINEDFRAME)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("TWOVXF: linearly dependent positions", (ftnlen)36);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    index1 = 1;
    index2 = 2;
    twovxf_(state1, &index1, state1, &index2, xform);
    chckxc_(&c_true, "SPICE(DEPENDENTVECTORS)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTWOVXF: Bad index", (ftnlen)19);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    vpack_(&c_b6, &c_b4, &c_b6, state2);
    vpack_(&c_b4, &c_b4, &c_b7, &state2[3]);
    index1 = 0;
    index2 = 2;
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 4;
    index2 = 2;
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 1;
    index2 = 0;
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);
    index1 = 1;
    index2 = 4;
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(BADINDEX)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTWOVXF: indices match", (ftnlen)23);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    vpack_(&c_b6, &c_b4, &c_b6, state2);
    vpack_(&c_b4, &c_b4, &c_b7, &state2[3]);
    index1 = 1;
    index2 = 1;
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_true, "SPICE(UNDEFINEDFRAME)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTWOVXF: linearly dependent positions", (ftnlen)38);
    vpack_(&c_b4, &c_b4, &c_b6, state1);
    vpack_(&c_b7, &c_b4, &c_b4, &state1[3]);
    index1 = 1;
    index2 = 2;
    zztwovxf_(state1, &index1, state1, &index2, xform);
    chckxc_(&c_true, "SPICE(DEPENDENTVECTORS)", ok, (ftnlen)23);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("TWOVXF: identity orientation, rotation about Z", (ftnlen)46);
    vpack_(&c_b6, &c_b4, &c_b4, state1);
    vpack_(&c_b4, &c_b6, &c_b4, &state1[3]);
    vpack_(&c_b4, &c_b6, &c_b4, state2);
    vpack_(&c_b7, &c_b4, &c_b4, &state2[3]);
    index1 = 1;
    index2 = 2;
    twovxf_(state1, &index1, state2, &index2, xform);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ident_(xr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xf2rav_(xform, r__, av);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("R", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)1, (ftnlen)1);

/*     Construct the expected angular velocity vector of */
/*     the reference frame constructed by TWOVXF. Units */
/*     are radians/s. */

    vpack_(&c_b4, &c_b4, &c_b6, xav);
    chckad_("AV", av, "~", xav, &c__3, &c_b115, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTWOVXF: identity orientation, rotation about Z", (ftnlen)48);

/*     Compare against results from TWOVXF. The matrix constructed */
/*     by ZZTWOVXF should be the inverse of that produced by TWOVXF. */

    invstm_(xform, xxform);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztwovxf_(state1, &index1, state2, &index2, xform);
    chckad_("XFORM", xform, "~", xxform, &c__36, &c_b115, ok, (ftnlen)5, (
	    ftnlen)1);
/* ********************************************************************** */

/*     Normal cases using pseudo-random vectors */

/* ********************************************************************** */
    tcase_("Random setup", (ftnlen)12);

/*     Initialize the pseudo-random number generator. */

    seed = -1;

/*     UB and LB are bounds for the exponent scale. */

    ub = 150.;
    lb = -150.;
    ncase = 1000;
    caseno = 0;
    i__1 = ncase;
    for (j = 1; j <= i__1; ++j) {

/*        Loop over the index of the primary vector. */

	for (index1 = 1; index1 <= 3; ++index1) {
	    ++caseno;

/*           Let the secondary vector be that following the primary in */
/*           the right-handed sense. */

	    index2 = next[(i__2 = index1 - 1) < 3 && 0 <= i__2 ? i__2 : 
		    s_rnge("next", i__2, "f_twovxf__", (ftnlen)420)];
	    s_copy(title, "Random case #: TWOVXF/ZZTWOVXF random: primary = "
		    "#, secondary = #.", (ftnlen)240, (ftnlen)66);
	    repmi_(title, "#", &caseno, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(title, "#", &index1, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(title, "#", &index2, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

	    tcase_(title, (ftnlen)240);

/*           Generate position and velocity components for the primary */
/*           state vector. */

	    d__1 = t_randd__(&lb, &ub, &seed);
	    pscal1 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		state1[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state1", i__2, "f_twovxf__", (ftnlen)446)] = 
			t_randd__(&c_b7, &c_b6, &seed) * pscal1;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    d__1 = t_randd__(&lb, &ub, &seed);
	    vscal1 = pow_dd(&c_b143, &d__1);
	    for (i__ = 4; i__ <= 6; ++i__) {
		state1[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state1", i__2, "f_twovxf__", (ftnlen)453)] = 
			t_randd__(&c_b7, &c_b6, &seed) * vscal1;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/*           Generate position and velocity components for the secondary */
/*           state vector. */

	    d__1 = t_randd__(&lb, &ub, &seed);
	    pscal2 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		state2[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state2", i__2, "f_twovxf__", (ftnlen)464)] = 
			t_randd__(&c_b7, &c_b6, &seed) * pscal2;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    d__1 = t_randd__(&lb, &ub, &seed);
	    vscal2 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 4; i__ <= 6; ++i__) {
		state2[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state2", i__2, "f_twovxf__", (ftnlen)472)] = 
			t_randd__(&c_b7, &c_b6, &seed) * vscal2;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/*           We may have inadvertently created nearly linearly */
/*           dependent vectors. Alter the second position vector */
/*           if this is the case. */

	    if (vsep_(state1, state2) < .001) {
		state2[0] *= .9;
		state2[1] *= .8;
		state2[2] *= .7;
	    }

/*           Compute the state transformation matrix. */

	    twovxf_(state1, &index1, state2, &index2, xform);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Compare results to those from ZZTWOVXF. */

	    zztwovxf_(state1, &index1, state2, &index2, xformi);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    invstm_(xformi, xxform);
	    chckad_("XFORM (vs ZZ)", xform, "~", xxform, &c__36, &c_b115, ok, 
		    (ftnlen)13, (ftnlen)1);

/*           Check the rotation portions of the transformation. */
/*           Check both rotation blocks. */

	    twovec_(state1, &index1, state2, &index2, xr);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = i__ * 6 - 6) < 36 && 0 <= i__2 ? i__2 : 
			s_rnge("xform", i__2, "f_twovxf__", (ftnlen)514)], &
			r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("r", i__3, "f_twovxf__", (ftnlen)514)]);
	    }
	    chckad_("R (UL)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = (i__ + 3) * 6 - 3) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("xform", i__2, "f_twovxf__", (ftnlen)
			520)], &r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? 
			i__3 : s_rnge("r", i__3, "f_twovxf__", (ftnlen)520)]);
	    }
	    chckad_("R (LR)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the upper right block. */

	    cleard_(&c__9, xr);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = (i__ + 3) * 6 - 6) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("xform", i__2, "f_twovxf__", (ftnlen)
			531)], &r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? 
			i__3 : s_rnge("r", i__3, "f_twovxf__", (ftnlen)531)]);
	    }
	    chckad_("R (UR)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the derivative block. */

	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = i__ * 6 - 3) < 36 && 0 <= i__2 ? i__2 : 
			s_rnge("xform", i__2, "f_twovxf__", (ftnlen)540)], &
			dr[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("dr", i__3, "f_twovxf__", (ftnlen)540)]);
	    }

/*           We'll need to compute the expected derivative. For that, */
/*           we'll need the velocities of basis vectors of the "to" */
/*           frame. */

/*           Note that the axis order here is dependent on the */
/*           selection of primary and secondary vectors. In this */
/*           case the secondary vector follows the primary one */
/*           in the right-handed sense. */

/*           Start with the primary axis. */

	    dvhat_(state1, uxstat);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__2 = index1 + i__ * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			s_rnge("xr", i__2, "f_twovxf__", (ftnlen)558)] = 
			uxstat[(i__3 = i__ + 2) < 6 && 0 <= i__3 ? i__3 : 
			s_rnge("uxstat", i__3, "f_twovxf__", (ftnlen)558)];
	    }

/*           Next find the state of the unit cross product */
/*           of the primary and secondary axes. */

	    ducrss_(state1, state2, uxstat);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__3 = prev[(i__2 = index1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			s_rnge("prev", i__2, "f_twovxf__", (ftnlen)569)] + 
			i__ * 3 - 4) < 9 && 0 <= i__3 ? i__3 : s_rnge("xr", 
			i__3, "f_twovxf__", (ftnlen)569)] = uxstat[(i__4 = 
			i__ + 2) < 6 && 0 <= i__4 ? i__4 : s_rnge("uxstat", 
			i__4, "f_twovxf__", (ftnlen)569)];
	    }

/*           Next find the state of the unit cross product */
/*           of the third axis and the primary vector. */

	    ducrss_(uxstat, state1, uxsta2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__2 = index2 + i__ * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			s_rnge("xr", i__2, "f_twovxf__", (ftnlen)580)] = 
			uxsta2[(i__3 = i__ + 2) < 6 && 0 <= i__3 ? i__3 : 
			s_rnge("uxsta2", i__3, "f_twovxf__", (ftnlen)580)];
	    }
	    chckad_("(d/dt)R", dr, "~", xr, &c__9, &c_b115, ok, (ftnlen)7, (
		    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*           Let the secondary vector be that preceding the primary in */
/*           the right-handed sense. */

	    index2 = prev[(i__2 = index1 - 1) < 3 && 0 <= i__2 ? i__2 : 
		    s_rnge("prev", i__2, "f_twovxf__", (ftnlen)592)];
	    s_copy(title, "Random case #: TWOVXF/ZZTWOVXF random: primary = "
		    "#, secondary = #.", (ftnlen)240, (ftnlen)66);
	    repmi_(title, "#", &caseno, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(title, "#", &index1, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(title, "#", &index2, title, (ftnlen)240, (ftnlen)1, (
		    ftnlen)240);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)240);

/*           Generate position and velocity components for the primary */
/*           state vector. */

	    d__1 = t_randd__(&lb, &ub, &seed);
	    pscal1 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		state1[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state1", i__2, "f_twovxf__", (ftnlen)617)] = 
			t_randd__(&c_b7, &c_b6, &seed) * pscal1;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    d__1 = t_randd__(&lb, &ub, &seed);
	    vscal1 = pow_dd(&c_b143, &d__1);
	    for (i__ = 4; i__ <= 6; ++i__) {
		state1[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state1", i__2, "f_twovxf__", (ftnlen)624)] = 
			t_randd__(&c_b7, &c_b6, &seed) * vscal1;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/*           Generate position and velocity components for the secondary */
/*           state vector. */

	    d__1 = t_randd__(&lb, &ub, &seed);
	    pscal2 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		state2[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state2", i__2, "f_twovxf__", (ftnlen)635)] = 
			t_randd__(&c_b7, &c_b6, &seed) * pscal2;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    d__1 = t_randd__(&lb, &ub, &seed);
	    vscal2 = pow_dd(&c_b143, &d__1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 4; i__ <= 6; ++i__) {
		state2[(i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
			"state2", i__2, "f_twovxf__", (ftnlen)643)] = 
			t_randd__(&c_b7, &c_b6, &seed) * vscal2;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/*           We may have inadvertently created nearly linearly */
/*           dependent vectors. Alter the second position vector */
/*           if this is the case. */

	    if (vsep_(state1, state2) < .001) {
		state2[0] *= .9;
		state2[1] *= .8;
		state2[2] *= .7;
	    }

/*           Compute the state transformation matrix. */

	    twovxf_(state1, &index1, state2, &index2, xform);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Compare results to those from ZZTWOVXF. */

	    zztwovxf_(state1, &index1, state2, &index2, xformi);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    invstm_(xformi, xxform);
	    chckad_("XFORM (vs ZZ)", xform, "~", xxform, &c__36, &c_b115, ok, 
		    (ftnlen)13, (ftnlen)1);

/*           Check the rotation portions of the transformation. */
/*           Check both rotation blocks. */

	    twovec_(state1, &index1, state2, &index2, xr);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = i__ * 6 - 6) < 36 && 0 <= i__2 ? i__2 : 
			s_rnge("xform", i__2, "f_twovxf__", (ftnlen)686)], &
			r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("r", i__3, "f_twovxf__", (ftnlen)686)]);
	    }
	    chckad_("R (UL)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = (i__ + 3) * 6 - 3) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("xform", i__2, "f_twovxf__", (ftnlen)
			692)], &r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? 
			i__3 : s_rnge("r", i__3, "f_twovxf__", (ftnlen)692)]);
	    }
	    chckad_("R (LR)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the upper right block. */

	    cleard_(&c__9, xr);
	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = (i__ + 3) * 6 - 6) < 36 && 0 <= i__2 ? 
			i__2 : s_rnge("xform", i__2, "f_twovxf__", (ftnlen)
			703)], &r__[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? 
			i__3 : s_rnge("r", i__3, "f_twovxf__", (ftnlen)703)]);
	    }
	    chckad_("R (UR)", r__, "~", xr, &c__9, &c_b115, ok, (ftnlen)6, (
		    ftnlen)1);

/*           Check the derivative block. */

	    for (i__ = 1; i__ <= 3; ++i__) {
		vequ_(&xform[(i__2 = i__ * 6 - 3) < 36 && 0 <= i__2 ? i__2 : 
			s_rnge("xform", i__2, "f_twovxf__", (ftnlen)712)], &
			dr[(i__3 = i__ * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("dr", i__3, "f_twovxf__", (ftnlen)712)]);
	    }

/*           We'll need to compute the expected derivative. For that, */
/*           we'll need the velocities of basis vectors of the "to" */
/*           frame. */

/*           Note that the axis order here is dependent on the */
/*           selection of primary and secondary vectors. In this */
/*           case the secondary vector precedes the primary one */
/*           in the right-handed sense. */

/*           Start with the primary axis. */

	    dvhat_(state1, uxstat);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__2 = index1 + i__ * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			s_rnge("xr", i__2, "f_twovxf__", (ftnlen)730)] = 
			uxstat[(i__3 = i__ + 2) < 6 && 0 <= i__3 ? i__3 : 
			s_rnge("uxstat", i__3, "f_twovxf__", (ftnlen)730)];
	    }

/*           Next find the state of the unit cross product */
/*           of the secondary and primary axes. */

	    ducrss_(state2, state1, uxstat);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__3 = next[(i__2 = index1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			s_rnge("next", i__2, "f_twovxf__", (ftnlen)741)] + 
			i__ * 3 - 4) < 9 && 0 <= i__3 ? i__3 : s_rnge("xr", 
			i__3, "f_twovxf__", (ftnlen)741)] = uxstat[(i__4 = 
			i__ + 2) < 6 && 0 <= i__4 ? i__4 : s_rnge("uxstat", 
			i__4, "f_twovxf__", (ftnlen)741)];
	    }

/*           Next find the state of the unit cross product of the */
/*           primary vector and the third axis. */

	    ducrss_(state1, uxstat, uxsta2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xr[(i__2 = index2 + i__ * 3 - 4) < 9 && 0 <= i__2 ? i__2 : 
			s_rnge("xr", i__2, "f_twovxf__", (ftnlen)752)] = 
			uxsta2[(i__3 = i__ + 2) < 6 && 0 <= i__3 ? i__3 : 
			s_rnge("uxsta2", i__3, "f_twovxf__", (ftnlen)752)];
	    }
	    chckad_("(d/dt)R", dr, "~", xr, &c__9, &c_b115, ok, (ftnlen)7, (
		    ftnlen)1);
	}
    }

/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_twovxf__ */

