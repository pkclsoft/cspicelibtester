/* f_vector3.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b7 = -1.;
static doublereal c_b8 = 1.;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b24 = 0.;
static doublereal c_b105 = 1e-12;
static doublereal c_b129 = 1e-15;
static integer c__1 = 1;
static integer c__2 = 2;

/* $Procedure F_VECTOR3 (Family of tests on 3-vector operations) */
/* Subroutine */ int f_vector3__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    integer seed;
    doublereal eval, vmag;
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *), vscl_(
	    doublereal *, doublereal *, doublereal *);
    extern doublereal vrel_(doublereal *, doublereal *), vdot_(doublereal *, 
	    doublereal *), vsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    );
    doublereal expt;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    integer i__;
    logical check;
    doublereal scale, x, y, z__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal v_ran__[3], theta;
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *), topen_(char *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    doublereal dummy, v_out__[3];
    extern /* Subroutine */ int vperp_(doublereal *, doublereal *, doublereal 
	    *), ucrss_(doublereal *, doublereal *, doublereal *), unorm_(
	    doublereal *, doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *), twopi_(void);
    extern /* Subroutine */ int vproj_(doublereal *, doublereal *, doublereal 
	    *), vcrss_(doublereal *, doublereal *, doublereal *);
    extern logical vzero_(doublereal *);
    doublereal scale1, scale2, scale3;
    extern /* Subroutine */ int t_success__(logical *), vrotv_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), vlcom3_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    , chcksd_(char *, doublereal *, char *, doublereal *, doublereal *
	    , logical *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *
	    , logical *, ftnlen);
    doublereal v_indx__[3];
    extern /* Subroutine */ int vupack_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal v_temp__[3];
    extern /* Subroutine */ int vhatip_(doublereal *), vsclip_(doublereal *, 
	    doublereal *), rotvec_(doublereal *, doublereal *, integer *, 
	    doublereal *);
    doublereal v_zero__[3], v_expt__[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);

/* $ Abstract */

/*     This routine performs rudimentary tests on a collection */
/*     of 3D-vector operation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     M.C. Kim      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 26-SEP-2014 (MCK) */

/*        Initial code to test */

/*           ROTVEC */
/*           UCRSS */
/*           UNORM */
/*           VADD */
/*           VCRSS */
/*           VDIST */
/*           VDOT */
/*           VEQU */
/*           VHAT */
/*           VHATIP */
/*           VLCOM */
/*           VLCOM3 */
/*           VMINUS */
/*           VNORM */
/*           VPACK */
/*           VPERP */
/*           VPROJ */
/*           VREL */
/*           VROTV */
/*           VSCL */
/*           VSCLIP */
/*           VSEP */
/*           VSUB */
/*           VUPACK */
/*           VZERO */

/* -& */
/* $ Index_Entries */

/*     test 3-vector operation routines */
/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     initial seed number for pseudo-random number generation */


/*     Local Variables */


/*     An index */


/*     Seed number for pseudo-random number generation */


/*     Begin every test family with an open call. */

    topen_("F_VECTOR3", (ftnlen)9);

/*     Form vectors to be used for more than one test case. */


/*     A zero vector */

    for (i__ = 1; i__ <= 3; ++i__) {
	v_zero__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_zero", 
		i__1, "f_vector3__", (ftnlen)222)] = 0.;
    }

/*     An index vector, whose components are their indices */

    for (i__ = 1; i__ <= 3; ++i__) {
	v_indx__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_indx", 
		i__1, "f_vector3__", (ftnlen)229)] = (doublereal) i__;
    }

/*     A pseudo-random vector, whose components are */
/*     uniformly distributed on the interval [-1.0D0, +1.0D0] */

    seed = 1980518;
    for (i__ = 1; i__ <= 3; ++i__) {
	v_ran__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_ran", 
		i__1, "f_vector3__", (ftnlen)239)] = t_randd__(&c_b7, &c_b8, &
		seed);
    }

/*     Cases to test VZERO */

    tcase_("VZERO: zero vector", (ftnlen)18);
    check = vzero_(v_zero__);
    chcksl_("CHECK", &check, &c_true, ok, (ftnlen)5);
    tcase_("VZERO: index vector", (ftnlen)19);
    check = vzero_(v_indx__);
    chcksl_("CHECK", &check, &c_false, ok, (ftnlen)5);
    tcase_("VZERO: random vector", (ftnlen)20);
    check = vzero_(v_ran__);
    chcksl_("CHECK", &check, &c_false, ok, (ftnlen)5);

/*     Cases to test VEQU */

    tcase_("VEQU: zero vector", (ftnlen)17);
    vequ_(v_zero__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VEQU: index vector", (ftnlen)18);
    vequ_(v_indx__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_indx__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VEQU: random vector", (ftnlen)19);
    vequ_(v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VADD */

    tcase_("VADD: zero vector and random vector", (ftnlen)35);
    vadd_(v_zero__, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VADD: index vector and random vector", (ftnlen)36);
    vadd_(v_indx__, v_ran__, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)303)] = v_indx__[(i__2 = i__ - 1)
		 < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, "f_vector3"
		"__", (ftnlen)303)] + v_ran__[(i__3 = i__ - 1) < 3 && 0 <= 
		i__3 ? i__3 : s_rnge("v_ran", i__3, "f_vector3__", (ftnlen)
		303)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VSUB */

    tcase_("VSUB: zero vector and random vector", (ftnlen)35);
    vsub_(v_ran__, v_zero__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VSUB: index vector and random vector", (ftnlen)36);
    vsub_(v_ran__, v_indx__, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)323)] = v_ran__[(i__2 = i__ - 1) 
		< 3 && 0 <= i__2 ? i__2 : s_rnge("v_ran", i__2, "f_vector3__",
		 (ftnlen)323)] - v_indx__[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? 
		i__3 : s_rnge("v_indx", i__3, "f_vector3__", (ftnlen)323)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VDOT. */

    tcase_("VDOT: zero vector and random vector", (ftnlen)35);
    eval = vdot_(v_zero__, v_ran__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b24, ok, (ftnlen)4, (ftnlen)1);
    tcase_("VDOT: index vector and random vector", (ftnlen)36);
    eval = vdot_(v_indx__, v_ran__);
    expt = 0.;
    for (i__ = 1; i__ <= 3; ++i__) {
	expt += v_indx__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"v_indx", i__1, "f_vector3__", (ftnlen)345)] * v_ran__[(i__2 =
		 i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_ran", i__2, 
		"f_vector3__", (ftnlen)345)];
    }
    chcksd_("EVAL", &eval, "~", &expt, &c_b24, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VPACK */

    tcase_("VPACK: random vector.", (ftnlen)21);
    x = v_ran__[0];
    y = v_ran__[1];
    z__ = v_ran__[2];
    vpack_(&x, &y, &z__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VUPACK */

    tcase_("VUPACK: random vector", (ftnlen)21);
    vupack_(v_ran__, &x, &y, &z__);
    v_expt__[0] = x;
    v_expt__[1] = y;
    v_expt__[2] = z__;
    chckad_("V_EXPT", v_expt__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Cases to test VSCL */

    tcase_("VSCL: scaling an index vector", (ftnlen)29);
    scale = t_randd__(&c_b7, &c_b8, &seed);
    vscl_(&scale, v_indx__, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)386)] = scale * v_indx__[(i__2 = 
		i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vector3__", (ftnlen)386)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VSCL: scaling back to the index vector", (ftnlen)38);
    scale = 1. / scale;
    vscl_(&scale, v_out__, v_temp__);
    chckad_("V_TEMP", v_temp__, "~", v_indx__, &c__3, &c_b105, ok, (ftnlen)6, 
	    (ftnlen)1);

/*     Cases to test VSCLIP */

    tcase_("VSCLIP: scaling an index vector", (ftnlen)31);
    scale = t_randd__(&c_b7, &c_b8, &seed);
    vequ_(v_indx__, v_out__);
    vsclip_(&scale, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)412)] = scale * v_indx__[(i__2 = 
		i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vector3__", (ftnlen)412)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VNORM */

    tcase_("VNORM: zero vector", (ftnlen)18);
    eval = vnorm_(v_zero__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b24, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VNORM with an index vector", (ftnlen)31);
    eval = vnorm_(v_indx__);
    expt = 0.;
    for (i__ = 1; i__ <= 3; ++i__) {
	expt += v_indx__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"v_indx", i__1, "f_vector3__", (ftnlen)433)] * v_indx__[(i__2 
		= i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vector3__", (ftnlen)433)];
    }
    expt = sqrt(expt);
    chcksd_("EVAL", &eval, "~", &expt, &c_b129, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VMINUS */

    tcase_("VMINUS: random vector", (ftnlen)21);
    vminus_(v_ran__, v_out__);
    vadd_(v_ran__, v_out__, v_temp__);
    chckad_("V_TEMP", v_temp__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Cases to test UNORM */

    tcase_("UNORM: zero vector", (ftnlen)18);
    unorm_(v_zero__, v_out__, &vmag);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksd_("VMAG", &vmag, "~", &c_b24, &c_b24, ok, (ftnlen)4, (ftnlen)1);
    tcase_("UNORM:  random vector.", (ftnlen)22);
    unorm_(v_ran__, v_out__, &vmag);
    expt = vnorm_(v_ran__);
    chcksd_("VMAG", &vmag, "~", &expt, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    eval = vnorm_(v_out__);
    chcksd_("EVAL", &eval, "~", &c_b8, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    vscl_(&vmag, v_out__, v_expt__);
    chckad_("V_EXPT", v_expt__, "~", v_ran__, &c__3, &c_b105, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Cases to test VHAT */

    tcase_("VHAT: zero vector", (ftnlen)17);
    vhat_(v_zero__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VHAT: random vector.", (ftnlen)20);
    vhat_(v_ran__, v_out__);
    eval = vnorm_(v_out__);
    chcksd_("EVAL", &eval, "~", &c_b8, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VHATIP */

    tcase_("VHATIP: zero vector", (ftnlen)19);
    vequ_(v_zero__, v_out__);
    vhatip_(v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VHATIP: random vector", (ftnlen)21);
    vequ_(v_ran__, v_out__);
    vhatip_(v_out__);
    eval = vnorm_(v_out__);
    chcksd_("EVAL", &eval, "~", &c_b8, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VDIST */

    tcase_("VDIST: same vector", (ftnlen)18);
    eval = vdist_(v_ran__, v_ran__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b24, ok, (ftnlen)4, (ftnlen)1);
    tcase_("VDIST: index vector and random vector", (ftnlen)37);
    eval = vdist_(v_indx__, v_ran__);
    vsub_(v_indx__, v_ran__, v_out__);
    expt = vnorm_(v_out__);
    chcksd_("EVAL", &eval, "~", &expt, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VREL */

    tcase_("VREL: two zero vectors", (ftnlen)22);
    eval = vrel_(v_zero__, v_zero__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b24, ok, (ftnlen)4, (ftnlen)1);
    tcase_("VREL: index vector and random vector", (ftnlen)36);
    eval = vrel_(v_indx__, v_ran__);
/* Computing MAX */
    d__1 = vnorm_(v_indx__), d__2 = vnorm_(v_ran__);
    expt = vdist_(v_indx__, v_ran__) / max(d__1,d__2);
    chcksd_("EVAL", &eval, "~", &expt, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VSEP */

    tcase_("VSEP: angle between a zero vector and a random vector", (ftnlen)
	    53);
    eval = vsep_(v_zero__, v_ran__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    tcase_("VSEP: angle between a random vector and a zero vector", (ftnlen)
	    53);
    eval = vsep_(v_ran__, v_zero__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VLCOM */

    tcase_("VLCOM: index vector and random vector", (ftnlen)37);
    scale = t_randd__(&c_b7, &c_b8, &seed);
    dummy = t_randd__(&c_b7, &c_b8, &seed);
    vlcom_(&scale, v_indx__, &dummy, v_ran__, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)581)] = scale * v_indx__[(i__2 = 
		i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vector3__", (ftnlen)581)] + dummy * v_ran__[(i__3 = i__ - 
		1) < 3 && 0 <= i__3 ? i__3 : s_rnge("v_ran", i__3, "f_vector"
		"3__", (ftnlen)581)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VLCOM: zero vector and random vector", (ftnlen)36);
    vlcom_(&scale, v_zero__, &c_b8, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VLCOM3 */

    tcase_("VLCOM: index vector and random vector", (ftnlen)37);
    scale1 = t_randd__(&c_b7, &c_b8, &seed);
    scale2 = t_randd__(&c_b7, &c_b8, &seed);
    scale3 = t_randd__(&c_b7, &c_b8, &seed);
    vlcom3_(&scale1, v_indx__, &scale2, v_ran__, &scale3, v_ran__, v_out__);
    for (i__ = 1; i__ <= 3; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vector3__", (ftnlen)606)] = scale1 * v_indx__[(i__2 =
		 i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vector3__", (ftnlen)606)] + scale2 * v_ran__[(i__3 = i__ - 
		1) < 3 && 0 <= i__3 ? i__3 : s_rnge("v_ran", i__3, "f_vector"
		"3__", (ftnlen)606)] + scale3 * v_ran__[(i__4 = i__ - 1) < 3 &&
		 0 <= i__4 ? i__4 : s_rnge("v_ran", i__4, "f_vector3__", (
		ftnlen)606)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VPROJ */

    tcase_("VPROJ: projecting a random vector to a zero vector", (ftnlen)50);
    vproj_(v_ran__, v_zero__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VPROJ: projecting a random vector to an index vector", (ftnlen)52)
	    ;
    vproj_(v_ran__, v_indx__, v_out__);

/*     Compute V_TEMP to be the unit vector along V_INDX. */

    vhat_(v_indx__, v_temp__);

/*     Check ( V_RAN DOT V_TEMP ) .EQ. ( V_OUT DOT V_TEMP ). */

    dummy = vdot_(v_ran__, v_temp__);
    eval = vdot_(v_out__, v_temp__);
    chcksd_("EVAL", &eval, "~", &dummy, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VPERP */

    tcase_("VPERP: projecting a zero vector to a random vector", (ftnlen)50);
    vperp_(v_zero__, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VPERP: projecting an index vector to a random vector", (ftnlen)52)
	    ;
    vperp_(v_indx__, v_ran__, v_out__);
    vsub_(v_indx__, v_out__, v_temp__);
    vcrss_(v_temp__, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test UCRSS */

    tcase_("UCRSS: index vector and random vector", (ftnlen)37);
    ucrss_(v_indx__, v_ran__, v_out__);
    eval = vdot_(v_out__, v_indx__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    eval = vdot_(v_out__, v_ran__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    eval = vdot_(v_out__, v_out__);
    chcksd_("EVAL", &eval, "~", &c_b8, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    tcase_("UCRSS: zero vector and random vector", (ftnlen)36);
    ucrss_(v_zero__, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VCRSS */

    tcase_("VCRSS: index vector and random vector", (ftnlen)37);
    vcrss_(v_indx__, v_ran__, v_out__);
    eval = vdot_(v_out__, v_indx__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    eval = vdot_(v_out__, v_ran__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
/* Computing 2nd power */
    d__1 = vnorm_(v_indx__) * vnorm_(v_ran__);
/* Computing 2nd power */
    d__2 = vnorm_(v_out__);
/* Computing 2nd power */
    d__3 = vdot_(v_indx__, v_ran__);
    eval = d__1 * d__1 - d__2 * d__2 - d__3 * d__3;
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);
    tcase_("VCRSS: zero vector and random vector", (ftnlen)36);
    vcrss_(v_zero__, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VROTV */

    tcase_("VROTV: rotating a random vector wrt a zero vector", (ftnlen)49);
    d__1 = twopi_();
    theta = t_randd__(&c_b24, &d__1, &seed);
    vrotv_(v_ran__, v_zero__, &theta, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__3, &c_b24, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("VROTV: rotating a random vector wrt an index vector", (ftnlen)51);
    d__1 = twopi_();
    theta = t_randd__(&c_b24, &d__1, &seed);
    vrotv_(v_ran__, v_indx__, &theta, v_out__);

/*     Angle between V_RAN and V_INDX should not be changed. */

    eval = vsep_(v_ran__, v_indx__) - vsep_(v_out__, v_indx__);
    chcksd_("EVAL", &eval, "~", &c_b24, &c_b105, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test ROTVEC */

    tcase_("ROTVEC: rotating a random vector with respect to the x-axis", (
	    ftnlen)59);
    rotvec_(v_ran__, &theta, &c__1, v_out__);
    v_temp__[0] = 1.;
    v_temp__[1] = 0.;
    v_temp__[2] = 0.;
    d__1 = -theta;
    vrotv_(v_ran__, v_temp__, &d__1, v_expt__);
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);
    rotvec_(v_ran__, &theta, &c__2, v_out__);
    v_temp__[0] = 0.;
    v_temp__[1] = 1.;
    v_temp__[2] = 0.;
    d__1 = -theta;
    vrotv_(v_ran__, v_temp__, &d__1, v_expt__);
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);
    rotvec_(v_ran__, &theta, &c__3, v_out__);
    v_temp__[0] = 0.;
    v_temp__[1] = 0.;
    v_temp__[2] = 1.;
    d__1 = -theta;
    vrotv_(v_ran__, v_temp__, &d__1, v_expt__);
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__3, &c_b105, ok, (ftnlen)5, (
	    ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_vector3__ */

