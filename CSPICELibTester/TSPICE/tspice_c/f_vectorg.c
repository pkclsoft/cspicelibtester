/* f_vectorg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b7 = -1.;
static doublereal c_b8 = 1.;
static integer c__99 = 99;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c_n99 = -99;
static doublereal c_b32 = 0.;
static doublereal c_b94 = 1e-99;
static doublereal c_b108 = 1e-12;

/* $Procedure F_VECTORG (Family of tests on general vector operations) */
/* Subroutine */ int f_vectorg__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal), acos(doublereal);

    /* Local variables */
    integer seed;
    doublereal eval, expt;
    integer i__;
    extern /* Subroutine */ int vaddg_(doublereal *, doublereal *, integer *, 
	    doublereal *);
    doublereal scale;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal v_ran__[99];
    extern /* Subroutine */ int vhatg_(doublereal *, integer *, doublereal *),
	     vsclg_(doublereal *, doublereal *, integer *, doublereal *);
    extern doublereal vrelg_(doublereal *, doublereal *, integer *), vdotg_(
	    doublereal *, doublereal *, integer *), vsepg_(doublereal *, 
	    doublereal *, integer *);
    doublereal v_tmp__[99];
    extern /* Subroutine */ int topen_(char *, ftnlen), vequg_(doublereal *, 
	    integer *, doublereal *), vsubg_(doublereal *, doublereal *, 
	    integer *, doublereal *);
    doublereal dummy, v_out__[99];
    logical vzero;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal vecmag;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    doublereal v_indx__[99];
    extern /* Subroutine */ int vlcomg_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *);
    extern doublereal vdistg_(doublereal *, doublereal *, integer *);
    doublereal v_zero__[99], v_expt__[99];
    extern /* Subroutine */ int unormg_(doublereal *, integer *, doublereal *,
	     doublereal *);
    extern doublereal vnormg_(doublereal *, integer *);
    extern /* Subroutine */ int vprojg_(doublereal *, doublereal *, integer *,
	     doublereal *);
    extern logical vzerog_(doublereal *, integer *);
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);

/* $ Abstract */

/*     This routine performs rudimentary tests on a collection of */
/*     general vector operation routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     M.C. Kim      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 26-SEP-2014 (MCK) */

/*        Initial code to test */

/*           UNORMG */
/*           VADDG */
/*           VDISTG */
/*           VDOTG */
/*           VEQUG */
/*           VHATG */
/*           VLCOMG */
/*           VNORMG */
/*           VPROJG */
/*           VRELG */
/*           VSCLG */
/*           VSEPG */
/*           VSUBG */
/*           VZEROG */

/* -& */
/* $ Index_Entries */

/*     test general vector operation routines */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_VECTORG", (ftnlen)9);

/*     Form vectors to be used for more than one test case. */


/*     A zero vector */

    for (i__ = 1; i__ <= 99; ++i__) {
	v_zero__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_zero", 
		i__1, "f_vectorg__", (ftnlen)194)] = 0.;
    }

/*     An index vector, whose components are their indices. */

    for (i__ = 1; i__ <= 99; ++i__) {
	v_indx__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_indx", 
		i__1, "f_vectorg__", (ftnlen)201)] = (doublereal) i__;
    }

/*     A random vector, whose components are */
/*     uniformly distributed on the interval [-1.0D0,+1.0D0]. */

    seed = 19810518;
    for (i__ = 1; i__ <= 99; ++i__) {
	v_ran__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_ran", 
		i__1, "f_vectorg__", (ftnlen)211)] = t_randd__(&c_b7, &c_b8, &
		seed);
    }

/*     Cases to test VZEROG */

    tcase_("Test VZEROG with a zero vector", (ftnlen)30);
    vzero = vzerog_(v_zero__, &c__99);
    chcksl_("VZERO", &vzero, &c_true, ok, (ftnlen)5);
    tcase_("Test VZEROG with an index vector", (ftnlen)32);
    vzero = vzerog_(v_indx__, &c__99);
    chcksl_("VZERO", &vzero, &c_false, ok, (ftnlen)5);
    tcase_("Test VZEROG with a zero dimension", (ftnlen)33);
    vzero = vzerog_(v_zero__, &c__0);
    chcksl_("VZERO", &vzero, &c_false, ok, (ftnlen)5);
    tcase_("Test VZEROG with a negative dimension", (ftnlen)37);
    vzero = vzerog_(v_zero__, &c_n99);
    chcksl_("VZERO", &vzero, &c_false, ok, (ftnlen)5);

/*     Cases to test VEQUG */

    tcase_("Test VEQUG with a zero vector", (ftnlen)29);
    vequg_(v_zero__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VEQUG with an index vector", (ftnlen)31);
    vequg_(v_indx__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_indx__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VEQUG with a random vector", (ftnlen)31);
    vequg_(v_ran__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VADDG */

    tcase_("Test VADDG by adding a zero vector and a random vector", (ftnlen)
	    54);
    vaddg_(v_zero__, v_ran__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VADDG by adding an index vector and a random vector", (
	    ftnlen)56);
    vaddg_(v_indx__, v_ran__, &c__99, v_out__);
    for (i__ = 1; i__ <= 99; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vectorg__", (ftnlen)284)] = v_indx__[(i__2 = i__ - 1)
		 < 99 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, "f_vector"
		"g__", (ftnlen)284)] + v_ran__[(i__3 = i__ - 1) < 99 && 0 <= 
		i__3 ? i__3 : s_rnge("v_ran", i__3, "f_vectorg__", (ftnlen)
		284)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VSUBG */

    tcase_("Test VSUBG by subtracting a zero vector from a random vector", (
	    ftnlen)60);
    vsubg_(v_ran__, v_zero__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VADDG by subtracting an index vector from a random vector", (
	    ftnlen)62);
    vsubg_(v_ran__, v_indx__, &c__99, v_out__);
    for (i__ = 1; i__ <= 99; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vectorg__", (ftnlen)307)] = v_ran__[(i__2 = i__ - 1) 
		< 99 && 0 <= i__2 ? i__2 : s_rnge("v_ran", i__2, "f_vectorg__"
		, (ftnlen)307)] - v_indx__[(i__3 = i__ - 1) < 99 && 0 <= i__3 
		? i__3 : s_rnge("v_indx", i__3, "f_vectorg__", (ftnlen)307)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VDOTG */

    tcase_("Test VDOTG with a zero vector and a random vector", (ftnlen)49);
    eval = vdotg_(v_zero__, v_ran__, &c__99);
    expt = 0.;
    chcksd_("EVAL", &eval, "~", &expt, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VDOTG with an index vector and a random vector", (ftnlen)51);
    eval = vdotg_(v_indx__, v_ran__, &c__99);
    expt = 0.;
    for (i__ = 1; i__ <= 99; ++i__) {
	expt += v_indx__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge(
		"v_indx", i__1, "f_vectorg__", (ftnlen)333)] * v_ran__[(i__2 =
		 i__ - 1) < 99 && 0 <= i__2 ? i__2 : s_rnge("v_ran", i__2, 
		"f_vectorg__", (ftnlen)333)];
    }
    chcksd_("EVAL", &eval, "~", &expt, &c_b94, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VNORMG */

    tcase_("Test VNORMG with a zero vector", (ftnlen)30);
    eval = vnormg_(v_zero__, &c__99);
    expt = 0.;
    chcksd_("EVAL", &eval, "~", &expt, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VNORMG with an index vector", (ftnlen)32);
    eval = vnormg_(v_indx__, &c__99);
    dummy = 0.;
    for (i__ = 1; i__ <= 99; ++i__) {
	dummy += v_indx__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge(
		"v_indx", i__1, "f_vectorg__", (ftnlen)357)] * v_indx__[(i__2 
		= i__ - 1) < 99 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vectorg__", (ftnlen)357)];
    }
    expt = sqrt(dummy);
    chcksd_("EVAL", &eval, "~/", &expt, &c_b108, ok, (ftnlen)4, (ftnlen)2);

/*     Cases to test UNORMG */

    tcase_("Test UNORMG with a zero vector", (ftnlen)30);
    unormg_(v_zero__, &c__99, v_out__, &vecmag);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksd_("VECMAG", &vecmag, "~", &c_b32, &c_b32, ok, (ftnlen)6, (ftnlen)1);
    tcase_("Test UNORMG with a random vector", (ftnlen)32);
    unormg_(v_ran__, &c__99, v_out__, &vecmag);
    expt = vnormg_(v_ran__, &c__99);
    chcksd_("VECMAG", &vecmag, "~", &expt, &c_b32, ok, (ftnlen)6, (ftnlen)1);
    expt = 1.;
    dummy = vnormg_(v_out__, &c__99);
    chcksd_("DUMMY", &dummy, "~", &expt, &c_b108, ok, (ftnlen)5, (ftnlen)1);
    vsclg_(&vecmag, v_out__, &c__99, v_tmp__);
    chckad_("V_TMP", v_tmp__, "~", v_ran__, &c__99, &c_b108, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VHATG */

    tcase_("Test VHATG with a zero vector", (ftnlen)29);
    vhatg_(v_zero__, &c__99, v_out__);

/*     VHATG returns a zero vector for a zero vector input. */

    chckad_("V_OUT", v_out__, "~", v_zero__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VHATG with a random vector", (ftnlen)31);
    vhatg_(v_ran__, &c__99, v_out__);
    eval = vnormg_(v_out__, &c__99);
    chcksd_("EVAL", &eval, "~", &c_b8, &c_b108, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VSCLG */

    tcase_("Test VSCLG by scaling an index vector", (ftnlen)37);
    scale = t_randd__(&c_b7, &c_b8, &seed);
    vsclg_(&scale, v_indx__, &c__99, v_out__);
    for (i__ = 1; i__ <= 99; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vectorg__", (ftnlen)424)] = scale * v_indx__[(i__2 = 
		i__ - 1) < 99 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vectorg__", (ftnlen)424)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__99, &c_b108, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VSCLG by scaling back to the index vector", (ftnlen)46);
    scale = 1. / scale;
    vsclg_(&scale, v_out__, &c__99, v_tmp__);
    chckad_("V_TMP", v_tmp__, "~", v_indx__, &c__99, &c_b108, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VDISTG */

    tcase_("Test VDISTG using the same vector", (ftnlen)33);
    eval = vdistg_(v_indx__, v_indx__, &c__99);

/*     Distance between same vectors should be zero. */

    chcksd_("EVAL", &eval, "~", &c_b32, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VDISTG using an index vector and a random vector", (ftnlen)
	    53);
    eval = vdistg_(v_indx__, v_ran__, &c__99);
    vsubg_(v_indx__, v_ran__, &c__99, v_tmp__);
    expt = vnormg_(v_tmp__, &c__99);
    chcksd_("EVAL", &eval, "~", &expt, &c_b108, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VRELG */

    tcase_("Test VRELG using two zero vectors", (ftnlen)33);
    eval = vrelg_(v_zero__, v_zero__, &c__99);
    chcksd_("EVAL", &eval, "~", &c_b32, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VRELG using an index vector and a random vector", (ftnlen)52)
	    ;
    eval = vrelg_(v_indx__, v_ran__, &c__99);
/* Computing MAX */
    d__1 = vnormg_(v_indx__, &c__99), d__2 = vnormg_(v_ran__, &c__99);
    expt = vdistg_(v_indx__, v_ran__, &c__99) / max(d__1,d__2);
    chcksd_("EVAL", &eval, "~", &expt, &c_b32, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VSEPG */

    tcase_("Test VSEPG for the angle between a zero vector and a random vect"
	    "or", (ftnlen)66);
    eval = vsepg_(v_zero__, v_ran__, &c__99);
    chcksd_("EVAL", &eval, "~", &c_b32, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VSEPG for the angle between a random vector and a zero vect"
	    "or", (ftnlen)66);
    eval = vsepg_(v_ran__, v_zero__, &c__99);
    chcksd_("EVAL", &eval, "~", &c_b32, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VSEPG for the angle between two same vectors", (ftnlen)49);
    eval = vsepg_(v_indx__, v_indx__, &c__99);
    chcksd_("EVAL", &eval, "~", &c_b32, &c_b32, ok, (ftnlen)4, (ftnlen)1);
    tcase_("Test VSEPG for the angle between a random vector and an index ve"
	    "ctor", (ftnlen)68);
    eval = vsepg_(v_indx__, v_ran__, &c__99);
    expt = acos(vdotg_(v_indx__, v_ran__, &c__99) / (vnormg_(v_indx__, &c__99)
	     * vnormg_(v_ran__, &c__99)));
    chcksd_("EVAL", &eval, "~", &expt, &c_b108, ok, (ftnlen)4, (ftnlen)1);

/*     Cases to test VLCOMG */

    tcase_("Test VLCOMG using an index vector and a random vector", (ftnlen)
	    53);
    scale = t_randd__(&c_b7, &c_b8, &seed);
    dummy = t_randd__(&c_b7, &c_b8, &seed);
    vlcomg_(&c__99, &scale, v_indx__, &dummy, v_ran__, v_out__);
    for (i__ = 1; i__ <= 99; ++i__) {
	v_expt__[(i__1 = i__ - 1) < 99 && 0 <= i__1 ? i__1 : s_rnge("v_expt", 
		i__1, "f_vectorg__", (ftnlen)531)] = scale * v_indx__[(i__2 = 
		i__ - 1) < 99 && 0 <= i__2 ? i__2 : s_rnge("v_indx", i__2, 
		"f_vectorg__", (ftnlen)531)] + dummy * v_ran__[(i__3 = i__ - 
		1) < 99 && 0 <= i__3 ? i__3 : s_rnge("v_ran", i__3, "f_vecto"
		"rg__", (ftnlen)531)];
    }
    chckad_("V_OUT", v_out__, "~", v_expt__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VLCOMG using an zero vector and a random vector", (ftnlen)52)
	    ;
    scale = t_randd__(&c_b7, &c_b8, &seed);
    vlcomg_(&c__99, &scale, v_zero__, &c_b8, v_ran__, v_out__);
    chckad_("V_OUT", v_out__, "~", v_ran__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);

/*     Cases to test VPROJG */

    tcase_("Test VPROJG by projecting a random vector to a zero vector", (
	    ftnlen)58);
    vprojg_(v_ran__, v_zero__, &c__99, v_out__);
    chckad_("V_OUT", v_out__, "~", v_zero__, &c__99, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    tcase_("Test VPROJG by projecting a random vector to an index vector", (
	    ftnlen)60);
    vprojg_(v_ran__, v_indx__, &c__99, v_out__);

/*     Compute V_TMP to be the unit vector along V_INDX. */

    vhatg_(v_indx__, &c__99, v_tmp__);

/*     Check ( V_RAN DOT V_TMP ) .EQ. ( V_OUT DOT V_TMP ). */

    dummy = vdotg_(v_ran__, v_tmp__, &c__99);
    eval = vdotg_(v_out__, v_tmp__, &c__99);
    chcksd_("EVAL", &eval, "~", &dummy, &c_b108, ok, (ftnlen)4, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_vectorg__ */

