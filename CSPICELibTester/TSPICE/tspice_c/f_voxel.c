/* f_voxel.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b28 = 10.;
static doublereal c_b29 = 20.;
static doublereal c_b30 = -30.;
static doublereal c_b31 = -100.;
static doublereal c_b32 = 70.;
static doublereal c_b33 = 120.;
static integer c__3 = 3;
static logical c_true = TRUE_;
static doublereal c_b57 = 30.;
static doublereal c_b59 = -50.;
static doublereal c_b115 = 300.;
static doublereal c_b116 = 200.;
static doublereal c_b117 = -500.;
static integer c__0 = 0;
static integer c__1 = 1;

/* $Procedure F_VOXEL ( VOXEL utility tests ) */
/* Subroutine */ int f_voxel__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7, i__8;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static logical isin;
    extern integer zzvox2id_(integer *, integer *);
    static integer nvox[3];
    extern /* Subroutine */ int zztogrid_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static integer i__, j, k, m;
    extern /* Subroutine */ int zzgetvox_(doublereal *, doublereal *, integer 
	    *, doublereal *, logical *, integer *);
    static doublereal p[3];
    static integer cgoff[3];
    extern /* Subroutine */ int zzvoxcvo_(integer *, integer *, integer *, 
	    integer *, integer *, integer *), tcase_(char *, ftnlen), filli_(
	    integer *, integer *, integer *), vpack_(doublereal *, doublereal 
	    *, doublereal *, doublereal *), topen_(char *, ftnlen);
    static integer voxid, voxel[3], cgof1d;
    extern /* Subroutine */ int t_success__(logical *);
    static integer xcgo1d;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static integer xcgoff[3], cgrscl, cgrcor[3];
    static doublereal grdcor[3];
    static integer xcgcor[3];
    static doublereal xgrcor[3];
    static integer cgnvox[3], voxcor[3], xvoxid, xvoxel[3];
    static doublereal voxori[3], voxsiz, tol;
    static logical xin;
    extern logical zzingrd_(integer *, integer *);

/* $ Abstract */

/*     Exercise private SPICELIB routine associated with voxel grids. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This test family exercises private SPICELIB routine associated */
/*     with voxel grids. Routines covered by this family are: */

/*        ZZGETVOX */
/*        ZZINGRD */
/*        ZZTOGRID */
/*        ZZVOX2ID */
/*        ZZVOXCVO */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 13-FEB-2017 (NJB) */

/*        Corrected ZZGETVOX test titles. */

/*        Previous version 18-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_VOXEL", (ftnlen)7);
/* *********************************************************************** */

/*     ZZINGRD tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZINGRD Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZINGRD: Point is inside grid.", (ftnlen)30);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		xin = TRUE_;
		isin = zzingrd_(nvox, voxel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is outside grid (all components)", (ftnlen)38);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    for (i__ = 1; i__ <= 2; ++i__) {
	voxel[0] = (i__ - 1) * (nvox[0] + 1);
	for (j = 1; j <= 2; ++j) {
	    voxel[1] = (j - 1) * (nvox[1] + 1);
	    for (k = 1; k <= 2; ++k) {
		voxel[2] = (k - 1) * (nvox[2] + 1);
		xin = FALSE_;
		isin = zzingrd_(nvox, voxel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is outside grid (X only", (ftnlen)29);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    for (i__ = 1; i__ <= 2; ++i__) {
	voxel[0] = (i__ - 1) * (nvox[0] + 1);
	i__1 = nvox[1];
	for (j = 1; j <= i__1; ++j) {
	    voxel[1] = j;
	    i__2 = nvox[2];
	    for (k = 1; k <= i__2; ++k) {
		voxel[2] = k;
		xin = FALSE_;
		isin = zzingrd_(nvox, voxel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is outside grid (Y only", (ftnlen)29);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	for (j = 1; j <= 2; ++j) {
	    voxel[1] = (j - 1) * (nvox[1] + 1);
	    i__2 = nvox[2];
	    for (k = 1; k <= i__2; ++k) {
		voxel[2] = k;
		xin = FALSE_;
		isin = zzingrd_(nvox, voxel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is outside grid (Z only", (ftnlen)29);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    for (k = 1; k <= 2; ++k) {
		voxel[2] = (k - 1) * (nvox[2] + 1);
		xin = FALSE_;
		isin = zzingrd_(nvox, voxel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
	    }
	}
    }
/* ********************************************************************* */
/* * */
/* *    ZZINGRD Non-error exceptional cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Grid dimensions are negative", (ftnlen)28);
    nvox[0] = -1;
    nvox[1] = -2;
    nvox[2] = -3;
    voxel[0] = 0;
    voxel[1] = 0;
    voxel[2] = 0;
    xin = FALSE_;
    isin = zzingrd_(nvox, voxel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
/* *********************************************************************** */

/*     ZZTOGRID tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZTOGRID normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTOGRID normal case", (ftnlen)20);
    vpack_(&c_b28, &c_b29, &c_b30, voxori);
    vpack_(&c_b31, &c_b32, &c_b33, p);
    voxsiz = 10.;
    zztogrid_(p, voxori, &voxsiz, grdcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	xgrcor[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xgrcor", 
		i__1, "f_voxel__", (ftnlen)433)] = (p[(i__2 = i__ - 1) < 3 && 
		0 <= i__2 ? i__2 : s_rnge("p", i__2, "f_voxel__", (ftnlen)433)
		] - voxori[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		"voxori", i__3, "f_voxel__", (ftnlen)433)]) / voxsiz;
    }
    tol = 1e-14;
    chckad_("GDRCOR", grdcor, "~~/", xgrcor, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);
/* ********************************************************************* */
/* * */
/* *    ZZTOGRID error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTOGRID non-positive voxel size.", (ftnlen)33);
    vpack_(&c_b28, &c_b29, &c_b30, voxori);
    vpack_(&c_b31, &c_b32, &c_b33, p);
    voxsiz = 0.;
    zztogrid_(p, voxori, &voxsiz, grdcor);
    chckxc_(&c_true, "SPICE(NONPOSITIVEVALUE)", ok, (ftnlen)23);
    voxsiz = -1.;
    zztogrid_(p, voxori, &voxsiz, grdcor);
    chckxc_(&c_true, "SPICE(NONPOSITIVEVALUE)", ok, (ftnlen)23);
/* *********************************************************************** */

/*     ZZGETVOX tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZGETVOX normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is at voxel center.", (ftnlen)35);
    voxsiz = 10.;
    vpack_(&c_b57, &c_b29, &c_b59, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		for (m = 1; m <= 3; ++m) {
		    p[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge("p", 
			    i__4, "f_voxel__", (ftnlen)516)] = voxori[(i__5 = 
			    m - 1) < 3 && 0 <= i__5 ? i__5 : s_rnge("voxori", 
			    i__5, "f_voxel__", (ftnlen)516)] + (voxel[(i__6 = 
			    m - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge("voxel", 
			    i__6, "f_voxel__", (ftnlen)516)] - 1 + .5) * 
			    voxsiz;
		}
		zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		xin = TRUE_;
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
		chckai_("VOXCOR", voxcor, "=", voxel, &c__3, ok, (ftnlen)6, (
			ftnlen)1);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is at high-indexed corner of voxel.", (ftnlen)51);

/*     Use integer values throughout to avoid round-off. */

    voxsiz = 10.;
    vpack_(&c_b57, &c_b29, &c_b59, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		for (m = 1; m <= 3; ++m) {
		    p[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge("p", 
			    i__4, "f_voxel__", (ftnlen)567)] = voxori[(i__5 = 
			    m - 1) < 3 && 0 <= i__5 ? i__5 : s_rnge("voxori", 
			    i__5, "f_voxel__", (ftnlen)567)] + voxel[(i__6 = 
			    m - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge("voxel", 
			    i__6, "f_voxel__", (ftnlen)567)] * voxsiz;
		}
		zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		xin = TRUE_;
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
		for (m = 1; m <= 3; ++m) {
/* Computing MIN */
		    i__7 = voxel[(i__5 = m - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("voxel", i__5, "f_voxel__", (ftnlen)579)] 
			    + 1, i__8 = nvox[(i__6 = m - 1) < 3 && 0 <= i__6 ?
			     i__6 : s_rnge("nvox", i__6, "f_voxel__", (ftnlen)
			    579)];
		    xvoxel[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
			    "xvoxel", i__4, "f_voxel__", (ftnlen)579)] = min(
			    i__7,i__8);
		}

/*              The validity of this check relies on accurate integer */
/*              arithmetic being done with values of double precision */
/*              type. */

		chckai_("VOXCOR", voxcor, "=", xvoxel, &c__3, ok, (ftnlen)6, (
			ftnlen)1);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is at low-indexed corner of voxel.", (ftnlen)50);

/*     Use integer values throughout to avoid round-off. */

    voxsiz = 10.;
    vpack_(&c_b57, &c_b29, &c_b59, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		for (m = 1; m <= 3; ++m) {
		    p[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge("p", 
			    i__4, "f_voxel__", (ftnlen)627)] = voxori[(i__5 = 
			    m - 1) < 3 && 0 <= i__5 ? i__5 : s_rnge("voxori", 
			    i__5, "f_voxel__", (ftnlen)627)] + (voxel[(i__6 = 
			    m - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge("voxel", 
			    i__6, "f_voxel__", (ftnlen)627)] - 1) * voxsiz;
		}
		zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		xin = TRUE_;
		chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
		for (m = 1; m <= 3; ++m) {
		    xvoxel[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
			    "xvoxel", i__4, "f_voxel__", (ftnlen)638)] = 
			    voxel[(i__5 = m - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("voxel", i__5, "f_voxel__", (ftnlen)638)];
		}

/*              The validity of this check relies on accurate integer */
/*              arithmetic being done with values of double precision */
/*              type. */

		chckai_("VOXCOR", voxcor, "=", xvoxel, &c__3, ok, (ftnlen)6, (
			ftnlen)1);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; X < min X", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] - 1e-13;
    p[1] = voxori[1] + 1.;
    p[2] = voxori[2] + 1.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; X > max X", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] + nvox[0] * voxsiz + 1e-13;
    p[1] = voxori[1] + 1.;
    p[2] = voxori[2] + 1.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; Y < min Y", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] + 1.;
    p[1] = voxori[1] - 1e-13;
    p[2] = voxori[2] + 1.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; Y > max Y", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] + 1.;
    p[1] = voxori[1] + voxsiz * nvox[1] + 1e-13;
    p[2] = voxori[2] + 1.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; Z < min Z", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] + 1.;
    p[1] = voxori[1] + 1.;
    p[2] = voxori[2] - 1e-13;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX: Point is outside grid; Z > max Z", (ftnlen)42);
    voxsiz = 20.;
    vpack_(&c_b115, &c_b116, &c_b117, voxori);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    p[0] = voxori[0] + 1.;
    p[1] = voxori[1] + 1.;
    p[2] = voxori[2] + voxsiz * nvox[2] + 1e-13;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("ISIN", &isin, &xin, ok, (ftnlen)4);
/* *********************************************************************** */

/*     ZZGETVOX error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZGETVOX non-positive voxel size.", (ftnlen)33);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[2] = 7;
    vpack_(&c_b28, &c_b29, &c_b30, voxori);
    vpack_(&c_b31, &c_b32, &c_b33, p);
    voxsiz = 0.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_true, "SPICE(NONPOSITIVEVALUE)", ok, (ftnlen)23);
    voxsiz = -1.;
    zzgetvox_(&voxsiz, voxori, nvox, p, &isin, voxcor);
    chckxc_(&c_true, "SPICE(NONPOSITIVEVALUE)", ok, (ftnlen)23);
/* *********************************************************************** */

/*     ZZVOX2ID tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZVOX2ID normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZVOX2ID: point is inside grid.", (ftnlen)31);
    nvox[0] = 10;
    nvox[1] = 15;
    nvox[1] = 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		voxid = zzvox2id_(voxel, nvox);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute the 1-D index of the voxel. The index */
/*              is 1-based. */

		xvoxid = (voxel[2] - 1) * nvox[1] * nvox[0] + (voxel[1] - 1) *
			 nvox[0] + voxel[0];
		chcksi_("VOXID", &voxid, "=", &xvoxid, &c__0, ok, (ftnlen)5, (
			ftnlen)1);
	    }
	}
    }
/* *********************************************************************** */

/*     ZZVOX2ID error cases */

/* *********************************************************************** */

/*     None. The routine has no special logical to deal with voxel */
/*     coordinates that are out of range. */

/* *********************************************************************** */

/*     ZZVOXCVO tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZVOXCVO normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZVOXCVO: point is inside grid.", (ftnlen)31);
    cgrscl = 3;
    filli_(&cgrscl, &c__3, cgnvox);
    nvox[0] = cgrscl * 10;
    nvox[1] = cgrscl * 15;
    nvox[1] = cgrscl * 7;
    i__1 = nvox[0];
    for (i__ = 1; i__ <= i__1; ++i__) {
	voxel[0] = i__;
	i__2 = nvox[1];
	for (j = 1; j <= i__2; ++j) {
	    voxel[1] = j;
	    i__3 = nvox[2];
	    for (k = 1; k <= i__3; ++k) {
		voxel[2] = k;
		zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute the expected coordinates of the coarse voxel */
/*              containing the fine voxel VOXEL. The coordinates */
/*              are 1-based. */

		for (m = 1; m <= 3; ++m) {
		    xcgcor[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
			    "xcgcor", i__4, "f_voxel__", (ftnlen)958)] = (
			    voxel[(i__5 = m - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("voxel", i__5, "f_voxel__", (ftnlen)958)] 
			    - 1) / cgrscl + 1;
		}
		chckai_("CGRCOR", cgrcor, "=", xcgcor, &c__3, ok, (ftnlen)6, (
			ftnlen)1);

/*              Compute the expected coordinates of the fine voxel */
/*              relative to the base of the coarse voxel containing it. */
/*              The relative coordinates are 1-based. */

		for (m = 1; m <= 3; ++m) {
		    xcgoff[(i__4 = m - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
			    "xcgoff", i__4, "f_voxel__", (ftnlen)969)] = 
			    voxel[(i__5 = m - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("voxel", i__5, "f_voxel__", (ftnlen)969)] 
			    - (cgrcor[(i__6 = m - 1) < 3 && 0 <= i__6 ? i__6 :
			     s_rnge("cgrcor", i__6, "f_voxel__", (ftnlen)969)]
			     - 1) * cgrscl;
		}
		chckai_("XCGOFF", xcgoff, "=", cgoff, &c__3, ok, (ftnlen)6, (
			ftnlen)1);

/*              Convert the expected fine voxel offset to a 1-D offset. */

		xcgo1d = zzvox2id_(cgoff, cgnvox);
		chcksi_("CGOF1D", &cgof1d, "=", &xcgo1d, &c__0, ok, (ftnlen)6,
			 (ftnlen)1);
	    }
	}
    }
/* *********************************************************************** */

/*     ZZVOXCVO error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZVOXCVO: non-positive fine grid dimensions.", (ftnlen)44);
    cgrscl = 2;
    nvox[0] = -cgrscl;
    nvox[1] = cgrscl * 15;
    nvox[2] = cgrscl * 7;
    filli_(&c__1, &c__3, voxel);
    zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    nvox[0] = cgrscl;
    nvox[1] = cgrscl * -15;
    nvox[2] = cgrscl * 7;
    zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    nvox[0] = cgrscl;
    nvox[1] = cgrscl * 15;
    nvox[2] = cgrscl * -7;
    zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZVOXCVO: fine voxel coordinates out of range.", (ftnlen)46);
    cgrscl = 2;
    nvox[0] = cgrscl;
    nvox[1] = cgrscl * 15;
    nvox[2] = cgrscl * 7;
    for (i__ = 1; i__ <= 3; ++i__) {
	filli_(&c__1, &c__3, voxel);
	voxel[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("voxel", i__1,
		 "f_voxel__", (ftnlen)1043)] = 0;
	zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
	voxel[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("voxel", i__1,
		 "f_voxel__", (ftnlen)1048)] = nvox[(i__2 = i__ - 1) < 3 && 0 
		<= i__2 ? i__2 : s_rnge("nvox", i__2, "f_voxel__", (ftnlen)
		1048)] + 1;
	zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
	chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZVOXCVO: non-positive coarse voxel scale.", (ftnlen)42);
    cgrscl = 0;
    nvox[0] = cgrscl;
    nvox[1] = cgrscl * 15;
    nvox[2] = cgrscl * 7;
    filli_(&c__1, &c__3, voxel);
    zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    cgrscl = -1;
    zzvoxcvo_(voxel, nvox, &cgrscl, cgrcor, cgoff, &cgof1d);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_voxel__ */

