/* f_win.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__20000 = 20000;
static integer c__0 = 0;
static doublereal c_b6 = 0.;
static doublereal c_b7 = -1.;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__116 = 116;
static doublereal c_b21 = 1.;
static integer c__90 = 90;
static integer c__13 = 13;

/* $Procedure F_WIN ( Window family tests ) */
/* Subroutine */ int f_win__(logical *ok)
{
    doublereal meas;
    integer long__;
    extern /* Subroutine */ int zzwninsd_(doublereal *, doublereal *, char *, 
	    doublereal *, ftnlen);
    integer m;
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    ;
    integer short__;
    extern /* Subroutine */ int t_success__(logical *), scardd_(integer *, 
	    doublereal *), chckxc_(logical *, char *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen), getmsg_(char *, char *, ftnlen, ftnlen);
    doublereal stddev;
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wnsumd_(
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *,
	     integer *);
    doublereal result[20006], avg;
    char msg[320];
    extern integer pos_(char *, char *, integer *, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests SPICELIB window routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 03-MAR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Begin every test family with an open call. */

    topen_("F_WIN", (ftnlen)5);

/*   Case 1 */


    tcase_("Check ZZWNINSD error cases", (ftnlen)26);
    ssized_(&c__20000, result);
    scardd_(&c__0, result);

/*     Try to add an invalid interval to the window. */

    zzwninsd_(&c_b6, &c_b7, "TEST_STRING_1", result, (ftnlen)13);
    getmsg_("LONG", msg, (ftnlen)4, (ftnlen)320);
    chckxc_(&c_true, "SPICE(BADENDPOINTS)", ok, (ftnlen)19);
    m = pos_(msg, "TEST_STRING_1", &c__1, (ftnlen)320, (ftnlen)13);
    chcksi_("TEST_STRING_1", &m, "=", &c__116, &c__0, ok, (ftnlen)13, (ftnlen)
	    1);

/*     Empty the window, then reduce size to zero. */

    ssized_(&c__0, result);
    scardd_(&c__0, result);

/*     Try to add an interval to the window. */

    zzwninsd_(&c_b6, &c_b21, "TEST_STRING_2", result, (ftnlen)13);
    getmsg_("LONG", msg, (ftnlen)4, (ftnlen)320);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);
    m = pos_(msg, "TEST_STRING_2", &c__1, (ftnlen)320, (ftnlen)13);
    chcksi_("TEST_STRING_2", &m, "=", &c__90, &c__0, ok, (ftnlen)13, (ftnlen)
	    1);

/*   Case 2 */


    tcase_("Check WNSUMD error case", (ftnlen)23);

/*     An odd cardinality for a WNSUMD input window should */
/*     signal an error. */

    ssized_(&c__20000, result);
    scardd_(&c__13, result);
    wnsumd_(result, &meas, &avg, &stddev, &short__, &long__);
    chckxc_(&c_true, "SPICE(INVALIDCARDINALITY)", ok, (ftnlen)25);
    t_success__(ok);
    return 0;
} /* f_win__ */

