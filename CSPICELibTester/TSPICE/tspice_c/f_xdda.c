/* f_xdda.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 10.;
static doublereal c_b9 = -1.;
static integer c__3 = 3;
static logical c_true = TRUE_;
static doublereal c_b19 = 40.;
static doublereal c_b39 = 15.;
static doublereal c_b41 = 5.;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__10000 = 10000;
static doublereal c_b329 = 1.;
static doublereal c_b330 = .5;
static integer c__10 = 10;
static integer c__14 = 14;

/* $Procedure F_XDDA ( XDDA tests ) */
/* Subroutine */ int f_xdda__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };
    static doublereal voxori[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_stop(char *
	    , ftnlen);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    ), xdda_(doublereal *, doublereal *, integer *, integer *, 
	    integer *, integer *);
    static doublereal dlat, dlon;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static integer xvox[3];
    extern /* Subroutine */ int t_chkvox__(doublereal *, doublereal *, 
	    integer *, integer *, integer *);
    static doublereal d__;
    extern /* Subroutine */ int zzraybox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *);
    static integer i__, j, k, m;
    static char label[80];
    static integer s;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal vtemp[3];
    extern doublereal vnorm_(doublereal *);
    extern logical vzero_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    static integer ub, vc, xc;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen);
    static integer vr, xr;
    extern /* Subroutine */ int latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    extern integer brckti_(integer *, integer *, integer *);
    static doublereal center[3], egress[3], raydir[3];
    static integer vcoord[2], xcoord[2];
    static doublereal dxtent[3];
    static integer extent[3];
    static doublereal vertex[3];
    static integer maxnvx;
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    static doublereal boxvtx[3];
    static integer voxlst[30000]	/* was [3][10000] */;
    static doublereal lat;
    extern doublereal rpd_(void);
    static doublereal lon, xpt[3];
    static integer nvx;

/* $ Abstract */

/*     Exercise the ray-voxel grid intersection routine XDDA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSKLIB type 2 ray-voxel grid */
/*     intersection routine XDDA. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 10-JUL-2014 (NJB) */

/*        Updated to text XDDA bug fix. Logic */
/*        for vertices slightly outside the grid */
/*        or just inside it is now tested. */

/* -    TSPICE Version 1.0.0, 05-JUN-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     The parameter GRDTOL must be kept in sync with */
/*     the value used in XDDA. */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_XDDA", (ftnlen)6);

/*     Test XDDA:  start out with error handling. */

/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray direction is zero vector.", (ftnlen)29);

/*     Default inputs: */


/*     EXTENT is of integer type, so we can't use VPACK. */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    vpack_(&c_b4, &c_b4, &c_b6, vertex);
    vpack_(&c_b4, &c_b4, &c_b9, raydir);
    maxnvx = 10000;

/*     Zero out the direction vector. */

    cleard_(&c__3, raydir);
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);
    vpack_(&c_b4, &c_b4, &c_b9, raydir);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive extents", (ftnlen)20);

/*     Default inputs: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    vpack_(&c_b4, &c_b4, &c_b19, vertex);
    vpack_(&c_b4, &c_b4, &c_b9, raydir);
    maxnvx = 10000;

/*     Set an invalid extent value. */

    extent[0] = 0;
    extent[1] = 20;
    extent[2] = 10;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);
    extent[0] = -1;
    extent[1] = 20;
    extent[2] = 10;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);
    extent[0] = 10;
    extent[1] = 0;
    extent[2] = 10;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);
    extent[0] = 10;
    extent[1] = -1;
    extent[2] = 10;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);
    extent[0] = 10;
    extent[1] = 20;
    extent[2] = 0;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);
    extent[0] = 10;
    extent[1] = 20;
    extent[2] = -1;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(BADDIMENSIONS)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Vertex is not in voxel grid", (ftnlen)27);

/*     Default inputs: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    maxnvx = 10000;
    vpack_(&c_b4, &c_b4, &c_b9, raydir);

/*     Vertex is outside of grid by one unit. */

    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 0; j <= 1; ++j) {
	    s = (j << 1) - 1;
	    vpack_(&c_b39, &c_b6, &c_b41, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_xdda__", (ftnlen)334)] = vertex[(i__2 = i__ - 1)
		     < 3 && 0 <= i__2 ? i__2 : s_rnge("vertex", i__2, "f_xdd"
		    "a__", (ftnlen)334)] + s * (extent[(i__3 = i__ - 1) < 3 && 
		    0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_xdda__", (
		    ftnlen)334)] / 2. + 1);
	    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
	    chckxc_(&c_true, "SPICE(VERTEXNOTINGRID)", ok, (ftnlen)22);
	}
    }

/*     Vertex is outside of grid by less than the limit. */

    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 0; j <= 1; ++j) {
	    s = (j << 1) - 1;
	    vpack_(&c_b39, &c_b6, &c_b41, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_xdda__", (ftnlen)354)] = vertex[(i__2 = i__ - 1)
		     < 3 && 0 <= i__2 ? i__2 : s_rnge("vertex", i__2, "f_xdd"
		    "a__", (ftnlen)354)] + s * (extent[(i__3 = i__ - 1) < 3 && 
		    0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_xdda__", (
		    ftnlen)354)] * 1.0000000000010001 / 2);
	    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Output array size is non-positive", (ftnlen)33);

/*     Default inputs: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    vpack_(&c_b4, &c_b4, &c_b6, vertex);
    vpack_(&c_b4, &c_b4, &c_b9, raydir);

/*     Set an invalid array size value. */

    maxnvx = 0;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(INVALIDSIZE)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Output array size is too small", (ftnlen)30);

/*     Default inputs: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    vpack_(&c_b39, &c_b6, &c_b6, vertex);
    vpack_(&c_b4, &c_b4, &c_b9, raydir);

/*     Set an invalid array size value. */

    maxnvx = 9;
    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
    chckxc_(&c_true, "SPICE(ARRAYTOOSMALL)", ok, (ftnlen)20);
/* ********************************************************************** */

/*     Non-error exception cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     In the following cases, the vertex is slightly outside of the */
/*     voxel grid, and an intersection exists. Rays are chosen so */
/*     that incorrect determination of the vertex offset should lead */
/*     to incorrect determination of the second or third intersected */
/*     voxels. */


/*     Grid extents: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    maxnvx = 10000;
    for (i__ = 1; i__ <= 3; ++i__) {
	xcoord[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_xdda__", (ftnlen)444)];
	xcoord[1] = next[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("next", i__1, "f_xdda__", (ftnlen)445)];
	for (j = 0; j <= 1; ++j) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Vertex is not in voxel grid; intersection exists."
		    " I = #; J = #.", (ftnlen)320, (ftnlen)63);
	    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)320);
	    s = (j << 1) - 1;

/*           Vertex is outside of grid by less than the limit. */

	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_xdda__", (ftnlen)469)] = extent[(i__2 = i__ - 1)
		     < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_xdd"
		    "a__", (ftnlen)469)] / 2. + s * (extent[(i__3 = i__ - 1) < 
		    3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_xdda__",
		     (ftnlen)469)] * 1.0000000000010001 / 2.);
	    vertex[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "vertex", i__1, "f_xdda__", (ftnlen)472)] = (extent[(i__2 
		    = xcoord[0] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent"
		    , i__2, "f_xdda__", (ftnlen)472)] + 1.) / 2.;
	    vertex[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "vertex", i__1, "f_xdda__", (ftnlen)473)] = (extent[(i__2 
		    = xcoord[1] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent"
		    , i__2, "f_xdda__", (ftnlen)473)] + 1.) / 2.;
/*            WRITE (*,*) 'VERTEX(i)         = ', VERTEX(I) */
/*            WRITE (*,*) 'VERTEX(xcoord(1)) = ', VERTEX(XCOORD(1)) */
/*            WRITE (*,*) 'VERTEX(xcoord(2)) = ', VERTEX(XCOORD(2)) */
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_xdda__", (ftnlen)481)] = (doublereal) (-s);
	    raydir[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "raydir", i__1, "f_xdda__", (ftnlen)482)] = (doublereal) (
		    abs(s) << 1);
	    raydir[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "raydir", i__1, "f_xdda__", (ftnlen)483)] = (doublereal) (
		    abs(s) * 3);
	    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           In all cases at least 3 voxels should be hit. */

	    s_copy(label, "NVX (#,#)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &nvx, ">=", &c__3, &c__0, ok, (ftnlen)80, (ftnlen)
		    2);

/*           Check the first voxel returned. First compute */
/*           the expected values of its components. */

	    if (s < 0) {
		xvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xvox",
			 i__1, "f_xdda__", (ftnlen)504)] = 1;
	    } else {
		xvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xvox",
			 i__1, "f_xdda__", (ftnlen)506)] = extent[(i__2 = i__ 
			- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", i__2, 
			"f_xdda__", (ftnlen)506)];
	    }
	    i__4 = (integer) vertex[(i__2 = xcoord[0] - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("vertex", i__2, "f_xdda__", (ftnlen)509)] + 
		    1;
	    xvox[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)509)] = brckti_(&i__4, &
		    c__1, &extent[(i__3 = xcoord[0] - 1) < 3 && 0 <= i__3 ? 
		    i__3 : s_rnge("extent", i__3, "f_xdda__", (ftnlen)509)]);
	    i__4 = (integer) vertex[(i__2 = xcoord[1] - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("vertex", i__2, "f_xdda__", (ftnlen)513)] + 
		    1;
	    xvox[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)513)] = brckti_(&i__4, &
		    c__1, &extent[(i__3 = xcoord[1] - 1) < 3 && 0 <= i__3 ? 
		    i__3 : s_rnge("extent", i__3, "f_xdda__", (ftnlen)513)]);
	    s_copy(label, "VOXEL 1 (#,#)", (ftnlen)80, (ftnlen)13);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_(label, voxlst, "=", xvox, &c__3, ok, (ftnlen)80, (ftnlen)
		    1);
/*            WRITE (*,*) '1 XVOX = ', XVOX */

/*           We expect the coordinates of the second voxel to match, */
/*           except for coordinate indexed XCOORD(2), which should */
/*           be incremented. */

	    xvox[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)531)] = xvox[(i__2 = 
		    xcoord[1] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("xvox", 
		    i__2, "f_xdda__", (ftnlen)531)] + 1;

/*           Instead of calling CHCKAI, we'll check each voxel */
/*           element individually. */

/*            WRITE (*,*) '2 XVOX = ', XVOX */
	    for (k = 1; k <= 3; ++k) {
		s_copy(label, "VOXEL 2 (#) ", (ftnlen)80, (ftnlen)12);
		repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_(label, &voxlst[(i__1 = k + 2) < 30000 && 0 <= i__1 ? 
			i__1 : s_rnge("voxlst", i__1, "f_xdda__", (ftnlen)545)
			], "=", &xvox[(i__2 = k - 1) < 3 && 0 <= i__2 ? i__2 :
			 s_rnge("xvox", i__2, "f_xdda__", (ftnlen)545)], &
			c__0, ok, (ftnlen)80, (ftnlen)1);
	    }

/*           We expect the coordinates of the third voxel to match those */
/*           of the second, except for coordinate indexed XCOORD(1), */
/*           which should be incremented. */

	    xvox[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)553)] = xvox[(i__2 = 
		    xcoord[0] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("xvox", 
		    i__2, "f_xdda__", (ftnlen)553)] + 1;
	    for (k = 1; k <= 3; ++k) {
		s_copy(label, "VOXEL 3 (#) ", (ftnlen)80, (ftnlen)12);
		repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_(label, &voxlst[(i__1 = k + 5) < 30000 && 0 <= i__1 ? 
			i__1 : s_rnge("voxlst", i__1, "f_xdda__", (ftnlen)561)
			], "=", &xvox[(i__2 = k - 1) < 3 && 0 <= i__2 ? i__2 :
			 s_rnge("xvox", i__2, "f_xdda__", (ftnlen)561)], &
			c__0, ok, (ftnlen)80, (ftnlen)1);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     In the following cases, the vertex is slightly inside of the */
/*     voxel grid. Rays are chosen so that incorrect determination of */
/*     the vertex offset should lead to incorrect determination of the */
/*     second or third intersected voxels. */


/*     Grid extents: */

    extent[0] = 30;
    extent[1] = 20;
    extent[2] = 10;
    maxnvx = 10000;
    for (i__ = 1; i__ <= 3; ++i__) {
	xcoord[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_xdda__", (ftnlen)591)];
	xcoord[1] = next[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("next", i__1, "f_xdda__", (ftnlen)592)];
	for (j = 0; j <= 1; ++j) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Vertex is just inside voxel grid; intersection ex"
		    "ists. I = #; J = #.", (ftnlen)320, (ftnlen)68);
	    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)320)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)320);
	    s = (j << 1) - 1;

/*           Vertex is inside of grid and separated from the */
/*           nearest boundary plane by a small amount. */

	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_xdda__", (ftnlen)617)] = extent[(i__2 = i__ - 1)
		     < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_xdd"
		    "a__", (ftnlen)617)] / 2. + s * (extent[(i__3 = i__ - 1) < 
		    3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_xdda__",
		     (ftnlen)617)] * .99999999999900002 / 2.);
	    vertex[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "vertex", i__1, "f_xdda__", (ftnlen)620)] = (extent[(i__2 
		    = xcoord[0] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent"
		    , i__2, "f_xdda__", (ftnlen)620)] + 1.) / 2.;
	    vertex[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "vertex", i__1, "f_xdda__", (ftnlen)621)] = (extent[(i__2 
		    = xcoord[1] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent"
		    , i__2, "f_xdda__", (ftnlen)621)] + 1.) / 2.;
/*            WRITE (*,*) 'VERTEX(i)         = ', VERTEX(I) */
/*            WRITE (*,*) 'VERTEX(xcoord(1)) = ', VERTEX(XCOORD(1)) */
/*            WRITE (*,*) 'VERTEX(xcoord(2)) = ', VERTEX(XCOORD(2)) */
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_xdda__", (ftnlen)629)] = (doublereal) (-s);
	    raydir[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "raydir", i__1, "f_xdda__", (ftnlen)630)] = (doublereal) (
		    abs(s) << 1);
	    raydir[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "raydir", i__1, "f_xdda__", (ftnlen)631)] = (doublereal) (
		    abs(s) * 3);
	    xdda_(vertex, raydir, extent, &maxnvx, &nvx, voxlst);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           In all cases at least 3 voxels should be hit. */

	    s_copy(label, "NVX (#,#)", (ftnlen)80, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &nvx, ">=", &c__3, &c__0, ok, (ftnlen)80, (ftnlen)
		    2);

/*           Check the first voxel returned. First compute */
/*           the expected values of its components. */

	    if (s < 0) {
		xvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xvox",
			 i__1, "f_xdda__", (ftnlen)652)] = 1;
	    } else {
		xvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xvox",
			 i__1, "f_xdda__", (ftnlen)654)] = extent[(i__2 = i__ 
			- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", i__2, 
			"f_xdda__", (ftnlen)654)];
	    }
	    i__4 = (integer) vertex[(i__2 = xcoord[0] - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("vertex", i__2, "f_xdda__", (ftnlen)657)] + 
		    1;
	    xvox[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)657)] = brckti_(&i__4, &
		    c__1, &extent[(i__3 = xcoord[0] - 1) < 3 && 0 <= i__3 ? 
		    i__3 : s_rnge("extent", i__3, "f_xdda__", (ftnlen)657)]);
	    i__4 = (integer) vertex[(i__2 = xcoord[1] - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("vertex", i__2, "f_xdda__", (ftnlen)661)] + 
		    1;
	    xvox[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)661)] = brckti_(&i__4, &
		    c__1, &extent[(i__3 = xcoord[1] - 1) < 3 && 0 <= i__3 ? 
		    i__3 : s_rnge("extent", i__3, "f_xdda__", (ftnlen)661)]);
	    s_copy(label, "VOXEL 1 (#,#)", (ftnlen)80, (ftnlen)13);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_(label, voxlst, "=", xvox, &c__3, ok, (ftnlen)80, (ftnlen)
		    1);
/*            WRITE (*,*) '1 XVOX = ', XVOX */

/*           We expect the coordinates of the second voxel to match, */
/*           except for coordinate indexed XCOORD(2), which should */
/*           be incremented. */

	    xvox[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)679)] = xvox[(i__2 = 
		    xcoord[1] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("xvox", 
		    i__2, "f_xdda__", (ftnlen)679)] + 1;

/*           Instead of calling CHCKAI, we'll check each voxel */
/*           element individually. */

/*            WRITE (*,*) '2 XVOX = ', XVOX */
	    for (k = 1; k <= 3; ++k) {
		s_copy(label, "VOXEL 2 (#) ", (ftnlen)80, (ftnlen)12);
		repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_(label, &voxlst[(i__1 = k + 2) < 30000 && 0 <= i__1 ? 
			i__1 : s_rnge("voxlst", i__1, "f_xdda__", (ftnlen)693)
			], "=", &xvox[(i__2 = k - 1) < 3 && 0 <= i__2 ? i__2 :
			 s_rnge("xvox", i__2, "f_xdda__", (ftnlen)693)], &
			c__0, ok, (ftnlen)80, (ftnlen)1);
	    }

/*           We expect the coordinates of the third voxel to match those */
/*           of the second, except for coordinate indexed XCOORD(1), */
/*           which should be incremented. */

	    xvox[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "xvox", i__1, "f_xdda__", (ftnlen)701)] = xvox[(i__2 = 
		    xcoord[0] - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("xvox", 
		    i__2, "f_xdda__", (ftnlen)701)] + 1;
	    for (k = 1; k <= 3; ++k) {
		s_copy(label, "VOXEL 3 (#) ", (ftnlen)80, (ftnlen)12);
		repmi_(label, "#", &k, label, (ftnlen)80, (ftnlen)1, (ftnlen)
			80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_(label, &voxlst[(i__1 = k + 5) < 30000 && 0 <= i__1 ? 
			i__1 : s_rnge("voxlst", i__1, "f_xdda__", (ftnlen)709)
			], "=", &xvox[(i__2 = k - 1) < 3 && 0 <= i__2 ? i__2 :
			 s_rnge("xvox", i__2, "f_xdda__", (ftnlen)709)], &
			c__0, ok, (ftnlen)80, (ftnlen)1);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     The following tests exercise logic that deals with rays that */
/*     are nearly parallel to a coordinate axis. */

    for (i__ = 1; i__ <= 3; ++i__) {
	xcoord[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_xdda__", (ftnlen)731)];
	xcoord[1] = next[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("next", i__1, "f_xdda__", (ftnlen)732)];

/*        The selected value of EXTENT(I) is less than MAXLST. */

	extent[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("extent", 
		i__1, "f_xdda__", (ftnlen)737)] = 9999;
	extent[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("exte"
		"nt", i__1, "f_xdda__", (ftnlen)738)] = 2;
	extent[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("exte"
		"nt", i__1, "f_xdda__", (ftnlen)739)] = 2;
	for (j = 0; j <= 1; ++j) {
	    for (k = 0; k <= 1; ++k) {

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Ray is nearly parallel to  coordinate axis te"
			"st: I = #; J = #; K = #. Vertex is at face center.", (
			ftnlen)320, (ftnlen)95);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"vertex", i__1, "f_xdda__", (ftnlen)759)] = 0.;
		vertex[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("vertex", i__1, "f_xdda__", (ftnlen)760)] = 1.;
		vertex[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("vertex", i__1, "f_xdda__", (ftnlen)761)] = 1.;

/*              Use small magnitudes for ray components that */
/*              are orthogonal to the Ith component. */

		raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"raydir", i__1, "f_xdda__", (ftnlen)767)] = 1.;
		raydir[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("raydir", i__1, "f_xdda__", (ftnlen)768)] = ((
			j << 1) - 1) * 1e-300;
		raydir[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("raydir", i__1, "f_xdda__", (ftnlen)769)] = ((
			k << 1) - 1) * 1e-30;
		xdda_(vertex, raydir, extent, &c__10000, &nvx, voxlst);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the list. */

		t_chkvox__(vertex, raydir, extent, &nvx, voxlst);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "Ray is nearly parallel to  coordinate axis te"
			"st: I = #; J = #; K = #. Vertex is at face edge.", (
			ftnlen)320, (ftnlen)93);
		repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		repmi_(title, "#", &k, title, (ftnlen)320, (ftnlen)1, (ftnlen)
			320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"vertex", i__1, "f_xdda__", (ftnlen)795)] = 0.;
		vertex[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("vertex", i__1, "f_xdda__", (ftnlen)796)] = 2.;
		vertex[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("vertex", i__1, "f_xdda__", (ftnlen)797)] = 2.;

/*              Use small magnitudes for ray components that */
/*              are orthogonal to the Ith component. */

		raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"raydir", i__1, "f_xdda__", (ftnlen)803)] = 1.;
		raydir[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("raydir", i__1, "f_xdda__", (ftnlen)804)] = ((
			j << 1) - 1) * 1e-300;
		raydir[(i__1 = xcoord[1] - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("raydir", i__1, "f_xdda__", (ftnlen)805)] = ((
			k << 1) - 1) * 1e-30;
		xdda_(vertex, raydir, extent, &c__10000, &nvx, voxlst);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the list. In this case the checking code */
/*              may fail to correctly compute the expected point */
/*              of exit from the grid. However, we can get the */
/*              checking algorithm to work by passing in a ray direction */
/*              for which the components orthogonal to the Ith axis */
/*              have been zeroed out. */

		cleard_(&c__3, vtemp);
		vtemp[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vte"
			"mp", i__1, "f_xdda__", (ftnlen)820)] = 1.;
		t_chkvox__(vertex, vtemp, extent, &nvx, voxlst);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}
    }
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Trivial case: grid has one voxel. Vertex is in the middle of the"
	    " X = 1 face. Ray points inward.", (ftnlen)95);
    vpack_(&c_b329, &c_b330, &c_b330, vertex);
    vpack_(&c_b9, &c_b4, &c_b4, raydir);
    extent[0] = 1;
    extent[1] = 1;
    extent[2] = 1;
    xdda_(vertex, raydir, extent, &c__10, &nvx, voxlst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NVX", &nvx, "=", &c__1, &c__0, ok, (ftnlen)3, (ftnlen)1);
    t_chkvox__(vertex, raydir, extent, &nvx, voxlst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Using a small but non-trivial grid, place vertices of rays */
/*     at the midpoint of each exterior voxel face. Place exit points */
/*     at the midpoint of each exterior voxel face on grid surfaces */
/*     other than the one containing the vertex. */

    extent[0] = 4;
    extent[1] = 5;
    extent[2] = 6;
    maxnvx = 10000;
    cleard_(&c__3, raydir);
    cleard_(&c__3, vertex);
    cleard_(&c__3, egress);
    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the "horizontal" coordinates on the vertex face */
/*        of the grid: these are orthogonal to the Ith axis. */

	vcoord[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_xdda__", (ftnlen)886)];
	vcoord[1] = next[(i__1 = vcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("next", i__1, "f_xdda__", (ftnlen)887)];

/*        Loop over both vertex faces orthogonal to axis I. */

	for (j = 0; j <= 1; ++j) {

/*           Loop over the exit faces. There's one face on */
/*           the opposite side from the vertex face, and four */
/*           faces normal to the horizontal coordinate axes. */

	    for (k = 1; k <= 3; ++k) {

/*              Set the "horizontal" coordinates on the exit face */
/*              of the grid: these are orthogonal to the Kth axis. */

		xcoord[0] = next[(i__1 = k - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("next", i__1, "f_xdda__", (ftnlen)903)];
		xcoord[1] = next[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? 
			i__1 : s_rnge("next", i__1, "f_xdda__", (ftnlen)904)];
		if (k == i__) {
		    ub = 0;
		} else {
		    ub = 1;
		}

/*              Loop over both exit faces orthogonal to axis K, */
/*              except when K = I. In the latter case, the exit */
/*              face must be on the opposite side of the grid */
/*              from the vertex. */

		i__1 = ub;
		for (m = 0; m <= i__1; ++m) {

/*                 S is (1-J) on the first pass; J on the second. */

		    s = 1 - m + ((m << 1) - 1) * j;

/*                 Loop over the horizontal coordinates on the grid face */
/*                 containing the vertex. VR is the row index for the */
/*                 vertex; VC is the column index. */

		    i__3 = extent[(i__2 = vcoord[0] - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("extent", i__2, "f_xdda__", (ftnlen)
			    929)];
		    for (vr = 1; vr <= i__3; ++vr) {
			i__4 = extent[(i__2 = vcoord[1] - 1) < 3 && 0 <= i__2 
				? i__2 : s_rnge("extent", i__2, "f_xdda__", (
				ftnlen)931)];
			for (vc = 1; vc <= i__4; ++vc) {
			    i__5 = extent[(i__2 = xcoord[0] - 1) < 3 && 0 <= 
				    i__2 ? i__2 : s_rnge("extent", i__2, 
				    "f_xdda__", (ftnlen)933)];
			    for (xr = 1; xr <= i__5; ++xr) {
				i__6 = extent[(i__2 = xcoord[1] - 1) < 3 && 0 
					<= i__2 ? i__2 : s_rnge("extent", 
					i__2, "f_xdda__", (ftnlen)935)];
				for (xc = 1; xc <= i__6; ++xc) {

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, "Voxel face center to voxe"
					    "l face center test: I = #; J = #"
					    "; K = #; M = #; VR = #; VC = #; "
					    "XR = #; XC = #.", (ftnlen)320, (
					    ftnlen)104);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &j, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &k, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &m, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &vr, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &vc, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &xr, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &xc, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)320);

/*                             Loop over the horizontal coordinates on */
/*                             the face containing the exit. XR is the */
/*                             row index for the vertex; XC is the */
/*                             column index. */

/*                             Set the vertex and exit points. It's */
/*                             inefficient to set coordinates here that */
/*                             could be set in outer loops, but the code */
/*                             is easier to read this way. */

				    vertex[(i__2 = i__ - 1) < 3 && 0 <= i__2 ?
					     i__2 : s_rnge("vertex", i__2, 
					    "f_xdda__", (ftnlen)968)] = (
					    doublereal) (j * extent[(i__7 = 
					    i__ - 1) < 3 && 0 <= i__7 ? i__7 :
					     s_rnge("extent", i__7, "f_xdda__"
					    , (ftnlen)968)]);
				    vertex[(i__2 = vcoord[0] - 1) < 3 && 0 <= 
					    i__2 ? i__2 : s_rnge("vertex", 
					    i__2, "f_xdda__", (ftnlen)969)] = 
					    vr - .5;
				    vertex[(i__2 = vcoord[1] - 1) < 3 && 0 <= 
					    i__2 ? i__2 : s_rnge("vertex", 
					    i__2, "f_xdda__", (ftnlen)970)] = 
					    vc - .5;
				    egress[(i__2 = k - 1) < 3 && 0 <= i__2 ? 
					    i__2 : s_rnge("egress", i__2, 
					    "f_xdda__", (ftnlen)972)] = (
					    doublereal) (s * extent[(i__7 = k 
					    - 1) < 3 && 0 <= i__7 ? i__7 : 
					    s_rnge("extent", i__7, "f_xdda__",
					     (ftnlen)972)]);
				    egress[(i__2 = xcoord[0] - 1) < 3 && 0 <= 
					    i__2 ? i__2 : s_rnge("egress", 
					    i__2, "f_xdda__", (ftnlen)973)] = 
					    xr - .5;
				    egress[(i__2 = xcoord[1] - 1) < 3 && 0 <= 
					    i__2 ? i__2 : s_rnge("egress", 
					    i__2, "f_xdda__", (ftnlen)974)] = 
					    xc - .5;
				    vsub_(egress, vertex, raydir);

/*                             Compute the list of intersected voxels. */

				    xdda_(vertex, raydir, extent, &maxnvx, &
					    nvx, voxlst);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Check the list. */

				    t_chkvox__(vertex, raydir, extent, &nvx, 
					    voxlst);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				}
			    }
			}
		    }
		}
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     Using a small but non-trivial grid, place vertices of rays */
/*     at the corners of each exterior voxel face. Place exit points */
/*     at the corners of each exterior voxel face on grid surfaces */
/*     other than the one containing the vertex. */

    extent[0] = 4;
    extent[1] = 4;
    extent[2] = 6;
    maxnvx = 10000;
    cleard_(&c__3, raydir);
    cleard_(&c__3, vertex);
    cleard_(&c__3, egress);
    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the "horizontal" coordinates on the vertex face */
/*        of the grid: these are orthogonal to the Ith axis. */

	vcoord[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_xdda__", (ftnlen)1036)];
	vcoord[1] = next[(i__1 = vcoord[0] - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("next", i__1, "f_xdda__", (ftnlen)1037)];

/*        Loop over both vertex faces orthogonal to axis I. */

	for (j = 0; j <= 1; ++j) {

/*           Loop over the exit faces. There's one face on */
/*           the opposite side from the vertex face, and four */
/*           faces normal to the horizontal coordinate axes. */

	    for (k = 1; k <= 3; ++k) {

/*              Set the "horizontal" coordinates on the exit face */
/*              of the grid: these are orthogonal to the Kth axis. */

		xcoord[0] = next[(i__1 = k - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("next", i__1, "f_xdda__", (ftnlen)1053)];
		xcoord[1] = next[(i__1 = xcoord[0] - 1) < 3 && 0 <= i__1 ? 
			i__1 : s_rnge("next", i__1, "f_xdda__", (ftnlen)1054)]
			;
		if (k == i__) {
		    ub = 0;
		} else {
		    ub = 1;
		}

/*              Loop over both exit faces orthogonal to axis K, */
/*              except when K = I. In the latter case, the exit */
/*              face must be on the opposite side of the grid */
/*              from the vertex. */

		i__1 = ub;
		for (m = 0; m <= i__1; ++m) {

/*                 S is (1-J) on the first pass; J on the second. */

		    s = 1 - m + ((m << 1) - 1) * j;

/*                 Loop over the horizontal coordinates on the grid face */
/*                 containing the vertex. VR is the row index for the */
/*                 vertex; VC is the column index. */

		    i__4 = extent[(i__3 = vcoord[0] - 1) < 3 && 0 <= i__3 ? 
			    i__3 : s_rnge("extent", i__3, "f_xdda__", (ftnlen)
			    1079)];
		    for (vr = 0; vr <= i__4; ++vr) {
			i__5 = extent[(i__3 = vcoord[1] - 1) < 3 && 0 <= i__3 
				? i__3 : s_rnge("extent", i__3, "f_xdda__", (
				ftnlen)1081)];
			for (vc = 0; vc <= i__5; ++vc) {
			    i__6 = extent[(i__3 = xcoord[0] - 1) < 3 && 0 <= 
				    i__3 ? i__3 : s_rnge("extent", i__3, 
				    "f_xdda__", (ftnlen)1083)];
			    for (xr = 0; xr <= i__6; ++xr) {
				i__2 = extent[(i__3 = xcoord[1] - 1) < 3 && 0 
					<= i__3 ? i__3 : s_rnge("extent", 
					i__3, "f_xdda__", (ftnlen)1085)];
				for (xc = 0; xc <= i__2; ++xc) {

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, "Voxel face corner to voxe"
					    "l face corner test: I = #; J = #"
					    "; K = #; M = #; VR = #; VC = #; "
					    "XR = #; XC = #.", (ftnlen)320, (
					    ftnlen)104);
				    repmi_(title, "#", &i__, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &j, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &k, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &m, title, (ftnlen)320,
					     (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &vr, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &vc, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &xr, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    repmi_(title, "#", &xc, title, (ftnlen)
					    320, (ftnlen)1, (ftnlen)320);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)320);

/*                             Loop over the horizontal coordinates on */
/*                             the face containing the exit. XR is the */
/*                             row index for the vertex; XC is the */
/*                             column index. */

/*                             Set the vertex and exit points. It's */
/*                             inefficient to set coordinates here that */
/*                             could be set in outer loops, but the code */
/*                             is easier to read this way. */

				    vertex[(i__3 = i__ - 1) < 3 && 0 <= i__3 ?
					     i__3 : s_rnge("vertex", i__3, 
					    "f_xdda__", (ftnlen)1118)] = (
					    doublereal) (j * extent[(i__7 = 
					    i__ - 1) < 3 && 0 <= i__7 ? i__7 :
					     s_rnge("extent", i__7, "f_xdda__"
					    , (ftnlen)1118)]);
				    vertex[(i__3 = vcoord[0] - 1) < 3 && 0 <= 
					    i__3 ? i__3 : s_rnge("vertex", 
					    i__3, "f_xdda__", (ftnlen)1119)] =
					     (doublereal) vr;
				    vertex[(i__3 = vcoord[1] - 1) < 3 && 0 <= 
					    i__3 ? i__3 : s_rnge("vertex", 
					    i__3, "f_xdda__", (ftnlen)1120)] =
					     (doublereal) vc;
				    egress[(i__3 = k - 1) < 3 && 0 <= i__3 ? 
					    i__3 : s_rnge("egress", i__3, 
					    "f_xdda__", (ftnlen)1122)] = (
					    doublereal) (s * extent[(i__7 = k 
					    - 1) < 3 && 0 <= i__7 ? i__7 : 
					    s_rnge("extent", i__7, "f_xdda__",
					     (ftnlen)1122)]);
				    egress[(i__3 = xcoord[0] - 1) < 3 && 0 <= 
					    i__3 ? i__3 : s_rnge("egress", 
					    i__3, "f_xdda__", (ftnlen)1123)] =
					     (doublereal) xr;
				    egress[(i__3 = xcoord[1] - 1) < 3 && 0 <= 
					    i__3 ? i__3 : s_rnge("egress", 
					    i__3, "f_xdda__", (ftnlen)1124)] =
					     (doublereal) xc;
				    vsub_(egress, vertex, raydir);
				    if (! vzero_(raydir)) {

/*                                Compute the list of intersected voxels. */

					xdda_(vertex, raydir, extent, &maxnvx,
						 &nvx, voxlst);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Check the list. */

					t_chkvox__(vertex, raydir, extent, &
						nvx, voxlst);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     Using a realistic, large grid, use rays pointed toward the */
/*     origin. This is the classic "spear test." */

    extent[0] = 151;
    extent[1] = 180;
    extent[2] = 119;
    for (i__ = 1; i__ <= 3; ++i__) {
	dxtent[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("dxtent", 
		i__1, "f_xdda__", (ftnlen)1176)] = (doublereal) extent[(i__4 =
		 i__ - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge("extent", i__4, 
		"f_xdda__", (ftnlen)1176)];
	center[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("center", 
		i__1, "f_xdda__", (ftnlen)1177)] = dxtent[(i__4 = i__ - 1) < 
		3 && 0 <= i__4 ? i__4 : s_rnge("dxtent", i__4, "f_xdda__", (
		ftnlen)1177)] / 2;
    }
    d__ = vnorm_(dxtent);
    dlat = 1.;
    dlon = 2.;
    lat = 90.;
    while(lat >= -90.) {
	lon = 0.;
	while(lon < 360.) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Spear test: lon = # deg.; lat = # deg., extents a"
		    "re # # #.", (ftnlen)320, (ftnlen)58);
	    repmd_(title, "#", &lon, &c__14, title, (ftnlen)320, (ftnlen)1, (
		    ftnlen)320);
	    repmd_(title, "#", &lat, &c__14, title, (ftnlen)320, (ftnlen)1, (
		    ftnlen)320);
	    repmi_(title, "#", extent, title, (ftnlen)320, (ftnlen)1, (ftnlen)
		    320);
	    repmi_(title, "#", &extent[1], title, (ftnlen)320, (ftnlen)1, (
		    ftnlen)320);
	    repmi_(title, "#", &extent[2], title, (ftnlen)320, (ftnlen)1, (
		    ftnlen)320);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)320);
	    d__1 = lon * rpd_();
	    d__2 = lat * rpd_();
	    latrec_(&d__, &d__1, &d__2, vertex);
	    vminus_(vertex, raydir);
	    vadd_(vertex, center, vtemp);

/*           Unlike DSKX02, we need the vertex on the grid surface */
/*           for XDDA to work properly. */

	    zzraybox_(vtemp, raydir, voxori, dxtent, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vequ_(xpt, boxvtx);
	    xdda_(boxvtx, raydir, extent, &c__10000, &nvx, voxlst);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_chkvox__(boxvtx, raydir, extent, &nvx, voxlst);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (! (*ok)) {
		s_stop("", (ftnlen)0);
	    }
	    lon += dlon;
	}
	lat -= dlat;
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_xdda__ */

/* ********************************************************************** */

/*     U T I L I T Y   R O U T I N E S */

/* ********************************************************************** */

/*     Routine: T_CHKVOX */

/*     Check a solution produced by XDDA. */

/*     Note that, in the event that the ray intersects a voxel */
/*     edge or corner, the solution is not unique. Therefore */
/*     computing an expected solution is not a general approach */
/*     to validating XDDA. */

/*     A solution must have the following properties: */

/*        1)  The first voxel contains the ray's vertex. */

/*        2)  The last voxel contains the ray's point of exit from the */
/*            grid. */

/*        3)  All voxels have coordinates in within the ranges defined */
/*            by the grid extents. */

/*        4)  The (i+1)st voxel differs from the ith voxel in only one */
/*            coordinate, and the difference has absolute value equal to */
/*            1. */

/*        5)  The ray intersects each voxel. The intersection test */
/*            must be carried out using a tolerance. */


/*     This routine signals an error if any of the above conditions */
/*     are not met. The calling test family should call CHCKXC */
/*     immediately following each call to this routine. */

/* Subroutine */ int t_chkvox__(doublereal *vertex, doublereal *raydir, 
	integer *extent, integer *n, integer *voxlst)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal udir[3];
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static doublereal d__;
    extern /* Subroutine */ int zzraybox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *);
    static integer i__, j;
    extern /* Subroutine */ int chkin_(char *, ftnlen);
    static logical found;
    extern /* Subroutine */ int errdp_(char *, doublereal *, ftnlen), vlcom_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static integer idelta;
    static doublereal negdir[3];
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    static char lngmsg[320];
    static doublereal egress[3], origin[3];
    extern /* Subroutine */ int sigerr_(char *, ftnlen), chkout_(char *, 
	    ftnlen), setmsg_(char *, ftnlen), errint_(char *, integer *, 
	    ftnlen);
    static doublereal dxtent[3];
    extern logical return_(void);
    static doublereal egrvtx[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    static doublereal prvxpt[3], xpt[3];


/*     SPICELIB functions */


/*     Other functions */


/*     Local parameters */


/*     Tolerance used for testing whether a point is inside a */
/*     given voxel: */


/*     Expansion amount used for ray-voxel and ray-grid */
/*     intersection testing: */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */

    if (return_()) {
	return 0;
    }
    chkin_("T_CHKVOX", (ftnlen)8);

/*     Make sure the solution has at least one voxel, so we */
/*     can safely refer the first and last voxel in the list. */

    if (*n < 1) {
	setmsg_("Voxel list size is #; size must be positive.", (ftnlen)44);
	errint_("#", n, (ftnlen)1);
	sigerr_("SPICE(BADLISTSIZE)", (ftnlen)18);
	chkout_("T_CHKVOX", (ftnlen)8);
	return 0;
    }

/*     Make sure the voxel coordinates are in range. */

    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    if (voxlst[j + i__ * 3 - 4] < 1 || voxlst[j + i__ * 3 - 4] > 
		    extent[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
		    "extent", i__2, "t_chkvox__", (ftnlen)1392)]) {
		setmsg_("Coordinate # of voxel # was #; valid range is 1:#.", 
			(ftnlen)50);
		errint_("#", &j, (ftnlen)1);
		errint_("#", &i__, (ftnlen)1);
		errint_("#", &voxlst[j + i__ * 3 - 4], (ftnlen)1);
		errint_("#", &extent[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : 
			s_rnge("extent", i__2, "t_chkvox__", (ftnlen)1400)], (
			ftnlen)1);
		sigerr_("SPICE(BADVOXCOORD)", (ftnlen)18);
		chkout_("T_CHKVOX", (ftnlen)8);
		return 0;
	    }
	}
    }

/*     Check the first voxel. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Do not use a tolerance for this check. Bracketing should */
/*        ensure the voxel is inside the grid (this includes the */
/*        surface). Computation of voxel coordinates for the voxel */
/*        containing the vertex should be portable. */

	if (vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		 i__1, "t_chkvox__", (ftnlen)1422)] < (doublereal) (voxlst[
		i__ - 1] - 1) || vertex[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
		i__2 : s_rnge("vertex", i__2, "t_chkvox__", (ftnlen)1422)] > (
		doublereal) voxlst[i__ - 1]) {
	    setmsg_("Coordinate # of vertex was #; valid range, given by vox"
		    "el 1, is #:#.", (ftnlen)68);
	    errint_("#", &i__, (ftnlen)1);
	    errdp_("#", &vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("vertex", i__1, "t_chkvox__", (ftnlen)1428)], (
		    ftnlen)1);
	    i__1 = voxlst[i__ - 1] - 1;
	    errint_("#", &i__1, (ftnlen)1);
	    errint_("#", &voxlst[i__ - 1], (ftnlen)1);
	    sigerr_("SPICE(BADFIRSTVOX)", (ftnlen)18);
	    chkout_("T_CHKVOX", (ftnlen)8);
	    return 0;
	}
    }

/*     Check the last voxel. In order to do this, we'll need to */
/*     find the ray's point of exit from the grid. */

/*     Start by computing the vertex of a ray pointing in the opposite */
/*     direction as the input ray and passing through the input vertex. */
/*     The vertex of this ray is beyond the exit point. Call this vertex */
/*     EGRVTX. */

/*     We'll expand the grid slightly before computing the exit point, */
/*     since the ray may lie along the grid's surface.. The extents will */
/*     be lengthened by 2*EXPAND, and the "origin" of the grid will be */
/*     shifted by -EXPAND in each coordinate. This will give us an */
/*     expanded grid concentric with the original. Let D be the diameter */
/*     of the grid (the supremum of the distances between any two points */
/*     in the grid). */

    for (i__ = 1; i__ <= 3; ++i__) {
	dxtent[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("dxtent", 
		i__1, "t_chkvox__", (ftnlen)1458)] = (doublereal) extent[(
		i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", 
		i__2, "t_chkvox__", (ftnlen)1458)];
    }
    d__ = vnorm_(dxtent);
    vhat_(raydir, udir);
    d__1 = d__ * 2.;
    vlcom_(&c_b329, vertex, &d__1, udir, egrvtx);
    vminus_(udir, negdir);
    for (i__ = 1; i__ <= 3; ++i__) {
	dxtent[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("dxtent", 
		i__1, "t_chkvox__", (ftnlen)1470)] = (doublereal) extent[(
		i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("extent", 
		i__2, "t_chkvox__", (ftnlen)1470)] + 2e-12;
    }
    for (i__ = 1; i__ <= 3; ++i__) {
	origin[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("origin", 
		i__1, "t_chkvox__", (ftnlen)1474)] = -1e-12;
    }
    zzraybox_(egrvtx, negdir, origin, dxtent, egress, &found);
    if (! found) {
	setmsg_("Uh-oh. Intersection of reversed ray with voxel grid was not"
		" found. Input ray vertex: # # #; direction: # # #; grid exte"
		"nts: # # #.", (ftnlen)130);
	errdp_("#", vertex, (ftnlen)1);
	errdp_("#", &vertex[1], (ftnlen)1);
	errdp_("#", &vertex[2], (ftnlen)1);
	errdp_("#", raydir, (ftnlen)1);
	errdp_("#", &raydir[1], (ftnlen)1);
	errdp_("#", &raydir[2], (ftnlen)1);
	errint_("#", extent, (ftnlen)1);
	errint_("#", &extent[1], (ftnlen)1);
	errint_("#", &extent[2], (ftnlen)1);
	sigerr_("SPICE(BUG)", (ftnlen)10);
	chkout_("T_CHKVOX", (ftnlen)8);
	return 0;
    }

/*     Bracket the egress within the voxel grid. */

    for (i__ = 1; i__ <= 3; ++i__) {
	d__1 = (doublereal) extent[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : 
		s_rnge("extent", i__3, "t_chkvox__", (ftnlen)1503)];
	egress[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("egress", 
		i__1, "t_chkvox__", (ftnlen)1503)] = brcktd_(&egress[(i__2 = 
		i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("egress", i__2, 
		"t_chkvox__", (ftnlen)1503)], &c_b4, &d__1);
    }

/*     Ok, check the last voxel to see whether it contains EGRESS. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Use a tolerance for this check. */

	if (egress[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("egress",
		 i__1, "t_chkvox__", (ftnlen)1513)] < voxlst[i__ + *n * 3 - 4]
		 - 1 - 1e-11 || egress[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
		i__2 : s_rnge("egress", i__2, "t_chkvox__", (ftnlen)1513)] > 
		voxlst[i__ + *n * 3 - 4] + 1e-11) {
	    setmsg_("Coordinate # of egress was #; valid range, given by vox"
		    "el N, is #:#.", (ftnlen)68);
	    errint_("#", &i__, (ftnlen)1);
	    errdp_("#", &egress[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("egress", i__1, "t_chkvox__", (ftnlen)1519)], (
		    ftnlen)1);
	    d__1 = voxlst[i__ + *n * 3 - 4] - 1 - 1e-11;
	    errdp_("#", &d__1, (ftnlen)1);
	    d__1 = voxlst[i__ + *n * 3 - 4] + 1e-11;
	    errdp_("#", &d__1, (ftnlen)1);
	    sigerr_("SPICE(BADLASSTVOX)", (ftnlen)18);
	    chkout_("T_CHKVOX", (ftnlen)8);
	    return 0;
	}
    }

/*     Check the coordinate differences of successive voxels in the */
/*     list. Each voxel should be different from the previous one */
/*     in one coordinate, and by a value of 1. A way of checking */
/*     this is to sum the absolute values of the differences of the */
/*     coordinates. */

    i__1 = *n;
    for (i__ = 2; i__ <= i__1; ++i__) {

/*        Note DELTA has INTEGER type. */

	idelta = 0;
	for (j = 1; j <= 3; ++j) {
	    idelta += (i__2 = voxlst[j + i__ * 3 - 4] - voxlst[j + (i__ - 1) *
		     3 - 4], abs(i__2));
	}
	if (idelta != 1) {
	    setmsg_("Voxel at index # is # # #; voxel at index # is # # #. A"
		    "bsolute values of  coordinate differences should sum to "
		    "1.", (ftnlen)113);
	    errint_("#", &i__, (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 3], (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 2], (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 1], (ftnlen)1);
	    i__2 = i__ - 1;
	    errint_("#", &i__2, (ftnlen)1);
	    errint_("#", &voxlst[(i__ - 1) * 3 - 3], (ftnlen)1);
	    errint_("#", &voxlst[(i__ - 1) * 3 - 2], (ftnlen)1);
	    errint_("#", &voxlst[(i__ - 1) * 3 - 1], (ftnlen)1);
	    sigerr_("SPICE(BADVOXELDIFF)", (ftnlen)19);
	    chkout_("T_CHKVOX", (ftnlen)8);
	    return 0;
	}
    }

/*     Make sure the ray hits each voxel in the list. We'll expand the */
/*     voxels slightly before testing them. The extents will be */
/*     lengthened by 2*EXPAND, and the "origin" of each voxel will be */
/*     shifted by -EXPAND in each coordinate. This will give us an */
/*     expanded voxel concentric with the original. */

    cleard_(&c__3, prvxpt);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
	    dxtent[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("dxtent", 
		    i__2, "t_chkvox__", (ftnlen)1580)] = (doublereal) extent[(
		    i__3 = j - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", 
		    i__3, "t_chkvox__", (ftnlen)1580)] + 2e-12;
	}
	for (j = 1; j <= 3; ++j) {
	    origin[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("origin", 
		    i__2, "t_chkvox__", (ftnlen)1584)] = voxlst[j + i__ * 3 - 
		    4] - 1. - 1e-12;
	}
	zzraybox_(vertex, raydir, origin, dxtent, xpt, &found);
	if (! found) {
	    if (i__ > 1) {
		s_copy(lngmsg, "Intersection of ray with voxel # (list size "
			"= #) was not  found. Input ray vertex: # # #; direct"
			"ion: # # #; grid extents: # # #. Voxel coordinates: "
			"# # #. Coordinates of ray intercept with previous vo"
			"xel are # # #.", (ftnlen)320, (ftnlen)214);
	    } else {
		s_copy(lngmsg, "Intersection of ray with voxel # (list size "
			"= #) was not  found. Input ray vertex: # # #; direct"
			"ion: # # #; grid extents: # # #. Voxel coordinates: "
			"# # #.", (ftnlen)320, (ftnlen)154);
	    }
	    setmsg_(lngmsg, (ftnlen)320);
	    errint_("#", &i__, (ftnlen)1);
	    errint_("#", n, (ftnlen)1);
	    errdp_("#", vertex, (ftnlen)1);
	    errdp_("#", &vertex[1], (ftnlen)1);
	    errdp_("#", &vertex[2], (ftnlen)1);
	    errdp_("#", raydir, (ftnlen)1);
	    errdp_("#", &raydir[1], (ftnlen)1);
	    errdp_("#", &raydir[2], (ftnlen)1);
	    errint_("#", extent, (ftnlen)1);
	    errint_("#", &extent[1], (ftnlen)1);
	    errint_("#", &extent[2], (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 3], (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 2], (ftnlen)1);
	    errint_("#", &voxlst[i__ * 3 - 1], (ftnlen)1);
	    if (i__ > 1) {
		errdp_("#", prvxpt, (ftnlen)1);
		errdp_("#", &prvxpt[1], (ftnlen)1);
		errdp_("#", &prvxpt[2], (ftnlen)1);
	    }
	    sigerr_("SPICE(VOXELNOTHIT)", (ftnlen)18);
	    chkout_("T_CHKVOX", (ftnlen)8);
	    return 0;
	}
	vequ_(xpt, prvxpt);
    }
    chkout_("T_CHKVOX", (ftnlen)8);
    return 0;
} /* t_chkvox__ */

