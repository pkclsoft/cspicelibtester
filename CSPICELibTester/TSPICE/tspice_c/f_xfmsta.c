/* f_xfmsta.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__6 = 6;
static doublereal c_b40 = 1e-13;
static doublereal c_b139 = 1e-14;
static doublereal c_b156 = 1e-11;
static doublereal c_b170 = 10.;

/* $Procedure      F_XFMSTA ( Family of tests for XFMSTA ) */
/* Subroutine */ int f_xfmsta__(logical *ok)
{
    /* Initialized data */

    static char cosys[30*6] = "RECTANGULAR                   " "CYLINDRICAL "
	    "                  " "LATITUDINAL                   " "SPHERICAL "
	    "                    " "GEODETIC                      " "PLANETOG"
	    "RAPHIC                ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    doublereal temp[6], temp2[6];
    integer i__, j, k, l;
    doublereal r__[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    , t_success__(logical *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal badsta[6];
    extern /* Subroutine */ int delfil_(char *, ftnlen), kclear_(void), 
	    chckxc_(logical *, char *, logical *, ftnlen), t_pck08__(char *, 
	    logical *, logical *, ftnlen);
    doublereal savrad[3];
    extern /* Subroutine */ int bodvrd_(char *, char *, integer *, doublereal 
	    *, doublereal *, ftnlen, ftnlen);
    integer sedvel, sdsign;
    doublereal istate[6];
    integer sedpos;
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen);
    doublereal ostate[6];
    extern /* Subroutine */ int xfmsta_(doublereal *, char *, char *, char *, 
	    doublereal *, ftnlen, ftnlen, ftnlen), furnsh_(char *, ftnlen);
    extern integer t_randsign__(integer *);
    doublereal lowerp, upperp, lowerv, upperv, dim;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    doublereal tol;

/* $ Abstract */

/*     This routine tests the routine XFMSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */


/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     S.C. Krening   (JPL) */
/*     B.V. Semenov   (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 09-FEB-2017 (BVS) */

/*        Added cases to check the exception for geodetic and */
/*        planetographic conversions for bodies with unequal equatorial */
/*        radii. */

/*        BUG FIX: replaced '' with ' ' in a few places. */

/* -    TSPICE Version 1.0.0, 22-APR-2014 (SCK)(BVS) */

/* -& */

/*     SPICELIB Functions */


/*     Other Functions */


/*     Local parameters */


/*     Local Variables */


/*     Seed values for input state vector, input coordinate */
/*     system, and output coordinate system. */


/*     Coordinate Systems: */


/*     Begin every test family with an open call. */

    topen_("F_XFMSTA", (ftnlen)8);
    tcase_("Setup: create and load the PCK.", (ftnlen)31);

/*     Create a PCK, load using FURNSH. */

    t_pck08__("xfmsta.pck", &c_false, &c_true, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("xfmsta.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Save the true radii value to a variable. */

    bodvrd_("EARTH", "RADII", &c__3, &dim, savrad, (ftnlen)5, (ftnlen)5);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure incorrect coordinate frames are not accepted. */

    tcase_("Make sure an incorrect coordinate frame input is not accepted. ", 
	    (ftnlen)63);
    istate[0] = 1.;
    istate[1] = 1.;
    istate[2] = 1.;
    istate[3] = 1.;
    istate[4] = 1.;
    istate[5] = 1.;
    xfmsta_(istate, "LATITUD", "RECTANGULAR", " ", ostate, (ftnlen)7, (ftnlen)
	    11, (ftnlen)1);
    chckxc_(&c_true, "SPICE(COORDSYSNOTREC)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure incorrect coordinate frames are not accepted. */

    tcase_("Make sure an incorrect coordinate frame output is not accepted. ",
	     (ftnlen)64);
    xfmsta_(istate, "CYLINDRICAL", "RECTANLAR", " ", ostate, (ftnlen)11, (
	    ftnlen)9, (ftnlen)1);
    chckxc_(&c_true, "SPICE(COORDSYSNOTREC)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure incorrect coordinate frames are not accepted. */

    tcase_("Make sure an incorrect coordinate frame input and output are not"
	    " accepted. ", (ftnlen)75);
    xfmsta_(istate, "CYLINDRCL", "PLANETOGRPHIC", " ", ostate, (ftnlen)9, (
	    ftnlen)13, (ftnlen)1);
    chckxc_(&c_true, "SPICE(COORDSYSNOTREC)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure identical input/output coordinate systems return the */
/*     input. */

    tcase_("Make sure identical input and output coordinate systems return t"
	    "he input. ", (ftnlen)74);
    xfmsta_(istate, "CYLINDRICAL", "CYLINDRICAL", " ", ostate, (ftnlen)11, (
	    ftnlen)11, (ftnlen)1);
    chckad_("OSTATE", ostate, "=", istate, &c__6, &c_b40, ok, (ftnlen)6, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

/*     For planetographic or geodetic, verify an equatorial radius of */
/*     zero is not accepted. */

    tcase_("Verify an equatorial radius of zero is not accepted for geodetic"
	    "/planetographic.", (ftnlen)80);
    r__[0] = 0.;
    r__[1] = 6378.14;
    r__[2] = 6356.75;
    pdpool_("BODY399_RADII", &c__3, r__, (ftnlen)13);
    xfmsta_(istate, "GEODETIC", "RECTANGULAR", "EARTH", ostate, (ftnlen)8, (
	    ftnlen)11, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);

/*          Every time the kernel pool is altered, reset the value */
/*          after the test. */

    pdpool_("BODY399_RADII", &c__3, savrad, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

/*     For planetographic or geodetic, verify (a-b)/a overflow case. */

    tcase_("Verify numeric overflow case for flattening coeff for the geodet"
	    "ic case. ", (ftnlen)73);
    r__[0] = 1e-307;
    r__[1] = 6378.14;
    r__[2] = 1e307;
    pdpool_("BODY399_RADII", &c__3, r__, (ftnlen)13);
    xfmsta_(istate, "GEODETIC", "RECTANGULAR", "EARTH", ostate, (ftnlen)8, (
	    ftnlen)11, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);

/*          Every time the kernel pool is altered, reset the value */
/*          after the test. */

    pdpool_("BODY399_RADII", &c__3, savrad, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

/*     For planetographic or geodetic, verify the radii cannot be less */
/*     than zero. */

    tcase_("Verify error signal on negative radii.", (ftnlen)38);
    r__[0] = -1.;
    r__[1] = 6378.14;
    r__[2] = -1.;
    pdpool_("BODY399_RADII", &c__3, r__, (ftnlen)13);
    xfmsta_(istate, "GEODETIC", "RECTANGULAR", "EARTH", ostate, (ftnlen)8, (
	    ftnlen)11, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);

/*          Every time the kernel pool is altered, reset the value */
/*          after the test. */

    pdpool_("BODY399_RADII", &c__3, savrad, (ftnlen)13);

/* ---- Case ----------------------------------------------------------- */

/*     For planetographic or geodetic, verify the exception for bodies */
/*     with unequal equatorial radii. */

    tcase_("Verify the exception for geodetic and planetographic conversions"
	    " for bodies with unequal equatorial radii.", (ftnlen)106);
    xfmsta_(istate, "RECTANGULAR", "GEODETIC", "PHOBOS", temp, (ftnlen)11, (
	    ftnlen)8, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    xfmsta_(istate, "RECTANGULAR", "PLANETOGRAPHIC", "PHOBOS", temp, (ftnlen)
	    11, (ftnlen)14, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    xfmsta_(istate, "GEODETIC", "RECTANGULAR", "PHOBOS", temp, (ftnlen)8, (
	    ftnlen)11, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    xfmsta_(istate, "PLANETOGRAPHIC", "RECTANGULAR", "PHOBOS", temp, (ftnlen)
	    14, (ftnlen)11, (ftnlen)6);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure incorrect body names are not accepted (for geodetic) */
    tcase_("Make sure incorrect body names are not accepted for the geodetic"
	    " case. ", (ftnlen)71);
    xfmsta_(istate, "GEODETIC", "RECTANGULAR", "ERTH", ostate, (ftnlen)8, (
	    ftnlen)11, (ftnlen)4);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure incorrect body names are not accepted (planetographic) */

    tcase_("Make sure incorrect body names are not accepted for the planetog"
	    "raphic case. ", (ftnlen)77);
    xfmsta_(istate, "PLANETOGRAPHIC", "RECTANGULAR", "MRS", ostate, (ftnlen)
	    14, (ftnlen)11, (ftnlen)3);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

/*     Make sure NAIF ID strings are accepted */

    tcase_("Make sure NAIF ID strings are accepted. ", (ftnlen)40);
    xfmsta_(istate, "geodetic", "rectangular", "399", ostate, (ftnlen)8, (
	    ftnlen)11, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

/*     Verify the conversion to non-rectangular does not work for */
/*     (X=0, Y=0, Z, Vx, Vy, Vz) */

    tcase_("Verify the z-axis when converting to anything except rectangular"
	    " is not allowed if only the position is on the z-axis.", (ftnlen)
	    118);
    istate[0] = 0.;
    istate[1] = 0.;
    istate[2] = .5;
    istate[3] = .2;
    istate[4] = .1;
    istate[5] = -.2;
    for (i__ = 2; i__ <= 6; ++i__) {
	xfmsta_(istate, cosys, cosys + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? 
		i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (ftnlen)401)) * 30,
		 "EARTH", ostate, (ftnlen)30, (ftnlen)30, (ftnlen)5);
	chckxc_(&c_true, "SPICE(INVALIDSTATE)", ok, (ftnlen)19);
    }

/* ---- Case ------------------------------------------------------------- */

/*     Verify the conversion to non-rectangular works even if */
/*     the Jacobian is not defined if both the position and velocity */
/*     are along the z-axis. */

    tcase_("Verify the z-axis when converting to anything except rectangular "
	    , (ftnlen)65);
    istate[0] = 0.;
    istate[1] = 0.;
    istate[2] = .5;
    istate[3] = 0.;
    istate[4] = 0.;
    istate[5] = .5;
    temp[3] = istate[5];
    temp[4] = 0.;
    temp[5] = 0.;
    for (i__ = 2; i__ <= 6; ++i__) {
	xfmsta_(istate, cosys, cosys + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? 
		i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (ftnlen)428)) * 30,
		 "EARTH", ostate, (ftnlen)30, (ftnlen)30, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (i__ == 2 || i__ == 5 || i__ == 6) {

/*                 If the output coordinate system is cylindrical, */
/*                 geodetic, or planetographic, make sure the velocity */
/*                 output is (0, 0, dz_dt). */

	    chckad_("OSTATE", &ostate[3], "~", &istate[3], &c__3, &c_b40, ok, 
		    (ftnlen)6, (ftnlen)1);

/*                 If the output coordinate system is spherical or */
/*                 latitudinal, make sure the velocity output is */
/*                 (dz_dt, 0, 0). */

	} else {
	    chckad_("OSTATE", &ostate[3], "~", &temp[3], &c__3, &c_b40, ok, (
		    ftnlen)6, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

/*     Verify a zero vector case. */

    tcase_("Verify all combinations of coordinate systems ", (ftnlen)46);
    istate[0] = 0.;
    istate[1] = 0.;
    istate[2] = 0.;
    istate[3] = 0.;
    istate[4] = 0.;
    istate[5] = 0.;
    xfmsta_(istate, "CYLINDRICAL", "RECTANGULAR", " ", ostate, (ftnlen)11, (
	    ftnlen)11, (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("OSTATE", ostate, "~", istate, &c__6, &c_b139, ok, (ftnlen)6, (
	    ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

/*     Verify all possible combinations of TO and FROM */

    tcase_("Verify all combinations of coordinate systems ", (ftnlen)46);
    istate[0] = 1.;
    istate[1] = .5;
    istate[2] = .5;
    istate[3] = .2;
    istate[4] = .1;
    istate[5] = -.2;
    for (i__ = 1; i__ <= 6; ++i__) {
	for (j = 1; j <= 6; ++j) {
	    if (i__ != j) {
		xfmsta_(istate, cosys + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? 
			i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (ftnlen)
			493)) * 30, cosys + ((i__2 = j - 1) < 6 && 0 <= i__2 ?
			 i__2 : s_rnge("cosys", i__2, "f_xfmsta__", (ftnlen)
			493)) * 30, "EARTH", temp, (ftnlen)30, (ftnlen)30, (
			ftnlen)5);
		xfmsta_(temp, cosys + ((i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 
			: s_rnge("cosys", i__1, "f_xfmsta__", (ftnlen)495)) * 
			30, cosys + ((i__2 = i__ - 1) < 6 && 0 <= i__2 ? i__2 
			: s_rnge("cosys", i__2, "f_xfmsta__", (ftnlen)495)) * 
			30, "EARTH", ostate, (ftnlen)30, (ftnlen)30, (ftnlen)
			5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("OSTATE", ostate, "~", istate, &c__6, &c_b156, ok, (
			ftnlen)6, (ftnlen)1);
	    }
	}
    }

/* ---- Case ------------------------------------------------------------- */

/*     Verify the overflow case of a non-rectangular input to */
/*     any coordinate system (it will be translated to rectangular */
/*     within the code). (Test 1) */

    tcase_("Bad velocity input -> rec ", (ftnlen)26);
    badsta[0] = 300.;
    badsta[1] = .5;
    badsta[2] = .5;
    badsta[3] = 1e307;
    badsta[4] = 1e307;
    badsta[5] = 1e307;
    xfmsta_(badsta, "LATITUDINAL", "RECTANGULAR", " ", ostate, (ftnlen)11, (
	    ftnlen)11, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

/*     Verify the overflow case of rectangular -> output. */
/*     There are two different places in the code that can signal */
/*     the near-singular error, which is why two test cases with */
/*     different inputs must be run.  (Test 2) */

    tcase_("Bad velocity rec -> output ", (ftnlen)27);
    badsta[0] = 300.;
    badsta[1] = .5;
    badsta[2] = .5;
    badsta[3] = 1e307;
    badsta[4] = 1e307;
    badsta[5] = 1e307;
    xfmsta_(badsta, "RECTANGULAR", "LATITUDINAL", " ", ostate, (ftnlen)11, (
	    ftnlen)11, (ftnlen)1);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

/*     Verify a series of random cases for real possible input. */
/*     Distances on the order of 10^11 can be reached for missions */
/*     like Voyager.  Calculations including Pluto are around that */
/*     order. */

/*     The tolerance was chosen because the boundary values are on the */
/*     order of 10^11.  The output values will match the expected on */
/*     by x*10^-5, which can be greater than 1*10^-5.  The tolerance */
/*     allows for all numbers on the order of 10^-5 by being 10^-4. */

/*     Rectangular is used as the 'to' and 'from' states since there */
/*     are no angle limits for the input state. */

    tcase_("Verify a series of random input states from rectangular to every"
	    " other coordinate system and back to rectangular.", (ftnlen)113);
    sedpos = -1;
    sedvel = -1;
    sdsign = -1;
    lowerp = -6.;
    upperp = 11.;
    lowerv = -7.;
    upperv = 7.;
    tol = 1e-4;
    for (k = 1; k <= 1000; ++k) {

/*              T_RANDSIGN is used to determine a random sign for */
/*              the components of ISTATE. */

	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[0] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[1] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[2] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[3] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[4] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[5] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	for (j = 2; j <= 6; ++j) {
	    xfmsta_(istate, "RECTANGULAR", cosys + ((i__1 = j - 1) < 6 && 0 <=
		     i__1 ? i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (
		    ftnlen)602)) * 30, "EARTH", temp, (ftnlen)11, (ftnlen)30, 
		    (ftnlen)5);
	    xfmsta_(temp, cosys + ((i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("cosys", i__1, "f_xfmsta__", (ftnlen)604)) * 30, 
		    "RECTANGULAR", "EARTH", ostate, (ftnlen)30, (ftnlen)11, (
		    ftnlen)5);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_("OSTATE", ostate, "~", istate, &c__6, &tol, ok, (ftnlen)6,
		     (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

/*     Verify to and from every coordinate system works (except */
/*     rectangular, which was already tested).  The test */
/*     starts out in rectangular to ensure inputs of the right order */
/*     are chosen.  The state is converted to a non-rectangular */
/*     coordinate system (J), then converted from that coordinate system */
/*     to another coordinate system (K) and back to (J). */

    tcase_("Verify a series of random input states from non-rectangular coor"
	    "dinate systems to every other non-rectangular coordinate system.",
	     (ftnlen)128);
    sedpos = -1;
    sedvel = -1;
    lowerp = -3.;
    upperp = 9.;
    lowerv = -2.;
    upperv = 2.;
    tol = .1;
    for (k = 1; k <= 1000; ++k) {

/*              T_RANDSIGN is used to determine a random sign for */
/*              the components of ISTATE. */

	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[0] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[1] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerp, &upperp, &sedpos);
	istate[2] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[3] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[4] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	d__1 = t_randd__(&lowerv, &upperv, &sedvel);
	istate[5] = t_randsign__(&sdsign) * pow_dd(&c_b170, &d__1);
	for (j = 2; j <= 6; ++j) {

/*                 Convert to a non-rectangular coordinate system. */

	    xfmsta_(istate, "RECTANGULAR", cosys + ((i__1 = j - 1) < 6 && 0 <=
		     i__1 ? i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (
		    ftnlen)662)) * 30, "EARTH", temp, (ftnlen)11, (ftnlen)30, 
		    (ftnlen)5);
	    for (l = 2; l <= 6; ++l) {
		if (j != l) {

/*                       Convert from the 'J' coordinate system to 'K' */
/*                       and then back to 'J'.  Check that the resulting */
/*                       state is within the tolerance of the initial */
/*                       state. */

		    xfmsta_(temp, cosys + ((i__1 = j - 1) < 6 && 0 <= i__1 ? 
			    i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (
			    ftnlen)674)) * 30, cosys + ((i__2 = l - 1) < 6 && 
			    0 <= i__2 ? i__2 : s_rnge("cosys", i__2, "f_xfms"
			    "ta__", (ftnlen)674)) * 30, "EARTH", temp2, (
			    ftnlen)30, (ftnlen)30, (ftnlen)5);
		    xfmsta_(temp2, cosys + ((i__1 = l - 1) < 6 && 0 <= i__1 ? 
			    i__1 : s_rnge("cosys", i__1, "f_xfmsta__", (
			    ftnlen)676)) * 30, cosys + ((i__2 = j - 1) < 6 && 
			    0 <= i__2 ? i__2 : s_rnge("cosys", i__2, "f_xfms"
			    "ta__", (ftnlen)676)) * 30, "EARTH", ostate, (
			    ftnlen)30, (ftnlen)30, (ftnlen)5);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chckad_("OSTATE", ostate, "~", temp, &c__6, &tol, ok, (
			    ftnlen)6, (ftnlen)1);
		}
	    }
	}
    }


/* ---- Case n ----------------------------------------------------------- */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("xfmsta.pck", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_xfmsta__ */

