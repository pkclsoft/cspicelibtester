/*

-Procedure f_xfmsta_c ( Test xfmsta_c )

 
-Abstract
 
   Perform tests on the CSPICE wrapper xfmsta_c.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/

   #include <math.h>
   #include <stdio.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "tutils_c.h"
   

   void f_xfmsta_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the wrappers for xfmsta_c.
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   S.C. Krening    (JPL)
 
-Version
 
   -tspice_c Version 1.1.0, 10-FEB-2017 (NJB)
 
       Removed unneeded declarations.       

   -tspice_c Version 1.0.0 23-JAN-2012 (SCK) 

-Index_Entries

   test xfmsta_c

-&
*/

{ /* Begin f_xfmsta_c */

   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Constants
   */
   #define PCK             "xfmsta.pck"
   
   
   /*
   Local constants
   */
   
   /*
   Local variables
   */
   SpiceDouble             istate [6] = {1, 1, 1, 1, 1, 1};
   SpiceDouble             ostate [6];
   SpiceDouble             temp   [6];

   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_xfmsta_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   
   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   
   /*
   Create a PCK, load using FURNSH.
   */
   t_pck08_c ( PCK, SPICEFALSE, SPICETRUE );
   chckxc_c ( SPICEFALSE, " ", ok );

   furnsh_c( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   
   tcase_c ( "Bad input string pointers" );
   
   xfmsta_c ( istate, NULLCPTR, "rectangular", " ", ostate );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   xfmsta_c ( istate, "rectangular", NULLCPTR,  " ", ostate );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );
   
   xfmsta_c ( istate, "cylindrical", "rectangular", NULLCPTR, ostate );
   chckxc_c ( SPICETRUE, "SPICE(NULLPOINTER)", ok );

   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Empty body input string" );
   
   xfmsta_c ( istate, "cylindrical", "rectangular", "", ostate );
   chckxc_c ( SPICETRUE, "SPICE(EMPTYSTRING)", ok );


   /* 
   *******************************************************************
   *
   *  Normal cases
   * 
   *******************************************************************
   */

   /* 
   ---- Case ---------------------------------------------------------
   */
   /*
   Convert a state from rectangular to planetographic and back to 
   rectangular again.  Check to make sure the result 'ostate' is close
   to the input 'istate'.
   */
   tcase_c ( "Check a normal case with the C interface." );
   
   xfmsta_c ( istate, "rectangular", "planetographic", "earth", temp );
   chckxc_c( SPICEFALSE, " ", ok );
   
   xfmsta_c ( temp,   "planetographic", "rectangular", "earth", ostate );
   chckxc_c( SPICEFALSE, " ", ok );
   
   chckad_c ( "ostate", ostate, "~", istate, 6, 1.e-11, ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Clean up:  delete kernels." );

   kclear_c ();
   
   TRASH ( PCK );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 

} /* End f_xfmsta_c */

