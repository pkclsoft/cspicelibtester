/*

-Procedure f_zzadapt_c ( Test CSPICE adapters )

 
-Abstract
 
   Perform tests on the CSPICE adapter functions.
    
-Disclaimer

   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.

-Required_Reading
 
   None. 
 
-Keywords
 
   TESTING 
 
*/
   #include <math.h>
   #include <stdio.h>
   #include <string.h>
   #include <signal.h>
   #include "SpiceUsr.h"
   #include "SpiceZfc.h"
   #include "SpiceZmc.h"
   #include "SpiceZad.h"
   #include "tutils_c.h"

   /*
   Constants
   */
   #define MAXWIN          100000
   #define LNSIZE          81
   #define MSGLEN          241
   #define VTIGHT        ( 1.e-14 )

   /*
   File scope static variables
   */ 
   SPICEDOUBLE_CELL      ( sv_window, MAXWIN );

   static SpiceBoolean     gfbail_sp_called;
   static SpiceBoolean     gfrefn_sp_called;
   static SpiceBoolean     gfrepf_sp_called;
   static SpiceBoolean     gfrepi_sp_called;
   static SpiceBoolean     gfrepu_sp_called;
   static SpiceBoolean     gfstep_sp_called;

   static SpiceBoolean     sv_gfbail_retval;
   static SpiceBoolean     svs1;
   static SpiceBoolean     svs2;

   static SpiceChar        sv_begmss [ MSGLEN ];
   static SpiceChar        sv_endmss [ MSGLEN ];

   static SpiceDouble      sv_gfstep_retval;
   static SpiceDouble      svivbeg;
   static SpiceDouble      svivend;
   static SpiceDouble      svt1;
   static SpiceDouble      svt2;
   static SpiceDouble      svtime;
   





   void f_zzadapt_c ( SpiceBoolean * ok )

/*

-Brief_I/O

   VARIABLE  I/O  DESCRIPTION 
   --------  ---  -------------------------------------------------- 
   ok         O   SPICETRUE if the test passes, SPICEFALSE otherwise.. 
 
-Detailed_Input
 
   None.
 
-Detailed_Output
 
   ok         if all tests pass.  Otherwise ok is given the value
              SPICEFALSE and a diagnostic message is sent to the test
              logger.
 
-Parameters
 
   None. 
 
-Exceptions
 
   Error free. 
 
-Files
 
   None. 
 
-Particulars
 
   This routine tests the CSPICE adapters for passed-in function
   arguments. Each CSPICE-style function that is called by
   an adapter is spoofed by a function defined in this file;
   the spoofing functions store their inputs in static memory,
   enabling testing of these inputs.

   The set of adapters tested here is

      zzadbail_c
      zzadrefn_c
      zzadrepf_c
      zzadrepi_c
      zzadrepu_c
      zzadstep_c

   The utility functions

      zzadget_c
      zzadsave_c
      
   are also exercised by this test family.

   This routine also tests those aspects of the CSPICE mid-level 
   wrappers that involve use of the adapter functions. The set
   of such wrappter routines tested here is:

      gfevnt_c
      gffove_c
      gfocce_c
                   
-Examples
 
   None.
    
-Restrictions
 
   None. 
 
-Literature_References
 
   None. 
 
-Author_and_Institution
 
   N.J. Bachman    (JPL)
   E.D. Wright     (JPL)
 
-Version

   -tspice_c Version 1.1.0 23-MAY-2010 (EDW) 
  
      Update for gfuds_c.c adapter set.

   -tspice_c Version 1.0.0 29-MAR-2009 (NJB) (EDW)

-Index_Entries

   test CSPICE adapters

-&
*/

{ /* Begin f_zzadapt_c */


   /*
   Prototypes 
   */

   int natik_  ( char    *nameik, 
                 char    *spk, 
                 char    *pck, 
                 logical *loadik, 
                 logical *keepik, 
                 ftnlen  nameik_len, 
                 ftnlen  spk_len, 
                 ftnlen  pck_len    );

   int natpck_ ( char    *file, 
                 logical *loadpc, 
                 logical *keeppc, 
                 ftnlen  file_len );

   int natspk_ ( char    *file, 
                 logical *load, 
                 integer *handle,
                 ftnlen  file_len );

   /*
   Adapter spoof functions:  
   */
   SpiceBoolean      gfbail_sp ( void );

   void              gfrefn_sp ( SpiceDouble        t1,
                                 SpiceDouble        t2,
                                 SpiceBoolean       s1,
                                 SpiceBoolean       s2,
                                 SpiceDouble      * t  );

   void              gfrepf_sp ( void );


   void              gfrepi_sp ( SpiceCell        * window,
                                 ConstSpiceChar   * begmss,
                                 ConstSpiceChar   * endmss  );


   void              gfrepu_sp ( SpiceDouble         ivbeg,
                                 SpiceDouble         ivend,
                                 SpiceDouble         time   );

   void              gfstep_sp ( SpiceDouble         time,
                                 SpiceDouble       * step  ); 


   /*
   Local macros
   */
   #define TRASH(file)     if ( removeFile(file) !=0 )                        \
                              {                                           \
                              setmsg_c ( "Unable to delete file #." );    \
                              errch_c  ( "#", file );                     \
                              sigerr_c ( "TSPICE(DELETEFAILED)"  );       \
                              }                                           \
                           chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Prototypes
   */

   /*
   Local constants 
   */
   #define  CNVTOL         1.e-6
   #define  SPK            "zzadapt_c.bsp"   
   #define  TIMFMT         "YYYY MON DD HR:MN:SC.###### ::TDB (TDB)"
   #define  TIMLEN         41
   #define  IK             "nat.ti"   
   #define  PCK            "nat.tpc"   
   #define  NATSPK         "nat.bsp"  
   #define  PARAMLN        81
   #define  GFEVNT_MAXPAR  20
   

   /*
   Local variables
   */
   SPICEDOUBLE_CELL      ( cnfine, MAXWIN );
   SPICEDOUBLE_CELL      ( result, MAXWIN );
   SPICEDOUBLE_CELL      ( window, MAXWIN );
 
   logical                 logRetval;
   logical                 ls1;
   logical                 ls2;

   static logical          keepik  = SPICETRUE;
   static logical          keeppc  = SPICETRUE;
   static logical          loadik  = SPICETRUE;
   static logical          loadpc  = SPICETRUE;
   static logical          loadspk = SPICETRUE;

   SpiceBoolean            bail;
   SpiceBoolean            qlpars  [GFEVNT_MAXPAR];
   SpiceBoolean            rpt;

   SpiceChar             * gquant;
   SpiceChar             * relate;
   SpiceChar               qcpars  [ GFEVNT_MAXPAR ][ PARAMLN ];
   SpiceChar               qpnams  [ GFEVNT_MAXPAR ][ PARAMLN ];
   SpiceChar               xbegmss [ MSGLEN ];
   SpiceChar               xendmss [ MSGLEN ];
   SpiceChar             * win0;
   SpiceChar             * win1;

   SpiceDouble             adjust;
   SpiceDouble             et0;
   SpiceDouble             et1;
   SpiceDouble             ivbeg;
   SpiceDouble             ivend;
   SpiceDouble             qdpars [GFEVNT_MAXPAR];
   SpiceDouble             step;
   SpiceDouble             raydir [3] = { 0.0, 0.0, 0.0 };
   SpiceDouble             refval;
   SpiceDouble             t1;
   SpiceDouble             t2;
   SpiceDouble             t;
   SpiceDouble             time;
   SpiceDouble             xt;

   SpiceInt                handle;
   SpiceInt                i;
   SpiceInt                NatSPKHan;
   SpiceInt                n;
   SpiceInt                qipars [ GFEVNT_MAXPAR ];
   SpiceInt                qnpars;
   SpiceInt                winsiz;

   void                  * fPtr;


   /*
   Begin every test family with an open call.
   */
   topen_c ( "f_zzadapt_c" );
   
   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Set up: create and load kernels." );
   

   /*
   Make sure the kernel pool doesn't contain any unexpected 
   definitions.
   */
   clpool_c();
   
   /*
   Load a leapseconds kernel.  
   
   Note that the LSK is deleted after loading, so we don't have to clean
   it up later.
   */
   tstlsk_c();
   chckxc_c ( SPICEFALSE, " ", ok );
   
   
   /*
   Create and load a PCK file. Delete the file afterward.
   */
   tstpck_c ( "test.pck", SPICETRUE, SPICEFALSE );
   
   
   /*
   Load an SPK file as well.
   */
   tstspk_c ( SPK, SPICETRUE, &handle );
   chckxc_c ( SPICEFALSE, " ", ok );


 
   


   /* 
   *******************************************************************
   *
   *  Adapter test cases
   * 
   *******************************************************************
   */
   



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadbail_c" );


   /*
   Indicate that the gfbail_sp spoof function hasn't yet
   been called.
   */
   gfbail_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDBAIL, (void *)gfbail_sp );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set the bail-out indicator return value. 
   */
   sv_gfbail_retval = SPICETRUE;


   /*
   Call the adapter. 
   */
   logRetval = zzadbail_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfbail_sp_called", gfbail_sp_called, SPICETRUE, ok );

   /*
   Check the returned value from the adapter. 
   */
   chcksl_c ( "zzadbail return value", 
              logRetval, SPICETRUE, ok );

   /*
   Make another call, this time with the return value SPICEFALSE. 
   */
 
   /*
   Set the bail-out indicator return value. 
   */
   sv_gfbail_retval = SPICEFALSE;

   /*
   Indicate that the gfbail_sp spoof function hasn't yet
   been called.
   */
   gfbail_sp_called = SPICEFALSE;

   /*
   Call the adapter. 
   */
   logRetval = zzadbail_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfbail_sp_called", gfbail_sp_called, SPICETRUE, ok );

   /*
   Check the returned value from the adapter. 
   */
   chcksl_c ( "zzadbail return value", 
              logRetval, SPICEFALSE, ok );





   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadrefn_c" );


   /*
   Indicate that the gfrefn_sp spoof function hasn't yet
   been called.
   */
   gfrefn_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDREFN, (void *)gfrefn_sp );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set the inputs and expected output.
   */
   t1  =  1.0;
   t2  =  3.0;
   ls1 =  (logical)SPICETRUE;
   ls2 =  (logical)SPICEFALSE;
   xt  =  2.0;

   /*
   Call the adapter. 
   */
   zzadrefn_c( (doublereal *) &t1, 
               (doublereal *) &t2,
               (logical    *) &ls1,
               (logical    *) &ls2,
               (doublereal *) &t    );


   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrefn_sp_called", gfrefn_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksd_c ( "svt1 (1)", svt1, "=", t1, 0.0, ok );
   chcksd_c ( "svt2 (1)", svt2, "=", t2, 0.0, ok );

   chcksl_c ( "svs1 (1)", svs1, (SpiceBoolean)ls1,  ok );
   chcksl_c ( "svs2 (1)", svs2, (SpiceBoolean)ls2,  ok );

   /*
   Check the output time value.
   */
   chcksd_c ( "t (1)", (SpiceDouble)t, "=", xt, VTIGHT,  ok );


   /*
   Repeat the test with different inputs. 
   */

   /*
   Set the inputs and expected output.
   */
   t1  =  -3.0;
   t2  =  -1.0;
   ls1 =  (logical)SPICEFALSE;
   ls2 =  (logical)SPICETRUE;
   xt  =  -2.0;

   gfrefn_sp_called = SPICEFALSE;

   /*
   Call the adapter. 
   */
   zzadrefn_c( (doublereal *) &t1, 
               (doublereal *) &t2,
               (logical    *) &ls1,
               (logical    *) &ls2,
               (doublereal *) &t    );


   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrefn_sp_called", gfrefn_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksd_c ( "svt1 (2)", svt1, "=", t1, 0.0, ok );
   chcksd_c ( "svt2 (2)", svt2, "=", t2, 0.0, ok );

   chcksl_c ( "svs1 (2)", svs1, (SpiceBoolean)ls1,  ok );
   chcksl_c ( "svs2 (2)", svs2, (SpiceBoolean)ls2,  ok );

   /*
   Check the output time value.
   */
   chcksd_c ( "t (2)", (SpiceDouble)t, "=", xt, VTIGHT,  ok );





   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadrepi_c" );


   /*
   Indicate that the gfrepi_sp spoof function hasn't yet
   been called.
   */
   gfrepi_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDREPI, (void *)gfrepi_sp );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set the inputs and expected output.
   */

   /*
   Create a window. 
   */
   winsiz = 200;

   scard_c ( 0, &window );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i = 0;  i < winsiz;  i += 2 )
   {
      wninsd_c ( (SpiceDouble)i, (SpiceDouble)(i+1), &window );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Create "beginning" and "ending" messages. q
   */
   strncpy ( xbegmss, "<-----begin message----->", MSGLEN );
   strncpy ( xendmss, "@_____end message_______@", MSGLEN );


   /*
   Call the adapter. 
   */
   zzadrepi_c( (doublereal *) window.base,
               (char       *) xbegmss,
               (char       *) xendmss,
               (ftnlen      ) strlen(xbegmss),
               (ftnlen      ) strlen(xendmss)  );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrepi_sp_called", gfrepi_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksc_c ( "sv_begmss (1)", sv_begmss, "=", xbegmss, ok );
   chcksc_c ( "sv_endmss (1)", sv_endmss, "=", xendmss, ok );


   chcksi_c ( "sv_window size (1)", size_c( &sv_window ), "=",
                                    size_c( &window    ), 0.0,  ok  );

   chcksi_c ( "sv_window card (1)", card_c( &sv_window ), "=",
                                    card_c( &window    ), 0.0,  ok  );

   chckad_c ( "sv_window (1)", 
              (SpiceDouble *) sv_window.data, 
              "=", 
              (SpiceDouble *) window.data,   
              winsiz,
              0.0,
              ok                             );
            

   /*
   Repeat the test with different inputs. 
   */

   /*
   Indicate that the gfrepi_sp spoof function hasn't yet
   been called.
   */
   gfrepi_sp_called = SPICEFALSE;


   /*
   Set the inputs and expected output.
   */

   /*
   Create a window. 
   */
   winsiz = 300;

   scard_c ( 0, &window );
   chckxc_c ( SPICEFALSE, " ", ok );

   for ( i = 0;  i < winsiz;  i += 2 )
   {
      wninsd_c ( 3.0*i, (3.0*i) + 1, &window );
      chckxc_c ( SPICEFALSE, " ", ok );
   }

   /*
   Create "beginning" and "ending" messages. q
   */
   strncpy ( xbegmss, "#*****begin message***#",      MSGLEN );
   strncpy ( xendmss, "!,,,,,,,,end message,,,,,,,!", MSGLEN );


   /*
   Call the adapter. 
   */
   zzadrepi_c( (doublereal *) window.base,
               (char       *) xbegmss,
               (char       *) xendmss,
               (ftnlen      ) strlen(xbegmss),
               (ftnlen      ) strlen(xendmss)  );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrepi_sp_called", gfrepi_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksc_c ( "sv_begmss (2)", sv_begmss, "=", xbegmss, ok );
   chcksc_c ( "sv_endmss (2)", sv_endmss, "=", xendmss, ok );


   chcksi_c ( "sv_window size (2)", size_c( &sv_window ), "=",
                                    size_c( &window    ), 0.0,  ok  );

   chcksi_c ( "sv_window card (2)", card_c( &sv_window ), "=",
                                    card_c( &window    ), 0.0,  ok  );

   chckad_c ( "sv_window (2)", 
              (SpiceDouble *) sv_window.data, 
              "=", 
              (SpiceDouble *) window.data,   
              winsiz,
              0.0,
              ok                             );
            





   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadrepu_c" );


   /*
   Indicate that the gfrepu_sp spoof function hasn't yet
   been called.
   */
   gfrepu_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDREPU, (void *)gfrepu_sp );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set the inputs and expected output.
   */

   /*
   Choose an arbitrary interval begin, end, and intermediate time. 
   */
   ivbeg = 2.0;
   ivbeg = 6.0;
   time  = 4.0;
   
   /*
   Call the adapter. 
   */
   zzadrepu_c( (doublereal *) &ivbeg,
               (doublereal *) &ivend,
               (doublereal *) &time    );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrepu_sp_called", gfrepu_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksd_c ( "svivbeg (1)", svivbeg, "=", ivbeg, 0.0,  ok  );
   chcksd_c ( "svivend (1)", svivend, "=", ivend, 0.0,  ok  );
   chcksd_c ( "svtime  (1)", svtime,  "=", time,  0.0,  ok  );


   /*
   Repeat the test with different inputs. 
   */

   /*
   Indicate that the gfrepu_sp spoof function hasn't yet
   been called.
   */
   gfrepu_sp_called = SPICEFALSE;
   
   /*
   Choose an arbitrary interval begin, end, and intermediate time. 
   */
   ivbeg = -6.0;
   ivbeg = -4.0;
   time  = -2.0;
   
   /*
   Call the adapter. 
   */
   zzadrepu_c( (doublereal *) &ivbeg,
               (doublereal *) &ivend,
               (doublereal *) &time    );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrepu_sp_called", gfrepu_sp_called, SPICETRUE, ok );

   /*
   Check the saved inputs values passed to the adapter. 
   */
   chcksd_c ( "svivbeg (2)", svivbeg, "=", ivbeg, 0.0,  ok  );
   chcksd_c ( "svivend (2)", svivend, "=", ivend, 0.0,  ok  );
   chcksd_c ( "svtime  (2)", svtime,  "=", time,  0.0,  ok  );






   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadrepf_c" );


   /*
   Indicate that the gfrepf_sp spoof function hasn't yet
   been called.
   */
   gfrepf_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDREPF, (void *)gfrepf_sp );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Call the adapter. 
   */
   zzadrepf_c();

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfrepf_sp_called", gfrepf_sp_called, SPICETRUE, ok );

   /*
   Since gfrepf_sp doesn't have any inputs or outputs, just making
   sure it was called by the adapter is all we can do.
   */





   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Test zzadstep_c" );


   /*
   Indicate that the gfstep_sp spoof function hasn't yet
   been called.
   */
   gfstep_sp_called = SPICEFALSE;


   /*
   Store the address of the spoof function to be 
   called later, along with its SpicePassedInFunc identifier. 
   */
   zzadsave_c ( UDSTEP, (void *)gfstep_sp );

   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set the inputs and expected output.
   */
   time = 1.e9;

   /*
   Choose an arbitrary step. In this case, the value we store in
   the static variable is the one we expect to be returned from
   the adapter.
   */
   sv_gfstep_retval = 300.0;

   
   /*
   Call the adapter. 
   */
   zzadstep_c( (doublereal *) &time,
               (doublereal *) &step  );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfstep_sp_called", gfstep_sp_called, SPICETRUE, ok );

   /*
   Make sure the spoof function received the input time argument. 
   */
   chcksd_c ( "svtime (1)", svtime, "=", time, 0.0,  ok  );


   /*
   Check the value returned by the adapter against the saved input value.
   */
   chcksd_c ( "step (1)", step, "=", sv_gfstep_retval, 0.0,  ok  );


   /*
   Repeat the test with different data. 
   */

   /*
   Indicate that the gfstep_sp spoof function hasn't yet
   been called.
   */
   gfstep_sp_called = SPICEFALSE;


   /*
   Set the inputs and expected output.
   */
   time = 1.e10;

   /*
   Choose an arbitrary step. In this case, the value we store in
   the static variable is the one we expect to be returned from
   the adapter.
   */
   sv_gfstep_retval = 400.0;

   
   /*
   Call the adapter. 
   */
   zzadstep_c( (doublereal *) &time,
               (doublereal *) &step  );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Make sure the spoof function was called. 
   */
   chcksl_c ( "gfstep_sp_called", gfstep_sp_called, SPICETRUE, ok );

   /*
   Make sure the spoof function received the input time argument. 
   */
   chcksd_c ( "svtime (1)", svtime, "=", time, 0.0,  ok  );


   /*
   Check the value returned by the adapter against the saved input value.
   */
   chcksd_c ( "step (1)", step, "=", sv_gfstep_retval, 0.0,  ok  );




   /* 
   *******************************************************************
   *
   *  Error cases
   * 
   *******************************************************************
   */



   /*
   The only adapter errors that are signaled are 

      SPICE(MALLOCFAILURE)

   which is signaled from 

      zzadrepi_c

   and 

      SPICE(VALUEOUTOFRANGE)

   which is signaled from the routines
 
      zzadget_c
      zzadsave_c

   Only the latter error is easily tested within tspice_c.
   */


   /* 
   ---- Case ---------------------------------------------------------
   */

   tcase_c ( "zzadsave_c: bad passed-in function type arguments." );

   /*
   Pass in a type lower than the least element of the enum 
   set SpicePassedInFunc.
   */
   zzadsave_c ( -1, (void *)gfstep_c );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );

   /*
   Pass in a type greater than the largest element of the enum 
   set SpicePassedInFunc. The parameter SPICE_N_PASSED_IN_FUNC
   has the value of of the number of elements of SpicePassedInFunc,
   so use that for an invalid type.
   */
   zzadsave_c ( SPICE_N_PASSED_IN_FUNC, (void *)gfstep_c );
   
   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "zzadget_c: bad passed-in function type arguments." );

   /*
   Pass in a type lower than the least element of the enum 
   set SpicePassedInFunc.
   */
   fPtr = zzadget_c ( -1 );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );


   /*
   Pass in a type greater than the largest element of the enum 
   set SpicePassedInFunc.
   */
   fPtr = zzadget_c ( SPICE_N_PASSED_IN_FUNC );

   chckxc_c ( SPICETRUE, "SPICE(VALUEOUTOFRANGE)", ok );




   /* 
   *******************************************************************
   *
   *  Tests of adapter usage within CSPICE mid-level search APIs
   * 
   *******************************************************************
   */


   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test adapter use in gfocce_c." );

   /*
   Perform an occultation search using gfocce_c. Use both
   default progress reporting and signal handling. 
   */


   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Normally we'd establish an interrupt signal handler here,
   but we don't want to make the user unable to terminate
   the test program.
 
   Obtain the TDB time bounds of the confinement
   window, which is a single interval in this case.

   We use bounds for an occultation we know can be
   found using our test kernels. The solar eclipse in
   2001 that we'd normally use never happened according
   to these kernels.
   */
   win0 = "2002 NOV 01 00:00:00 TDB";
   win1 = "2002 DEC 01 00:00:00 TDB";

   str2et_c ( win0, &et0 );
   str2et_c ( win1, &et1 );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Insert the time bounds into the confinement
   window.
   */
   wninsd_c ( et0, et1, &cnfine );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Select a 3600-second step. We'll ignore any occultations
   lasting less than 3600 seconds. Use the spoof step size
   static variable rather than the default step size setting
   routine.
   */
   step             = 3600.0;
   sv_gfstep_retval = step;

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Turn on interrupt handling and progress reporting.
   */
   bail = SPICETRUE;
   rpt  = SPICETRUE;

   /*
   Perform the search. Use the spoof progress reporting,
   refinement, interrupt testing, and step size routines.

   Before starting the search, set the static variables 
   to indicate the spoof functions haven't been called.
   */

   gfbail_sp_called = SPICEFALSE;
   gfrefn_sp_called = SPICEFALSE;
   gfrepf_sp_called = SPICEFALSE;
   gfrepi_sp_called = SPICEFALSE;
   gfrepu_sp_called = SPICEFALSE;
   gfstep_sp_called = SPICEFALSE;

   gfocce_c ( "ANY",                            
              "MOON",     "ellipsoid",  "IAU_MOON", 
              "SUN",      "ellipsoid",  "IAU_SUN",  
              "LT",       "EARTH",      CNVTOL,    
              gfstep_sp,  gfrefn_sp,    rpt,       
              gfrepi_sp,  gfrepu_sp,    gfrepf_sp, 
              bail,       gfbail_sp,    &cnfine,   
              &result                              );

   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Verify that the spoof functions were called. 
   */

   chcksl_c ( "gfstep_sp_called", gfstep_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrefn_sp_called", gfrefn_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepi_sp_called", gfrepi_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepu_sp_called", gfrepu_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepf_sp_called", gfrepf_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfbail_sp_called", gfbail_sp_called, SPICETRUE, ok );



   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test adapter use in gffove_c." );


   /*
   Create and load Nat's solar system SPK, PCK/FK, and IK files.
   */
 

   /*
   Create and load a PCK file. Do NOT delete the file afterward.
   */
   natpck_ ( ( char      * ) PCK,
             ( logical   * ) &loadpc, 
             ( logical   * ) &keeppc, 
             ( ftnlen      ) strlen(PCK) );   
   chckxc_c ( SPICEFALSE, " ", ok );
   
   /*
   Load an SPK file as well.
   */
   natspk_ ( ( char      * ) NATSPK,
             ( logical   * ) &loadspk, 
             ( integer   * ) &NatSPKHan, 
             ( ftnlen      ) strlen(NATSPK) );   
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Load an IK.
   */
   natik_  ( ( char      * ) IK,
             ( char      * ) NATSPK,
             ( char      * ) PCK,
             ( logical   * ) &loadik, 
             ( logical   * ) &keepik, 
             ( ftnlen      ) strlen(IK),
             ( ftnlen      ) strlen(SPK),   
             ( ftnlen      ) strlen(PCK)  );   
   chckxc_c ( SPICEFALSE, " ", ok );



   /*
   Normal search: find appearances of beta
   in FOV of ALPHA_ELLIPSE_NONE.
   */

   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Select a 300 second step. We'll ignore any visibility
   events lasting less than 300 seconds. Use the spoof step size
   static variable rather than the default step size setting
   routine.
   */
   step             = 300.0;
   sv_gfstep_retval = step;

   /*
   Turn on interrupt handling and progress reporting.
   */
   bail = SPICETRUE;
   rpt  = SPICETRUE;

   /*
   Perform the search. Use the spoof progress reporting,
   refinement, interrupt testing, and step size routines.

   Before starting the search, set the static variables 
   to indicate the spoof functions haven't been called.
   */

   gfbail_sp_called = SPICEFALSE;
   gfrefn_sp_called = SPICEFALSE;
   gfrepf_sp_called = SPICEFALSE;
   gfrepi_sp_called = SPICEFALSE;
   gfrepu_sp_called = SPICEFALSE;
   gfstep_sp_called = SPICEFALSE;

   /*
   gftfov_c ( "ALPHA_ELLIPSE_NONE", 
              "beta", "ellipsoid", "betafixed",
              "none", "sun",       step, &cnfine, &result );
   chckxc_c( SPICEFALSE, " ", ok );
   */

   gffove_c ( "ALPHA_ELLIPSE_NONE",  "ellipsoid",  raydir, 
              "beta",                "betafixed",  "none",
              "Sun",                 CNVTOL,       
              gfstep_sp,             gfrefn_sp,    rpt,
              gfrepi_sp,             gfrepu_sp,    gfrepf_sp,
              bail,                  gfbail_sp,    &cnfine,   &result );

   chckxc_c( SPICEFALSE, " ", ok );



   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );

   /*
   Verify that the spoof functions were called. 
   */

   chcksl_c ( "gfstep_sp_called", gfstep_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrefn_sp_called", gfrefn_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepi_sp_called", gfrepi_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepu_sp_called", gfrepu_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepf_sp_called", gfrepf_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfbail_sp_called", gfbail_sp_called, SPICETRUE, ok );





   /* 
   ---- Case ---------------------------------------------------------
   */
   tcase_c ( "Test adapter use in gfevnt_c." );

   /*
   Clean up cnfine before setting the new value. 
   */
   scard_c  ( 0, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 1 TDB", &et0 );
   chckxc_c ( SPICEFALSE, " ", ok );

   str2et_c ( "2000 jan 5 13:00TDB", &et1 );
   chckxc_c( SPICEFALSE, " ", ok );
   
   wninsd_c ( et0, et1, &cnfine );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Use a 300 second step. 
   */
   gfsstp_c ( 300.0 );
 
   /*
   Turn on interrupt handling and progress reporting.
   */
   bail   = SPICETRUE;
   rpt    = SPICETRUE;

   /*
   Clear the bail-out status. 
   */
   gfclrh_c();
   chckxc_c ( SPICEFALSE, " ", ok );


   /*
   Set up search parameters.
   */

   gquant = "ANGULAR SEPARATION";
   qnpars = 9;

   strncpy ( qpnams[0], "TARGET1",         PARAMLN );
   strncpy ( qpnams[1], "FRAME1",          PARAMLN );
   strncpy ( qpnams[2], "SHAPE1",          PARAMLN );
   strncpy ( qpnams[3], "TARGET2",         PARAMLN );
   strncpy ( qpnams[4], "FRAME2",          PARAMLN );
   strncpy ( qpnams[5], "SHAPE2",          PARAMLN );
   strncpy ( qpnams[6], "OBSERVER",        PARAMLN );
   strncpy ( qpnams[7], "ABCORR",          PARAMLN );
   strncpy ( qpnams[8], "REFERENCE FRAME", PARAMLN );

   strncpy ( qcpars[0], "ALPHA",           PARAMLN );
   strncpy ( qcpars[1], "ALPHAFIXED",      PARAMLN );
   strncpy ( qcpars[2], "SPHERE",          PARAMLN );
   strncpy ( qcpars[3], "BETA",            PARAMLN );
   strncpy ( qcpars[4], "BETAFIXED",       PARAMLN );
   strncpy ( qcpars[5], "SPHERE",          PARAMLN );
   strncpy ( qcpars[6], "SUN",             PARAMLN );
   strncpy ( qcpars[7], "NONE",            PARAMLN );
   strncpy ( qcpars[8], "J2000",           PARAMLN );

   relate = "<";
   refval = 5.0 * rpd_c();
   adjust = 0.0;

   /*
   Perform the search. Use the spoof progress reporting,
   refinement, interrupt testing, and step size routines.

   Before starting the search, set the static variables 
   to indicate the spoof functions haven't been called.
   */

   gfbail_sp_called = SPICEFALSE;
   gfrefn_sp_called = SPICEFALSE;
   gfrepf_sp_called = SPICEFALSE;
   gfrepi_sp_called = SPICEFALSE;
   gfrepu_sp_called = SPICEFALSE;
   gfstep_sp_called = SPICEFALSE;


   gfevnt_c ( gfstep_sp,  gfrefn_sp,   gquant,    qnpars,
              PARAMLN,    qpnams,      qcpars,    qdpars,
              qipars,     qlpars,      relate,    refval,
              CNVTOL,     adjust,      rpt,       gfrepi_sp,   
              gfrepu_sp,  gfrepf_sp,   MAXWIN,    bail, 
              gfbail_sp,  &cnfine,    &result              ); 

   chckxc_c ( SPICEFALSE, " ", ok );


   n = wncard_c ( &result );
   chckxc_c ( SPICEFALSE, " ", ok );

   /*
   Check the number of solution intervals. 
   */
   chcksi_c ( "n", n, "=", 5, 0, ok );


   /*
   Verify that the spoof functions were called. 
   */
   chcksl_c ( "gfstep_sp_called", gfstep_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrefn_sp_called", gfrefn_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepi_sp_called", gfrepi_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepu_sp_called", gfrepu_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfrepf_sp_called", gfrepf_sp_called, SPICETRUE, ok );
   chcksl_c ( "gfbail_sp_called", gfbail_sp_called, SPICETRUE, ok );



   /*
   ------ Case -------------------------------------------------------
   */
   tcase_c ( "Clean up." );

   /*
   Get rid of the SPK files.
   */
   spkuef_c ( handle );
   TRASH    ( SPK    );

   spkuef_c ( NatSPKHan );
   TRASH    ( NATSPK    );
    
   /*
   Retrieve the current test status.
   */ 
   t_success_c ( ok ); 
   
   
} /* End f_zzadapt_c */







   
   /* 
   *******************************************************************
   *
   *  Spoof function definitions
   * 
   *******************************************************************
   */
   

   /*
   Spoof of gfbail_c: 
   */
   SpiceBoolean gfbail_sp ( void )
   {
      /*
      Indicate this function was called. 
      */
      gfbail_sp_called = SPICETRUE;


      /*
      Return whatever value has been chosen by the
      calling program. 
      */
      return sv_gfbail_retval;
   }



   /*
   Spoof of gfrefn_c: 
   */
   void              gfrefn_sp ( SpiceDouble        t1,
                                 SpiceDouble        t2,
                                 SpiceBoolean       s1,
                                 SpiceBoolean       s2,
                                 SpiceDouble      * t  )
   {
      /*
      Indicate this function was called. 
      */
      gfrefn_sp_called = SPICETRUE;


      /*
      All we need do here is store the inputs in 
      static variables for later examination. 
      */
      svt1   =  t1;
      svt2   =  t2;

      *t     =  ( t1 + t2 ) / 2;

      svs1   =  s1;
      svs2   =  s2;

      return;
   }


   /*
   Spoof of gfrepf_c: 
   */
   void              gfrepf_sp ( void )
   {
      /*
      Indicate this function was called. 
      */
      gfrepf_sp_called = SPICETRUE;

      return;
   }


   /*
   Spoof of gfrepi_c: 
   */
   void              gfrepi_sp ( SpiceCell        * window,
                                 ConstSpiceChar   * begmss,
                                 ConstSpiceChar   * endmss  )
   {
      /*
      Indicate this function was called. 
      */
      gfrepi_sp_called = SPICETRUE;


      /*
      All we need do here is store the inputs in 
      static variables for later examination. 
      */
      copy_c ( window, &sv_window );

      strncpy ( sv_begmss, begmss, MSGLEN );
      strncpy ( sv_endmss, endmss, MSGLEN );

      return;
   }


   /*
   Spoof of gfrepu_c: 
   */
   void              gfrepu_sp ( SpiceDouble         ivbeg,
                                 SpiceDouble         ivend,
                                 SpiceDouble         time   )   
   {
      /*
      Indicate this function was called. 
      */
      gfrepu_sp_called = SPICETRUE;

      /*
      All we need do here is store the inputs in 
      static variables for later examination. 
      */
      svivbeg = ivbeg;
      svivend = ivend;
      svtime  = time;

      return;
   }


   /*
   Spoof of gfstep_c: 
   */
   void              gfstep_sp ( SpiceDouble         time,
                                 SpiceDouble       * step  )
   {
      /*
      Indicate this function was called. 
      */
      gfstep_sp_called = SPICETRUE;

      /*
      All we need do here is store the inputs in 
      static variables for later examination. 
      */
      svtime  = time;

      *step   = sv_gfstep_retval;

      return;
   }


