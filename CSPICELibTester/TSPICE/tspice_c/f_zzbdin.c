/* f_zzbdin.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__6 = 6;
static integer c__5 = 5;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;

/* $Procedure F_ZZBDIN ( Body Name/Code Initialization Test Family ) */
/* Subroutine */ int f_zzbdin__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer case__;
    static char line[80];
    extern /* Subroutine */ int zzhscchk_(integer *, integer *, char *, char *
	    , integer *, ftnlen, ftnlen), zzbodini_(char *, char *, integer *,
	     integer *, integer *, integer *, integer *, char *, integer *, 
	    integer *, integer *, integer *, integer *, ftnlen, ftnlen, 
	    ftnlen), zzhsichk_(integer *, integer *, integer *, integer *, 
	    integer *);
    static integer i__, j, codes[5];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char names[36*5];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80];
    static integer nvals;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , clearc_(integer *, char *, ftnlen);
    static logical codfnd[5];
    static integer bidids[5];
    extern /* Subroutine */ int cleari_(integer *, integer *);
    static integer bididx[5];
    static logical namfnd[5];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static integer bidpol[11], codidx[5], namidx[5], bnmidx[5], bidlst[5], 
	    bnmpol[11];
    static char nornam[36*5], bnmnms[36*5];
    static integer bnmlst[5];

/* $ Abstract */

/*     Test family to exercise the logic and code in the body name-code */
/*     processing software. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This include file lists the parameter collection */
/*     defining the number of SPICE ID -> NAME mappings. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     MAXL        is the maximum length of a body name. */

/*     MAXP        is the maximum number of additional names that may */
/*                 be added via the ZZBODDEF interface. */

/*     NPERM       is the count of the mapping assignments built into */
/*                 SPICE. */

/*     MAXE        is the size of the lists and hashes storing combined */
/*                 built-in and ZZBODDEF-defined name/ID mappings. To */
/*                 ensure efficient hashing this size is the set to the */
/*                 first prime number greater than ( MAXP + NPERM ). */

/*     NROOM       is the size of the lists and hashes storing the */
/*                 POOL-defined name/ID mappings. To ensure efficient */
/*                 hashing and to provide the ability to store nearly as */
/*                 many names as can fit in the POOL, this size is */
/*                 set to the first prime number less than MAXLIN */
/*                 defined in the POOL umbrella routine. */

/* $ Required_Reading */

/*     naif_ids.req */

/* $ Keywords */

/*     BODY */
/*     CONVERSION */

/* $ Author_and_Institution */

/*     B.V. Semenov (JPL) */
/*     E.D. Wright  (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 04-APR-2017 (BVS)(EDW) */

/*        Increased NROOM to 14983. Added a comment note explaining */
/*        NROOM and MAXE */

/* -    SPICELIB Version 1.0.0, 20-MAY-2010 (EDW) */

/*        N0064 version with MAXP = 150, NPERM = 563, */
/*        MAXE = MAXP + NPERM, and NROOM = 2000. */

/*     A script generates this file. Do not edit by hand. */
/*     Edit the creation script to modify the contents of */
/*     ZZBODTRN.INC. */


/*     Maximum size of a NAME string */


/*     Maximum number of additional names that may be added via the */
/*     ZZBODDEF interface. */


/*     Count of default SPICE mapping assignments. */


/*     Size of the lists and hashes storing the built-in and */
/*     ZZBODDEF-defined name/ID mappings. To ensure efficient hashing */
/*     this size is the set to the first prime number greater than */
/*     ( MAXP + NPERM ). */


/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing and to provide the ability */
/*     to store nearly as many names as can fit in the POOL, this size */
/*     is set to the first prime number less than MAXLIN defined in */
/*     the POOL umbrella routine. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine exercise the conformance of the body name-code */
/*     initialization routine, ZZBODINI, to the intended/designed */
/*     behavior. */

/*     We need not perform any stress tests here, as F_BODCOD */
/*     attempts to provide these sorts of tests from higher level */
/*     interfaces. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 16-SEP-2013 (BVS) */

/*        Updated to test the hash-based version of ZZBODINI. */

/* -    TSPICE Version 1.0.0, 26-AUG-2002 (FST) */


/* -& */

/*     Local parameters. */


/*     Local Variables */


/*     Save everything. */


/*     Open the test family. */

    topen_("F_ZZBDIN", (ftnlen)8);

/*     First check the only ZZBODINI exception that we can trigger. */

    tcase_("Exception: inconsistent dimensions.", (ftnlen)35);
    zzbodini_(names, nornam, codes, &c__6, &c__5, bnmlst, bnmpol, bnmnms, 
	    bnmidx, bidlst, bidpol, bidids, bididx, (ftnlen)36, (ftnlen)36, (
	    ftnlen)36);
    chckxc_(&c_true, "SPICE(BUG1)", ok, (ftnlen)11);

/*     Then check success cases. */

    for (case__ = 1; case__ <= 5; ++case__) {

/*        Force-clear inputs and hashes. */

	for (i__ = 1; i__ <= 5; ++i__) {
	    namfnd[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge("namfnd",
		     i__1, "f_zzbdin__", (ftnlen)192)] = TRUE_;
	    codfnd[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge("codfnd",
		     i__1, "f_zzbdin__", (ftnlen)193)] = TRUE_;
	}
	clearc_(&c__5, names, (ftnlen)36);
	clearc_(&c__5, nornam, (ftnlen)36);
	cleari_(&c__5, codes);
	cleari_(&c__5, namidx);
	cleari_(&c__5, codidx);
	cleari_(&c__5, bnmlst);
	cleari_(&c__5, bnmpol);
	clearc_(&c__5, bnmnms, (ftnlen)36);
	cleari_(&c__5, bnmidx);
	cleari_(&c__5, bidlst);
	cleari_(&c__5, bidpol);
	cleari_(&c__5, bidids);
	cleari_(&c__5, bididx);

/*        Set up inputs and expected values for each test case. */

	if (case__ == 1) {

/*           This is simple mapping with a few distinct names mapping */
/*           to a few distinct IDs. */

	    s_copy(title, "Simple one-to-one mapping", (ftnlen)80, (ftnlen)25)
		    ;
	    nvals = 3;
	    s_copy(names, " Alpha", (ftnlen)36, (ftnlen)6);
	    s_copy(nornam, "ALPHA", (ftnlen)36, (ftnlen)5);
	    codes[0] = 1001;
	    namfnd[0] = TRUE_;
	    codfnd[0] = TRUE_;
	    namidx[0] = 1;
	    codidx[0] = 1;
	    s_copy(names + 36, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 36, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[1] = 1002;
	    namfnd[1] = TRUE_;
	    codfnd[1] = TRUE_;
	    namidx[1] = 2;
	    codidx[1] = 2;
	    s_copy(names + 72, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 72, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[2] = 1003;
	    namfnd[2] = TRUE_;
	    codfnd[2] = TRUE_;
	    namidx[2] = 3;
	    codidx[2] = 3;
	    s_copy(names + 108, "   Delta", (ftnlen)36, (ftnlen)8);
	    s_copy(nornam + 108, "DELTA", (ftnlen)36, (ftnlen)5);
	    codes[3] = 1004;
	    namfnd[3] = FALSE_;
	    codfnd[3] = FALSE_;
	    namidx[3] = 0;
	    codidx[3] = 0;
	    s_copy(names + 144, "     eCHo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 144, "ECHO", (ftnlen)36, (ftnlen)4);
	    codes[4] = 1005;
	    namfnd[4] = FALSE_;
	    codfnd[4] = FALSE_;
	    namidx[4] = 0;
	    codidx[4] = 0;
	} else if (case__ == 2) {

/*           This mapping has a few duplicate pairs. */

	    s_copy(title, "Mapping with duplicates", (ftnlen)80, (ftnlen)23);
	    nvals = 4;
	    s_copy(names, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[0] = 1002;
	    namfnd[0] = TRUE_;
	    codfnd[0] = TRUE_;
	    namidx[0] = 2;
	    codidx[0] = 2;
	    s_copy(names + 36, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 36, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[1] = 1002;
	    namfnd[1] = TRUE_;
	    codfnd[1] = TRUE_;
	    namidx[1] = 2;
	    codidx[1] = 2;
	    s_copy(names + 72, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 72, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[2] = 1003;
	    namfnd[2] = TRUE_;
	    codfnd[2] = TRUE_;
	    namidx[2] = 4;
	    codidx[2] = 4;
	    s_copy(names + 108, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 108, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[3] = 1003;
	    namfnd[3] = TRUE_;
	    codfnd[3] = TRUE_;
	    namidx[3] = 4;
	    codidx[3] = 4;
	    s_copy(names + 144, "     eCHo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 144, "ECHO", (ftnlen)36, (ftnlen)4);
	    codes[4] = 1005;
	    namfnd[4] = FALSE_;
	    codfnd[4] = FALSE_;
	    namidx[4] = 0;
	    codidx[4] = 0;
	} else if (case__ == 3) {

/*           This mapping has a few distinct names mapping to the */
/*           same ID. */

	    s_copy(title, "Mapping with different names and same ID", (ftnlen)
		    80, (ftnlen)40);
	    nvals = 3;
	    s_copy(names, " Alpha", (ftnlen)36, (ftnlen)6);
	    s_copy(nornam, "ALPHA", (ftnlen)36, (ftnlen)5);
	    codes[0] = 1003;
	    namfnd[0] = TRUE_;
	    codfnd[0] = TRUE_;
	    namidx[0] = 1;
	    codidx[0] = 3;
	    s_copy(names + 36, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 36, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[1] = 1003;
	    namfnd[1] = TRUE_;
	    codfnd[1] = TRUE_;
	    namidx[1] = 2;
	    codidx[1] = 3;
	    s_copy(names + 72, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 72, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[2] = 1003;
	    namfnd[2] = TRUE_;
	    codfnd[2] = TRUE_;
	    namidx[2] = 3;
	    codidx[2] = 3;
	    s_copy(names + 108, "   Delta", (ftnlen)36, (ftnlen)8);
	    s_copy(nornam + 108, "DELTA", (ftnlen)36, (ftnlen)5);
	    codes[3] = 1004;
	    namfnd[3] = FALSE_;
	    codfnd[3] = FALSE_;
	    namidx[3] = 0;
	    codidx[3] = 0;
	    s_copy(names + 144, "     eCHo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 144, "ECHO", (ftnlen)36, (ftnlen)4);
	    codes[4] = 1005;
	    namfnd[4] = FALSE_;
	    codfnd[4] = FALSE_;
	    namidx[4] = 0;
	    codidx[4] = 0;
	} else if (case__ == 4) {

/*           This mapping has the single name mapping to a few different */
/*           IDs, all of which but the last should be masked. */

	    s_copy(title, "Mapping with same name different (masked) ID", (
		    ftnlen)80, (ftnlen)44);
	    nvals = 5;
	    s_copy(names, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[0] = 1001;
	    namfnd[0] = TRUE_;
	    codfnd[0] = FALSE_;
	    namidx[0] = 2;
	    codidx[0] = 0;
	    s_copy(names + 36, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 36, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[1] = 1002;
	    namfnd[1] = TRUE_;
	    codfnd[1] = TRUE_;
	    namidx[1] = 2;
	    codidx[1] = 2;
	    s_copy(names + 72, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 72, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[2] = 1003;
	    namfnd[2] = TRUE_;
	    codfnd[2] = FALSE_;
	    namidx[2] = 5;
	    codidx[2] = 0;
	    s_copy(names + 108, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 108, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[3] = 1004;
	    namfnd[3] = TRUE_;
	    codfnd[3] = FALSE_;
	    namidx[3] = 5;
	    codidx[3] = 0;
	    s_copy(names + 144, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 144, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[4] = 1005;
	    namfnd[4] = TRUE_;
	    codfnd[4] = TRUE_;
	    namidx[4] = 5;
	    codidx[4] = 5;
	} else if (case__ == 5) {

/*           This is a mixed case with a few distinct names mapping to */
/*           the same ID and a single name mapping for more than one ID. */

	    s_copy(title, "Mixed mapping", (ftnlen)80, (ftnlen)13);
	    nvals = 4;
	    s_copy(names, " Alpha", (ftnlen)36, (ftnlen)6);
	    s_copy(nornam, "ALPHA", (ftnlen)36, (ftnlen)5);
	    codes[0] = 1002;
	    namfnd[0] = TRUE_;
	    codfnd[0] = TRUE_;
	    namidx[0] = 1;
	    codidx[0] = 2;
	    s_copy(names + 36, "    BraVo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 36, "BRAVO", (ftnlen)36, (ftnlen)5);
	    codes[1] = 1002;
	    namfnd[1] = TRUE_;
	    codfnd[1] = TRUE_;
	    namidx[1] = 2;
	    codidx[1] = 2;
	    s_copy(names + 72, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 72, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[2] = 1003;
	    namfnd[2] = TRUE_;
	    codfnd[2] = FALSE_;
	    namidx[2] = 4;
	    codidx[2] = 0;
	    s_copy(names + 108, "  Charlie", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 108, "CHARLIE", (ftnlen)36, (ftnlen)7);
	    codes[3] = 1004;
	    namfnd[3] = TRUE_;
	    codfnd[3] = TRUE_;
	    namidx[3] = 4;
	    codidx[3] = 4;
	    s_copy(names + 144, "     eCHo", (ftnlen)36, (ftnlen)9);
	    s_copy(nornam + 144, "ECHO", (ftnlen)36, (ftnlen)4);
	    codes[4] = 1005;
	    namfnd[4] = FALSE_;
	    codfnd[4] = FALSE_;
	    namidx[4] = 0;
	    codidx[4] = 0;
	}

/*        Report case title. */

	tcase_(title, (ftnlen)80);

/*        Toss inputs at ZZBODINI. */

	zzbodini_(names, nornam, codes, &nvals, &c__5, bnmlst, bnmpol, bnmnms,
		 bnmidx, bidlst, bidpol, bidids, bididx, (ftnlen)36, (ftnlen)
		36, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check if we got back what we expected. */

	for (i__ = 1; i__ <= 5; ++i__) {

/*           Check name hash first. */

	    zzhscchk_(bnmlst, bnmpol, bnmnms, nornam + ((i__1 = i__ - 1) < 5 
		    && 0 <= i__1 ? i__1 : s_rnge("nornam", i__1, "f_zzbdin__",
		     (ftnlen)488)) * 36, &j, (ftnlen)36, (ftnlen)36);
	    found = j != 0;
	    s_copy(line, "Name # found in hash", (ftnlen)80, (ftnlen)20);
	    repmc_(line, "#", nornam + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("nornam", i__1, "f_zzbdin__", (ftnlen)493)) 
		    * 36, line, (ftnlen)80, (ftnlen)1, (ftnlen)36, (ftnlen)80)
		    ;
	    chcksl_(line, &found, &namfnd[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("namfnd", i__1, "f_zzbdin__", (ftnlen)494)],
		     ok, (ftnlen)80);
	    if (j != 0) {
		chcksi_("BNMIDX(J)", &bnmidx[(i__1 = j - 1) < 5 && 0 <= i__1 ?
			 i__1 : s_rnge("bnmidx", i__1, "f_zzbdin__", (ftnlen)
			497)], "=", &namidx[(i__2 = i__ - 1) < 5 && 0 <= i__2 
			? i__2 : s_rnge("namidx", i__2, "f_zzbdin__", (ftnlen)
			497)], &c__0, ok, (ftnlen)9, (ftnlen)1);
	    }

/*           Now check ID hash. */

	    zzhsichk_(bidlst, bidpol, bidids, &codes[(i__1 = i__ - 1) < 5 && 
		    0 <= i__1 ? i__1 : s_rnge("codes", i__1, "f_zzbdin__", (
		    ftnlen)504)], &j);
	    found = j != 0;
	    s_copy(line, "CODE # found in hash", (ftnlen)80, (ftnlen)20);
	    repmi_(line, "#", &codes[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 
		    : s_rnge("codes", i__1, "f_zzbdin__", (ftnlen)509)], line,
		     (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chcksl_(line, &found, &codfnd[(i__1 = i__ - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("codfnd", i__1, "f_zzbdin__", (ftnlen)510)],
		     ok, (ftnlen)80);
	    if (j != 0) {
		chcksi_("BIDIDX(J)", &bididx[(i__1 = j - 1) < 5 && 0 <= i__1 ?
			 i__1 : s_rnge("bididx", i__1, "f_zzbdin__", (ftnlen)
			513)], "=", &codidx[(i__2 = i__ - 1) < 5 && 0 <= i__2 
			? i__2 : s_rnge("codidx", i__2, "f_zzbdin__", (ftnlen)
			513)], &c__0, ok, (ftnlen)9, (ftnlen)1);
	    }
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzbdin__ */

