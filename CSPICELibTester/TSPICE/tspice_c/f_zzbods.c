/* f_zzbods.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;

/* $Procedure F_ZZBODS (Family of tests for ZZBODTRN) */
/* Subroutine */ int f_zzbods__(logical *ok)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    integer code;
    char name__[36];
    integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), bodn2c_(char *, 
	    integer *, logical *, ftnlen);
    integer c2ncod[620], n2ccod[620];
    extern /* Subroutine */ int bodc2n_(integer *, char *, logical *, ftnlen),
	     t_success__(logical *);
    char c2nnam[36*620], n2cnam[36*620];
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    char tststr[100];

/* $ Abstract */

/*     This family of tests exercises the ZZBODTRN interfaces */
/*     BODN2C, BODC2N. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     N/A. */

/* $ Declarations */

/*     None. */

/* $ Brief_I/O */

/*     None. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     None. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright, 04-APR-2017 (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 04-APR-2017 (EDW) */

/*        Name-ID list update. */

/* -    TSPICE Version 1.1.1, 27-FEB-2012 (EDW) */

/*       Expended Version section. Corrected format to meet */
/*       standard for NAIF headers. */

/* -    TSPICE Version 1.1.0, 06-MAR-2009 (EDW) */

/*       Expanded header block to match current NAIF standard */
/*       for headers. */

/* -    TSPICE Version 1.0.0, 18-MAR-2003 (EDW) */

/* -& */

/* Local variables. */

/* $ Abstract */

/*     This include file lists the parameter collection */
/*     defining the number of SPICE ID -> NAME mappings. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     MAXL        is the maximum length of a body name. */

/*     MAXP        is the maximum number of additional names that may */
/*                 be added via the ZZBODDEF interface. */

/*     NPERM       is the count of the mapping assignments built into */
/*                 SPICE. */

/*     MAXE        is the size of the lists and hashes storing combined */
/*                 built-in and ZZBODDEF-defined name/ID mappings. To */
/*                 ensure efficient hashing this size is the set to the */
/*                 first prime number greater than ( MAXP + NPERM ). */

/*     NROOM       is the size of the lists and hashes storing the */
/*                 POOL-defined name/ID mappings. To ensure efficient */
/*                 hashing and to provide the ability to store nearly as */
/*                 many names as can fit in the POOL, this size is */
/*                 set to the first prime number less than MAXLIN */
/*                 defined in the POOL umbrella routine. */

/* $ Required_Reading */

/*     naif_ids.req */

/* $ Keywords */

/*     BODY */
/*     CONVERSION */

/* $ Author_and_Institution */

/*     B.V. Semenov (JPL) */
/*     E.D. Wright  (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 04-APR-2017 (BVS)(EDW) */

/*        Increased NROOM to 14983. Added a comment note explaining */
/*        NROOM and MAXE */

/* -    SPICELIB Version 1.0.0, 20-MAY-2010 (EDW) */

/*        N0064 version with MAXP = 150, NPERM = 563, */
/*        MAXE = MAXP + NPERM, and NROOM = 2000. */

/*     A script generates this file. Do not edit by hand. */
/*     Edit the creation script to modify the contents of */
/*     ZZBODTRN.INC. */


/*     Maximum size of a NAME string */


/*     Maximum number of additional names that may be added via the */
/*     ZZBODDEF interface. */


/*     Count of default SPICE mapping assignments. */


/*     Size of the lists and hashes storing the built-in and */
/*     ZZBODDEF-defined name/ID mappings. To ensure efficient hashing */
/*     this size is the set to the first prime number greater than */
/*     ( MAXP + NPERM ). */


/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing and to provide the ability */
/*     to store nearly as many names as can fit in the POOL, this size */
/*     is set to the first prime number less than MAXLIN defined in */
/*     the POOL umbrella routine. */


/*     A script generates this file. Do not edit by hand. */
/*     Edit the creation script to modify the contents of */
/*     F_ZZBODS.FOR. */


/*     The data list for the NAME -> ID tests. */

    n2ccod[0] = 0;
    s_copy(n2cnam, "SOLAR_SYSTEM_BARYCENTER", (ftnlen)36, (ftnlen)23);
    n2ccod[1] = 0;
    s_copy(n2cnam + 36, "SSB", (ftnlen)36, (ftnlen)3);
    n2ccod[2] = 0;
    s_copy(n2cnam + 72, "SOLAR SYSTEM BARYCENTER", (ftnlen)36, (ftnlen)23);
    n2ccod[3] = 1;
    s_copy(n2cnam + 108, "MERCURY_BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[4] = 1;
    s_copy(n2cnam + 144, "MERCURY BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[5] = 2;
    s_copy(n2cnam + 180, "VENUS_BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[6] = 2;
    s_copy(n2cnam + 216, "VENUS BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[7] = 3;
    s_copy(n2cnam + 252, "EARTH_BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[8] = 3;
    s_copy(n2cnam + 288, "EMB", (ftnlen)36, (ftnlen)3);
    n2ccod[9] = 3;
    s_copy(n2cnam + 324, "EARTH MOON BARYCENTER", (ftnlen)36, (ftnlen)21);
    n2ccod[10] = 3;
    s_copy(n2cnam + 360, "EARTH-MOON BARYCENTER", (ftnlen)36, (ftnlen)21);
    n2ccod[11] = 3;
    s_copy(n2cnam + 396, "EARTH BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[12] = 4;
    s_copy(n2cnam + 432, "MARS_BARYCENTER", (ftnlen)36, (ftnlen)15);
    n2ccod[13] = 4;
    s_copy(n2cnam + 468, "MARS BARYCENTER", (ftnlen)36, (ftnlen)15);
    n2ccod[14] = 5;
    s_copy(n2cnam + 504, "JUPITER_BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[15] = 5;
    s_copy(n2cnam + 540, "JUPITER BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[16] = 6;
    s_copy(n2cnam + 576, "SATURN_BARYCENTER", (ftnlen)36, (ftnlen)17);
    n2ccod[17] = 6;
    s_copy(n2cnam + 612, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    n2ccod[18] = 7;
    s_copy(n2cnam + 648, "URANUS_BARYCENTER", (ftnlen)36, (ftnlen)17);
    n2ccod[19] = 7;
    s_copy(n2cnam + 684, "URANUS BARYCENTER", (ftnlen)36, (ftnlen)17);
    n2ccod[20] = 8;
    s_copy(n2cnam + 720, "NEPTUNE_BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[21] = 8;
    s_copy(n2cnam + 756, "NEPTUNE BARYCENTER", (ftnlen)36, (ftnlen)18);
    n2ccod[22] = 9;
    s_copy(n2cnam + 792, "PLUTO_BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[23] = 9;
    s_copy(n2cnam + 828, "PLUTO BARYCENTER", (ftnlen)36, (ftnlen)16);
    n2ccod[24] = 10;
    s_copy(n2cnam + 864, "SUN", (ftnlen)36, (ftnlen)3);
    n2ccod[25] = 199;
    s_copy(n2cnam + 900, "MERCURY", (ftnlen)36, (ftnlen)7);
    n2ccod[26] = 299;
    s_copy(n2cnam + 936, "VENUS", (ftnlen)36, (ftnlen)5);
    n2ccod[27] = 399;
    s_copy(n2cnam + 972, "EARTH", (ftnlen)36, (ftnlen)5);
    n2ccod[28] = 301;
    s_copy(n2cnam + 1008, "MOON", (ftnlen)36, (ftnlen)4);
    n2ccod[29] = 499;
    s_copy(n2cnam + 1044, "MARS", (ftnlen)36, (ftnlen)4);
    n2ccod[30] = 401;
    s_copy(n2cnam + 1080, "PHOBOS", (ftnlen)36, (ftnlen)6);
    n2ccod[31] = 402;
    s_copy(n2cnam + 1116, "DEIMOS", (ftnlen)36, (ftnlen)6);
    n2ccod[32] = 599;
    s_copy(n2cnam + 1152, "JUPITER", (ftnlen)36, (ftnlen)7);
    n2ccod[33] = 501;
    s_copy(n2cnam + 1188, "IO", (ftnlen)36, (ftnlen)2);
    n2ccod[34] = 502;
    s_copy(n2cnam + 1224, "EUROPA", (ftnlen)36, (ftnlen)6);
    n2ccod[35] = 503;
    s_copy(n2cnam + 1260, "GANYMEDE", (ftnlen)36, (ftnlen)8);
    n2ccod[36] = 504;
    s_copy(n2cnam + 1296, "CALLISTO", (ftnlen)36, (ftnlen)8);
    n2ccod[37] = 505;
    s_copy(n2cnam + 1332, "AMALTHEA", (ftnlen)36, (ftnlen)8);
    n2ccod[38] = 506;
    s_copy(n2cnam + 1368, "HIMALIA", (ftnlen)36, (ftnlen)7);
    n2ccod[39] = 507;
    s_copy(n2cnam + 1404, "ELARA", (ftnlen)36, (ftnlen)5);
    n2ccod[40] = 508;
    s_copy(n2cnam + 1440, "PASIPHAE", (ftnlen)36, (ftnlen)8);
    n2ccod[41] = 509;
    s_copy(n2cnam + 1476, "SINOPE", (ftnlen)36, (ftnlen)6);
    n2ccod[42] = 510;
    s_copy(n2cnam + 1512, "LYSITHEA", (ftnlen)36, (ftnlen)8);
    n2ccod[43] = 511;
    s_copy(n2cnam + 1548, "CARME", (ftnlen)36, (ftnlen)5);
    n2ccod[44] = 512;
    s_copy(n2cnam + 1584, "ANANKE", (ftnlen)36, (ftnlen)6);
    n2ccod[45] = 513;
    s_copy(n2cnam + 1620, "LEDA", (ftnlen)36, (ftnlen)4);
    n2ccod[46] = 514;
    s_copy(n2cnam + 1656, "THEBE", (ftnlen)36, (ftnlen)5);
    n2ccod[47] = 515;
    s_copy(n2cnam + 1692, "ADRASTEA", (ftnlen)36, (ftnlen)8);
    n2ccod[48] = 516;
    s_copy(n2cnam + 1728, "METIS", (ftnlen)36, (ftnlen)5);
    n2ccod[49] = 517;
    s_copy(n2cnam + 1764, "CALLIRRHOE", (ftnlen)36, (ftnlen)10);
    n2ccod[50] = 518;
    s_copy(n2cnam + 1800, "THEMISTO", (ftnlen)36, (ftnlen)8);
    n2ccod[51] = 519;
    s_copy(n2cnam + 1836, "MAGACLITE", (ftnlen)36, (ftnlen)9);
    n2ccod[52] = 520;
    s_copy(n2cnam + 1872, "TAYGETE", (ftnlen)36, (ftnlen)7);
    n2ccod[53] = 521;
    s_copy(n2cnam + 1908, "CHALDENE", (ftnlen)36, (ftnlen)8);
    n2ccod[54] = 522;
    s_copy(n2cnam + 1944, "HARPALYKE", (ftnlen)36, (ftnlen)9);
    n2ccod[55] = 523;
    s_copy(n2cnam + 1980, "KALYKE", (ftnlen)36, (ftnlen)6);
    n2ccod[56] = 524;
    s_copy(n2cnam + 2016, "IOCASTE", (ftnlen)36, (ftnlen)7);
    n2ccod[57] = 525;
    s_copy(n2cnam + 2052, "ERINOME", (ftnlen)36, (ftnlen)7);
    n2ccod[58] = 526;
    s_copy(n2cnam + 2088, "ISONOE", (ftnlen)36, (ftnlen)6);
    n2ccod[59] = 527;
    s_copy(n2cnam + 2124, "PRAXIDIKE", (ftnlen)36, (ftnlen)9);
    n2ccod[60] = 528;
    s_copy(n2cnam + 2160, "AUTONOE", (ftnlen)36, (ftnlen)7);
    n2ccod[61] = 529;
    s_copy(n2cnam + 2196, "THYONE", (ftnlen)36, (ftnlen)6);
    n2ccod[62] = 530;
    s_copy(n2cnam + 2232, "HERMIPPE", (ftnlen)36, (ftnlen)8);
    n2ccod[63] = 531;
    s_copy(n2cnam + 2268, "AITNE", (ftnlen)36, (ftnlen)5);
    n2ccod[64] = 532;
    s_copy(n2cnam + 2304, "EURYDOME", (ftnlen)36, (ftnlen)8);
    n2ccod[65] = 533;
    s_copy(n2cnam + 2340, "EUANTHE", (ftnlen)36, (ftnlen)7);
    n2ccod[66] = 534;
    s_copy(n2cnam + 2376, "EUPORIE", (ftnlen)36, (ftnlen)7);
    n2ccod[67] = 535;
    s_copy(n2cnam + 2412, "ORTHOSIE", (ftnlen)36, (ftnlen)8);
    n2ccod[68] = 536;
    s_copy(n2cnam + 2448, "SPONDE", (ftnlen)36, (ftnlen)6);
    n2ccod[69] = 537;
    s_copy(n2cnam + 2484, "KALE", (ftnlen)36, (ftnlen)4);
    n2ccod[70] = 538;
    s_copy(n2cnam + 2520, "PASITHEE", (ftnlen)36, (ftnlen)8);
    n2ccod[71] = 539;
    s_copy(n2cnam + 2556, "HEGEMONE", (ftnlen)36, (ftnlen)8);
    n2ccod[72] = 540;
    s_copy(n2cnam + 2592, "MNEME", (ftnlen)36, (ftnlen)5);
    n2ccod[73] = 541;
    s_copy(n2cnam + 2628, "AOEDE", (ftnlen)36, (ftnlen)5);
    n2ccod[74] = 542;
    s_copy(n2cnam + 2664, "THELXINOE", (ftnlen)36, (ftnlen)9);
    n2ccod[75] = 543;
    s_copy(n2cnam + 2700, "ARCHE", (ftnlen)36, (ftnlen)5);
    n2ccod[76] = 544;
    s_copy(n2cnam + 2736, "KALLICHORE", (ftnlen)36, (ftnlen)10);
    n2ccod[77] = 545;
    s_copy(n2cnam + 2772, "HELIKE", (ftnlen)36, (ftnlen)6);
    n2ccod[78] = 546;
    s_copy(n2cnam + 2808, "CARPO", (ftnlen)36, (ftnlen)5);
    n2ccod[79] = 547;
    s_copy(n2cnam + 2844, "EUKELADE", (ftnlen)36, (ftnlen)8);
    n2ccod[80] = 548;
    s_copy(n2cnam + 2880, "CYLLENE", (ftnlen)36, (ftnlen)7);
    n2ccod[81] = 549;
    s_copy(n2cnam + 2916, "KORE", (ftnlen)36, (ftnlen)4);
    n2ccod[82] = 550;
    s_copy(n2cnam + 2952, "HERSE", (ftnlen)36, (ftnlen)5);
    n2ccod[83] = 553;
    s_copy(n2cnam + 2988, "DIA", (ftnlen)36, (ftnlen)3);
    n2ccod[84] = 699;
    s_copy(n2cnam + 3024, "SATURN", (ftnlen)36, (ftnlen)6);
    n2ccod[85] = 601;
    s_copy(n2cnam + 3060, "MIMAS", (ftnlen)36, (ftnlen)5);
    n2ccod[86] = 602;
    s_copy(n2cnam + 3096, "ENCELADUS", (ftnlen)36, (ftnlen)9);
    n2ccod[87] = 603;
    s_copy(n2cnam + 3132, "TETHYS", (ftnlen)36, (ftnlen)6);
    n2ccod[88] = 604;
    s_copy(n2cnam + 3168, "DIONE", (ftnlen)36, (ftnlen)5);
    n2ccod[89] = 605;
    s_copy(n2cnam + 3204, "RHEA", (ftnlen)36, (ftnlen)4);
    n2ccod[90] = 606;
    s_copy(n2cnam + 3240, "TITAN", (ftnlen)36, (ftnlen)5);
    n2ccod[91] = 607;
    s_copy(n2cnam + 3276, "HYPERION", (ftnlen)36, (ftnlen)8);
    n2ccod[92] = 608;
    s_copy(n2cnam + 3312, "IAPETUS", (ftnlen)36, (ftnlen)7);
    n2ccod[93] = 609;
    s_copy(n2cnam + 3348, "PHOEBE", (ftnlen)36, (ftnlen)6);
    n2ccod[94] = 610;
    s_copy(n2cnam + 3384, "JANUS", (ftnlen)36, (ftnlen)5);
    n2ccod[95] = 611;
    s_copy(n2cnam + 3420, "EPIMETHEUS", (ftnlen)36, (ftnlen)10);
    n2ccod[96] = 612;
    s_copy(n2cnam + 3456, "HELENE", (ftnlen)36, (ftnlen)6);
    n2ccod[97] = 613;
    s_copy(n2cnam + 3492, "TELESTO", (ftnlen)36, (ftnlen)7);
    n2ccod[98] = 614;
    s_copy(n2cnam + 3528, "CALYPSO", (ftnlen)36, (ftnlen)7);
    n2ccod[99] = 615;
    s_copy(n2cnam + 3564, "ATLAS", (ftnlen)36, (ftnlen)5);
    n2ccod[100] = 616;
    s_copy(n2cnam + 3600, "PROMETHEUS", (ftnlen)36, (ftnlen)10);
    n2ccod[101] = 617;
    s_copy(n2cnam + 3636, "PANDORA", (ftnlen)36, (ftnlen)7);
    n2ccod[102] = 618;
    s_copy(n2cnam + 3672, "PAN", (ftnlen)36, (ftnlen)3);
    n2ccod[103] = 619;
    s_copy(n2cnam + 3708, "YMIR", (ftnlen)36, (ftnlen)4);
    n2ccod[104] = 620;
    s_copy(n2cnam + 3744, "PAALIAQ", (ftnlen)36, (ftnlen)7);
    n2ccod[105] = 621;
    s_copy(n2cnam + 3780, "TARVOS", (ftnlen)36, (ftnlen)6);
    n2ccod[106] = 622;
    s_copy(n2cnam + 3816, "IJIRAQ", (ftnlen)36, (ftnlen)6);
    n2ccod[107] = 623;
    s_copy(n2cnam + 3852, "SUTTUNGR", (ftnlen)36, (ftnlen)8);
    n2ccod[108] = 624;
    s_copy(n2cnam + 3888, "KIVIUQ", (ftnlen)36, (ftnlen)6);
    n2ccod[109] = 625;
    s_copy(n2cnam + 3924, "MUNDILFARI", (ftnlen)36, (ftnlen)10);
    n2ccod[110] = 626;
    s_copy(n2cnam + 3960, "ALBIORIX", (ftnlen)36, (ftnlen)8);
    n2ccod[111] = 627;
    s_copy(n2cnam + 3996, "SKATHI", (ftnlen)36, (ftnlen)6);
    n2ccod[112] = 628;
    s_copy(n2cnam + 4032, "ERRIAPUS", (ftnlen)36, (ftnlen)8);
    n2ccod[113] = 629;
    s_copy(n2cnam + 4068, "SIARNAQ", (ftnlen)36, (ftnlen)7);
    n2ccod[114] = 630;
    s_copy(n2cnam + 4104, "THRYMR", (ftnlen)36, (ftnlen)6);
    n2ccod[115] = 631;
    s_copy(n2cnam + 4140, "NARVI", (ftnlen)36, (ftnlen)5);
    n2ccod[116] = 632;
    s_copy(n2cnam + 4176, "METHONE", (ftnlen)36, (ftnlen)7);
    n2ccod[117] = 633;
    s_copy(n2cnam + 4212, "PALLENE", (ftnlen)36, (ftnlen)7);
    n2ccod[118] = 634;
    s_copy(n2cnam + 4248, "POLYDEUCES", (ftnlen)36, (ftnlen)10);
    n2ccod[119] = 635;
    s_copy(n2cnam + 4284, "DAPHNIS", (ftnlen)36, (ftnlen)7);
    n2ccod[120] = 636;
    s_copy(n2cnam + 4320, "AEGIR", (ftnlen)36, (ftnlen)5);
    n2ccod[121] = 637;
    s_copy(n2cnam + 4356, "BEBHIONN", (ftnlen)36, (ftnlen)8);
    n2ccod[122] = 638;
    s_copy(n2cnam + 4392, "BERGELMIR", (ftnlen)36, (ftnlen)9);
    n2ccod[123] = 639;
    s_copy(n2cnam + 4428, "BESTLA", (ftnlen)36, (ftnlen)6);
    n2ccod[124] = 640;
    s_copy(n2cnam + 4464, "FARBAUTI", (ftnlen)36, (ftnlen)8);
    n2ccod[125] = 641;
    s_copy(n2cnam + 4500, "FENRIR", (ftnlen)36, (ftnlen)6);
    n2ccod[126] = 642;
    s_copy(n2cnam + 4536, "FORNJOT", (ftnlen)36, (ftnlen)7);
    n2ccod[127] = 643;
    s_copy(n2cnam + 4572, "HATI", (ftnlen)36, (ftnlen)4);
    n2ccod[128] = 644;
    s_copy(n2cnam + 4608, "HYRROKKIN", (ftnlen)36, (ftnlen)9);
    n2ccod[129] = 645;
    s_copy(n2cnam + 4644, "KARI", (ftnlen)36, (ftnlen)4);
    n2ccod[130] = 646;
    s_copy(n2cnam + 4680, "LOGE", (ftnlen)36, (ftnlen)4);
    n2ccod[131] = 647;
    s_copy(n2cnam + 4716, "SKOLL", (ftnlen)36, (ftnlen)5);
    n2ccod[132] = 648;
    s_copy(n2cnam + 4752, "SURTUR", (ftnlen)36, (ftnlen)6);
    n2ccod[133] = 649;
    s_copy(n2cnam + 4788, "ANTHE", (ftnlen)36, (ftnlen)5);
    n2ccod[134] = 650;
    s_copy(n2cnam + 4824, "JARNSAXA", (ftnlen)36, (ftnlen)8);
    n2ccod[135] = 651;
    s_copy(n2cnam + 4860, "GREIP", (ftnlen)36, (ftnlen)5);
    n2ccod[136] = 652;
    s_copy(n2cnam + 4896, "TARQEQ", (ftnlen)36, (ftnlen)6);
    n2ccod[137] = 653;
    s_copy(n2cnam + 4932, "AEGAEON", (ftnlen)36, (ftnlen)7);
    n2ccod[138] = 799;
    s_copy(n2cnam + 4968, "URANUS", (ftnlen)36, (ftnlen)6);
    n2ccod[139] = 701;
    s_copy(n2cnam + 5004, "ARIEL", (ftnlen)36, (ftnlen)5);
    n2ccod[140] = 702;
    s_copy(n2cnam + 5040, "UMBRIEL", (ftnlen)36, (ftnlen)7);
    n2ccod[141] = 703;
    s_copy(n2cnam + 5076, "TITANIA", (ftnlen)36, (ftnlen)7);
    n2ccod[142] = 704;
    s_copy(n2cnam + 5112, "OBERON", (ftnlen)36, (ftnlen)6);
    n2ccod[143] = 705;
    s_copy(n2cnam + 5148, "MIRANDA", (ftnlen)36, (ftnlen)7);
    n2ccod[144] = 706;
    s_copy(n2cnam + 5184, "CORDELIA", (ftnlen)36, (ftnlen)8);
    n2ccod[145] = 707;
    s_copy(n2cnam + 5220, "OPHELIA", (ftnlen)36, (ftnlen)7);
    n2ccod[146] = 708;
    s_copy(n2cnam + 5256, "BIANCA", (ftnlen)36, (ftnlen)6);
    n2ccod[147] = 709;
    s_copy(n2cnam + 5292, "CRESSIDA", (ftnlen)36, (ftnlen)8);
    n2ccod[148] = 710;
    s_copy(n2cnam + 5328, "DESDEMONA", (ftnlen)36, (ftnlen)9);
    n2ccod[149] = 711;
    s_copy(n2cnam + 5364, "JULIET", (ftnlen)36, (ftnlen)6);
    n2ccod[150] = 712;
    s_copy(n2cnam + 5400, "PORTIA", (ftnlen)36, (ftnlen)6);
    n2ccod[151] = 713;
    s_copy(n2cnam + 5436, "ROSALIND", (ftnlen)36, (ftnlen)8);
    n2ccod[152] = 714;
    s_copy(n2cnam + 5472, "BELINDA", (ftnlen)36, (ftnlen)7);
    n2ccod[153] = 715;
    s_copy(n2cnam + 5508, "PUCK", (ftnlen)36, (ftnlen)4);
    n2ccod[154] = 716;
    s_copy(n2cnam + 5544, "CALIBAN", (ftnlen)36, (ftnlen)7);
    n2ccod[155] = 717;
    s_copy(n2cnam + 5580, "SYCORAX", (ftnlen)36, (ftnlen)7);
    n2ccod[156] = 718;
    s_copy(n2cnam + 5616, "PROSPERO", (ftnlen)36, (ftnlen)8);
    n2ccod[157] = 719;
    s_copy(n2cnam + 5652, "SETEBOS", (ftnlen)36, (ftnlen)7);
    n2ccod[158] = 720;
    s_copy(n2cnam + 5688, "STEPHANO", (ftnlen)36, (ftnlen)8);
    n2ccod[159] = 721;
    s_copy(n2cnam + 5724, "TRINCULO", (ftnlen)36, (ftnlen)8);
    n2ccod[160] = 722;
    s_copy(n2cnam + 5760, "FRANCISCO", (ftnlen)36, (ftnlen)9);
    n2ccod[161] = 723;
    s_copy(n2cnam + 5796, "MARGARET", (ftnlen)36, (ftnlen)8);
    n2ccod[162] = 724;
    s_copy(n2cnam + 5832, "FERDINAND", (ftnlen)36, (ftnlen)9);
    n2ccod[163] = 725;
    s_copy(n2cnam + 5868, "PERDITA", (ftnlen)36, (ftnlen)7);
    n2ccod[164] = 726;
    s_copy(n2cnam + 5904, "MAB", (ftnlen)36, (ftnlen)3);
    n2ccod[165] = 727;
    s_copy(n2cnam + 5940, "CUPID", (ftnlen)36, (ftnlen)5);
    n2ccod[166] = 899;
    s_copy(n2cnam + 5976, "NEPTUNE", (ftnlen)36, (ftnlen)7);
    n2ccod[167] = 801;
    s_copy(n2cnam + 6012, "TRITON", (ftnlen)36, (ftnlen)6);
    n2ccod[168] = 802;
    s_copy(n2cnam + 6048, "NEREID", (ftnlen)36, (ftnlen)6);
    n2ccod[169] = 803;
    s_copy(n2cnam + 6084, "NAIAD", (ftnlen)36, (ftnlen)5);
    n2ccod[170] = 804;
    s_copy(n2cnam + 6120, "THALASSA", (ftnlen)36, (ftnlen)8);
    n2ccod[171] = 805;
    s_copy(n2cnam + 6156, "DESPINA", (ftnlen)36, (ftnlen)7);
    n2ccod[172] = 806;
    s_copy(n2cnam + 6192, "GALATEA", (ftnlen)36, (ftnlen)7);
    n2ccod[173] = 807;
    s_copy(n2cnam + 6228, "LARISSA", (ftnlen)36, (ftnlen)7);
    n2ccod[174] = 808;
    s_copy(n2cnam + 6264, "PROTEUS", (ftnlen)36, (ftnlen)7);
    n2ccod[175] = 809;
    s_copy(n2cnam + 6300, "HALIMEDE", (ftnlen)36, (ftnlen)8);
    n2ccod[176] = 810;
    s_copy(n2cnam + 6336, "PSAMATHE", (ftnlen)36, (ftnlen)8);
    n2ccod[177] = 811;
    s_copy(n2cnam + 6372, "SAO", (ftnlen)36, (ftnlen)3);
    n2ccod[178] = 812;
    s_copy(n2cnam + 6408, "LAOMEDEIA", (ftnlen)36, (ftnlen)9);
    n2ccod[179] = 813;
    s_copy(n2cnam + 6444, "NESO", (ftnlen)36, (ftnlen)4);
    n2ccod[180] = 999;
    s_copy(n2cnam + 6480, "PLUTO", (ftnlen)36, (ftnlen)5);
    n2ccod[181] = 901;
    s_copy(n2cnam + 6516, "CHARON", (ftnlen)36, (ftnlen)6);
    n2ccod[182] = 902;
    s_copy(n2cnam + 6552, "NIX", (ftnlen)36, (ftnlen)3);
    n2ccod[183] = 903;
    s_copy(n2cnam + 6588, "HYDRA", (ftnlen)36, (ftnlen)5);
    n2ccod[184] = 904;
    s_copy(n2cnam + 6624, "KERBEROS", (ftnlen)36, (ftnlen)8);
    n2ccod[185] = 905;
    s_copy(n2cnam + 6660, "STYX", (ftnlen)36, (ftnlen)4);
    n2ccod[186] = -1;
    s_copy(n2cnam + 6696, "GEOTAIL", (ftnlen)36, (ftnlen)7);
    n2ccod[187] = -3;
    s_copy(n2cnam + 6732, "MOM", (ftnlen)36, (ftnlen)3);
    n2ccod[188] = -3;
    s_copy(n2cnam + 6768, "MARS ORBITER MISSION", (ftnlen)36, (ftnlen)20);
    n2ccod[189] = -5;
    s_copy(n2cnam + 6804, "AKATSUKI", (ftnlen)36, (ftnlen)8);
    n2ccod[190] = -5;
    s_copy(n2cnam + 6840, "VCO", (ftnlen)36, (ftnlen)3);
    n2ccod[191] = -5;
    s_copy(n2cnam + 6876, "PLC", (ftnlen)36, (ftnlen)3);
    n2ccod[192] = -5;
    s_copy(n2cnam + 6912, "PLANET-C", (ftnlen)36, (ftnlen)8);
    n2ccod[193] = -6;
    s_copy(n2cnam + 6948, "P6", (ftnlen)36, (ftnlen)2);
    n2ccod[194] = -6;
    s_copy(n2cnam + 6984, "PIONEER-6", (ftnlen)36, (ftnlen)9);
    n2ccod[195] = -7;
    s_copy(n2cnam + 7020, "P7", (ftnlen)36, (ftnlen)2);
    n2ccod[196] = -7;
    s_copy(n2cnam + 7056, "PIONEER-7", (ftnlen)36, (ftnlen)9);
    n2ccod[197] = -8;
    s_copy(n2cnam + 7092, "WIND", (ftnlen)36, (ftnlen)4);
    n2ccod[198] = -12;
    s_copy(n2cnam + 7128, "VENUS ORBITER", (ftnlen)36, (ftnlen)13);
    n2ccod[199] = -12;
    s_copy(n2cnam + 7164, "P12", (ftnlen)36, (ftnlen)3);
    n2ccod[200] = -12;
    s_copy(n2cnam + 7200, "PIONEER 12", (ftnlen)36, (ftnlen)10);
    n2ccod[201] = -12;
    s_copy(n2cnam + 7236, "LADEE", (ftnlen)36, (ftnlen)5);
    n2ccod[202] = -13;
    s_copy(n2cnam + 7272, "POLAR", (ftnlen)36, (ftnlen)5);
    n2ccod[203] = -18;
    s_copy(n2cnam + 7308, "MGN", (ftnlen)36, (ftnlen)3);
    n2ccod[204] = -18;
    s_copy(n2cnam + 7344, "MAGELLAN", (ftnlen)36, (ftnlen)8);
    n2ccod[205] = -18;
    s_copy(n2cnam + 7380, "LCROSS", (ftnlen)36, (ftnlen)6);
    n2ccod[206] = -20;
    s_copy(n2cnam + 7416, "P8", (ftnlen)36, (ftnlen)2);
    n2ccod[207] = -20;
    s_copy(n2cnam + 7452, "PIONEER-8", (ftnlen)36, (ftnlen)9);
    n2ccod[208] = -21;
    s_copy(n2cnam + 7488, "SOHO", (ftnlen)36, (ftnlen)4);
    n2ccod[209] = -23;
    s_copy(n2cnam + 7524, "P10", (ftnlen)36, (ftnlen)3);
    n2ccod[210] = -23;
    s_copy(n2cnam + 7560, "PIONEER-10", (ftnlen)36, (ftnlen)10);
    n2ccod[211] = -24;
    s_copy(n2cnam + 7596, "P11", (ftnlen)36, (ftnlen)3);
    n2ccod[212] = -24;
    s_copy(n2cnam + 7632, "PIONEER-11", (ftnlen)36, (ftnlen)10);
    n2ccod[213] = -25;
    s_copy(n2cnam + 7668, "LP", (ftnlen)36, (ftnlen)2);
    n2ccod[214] = -25;
    s_copy(n2cnam + 7704, "LUNAR PROSPECTOR", (ftnlen)36, (ftnlen)16);
    n2ccod[215] = -27;
    s_copy(n2cnam + 7740, "VK1", (ftnlen)36, (ftnlen)3);
    n2ccod[216] = -27;
    s_copy(n2cnam + 7776, "VIKING 1 ORBITER", (ftnlen)36, (ftnlen)16);
    n2ccod[217] = -28;
    s_copy(n2cnam + 7812, "JUPITER ICY MOONS EXPLORER", (ftnlen)36, (ftnlen)
	    26);
    n2ccod[218] = -28;
    s_copy(n2cnam + 7848, "JUICE", (ftnlen)36, (ftnlen)5);
    n2ccod[219] = -29;
    s_copy(n2cnam + 7884, "STARDUST", (ftnlen)36, (ftnlen)8);
    n2ccod[220] = -29;
    s_copy(n2cnam + 7920, "SDU", (ftnlen)36, (ftnlen)3);
    n2ccod[221] = -29;
    s_copy(n2cnam + 7956, "NEXT", (ftnlen)36, (ftnlen)4);
    n2ccod[222] = -30;
    s_copy(n2cnam + 7992, "VK2", (ftnlen)36, (ftnlen)3);
    n2ccod[223] = -30;
    s_copy(n2cnam + 8028, "VIKING 2 ORBITER", (ftnlen)36, (ftnlen)16);
    n2ccod[224] = -30;
    s_copy(n2cnam + 8064, "DS-1", (ftnlen)36, (ftnlen)4);
    n2ccod[225] = -31;
    s_copy(n2cnam + 8100, "VG1", (ftnlen)36, (ftnlen)3);
    n2ccod[226] = -31;
    s_copy(n2cnam + 8136, "VOYAGER 1", (ftnlen)36, (ftnlen)9);
    n2ccod[227] = -32;
    s_copy(n2cnam + 8172, "VG2", (ftnlen)36, (ftnlen)3);
    n2ccod[228] = -32;
    s_copy(n2cnam + 8208, "VOYAGER 2", (ftnlen)36, (ftnlen)9);
    n2ccod[229] = -40;
    s_copy(n2cnam + 8244, "CLEMENTINE", (ftnlen)36, (ftnlen)10);
    n2ccod[230] = -41;
    s_copy(n2cnam + 8280, "MEX", (ftnlen)36, (ftnlen)3);
    n2ccod[231] = -41;
    s_copy(n2cnam + 8316, "MARS EXPRESS", (ftnlen)36, (ftnlen)12);
    n2ccod[232] = -44;
    s_copy(n2cnam + 8352, "BEAGLE2", (ftnlen)36, (ftnlen)7);
    n2ccod[233] = -44;
    s_copy(n2cnam + 8388, "BEAGLE 2", (ftnlen)36, (ftnlen)8);
    n2ccod[234] = -46;
    s_copy(n2cnam + 8424, "MS-T5", (ftnlen)36, (ftnlen)5);
    n2ccod[235] = -46;
    s_copy(n2cnam + 8460, "SAKIGAKE", (ftnlen)36, (ftnlen)8);
    n2ccod[236] = -47;
    s_copy(n2cnam + 8496, "PLANET-A", (ftnlen)36, (ftnlen)8);
    n2ccod[237] = -47;
    s_copy(n2cnam + 8532, "SUISEI", (ftnlen)36, (ftnlen)6);
    n2ccod[238] = -47;
    s_copy(n2cnam + 8568, "GNS", (ftnlen)36, (ftnlen)3);
    n2ccod[239] = -47;
    s_copy(n2cnam + 8604, "GENESIS", (ftnlen)36, (ftnlen)7);
    n2ccod[240] = -48;
    s_copy(n2cnam + 8640, "HUBBLE SPACE TELESCOPE", (ftnlen)36, (ftnlen)22);
    n2ccod[241] = -48;
    s_copy(n2cnam + 8676, "HST", (ftnlen)36, (ftnlen)3);
    n2ccod[242] = -49;
    s_copy(n2cnam + 8712, "LUCY", (ftnlen)36, (ftnlen)4);
    n2ccod[243] = -53;
    s_copy(n2cnam + 8748, "MARS PATHFINDER", (ftnlen)36, (ftnlen)15);
    n2ccod[244] = -53;
    s_copy(n2cnam + 8784, "MPF", (ftnlen)36, (ftnlen)3);
    n2ccod[245] = -53;
    s_copy(n2cnam + 8820, "MARS ODYSSEY", (ftnlen)36, (ftnlen)12);
    n2ccod[246] = -53;
    s_copy(n2cnam + 8856, "MARS SURVEYOR 01 ORBITER", (ftnlen)36, (ftnlen)24);
    n2ccod[247] = -54;
    s_copy(n2cnam + 8892, "ARM", (ftnlen)36, (ftnlen)3);
    n2ccod[248] = -54;
    s_copy(n2cnam + 8928, "ASTEROID RETRIEVAL MISSION", (ftnlen)36, (ftnlen)
	    26);
    n2ccod[249] = -55;
    s_copy(n2cnam + 8964, "ULYSSES", (ftnlen)36, (ftnlen)7);
    n2ccod[250] = -58;
    s_copy(n2cnam + 9000, "VSOP", (ftnlen)36, (ftnlen)4);
    n2ccod[251] = -58;
    s_copy(n2cnam + 9036, "HALCA", (ftnlen)36, (ftnlen)5);
    n2ccod[252] = -59;
    s_copy(n2cnam + 9072, "RADIOASTRON", (ftnlen)36, (ftnlen)11);
    n2ccod[253] = -61;
    s_copy(n2cnam + 9108, "JUNO", (ftnlen)36, (ftnlen)4);
    n2ccod[254] = -62;
    s_copy(n2cnam + 9144, "EMM", (ftnlen)36, (ftnlen)3);
    n2ccod[255] = -62;
    s_copy(n2cnam + 9180, "EMIRATES MARS MISSION", (ftnlen)36, (ftnlen)21);
    n2ccod[256] = -64;
    s_copy(n2cnam + 9216, "ORX", (ftnlen)36, (ftnlen)3);
    n2ccod[257] = -64;
    s_copy(n2cnam + 9252, "OSIRIS-REX", (ftnlen)36, (ftnlen)10);
    n2ccod[258] = -65;
    s_copy(n2cnam + 9288, "MCOA", (ftnlen)36, (ftnlen)4);
    n2ccod[259] = -65;
    s_copy(n2cnam + 9324, "MARCO-A", (ftnlen)36, (ftnlen)7);
    n2ccod[260] = -66;
    s_copy(n2cnam + 9360, "VEGA 1", (ftnlen)36, (ftnlen)6);
    n2ccod[261] = -66;
    s_copy(n2cnam + 9396, "MCOB", (ftnlen)36, (ftnlen)4);
    n2ccod[262] = -66;
    s_copy(n2cnam + 9432, "MARCO-B", (ftnlen)36, (ftnlen)7);
    n2ccod[263] = -67;
    s_copy(n2cnam + 9468, "VEGA 2", (ftnlen)36, (ftnlen)6);
    n2ccod[264] = -68;
    s_copy(n2cnam + 9504, "MERCURY MAGNETOSPHERIC ORBITER", (ftnlen)36, (
	    ftnlen)30);
    n2ccod[265] = -68;
    s_copy(n2cnam + 9540, "MMO", (ftnlen)36, (ftnlen)3);
    n2ccod[266] = -68;
    s_copy(n2cnam + 9576, "BEPICOLOMBO MMO", (ftnlen)36, (ftnlen)15);
    n2ccod[267] = -69;
    s_copy(n2cnam + 9612, "PSYC", (ftnlen)36, (ftnlen)4);
    n2ccod[268] = -70;
    s_copy(n2cnam + 9648, "DEEP IMPACT IMPACTOR SPACECRAFT", (ftnlen)36, (
	    ftnlen)31);
    n2ccod[269] = -74;
    s_copy(n2cnam + 9684, "MRO", (ftnlen)36, (ftnlen)3);
    n2ccod[270] = -74;
    s_copy(n2cnam + 9720, "MARS RECON ORBITER", (ftnlen)36, (ftnlen)18);
    n2ccod[271] = -76;
    s_copy(n2cnam + 9756, "CURIOSITY", (ftnlen)36, (ftnlen)9);
    n2ccod[272] = -76;
    s_copy(n2cnam + 9792, "MSL", (ftnlen)36, (ftnlen)3);
    n2ccod[273] = -76;
    s_copy(n2cnam + 9828, "MARS SCIENCE LABORATORY", (ftnlen)36, (ftnlen)23);
    n2ccod[274] = -77;
    s_copy(n2cnam + 9864, "GLL", (ftnlen)36, (ftnlen)3);
    n2ccod[275] = -77;
    s_copy(n2cnam + 9900, "GALILEO ORBITER", (ftnlen)36, (ftnlen)15);
    n2ccod[276] = -78;
    s_copy(n2cnam + 9936, "GIOTTO", (ftnlen)36, (ftnlen)6);
    n2ccod[277] = -79;
    s_copy(n2cnam + 9972, "SPITZER", (ftnlen)36, (ftnlen)7);
    n2ccod[278] = -79;
    s_copy(n2cnam + 10008, "SPACE INFRARED TELESCOPE FACILITY", (ftnlen)36, (
	    ftnlen)33);
    n2ccod[279] = -79;
    s_copy(n2cnam + 10044, "SIRTF", (ftnlen)36, (ftnlen)5);
    n2ccod[280] = -81;
    s_copy(n2cnam + 10080, "CASSINI ITL", (ftnlen)36, (ftnlen)11);
    n2ccod[281] = -82;
    s_copy(n2cnam + 10116, "CAS", (ftnlen)36, (ftnlen)3);
    n2ccod[282] = -82;
    s_copy(n2cnam + 10152, "CASSINI", (ftnlen)36, (ftnlen)7);
    n2ccod[283] = -84;
    s_copy(n2cnam + 10188, "PHOENIX", (ftnlen)36, (ftnlen)7);
    n2ccod[284] = -85;
    s_copy(n2cnam + 10224, "LRO", (ftnlen)36, (ftnlen)3);
    n2ccod[285] = -85;
    s_copy(n2cnam + 10260, "LUNAR RECON ORBITER", (ftnlen)36, (ftnlen)19);
    n2ccod[286] = -85;
    s_copy(n2cnam + 10296, "LUNAR RECONNAISSANCE ORBITER", (ftnlen)36, (
	    ftnlen)28);
    n2ccod[287] = -86;
    s_copy(n2cnam + 10332, "CH1", (ftnlen)36, (ftnlen)3);
    n2ccod[288] = -86;
    s_copy(n2cnam + 10368, "CHANDRAYAAN-1", (ftnlen)36, (ftnlen)13);
    n2ccod[289] = -90;
    s_copy(n2cnam + 10404, "CASSINI SIMULATION", (ftnlen)36, (ftnlen)18);
    n2ccod[290] = -93;
    s_copy(n2cnam + 10440, "NEAR EARTH ASTEROID RENDEZVOUS", (ftnlen)36, (
	    ftnlen)30);
    n2ccod[291] = -93;
    s_copy(n2cnam + 10476, "NEAR", (ftnlen)36, (ftnlen)4);
    n2ccod[292] = -94;
    s_copy(n2cnam + 10512, "MO", (ftnlen)36, (ftnlen)2);
    n2ccod[293] = -94;
    s_copy(n2cnam + 10548, "MARS OBSERVER", (ftnlen)36, (ftnlen)13);
    n2ccod[294] = -94;
    s_copy(n2cnam + 10584, "MGS", (ftnlen)36, (ftnlen)3);
    n2ccod[295] = -94;
    s_copy(n2cnam + 10620, "MARS GLOBAL SURVEYOR", (ftnlen)36, (ftnlen)20);
    n2ccod[296] = -95;
    s_copy(n2cnam + 10656, "MGS SIMULATION", (ftnlen)36, (ftnlen)14);
    n2ccod[297] = -96;
    s_copy(n2cnam + 10692, "SPP", (ftnlen)36, (ftnlen)3);
    n2ccod[298] = -96;
    s_copy(n2cnam + 10728, "SOLAR PROBE PLUS", (ftnlen)36, (ftnlen)16);
    n2ccod[299] = -97;
    s_copy(n2cnam + 10764, "TOPEX/POSEIDON", (ftnlen)36, (ftnlen)14);
    n2ccod[300] = -98;
    s_copy(n2cnam + 10800, "NEW HORIZONS", (ftnlen)36, (ftnlen)12);
    n2ccod[301] = -107;
    s_copy(n2cnam + 10836, "TROPICAL RAINFALL MEASURING MISSION", (ftnlen)36, 
	    (ftnlen)35);
    n2ccod[302] = -107;
    s_copy(n2cnam + 10872, "TRMM", (ftnlen)36, (ftnlen)4);
    n2ccod[303] = -112;
    s_copy(n2cnam + 10908, "ICE", (ftnlen)36, (ftnlen)3);
    n2ccod[304] = -116;
    s_copy(n2cnam + 10944, "MARS POLAR LANDER", (ftnlen)36, (ftnlen)17);
    n2ccod[305] = -116;
    s_copy(n2cnam + 10980, "MPL", (ftnlen)36, (ftnlen)3);
    n2ccod[306] = -117;
    s_copy(n2cnam + 11016, "EDL DEMONSTRATOR MODULE", (ftnlen)36, (ftnlen)23);
    n2ccod[307] = -117;
    s_copy(n2cnam + 11052, "EDM", (ftnlen)36, (ftnlen)3);
    n2ccod[308] = -117;
    s_copy(n2cnam + 11088, "EXOMARS 2016 EDM", (ftnlen)36, (ftnlen)16);
    n2ccod[309] = -121;
    s_copy(n2cnam + 11124, "MERCURY PLANETARY ORBITER", (ftnlen)36, (ftnlen)
	    25);
    n2ccod[310] = -121;
    s_copy(n2cnam + 11160, "MPO", (ftnlen)36, (ftnlen)3);
    n2ccod[311] = -121;
    s_copy(n2cnam + 11196, "BEPICOLOMBO MPO", (ftnlen)36, (ftnlen)15);
    n2ccod[312] = -127;
    s_copy(n2cnam + 11232, "MARS CLIMATE ORBITER", (ftnlen)36, (ftnlen)20);
    n2ccod[313] = -127;
    s_copy(n2cnam + 11268, "MCO", (ftnlen)36, (ftnlen)3);
    n2ccod[314] = -130;
    s_copy(n2cnam + 11304, "MUSES-C", (ftnlen)36, (ftnlen)7);
    n2ccod[315] = -130;
    s_copy(n2cnam + 11340, "HAYABUSA", (ftnlen)36, (ftnlen)8);
    n2ccod[316] = -131;
    s_copy(n2cnam + 11376, "SELENE", (ftnlen)36, (ftnlen)6);
    n2ccod[317] = -131;
    s_copy(n2cnam + 11412, "KAGUYA", (ftnlen)36, (ftnlen)6);
    n2ccod[318] = -135;
    s_copy(n2cnam + 11448, "DRTS-W", (ftnlen)36, (ftnlen)6);
    n2ccod[319] = -140;
    s_copy(n2cnam + 11484, "EPOCH", (ftnlen)36, (ftnlen)5);
    n2ccod[320] = -140;
    s_copy(n2cnam + 11520, "DIXI", (ftnlen)36, (ftnlen)4);
    n2ccod[321] = -140;
    s_copy(n2cnam + 11556, "EPOXI", (ftnlen)36, (ftnlen)5);
    n2ccod[322] = -140;
    s_copy(n2cnam + 11592, "DEEP IMPACT FLYBY SPACECRAFT", (ftnlen)36, (
	    ftnlen)28);
    n2ccod[323] = -142;
    s_copy(n2cnam + 11628, "TERRA", (ftnlen)36, (ftnlen)5);
    n2ccod[324] = -142;
    s_copy(n2cnam + 11664, "EOS-AM1", (ftnlen)36, (ftnlen)7);
    n2ccod[325] = -143;
    s_copy(n2cnam + 11700, "TRACE GAS ORBITER", (ftnlen)36, (ftnlen)17);
    n2ccod[326] = -143;
    s_copy(n2cnam + 11736, "TGO", (ftnlen)36, (ftnlen)3);
    n2ccod[327] = -143;
    s_copy(n2cnam + 11772, "EXOMARS 2016 TGO", (ftnlen)36, (ftnlen)16);
    n2ccod[328] = -144;
    s_copy(n2cnam + 11808, "SOLO", (ftnlen)36, (ftnlen)4);
    n2ccod[329] = -144;
    s_copy(n2cnam + 11844, "SOLAR ORBITER", (ftnlen)36, (ftnlen)13);
    n2ccod[330] = -146;
    s_copy(n2cnam + 11880, "LUNAR-A", (ftnlen)36, (ftnlen)7);
    n2ccod[331] = -150;
    s_copy(n2cnam + 11916, "CASSINI PROBE", (ftnlen)36, (ftnlen)13);
    n2ccod[332] = -150;
    s_copy(n2cnam + 11952, "HUYGENS PROBE", (ftnlen)36, (ftnlen)13);
    n2ccod[333] = -150;
    s_copy(n2cnam + 11988, "CASP", (ftnlen)36, (ftnlen)4);
    n2ccod[334] = -151;
    s_copy(n2cnam + 12024, "AXAF", (ftnlen)36, (ftnlen)4);
    n2ccod[335] = -151;
    s_copy(n2cnam + 12060, "CHANDRA", (ftnlen)36, (ftnlen)7);
    n2ccod[336] = -152;
    s_copy(n2cnam + 12096, "CH2", (ftnlen)36, (ftnlen)3);
    n2ccod[337] = -152;
    s_copy(n2cnam + 12132, "CHANDRAYAAN-2", (ftnlen)36, (ftnlen)13);
    n2ccod[338] = -154;
    s_copy(n2cnam + 12168, "AQUA", (ftnlen)36, (ftnlen)4);
    n2ccod[339] = -159;
    s_copy(n2cnam + 12204, "EURC", (ftnlen)36, (ftnlen)4);
    n2ccod[340] = -159;
    s_copy(n2cnam + 12240, "EUROPA CLIPPER", (ftnlen)36, (ftnlen)14);
    n2ccod[341] = -164;
    s_copy(n2cnam + 12276, "YOHKOH", (ftnlen)36, (ftnlen)6);
    n2ccod[342] = -164;
    s_copy(n2cnam + 12312, "SOLAR-A", (ftnlen)36, (ftnlen)7);
    n2ccod[343] = -165;
    s_copy(n2cnam + 12348, "MAP", (ftnlen)36, (ftnlen)3);
    n2ccod[344] = -166;
    s_copy(n2cnam + 12384, "IMAGE", (ftnlen)36, (ftnlen)5);
    n2ccod[345] = -170;
    s_copy(n2cnam + 12420, "JWST", (ftnlen)36, (ftnlen)4);
    n2ccod[346] = -170;
    s_copy(n2cnam + 12456, "JAMES WEBB SPACE TELESCOPE", (ftnlen)36, (ftnlen)
	    26);
    n2ccod[347] = -177;
    s_copy(n2cnam + 12492, "GRAIL-A", (ftnlen)36, (ftnlen)7);
    n2ccod[348] = -178;
    s_copy(n2cnam + 12528, "PLANET-B", (ftnlen)36, (ftnlen)8);
    n2ccod[349] = -178;
    s_copy(n2cnam + 12564, "NOZOMI", (ftnlen)36, (ftnlen)6);
    n2ccod[350] = -181;
    s_copy(n2cnam + 12600, "GRAIL-B", (ftnlen)36, (ftnlen)7);
    n2ccod[351] = -183;
    s_copy(n2cnam + 12636, "CLUSTER 1", (ftnlen)36, (ftnlen)9);
    n2ccod[352] = -185;
    s_copy(n2cnam + 12672, "CLUSTER 2", (ftnlen)36, (ftnlen)9);
    n2ccod[353] = -188;
    s_copy(n2cnam + 12708, "MUSES-B", (ftnlen)36, (ftnlen)7);
    n2ccod[354] = -189;
    s_copy(n2cnam + 12744, "NSYT", (ftnlen)36, (ftnlen)4);
    n2ccod[355] = -189;
    s_copy(n2cnam + 12780, "INSIGHT", (ftnlen)36, (ftnlen)7);
    n2ccod[356] = -190;
    s_copy(n2cnam + 12816, "SIM", (ftnlen)36, (ftnlen)3);
    n2ccod[357] = -194;
    s_copy(n2cnam + 12852, "CLUSTER 3", (ftnlen)36, (ftnlen)9);
    n2ccod[358] = -196;
    s_copy(n2cnam + 12888, "CLUSTER 4", (ftnlen)36, (ftnlen)9);
    n2ccod[359] = -198;
    s_copy(n2cnam + 12924, "INTEGRAL", (ftnlen)36, (ftnlen)8);
    n2ccod[360] = -198;
    s_copy(n2cnam + 12960, "NASA-ISRO SAR MISSION", (ftnlen)36, (ftnlen)21);
    n2ccod[361] = -198;
    s_copy(n2cnam + 12996, "NISAR", (ftnlen)36, (ftnlen)5);
    n2ccod[362] = -200;
    s_copy(n2cnam + 13032, "CONTOUR", (ftnlen)36, (ftnlen)7);
    n2ccod[363] = -202;
    s_copy(n2cnam + 13068, "MAVEN", (ftnlen)36, (ftnlen)5);
    n2ccod[364] = -203;
    s_copy(n2cnam + 13104, "DAWN", (ftnlen)36, (ftnlen)4);
    n2ccod[365] = -205;
    s_copy(n2cnam + 13140, "SOIL MOISTURE ACTIVE AND PASSIVE", (ftnlen)36, (
	    ftnlen)32);
    n2ccod[366] = -205;
    s_copy(n2cnam + 13176, "SMAP", (ftnlen)36, (ftnlen)4);
    n2ccod[367] = -212;
    s_copy(n2cnam + 13212, "STV51", (ftnlen)36, (ftnlen)5);
    n2ccod[368] = -213;
    s_copy(n2cnam + 13248, "STV52", (ftnlen)36, (ftnlen)5);
    n2ccod[369] = -214;
    s_copy(n2cnam + 13284, "STV53", (ftnlen)36, (ftnlen)5);
    n2ccod[370] = -226;
    s_copy(n2cnam + 13320, "ROSETTA", (ftnlen)36, (ftnlen)7);
    n2ccod[371] = -227;
    s_copy(n2cnam + 13356, "KEPLER", (ftnlen)36, (ftnlen)6);
    n2ccod[372] = -228;
    s_copy(n2cnam + 13392, "GLL PROBE", (ftnlen)36, (ftnlen)9);
    n2ccod[373] = -228;
    s_copy(n2cnam + 13428, "GALILEO PROBE", (ftnlen)36, (ftnlen)13);
    n2ccod[374] = -234;
    s_copy(n2cnam + 13464, "STEREO AHEAD", (ftnlen)36, (ftnlen)12);
    n2ccod[375] = -235;
    s_copy(n2cnam + 13500, "STEREO BEHIND", (ftnlen)36, (ftnlen)13);
    n2ccod[376] = -236;
    s_copy(n2cnam + 13536, "MESSENGER", (ftnlen)36, (ftnlen)9);
    n2ccod[377] = -238;
    s_copy(n2cnam + 13572, "SMART1", (ftnlen)36, (ftnlen)6);
    n2ccod[378] = -238;
    s_copy(n2cnam + 13608, "SM1", (ftnlen)36, (ftnlen)3);
    n2ccod[379] = -238;
    s_copy(n2cnam + 13644, "S1", (ftnlen)36, (ftnlen)2);
    n2ccod[380] = -238;
    s_copy(n2cnam + 13680, "SMART-1", (ftnlen)36, (ftnlen)7);
    n2ccod[381] = -248;
    s_copy(n2cnam + 13716, "VEX", (ftnlen)36, (ftnlen)3);
    n2ccod[382] = -248;
    s_copy(n2cnam + 13752, "VENUS EXPRESS", (ftnlen)36, (ftnlen)13);
    n2ccod[383] = -253;
    s_copy(n2cnam + 13788, "OPPORTUNITY", (ftnlen)36, (ftnlen)11);
    n2ccod[384] = -253;
    s_copy(n2cnam + 13824, "MER-1", (ftnlen)36, (ftnlen)5);
    n2ccod[385] = -254;
    s_copy(n2cnam + 13860, "SPIRIT", (ftnlen)36, (ftnlen)6);
    n2ccod[386] = -254;
    s_copy(n2cnam + 13896, "MER-2", (ftnlen)36, (ftnlen)5);
    n2ccod[387] = -301;
    s_copy(n2cnam + 13932, "HELIOS 1", (ftnlen)36, (ftnlen)8);
    n2ccod[388] = -302;
    s_copy(n2cnam + 13968, "HELIOS 2", (ftnlen)36, (ftnlen)8);
    n2ccod[389] = -362;
    s_copy(n2cnam + 14004, "RADIATION BELT STORM PROBE A", (ftnlen)36, (
	    ftnlen)28);
    n2ccod[390] = -362;
    s_copy(n2cnam + 14040, "RBSP_A", (ftnlen)36, (ftnlen)6);
    n2ccod[391] = -363;
    s_copy(n2cnam + 14076, "RADIATION BELT STORM PROBE B", (ftnlen)36, (
	    ftnlen)28);
    n2ccod[392] = -363;
    s_copy(n2cnam + 14112, "RBSP_B", (ftnlen)36, (ftnlen)6);
    n2ccod[393] = -500;
    s_copy(n2cnam + 14148, "RSAT", (ftnlen)36, (ftnlen)4);
    n2ccod[394] = -500;
    s_copy(n2cnam + 14184, "SELENE Relay Satellite", (ftnlen)36, (ftnlen)22);
    n2ccod[395] = -500;
    s_copy(n2cnam + 14220, "SELENE Rstar", (ftnlen)36, (ftnlen)12);
    n2ccod[396] = -500;
    s_copy(n2cnam + 14256, "Rstar", (ftnlen)36, (ftnlen)5);
    n2ccod[397] = -502;
    s_copy(n2cnam + 14292, "VSAT", (ftnlen)36, (ftnlen)4);
    n2ccod[398] = -502;
    s_copy(n2cnam + 14328, "SELENE VLBI Radio Satellite", (ftnlen)36, (ftnlen)
	    27);
    n2ccod[399] = -502;
    s_copy(n2cnam + 14364, "SELENE VRAD Satellite", (ftnlen)36, (ftnlen)21);
    n2ccod[400] = -502;
    s_copy(n2cnam + 14400, "SELENE Vstar", (ftnlen)36, (ftnlen)12);
    n2ccod[401] = -502;
    s_copy(n2cnam + 14436, "Vstar", (ftnlen)36, (ftnlen)5);
    n2ccod[402] = -550;
    s_copy(n2cnam + 14472, "MARS-96", (ftnlen)36, (ftnlen)7);
    n2ccod[403] = -550;
    s_copy(n2cnam + 14508, "M96", (ftnlen)36, (ftnlen)3);
    n2ccod[404] = -550;
    s_copy(n2cnam + 14544, "MARS 96", (ftnlen)36, (ftnlen)7);
    n2ccod[405] = -550;
    s_copy(n2cnam + 14580, "MARS96", (ftnlen)36, (ftnlen)6);
    n2ccod[406] = -750;
    s_copy(n2cnam + 14616, "SPRINT-A", (ftnlen)36, (ftnlen)8);
    n2ccod[407] = 50000001;
    s_copy(n2cnam + 14652, "SHOEMAKER-LEVY 9-W", (ftnlen)36, (ftnlen)18);
    n2ccod[408] = 50000002;
    s_copy(n2cnam + 14688, "SHOEMAKER-LEVY 9-V", (ftnlen)36, (ftnlen)18);
    n2ccod[409] = 50000003;
    s_copy(n2cnam + 14724, "SHOEMAKER-LEVY 9-U", (ftnlen)36, (ftnlen)18);
    n2ccod[410] = 50000004;
    s_copy(n2cnam + 14760, "SHOEMAKER-LEVY 9-T", (ftnlen)36, (ftnlen)18);
    n2ccod[411] = 50000005;
    s_copy(n2cnam + 14796, "SHOEMAKER-LEVY 9-S", (ftnlen)36, (ftnlen)18);
    n2ccod[412] = 50000006;
    s_copy(n2cnam + 14832, "SHOEMAKER-LEVY 9-R", (ftnlen)36, (ftnlen)18);
    n2ccod[413] = 50000007;
    s_copy(n2cnam + 14868, "SHOEMAKER-LEVY 9-Q", (ftnlen)36, (ftnlen)18);
    n2ccod[414] = 50000008;
    s_copy(n2cnam + 14904, "SHOEMAKER-LEVY 9-P", (ftnlen)36, (ftnlen)18);
    n2ccod[415] = 50000009;
    s_copy(n2cnam + 14940, "SHOEMAKER-LEVY 9-N", (ftnlen)36, (ftnlen)18);
    n2ccod[416] = 50000010;
    s_copy(n2cnam + 14976, "SHOEMAKER-LEVY 9-M", (ftnlen)36, (ftnlen)18);
    n2ccod[417] = 50000011;
    s_copy(n2cnam + 15012, "SHOEMAKER-LEVY 9-L", (ftnlen)36, (ftnlen)18);
    n2ccod[418] = 50000012;
    s_copy(n2cnam + 15048, "SHOEMAKER-LEVY 9-K", (ftnlen)36, (ftnlen)18);
    n2ccod[419] = 50000013;
    s_copy(n2cnam + 15084, "SHOEMAKER-LEVY 9-J", (ftnlen)36, (ftnlen)18);
    n2ccod[420] = 50000014;
    s_copy(n2cnam + 15120, "SHOEMAKER-LEVY 9-H", (ftnlen)36, (ftnlen)18);
    n2ccod[421] = 50000015;
    s_copy(n2cnam + 15156, "SHOEMAKER-LEVY 9-G", (ftnlen)36, (ftnlen)18);
    n2ccod[422] = 50000016;
    s_copy(n2cnam + 15192, "SHOEMAKER-LEVY 9-F", (ftnlen)36, (ftnlen)18);
    n2ccod[423] = 50000017;
    s_copy(n2cnam + 15228, "SHOEMAKER-LEVY 9-E", (ftnlen)36, (ftnlen)18);
    n2ccod[424] = 50000018;
    s_copy(n2cnam + 15264, "SHOEMAKER-LEVY 9-D", (ftnlen)36, (ftnlen)18);
    n2ccod[425] = 50000019;
    s_copy(n2cnam + 15300, "SHOEMAKER-LEVY 9-C", (ftnlen)36, (ftnlen)18);
    n2ccod[426] = 50000020;
    s_copy(n2cnam + 15336, "SHOEMAKER-LEVY 9-B", (ftnlen)36, (ftnlen)18);
    n2ccod[427] = 50000021;
    s_copy(n2cnam + 15372, "SHOEMAKER-LEVY 9-A", (ftnlen)36, (ftnlen)18);
    n2ccod[428] = 50000022;
    s_copy(n2cnam + 15408, "SHOEMAKER-LEVY 9-Q1", (ftnlen)36, (ftnlen)19);
    n2ccod[429] = 50000023;
    s_copy(n2cnam + 15444, "SHOEMAKER-LEVY 9-P2", (ftnlen)36, (ftnlen)19);
    n2ccod[430] = 1000001;
    s_copy(n2cnam + 15480, "AREND", (ftnlen)36, (ftnlen)5);
    n2ccod[431] = 1000002;
    s_copy(n2cnam + 15516, "AREND-RIGAUX", (ftnlen)36, (ftnlen)12);
    n2ccod[432] = 1000003;
    s_copy(n2cnam + 15552, "ASHBROOK-JACKSON", (ftnlen)36, (ftnlen)16);
    n2ccod[433] = 1000004;
    s_copy(n2cnam + 15588, "BOETHIN", (ftnlen)36, (ftnlen)7);
    n2ccod[434] = 1000005;
    s_copy(n2cnam + 15624, "BORRELLY", (ftnlen)36, (ftnlen)8);
    n2ccod[435] = 1000006;
    s_copy(n2cnam + 15660, "BOWELL-SKIFF", (ftnlen)36, (ftnlen)12);
    n2ccod[436] = 1000007;
    s_copy(n2cnam + 15696, "BRADFIELD", (ftnlen)36, (ftnlen)9);
    n2ccod[437] = 1000008;
    s_copy(n2cnam + 15732, "BROOKS 2", (ftnlen)36, (ftnlen)8);
    n2ccod[438] = 1000009;
    s_copy(n2cnam + 15768, "BRORSEN-METCALF", (ftnlen)36, (ftnlen)15);
    n2ccod[439] = 1000010;
    s_copy(n2cnam + 15804, "BUS", (ftnlen)36, (ftnlen)3);
    n2ccod[440] = 1000011;
    s_copy(n2cnam + 15840, "CHERNYKH", (ftnlen)36, (ftnlen)8);
    n2ccod[441] = 1000012;
    s_copy(n2cnam + 15876, "67P/CHURYUMOV-GERASIMENKO (1969 R1)", (ftnlen)36, 
	    (ftnlen)35);
    n2ccod[442] = 1000012;
    s_copy(n2cnam + 15912, "CHURYUMOV-GERASIMENKO", (ftnlen)36, (ftnlen)21);
    n2ccod[443] = 1000013;
    s_copy(n2cnam + 15948, "CIFFREO", (ftnlen)36, (ftnlen)7);
    n2ccod[444] = 1000014;
    s_copy(n2cnam + 15984, "CLARK", (ftnlen)36, (ftnlen)5);
    n2ccod[445] = 1000015;
    s_copy(n2cnam + 16020, "COMAS SOLA", (ftnlen)36, (ftnlen)10);
    n2ccod[446] = 1000016;
    s_copy(n2cnam + 16056, "CROMMELIN", (ftnlen)36, (ftnlen)9);
    n2ccod[447] = 1000017;
    s_copy(n2cnam + 16092, "D'ARREST", (ftnlen)36, (ftnlen)8);
    n2ccod[448] = 1000018;
    s_copy(n2cnam + 16128, "DANIEL", (ftnlen)36, (ftnlen)6);
    n2ccod[449] = 1000019;
    s_copy(n2cnam + 16164, "DE VICO-SWIFT", (ftnlen)36, (ftnlen)13);
    n2ccod[450] = 1000020;
    s_copy(n2cnam + 16200, "DENNING-FUJIKAWA", (ftnlen)36, (ftnlen)16);
    n2ccod[451] = 1000021;
    s_copy(n2cnam + 16236, "DU TOIT 1", (ftnlen)36, (ftnlen)9);
    n2ccod[452] = 1000022;
    s_copy(n2cnam + 16272, "DU TOIT-HARTLEY", (ftnlen)36, (ftnlen)15);
    n2ccod[453] = 1000023;
    s_copy(n2cnam + 16308, "DUTOIT-NEUJMIN-DELPORTE", (ftnlen)36, (ftnlen)23);
    n2ccod[454] = 1000024;
    s_copy(n2cnam + 16344, "DUBIAGO", (ftnlen)36, (ftnlen)7);
    n2ccod[455] = 1000025;
    s_copy(n2cnam + 16380, "ENCKE", (ftnlen)36, (ftnlen)5);
    n2ccod[456] = 1000026;
    s_copy(n2cnam + 16416, "FAYE", (ftnlen)36, (ftnlen)4);
    n2ccod[457] = 1000027;
    s_copy(n2cnam + 16452, "FINLAY", (ftnlen)36, (ftnlen)6);
    n2ccod[458] = 1000028;
    s_copy(n2cnam + 16488, "FORBES", (ftnlen)36, (ftnlen)6);
    n2ccod[459] = 1000029;
    s_copy(n2cnam + 16524, "GEHRELS 1", (ftnlen)36, (ftnlen)9);
    n2ccod[460] = 1000030;
    s_copy(n2cnam + 16560, "GEHRELS 2", (ftnlen)36, (ftnlen)9);
    n2ccod[461] = 1000031;
    s_copy(n2cnam + 16596, "GEHRELS 3", (ftnlen)36, (ftnlen)9);
    n2ccod[462] = 1000032;
    s_copy(n2cnam + 16632, "GIACOBINI-ZINNER", (ftnlen)36, (ftnlen)16);
    n2ccod[463] = 1000033;
    s_copy(n2cnam + 16668, "GICLAS", (ftnlen)36, (ftnlen)6);
    n2ccod[464] = 1000034;
    s_copy(n2cnam + 16704, "GRIGG-SKJELLERUP", (ftnlen)36, (ftnlen)16);
    n2ccod[465] = 1000035;
    s_copy(n2cnam + 16740, "GUNN", (ftnlen)36, (ftnlen)4);
    n2ccod[466] = 1000036;
    s_copy(n2cnam + 16776, "HALLEY", (ftnlen)36, (ftnlen)6);
    n2ccod[467] = 1000037;
    s_copy(n2cnam + 16812, "HANEDA-CAMPOS", (ftnlen)36, (ftnlen)13);
    n2ccod[468] = 1000038;
    s_copy(n2cnam + 16848, "HARRINGTON", (ftnlen)36, (ftnlen)10);
    n2ccod[469] = 1000039;
    s_copy(n2cnam + 16884, "HARRINGTON-ABELL", (ftnlen)36, (ftnlen)16);
    n2ccod[470] = 1000040;
    s_copy(n2cnam + 16920, "HARTLEY 1", (ftnlen)36, (ftnlen)9);
    n2ccod[471] = 1000041;
    s_copy(n2cnam + 16956, "HARTLEY 2", (ftnlen)36, (ftnlen)9);
    n2ccod[472] = 1000042;
    s_copy(n2cnam + 16992, "HARTLEY-IRAS", (ftnlen)36, (ftnlen)12);
    n2ccod[473] = 1000043;
    s_copy(n2cnam + 17028, "HERSCHEL-RIGOLLET", (ftnlen)36, (ftnlen)17);
    n2ccod[474] = 1000044;
    s_copy(n2cnam + 17064, "HOLMES", (ftnlen)36, (ftnlen)6);
    n2ccod[475] = 1000045;
    s_copy(n2cnam + 17100, "HONDA-MRKOS-PAJDUSAKOVA", (ftnlen)36, (ftnlen)23);
    n2ccod[476] = 1000046;
    s_copy(n2cnam + 17136, "HOWELL", (ftnlen)36, (ftnlen)6);
    n2ccod[477] = 1000047;
    s_copy(n2cnam + 17172, "IRAS", (ftnlen)36, (ftnlen)4);
    n2ccod[478] = 1000048;
    s_copy(n2cnam + 17208, "JACKSON-NEUJMIN", (ftnlen)36, (ftnlen)15);
    n2ccod[479] = 1000049;
    s_copy(n2cnam + 17244, "JOHNSON", (ftnlen)36, (ftnlen)7);
    n2ccod[480] = 1000050;
    s_copy(n2cnam + 17280, "KEARNS-KWEE", (ftnlen)36, (ftnlen)11);
    n2ccod[481] = 1000051;
    s_copy(n2cnam + 17316, "KLEMOLA", (ftnlen)36, (ftnlen)7);
    n2ccod[482] = 1000052;
    s_copy(n2cnam + 17352, "KOHOUTEK", (ftnlen)36, (ftnlen)8);
    n2ccod[483] = 1000053;
    s_copy(n2cnam + 17388, "KOJIMA", (ftnlen)36, (ftnlen)6);
    n2ccod[484] = 1000054;
    s_copy(n2cnam + 17424, "KOPFF", (ftnlen)36, (ftnlen)5);
    n2ccod[485] = 1000055;
    s_copy(n2cnam + 17460, "KOWAL 1", (ftnlen)36, (ftnlen)7);
    n2ccod[486] = 1000056;
    s_copy(n2cnam + 17496, "KOWAL 2", (ftnlen)36, (ftnlen)7);
    n2ccod[487] = 1000057;
    s_copy(n2cnam + 17532, "KOWAL-MRKOS", (ftnlen)36, (ftnlen)11);
    n2ccod[488] = 1000058;
    s_copy(n2cnam + 17568, "KOWAL-VAVROVA", (ftnlen)36, (ftnlen)13);
    n2ccod[489] = 1000059;
    s_copy(n2cnam + 17604, "LONGMORE", (ftnlen)36, (ftnlen)8);
    n2ccod[490] = 1000060;
    s_copy(n2cnam + 17640, "LOVAS 1", (ftnlen)36, (ftnlen)7);
    n2ccod[491] = 1000061;
    s_copy(n2cnam + 17676, "MACHHOLZ", (ftnlen)36, (ftnlen)8);
    n2ccod[492] = 1000062;
    s_copy(n2cnam + 17712, "MAURY", (ftnlen)36, (ftnlen)5);
    n2ccod[493] = 1000063;
    s_copy(n2cnam + 17748, "NEUJMIN 1", (ftnlen)36, (ftnlen)9);
    n2ccod[494] = 1000064;
    s_copy(n2cnam + 17784, "NEUJMIN 2", (ftnlen)36, (ftnlen)9);
    n2ccod[495] = 1000065;
    s_copy(n2cnam + 17820, "NEUJMIN 3", (ftnlen)36, (ftnlen)9);
    n2ccod[496] = 1000066;
    s_copy(n2cnam + 17856, "OLBERS", (ftnlen)36, (ftnlen)6);
    n2ccod[497] = 1000067;
    s_copy(n2cnam + 17892, "PETERS-HARTLEY", (ftnlen)36, (ftnlen)14);
    n2ccod[498] = 1000068;
    s_copy(n2cnam + 17928, "PONS-BROOKS", (ftnlen)36, (ftnlen)11);
    n2ccod[499] = 1000069;
    s_copy(n2cnam + 17964, "PONS-WINNECKE", (ftnlen)36, (ftnlen)13);
    n2ccod[500] = 1000070;
    s_copy(n2cnam + 18000, "REINMUTH 1", (ftnlen)36, (ftnlen)10);
    n2ccod[501] = 1000071;
    s_copy(n2cnam + 18036, "REINMUTH 2", (ftnlen)36, (ftnlen)10);
    n2ccod[502] = 1000072;
    s_copy(n2cnam + 18072, "RUSSELL 1", (ftnlen)36, (ftnlen)9);
    n2ccod[503] = 1000073;
    s_copy(n2cnam + 18108, "RUSSELL 2", (ftnlen)36, (ftnlen)9);
    n2ccod[504] = 1000074;
    s_copy(n2cnam + 18144, "RUSSELL 3", (ftnlen)36, (ftnlen)9);
    n2ccod[505] = 1000075;
    s_copy(n2cnam + 18180, "RUSSELL 4", (ftnlen)36, (ftnlen)9);
    n2ccod[506] = 1000076;
    s_copy(n2cnam + 18216, "SANGUIN", (ftnlen)36, (ftnlen)7);
    n2ccod[507] = 1000077;
    s_copy(n2cnam + 18252, "SCHAUMASSE", (ftnlen)36, (ftnlen)10);
    n2ccod[508] = 1000078;
    s_copy(n2cnam + 18288, "SCHUSTER", (ftnlen)36, (ftnlen)8);
    n2ccod[509] = 1000079;
    s_copy(n2cnam + 18324, "SCHWASSMANN-WACHMANN 1", (ftnlen)36, (ftnlen)22);
    n2ccod[510] = 1000080;
    s_copy(n2cnam + 18360, "SCHWASSMANN-WACHMANN 2", (ftnlen)36, (ftnlen)22);
    n2ccod[511] = 1000081;
    s_copy(n2cnam + 18396, "SCHWASSMANN-WACHMANN 3", (ftnlen)36, (ftnlen)22);
    n2ccod[512] = 1000082;
    s_copy(n2cnam + 18432, "SHAJN-SCHALDACH", (ftnlen)36, (ftnlen)15);
    n2ccod[513] = 1000083;
    s_copy(n2cnam + 18468, "SHOEMAKER 1", (ftnlen)36, (ftnlen)11);
    n2ccod[514] = 1000084;
    s_copy(n2cnam + 18504, "SHOEMAKER 2", (ftnlen)36, (ftnlen)11);
    n2ccod[515] = 1000085;
    s_copy(n2cnam + 18540, "SHOEMAKER 3", (ftnlen)36, (ftnlen)11);
    n2ccod[516] = 1000086;
    s_copy(n2cnam + 18576, "SINGER-BREWSTER", (ftnlen)36, (ftnlen)15);
    n2ccod[517] = 1000087;
    s_copy(n2cnam + 18612, "SLAUGHTER-BURNHAM", (ftnlen)36, (ftnlen)17);
    n2ccod[518] = 1000088;
    s_copy(n2cnam + 18648, "SMIRNOVA-CHERNYKH", (ftnlen)36, (ftnlen)17);
    n2ccod[519] = 1000089;
    s_copy(n2cnam + 18684, "STEPHAN-OTERMA", (ftnlen)36, (ftnlen)14);
    n2ccod[520] = 1000090;
    s_copy(n2cnam + 18720, "SWIFT-GEHRELS", (ftnlen)36, (ftnlen)13);
    n2ccod[521] = 1000091;
    s_copy(n2cnam + 18756, "TAKAMIZAWA", (ftnlen)36, (ftnlen)10);
    n2ccod[522] = 1000092;
    s_copy(n2cnam + 18792, "TAYLOR", (ftnlen)36, (ftnlen)6);
    n2ccod[523] = 1000093;
    s_copy(n2cnam + 18828, "TEMPEL_1", (ftnlen)36, (ftnlen)8);
    n2ccod[524] = 1000093;
    s_copy(n2cnam + 18864, "TEMPEL 1", (ftnlen)36, (ftnlen)8);
    n2ccod[525] = 1000094;
    s_copy(n2cnam + 18900, "TEMPEL 2", (ftnlen)36, (ftnlen)8);
    n2ccod[526] = 1000095;
    s_copy(n2cnam + 18936, "TEMPEL-TUTTLE", (ftnlen)36, (ftnlen)13);
    n2ccod[527] = 1000096;
    s_copy(n2cnam + 18972, "TRITTON", (ftnlen)36, (ftnlen)7);
    n2ccod[528] = 1000097;
    s_copy(n2cnam + 19008, "TSUCHINSHAN 1", (ftnlen)36, (ftnlen)13);
    n2ccod[529] = 1000098;
    s_copy(n2cnam + 19044, "TSUCHINSHAN 2", (ftnlen)36, (ftnlen)13);
    n2ccod[530] = 1000099;
    s_copy(n2cnam + 19080, "TUTTLE", (ftnlen)36, (ftnlen)6);
    n2ccod[531] = 1000100;
    s_copy(n2cnam + 19116, "TUTTLE-GIACOBINI-KRESAK", (ftnlen)36, (ftnlen)23);
    n2ccod[532] = 1000101;
    s_copy(n2cnam + 19152, "VAISALA 1", (ftnlen)36, (ftnlen)9);
    n2ccod[533] = 1000102;
    s_copy(n2cnam + 19188, "VAN BIESBROECK", (ftnlen)36, (ftnlen)14);
    n2ccod[534] = 1000103;
    s_copy(n2cnam + 19224, "VAN HOUTEN", (ftnlen)36, (ftnlen)10);
    n2ccod[535] = 1000104;
    s_copy(n2cnam + 19260, "WEST-KOHOUTEK-IKEMURA", (ftnlen)36, (ftnlen)21);
    n2ccod[536] = 1000105;
    s_copy(n2cnam + 19296, "WHIPPLE", (ftnlen)36, (ftnlen)7);
    n2ccod[537] = 1000106;
    s_copy(n2cnam + 19332, "WILD 1", (ftnlen)36, (ftnlen)6);
    n2ccod[538] = 1000107;
    s_copy(n2cnam + 19368, "WILD 2", (ftnlen)36, (ftnlen)6);
    n2ccod[539] = 1000108;
    s_copy(n2cnam + 19404, "WILD 3", (ftnlen)36, (ftnlen)6);
    n2ccod[540] = 1000109;
    s_copy(n2cnam + 19440, "WIRTANEN", (ftnlen)36, (ftnlen)8);
    n2ccod[541] = 1000110;
    s_copy(n2cnam + 19476, "WOLF", (ftnlen)36, (ftnlen)4);
    n2ccod[542] = 1000111;
    s_copy(n2cnam + 19512, "WOLF-HARRINGTON", (ftnlen)36, (ftnlen)15);
    n2ccod[543] = 1000112;
    s_copy(n2cnam + 19548, "LOVAS 2", (ftnlen)36, (ftnlen)7);
    n2ccod[544] = 1000113;
    s_copy(n2cnam + 19584, "URATA-NIIJIMA", (ftnlen)36, (ftnlen)13);
    n2ccod[545] = 1000114;
    s_copy(n2cnam + 19620, "WISEMAN-SKIFF", (ftnlen)36, (ftnlen)13);
    n2ccod[546] = 1000115;
    s_copy(n2cnam + 19656, "HELIN", (ftnlen)36, (ftnlen)5);
    n2ccod[547] = 1000116;
    s_copy(n2cnam + 19692, "MUELLER", (ftnlen)36, (ftnlen)7);
    n2ccod[548] = 1000117;
    s_copy(n2cnam + 19728, "SHOEMAKER-HOLT 1", (ftnlen)36, (ftnlen)16);
    n2ccod[549] = 1000118;
    s_copy(n2cnam + 19764, "HELIN-ROMAN-CROCKETT", (ftnlen)36, (ftnlen)20);
    n2ccod[550] = 1000119;
    s_copy(n2cnam + 19800, "HARTLEY 3", (ftnlen)36, (ftnlen)9);
    n2ccod[551] = 1000120;
    s_copy(n2cnam + 19836, "PARKER-HARTLEY", (ftnlen)36, (ftnlen)14);
    n2ccod[552] = 1000121;
    s_copy(n2cnam + 19872, "HELIN-ROMAN-ALU 1", (ftnlen)36, (ftnlen)17);
    n2ccod[553] = 1000122;
    s_copy(n2cnam + 19908, "WILD 4", (ftnlen)36, (ftnlen)6);
    n2ccod[554] = 1000123;
    s_copy(n2cnam + 19944, "MUELLER 2", (ftnlen)36, (ftnlen)9);
    n2ccod[555] = 1000124;
    s_copy(n2cnam + 19980, "MUELLER 3", (ftnlen)36, (ftnlen)9);
    n2ccod[556] = 1000125;
    s_copy(n2cnam + 20016, "SHOEMAKER-LEVY 1", (ftnlen)36, (ftnlen)16);
    n2ccod[557] = 1000126;
    s_copy(n2cnam + 20052, "SHOEMAKER-LEVY 2", (ftnlen)36, (ftnlen)16);
    n2ccod[558] = 1000127;
    s_copy(n2cnam + 20088, "HOLT-OLMSTEAD", (ftnlen)36, (ftnlen)13);
    n2ccod[559] = 1000128;
    s_copy(n2cnam + 20124, "METCALF-BREWINGTON", (ftnlen)36, (ftnlen)18);
    n2ccod[560] = 1000129;
    s_copy(n2cnam + 20160, "LEVY", (ftnlen)36, (ftnlen)4);
    n2ccod[561] = 1000130;
    s_copy(n2cnam + 20196, "SHOEMAKER-LEVY 9", (ftnlen)36, (ftnlen)16);
    n2ccod[562] = 1000131;
    s_copy(n2cnam + 20232, "HYAKUTAKE", (ftnlen)36, (ftnlen)9);
    n2ccod[563] = 1000132;
    s_copy(n2cnam + 20268, "HALE-BOPP", (ftnlen)36, (ftnlen)9);
    n2ccod[564] = 1003228;
    s_copy(n2cnam + 20304, "C/2013 A1", (ftnlen)36, (ftnlen)9);
    n2ccod[565] = 1003228;
    s_copy(n2cnam + 20340, "SIDING SPRING", (ftnlen)36, (ftnlen)13);
    n2ccod[566] = 9511010;
    s_copy(n2cnam + 20376, "GASPRA", (ftnlen)36, (ftnlen)6);
    n2ccod[567] = 2431010;
    s_copy(n2cnam + 20412, "IDA", (ftnlen)36, (ftnlen)3);
    n2ccod[568] = 2431011;
    s_copy(n2cnam + 20448, "DACTYL", (ftnlen)36, (ftnlen)6);
    n2ccod[569] = 2000001;
    s_copy(n2cnam + 20484, "CERES", (ftnlen)36, (ftnlen)5);
    n2ccod[570] = 2000002;
    s_copy(n2cnam + 20520, "PALLAS", (ftnlen)36, (ftnlen)6);
    n2ccod[571] = 2000004;
    s_copy(n2cnam + 20556, "VESTA", (ftnlen)36, (ftnlen)5);
    n2ccod[572] = 2000016;
    s_copy(n2cnam + 20592, "PSYCHE", (ftnlen)36, (ftnlen)6);
    n2ccod[573] = 2000021;
    s_copy(n2cnam + 20628, "LUTETIA", (ftnlen)36, (ftnlen)7);
    n2ccod[574] = 2000216;
    s_copy(n2cnam + 20664, "KLEOPATRA", (ftnlen)36, (ftnlen)9);
    n2ccod[575] = 2000433;
    s_copy(n2cnam + 20700, "EROS", (ftnlen)36, (ftnlen)4);
    n2ccod[576] = 2000511;
    s_copy(n2cnam + 20736, "DAVIDA", (ftnlen)36, (ftnlen)6);
    n2ccod[577] = 2000253;
    s_copy(n2cnam + 20772, "MATHILDE", (ftnlen)36, (ftnlen)8);
    n2ccod[578] = 2002867;
    s_copy(n2cnam + 20808, "STEINS", (ftnlen)36, (ftnlen)6);
    n2ccod[579] = 2009969;
    s_copy(n2cnam + 20844, "1992KD", (ftnlen)36, (ftnlen)6);
    n2ccod[580] = 2009969;
    s_copy(n2cnam + 20880, "BRAILLE", (ftnlen)36, (ftnlen)7);
    n2ccod[581] = 2004015;
    s_copy(n2cnam + 20916, "WILSON-HARRINGTON", (ftnlen)36, (ftnlen)17);
    n2ccod[582] = 2004179;
    s_copy(n2cnam + 20952, "TOUTATIS", (ftnlen)36, (ftnlen)8);
    n2ccod[583] = 2025143;
    s_copy(n2cnam + 20988, "ITOKAWA", (ftnlen)36, (ftnlen)7);
    n2ccod[584] = 2101955;
    s_copy(n2cnam + 21024, "BENNU", (ftnlen)36, (ftnlen)5);
    n2ccod[585] = 398989;
    s_copy(n2cnam + 21060, "NOTO", (ftnlen)36, (ftnlen)4);
    n2ccod[586] = 398990;
    s_copy(n2cnam + 21096, "NEW NORCIA", (ftnlen)36, (ftnlen)10);
    n2ccod[587] = 399001;
    s_copy(n2cnam + 21132, "GOLDSTONE", (ftnlen)36, (ftnlen)9);
    n2ccod[588] = 399002;
    s_copy(n2cnam + 21168, "CANBERRA", (ftnlen)36, (ftnlen)8);
    n2ccod[589] = 399003;
    s_copy(n2cnam + 21204, "MADRID", (ftnlen)36, (ftnlen)6);
    n2ccod[590] = 399004;
    s_copy(n2cnam + 21240, "USUDA", (ftnlen)36, (ftnlen)5);
    n2ccod[591] = 399005;
    s_copy(n2cnam + 21276, "DSS-05", (ftnlen)36, (ftnlen)6);
    n2ccod[592] = 399005;
    s_copy(n2cnam + 21312, "PARKES", (ftnlen)36, (ftnlen)6);
    n2ccod[593] = 399012;
    s_copy(n2cnam + 21348, "DSS-12", (ftnlen)36, (ftnlen)6);
    n2ccod[594] = 399013;
    s_copy(n2cnam + 21384, "DSS-13", (ftnlen)36, (ftnlen)6);
    n2ccod[595] = 399014;
    s_copy(n2cnam + 21420, "DSS-14", (ftnlen)36, (ftnlen)6);
    n2ccod[596] = 399015;
    s_copy(n2cnam + 21456, "DSS-15", (ftnlen)36, (ftnlen)6);
    n2ccod[597] = 399016;
    s_copy(n2cnam + 21492, "DSS-16", (ftnlen)36, (ftnlen)6);
    n2ccod[598] = 399017;
    s_copy(n2cnam + 21528, "DSS-17", (ftnlen)36, (ftnlen)6);
    n2ccod[599] = 399023;
    s_copy(n2cnam + 21564, "DSS-23", (ftnlen)36, (ftnlen)6);
    n2ccod[600] = 399024;
    s_copy(n2cnam + 21600, "DSS-24", (ftnlen)36, (ftnlen)6);
    n2ccod[601] = 399025;
    s_copy(n2cnam + 21636, "DSS-25", (ftnlen)36, (ftnlen)6);
    n2ccod[602] = 399026;
    s_copy(n2cnam + 21672, "DSS-26", (ftnlen)36, (ftnlen)6);
    n2ccod[603] = 399027;
    s_copy(n2cnam + 21708, "DSS-27", (ftnlen)36, (ftnlen)6);
    n2ccod[604] = 399028;
    s_copy(n2cnam + 21744, "DSS-28", (ftnlen)36, (ftnlen)6);
    n2ccod[605] = 399033;
    s_copy(n2cnam + 21780, "DSS-33", (ftnlen)36, (ftnlen)6);
    n2ccod[606] = 399034;
    s_copy(n2cnam + 21816, "DSS-34", (ftnlen)36, (ftnlen)6);
    n2ccod[607] = 399042;
    s_copy(n2cnam + 21852, "DSS-42", (ftnlen)36, (ftnlen)6);
    n2ccod[608] = 399043;
    s_copy(n2cnam + 21888, "DSS-43", (ftnlen)36, (ftnlen)6);
    n2ccod[609] = 399045;
    s_copy(n2cnam + 21924, "DSS-45", (ftnlen)36, (ftnlen)6);
    n2ccod[610] = 399046;
    s_copy(n2cnam + 21960, "DSS-46", (ftnlen)36, (ftnlen)6);
    n2ccod[611] = 399049;
    s_copy(n2cnam + 21996, "DSS-49", (ftnlen)36, (ftnlen)6);
    n2ccod[612] = 399053;
    s_copy(n2cnam + 22032, "DSS-53", (ftnlen)36, (ftnlen)6);
    n2ccod[613] = 399054;
    s_copy(n2cnam + 22068, "DSS-54", (ftnlen)36, (ftnlen)6);
    n2ccod[614] = 399055;
    s_copy(n2cnam + 22104, "DSS-55", (ftnlen)36, (ftnlen)6);
    n2ccod[615] = 399061;
    s_copy(n2cnam + 22140, "DSS-61", (ftnlen)36, (ftnlen)6);
    n2ccod[616] = 399063;
    s_copy(n2cnam + 22176, "DSS-63", (ftnlen)36, (ftnlen)6);
    n2ccod[617] = 399064;
    s_copy(n2cnam + 22212, "DSS-64", (ftnlen)36, (ftnlen)6);
    n2ccod[618] = 399065;
    s_copy(n2cnam + 22248, "DSS-65", (ftnlen)36, (ftnlen)6);
    n2ccod[619] = 399066;
    s_copy(n2cnam + 22284, "DSS-66", (ftnlen)36, (ftnlen)6);

/*     The data list for the ID -> NAME tests. */
/*     One-to-one and on-to. */

    c2ncod[0] = -1;
    s_copy(c2nnam, "GEOTAIL", (ftnlen)36, (ftnlen)7);
    c2ncod[1] = -107;
    s_copy(c2nnam + 36, "TRMM", (ftnlen)36, (ftnlen)4);
    c2ncod[2] = -112;
    s_copy(c2nnam + 72, "ICE", (ftnlen)36, (ftnlen)3);
    c2ncod[3] = -116;
    s_copy(c2nnam + 108, "MPL", (ftnlen)36, (ftnlen)3);
    c2ncod[4] = -117;
    s_copy(c2nnam + 144, "EXOMARS 2016 EDM", (ftnlen)36, (ftnlen)16);
    c2ncod[5] = -12;
    s_copy(c2nnam + 180, "LADEE", (ftnlen)36, (ftnlen)5);
    c2ncod[6] = -121;
    s_copy(c2nnam + 216, "BEPICOLOMBO MPO", (ftnlen)36, (ftnlen)15);
    c2ncod[7] = -127;
    s_copy(c2nnam + 252, "MCO", (ftnlen)36, (ftnlen)3);
    c2ncod[8] = -13;
    s_copy(c2nnam + 288, "POLAR", (ftnlen)36, (ftnlen)5);
    c2ncod[9] = -130;
    s_copy(c2nnam + 324, "HAYABUSA", (ftnlen)36, (ftnlen)8);
    c2ncod[10] = -131;
    s_copy(c2nnam + 360, "KAGUYA", (ftnlen)36, (ftnlen)6);
    c2ncod[11] = -135;
    s_copy(c2nnam + 396, "DRTS-W", (ftnlen)36, (ftnlen)6);
    c2ncod[12] = -140;
    s_copy(c2nnam + 432, "DEEP IMPACT FLYBY SPACECRAFT", (ftnlen)36, (ftnlen)
	    28);
    c2ncod[13] = -142;
    s_copy(c2nnam + 468, "EOS-AM1", (ftnlen)36, (ftnlen)7);
    c2ncod[14] = -143;
    s_copy(c2nnam + 504, "EXOMARS 2016 TGO", (ftnlen)36, (ftnlen)16);
    c2ncod[15] = -144;
    s_copy(c2nnam + 540, "SOLAR ORBITER", (ftnlen)36, (ftnlen)13);
    c2ncod[16] = -146;
    s_copy(c2nnam + 576, "LUNAR-A", (ftnlen)36, (ftnlen)7);
    c2ncod[17] = -150;
    s_copy(c2nnam + 612, "CASP", (ftnlen)36, (ftnlen)4);
    c2ncod[18] = -151;
    s_copy(c2nnam + 648, "CHANDRA", (ftnlen)36, (ftnlen)7);
    c2ncod[19] = -152;
    s_copy(c2nnam + 684, "CHANDRAYAAN-2", (ftnlen)36, (ftnlen)13);
    c2ncod[20] = -154;
    s_copy(c2nnam + 720, "AQUA", (ftnlen)36, (ftnlen)4);
    c2ncod[21] = -159;
    s_copy(c2nnam + 756, "EUROPA CLIPPER", (ftnlen)36, (ftnlen)14);
    c2ncod[22] = -164;
    s_copy(c2nnam + 792, "SOLAR-A", (ftnlen)36, (ftnlen)7);
    c2ncod[23] = -165;
    s_copy(c2nnam + 828, "MAP", (ftnlen)36, (ftnlen)3);
    c2ncod[24] = -166;
    s_copy(c2nnam + 864, "IMAGE", (ftnlen)36, (ftnlen)5);
    c2ncod[25] = -170;
    s_copy(c2nnam + 900, "JAMES WEBB SPACE TELESCOPE", (ftnlen)36, (ftnlen)26)
	    ;
    c2ncod[26] = -177;
    s_copy(c2nnam + 936, "GRAIL-A", (ftnlen)36, (ftnlen)7);
    c2ncod[27] = -178;
    s_copy(c2nnam + 972, "NOZOMI", (ftnlen)36, (ftnlen)6);
    c2ncod[28] = -18;
    s_copy(c2nnam + 1008, "LCROSS", (ftnlen)36, (ftnlen)6);
    c2ncod[29] = -181;
    s_copy(c2nnam + 1044, "GRAIL-B", (ftnlen)36, (ftnlen)7);
    c2ncod[30] = -183;
    s_copy(c2nnam + 1080, "CLUSTER 1", (ftnlen)36, (ftnlen)9);
    c2ncod[31] = -185;
    s_copy(c2nnam + 1116, "CLUSTER 2", (ftnlen)36, (ftnlen)9);
    c2ncod[32] = -188;
    s_copy(c2nnam + 1152, "MUSES-B", (ftnlen)36, (ftnlen)7);
    c2ncod[33] = -189;
    s_copy(c2nnam + 1188, "INSIGHT", (ftnlen)36, (ftnlen)7);
    c2ncod[34] = -190;
    s_copy(c2nnam + 1224, "SIM", (ftnlen)36, (ftnlen)3);
    c2ncod[35] = -194;
    s_copy(c2nnam + 1260, "CLUSTER 3", (ftnlen)36, (ftnlen)9);
    c2ncod[36] = -196;
    s_copy(c2nnam + 1296, "CLUSTER 4", (ftnlen)36, (ftnlen)9);
    c2ncod[37] = -198;
    s_copy(c2nnam + 1332, "NISAR", (ftnlen)36, (ftnlen)5);
    c2ncod[38] = -20;
    s_copy(c2nnam + 1368, "PIONEER-8", (ftnlen)36, (ftnlen)9);
    c2ncod[39] = -200;
    s_copy(c2nnam + 1404, "CONTOUR", (ftnlen)36, (ftnlen)7);
    c2ncod[40] = -202;
    s_copy(c2nnam + 1440, "MAVEN", (ftnlen)36, (ftnlen)5);
    c2ncod[41] = -203;
    s_copy(c2nnam + 1476, "DAWN", (ftnlen)36, (ftnlen)4);
    c2ncod[42] = -205;
    s_copy(c2nnam + 1512, "SMAP", (ftnlen)36, (ftnlen)4);
    c2ncod[43] = -21;
    s_copy(c2nnam + 1548, "SOHO", (ftnlen)36, (ftnlen)4);
    c2ncod[44] = -212;
    s_copy(c2nnam + 1584, "STV51", (ftnlen)36, (ftnlen)5);
    c2ncod[45] = -213;
    s_copy(c2nnam + 1620, "STV52", (ftnlen)36, (ftnlen)5);
    c2ncod[46] = -214;
    s_copy(c2nnam + 1656, "STV53", (ftnlen)36, (ftnlen)5);
    c2ncod[47] = -226;
    s_copy(c2nnam + 1692, "ROSETTA", (ftnlen)36, (ftnlen)7);
    c2ncod[48] = -227;
    s_copy(c2nnam + 1728, "KEPLER", (ftnlen)36, (ftnlen)6);
    c2ncod[49] = -228;
    s_copy(c2nnam + 1764, "GALILEO PROBE", (ftnlen)36, (ftnlen)13);
    c2ncod[50] = -23;
    s_copy(c2nnam + 1800, "PIONEER-10", (ftnlen)36, (ftnlen)10);
    c2ncod[51] = -234;
    s_copy(c2nnam + 1836, "STEREO AHEAD", (ftnlen)36, (ftnlen)12);
    c2ncod[52] = -235;
    s_copy(c2nnam + 1872, "STEREO BEHIND", (ftnlen)36, (ftnlen)13);
    c2ncod[53] = -236;
    s_copy(c2nnam + 1908, "MESSENGER", (ftnlen)36, (ftnlen)9);
    c2ncod[54] = -238;
    s_copy(c2nnam + 1944, "SMART-1", (ftnlen)36, (ftnlen)7);
    c2ncod[55] = -24;
    s_copy(c2nnam + 1980, "PIONEER-11", (ftnlen)36, (ftnlen)10);
    c2ncod[56] = -248;
    s_copy(c2nnam + 2016, "VENUS EXPRESS", (ftnlen)36, (ftnlen)13);
    c2ncod[57] = -25;
    s_copy(c2nnam + 2052, "LUNAR PROSPECTOR", (ftnlen)36, (ftnlen)16);
    c2ncod[58] = -253;
    s_copy(c2nnam + 2088, "MER-1", (ftnlen)36, (ftnlen)5);
    c2ncod[59] = -254;
    s_copy(c2nnam + 2124, "MER-2", (ftnlen)36, (ftnlen)5);
    c2ncod[60] = -27;
    s_copy(c2nnam + 2160, "VIKING 1 ORBITER", (ftnlen)36, (ftnlen)16);
    c2ncod[61] = -28;
    s_copy(c2nnam + 2196, "JUICE", (ftnlen)36, (ftnlen)5);
    c2ncod[62] = -29;
    s_copy(c2nnam + 2232, "NEXT", (ftnlen)36, (ftnlen)4);
    c2ncod[63] = -3;
    s_copy(c2nnam + 2268, "MARS ORBITER MISSION", (ftnlen)36, (ftnlen)20);
    c2ncod[64] = -30;
    s_copy(c2nnam + 2304, "DS-1", (ftnlen)36, (ftnlen)4);
    c2ncod[65] = -301;
    s_copy(c2nnam + 2340, "HELIOS 1", (ftnlen)36, (ftnlen)8);
    c2ncod[66] = -302;
    s_copy(c2nnam + 2376, "HELIOS 2", (ftnlen)36, (ftnlen)8);
    c2ncod[67] = -31;
    s_copy(c2nnam + 2412, "VOYAGER 1", (ftnlen)36, (ftnlen)9);
    c2ncod[68] = -32;
    s_copy(c2nnam + 2448, "VOYAGER 2", (ftnlen)36, (ftnlen)9);
    c2ncod[69] = -362;
    s_copy(c2nnam + 2484, "RBSP_A", (ftnlen)36, (ftnlen)6);
    c2ncod[70] = -363;
    s_copy(c2nnam + 2520, "RBSP_B", (ftnlen)36, (ftnlen)6);
    c2ncod[71] = -40;
    s_copy(c2nnam + 2556, "CLEMENTINE", (ftnlen)36, (ftnlen)10);
    c2ncod[72] = -41;
    s_copy(c2nnam + 2592, "MARS EXPRESS", (ftnlen)36, (ftnlen)12);
    c2ncod[73] = -44;
    s_copy(c2nnam + 2628, "BEAGLE 2", (ftnlen)36, (ftnlen)8);
    c2ncod[74] = -46;
    s_copy(c2nnam + 2664, "SAKIGAKE", (ftnlen)36, (ftnlen)8);
    c2ncod[75] = -47;
    s_copy(c2nnam + 2700, "GENESIS", (ftnlen)36, (ftnlen)7);
    c2ncod[76] = -48;
    s_copy(c2nnam + 2736, "HST", (ftnlen)36, (ftnlen)3);
    c2ncod[77] = -49;
    s_copy(c2nnam + 2772, "LUCY", (ftnlen)36, (ftnlen)4);
    c2ncod[78] = -5;
    s_copy(c2nnam + 2808, "PLANET-C", (ftnlen)36, (ftnlen)8);
    c2ncod[79] = -500;
    s_copy(c2nnam + 2844, "Rstar", (ftnlen)36, (ftnlen)5);
    c2ncod[80] = -502;
    s_copy(c2nnam + 2880, "Vstar", (ftnlen)36, (ftnlen)5);
    c2ncod[81] = -53;
    s_copy(c2nnam + 2916, "MARS SURVEYOR 01 ORBITER", (ftnlen)36, (ftnlen)24);
    c2ncod[82] = -54;
    s_copy(c2nnam + 2952, "ASTEROID RETRIEVAL MISSION", (ftnlen)36, (ftnlen)
	    26);
    c2ncod[83] = -55;
    s_copy(c2nnam + 2988, "ULYSSES", (ftnlen)36, (ftnlen)7);
    c2ncod[84] = -550;
    s_copy(c2nnam + 3024, "MARS96", (ftnlen)36, (ftnlen)6);
    c2ncod[85] = -58;
    s_copy(c2nnam + 3060, "HALCA", (ftnlen)36, (ftnlen)5);
    c2ncod[86] = -59;
    s_copy(c2nnam + 3096, "RADIOASTRON", (ftnlen)36, (ftnlen)11);
    c2ncod[87] = -6;
    s_copy(c2nnam + 3132, "PIONEER-6", (ftnlen)36, (ftnlen)9);
    c2ncod[88] = -61;
    s_copy(c2nnam + 3168, "JUNO", (ftnlen)36, (ftnlen)4);
    c2ncod[89] = -62;
    s_copy(c2nnam + 3204, "EMIRATES MARS MISSION", (ftnlen)36, (ftnlen)21);
    c2ncod[90] = -64;
    s_copy(c2nnam + 3240, "OSIRIS-REX", (ftnlen)36, (ftnlen)10);
    c2ncod[91] = -65;
    s_copy(c2nnam + 3276, "MARCO-A", (ftnlen)36, (ftnlen)7);
    c2ncod[92] = -66;
    s_copy(c2nnam + 3312, "MARCO-B", (ftnlen)36, (ftnlen)7);
    c2ncod[93] = -67;
    s_copy(c2nnam + 3348, "VEGA 2", (ftnlen)36, (ftnlen)6);
    c2ncod[94] = -68;
    s_copy(c2nnam + 3384, "BEPICOLOMBO MMO", (ftnlen)36, (ftnlen)15);
    c2ncod[95] = -69;
    s_copy(c2nnam + 3420, "PSYC", (ftnlen)36, (ftnlen)4);
    c2ncod[96] = -7;
    s_copy(c2nnam + 3456, "PIONEER-7", (ftnlen)36, (ftnlen)9);
    c2ncod[97] = -70;
    s_copy(c2nnam + 3492, "DEEP IMPACT IMPACTOR SPACECRAFT", (ftnlen)36, (
	    ftnlen)31);
    c2ncod[98] = -74;
    s_copy(c2nnam + 3528, "MARS RECON ORBITER", (ftnlen)36, (ftnlen)18);
    c2ncod[99] = -750;
    s_copy(c2nnam + 3564, "SPRINT-A", (ftnlen)36, (ftnlen)8);
    c2ncod[100] = -76;
    s_copy(c2nnam + 3600, "MARS SCIENCE LABORATORY", (ftnlen)36, (ftnlen)23);
    c2ncod[101] = -77;
    s_copy(c2nnam + 3636, "GALILEO ORBITER", (ftnlen)36, (ftnlen)15);
    c2ncod[102] = -78;
    s_copy(c2nnam + 3672, "GIOTTO", (ftnlen)36, (ftnlen)6);
    c2ncod[103] = -79;
    s_copy(c2nnam + 3708, "SIRTF", (ftnlen)36, (ftnlen)5);
    c2ncod[104] = -8;
    s_copy(c2nnam + 3744, "WIND", (ftnlen)36, (ftnlen)4);
    c2ncod[105] = -81;
    s_copy(c2nnam + 3780, "CASSINI ITL", (ftnlen)36, (ftnlen)11);
    c2ncod[106] = -82;
    s_copy(c2nnam + 3816, "CASSINI", (ftnlen)36, (ftnlen)7);
    c2ncod[107] = -84;
    s_copy(c2nnam + 3852, "PHOENIX", (ftnlen)36, (ftnlen)7);
    c2ncod[108] = -85;
    s_copy(c2nnam + 3888, "LUNAR RECONNAISSANCE ORBITER", (ftnlen)36, (ftnlen)
	    28);
    c2ncod[109] = -86;
    s_copy(c2nnam + 3924, "CHANDRAYAAN-1", (ftnlen)36, (ftnlen)13);
    c2ncod[110] = -90;
    s_copy(c2nnam + 3960, "CASSINI SIMULATION", (ftnlen)36, (ftnlen)18);
    c2ncod[111] = -93;
    s_copy(c2nnam + 3996, "NEAR", (ftnlen)36, (ftnlen)4);
    c2ncod[112] = -94;
    s_copy(c2nnam + 4032, "MARS GLOBAL SURVEYOR", (ftnlen)36, (ftnlen)20);
    c2ncod[113] = -95;
    s_copy(c2nnam + 4068, "MGS SIMULATION", (ftnlen)36, (ftnlen)14);
    c2ncod[114] = -96;
    s_copy(c2nnam + 4104, "SOLAR PROBE PLUS", (ftnlen)36, (ftnlen)16);
    c2ncod[115] = -97;
    s_copy(c2nnam + 4140, "TOPEX/POSEIDON", (ftnlen)36, (ftnlen)14);
    c2ncod[116] = -98;
    s_copy(c2nnam + 4176, "NEW HORIZONS", (ftnlen)36, (ftnlen)12);
    c2ncod[117] = 0;
    s_copy(c2nnam + 4212, "SOLAR SYSTEM BARYCENTER", (ftnlen)36, (ftnlen)23);
    c2ncod[118] = 1;
    s_copy(c2nnam + 4248, "MERCURY BARYCENTER", (ftnlen)36, (ftnlen)18);
    c2ncod[119] = 10;
    s_copy(c2nnam + 4284, "SUN", (ftnlen)36, (ftnlen)3);
    c2ncod[120] = 1000001;
    s_copy(c2nnam + 4320, "AREND", (ftnlen)36, (ftnlen)5);
    c2ncod[121] = 1000002;
    s_copy(c2nnam + 4356, "AREND-RIGAUX", (ftnlen)36, (ftnlen)12);
    c2ncod[122] = 1000003;
    s_copy(c2nnam + 4392, "ASHBROOK-JACKSON", (ftnlen)36, (ftnlen)16);
    c2ncod[123] = 1000004;
    s_copy(c2nnam + 4428, "BOETHIN", (ftnlen)36, (ftnlen)7);
    c2ncod[124] = 1000005;
    s_copy(c2nnam + 4464, "BORRELLY", (ftnlen)36, (ftnlen)8);
    c2ncod[125] = 1000006;
    s_copy(c2nnam + 4500, "BOWELL-SKIFF", (ftnlen)36, (ftnlen)12);
    c2ncod[126] = 1000007;
    s_copy(c2nnam + 4536, "BRADFIELD", (ftnlen)36, (ftnlen)9);
    c2ncod[127] = 1000008;
    s_copy(c2nnam + 4572, "BROOKS 2", (ftnlen)36, (ftnlen)8);
    c2ncod[128] = 1000009;
    s_copy(c2nnam + 4608, "BRORSEN-METCALF", (ftnlen)36, (ftnlen)15);
    c2ncod[129] = 1000010;
    s_copy(c2nnam + 4644, "BUS", (ftnlen)36, (ftnlen)3);
    c2ncod[130] = 1000011;
    s_copy(c2nnam + 4680, "CHERNYKH", (ftnlen)36, (ftnlen)8);
    c2ncod[131] = 1000012;
    s_copy(c2nnam + 4716, "CHURYUMOV-GERASIMENKO", (ftnlen)36, (ftnlen)21);
    c2ncod[132] = 1000013;
    s_copy(c2nnam + 4752, "CIFFREO", (ftnlen)36, (ftnlen)7);
    c2ncod[133] = 1000014;
    s_copy(c2nnam + 4788, "CLARK", (ftnlen)36, (ftnlen)5);
    c2ncod[134] = 1000015;
    s_copy(c2nnam + 4824, "COMAS SOLA", (ftnlen)36, (ftnlen)10);
    c2ncod[135] = 1000016;
    s_copy(c2nnam + 4860, "CROMMELIN", (ftnlen)36, (ftnlen)9);
    c2ncod[136] = 1000017;
    s_copy(c2nnam + 4896, "D'ARREST", (ftnlen)36, (ftnlen)8);
    c2ncod[137] = 1000018;
    s_copy(c2nnam + 4932, "DANIEL", (ftnlen)36, (ftnlen)6);
    c2ncod[138] = 1000019;
    s_copy(c2nnam + 4968, "DE VICO-SWIFT", (ftnlen)36, (ftnlen)13);
    c2ncod[139] = 1000020;
    s_copy(c2nnam + 5004, "DENNING-FUJIKAWA", (ftnlen)36, (ftnlen)16);
    c2ncod[140] = 1000021;
    s_copy(c2nnam + 5040, "DU TOIT 1", (ftnlen)36, (ftnlen)9);
    c2ncod[141] = 1000022;
    s_copy(c2nnam + 5076, "DU TOIT-HARTLEY", (ftnlen)36, (ftnlen)15);
    c2ncod[142] = 1000023;
    s_copy(c2nnam + 5112, "DUTOIT-NEUJMIN-DELPORTE", (ftnlen)36, (ftnlen)23);
    c2ncod[143] = 1000024;
    s_copy(c2nnam + 5148, "DUBIAGO", (ftnlen)36, (ftnlen)7);
    c2ncod[144] = 1000025;
    s_copy(c2nnam + 5184, "ENCKE", (ftnlen)36, (ftnlen)5);
    c2ncod[145] = 1000026;
    s_copy(c2nnam + 5220, "FAYE", (ftnlen)36, (ftnlen)4);
    c2ncod[146] = 1000027;
    s_copy(c2nnam + 5256, "FINLAY", (ftnlen)36, (ftnlen)6);
    c2ncod[147] = 1000028;
    s_copy(c2nnam + 5292, "FORBES", (ftnlen)36, (ftnlen)6);
    c2ncod[148] = 1000029;
    s_copy(c2nnam + 5328, "GEHRELS 1", (ftnlen)36, (ftnlen)9);
    c2ncod[149] = 1000030;
    s_copy(c2nnam + 5364, "GEHRELS 2", (ftnlen)36, (ftnlen)9);
    c2ncod[150] = 1000031;
    s_copy(c2nnam + 5400, "GEHRELS 3", (ftnlen)36, (ftnlen)9);
    c2ncod[151] = 1000032;
    s_copy(c2nnam + 5436, "GIACOBINI-ZINNER", (ftnlen)36, (ftnlen)16);
    c2ncod[152] = 1000033;
    s_copy(c2nnam + 5472, "GICLAS", (ftnlen)36, (ftnlen)6);
    c2ncod[153] = 1000034;
    s_copy(c2nnam + 5508, "GRIGG-SKJELLERUP", (ftnlen)36, (ftnlen)16);
    c2ncod[154] = 1000035;
    s_copy(c2nnam + 5544, "GUNN", (ftnlen)36, (ftnlen)4);
    c2ncod[155] = 1000036;
    s_copy(c2nnam + 5580, "HALLEY", (ftnlen)36, (ftnlen)6);
    c2ncod[156] = 1000037;
    s_copy(c2nnam + 5616, "HANEDA-CAMPOS", (ftnlen)36, (ftnlen)13);
    c2ncod[157] = 1000038;
    s_copy(c2nnam + 5652, "HARRINGTON", (ftnlen)36, (ftnlen)10);
    c2ncod[158] = 1000039;
    s_copy(c2nnam + 5688, "HARRINGTON-ABELL", (ftnlen)36, (ftnlen)16);
    c2ncod[159] = 1000040;
    s_copy(c2nnam + 5724, "HARTLEY 1", (ftnlen)36, (ftnlen)9);
    c2ncod[160] = 1000041;
    s_copy(c2nnam + 5760, "HARTLEY 2", (ftnlen)36, (ftnlen)9);
    c2ncod[161] = 1000042;
    s_copy(c2nnam + 5796, "HARTLEY-IRAS", (ftnlen)36, (ftnlen)12);
    c2ncod[162] = 1000043;
    s_copy(c2nnam + 5832, "HERSCHEL-RIGOLLET", (ftnlen)36, (ftnlen)17);
    c2ncod[163] = 1000044;
    s_copy(c2nnam + 5868, "HOLMES", (ftnlen)36, (ftnlen)6);
    c2ncod[164] = 1000045;
    s_copy(c2nnam + 5904, "HONDA-MRKOS-PAJDUSAKOVA", (ftnlen)36, (ftnlen)23);
    c2ncod[165] = 1000046;
    s_copy(c2nnam + 5940, "HOWELL", (ftnlen)36, (ftnlen)6);
    c2ncod[166] = 1000047;
    s_copy(c2nnam + 5976, "IRAS", (ftnlen)36, (ftnlen)4);
    c2ncod[167] = 1000048;
    s_copy(c2nnam + 6012, "JACKSON-NEUJMIN", (ftnlen)36, (ftnlen)15);
    c2ncod[168] = 1000049;
    s_copy(c2nnam + 6048, "JOHNSON", (ftnlen)36, (ftnlen)7);
    c2ncod[169] = 1000050;
    s_copy(c2nnam + 6084, "KEARNS-KWEE", (ftnlen)36, (ftnlen)11);
    c2ncod[170] = 1000051;
    s_copy(c2nnam + 6120, "KLEMOLA", (ftnlen)36, (ftnlen)7);
    c2ncod[171] = 1000052;
    s_copy(c2nnam + 6156, "KOHOUTEK", (ftnlen)36, (ftnlen)8);
    c2ncod[172] = 1000053;
    s_copy(c2nnam + 6192, "KOJIMA", (ftnlen)36, (ftnlen)6);
    c2ncod[173] = 1000054;
    s_copy(c2nnam + 6228, "KOPFF", (ftnlen)36, (ftnlen)5);
    c2ncod[174] = 1000055;
    s_copy(c2nnam + 6264, "KOWAL 1", (ftnlen)36, (ftnlen)7);
    c2ncod[175] = 1000056;
    s_copy(c2nnam + 6300, "KOWAL 2", (ftnlen)36, (ftnlen)7);
    c2ncod[176] = 1000057;
    s_copy(c2nnam + 6336, "KOWAL-MRKOS", (ftnlen)36, (ftnlen)11);
    c2ncod[177] = 1000058;
    s_copy(c2nnam + 6372, "KOWAL-VAVROVA", (ftnlen)36, (ftnlen)13);
    c2ncod[178] = 1000059;
    s_copy(c2nnam + 6408, "LONGMORE", (ftnlen)36, (ftnlen)8);
    c2ncod[179] = 1000060;
    s_copy(c2nnam + 6444, "LOVAS 1", (ftnlen)36, (ftnlen)7);
    c2ncod[180] = 1000061;
    s_copy(c2nnam + 6480, "MACHHOLZ", (ftnlen)36, (ftnlen)8);
    c2ncod[181] = 1000062;
    s_copy(c2nnam + 6516, "MAURY", (ftnlen)36, (ftnlen)5);
    c2ncod[182] = 1000063;
    s_copy(c2nnam + 6552, "NEUJMIN 1", (ftnlen)36, (ftnlen)9);
    c2ncod[183] = 1000064;
    s_copy(c2nnam + 6588, "NEUJMIN 2", (ftnlen)36, (ftnlen)9);
    c2ncod[184] = 1000065;
    s_copy(c2nnam + 6624, "NEUJMIN 3", (ftnlen)36, (ftnlen)9);
    c2ncod[185] = 1000066;
    s_copy(c2nnam + 6660, "OLBERS", (ftnlen)36, (ftnlen)6);
    c2ncod[186] = 1000067;
    s_copy(c2nnam + 6696, "PETERS-HARTLEY", (ftnlen)36, (ftnlen)14);
    c2ncod[187] = 1000068;
    s_copy(c2nnam + 6732, "PONS-BROOKS", (ftnlen)36, (ftnlen)11);
    c2ncod[188] = 1000069;
    s_copy(c2nnam + 6768, "PONS-WINNECKE", (ftnlen)36, (ftnlen)13);
    c2ncod[189] = 1000070;
    s_copy(c2nnam + 6804, "REINMUTH 1", (ftnlen)36, (ftnlen)10);
    c2ncod[190] = 1000071;
    s_copy(c2nnam + 6840, "REINMUTH 2", (ftnlen)36, (ftnlen)10);
    c2ncod[191] = 1000072;
    s_copy(c2nnam + 6876, "RUSSELL 1", (ftnlen)36, (ftnlen)9);
    c2ncod[192] = 1000073;
    s_copy(c2nnam + 6912, "RUSSELL 2", (ftnlen)36, (ftnlen)9);
    c2ncod[193] = 1000074;
    s_copy(c2nnam + 6948, "RUSSELL 3", (ftnlen)36, (ftnlen)9);
    c2ncod[194] = 1000075;
    s_copy(c2nnam + 6984, "RUSSELL 4", (ftnlen)36, (ftnlen)9);
    c2ncod[195] = 1000076;
    s_copy(c2nnam + 7020, "SANGUIN", (ftnlen)36, (ftnlen)7);
    c2ncod[196] = 1000077;
    s_copy(c2nnam + 7056, "SCHAUMASSE", (ftnlen)36, (ftnlen)10);
    c2ncod[197] = 1000078;
    s_copy(c2nnam + 7092, "SCHUSTER", (ftnlen)36, (ftnlen)8);
    c2ncod[198] = 1000079;
    s_copy(c2nnam + 7128, "SCHWASSMANN-WACHMANN 1", (ftnlen)36, (ftnlen)22);
    c2ncod[199] = 1000080;
    s_copy(c2nnam + 7164, "SCHWASSMANN-WACHMANN 2", (ftnlen)36, (ftnlen)22);
    c2ncod[200] = 1000081;
    s_copy(c2nnam + 7200, "SCHWASSMANN-WACHMANN 3", (ftnlen)36, (ftnlen)22);
    c2ncod[201] = 1000082;
    s_copy(c2nnam + 7236, "SHAJN-SCHALDACH", (ftnlen)36, (ftnlen)15);
    c2ncod[202] = 1000083;
    s_copy(c2nnam + 7272, "SHOEMAKER 1", (ftnlen)36, (ftnlen)11);
    c2ncod[203] = 1000084;
    s_copy(c2nnam + 7308, "SHOEMAKER 2", (ftnlen)36, (ftnlen)11);
    c2ncod[204] = 1000085;
    s_copy(c2nnam + 7344, "SHOEMAKER 3", (ftnlen)36, (ftnlen)11);
    c2ncod[205] = 1000086;
    s_copy(c2nnam + 7380, "SINGER-BREWSTER", (ftnlen)36, (ftnlen)15);
    c2ncod[206] = 1000087;
    s_copy(c2nnam + 7416, "SLAUGHTER-BURNHAM", (ftnlen)36, (ftnlen)17);
    c2ncod[207] = 1000088;
    s_copy(c2nnam + 7452, "SMIRNOVA-CHERNYKH", (ftnlen)36, (ftnlen)17);
    c2ncod[208] = 1000089;
    s_copy(c2nnam + 7488, "STEPHAN-OTERMA", (ftnlen)36, (ftnlen)14);
    c2ncod[209] = 1000090;
    s_copy(c2nnam + 7524, "SWIFT-GEHRELS", (ftnlen)36, (ftnlen)13);
    c2ncod[210] = 1000091;
    s_copy(c2nnam + 7560, "TAKAMIZAWA", (ftnlen)36, (ftnlen)10);
    c2ncod[211] = 1000092;
    s_copy(c2nnam + 7596, "TAYLOR", (ftnlen)36, (ftnlen)6);
    c2ncod[212] = 1000093;
    s_copy(c2nnam + 7632, "TEMPEL 1", (ftnlen)36, (ftnlen)8);
    c2ncod[213] = 1000094;
    s_copy(c2nnam + 7668, "TEMPEL 2", (ftnlen)36, (ftnlen)8);
    c2ncod[214] = 1000095;
    s_copy(c2nnam + 7704, "TEMPEL-TUTTLE", (ftnlen)36, (ftnlen)13);
    c2ncod[215] = 1000096;
    s_copy(c2nnam + 7740, "TRITTON", (ftnlen)36, (ftnlen)7);
    c2ncod[216] = 1000097;
    s_copy(c2nnam + 7776, "TSUCHINSHAN 1", (ftnlen)36, (ftnlen)13);
    c2ncod[217] = 1000098;
    s_copy(c2nnam + 7812, "TSUCHINSHAN 2", (ftnlen)36, (ftnlen)13);
    c2ncod[218] = 1000099;
    s_copy(c2nnam + 7848, "TUTTLE", (ftnlen)36, (ftnlen)6);
    c2ncod[219] = 1000100;
    s_copy(c2nnam + 7884, "TUTTLE-GIACOBINI-KRESAK", (ftnlen)36, (ftnlen)23);
    c2ncod[220] = 1000101;
    s_copy(c2nnam + 7920, "VAISALA 1", (ftnlen)36, (ftnlen)9);
    c2ncod[221] = 1000102;
    s_copy(c2nnam + 7956, "VAN BIESBROECK", (ftnlen)36, (ftnlen)14);
    c2ncod[222] = 1000103;
    s_copy(c2nnam + 7992, "VAN HOUTEN", (ftnlen)36, (ftnlen)10);
    c2ncod[223] = 1000104;
    s_copy(c2nnam + 8028, "WEST-KOHOUTEK-IKEMURA", (ftnlen)36, (ftnlen)21);
    c2ncod[224] = 1000105;
    s_copy(c2nnam + 8064, "WHIPPLE", (ftnlen)36, (ftnlen)7);
    c2ncod[225] = 1000106;
    s_copy(c2nnam + 8100, "WILD 1", (ftnlen)36, (ftnlen)6);
    c2ncod[226] = 1000107;
    s_copy(c2nnam + 8136, "WILD 2", (ftnlen)36, (ftnlen)6);
    c2ncod[227] = 1000108;
    s_copy(c2nnam + 8172, "WILD 3", (ftnlen)36, (ftnlen)6);
    c2ncod[228] = 1000109;
    s_copy(c2nnam + 8208, "WIRTANEN", (ftnlen)36, (ftnlen)8);
    c2ncod[229] = 1000110;
    s_copy(c2nnam + 8244, "WOLF", (ftnlen)36, (ftnlen)4);
    c2ncod[230] = 1000111;
    s_copy(c2nnam + 8280, "WOLF-HARRINGTON", (ftnlen)36, (ftnlen)15);
    c2ncod[231] = 1000112;
    s_copy(c2nnam + 8316, "LOVAS 2", (ftnlen)36, (ftnlen)7);
    c2ncod[232] = 1000113;
    s_copy(c2nnam + 8352, "URATA-NIIJIMA", (ftnlen)36, (ftnlen)13);
    c2ncod[233] = 1000114;
    s_copy(c2nnam + 8388, "WISEMAN-SKIFF", (ftnlen)36, (ftnlen)13);
    c2ncod[234] = 1000115;
    s_copy(c2nnam + 8424, "HELIN", (ftnlen)36, (ftnlen)5);
    c2ncod[235] = 1000116;
    s_copy(c2nnam + 8460, "MUELLER", (ftnlen)36, (ftnlen)7);
    c2ncod[236] = 1000117;
    s_copy(c2nnam + 8496, "SHOEMAKER-HOLT 1", (ftnlen)36, (ftnlen)16);
    c2ncod[237] = 1000118;
    s_copy(c2nnam + 8532, "HELIN-ROMAN-CROCKETT", (ftnlen)36, (ftnlen)20);
    c2ncod[238] = 1000119;
    s_copy(c2nnam + 8568, "HARTLEY 3", (ftnlen)36, (ftnlen)9);
    c2ncod[239] = 1000120;
    s_copy(c2nnam + 8604, "PARKER-HARTLEY", (ftnlen)36, (ftnlen)14);
    c2ncod[240] = 1000121;
    s_copy(c2nnam + 8640, "HELIN-ROMAN-ALU 1", (ftnlen)36, (ftnlen)17);
    c2ncod[241] = 1000122;
    s_copy(c2nnam + 8676, "WILD 4", (ftnlen)36, (ftnlen)6);
    c2ncod[242] = 1000123;
    s_copy(c2nnam + 8712, "MUELLER 2", (ftnlen)36, (ftnlen)9);
    c2ncod[243] = 1000124;
    s_copy(c2nnam + 8748, "MUELLER 3", (ftnlen)36, (ftnlen)9);
    c2ncod[244] = 1000125;
    s_copy(c2nnam + 8784, "SHOEMAKER-LEVY 1", (ftnlen)36, (ftnlen)16);
    c2ncod[245] = 1000126;
    s_copy(c2nnam + 8820, "SHOEMAKER-LEVY 2", (ftnlen)36, (ftnlen)16);
    c2ncod[246] = 1000127;
    s_copy(c2nnam + 8856, "HOLT-OLMSTEAD", (ftnlen)36, (ftnlen)13);
    c2ncod[247] = 1000128;
    s_copy(c2nnam + 8892, "METCALF-BREWINGTON", (ftnlen)36, (ftnlen)18);
    c2ncod[248] = 1000129;
    s_copy(c2nnam + 8928, "LEVY", (ftnlen)36, (ftnlen)4);
    c2ncod[249] = 1000130;
    s_copy(c2nnam + 8964, "SHOEMAKER-LEVY 9", (ftnlen)36, (ftnlen)16);
    c2ncod[250] = 1000131;
    s_copy(c2nnam + 9000, "HYAKUTAKE", (ftnlen)36, (ftnlen)9);
    c2ncod[251] = 1000132;
    s_copy(c2nnam + 9036, "HALE-BOPP", (ftnlen)36, (ftnlen)9);
    c2ncod[252] = 1003228;
    s_copy(c2nnam + 9072, "SIDING SPRING", (ftnlen)36, (ftnlen)13);
    c2ncod[253] = 199;
    s_copy(c2nnam + 9108, "MERCURY", (ftnlen)36, (ftnlen)7);
    c2ncod[254] = 2;
    s_copy(c2nnam + 9144, "VENUS BARYCENTER", (ftnlen)36, (ftnlen)16);
    c2ncod[255] = 2000001;
    s_copy(c2nnam + 9180, "CERES", (ftnlen)36, (ftnlen)5);
    c2ncod[256] = 2000002;
    s_copy(c2nnam + 9216, "PALLAS", (ftnlen)36, (ftnlen)6);
    c2ncod[257] = 2000004;
    s_copy(c2nnam + 9252, "VESTA", (ftnlen)36, (ftnlen)5);
    c2ncod[258] = 2000016;
    s_copy(c2nnam + 9288, "PSYCHE", (ftnlen)36, (ftnlen)6);
    c2ncod[259] = 2000021;
    s_copy(c2nnam + 9324, "LUTETIA", (ftnlen)36, (ftnlen)7);
    c2ncod[260] = 2000216;
    s_copy(c2nnam + 9360, "KLEOPATRA", (ftnlen)36, (ftnlen)9);
    c2ncod[261] = 2000253;
    s_copy(c2nnam + 9396, "MATHILDE", (ftnlen)36, (ftnlen)8);
    c2ncod[262] = 2000433;
    s_copy(c2nnam + 9432, "EROS", (ftnlen)36, (ftnlen)4);
    c2ncod[263] = 2000511;
    s_copy(c2nnam + 9468, "DAVIDA", (ftnlen)36, (ftnlen)6);
    c2ncod[264] = 2002867;
    s_copy(c2nnam + 9504, "STEINS", (ftnlen)36, (ftnlen)6);
    c2ncod[265] = 2004015;
    s_copy(c2nnam + 9540, "WILSON-HARRINGTON", (ftnlen)36, (ftnlen)17);
    c2ncod[266] = 2004179;
    s_copy(c2nnam + 9576, "TOUTATIS", (ftnlen)36, (ftnlen)8);
    c2ncod[267] = 2009969;
    s_copy(c2nnam + 9612, "BRAILLE", (ftnlen)36, (ftnlen)7);
    c2ncod[268] = 2025143;
    s_copy(c2nnam + 9648, "ITOKAWA", (ftnlen)36, (ftnlen)7);
    c2ncod[269] = 2101955;
    s_copy(c2nnam + 9684, "BENNU", (ftnlen)36, (ftnlen)5);
    c2ncod[270] = 2431010;
    s_copy(c2nnam + 9720, "IDA", (ftnlen)36, (ftnlen)3);
    c2ncod[271] = 2431011;
    s_copy(c2nnam + 9756, "DACTYL", (ftnlen)36, (ftnlen)6);
    c2ncod[272] = 299;
    s_copy(c2nnam + 9792, "VENUS", (ftnlen)36, (ftnlen)5);
    c2ncod[273] = 3;
    s_copy(c2nnam + 9828, "EARTH BARYCENTER", (ftnlen)36, (ftnlen)16);
    c2ncod[274] = 301;
    s_copy(c2nnam + 9864, "MOON", (ftnlen)36, (ftnlen)4);
    c2ncod[275] = 398989;
    s_copy(c2nnam + 9900, "NOTO", (ftnlen)36, (ftnlen)4);
    c2ncod[276] = 398990;
    s_copy(c2nnam + 9936, "NEW NORCIA", (ftnlen)36, (ftnlen)10);
    c2ncod[277] = 399;
    s_copy(c2nnam + 9972, "EARTH", (ftnlen)36, (ftnlen)5);
    c2ncod[278] = 399001;
    s_copy(c2nnam + 10008, "GOLDSTONE", (ftnlen)36, (ftnlen)9);
    c2ncod[279] = 399002;
    s_copy(c2nnam + 10044, "CANBERRA", (ftnlen)36, (ftnlen)8);
    c2ncod[280] = 399003;
    s_copy(c2nnam + 10080, "MADRID", (ftnlen)36, (ftnlen)6);
    c2ncod[281] = 399004;
    s_copy(c2nnam + 10116, "USUDA", (ftnlen)36, (ftnlen)5);
    c2ncod[282] = 399005;
    s_copy(c2nnam + 10152, "PARKES", (ftnlen)36, (ftnlen)6);
    c2ncod[283] = 399012;
    s_copy(c2nnam + 10188, "DSS-12", (ftnlen)36, (ftnlen)6);
    c2ncod[284] = 399013;
    s_copy(c2nnam + 10224, "DSS-13", (ftnlen)36, (ftnlen)6);
    c2ncod[285] = 399014;
    s_copy(c2nnam + 10260, "DSS-14", (ftnlen)36, (ftnlen)6);
    c2ncod[286] = 399015;
    s_copy(c2nnam + 10296, "DSS-15", (ftnlen)36, (ftnlen)6);
    c2ncod[287] = 399016;
    s_copy(c2nnam + 10332, "DSS-16", (ftnlen)36, (ftnlen)6);
    c2ncod[288] = 399017;
    s_copy(c2nnam + 10368, "DSS-17", (ftnlen)36, (ftnlen)6);
    c2ncod[289] = 399023;
    s_copy(c2nnam + 10404, "DSS-23", (ftnlen)36, (ftnlen)6);
    c2ncod[290] = 399024;
    s_copy(c2nnam + 10440, "DSS-24", (ftnlen)36, (ftnlen)6);
    c2ncod[291] = 399025;
    s_copy(c2nnam + 10476, "DSS-25", (ftnlen)36, (ftnlen)6);
    c2ncod[292] = 399026;
    s_copy(c2nnam + 10512, "DSS-26", (ftnlen)36, (ftnlen)6);
    c2ncod[293] = 399027;
    s_copy(c2nnam + 10548, "DSS-27", (ftnlen)36, (ftnlen)6);
    c2ncod[294] = 399028;
    s_copy(c2nnam + 10584, "DSS-28", (ftnlen)36, (ftnlen)6);
    c2ncod[295] = 399033;
    s_copy(c2nnam + 10620, "DSS-33", (ftnlen)36, (ftnlen)6);
    c2ncod[296] = 399034;
    s_copy(c2nnam + 10656, "DSS-34", (ftnlen)36, (ftnlen)6);
    c2ncod[297] = 399042;
    s_copy(c2nnam + 10692, "DSS-42", (ftnlen)36, (ftnlen)6);
    c2ncod[298] = 399043;
    s_copy(c2nnam + 10728, "DSS-43", (ftnlen)36, (ftnlen)6);
    c2ncod[299] = 399045;
    s_copy(c2nnam + 10764, "DSS-45", (ftnlen)36, (ftnlen)6);
    c2ncod[300] = 399046;
    s_copy(c2nnam + 10800, "DSS-46", (ftnlen)36, (ftnlen)6);
    c2ncod[301] = 399049;
    s_copy(c2nnam + 10836, "DSS-49", (ftnlen)36, (ftnlen)6);
    c2ncod[302] = 399053;
    s_copy(c2nnam + 10872, "DSS-53", (ftnlen)36, (ftnlen)6);
    c2ncod[303] = 399054;
    s_copy(c2nnam + 10908, "DSS-54", (ftnlen)36, (ftnlen)6);
    c2ncod[304] = 399055;
    s_copy(c2nnam + 10944, "DSS-55", (ftnlen)36, (ftnlen)6);
    c2ncod[305] = 399061;
    s_copy(c2nnam + 10980, "DSS-61", (ftnlen)36, (ftnlen)6);
    c2ncod[306] = 399063;
    s_copy(c2nnam + 11016, "DSS-63", (ftnlen)36, (ftnlen)6);
    c2ncod[307] = 399064;
    s_copy(c2nnam + 11052, "DSS-64", (ftnlen)36, (ftnlen)6);
    c2ncod[308] = 399065;
    s_copy(c2nnam + 11088, "DSS-65", (ftnlen)36, (ftnlen)6);
    c2ncod[309] = 399066;
    s_copy(c2nnam + 11124, "DSS-66", (ftnlen)36, (ftnlen)6);
    c2ncod[310] = 4;
    s_copy(c2nnam + 11160, "MARS BARYCENTER", (ftnlen)36, (ftnlen)15);
    c2ncod[311] = 401;
    s_copy(c2nnam + 11196, "PHOBOS", (ftnlen)36, (ftnlen)6);
    c2ncod[312] = 402;
    s_copy(c2nnam + 11232, "DEIMOS", (ftnlen)36, (ftnlen)6);
    c2ncod[313] = 499;
    s_copy(c2nnam + 11268, "MARS", (ftnlen)36, (ftnlen)4);
    c2ncod[314] = 5;
    s_copy(c2nnam + 11304, "JUPITER BARYCENTER", (ftnlen)36, (ftnlen)18);
    c2ncod[315] = 50000001;
    s_copy(c2nnam + 11340, "SHOEMAKER-LEVY 9-W", (ftnlen)36, (ftnlen)18);
    c2ncod[316] = 50000002;
    s_copy(c2nnam + 11376, "SHOEMAKER-LEVY 9-V", (ftnlen)36, (ftnlen)18);
    c2ncod[317] = 50000003;
    s_copy(c2nnam + 11412, "SHOEMAKER-LEVY 9-U", (ftnlen)36, (ftnlen)18);
    c2ncod[318] = 50000004;
    s_copy(c2nnam + 11448, "SHOEMAKER-LEVY 9-T", (ftnlen)36, (ftnlen)18);
    c2ncod[319] = 50000005;
    s_copy(c2nnam + 11484, "SHOEMAKER-LEVY 9-S", (ftnlen)36, (ftnlen)18);
    c2ncod[320] = 50000006;
    s_copy(c2nnam + 11520, "SHOEMAKER-LEVY 9-R", (ftnlen)36, (ftnlen)18);
    c2ncod[321] = 50000007;
    s_copy(c2nnam + 11556, "SHOEMAKER-LEVY 9-Q", (ftnlen)36, (ftnlen)18);
    c2ncod[322] = 50000008;
    s_copy(c2nnam + 11592, "SHOEMAKER-LEVY 9-P", (ftnlen)36, (ftnlen)18);
    c2ncod[323] = 50000009;
    s_copy(c2nnam + 11628, "SHOEMAKER-LEVY 9-N", (ftnlen)36, (ftnlen)18);
    c2ncod[324] = 50000010;
    s_copy(c2nnam + 11664, "SHOEMAKER-LEVY 9-M", (ftnlen)36, (ftnlen)18);
    c2ncod[325] = 50000011;
    s_copy(c2nnam + 11700, "SHOEMAKER-LEVY 9-L", (ftnlen)36, (ftnlen)18);
    c2ncod[326] = 50000012;
    s_copy(c2nnam + 11736, "SHOEMAKER-LEVY 9-K", (ftnlen)36, (ftnlen)18);
    c2ncod[327] = 50000013;
    s_copy(c2nnam + 11772, "SHOEMAKER-LEVY 9-J", (ftnlen)36, (ftnlen)18);
    c2ncod[328] = 50000014;
    s_copy(c2nnam + 11808, "SHOEMAKER-LEVY 9-H", (ftnlen)36, (ftnlen)18);
    c2ncod[329] = 50000015;
    s_copy(c2nnam + 11844, "SHOEMAKER-LEVY 9-G", (ftnlen)36, (ftnlen)18);
    c2ncod[330] = 50000016;
    s_copy(c2nnam + 11880, "SHOEMAKER-LEVY 9-F", (ftnlen)36, (ftnlen)18);
    c2ncod[331] = 50000017;
    s_copy(c2nnam + 11916, "SHOEMAKER-LEVY 9-E", (ftnlen)36, (ftnlen)18);
    c2ncod[332] = 50000018;
    s_copy(c2nnam + 11952, "SHOEMAKER-LEVY 9-D", (ftnlen)36, (ftnlen)18);
    c2ncod[333] = 50000019;
    s_copy(c2nnam + 11988, "SHOEMAKER-LEVY 9-C", (ftnlen)36, (ftnlen)18);
    c2ncod[334] = 50000020;
    s_copy(c2nnam + 12024, "SHOEMAKER-LEVY 9-B", (ftnlen)36, (ftnlen)18);
    c2ncod[335] = 50000021;
    s_copy(c2nnam + 12060, "SHOEMAKER-LEVY 9-A", (ftnlen)36, (ftnlen)18);
    c2ncod[336] = 50000022;
    s_copy(c2nnam + 12096, "SHOEMAKER-LEVY 9-Q1", (ftnlen)36, (ftnlen)19);
    c2ncod[337] = 50000023;
    s_copy(c2nnam + 12132, "SHOEMAKER-LEVY 9-P2", (ftnlen)36, (ftnlen)19);
    c2ncod[338] = 501;
    s_copy(c2nnam + 12168, "IO", (ftnlen)36, (ftnlen)2);
    c2ncod[339] = 502;
    s_copy(c2nnam + 12204, "EUROPA", (ftnlen)36, (ftnlen)6);
    c2ncod[340] = 503;
    s_copy(c2nnam + 12240, "GANYMEDE", (ftnlen)36, (ftnlen)8);
    c2ncod[341] = 504;
    s_copy(c2nnam + 12276, "CALLISTO", (ftnlen)36, (ftnlen)8);
    c2ncod[342] = 505;
    s_copy(c2nnam + 12312, "AMALTHEA", (ftnlen)36, (ftnlen)8);
    c2ncod[343] = 506;
    s_copy(c2nnam + 12348, "HIMALIA", (ftnlen)36, (ftnlen)7);
    c2ncod[344] = 507;
    s_copy(c2nnam + 12384, "ELARA", (ftnlen)36, (ftnlen)5);
    c2ncod[345] = 508;
    s_copy(c2nnam + 12420, "PASIPHAE", (ftnlen)36, (ftnlen)8);
    c2ncod[346] = 509;
    s_copy(c2nnam + 12456, "SINOPE", (ftnlen)36, (ftnlen)6);
    c2ncod[347] = 510;
    s_copy(c2nnam + 12492, "LYSITHEA", (ftnlen)36, (ftnlen)8);
    c2ncod[348] = 511;
    s_copy(c2nnam + 12528, "CARME", (ftnlen)36, (ftnlen)5);
    c2ncod[349] = 512;
    s_copy(c2nnam + 12564, "ANANKE", (ftnlen)36, (ftnlen)6);
    c2ncod[350] = 513;
    s_copy(c2nnam + 12600, "LEDA", (ftnlen)36, (ftnlen)4);
    c2ncod[351] = 514;
    s_copy(c2nnam + 12636, "THEBE", (ftnlen)36, (ftnlen)5);
    c2ncod[352] = 515;
    s_copy(c2nnam + 12672, "ADRASTEA", (ftnlen)36, (ftnlen)8);
    c2ncod[353] = 516;
    s_copy(c2nnam + 12708, "METIS", (ftnlen)36, (ftnlen)5);
    c2ncod[354] = 517;
    s_copy(c2nnam + 12744, "CALLIRRHOE", (ftnlen)36, (ftnlen)10);
    c2ncod[355] = 518;
    s_copy(c2nnam + 12780, "THEMISTO", (ftnlen)36, (ftnlen)8);
    c2ncod[356] = 519;
    s_copy(c2nnam + 12816, "MAGACLITE", (ftnlen)36, (ftnlen)9);
    c2ncod[357] = 520;
    s_copy(c2nnam + 12852, "TAYGETE", (ftnlen)36, (ftnlen)7);
    c2ncod[358] = 521;
    s_copy(c2nnam + 12888, "CHALDENE", (ftnlen)36, (ftnlen)8);
    c2ncod[359] = 522;
    s_copy(c2nnam + 12924, "HARPALYKE", (ftnlen)36, (ftnlen)9);
    c2ncod[360] = 523;
    s_copy(c2nnam + 12960, "KALYKE", (ftnlen)36, (ftnlen)6);
    c2ncod[361] = 524;
    s_copy(c2nnam + 12996, "IOCASTE", (ftnlen)36, (ftnlen)7);
    c2ncod[362] = 525;
    s_copy(c2nnam + 13032, "ERINOME", (ftnlen)36, (ftnlen)7);
    c2ncod[363] = 526;
    s_copy(c2nnam + 13068, "ISONOE", (ftnlen)36, (ftnlen)6);
    c2ncod[364] = 527;
    s_copy(c2nnam + 13104, "PRAXIDIKE", (ftnlen)36, (ftnlen)9);
    c2ncod[365] = 528;
    s_copy(c2nnam + 13140, "AUTONOE", (ftnlen)36, (ftnlen)7);
    c2ncod[366] = 529;
    s_copy(c2nnam + 13176, "THYONE", (ftnlen)36, (ftnlen)6);
    c2ncod[367] = 530;
    s_copy(c2nnam + 13212, "HERMIPPE", (ftnlen)36, (ftnlen)8);
    c2ncod[368] = 531;
    s_copy(c2nnam + 13248, "AITNE", (ftnlen)36, (ftnlen)5);
    c2ncod[369] = 532;
    s_copy(c2nnam + 13284, "EURYDOME", (ftnlen)36, (ftnlen)8);
    c2ncod[370] = 533;
    s_copy(c2nnam + 13320, "EUANTHE", (ftnlen)36, (ftnlen)7);
    c2ncod[371] = 534;
    s_copy(c2nnam + 13356, "EUPORIE", (ftnlen)36, (ftnlen)7);
    c2ncod[372] = 535;
    s_copy(c2nnam + 13392, "ORTHOSIE", (ftnlen)36, (ftnlen)8);
    c2ncod[373] = 536;
    s_copy(c2nnam + 13428, "SPONDE", (ftnlen)36, (ftnlen)6);
    c2ncod[374] = 537;
    s_copy(c2nnam + 13464, "KALE", (ftnlen)36, (ftnlen)4);
    c2ncod[375] = 538;
    s_copy(c2nnam + 13500, "PASITHEE", (ftnlen)36, (ftnlen)8);
    c2ncod[376] = 539;
    s_copy(c2nnam + 13536, "HEGEMONE", (ftnlen)36, (ftnlen)8);
    c2ncod[377] = 540;
    s_copy(c2nnam + 13572, "MNEME", (ftnlen)36, (ftnlen)5);
    c2ncod[378] = 541;
    s_copy(c2nnam + 13608, "AOEDE", (ftnlen)36, (ftnlen)5);
    c2ncod[379] = 542;
    s_copy(c2nnam + 13644, "THELXINOE", (ftnlen)36, (ftnlen)9);
    c2ncod[380] = 543;
    s_copy(c2nnam + 13680, "ARCHE", (ftnlen)36, (ftnlen)5);
    c2ncod[381] = 544;
    s_copy(c2nnam + 13716, "KALLICHORE", (ftnlen)36, (ftnlen)10);
    c2ncod[382] = 545;
    s_copy(c2nnam + 13752, "HELIKE", (ftnlen)36, (ftnlen)6);
    c2ncod[383] = 546;
    s_copy(c2nnam + 13788, "CARPO", (ftnlen)36, (ftnlen)5);
    c2ncod[384] = 547;
    s_copy(c2nnam + 13824, "EUKELADE", (ftnlen)36, (ftnlen)8);
    c2ncod[385] = 548;
    s_copy(c2nnam + 13860, "CYLLENE", (ftnlen)36, (ftnlen)7);
    c2ncod[386] = 549;
    s_copy(c2nnam + 13896, "KORE", (ftnlen)36, (ftnlen)4);
    c2ncod[387] = 550;
    s_copy(c2nnam + 13932, "HERSE", (ftnlen)36, (ftnlen)5);
    c2ncod[388] = 553;
    s_copy(c2nnam + 13968, "DIA", (ftnlen)36, (ftnlen)3);
    c2ncod[389] = 599;
    s_copy(c2nnam + 14004, "JUPITER", (ftnlen)36, (ftnlen)7);
    c2ncod[390] = 6;
    s_copy(c2nnam + 14040, "SATURN BARYCENTER", (ftnlen)36, (ftnlen)17);
    c2ncod[391] = 601;
    s_copy(c2nnam + 14076, "MIMAS", (ftnlen)36, (ftnlen)5);
    c2ncod[392] = 602;
    s_copy(c2nnam + 14112, "ENCELADUS", (ftnlen)36, (ftnlen)9);
    c2ncod[393] = 603;
    s_copy(c2nnam + 14148, "TETHYS", (ftnlen)36, (ftnlen)6);
    c2ncod[394] = 604;
    s_copy(c2nnam + 14184, "DIONE", (ftnlen)36, (ftnlen)5);
    c2ncod[395] = 605;
    s_copy(c2nnam + 14220, "RHEA", (ftnlen)36, (ftnlen)4);
    c2ncod[396] = 606;
    s_copy(c2nnam + 14256, "TITAN", (ftnlen)36, (ftnlen)5);
    c2ncod[397] = 607;
    s_copy(c2nnam + 14292, "HYPERION", (ftnlen)36, (ftnlen)8);
    c2ncod[398] = 608;
    s_copy(c2nnam + 14328, "IAPETUS", (ftnlen)36, (ftnlen)7);
    c2ncod[399] = 609;
    s_copy(c2nnam + 14364, "PHOEBE", (ftnlen)36, (ftnlen)6);
    c2ncod[400] = 610;
    s_copy(c2nnam + 14400, "JANUS", (ftnlen)36, (ftnlen)5);
    c2ncod[401] = 611;
    s_copy(c2nnam + 14436, "EPIMETHEUS", (ftnlen)36, (ftnlen)10);
    c2ncod[402] = 612;
    s_copy(c2nnam + 14472, "HELENE", (ftnlen)36, (ftnlen)6);
    c2ncod[403] = 613;
    s_copy(c2nnam + 14508, "TELESTO", (ftnlen)36, (ftnlen)7);
    c2ncod[404] = 614;
    s_copy(c2nnam + 14544, "CALYPSO", (ftnlen)36, (ftnlen)7);
    c2ncod[405] = 615;
    s_copy(c2nnam + 14580, "ATLAS", (ftnlen)36, (ftnlen)5);
    c2ncod[406] = 616;
    s_copy(c2nnam + 14616, "PROMETHEUS", (ftnlen)36, (ftnlen)10);
    c2ncod[407] = 617;
    s_copy(c2nnam + 14652, "PANDORA", (ftnlen)36, (ftnlen)7);
    c2ncod[408] = 618;
    s_copy(c2nnam + 14688, "PAN", (ftnlen)36, (ftnlen)3);
    c2ncod[409] = 619;
    s_copy(c2nnam + 14724, "YMIR", (ftnlen)36, (ftnlen)4);
    c2ncod[410] = 620;
    s_copy(c2nnam + 14760, "PAALIAQ", (ftnlen)36, (ftnlen)7);
    c2ncod[411] = 621;
    s_copy(c2nnam + 14796, "TARVOS", (ftnlen)36, (ftnlen)6);
    c2ncod[412] = 622;
    s_copy(c2nnam + 14832, "IJIRAQ", (ftnlen)36, (ftnlen)6);
    c2ncod[413] = 623;
    s_copy(c2nnam + 14868, "SUTTUNGR", (ftnlen)36, (ftnlen)8);
    c2ncod[414] = 624;
    s_copy(c2nnam + 14904, "KIVIUQ", (ftnlen)36, (ftnlen)6);
    c2ncod[415] = 625;
    s_copy(c2nnam + 14940, "MUNDILFARI", (ftnlen)36, (ftnlen)10);
    c2ncod[416] = 626;
    s_copy(c2nnam + 14976, "ALBIORIX", (ftnlen)36, (ftnlen)8);
    c2ncod[417] = 627;
    s_copy(c2nnam + 15012, "SKATHI", (ftnlen)36, (ftnlen)6);
    c2ncod[418] = 628;
    s_copy(c2nnam + 15048, "ERRIAPUS", (ftnlen)36, (ftnlen)8);
    c2ncod[419] = 629;
    s_copy(c2nnam + 15084, "SIARNAQ", (ftnlen)36, (ftnlen)7);
    c2ncod[420] = 630;
    s_copy(c2nnam + 15120, "THRYMR", (ftnlen)36, (ftnlen)6);
    c2ncod[421] = 631;
    s_copy(c2nnam + 15156, "NARVI", (ftnlen)36, (ftnlen)5);
    c2ncod[422] = 632;
    s_copy(c2nnam + 15192, "METHONE", (ftnlen)36, (ftnlen)7);
    c2ncod[423] = 633;
    s_copy(c2nnam + 15228, "PALLENE", (ftnlen)36, (ftnlen)7);
    c2ncod[424] = 634;
    s_copy(c2nnam + 15264, "POLYDEUCES", (ftnlen)36, (ftnlen)10);
    c2ncod[425] = 635;
    s_copy(c2nnam + 15300, "DAPHNIS", (ftnlen)36, (ftnlen)7);
    c2ncod[426] = 636;
    s_copy(c2nnam + 15336, "AEGIR", (ftnlen)36, (ftnlen)5);
    c2ncod[427] = 637;
    s_copy(c2nnam + 15372, "BEBHIONN", (ftnlen)36, (ftnlen)8);
    c2ncod[428] = 638;
    s_copy(c2nnam + 15408, "BERGELMIR", (ftnlen)36, (ftnlen)9);
    c2ncod[429] = 639;
    s_copy(c2nnam + 15444, "BESTLA", (ftnlen)36, (ftnlen)6);
    c2ncod[430] = 640;
    s_copy(c2nnam + 15480, "FARBAUTI", (ftnlen)36, (ftnlen)8);
    c2ncod[431] = 641;
    s_copy(c2nnam + 15516, "FENRIR", (ftnlen)36, (ftnlen)6);
    c2ncod[432] = 642;
    s_copy(c2nnam + 15552, "FORNJOT", (ftnlen)36, (ftnlen)7);
    c2ncod[433] = 643;
    s_copy(c2nnam + 15588, "HATI", (ftnlen)36, (ftnlen)4);
    c2ncod[434] = 644;
    s_copy(c2nnam + 15624, "HYRROKKIN", (ftnlen)36, (ftnlen)9);
    c2ncod[435] = 645;
    s_copy(c2nnam + 15660, "KARI", (ftnlen)36, (ftnlen)4);
    c2ncod[436] = 646;
    s_copy(c2nnam + 15696, "LOGE", (ftnlen)36, (ftnlen)4);
    c2ncod[437] = 647;
    s_copy(c2nnam + 15732, "SKOLL", (ftnlen)36, (ftnlen)5);
    c2ncod[438] = 648;
    s_copy(c2nnam + 15768, "SURTUR", (ftnlen)36, (ftnlen)6);
    c2ncod[439] = 649;
    s_copy(c2nnam + 15804, "ANTHE", (ftnlen)36, (ftnlen)5);
    c2ncod[440] = 650;
    s_copy(c2nnam + 15840, "JARNSAXA", (ftnlen)36, (ftnlen)8);
    c2ncod[441] = 651;
    s_copy(c2nnam + 15876, "GREIP", (ftnlen)36, (ftnlen)5);
    c2ncod[442] = 652;
    s_copy(c2nnam + 15912, "TARQEQ", (ftnlen)36, (ftnlen)6);
    c2ncod[443] = 653;
    s_copy(c2nnam + 15948, "AEGAEON", (ftnlen)36, (ftnlen)7);
    c2ncod[444] = 699;
    s_copy(c2nnam + 15984, "SATURN", (ftnlen)36, (ftnlen)6);
    c2ncod[445] = 7;
    s_copy(c2nnam + 16020, "URANUS BARYCENTER", (ftnlen)36, (ftnlen)17);
    c2ncod[446] = 701;
    s_copy(c2nnam + 16056, "ARIEL", (ftnlen)36, (ftnlen)5);
    c2ncod[447] = 702;
    s_copy(c2nnam + 16092, "UMBRIEL", (ftnlen)36, (ftnlen)7);
    c2ncod[448] = 703;
    s_copy(c2nnam + 16128, "TITANIA", (ftnlen)36, (ftnlen)7);
    c2ncod[449] = 704;
    s_copy(c2nnam + 16164, "OBERON", (ftnlen)36, (ftnlen)6);
    c2ncod[450] = 705;
    s_copy(c2nnam + 16200, "MIRANDA", (ftnlen)36, (ftnlen)7);
    c2ncod[451] = 706;
    s_copy(c2nnam + 16236, "CORDELIA", (ftnlen)36, (ftnlen)8);
    c2ncod[452] = 707;
    s_copy(c2nnam + 16272, "OPHELIA", (ftnlen)36, (ftnlen)7);
    c2ncod[453] = 708;
    s_copy(c2nnam + 16308, "BIANCA", (ftnlen)36, (ftnlen)6);
    c2ncod[454] = 709;
    s_copy(c2nnam + 16344, "CRESSIDA", (ftnlen)36, (ftnlen)8);
    c2ncod[455] = 710;
    s_copy(c2nnam + 16380, "DESDEMONA", (ftnlen)36, (ftnlen)9);
    c2ncod[456] = 711;
    s_copy(c2nnam + 16416, "JULIET", (ftnlen)36, (ftnlen)6);
    c2ncod[457] = 712;
    s_copy(c2nnam + 16452, "PORTIA", (ftnlen)36, (ftnlen)6);
    c2ncod[458] = 713;
    s_copy(c2nnam + 16488, "ROSALIND", (ftnlen)36, (ftnlen)8);
    c2ncod[459] = 714;
    s_copy(c2nnam + 16524, "BELINDA", (ftnlen)36, (ftnlen)7);
    c2ncod[460] = 715;
    s_copy(c2nnam + 16560, "PUCK", (ftnlen)36, (ftnlen)4);
    c2ncod[461] = 716;
    s_copy(c2nnam + 16596, "CALIBAN", (ftnlen)36, (ftnlen)7);
    c2ncod[462] = 717;
    s_copy(c2nnam + 16632, "SYCORAX", (ftnlen)36, (ftnlen)7);
    c2ncod[463] = 718;
    s_copy(c2nnam + 16668, "PROSPERO", (ftnlen)36, (ftnlen)8);
    c2ncod[464] = 719;
    s_copy(c2nnam + 16704, "SETEBOS", (ftnlen)36, (ftnlen)7);
    c2ncod[465] = 720;
    s_copy(c2nnam + 16740, "STEPHANO", (ftnlen)36, (ftnlen)8);
    c2ncod[466] = 721;
    s_copy(c2nnam + 16776, "TRINCULO", (ftnlen)36, (ftnlen)8);
    c2ncod[467] = 722;
    s_copy(c2nnam + 16812, "FRANCISCO", (ftnlen)36, (ftnlen)9);
    c2ncod[468] = 723;
    s_copy(c2nnam + 16848, "MARGARET", (ftnlen)36, (ftnlen)8);
    c2ncod[469] = 724;
    s_copy(c2nnam + 16884, "FERDINAND", (ftnlen)36, (ftnlen)9);
    c2ncod[470] = 725;
    s_copy(c2nnam + 16920, "PERDITA", (ftnlen)36, (ftnlen)7);
    c2ncod[471] = 726;
    s_copy(c2nnam + 16956, "MAB", (ftnlen)36, (ftnlen)3);
    c2ncod[472] = 727;
    s_copy(c2nnam + 16992, "CUPID", (ftnlen)36, (ftnlen)5);
    c2ncod[473] = 799;
    s_copy(c2nnam + 17028, "URANUS", (ftnlen)36, (ftnlen)6);
    c2ncod[474] = 8;
    s_copy(c2nnam + 17064, "NEPTUNE BARYCENTER", (ftnlen)36, (ftnlen)18);
    c2ncod[475] = 801;
    s_copy(c2nnam + 17100, "TRITON", (ftnlen)36, (ftnlen)6);
    c2ncod[476] = 802;
    s_copy(c2nnam + 17136, "NEREID", (ftnlen)36, (ftnlen)6);
    c2ncod[477] = 803;
    s_copy(c2nnam + 17172, "NAIAD", (ftnlen)36, (ftnlen)5);
    c2ncod[478] = 804;
    s_copy(c2nnam + 17208, "THALASSA", (ftnlen)36, (ftnlen)8);
    c2ncod[479] = 805;
    s_copy(c2nnam + 17244, "DESPINA", (ftnlen)36, (ftnlen)7);
    c2ncod[480] = 806;
    s_copy(c2nnam + 17280, "GALATEA", (ftnlen)36, (ftnlen)7);
    c2ncod[481] = 807;
    s_copy(c2nnam + 17316, "LARISSA", (ftnlen)36, (ftnlen)7);
    c2ncod[482] = 808;
    s_copy(c2nnam + 17352, "PROTEUS", (ftnlen)36, (ftnlen)7);
    c2ncod[483] = 809;
    s_copy(c2nnam + 17388, "HALIMEDE", (ftnlen)36, (ftnlen)8);
    c2ncod[484] = 810;
    s_copy(c2nnam + 17424, "PSAMATHE", (ftnlen)36, (ftnlen)8);
    c2ncod[485] = 811;
    s_copy(c2nnam + 17460, "SAO", (ftnlen)36, (ftnlen)3);
    c2ncod[486] = 812;
    s_copy(c2nnam + 17496, "LAOMEDEIA", (ftnlen)36, (ftnlen)9);
    c2ncod[487] = 813;
    s_copy(c2nnam + 17532, "NESO", (ftnlen)36, (ftnlen)4);
    c2ncod[488] = 899;
    s_copy(c2nnam + 17568, "NEPTUNE", (ftnlen)36, (ftnlen)7);
    c2ncod[489] = 9;
    s_copy(c2nnam + 17604, "PLUTO BARYCENTER", (ftnlen)36, (ftnlen)16);
    c2ncod[490] = 901;
    s_copy(c2nnam + 17640, "CHARON", (ftnlen)36, (ftnlen)6);
    c2ncod[491] = 902;
    s_copy(c2nnam + 17676, "NIX", (ftnlen)36, (ftnlen)3);
    c2ncod[492] = 903;
    s_copy(c2nnam + 17712, "HYDRA", (ftnlen)36, (ftnlen)5);
    c2ncod[493] = 904;
    s_copy(c2nnam + 17748, "KERBEROS", (ftnlen)36, (ftnlen)8);
    c2ncod[494] = 905;
    s_copy(c2nnam + 17784, "STYX", (ftnlen)36, (ftnlen)4);
    c2ncod[495] = 9511010;
    s_copy(c2nnam + 17820, "GASPRA", (ftnlen)36, (ftnlen)6);
    c2ncod[496] = 999;
    s_copy(c2nnam + 17856, "PLUTO", (ftnlen)36, (ftnlen)5);
    topen_("F_ZZBODS", (ftnlen)8);

/*     Test the BODN2C call against the known body list. */

    for (i__ = 1; i__ <= 620; ++i__) {
/* Writing concatenation */
	i__2[0] = 40, a__1[0] = "Check known name/ID code pairs, BODN2C: ";
	i__2[1] = 36, a__1[1] = n2cnam + ((i__1 = i__ - 1) < 620 && 0 <= i__1 
		? i__1 : s_rnge("n2cnam", i__1, "f_zzbods__", (ftnlen)3509)) *
		 36;
	s_cat(tststr, a__1, i__2, &c__2, (ftnlen)100);
	tcase_(tststr, (ftnlen)100);
	bodn2c_(n2cnam + ((i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge(
		"n2cnam", i__1, "f_zzbods__", (ftnlen)3512)) * 36, &code, &
		found, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("CODE", &code, "=", &n2ccod[(i__1 = i__ - 1) < 620 && 0 <= 
		i__1 ? i__1 : s_rnge("n2ccod", i__1, "f_zzbods__", (ftnlen)
		3515)], &c__0, ok, (ftnlen)4, (ftnlen)1);
    }

/*     Test the BODC2N call against the known body list. */
/*     This body list removed duplicate ID -> NAME assignments, */
/*     preserving the expected mapping precedence for IDs */
/*     mapped to multiple names. */

    for (i__ = 1; i__ <= 497; ++i__) {
	s_copy(tststr, "Check known name/ID code pairs, BODC2N: #", (ftnlen)
		100, (ftnlen)41);
	repmi_(tststr, "#", &c2ncod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? 
		i__1 : s_rnge("c2ncod", i__1, "f_zzbods__", (ftnlen)3528)], 
		tststr, (ftnlen)100, (ftnlen)1, (ftnlen)100);
	tcase_(tststr, (ftnlen)100);
	bodc2n_(&c2ncod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge(
		"c2ncod", i__1, "f_zzbods__", (ftnlen)3531)], name__, &found, 
		(ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksc_("NAME", name__, "=", c2nnam + ((i__1 = i__ - 1) < 620 && 0 <= 
		i__1 ? i__1 : s_rnge("c2nnam", i__1, "f_zzbods__", (ftnlen)
		3534)) * 36, ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (ftnlen)36)
		;
    }
    t_success__(ok);
    return 0;
} /* f_zzbods__ */

