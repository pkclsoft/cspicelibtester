/* f_zzbods2c.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__10 = 10;
static logical c_true = TRUE_;
static integer c__20 = 20;
static integer c__30 = 30;
static integer c__6 = 6;
static integer c__3 = 3;
static doublereal c_b326 = 0.;
static integer c__9 = 9;
static doublereal c_b357 = .5;
static doublereal c_b359 = 1.;
static integer c__8 = 8;
static integer c__24 = 24;

/* $Procedure F_ZZBODS2C ( Family of tests for ZZBODS2C and its callers ) */
/* Subroutine */ int f_zzbods2c__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int zzbods2c_(integer *, char *, integer *, 
	    logical *, char *, integer *, logical *, ftnlen, ftnlen);
    doublereal dvec[3];
    char targ[36], body1[36], body2[36], body3[36];
    doublereal dist4[4];
    extern /* Subroutine */ int zzbodrst_(void);
    doublereal f;
    extern /* Subroutine */ int zzctruin_(integer *);
    integer i__, n[4];
    doublereal radii[12]	/* was [3][4] */;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    logical found;
    extern doublereal lspcn_(char *, doublereal *, char *, ftnlen, ftnlen);
    logical svfnd;
    char title[80];
    extern /* Subroutine */ int movei_(integer *, integer *, integer *), 
	    illum_(char *, doublereal *, char *, char *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen),
	     topen_(char *, ftnlen), t_ctrbeqf__(void);
    integer b1code;
    extern /* Subroutine */ int subpt_(char *, char *, doublereal *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    doublereal jacob1[36]	/* was [3][3][4] */, jacob2[36]	/* was [3][3][
	    4] */;
    integer b3code;
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *);
    doublereal incdn2[4], recta1[12]	/* was [3][4] */, phase1[4], phase2[4]
	    , phase3[4];
    logical found3[4], found4[4];
    doublereal solar1[4], trgep1[4], obspo1[12]	/* was [3][4] */, emiss1[4], 
	    trgep2[4], emiss2[4], srfve2[12]	/* was [3][4] */, trgep3[4], 
	    srfve3[12]	/* was [3][4] */, state1[24]	/* was [6][4] */, 
	    spoin3[12]	/* was [3][4] */;
    integer ci;
    doublereal state2[24]	/* was [6][4] */, state3[24]	/* was [6][4] 
	    */, spoin4[12]	/* was [3][4] */, trgep4[4], obspo4[12]	/* 
	    was [3][4] */, spoin5[12]	/* was [3][4] */, trgep5[4], srfve5[
	    12]	/* was [3][4] */, spoin6[12]	/* was [3][4] */, trmpt1[96]	
	    /* was [3][8][4] */, spoin7[12]	/* was [3][4] */, et;
    integer handle;
    doublereal trgep7[4], srfve7[12]	/* was [3][4] */, spoin8[12]	/* 
	    was [3][4] */, ls[4], lt;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    integer frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chckai_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), boddef_(char *, integer *, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen),
	     chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen);
    char buffer[80*6];
    integer targid;
    doublereal rectan[3];
    extern /* Subroutine */ int kilfil_(char *, ftnlen), cidfrm_(integer *, 
	    integer *, char *, logical *, ftnlen), bodvrd_(char *, char *, 
	    integer *, integer *, doublereal *, ftnlen, ftnlen);
    extern doublereal phaseq_(doublereal *, char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    char fixref[36];
    extern /* Subroutine */ int dpgrdr_(char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen), 
	    drdpgr_(char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen), edterm_(char *,
	     char *, char *, doublereal *, char *, char *, char *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), clpool_(
	    void);
    doublereal istate[6];
    extern /* Subroutine */ int illumg_(char *, char *, char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    doublereal obssta[6];
    extern /* Subroutine */ int pgrrec_(char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen), 
	    recpgr_(char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen), cleard_(
	    integer *, doublereal *);
    doublereal ostate[24]	/* was [6][4] */;
    integer svtgid;
    extern /* Subroutine */ int sincpt_(char *, char *, doublereal *, char *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    integer savctr[2];
    extern /* Subroutine */ int lmpool_(char *, integer *, ftnlen), xfmsta_(
	    doublereal *, char *, char *, char *, doublereal *, ftnlen, 
	    ftnlen, ftnlen), spkcvo_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    char svtarg[36];
    extern /* Subroutine */ int subsol_(char *, char *, doublereal *, char *, 
	    char *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), tstpck_(
	    char *, logical *, logical *, ftnlen), spkcvt_(doublereal *, 
	    doublereal *, char *, char *, doublereal *, char *, char *, char *
	    , char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), subpnt_(char *, char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    doublereal spoint[3];
    extern /* Subroutine */ int spkezp_(integer *, doublereal *, char *, char 
	    *, integer *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    spkezr_(char *, doublereal *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), spkpos_(char *, 
	    doublereal *, char *, char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    doublereal lt1[4];
    integer usrctr[2];
    doublereal lt2[4], lt3[4], lt4[4];
    extern /* Subroutine */ int srfxpt_(char *, char *, doublereal *, char *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), subslr_(char *, char *, doublereal *, char *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), tstspk_(char *, logical *, 
	    integer *, ftnlen);
    doublereal lat[4], alt[4], lon[4], alt6[4], pos4[12]	/* was [3][4] 
	    */;

/* $ Abstract */

/*     This routine tests ZZBODS2C and the internal save functionality */
/*     of all routines that call it. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     None */

/* $ Version */

/* -    Version 1.0.0  31-MAR-2014 (BVS) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZBODS2C", (ftnlen)10);

/*     Check ZZBODS2C update after counter set to user value. */

/*     Call ZZBODS2C twice, first to make sure that there is an */
/*     update, second that there is not update. */

    tcase_("ZZBODS2C update after initial counter setting.", (ftnlen)46);
    zzctruin_(usrctr);
    movei_(usrctr, &c__2, savctr);
    s_copy(targ, "SUN", (ftnlen)36, (ftnlen)3);
    targid = -1;
    found = FALSE_;
    s_copy(svtarg, "SUN", (ftnlen)36, (ftnlen)3);
    svtgid = -1;
    svfnd = TRUE_;
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__10, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__10, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(usrctr, &c__2, savctr);
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__10, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__10, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check ZZBODS2C update after BODDEF mapping update. */

/*     Call ZZBODS2C twice, first to make sure that there is an */
/*     update, second that there is not update. */

    tcase_("ZZBODS2C update after BODDEF.", (ftnlen)29);
    boddef_("SUN", &c__20, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    s_copy(targ, "SUN", (ftnlen)36, (ftnlen)3);
    targid = -1;
    found = FALSE_;
    s_copy(svtarg, "SUN", (ftnlen)36, (ftnlen)3);
    svtgid = 10;
    svfnd = TRUE_;
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__20, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__20, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(usrctr, &c__2, savctr);
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__20, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__20, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check ZZBODS2C update after POOL mapping update. */

/*     Call ZZBODS2C twice, first to make sure that there is an */
/*     update, second that there is not update. */

    tcase_("ZZBODS2C update after POOL mapping change.", (ftnlen)42);
    s_copy(buffer, "NAIF_BODY_CODE += 30", (ftnlen)80, (ftnlen)20);
    s_copy(buffer + 80, "NAIF_BODY_NAME += 'SUN'", (ftnlen)80, (ftnlen)23);
    lmpool_(buffer, &c__2, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    s_copy(targ, "SUN", (ftnlen)36, (ftnlen)3);
    targid = -1;
    found = FALSE_;
    s_copy(svtarg, "SUN", (ftnlen)36, (ftnlen)3);
    svtgid = 10;
    svfnd = TRUE_;
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(usrctr, &c__2, savctr);
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check ZZBODS2C update for input saved flag set to .FALSE. */

/*     Call ZZBODS2C twice, first to make sure that there is an */
/*     update, second that there is not update. */

    tcase_("ZZBODS2C update after .FALSE. saved flag.", (ftnlen)41);
    movei_(usrctr, &c__2, savctr);
    s_copy(targ, "SUN", (ftnlen)36, (ftnlen)3);
    targid = -1;
    found = FALSE_;
    s_copy(svtarg, "SUN", (ftnlen)36, (ftnlen)3);
    svtgid = -1;
    svfnd = FALSE_;
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(usrctr, &c__2, savctr);
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "SUN", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "SUN", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Check ZZBODS2C update for different input name. */

/*     Call ZZBODS2C twice, first to make sure that there is an */
/*     update, second that there is not update. */

    tcase_("ZZBODS2C update for different input name.", (ftnlen)41);
    movei_(usrctr, &c__2, savctr);
    s_copy(targ, "sun", (ftnlen)36, (ftnlen)3);
    targid = -1;
    found = FALSE_;
    s_copy(svtarg, "SUN", (ftnlen)36, (ftnlen)3);
    svtgid = -1;
    svfnd = TRUE_;
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "sun", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "sun", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(usrctr, &c__2, savctr);
    zzbods2c_(usrctr, svtarg, &svtgid, &svfnd, targ, &targid, &found, (ftnlen)
	    36, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SVTARG", svtarg, "=", "sun", ok, (ftnlen)6, (ftnlen)36, (ftnlen)
	    1, (ftnlen)3);
    chcksi_("SVTGID", &svtgid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("SVFND", &svfnd, &c_true, ok, (ftnlen)5);
    chcksc_("TARG", targ, "=", "sun", ok, (ftnlen)4, (ftnlen)36, (ftnlen)1, (
	    ftnlen)3);
    chcksi_("TARGID", &targid, "=", &c__30, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Clear POOL, reset BODTRN, and load some test data. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzbodrst_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kilfil_("spkfun.bsp", (ftnlen)10);
    kilfil_("spkfun.ker", (ftnlen)10);
    tstspk_("spkfun.bsp", &c_true, &handle, (ftnlen)10);
    tstpck_("spkfun.ker", &c_true, &c_false, (ftnlen)10);

/*     Test all routines that call ZZBODS2C. Do it by computing two */
/*     bench mark sets of data (at indexes 1 and 2), each for a unique, */
/*     non-overlapping set of bodies, then setting two unique sets of */
/*     body aliases and computing the same sets of data for them (at */
/*     indexes 3 and 4), and checking each "alias" set against its */
/*     benchmark. It saved mapping was updated correctly set 1 should */
/*     match set 3 and set 2 should match set 4. */

/*     In addition to setting up all inputs we will call T_CTRBEQF to */
/*     adjust ZZBODS2C and ZZNAMFRM counters to the same value before */
/*     calling each of the routine that make use of saved values and */
/*     counters. This will help exposing any cases when the same counter */
/*     is erroneously used in more than one place. */

    for (i__ = 1; i__ <= 4; ++i__) {
	if (i__ == 1) {
	    s_copy(body1, "MERCURY", (ftnlen)36, (ftnlen)7);
	    s_copy(body2, "VENUS", (ftnlen)36, (ftnlen)5);
	    s_copy(body3, "EARTH", (ftnlen)36, (ftnlen)5);
	    ci = 1;
	    s_copy(title, "Check 1st bench data set against itself", (ftnlen)
		    80, (ftnlen)39);
	} else if (i__ == 2) {
	    s_copy(body1, "MARS", (ftnlen)36, (ftnlen)4);
	    s_copy(body2, "JUPITER", (ftnlen)36, (ftnlen)7);
	    s_copy(body3, "SATURN", (ftnlen)36, (ftnlen)6);
	    ci = 2;
	    s_copy(title, "Check 2nd bench data set against itself", (ftnlen)
		    80, (ftnlen)39);
	} else if (i__ == 3) {
	    s_copy(body1, "BODY1", (ftnlen)36, (ftnlen)5);
	    s_copy(body2, "BODY2", (ftnlen)36, (ftnlen)5);
	    s_copy(body3, "BODY3", (ftnlen)36, (ftnlen)5);
	    ci = 1;
	    s_copy(buffer, "NAIF_BODY_CODE += 199", (ftnlen)80, (ftnlen)21);
	    s_copy(buffer + 80, "NAIF_BODY_NAME += 'BODY1'", (ftnlen)80, (
		    ftnlen)25);
	    s_copy(buffer + 160, "NAIF_BODY_CODE += 299", (ftnlen)80, (ftnlen)
		    21);
	    s_copy(buffer + 240, "NAIF_BODY_NAME += 'BODY2'", (ftnlen)80, (
		    ftnlen)25);
	    s_copy(buffer + 320, "NAIF_BODY_CODE += 399", (ftnlen)80, (ftnlen)
		    21);
	    s_copy(buffer + 400, "NAIF_BODY_NAME += 'BODY3'", (ftnlen)80, (
		    ftnlen)25);
	    lmpool_(buffer, &c__6, (ftnlen)80);
	    s_copy(title, "Check 1st mapped data set against 1st benchmark", (
		    ftnlen)80, (ftnlen)47);
	} else if (i__ == 4) {
	    s_copy(body1, "BODY1", (ftnlen)36, (ftnlen)5);
	    s_copy(body2, "BODY2", (ftnlen)36, (ftnlen)5);
	    s_copy(body3, "BODY3", (ftnlen)36, (ftnlen)5);
	    ci = 2;
	    s_copy(buffer, "NAIF_BODY_CODE += 499", (ftnlen)80, (ftnlen)21);
	    s_copy(buffer + 80, "NAIF_BODY_NAME += 'BODY1'", (ftnlen)80, (
		    ftnlen)25);
	    s_copy(buffer + 160, "NAIF_BODY_CODE += 599", (ftnlen)80, (ftnlen)
		    21);
	    s_copy(buffer + 240, "NAIF_BODY_NAME += 'BODY2'", (ftnlen)80, (
		    ftnlen)25);
	    s_copy(buffer + 320, "NAIF_BODY_CODE += 699", (ftnlen)80, (ftnlen)
		    21);
	    s_copy(buffer + 400, "NAIF_BODY_NAME += 'BODY3'", (ftnlen)80, (
		    ftnlen)25);
	    lmpool_(buffer, &c__6, (ftnlen)80);
	    s_copy(title, "Check 2nd mapped data set against 2nd benchmark", (
		    ftnlen)80, (ftnlen)47);
	}

/*        Declare test case. */

	tcase_(title, (ftnlen)80);

/*        Call all routines that make use of ZZBODS2C. Set arbitrary */
/*        input time. */

	et = 1e7;

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	bodvrd_(body1, "RADII", &c__3, &n[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("n", i__1, "f_zzbods2c__", (ftnlen)594)], &
		radii[(i__2 = i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge(
		"radii", i__2, "f_zzbods2c__", (ftnlen)594)], (ftnlen)36, (
		ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("BODVRD N", &n[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("n", i__1, "f_zzbods2c__", (ftnlen)597)], "=", &n[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("n", i__2, 
		"f_zzbods2c__", (ftnlen)597)], &c__0, ok, (ftnlen)8, (ftnlen)
		1);
	chckad_("BODVRD RADII", &radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 
		? i__1 : s_rnge("radii", i__1, "f_zzbods2c__", (ftnlen)599)], 
		"=", &radii[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("radii", i__2, "f_zzbods2c__", (ftnlen)599)], &c__3, &
		c_b326, ok, (ftnlen)12, (ftnlen)1);

/*        ----------------------------------------------------------- */

	f = (radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"radii", i__1, "f_zzbods2c__", (ftnlen)605)] - radii[(i__2 = 
		i__ * 3 - 1) < 12 && 0 <= i__2 ? i__2 : s_rnge("radii", i__2, 
		"f_zzbods2c__", (ftnlen)605)]) / radii[(i__3 = i__ * 3 - 3) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("radii", i__3, "f_zzbods2c__",
		 (ftnlen)605)];
	t_ctrbeqf__();
	dpgrdr_(body1, &radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : 
		s_rnge("radii", i__1, "f_zzbods2c__", (ftnlen)609)], &radii[(
		i__2 = i__ * 3 - 2) < 12 && 0 <= i__2 ? i__2 : s_rnge("radii",
		 i__2, "f_zzbods2c__", (ftnlen)609)], &radii[(i__3 = i__ * 3 
		- 1) < 12 && 0 <= i__3 ? i__3 : s_rnge("radii", i__3, "f_zzb"
		"ods2c__", (ftnlen)609)], &radii[(i__4 = i__ * 3 - 3) < 12 && 
		0 <= i__4 ? i__4 : s_rnge("radii", i__4, "f_zzbods2c__", (
		ftnlen)609)], &f, &jacob1[(i__5 = (i__ * 3 + 1) * 3 - 12) < 
		36 && 0 <= i__5 ? i__5 : s_rnge("jacob1", i__5, "f_zzbods2c__"
		, (ftnlen)609)], (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("DPGRDR JACOBI", &jacob1[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__1 ? i__1 : s_rnge("jacob1", i__1, "f_zzbods2c__", (
		ftnlen)613)], "=", &jacob1[(i__2 = (ci * 3 + 1) * 3 - 12) < 
		36 && 0 <= i__2 ? i__2 : s_rnge("jacob1", i__2, "f_zzbods2c__"
		, (ftnlen)613)], &c__9, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	drdpgr_(body1, &c_b357, &c_b357, &c_b359, &radii[(i__1 = i__ * 3 - 3) 
		< 12 && 0 <= i__1 ? i__1 : s_rnge("radii", i__1, "f_zzbods2c"
		"__", (ftnlen)621)], &f, &jacob2[(i__2 = (i__ * 3 + 1) * 3 - 
		12) < 36 && 0 <= i__2 ? i__2 : s_rnge("jacob2", i__2, "f_zzb"
		"ods2c__", (ftnlen)621)], (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("DRDPGR JACOBI", &jacob2[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__1 ? i__1 : s_rnge("jacob2", i__1, "f_zzbods2c__", (
		ftnlen)625)], "=", &jacob2[(i__2 = (ci * 3 + 1) * 3 - 12) < 
		36 && 0 <= i__2 ? i__2 : s_rnge("jacob2", i__2, "f_zzbods2c__"
		, (ftnlen)625)], &c__9, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	bodn2c_(body1, &b1code, &found, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("BODN2C FOUND", &found, &c_true, ok, (ftnlen)12);
	cidfrm_(&b1code, &frcode, fixref, &found, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("CIDFRM FOUND", &found, &c_true, ok, (ftnlen)12);
	t_ctrbeqf__();
	edterm_("UMBRAL", body2, body1, &et, fixref, "NONE", body3, &c__8, &
		trgep1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"trgep1", i__1, "f_zzbods2c__", (ftnlen)643)], &obspo1[(i__2 =
		 i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge("obspo1", 
		i__2, "f_zzbods2c__", (ftnlen)643)], &trmpt1[(i__3 = ((i__ << 
		3) + 1) * 3 - 27) < 96 && 0 <= i__3 ? i__3 : s_rnge("trmpt1", 
		i__3, "f_zzbods2c__", (ftnlen)643)], (ftnlen)6, (ftnlen)36, (
		ftnlen)36, (ftnlen)36, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("EDTERM TRGEPC", &trgep1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep1", i__1, "f_zzbods2c__", (ftnlen)648)], 
		"=", &trgep1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep1", i__2, "f_zzbods2c__", (ftnlen)648)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("EDTERM OBSPOS", &obspo1[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("obspo1", i__1, "f_zzbods2c__", (ftnlen)
		650)], "=", &obspo1[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("obspo1", i__2, "f_zzbods2c__", (ftnlen)650)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chckad_("EDTERM JACOBI", &trmpt1[(i__1 = ((i__ << 3) + 1) * 3 - 27) < 
		96 && 0 <= i__1 ? i__1 : s_rnge("trmpt1", i__1, "f_zzbods2c__"
		, (ftnlen)652)], "=", &trmpt1[(i__2 = ((ci << 3) + 1) * 3 - 
		27) < 96 && 0 <= i__2 ? i__2 : s_rnge("trmpt1", i__2, "f_zzb"
		"ods2c__", (ftnlen)652)], &c__24, &c_b326, ok, (ftnlen)13, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	vpack_(&radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"radii", i__1, "f_zzbods2c__", (ftnlen)658)], &c_b326, &
		c_b326, spoint);
	t_ctrbeqf__();
	illum_(body1, &et, "NONE", body3, spoint, &phase1[(i__1 = i__ - 1) < 
		4 && 0 <= i__1 ? i__1 : s_rnge("phase1", i__1, "f_zzbods2c__",
		 (ftnlen)662)], &solar1[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? 
		i__2 : s_rnge("solar1", i__2, "f_zzbods2c__", (ftnlen)662)], &
		emiss1[(i__3 = i__ - 1) < 4 && 0 <= i__3 ? i__3 : s_rnge(
		"emiss1", i__3, "f_zzbods2c__", (ftnlen)662)], (ftnlen)36, (
		ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("ILLUM PHASE", &phase1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("phase1", i__1, "f_zzbods2c__", (ftnlen)666)], 
		"=", &phase1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"phase1", i__2, "f_zzbods2c__", (ftnlen)666)], &c_b326, ok, (
		ftnlen)11, (ftnlen)1);
	chcksd_("ILLUM SOLAR", &solar1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("solar1", i__1, "f_zzbods2c__", (ftnlen)668)], 
		"=", &solar1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"solar1", i__2, "f_zzbods2c__", (ftnlen)668)], &c_b326, ok, (
		ftnlen)11, (ftnlen)1);
	chcksd_("ILLUM EMISSN", &emiss1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("emiss1", i__1, "f_zzbods2c__", (ftnlen)670)], 
		"=", &emiss1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"emiss1", i__2, "f_zzbods2c__", (ftnlen)670)], &c_b326, ok, (
		ftnlen)12, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	illumg_("Ellipsoid", body1, body2, &et, fixref, "NONE", body3, spoint,
		 &trgep2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"trgep2", i__1, "f_zzbods2c__", (ftnlen)678)], &srfve2[(i__2 =
		 i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge("srfve2", 
		i__2, "f_zzbods2c__", (ftnlen)678)], &phase2[(i__3 = i__ - 1) 
		< 4 && 0 <= i__3 ? i__3 : s_rnge("phase2", i__3, "f_zzbods2c"
		"__", (ftnlen)678)], &incdn2[(i__4 = i__ - 1) < 4 && 0 <= i__4 
		? i__4 : s_rnge("incdn2", i__4, "f_zzbods2c__", (ftnlen)678)],
		 &emiss2[(i__5 = i__ - 1) < 4 && 0 <= i__5 ? i__5 : s_rnge(
		"emiss2", i__5, "f_zzbods2c__", (ftnlen)678)], (ftnlen)9, (
		ftnlen)36, (ftnlen)36, (ftnlen)36, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("ILLUMG TRGEPC", &trgep2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep2", i__1, "f_zzbods2c__", (ftnlen)684)], 
		"=", &trgep2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep2", i__2, "f_zzbods2c__", (ftnlen)684)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("ILLUMG SRFVEC", &srfve2[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve2", i__1, "f_zzbods2c__", (ftnlen)
		686)], "=", &srfve2[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve2", i__2, "f_zzbods2c__", (ftnlen)686)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("ILLUMG PHASE", &phase2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("phase2", i__1, "f_zzbods2c__", (ftnlen)688)], 
		"=", &phase2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"phase2", i__2, "f_zzbods2c__", (ftnlen)688)], &c_b326, ok, (
		ftnlen)12, (ftnlen)1);
	chcksd_("ILLUMG INCDNC", &incdn2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("incdn2", i__1, "f_zzbods2c__", (ftnlen)690)], 
		"=", &incdn2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"incdn2", i__2, "f_zzbods2c__", (ftnlen)690)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chcksd_("ILLUMG EMISSN", &emiss2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("emiss2", i__1, "f_zzbods2c__", (ftnlen)692)], 
		"=", &emiss2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"emiss2", i__2, "f_zzbods2c__", (ftnlen)692)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	ls[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge("ls", i__1, 
		"f_zzbods2c__", (ftnlen)700)] = lspcn_(body1, &et, "NONE", (
		ftnlen)36, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("LSPCN", &ls[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("ls", i__1, "f_zzbods2c__", (ftnlen)703)], "=", &ls[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("ls", i__2, 
		"f_zzbods2c__", (ftnlen)703)], &c_b326, ok, (ftnlen)5, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	pgrrec_(body1, &c_b357, &c_b357, &c_b359, &radii[(i__1 = i__ * 3 - 3) 
		< 12 && 0 <= i__1 ? i__1 : s_rnge("radii", i__1, "f_zzbods2c"
		"__", (ftnlen)711)], &f, &recta1[(i__2 = i__ * 3 - 3) < 12 && 
		0 <= i__2 ? i__2 : s_rnge("recta1", i__2, "f_zzbods2c__", (
		ftnlen)711)], (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PGRREC RECTAN", &recta1[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("recta1", i__1, "f_zzbods2c__", (ftnlen)
		715)], "=", &recta1[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("recta1", i__2, "f_zzbods2c__", (ftnlen)715)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	phase3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge("phase3", 
		i__1, "f_zzbods2c__", (ftnlen)723)] = phaseq_(&et, body1, 
		body2, body3, "NONE", (ftnlen)36, (ftnlen)36, (ftnlen)36, (
		ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("PHASEQ", &phase3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("phase3", i__1, "f_zzbods2c__", (ftnlen)726)], "=", &
		phase3[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("pha"
		"se3", i__2, "f_zzbods2c__", (ftnlen)726)], &c_b326, ok, (
		ftnlen)6, (ftnlen)1);

/*        ----------------------------------------------------------- */

	vpack_(&radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"radii", i__1, "f_zzbods2c__", (ftnlen)732)], &radii[(i__2 = 
		i__ * 3 - 2) < 12 && 0 <= i__2 ? i__2 : s_rnge("radii", i__2, 
		"f_zzbods2c__", (ftnlen)732)], &radii[(i__3 = i__ * 3 - 1) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("radii", i__3, "f_zzbods2c__",
		 (ftnlen)732)], rectan);
	t_ctrbeqf__();
	recpgr_(body1, rectan, &radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ?
		 i__1 : s_rnge("radii", i__1, "f_zzbods2c__", (ftnlen)736)], &
		f, &lon[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"lon", i__2, "f_zzbods2c__", (ftnlen)736)], &lat[(i__3 = i__ 
		- 1) < 4 && 0 <= i__3 ? i__3 : s_rnge("lat", i__3, "f_zzbods"
		"2c__", (ftnlen)736)], &alt[(i__4 = i__ - 1) < 4 && 0 <= i__4 ?
		 i__4 : s_rnge("alt", i__4, "f_zzbods2c__", (ftnlen)736)], (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("RECPGR LON", &lon[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lon", i__1, "f_zzbods2c__", (ftnlen)740)], "=", &lon[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lon", i__2, 
		"f_zzbods2c__", (ftnlen)740)], &c_b326, ok, (ftnlen)10, (
		ftnlen)1);
	chcksd_("RECPGR LAT", &lat[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lat", i__1, "f_zzbods2c__", (ftnlen)742)], "=", &lat[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lat", i__2, 
		"f_zzbods2c__", (ftnlen)742)], &c_b326, ok, (ftnlen)10, (
		ftnlen)1);
	chcksd_("RECPGR ALT", &alt[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("alt", i__1, "f_zzbods2c__", (ftnlen)744)], "=", &alt[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("alt", i__2, 
		"f_zzbods2c__", (ftnlen)744)], &c_b326, ok, (ftnlen)10, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	bodn2c_(body3, &b3code, &found, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("BODN2C FOUND", &found, &c_true, ok, (ftnlen)12);
	spkezp_(&b1code, &et, "J2000", "NONE", &b3code, dvec, &lt, (ftnlen)5, 
		(ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	sincpt_("Ellipsoid", body1, &et, fixref, "NONE", body3, "J2000", dvec,
		 &spoin3[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : 
		s_rnge("spoin3", i__1, "f_zzbods2c__", (ftnlen)761)], &trgep3[
		(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep3", 
		i__2, "f_zzbods2c__", (ftnlen)761)], &srfve3[(i__3 = i__ * 3 
		- 3) < 12 && 0 <= i__3 ? i__3 : s_rnge("srfve3", i__3, "f_zz"
		"bods2c__", (ftnlen)761)], &found3[(i__4 = i__ - 1) < 4 && 0 <=
		 i__4 ? i__4 : s_rnge("found3", i__4, "f_zzbods2c__", (ftnlen)
		761)], (ftnlen)9, (ftnlen)36, (ftnlen)36, (ftnlen)4, (ftnlen)
		36, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SINCPT SPOINT", &spoin3[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin3", i__1, "f_zzbods2c__", (ftnlen)
		766)], "=", &spoin3[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin3", i__2, "f_zzbods2c__", (ftnlen)766)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SINCPT TRGEPC", &trgep3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep3", i__1, "f_zzbods2c__", (ftnlen)768)], 
		"=", &trgep3[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep3", i__2, "f_zzbods2c__", (ftnlen)768)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SINCPT SRFVEC", &srfve3[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve3", i__1, "f_zzbods2c__", (ftnlen)
		770)], "=", &srfve3[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve3", i__2, "f_zzbods2c__", (ftnlen)770)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksl_("SINCPT FOUND", &found3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found3", i__1, "f_zzbods2c__", (ftnlen)772)], &
		found3[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd3", i__2, "f_zzbods2c__", (ftnlen)772)], ok, (ftnlen)12);

/*        ----------------------------------------------------------- */

	cleard_(&c__6, obssta);
	t_ctrbeqf__();
	spkcvo_(body1, &et, "J2000", "OBSERVER", "NONE", obssta, &et, body3, 
		"J2000", &state1[(i__1 = i__ * 6 - 6) < 24 && 0 <= i__1 ? 
		i__1 : s_rnge("state1", i__1, "f_zzbods2c__", (ftnlen)782)], &
		lt1[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt1", 
		i__2, "f_zzbods2c__", (ftnlen)782)], (ftnlen)36, (ftnlen)5, (
		ftnlen)8, (ftnlen)4, (ftnlen)36, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKCVO STATE", &state1[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state1", i__1, "f_zzbods2c__", (ftnlen)
		787)], "=", &state1[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state1", i__2, "f_zzbods2c__", (ftnlen)787)], &
		c__6, &c_b326, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SPKCVO LT", &lt1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt1", i__1, "f_zzbods2c__", (ftnlen)789)], "=", &lt1[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt1", i__2, 
		"f_zzbods2c__", (ftnlen)789)], &c_b326, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkcvt_(obssta, &et, body1, fixref, &et, "J2000", "OBSERVER", "NONE", 
		body3, &state2[(i__1 = i__ * 6 - 6) < 24 && 0 <= i__1 ? i__1 :
		 s_rnge("state2", i__1, "f_zzbods2c__", (ftnlen)797)], &lt2[(
		i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt2", i__2, 
		"f_zzbods2c__", (ftnlen)797)], (ftnlen)36, (ftnlen)36, (
		ftnlen)5, (ftnlen)8, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKCVT STATE", &state2[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state2", i__1, "f_zzbods2c__", (ftnlen)
		802)], "=", &state2[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state2", i__2, "f_zzbods2c__", (ftnlen)802)], &
		c__6, &c_b326, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SPKCVT LT", &lt2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt2", i__1, "f_zzbods2c__", (ftnlen)804)], "=", &lt2[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt2", i__2, 
		"f_zzbods2c__", (ftnlen)804)], &c_b326, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkezr_(body1, &et, "J2000", "NONE", body3, &state3[(i__1 = i__ * 6 - 
		6) < 24 && 0 <= i__1 ? i__1 : s_rnge("state3", i__1, "f_zzbo"
		"ds2c__", (ftnlen)812)], &lt3[(i__2 = i__ - 1) < 4 && 0 <= 
		i__2 ? i__2 : s_rnge("lt3", i__2, "f_zzbods2c__", (ftnlen)812)
		], (ftnlen)36, (ftnlen)5, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKEZR STATE", &state3[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state3", i__1, "f_zzbods2c__", (ftnlen)
		816)], "=", &state3[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state3", i__2, "f_zzbods2c__", (ftnlen)816)], &
		c__6, &c_b326, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SPKEZR LT", &lt3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt3", i__1, "f_zzbods2c__", (ftnlen)818)], "=", &lt3[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt3", i__2, 
		"f_zzbods2c__", (ftnlen)818)], &c_b326, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkpos_(body1, &et, "J2000", "NONE", body3, &pos4[(i__1 = i__ * 3 - 3)
		 < 12 && 0 <= i__1 ? i__1 : s_rnge("pos4", i__1, "f_zzbods2c"
		"__", (ftnlen)826)], &lt4[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? 
		i__2 : s_rnge("lt4", i__2, "f_zzbods2c__", (ftnlen)826)], (
		ftnlen)36, (ftnlen)5, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKPOS POS", &pos4[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("pos4", i__1, "f_zzbods2c__", (ftnlen)830)], 
		"=", &pos4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("pos4", i__2, "f_zzbods2c__", (ftnlen)830)], &c__3, &
		c_b326, ok, (ftnlen)10, (ftnlen)1);
	chcksd_("SPKPOS LT", &lt4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt4", i__1, "f_zzbods2c__", (ftnlen)832)], "=", &lt4[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt4", i__2, 
		"f_zzbods2c__", (ftnlen)832)], &c_b326, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	srfxpt_("Ellipsoid", body1, &et, "NONE", body3, "J2000", dvec, &
		spoin4[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"spoin4", i__1, "f_zzbods2c__", (ftnlen)840)], &dist4[(i__2 = 
		i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("dist4", i__2, 
		"f_zzbods2c__", (ftnlen)840)], &trgep4[(i__3 = i__ - 1) < 4 &&
		 0 <= i__3 ? i__3 : s_rnge("trgep4", i__3, "f_zzbods2c__", (
		ftnlen)840)], &obspo4[(i__4 = i__ * 3 - 3) < 12 && 0 <= i__4 ?
		 i__4 : s_rnge("obspo4", i__4, "f_zzbods2c__", (ftnlen)840)], 
		&found4[(i__5 = i__ - 1) < 4 && 0 <= i__5 ? i__5 : s_rnge(
		"found4", i__5, "f_zzbods2c__", (ftnlen)840)], (ftnlen)9, (
		ftnlen)36, (ftnlen)4, (ftnlen)36, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SRFXPT SPOINT", &spoin4[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin4", i__1, "f_zzbods2c__", (ftnlen)
		845)], "=", &spoin4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin4", i__2, "f_zzbods2c__", (ftnlen)845)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SRFXPT DIST", &dist4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("dist4", i__1, "f_zzbods2c__", (ftnlen)847)], 
		"=", &dist4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"dist4", i__2, "f_zzbods2c__", (ftnlen)847)], &c_b326, ok, (
		ftnlen)11, (ftnlen)1);
	chcksd_("SRFXPT TRGEPC", &trgep4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep4", i__1, "f_zzbods2c__", (ftnlen)849)], 
		"=", &trgep4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep4", i__2, "f_zzbods2c__", (ftnlen)849)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SRFXPT OBSPOS", &obspo4[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("obspo4", i__1, "f_zzbods2c__", (ftnlen)
		851)], "=", &obspo4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("obspo4", i__2, "f_zzbods2c__", (ftnlen)851)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksl_("SRFXPT FOUND", &found4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found4", i__1, "f_zzbods2c__", (ftnlen)853)], &
		found4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd4", i__2, "f_zzbods2c__", (ftnlen)853)], ok, (ftnlen)12);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subpnt_("Near point: ellipsoid", body1, &et, fixref, "NONE", body3, &
		spoin5[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"spoin5", i__1, "f_zzbods2c__", (ftnlen)861)], &trgep5[(i__2 =
		 i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep5", i__2, 
		"f_zzbods2c__", (ftnlen)861)], &srfve5[(i__3 = i__ * 3 - 3) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("srfve5", i__3, "f_zzbods2c__"
		, (ftnlen)861)], (ftnlen)21, (ftnlen)36, (ftnlen)36, (ftnlen)
		4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBPNT SPOINT", &spoin5[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin5", i__1, "f_zzbods2c__", (ftnlen)
		866)], "=", &spoin5[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin5", i__2, "f_zzbods2c__", (ftnlen)866)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SUBPNT TRGEPC", &trgep5[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep5", i__1, "f_zzbods2c__", (ftnlen)868)], 
		"=", &trgep5[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep5", i__2, "f_zzbods2c__", (ftnlen)868)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SUBPNT SRFVEC", &srfve5[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve5", i__1, "f_zzbods2c__", (ftnlen)
		870)], "=", &srfve5[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve5", i__2, "f_zzbods2c__", (ftnlen)870)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subpt_("Near point", body1, &et, "NONE", body3, &spoin6[(i__1 = i__ * 
		3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge("spoin6", i__1, 
		"f_zzbods2c__", (ftnlen)878)], &alt6[(i__2 = i__ - 1) < 4 && 
		0 <= i__2 ? i__2 : s_rnge("alt6", i__2, "f_zzbods2c__", (
		ftnlen)878)], (ftnlen)10, (ftnlen)36, (ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBPT SPOINT", &spoin6[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin6", i__1, "f_zzbods2c__", (ftnlen)
		882)], "=", &spoin6[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin6", i__2, "f_zzbods2c__", (ftnlen)882)], &
		c__3, &c_b326, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SUBPT ALT", &alt6[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("alt6", i__1, "f_zzbods2c__", (ftnlen)884)], "=", &
		alt6[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("alt6", 
		i__2, "f_zzbods2c__", (ftnlen)884)], &c_b326, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subslr_("Near point: ellipsoid", body1, &et, fixref, "NONE", body3, &
		spoin7[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"spoin7", i__1, "f_zzbods2c__", (ftnlen)892)], &trgep7[(i__2 =
		 i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep7", i__2, 
		"f_zzbods2c__", (ftnlen)892)], &srfve7[(i__3 = i__ * 3 - 3) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("srfve7", i__3, "f_zzbods2c__"
		, (ftnlen)892)], (ftnlen)21, (ftnlen)36, (ftnlen)36, (ftnlen)
		4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBSLR SPOINT", &spoin7[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin7", i__1, "f_zzbods2c__", (ftnlen)
		897)], "=", &spoin7[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin7", i__2, "f_zzbods2c__", (ftnlen)897)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SUBSLR TRGEPC", &trgep7[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep7", i__1, "f_zzbods2c__", (ftnlen)899)], 
		"=", &trgep7[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep7", i__2, "f_zzbods2c__", (ftnlen)899)], &c_b326, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SUBSLR SRFVEC", &srfve7[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve7", i__1, "f_zzbods2c__", (ftnlen)
		901)], "=", &srfve7[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve7", i__2, "f_zzbods2c__", (ftnlen)901)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subsol_("Near point", body1, &et, "NONE", body3, &spoin8[(i__1 = i__ *
		 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge("spoin8", i__1, 
		"f_zzbods2c__", (ftnlen)909)], (ftnlen)10, (ftnlen)36, (
		ftnlen)4, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBSOL SPOINT", &spoin8[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin8", i__1, "f_zzbods2c__", (ftnlen)
		913)], "=", &spoin8[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin8", i__2, "f_zzbods2c__", (ftnlen)913)], &
		c__3, &c_b326, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	vpack_(&radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"radii", i__1, "f_zzbods2c__", (ftnlen)919)], &radii[(i__2 = 
		i__ * 3 - 2) < 12 && 0 <= i__2 ? i__2 : s_rnge("radii", i__2, 
		"f_zzbods2c__", (ftnlen)919)], &radii[(i__3 = i__ * 3 - 1) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("radii", i__3, "f_zzbods2c__",
		 (ftnlen)919)], istate);
	vpack_(&c_b326, &c_b326, &c_b326, &istate[3]);
	t_ctrbeqf__();
	xfmsta_(istate, "RECTANGULAR", "PLANETOGRAPHIC", body1, &ostate[(i__1 
		= i__ * 6 - 6) < 24 && 0 <= i__1 ? i__1 : s_rnge("ostate", 
		i__1, "f_zzbods2c__", (ftnlen)924)], (ftnlen)11, (ftnlen)14, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("XFMSTA OSTATE", &ostate[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("ostate", i__1, "f_zzbods2c__", (ftnlen)
		928)], "=", &ostate[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("ostate", i__2, "f_zzbods2c__", (ftnlen)928)], &
		c__6, &c_b326, ok, (ftnlen)13, (ftnlen)1);
    }

/*     Clean up. */

    kilfil_("spkfun.bsp", (ftnlen)10);
    kilfil_("spkfun.ker", (ftnlen)10);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_zzbods2c__ */

