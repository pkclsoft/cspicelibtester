/* f_zzbodvcd.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__0 = 0;
static doublereal c_b36 = 0.;
static integer c__10000 = 10000;

/* $Procedure F_ZZBODVCD ( ZZBODVCD tests ) */
/* Subroutine */ int f_zzbodvcd__(logical *ok)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char item[32];
    static integer xctr[2];
    extern /* Subroutine */ int zzbodvcd_(integer *, char *, integer *, 
	    integer *, integer *, doublereal *, ftnlen), zzpctrck_(integer *, 
	    logical *), zzctruin_(integer *);
    static integer n;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal xbuff[10000];
    static char title[255];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen), chckai_(char *, integer 
	    *, char *, integer *, integer *, logical *, ftnlen, ftnlen), 
	    cleard_(integer *, doublereal *), delfil_(char *, ftnlen), 
	    bodvcd_(integer *, char *, integer *, integer *, doublereal *, 
	    ftnlen), chckxc_(logical *, char *, logical *, ftnlen), chcksi_(
	    char *, integer *, char *, integer *, integer *, logical *, 
	    ftnlen, ftnlen);
    static doublereal dpbuff[10000];
    static integer xn;
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static integer bodyid;
    static logical update;
    static integer varctr[2];
    extern /* Subroutine */ int dvpool_(char *, ftnlen);
    extern logical exists_(char *, ftnlen);

/* $ Abstract */

/*     Exercise the routine ZZBODVCD. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZBODVCD. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 09-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZBODVCD", (ftnlen)10);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create PCK.", (ftnlen)19);
    if (exists_("zzbodvcd.tpc", (ftnlen)12)) {
	delfil_("zzbodvcd.tpc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_pck08__("zzbodvcd.tpc", &c_true, &c_true, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize the user counters. */

    zzctruin_(varctr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzctruin_(xctr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Fetch moon radii.", (ftnlen)255, (ftnlen)17);
    tcase_(title, (ftnlen)255);
    s_copy(item, "RADII", (ftnlen)32, (ftnlen)5);
    bodyid = 399;

/*     Get expected values. */

    bodvcd_(&bodyid, item, &c__3, &xn, xbuff, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzbodvcd_(&bodyid, item, &c__3, varctr, &n, dpbuff, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check counter. */

    zzpctrck_(xctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    chckai_("VARCTR", varctr, "=", xctr, &c__2, ok, (ftnlen)6, (ftnlen)1);

/*     Check data size and values. */

    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chckad_(item, dpbuff, "=", xbuff, &xn, &c_b36, ok, (ftnlen)32, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Fetch moon radii a second time. Counter should remain unc"
	    "hanged.", (ftnlen)255, (ftnlen)64);
    tcase_(title, (ftnlen)255);
    zzbodvcd_(&bodyid, item, &c__3, varctr, &n, dpbuff, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check counter. */

    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    chckai_("VARCTR", varctr, "=", xctr, &c__2, ok, (ftnlen)6, (ftnlen)1);

/*     Check data size and values. */

    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chckad_(item, dpbuff, "=", xbuff, &xn, &c_b36, ok, (ftnlen)32, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Touch the pool; fetch moon radii a third time. Counter sh"
	    "ould be updated.", (ftnlen)255, (ftnlen)73);
    dvpool_("BODY599_PM", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzbodvcd_(&bodyid, item, &c__3, varctr, &n, dpbuff, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check counter. */

    zzpctrck_(xctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);

/*     Check counter. */

    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    chckai_("VARCTR", varctr, "=", xctr, &c__2, ok, (ftnlen)6, (ftnlen)1);

/*     Check data size and values. */

    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chckad_(item, dpbuff, "=", xbuff, &xn, &c_b36, ok, (ftnlen)32, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Verify that input values are returned if the pool wasn't "
	    "touched.", (ftnlen)255, (ftnlen)65);
    tcase_(title, (ftnlen)255);
    cleard_(&c__10000, dpbuff);
    cleard_(&c__10000, xbuff);
    n = 0;
    zzbodvcd_(&bodyid, item, &c__3, varctr, &n, dpbuff, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check counter. */

    chckai_("VARCTR", varctr, "=", xctr, &c__2, ok, (ftnlen)6, (ftnlen)1);

/*     Check data size and values. */

    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chckad_(item, dpbuff, "=", xbuff, &c__10000, &c_b36, ok, (ftnlen)32, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    delfil_("zzbodvcd.tpc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzbodvcd__ */

