/* f_zzcke06.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__340 = 340;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__4 = 4;
static integer c_b24 = 140000;
static doublereal c_b51 = 1.;
static doublereal c_b52 = 2.;
static doublereal c_b53 = 3.;
static integer c__14 = 14;
static integer c__9 = 9;
static doublereal c_b84 = 0.;
static integer c__3 = 3;
static doublereal c_b268 = -1.;

/* $Procedure      F_ZZCKE06 ( Family of tests for ZZCKE06 ) */
/* Subroutine */ int f_zzcke06__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double acos(doublereal);

    /* Local variables */
    extern /* Subroutine */ int cke06_(logical *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal cmat[9]	/* was [3][3] */, rate, span, qmat[9]	/* 
	    was [3][3] */, axis[3];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal quat[4];
    static integer size;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), mtxm_(
	    doublereal *, doublereal *, doublereal *);
    static doublereal c__;
    static integer i__;
    static doublereal s, alpha, angle, w, delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen), moved_(
	    doublereal *, integer *, doublereal *);
    static doublereal xcmat[9]	/* was [3][3] */;
    static integer nsamp;
    static char title[240];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    extern doublereal vdotg_(doublereal *, doublereal *, integer *);
    static doublereal xsclk, qaxis[3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal prevq[4], xquat[4], angle0;
    extern /* Subroutine */ int t_success__(logical *), qdq2av_(doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    ;
    static doublereal dq[4], av[3];
    static integer degree;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal clkbuf[10000], qangle;
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    static doublereal tdelta, record[340], sclkdp, qqangl, packts[140000];
    extern /* Subroutine */ int axisar_(doublereal *, doublereal *, 
	    doublereal *), raxisa_(doublereal *, doublereal *, doublereal *), 
	    vhatip_(doublereal *), zzcke06_(doublereal *, doublereal *, 
	    doublereal *);
    static doublereal clkout, qstate[8];
    extern /* Subroutine */ int q2m_(doublereal *, doublereal *), m2q_(
	    doublereal *, doublereal *);
    static integer winsiz, pktsiz, subtyp;
    static doublereal dav[3];
    extern doublereal dpr_(void), rpd_(void);
    static doublereal tol, xav[3];
    static integer ptr;
    static doublereal rec2[340];

/* $ Abstract */

/*     This test family exercises ZZCKE06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declarations of the CK data type specific and general CK low */
/*     level routine parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK.REQ */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     1) If new CK types are added, the size of the record passed */
/*        between CKRxx and CKExx must be registered as separate */
/*        parameter. If this size will be greater than current value */
/*        of the CKMRSZ parameter (which specifies the maximum record */
/*        size for the record buffer used inside CKPFS) then it should */
/*        be assigned to CKMRSZ as a new value. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     CK Required Reading. */

/* $ Version */

/* -    SPICELIB Version 3.0.0, 27-JAN-2014 (NJB) */

/*        Updated to support CK type 6. Maximum degree for */
/*        type 5 was updated to be consistent with the */
/*        maximum degree for type 6. */

/* -    SPICELIB Version 2.0.0, 19-AUG-2002 (NJB) */

/*        Updated to support CK type 5. */

/* -    SPICELIB Version 1.0.0, 05-APR-1999 (BVS) */

/* -& */

/*     Number of quaternion components and number of quaternion and */
/*     angular rate components together. */


/*     CK Type 1 parameters: */

/*     CK1DTP   CK data type 1 ID; */

/*     CK1RSZ   maximum size of a record passed between CKR01 */
/*              and CKE01. */


/*     CK Type 2 parameters: */

/*     CK2DTP   CK data type 2 ID; */

/*     CK2RSZ   maximum size of a record passed between CKR02 */
/*              and CKE02. */


/*     CK Type 3 parameters: */

/*     CK3DTP   CK data type 3 ID; */

/*     CK3RSZ   maximum size of a record passed between CKR03 */
/*              and CKE03. */


/*     CK Type 4 parameters: */

/*     CK4DTP   CK data type 4 ID; */

/*     CK4PCD   parameter defining integer to DP packing schema that */
/*              is applied when seven number integer array containing */
/*              polynomial degrees for quaternion and angular rate */
/*              components packed into a single DP number stored in */
/*              actual CK records in a file; the value of must not be */
/*              changed or compatibility with existing type 4 CK files */
/*              will be lost. */

/*     CK4MXD   maximum Chebychev polynomial degree allowed in type 4 */
/*              records; the value of this parameter must never exceed */
/*              value of the CK4PCD; */

/*     CK4SFT   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 4 */
/*              CK record that passed between routines CKR04 and CKE04; */

/*     CK4RSZ   maximum size of type 4 CK record passed between CKR04 */
/*              and CKE04; CK4RSZ is computed as follows: */

/*                 CK4RSZ = ( CK4MXD + 1 ) * QAVSIZ + CK4SFT */


/*     CK Type 5 parameters: */


/*     CK5DTP   CK data type 5 ID; */

/*     CK5MXD   maximum polynomial degree allowed in type 5 */
/*              records. */

/*     CK5MET   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 5 */
/*              CK record that passed between routines CKR05 and CKE05; */

/*     CK5MXP   maximum packet size for any subtype.  Subtype 2 */
/*              has the greatest packet size, since these packets */
/*              contain a quaternion, its derivative, an angular */
/*              velocity vector, and its derivative.  See ck05.inc */
/*              for a description of the subtypes. */

/*     CK5RSZ   maximum size of type 5 CK record passed between CKR05 */
/*              and CKE05; CK5RSZ is computed as follows: */

/*                 CK5RSZ = ( CK5MXD + 1 ) * CK5MXP + CK5MET */


/*     CK Type 6 parameters: */


/*     CK6DTP   CK data type 6 ID; */

/*     CK6MXD   maximum polynomial degree allowed in type 6 */
/*              records. */

/*     CK6MET   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 6 */
/*              CK record that passed between routines CKR06 and CKE06; */

/*     CK6MXP   maximum packet size for any subtype.  Subtype 2 */
/*              has the greatest packet size, since these packets */
/*              contain a quaternion, its derivative, an angular */
/*              velocity vector, and its derivative.  See ck06.inc */
/*              for a description of the subtypes. */

/*     CK6RSZ   maximum size of type 6 CK record passed between CKR06 */
/*              and CKE06; CK6RSZ is computed as follows: */

/*                 CK6RSZ = CK6MET + ( CK6MXD + 1 ) * ( CK6PS3 + 1 ) */

/*              where CK6PS3 is equal to the parameter CK06PS3 defined */
/*              in ck06.inc. Note that the subtype having the largest */
/*              packet size (subtype 2) does not give rise to the */
/*              largest record size, because that type is Hermite and */
/*              requires half the window size used by subtype 3 for a */
/*              given polynomial degree. */


/*     The parameter CK6PS3 must be in sync with C06PS3 defined in */
/*     ck06.inc. */



/*     Maximum record size that can be handled by CKPFS. This value */
/*     must be set to the maximum of all CKxRSZ parameters (currently */
/*     CK5RSZ.) */

/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the CK type 6 quaternion state */
/*     evaluator ZZCKE06. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 07-AUG-2015 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */

/*     Save all variables to avoid CSPICE stack overflow */
/*     problems. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZCKE06", (ftnlen)9);
/* ********************************************************************** */


/*     Error test cases */


/* ********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid subtype", (ftnlen)15);
    cleard_(&c__340, record);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    subtyp = -1;
    winsiz = 11;
    sclkdp = 0.;
    rate = 1.;
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Hermite subtype with invalid quaternion sequence", (ftnlen)48);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 0;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 8;
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	if (i__ == 3) {
	    quat[0] = -c__;
	} else {
	    quat[0] = c__;
	}
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)288)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)291)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)306)]);
    }
    i__1 = pktsiz * winsiz;
    moved_(packts, &i__1, &record[4]);
    ptr = pktsiz * winsiz + 5;
    moved_(clkbuf, &winsiz, &record[(i__1 = ptr - 1) < 340 && 0 <= i__1 ? 
	    i__1 : s_rnge("record", i__1, "f_zzcke06__", (ftnlen)315)]);
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(BADQUATSIGN)", ok, (ftnlen)18);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Subtype 0 divide-by zero case", (ftnlen)29);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 0;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 8;
    cleard_(&c__340, record);
    cleard_(&c_b24, packts);
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    i__1 = pktsiz * winsiz;
    moved_(packts, &i__1, &record[4]);
    ptr = pktsiz * winsiz + 5;
    moved_(clkbuf, &winsiz, &record[(i__1 = ptr - 1) < 340 && 0 <= i__1 ? 
	    i__1 : s_rnge("record", i__1, "f_zzcke06__", (ftnlen)350)]);
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Subtype 1 divide-by zero case", (ftnlen)29);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 1;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 4;
    cleard_(&c__340, record);
    cleard_(&c_b24, packts);
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    i__1 = pktsiz * winsiz;
    moved_(packts, &i__1, &record[4]);
    ptr = pktsiz * winsiz + 5;
    moved_(clkbuf, &winsiz, &record[(i__1 = ptr - 1) < 340 && 0 <= i__1 ? 
	    i__1 : s_rnge("record", i__1, "f_zzcke06__", (ftnlen)384)]);
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Subtype 2 divide-by zero case", (ftnlen)29);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 2;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 14;
    cleard_(&c__340, record);
    cleard_(&c_b24, packts);
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    i__1 = pktsiz * winsiz;
    moved_(packts, &i__1, &record[4]);
    ptr = pktsiz * winsiz + 5;
    moved_(clkbuf, &winsiz, &record[(i__1 = ptr - 1) < 340 && 0 <= i__1 ? 
	    i__1 : s_rnge("record", i__1, "f_zzcke06__", (ftnlen)418)]);
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Subtype 3 divide-by zero case", (ftnlen)29);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 3;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 7;
    cleard_(&c__340, record);
    cleard_(&c_b24, packts);
    record[0] = sclkdp;
    record[1] = (doublereal) subtyp;
    record[2] = (doublereal) winsiz;
    record[3] = rate;
    i__1 = pktsiz * winsiz;
    moved_(packts, &i__1, &record[4]);
    ptr = pktsiz * winsiz + 5;
    moved_(clkbuf, &winsiz, &record[(i__1 = ptr - 1) < 340 && 0 <= i__1 ? 
	    i__1 : s_rnge("record", i__1, "f_zzcke06__", (ftnlen)451)]);
    zzcke06_(record, qstate, &clkout);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);
/* ********************************************************************** */


/*     Normal test cases */


/* ********************************************************************** */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test using type 6, subtype 1 attitude record.", (ftnlen)45);

/*     We'll use a fixed axis and a rotation angle that */
/*     increases quadratically with time. */

/*     Our time step will be 1 second (10000 ticks ) */

    vpack_(&c_b51, &c_b52, &c_b53, axis);
    vhatip_(axis);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 1;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 4;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)508)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)511)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)519)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 1 test for I = #; SCLKDP #", (ftnlen)240, (
		ftnlen)34);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)550)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test using type 6, subtype 0 attitude record.", (ftnlen)45);

/*     We use the packet set from the previous test. */

    subtyp = 0;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 8;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)667)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)670)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)685)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)692)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 0 test for I = #; SCLKDP #", (ftnlen)240, (
		ftnlen)34);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)723)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test using type 6, subtype 2 attitude record.", (ftnlen)45);

/*     We use the packet set from the previous test. */

    subtyp = 2;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 14;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)841)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)844)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)859)]);

/*        Next up is angular velocity. */

	qdq2av_(quat, dq, av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(av, &packts[(i__2 = ptr + 7) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)867)]);

/*        We need angular acceleration. The angular velocity */
/*        direction is fixed; we just need the acceleration */
/*        of the rotation angle. We already have */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/*        so */


/*                d(ANGLE) */
/*              ------------ =   2 * ALPHA / TDELTA**2 */
/*              d**2(SCLKDP) */


/*        and */

/*              d(ANGLE) */
/*              ---------    =  ALPHA / (RATE*TDELTA)**2 */
/*              d**2(TDB) */


/* Computing 2nd power */
	d__2 = rate * tdelta;
	d__1 = alpha / (d__2 * d__2);
	vscl_(&d__1, axis, dav);
	vequ_(dav, &packts[(i__2 = ptr + 10) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)895)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)902)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 2 test for I = #; SCLKDP #", (ftnlen)240, (
		ftnlen)34);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)933)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test using type 6, subtype 3 attitude record.", (ftnlen)45);

/*     We use the packet set from the previous test. */

    subtyp = 3;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 7;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2);
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)1052)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1055)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1070)]);

/*        Next up is angular velocity. */

	qdq2av_(quat, dq, av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(av, &packts[(i__2 = ptr + 7) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1078)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)1085)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 3 test for I = #; SCLKDP #", (ftnlen)240, (
		ftnlen)34);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)1116)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);
    }
/* ********************************************************************** */


/*     Continuity test cases */


/* ********************************************************************** */

/*     M2Q output is discontinuous at the rotation angle pi. The output */
/*     of ZZCKE06 should be continuous. */

/*     We'll repeat the previous set of tests using sequence of rotation */
/*     angles that start out negative and cross over the boundary. */

/*     The total difference between the initial and final angle is about */
/*     1.21 degrees. We'll start with a rotation angle of 180-0.6 */
/*     degrees. */


/* ---- Case ------------------------------------------------------------- */

    tcase_("Continuity test using type 6, subtype 1 attitude record.", (
	    ftnlen)56);
    cleard_(&c__4, prevq);

/*     We'll use a fixed axis and a rotation angle that */
/*     increases quadratically with time. */

/*     Our time step will be 1 second (10000 ticks ) */

    vpack_(&c_b51, &c_b52, &c_b53, axis);
    vhatip_(axis);
    sclkdp = 0.;
    tdelta = 1e4;
    rate = 1e-4;
    subtyp = 1;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 4;
    angle0 = rpd_() * 179.40000000000001;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2) + angle0;
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)1270)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1273)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)1281)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 1 continuity test for I = #; SCLKDP #", (
		ftnlen)240, (ftnlen)45);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)1312)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);
	m2q_(xcmat, xquat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        Check quaternion "continuity." We expect a rotation angle */
/*        of less than two degrees between the quaternion and its */
/*        predecessor. Note that the dot product of two quaternions */
/*        Q1, Q2 is the real part of their quotient (Q1_conj * Q2). */

	if (i__ > 1) {
	    d__1 = vdotg_(quat, prevq, &c__4);
	    qqangl = acos(brcktd_(&d__1, &c_b268, &c_b51)) * dpr_();
	    chcksd_("QQANGL", &qqangl, "<", &c_b52, &c_b84, ok, (ftnlen)6, (
		    ftnlen)1);
	}

/*        Save the quaternion for the continuity check on the */
/*        next pass. */

	moved_(quat, &c__4, prevq);
    }

/* ---- Case ------------------------------------------------------------- */


    tcase_("Continuity test using type 6, subtype 0 attitude record.", (
	    ftnlen)56);

/*     We use the packet set from the previous test. */

    subtyp = 0;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 8;
    angle0 = rpd_() * 179.40000000000001;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2) + angle0;
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)1456)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1459)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1474)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)1481)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 0 continuity test for I = #; SCLKDP #", (
		ftnlen)240, (ftnlen)45);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)1512)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-12;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        Check quaternion "continuity." We expect a rotation angle */
/*        of less than two degrees between the quaternion and its */
/*        predecessor. Note that the dot product of two quaternions */
/*        Q1, Q2 is the real part of their quotient (Q1_conj * Q2). */

	if (i__ > 1) {
	    d__1 = vdotg_(quat, prevq, &c__4);
	    qqangl = acos(brcktd_(&d__1, &c_b268, &c_b51)) * dpr_();
	    chcksd_("QQANGL", &qqangl, "<", &c_b52, &c_b84, ok, (ftnlen)6, (
		    ftnlen)1);
	}

/*        Save the quaternion for the continuity check on the */
/*        next pass. */

	moved_(quat, &c__4, prevq);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Continuity test using type 6, subtype 2 attitude record.", (
	    ftnlen)56);

/*     We use the packet set from the previous test. */

    subtyp = 2;
    degree = 11;
    winsiz = (degree + 1) / 2;
    pktsiz = 14;
    angle0 = rpd_() * 179.40000000000001;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2) + angle0;
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)1655)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1658)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1673)]);

/*        Next up is angular velocity. */

	qdq2av_(quat, dq, av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(av, &packts[(i__2 = ptr + 7) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1681)]);

/*        We need angular acceleration. The angular velocity */
/*        direction is fixed; we just need the acceleration */
/*        of the rotation angle. We already have */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/*        so */


/*                d(ANGLE) */
/*              ------------ =   2 * ALPHA / TDELTA**2 */
/*              d**2(SCLKDP) */


/*        and */

/*              d(ANGLE) */
/*              ---------    =  ALPHA / (RATE*TDELTA)**2 */
/*              d**2(TDB) */


/* Computing 2nd power */
	d__2 = rate * tdelta;
	d__1 = alpha / (d__2 * d__2);
	vscl_(&d__1, axis, dav);
	vequ_(dav, &packts[(i__2 = ptr + 10) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1709)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)1716)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 2 continuity test for I = #; SCLKDP #", (
		ftnlen)240, (ftnlen)45);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)1747)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        Check quaternion "continuity." We expect a rotation angle */
/*        of less than two degrees between the quaternion and its */
/*        predecessor. Note that the dot product of two quaternions */
/*        Q1, Q2 is the real part of their quotient (Q1_conj * Q2). */

	if (i__ > 1) {
	    d__1 = vdotg_(quat, prevq, &c__4);
	    qqangl = acos(brcktd_(&d__1, &c_b268, &c_b51)) * dpr_();
	    chcksd_("QQANGL", &qqangl, "<", &c_b52, &c_b84, ok, (ftnlen)6, (
		    ftnlen)1);
	}

/*        Save the quaternion for the continuity check on the */
/*        next pass. */

	moved_(quat, &c__4, prevq);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Continuity test using type 6, subtype 3 attitude record.", (
	    ftnlen)56);

/*     We use the packet set from the previous test. */

    subtyp = 3;
    degree = 11;
    winsiz = degree + 1;
    pktsiz = 7;
    angle0 = rpd_() * 179.40000000000001;
    i__1 = winsiz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sclkdp = (i__ - 1) * tdelta;
	alpha = rpd_() * 100 / tdelta;
/* Computing 2nd power */
	i__2 = i__ - 1;
	angle = alpha * (i__2 * i__2) + angle0;
	d__1 = -angle;
	axisar_(axis, &d__1, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	c__ = cos(angle / 2);
	s = sin(angle / 2);
	quat[0] = c__;
	vscl_(&s, axis, &quat[1]);
	clkbuf[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge("clkbuf",
		 i__2, "f_zzcke06__", (ftnlen)1891)] = sclkdp;
	ptr = (i__ - 1) * pktsiz + 1;
	moved_(quat, &c__4, &packts[(i__2 = ptr - 1) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1894)]);

/*        Compute the quaternion derivative with respect */
/*        to TDB; store it after the quaternion. */

/*        As in the previous case, the angular rate */
/*        in radians/second is */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	dq[0] = s * -.5 * w;
	d__1 = c__ * .5 * w;
	vscl_(&d__1, axis, &dq[1]);
	moved_(dq, &c__4, &packts[(i__2 = ptr + 3) < 140000 && 0 <= i__2 ? 
		i__2 : s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1909)]);

/*        Next up is angular velocity. */

	qdq2av_(quat, dq, av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vequ_(av, &packts[(i__2 = ptr + 7) < 140000 && 0 <= i__2 ? i__2 : 
		s_rnge("packts", i__2, "f_zzcke06__", (ftnlen)1917)]);
    }
    nsamp = 300;
    span = clkbuf[(i__1 = winsiz - 1) < 10000 && 0 <= i__1 ? i__1 : s_rnge(
	    "clkbuf", i__1, "f_zzcke06__", (ftnlen)1924)] - clkbuf[0];
    delta = span / nsamp;
    i__1 = nsamp;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the sample time. */

	sclkdp = (i__ - 1) * delta;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Subtype 3 continuity test for I = #; SCLKDP #", (
		ftnlen)240, (ftnlen)45);
	repmi_(title, "#", &i__, title, (ftnlen)240, (ftnlen)1, (ftnlen)240);
	repmd_(title, "#", &sclkdp, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	tcase_(title, (ftnlen)240);

/*        Create a type 6 pointing record using the SCLK, subtype, and */
/*        packet count. We have to re-create the record for each call */
/*        because the CK evaluators use it destructively. */

	record[0] = sclkdp;
	record[1] = (doublereal) subtyp;
	record[2] = (doublereal) winsiz;
	record[3] = rate;
	i__2 = pktsiz * winsiz;
	moved_(packts, &i__2, &record[4]);
	ptr = pktsiz * winsiz + 5;
	moved_(clkbuf, &winsiz, &record[(i__2 = ptr - 1) < 340 && 0 <= i__2 ? 
		i__2 : s_rnge("record", i__2, "f_zzcke06__", (ftnlen)1955)]);
	size = (pktsiz + 1) * winsiz + 4;
	moved_(record, &size, rec2);

/*        Compute the angular velocity at the sample time. */
/*        Units are radians/second. */

/*           We have */

/*              ANGLE     =   ALPHA * (I-1)**2 */

/*              SCLKDP    =   (I-1)*TDELTA */

/*           so */

/*              ANGLE     =   ALPHA * (SCLKDP/TDELTA)**2 */

/*           and */

/*              d(ANGLE) */
/*              --------- =   2 * ALPHA * SCLKDP / TDELTA**2 */
/*              d(SCLKDP) */

/* Computing 2nd power */
	d__1 = tdelta;
	w = rate * 2 * alpha * (sclkdp / (d__1 * d__1));
	vscl_(&w, axis, av);

/*        Compute the attitude quaternion and its derivative */
/*        at SCLKDP. */

	zzcke06_(record, qstate, &clkout);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Create C-matrix and AV from the quaternion state. */

	q2m_(qstate, cmat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	qdq2av_(qstate, &qstate[4], av);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	moved_(qstate, &c__4, quat);

/*        Compute the expected C-matrix and angular velocity. */

	cke06_(&c_true, rec2, xcmat, xav, &xsclk);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check C-matrix derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("CMAT", cmat, "~~/", xcmat, &c__9, &tol, ok, (ftnlen)4, (
		ftnlen)3);

/*        Check C-matrix quotient rotation angle. */

	mtxm_(cmat, xcmat, qmat);
	raxisa_(qmat, qaxis, &qangle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-14;
	chcksd_("quotient angle", &qangle, "~", &c_b84, &tol, ok, (ftnlen)14, 
		(ftnlen)1);

/*        Check angular velocity derived from ZZCKE06 output state. */

	tol = 1e-14;
	chckad_("AV", av, "~~/", xav, &c__3, &tol, ok, (ftnlen)2, (ftnlen)3);

/*        Check output SCLK from ZZCKE06. */

	chcksd_("CLKOUT", &clkout, "~", &xsclk, &tol, ok, (ftnlen)6, (ftnlen)
		1);

/*        Check quaternion "continuity." We expect a rotation angle */
/*        of less than two degrees between the quaternion and its */
/*        predecessor. Note that the dot product of two quaternions */
/*        Q1, Q2 is the real part of their quotient (Q1_conj * Q2). */

	if (i__ > 1) {
	    d__1 = vdotg_(quat, prevq, &c__4);
	    qqangl = acos(brcktd_(&d__1, &c_b268, &c_b51)) * dpr_();
	    chcksd_("QQANGL", &qqangl, "<", &c_b52, &c_b84, ok, (ftnlen)6, (
		    ftnlen)1);
	}

/*        Save the quaternion for the continuity check on the */
/*        next pass. */

	moved_(quat, &c__4, prevq);
    }
    t_success__(ok);
    return 0;
} /* f_zzcke06__ */

