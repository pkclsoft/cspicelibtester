/* f_zzcnquad.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b134 = 0.;
static doublereal c_b164 = 2.;
static doublereal c_b165 = -37.;
static doublereal c_b167 = 35.;
static integer c__2 = 2;

/* $Procedure F_ZZCNQUAD ( ZZCNQUAD tests ) */
/* Subroutine */ int f_zzcnquad__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    extern /* Subroutine */ int zzcnquad_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *)
	    ;
    static doublereal xval1, xval2, a, b, c__, f;
    static integer n;
    extern /* Subroutine */ int tcase_(char *, ftnlen), rquad_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static char title[320];
    static doublereal c1[2], c2[2];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal r1, r2;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal ub;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen);
    static integer xn;
    extern doublereal touchd_(doublereal *);
    static doublereal xr1, xr2, tol;

/* $ Abstract */

/*     Exercise the cone intercept quadratic solution routine ZZCNQUAD. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the cone intercept quadratic solution */
/*     routine ZZCNQUAD. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 07-FEB-2017 (NJB) */

/*        Previous version 23-SEP-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZCNQUAD", (ftnlen)10);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Bound has absolute values that are too large.", (ftnlen)
	    320, (ftnlen)45);
    tcase_(title, (ftnlen)320);
    a = 1.;
    b = 1.;
    c__ = 1.;
    ub = 1e160;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    ub = -1e160;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* ********************************************************************** */

/*     Non-error exceptions */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Complex roots", (ftnlen)320, (ftnlen)13);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X + 2i ) * ( X - 2i ) = 0 */

/*     or */

/*         2 */
/*        X  + 4 = 0 */


/*     Use an upper bound of 10. */

    a = 1.;
    b = 0.;
    c__ = 4.;
    ub = 10.;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "First order equation, no roots excluded", (ftnlen)320, (
	    ftnlen)39);
    tcase_(title, (ftnlen)320);

/*     The equation is */


/*        X + 4 = 0 */


/*     Use an upper bound of 10. */

    a = 0.;
    b = 1.;
    c__ = 4.;
    ub = 10.;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = -4.;
    xr2 = -4.;
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", &xr2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "First order equation, both roots excluded", (ftnlen)320, (
	    ftnlen)41);
    tcase_(title, (ftnlen)320);

/*     The equation is */


/*        X + 4 = 0 */


/*     Use an upper bound of 4 - 1e-12. */

    a = 0.;
    b = 1.;
    c__ = 4.;
    ub = 3.9999999999989999;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate equation; form is 0 = 0. N should be set to -1."
	    , (ftnlen)320, (ftnlen)58);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*           2 */
/*        0*X  + 0*X + 0 = 0 */


/*     Use an upper bound of 10. */

    a = 0.;
    b = 0.;
    c__ = 0.;
    ub = 10.;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = -1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Degenerate equation; form is 1 = 0. N should be set to -2."
	    , (ftnlen)320, (ftnlen)58);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*           2 */
/*        0*X  + 0*X + 1 = 0 */


/*     Use an upper bound of 10. */

    a = 0.;
    b = 0.;
    c__ = 1.;
    ub = 10.;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = -2;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Trivial case: equation with two small real roots. The upp"
	    "er bound does not exclude roots.", (ftnlen)320, (ftnlen)89);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1 ) * ( X - 2 ) = 0 */

/*     or */

/*         2 */
/*        X  - 3X + 2 = 0 */


/*     Use an upper bound of 10. */

    a = 1.;
    b = -3.;
    c__ = 2.;
    ub = 10.;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 2;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1.;
    xr2 = 2.;
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", &xr2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two small real roots; the root of larger ma"
	    "gnitude is positive. The upper bound excludes the larger root.", (
	    ftnlen)320, (ftnlen)119);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1 ) * ( X - 200 ) = 0 */

/*     or */

/*         2 */
/*        X  - 201X + 200 = 0 */


/*     Use an upper bound of 200 - 1e-12 */

    a = 1.;
    b = -201.;
    c__ = 200.;
    ub = 199.99999999999901;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1.;
    xr2 = 0.;
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two small real roots; the root of larger ma"
	    "gnitude is positive. The upper bound excludes both roots.", (
	    ftnlen)320, (ftnlen)114);
    tcase_(title, (ftnlen)320);

/*     Use the equation above but change UB. */

    ub = .99999999999900002;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two small real roots; the root of larger ma"
	    "gnitude is negative. No roots are excluded.", (ftnlen)320, (
	    ftnlen)100);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1 ) * ( X + 200 ) = 0 */

/*     or */

/*         2 */
/*        X  + 199 X - 200 = 0 */


/*     Use an upper bound of 200 + 1e-12 */

    a = 1.;
    b = 199.;
    c__ = -200.;
    ub = 200.00000000000099;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 2;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1.;
    xr2 = -200.;
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", &xr2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two small real roots; the root of larger ma"
	    "gnitude is negative. The upper bound excludes the root of larger"
	    " magnitude.", (ftnlen)320, (ftnlen)132);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1 ) * ( X + 200 ) = 0 */

/*     or */

/*         2 */
/*        X  + 199 X - 200 = 0 */


/*     Use an upper bound of 200 - 1e-12 */

    a = 1.;
    b = 199.;
    c__ = -200.;
    ub = 199.99999999999901;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1.;
    xr2 = 0.;
    tol = 1e-12;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    tol = 0.;
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two small real roots; the root of larger ma"
	    "gnitude is negative. The upper bound excludes both roots.", (
	    ftnlen)320, (ftnlen)114);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1 ) * ( X + 200 ) = 0 */

/*     or */

/*         2 */
/*        X  + 199 X - 200 = 0 */


/*     Use an upper bound of 1 - 1e-12 */

    a = 1.;
    b = 199.;
    c__ = -200.;
    ub = .99999999999900002;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two large real roots. The upper bound exclu"
	    "des the larger root.", (ftnlen)320, (ftnlen)77);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1.D70 ) * ( X - 2.D70 ) = 0 */

/*     or */

/*         2 */
/*        X  - 3.D70 X + 2.D140 = 0 */


/*     Use an upper bound of 1.5D70 */

    a = 1.;
    b = -3e70;
    c__ = 2e140;
    ub = 1.5e70;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1e70;
    xr2 = 0.;
    tol = 1e-12;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "=", &xr2, &c_b134, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with one small real root, one root having magnit"
	    "ude near 1. The upper bound excludes the larger root. The quadra"
	    "tic and zero-order coefficients are small; the linear coefficien"
	    "t has magnitude near 1.", (ftnlen)320, (ftnlen)208);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( 1.D-5 X - 1.D-10 ) * ( 1.D-5 X - 1.D5 ) = 0 */

/*     or */

/*                2 */
/*        1.D-10 X  -(1+1.D-15) X + 1.D-5 =  0 */


/*     Use an upper bound of 1.D9 */

    a = 1e-10;
    b = -1.0000000000000011;
    c__ = 1e-5;
    ub = 1e9;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 1e-5;
    xr2 = 0.;
    tol = 1e-12;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "=", &xr2, &c_b134, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with one small real root, one root having magnit"
	    "ude near 1. The upper bound excludes both roots. The quadratic a"
	    "nd zero-order coefficients are small; the linear coefficient has"
	    " magnitude near 1.", (ftnlen)320, (ftnlen)203);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( 1.D-5 X - 1.D-10 ) * ( 1.D-5 X - 1.D5 ) = 0 */

/*     or */

/*                2 */
/*        1.D-10 X  -(1+1.D-15) X + 1.D-5 =  0 */


/*     Use an upper bound of 1.D9 */

    a = 1e-10;
    b = -1.0000000000000011;
    c__ = 1e-5;
    ub = 1e-6;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 1e-12;
    chcksd_("R1", &r1, "~/", &xr1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "=", &xr2, &c_b134, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Reciprocal solution case: negative discriminant.", (ftnlen)
	    320, (ftnlen)48);
    tcase_(title, (ftnlen)320);
    a = 1e-10;
    b = 1.;
    c__ = 1e11;
    ub = 1e9;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Reciprocal solution case: zero discriminant. Root has mul"
	    "tiplicity 2. Root magnitude is less than bound.", (ftnlen)320, (
	    ftnlen)104);
    tcase_(title, (ftnlen)320);
    a = pow_dd(&c_b164, &c_b165);
    b = 1.;
    c__ = pow_dd(&c_b164, &c_b167);
    ub = 1e15;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 1;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Reciprocal solution case: zero discriminant. Root has mul"
	    "tiplicity 2. Root is too large to accept.", (ftnlen)320, (ftnlen)
	    98);
    tcase_(title, (ftnlen)320);
    a = pow_dd(&c_b164, &c_b165);
    b = 1.;
    c__ = pow_dd(&c_b164, &c_b167);
    ub = 1e9;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation with two large real roots. The upper bound exclu"
	    "des both roots.", (ftnlen)320, (ftnlen)72);
    tcase_(title, (ftnlen)320);

/*     The equation is */

/*        ( X - 1.D70 ) * ( X - 2.D70 ) = 0 */

/*     or */

/*         2 */
/*        X  - 3.D70 X + 2.D140 = 0 */


/*     Use an upper bound of 1.D70 - 1.D-56 */

    a = 1.;
    b = -3e70;
    c__ = 2e140;
    ub = 9.9999999999999011e69;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xn = 0;
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xr1 = 0.;
    xr2 = 0.;
    tol = 0.;
    chcksd_("R1", &r1, "=", &xr1, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("R2", &r2, "=", &xr2, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation has very large B compared to both A and C.", (
	    ftnlen)320, (ftnlen)51);
    tcase_(title, (ftnlen)320);
    a = 3.;
    b = 4e11;
    c__ = 500.;
    ub = 1e20;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
/* Computing 2nd power */
    d__2 = r1;
    d__1 = a * (d__2 * d__2) + b * r1 + c__;
    f = touchd_(&d__1);
    tol = 1e-6;
    chcksd_("F(R1)", &f, "~", &c_b134, &tol, ok, (ftnlen)5, (ftnlen)1);
/*      WRITE (*,*) 'f(R1): ', F */

/*     Compare R1 and R2 to results from RQUAD: */

    rquad_(&a, &b, &c__, c1, c2);
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", c1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", c2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);
/*     F  = TOUCHD( A * R2**2  +  B * R2  +  C ) */
/*      WRITE (*,*) 'f(R2): ', F */
/*      WRITE (*,*) 'C1 = ', C1 */
/*      WRITE (*,*) 'C2 = ', C2 */
/*      WRITE (*,*) 'C1(1) - R1 = ', C1(1) - R1 */
/*      WRITE (*,*) 'C2(1) - R2 = ', C2(1) - R2 */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation has very large B compared to both A and C; B is "
	    "negative.", (ftnlen)320, (ftnlen)66);
    tcase_(title, (ftnlen)320);
    a = 3.;
    b = -4e11;
    c__ = 500.;
    ub = 1e20;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
/* Computing 2nd power */
    d__2 = r1;
    d__1 = a * (d__2 * d__2) + b * r1 + c__;
    f = touchd_(&d__1);
    tol = 1e-6;
    chcksd_("F(R1)", &f, "~", &c_b134, &tol, ok, (ftnlen)5, (ftnlen)1);
/*      WRITE (*,*) 'f(R1): ', F */

/*     Compare R1 and R2 to results from RQUAD: */

    rquad_(&a, &b, &c__, c1, c2);
    tol = 1e-14;
    if (abs(c1[0]) < abs(c2[0])) {
	xval1 = c1[0];
	xval2 = c2[0];
    } else {
	xval2 = c1[0];
	xval1 = c2[0];
    }
    chcksd_("R1", &r1, "~/", &xval1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", &xval2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);
/* Computing 2nd power */
    d__2 = r2;
    d__1 = a * (d__2 * d__2) + b * r2 + c__;
    f = touchd_(&d__1);
/*      WRITE (*,*) 'f(R2): ', F */
/*      WRITE (*,*) 'C1 = ', C1 */
/*      WRITE (*,*) 'C2 = ', C2 */
/*      WRITE (*,*) 'C1(1) - R1 = ', C1(1) - R1 */
/*      WRITE (*,*) 'C2(1) - R2 = ', C2(1) - R2 */

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation has very large B compared to A; C has the same s"
	    "cale as B.", (ftnlen)320, (ftnlen)67);
    tcase_(title, (ftnlen)320);
    a = 2.;
    b = 3e12;
    c__ = 4e12;
    ub = 1e40;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Compare R1 and R2 to results from RQUAD: */

    rquad_(&a, &b, &c__, c1, c2);
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", c1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", c2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    s_copy(title, "Equation has very large C compared to A,B.", (ftnlen)320, (
	    ftnlen)42);
    tcase_(title, (ftnlen)320);

/*     This is a case that led to severe loss of precision */
/*     in ZZBQUAD: */

    a = -1.11022302462515654e-16;
    b = 518.74106559784411;
    c__ = -920626.76641559054;
    zzcnquad_(&a, &b, &c__, &ub, &n, &r1, &r2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
/* Computing 2nd power */
    d__2 = r1;
    d__1 = a * (d__2 * d__2) + b * r1 + c__;
    f = touchd_(&d__1);
    tol = 1e-6;
    chcksd_("F(R1)", &f, "~", &c_b134, &tol, ok, (ftnlen)5, (ftnlen)1);

/*     Compare R1 and R2 to results from RQUAD: */

    rquad_(&a, &b, &c__, c1, c2);
    tol = 1e-14;
    chcksd_("R1", &r1, "~/", c1, &tol, ok, (ftnlen)2, (ftnlen)2);
    chcksd_("R2", &r2, "~/", c2, &tol, ok, (ftnlen)2, (ftnlen)2);

/*     Make sure the root of larger magnitude is the second */
/*     one. */

    d__1 = abs(r1);
    d__2 = abs(r2);
    chcksd_("|R1|", &d__1, "<=", &d__2, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzcnquad__ */

