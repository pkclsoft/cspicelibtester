/* f_zzctr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__2 = 2;
static logical c_false = FALSE_;
static logical c_true = TRUE_;

/* $Procedure F_ZZCTR ( Family of tests for ZZCTR ) */
/* Subroutine */ int f_zzctr__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    extern /* Subroutine */ int zzctrchk_(integer *, integer *, logical *), 
	    zzctrinc_(integer *), zzctrsin_(integer *), zzctruin_(integer *), 
	    tcase_(char *, ftnlen), movei_(integer *, integer *, integer *), 
	    topen_(char *, ftnlen), t_success__(logical *), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), chckxc_(logical *, char *, logical *, ftnlen), chcksi_(
	    char *, integer *, char *, integer *, integer *, logical *, 
	    ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen);
    logical update;
    integer oldctr[2];
    extern integer intmin_(void), intmax_(void);
    integer newctr[2], savctr[2];

/* $ Abstract */

/*     This routine tests state counter arrays. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     None */

/* $ Version */

/* -    Version 1.0.0  29-JUL-2013 (BVS) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZCTR", (ftnlen)7);

/*     Initialize counters to high value. */

    tcase_("Initialize counter to user values.", (ftnlen)34);
    zzctruin_(oldctr);
    i__1 = intmax_();
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmax_();
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Initialize counters to low value. */

    tcase_("Initialize counter to subsystem values.", (ftnlen)39);
    zzctrsin_(oldctr);
    i__1 = intmin_();
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmin_();
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Increment counters. */

    tcase_("Increment counter.", (ftnlen)18);
    zzctrinc_(oldctr);
    i__1 = intmin_() + 1;
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmin_();
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Increment counters with roll over. */

    tcase_("Increment counter with roll over.", (ftnlen)33);
    oldctr[0] = intmax_();
    zzctrinc_(oldctr);
    i__1 = intmin_();
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmin_() + 1;
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);

/*     Check counters without update. */

    tcase_("Check counter with no update.", (ftnlen)29);
    movei_(oldctr, &c__2, newctr);
    movei_(oldctr, &c__2, savctr);
    zzctrchk_(newctr, oldctr, &update);
    chckai_("OLDCTR", oldctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chckai_("NEWCTR", newctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*     Check with first element different and update. */

    tcase_("Check counter with update, first element.", (ftnlen)41);
    movei_(oldctr, &c__2, newctr);
    ++newctr[0];
    movei_(newctr, &c__2, savctr);
    zzctrchk_(newctr, oldctr, &update);
    chckai_("OLDCTR", oldctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chckai_("NEWCTR", newctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);

/*     Check with second element different and update. */

    tcase_("Check counter with update, second element.", (ftnlen)42);
    movei_(oldctr, &c__2, newctr);
    ++newctr[1];
    movei_(newctr, &c__2, savctr);
    zzctrchk_(newctr, oldctr, &update);
    chckai_("OLDCTR", oldctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chckai_("NEWCTR", newctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);

/*     Exception: try to increment counters at maximum value. */

    tcase_("Exception: try to increment at maximum value.", (ftnlen)45);
    zzctruin_(oldctr);
    i__1 = intmax_();
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmax_();
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);
    zzctrinc_(oldctr);
    chckxc_(&c_true, "SPICE(SPICEISTIRED)", ok, (ftnlen)19);
    i__1 = intmax_();
    chcksi_("OLDCTR(1)", oldctr, "=", &i__1, &c__0, ok, (ftnlen)9, (ftnlen)1);
    i__1 = intmax_();
    chcksi_("OLDCTR(2)", &oldctr[1], "=", &i__1, &c__0, ok, (ftnlen)9, (
	    ftnlen)1);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_zzctr__ */

