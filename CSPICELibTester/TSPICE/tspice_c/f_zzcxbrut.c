/* f_zzcxbrut.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 1.;
static doublereal c_b10 = 2.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static logical c_true = TRUE_;
static doublereal c_b61 = 3.;
static doublereal c_b104 = -1e10;
static doublereal c_b127 = -1.;
static doublereal c_b169 = 4.;
static doublereal c_b170 = 5.;
static doublereal c_b196 = 6.;

/* $Procedure      F_ZZCXBRUT ( Test brute force cone-segment intercept ) */
/* Subroutine */ int f_zzcxbrut__(logical *ok)
{
    doublereal apex[3], axis[3], xxpt[3];
    extern /* Subroutine */ int zzcxbrut_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *)
	    ;
    doublereal angle;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), topen_(char *, ftnlen),
	     t_success__(logical *);
    doublereal endpt1[3], endpt2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen);
    logical isbrck;
    doublereal tol, xpt[3];

/* $ Abstract */

/*     Test the SPICELIB brute force line segment-cone intersection */
/*     routine ZZCXBRUT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the routine ZZCXBRUT. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-JAN-2017 (NJB) */

/*        Previous version 30-SEP-2016 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */

/*      DOUBLE PRECISION      TIGHT */
/*      PARAMETER           ( TIGHT  = 1.D-12 ) */

/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZCXBRUT", (ftnlen)10);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to X-Y plane. First endpoin"
	    "t is outside cone.", (ftnlen)82);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b6, endpt1);
    vpack_(&c_b4, &c_b4, &c_b6, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b6, &c_b4, &c_b6, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to X-Y plane. First endpoin"
	    "t is inside cone.", (ftnlen)81);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b6, endpt2);
    vpack_(&c_b4, &c_b4, &c_b6, endpt1);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b6, &c_b4, &c_b6, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to Z axis. First endpoint i"
	    "s outside cone.", (ftnlen)79);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b4, endpt1);
    vpack_(&c_b10, &c_b4, &c_b61, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b10, &c_b4, &c_b10, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to Z axis. First endpoint i"
	    "s inside cone.", (ftnlen)78);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b4, endpt2);
    vpack_(&c_b10, &c_b4, &c_b61, endpt1);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b10, &c_b4, &c_b10, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to Z axis; first endpoint i"
	    "s far below X-Y plane.", (ftnlen)86);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b104, endpt1);
    vpack_(&c_b10, &c_b4, &c_b61, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b10, &c_b4, &c_b10, xxpt);
    tol = 1e-5;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is on the Z axis;first endpoint is belo"
	    "w X-Y plane.", (ftnlen)76);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b4, &c_b4, &c_b127, endpt1);
    vpack_(&c_b4, &c_b4, &c_b61, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b4, &c_b4, &c_b4, xxpt);
    tol = 1e-14;

/*     Use absolute test since the expected result is the */
/*     zero vector. */

    chckad_("XPT", xpt, "~~", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)2);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to X-Y plane. First endpoin"
	    "t is outside cone. Angle > pi/2.", (ftnlen)96);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() * 3 / 4;
    vpack_(&c_b10, &c_b4, &c_b127, endpt1);
    vpack_(&c_b4, &c_b4, &c_b127, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b6, &c_b4, &c_b127, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is +Z; line segment is parallel to X-Y plane. First endpoin"
	    "t is outside cone. Angle > pi/2. APEX is non-zero.", (ftnlen)114);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b61, &c_b169, &c_b170, apex);
    angle = pi_() * 3 / 4;
    vpack_(&c_b170, &c_b169, &c_b169, endpt1);
    vpack_(&c_b61, &c_b169, &c_b169, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b169, &c_b169, &c_b169, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("AXIS is -Z; line segment is parallel to X-Y plane. First endpoin"
	    "t is outside cone. Angle > pi/2. APEX is non-zero.", (ftnlen)114);
    vpack_(&c_b4, &c_b4, &c_b127, axis);
    vpack_(&c_b61, &c_b169, &c_b170, apex);
    angle = pi_() * 3 / 4;
    vpack_(&c_b170, &c_b169, &c_b196, endpt1);
    vpack_(&c_b61, &c_b169, &c_b196, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b169, &c_b169, &c_b196, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
    chcksl_("ISBRCK", &isbrck, &c_true, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("Root is not bracketed.", (ftnlen)22);
    vpack_(&c_b4, &c_b4, &c_b6, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b6, endpt1);
    vpack_(&c_b61, &c_b4, &c_b6, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISBRCK", &isbrck, &c_false, ok, (ftnlen)6);

/* --- Case -------------------------------------------------------- */

    tcase_("Error: axis is zero vector.", (ftnlen)27);
    vpack_(&c_b4, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b4, &c_b4, apex);
    angle = pi_() / 4;
    vpack_(&c_b10, &c_b4, &c_b6, endpt1);
    vpack_(&c_b61, &c_b4, &c_b6, endpt2);
    zzcxbrut_(apex, axis, &angle, endpt1, endpt2, xpt, &isbrck);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);
    t_success__(ok);
    return 0;
} /* f_zzcxbrut__ */

