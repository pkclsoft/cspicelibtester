/* f_zzdasgrc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c_b98 = -10000;
static integer c__2 = 2;
static doublereal c_b117 = 0.;
static integer c__3 = 3;

/* $Procedure F_ZZDASGRC ( DAS record fetch tests ) */
/* Subroutine */ int f_zzdasgrc__(logical *ok)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static integer obff;
    static doublereal drec[128];
    static integer free, irec[256], nlat, nlon;
    static char xidw[8];
    static integer unit;
    extern /* Subroutine */ int zzddhnfc_(integer *), zzdasgrd_(integer *, 
	    integer *, doublereal *), zzddhhlu_(integer *, char *, logical *, 
	    integer *, ftnlen), zzdasgri_(integer *, integer *, integer *), 
	    zzdasrfr_(integer *, char *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen, ftnlen);
    static integer b, e;
    extern /* Subroutine */ int dasac_(integer *, integer *, char *, ftnlen);
    static char label[40];
    static integer nread, ncomc;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer nnhan;
    static doublereal xdrec[128];
    static integer recno, xirec[256];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer ncomr;
    extern /* Subroutine */ int topen_(char *, ftnlen), dasa2l_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     t_success__(logical *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chckai_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    extern logical failed_(void);
    static integer clbase, handle;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    static char ifname[60];
    static integer natbff;
    extern /* Subroutine */ int delfil_(char *, ftnlen), dasrdd_(integer *, 
	    integer *, integer *, doublereal *), chckxc_(logical *, char *, 
	    logical *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    static integer nw;
    extern /* Subroutine */ int dasiod_(char *, integer *, integer *, 
	    doublereal *, ftnlen), dasrdi_(integer *, integer *, integer *, 
	    integer *);
    static char frname[32];
    extern /* Subroutine */ int dascls_(integer *);
    static integer bodyid, remain;
    extern /* Subroutine */ int dashfs_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     dasioi_(char *, integer *, integer *, integer *, ftnlen);
    static integer lastla[3];
    static char xifnam[60];
    extern /* Subroutine */ int unload_(char *, ftnlen);
    static integer xncomc;
    static char idword[8];
    static integer clsize, lastrc[3];
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    static integer surfid, lastwd[3], nresvc;
    extern /* Subroutine */ int dasopw_(char *, integer *, ftnlen), setmsg_(
	    char *, ftnlen), errint_(char *, integer *, ftnlen), dasopr_(char 
	    *, integer *, ftnlen);
    static integer xncomr, wordno;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen);
    extern logical exists_(char *, ftnlen);
    static integer nresvr, xnrsvc, xnrsvr;
    extern /* Subroutine */ int t_bingo__(char *, char *, integer *, ftnlen, 
	    ftnlen);

/* $ Abstract */

/*     Exercise the private SPICELIB DAS record fetch routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file das.inc */

/*     This include file declares public parameters for the DAS */
/*     subsystem. */

/*        Version 1.0.0 10-FEB-2017 (NJB) */

/*     Parameter declarations follow. */


/*     DAS file table size: */

/*        The parameter name is FTSIZE. The value of the parameter is */
/*        defined in the include file */

/*           zzddhman.inc */

/*        That value is duplicated here, since zzddhman.inc contains */
/*        other declarations that conflict with some of those in DAS */
/*        routines. */


/*     Capacity of DAS data records: */

/*        -- NWD double precision numbers. */
/*        -- NWI integers. */
/*        -- NWC characters. */
/*     These parameters are named to enhance ease of maintenance of */
/*     the code; the values should not be changed. */

/*     End of include file das.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB DAS routines */

/*        ZZDASGRD */
/*        ZZDASGRI */
/*        ZZDASRFR */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 31-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     These parameters must be kept consistent with zzddhman.inc. */
/*     That file is not included because of a parameter name conflict */
/*     with das.inc. */


/*     Other parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZDASGRC", (ftnlen)10);
/* *********************************************************************** */

/*     Setup */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create supporting kernels.", (ftnlen)33);
    if (exists_("zzdasgrc_test.tpc", (ftnlen)17)) {
	delfil_("zzdasgrc_test.tpc", (ftnlen)17);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tstpck_("zzdasgrc_test.tpc", &c_true, &c_false, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create tessellated ellipsoid DSK.", (ftnlen)40);
    bodyid = 499;
    surfid = 1;
    s_copy(frname, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    nlon = 40;
    nlat = 20;
    if (exists_("zzdasgrc_test.bds", (ftnlen)17)) {
	delfil_("zzdasgrc_test.bds", (ftnlen)17);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elds2z__(&bodyid, &surfid, frname, &nlon, &nlat, "zzdasgrc_test.bds", (
	    ftnlen)32, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add a few comments to the DSK. */

    dasopw_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dasac_(&handle, &c__1, "Comment line number one out of one.", (ftnlen)35);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create non-native version of DSK.", (ftnlen)40);

/*     Get the native format code and the code of the format */
/*     to which the file will be transformed. */


    zzddhnfc_(&natbff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (natbff == 2) {
	obff = 1;
    } else if (natbff == 1) {
	obff = 2;
    } else {
	setmsg_("Found unexpected native BFF code #.", (ftnlen)35);
	errint_("#", &natbff, (ftnlen)1);
	sigerr_("SPICE(UNSUPPORTEDBFF)", (ftnlen)21);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("nn_zzdasgrc_test.bds", (ftnlen)20)) {
	delfil_("nn_zzdasgrc_test.bds", (ftnlen)20);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_bingo__("zzdasgrc_test.bds", "nn_zzdasgrc_test.bds", &obff, (ftnlen)17, 
	    (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* *********************************************************************** */

/*     Test ZZDASRFR */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Open native DSK for read access; read file record.", (ftnlen)50);
    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdasrfr_(&handle, idword, ifname, &nresvr, &nresvc, &ncomr, &ncomc, (
	    ftnlen)8, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(xidw, "DAS/DSK", (ftnlen)8, (ftnlen)7);
    s_copy(xifnam, "zzdasgrc_test.bds", (ftnlen)60, (ftnlen)17);
    xnrsvr = 0;
    xnrsvc = 0;
    xncomr = 1;
    xncomc = 36;
    chcksc_("IDWORD", idword, "=", xidw, ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, 
	    (ftnlen)8);
    chcksc_("IFNAME", ifname, "=", xifnam, ok, (ftnlen)6, (ftnlen)60, (ftnlen)
	    1, (ftnlen)60);
    chcksi_("NRESVR", &nresvr, "=", &xnrsvr, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NRESVC", &nresvc, "=", &xnrsvc, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NCOMR", &ncomr, "=", &xncomr, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOMC", &ncomc, "=", &xncomc, &c__0, ok, (ftnlen)5, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    tcase_("Open NON-native DSK for read access; read file record.", (ftnlen)
	    54);
    dasopr_("nn_zzdasgrc_test.bds", &handle, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdasrfr_(&handle, idword, ifname, &nresvr, &nresvc, &ncomr, &ncomc, (
	    ftnlen)8, (ftnlen)60);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use the expected values from the previous test case. */

    chcksc_("IDWORD", idword, "=", xidw, ok, (ftnlen)6, (ftnlen)8, (ftnlen)1, 
	    (ftnlen)8);
    chcksc_("IFNAME", ifname, "=", xifnam, ok, (ftnlen)6, (ftnlen)60, (ftnlen)
	    1, (ftnlen)60);
    chcksi_("NRESVR", &nresvr, "=", &xnrsvr, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NRESVC", &nresvc, "=", &xnrsvc, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NCOMR", &ncomr, "=", &xncomr, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksi_("NCOMC", &ncomc, "=", &xncomc, &c__0, ok, (ftnlen)5, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     We test only exception (2) of ZZDASRFR. The others should */
/*     be covered by tests of lower-level routines. */


/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDASRFR: Invalid file handle", (ftnlen)29);
    zzdasrfr_(&c_b98, idword, ifname, &nresvr, &nresvc, &ncomr, &ncomc, (
	    ftnlen)8, (ftnlen)60);
    chckxc_(&c_true, "SPICE(NOSUCHHANDLE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

/*     CALL TCASE ( 'ZZDASRFR: error reading file' ) */

/*     This test has the undesirable side effect of leaving behind */
/*     a file with a system-dependent file name. We exclude this */
/*     case from standard TSPICE runs, but it can be run independently. */


/*     Open the file for read access, then delete it. */


/*     CALL DASOPR ( DSK, HANDLE ) */
/*     CALL CHCKXC ( .FALSE., ' ', OK ) */

/*     CALL KILFIL ( DSK ) */

/*     CALL ZZDASRFR ( HANDLE, IDWORD, IFNAME, NRESVR, */
/*    .                NRESVC, NCOMR,  NCOMC          ) */
/*     CALL CHCKXC ( .TRUE., 'SPICE(FILEREADFAILED)', OK ) */

/* *********************************************************************** */

/*     Test ZZDASGRD */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Check d.p. records read from native DSK.", (ftnlen)40);

/*     This is a consistency check only: DASRDD uses ZZDASGRD. */
/*     The next test case uses the independent routine DASIOD. */


/*     Get the DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of d.p. data words in the file. */

    nw = lastla[1];

/*     Read the d.p. data; compare with data that we fetch using */
/*     ZZDASGRD. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,128);
	e = b + nread - 1;
	dasrdd_(&handle, &b, &e, xdrec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__2, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgrd_(&handle, &recno, drec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "DREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, drec, "=", xdrec, &nread, &c_b117, ok, (ftnlen)40, (
		ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check d.p. records read from native DSK. Use unit-oriented recor"
	    "d reader to fetch expected record.", (ftnlen)98);

/*     Get the DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of d.p. data words in the file. */

    nw = lastla[1];

/*     Read the d.p. data; compare with data that we fetch using */
/*     ZZDASGRD. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,128);
	e = b + nread - 1;

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__2, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get an unlocked logical unit for the DSK file. */
/*        Read the expected record. */

	zzddhhlu_(&handle, "DAS", &c_false, &unit, (ftnlen)3);
	dasiod_("READ", &unit, &recno, xdrec, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgrd_(&handle, &recno, drec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "DREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, drec, "=", xdrec, &nread, &c_b117, ok, (ftnlen)40, (
		ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check d.p. records read from non-native DSK.", (ftnlen)44);

/*     Get the DSK's file summary info. */

    dasopr_("nn_zzdasgrc_test.bds", &handle, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of d.p. data words in the file. */

    nw = lastla[1];

/*     Read the d.p. data; compare with data that we fetch using */
/*     ZZDASGRD. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,128);
	e = b + nread - 1;
	dasrdd_(&handle, &b, &e, xdrec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__2, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgrd_(&handle, &recno, drec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "DREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, drec, "=", xdrec, &nread, &c_b117, ok, (ftnlen)40, (
		ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check d.p. records read from non-native DSK, comparing directly "
	    "to native data.", (ftnlen)79);

/*     Get the native DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Open the non-native DSK for read access as well. */

    dasopr_("nn_zzdasgrc_test.bds", &nnhan, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Let NW be the number of d.p. data words in the file. */

    nw = lastla[1];

/*     Read the d.p. data from the native file; compare with data that */
/*     we fetch from the non-native file using ZZDASGRD. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,128);
	e = b + nread - 1;
	dasrdd_(&handle, &b, &e, xdrec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. We expect */
/*        this record number to be invariant across BFFs. */

	dasa2l_(&handle, &c__2, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO in the non-native file. */

	zzdasgrd_(&nnhan, &recno, drec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "DREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, drec, "=", xdrec, &nread, &c_b117, ok, (ftnlen)40, (
		ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&nnhan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     We test only exception (1) of ZZDASGRD. The others should */
/*     be covered by tests of lower-level routines. */


/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDASGRD: Invalid file handle", (ftnlen)29);
    zzdasgrd_(&c_b98, &recno, drec);
    chckxc_(&c_true, "SPICE(NOSUCHHANDLE)", ok, (ftnlen)19);
/* *********************************************************************** */

/*     Test ZZDASGRI */

/* *********************************************************************** */
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Check integer records read from native DSK.", (ftnlen)43);

/*     This is a consistency check only: DASRDI uses ZZDASGRI. */
/*     The next test case uses the independent routine DASIOI. */


/*     Get the DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of integer data words in the file. */

    nw = lastla[2];

/*     Read the integer data; compare with data that we fetch using */
/*     ZZDASGRI. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,256);
	e = b + nread - 1;
	dasrdi_(&handle, &b, &e, xirec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__3, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgri_(&handle, &recno, irec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "IREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, irec, "=", xirec, &nread, ok, (ftnlen)40, (ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check integer records read from native DSK.Use unit-oriented rec"
	    "ord reader to fetch expected record.", (ftnlen)100);

/*     Get the DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of integer data words in the file. */

    nw = lastla[2];

/*     Read the integer data; compare with data that we fetch using */
/*     ZZDASGRI. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,256);
	e = b + nread - 1;

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__3, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get an unlocked unit and read the expected record. */

	zzddhhlu_(&handle, "DAS", &c_false, &unit, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dasioi_("READ", &unit, &recno, xirec, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgri_(&handle, &recno, irec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "IREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, irec, "=", xirec, &nread, ok, (ftnlen)40, (ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check integer records read from non-native DSK.", (ftnlen)47);

/*     Get the DSK's file summary info. */

    dasopr_("nn_zzdasgrc_test.bds", &handle, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Let NW be the number of integer data words in the file. */

    nw = lastla[2];

/*     Read the integer data; compare with data that we fetch using */
/*     ZZDASGRI. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,256);
	e = b + nread - 1;
	dasrdi_(&handle, &b, &e, xirec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. */

	dasa2l_(&handle, &c__3, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO. */

	zzdasgri_(&handle, &recno, irec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "IREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, irec, "=", xirec, &nread, ok, (ftnlen)40, (ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check integer records read from non-native DSK, comparing direct"
	    "ly to native data.", (ftnlen)82);

/*     Get the native DSK's file summary info. */

    dasopr_("zzdasgrc_test.bds", &handle, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dashfs_(&handle, &nresvr, &nresvc, &ncomr, &ncomc, &free, lastla, lastrc, 
	    lastwd);

/*     Open the non-native DSK for read access as well. */

    dasopr_("nn_zzdasgrc_test.bds", &nnhan, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Let NW be the number integer data words in the file. */

    nw = lastla[2];

/*     Read the integer data from the native file; compare with data that */
/*     we fetch from the non-native file using ZZDASGRI. */

    remain = nw;
    b = 1;
    while(remain > 0 && ! failed_()) {
	nread = min(remain,256);
	e = b + nread - 1;
	dasrdi_(&handle, &b, &e, xirec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the number of the record containing address B. We expect */
/*        this record number to be invariant across BFFs. */

	dasa2l_(&handle, &c__3, &b, &clbase, &clsize, &recno, &wordno);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Read the record at index RECNO in the non-native file. */

	zzdasgri_(&nnhan, &recno, irec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect an exact match. */

	s_copy(label, "IREC@ record #", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &recno, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, irec, "=", xirec, &nread, ok, (ftnlen)40, (ftnlen)1);

/*        Prepare for the next read, if any. */

	remain -= nread;
	b += nread;
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&nnhan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     We test only exception (1) of ZZDASGRI. The others should */
/*     be covered by tests of lower-level routines. */


/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDASGRI: Invalid file handle", (ftnlen)29);
    zzdasgri_(&c_b98, &recno, irec);
    chckxc_(&c_true, "SPICE(NOSUCHHANDLE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up: delete kernels.", (ftnlen)25);
    if (exists_("zzdasgrc_test.tpc", (ftnlen)17)) {
	delfil_("zzdasgrc_test.tpc", (ftnlen)17);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    unload_("zzdasgrc_test.bds", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdasgrc_test.bds", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("nn_zzdasgrc_test.bds", (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nn_zzdasgrc_test.bds", (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzdasgrc__ */

