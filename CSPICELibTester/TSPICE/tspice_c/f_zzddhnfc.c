/* f_zzddhnfc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* $Procedure F_ZZDDHNFC ( ZZDDHNFC tests ) */
/* Subroutine */ int f_zzddhnfc__(logical *ok)
{
    extern /* Subroutine */ int zzddhnfc_(integer *), zzddhgsd_(char *, 
	    integer *, char *, ftnlen, ftnlen), zzplatfm_(char *, char *, 
	    ftnlen, ftnlen), tcase_(char *, ftnlen), topen_(char *, ftnlen), 
	    t_success__(logical *), chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer natbff;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char bffstr[80], fmtstr[80];

/* $ Abstract */

/*     Exercise the SPICELIB routine ZZDDHNFC. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZDDHNFC. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 27-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZDDHNFC", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Fetch the integer BFF code for the host system.", (ftnlen)47);

/*     Look up the BFF integer code. */

    zzddhnfc_(&natbff);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Look up the equivalent string. */

    zzddhgsd_("BFF", &natbff, bffstr, (ftnlen)3, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzplatfm_("FILE_FORMAT", fmtstr, (ftnlen)11, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksc_("BFFSTR", bffstr, "=", fmtstr, ok, (ftnlen)6, (ftnlen)80, (ftnlen)
	    1, (ftnlen)80);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     None. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzddhnfc__ */

