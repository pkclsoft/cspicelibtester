/* f_zzdm.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static doublereal c_b8 = .1;
static doublereal c_b9 = 1e-12;
static doublereal c_b15 = 0.;
static logical c_true = TRUE_;
static doublereal c_b43 = 20.;
static doublereal c_b81 = 10.;

/* $Procedure F_ZZDM ( Test ZZDIV, ZZMULT ) */
/* Subroutine */ int f_zzdm__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double d_lg10(doublereal *), pow_dd(doublereal *, doublereal *);

    /* Local variables */
    integer seed;
    logical cont;
    doublereal mult, numr, a, b;
    integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal denom;
    extern doublereal dpmax_(void);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal expnt;
    extern doublereal zzdiv_(doublereal *, doublereal *);
    doublereal multx;
    extern /* Subroutine */ int t_success__(logical *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    doublereal logval;
    extern doublereal zzmult_(doublereal *, doublereal *);
    doublereal div;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);

/* $ Abstract */

/*     Test family to verify that ZZDIV behaves as expected. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 17-APR-2014 (EDW) */

/* -& */

/*     SPICELIB Functions */


/*     Local Variables */


/*     Start the test family with an open call. */

    topen_("F_ZZDM", (ftnlen)6);

/*     Initialize the random number seed. */

    seed = -6273618;

/*     Initialize the exponent value. */

    d__1 = dpmax_();
    expnt = (doublereal) ((integer) d_lg10(&d__1)) - 1.;

/*     Case 1 */

    tcase_("Safe division check.", (ftnlen)20);

/*     Standard, safe evaluation. */

    numr = 1.;
    denom = 10.;
    div = zzdiv_(&numr, &denom);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZDIV 1/10", &div, "~", &c_b8, &c_b9, ok, (ftnlen)10, (ftnlen)1);

/*     Case 2 */

    tcase_("Underflow division check.", (ftnlen)25);

/*     A numeric underflow event as defined in ZZDIV. No error, */
/*     return zero. */

    numr = 1e-200;
    denom = 1e200;
    div = zzdiv_(&numr, &denom);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZDIV 1D-400", &div, "=", &c_b15, &c_b15, ok, (ftnlen)12, (
	    ftnlen)1);

/*     Case 3 */

    tcase_("Overflow division check.", (ftnlen)24);

/*     A numeric overflow event as defined in ZZDIV. */

    numr = dpmax_();
    denom = 1.;
    div = zzdiv_(&numr, &denom);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);
    chcksd_("ZZDIV DPMAX/1", &div, "=", &c_b15, &c_b15, ok, (ftnlen)13, (
	    ftnlen)1);

/*     Case 4 */

    tcase_("1/0 division check.", (ftnlen)19);

/*     A divide by zero event. */

    numr = 1.;
    denom = 0.;
    div = zzdiv_(&numr, &denom);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);
    chcksd_("ZZDIV 1/0", &div, "=", &c_b15, &c_b15, ok, (ftnlen)9, (ftnlen)1);

/*     Case 5 */

    tcase_("0/0 division check.", (ftnlen)19);

/*     A 0/0 event. ZZDIV treats this as a divide by zero */
/*     event. */

    numr = 0.;
    denom = 0.;
    div = zzdiv_(&numr, &denom);
    chckxc_(&c_true, "SPICE(DIVIDEBYZERO)", ok, (ftnlen)19);
    chcksd_("ZZDIV 0/0", &div, "=", &c_b15, &c_b15, ok, (ftnlen)9, (ftnlen)1);

/*     Case 6 */

    tcase_("Safe multiplication check.", (ftnlen)26);

/*     Standard, safe evaluation. */

    a = 2.;
    b = 10.;
    mult = zzmult_(&a, &b);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZMULT 2*10", &mult, "~", &c_b43, &c_b9, ok, (ftnlen)11, (ftnlen)
	    1);

/*     Case 7 */

    tcase_("A numeric overflow event (1).", (ftnlen)29);
    a = 1.;
    b = dpmax_();
    mult = zzmult_(&a, &b);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);
    chcksd_("ZZMULT overflow (1)", &mult, "=", &c_b15, &c_b9, ok, (ftnlen)19, 
	    (ftnlen)1);

/*     Case 8 */

    tcase_("A numeric overflow event (2).", (ftnlen)29);
    a = 1e208;
    b = 1e307;
    mult = zzmult_(&a, &b);
    chckxc_(&c_true, "SPICE(NUMERICOVERFLOW)", ok, (ftnlen)22);
    chcksd_("ZZMULT overflow (2)", &mult, "=", &c_b15, &c_b9, ok, (ftnlen)19, 
	    (ftnlen)1);

/*     Case 9 */

    tcase_("Zero multiplication check (1).", (ftnlen)30);
    a = 0.;
    b = 10.;
    mult = zzmult_(&a, &b);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZMULT zero (1)", &mult, "=", &c_b15, &c_b9, ok, (ftnlen)15, (
	    ftnlen)1);

/*     Case 10 */

    tcase_("Zero multiplication check (2).", (ftnlen)30);
    a = 10.;
    b = 0.;
    mult = zzmult_(&a, &b);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZMULT zero (2)", &mult, "=", &c_b15, &c_b9, ok, (ftnlen)15, (
	    ftnlen)1);

/*     Case 11 */

    tcase_("Zero multiplication check (3).", (ftnlen)30);
    a = 0.;
    b = 0.;
    mult = zzmult_(&a, &b);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("ZZMULT zero (3)", &mult, "=", &c_b15, &c_b9, ok, (ftnlen)15, (
	    ftnlen)1);

/*     Note, the use of 5000 iterations is an arbitrary choice. */


/*     Case 12 */

    tcase_("Random multiplication check.", (ftnlen)28);
    for (i__ = 1; i__ <= 5000; ++i__) {
	d__2 = -expnt;
	d__1 = t_randd__(&d__2, &expnt, &seed);
	a = pow_dd(&c_b81, &d__1);
	d__2 = -expnt;
	d__1 = t_randd__(&d__2, &expnt, &seed);
	b = pow_dd(&c_b81, &d__1);
	cont = FALSE_;
	d__1 = abs(a);
	d__2 = abs(b);
	logval = d_lg10(&d__1) + d_lg10(&d__2);

/*        Pass over the calculation if the log10 of the calculated is not */
/*        within (-EXPNT, EXPNT). */

	cont = logval < expnt && logval > -expnt;
	if (cont) {
	    mult = zzmult_(&a, &b);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    multx = a * b;
	    chcksd_("Random multiplication", &mult, "=", &multx, &c_b9, ok, (
		    ftnlen)21, (ftnlen)1);
	}
    }

/*     Case 13 */

    tcase_("Random division check.", (ftnlen)22);
    for (i__ = 1; i__ <= 5000; ++i__) {
	d__2 = -expnt;
	d__1 = t_randd__(&d__2, &expnt, &seed);
	numr = pow_dd(&c_b81, &d__1);
	d__2 = -expnt;
	d__1 = t_randd__(&d__2, &expnt, &seed);
	denom = pow_dd(&c_b81, &d__1);
	cont = FALSE_;
	d__1 = abs(numr);
	d__2 = abs(denom);
	logval = d_lg10(&d__1) - d_lg10(&d__2);

/*        Pass over the calculation if the log10 of the calculated is not */
/*        within (-EXPNT, EXPNT) or if the denominator is zero. */

	cont = logval < expnt && logval > -expnt && denom != 0.;
	if (cont) {
	    div = zzdiv_(&numr, &denom);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    multx = numr / denom;
	    chcksd_("Random division", &div, "=", &multx, &c_b9, ok, (ftnlen)
		    15, (ftnlen)1);
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzdm__ */

