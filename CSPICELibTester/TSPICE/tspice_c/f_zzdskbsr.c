/* f_zzdskbsr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__8 = 8;
static integer c__24 = 24;
static doublereal c_b66 = 0.;
static integer c__10000 = 10000;
static integer c__5000 = 5000;
static integer c__499 = 499;
static integer c__599 = 599;

/* $Procedure F_ZZDSKBSR ( ZZDSKBSR tests ) */
/* Subroutine */ int f_zzdskbsr__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    static integer nbod, nseg;
    extern logical even_(integer *);
    static char dsks[255*5004];
    static integer nsat;
    extern logical zzdskbdc_();
    extern /* Subroutine */ int t_smldsk__(integer *, integer *, char *, char 
	    *, ftnlen, ftnlen), zzdskbbl_(integer *), zzdskchk_(integer *, 
	    logical *);
    extern logical zzdsksbd_(integer *);
    extern /* Subroutine */ int zzdsklsf_(char *, integer *, ftnlen), 
	    zzdskbsr_(char *, integer *, integer *, L_fp, integer *, logical *
	    , integer *, doublereal *, logical *, ftnlen), zzdskbss_(integer *
	    ), zzdskusf_(integer *);
    extern logical zzdsknot_();
    extern /* Subroutine */ int zzctruin_(integer *), zzdsksns_(L_fp, integer 
	    *, integer *, doublereal *, logical *);
    static integer i__, j, k, n;
    static char label[40], frame[32];
    static integer bthan;
    extern /* Subroutine */ int dskgd_(integer *, integer *, doublereal *), 
	    tcase_(char *, ftnlen);
    static integer class__;
    static char btdsk[255];
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), movei_(integer *, integer *, integer *), 
	    topen_(char *, ftnlen), t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), chckai_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), dlabbs_(integer 
	    *, integer *, logical *);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *), 
	    delfil_(char *, ftnlen);
    static integer framid;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), dlafns_(integer *, integer *, integer *, logical *);
    static integer handls[5004], bodyid;
    static doublereal dskdsc[24];
    static integer fileno, clssid, centrs[101];
    extern logical exists_(char *, ftnlen);
    static char ovrdsk[255];
    static doublereal xdskds[24];
    static integer dskctr[2], nearth, nxtdsc[8], ovrhan, prvdsc[8], surfid, 
	    xdlads[8];
    static logical retval, update;
    extern /* Subroutine */ int dlafps_(integer *, integer *, integer *, 
	    logical *), frinfo_(integer *, integer *, integer *, integer *, 
	    logical *), frmnam_(integer *, char *, ftnlen), setmsg_(char *, 
	    ftnlen), errint_(char *, integer *, ftnlen), sigerr_(char *, 
	    ftnlen);
    extern logical odd_(integer *);
    static logical fnd;

/* $ Abstract */

/*     Exercise entry points of the SPICELIB routine ZZDSKSBR. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests entry points of the SPICELIB routine KEEPER, */
/*     using DSK files. Entry points exercised by this test family are: */

/*        ZZDSKBSR (umbrella) */
/*        ZZDSKLSF */
/*        ZZDSKUSF */
/*        ZZDSKBSS */
/*        ZZDSKSNS */
/*        ZZDSKCHK */

/*     The routine */

/*        ZZDSKBBL */

/*     is also exercised by this test family. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 08-JUL-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL functions */


/*     ZZDSKBSR parameters */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Save everything in order to avoid stack problems on */
/*     some platforms. */

/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZDSKBSR", (ftnlen)10);
/* *********************************************************************** */

/*     ZZDSKBSR entry point tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSR error: direct call to ZZDSKBSR.", (ftnlen)40);
    zzdskbsr_(" ", &c__1, &c__1, (L_fp)zzdskbdc_, dskctr, &update, dladsc, 
	    dskdsc, &found, (ftnlen)1);
    chckxc_(&c_true, "SPICE(DSKBOGUSENTRY)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create two DSK files, each containing data for bodies 499"
	    " and 599.", (ftnlen)73);
    s_copy(dsks, "zzdskbsr_test_1.bds", (ftnlen)255, (ftnlen)19);
    s_copy(dsks + 255, "zzdskbsr_test_2.bds", (ftnlen)255, (ftnlen)19);
    for (i__ = 1; i__ <= 2; ++i__) {
	if (exists_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)269)) * 255, (
		ftnlen)255)) {
	    delfil_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)271)) * 255, 
		    (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	bodyid = 499;
	s_copy(frame, "IAU_MARS", (ftnlen)32, (ftnlen)8);
	surfid = i__;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__1 = i__ - 1) < 5004 &&
		 0 <= i__1 ? i__1 : s_rnge("dsks", i__1, "f_zzdskbsr__", (
		ftnlen)279)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Append segment. */

	bodyid = 599;
	s_copy(frame, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
	surfid = i__;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__1 = i__ - 1) < 5004 &&
		 0 <= i__1 ? i__1 : s_rnge("dsks", i__1, "f_zzdskbsr__", (
		ftnlen)289)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* *********************************************************************** */

/*     ZZDSKLSF/ZZDSKUSF/ZZDSKBSS/ZZDSKSNS basic tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSNS Error case: search for segments for body 499 without fi"
	    "rst calling ZZDSKBSS.", (ftnlen)85);
    bodyid = 499;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_true, "SPICE(CALLZZDSKBSSFIRST)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS Error case: search for segments for body 499. No DSKs h"
	    "ave been loaded yet.", (ftnlen)84);
    bodyid = 499;
    zzdskbss_(&bodyid);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load first DSK; search for segments for body 499.", (
	    ftnlen)59);

/*     Initialize the local DSK state counter. */

    zzctruin_(dskctr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load the first DSK file. */

    zzdsklsf_(dsks, handls, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 499;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find one segment. */

    chcksl_("FOUND (1)", &found, &c_true, ok, (ftnlen)9);
    if (found) {

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", handls, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
	dlabfs_(handls, xdlads, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	dskgd_(handls, xdlads, xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)
		1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, 
		(ftnlen)1);
    }

/*     Continue the search. We should not find another segment. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (2)", &found, &c_false, ok, (ftnlen)9);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Search for segments for body 599.", (ftnlen)43);
    bodyid = 599;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find one segment. */

    chcksl_("FOUND (1)", &found, &c_true, ok, (ftnlen)9);
    if (found) {

/*        Check the other outputs. We need to locate the second */
/*        segment in the file first. */

	chcksi_("HANDLE", &handle, "=", handls, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
	dlabfs_(handls, xdlads, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dlafns_(handls, xdlads, nxtdsc, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	movei_(nxtdsc, &c__8, xdlads);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLAFNS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	dskgd_(handls, xdlads, xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)
		1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, 
		(ftnlen)1);
    }

/*     Continue the search. We should not find another segment. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (2)", &found, &c_false, ok, (ftnlen)9);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Search for segments for body 699. None should be found. No error"
	    " should be signaled.", (ftnlen)84);
    bodyid = 699;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find...nothing. */

    chcksl_("FOUND (1)", &found, &c_false, ok, (ftnlen)9);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load second DSK; search for segments for body 499.", (
	    ftnlen)60);
    zzdsklsf_(dsks + 255, &handls[1], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 499;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 2;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find two segments. The first one found should */
/*        be in the file loaded last. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	if (found) {

/*           Check the other outputs. */

	    chcksi_("HANDLE", &handle, "=", &handls[(i__1 = j - 1) < 5004 && 
		    0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", 
		    (ftnlen)564)], &c__0, ok, (ftnlen)6, (ftnlen)1);

/*           Search backwards in the file for the expected segment. */
/*           In each case we're looking for the first segment in */
/*           the file. */

	    dlabbs_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)570)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dlafps_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)573)], 
		    xdlads, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DLAFPS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	    dskgd_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		    "handls", i__1, "f_zzdskbsr__", (ftnlen)582)], xdlads, 
		    xdskds);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (
		    ftnlen)1);
	    chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		    ftnlen)6, (ftnlen)1);
	}

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Keep both files loaded; search for segments for body 5"
	    "99.", (ftnlen)67);
    bodyid = 599;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 2;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find two segments. The first one found should */
/*        be in the file loaded last. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	if (found) {

/*           Check the other outputs. */

	    chcksi_("HANDLE", &handle, "=", &handls[(i__1 = j - 1) < 5004 && 
		    0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", 
		    (ftnlen)641)], &c__0, ok, (ftnlen)6, (ftnlen)1);

/*           Search backwards in the file for the expected segment. */
/*           In each case we're looking for the second segment in */
/*           the file. */

	    dlabbs_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)647)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DLABBS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	    dskgd_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		    "handls", i__1, "f_zzdskbsr__", (ftnlen)653)], xdlads, 
		    xdskds);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (
		    ftnlen)1);
	    chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		    ftnlen)6, (ftnlen)1);
	}

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load first DSK a second time; verify that the handle i"
	    "s unchanged.", (ftnlen)76);
    zzdsklsf_(dsks, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HANDLE", &handle, "=", handls, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load second DSK a second time; verify that the handle "
	    "is unchanged.", (ftnlen)77);
    zzdsklsf_(dsks + 255, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HANDLE", &handle, "=", &handls[1], &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSF: Unload the first file; keep the second file loaded; se"
	    "arch for segments for body 499.", (ftnlen)95);
    zzdskusf_(handls);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 499;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find one segment. */

    chcksl_("FOUND (1)", &found, &c_true, ok, (ftnlen)9);
    if (found) {

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[1], &c__0, ok, (ftnlen)6, (
		ftnlen)1);
	dlabfs_(&handls[1], xdlads, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	dskgd_(&handls[1], xdlads, xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)
		1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, 
		(ftnlen)1);
    }

/*     Continue the search. We should not find another segment. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (2)", &found, &c_false, ok, (ftnlen)9);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSF: Continue the previous case; search for segments for bo"
	    "dy 599.", (ftnlen)71);
    bodyid = 599;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find one segment. */

    chcksl_("FOUND (1)", &found, &c_true, ok, (ftnlen)9);
    if (found) {

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[1], &c__0, ok, (ftnlen)6, (
		ftnlen)1);

/*        Use a backward search here. */

	dlabbs_(&handls[1], xdlads, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	dskgd_(&handls[1], xdlads, xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)
		1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, 
		(ftnlen)1);
    }

/*     Continue the search. We should not find another segment. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (2)", &found, &c_false, ok, (ftnlen)9);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSF: unload the second file. Call ZZDSKBSS to try to start "
	    "a search for segments for body 499.", (ftnlen)99);
    zzdskusf_(&handls[1]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 499;
    zzdskbss_(&bodyid);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load first DSK a third time; verify that the handle is"
	    " new.", (ftnlen)69);
    zzdsklsf_(dsks, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    i__1 = handls[1] + 1;
    chcksi_("HANDLE", &handle, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    handls[0] = handle;

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: Load second DSK a third time; verify that the handle i"
	    "s unchanged.", (ftnlen)76);
    zzdsklsf_(dsks + 255, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    i__1 = handls[0] + 1;
    chcksi_("HANDLE", &handle, "=", &i__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    handls[1] = handle;

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: After reload: search for segments for body 499.", (
	    ftnlen)57);

/*     We must first build a segment list for body 499, so the segments */
/*     will be found in the expected order. */

    bodyid = 499;
    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 2;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find two segments. The first one found should */
/*        be in the file loaded last. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	if (found) {

/*           Check the other outputs. */

	    chcksi_("HANDLE", &handle, "=", &handls[(i__1 = j - 1) < 5004 && 
		    0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", 
		    (ftnlen)955)], &c__0, ok, (ftnlen)6, (ftnlen)1);

/*           Search backwards in the file for the expected segment. */
/*           In each case we're looking for the first segment in */
/*           the file. */

	    dlabbs_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)961)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dlafps_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)964)], 
		    xdlads, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DLAFPS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	    dskgd_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		    "handls", i__1, "f_zzdskbsr__", (ftnlen)973)], xdlads, 
		    xdskds);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (
		    ftnlen)1);
	    chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		    ftnlen)6, (ftnlen)1);
	}

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: After reload: search for segments for body 599.", (
	    ftnlen)57);
    bodyid = 599;

/*     Rebuild the segment list for 599. */

    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Start a search. */

    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 2;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find two segments. The first one found should */
/*        be in the file loaded last. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	if (found) {

/*           Check the other outputs. */

	    chcksi_("HANDLE", &handle, "=", &handls[(i__1 = j - 1) < 5004 && 
		    0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", 
		    (ftnlen)1042)], &c__0, ok, (ftnlen)6, (ftnlen)1);

/*           Search backwards in the file for the expected segment. */
/*           In each case we're looking for the second segment in */
/*           the file. */

	    dlabbs_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1048)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DLABBS FOUND", &fnd, &c_true, ok, (ftnlen)12);
	    dskgd_(&handls[(i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		    "handls", i__1, "f_zzdskbsr__", (ftnlen)1054)], xdlads, 
		    xdskds);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (
		    ftnlen)1);
	    chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		    ftnlen)6, (ftnlen)1);
	}

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* *********************************************************************** */


/*     ZZDSKLSF/ZZDSKBSS/ZZDSKSNS full capability tests */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK containing data for BTSIZE+1bodies. There are "
	    "two segments per body.", (ftnlen)86);
    s_copy(btdsk, "zzdskbsr_body_table.bds", (ftnlen)255, (ftnlen)23);
    if (exists_(btdsk, (ftnlen)255)) {
	delfil_(btdsk, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We want to take advantage of the SPICE built-in non-inertial */
/*     frames. We need BTSIZE+1 of them, so we must include the nonsense */
/*     frames such as those associated with planetary barycenters. */
/*     This doesn't matter from the perspective of this test. */


    nbod = 101;
    n = 0;
    i__ = 1;
    while(n < nbod && i__ < 110) {
	framid = i__ + 10000;
	frinfo_(&framid, &bodyid, &class__, &clssid, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Note that there's a gap in the sequence for class ID 3000; */
/*        there may be other gaps. */

/*        Also, we want to avoid using the earth, since IAU_EARTH */
/*        and EARTH_FIXED are both built-in frames centered on */
/*        the earth. This is an unnecessary complication. */

	if (found && bodyid != 399) {
	    ++n;
	    frmnam_(&framid, frame, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (s_cmp(frame, " ", (ftnlen)32, (ftnlen)1) == 0) {
		setmsg_("Frame name for ID # is blank.", (ftnlen)29);
		errint_("#", &framid, (ftnlen)1);
		sigerr_("SPICE(NOTRANSLATION)", (ftnlen)20);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	    surfid = n;
	    t_smldsk__(&bodyid, &surfid, frame, btdsk, (ftnlen)32, (ftnlen)
		    255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    surfid = -n;
	    t_smldsk__(&bodyid, &surfid, frame, btdsk, (ftnlen)32, (ftnlen)
		    255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    centrs[(i__1 = n - 1) < 101 && 0 <= i__1 ? i__1 : s_rnge("centrs",
		     i__1, "f_zzdskbsr__", (ftnlen)1150)] = bodyid;
	}
	++i__;
    }
    chcksi_("N", &n, "=", &nbod, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Load the DSK that contains BTSIZE+1 bodies. Look up data for eac"
	    "h one.", (ftnlen)70);

/*     This test will force one body out of the body list. */

    zzdsklsf_(btdsk, &bthan, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = nbod; i__ >= 1; --i__) {
	bodyid = centrs[(i__1 = i__ - 1) < 101 && 0 <= i__1 ? i__1 : s_rnge(
		"centrs", i__1, "f_zzdskbsr__", (ftnlen)1177)];
	zzdskbss_(&bodyid);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Prepare segment selection test function for use in BSR search. */

	retval = zzdsksbd_(&bodyid);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Perform the search. We expect to find two segments per body. */

	for (j = 1; j <= 2; ++j) {
	    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("ZZDSKSNS FOUND", &found, &c_true, ok, (ftnlen)14);
	    if (found) {

/*              Check the other outputs. */

		s_copy(label, "HANDLE I=@; J=@", (ftnlen)40, (ftnlen)15);
		repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (
			ftnlen)40);
		repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)
			40);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksi_(label, &handle, "=", &bthan, &c__0, ok, (ftnlen)40, (
			ftnlen)1);

/*              Search backwards in the file for the expected segment. */

		if (i__ == nbod && j == 1) {
		    dlabbs_(&bthan, xdlads, &fnd);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("DLABBS FOUND", &fnd, &c_true, ok, (ftnlen)12);
		} else {
		    dlafps_(&bthan, xdlads, prvdsc, &fnd);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("DLAFPS FOUND", &fnd, &c_true, ok, (ftnlen)12);
		    movei_(prvdsc, &c__8, xdlads);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
		dskgd_(&bthan, xdlads, xdskds);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(label, "DLADSC I=@; J=@", (ftnlen)40, (ftnlen)15);
		repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (
			ftnlen)40);
		repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)
			40);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckai_(label, dladsc, "=", xdlads, &c__8, ok, (ftnlen)40, (
			ftnlen)1);
		s_copy(label, "DSKDSC I=@; J=@", (ftnlen)40, (ftnlen)15);
		repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (
			ftnlen)40);
		repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)
			40);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_(label, dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
			ftnlen)40, (ftnlen)1);
	    }
	}
    }

/*     Unload this file to simplify the later tests. */

    zzdskusf_(&bthan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create FTSIZE+1 DSKs containing data for the earth. Each "
	    "file but the last contains two segments. The last one contains f"
	    "ive segments.", (ftnlen)141);
    nearth = 5001;
    i__1 = nearth + 2;
    for (i__ = 3; i__ <= i__1; ++i__) {
	s_copy(dsks + ((i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_zzdskbsr__", (ftnlen)1279)) * 255, "zzdskbs"
		"r_test_#.bds", (ftnlen)255, (ftnlen)19);
	repmi_(dsks + ((i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"dsks", i__2, "f_zzdskbsr__", (ftnlen)1280)) * 255, "#", &i__,
		 dsks + ((i__3 = i__ - 1) < 5004 && 0 <= i__3 ? i__3 : s_rnge(
		"dsks", i__3, "f_zzdskbsr__", (ftnlen)1280)) * 255, (ftnlen)
		255, (ftnlen)1, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (exists_(dsks + ((i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : 
		s_rnge("dsks", i__2, "f_zzdskbsr__", (ftnlen)1283)) * 255, (
		ftnlen)255)) {
	    delfil_(dsks + ((i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : 
		    s_rnge("dsks", i__2, "f_zzdskbsr__", (ftnlen)1285)) * 255,
		     (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	bodyid = 399;
	s_copy(frame, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
	surfid = i__;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__2 = i__ - 1) < 5004 &&
		 0 <= i__2 ? i__2 : s_rnge("dsks", i__2, "f_zzdskbsr__", (
		ftnlen)1293)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	surfid = -i__;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__2 = i__ - 1) < 5004 &&
		 0 <= i__2 ? i__2 : s_rnge("dsks", i__2, "f_zzdskbsr__", (
		ftnlen)1297)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    i__ = nearth + 2;
    for (j = 1; j <= 3; ++j) {
	surfid = j;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__1 = i__ - 1) < 5004 &&
		 0 <= i__1 ? i__1 : s_rnge("dsks", i__1, "f_zzdskbsr__", (
		ftnlen)1307)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: load FTSIZE-2 earth DSKs.", (ftnlen)35);

/*     Note that DSKs 1 and 2 are still loaded. */

    for (i__ = 3; i__ <= 5000; ++i__) {
	zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)1322)) * 255, &
		handls[(i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"handls", i__2, "f_zzdskbsr__", (ftnlen)1322)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the state change status. We expect that an update is */
/*        indicated. */

	zzdskchk_(dskctr, &update);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF error case: try to load one more earth DSK.", (ftnlen)52)
	    ;
    i__ = 5001;
    zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "dsks", i__1, "f_zzdskbsr__", (ftnlen)1345)) * 255, &handls[(i__2 
	    = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
	    "f_zzdskbsr__", (ftnlen)1345)], (ftnlen)255);
    chckxc_(&c_true, "SPICE(FTFULL)", ok, (ftnlen)13);

/*     The error message above brought to you by ZZDDHOPN. */


/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKLSF: unload first two DSKs and load remaining earth DSKs, e"
	    "xcept for the last one.", (ftnlen)87);
    for (i__ = 1; i__ <= 2; ++i__) {
	zzdskusf_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1360)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the state change status. We expect that an update is */
/*        indicated. */

	zzdskchk_(dskctr, &update);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    }
    for (i__ = 1; i__ <= 2; ++i__) {
	j = i__ + 5000;
	zzdsklsf_(dsks + ((i__1 = j - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_zzdskbsr__", (ftnlen)1377)) * 255, &handls[(
		i__2 = j - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("handls", 
		i__2, "f_zzdskbsr__", (ftnlen)1377)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check the state change status. We expect that an update is */
/*        indicated. */

	zzdskchk_(dskctr, &update);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: prepare to look up all earth segments but tho"
	    "se in the last earth file. Force ZZDSKBSR to build a segment lis"
	    "t for the earth via a call to ZZDSKBBL.", (ftnlen)167);
    bodyid = 399;
    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: look up all loaded earth segments ", (ftnlen)
	    53);

/*     Note that the last earth file is not loaded. */

    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 10000;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find STSIZE segments. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	fileno = (j - 1) / 2 + 3;

/*        Check the state change status. We expect that no update is */
/*        indicated. */

	zzdskchk_(dskctr, &update);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[(i__1 = fileno - 1) < 5004 && 
		0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (
		ftnlen)1478)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	if (odd_(&i__)) {

/*           Start a backward search for the expected */
/*           segment. */

	    dlabbs_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1485)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("(1) DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)16);
	} else {

/*           Continue the backward search for the */
/*           expected segment. */

	    dlafps_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1495)], 
		    dladsc, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	dskgd_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1503)], xdlads,
		 xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("(1) DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)10, (
		ftnlen)1);
	chckad_("(1) DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		ftnlen)10, (ftnlen)1);

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We should have found STSIZE segments. */

    chcksi_("num. segments found", &i__, "=", &c__10000, &c__0, ok, (ftnlen)
	    19, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: repeat look up.", (ftnlen)34);
    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 10000;
    i__ = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find STSIZE segments. */

	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	++i__;
	j = nseg + 1 - i__;
	fileno = (j - 1) / 2 + 3;

/*        Check the state change status. We expect that no update is */
/*        indicated. */

	zzdskchk_(dskctr, &update);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[(i__1 = fileno - 1) < 5004 && 
		0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (
		ftnlen)1585)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	if (odd_(&i__)) {

/*           Start a backward search for the expected */
/*           segment. */

	    dlabbs_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1592)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("(1) DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)16);
	} else {

/*           Continue the backward search for the */
/*           expected segment. */

	    dlafps_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1602)], 
		    dladsc, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	dskgd_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1610)], xdlads,
		 xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("(1) DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)10, (
		ftnlen)1);
	chckad_("(1) DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		ftnlen)10, (ftnlen)1);

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We should have found STSIZE segments. */

    chcksi_("num. segments found", &i__, "=", &c__10000, &c__0, ok, (ftnlen)
	    19, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: unload even-indexed earth DSKs. Look up remai"
	    "ning segments. ", (ftnlen)79);
    for (i__ = 4; i__ <= 5002; i__ += 2) {
	zzdskusf_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1643)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);

/*     Start a new search. */

    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 10000;

/*     I is the segment offset from the head of the list. I is 1-based. */
/*     K is the iteration count. */
    i__ = 0;
    k = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find STSIZE/2 segments. */

	++k;
	if (even_(&i__)) {
	    i__ += 3;
	} else {
	    ++i__;
	}
	j = nseg + 1 - i__;
	fileno = (j - 1) / 2 + 3;

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[(i__1 = fileno - 1) < 5004 && 
		0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (
		ftnlen)1703)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	if (even_(&j)) {

/*           Start a backward search in the file at index FILENO for the */
/*           expected segment. */

	    dlabbs_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1710)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("(1) DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)16);
	} else {

/*           Continue the backward search for the */
/*           expected segment. */

	    dlafps_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1720)], 
		    dladsc, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	dskgd_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1728)], xdlads,
		 xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("(1) DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)10, (
		ftnlen)1);
	chckad_("(1) DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		ftnlen)10, (ftnlen)1);

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We should have found STSIZE/2 segments. */

    chcksi_("num. segments found", &k, "=", &c__5000, &c__0, ok, (ftnlen)19, (
	    ftnlen)1);

/*     Reload the earth kernels indexed 1 through FTSIZE to restore */
/*     the original file priorities. */

    for (i__ = 3; i__ <= 5002; ++i__) {
	zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)1758)) * 255, &
		handls[(i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"handls", i__2, "f_zzdskbsr__", (ftnlen)1758)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Force construction of a new segment list. */

    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: unload odd-indexed earth DSKs. Look up remain"
	    "ing segments. ", (ftnlen)78);
    for (i__ = 3; i__ <= 5002; i__ += 2) {
	zzdskusf_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1780)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);

/*     Start a new search. */

    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 10000;

/*     I is the segment offset from the head of the list. I is 1-based. */
/*     K is the iteration count. */
    i__ = -2;
    k = 0;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    while(found) {

/*        We should find STSIZE/2 segments. */

	++k;
	if (even_(&i__)) {
	    i__ += 3;
	} else {
	    ++i__;
	}
	j = nseg + 1 - i__;
	fileno = (j - 1) / 2 + 3;

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[(i__1 = fileno - 1) < 5004 && 
		0 <= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (
		ftnlen)1842)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	if (even_(&j)) {

/*           Start a backward search in the file at index FILENO for the */
/*           expected segment. */

	    dlabbs_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1849)], 
		    xdlads, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("(1) DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)16);
	} else {

/*           Continue the backward search for the */
/*           expected segment. */

	    dlafps_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		    s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1859)], 
		    dladsc, prvdsc, &fnd);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    movei_(prvdsc, &c__8, xdlads);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	dskgd_(&handls[(i__1 = fileno - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)1867)], xdlads,
		 xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("(1) DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)10, (
		ftnlen)1);
	chckad_("(1) DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (
		ftnlen)10, (ftnlen)1);

/*        Fetch information for the next segment. */

	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We should have found STSIZE/2 segments. */

    chcksi_("num. segments found", &k, "=", &c__5000, &c__0, ok, (ftnlen)19, (
	    ftnlen)1);

/*     Reload the earth kernels indexed 1 through FTSIZE to restore */
/*     the original file priorities. */

    for (i__ = 3; i__ <= 5002; ++i__) {
	zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)1896)) * 255, &
		handls[(i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"handls", i__2, "f_zzdskbsr__", (ftnlen)1896)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Force construction of a new segment list. */

    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Unload the first earth DSK. Load the final earth DSK. Attempt a "
	    "search.", (ftnlen)71);

/*     The segment list should run out of room while the front of the */
/*     list is built. This should force the reconstruction of the list, */
/*     with eventual failure when the tail of the list cannot be */
/*     accommodated. */

    zzdskusf_(&handls[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    k = nearth + 2;
    zzdsklsf_(dsks + ((i__1 = k - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "dsks", i__1, "f_zzdskbsr__", (ftnlen)1926)) * 255, &handls[(i__2 
	    = k - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
	    "f_zzdskbsr__", (ftnlen)1926)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search over all segments. We expect a failure. */

/*     Use ZZDSKNOT to force a complete search. */

    zzdsksns_((L_fp)zzdsknot_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_true, "SPICE(BUFFEROVERFLOW)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Search for one earth segment. It should be located in the last f"
	    "ile loaded.", (ftnlen)75);
    k = nearth + 2;
    zzdsklsf_(dsks + ((i__1 = k - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "dsks", i__1, "f_zzdskbsr__", (ftnlen)1971)) * 255, &handls[(i__2 
	    = k - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
	    "f_zzdskbsr__", (ftnlen)1971)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that no update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_false, ok, (ftnlen)6);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    nseg = 10000;
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch data to compare. */

    dlabbs_(&handls[(i__1 = k - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge("hand"
	    "ls", i__1, "f_zzdskbsr__", (ftnlen)2015)], xdlads, &fnd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)12);

/*     Check the other outputs. */

    chcksi_("HANDLE", &handle, "=", &handls[(i__1 = k - 1) < 5004 && 0 <= 
	    i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)2023)
	    ], &c__0, ok, (ftnlen)6, (ftnlen)1);
    dskgd_(&handls[(i__1 = k - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge("handls"
	    , i__1, "f_zzdskbsr__", (ftnlen)2026)], xdlads, xdskds);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)1);
    chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Look up earth segments until we encounter a failure. We don't ha"
	    "ve enough room in the segment table for all earth segments.", (
	    ftnlen)123);
    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksns_((L_fp)zzdsknot_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_true, "SPICE(BUFFEROVERFLOW)", ok, (ftnlen)21);

/*     Unload the last earth DSK. */

    zzdskusf_(&handls[(i__1 = nearth + 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "handls", i__1, "f_zzdskbsr__", (ftnlen)2054)]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore the first FTSIZE earth DSKs. */

    for (i__ = 3; i__ <= 5002; ++i__) {
	zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("dsks", i__1, "f_zzdskbsr__", (ftnlen)2062)) * 255, &
		handls[(i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge(
		"handls", i__2, "f_zzdskbsr__", (ftnlen)2062)], (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Force construction of a new segment list. */

    zzdskbbl_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create a DSK file containing 10 segments for body 699.", (
	    ftnlen)61);

/*     To even create a new DSK, we need to unload one of them, */
/*     since we'll open a new DSK. We normally would need to */
/*     unload two files to accommodate the scratch DAS used for */
/*     DAS segregation, but T_SMLDSK doesn't segregate its output */
/*     file. */

    zzdskusf_(&handls[5001]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    nsat = 10;
    i__ = 5003;
    s_copy(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge("dsks"
	    , i__1, "f_zzdskbsr__", (ftnlen)2105)) * 255, "zzdskbsr_test_#.b"
	    "ds", (ftnlen)255, (ftnlen)19);
    repmi_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge("dsks"
	    , i__1, "f_zzdskbsr__", (ftnlen)2106)) * 255, "#", &i__, dsks + ((
	    i__2 = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("dsks", i__2, 
	    "f_zzdskbsr__", (ftnlen)2106)) * 255, (ftnlen)255, (ftnlen)1, (
	    ftnlen)255);
    if (exists_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "dsks", i__1, "f_zzdskbsr__", (ftnlen)2108)) * 255, (ftnlen)255)) 
	    {
	delfil_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_zzdskbsr__", (ftnlen)2110)) * 255, (ftnlen)
		255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    bodyid = 699;
    s_copy(frame, "IAU_SATURN", (ftnlen)32, (ftnlen)10);
    i__1 = nsat;
    for (j = 1; j <= i__1; ++j) {
	surfid = j;
	t_smldsk__(&bodyid, &surfid, frame, dsks + ((i__2 = i__ - 1) < 5004 &&
		 0 <= i__2 ? i__2 : s_rnge("dsks", i__2, "f_zzdskbsr__", (
		ftnlen)2121)) * 255, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKBSS/ZZDSKSNS: load the Saturn DSK. Look up the Saturn segme"
	    "nts. This look-up should cause the 399, 499, and 599 segment lis"
	    "ts to be dumped.", (ftnlen)144);

/*     Load the first DSK and find data for 499 and 599, so we have */
/*     segment lists that need to be removed. We have too many files */
/*     loaded, so unload an earth file first. */

    zzdskusf_(&handls[2]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsklsf_(dsks, handls, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdskbbl_(&c__499);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdskbbl_(&c__599);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__ = 5003;
    zzdsklsf_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "dsks", i__1, "f_zzdskbsr__", (ftnlen)2154)) * 255, &handls[(i__2 
	    = i__ - 1) < 5004 && 0 <= i__2 ? i__2 : s_rnge("handls", i__2, 
	    "f_zzdskbsr__", (ftnlen)2154)], (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the state change status. We expect that an update is */
/*     indicated. */

    zzdskchk_(dskctr, &update);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("UPDATE", &update, &c_true, ok, (ftnlen)6);
    bodyid = 699;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Prepare segment selection test function for use in BSR search. */

    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Perform the search. */

    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find NSAT segments from DSK #(FTSIZE+3). */

    chcksl_("FOUND (1)", &found, &c_true, ok, (ftnlen)9);
    dlabbs_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "handls", i__1, "f_zzdskbsr__", (ftnlen)2188)], xdlads, &fnd);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLABFS FOUND", &fnd, &c_true, ok, (ftnlen)12);
    j = 0;
    while(found && fnd && j < nsat) {
	++j;

/*        Check the other outputs. */

	chcksi_("HANDLE", &handle, "=", &handls[(i__1 = i__ - 1) < 5004 && 0 
		<= i__1 ? i__1 : s_rnge("handls", i__1, "f_zzdskbsr__", (
		ftnlen)2201)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	dskgd_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		"handls", i__1, "f_zzdskbsr__", (ftnlen)2204)], xdlads, 
		xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_("DLADSC", dladsc, "=", xdlads, &c__8, ok, (ftnlen)6, (ftnlen)
		1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b66, ok, (ftnlen)6, 
		(ftnlen)1);

/*        Continue to search backwards. */

	dlafps_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		"handls", i__1, "f_zzdskbsr__", (ftnlen)2215)], xdlads, 
		prvdsc, &fnd);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	movei_(prvdsc, &c__8, xdlads);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSF test: cause 699 body table entry to be deleted, when 69"
	    "9 is not at index NBT", (ftnlen)85);

/*     To set this up, we'll put the earth body table entry at */
/*     the end of the table. We need to do an earth lookup to */
/*     get the earth back into the body table, while avoiding */
/*     removal of the Saturn list. Make the set of earth segments */
/*     smaller by unloading some earth files. */

    for (i__ = 3; i__ <= 12; ++i__) {
	zzdskusf_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)2241)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Now do an earth search. */

    bodyid = 399;
    zzdskbss_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    retval = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksns_((L_fp)zzdskbdc_, &handle, dladsc, dskdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     All of the preceding was to get the earth into the last */
/*     position of the body table. Now unload the Saturn file. */

    k = 5003;
    zzdskusf_(&handls[(i__1 = k - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
	    "handls", i__1, "f_zzdskbsr__", (ftnlen)2263)]);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSNS test: search a file that has more than STSIZE segments.",
	     (ftnlen)64);

/*     Create a monster DSK. */

    s_copy(ovrdsk, "zzdskbsr_toobig.bds", (ftnlen)255, (ftnlen)19);
    if (exists_(ovrdsk, (ftnlen)255)) {
	delfil_(ovrdsk, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    bodyid = 999;
    for (i__ = 1; i__ <= 10001; ++i__) {
	s_copy(frame, "IAU_PLUTO", (ftnlen)32, (ftnlen)9);
	surfid = i__;
	t_smldsk__(&bodyid, &surfid, frame, ovrdsk, (ftnlen)32, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Try to build a segment list for 999. */

    zzdsklsf_(ovrdsk, &ovrhan, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdskbbl_(&bodyid);
    chckxc_(&c_true, "SPICE(BUFFEROVERFLOW)", ok, (ftnlen)21);

/*     Clean up. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzdskusf_(&handls[(i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : 
		s_rnge("handls", i__1, "f_zzdskbsr__", (ftnlen)2319)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(dsks + ((i__1 = i__ - 1) < 5004 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_zzdskbsr__", (ftnlen)2322)) * 255, (ftnlen)
		255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    zzdskusf_(&bthan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(btdsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdskusf_(&ovrhan);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_(ovrdsk, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzdskbsr__ */

