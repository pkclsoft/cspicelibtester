/* f_zzdskbux.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;
static integer c__3 = 3;
static integer c__10000 = 10000;
static integer c__20000 = 20000;
static doublereal c_b168 = 0.;
static integer c__24 = 24;
static integer c__1 = 1;
static integer c_n1 = -1;
static doublereal c_b835 = 1.;
static doublereal c_b836 = .001;

/* $Procedure F_ZZDSKBUX ( ZZDSKBUX tests ) */
/* Subroutine */ int f_zzdskbux__(logical *ok)
{
    /* Initialized data */

    static integer bids[2] = { 499,699 };
    static char targs[32*2] = "MARS                            " "SATURN    "
	    "                      ";
    static char fixrfs[32*2] = "IAU_MARS                        " "IAU_SATUR"
	    "N                      ";

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), i_dnnt(doublereal *);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal dlat;
    static integer plid;
    static doublereal dlon;
    static integer fixh, nlat, axes[24]	/* was [3][2][4] */;
    static doublereal last;
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static integer fixw;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), mtxv_(doublereal *, 
	    doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int t_topker__(char *, char *, char *, char *, 
	    integer *, integer *, char *, doublereal *, char *, integer *, 
	    doublereal *, doublereal *, integer *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), zzellsec_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, integer *, integer *, 
	    integer *, integer *, doublereal *, integer *, integer *), 
	    zzsbfnrm_(integer *, integer *, integer *, doublereal *, integer *
	    , doublereal *, doublereal *), t_wrtplt__(integer *, integer *, 
	    char *, doublereal *, doublereal *, integer *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, integer *, 
	    logical *, char *, ftnlen, ftnlen), zzsbfxri_(integer *, integer *
	    , integer *, doublereal *, integer *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    integer *, logical *);
    static doublereal a, b, c__;
    static integer h__, i__, j, n;
    static char label[40];
    static doublereal r__, scale, radii[3];
    static integer w;
    static char frame[32];
    extern /* Subroutine */ int lcase_(char *, char *, ftnlen, ftnlen), 
	    dskgd_(integer *, integer *, doublereal *), tcase_(char *, ftnlen)
	    , dskn02_(integer *, integer *, integer *, doublereal *);
    static doublereal dplat, tileh;
    static integer class__;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), vpack_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    extern doublereal jyear_(void);
    static doublereal dplon;
    static integer nplat;
    static logical found;
    static char topfk[255];
    static doublereal tilew;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nplon;
    static doublereal first;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), dskx02_(integer *, integer *, doublereal 
	    *, doublereal *, integer *, doublereal *, logical *), vlcom_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal vtemp[3], xform[9]	/* was [3][3] */;
    static integer nsurf;
    static doublereal verts[30000]	/* was [3][10000] */;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal dc[1];
    static integer ic[1];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    static doublereal et;
    static integer dladsc[8], handle, np;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static doublereal lt;
    static integer segfid;
    extern doublereal halfpi_(void);
    static integer nv;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     cleard_(integer *, doublereal *), bodvcd_(integer *, char *, 
	    integer *, integer *, doublereal *, ftnlen);
    extern logical exists_(char *, ftnlen);
    static char fixref[32], sitfnm[32*2*4*2], sitnms[32*2*4*2], topspk[255], 
	    target[32], tildsk[255*3*6*3*2];
    static doublereal angles[24]	/* was [3][2][4] */, bounds[4]	/* 
	    was [2][2] */, corpar[10], ctrlat, ctrlon, dfrlat, dfrlon, dskdsc[
	    24], lat, lon, maxlat, maxlon, minlat, minlon, normal[3], offset[
	    3], raydir[3], segdir[3], segvtx[3], sitpos[48]	/* was [3][2][
	    4][2] */, tol, vertex[3], xdskds[24], xnorml[3], xpt[3];
    static integer bix, bodyid, clssid, corsys, fixfid, frmctr, layrix, 
	    laysrf[300]	/* was [100][3] */, nsites, plates[20000], sitfid[16]	
	    /* was [2][4][2] */, sitids[16]	/* was [2][4][2] */, surfid, 
	    targix, vix;
    static logical makvtl, usepad;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     suffix_(char *, integer *, char *, ftnlen, ftnlen), srfrec_(
	    integer *, doublereal *, doublereal *, doublereal *), furnsh_(
	    char *, ftnlen), spkezp_(integer *, doublereal *, char *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen, ftnlen), pxform_(
	    char *, char *, doublereal *, doublereal *, ftnlen, ftnlen), mxv_(
	    doublereal *, doublereal *, doublereal *), vminus_(doublereal *, 
	    doublereal *), namfrm_(char *, integer *, ftnlen), chcksl_(char *,
	     logical *, logical *, logical *, ftnlen), frinfo_(integer *, 
	    integer *, integer *, integer *, logical *), refchg_(integer *, 
	    integer *, doublereal *, doublereal *), chcksi_(char *, integer *,
	     char *, integer *, integer *, logical *, ftnlen, ftnlen), 
	    vsclip_(doublereal *, doublereal *), latrec_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), kclear_(void), 
	    t_secds2__(integer *, integer *, char *, doublereal *, doublereal 
	    *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, integer *, logical *, 
	    logical *, char *, ftnlen, ftnlen), zzsbfxr_(integer *, integer *,
	     integer *, doublereal *, integer *, doublereal *, doublereal *, 
	    doublereal *, logical *);

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZDSKBUX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routines */

/*        ZZDSKBUX */
/*        ZZSBFXR */
/*        ZZSBFXRI */

/*     as well as */

/*        ZZDSKBUN */
/*        ZZSBFNRM */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 22-JUL-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */

/*      DOUBLE PRECISION      VTIGHT */
/*      PARAMETER           ( VTIGHT = 1.D-14 ) */

/*     Local Variables */

/*      INTEGER               K */
/*      INTEGER               L */
/*      INTEGER               NFLAT */
/*      INTEGER               NFLON */
/*      INTEGER               SRFLST ( MAXSRF ) */

/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZDSKBUX", (ftnlen)10);
/* *********************************************************************** */

/*     Set-up */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Create topocentric kernels.", (ftnlen)27);

/*     Our setup will be rather elaborate, since we're creating a */
/*     set of kernels that can be used to exercise all logic branches */
/*     of a moderately complex algorithm. */

/*     We'll need a generic text PCK. Keep the file after loading it. */

    if (exists_("zzdskbux_test0.tpc", (ftnlen)18)) {
	delfil_("zzdskbux_test0.tpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    tstpck_("zzdskbux_test0.tpc", &c_true, &c_true, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a set of topocentric frames for Mars and Saturn. */

    first = jyear_() * -100;
    last = jyear_() * 100;
    nsites = 8;
    dfrlon = pi_() * 2 / 4;
    dfrlat = pi_() / 2;
    for (i__ = 1; i__ <= 2; ++i__) {
	if (i__ == 1) {
	    s_copy(topfk, "zzdskbux_mars_topo.tf", (ftnlen)255, (ftnlen)21);
	    s_copy(topspk, "zzdskbux_mars_topo.bsp", (ftnlen)255, (ftnlen)22);
	} else {
	    s_copy(topfk, "zzdskbux_saturn_topo.tf", (ftnlen)255, (ftnlen)23);
	    s_copy(topspk, "zzdskbux_saturn_topo.bsp", (ftnlen)255, (ftnlen)
		    24);
	}
	s_copy(target, targs + (((i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : 
		s_rnge("targs", i__1, "f_zzdskbux__", (ftnlen)374)) << 5), (
		ftnlen)32, (ftnlen)32);
	bodyid = bids[(i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge("bids"
		, i__1, "f_zzdskbux__", (ftnlen)375)];
	s_copy(fixref, fixrfs + (((i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : 
		s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)376)) << 5), (
		ftnlen)32, (ftnlen)32);
	for (w = 1; w <= 4; ++w) {
	    lon = (w - .5f) * dfrlon;
	    for (h__ = 1; h__ <= 2; ++h__) {
		lat = halfpi_() - (h__ - .5) * dfrlat;
		s_copy(sitnms + (((i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 
			16 && 0 <= i__1 ? i__1 : s_rnge("sitnms", i__1, "f_z"
			"zdskbux__", (ftnlen)386)) << 5), "#_H#_W#", (ftnlen)
			32, (ftnlen)7);
		repmc_(sitnms + (((i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 
			16 && 0 <= i__1 ? i__1 : s_rnge("sitnms", i__1, "f_z"
			"zdskbux__", (ftnlen)387)) << 5), "#", target, sitnms 
			+ (((i__2 = h__ + (w + (i__ << 2) << 1) - 11) < 16 && 
			0 <= i__2 ? i__2 : s_rnge("sitnms", i__2, "f_zzdskbu"
			"x__", (ftnlen)387)) << 5), (ftnlen)32, (ftnlen)1, (
			ftnlen)32, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(sitnms + (((i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 
			16 && 0 <= i__1 ? i__1 : s_rnge("sitnms", i__1, "f_z"
			"zdskbux__", (ftnlen)390)) << 5), "#", &h__, sitnms + (
			((i__2 = h__ + (w + (i__ << 2) << 1) - 11) < 16 && 0 
			<= i__2 ? i__2 : s_rnge("sitnms", i__2, "f_zzdskbux__"
			, (ftnlen)390)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)
			32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(sitnms + (((i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 
			16 && 0 <= i__1 ? i__1 : s_rnge("sitnms", i__1, "f_z"
			"zdskbux__", (ftnlen)393)) << 5), "#", &w, sitnms + (((
			i__2 = h__ + (w + (i__ << 2) << 1) - 11) < 16 && 0 <= 
			i__2 ? i__2 : s_rnge("sitnms", i__2, "f_zzdskbux__", (
			ftnlen)393)) << 5), (ftnlen)32, (ftnlen)1, (ftnlen)32)
			;
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		sitids[(i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 16 && 0 <= 
			i__1 ? i__1 : s_rnge("sitids", i__1, "f_zzdskbux__", (
			ftnlen)396)] = bodyid * 1000 + h__ + (w - 1 << 1);
		s_copy(sitfnm + (((i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 
			16 && 0 <= i__1 ? i__1 : s_rnge("sitfnm", i__1, "f_z"
			"zdskbux__", (ftnlen)397)) << 5), sitnms + (((i__2 = 
			h__ + (w + (i__ << 2) << 1) - 11) < 16 && 0 <= i__2 ? 
			i__2 : s_rnge("sitnms", i__2, "f_zzdskbux__", (ftnlen)
			397)) << 5), (ftnlen)32, (ftnlen)32);
		suffix_("_TOPO", &c__0, sitfnm + (((i__1 = h__ + (w + (i__ << 
			2) << 1) - 11) < 16 && 0 <= i__1 ? i__1 : s_rnge(
			"sitfnm", i__1, "f_zzdskbux__", (ftnlen)398)) << 5), (
			ftnlen)5, (ftnlen)32);
		sitfid[(i__1 = h__ + (w + (i__ << 2) << 1) - 11) < 16 && 0 <= 
			i__1 ? i__1 : s_rnge("sitfid", i__1, "f_zzdskbux__", (
			ftnlen)400)] = sitids[(i__2 = h__ + (w + (i__ << 2) <<
			 1) - 11) < 16 && 0 <= i__2 ? i__2 : s_rnge("sitids", 
			i__2, "f_zzdskbux__", (ftnlen)400)];
		axes[(i__1 = (h__ + (w << 1)) * 3 - 9) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("axes", i__1, "f_zzdskbux__", (ftnlen)
			402)] = 3;
		axes[(i__1 = (h__ + (w << 1)) * 3 - 8) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("axes", i__1, "f_zzdskbux__", (ftnlen)
			403)] = 2;
		axes[(i__1 = (h__ + (w << 1)) * 3 - 7) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("axes", i__1, "f_zzdskbux__", (ftnlen)
			404)] = 3;
		angles[(i__1 = (h__ + (w << 1)) * 3 - 9) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("angles", i__1, "f_zzdskbux__", (ftnlen)
			406)] = -lon;
		angles[(i__1 = (h__ + (w << 1)) * 3 - 8) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("angles", i__1, "f_zzdskbux__", (ftnlen)
			407)] = lat - halfpi_();
		angles[(i__1 = (h__ + (w << 1)) * 3 - 7) < 24 && 0 <= i__1 ? 
			i__1 : s_rnge("angles", i__1, "f_zzdskbux__", (ftnlen)
			408)] = pi_();
		srfrec_(&bodyid, &lon, &lat, &sitpos[(i__1 = (h__ + (w + (i__ 
			<< 2) << 1)) * 3 - 33) < 48 && 0 <= i__1 ? i__1 : 
			s_rnge("sitpos", i__1, "f_zzdskbux__", (ftnlen)410)]);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}
	if (exists_(topfk, (ftnlen)255)) {
	    delfil_(topfk, (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	if (exists_(topspk, (ftnlen)255)) {
	    delfil_(topspk, (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	t_topker__(topfk, topspk, target, fixref, &nsites, &sitids[(i__1 = ((
		i__ << 2) + 1 << 1) - 10) < 16 && 0 <= i__1 ? i__1 : s_rnge(
		"sitids", i__1, "f_zzdskbux__", (ftnlen)428)], sitnms + (((
		i__2 = ((i__ << 2) + 1 << 1) - 10) < 16 && 0 <= i__2 ? i__2 : 
		s_rnge("sitnms", i__2, "f_zzdskbux__", (ftnlen)428)) << 5), &
		sitpos[(i__3 = (((i__ << 2) + 1 << 1) + 1) * 3 - 33) < 48 && 
		0 <= i__3 ? i__3 : s_rnge("sitpos", i__3, "f_zzdskbux__", (
		ftnlen)428)], sitfnm + (((i__4 = ((i__ << 2) + 1 << 1) - 10) <
		 16 && 0 <= i__4 ? i__4 : s_rnge("sitfnm", i__4, "f_zzdskbux"
		"__", (ftnlen)428)) << 5), &sitfid[(i__5 = ((i__ << 2) + 1 << 
		1) - 10) < 16 && 0 <= i__5 ? i__5 : s_rnge("sitfid", i__5, 
		"f_zzdskbux__", (ftnlen)428)], &first, &last, axes, angles, (
		ftnlen)255, (ftnlen)255, (ftnlen)32, (ftnlen)32, (ftnlen)32, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Load the topocentric kernels. */

    furnsh_("zzdskbux_mars_topo.tf", (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zzdskbux_mars_topo.bsp", (ftnlen)22);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zzdskbux_saturn_topo.tf", (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zzdskbux_saturn_topo.bsp", (ftnlen)24);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create DSKs.", (ftnlen)12);

/*     Start out by creating a three sets of tiled ellipsoid DSKs for */
/*     Mars and Saturn respectively. The DSKs will represent different */
/*     "layers": they'll be scaled versions of each other. */

/*     The first layer will be a tessellation using the default */
/*     target body radii. */


/*     Set the coordinate system. */

    corsys = 1;
    cleard_(&c__3, corpar);

/*     Use padding. */

    usepad = TRUE_;

/*     Don't bother with the vertex-plate mapping. */

    makvtl = FALSE_;

/*     NLON and NLAT indicate the number of longitude and latitude */
/*     bands of plates per tile, respectively. */

    nlon = 4;
    nlat = 8;

/*     Determine the spatial coverage of each tile. */

    tilew = pi_() * 2 / 6;
    tileh = pi_() / 3;
    for (targix = 1; targix <= 2; ++targix) {
	s_copy(target, targs + (((i__1 = targix - 1) < 2 && 0 <= i__1 ? i__1 :
		 s_rnge("targs", i__1, "f_zzdskbux__", (ftnlen)500)) << 5), (
		ftnlen)32, (ftnlen)32);
	bodyid = bids[(i__1 = targix - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
		"bids", i__1, "f_zzdskbux__", (ftnlen)501)];
	s_copy(fixref, fixrfs + (((i__1 = targix - 1) < 2 && 0 <= i__1 ? i__1 
		: s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)502)) << 5), 
		(ftnlen)32, (ftnlen)32);

/*        Get the radii used for the tessellation. */

	bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	a = radii[0];
	b = radii[1];
	c__ = radii[2];
	for (layrix = 1; layrix <= 3; ++layrix) {
	    for (w = 1; w <= 6; ++w) {
		for (h__ = 1; h__ <= 3; ++h__) {
		    s_copy(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)522)) * 
			    255, "zzdskbux_#_h#_w#_layer#.bds", (ftnlen)255, (
			    ftnlen)27);
		    repmc_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)525)) * 
			    255, "#", targs + (((i__2 = targix - 1) < 2 && 0 
			    <= i__2 ? i__2 : s_rnge("targs", i__2, "f_zzdskb"
			    "ux__", (ftnlen)525)) << 5), tildsk + ((i__3 = h__ 
			    + (w + (layrix + targix * 3) * 6) * 3 - 76) < 108 
			    && 0 <= i__3 ? i__3 : s_rnge("tildsk", i__3, 
			    "f_zzdskbux__", (ftnlen)525)) * 255, (ftnlen)255, 
			    (ftnlen)1, (ftnlen)32, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    lcase_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)530)) * 
			    255, tildsk + ((i__2 = h__ + (w + (layrix + 
			    targix * 3) * 6) * 3 - 76) < 108 && 0 <= i__2 ? 
			    i__2 : s_rnge("tildsk", i__2, "f_zzdskbux__", (
			    ftnlen)530)) * 255, (ftnlen)255, (ftnlen)255);
		    repmi_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)533)) * 
			    255, "#", &h__, tildsk + ((i__2 = h__ + (w + (
			    layrix + targix * 3) * 6) * 3 - 76) < 108 && 0 <= 
			    i__2 ? i__2 : s_rnge("tildsk", i__2, "f_zzdskbux"
			    "__", (ftnlen)533)) * 255, (ftnlen)255, (ftnlen)1, 
			    (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)537)) * 
			    255, "#", &w, tildsk + ((i__2 = h__ + (w + (
			    layrix + targix * 3) * 6) * 3 - 76) < 108 && 0 <= 
			    i__2 ? i__2 : s_rnge("tildsk", i__2, "f_zzdskbux"
			    "__", (ftnlen)537)) * 255, (ftnlen)255, (ftnlen)1, 
			    (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmi_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3) 
			    * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)541)) * 
			    255, "#", &layrix, tildsk + ((i__2 = h__ + (w + (
			    layrix + targix * 3) * 6) * 3 - 76) < 108 && 0 <= 
			    i__2 ? i__2 : s_rnge("tildsk", i__2, "f_zzdskbux"
			    "__", (ftnlen)541)) * 255, (ftnlen)255, (ftnlen)1, 
			    (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*                  CALL TOSTDO ( '0 '//TILDSK(H,W,LAYRIX,TARGIX) ) */
		    if (exists_(tildsk + ((i__1 = h__ + (w + (layrix + targix 
			    * 3) * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : 
			    s_rnge("tildsk", i__1, "f_zzdskbux__", (ftnlen)
			    549)) * 255, (ftnlen)255)) {
			delfil_(tildsk + ((i__1 = h__ + (w + (layrix + targix 
				* 3) * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 
				: s_rnge("tildsk", i__1, "f_zzdskbux__", (
				ftnlen)550)) * 255, (ftnlen)255);
		    }

/*                 Each tile gets its own surface ID. */

		    surfid = h__ + (w - 1) * 3 + (layrix - 1) * 18;

/*                 Set coverage for the current tile. */

		    lon = (w - 1) * tilew;
		    bounds[0] = lon;
		    bounds[1] = lon + tilew;
		    lat = halfpi_() - (h__ - 1) * tileh;
		    bounds[3] = lat;
		    bounds[2] = lat - tileh;
		    if (layrix == 1) {

/*                    Use one of the topocentric frames. */

/*                    Decide which frame to use. The frame will be that */
/*                    associated with the lat/lon rectangle centered at */
/*                    the closest topocentric frame center. */

			ctrlat = lat - tileh / 2;
			ctrlon = lon + tilew / 2;
/* Computing MIN */
			i__1 = (integer) ((halfpi_() - ctrlat) / dfrlat) + 1;
			fixh = min(i__1,2);
/* Computing MIN */
			i__1 = (integer) (ctrlon / dfrlon) + 1;
			fixw = min(i__1,4);
			s_copy(frame, sitfnm + (((i__1 = fixh + (fixw + (
				targix << 2) << 1) - 11) < 16 && 0 <= i__1 ? 
				i__1 : s_rnge("sitfnm", i__1, "f_zzdskbux__", 
				(ftnlen)589)) << 5), (ftnlen)32, (ftnlen)32);
		    } else {
			s_copy(frame, fixrfs + (((i__1 = targix - 1) < 2 && 0 
				<= i__1 ? i__1 : s_rnge("fixrfs", i__1, "f_z"
				"zdskbux__", (ftnlen)593)) << 5), (ftnlen)32, (
				ftnlen)32);
			if (layrix == 3) {

/*                       Use greatly contracted radii. */

			    scale = .1;
			    a = radii[0] * scale;
			    b = radii[1] * scale;
			    c__ = radii[2] * scale;
			}
		    }

/*                  TILDSK(H,W,LAYRIX,TARGIX) = 'bux0.bds' */


/*                 Note the time bounds. */

		    first = (layrix - 4) * 10 * jyear_();
		    last = layrix * 10 * jyear_();
		    if (layrix > 1) {
			t_secds2__(&bodyid, &surfid, frame, &first, &last, &
				corsys, corpar, bounds, &a, &b, &c__, &nlon, &
				nlat, &makvtl, &usepad, tildsk + ((i__1 = h__ 
				+ (w + (layrix + targix * 3) * 6) * 3 - 76) < 
				108 && 0 <= i__1 ? i__1 : s_rnge("tildsk", 
				i__1, "f_zzdskbux__", (ftnlen)621)) * 255, (
				ftnlen)32, (ftnlen)255);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    } else {

/*                    Create the same section we would have done for the */
/*                    body-centered frame, but use a topocentric frame. */
/*                    We can't call T_SECDS2 to do this for us; we'll */
/*                    need to get our hands dirty. */

/*                    We can, however, create the plate set easily */
/*                    enough. */

			minlon = bounds[0];
			maxlon = bounds[1];
			minlat = bounds[2];
			maxlat = bounds[3];

/*                    Add longitude and latitude padding. */

			dplon = (bounds[1] - bounds[0]) / nlon;
			dplat = (bounds[3] - bounds[2]) / nlat;
			nplon = nlon;
			nplat = nlat;
			if (minlat > 0.) {
			    minlat -= dplat;
			    ++nplat;
			}
			if (maxlat < 0.) {
			    maxlat += dplat;
			    ++nplat;
			}
			minlon -= dplon;
			maxlon += dplon;
			nplon += 2;
			zzellsec_(&a, &b, &c__, &minlon, &maxlon, &minlat, &
				maxlat, &nplon, &nplat, &c__10000, &c__20000, 
				&nv, verts, &np, plates);

/*                    We're not done with this DSK. We're going to shift */
/*                    the vertices to make them relative to the */
/*                    topocentric frame center, and we'll transform the */
/*                    vertices to the reference frame FRAME. */

/*                    Get the frame center offset. */

			spkezp_(&sitids[(i__1 = fixh + (fixw + (targix << 2) 
				<< 1) - 11) < 16 && 0 <= i__1 ? i__1 : s_rnge(
				"sitids", i__1, "f_zzdskbux__", (ftnlen)679)],
				 &c_b168, fixref, "NONE", &bodyid, offset, &
				lt, (ftnlen)32, (ftnlen)4);
			chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                    Get the transformation from the body-centered, */
/*                    body-fixed frame to the topocentric one. */

			pxform_(fixref, sitfnm + (((i__1 = fixh + (fixw + (
				targix << 2) << 1) - 11) < 16 && 0 <= i__1 ? 
				i__1 : s_rnge("sitfnm", i__1, "f_zzdskbux__", 
				(ftnlen)690)) << 5), &c_b168, xform, (ftnlen)
				32, (ftnlen)32);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			i__1 = nv;
			for (vix = 1; vix <= i__1; ++vix) {
			    vsub_(&verts[(i__2 = vix * 3 - 3) < 30000 && 0 <= 
				    i__2 ? i__2 : s_rnge("verts", i__2, "f_z"
				    "zdskbux__", (ftnlen)696)], offset, vtemp);
			    mxv_(xform, vtemp, &verts[(i__2 = vix * 3 - 3) < 
				    30000 && 0 <= i__2 ? i__2 : s_rnge("verts"
				    , i__2, "f_zzdskbux__", (ftnlen)698)]);
			}

/*                    Update the segment bounds to reflect that we're */
/*                    using a topocentric frame. The longitude extent is */
/*                    360 degrees; the latitude extent is 180 degrees. */

			bounds[0] = 0.;
			bounds[1] = pi_() * 2;
			bounds[2] = -halfpi_();
			bounds[3] = halfpi_();

/*                    Create a DSK from our plate set. */

			t_wrtplt__(&bodyid, &surfid, frame, &first, &last, &
				corsys, corpar, bounds, &nv, &np, verts, 
				plates, &makvtl, tildsk + ((i__1 = h__ + (w + 
				(layrix + targix * 3) * 6) * 3 - 76) < 108 && 
				0 <= i__1 ? i__1 : s_rnge("tildsk", i__1, 
				"f_zzdskbux__", (ftnlen)715)) * 255, (ftnlen)
				32, (ftnlen)255);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }
		}
	    }
	}
    }
/* *********************************************************************** */

/*     ZZDSKBUX, ZZSBFXR, ZZSBFXRI Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars intercept case. Use layers 1 and 2.", (ftnlen)47);

/*     Load all DSKs. */

    for (targix = 1; targix <= 2; ++targix) {
	for (layrix = 1; layrix <= 3; ++layrix) {
	    for (w = 1; w <= 6; ++w) {
		for (h__ = 1; h__ <= 3; ++h__) {
		    furnsh_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3)
			     * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : 
			    s_rnge("tildsk", i__1, "f_zzdskbux__", (ftnlen)
			    757)) * 255, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
	    }
	}
    }

/*     Specify the surfaces from the first Mars layer. This layer */
/*     uses topocentric frames. */

    nsurf = 18;
    i__1 = nsurf;
    for (i__ = 1; i__ <= i__1; ++i__) {
	laysrf[(i__2 = i__ - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf", 
		i__2, "f_zzdskbux__", (ftnlen)777)] = i__;
	laysrf[(i__2 = i__ + 99) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf", 
		i__2, "f_zzdskbux__", (ftnlen)778)] = i__ + nsurf;
	laysrf[(i__2 = i__ + 199) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf",
		 i__2, "f_zzdskbux__", (ftnlen)779)] = i__ + (nsurf << 1);
    }

/*     Create ray. R is a scale factor used to */
/*     control the magnitude of the vertex. */

    r__ = 1e6;

/*     Pick a vertex that doesn't lie on a meridian */
/*     containing any plate vertices. */

    d__1 = r__ * 1.1;
    d__2 = -r__;
    d__3 = r__ * 2;
    vpack_(&d__1, &d__2, &d__3, vertex);
    vminus_(vertex, raydir);
    bodyid = 499;
    et = 0.;

/*     Compute our intercept in the IAU_MARS frame. */

    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)806)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We'll call ZZSBFXR rather than ZZDSKBUX since the former */
/*     calls the latter, and the former has all of the flexibility */
/*     we need, but has a less complex calling sequence. */

    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
    if (found) {

/*        Get the intercept on the second layer. This layer uses */
/*        the body-centered frame. */

	zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, raydir, 
		xxpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	tol = 1e-12;

/*        Compare to the intercept on layer 1 found by ZZDSKXR. */

	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)
		3);

/*        Now use ZZSBFXRI to do the computation. Use the second layer. */

	zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, raydir,
		 xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("ZZSBFXRI FOUND (layer 2)", &found, &c_true, ok, (ftnlen)24);
	chckad_("ZZSBFXRI XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)12,
		 (ftnlen)3);

/*        Use ZZSBFXRI to find the intercept on the first layer. */

	zzsbfxri_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, xpt, 
		&handle, dladsc, dskdsc, dc, ic, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("ZZSBFXRI FOUND (layer 2)", &found, &c_true, ok, (ftnlen)24);
	chckad_("ZZSBFXRI XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)12,
		 (ftnlen)3);

/*        Check the handle, DSK and DLA descriptors, and plate ID */
/*        from ZZSBFXRI. */

	dskgd_(&handle, dladsc, xdskds);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("DSKDSC", dskdsc, "=", xdskds, &c__24, &c_b168, ok, (ftnlen)6,
		 (ftnlen)1);

/*        Confirm that we have the correct DLA descriptor by comparing */
/*        the ZZDSKXRI intercept to the one we find using DSKX02. */

/*        In order to use DSKX02, we need to have a vertex and ray */
/*        direction expressed in the segment frame. The vertex must */
/*        be an offset relative to the frame center. */

	segfid = i_dnnt(&dskdsc[4]);
	if (segfid == fixfid) {
	    vequ_(vertex, segvtx);
	    vequ_(raydir, segdir);
	} else {

/*           Get the frame center offset in the body-centered frame. */

	    frinfo_(&segfid, &frmctr, &class__, &clssid, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    spkezp_(&frmctr, &et, fixref, "NONE", &bodyid, offset, &lt, (
		    ftnlen)32, (ftnlen)4);

/*           Translate the ray's vertex to make it relative to the */
/*           segment frame's center. */

	    vsub_(vertex, offset, vtemp);

/*           Transform the offset vertex and the direction vector to */
/*           the segment frame. */

	    refchg_(&fixfid, &segfid, &et, xform);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    mxv_(xform, raydir, segdir);
	    mxv_(xform, vtemp, segvtx);
	}
	dskx02_(&handle, dladsc, segvtx, segdir, &plid, xxpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (segfid != fixfid) {

/*           We must map the intercept back to the body-centered */
/*           frame. */

	    mtxv_(xform, xxpt, vtemp);
	    vadd_(vtemp, offset, xxpt);
	}
	chcksl_("DSKX02 FOUND", &found, &c_true, ok, (ftnlen)12);
	tol = 1e-12;
	chckad_("ZZDSKXRI XPT (2)", xpt, "~~/", xxpt, &c__3, &tol, ok, (
		ftnlen)16, (ftnlen)3);

/*        Check the plate ID from the integer parameter array returned */
/*        by ZZDSKXRI. */

	chcksi_("IC(1)", ic, "=", &plid, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Saturn intercept case. Use layers 1 and 2.", (ftnlen)49);

/*     Create ray. */

    r__ = 1e6;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);

/*     Specify the surfaces from the first Saturn layer. This layer */
/*     uses topocentric frames. */

    nsurf = 18;
    i__1 = nsurf;
    for (i__ = 1; i__ <= i__1; ++i__) {
	laysrf[(i__2 = i__ - 1) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf", 
		i__2, "f_zzdskbux__", (ftnlen)970)] = i__;
	laysrf[(i__2 = i__ + 99) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf", 
		i__2, "f_zzdskbux__", (ftnlen)971)] = i__ + nsurf;
	laysrf[(i__2 = i__ + 199) < 300 && 0 <= i__2 ? i__2 : s_rnge("laysrf",
		 i__2, "f_zzdskbux__", (ftnlen)972)] = i__ + (nsurf << 1);
    }
    bodyid = 699;
    et = 0.;

/*     Compute our intercept in the IAU_SATURN frame. */

    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)985)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We'll call ZZSBFXR rather than ZZDSKBUX since the former */
/*     calls the latter, and the former has all of the flexibility */
/*     we need, but has a less complex calling sequence. */

    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
    if (found) {

/*        Get the intercept on the second layer. This layer uses */
/*        the body-centered frame. */

	zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, raydir, 
		xxpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	tol = 1e-12;
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)
		3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars intercept case. Ray's vertex is inside layers 1 and "
	    "2 but outside layer 3.", (ftnlen)86);
    bodyid = 499;

/*     Compute the expected intercept. */

    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, raydir, 
	    xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*     Scale down the point on layer 2 to the scale of layer 3. */

    vsclip_(&scale, xxpt);

/*     Create the scaled vertex. */

    r__ = 1e3;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (small vertex)", &found, &c_true, ok, (ftnlen)20);
    tol = 1e-12;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     Restore the default vertex. */

    r__ = 1e6;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars non-intercept case. Ray points away from the target."
	    " Use all layers.", (ftnlen)80);
    bodyid = 499;

/*     Point the ray away from the target. */

    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, vertex, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (all)", &found, &c_false, ok, (ftnlen)11);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars non-intercept case. Ray emanates from the target cen"
	    "ter. Use all layers.", (ftnlen)84);
    bodyid = 499;
    cleard_(&c__3, vertex);

/*     Point the ray away from the target. */

    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (all)", &found, &c_false, ok, (ftnlen)11);

/*     Restore the default vertex and ray direction. */

    r__ = 1e6;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars non-intercept case. Time is too early for all segmen"
	    "ts. Use all layers.", (ftnlen)83);
    bodyid = 499;

/*     The first segment starts at 30 Julian years before J2000. The */
/*     other segments start later. */

    et = jyear_() * -35;
    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (all)", &found, &c_false, ok, (ftnlen)11);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Mars non-intercept case. Time is too late for all segment"
	    "s. Use all layers.", (ftnlen)82);
    bodyid = 499;

/*     The last segment ends at 30 Julian years after J2000. The */
/*     other segments end later. */

    et = jyear_() * 35;
    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (all)", &found, &c_false, ok, (ftnlen)11);

/*     Restore ET. */

    et = 0.;

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Saturn non-intercept case. The selected surfaces don't co"
	    "ver the potential intercept coordinates.", (ftnlen)104);
    bodyid = 699;
    d__1 = -r__;
    vpack_(&c_b168, &c_b168, &d__1, vertex);
    vminus_(vertex, raydir);
    zzsbfxr_(&bodyid, &c__1, &c__1, &et, &fixfid, vertex, raydir, xpt, &found)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND (surface ID 1)", &found, &c_false, ok, (ftnlen)20);

/*     Restore the default vertex and ray direction. */

    r__ = 1e6;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars spear case. Compare results using layers 1 and 2.", (ftnlen)
	    54);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1205)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 80;
    nlat = 40;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn spear case. Compare results using layers 1 and 2.", (
	    ftnlen)56);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1261)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 80;
    nlat = 40;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars spear case. Compare results using layers 1 and 2. This time"
	    ", load DSK kernels for the different layers in reverse order.", (
	    ftnlen)125);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1319)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (layrix = 3; layrix >= 1; --layrix) {
	for (w = 1; w <= 6; ++w) {
	    for (h__ = 1; h__ <= 3; ++h__) {
		furnsh_(tildsk + ((i__1 = h__ + (w + (layrix + bix * 3) * 6) *
			 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge("tildsk", 
			i__1, "f_zzdskbux__", (ftnlen)1330)) * 255, (ftnlen)
			255);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}
    }
    nlon = 80;
    nlat = 40;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn spear case. Compare results using layers 1 and 2. This ti"
	    "me, load DSK kernels for the different layers in reverse order.", 
	    (ftnlen)127);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1393)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (layrix = 3; layrix >= 1; --layrix) {
	for (w = 1; w <= 6; ++w) {
	    for (h__ = 1; h__ <= 3; ++h__) {
		furnsh_(tildsk + ((i__1 = h__ + (w + (layrix + bix * 3) * 6) *
			 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge("tildsk", 
			i__1, "f_zzdskbux__", (ftnlen)1403)) * 255, (ftnlen)
			255);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}
    }
    nlon = 80;
    nlat = 40;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars spear case. Compare results using layer 1 against results o"
	    "btained using all layers.", (ftnlen)89);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1465)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nsurf = 18;

/*     Reduce the number of intercepts for this one. */

    nlon = 40;
    nlat = 20;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, 
		    xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (all layers)", &found, &c_true, ok, (ftnlen)18);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn spear case. Compare results using layer 1 against results"
	    " obtained using all layers.", (ftnlen)91);
    bodyid = 699;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1528)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nsurf = 18;

/*     Reduce the number of intercepts for this one. */

    nlon = 40;
    nlat = 20;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, 
		    xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (all layers)", &found, &c_true, ok, (ftnlen)18);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars greedy spear case. Deliberately hit segment boundaries on m"
	    "eridians and parallels.", (ftnlen)87);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1591)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn greedy spear case. Deliberately hit segment boundaries on"
	    " meridians and parallels.", (ftnlen)89);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1648)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1) * dlon;
	i__2 = nlat;
	for (j = 0; j <= i__2; ++j) {
	    lat = halfpi_() - j * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);
	    zzsbfxr_(&bodyid, &nsurf, laysrf, &et, &fixfid, vertex, raydir, 
		    xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 1)", &found, &c_true, ok, (ftnlen)15);
	    zzsbfxr_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xxpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);
	    tol = 1e-12;
	    s_copy(label, "XPT I=@ J=@", (ftnlen)40, (ftnlen)11);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)40, (
		    ftnlen)3);
	}
    }
/* *********************************************************************** */

/*     ZZDSKBUX Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Saturn non-intercept case. No DSK data are available for "
	    "the specified surfaces. Use all layers for Mars and Saturn. This"
	    " is an error case.", (ftnlen)146);
    bodyid = 699;
    zzsbfxr_(&bodyid, &c__1, &c_n1, &et, &fixfid, vertex, raydir, xpt, &found)
	    ;
    chckxc_(&c_true, "SPICE(DSKDATANOTFOUND)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple Venus non-intercept case. No DSK data are available for V"
	    "enus. Use all layers for Mars and Saturn. This is an error case.",
	     (ftnlen)128);
    bodyid = 299;
    zzsbfxr_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    found);
    chckxc_(&c_true, "SPICE(NODSKSEGMENTS)", ok, (ftnlen)20);
/* *********************************************************************** */



/*     ZZDSKBUN, ZZSBFNRM tests */



/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZDSKBUN, ZZSBFNRM Normal cases */

/* *********************************************************************** */

/*     We'll call ZZSBFNRM rather than ZZDSKBUN since the former */
/*     calls the latter, and the former has all of the flexibility */
/*     we need, but has a less complex calling sequence. */

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars case: find outward normals on every plate of layer 1. Compa"
	    "re to normals on layer 2. Choose points so as to avoid plate edg"
	    "es.", (ftnlen)131);

/*     Re-load DSKS in their original order. */

    for (targix = 1; targix <= 2; ++targix) {
	for (layrix = 1; layrix <= 3; ++layrix) {
	    for (w = 1; w <= 6; ++w) {
		for (h__ = 1; h__ <= 3; ++h__) {
		    furnsh_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3)
			     * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : 
			    s_rnge("tildsk", i__1, "f_zzdskbux__", (ftnlen)
			    1774)) * 255, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
	    }
	}
    }
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1789)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. */

	    zzsbfnrm_(&bodyid, &nsurf, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn case: find outward normals on every plate of layer 1. Com"
	    "pare to normals on layer 2. Choose points so as to avoid plate e"
	    "dges.", (ftnlen)133);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1879)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. */

	    zzsbfnrm_(&bodyid, &nsurf, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars case: find outward normals on every plate, using all layers"
	    ". Compare to normals on layer 2. Choose points so as to avoid pl"
	    "ate edges.", (ftnlen)138);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)1971)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using all layers. */

	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn case: find outward normals on every plate, using all laye"
	    "rs. Compare to normals on layer 2. Choose points so as to avoid "
	    "plate edges.", (ftnlen)140);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2061)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using all layers. */

	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars case: find outward normals on every plate, using all layers"
	    ". Compare to normals on layer 2. Choose points so as to avoid pl"
	    "ate edges. Load DSKs so as to reverse the order of the layers.", (
	    ftnlen)190);

/*     Re-load DSKs, reversing the order of the layers. */

    for (targix = 1; targix <= 2; ++targix) {
	for (layrix = 3; layrix >= 1; --layrix) {
	    for (w = 1; w <= 6; ++w) {
		for (h__ = 1; h__ <= 3; ++h__) {
		    furnsh_(tildsk + ((i__1 = h__ + (w + (layrix + targix * 3)
			     * 6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : 
			    s_rnge("tildsk", i__1, "f_zzdskbux__", (ftnlen)
			    2160)) * 255, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
	    }
	}
    }
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2175)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Reload the last DSK loaded. This looks strange, but we */
/*           need to do this here to exercise the BSR update logic */
/*           in ZZDSKNRM. Otherwise, the buffer updates will be */
/*           handled in ZZDSKXRI before ZZDSKSNRM gets a chance to */
/*           respond. */

	    furnsh_(tildsk + 18105, (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT, using all layers. */

	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn case: find outward normals on every plate, using all laye"
	    "rs. Compare to normals on layer 2. Choose points so as to avoid "
	    "plate edges. DSKs have been reloaded so as to reverse the order "
	    "of the layers.", (ftnlen)206);

/*     The reload was performed in the previous case. */

    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2283)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Reload the last DSK loaded. This looks strange, but we */
/*           need to do this here to exercise the BSR update logic */
/*           in ZZDSKNRM. Otherwise, the buffer updates will be */
/*           handled in ZZDSKXRI before ZZDSKSNRM gets a chance to */
/*           respond. */

	    furnsh_(tildsk + 18105, (ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT, using all layers. */

	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at XPT using the second layer. */

	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, xpt, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 1 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars case: find outward normals on every plate of layer 1. Compa"
	    "re to normals on layer 3. Choose points so as to avoid plate edg"
	    "es.", (ftnlen)131);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2385)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. */

	    zzsbfnrm_(&bodyid, &nsurf, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at the lon/lat of XPT */
/*           using the third layer. */

/*           We need to use a scaled version of XPT for this layer. */

	    vscl_(&scale, xpt, vtemp);
	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[200], &et, &fixfid, vtemp, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 3 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn case: find outward normals on every plate of layer 1. Com"
	    "pare to normals on layer 3. Choose points so as to avoid plate e"
	    "dges.", (ftnlen)133);
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2482)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. */

	    zzsbfnrm_(&bodyid, &nsurf, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at the lon/lat of XPT */
/*           using the third layer. */

/*           We need to use a scaled version of XPT for this layer. */

	    vscl_(&scale, xpt, vtemp);
	    zzsbfnrm_(&bodyid, &nsurf, &laysrf[200], &et, &fixfid, vtemp, 
		    xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 3 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars case: find outward normals on every plate of layer 1. Compa"
	    "re to normals on layer 3. Choose points so as to avoid plate edg"
	    "es. Select layers by time rather than surface ID.", (ftnlen)177);
    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2580)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

/*           Set ET for this layer. */

	    et = 0.;
	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. Set */
/*           ET to a value covered only by the first layer. */

	    et = jyear_() * -25;
	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at the lon/lat of XPT */
/*           using the third layer. */

/*           We need to use a scaled version of XPT for this layer. */

	    vscl_(&scale, xpt, vtemp);
	    et = jyear_() * 25;
	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, vtemp, xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 3 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Saturn case: find outward normals on every plate of layer 1. Com"
	    "pare to normals on layer 3. Choose points so as to avoid plate e"
	    "dges. Select layers by time rather than surface ID.", (ftnlen)179)
	    ;
    bodyid = 699;
    bix = 2;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2689)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 12;
    nlat = 6;
    dlon = pi_() * 2 / nlon;
    dlat = pi_() / nlat;
    r__ = 1e6;
    et = 0.;
    i__1 = nlon;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lon = (i__ - 1 + .33333333333333331) * dlon;
	i__2 = nlat;
	for (j = 1; j <= i__2; ++j) {
	    lat = halfpi_() - (j - .5) * dlat;
	    latrec_(&r__, &lon, &lat, vertex);
	    vminus_(vertex, raydir);

/*           Generate a surface point on the second layer. This */
/*           point should be very close to the first layer as well. */

/*           Set ET for this layer. */

	    et = 0.;
	    zzsbfxri_(&bodyid, &nsurf, &laysrf[100], &et, &fixfid, vertex, 
		    raydir, xpt, &handle, dladsc, dskdsc, dc, ic, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND (layer 2)", &found, &c_true, ok, (ftnlen)15);

/*           Find the outward normal at XPT, using the first layer. Set */
/*           ET to a value covered only by the first layer. */

	    et = jyear_() * -25;
	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Find the outward normal at the lon/lat of XPT */
/*           using the third layer. */

/*           We need to use a scaled version of XPT for this layer. */

	    vscl_(&scale, xpt, vtemp);
	    et = jyear_() * 25;
	    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, vtemp, xnorml);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "layer 3 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)22);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);

/*           Fetch the plate normal directly from the segment found */
/*           by ZZDSKXRI. */

	    dskn02_(&handle, dladsc, ic, normal);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-12;
	    s_copy(label, "DSKN02 NORMAL I=@ J=@", (ftnlen)40, (ftnlen)21);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "@", &j, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, normal, "~~/", xnorml, &c__3, &tol, ok, (ftnlen)40,
		     (ftnlen)3);
	}
    }
/* *********************************************************************** */

/*     ZZDSKBUN Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars error case: non-existent surface. Use all layers for Mars a"
	    "nd Saturn.", (ftnlen)74);
    et = 0.;

/*     Generate a legitimate surface point. */

    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2805)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = 1e6;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);
    zzsbfxri_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    handle, dladsc, dskdsc, dc, ic, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzsbfnrm_(&bodyid, &c__1, &c_n1, &et, &fixfid, xpt, xnorml);
    chckxc_(&c_true, "SPICE(POINTNOTINSEGMENT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars error case: point outside all segments.", (ftnlen)44);
    r__ = 1e6;
    vpack_(&r__, &r__, &r__, vtemp);
    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, vtemp, xnorml);
    chckxc_(&c_true, "SPICE(POINTNOTINSEGMENT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars error case: point inside segment, but too far from any plat"
	    "e.", (ftnlen)66);

/*     Start by generating a legitimate point. */

    bodyid = 499;
    bix = 1;
    s_copy(fixref, fixrfs + (((i__1 = bix - 1) < 2 && 0 <= i__1 ? i__1 : 
	    s_rnge("fixrfs", i__1, "f_zzdskbux__", (ftnlen)2850)) << 5), (
	    ftnlen)32, (ftnlen)32);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = 1e6;
    et = 0.;
    d__1 = -r__;
    d__2 = r__ * 2;
    vpack_(&r__, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);
    zzsbfxri_(&bodyid, &c__0, laysrf, &et, &fixfid, vertex, raydir, xpt, &
	    handle, dladsc, dskdsc, dc, ic, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Get the normal at this point. */

    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, xpt, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Add to the point a small multiple of the normal. */

    vlcom_(&c_b835, xpt, &c_b836, normal, vtemp);

/*     Now try to find the normal at the offset point. */

    zzsbfnrm_(&bodyid, &c__0, laysrf, &et, &fixfid, vtemp, normal);
    chckxc_(&c_true, "SPICE(POINTOFFSURFACE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     Clean up. */

/* *********************************************************************** */

/* --------------------------------------------------------- */

    tcase_("Clean up", (ftnlen)8);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdskbux_test0.tpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdskbux_mars_topo.tf", (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdskbux_saturn_topo.tf", (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdskbux_mars_topo.bsp", (ftnlen)22);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdskbux_saturn_topo.bsp", (ftnlen)24);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (bix = 1; bix <= 2; ++bix) {
	for (layrix = 1; layrix <= 3; ++layrix) {
	    for (w = 1; w <= 6; ++w) {
		for (h__ = 1; h__ <= 3; ++h__) {
		    delfil_(tildsk + ((i__1 = h__ + (w + (layrix + bix * 3) * 
			    6) * 3 - 76) < 108 && 0 <= i__1 ? i__1 : s_rnge(
			    "tildsk", i__1, "f_zzdskbux__", (ftnlen)2932)) * 
			    255, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
	    }
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzdskbux__ */

