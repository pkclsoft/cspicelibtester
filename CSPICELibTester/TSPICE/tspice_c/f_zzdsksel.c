/* f_zzdsksel.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__8 = 8;
static logical c_false = FALSE_;
static integer c__24 = 24;
static integer c__100 = 100;
static integer c__1 = 1;
static integer c_n3 = -3;
static integer c__101 = 101;

/* $Procedure      F_ZZDSKSEL ( Test the entry points of ZZDSKSEL ) */
/* Subroutine */ int f_zzdsksel__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    integer i_dnnt(doublereal *);

    /* Local variables */
    extern logical zzdskbdc_(integer *, integer *, doublereal *), zzdsksbd_(
	    integer *), zzdskcit_(integer *, integer *, doublereal *), 
	    zzdsksel_(integer *, integer *, integer *, integer *, integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, integer *, 
	    doublereal *, doublereal *, integer *, integer *, doublereal *), 
	    zzdskumc_(integer *, integer *, doublereal *), zzdskusc_(integer *
	    , doublereal *, doublereal *, doublereal *), zzdsksit_(integer *, 
	    integer *, integer *, doublereal *);
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal btime, etime;
    extern doublereal jyear_(void);
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer nsurf;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal et;
    static integer dladsc[8], handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), cleari_(
	    integer *, integer *);
    static integer framid;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static integer dclass, bodyid;
    static doublereal dskdsc[24], corpar[10];
    static integer surfid;
    static logical ismtch, lgretv;
    static integer srflst[100], corsys;
    extern doublereal rpd_(void);
    static doublereal pos[3], cor1, cor2;

/* $ Abstract */

/*     Perform a collection of rudimentary tests on the */
/*     ZZDSKSEL collection of entry points. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     The umbrella routine ZZDSKSEL contains the following */
/*     entry points: */


/*     listed below. */

/*         Match body ID only */
/*         ------------------ */

/*           Initialization:       ZZDSKSBD */
/*           Segment comparison:   ZZDSKBDC */


/*     The remaining entry points are included to support the SMAP */
/*     Alpha DSK Toolkit. They may be useful for DSK type 4 in a */
/*     future SPICELIB version. */


/*         Match body ID, surface list, and time */
/*         ------------------------------------- */

/*           Initialization:       ZZDSKSIT */
/*           Segment comparison:   ZZDSKCIT */


/*         Match body ID, time, and coordinates */
/*         ------------------------------------ */

/*           Initialization:       ZZDSKUSC */
/*           Segment comparison:   ZZDSKUMC */


/*         Match body ID, surface ID, frame ID, coordinate system, */
/*         coordinate parameters, time, and coordinates */
/*         ------------------------------------------------------ */

/*           Initialization:       ZZDSKMSC */
/*           Segment comparison:   ZZDSKMMC */


/*         Match body ID, surface ID, frame ID, data class, time, */
/*         and whether coverage includes a specified position vector */
/*         --------------------------------------------------------- */

/*           Initialization:       ZZDSKSRC */
/*           Segment comparison:   ZZDSKMRC */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0 20-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZDSKSEL", (ftnlen)10);
/* *********************************************************************** */

/*     ZZDSKSEL umbrella tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Direct call to ZZDSKSEL", (ftnlen)23);
    lgretv = zzdsksel_(&surfid, &nsurf, srflst, &bodyid, &dclass, &corsys, 
	    corpar, &cor1, &cor2, &framid, pos, &et, &handle, dladsc, dskdsc);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
/* *********************************************************************** */

/*     ZZDSKSBD/ZZDSKBDC tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZDSKSBD/ZZDSKBDC normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: match case", (ftnlen)29);

/*     Initialize handle and DLA descriptor. Neither are */
/*     used for this comparison. */

    handle = 0;
    cleari_(&c__8, dladsc);
    bodyid = 599;
    lgretv = zzdsksbd_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The DSK descriptor must have a matching body ID code. */

    cleard_(&c__24, dskdsc);

/*     The body ID index in a DSK descriptor is called 'CTRIDX'. */

    dskdsc[1] = (doublereal) bodyid;
    ismtch = zzdskbdc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: non-match case", (ftnlen)33);
    dskdsc[1] = (doublereal) (bodyid + 1);
    ismtch = zzdskbdc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/*     No error cases exist for these routines. */

/* *********************************************************************** */

/*     ZZDSKSIT/ZZDSKCIT tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZDSKSIT/ZZDSKCIT normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: match case, empty surface list.", (ftnlen)50);

/*     Initialize handle and DLA descriptor. Neither are */
/*     used for this comparison. */

    handle = 0;
    cleari_(&c__8, dladsc);
    bodyid = 599;
    nsurf = 0;
    cleari_(&c__100, srflst);
    et = 10.;
    lgretv = zzdsksit_(&bodyid, &nsurf, srflst, &et);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The DSK descriptor must have a matching body ID code. */

    cleard_(&c__24, dskdsc);
    dskdsc[1] = (doublereal) bodyid;

/*     Set time bounds. The bounds must bracket ET. */

    btime = jyear_() * -50;
    etime = jyear_() * 50;
    dskdsc[22] = btime;
    dskdsc[23] = etime;

/*     The body ID index in a DSK descriptor is called 'CTRIDX'. */

    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: match case, non-empty surface list.", (ftnlen)
	    54);

/*     Initialize handle and DLA descriptor. Neither are */
/*     used for this comparison. */

    handle = 0;
    cleari_(&c__8, dladsc);
    bodyid = 599;
    cleari_(&c__100, srflst);
    nsurf = 2;
    srflst[0] = -1;
    srflst[1] = -2;
    et = 10.;
    lgretv = zzdsksit_(&bodyid, &nsurf, srflst, &et);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The DSK descriptor must have a matching body ID code. */

    cleard_(&c__24, dskdsc);

/*     Set the surface ID code. */

    dskdsc[0] = (doublereal) srflst[0];

/*     Set time bounds. The bounds must bracket ET. */

    dskdsc[1] = (doublereal) bodyid;
    btime = jyear_() * -50;
    etime = jyear_() * 50;
    dskdsc[22] = btime;
    dskdsc[23] = etime;

/*     Look for a match. */

    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH (-1)", &ismtch, &c_true, ok, (ftnlen)11);

/*     Try again with the other surface. */

    dskdsc[0] = (doublereal) srflst[1];
    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH (-2)", &ismtch, &c_true, ok, (ftnlen)11);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: non-match case, body mismatch.", (ftnlen)49);

/*     Save body ID. */

    bodyid = i_dnnt(&dskdsc[1]);
    dskdsc[1] = 0.;
    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/*     Restore body ID. */

    dskdsc[1] = (doublereal) bodyid;

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: non-match case, time too early.", (ftnlen)50);
    d__1 = btime - 1.;
    lgretv = zzdsksit_(&bodyid, &nsurf, srflst, &d__1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: non-match case, time too late.", (ftnlen)49);
    d__1 = etime + 1.;
    lgretv = zzdsksit_(&bodyid, &nsurf, srflst, &d__1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: non-match case, surface not in list.", (ftnlen)
	    55);
    lgretv = zzdsksit_(&bodyid, &c__1, &c_n3, &et);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskcit_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);
/* *********************************************************************** */

/*     ZZDSKSIT/ZZDSKCIT error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKSBD/ZZDSKBDC: surface list too big.", (ftnlen)40);
    lgretv = zzdsksit_(&bodyid, &c__101, srflst, &et);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     ZZDSKUSC/ZZDSKUMC tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZDSKUSC/ZZDSKUMC normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: match case.", (ftnlen)30);

/*     Initialize handle and DLA descriptor. Neither are */
/*     used for this comparison. */

    handle = 0;
    cleari_(&c__8, dladsc);
    bodyid = 599;
    et = 10.;
    cor1 = rpd_() * 30.;
    cor2 = rpd_() * -5.;
    lgretv = zzdskusc_(&bodyid, &et, &cor1, &cor2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The DSK descriptor must have a matching body ID code. */

    cleard_(&c__24, dskdsc);
    dskdsc[1] = (doublereal) bodyid;

/*     Set time bounds. The bounds must bracket ET. */

    btime = jyear_() * -50;
    etime = jyear_() * 50;
    dskdsc[22] = btime;
    dskdsc[23] = etime;

/*     Set coordinate system and coordinate bounds. */

    dskdsc[5] = 1.;
    dskdsc[16] = rpd_() * -90.;
    dskdsc[17] = rpd_() * 90.;
    dskdsc[18] = rpd_() * -45.;
    dskdsc[19] = rpd_() * 45.;
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, body mismatch.", (ftnlen)49);

/*     Save body ID. */

    bodyid = i_dnnt(&dskdsc[1]);
    dskdsc[1] = 0.;
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/*     Restore body ID. */

    dskdsc[1] = (doublereal) bodyid;

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, time too early.", (ftnlen)50);
    d__1 = btime - 1;
    lgretv = zzdskusc_(&bodyid, &d__1, &cor1, &cor2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, time too late.", (ftnlen)49);
    d__1 = etime + 1;
    lgretv = zzdskusc_(&bodyid, &d__1, &cor1, &cor2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, coordinate 1 too small", (
	    ftnlen)57);
    d__1 = dskdsc[16] - .1;
    lgretv = zzdskusc_(&bodyid, &et, &d__1, &cor2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, coordinate 1 too large", (
	    ftnlen)57);
    d__1 = dskdsc[17] + .1;
    lgretv = zzdskusc_(&bodyid, &et, &d__1, &cor2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, coordinate 2 too small", (
	    ftnlen)57);
    d__1 = dskdsc[18] - .1;
    lgretv = zzdskusc_(&bodyid, &et, &cor1, &d__1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZDSKUSC/ZZDSKUMC: non-match case, coordinate 2 too large", (
	    ftnlen)57);
    d__1 = dskdsc[19] + .1;
    lgretv = zzdskusc_(&bodyid, &et, &cor1, &d__1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ismtch = zzdskumc_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ISMTCH", &ismtch, &c_false, ok, (ftnlen)6);

/*     No error cases exist for these routines. */

/* *********************************************************************** */

/*     ZZDSKMSC/ZZDSKMMC tests */

/* *********************************************************************** */
/* *********************************************************************** */

/*     ZZDSKMSC/ZZDSKMMC normal cases */

/* *********************************************************************** */

/*     TBD */

    t_success__(ok);
    return 0;
} /* f_zzdsksel__ */

