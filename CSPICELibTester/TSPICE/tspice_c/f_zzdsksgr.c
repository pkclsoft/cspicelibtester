/* f_zzdsksgr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__24 = 24;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__6 = 6;

/* $Procedure      F_ZZDSKSGR ( Test ZZDSKSGR ) */
/* Subroutine */ int f_zzdsksgr__(logical *ok)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern doublereal zzdsksgr_(doublereal *);
    doublereal f;
    integer i__, j, k;
    doublereal r__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *), topen_(char *, ftnlen);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *);
    doublereal re;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    doublereal rp;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    doublereal xr, dskdsc[24], corner[3], bds[6]	/* was [2][3] */, tol;

/* $ Abstract */

/*     Test the SPICELIB bounding ellipsoid routine ZZDSKSGR. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the routine ZZDSKSGR. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 13-FEB-2017 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZDSKSGR", (ftnlen)10);

/*     ZZDSKSGR error cases: */


/* --- Case -------------------------------------------------------- */

    tcase_("Error case: unrecognized coordinate system code.", (ftnlen)48);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = -1.;
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: bad flattening coefficient.", (ftnlen)39);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 4.;
    re = 3e3;
    f = 1.0000000000000999;
    dskdsc[6] = re;
    dskdsc[7] = f;
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: negative equatorial radius.", (ftnlen)39);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 4.;
    re = -3e3;
    f = 0.;
    dskdsc[6] = re;
    dskdsc[7] = f;
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: negative minimum radius.", (ftnlen)36);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 1.;
    dskdsc[20] = -1.;
    dskdsc[21] = 1.;
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     ZZDSKSGR normal cases: */


/* --- Case -------------------------------------------------------- */

    tcase_("Find radius for rectangular boundary.", (ftnlen)37);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 3.;
    dskdsc[16] = -7.;
    dskdsc[17] = 3.;
    dskdsc[18] = 5.;
    dskdsc[19] = 6.;
    dskdsc[20] = -1.;
    dskdsc[21] = 3.;

/*     Compute expected radius. This is the magnitude of the longest */
/*     vector from the origin to a corner of the bounding box. */

/*     Use the dumbest possible brute force approach. This is */
/*     done to have a different algorithm from that in the */
/*     SPICELIB routine. */

    moved_(&dskdsc[16], &c__6, bds);
    xr = 0.;
    for (i__ = 1; i__ <= 2; ++i__) {
	corner[0] = bds[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge(
		"bds", i__1, "f_zzdsksgr__", (ftnlen)260)];
	for (j = 1; j <= 2; ++j) {
	    corner[1] = bds[(i__1 = j + 1) < 6 && 0 <= i__1 ? i__1 : s_rnge(
		    "bds", i__1, "f_zzdsksgr__", (ftnlen)264)];
	    for (k = 1; k <= 2; ++k) {
		corner[2] = bds[(i__1 = k + 3) < 6 && 0 <= i__1 ? i__1 : 
			s_rnge("bds", i__1, "f_zzdsksgr__", (ftnlen)268)];
/* Computing MAX */
		d__1 = xr, d__2 = vnorm_(corner);
		xr = max(d__1,d__2);
	    }
	}
    }
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chcksd_("R", &r__, "~/", &xr, &tol, ok, (ftnlen)1, (ftnlen)2);

/* --- Case -------------------------------------------------------- */

    tcase_("Find radius for latitudinal boundary.", (ftnlen)37);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 1.;
    dskdsc[16] = -pi_() / 4;
    dskdsc[17] = pi_() / 2;
    dskdsc[18] = pi_() / 4;
    dskdsc[19] = pi_() / 3;
    dskdsc[20] = 1e3;
    dskdsc[21] = 2e3;
    xr = dskdsc[21];
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 0.;
    chcksd_("R", &r__, "~/", &xr, &tol, ok, (ftnlen)1, (ftnlen)2);

/* --- Case -------------------------------------------------------- */

    tcase_("Find radius for oblate planetodetic boundary.", (ftnlen)45);
    cleard_(&c__24, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dskdsc[5] = 4.;
    re = 3e3;
    rp = 2e3;
    f = (re - rp) / re;
    dskdsc[6] = re;
    dskdsc[7] = f;
    dskdsc[16] = -pi_() / 4;
    dskdsc[17] = pi_() / 2;
    dskdsc[18] = pi_() / 4;
    dskdsc[19] = pi_() / 3;
    dskdsc[20] = -10.;
    dskdsc[21] = 20.;

/*     The largest distance from the origin is on the equator */
/*     at max altitude. */

    xr = re + dskdsc[21];
    r__ = zzdsksgr_(dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chcksd_("R", &r__, "~/", &xr, &tol, ok, (ftnlen)1, (ftnlen)2);
    t_success__(ok);
    return 0;
} /* f_zzdsksgr__ */

