/* f_zzdsksph.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static doublereal c_b54 = 100.;
static doublereal c_b55 = 200.;
static doublereal c_b56 = 300.;
static doublereal c_b59 = 0.;
static integer c__5000 = 5000;
static integer c__10000 = 10000;
static integer c__100 = 100;
static integer c__1 = 1;
static integer c__8 = 8;

/* $Procedure F_ZZDSKSPH ( ZZDSKSPH tests ) */
/* Subroutine */ int f_zzdsksph__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal);

    /* Local variables */
    static integer nlat, axes[9]	/* was [3][3] */;
    static doublereal dist, last;
    static integer nlon;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), t_topker__(char *, char *, char *, char *, integer *, integer *
	    , char *, doublereal *, char *, integer *, doublereal *, 
	    doublereal *, integer *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), zzellsec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *), t_wrtplt__(integer *, 
	    integer *, char *, doublereal *, doublereal *, integer *, 
	    doublereal *, doublereal *, integer *, integer *, doublereal *, 
	    integer *, logical *, char *, ftnlen, ftnlen), zzellplt_(
	    doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    integer *, integer *, integer *, doublereal *, integer *, integer 
	    *), zzdsksph_(integer *, integer *, integer *, doublereal *, 
	    doublereal *);
    static doublereal a, b, c__, f;
    static integer i__, j, k, n;
    static doublereal radii[3];
    extern /* Subroutine */ int dskgd_(integer *, integer *, doublereal *), 
	    tcase_(char *, ftnlen), dskp02_(integer *, integer *, integer *, 
	    integer *, integer *, integer *), vpack_(doublereal *, doublereal 
	    *, doublereal *, doublereal *);
    static doublereal pnear[3];
    static integer plate[3];
    extern doublereal dpmax_(void), jyear_(void);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), dskz02_(integer *, integer *, integer *, 
	    integer *), dskv02_(integer *, integer *, integer *, integer *, 
	    integer *, doublereal *), movei_(integer *, integer *, integer *),
	     topen_(char *, ftnlen);
    static doublereal first, vtemp[3], xform[9]	/* was [3][3] */, xminr;
    static integer nsurf;
    static doublereal xmaxr;
    extern /* Subroutine */ int pltnp_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *), twopi_(void);
    static doublereal verts[9]	/* was [3][3] */;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal re;
    extern doublereal pi_(void);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static integer np;
    static doublereal offmag, lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    extern doublereal halfpi_(void);
    static doublereal rp;
    static integer nv;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     bodvcd_(integer *, char *, integer *, integer *, doublereal *, 
	    ftnlen), cleari_(integer *, integer *);
    static doublereal angles[9]	/* was [3][3] */;
    static char frames[32*6];
    static doublereal dskdsc[24];
    static char fixref[32], target[32];
    static doublereal bounds[6]	/* was [2][3] */;
    static char sitfnm[32*3], sitnms[32*3];
    extern logical exists_(char *, ftnlen);
    static char topref[32];
    static doublereal corpar[10], maxlat, maxlon, maxrad, minlat, minlon, 
	    minrad, offset[3], sgmaxr, sgminr, sitpos[9]	/* was [3][3] 
	    */, vrtces[15000]	/* was [3][5000] */;
    static integer bodyid, corsys, nxtdsc[8], plates[30000]	/* was [3][
	    10000] */, sitfid[3], sitids[3], surfid, srflst[100];
    static logical makvtl;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     furnsh_(char *, ftnlen), srfrec_(integer *, doublereal *, 
	    doublereal *, doublereal *), sigerr_(char *, ftnlen), spkezp_(
	    integer *, doublereal *, char *, char *, integer *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), pxform_(char *, char *, doublereal 
	    *, doublereal *, ftnlen, ftnlen), chcksd_(char *, doublereal *, 
	    char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen), 
	    dasopr_(char *, integer *, ftnlen), dlabfs_(integer *, integer *, 
	    logical *), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen), dlafns_(integer *, integer *, integer *, logical *), 
	    dascls_(integer *), unload_(char *, ftnlen), kclear_(void);
    static doublereal lat;
    extern doublereal rpd_(void);
    static doublereal lon, tol;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    , t_eldsk2__(integer *, integer *, char *, doublereal *, 
	    doublereal *, integer *, doublereal *, doublereal *, doublereal *,
	     doublereal *, doublereal *, integer *, integer *, logical *, 
	    char *, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the routine ZZDSKSPH. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZDSKSPH. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 26-JUL-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZDSKSPH", (ftnlen)10);
/* ********************************************************************** */

/*     Set-up */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create topocentric frame kernels and supporting kernels.", 
	    (ftnlen)63);

/*     Create and load the PCK; keep the file. */

    if (exists_("zzdsksph_test0.tpc", (ftnlen)18)) {
	delfil_("zzdsksph_test0.tpc", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     We'll use FURNSH to perform the load; this'll save grief later. */

    tstpck_("zzdsksph_test0.tpc", &c_false, &c_true, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zzdsksph_test0.tpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the target. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    bodyid = 499;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
/*     Create the FKs and SPKs needed to support topocentric frames. */

    for (i__ = 1; i__ <= 3; ++i__) {
	sitids[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("sitids", 
		i__1, "f_zzdsksph__", (ftnlen)310)] = i__ + 499000;
	s_copy(sitnms + (((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"sitnms", i__1, "f_zzdsksph__", (ftnlen)312)) << 5), "MARS_S"
		"URFACE_SITE_#", (ftnlen)32, (ftnlen)19);
	repmi_(sitnms + (((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"sitnms", i__1, "f_zzdsksph__", (ftnlen)313)) << 5), "#", &
		i__, sitnms + (((i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : 
		s_rnge("sitnms", i__2, "f_zzdsksph__", (ftnlen)313)) << 5), (
		ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(sitfnm + (((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"sitfnm", i__1, "f_zzdsksph__", (ftnlen)316)) << 5), "MARS_T"
		"OPO_#", (ftnlen)32, (ftnlen)11);
	repmi_(sitfnm + (((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"sitfnm", i__1, "f_zzdsksph__", (ftnlen)317)) << 5), "#", &
		i__, sitfnm + (((i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : 
		s_rnge("sitfnm", i__2, "f_zzdsksph__", (ftnlen)317)) << 5), (
		ftnlen)32, (ftnlen)1, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	sitfid[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("sitfid", 
		i__1, "f_zzdsksph__", (ftnlen)320)] = sitids[(i__2 = i__ - 1) 
		< 3 && 0 <= i__2 ? i__2 : s_rnge("sitids", i__2, "f_zzdsksph"
		"__", (ftnlen)320)];
	s_copy(frames + (((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge(
		"frames", i__1, "f_zzdsksph__", (ftnlen)322)) << 5), "IAU_MA"
		"RS", (ftnlen)32, (ftnlen)8);
	lon = ((i__ - 1) * 3 + 30.) * rpd_();
	lat = ((i__ - 1 << 1) + 45.) * rpd_();
	if (i__ == 1) {

/*           Create a site on Mars' surface. */

	    srfrec_(&bodyid, &lon, &lat, &sitpos[(i__1 = i__ * 3 - 3) < 9 && 
		    0 <= i__1 ? i__1 : s_rnge("sitpos", i__1, "f_zzdsksph__", 
		    (ftnlen)331)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	} else if (i__ == 2) {

/*           Create a site near Mars' center. */

	    vpack_(&c_b54, &c_b55, &c_b56, &sitpos[(i__1 = i__ * 3 - 3) < 9 &&
		     0 <= i__1 ? i__1 : s_rnge("sitpos", i__1, "f_zzdsksph__",
		     (ftnlen)338)]);
	} else if (i__ == 3) {

/*           Create a site near Mars' surface, at a radius */
/*           between C and A. */

	    d__1 = (a + c__) / 2;
	    vpack_(&d__1, &c_b59, &c_b59, &sitpos[(i__1 = i__ * 3 - 3) < 9 && 
		    0 <= i__1 ? i__1 : s_rnge("sitpos", i__1, "f_zzdsksph__", 
		    (ftnlen)345)]);
	} else {

/*           This is the backstop case. */

	    sigerr_("TEST BUG", (ftnlen)8);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	axes[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_zzdsksph__", (ftnlen)356)] = 3;
	axes[(i__1 = i__ * 3 - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_zzdsksph__", (ftnlen)357)] = 2;
	axes[(i__1 = i__ * 3 - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("axes", 
		i__1, "f_zzdsksph__", (ftnlen)358)] = 3;
	angles[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_zzdsksph__", (ftnlen)360)] = -lon;
	angles[(i__1 = i__ * 3 - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_zzdsksph__", (ftnlen)361)] = lat - pi_() / 2;
	angles[(i__1 = i__ * 3 - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("angles",
		 i__1, "f_zzdsksph__", (ftnlen)362)] = pi_();
    }
    first = jyear_() * -100;
    last = jyear_() * 100;

/*     Create topocentric kernels. We'll need to create */
/*     separate kernels for the different target bodies. */

    if (exists_("zzdsksph_topo.tf", (ftnlen)16)) {
	delfil_("zzdsksph_topo.tf", (ftnlen)16);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    if (exists_("zzdsksph_topo.bsp", (ftnlen)17)) {
	delfil_("zzdsksph_topo.bsp", (ftnlen)17);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_topker__("zzdsksph_topo.tf", "zzdsksph_topo.bsp", target, frames, &c__3,
	     sitids, sitnms, sitpos, sitfnm, sitfid, &first, &last, axes, 
	    angles, (ftnlen)16, (ftnlen)17, (ftnlen)32, (ftnlen)32, (ftnlen)
	    32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*      Load the kernels. */

    furnsh_("zzdsksph_topo.tf", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zzdsksph_topo.bsp", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create Mars DSK containing segments that use each coordinate sys"
	    "tem.", (ftnlen)68);
    if (exists_("zzdsksph_test0.bds", (ftnlen)18)) {
	delfil_("zzdsksph_test0.bds", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Start out with a tessellated ellipsoid DSK for Mars. Use */
/*     latitudinal coordinates. */

    bodyid = 499;
    surfid = 1;
    corsys = 1;
    cleard_(&c__3, corpar);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    nlon = 20;
    nlat = 10;
    makvtl = FALSE_;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, "zzdsksph_test0.bds",
	     (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Second segment: use planetodetic coordinates: */

    corsys = 4;
    re = a;
    rp = c__;
    f = (re - rp) / re;
    corpar[0] = re;
    corpar[1] = f;
    surfid = 2;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, "zzdsksph_test0.bds",
	     (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Third segment: use rectangular coordinates: */

    corsys = 3;
    bounds[0] = -a;
    bounds[1] = a;
    bounds[2] = -a;
    bounds[3] = a;
    surfid = 3;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, "zzdsksph_test0.bds",
	     (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Add to Mars DSK three segments having frames with offset centers."
	    , (ftnlen)65);

/*     The fourth segment of the file, which contains three segments */
/*     already, will be centered at a surface point. The segment */
/*     will have rather small extents, so the central body will be */
/*     well outside of the segment's outer bounding surface. */

    corsys = 1;
    cleard_(&c__3, corpar);
    bodyid = 499;
    surfid = 4;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(topref, sitfnm, (ftnlen)32, (ftnlen)32);
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    minlon = rpd_() * 25;
    maxlon = rpd_() * 35;
    minlat = rpd_() * 40;
    maxlat = rpd_() * 50;
    nlon = 20;
    nlat = 10;

/*     Create plate set. */

    zzellsec_(&a, &b, &c__, &minlon, &maxlon, &minlat, &maxlat, &nlon, &nlat, 
	    &c__5000, &c__10000, &nv, vrtces, &np, plates);

/*     Translate the vertices to make them relative to the origin */
/*     of the topocentric frame. Rotate the translated vertices */
/*     to that frame. */

    spkezp_(sitids, &c_b59, fixref, "NONE", &bodyid, offset, &lt, (ftnlen)32, 
	    (ftnlen)4);
    pxform_(fixref, topref, &c_b59, xform, (ftnlen)32, (ftnlen)32);
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	vsub_(&vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ? i__2 : 
		s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)553)], offset, 
		vtemp);
	mxv_(xform, vtemp, &vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ?
		 i__2 : s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)554)]);
    }

/*     Add this segment to the existing Mars DSK. */

    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    makvtl = FALSE_;
    t_wrtplt__(&bodyid, &surfid, topref, &first, &last, &corsys, corpar, 
	    bounds, &nv, &np, vrtces, plates, &makvtl, "zzdsksph_test0.bds", (
	    ftnlen)32, (ftnlen)18);

/*     The fifth segment of the file will be centered at a point */
/*     close to Mars' center. The segment will surround the */
/*     frame center and be bounded away from it. */

    corsys = 1;
    cleard_(&c__3, corpar);
    bodyid = 499;
    surfid = 5;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(topref, sitfnm + 32, (ftnlen)32, (ftnlen)32);
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    nlon = 20;
    nlat = 10;

/*     Create plate set. */

    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__5000, &c__10000, &nv, vrtces, &
	    np, plates);

/*     Translate the vertices to make them relative to the origin */
/*     of the topocentric frame. Rotate the translated vertices */
/*     to that frame. */

    spkezp_(&sitids[1], &c_b59, fixref, "NONE", &bodyid, offset, &lt, (ftnlen)
	    32, (ftnlen)4);
    pxform_(fixref, topref, &c_b59, xform, (ftnlen)32, (ftnlen)32);
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	vsub_(&vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ? i__2 : 
		s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)619)], offset, 
		vtemp);
	mxv_(xform, vtemp, &vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ?
		 i__2 : s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)620)]);
    }

/*     Add this segment to the existing Mars DSK. */

    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    makvtl = FALSE_;
    t_wrtplt__(&bodyid, &surfid, topref, &first, &last, &corsys, corpar, 
	    bounds, &nv, &np, vrtces, plates, &makvtl, "zzdsksph_test0.bds", (
	    ftnlen)32, (ftnlen)18);

/*     The sixth segment of the file will be centered at a point */
/*     close to Mars' surface, but below the sphere of maximum radius. */

    corsys = 1;
    cleard_(&c__3, corpar);
    bodyid = 499;
    surfid = 6;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    s_copy(topref, sitfnm + 64, (ftnlen)32, (ftnlen)32);
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    nlon = 20;
    nlat = 10;

/*     Create plate set. */

    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__5000, &c__10000, &nv, vrtces, &
	    np, plates);

/*     Translate the vertices to make them relative to the origin */
/*     of the topocentric frame. Rotate the translated vertices */
/*     to that frame. */

    spkezp_(&sitids[2], &c_b59, fixref, "NONE", &bodyid, offset, &lt, (ftnlen)
	    32, (ftnlen)4);
    pxform_(fixref, topref, &c_b59, xform, (ftnlen)32, (ftnlen)32);
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	vsub_(&vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ? i__2 : 
		s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)685)], offset, 
		vtemp);
	mxv_(xform, vtemp, &vrtces[(i__2 = i__ * 3 - 3) < 15000 && 0 <= i__2 ?
		 i__2 : s_rnge("vrtces", i__2, "f_zzdsksph__", (ftnlen)686)]);
    }

/*     Add this segment to the existing Mars DSK. */

    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    makvtl = FALSE_;
    t_wrtplt__(&bodyid, &surfid, topref, &first, &last, &corsys, corpar, 
	    bounds, &nv, &np, vrtces, plates, &makvtl, "zzdsksph_test0.bds", (
	    ftnlen)32, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Create Saturn DSK containing segments that use each coordinate s"
	    "ystem.", (ftnlen)70);
    if (exists_("zzdsksph_test1.bds", (ftnlen)18)) {
	delfil_("zzdsksph_test1.bds", (ftnlen)18);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Start out with a tessellated ellipsoid DSK for Mars. Use */
/*     latitudinal coordinates. */

    bodyid = 699;
    surfid = 1;
    corsys = 1;
    cleard_(&c__3, corpar);
    s_copy(fixref, "IAU_SATURN", (ftnlen)32, (ftnlen)10);
    first = jyear_() * -100;
    last = jyear_() * 100;
    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    nlon = 20;
    nlat = 10;
    makvtl = FALSE_;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, "zzdsksph_test1.bds",
	     (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Second segment: use planetodetic coordinates. Make */
/*     Saturn into a *prolate* shape to excercise logic for */
/*     such shapes. */

    corsys = 4;
    re = c__;
    rp = a;
    f = (re - rp) / re;
    corpar[0] = re;
    corpar[1] = f;
    surfid = 2;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &c__, &c__, &a, &nlon, &nlat, &makvtl, "zzdsksph_test1.b"
	    "ds", (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Third segment: use rectangular coordinates: */

    corsys = 3;
    bounds[0] = -a;
    bounds[1] = a;
    bounds[2] = -a;
    bounds[3] = a;
    surfid = 3;
    t_eldsk2__(&bodyid, &surfid, fixref, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, "zzdsksph_test1.bds",
	     (ftnlen)32, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars latitudinal segment.", (ftnlen)49);
    furnsh_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__100, srflst);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 1;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Since the number of latitude bands is even, the maximum */
/*     radius should be attained by vertices on the equator. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    xmaxr = a;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The minimum radius should match the bound in the dscriptor. */

    dasopr_("zzdsksph_test0.bds", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xminr = dskdsc[20];
    tol = 0.;
    chcksd_("(1) MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)10, (ftnlen)
	    1);

/*     Find the actual minimum radius of the surface and use this */
/*     as a secondary check. */

/*     The minimum radius is not so simple to find. We'll brute-force */
/*     it by finding the distance of each plate from the origin. */

    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xminr = a;
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dskp02_(&handle, dladsc, &i__, &c__1, &n, plate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Fetch vertices of the Ith plate. */

	for (j = 1; j <= 3; ++j) {
	    k = plate[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("plate",
		     i__2, "f_zzdsksph__", (ftnlen)885)];
	    dskv02_(&handle, dladsc, &k, &c__1, &n, &verts[(i__2 = j * 3 - 3) 
		    < 9 && 0 <= i__2 ? i__2 : s_rnge("verts", i__2, "f_zzdsk"
		    "sph__", (ftnlen)887)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the distance of the Ith plate from the origin. */

	pltnp_(origin, verts, &verts[3], &verts[6], pnear, &dist);
	xminr = min(xminr,dist);
    }
    tol = 1e-13;
    chcksd_("(2) MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)10, (
	    ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Mars latitudinal case; re-use segment list.", (ftnlen)50);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars planetodetic segment.", (ftnlen)50);

/*     Get the DLA descriptor for this segment. */

    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    movei_(nxtdsc, &c__8, dladsc);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The minimum radius will be computed from the reference */
/*     ellipsoid's radii and the segment's altitude bounds. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xminr = c__ + dskdsc[20];
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     Since the number of latitude bands is even, the maximum */
/*     radius should be attained by vertices on the equator. */

    xmaxr = a;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     Restore the expected minimum radius from the DSK descriptor. */

    xminr = dskdsc[20] + c__;

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Mars planetodetic case; re-use segment list.", (ftnlen)51);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars rectangular segment.", (ftnlen)49);

/*     Get the DLA descriptor for this segment. */

    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    movei_(nxtdsc, &c__8, dladsc);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 3;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the outer bounding radius is that of a box */
/*     that circumscribes the ellipsoid. */

/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = c__;
    xmaxr = sqrt(d__1 * d__1 * 2 + d__2 * d__2);
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The segment's central body is inside the segment's bounding box; */
/*     the expected minimum radius is zero. */

    xminr = 0.;
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Mars rectangular case; re-use segment list.", (ftnlen)50);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     Close the DSK so we can unload it later. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/*     Now load Saturn kernels; repeat tests for Saturn. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Saturn latitudinal segment.", (ftnlen)51);

/*     Load Saturn DSK; leave Mars DSK loaded. */

    furnsh_("zzdsksph_test1.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__100, srflst);
    bodyid = 699;
    nsurf = 1;
    srflst[0] = 1;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Since the number of latitude bands is even, the maximum */
/*     radius should be attained by vertices on the equator. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, radii, (ftnlen)5);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    xmaxr = a;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The minimum radius should match the bound in the dscriptor. */

    dasopr_("zzdsksph_test1.bds", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xminr = dskdsc[20];
    tol = 0.;
    chcksd_("(1) MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)10, (ftnlen)
	    1);

/*     Find the actual minimum radius of the surface and use this */
/*     as a secondary check. */

/*     The minimum radius is not so simple to find. We'll brute-force */
/*     it by finding the distance of each plate from the origin. */

    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xminr = a;
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dskp02_(&handle, dladsc, &i__, &c__1, &n, plate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Fetch vertices of the Ith plate. */

	for (j = 1; j <= 3; ++j) {
	    k = plate[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("plate",
		     i__2, "f_zzdsksph__", (ftnlen)1154)];
	    dskv02_(&handle, dladsc, &k, &c__1, &n, &verts[(i__2 = j * 3 - 3) 
		    < 9 && 0 <= i__2 ? i__2 : s_rnge("verts", i__2, "f_zzdsk"
		    "sph__", (ftnlen)1156)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the distance of the Ith plate from the origin. */

	pltnp_(origin, verts, &verts[3], &verts[6], pnear, &dist);
	xminr = min(xminr,dist);
    }
    tol = 1e-13;
    chcksd_("(2) MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)10, (
	    ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Saturn latitudinal case; re-use segment list.", (ftnlen)52)
	    ;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Saturn planetodetic segment.", (ftnlen)52)
	    ;

/*     Get the DLA descriptor for this segment. */

    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    movei_(nxtdsc, &c__8, dladsc);

/*     We'll need the DSK descriptor for this segment, since */
/*     the segment bounds are unique to this segment. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodyid = 699;
    nsurf = 1;
    srflst[0] = 2;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Since the number of latitude bands is even, the maximum */
/*     radius should be attained by vertices on the pole (recall */
/*     we've made Saturn prolate in this segment). */

    xmaxr = a;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The minimum radius will be computed from the reference */
/*     ellipsoid's radii and the segment's altitude bounds. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Recall this model is prolate. However, A, B, C are from */
/*     the kernel pool. */

    xminr = c__ + dskdsc[20];
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Saturn planetodetic case; re-use segment list.", (ftnlen)
	    53);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Saturn rectangular segment.", (ftnlen)51);

/*     Get the DLA descriptor for this segment. */

    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA FOUND", &found, &c_true, ok, (ftnlen)9);
    movei_(nxtdsc, &c__8, dladsc);
    bodyid = 699;
    nsurf = 1;
    srflst[0] = 3;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In this case, the outer bounding radius is that of a box */
/*     that circumscribes the ellipsoid. */

/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = c__;
    xmaxr = sqrt(d__1 * d__1 * 2 + d__2 * d__2);
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The segment's central body is inside the segment's bounding box; */
/*     the expected minimum radius is zero. */

    xminr = 0.;
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat Saturn rectangular case; re-use segment list.", (ftnlen)52)
	    ;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     Check handling of BSR state change. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Unload Mars DSK and repeat Saturn rectangular case again; re-use"
	    " segment list.", (ftnlen)78);
    unload_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);
    chcksd_("MINRAD", &minrad, "~/", &xminr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     Close the Saturn DSK so we can unload it later. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The next set of tests uses frames having centers offset */
/*     from the central body. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars segment having its frame center nea"
	    "r Mars' surface. For this case, the minimum radius is the maximu"
	    "m segment radius minus the frame offset.", (ftnlen)168);
    furnsh_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleari_(&c__100, srflst);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 4;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For this case, the upper bound is the sum of the */
/*     DSK descriptor's upper radius bound and the magnitude of */
/*     the offset between the frame's center and Mars. */

    spkezp_(sitids, &c_b59, sitfnm, "NONE", &bodyid, offset, &lt, (ftnlen)32, 
	    (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    offmag = vnorm_(offset);
    dasopr_("zzdsksph_test0.bds", &handle, (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLABFS found", &found, &c_true, ok, (ftnlen)12);
    for (i__ = 1; i__ <= 3; ++i__) {
	dlafns_(&handle, nxtdsc, dladsc, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DLAFNS found", &found, &c_true, ok, (ftnlen)12);
	movei_(dladsc, &c__8, nxtdsc);
    }
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xmaxr = dskdsc[21] + offmag;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The minimum radius should be the lower bound in the descriptor, */
/*     minus the offset magnitude. */

/*     The minimum radius should be the offset magnitude, minus */
/*     the upper bound in the descriptor. */

    xminr = offmag - dskdsc[21];
    tol = 0.;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars segment having its frame center nea"
	    "r Mars' center. For this case, the minimum radius is the minimum"
	    " segment radius minus the frame offset.", (ftnlen)167);
    cleari_(&c__100, srflst);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 5;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For this case, the upper bound is the sum of the */
/*     DSK descriptor's upper radius bound and the magnitude of */
/*     the offset between the frame's center and Mars. */

    spkezp_(&sitids[1], &c_b59, sitfnm + 64, "NONE", &bodyid, offset, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    offmag = vnorm_(offset);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLAFNS found", &found, &c_true, ok, (ftnlen)12);
    movei_(nxtdsc, &c__8, dladsc);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xmaxr = dskdsc[21] + offmag;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The minimum radius should be the lower bound in the descriptor, */
/*     minus the offset magnitude. */

/*     The minimum radius should be the offset magnitude, minus */
/*     the upper bound in the descriptor. */

    xminr = dskdsc[20] - offmag;
    tol = 0.;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find bounding radii for Mars segment having its frame center sli"
	    "ghtly below Mars' surface. For this case, the minimum radius is "
	    "zero.", (ftnlen)133);
    cleari_(&c__100, srflst);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 6;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     For this case, the upper bound is the sum of the */
/*     DSK descriptor's upper radius bound and the magnitude of */
/*     the offset between the frame's center and Mars. */

    spkezp_(&sitids[2], &c_b59, sitfnm + 64, "NONE", &bodyid, offset, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    offmag = vnorm_(offset);
    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLAFNS found", &found, &c_true, ok, (ftnlen)12);
    movei_(nxtdsc, &c__8, dladsc);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xmaxr = dskdsc[21] + offmag;
    tol = 1e-13;
    chcksd_("MAXRAD", &maxrad, "~/", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)2);

/*     The frame center offset is between the segment's minimum */
/*     and maximum radius, so we can't guarantee that the frame */
/*     center is a positive distance from all plates. */

    xminr = 0.;
    tol = 0.;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Close the Mars DSK so we can unload it later. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find Mars radius bounds using empty surface list.", (ftnlen)49);
    bodyid = 499;
    nsurf = 0;
    cleari_(&c__100, srflst);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected bounds by combining bounds for each surface. */

    xminr = dpmax_();
    xmaxr = 0.;
    for (i__ = 1; i__ <= 6; ++i__) {
	zzdsksph_(&bodyid, &nsurf, &srflst[(i__1 = i__ - 1) < 100 && 0 <= 
		i__1 ? i__1 : s_rnge("srflst", i__1, "f_zzdsksph__", (ftnlen)
		1592)], &sgminr, &sgmaxr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xminr = min(xminr,sgminr);
	xmaxr = max(xmaxr,sgmaxr);
    }
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "=", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)1);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: try to find bounds for body having no loaded DSK segments."
	    , (ftnlen)65);
    bodyid = 299;
    nsurf = 0;
    cleari_(&c__100, srflst);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_true, "SPICE(DSKDATANOTFOUND)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: find bounds for Mars. Unload Mars DSK. Try again to find "
	    "bounds for Mars.", (ftnlen)80);
    bodyid = 499;
    nsurf = 0;
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_true, "SPICE(DSKDATANOTFOUND)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reload Mars DSK. Unload Mars SPK. Try again to find bounds for M"
	    "ars.", (ftnlen)68);
    furnsh_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzdsksph_topo.bsp", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_true, "SPICE(NOLOADEDFILES)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reload Mars SPK. Unload Mars FK. Try again to find bounds for Ma"
	    "rs.", (ftnlen)67);
    furnsh_("zzdsksph_topo.bsp", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzdsksph_topo.tf", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_true, "SPICE(NOFRAMEDATA)", ok, (ftnlen)18);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reload FK.  Try again to find bounds for Mars. This is a non-err"
	    "or case.", (ftnlen)72);
    furnsh_("zzdsksph_topo.tf", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected bounds by combining bounds for each surface. */

    xminr = dpmax_();
    xmaxr = 0.;
    for (i__ = 1; i__ <= 6; ++i__) {
	zzdsksph_(&bodyid, &nsurf, &srflst[(i__1 = i__ - 1) < 100 && 0 <= 
		i__1 ? i__1 : s_rnge("srflst", i__1, "f_zzdsksph__", (ftnlen)
		1710)], &sgminr, &sgmaxr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xminr = min(xminr,sgminr);
	xmaxr = max(xmaxr,sgmaxr);
    }
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "=", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat previous case, minus the kernel loads.", (ftnlen)45);
    zzdsksph_(&bodyid, &nsurf, srflst, &minrad, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected bounds by combining bounds for each surface. */

    xminr = dpmax_();
    xmaxr = 0.;
    for (i__ = 1; i__ <= 6; ++i__) {
	zzdsksph_(&bodyid, &nsurf, &srflst[(i__1 = i__ - 1) < 100 && 0 <= 
		i__1 ? i__1 : s_rnge("srflst", i__1, "f_zzdsksph__", (ftnlen)
		1743)], &sgminr, &sgmaxr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xminr = min(xminr,sgminr);
	xmaxr = max(xmaxr,sgmaxr);
    }
    tol = 1e-13;
    chcksd_("MINRAD", &minrad, "=", &xminr, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("MAXRAD", &maxrad, "=", &xmaxr, &tol, ok, (ftnlen)6, (ftnlen)1);
/* ********************************************************************** */

/*     Clean up */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdsksph_test0.tpc", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdsksph_topo.tf", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdsksph_topo.bsp", (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdsksph_test0.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzdsksph_test1.bds", (ftnlen)18);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzdsksph__ */

