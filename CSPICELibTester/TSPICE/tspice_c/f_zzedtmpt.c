/* f_zzedtmpt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b7 = 1.;
static logical c_false = FALSE_;
static integer c__3 = 3;
static doublereal c_b153 = -100.;
static doublereal c_b154 = 100.;
static doublereal c_b155 = 10.;
static integer c__14 = 14;
static logical c_true = TRUE_;
static doublereal c_b225 = -1.;

/* $Procedure F_ZZEDTMPT ( ZZEDTMPT tests ) */
/* Subroutine */ int f_zzedtmpt__(logical *ok)
{
    /* Initialized data */

    static char lchar[1*2] = "T" "F";
    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1;

    /* Builtin functions */
    double sqrt(doublereal), asin(doublereal), sin(doublereal), cos(
	    doublereal), pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal beta;
    static integer seed;
    static doublereal dlat, dlon;
    static integer nlat;
    static doublereal dist, axis[3];
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal proj[3];
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), zzedtmpt_(logical *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal a, b, c__;
    static integer i__, j, k;
    static doublereal alpha, scale, x[3], y[3], z__[3];
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), tcase_(char *, ftnlen), vpack_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal theta;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen), edpnt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static char title[255];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    static doublereal point[3];
    extern /* Subroutine */ int vprjp_(doublereal *, doublereal *, doublereal 
	    *), t_success__(logical *), vrotv_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    , nvp2pl_(doublereal *, doublereal *, doublereal *), psv2pl_(
	    doublereal *, doublereal *, doublereal *, doublereal *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     latrec_(doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal axioff[3], srcrad;
    static integer nshape, shadix;
    static logical umbral;
    static doublereal refpln[4], normal[3], plnvec[3], tanpln[4];
    extern /* Subroutine */ int surfnm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal xpoint[3], lat;
    extern doublereal dpr_(void);
    static doublereal dot, lon;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static doublereal tol;

/* $ Abstract */

/*     Exercise the routine ZZEDTMPT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZEDTMPT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 30-JUN-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Other functions */

/*        T_RANDD ( LOWER, UPPER, SEED ) */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZEDTMPT", (ftnlen)10);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/*     The following simple cases are meant to assist debugging. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Light source and target are spheres of the same size: A. Distanc"
	    "e between centers is 3*A. Umbral case.", (ftnlen)102);
    a = 1e3;
    b = a;
    c__ = a;
    srcrad = a;
    umbral = TRUE_;
    d__1 = a * 3;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = sqrt(2.) * .5 * a;
    vscl_(&d__1, plnvec, xpoint);
    tol = 1e-11;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Light source and target are spheres of the same size: A. Distanc"
	    "e between centers is 3*A. Penumbral case.", (ftnlen)105);
    a = 1e3;
    b = a;
    c__ = a;
    srcrad = a;
    umbral = FALSE_;
    d__1 = a * 3;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     A tangent segment connecting the sphere and target, */
/*     in the plane defined by PLNVEC, crosses the axis with */
/*     angle ALPHA, where */

/*        sin( ALPHA ) = A / (1.5 * A) */

/*     The complementary angle is BETA. */

    alpha = asin(.66666666666666663);
    beta = halfpi_() - alpha;
    d__1 = sin(beta) * a * sqrt(2.) / 2;
    vscl_(&d__1, plnvec, xpoint);
    xpoint[0] = a * cos(beta);
    tol = 1e-11;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial target. Source is larger than longest target axis. Umbr"
	    "al case.", (ftnlen)72);
    a = 1e3;
    b = a * 2;
    c__ = a / 2;
    srcrad = a * 10;
    umbral = TRUE_;
    d__1 = a * 20;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure POINT is on the target. Find the scaled version */
/*     of POINT on the target surface and compare. */

    edpnt_(point, &a, &b, &c__, xpoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is in the reference plane. Project POINT */
/*     onto the plane and compare. */

    psv2pl_(origin, axis, plnvec, refpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(point, refpln, xpoint);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is on the correct side of the axis with respect */
/*     to PLNVEC. */

    dot = vdot_(point, plnvec);
    chcksd_("point DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)9, (ftnlen)1);

/*     Create a tangent plane at the terminator point. The distance */
/*     between this plane and the center of the source should be */
/*     the radius of the source. */

    surfnm_(&a, &b, &c__, point, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nvp2pl_(normal, point, tanpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(axis, tanpln, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dist = vdist_(axis, proj);
    tol = 1e-11;
    chcksd_("DIST", &dist, "~/", &srcrad, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     Make sure the source center is on the correct side of the */
/*     tangent plane. */

    vsub_(axis, proj, axioff);
    dot = vdot_(axioff, normal);
    chcksd_("axis DOT", &dot, "<", &c_b4, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial target. Source is smaller than shortest target axis. Um"
	    "bral case.", (ftnlen)74);
    a = 1e3;
    b = a * 2;
    c__ = a / 2;
    srcrad = a / 10;
    umbral = TRUE_;
    d__1 = a * 40;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure POINT is on the target. Find the scaled version */
/*     of POINT on the target surface and compare. */

    edpnt_(point, &a, &b, &c__, xpoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is in the reference plane. Project POINT */
/*     onto the plane and compare. */

    psv2pl_(origin, axis, plnvec, refpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(point, refpln, xpoint);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is on the correct side of the axis with respect */
/*     to PLNVEC. */

    dot = vdot_(point, plnvec);
    chcksd_("DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)3, (ftnlen)1);

/*     Create a tangent plane at the terminator point. The distance */
/*     between this plane and the center of the source should be */
/*     the radius of the source. */

    surfnm_(&a, &b, &c__, point, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nvp2pl_(normal, point, tanpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(axis, tanpln, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dist = vdist_(axis, proj);
    tol = 1e-11;
    chcksd_("DIST", &dist, "~/", &srcrad, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     Make sure the source center is on the correct side of the */
/*     tangent plane. */

    vsub_(axis, proj, axioff);
    dot = vdot_(axioff, normal);
    chcksd_("axis DOT", &dot, "<", &c_b4, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial target. Source is larger than longest target axis. Penu"
	    "mbral case.", (ftnlen)75);
    a = 1e3;
    b = a * 2;
    c__ = a / 2;
    srcrad = a * 10;
    umbral = FALSE_;
    d__1 = a * 30;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure POINT is on the target. Find the scaled version */
/*     of POINT on the target surface and compare. */

    edpnt_(point, &a, &b, &c__, xpoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is in the reference plane. Project POINT */
/*     onto the plane and compare. */

    psv2pl_(origin, axis, plnvec, refpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(point, refpln, xpoint);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is on the correct side of the axis with respect */
/*     to PLNVEC. */

    dot = vdot_(point, plnvec);
    chcksd_("DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)3, (ftnlen)1);

/*     Create a tangent plane at the terminator point. The distance */
/*     between this plane and the center of the source should be */
/*     the radius of the source. */

    surfnm_(&a, &b, &c__, point, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nvp2pl_(normal, point, tanpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(axis, tanpln, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dist = vdist_(axis, proj);
    tol = 1e-11;
    chcksd_("DIST", &dist, "~/", &srcrad, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     Make sure the source center is on the correct side of the */
/*     tangent plane. */

    vsub_(axis, proj, axioff);
    dot = vdot_(axioff, normal);
    chcksd_("axis DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Triaxial target. Source is smaller than shortest target axis. Pe"
	    "numbral case.", (ftnlen)77);
    a = 1e3;
    b = a * 2;
    c__ = a / 2;
    srcrad = a / 10;
    umbral = FALSE_;
    d__1 = a * 30;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure POINT is on the target. Find the scaled version */
/*     of POINT on the target surface and compare. */

    edpnt_(point, &a, &b, &c__, xpoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is in the reference plane. Project POINT */
/*     onto the plane and compare. */

    psv2pl_(origin, axis, plnvec, refpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(point, refpln, xpoint);
    tol = 1e-13;
    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     Make sure POINT is on the correct side of the axis with respect */
/*     to PLNVEC. */

    dot = vdot_(point, plnvec);
    chcksd_("DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)3, (ftnlen)1);

/*     Create a tangent plane at the terminator point. The distance */
/*     between this plane and the center of the source should be */
/*     the radius of the source. */

    surfnm_(&a, &b, &c__, point, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nvp2pl_(normal, point, tanpln);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vprjp_(axis, tanpln, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dist = vdist_(axis, proj);
    tol = 1e-11;
    chcksd_("DIST", &dist, "~/", &srcrad, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     Make sure the source center is on the correct side of the */
/*     tangent plane. */

    vsub_(axis, proj, axioff);
    dot = vdot_(axioff, normal);
    chcksd_("axis DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     The following cases use a variety of target shapes, scales, and */
/*     light source positions */


    nlat = 11;
    nlon = 10;
    dlat = pi_() / (nlat - 1);
    dlon = pi_() * 2 / nlon;
    nshape = 10;
    seed = -100;
    for (shadix = 1; shadix <= 2; ++shadix) {

/*        Set the shadow type flag. */

	umbral = shadix == 1;
	i__1 = nlon;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    lon = (i__ - 1) * dlon;
	    i__2 = nlat;
	    for (j = 1; j <= i__2; ++j) {
		lat = halfpi_() - (j - 1) * dlat;
		i__3 = nshape;
		for (k = 1; k <= i__3; ++k) {

/*                 Pick scale factor. */

		    d__1 = t_randd__(&c_b153, &c_b154, &seed);
		    scale = pow_dd(&c_b155, &d__1);
		    a = scale * t_randd__(&c_b7, &c_b155, &seed);
		    b = scale * t_randd__(&c_b7, &c_b155, &seed);
		    c__ = scale * t_randd__(&c_b7, &c_b155, &seed);
		    srcrad = scale * t_randd__(&c_b7, &c_b155, &seed);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Pick a rotation angle for the cutting half plane. */

		    d__1 = pi_() * 2;
		    theta = t_randd__(&c_b4, &d__1, &seed);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

		    s_copy(title, "Random case. SCALE = #; A = #; B = #; C ="
			    " #; SRCRAD = #, THETA (deg) = #; UMBRAL = #.", (
			    ftnlen)255, (ftnlen)85);
		    repmd_(title, "#", &scale, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    repmd_(title, "#", &a, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    repmd_(title, "#", &b, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    repmd_(title, "#", &c__, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    repmd_(title, "#", &srcrad, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    d__1 = theta * dpr_();
		    repmd_(title, "#", &d__1, &c__14, title, (ftnlen)255, (
			    ftnlen)1, (ftnlen)255);
		    repmc_(title, "#", lchar + ((i__4 = shadix - 1) < 2 && 0 
			    <= i__4 ? i__4 : s_rnge("lchar", i__4, "f_zzedtm"
			    "pt__", (ftnlen)691)), title, (ftnlen)255, (ftnlen)
			    1, (ftnlen)1, (ftnlen)255);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)255);
		    d__1 = scale * 1e3;
		    latrec_(&d__1, &lon, &lat, axis);
		    vequ_(axis, x);
		    frame_(x, y, z__);
		    vrotv_(z__, axis, &theta, plnvec);
		    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, 
			    point);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Make sure POINT is on the target. Find the scaled */
/*                 version of POINT on the target surface and compare. */

		    edpnt_(point, &a, &b, &c__, xpoint);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tol = 1e-13;
		    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (
			    ftnlen)5, (ftnlen)3);

/*                 Make sure POINT is in the reference plane. Project */
/*                 POINT onto the plane and compare. */

		    psv2pl_(origin, axis, plnvec, refpln);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    vprjp_(point, refpln, xpoint);
		    tol = 1e-13;
		    chckad_("POINT", point, "~~/", xpoint, &c__3, &tol, ok, (
			    ftnlen)5, (ftnlen)3);

/*                 Make sure POINT is on the correct side of the axis */
/*                 with respect to PLNVEC. */

		    dot = vdot_(point, plnvec);
		    chcksd_("DOT", &dot, ">", &c_b4, &tol, ok, (ftnlen)3, (
			    ftnlen)1);

/*                 Create a tangent plane at the terminator point. The */
/*                 distance between this plane and the center of the */
/*                 source should be the radius of the source. */

		    surfnm_(&a, &b, &c__, point, normal);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    nvp2pl_(normal, point, tanpln);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    vprjp_(axis, tanpln, proj);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    dist = vdist_(axis, proj);
		    tol = 1e-11;
		    chcksd_("DIST", &dist, "~/", &srcrad, &tol, ok, (ftnlen)4,
			     (ftnlen)2);

/*                 Make sure the source center is on the correct side of */
/*                 the tangent plane. */

		    vsub_(axis, proj, axioff);
		    dot = vdot_(axioff, normal);
		    if (umbral) {
			chcksd_("axis DOT", &dot, "<", &c_b4, &tol, ok, (
				ftnlen)8, (ftnlen)1);
		    } else {
			chcksd_("axis DOT", &dot, ">", &c_b4, &tol, ok, (
				ftnlen)8, (ftnlen)1);
		    }
		}
	    }
	}
    }
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii.", (ftnlen)17);
    a = 1e3;
    b = a;
    c__ = a;
    srcrad = a;
    umbral = FALSE_;
    d__1 = a * 3;
    vpack_(&d__1, &c_b4, &c_b4, axis);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);
    zzedtmpt_(&umbral, &c_b4, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedtmpt_(&umbral, &c_b225, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedtmpt_(&umbral, &a, &c_b4, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedtmpt_(&umbral, &a, &c_b225, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedtmpt_(&umbral, &a, &b, &c_b4, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);
    zzedtmpt_(&umbral, &a, &b, &c_b225, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDAXISLENGTH)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad source radius", (ftnlen)17);
    zzedtmpt_(&umbral, &a, &b, &c__, &c_b4, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);
    zzedtmpt_(&umbral, &a, &b, &c__, &c_b225, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(INVALIDRADIUS)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad axis vector", (ftnlen)15);
    cleard_(&c__3, axis);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);
    d__1 = a * 3;
    vpack_(&d__1, &c_b4, &c_b4, axis);

/* --- Case: ------------------------------------------------------ */

    tcase_("Source and target are too close.", (ftnlen)32);
    vpack_(&a, &c_b4, &c_b4, axis);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(OBJECTSTOOCLOSE)", ok, (ftnlen)22);
    d__1 = a * 3;
    vpack_(&d__1, &c_b4, &c_b4, axis);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad plane reference vector", (ftnlen)26);
    cleard_(&c__3, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);
    vpack_(&c_b4, &c_b7, &c_b7, plnvec);

/* --- Case: ------------------------------------------------------ */

    tcase_("Axis and plane reference vector are linearly dependent.", (ftnlen)
	    55);
    vequ_(axis, plnvec);
    zzedtmpt_(&umbral, &a, &b, &c__, &srcrad, axis, plnvec, point);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzedtmpt__ */

