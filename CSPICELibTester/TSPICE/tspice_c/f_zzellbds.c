/* f_zzellbds.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static doublereal c_b27 = 1e-15;
static doublereal c_b37 = 0.;

/* $Procedure      F_ZZELLBDS ( Test bounding ellipsoid routine ) */
/* Subroutine */ int f_zzellbds__(logical *ok)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    doublereal amin, bmin, amax, bmax, hmin, hmax, xlat, xlon;
    extern /* Subroutine */ int zzellbds_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal a, b;
    integer i__, n;
    doublereal p[3], delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal xamin, xbmin, xamax, xbmax;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), t_success__(
	    logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    char casnam[80];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     georec_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), recgeo_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal alt, lat, lon;

/* $ Abstract */

/*     Test the SPICELIB bounding ellipsoid routine ZZELLBDS. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the routine ZZELLBDS. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 13-FEB-2017 (NJB) */

/*        Previous version 15-JAN-2016 (NJB) */

/*        Reduced sample counts from 100001 to 1001. */

/* -    TSPICE Version 1.0.0, 27-NOV-2012 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZELLBDS", (ftnlen)10);

/*     ZZELLBDS error cases: */


/* --- Case -------------------------------------------------------- */

    tcase_("Error case: B is non-positive.", (ftnlen)30);
    a = 5.;
    b = 0.;
    hmax = 1.;
    hmin = -1.;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(NONPOSITIVERADIUS)", ok, (ftnlen)24);
    b = -1.;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(NONPOSITIVERADIUS)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: B > A.", (ftnlen)18);
    a = 5.;
    b = 5.1;
    hmax = 1.;
    hmin = -1.;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(RADIIOUTOFORDER)", ok, (ftnlen)22);
    b = -1.;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(NONPOSITIVERADIUS)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: HMIN > HMAX.", (ftnlen)24);
    a = 5.;
    b = 4.;
    hmax = 1.;
    hmin = 1.1;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: HMIN <= -B.", (ftnlen)23);
    a = 5.;
    b = 4.;
    hmax = 1.;
    hmin = -b;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(LOWERBOUNDTOOLOW)", ok, (ftnlen)23);

/* --- Case -------------------------------------------------------- */

    tcase_("Error case: B + (A/B)*HMIN <= 0", (ftnlen)31);
    a = 5.;
    b = 4.;
    hmax = 1.;
    hmin = b * -.9;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_true, "SPICE(LOWERBOUNDTOOLOW)", ok, (ftnlen)23);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Normal case: HMAX > 0, HMIN < 0", (ftnlen)31);
    a = 25.;
    b = 20.;
    hmax = 1.;
    hmin = -1.;
    xamax = a + hmax;
    xbmax = b + hmax * (a / b);
    xamin = a + hmin;
    xbmin = b + hmin * (a / b);
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("AMAX", &amax, "~", &xamax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMAX", &bmax, "~", &xbmax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("AMIN", &amin, "~", &xamin, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMIN", &bmin, "~", &xbmin, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     Sample heights from the bounding ellipsoids. Verify that */
/*     the heights satisfy the expected conditions. */

    n = 10001;
    delta = pi_() * 2 / (n - 1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the outer bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amax - bmax) / amax;
	georec_(&xlon, &xlat, &c_b37, &amax, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no smaller than HMAX. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt < hmax) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmax - 1e-13;
	    chcksd_(casnam, &alt, ">=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the inner bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amin - bmin) / amin;
	georec_(&xlon, &xlat, &c_b37, &amin, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no greater than HMIN. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt > hmin) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmin + 1e-13;
	    chcksd_(casnam, &alt, "<=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Normal case: HMAX = 0, HMIN = 0", (ftnlen)31);
    a = 25.;
    b = 20.;
    hmax = 0.;
    hmin = 0.;
    xamax = a + hmax;
    xbmax = b + hmax * (a / b);
    xamin = a + hmin;
    xbmin = b + hmin * (a / b);
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("AMAX", &amax, "~", &xamax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMAX", &bmax, "~", &xbmax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("AMIN", &amin, "~", &xamin, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMIN", &bmin, "~", &xbmin, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     Sample heights from the bounding ellipsoids. Verify that */
/*     the heights satisfy the expected conditions. */

    n = 10001;
    delta = pi_() * 2 / (n - 1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the outer bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amax - bmax) / amax;
	georec_(&xlon, &xlat, &c_b37, &amax, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no smaller than HMAX. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt < hmax) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmax - 1e-13;
	    chcksd_(casnam, &alt, ">=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the inner bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amin - bmin) / amin;
	georec_(&xlon, &xlat, &c_b37, &amin, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no greater than HMIN. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt > hmin) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmin + 1e-13;
	    chcksd_(casnam, &alt, "<=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Normal case: HMAX > 0, HMIN > 0", (ftnlen)31);
    a = 25.;
    b = 20.;
    hmax = 5.;
    hmin = 1.;
    xamax = a + hmax;
    xbmax = b + hmax * (a / b);
    xamin = a + hmin * (b / a);
    xbmin = b + hmin;
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("AMAX", &amax, "~", &xamax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMAX", &bmax, "~", &xbmax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("AMIN", &amin, "~", &xamin, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMIN", &bmin, "~", &xbmin, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     Sample heights from the bounding ellipsoids. Verify that */
/*     the heights satisfy the expected conditions. */

    n = 10001;
    delta = pi_() * 2 / (n - 1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the outer bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amax - bmax) / amax;
	georec_(&xlon, &xlat, &c_b37, &amax, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no smaller than HMAX. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt < hmax) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmax - 1e-13;
	    chcksd_(casnam, &alt, ">=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the inner bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amin - bmin) / amin;
	georec_(&xlon, &xlat, &c_b37, &amin, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no greater than HMIN. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt > hmin) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmin + 1e-13;
	    chcksd_(casnam, &alt, "<=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Normal case: HMAX < 0, HMIN < 0", (ftnlen)31);
    a = 25.;
    b = 20.;
    hmax = -1.;
    hmin = -2.;
    xamax = a + hmax * (b / a);
    xbmax = b + hmax;
    xamin = a + hmin;
    xbmin = b + hmin * (a / b);
    zzellbds_(&a, &b, &hmax, &hmin, &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("AMAX", &amax, "~", &xamax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMAX", &bmax, "~", &xbmax, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("AMIN", &amin, "~", &xamin, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("BMIN", &bmin, "~", &xbmin, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     Sample heights from the bounding ellipsoids. Verify that */
/*     the heights satisfy the expected conditions. */

    n = 10001;
    delta = pi_() * 2 / (n - 1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the outer bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amax - bmax) / amax;
	georec_(&xlon, &xlat, &c_b37, &amax, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no smaller than HMAX. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt < hmax) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmax - 1e-13;
	    chcksd_(casnam, &alt, ">=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xlat = (i__ - 1) * delta;
	xlon = pi_() / 3;

/*        Generate the cartesian coordinates of a point P on */
/*        the inner bounding ellipsoid at geodetic latitude LAT. */

	d__1 = (amin - bmin) / amin;
	georec_(&xlon, &xlat, &c_b37, &amin, &d__1, p);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the altitude of P relative to the spheroid with */
/*        radii A, B. The altitude should be no greater than HMIN. */

	d__1 = (a - b) / a;
	recgeo_(p, &a, &d__1, &lon, &lat, &alt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (alt > hmin) {
	    s_copy(casnam, "ALT for I = #", (ftnlen)80, (ftnlen)13);
	    repmi_(casnam, "#", &i__, casnam, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = hmin + 1e-13;
	    chcksd_(casnam, &alt, "<=", &d__1, &c_b37, ok, (ftnlen)80, (
		    ftnlen)2);
	}
    }
    t_success__(ok);
    return 0;
} /* f_zzellbds__ */

