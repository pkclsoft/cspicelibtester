/* f_zzelnaxx.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;

/* $Procedure F_ZZELNAXX ( ZZELNAXX tests ) */
/* Subroutine */ int f_zzelnaxx__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    double tan(doublereal), sqrt(doublereal), cos(doublereal), sin(doublereal)
	    ;

    /* Local variables */
    static doublereal xxpt, yxpt, a, b;
    extern /* Subroutine */ int zzelnaxx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal m, t, x, y;
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    , t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    static doublereal xx, xy, lat, eps, tol;

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZELNAXX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZELNAXX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 19-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZELNAXX", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT = pi/6", (ftnlen)23);
    a = 2.;
    b = 1.;
    lat = pi_() / 6;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     M is the slope of the normal vector at (x,y). */
/*     This vector is the gradient of */

/*                   2   2      2 */
/*        f(x,y) =  x / a   +  y / b */


    m = tan(lat);

/*     Let T = (y/x) (the tangent of the latitudinal latitude) */

/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    t = d__1 * d__1 / (d__2 * d__2) * m;

/*     Since */

/*        y = T*x */

/*     we have */

/*         2   2       2 2   2 */
/*        x / a    +  T x / b    =   1 */

/*     So */

/*         2        2     2   2  -1       2    2 2 */
/*        x  = ( 1/a  +  T / b  )  ,     y  = T x */

/*     Both x, y are >= 0. */


/*     XX is the expected X-axis intercept, which is */

/*                 2 2 */
/*         ( 1 - (b/a ) ) * x */


/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = t;
/* Computing 2nd power */
    d__3 = b;
    x = sqrt(1. / (1 / (d__1 * d__1) + d__2 * d__2 / (d__3 * d__3)));
/* Computing 2nd power */
    d__1 = t;
/* Computing 2nd power */
    d__2 = x;
    y = sqrt(d__1 * d__1 * (d__2 * d__2));
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);

/*     XY is the expected Y-axis intercept, which is */

/*                2   2 */
/*        ( 1 - (a / b ) ) y */

/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT = pi/3", (ftnlen)23);
    a = 2.;
    b = 1.;
    lat = pi_() / 3;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     See the pi/6 case for a discussion of what follows. */

    m = tan(lat);

/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    t = d__1 * d__1 / (d__2 * d__2) * m;
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = t;
/* Computing 2nd power */
    d__3 = b;
    x = sqrt(1. / (1 / (d__1 * d__1) + d__2 * d__2 / (d__3 * d__3)));
/* Computing 2nd power */
    d__1 = t;
/* Computing 2nd power */
    d__2 = x;
    y = sqrt(d__1 * d__1 * (d__2 * d__2));
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT << 1", (ftnlen)21);
    a = 2.;
    b = 1.;
    lat = 1e-13;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     See the pi/3 case for a discussion of what follows. */

    m = tan(lat);

/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    t = d__1 * d__1 / (d__2 * d__2) * m;
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = t;
/* Computing 2nd power */
    d__3 = b;
    x = sqrt(1. / (1 / (d__1 * d__1) + d__2 * d__2 / (d__3 * d__3)));
/* Computing 2nd power */
    d__1 = t;
/* Computing 2nd power */
    d__2 = x;
    y = sqrt(d__1 * d__1 * (d__2 * d__2));
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT = 0", (ftnlen)20);
    a = 2.;
    b = 1.;
    lat = 0.;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     See the pi/3 case for a discussion of what follows. */

    m = tan(lat);

/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    t = d__1 * d__1 / (d__2 * d__2) * m;
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = t;
/* Computing 2nd power */
    d__3 = b;
    x = sqrt(1. / (1 / (d__1 * d__1) + d__2 * d__2 / (d__3 * d__3)));
/* Computing 2nd power */
    d__1 = t;
/* Computing 2nd power */
    d__2 = x;
    y = sqrt(d__1 * d__1 * (d__2 * d__2));
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT = pi/2", (ftnlen)23);
    a = 2.;
    b = 1.;
    lat = pi_() / 2;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     See the pi/3 case for a discussion of what follows. */

    x = 0.;
    y = b;
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case, LAT = (pi/2) - EPS, EPS << 1", (ftnlen)41);
    a = 2.;
    b = 1.;
    eps = 1e-13;
    lat = pi_() / 2 - eps;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     In principle, tan(LAT) can be very large. We want to */
/*     work with cot(LAT) and x/y instead. Let T = x/y. */

/*                     2   2 */
/*        tan(LAT) = (a / b ) * y/x */

/*     so */

/*              2   2 */
/*        T = (a / b ) * cot(LAT) */


/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    t = d__1 * d__1 / (d__2 * d__2) * (cos(lat) / sin(lat));

/*     x = T*y, so */

/*              2        2 */
/*        (Ty/a)  + (y/b)  = 1 */

/*     Then */

/*          2      2  2       2  -1 */
/*         y  = ( T /a  +  1/b  ) */



/* Computing 2nd power */
    d__1 = t;
/* Computing 2nd power */
    d__2 = a;
/* Computing 2nd power */
    d__3 = b;
    y = sqrt(1. / (d__1 * d__1 / (d__2 * d__2) + 1. / (d__3 * d__3)));
    x = t * y;
/* Computing 2nd power */
    d__1 = b;
/* Computing 2nd power */
    d__2 = a;
    xx = (1. - d__1 * d__1 / (d__2 * d__2)) * x;
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
/* Computing 2nd power */
    d__1 = a;
/* Computing 2nd power */
    d__2 = b;
    xy = (1. - d__1 * d__1 / (d__2 * d__2)) * y;
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT = pi/6", (ftnlen)24);
    a = 1.;
    b = 2.;
    lat = pi_() / 6;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The prolate geometry is just a reflection of an oblate case */
/*     in which the latitude is complemented and the axis lengths */
/*     are swapped. The X intercept in the reflected case is the */
/*     Y intercept of the original case; the relation is the same */
/*     for the other intercepts. */

    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT = pi/3", (ftnlen)24);
    a = 1.;
    b = 2.;
    lat = pi_() / 3;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT << 1", (ftnlen)22);
    a = 1.;
    b = 2.;
    eps = 1e-13;
    lat = eps;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT = 0", (ftnlen)21);
    a = 1.;
    b = 2.;
    lat = 0.;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT = pi/2", (ftnlen)24);
    a = 1.;
    b = 2.;
    lat = pi_() / 2;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case, LAT = (pi/2) - EPS, EPS << 1", (ftnlen)42);
    a = 1.;
    b = 2.;
    eps = 1e-13;
    lat = pi_() / 2 - eps;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 2 - lat;
    zzelnaxx_(&b, &a, &d__1, &xy, &xx);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("X intercept", &xxpt, "~", &xx, &tol, ok, (ftnlen)11, (ftnlen)1);
    chcksd_("Y intercept", &yxpt, "~", &xy, &tol, ok, (ftnlen)11, (ftnlen)1);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid axis length.", (ftnlen)20);
    a = 0.;
    b = 1.;
    lat = pi_() / 2;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_true, "SPICE(NONPOSITIVEAXIS)", ok, (ftnlen)22);
    a = -1.;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_true, "SPICE(NONPOSITIVEAXIS)", ok, (ftnlen)22);
    a = 1.;
    b = 0.;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_true, "SPICE(NONPOSITIVEAXIS)", ok, (ftnlen)22);
    b = -1.;
    zzelnaxx_(&a, &b, &lat, &xxpt, &yxpt);
    chckxc_(&c_true, "SPICE(NONPOSITIVEAXIS)", ok, (ftnlen)22);

/* --------------------------------------------------------- */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzelnaxx__ */

