/* f_zzfdat.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__0 = 0;
static logical c_true = TRUE_;

/* $Procedure F_ZZFDAT ( ZZFDAT Test Family ) */
/* Subroutine */ int f_zzfdat__(logical *ok)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3[2], i__4;
    char ch__1[51];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    integer i__, bcode;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern integer rtrim_(char *, ftnlen);
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *), chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen);
    integer atcode[127], frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    char atname[32*127];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    char frname[32];
    integer atcent[127];
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen);
    integer frcent;
    extern /* Subroutine */ int frmnam_(integer *, char *, ftnlen), frinfo_(
	    integer *, integer *, integer *, integer *, logical *);
    integer ntochk, atclss[127], frclss, attype[127], frtype;

/* $ Abstract */

/*     Test family to exercise the logic and code in the ZZFDAT */
/*     routine. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     The parameters below form an enumerated list of the recognized */
/*     frame types.  They are: INERTL, PCK, CK, TK, DYN.  The meanings */
/*     are outlined below. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     INERTL      an inertial frame that is listed in the routine */
/*                 CHGIRF and that requires no external file to */
/*                 compute the transformation from or to any other */
/*                 inertial frame. */

/*     PCK         is a frame that is specified relative to some */
/*                 INERTL frame and that has an IAU model that */
/*                 may be retrieved from the PCK system via a call */
/*                 to the routine TISBOD. */

/*     CK          is a frame defined by a C-kernel. */

/*     TK          is a "text kernel" frame.  These frames are offset */
/*                 from their associated "relative" frames by a */
/*                 constant rotation. */

/*     DYN         is a "dynamic" frame.  These currently are */
/*                 parameterized, built-in frames where the full frame */
/*                 definition depends on parameters supplied via a */
/*                 frame kernel. */

/*     ALL         indicates any of the above classes. This parameter */
/*                 is used in APIs that fetch information about frames */
/*                 of a specified class. */


/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 4.0.0, 08-MAY-2012 (NJB) */

/*       The parameter ALL was added to support frame fetch APIs. */

/* -    SPICELIB Version 3.0.0, 28-MAY-2004 (NJB) */

/*       The parameter DYN was added to support the dynamic frame class. */

/* -    SPICELIB Version 2.0.0, 12-DEC-1996 (WLT) */

/*        Various unused frames types were removed and the */
/*        frame time TK was added. */

/* -    SPICELIB Version 1.0.0, 10-DEC-1995 (WLT) */

/* -& */

/*     End of INCLUDE file frmtyp.inc */

/* $ Abstract */

/*     This file contains the number of inertial reference */
/*     frames that are currently known by the SPICE toolkit */
/*     software. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of known inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of recognized inertial reference */
/*                frames.  This value is needed by both CHGIRF */
/*                ZZFDAT, and FRAMEX. */

/* $ Author_and_Institution */

/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Abstract */

/*     This file contains the number of non-inertial reference */
/*     frames that are currently built into the SPICE toolkit */
/*     software. */


/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     FRAMES */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     NINERT     P   Number of built-in non-inertial reference frames. */

/* $ Parameters */

/*     NINERT     is the number of built-in non-inertial reference */
/*                frames.  This value is needed by both  ZZFDAT, and */
/*                FRAMEX. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     W.L. Taber      (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.6.0, 30-OCT-2014 (BVS) */

/*        Increased the number of non-inertial frames from 105 to 106 */
/*        in order to accomodate the following PCK based frame: */

/*           IAU_BENNU */

/* -    SPICELIB Version 1.5.0, 11-OCT-2011 (BVS) */

/*        Increased the number of non-inertial frames from 100 to 105 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CERES */
/*           IAU_PALLAS */
/*           IAU_LUTETIA */
/*           IAU_DAVIDA */
/*           IAU_STEINS */

/* -    SPICELIB Version 1.4.0, 11-MAY-2010 (BVS) */

/*        Increased the number of non-inertial frames from 96 to 100 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_BORRELLY */
/*           IAU_TEMPEL_1 */
/*           IAU_VESTA */
/*           IAU_ITOKAWA */

/* -    SPICELIB Version 1.3.0, 12-DEC-2002 (BVS) */

/*        Increased the number of non-inertial frames from 85 to 96 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_CALLIRRHOE */
/*           IAU_THEMISTO */
/*           IAU_MAGACLITE */
/*           IAU_TAYGETE */
/*           IAU_CHALDENE */
/*           IAU_HARPALYKE */
/*           IAU_KALYKE */
/*           IAU_IOCASTE */
/*           IAU_ERINOME */
/*           IAU_ISONOE */
/*           IAU_PRAXIDIKE */

/* -    SPICELIB Version 1.2.0, 02-AUG-2002 (FST) */

/*        Increased the number of non-inertial frames from 81 to 85 */
/*        in order to accomodate the following PCK based frames: */

/*           IAU_PAN */
/*           IAU_GASPRA */
/*           IAU_IDA */
/*           IAU_EROS */

/* -    SPICELIB Version 1.1.0, 20-FEB-1997 (WLT) */

/*        Increased the number of non-inertial frames from 79 to 81 */
/*        in order to accomodate the following earth rotation */
/*        models: */

/*           ITRF93 */
/*           EARTH_FIXED */

/* -    SPICELIB Version 1.0.0, 10-OCT-1996 (WLT) */

/* -& */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine exercises updates to ZZFDAT through the FRAMEX */
/*     query interfaces. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.4.0, 30-OCT-2014 (BVS) */

/*        Updated for IAU_BENNU. */

/* -    TSPICE Version 1.3.0, 12-SEP-2011 (BVS) */

/*        Updated to check all frames defined in ZZFDAT (as of N0065). */

/* -    TSPICE Version 1.2.0, 11-MAY-2010 (BVS) */

/*        Updated for */

/*           IAU_BORRELLY */
/*           IAU_TEMPEL_1 */
/*           IAU_VESTA */
/*           IAU_ITOKAWA */

/*        frames added in N0064. */

/* -    TSPICE Version 1.1.0, 12-DEC-2002 (FST) */

/*        Updated to check for correct PAN frame value 618. Added */
/*        checks for new Jovian satellite frames. */

/* -    TSPICE Version 1.0.0, 02-AUG-2002 (FST) */

/* -& */

/*     Local Parameters */


/*     Local Variables */


/*     SPICELIB functions */


/*     Start the test family with an open call. */

    topen_("F_ZZFDAT", (ftnlen)8);

/*     Set expected values. */

    s_copy(atname, "IAU_MERCURY_BARYCENTER", (ftnlen)32, (ftnlen)22);
    atcode[0] = 10001;
    atcent[0] = 1;
    atclss[0] = 1;
    attype[0] = 2;
    s_copy(atname + 32, "IAU_VENUS_BARYCENTER", (ftnlen)32, (ftnlen)20);
    atcode[1] = 10002;
    atcent[1] = 2;
    atclss[1] = 2;
    attype[1] = 2;
    s_copy(atname + 64, "IAU_EARTH_BARYCENTER", (ftnlen)32, (ftnlen)20);
    atcode[2] = 10003;
    atcent[2] = 3;
    atclss[2] = 3;
    attype[2] = 2;
    s_copy(atname + 96, "IAU_MARS_BARYCENTER", (ftnlen)32, (ftnlen)19);
    atcode[3] = 10004;
    atcent[3] = 4;
    atclss[3] = 4;
    attype[3] = 2;
    s_copy(atname + 128, "IAU_JUPITER_BARYCENTER", (ftnlen)32, (ftnlen)22);
    atcode[4] = 10005;
    atcent[4] = 5;
    atclss[4] = 5;
    attype[4] = 2;
    s_copy(atname + 160, "IAU_SATURN_BARYCENTER", (ftnlen)32, (ftnlen)21);
    atcode[5] = 10006;
    atcent[5] = 6;
    atclss[5] = 6;
    attype[5] = 2;
    s_copy(atname + 192, "IAU_URANUS_BARYCENTER", (ftnlen)32, (ftnlen)21);
    atcode[6] = 10007;
    atcent[6] = 7;
    atclss[6] = 7;
    attype[6] = 2;
    s_copy(atname + 224, "IAU_NEPTUNE_BARYCENTER", (ftnlen)32, (ftnlen)22);
    atcode[7] = 10008;
    atcent[7] = 8;
    atclss[7] = 8;
    attype[7] = 2;
    s_copy(atname + 256, "IAU_PLUTO_BARYCENTER", (ftnlen)32, (ftnlen)20);
    atcode[8] = 10009;
    atcent[8] = 9;
    atclss[8] = 9;
    attype[8] = 2;
    s_copy(atname + 288, "IAU_SUN", (ftnlen)32, (ftnlen)7);
    atcode[9] = 10010;
    atcent[9] = 10;
    atclss[9] = 10;
    attype[9] = 2;
    s_copy(atname + 320, "IAU_MERCURY", (ftnlen)32, (ftnlen)11);
    atcode[10] = 10011;
    atcent[10] = 199;
    atclss[10] = 199;
    attype[10] = 2;
    s_copy(atname + 352, "IAU_VENUS", (ftnlen)32, (ftnlen)9);
    atcode[11] = 10012;
    atcent[11] = 299;
    atclss[11] = 299;
    attype[11] = 2;
    s_copy(atname + 384, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    atcode[12] = 10013;
    atcent[12] = 399;
    atclss[12] = 399;
    attype[12] = 2;
    s_copy(atname + 416, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    atcode[13] = 10014;
    atcent[13] = 499;
    atclss[13] = 499;
    attype[13] = 2;
    s_copy(atname + 448, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    atcode[14] = 10015;
    atcent[14] = 599;
    atclss[14] = 599;
    attype[14] = 2;
    s_copy(atname + 480, "IAU_SATURN", (ftnlen)32, (ftnlen)10);
    atcode[15] = 10016;
    atcent[15] = 699;
    atclss[15] = 699;
    attype[15] = 2;
    s_copy(atname + 512, "IAU_URANUS", (ftnlen)32, (ftnlen)10);
    atcode[16] = 10017;
    atcent[16] = 799;
    atclss[16] = 799;
    attype[16] = 2;
    s_copy(atname + 544, "IAU_NEPTUNE", (ftnlen)32, (ftnlen)11);
    atcode[17] = 10018;
    atcent[17] = 899;
    atclss[17] = 899;
    attype[17] = 2;
    s_copy(atname + 576, "IAU_PLUTO", (ftnlen)32, (ftnlen)9);
    atcode[18] = 10019;
    atcent[18] = 999;
    atclss[18] = 999;
    attype[18] = 2;
    s_copy(atname + 608, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    atcode[19] = 10020;
    atcent[19] = 301;
    atclss[19] = 301;
    attype[19] = 2;
    s_copy(atname + 640, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    atcode[20] = 10021;
    atcent[20] = 401;
    atclss[20] = 401;
    attype[20] = 2;
    s_copy(atname + 672, "IAU_DEIMOS", (ftnlen)32, (ftnlen)10);
    atcode[21] = 10022;
    atcent[21] = 402;
    atclss[21] = 402;
    attype[21] = 2;
    s_copy(atname + 704, "IAU_IO", (ftnlen)32, (ftnlen)6);
    atcode[22] = 10023;
    atcent[22] = 501;
    atclss[22] = 501;
    attype[22] = 2;
    s_copy(atname + 736, "IAU_EUROPA", (ftnlen)32, (ftnlen)10);
    atcode[23] = 10024;
    atcent[23] = 502;
    atclss[23] = 502;
    attype[23] = 2;
    s_copy(atname + 768, "IAU_GANYMEDE", (ftnlen)32, (ftnlen)12);
    atcode[24] = 10025;
    atcent[24] = 503;
    atclss[24] = 503;
    attype[24] = 2;
    s_copy(atname + 800, "IAU_CALLISTO", (ftnlen)32, (ftnlen)12);
    atcode[25] = 10026;
    atcent[25] = 504;
    atclss[25] = 504;
    attype[25] = 2;
    s_copy(atname + 832, "IAU_AMALTHEA", (ftnlen)32, (ftnlen)12);
    atcode[26] = 10027;
    atcent[26] = 505;
    atclss[26] = 505;
    attype[26] = 2;
    s_copy(atname + 864, "IAU_HIMALIA", (ftnlen)32, (ftnlen)11);
    atcode[27] = 10028;
    atcent[27] = 506;
    atclss[27] = 506;
    attype[27] = 2;
    s_copy(atname + 896, "IAU_ELARA", (ftnlen)32, (ftnlen)9);
    atcode[28] = 10029;
    atcent[28] = 507;
    atclss[28] = 507;
    attype[28] = 2;
    s_copy(atname + 928, "IAU_PASIPHAE", (ftnlen)32, (ftnlen)12);
    atcode[29] = 10030;
    atcent[29] = 508;
    atclss[29] = 508;
    attype[29] = 2;
    s_copy(atname + 960, "IAU_SINOPE", (ftnlen)32, (ftnlen)10);
    atcode[30] = 10031;
    atcent[30] = 509;
    atclss[30] = 509;
    attype[30] = 2;
    s_copy(atname + 992, "IAU_LYSITHEA", (ftnlen)32, (ftnlen)12);
    atcode[31] = 10032;
    atcent[31] = 510;
    atclss[31] = 510;
    attype[31] = 2;
    s_copy(atname + 1024, "IAU_CARME", (ftnlen)32, (ftnlen)9);
    atcode[32] = 10033;
    atcent[32] = 511;
    atclss[32] = 511;
    attype[32] = 2;
    s_copy(atname + 1056, "IAU_ANANKE", (ftnlen)32, (ftnlen)10);
    atcode[33] = 10034;
    atcent[33] = 512;
    atclss[33] = 512;
    attype[33] = 2;
    s_copy(atname + 1088, "IAU_LEDA", (ftnlen)32, (ftnlen)8);
    atcode[34] = 10035;
    atcent[34] = 513;
    atclss[34] = 513;
    attype[34] = 2;
    s_copy(atname + 1120, "IAU_THEBE", (ftnlen)32, (ftnlen)9);
    atcode[35] = 10036;
    atcent[35] = 514;
    atclss[35] = 514;
    attype[35] = 2;
    s_copy(atname + 1152, "IAU_ADRASTEA", (ftnlen)32, (ftnlen)12);
    atcode[36] = 10037;
    atcent[36] = 515;
    atclss[36] = 515;
    attype[36] = 2;
    s_copy(atname + 1184, "IAU_METIS", (ftnlen)32, (ftnlen)9);
    atcode[37] = 10038;
    atcent[37] = 516;
    atclss[37] = 516;
    attype[37] = 2;
    s_copy(atname + 1216, "IAU_MIMAS", (ftnlen)32, (ftnlen)9);
    atcode[38] = 10039;
    atcent[38] = 601;
    atclss[38] = 601;
    attype[38] = 2;
    s_copy(atname + 1248, "IAU_ENCELADUS", (ftnlen)32, (ftnlen)13);
    atcode[39] = 10040;
    atcent[39] = 602;
    atclss[39] = 602;
    attype[39] = 2;
    s_copy(atname + 1280, "IAU_TETHYS", (ftnlen)32, (ftnlen)10);
    atcode[40] = 10041;
    atcent[40] = 603;
    atclss[40] = 603;
    attype[40] = 2;
    s_copy(atname + 1312, "IAU_DIONE", (ftnlen)32, (ftnlen)9);
    atcode[41] = 10042;
    atcent[41] = 604;
    atclss[41] = 604;
    attype[41] = 2;
    s_copy(atname + 1344, "IAU_RHEA", (ftnlen)32, (ftnlen)8);
    atcode[42] = 10043;
    atcent[42] = 605;
    atclss[42] = 605;
    attype[42] = 2;
    s_copy(atname + 1376, "IAU_TITAN", (ftnlen)32, (ftnlen)9);
    atcode[43] = 10044;
    atcent[43] = 606;
    atclss[43] = 606;
    attype[43] = 2;
    s_copy(atname + 1408, "IAU_HYPERION", (ftnlen)32, (ftnlen)12);
    atcode[44] = 10045;
    atcent[44] = 607;
    atclss[44] = 607;
    attype[44] = 2;
    s_copy(atname + 1440, "IAU_IAPETUS", (ftnlen)32, (ftnlen)11);
    atcode[45] = 10046;
    atcent[45] = 608;
    atclss[45] = 608;
    attype[45] = 2;
    s_copy(atname + 1472, "IAU_PHOEBE", (ftnlen)32, (ftnlen)10);
    atcode[46] = 10047;
    atcent[46] = 609;
    atclss[46] = 609;
    attype[46] = 2;
    s_copy(atname + 1504, "IAU_JANUS", (ftnlen)32, (ftnlen)9);
    atcode[47] = 10048;
    atcent[47] = 610;
    atclss[47] = 610;
    attype[47] = 2;
    s_copy(atname + 1536, "IAU_EPIMETHEUS", (ftnlen)32, (ftnlen)14);
    atcode[48] = 10049;
    atcent[48] = 611;
    atclss[48] = 611;
    attype[48] = 2;
    s_copy(atname + 1568, "IAU_HELENE", (ftnlen)32, (ftnlen)10);
    atcode[49] = 10050;
    atcent[49] = 612;
    atclss[49] = 612;
    attype[49] = 2;
    s_copy(atname + 1600, "IAU_TELESTO", (ftnlen)32, (ftnlen)11);
    atcode[50] = 10051;
    atcent[50] = 613;
    atclss[50] = 613;
    attype[50] = 2;
    s_copy(atname + 1632, "IAU_CALYPSO", (ftnlen)32, (ftnlen)11);
    atcode[51] = 10052;
    atcent[51] = 614;
    atclss[51] = 614;
    attype[51] = 2;
    s_copy(atname + 1664, "IAU_ATLAS", (ftnlen)32, (ftnlen)9);
    atcode[52] = 10053;
    atcent[52] = 615;
    atclss[52] = 615;
    attype[52] = 2;
    s_copy(atname + 1696, "IAU_PROMETHEUS", (ftnlen)32, (ftnlen)14);
    atcode[53] = 10054;
    atcent[53] = 616;
    atclss[53] = 616;
    attype[53] = 2;
    s_copy(atname + 1728, "IAU_PANDORA", (ftnlen)32, (ftnlen)11);
    atcode[54] = 10055;
    atcent[54] = 617;
    atclss[54] = 617;
    attype[54] = 2;
    s_copy(atname + 1760, "IAU_ARIEL", (ftnlen)32, (ftnlen)9);
    atcode[55] = 10056;
    atcent[55] = 701;
    atclss[55] = 701;
    attype[55] = 2;
    s_copy(atname + 1792, "IAU_UMBRIEL", (ftnlen)32, (ftnlen)11);
    atcode[56] = 10057;
    atcent[56] = 702;
    atclss[56] = 702;
    attype[56] = 2;
    s_copy(atname + 1824, "IAU_TITANIA", (ftnlen)32, (ftnlen)11);
    atcode[57] = 10058;
    atcent[57] = 703;
    atclss[57] = 703;
    attype[57] = 2;
    s_copy(atname + 1856, "IAU_OBERON", (ftnlen)32, (ftnlen)10);
    atcode[58] = 10059;
    atcent[58] = 704;
    atclss[58] = 704;
    attype[58] = 2;
    s_copy(atname + 1888, "IAU_MIRANDA", (ftnlen)32, (ftnlen)11);
    atcode[59] = 10060;
    atcent[59] = 705;
    atclss[59] = 705;
    attype[59] = 2;
    s_copy(atname + 1920, "IAU_CORDELIA", (ftnlen)32, (ftnlen)12);
    atcode[60] = 10061;
    atcent[60] = 706;
    atclss[60] = 706;
    attype[60] = 2;
    s_copy(atname + 1952, "IAU_OPHELIA", (ftnlen)32, (ftnlen)11);
    atcode[61] = 10062;
    atcent[61] = 707;
    atclss[61] = 707;
    attype[61] = 2;
    s_copy(atname + 1984, "IAU_BIANCA", (ftnlen)32, (ftnlen)10);
    atcode[62] = 10063;
    atcent[62] = 708;
    atclss[62] = 708;
    attype[62] = 2;
    s_copy(atname + 2016, "IAU_CRESSIDA", (ftnlen)32, (ftnlen)12);
    atcode[63] = 10064;
    atcent[63] = 709;
    atclss[63] = 709;
    attype[63] = 2;
    s_copy(atname + 2048, "IAU_DESDEMONA", (ftnlen)32, (ftnlen)13);
    atcode[64] = 10065;
    atcent[64] = 710;
    atclss[64] = 710;
    attype[64] = 2;
    s_copy(atname + 2080, "IAU_JULIET", (ftnlen)32, (ftnlen)10);
    atcode[65] = 10066;
    atcent[65] = 711;
    atclss[65] = 711;
    attype[65] = 2;
    s_copy(atname + 2112, "IAU_PORTIA", (ftnlen)32, (ftnlen)10);
    atcode[66] = 10067;
    atcent[66] = 712;
    atclss[66] = 712;
    attype[66] = 2;
    s_copy(atname + 2144, "IAU_ROSALIND", (ftnlen)32, (ftnlen)12);
    atcode[67] = 10068;
    atcent[67] = 713;
    atclss[67] = 713;
    attype[67] = 2;
    s_copy(atname + 2176, "IAU_BELINDA", (ftnlen)32, (ftnlen)11);
    atcode[68] = 10069;
    atcent[68] = 714;
    atclss[68] = 714;
    attype[68] = 2;
    s_copy(atname + 2208, "IAU_PUCK", (ftnlen)32, (ftnlen)8);
    atcode[69] = 10070;
    atcent[69] = 715;
    atclss[69] = 715;
    attype[69] = 2;
    s_copy(atname + 2240, "IAU_TRITON", (ftnlen)32, (ftnlen)10);
    atcode[70] = 10071;
    atcent[70] = 801;
    atclss[70] = 801;
    attype[70] = 2;
    s_copy(atname + 2272, "IAU_NEREID", (ftnlen)32, (ftnlen)10);
    atcode[71] = 10072;
    atcent[71] = 802;
    atclss[71] = 802;
    attype[71] = 2;
    s_copy(atname + 2304, "IAU_NAIAD", (ftnlen)32, (ftnlen)9);
    atcode[72] = 10073;
    atcent[72] = 803;
    atclss[72] = 803;
    attype[72] = 2;
    s_copy(atname + 2336, "IAU_THALASSA", (ftnlen)32, (ftnlen)12);
    atcode[73] = 10074;
    atcent[73] = 804;
    atclss[73] = 804;
    attype[73] = 2;
    s_copy(atname + 2368, "IAU_DESPINA", (ftnlen)32, (ftnlen)11);
    atcode[74] = 10075;
    atcent[74] = 805;
    atclss[74] = 805;
    attype[74] = 2;
    s_copy(atname + 2400, "IAU_GALATEA", (ftnlen)32, (ftnlen)11);
    atcode[75] = 10076;
    atcent[75] = 806;
    atclss[75] = 806;
    attype[75] = 2;
    s_copy(atname + 2432, "IAU_LARISSA", (ftnlen)32, (ftnlen)11);
    atcode[76] = 10077;
    atcent[76] = 807;
    atclss[76] = 807;
    attype[76] = 2;
    s_copy(atname + 2464, "IAU_PROTEUS", (ftnlen)32, (ftnlen)11);
    atcode[77] = 10078;
    atcent[77] = 808;
    atclss[77] = 808;
    attype[77] = 2;
    s_copy(atname + 2496, "IAU_CHARON", (ftnlen)32, (ftnlen)10);
    atcode[78] = 10079;
    atcent[78] = 901;
    atclss[78] = 901;
    attype[78] = 2;
    s_copy(atname + 2528, "ITRF93", (ftnlen)32, (ftnlen)6);
    atcode[79] = 13000;
    atcent[79] = 399;
    atclss[79] = 3000;
    attype[79] = 2;
    s_copy(atname + 2560, "EARTH_FIXED", (ftnlen)32, (ftnlen)11);
    atcode[80] = 10081;
    atcent[80] = 399;
    atclss[80] = 10081;
    attype[80] = 4;
    s_copy(atname + 2592, "IAU_PAN", (ftnlen)32, (ftnlen)7);
    atcode[81] = 10082;
    atcent[81] = 618;
    atclss[81] = 618;
    attype[81] = 2;
    s_copy(atname + 2624, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    atcode[82] = 10083;
    atcent[82] = 9511010;
    atclss[82] = 9511010;
    attype[82] = 2;
    s_copy(atname + 2656, "IAU_IDA", (ftnlen)32, (ftnlen)7);
    atcode[83] = 10084;
    atcent[83] = 2431010;
    atclss[83] = 2431010;
    attype[83] = 2;
    s_copy(atname + 2688, "IAU_EROS", (ftnlen)32, (ftnlen)8);
    atcode[84] = 10085;
    atcent[84] = 2000433;
    atclss[84] = 2000433;
    attype[84] = 2;
    s_copy(atname + 2720, "IAU_CALLIRRHOE", (ftnlen)32, (ftnlen)14);
    atcode[85] = 10086;
    atcent[85] = 517;
    atclss[85] = 517;
    attype[85] = 2;
    s_copy(atname + 2752, "IAU_THEMISTO", (ftnlen)32, (ftnlen)12);
    atcode[86] = 10087;
    atcent[86] = 518;
    atclss[86] = 518;
    attype[86] = 2;
    s_copy(atname + 2784, "IAU_MAGACLITE", (ftnlen)32, (ftnlen)13);
    atcode[87] = 10088;
    atcent[87] = 519;
    atclss[87] = 519;
    attype[87] = 2;
    s_copy(atname + 2816, "IAU_TAYGETE", (ftnlen)32, (ftnlen)11);
    atcode[88] = 10089;
    atcent[88] = 520;
    atclss[88] = 520;
    attype[88] = 2;
    s_copy(atname + 2848, "IAU_CHALDENE", (ftnlen)32, (ftnlen)12);
    atcode[89] = 10090;
    atcent[89] = 521;
    atclss[89] = 521;
    attype[89] = 2;
    s_copy(atname + 2880, "IAU_HARPALYKE", (ftnlen)32, (ftnlen)13);
    atcode[90] = 10091;
    atcent[90] = 522;
    atclss[90] = 522;
    attype[90] = 2;
    s_copy(atname + 2912, "IAU_KALYKE", (ftnlen)32, (ftnlen)10);
    atcode[91] = 10092;
    atcent[91] = 523;
    atclss[91] = 523;
    attype[91] = 2;
    s_copy(atname + 2944, "IAU_IOCASTE", (ftnlen)32, (ftnlen)11);
    atcode[92] = 10093;
    atcent[92] = 524;
    atclss[92] = 524;
    attype[92] = 2;
    s_copy(atname + 2976, "IAU_ERINOME", (ftnlen)32, (ftnlen)11);
    atcode[93] = 10094;
    atcent[93] = 525;
    atclss[93] = 525;
    attype[93] = 2;
    s_copy(atname + 3008, "IAU_ISONOE", (ftnlen)32, (ftnlen)10);
    atcode[94] = 10095;
    atcent[94] = 526;
    atclss[94] = 526;
    attype[94] = 2;
    s_copy(atname + 3040, "IAU_PRAXIDIKE", (ftnlen)32, (ftnlen)13);
    atcode[95] = 10096;
    atcent[95] = 527;
    atclss[95] = 527;
    attype[95] = 2;
    s_copy(atname + 3072, "IAU_BORRELLY", (ftnlen)32, (ftnlen)12);
    atcode[96] = 10097;
    atcent[96] = 1000005;
    atclss[96] = 1000005;
    attype[96] = 2;
    s_copy(atname + 3104, "IAU_TEMPEL_1", (ftnlen)32, (ftnlen)12);
    atcode[97] = 10098;
    atcent[97] = 1000093;
    atclss[97] = 1000093;
    attype[97] = 2;
    s_copy(atname + 3136, "IAU_VESTA", (ftnlen)32, (ftnlen)9);
    atcode[98] = 10099;
    atcent[98] = 2000004;
    atclss[98] = 2000004;
    attype[98] = 2;
    s_copy(atname + 3168, "IAU_ITOKAWA", (ftnlen)32, (ftnlen)11);
    atcode[99] = 10100;
    atcent[99] = 2025143;
    atclss[99] = 2025143;
    attype[99] = 2;
    s_copy(atname + 3200, "IAU_CERES", (ftnlen)32, (ftnlen)9);
    atcode[100] = 10101;
    atcent[100] = 2000001;
    atclss[100] = 2000001;
    attype[100] = 2;
    s_copy(atname + 3232, "IAU_PALLAS", (ftnlen)32, (ftnlen)10);
    atcode[101] = 10102;
    atcent[101] = 2000002;
    atclss[101] = 2000002;
    attype[101] = 2;
    s_copy(atname + 3264, "IAU_LUTETIA", (ftnlen)32, (ftnlen)11);
    atcode[102] = 10103;
    atcent[102] = 2000021;
    atclss[102] = 2000021;
    attype[102] = 2;
    s_copy(atname + 3296, "IAU_DAVIDA", (ftnlen)32, (ftnlen)10);
    atcode[103] = 10104;
    atcent[103] = 2000511;
    atclss[103] = 2000511;
    attype[103] = 2;
    s_copy(atname + 3328, "IAU_STEINS", (ftnlen)32, (ftnlen)10);
    atcode[104] = 10105;
    atcent[104] = 2002867;
    atclss[104] = 2002867;
    attype[104] = 2;
    s_copy(atname + 3360, "IAU_BENNU", (ftnlen)32, (ftnlen)9);
    atcode[105] = 10106;
    atcent[105] = 2101955;
    atclss[105] = 2101955;
    attype[105] = 2;
    ntochk = 106;

/*     Check frames one by one. */

    i__1 = ntochk;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set test case name. */

/* Writing concatenation */
	i__3[0] = 19, a__1[0] = "ZZFDAT Entries for ";
	i__3[1] = 32, a__1[1] = atname + (((i__2 = i__ - 1) < 127 && 0 <= 
		i__2 ? i__2 : s_rnge("atname", i__2, "f_zzfdat__", (ftnlen)
		821)) << 5);
	s_cat(ch__1, a__1, i__3, &c__2, (ftnlen)51);
	tcase_(ch__1, (ftnlen)51);

/*        Check NAME to CODE and CODE to NAME mappings. */

	namfrm_(atname + (((i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : 
		s_rnge("atname", i__2, "f_zzfdat__", (ftnlen)826)) << 5), &
		frcode, (ftnlen)32);
	frmnam_(&atcode[(i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : s_rnge(
		"atcode", i__2, "f_zzfdat__", (ftnlen)827)], frname, (ftnlen)
		32);

/*        Check results. */

	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("FRCODE", &frcode, "=", &atcode[(i__2 = i__ - 1) < 127 && 0 <=
		 i__2 ? i__2 : s_rnge("atcode", i__2, "f_zzfdat__", (ftnlen)
		833)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksc_("FRNAME", frname, "=", atname + (((i__2 = i__ - 1) < 127 && 0 
		<= i__2 ? i__2 : s_rnge("atname", i__2, "f_zzfdat__", (ftnlen)
		834)) << 5), ok, (ftnlen)6, (ftnlen)32, (ftnlen)1, (ftnlen)32)
		;

/*        Now check frame attributes as reported by FRINFO. */

	frinfo_(&atcode[(i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : s_rnge(
		"atcode", i__2, "f_zzfdat__", (ftnlen)839)], &frcent, &frtype,
		 &frclss, &found);

/*        Check results. */

	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	chcksi_("CLSSID", &frclss, "=", &atclss[(i__2 = i__ - 1) < 127 && 0 <=
		 i__2 ? i__2 : s_rnge("atclss", i__2, "f_zzfdat__", (ftnlen)
		846)], &c__0, ok, (ftnlen)6, (ftnlen)1);
	chcksi_("CLASS", &frtype, "=", &attype[(i__2 = i__ - 1) < 127 && 0 <= 
		i__2 ? i__2 : s_rnge("attype", i__2, "f_zzfdat__", (ftnlen)
		847)], &c__0, ok, (ftnlen)5, (ftnlen)1);
	chcksi_("CENT", &frcent, "=", &atcent[(i__2 = i__ - 1) < 127 && 0 <= 
		i__2 ? i__2 : s_rnge("atcent", i__2, "f_zzfdat__", (ftnlen)
		848)], &c__0, ok, (ftnlen)4, (ftnlen)1);

/*        If it's an 'IAU_' style PCK frame, check that ID returned */
/*        for the body from the frame name is the same as the frame */
/*        center ID. */

	if (attype[(i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : s_rnge("atty"
		"pe", i__2, "f_zzfdat__", (ftnlen)855)] == 2 && s_cmp(atname + 
		(((i__4 = i__ - 1) < 127 && 0 <= i__4 ? i__4 : s_rnge("atname"
		, i__4, "f_zzfdat__", (ftnlen)855)) << 5), "IAU_", (ftnlen)4, 
		(ftnlen)4) == 0) {
	    bodn2c_(atname + ((((i__2 = i__ - 1) < 127 && 0 <= i__2 ? i__2 : 
		    s_rnge("atname", i__2, "f_zzfdat__", (ftnlen)858)) << 5) 
		    + 4), &bcode, &found, rtrim_(atname + (((i__4 = i__ - 1) <
		     127 && 0 <= i__4 ? i__4 : s_rnge("atname", i__4, "f_zzf"
		    "dat__", (ftnlen)858)) << 5), (ftnlen)32) - 4);

/*           Check results. */

	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    chcksi_("BCODE", &bcode, "=", &atcent[(i__2 = i__ - 1) < 127 && 0 
		    <= i__2 ? i__2 : s_rnge("atcent", i__2, "f_zzfdat__", (
		    ftnlen)865)], &c__0, ok, (ftnlen)5, (ftnlen)1);
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzfdat__ */

