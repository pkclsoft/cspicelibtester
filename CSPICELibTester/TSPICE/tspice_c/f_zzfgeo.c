/* f_zzfgeo.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 1.;
static doublereal c_b7 = 2.;
static doublereal c_b8 = 4.;
static doublereal c_b10 = -1.;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b29 = 1e-13;
static doublereal c_b65 = 1e-11;
static doublereal c_b66 = -10.;
static doublereal c_b67 = 10.;
static integer c__7 = 7;
static logical c_true = TRUE_;

/* $Procedure      F_ZZFGEO ( Test private "fast" geometry routines ) */
/* Subroutine */ int f_zzfgeo__(logical *ok)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer seed;
    static doublereal maxd, udir[3];
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int zzinrypl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *)
	    ;
    static doublereal r__, scale;
    static integer ncase;
    static doublereal plane[4];
    extern /* Subroutine */ int tcase_(char *, ftnlen), zzryxsph_(doublereal *
	    , doublereal *, doublereal *, doublereal *, logical *), vpack_(
	    doublereal *, doublereal *, doublereal *, doublereal *), repmd_(
	    char *, char *, doublereal *, integer *, char *, ftnlen, ftnlen, 
	    ftnlen);
    extern doublereal dpmax_(void);
    static logical found;
    static char title[240];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal const__;
    extern /* Subroutine */ int t_success__(logical *);
    static integer nxpts;
    extern /* Subroutine */ int nvc2pl_(doublereal *, doublereal *, 
	    doublereal *), chckad_(char *, doublereal *, char *, doublereal *,
	     integer *, doublereal *, logical *, ftnlen, ftnlen), cleard_(
	    integer *, doublereal *), chckxc_(logical *, char *, logical *, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, 
	    logical *, ftnlen);
    static integer caseno;
    static doublereal normal[3];
    static logical xfound;
    static doublereal unorml[3], vertex[3];
    extern /* Subroutine */ int inrypl_(doublereal *, doublereal *, 
	    doublereal *, integer *, doublereal *), surfpt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *);
    static integer xnxpts;
    static doublereal dir[3];
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static doublereal xpt[3];

/* $ Abstract */

/*     Test private SPICELIB "fast" geometry routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private routine routines */

/*        ZZINRYPL */
/*        ZZRYXSPH */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-JAN-2016 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */

/*      DOUBLE PRECISION      VNORM */

/*     Other functions */


/*     Local parameters */

/*      INTEGER               LNSIZE */
/*      PARAMETER           ( LNSIZE = 80 ) */

/*     Local Variables */

/*      CHARACTER*(LNSIZE)    LABEL */
/*      INTEGER               I */

/*     Save all variables to avoid stack problems on some */
/*     platforms. */


/*     Begin every test family with an open call. */

    topen_("F_ZZFGEO", (ftnlen)8);
/* ********************************************************************** */

/*     ZZINRYPL tests */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("ZZINRYPL: basic test with plane parallel to X-Y axes.", (ftnlen)
	    53);
    vpack_(&c_b4, &c_b4, &c_b6, normal);
    const__ = 1.;
    vpack_(&c_b7, &c_b8, &c_b7, vertex);
    vpack_(&c_b10, &c_b4, &c_b10, dir);
    vhat_(dir, udir);

/*     Compute expected result. */

    nvc2pl_(normal, &const__, plane);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    inrypl_(vertex, udir, plane, &xnxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Compute result using ZZINRYPL. */

    maxd = dpmax_();
    zzinrypl_(vertex, udir, normal, &const__, &maxd, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b29, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("ZZINRYPL: ray lies in plane.", (ftnlen)28);
    vpack_(&c_b4, &c_b4, &c_b6, normal);
    const__ = 0.;
    vpack_(&c_b7, &c_b8, &c_b4, vertex);
    vpack_(&c_b10, &c_b4, &c_b4, dir);
    vhat_(dir, udir);

/*     Compute result using ZZINRYPL. */

    maxd = dpmax_();
    zzinrypl_(vertex, udir, normal, &const__, &maxd, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This is a non-intersection case for ZZINRYPL. */

    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("ZZINRYPL: ray vertex only lies in plane.", (ftnlen)40);
    vpack_(&c_b4, &c_b4, &c_b6, normal);
    const__ = 0.;
    vpack_(&c_b7, &c_b8, &c_b4, vertex);
    vpack_(&c_b10, &c_b4, &c_b10, dir);
    vhat_(dir, udir);

/*     Compute result using ZZINRYPL. */

    maxd = dpmax_();
    zzinrypl_(vertex, udir, normal, &const__, &maxd, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This is an intersection case for ZZINRYPL. */

    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    if (*ok) {
	chckad_("XPT", xpt, "~~/", vertex, &c__3, &c_b65, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case -------------------------------------------------------- */


/*     ZZINRYPL random tests */

    seed = -1;
    ncase = 10000;
    i__1 = ncase;
    for (caseno = 1; caseno <= i__1; ++caseno) {
	d__1 = t_randd__(&c_b66, &c_b67, &seed);
	scale = pow_dd(&c_b67, &d__1);
	normal[0] = t_randd__(&c_b10, &c_b6, &seed);
	normal[1] = t_randd__(&c_b10, &c_b6, &seed);
	normal[2] = t_randd__(&c_b10, &c_b6, &seed);
	vhat_(normal, unorml);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	const__ = scale;
	nvc2pl_(unorml, &const__, plane);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	dir[0] = t_randd__(&c_b10, &c_b6, &seed);
	dir[1] = t_randd__(&c_b10, &c_b6, &seed);
	dir[2] = t_randd__(&c_b10, &c_b6, &seed);
	vhat_(dir, udir);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vertex[0] = t_randd__(&c_b10, &c_b6, &seed) * scale;
	vertex[1] = t_randd__(&c_b10, &c_b6, &seed) * scale;
	vertex[2] = t_randd__(&c_b10, &c_b6, &seed) * scale;

/* --- Case -------------------------------------------------------- */

	s_copy(title, "Scale = #; unit normal = (#,#,#); const = #; UDIR = ("
		"#,#,#), VERTEX = (#,#,#).", (ftnlen)240, (ftnlen)78);
	repmd_(title, "#", &scale, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", unorml, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &unorml[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &unorml[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &const__, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", udir, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &udir[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &udir[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", vertex, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &vertex[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &vertex[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	inrypl_(vertex, udir, plane, &xnxpts, xxpt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Compute result using ZZINRYPL. */

	maxd = scale * 1e10;
	zzinrypl_(vertex, udir, unorml, &const__, &maxd, &nxpts, xpt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (xnxpts == 1 || xnxpts == 0) {
	    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);
	    if (*ok && nxpts == 1) {
		chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b65, ok, (ftnlen)3,
			 (ftnlen)3);
	    }
	}
    }
/* ********************************************************************** */

/*     ZZRYXSPH tests */

/* ********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Unit sphere, vertex at center", (ftnlen)29);
    cleard_(&c__3, vertex);
    dir[0] = 1.;
    dir[1] = 0.;
    dir[2] = 0.;
    r__ = 1.;
    zzryxsph_(vertex, dir, &r__, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vpack_(&c_b6, &c_b4, &c_b4, xxpt);
    if (*ok && found) {
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b65, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Unit sphere, vertex on Z axis at Z=2; DIR points in -Z direction."
	    , (ftnlen)65);
    vpack_(&c_b4, &c_b4, &c_b7, vertex);
    dir[0] = 0.;
    dir[1] = 0.;
    dir[2] = -1.;
    r__ = 1.;
    zzryxsph_(vertex, dir, &r__, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vpack_(&c_b4, &c_b4, &c_b6, xxpt);
    if (*ok && found) {
	chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b65, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Unit sphere, vertex on line X=1,Y=2 at Z=2; DIR points in -Z dir"
	    "ection. Miss expected.", (ftnlen)86);
    vpack_(&c_b6, &c_b7, &c_b7, vertex);
    dir[0] = 0.;
    dir[1] = 0.;
    dir[2] = -1.;
    r__ = 1.;
    zzryxsph_(vertex, dir, &r__, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/* --- Case -------------------------------------------------------- */


/*     ZZRYXSPH random tests */

    seed = -1;
    ncase = 10000;
    i__1 = ncase;
    for (caseno = 1; caseno <= i__1; ++caseno) {
	d__1 = t_randd__(&c_b66, &c_b67, &seed);
	scale = pow_dd(&c_b67, &d__1);
	r__ = t_randd__(&c_b6, &c_b7, &seed) * scale / 2;
	vertex[0] = t_randd__(&c_b10, &c_b6, &seed) * scale;
	vertex[1] = t_randd__(&c_b10, &c_b6, &seed) * scale;
	vertex[2] = t_randd__(&c_b10, &c_b6, &seed) * scale;
	dir[0] = -vertex[0] + t_randd__(&c_b10, &c_b6, &seed) / 2;
	dir[1] = -vertex[1] + t_randd__(&c_b10, &c_b6, &seed) / 2;
	dir[2] = -vertex[2] + t_randd__(&c_b10, &c_b6, &seed) / 2;
	vhat_(dir, udir);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

	s_copy(title, "Scale = #; unit normal = (#,#,#); const = #; UDIR = ("
		"#,#,#), VERTEX = (#,#,#).", (ftnlen)240, (ftnlen)78);
	repmd_(title, "#", &scale, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", unorml, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &unorml[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &unorml[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &const__, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", udir, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &udir[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &udir[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", vertex, &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &vertex[1], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	repmd_(title, "#", &vertex[2], &c__7, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	inrypl_(vertex, udir, plane, &xnxpts, xxpt);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Compute result using ZZINRYPL. */

	surfpt_(vertex, udir, &r__, &r__, &r__, xxpt, &xfound);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzryxsph_(vertex, udir, &r__, xpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
/*         WRITE (*,*) 'FOUND, ||V||-R = ', FOUND, VNORM(VERTEX)-R */
	chcksl_("FOUND", &found, &xfound, ok, (ftnlen)5);
	if (*ok && found) {
	    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &c_b65, ok, (ftnlen)3, (
		    ftnlen)3);
	}
    }
    t_success__(ok);
    return 0;
} /* f_zzfgeo__ */

