/* f_zzgfcost.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b19 = 0.;
static doublereal c_b21 = 1.;
static integer c__3 = 3;
static integer c__0 = 0;
static doublereal c_b156 = 1e-14;
static integer c__1 = 1;

/* $Procedure      F_ZZGFCOST ( Test GF coordinate state computation ) */
/* Subroutine */ int f_zzgfcost__(logical *ok)
{
    /* Initialized data */

    static char corr[200*9] = "NONE                                         "
	    "                                                                "
	    "                                                                "
	    "                           " "lt                                "
	    "                                                                "
	    "                                                                "
	    "                                      " " lt+s                  "
	    "                                                                "
	    "                                                                "
	    "                                                 " " cn         "
	    "                                                                "
	    "                                                                "
	    "                                                            " 
	    " cn + s                                                        "
	    "                                                                "
	    "                                                                "
	    "         " "XLT                                                 "
	    "                                                                "
	    "                                                                "
	    "                    " "XLT + S                                  "
	    "                                                                "
	    "                                                                "
	    "                               " "XCN                           "
	    "                                                                "
	    "                                                                "
	    "                                          " "XCN+S              "
	    "                                                                "
	    "                                                                "
	    "                                                     ";
    static char eframe[32*4] = "J2000                           " "IAU_EARTH"
	    "                       " "ECLIPJ2000                      " "IAU"
	    "_MOON                        ";
    static char vcdefs[32*3] = "POSITION                        " "SUB-OBSER"
	    "VER POINT              " "SURFACE INTERCEPT POINT         ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    static char dref[32];
    static doublereal dvec[3];
    static integer dctr;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzgfssob_(
	    char *, integer *, doublereal *, char *, char *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen), zzgfcost_(
	    char *, char *, integer *, doublereal *, char *, char *, integer *
	    , char *, integer *, doublereal *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), zzgfssin_(
	    char *, integer *, doublereal *, char *, char *, integer *, char *
	    , integer *, doublereal *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen, ftnlen, ftnlen), zzprscor_(char *, logical *, 
	    ftnlen);
    static integer n;
    static doublereal radii[3], delta;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen), tcase_(
	    char *, ftnlen);
    static integer obsid;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    static integer trgid;
    static logical found;
    static doublereal state[6];
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen), bodn2c_(char *, 
	    integer *, logical *, ftnlen), t_success__(logical *), chckad_(
	    char *, doublereal *, char *, doublereal *, integer *, doublereal 
	    *, logical *, ftnlen, ftnlen);
    static integer nc, nd;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static integer nf;
    static doublereal et;
    static integer handle;
    static char vecdef[32];
    static doublereal lt;
    static integer frclid;
    static doublereal negvec[3];
    static char abcorr[200], method[80], obsrvr[36], target[36], timstr[50];
    static doublereal et0, xstate[6];
    static integer frclss, frcode, ns;
    static logical attblk[6];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen), bodvrd_(char *, 
	    char *, integer *, integer *, doublereal *, ftnlen, ftnlen), 
	    spkpos_(char *, doublereal *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), namfrm_(char *, 
	    integer *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), frinfo_(integer *, integer 
	    *, integer *, integer *, logical *), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), spkezr_(char *, doublereal *, char 
	    *, char *, char *, doublereal *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen), prefix_(char *, integer *, char *, ftnlen, 
	    ftnlen), vminus_(doublereal *, doublereal *), spkuef_(integer *), 
	    delfil_(char *, ftnlen);
    static logical fnd;
    static char ref[32];
    extern doublereal spd_(void);

/* $ Abstract */

/*     Test the GF private coordinate state computation routine */
/*     ZZGFCOST. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF coordinate state */
/*     computation routine ZZGFCOQ. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 3.0.0, 13-MAY-2009 (NJB) */

/*        Removed error checks for ID codes that don't */
/*        map to names, as these are no longer applicable. */

/* -    SPICELIB Version 2.0.0, 26-MAY-2008 (NJB) */

/*        Added test case for bad values of VECDEF. */

/* -    SPICELIB Version 1.0.0, 14-MAY-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local parameters */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Saved Variables */


/*     Initial values */


/*     Vector definition methods: */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFCOST", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgfssob.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgfssob.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad value of VECDEF", (ftnlen)19);
    s_copy(abcorr, "CN+S", (ftnlen)200, (ftnlen)4);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    trgid = 399;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    obsid = 301;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, ref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(vecdef, "Surface xcept", (ftnlen)32, (ftnlen)13);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction", (ftnlen)25);
    obsid = 301;
    dctr = 399;
    s_copy(abcorr, "None.", (ftnlen)200, (ftnlen)5);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad computation method", (ftnlen)22);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(ref, "ITRF93", (ftnlen)32, (ftnlen)6);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame not centered on target", (ftnlen)38);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(method, "NEAR POINT: ELLIPSOID", (ftnlen)80, (ftnlen)21);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    trgid = 499;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);
    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    trgid = 9511010;
    s_copy(ref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    trgid = 301;
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    obsid = 9511010;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF ephemeris data available", (ftnlen)32);
    s_copy(dref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    dctr = 9511010;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF orientation data available", (ftnlen)34);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    trgid = 399;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    obsid = 301;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    dctr = 399;
    s_copy(dref, "ITRF93", (ftnlen)32, (ftnlen)6);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, dref, &dctr, 
	    dvec, radii, state, &found, (ftnlen)32, (ftnlen)80, (ftnlen)32, (
	    ftnlen)200, (ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    dctr = 399;
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're going to loop over all vector definitions, */
/*     aberration corrections, coordinate systems, and */
/*     coordinates. */


    for (nd = 1; nd <= 3; ++nd) {
	s_copy(vecdef, vcdefs + (((i__1 = nd - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("vcdefs", i__1, "f_zzgfcost__", (ftnlen)505)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (s_cmp(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23) 
		== 0) {
	    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
	} else if (s_cmp(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18)
		 == 0) {
	    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
	} else {
	    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	}
	for (nc = 1; nc <= 9; ++nc) {
	    s_copy(abcorr, corr + ((i__1 = nc - 1) < 9 && 0 <= i__1 ? i__1 : 
		    s_rnge("corr", i__1, "f_zzgfcost__", (ftnlen)521)) * 200, 
		    (ftnlen)200, (ftnlen)200);
	    zzprscor_(abcorr, attblk, (ftnlen)200);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Loop over the set of sample times. */

	    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
	    str2et_(timstr, &et0, (ftnlen)50);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delta = spd_() * 100;
	    for (ns = 1; ns <= 10; ++ns) {

/* --- Case: ------------------------------------------------------ */

		if (s_cmp(vecdef, "POSITION", (ftnlen)32, (ftnlen)8) == 0) {
		    s_copy(title, "#-# position; #; #", (ftnlen)200, (ftnlen)
			    18);
		    repmc_(title, "#", obsrvr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)200, (ftnlen)200);
		} else if (s_cmp(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (
			ftnlen)18) == 0) {
		    s_copy(title, "# sub # point on #; #; #", (ftnlen)200, (
			    ftnlen)24);
		    repmc_(title, "#", method, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)80, (ftnlen)200);
		    repmc_(title, "#", obsrvr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)200, (ftnlen)200);
		} else if (s_cmp(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)
			32, (ftnlen)23) == 0) {
		    nf = ns % 4 + 1;
		    s_copy(dref, eframe + (((i__1 = nf - 1) < 4 && 0 <= i__1 ?
			     i__1 : s_rnge("eframe", i__1, "f_zzgfcost__", (
			    ftnlen)562)) << 5), (ftnlen)32, (ftnlen)32);
		    s_copy(title, "# #-# intercept. #; DREF = #; #", (ftnlen)
			    200, (ftnlen)31);
		    repmc_(title, "#", method, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)80, (ftnlen)200);
		    repmc_(title, "#", obsrvr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)36, (ftnlen)200);
		    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, 
			    (ftnlen)200, (ftnlen)200);
		    repmc_(title, "#", dref, title, (ftnlen)200, (ftnlen)1, (
			    ftnlen)32, (ftnlen)200);
		}
		et = et0 + (ns - 1) * delta;
		etcal_(&et, timstr, (ftnlen)50);
		repmc_(title, "#", timstr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)50, (ftnlen)200);
		tcase_(title, (ftnlen)200);
		if (s_cmp(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (
			ftnlen)23) == 0) {

/*                 Look up the direction vector. */

		    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (
			    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    namfrm_(dref, &frcode, (ftnlen)32);
		    chcksi_("FRCODE ", &frcode, "!=", &c__0, &c__0, ok, (
			    ftnlen)7, (ftnlen)2);
		    frinfo_(&frcode, &dctr, &frclss, &frclid, &fnd);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("FRINFO FND", &fnd, &c_true, ok, (ftnlen)10);
		}

/*              Look up the expected state vector. */

		if (s_cmp(vecdef, "POSITION", (ftnlen)32, (ftnlen)8) == 0) {
		    spkezr_(target, &et, ref, abcorr, obsrvr, xstate, &lt, (
			    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else if (s_cmp(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (
			ftnlen)18) == 0) {
		    zzgfssob_(method, &trgid, &et, ref, abcorr, &obsid, radii,
			     xstate, (ftnlen)80, (ftnlen)32, (ftnlen)200);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    zzgfssin_(method, &trgid, &et, ref, abcorr, &obsid, dref, 
			    &dctr, dvec, radii, xstate, &found, (ftnlen)80, (
			    ftnlen)32, (ftnlen)200, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 By construction, the intercept should be found. */

		    chcksl_("intercept found", &found, &c_true, ok, (ftnlen)
			    15);
		}

/*              Get the result from ZZGFCOST. */

		zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &obsid, 
			dref, &dctr, dvec, radii, state, &found, (ftnlen)32, (
			ftnlen)80, (ftnlen)32, (ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*              Check position. */

		chckad_("Position", state, "~~/", xstate, &c__3, &c_b156, ok, 
			(ftnlen)8, (ftnlen)3);

/*              Check velocity. */

		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b156, ok, (ftnlen)8, (ftnlen)3);
		if (s_cmp(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (
			ftnlen)23) == 0) {

/* --- Case: ------------------------------------------------------ */

/*                 Given the inputs above, make sure we do not find */
/*                    the state if we reverse the ray's direction.' */

		    prefix_("Earth non-intersection case:", &c__1, title, (
			    ftnlen)28, (ftnlen)200);
		    tcase_(title, (ftnlen)200);
		    vminus_(dvec, negvec);
		    vequ_(negvec, dvec);
		    zzgfcost_(vecdef, method, &trgid, &et, ref, abcorr, &
			    obsid, dref, &dctr, dvec, radii, state, &found, (
			    ftnlen)32, (ftnlen)80, (ftnlen)32, (ftnlen)200, (
			    ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 By construction, the state should NOT be found. */

		    chcksl_("ZZGFSSIN found (1)", &found, &c_false, ok, (
			    ftnlen)18);
		}
	    }

/*           End of cases for the current epoch. */

	}

/*        End of cases for the current aberration correction. */

    }

/*     End of cases for the current vector definition. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgfssob.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfcost__ */

