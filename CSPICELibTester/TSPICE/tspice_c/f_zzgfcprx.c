/* f_zzgfcprx.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static doublereal c_b10 = 1.;
static integer c__6 = 6;
static integer c__3 = 3;
static doublereal c_b49 = 0.;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c_n1 = -1;
static doublereal c_b228 = 2.;
static doublereal c_b229 = 3.;

/* $Procedure      F_ZZGFCPRX ( Test coordinate derivative proxy routine ) */
/* Subroutine */ int f_zzgfcprx__(logical *ok)
{
    /* Initialized data */

    static char sysnms[32*7] = "RECTANGULAR                     " "LATITUDIN"
	    "AL                     " "RA/DEC                          " "SPH"
	    "ERICAL                       " "CYLINDRICAL                     " 
	    "GEODETIC                        " "PLANETOGRAPHIC              "
	    "    ";
    static char crdnms[32*3*7] = "X                               " "Y      "
	    "                         " "Z                               " 
	    "RADIUS                          " "LONGITUDE                   "
	    "    " "LATITUDE                        " "RANGE                 "
	    "          " "RIGHT ASCENSION                 " "DECLINATION     "
	    "                " "RADIUS                          " "COLATITUDE"
	    "                      " "LONGITUDE                       " "RADI"
	    "US                          " "LONGITUDE                       " 
	    "Z                               " "LONGITUDE                   "
	    "    " "LATITUDE                        " "ALTITUDE              "
	    "          " "LONGITUDE                       " "LATITUDE        "
	    "                " "ALTITUDE                        ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    static doublereal dlat, dlon;
    static integer nlat, nlon;
    extern doublereal vdot_(doublereal *, doublereal *);
    static doublereal xvel[3];
    extern /* Subroutine */ int zzgfcprx_(doublereal *, char *, doublereal *, 
	    doublereal *, integer *, integer *, ftnlen);
    static doublereal f;
    static integer i__, j, k;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     tcase_(char *, ftnlen);
    static char qname[200];
    static integer ndlat;
    extern /* Subroutine */ int errch_(char *, char *, ftnlen, ftnlen), 
	    vpack_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    ident_(doublereal *), repmc_(char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen), repmd_(char *, char *, 
	    doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    static integer ndlon, sense;
    static doublereal state[6];
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    static integer ci;
    static doublereal dp, jacobi[9]	/* was [3][3] */, re;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static integer si;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal deltla;
    static integer cdsign[3];
    static char crdsys[32];
    static doublereal deltlo;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     suffix_(char *, integer *, char *, ftnlen, ftnlen), chcksi_(char 
	    *, integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), setmsg_(char *, ftnlen), sigerr_(char *, ftnlen), 
	    latrec_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    dlatdr_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    dsphdr_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    dcyldr_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    dgeodr_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), dpgrdr_(char *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen);
    static doublereal lat;
    extern doublereal dpr_(void);
    static doublereal lon;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     Test the GF private coordinate derivative proxy routine ZZGFCPRX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF */
/*     coordinate derivative proxy routine ZZGFCPRX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 27-JUN-2008 (NJB) */

/*        Added new test cases: */

/*           - Handling of invalid coordinate system */
/*           - Handling of the rectangular system */
/*           - Handling of the DPSIGN==0 case where */
/*             the position is on the Z-axis */
/*           - Handling of the Z-velocity == 0 case */
/*             for cylindrical coordinates, where the */
/*             position is on the Z-axis */
/*           - Zero input velocity */

/* -    SPICELIB Version 1.0.0, 11-MAY-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Small delta value used for near 0 and pi */
/*     latitudes. */


/*     Number of recognized coordinate systems. */


/*     Maximum length of a coordinate system name. */


/*     Maximum length of a coordinate name. */


/*     Numbers of latitude and longitude samples. */


/*     Local Variables */


/*     Save everything. */


/*     Initial values */


/*     Names of supported coordinate systems. */

/*     The Ith coordinate system in the array SYSNMS has coordinates */
/*     in the Ith row of the array CRDNMS. This association must be */
/*     preserved when this routine is updated. */


/*     Names of coordinate triples for the supported coordinate */
/*     systems. */

/*     The order of the coordinate names in the Ith row of this array */
/*     matches the order of the outputs of the corresponding */
/*     SPICELIB routine REC*, which maps a Cartesian vector to */
/*     the Ith coordinate system in the array SYSNMS. Again, this */
/*     order must be preserved. */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFCPRX", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load PCK file.", (ftnlen)32);

/*     Load a PCK. */

    tstpck_("zzgfcprx.tpc", &c_true, &c_false, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad coordinate system", (ftnlen)21);

/*     Set shape and spin parameters. */

    re = 1e5;
    f = .5;
    sense = -1;
    filld_(&c_b10, &c__6, state);
    zzgfcprx_(state, "GAUSSIAN", &re, &f, &sense, cdsign, (ftnlen)8);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad longitude sense", (ftnlen)19);

/*     Set shape and spin parameters. */

    re = 1e5;
    f = .5;
    sense = -2;
    filld_(&c_b10, &c__6, state);
    zzgfcprx_(state, "PLANETOGRAPHIC", &re, &f, &sense, cdsign, (ftnlen)14);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad equatorial radius", (ftnlen)21);

/*     Set shape and spin parameters. */

    re = -1e5;
    f = .5;
    sense = 1;
    filld_(&c_b10, &c__6, state);
    zzgfcprx_(state, "PLANETOGRAPHIC", &re, &f, &sense, cdsign, (ftnlen)14);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    zzgfcprx_(state, "GEODETIC", &re, &f, &sense, cdsign, (ftnlen)8);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad flattening coefficient", (ftnlen)26);

/*     Set shape and spin parameters. */

    re = 1e5;
    f = 1e6;
    sense = 1;
    filld_(&c_b10, &c__6, state);
    zzgfcprx_(state, "PLANETOGRAPHIC", &re, &f, &sense, cdsign, (ftnlen)14);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    zzgfcprx_(state, "GEODETIC", &re, &f, &sense, cdsign, (ftnlen)8);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* ********************************************************************* */
/* * */
/* *    Non-error Exceptional cases */
/* * */
/* ********************************************************************* */

/*     Test handling of zero velocity vector. */

    tcase_("Input velocity is zero; position is off Z-axis.", (ftnlen)47);
    vpack_(&c_b10, &c_b10, &c_b10, state);
    cleard_(&c__3, &state[3]);
    for (si = 1; si <= 7; ++si) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Zero velocity; system is #", (ftnlen)200, (ftnlen)26);
	repmc_(title, "#", sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 
		: s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)387)) << 5), 
		title, (ftnlen)200, (ftnlen)1, (ftnlen)32, (ftnlen)200);
	tcase_(title, (ftnlen)200);
	zzgfcprx_(state, sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)391)) << 5), &
		c_b10, &c_b49, &c__1, cdsign, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (ci = 1; ci <= 3; ++ci) {
	    s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)10);
	    suffix_(crdnms + (((i__1 = ci + si * 3 - 4) < 21 && 0 <= i__1 ? 
		    i__1 : s_rnge("crdnms", i__1, "f_zzgfcprx__", (ftnlen)397)
		    ) << 5), &c__0, qname, (ftnlen)32, (ftnlen)200);
	    suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)200);
	    chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("cdsign", i__1, "f_zzgfcprx__", (ftnlen)400)], 
		    "=", &c__0, &c__0, ok, (ftnlen)200, (ftnlen)1);
	}
    }
/*     Test handling of zero velocity vector. */

    tcase_("Input velocity is zero; position is on Z-axis.", (ftnlen)46);
    vpack_(&c_b49, &c_b49, &c_b10, state);
    cleard_(&c__3, &state[3]);
    for (si = 1; si <= 7; ++si) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Zero velocity; system is #", (ftnlen)200, (ftnlen)26);
	repmc_(title, "#", sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 
		: s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)422)) << 5), 
		title, (ftnlen)200, (ftnlen)1, (ftnlen)32, (ftnlen)200);
	tcase_(title, (ftnlen)200);
	zzgfcprx_(state, sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)426)) << 5), &
		c_b10, &c_b49, &c__1, cdsign, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (ci = 1; ci <= 3; ++ci) {
	    s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)10);
	    suffix_(crdnms + (((i__1 = ci + si * 3 - 4) < 21 && 0 <= i__1 ? 
		    i__1 : s_rnge("crdnms", i__1, "f_zzgfcprx__", (ftnlen)432)
		    ) << 5), &c__0, qname, (ftnlen)32, (ftnlen)200);
	    suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)200);
	    chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("cdsign", i__1, "f_zzgfcprx__", (ftnlen)435)], 
		    "=", &c__0, &c__0, ok, (ftnlen)200, (ftnlen)1);
	}
    }

/*     Test results for positions on the Z-axis, with velocity */
/*     orthogonal to Z. Applies only to the cylindrical system. */


/* ---- Case ------------------------------------------------------------- */

    s_copy(title, "velocity orthogonal to Z; system is #", (ftnlen)200, (
	    ftnlen)37);
    repmc_(title, "#", "CYLINDRICAL", title, (ftnlen)200, (ftnlen)1, (ftnlen)
	    11, (ftnlen)200);
    tcase_(title, (ftnlen)200);
    vpack_(&c_b49, &c_b49, &c_b10, state);
    vpack_(&c_b10, &c_b49, &c_b49, &state[3]);
    zzgfcprx_(state, "CYLINDRICAL", &c_b10, &c_b49, &c__1, cdsign, (ftnlen)11)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (ci = 1; ci <= 3; ++ci) {

/*        Note: the index of the cylindrical system is 5. */

	s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)10);
	suffix_(crdnms + (((i__1 = ci + 11) < 21 && 0 <= i__1 ? i__1 : s_rnge(
		"crdnms", i__1, "f_zzgfcprx__", (ftnlen)468)) << 5), &c__0, 
		qname, (ftnlen)32, (ftnlen)200);
	suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)200);
	chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("cdsign", i__1, "f_zzgfcprx__", (ftnlen)471)], "=", &
		c__0, &c__0, ok, (ftnlen)200, (ftnlen)1);
    }

/*     Test results for range or altitude coordinates for positions on */
/*     the Z-axis, with velocity orthogonal to Z. Applies to all but */
/*     rectangular systems. */

/*     The rectangular system has index 1, so start at index 2. */

    for (si = 2; si <= 7; ++si) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(crdsys, sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)487)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(title, "velocity orthogonal to Z; system is #", (ftnlen)200, (
		ftnlen)37);
	repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)32, 
		(ftnlen)200);
	tcase_(title, (ftnlen)200);
	vpack_(&c_b49, &c_b49, &c_b10, state);
	vpack_(&c_b10, &c_b49, &c_b49, &state[3]);
	zzgfcprx_(state, "CYLINDRICAL", &c_b10, &c_b49, &c__1, cdsign, (
		ftnlen)11);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Set CI to the index of the radius or altitude coordinate */
/*        of the current system. */

	if (s_cmp(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11) == 0) {
	    ci = 1;
	} else if (s_cmp(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) == 0) {
	    ci = 3;
	} else if (s_cmp(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6) == 0) {
	    ci = 1;
	} else if (s_cmp(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14) == 
		0) {
	    ci = 3;
	} else if (s_cmp(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) == 0) 
		{
	    ci = 1;
	} else if (s_cmp(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)9) == 0) {
	    ci = 1;
	} else {
	    setmsg_("Unexpected system: #", (ftnlen)20);
	    errch_("#", crdsys, (ftnlen)1, (ftnlen)32);
	    sigerr_("SPICE(BUG)", (ftnlen)10);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)10);
	suffix_(crdnms + (((i__1 = ci + si * 3 - 4) < 21 && 0 <= i__1 ? i__1 :
		 s_rnge("crdnms", i__1, "f_zzgfcprx__", (ftnlen)526)) << 5), &
		c__0, qname, (ftnlen)32, (ftnlen)200);
	suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)200);
	chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("cdsign", i__1, "f_zzgfcprx__", (ftnlen)529)], "=", &
		c__0, &c__0, ok, (ftnlen)200, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */


/*     Test results for positions on the Z-axis. */

    for (nlat = 1; nlat <= 3; ++nlat) {

/*        Generate Z values corresponding to */

/*           +1, 0, -1 */

/*        positions, respectively. */

	state[0] = 0.;
	state[1] = 0.;
	state[2] = 2. - (doublereal) nlat;
	for (ndlat = 1; ndlat <= 2; ++ndlat) {

/*           Generate latitude values corresponding to */

/*              +Pi/4, -Pi/4 */

/*           positions, respectively. */

	    dlat = (3 - (ndlat << 1)) * pi_() / 4.;
	    dlon = pi_() / 4.;
	    latrec_(&c_b10, &dlon, &dlat, &state[3]);
	    dp = vdot_(state, &state[3]);

/*           Check the coordinate derivative signs for */
/*           each coordinate system. */

	    for (si = 1; si <= 7; ++si) {

/* ---- Case ------------------------------------------------------------- */

		s_copy(title, "#; Z #; dlat #(deg); dlon #(deg)", (ftnlen)200,
			 (ftnlen)32);
		repmc_(title, "#", sysnms + (((i__1 = si - 1) < 7 && 0 <= 
			i__1 ? i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)578)) << 5), title, (ftnlen)200, (ftnlen)1, (
			ftnlen)32, (ftnlen)200);
		repmd_(title, "#", &state[2], &c__3, title, (ftnlen)200, (
			ftnlen)1, (ftnlen)200);
		d__1 = dpr_() * dlat;
		repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (ftnlen)
			1, (ftnlen)200);
		d__1 = dpr_() * dlon;
		repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (ftnlen)
			1, (ftnlen)200);
		tcase_(title, (ftnlen)200);
		if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? i__1 :
			 s_rnge("sysnms", i__1, "f_zzgfcprx__", (ftnlen)586)) 
			<< 5), "RECTANGULAR", (ftnlen)32, (ftnlen)11) == 0) {
		    for (i__ = 1; i__ <= 3; ++i__) {
			xvel[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
				s_rnge("xvel", i__1, "f_zzgfcprx__", (ftnlen)
				589)] = state[(i__2 = i__ + 2) < 6 && 0 <= 
				i__2 ? i__2 : s_rnge("state", i__2, "f_zzgfc"
				"prx__", (ftnlen)589)];
		    }
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)592)) << 5), "LATITUDINAL", (ftnlen)32, (
			ftnlen)11) == 0) {
		    xvel[0] = dp;
		    xvel[1] = 0.;
		    xvel[2] = 0.;
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)598)) << 5), "RA/DEC", (ftnlen)32, (ftnlen)6) 
			== 0) {
		    xvel[0] = dp;
		    xvel[1] = 0.;
		    xvel[2] = 0.;
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)604)) << 5), "SPHERICAL", (ftnlen)32, (ftnlen)
			9) == 0) {
		    xvel[0] = dp;
		    xvel[1] = 0.;
		    xvel[2] = 0.;
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)610)) << 5), "CYLINDRICAL", (ftnlen)32, (
			ftnlen)11) == 0) {
		    xvel[0] = 0.;
		    xvel[1] = 0.;
		    xvel[2] = state[5];
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)616)) << 5), "GEODETIC", (ftnlen)32, (ftnlen)8)
			 == 0) {
		    xvel[0] = 0.;
		    xvel[1] = 0.;
		    xvel[2] = dp;
		} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)622)) << 5), "PLANETOGRAPHIC", (ftnlen)32, (
			ftnlen)14) == 0) {
		    xvel[0] = 0.;
		    xvel[1] = 0.;
		    xvel[2] = dp;
		} else {
		    setmsg_("Bad coordinate system #", (ftnlen)23);
		    errch_("#", sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ? 
			    i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			    ftnlen)631)) << 5), (ftnlen)1, (ftnlen)32);
		    sigerr_("SPICE(BUG)", (ftnlen)10);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/*              Set shape and spin parameters. */

		re = 1e5;
		f = .5;
		sense = -1;
		zzgfcprx_(state, sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 ?
			 i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__", (
			ftnlen)644)) << 5), &re, &f, &sense, cdsign, (ftnlen)
			32);
		for (ci = 1; ci <= 3; ++ci) {
		    s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)10);
		    suffix_(crdnms + (((i__1 = ci + si * 3 - 4) < 21 && 0 <= 
			    i__1 ? i__1 : s_rnge("crdnms", i__1, "f_zzgfcprx"
			    "__", (ftnlen)649)) << 5), &c__0, qname, (ftnlen)
			    32, (ftnlen)200);
		    suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)200);
		    if (xvel[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "xvel", i__1, "f_zzgfcprx__", (ftnlen)652)] > 0.) 
			    {
			chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= 
				i__1 ? i__1 : s_rnge("cdsign", i__1, "f_zzgf"
				"cprx__", (ftnlen)654)], "=", &c__1, &c__0, ok,
				 (ftnlen)200, (ftnlen)1);
		    } else if (xvel[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 : 
			    s_rnge("xvel", i__1, "f_zzgfcprx__", (ftnlen)656)]
			     < 0.) {
			chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= 
				i__1 ? i__1 : s_rnge("cdsign", i__1, "f_zzgf"
				"cprx__", (ftnlen)658)], "=", &c_n1, &c__0, ok,
				 (ftnlen)200, (ftnlen)1);
		    } else {
			chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 0 <= 
				i__1 ? i__1 : s_rnge("cdsign", i__1, "f_zzgf"
				"cprx__", (ftnlen)660)], "=", &c__0, &c__0, ok,
				 (ftnlen)200, (ftnlen)1);
		    }
		}

/*              End of coordinate system loop. */

	    }

/*           End of coordinate loop. */

	}

/*        End of derivative direction loop. */

    }

/*     End of Z-axis position loop. */

/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Rectangular coordinate system cases */
/* * */
/* ********************************************************************* */

/*     Since outputs for the rectangular coordinate system require */
/*     special handling by ZZGFCPRX, we test the routine's processing */
/*     for this case separately. */

/*     We try cases where each velocity component is negative, 0, or */
/*     positive. */

    vpack_(&c_b10, &c_b228, &c_b229, state);
    for (i__ = 1; i__ <= 3; ++i__) {
	state[3] = (doublereal) (i__ - 2);
	for (j = 1; j <= 3; ++j) {
	    state[4] = (doublereal) (j - 2);
	    for (k = 1; k <= 3; ++k) {
		state[5] = (doublereal) (k - 2);

/* ---- Case ------------------------------------------------------------- */

		s_copy(title, "Velocity (# # #)", (ftnlen)200, (ftnlen)16);
		repmd_(title, "#", &state[3], &c__3, title, (ftnlen)200, (
			ftnlen)1, (ftnlen)200);
		repmd_(title, "#", &state[4], &c__3, title, (ftnlen)200, (
			ftnlen)1, (ftnlen)200);
		repmd_(title, "#", &state[5], &c__3, title, (ftnlen)200, (
			ftnlen)1, (ftnlen)200);
		tcase_(title, (ftnlen)200);
		re = 1.;
		f = 0.;
		sense = 1;
		zzgfcprx_(state, "RECTANGULAR", &re, &f, &sense, cdsign, (
			ftnlen)11);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		i__1 = (integer) state[3];
		chcksi_("X vel sign", cdsign, "=", &i__1, &c__0, ok, (ftnlen)
			10, (ftnlen)1);
	    }
	}
    }
/* ********************************************************************* */
/* * */
/* *    Non-rectangular coordinate system cases */
/* * */
/* ********************************************************************* */

/*     We're going to loop over a variety of state vectors */
/*     and all coordinate system/coordinate combinations. */
/*     For each test, the derivative signs we find must match */
/*     those of the corresponding coordinate derivative. */


/*     DELTLA and DELTLO are, respectively the latitude and longitude */
/*     increments we use to create different direction vectors. */

    deltla = pi_() / 4;
    deltlo = pi_() * 2 / 8;
    for (nlon = 1; nlon <= 8; ++nlon) {
	for (nlat = 1; nlat <= 5; ++nlat) {

/*           Create the latitudinal coordinates of a position vector. */

	    lon = (nlon - 1) * deltlo;
	    lat = pi_() / 2 - (nlat - 1) * deltla;

/*           Don't pick a singular latitude value. */

/* Computing MIN */
	    d__1 = lat, d__2 = pi_() / 2 - .1;
	    lat = min(d__1,d__2);
/* Computing MAX */
	    d__1 = lat, d__2 = .1 - pi_() / 2;
	    lat = max(d__1,d__2);

/*           Fill in the position portion of the state vector. */

	    latrec_(&c_b10, &lon, &lat, state);

/*           Loop over the velocity directions. */

	    for (ndlon = 1; ndlon <= 8; ++ndlon) {
		for (ndlat = 1; ndlat <= 5; ++ndlat) {

/*                 Create a velocity direction. Tweak the direction */
/*                 so it doesn't differ from the position direction */
/*                 only by round-off; this prevents situations where */
/*                 we get the wrong derivative sign due to noise. */

		    dlon = (ndlon - 1) * deltlo + .001;
		    dlat = pi_() / 2 - (ndlat - 1) * deltla + .001;

/*                 Fill in the position portion of the state vector. */

		    latrec_(&c_b10, &dlon, &dlat, &state[3]);
		    for (si = 1; si <= 7; ++si) {

/* ---- Case ------------------------------------------------------------- */

			s_copy(title, "#; lat #(deg); lon #(deg); dlat #(deg"
				"); dlon #(deg)", (ftnlen)200, (ftnlen)51);
			repmc_(title, "#", sysnms + (((i__1 = si - 1) < 7 && 
				0 <= i__1 ? i__1 : s_rnge("sysnms", i__1, 
				"f_zzgfcprx__", (ftnlen)813)) << 5), title, (
				ftnlen)200, (ftnlen)1, (ftnlen)32, (ftnlen)
				200);
			d__1 = dpr_() * lat;
			repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (
				ftnlen)1, (ftnlen)200);
			d__1 = dpr_() * lon;
			repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (
				ftnlen)1, (ftnlen)200);
			d__1 = dpr_() * dlat;
			repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (
				ftnlen)1, (ftnlen)200);
			d__1 = dpr_() * dlon;
			repmd_(title, "#", &d__1, &c__3, title, (ftnlen)200, (
				ftnlen)1, (ftnlen)200);
			tcase_(title, (ftnlen)200);

/*                    Set shape and spin parameters. */

			re = 1e5;
			f = .5;
			sense = -1;

/*                    Look up the Jacobian matrix for the current system */
/*                    and state. Find the expected coordinate rates. */

			if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 <= i__1 
				? i__1 : s_rnge("sysnms", i__1, "f_zzgfcprx__"
				, (ftnlen)832)) << 5), "RECTANGULAR", (ftnlen)
				32, (ftnlen)11) == 0) {
			    ident_(jacobi);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)836)) << 5), "LATITUDINAL"
				, (ftnlen)32, (ftnlen)11) == 0) {
			    dlatdr_(state, &state[1], &state[2], jacobi);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)842)) << 5), "RA/DEC", (
				ftnlen)32, (ftnlen)6) == 0) {
			    dlatdr_(state, &state[1], &state[2], jacobi);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)848)) << 5), "SPHERICAL", 
				(ftnlen)32, (ftnlen)9) == 0) {
			    dsphdr_(state, &state[1], &state[2], jacobi);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)854)) << 5), "CYLINDRICAL"
				, (ftnlen)32, (ftnlen)11) == 0) {
			    dcyldr_(state, &state[1], &state[2], jacobi);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)861)) << 5), "GEODETIC", (
				ftnlen)32, (ftnlen)8) == 0) {
			    dgeodr_(state, &state[1], &state[2], &re, &f, 
				    jacobi);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else if (s_cmp(sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)867)) << 5), "PLANETOGRA"
				"PHIC", (ftnlen)32, (ftnlen)14) == 0) {
			    dpgrdr_("Mars", state, &state[1], &state[2], &re, 
				    &f, jacobi, (ftnlen)4);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			} else {
			    setmsg_("Bad coordinate system #", (ftnlen)23);
			    errch_("#", sysnms + (((i__1 = si - 1) < 7 && 0 <=
				     i__1 ? i__1 : s_rnge("sysnms", i__1, 
				    "f_zzgfcprx__", (ftnlen)877)) << 5), (
				    ftnlen)1, (ftnlen)32);
			    sigerr_("SPICE(BUG)", (ftnlen)10);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			}
			mxv_(jacobi, &state[3], xvel);
			zzgfcprx_(state, sysnms + (((i__1 = si - 1) < 7 && 0 
				<= i__1 ? i__1 : s_rnge("sysnms", i__1, "f_z"
				"zgfcprx__", (ftnlen)887)) << 5), &re, &f, &
				sense, cdsign, (ftnlen)32);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			for (ci = 1; ci <= 3; ++ci) {
			    s_copy(qname, "sign of d(", (ftnlen)200, (ftnlen)
				    10);
			    suffix_(crdnms + (((i__1 = ci + si * 3 - 4) < 21 
				    && 0 <= i__1 ? i__1 : s_rnge("crdnms", 
				    i__1, "f_zzgfcprx__", (ftnlen)894)) << 5),
				     &c__0, qname, (ftnlen)32, (ftnlen)200);
			    suffix_(")/dt", &c__0, qname, (ftnlen)4, (ftnlen)
				    200);
			    if (xvel[(i__1 = ci - 1) < 3 && 0 <= i__1 ? i__1 :
				     s_rnge("xvel", i__1, "f_zzgfcprx__", (
				    ftnlen)897)] > 0.) {
				chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 
					0 <= i__1 ? i__1 : s_rnge("cdsign", 
					i__1, "f_zzgfcprx__", (ftnlen)899)], 
					"=", &c__1, &c__0, ok, (ftnlen)200, (
					ftnlen)1);
			    } else if (xvel[(i__1 = ci - 1) < 3 && 0 <= i__1 ?
				     i__1 : s_rnge("xvel", i__1, "f_zzgfcprx"
				    "__", (ftnlen)902)] < 0.) {
				chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 
					0 <= i__1 ? i__1 : s_rnge("cdsign", 
					i__1, "f_zzgfcprx__", (ftnlen)904)], 
					"=", &c_n1, &c__0, ok, (ftnlen)200, (
					ftnlen)1);
			    } else {
				chcksi_(qname, &cdsign[(i__1 = ci - 1) < 3 && 
					0 <= i__1 ? i__1 : s_rnge("cdsign", 
					i__1, "f_zzgfcprx__", (ftnlen)909)], 
					"=", &c__0, &c__0, ok, (ftnlen)200, (
					ftnlen)1);
			    }
			}

/*                    End of coordinate loop. */

		    }

/*                 End of system loop. */

		}

/*              End of latitude rate loop. */

	    }

/*           End of longitude rate loop. */

	}

/*        End of latitude loop. */

    }

/*     End of longitude loop. */

    t_success__(ok);
    return 0;
} /* f_zzgfcprx__ */

