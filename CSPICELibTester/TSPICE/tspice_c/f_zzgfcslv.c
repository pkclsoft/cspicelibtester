/* f_zzgfcslv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b27 = 0.;
static doublereal c_b29 = 1.;
static integer c__3 = 3;
static integer c__2000 = 2000;
static integer c__15 = 15;
static integer c__0 = 0;
static integer c__14 = 14;
static integer c_n1 = -1;
static integer c__1 = 1;
static integer c__2 = 2;
static doublereal c_b310 = 1e4;
static integer c__4 = 4;
static doublereal c_b324 = 1e-6;
static integer c__5 = 5;
static doublereal c_b528 = 1e-5;
static doublereal c_b615 = 28800.;
static doublereal c_b683 = 200.;
static doublereal c_b851 = 1e-8;

/* $Procedure      F_ZZGFCSLV ( Test GF coordinate solver ) */
/* Subroutine */ int f_zzgfcslv__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static logical bail;
    static char dref[32];
    static doublereal dvec[3], lats[3], qtol, work[30000]	/* was [2000][
	    15] */;
    extern /* Subroutine */ int zzgfcslv_(char *, char *, char *, char *, 
	    char *, char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, U_fp, U_fp, logical *, 
	    U_fp, U_fp, U_fp, logical *, L_fp, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer i__, j, n;
    static doublereal r__, radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer obsid;
    static char qname[200];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static integer trgid;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal state[6];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime, start;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int bodn2c_(char *, integer *, logical *, ftnlen),
	     t_success__(logical *);
    static doublereal reslt2[2006], reslt3[2006];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal ra;
    extern logical gfbail_();
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    extern doublereal pi_(void);
    static char vecdef[32];
    static doublereal et, lt;
    static integer handle;
    static doublereal cnfine[2006];
    extern /* Subroutine */ int gfrefn_(), gfrepi_();
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int gfstep_(), gfrepu_(), gfrepf_();
    static char abcorr[200], crdnam[32], crdsys[32], relate[40], method[80], 
	    obsrvr[36], target[36], timstr[50];
    static doublereal adjust, dec, et0, et1, et2, finish, maxadj, maxepc, 
	    minadj, minepc, ra0, ra1;
    static char ref[32];
    static doublereal refval, result[2006], srfvec[3], trgepc, lat;
    static integer xn;
    extern /* Subroutine */ int tstlsk_(void);
    extern doublereal rpd_(void), spd_(void);
    static doublereal lon;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     tstpck_(char *, logical *, logical *, ftnlen), tstspk_(char *, 
	    logical *, integer *, ftnlen), natpck_(char *, logical *, logical 
	    *, ftnlen);
    static doublereal tol;
    extern /* Subroutine */ int natspk_(char *, logical *, integer *, ftnlen),
	     bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen);
    static doublereal pos[3];
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), cleard_(integer *, 
	    doublereal *);
    static logical rpt;
    extern /* Subroutine */ int gdpool_(char *, integer *, integer *, integer 
	    *, doublereal *, logical *, ftnlen), pdpool_(char *, integer *, 
	    doublereal *, ftnlen), gfsstp_(doublereal *), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), wnfetd_(doublereal *, integer *, doublereal *, 
	    doublereal *), chcksd_(char *, doublereal *, char *, doublereal *,
	     doublereal *, logical *, ftnlen, ftnlen), spkezr_(char *, 
	    doublereal *, char *, char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen), spkpos_(char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), reclat_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), scardd_(integer *, doublereal *), 
	    subpnt_(char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), sincpt_(char *, char *, doublereal *, char *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen),
	     recrad_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    spkuef_(integer *), delfil_(char *, ftnlen);
    static integer han2;

/* $ Abstract */

/*     Test the GF private coordinate search routine ZZGFCSLV. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF */
/*     coordinate search routine ZZGFCSLV. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 2.1.0, 15-APR-2014 (EDW) */

/*        Added explicit declaration of GFBAIL type. */

/* -    TSPICE Version 2.0.0, 13-MAY-2009 (NJB) */

/*        Updated to use strings representing integers */
/*        as body "names" for at least one test case */
/*        for each of the vector definitions POSDEF, */
/*        SOBDEF, and SINDEF. */

/* -    TSPICE Version 1.0.0, 21-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     Local parameters */


/*     Local parameters */


/*     Maximum length of a coordinate system name. */


/*     Maximum length of a coordinate name. */


/*     Local variables */

/*      CHARACTER*(LNSIZE)    TITLE */

/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFCSLV", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgfcslv.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgfcslv.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK and PCK/FK */
/*     kernels. */

    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     The following error cases involve invalid initialization */
/*     values or missing data discovered at search time. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Bad observer or target name.", (ftnlen)28);
    s_copy(abcorr, "CN+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, ref, (ftnlen)32, (ftnlen)32);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    s_copy(target, "X", (ftnlen)36, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    et0 = et - spd_();
    et1 = et + spd_();
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "Y", (ftnlen)36, (ftnlen)1);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad window count", (ftnlen)16);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__0, work, cnfine, result, (ftnlen)32,
	     (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__14, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c_n1, work, cnfine, result, (ftnlen)32,
	     (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad window size", (ftnlen)15);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__0, &c__15, work, cnfine, result, (ftnlen)32, (
	    ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(WINDOWSTOOSMALL)", ok, (ftnlen)22);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__1, &c__15, work, cnfine, result, (ftnlen)32, (
	    ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(WINDOWSTOOSMALL)", ok, (ftnlen)22);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c_n1, &c__15, work, cnfine, result, (ftnlen)32, (
	    ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(WINDOWSTOOSMALL)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer equals target", (ftnlen)22);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Negative adjustment value", (ftnlen)25);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    adjust = -1.;
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive tolerance value", (ftnlen)28);

/*     Try both zero and negative tolerance values. */

    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    adjust = 0.;
    tol = 0.;
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    tol = -1.;
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Reset TOL to something valid. */

    tol = .001;

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad vector definition", (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    s_copy(vecdef, "ACCELERATION", (ftnlen)32, (ftnlen)12);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad computation method", (ftnlen)22);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction", (ftnlen)25);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None.", (ftnlen)200, (ftnlen)5);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate system", (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "GAUSSIAN", (ftnlen)32, (ftnlen)8);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate name", (ftnlen)19);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad reference frame REF", (ftnlen)23);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "EME2000", (ftnlen)32, (ftnlen)7);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("SINDEF: Reference frame REF not centered on target when VECDEF r"
	    "equires it.", (ftnlen)75);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(method, "NEAR POINT: ELLIPSOID", (ftnlen)80, (ftnlen)21);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SINDEF: Non-existent reference frame DREF", (ftnlen)41);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "EME2000", (ftnlen)32, (ftnlen)7);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad vector DVEC", (ftnlen)15);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "RECTANGULAR", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    cleard_(&c__3, dvec);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii count", (ftnlen)22);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    gdpool_("BODY399_RADII", &c__1, &c__1, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pdpool_("BODY399_RADII", &c__2, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(ref, "ITRF93", (ftnlen)32, (ftnlen)6);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    trgid = 9511010;
    s_copy(ref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    trgid = 301;
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    obsid = 9511010;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF ephemeris data available", (ftnlen)32);

/*     This error is detected post-initialization. */

    s_copy(dref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF orientation data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    trgid = 399;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    obsid = 301;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "ITRF93", (ftnlen)32, (ftnlen)6);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases --- Nat's solar system */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find times when the Alpha-Gamma vector crosses the */
/*     J2000 X-Y plane. This should occur every 12 hours, starting */
/*     at the J2000 epoch. */

    tcase_("Find times when Gamma crosses the J2000 x-y plane.", (ftnlen)50);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 3:00 TDB", &et0, (ftnlen)19);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 3 3:00 TDB", &et1, (ftnlen)19);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
    s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "z", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. */

    gfsstp_(&c_b310);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found 4 roots. */

    n = wncard_(result);
    chcksi_("N", &n, "=", &c__4, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     those we expect. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xtime = (i__ - 1) * 43200.;
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &xtime, &c_b324, ok, (ftnlen)200, (ftnlen)
		1);
	chcksd_(qname, &finish, "~", &xtime, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the Z coordinate at the crossing time. */

/*     In order to decide how accurate the coordinate should be, */
/*     we'll use the speed of Gamma together with the time */
/*     tolerance. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkezr_(target, &start, ref, abcorr, obsrvr, state, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (i__ == 1) {
	    qtol = vnorm_(&state[3]) * 1e-6;
	}
	s_copy(qname, "Z # (0)", (ftnlen)200, (ftnlen)7);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &state[2], "~", &c_b27, &qtol, ok, (ftnlen)200, (
		ftnlen)1);
    }
/* ********************************************************************* */
/* * */
/* *    Normal cases --- "real" solar system */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find times when the Earth-Sun vector crosses the */
/*     Earth equator (the x-y plane of the IAU_EARTH frame). We'll then */
/*     check the planetocentric latitude of the vector at the result */
/*     times. We'll also perform the search looking for 0 position */
/*     Z-component and compare the results of that search. */

    tcase_("Find times when the Sun crosses the IAU_EARTH x-y plane.", (
	    ftnlen)56);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1", &et0, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2001 JAN 1", &et1, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
    s_copy(target, "10", (ftnlen)36, (ftnlen)2);
    s_copy(obsrvr, "399", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "latitudinal", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "latitude", (ftnlen)32, (ftnlen)8);
    cleard_(&c__3, dvec);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. */

    d__1 = spd_() * 100;
    gfsstp_(&d__1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at least one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, ">", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the planetocentric latitude at the crossing time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkpos_(target, &start, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	reclat_(pos, &r__, &lon, &lat);
	s_copy(qname, "Latitude # (0)", (ftnlen)200, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lat, "~", &c_b27, &c_b324, ok, (ftnlen)200, (ftnlen)1)
		;
    }

/* --- Case: ------------------------------------------------------ */


/*     We're going to find times when the Earth-Sun vector attains */
/*     local minima and maxima. */

    tcase_("Find local min/max of Solar latitude as seen from Earth.", (
	    ftnlen)56);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1", &et0, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2005 JAN 1", &et1, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "latitudinal", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "latitude", (ftnlen)32, (ftnlen)8);
    cleard_(&c__3, dvec);
    s_copy(relate, "LOCMAX", (ftnlen)40, (ftnlen)6);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. */

    d__1 = spd_() * 100;
    gfsstp_(&d__1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found five roots. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__5, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the planetocentric latitude at the crossing time. We */
/*     should find the latitude is greater than that at +/- */
/*     TDELTA seconds from each interval start time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    et = start + (j - 2) * 120.;
	    spkpos_(target, &et, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		    ftnlen)32, (ftnlen)200, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    reclat_(pos, &r__, &lon, &lats[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("lats", i__2, "f_zzgfcslv__", (ftnlen)1317)]
		    );
	}
/*         WRITE (*, '(I10,3E24.16)'  ) I, LATS */
	s_copy(qname, "Max: Left side latitude #", (ftnlen)200, (ftnlen)25);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, lats, "<", &lats[1], &c_b324, ok, (ftnlen)200, (ftnlen)
		1);
	s_copy(qname, "Max: Right side latitude #", (ftnlen)200, (ftnlen)26);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lats[2], "<", &lats[1], &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Now test using local minima. */

    s_copy(relate, "locmin", (ftnlen)40, (ftnlen)6);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found five roots. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__5, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the planetocentric latitude at the crossing time. We */
/*     should find the latitude is greater than that at +/- */
/*     TDELTA seconds from each interval start time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    et = start + (j - 2) * 120.;
	    spkpos_(target, &et, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		    ftnlen)32, (ftnlen)200, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    reclat_(pos, &r__, &lon, &lats[(i__2 = j - 1) < 3 && 0 <= i__2 ? 
		    i__2 : s_rnge("lats", i__2, "f_zzgfcslv__", (ftnlen)1395)]
		    );
	}
/*         WRITE (*, '(I10,3E24.16)'  ) I, LATS */
	s_copy(qname, "Min: Left side latitude #", (ftnlen)200, (ftnlen)25);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, lats, ">", &lats[1], &c_b324, ok, (ftnlen)200, (ftnlen)
		1);
	s_copy(qname, "Min: Right side latitude #", (ftnlen)200, (ftnlen)26);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lats[2], ">", &lats[1], &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Find times when the Sun crosses the J2000 prime meridian.", (
	    ftnlen)57);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    ssized_(&c__2000, reslt3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1", &et0, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2001 JAN 1", &et1, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
    s_copy(target, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "Latitudinal", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LONGITUDE", (ftnlen)32, (ftnlen)9);
    cleard_(&c__3, dvec);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. */

    d__1 = spd_() * 100;
    gfsstp_(&d__1);
    d__1 = spd_() * 10;
    gfsstp_(&d__1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at least one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, ">", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the longitude at the crossing time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkpos_(target, &start, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	reclat_(pos, &r__, &lon, &lat);
	s_copy(qname, "Longitude # (0)", (ftnlen)200, (ftnlen)15);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lon, "~", &c_b27, &c_b324, ok, (ftnlen)200, (ftnlen)1)
		;
    }

/*     Now find times when the Sun crosses the J2000 x-z plane. */
/*     Delete times when the X coordinate is negative. The */
/*     result should match that from the longitude search. */

    s_copy(crdsys, "rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Y", (ftnlen)32, (ftnlen)1);
    refval = 0.;

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, reslt2, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Empty the window RESLT3. */

    scardd_(&c__0, reslt3);

/*     Copy the times corresponding to positive X-values to RESLT3. */

    i__1 = wncard_(reslt2);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(reslt2, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkpos_(target, &start, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	if (pos[0] > 0.) {
	    wninsd_(&start, &finish, reslt3);
	}
    }

/*     Make sure we found at as many roots as */
/*     we did on the previous search. */

    xn = wncard_(result);
    n = wncard_(reslt3);
    chcksi_("N (1)", &n, "=", &xn, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the solution times against those from the */
/*     Z-component search. */

    chckad_("RESLT3", reslt3, "~", result, &n, &c_b528, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     We're going to find times when the sub-solar point */
/*     crosses the Earth equator (the x-y plane of the */
/*     IAU_EARTH frame). We'll then check the Z component */
/*     of the vector at the result times. We'll also */
/*     perform the search looking for 0 geodetic latitude */
/*     and compare the results of that search. */

    tcase_("Find times when the sub-solar point on the Earth (near point def"
	    "inition) crosses the IAU_EARTH x-y plane.", (ftnlen)105);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1", &et0, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2001 JAN 1", &et1, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(method, "Near point: Ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "10", (ftnlen)36, (ftnlen)2);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "Geodetic", (ftnlen)32, (ftnlen)8);
    s_copy(crdnam, "latitude", (ftnlen)32, (ftnlen)8);
    cleard_(&c__3, dvec);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. */

    d__1 = spd_() * 100;
    gfsstp_(&d__1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at least one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, ">", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the latitude at the crossing time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkpos_(target, &start, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	reclat_(pos, &r__, &lon, &lat);
	s_copy(qname, "Latitude # (0)", (ftnlen)200, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lat, "~", &c_b27, &c_b324, ok, (ftnlen)200, (ftnlen)1)
		;
    }

/*     Repeat the search using Cartesian coordinates. */

    s_copy(crdsys, "Rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "z", (ftnlen)32, (ftnlen)1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, reslt2, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at as many roots as */
/*     we did on the previous search. */

    xn = n;
    n = wncard_(reslt2);
    chcksi_("N (1)", &n, "=", &xn, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the solution times against those from the */
/*     Z-component search. */

    chckad_("RESLT2", reslt2, "~", result, &n, &c_b528, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find times when the sub-solar point on the Earth (near point def"
	    "inition) crosses the IAU_EARTH prime meridian.", (ftnlen)110);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    ssized_(&c__2000, reslt3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We use a time period of a few days, not a year. */

    str2et_("2000 JAN 1", &et0, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 5", &et1, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "lt+s", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(method, "near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "Earth", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "Sun", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "Iau_Earth", (ftnlen)32, (ftnlen)9);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    s_copy(crdsys, "Latitudinal", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Longitude", (ftnlen)32, (ftnlen)9);
    cleard_(&c__3, dvec);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a size smaller than */
/*     one half day. */

    gfsstp_(&c_b615);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at least one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, ">", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the longitude at the crossing time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	subpnt_(method, target, &start, ref, abcorr, obsrvr, pos, &trgepc, 
		srfvec, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (
		ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	reclat_(pos, &r__, &lon, &lat);
	s_copy(qname, "Longitude # (0)", (ftnlen)200, (ftnlen)15);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lon, "~", &c_b27, &c_b324, ok, (ftnlen)200, (ftnlen)1)
		;
    }

/*     Now find times when the sub-solar point crosses the IAU_EARTH x-z */
/*     plane. Delete times when the X coordinate is negative. The result */
/*     should match that from the longitude search. */

    s_copy(crdsys, "rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "y", (ftnlen)32, (ftnlen)1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, reslt2, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Empty the window RESLT3. */

    scardd_(&c__0, reslt3);

/*     Copy the times corresponding to positive X-values to RESLT3. */

    i__1 = wncard_(reslt2);
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(reslt2, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	spkpos_(target, &start, ref, abcorr, obsrvr, pos, &lt, (ftnlen)36, (
		ftnlen)32, (ftnlen)200, (ftnlen)36);
	if (pos[0] > 0.) {
	    wninsd_(&start, &finish, reslt3);
	}
    }

/*     Make sure we found at as many roots as */
/*     we did on the previous search. */

    xn = wncard_(result);
    n = wncard_(reslt3);
    chcksi_("N (1)", &n, "=", &xn, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the solution times against those from the */
/*     Z-component search. */

    chckad_("RESLT3", reslt3, "~", result, &n, &c_b528, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     We're going to find times when a selected Sun-Earth ray crosses */
/*     the Earth equator (the x-y plane of the IAU_EARTH frame). We'll */
/*     perform the search looking for 0 geodetic latitude and compare */
/*     the results of that search. We'll then check the Z component of */
/*     the vector at the result times. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 crosses the IAU_EARTH x"
	    "-y plane.", (ftnlen)73);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "399", (ftnlen)36, (ftnlen)3);
    s_copy(obsrvr, "10", (ftnlen)36, (ftnlen)2);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "Geodetic", (ftnlen)32, (ftnlen)8);
    s_copy(crdnam, "latitude", (ftnlen)32, (ftnlen)8);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at least one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, ">", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Check the latitude at the crossing time. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	sincpt_(method, target, &start, ref, abcorr, obsrvr, dref, dvec, pos, 
		&trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
		ftnlen)200, (ftnlen)36, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);

/*        Although we're working with geodetic coordinates, we */
/*        can use RECLAT for latitudes close to zero. */

	reclat_(pos, &r__, &lon, &lat);
	sincpt_(method, target, &finish, ref, abcorr, obsrvr, dref, dvec, pos,
		 &trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, 
		(ftnlen)200, (ftnlen)36, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("Intercept FOUND (1)", &found, &c_true, ok, (ftnlen)19);
	reclat_(pos, &r__, &lon, &lat);
	s_copy(qname, "Latitude # (0)", (ftnlen)200, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &lat, "~", &c_b27, &c_b324, ok, (ftnlen)200, (ftnlen)1)
		;
    }

/*     Repeat the search using Cartesian coordinates. */

    s_copy(crdsys, "Rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "z", (ftnlen)32, (ftnlen)1);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, reslt2, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at as many roots as */
/*     we did on the previous search. */

    xn = n;
    n = wncard_(reslt2);
    chcksi_("N (1)", &n, "=", &xn, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the solution times against those from the */
/*     Z-component search. */

    chckad_("RESLT2", reslt2, "~", result, &n, &c_b528, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Using the same observer, target, ray, and time period from */
/*     the previous search, we'll find the times when the absolute */
/*     minimum and maximum intercept of right ascension occur. */
/*     Since orbital motion is a larger effect than the Earth's */
/*     spin in this case, the RA of the intercept is actually */
/*     increasing with time. */

/*     Note: since the intercept longitude passes through 180 degrees */
/*     during the time span of this test, the absolute minimum and */
/*     absolute maximum longitude are both 180 degrees. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 attains extreme RA valu"
	    "es.", (ftnlen)67);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "ra/dec", (ftnlen)32, (ftnlen)6);
    s_copy(crdnam, "right ascension", (ftnlen)32, (ftnlen)15);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "ABSMIN", (ftnlen)40, (ftnlen)6);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Save the epoch of the minimum. */

    minepc = result[6];
    s_copy(relate, "ABSMAX", (ftnlen)40, (ftnlen)6);

/*     Now search for an absolute maximum. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
	repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
	chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (
		ftnlen)1);
    }

/*     Save the epoch of the maximum. */

    maxepc = result[6];

/*     Compare RA at the epochs of minimum and */
/*     maximum values. */

    sincpt_(method, target, &minepc, ref, abcorr, obsrvr, dref, dvec, pos, &
	    trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
	    ftnlen)200, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);
    recrad_(pos, &r__, &ra0, &dec);
    sincpt_(method, target, &maxepc, ref, abcorr, obsrvr, dref, dvec, pos, &
	    trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
	    ftnlen)200, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);
    recrad_(pos, &r__, &ra1, &dec);

/*     Verify that MINEPC < MAXPEC and RA0 < RA1. */

    chcksd_("epoch order", &minepc, "<", &maxepc, &c_b27, ok, (ftnlen)11, (
	    ftnlen)1);
    chcksd_("RA order", &ra0, "<", &ra1, &c_b27, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Repeat the previous searches using an adjustment value of */
/*     1 degree. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 attains extreme RA valu"
	    "es; adjust = 1 deg.", (ftnlen)83);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "ra/dec", (ftnlen)32, (ftnlen)6);
    s_copy(crdnam, "right ascension", (ftnlen)32, (ftnlen)15);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "ABSMIN", (ftnlen)40, (ftnlen)6);
    refval = 0.;
    tol = 1e-6;
    adjust = rpd_() * 1.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. Note that the */
/*     expectation that there's one root is based on */
/*     the geometry of this case. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Save the final epoch of the solution interval. */

    minadj = result[7];

/*     Now search for an absolute maximum. */

    s_copy(relate, "ABSMAX", (ftnlen)40, (ftnlen)6);
    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Save the initial epoch of the solution interval. */

    maxadj = result[6];

/*     Check RA at the epochs of where the adjusted minimum and */
/*     maximum values occur. */

    sincpt_(method, target, &minadj, ref, abcorr, obsrvr, dref, dvec, pos, &
	    trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
	    ftnlen)200, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);
    recrad_(pos, &r__, &ra, &dec);
    d__1 = ra0 + adjust;
    chcksd_("Adjusted min RA", &ra, "~", &d__1, &c_b851, ok, (ftnlen)15, (
	    ftnlen)1);
    sincpt_(method, target, &maxadj, ref, abcorr, obsrvr, dref, dvec, pos, &
	    trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
	    ftnlen)200, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);
    recrad_(pos, &r__, &ra, &dec);
    d__1 = ra1 - adjust;
    chcksd_("Adjusted max RA", &ra, "~", &d__1, &c_b851, ok, (ftnlen)15, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Using the same observer, target, ray, and time period from */
/*     the previous search, we'll find the times when the right */
/*     ascension of the intercept is 180 degrees. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 attains RA 180 degrees.",
	     (ftnlen)64);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "ra/dec", (ftnlen)32, (ftnlen)6);
    s_copy(crdnam, "right ascension", (ftnlen)32, (ftnlen)15);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = pi_();
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Make sure that the start and finish times are */
/*     close together. */

    wnfetd_(result, &c__1, &start, &finish);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
    chcksd_(qname, &start, "~", &finish, &c_b324, ok, (ftnlen)200, (ftnlen)1);

/*     Save the epoch of this root for re-use. */

    et2 = start;

/*     Check the RA of the intercept at the start time. */

    sincpt_(method, target, &et2, ref, abcorr, obsrvr, dref, dvec, pos, &
	    trgepc, srfvec, &found, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
	    ftnlen)200, (ftnlen)36, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("Intercept FOUND (0)", &found, &c_true, ok, (ftnlen)19);
    recrad_(pos, &r__, &ra0, &dec);
    d__1 = pi_();
    chcksd_("RA0", &ra0, "~", &d__1, &c_b324, ok, (ftnlen)3, (ftnlen)1);

/*     Repeat the search using Cartesian coordinates. We're looking */
/*     for a time when the Y-coordinate is zero. For this case, we */
/*     happen not to have any roots for positive X, so we should */
/*     find the same epoch as we did in the RA search. */


    s_copy(crdsys, "Rectangular", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Y", (ftnlen)32, (ftnlen)1);
    refval = 0.;

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, reslt2, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found at as many roots as */
/*     we did on the previous search. */

    xn = n;
    n = wncard_(reslt2);
    chcksi_("N (1)", &n, "=", &xn, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check the solution times against those from the */
/*     Z-component search. */

    chckad_("RESLT2", reslt2, "~", result, &n, &c_b528, ok, (ftnlen)6, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Using the same observer, target, ray, and time period from */
/*     the previous search, we'll find the times when the right */
/*     ascension of the intercept is less than 180 degrees. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 has RA < 180 degrees.", (
	    ftnlen)62);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "ra/dec", (ftnlen)32, (ftnlen)6);
    s_copy(crdnam, "right ascension", (ftnlen)32, (ftnlen)15);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, "<", (ftnlen)40, (ftnlen)1);
    refval = pi_();
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     We expect the start and finish times to match MINPEC and ET2, */
/*     within a reasonable tolerance. */

    wnfetd_(result, &c__1, &start, &finish);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
    chcksd_(qname, &start, "~", &minepc, &c_b528, ok, (ftnlen)200, (ftnlen)1);
    s_copy(qname, "Interval end # (0)", (ftnlen)200, (ftnlen)18);
    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
    chcksd_(qname, &finish, "~", &et2, &c_b528, ok, (ftnlen)200, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Using the same observer, target, ray, and time period from */
/*     the previous search, we'll find the times when the right */
/*     ascension of the intercept is greater than 180 degrees. */

    tcase_("J2000-fixed Sun-Earth ray at 2000 Mar 21 has RA > 180 degrees.", (
	    ftnlen)62);
    ssized_(&c__2000, cnfine);
    ssized_(&c__2000, result);
    ssized_(&c__2000, reslt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 20", &et0, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 MAR 22", &et1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "LT+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    s_copy(crdsys, "ra/dec", (ftnlen)32, (ftnlen)6);
    s_copy(crdnam, "right ascension", (ftnlen)32, (ftnlen)15);

/*     Set DVEC. */

    str2et_("2000 MAR 21", &et, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)36, (ftnlen)
	    32, (ftnlen)200, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(relate, ">", (ftnlen)40, (ftnlen)1);
    refval = pi_();
    tol = 1e-6;
    adjust = 0.;
    rpt = FALSE_;
    bail = FALSE_;

/*     Set the search step size. We need a step small enough */
/*     to capture the existence window, which we expect to be */
/*     about 7 minutes long, based on the Earth's orbital speed */
/*     and the Earth's ellipsoidal radii. */

    gfsstp_(&c_b683);

/*     Perform the search. */

    zzgfcslv_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__2000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure we found exactly one root. */

    n = wncard_(result);
    chcksi_("N (0)", &n, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     We expect the start and finish times to match ET2 and MAXEPC, */
/*     within a reasonable tolerance. */

    wnfetd_(result, &c__1, &start, &finish);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20);
    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
    chcksd_(qname, &start, "~", &et2, &c_b528, ok, (ftnlen)200, (ftnlen)1);
    s_copy(qname, "Interval end # (0)", (ftnlen)200, (ftnlen)18);
    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)200);
    chcksd_(qname, &finish, "~", &maxepc, &c_b528, ok, (ftnlen)200, (ftnlen)1)
	    ;

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgfcslv.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfcslv__ */

