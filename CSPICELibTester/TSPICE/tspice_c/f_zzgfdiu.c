/* f_zzgfdiu.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b108 = 1e-12;

/* $Procedure F_ZZGFDIU ( ZZGFDIU family tests ) */
/* Subroutine */ int f_zzgfdiu__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal dist;
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int zzgfdidc_(U_fp, doublereal *, logical *), 
	    zzgfdiin_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    zzgfdigq_(doublereal *, doublereal *), zzgfudlt_(S_fp, doublereal 
	    *, logical *);
    static integer i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern doublereal dpmin_(void), dpmax_(void);
    static doublereal state[6];
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xdist;
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *), str2et_(char *, 
	    doublereal *, ftnlen);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    static logical decres;
    static char abcorr[80];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static doublereal refval;
    static char target[36];
    static logical xdecrs;
    extern /* Subroutine */ int spkuef_(integer *);
    static char obsrvr[36], timstr[50];
    static doublereal et0;
    static logical lssthn, xlsthn;
    extern /* Subroutine */ int tstlsk_(void), spkezr_(char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal stepsz;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen);
    extern logical odd_(integer *);
    extern /* Subroutine */ int udf_();
    extern doublereal spd_(void);
    extern /* Subroutine */ int zzgfref_(doublereal *);

/* $ Abstract */

/*     This routine tests the SPICELIB utility package */

/*        ZZGFDIU */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB geometry utility package ZZGFDIU. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 18-NOV-2013 (EDW) */

/*        Test families update to accommodate ZZGFCOU modifications */
/*        required by ZZGFRELX. */

/*        UDF call replaced ZZUDF. */

/* -    TSPICE Version 1.0.0, 22-MAY-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFDIU", (ftnlen)9);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load an SPK file as well. */

    tstspk_("zzgfdiu.bsp", &c_true, &handle, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL CHCKXC ( .FALSE., ' ', OK ) */
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */


/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized target or observer", (ftnlen)31);
    refval = 1e5;
    zzgfref_(&refval);
    zzgfdiin_("MOOON", "NONE", "EARTH", (ftnlen)5, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzgfdiin_("MOON", "NONE", "EEARTH", (ftnlen)4, (ftnlen)4, (ftnlen)6);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and observer are identical", (ftnlen)33);
    zzgfdiin_("MOON", "LT+S", "MOON", (ftnlen)4, (ftnlen)4, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction specifiers", (ftnlen)36);
    zzgfdiin_("MOON", "S", "EARTH", (ftnlen)4, (ftnlen)1, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfdiin_("MOON", "XS", "EARTH", (ftnlen)4, (ftnlen)2, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfdiin_("MOON", "RLT", "EARTH", (ftnlen)4, (ftnlen)3, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfdiin_("MOON", "XRLT", "EARTH", (ftnlen)4, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfdiin_("MOON", "z", "EARTH", (ftnlen)4, (ftnlen)1, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFDIDC: Ephemeris data unavailable", (ftnlen)36);
    s_copy(timstr, "2008 MAY 22", (ftnlen)50, (ftnlen)11);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfdiin_("GASPRA", "LT+S", "EARTH", (ftnlen)6, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfdidc_((U_fp)udf_, &et, &decres);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFDILT: Ephemeris data unavailable", (ftnlen)36);
    s_copy(timstr, "2008 MAY 22", (ftnlen)50, (ftnlen)11);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfdiin_("GASPRA", "LT+S", "EARTH", (ftnlen)6, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfudlt_((S_fp)zzgfdigq_, &et, &lssthn);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFDIGQ: Ephemeris data unavailable", (ftnlen)36);
    s_copy(timstr, "2008 MAY 22", (ftnlen)50, (ftnlen)11);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfdiin_("GASPRA", "LT+S", "EARTH", (ftnlen)6, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfdigq_(&et, &dist);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     Check values returned by ZZGFDIU entry points for each aberration */
/*     correction and a variety of times. */

    s_copy(timstr, "2008 MAY 22", (ftnlen)50, (ftnlen)11);
    str2et_(timstr, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    stepsz = spd_() * 20.;
    for (i__ = 1; i__ <= 9; ++i__) {

/*        Perform tests at a series of times. */

	s_copy(abcorr, corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_zzgfdiu__", (ftnlen)368)) * 80, (
		ftnlen)80, (ftnlen)80);
	for (j = 1; j <= 50; ++j) {
	    et = et0 + (j - 1) * stepsz;

/*           Use different observer-target pairs. */

	    if (odd_(&j)) {
		s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
		s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
	    } else {
		s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
		s_copy(target, "MARS", (ftnlen)36, (ftnlen)4);
	    }

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "ZZGFDIGQ: check #-# distance. ABCORR = #.", (
		    ftnlen)80, (ftnlen)41);
	    repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80, (ftnlen)80);
	    tcase_(title, (ftnlen)80);
	    refval = 4e5;
	    zzgfref_(&refval);
	    zzgfdiin_(target, abcorr, obsrvr, (ftnlen)36, (ftnlen)80, (ftnlen)
		    36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    zzgfdigq_(&et, &dist);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    spkezr_(target, &et, "J2000", abcorr, obsrvr, state, &lt, (ftnlen)
		    36, (ftnlen)5, (ftnlen)80, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xdist = vnorm_(state);

/*           We expect a pretty good match. */

	    chcksd_("DIST", &dist, "~/", &xdist, &c_b108, ok, (ftnlen)4, (
		    ftnlen)2);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "ZZGFDILT: check #-# \"is less than?\" state. ABCO"
		    "RR = #.", (ftnlen)80, (ftnlen)54);
	    repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80, (ftnlen)80);
	    tcase_(title, (ftnlen)80);
	    xlsthn = xdist < refval;
	    zzgfudlt_((S_fp)zzgfdigq_, &et, &lssthn);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("LSSTHN", &lssthn, &xlsthn, ok, (ftnlen)6);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "ZZGFREF: check #-# \"is less than?\" state. ABCOR"
		    "R = #.", (ftnlen)80, (ftnlen)53);
	    repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80, (ftnlen)80);
	    tcase_(title, (ftnlen)80);
	    xlsthn = xdist < refval;

/*           Set the reference value so as to invert the */
/*           expected relationship to the reference value. */

	    if (xlsthn) {
		d__1 = dpmin_();
		zzgfref_(&d__1);
	    } else {
		d__1 = dpmax_();
		zzgfref_(&d__1);
	    }
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    xlsthn = ! xlsthn;
	    zzgfudlt_((S_fp)zzgfdigq_, &et, &lssthn);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("LSSTHN", &lssthn, &xlsthn, ok, (ftnlen)6);

/* ---- Case ------------------------------------------------------------- */

	    s_copy(title, "ZZGFDIDC: check #-# \"is decreasing?\" state. ABC"
		    "ORR = #.", (ftnlen)80, (ftnlen)55);
	    repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    36, (ftnlen)80);
	    repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80, (ftnlen)80);
	    tcase_(title, (ftnlen)80);
	    xdecrs = vdot_(state, &state[3]) < 0.;
	    zzgfdidc_((U_fp)udf_, &et, &decres);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DECRES", &decres, &xdecrs, ok, (ftnlen)6);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgfdiu.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfdiu__ */

