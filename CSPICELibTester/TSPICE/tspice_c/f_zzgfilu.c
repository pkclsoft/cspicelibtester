/* f_zzgfilu.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__25 = 25;
static integer c__0 = 0;
static integer c__12 = 12;
static integer c__399 = 399;
static integer c__1 = 1;
static integer c__2 = 2;
static doublereal c_b153 = 0.;
static integer c__3 = 3;
static doublereal c_b291 = 1e-13;

/* $Procedure      F_ZZGFILU ( ZZGFILU family tests ) */
/* Subroutine */ int f_zzgfilu__(logical *ok)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int uddc_();
    char name__[80];
    logical xdec;
    doublereal last, step;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzgfildc_(
	    U_fp, doublereal *, logical *), zzgfilin_(char *, char *, char *, 
	    char *, char *, char *, char *, doublereal *, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), zzgfilgq_(doublereal *, 
	    doublereal *);
    char time0[50];
    extern /* Subroutine */ int zzilusta_(char *, char *, char *, doublereal *
	    , char *, char *, char *, doublereal *, doublereal *, doublereal *
	    , doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    integer i__, n;
    doublereal angle;
    logical isdec;
    doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal phase;
    integer octid;
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    char illum[36];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal first;
    extern /* Subroutine */ int spkw08_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    bodn2c_(char *, integer *, logical *, ftnlen), t_success__(
	    logical *), str2et_(char *, doublereal *, ftnlen);
    doublereal et;
    integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    doublereal incdnc;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen), chcksl_(char *, logical *, logical *, 
	    logical *, ftnlen);
    char abcorr[5], method[80], angtyp[25], fixref[32], obsrvr[36], target[36]
	    ;
    doublereal emissn, emista[2], et0, incsta[2], normal[3], phssta[2], 
	    spoint[3], srfvec[3], states[12]	/* was [6][2] */, trgepc;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     natpck_(char *, logical *, logical *, ftnlen), tstpck_(char *, 
	    logical *, logical *, ftnlen), tstlsk_(void), lmpool_(char *, 
	    integer *, ftnlen), spkopn_(char *, char *, integer *, integer *, 
	    ftnlen, ftnlen), spkcls_(integer *), furnsh_(char *, ftnlen), 
	    srfrec_(integer *, doublereal *, doublereal *, doublereal *), 
	    spkpos_(char *, doublereal *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), bodvrd_(char *, 
	    char *, integer *, integer *, doublereal *, ftnlen, ftnlen), 
	    surfnm_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), illumg_(char *, char *, char *, doublereal *, char 
	    *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen), spkuef_(integer *), unload_(char *, 
	    ftnlen);
    doublereal lat;
    extern doublereal rpd_(void), spd_(void);
    doublereal lon;
    char txt[80*25];
    integer han1;

/* $ Abstract */

/*     This routine tests entry points of the SPICELIB routine */

/*        ZZGFILU */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the higher-level SPICELIB */
/*     geometry routine GFILUM. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-MAY-2012 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/* ---- Case ------------------------------------------------------------- */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFILU", (ftnlen)9);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    tstspk_("generic.bsp", &c_true, &han1, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a confinement window. Initialize this and */
/*     the result window. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */


/*     This isn't a test, but we'll call it that so we'll have */
/*     a meaningful label in any error messages that arise. */

    tcase_("Create OCTL kernels.", (ftnlen)20);

/*     As mentioned, we go with a frame that's more convenient than */
/*     ITRF93: */

    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Prepare a frame kernel in a string array. */

    s_copy(txt, "FRAME_OCTL_TOPO            =  1398962", (ftnlen)80, (ftnlen)
	    37);
    s_copy(txt + 80, "FRAME_1398962_NAME         =  'OCTL_TOPO' ", (ftnlen)80,
	     (ftnlen)42);
    s_copy(txt + 160, "FRAME_1398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 240, "FRAME_1398962_CLASS_ID     =  1398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 320, "FRAME_1398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 400, "OBJECT_398962_FRAME        =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 480, "TKFRAME_1398962_RELATIVE   =  'IAU_EARTH' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 560, "TKFRAME_1398962_SPEC       =  'ANGLES' ", (ftnlen)80, (
	    ftnlen)39);
    s_copy(txt + 640, "TKFRAME_1398962_UNITS      =  'DEGREES' ", (ftnlen)80, 
	    (ftnlen)40);
    s_copy(txt + 720, "TKFRAME_1398962_AXES       =  ( 3, 2, 3 )", (ftnlen)80,
	     (ftnlen)41);
    s_copy(txt + 800, "TKFRAME_1398962_ANGLES     =  ( -242.3171620000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 880, "                                 -55.6182509000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 960, "                                 180.0000000000000  )",
	     (ftnlen)80, (ftnlen)53);
    s_copy(txt + 1040, "NAIF_BODY_NAME            +=  'OCTL' ", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1120, "NAIF_BODY_CODE            +=  398962", (ftnlen)80, (
	    ftnlen)36);

/*     It will be convenient to have a version of this frame that */
/*     has the +Z axis pointed down instead of up. */

    s_copy(txt + 1200, "FRAME_OCTL_FLIP            =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1280, "FRAME_2398962_NAME         =  'OCTL_FLIP' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1360, "FRAME_2398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 1440, "FRAME_2398962_CLASS_ID     =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1520, "FRAME_2398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 1600, "TKFRAME_2398962_RELATIVE   =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1680, "TKFRAME_2398962_SPEC       =  'ANGLES' ", (ftnlen)80, 
	    (ftnlen)39);
    s_copy(txt + 1760, "TKFRAME_2398962_UNITS      =  'DEGREES' ", (ftnlen)80,
	     (ftnlen)40);
    s_copy(txt + 1840, "TKFRAME_2398962_AXES       =  ( 3, 2, 3 )", (ftnlen)
	    80, (ftnlen)41);
    s_copy(txt + 1920, "TKFRAME_2398962_ANGLES     =  ( 0, 180.0, 0 ) ", (
	    ftnlen)80, (ftnlen)46);
    lmpool_(txt, &c__25, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now create an SPK file containing a type 8 segment for OCTL. */

    spkopn_("octl_test.bsp", "octl_test.bsp", &c__0, &handle, (ftnlen)13, (
	    ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize both states to zero. */

    cleard_(&c__12, states);

/*     The first position component: */

    spoint[0] = -2448.937761729;
    spoint[1] = -4667.935793438;
    spoint[2] = 3582.74849943;
    vequ_(spoint, states);

/*     The second position matches the first: we don't model */
/*     plate motion. */

    vequ_(spoint, &states[6]);

/*     Time bounds for the segment: */

    first = spd_() * -50 * 365.25;
    step = spd_() * 100 * 365.25;
    last = first + step - 1e-6;

/*     Get the OCTL ID code from the kernel we just */
/*     loaded. */

    bodn2c_("OCTL", &octid, &found, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Write the segment. */

    spkw08_(&handle, &octid, &c__399, fixref, &first, &last, "octl", &c__1, &
	    c__2, states, &first, &step, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now load the OCTL SPK file. */

    furnsh_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Error cases for ZZGFILIN */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized value of FIXREF.", (ftnlen)29);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfilin_(method, angtyp, target, illum, "XYZ", abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)3, (ftnlen)
	    5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("FIXREF is not centered on the target body.", (ftnlen)42);
    zzgfilin_(method, angtyp, target, illum, "IAU_MARS", abcorr, obsrvr, 
	    spoint, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)8,
	     (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad computation method.", (ftnlen)23);
    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    s_copy(method, "DSK", (ftnlen)80, (ftnlen)3);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad illumination angle type", (ftnlen)27);
    s_copy(angtyp, "XYZ", (ftnlen)25, (ftnlen)3);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction values", (ftnlen)32);
    zzgfilin_(method, angtyp, target, illum, fixref, "S", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)1, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfilin_(method, angtyp, target, illum, fixref, "XS", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)2, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfilin_(method, angtyp, target, illum, fixref, "RLT", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)3, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfilin_(method, angtyp, target, illum, fixref, "XRLT", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)4, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfilin_(method, angtyp, target, illum, fixref, "Z", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)1, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Transmission aberration correction values.", (ftnlen)42);
    zzgfilin_(method, angtyp, target, illum, fixref, "XLT", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)3, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    zzgfilin_(method, angtyp, target, illum, fixref, "XCN", obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)3, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    zzgfilin_(method, angtyp, target, illum, fixref, "XLT+S", obsrvr, spoint, 
	    (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    zzgfilin_(method, angtyp, target, illum, fixref, "XCN+S", obsrvr, spoint, 
	    (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized target, observer, or illumination source.", (ftnlen)
	    54);
    zzgfilin_(method, angtyp, "X", illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)1, (ftnlen)36, (ftnlen)32, (ftnlen)
	    5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzgfilin_(method, angtyp, target, "X", fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)1, (ftnlen)32, (ftnlen)
	    5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, "X", spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)1);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and observer are identical.", (ftnlen)34);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, target, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Target and illumination source are identical.", (ftnlen)45);
    zzgfilin_(method, angtyp, target, target, fixref, abcorr, obsrvr, spoint, 
	    (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Error cases for ZZGFILDC */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: No SPK data for observer", (ftnlen)34);
    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, "GASPRA", spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfildc_((U_fp)uddc_, &c_b153, &isdec);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: No SPK data for target", (ftnlen)32);
    s_copy(fixref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    zzgfilin_(method, angtyp, "GASPRA", illum, fixref, abcorr, obsrvr, spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)6, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfildc_((U_fp)uddc_, &c_b153, &isdec);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: No SPK data for illumination source", (ftnlen)45);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzgfilin_(method, angtyp, target, "GASPRA", fixref, abcorr, obsrvr, 
	    spoint, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)6, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfildc_((U_fp)uddc_, &c_b153, &isdec);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGILDC: No PCK orientation data for target.", (ftnlen)44);
    zzgfilin_(method, angtyp, target, illum, "ITRF93", abcorr, obsrvr, spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)6, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfildc_((U_fp)uddc_, &c_b153, &isdec);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Error cases for ZZGFILGQ */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: No SPK data for observer", (ftnlen)34);
    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, "GASPRA", spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfilgq_(&c_b153, &angle);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: No SPK data for target", (ftnlen)32);
    s_copy(fixref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    zzgfilin_(method, angtyp, "GASPRA", illum, fixref, abcorr, obsrvr, spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)6, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfilgq_(&c_b153, &angle);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: No SPK data for illumination source", (ftnlen)45);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzgfilin_(method, angtyp, target, "GASPRA", fixref, abcorr, obsrvr, 
	    spoint, (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)6, (ftnlen)32,
	     (ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfilgq_(&c_b153, &angle);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGILGQ: No PCK orientation data for target.", (ftnlen)44);
    zzgfilin_(method, angtyp, target, illum, "ITRF93", abcorr, obsrvr, spoint,
	     (ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)6, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfilgq_(&c_b153, &angle);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: check slope of phase angle angles for a list of times.",
	     (ftnlen)64);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    spkpos_("OCTL", &c_b153, fixref, "NONE", target, spoint, &lt, (ftnlen)4, (
	    ftnlen)32, (ftnlen)4, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Phase angle: */

    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the normal vector required by ZZILUSTA: */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(radii, &radii[1], &radii[2], spoint, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfildc_((U_fp)uddc_, &et, &isdec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
		normal, phssta, incsta, emista, (ftnlen)80, (ftnlen)36, (
		ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xdec = phssta[1] < 0.;

/*        Maintenance note: verify that both true and false */
/*        values of XDEC are generated by this test case. */

/*        WRITE (*,*) XDEC */
	s_copy(name__, "Phase angle decreasing #*", (ftnlen)80, (ftnlen)25);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksl_(name__, &isdec, &xdec, ok, (ftnlen)80);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: check slope of emission angle angles for a list of tim"
	    "es.", (ftnlen)67);

/*     Emission angle: */

    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the normal vector required by ZZILUSTA: */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(radii, &radii[1], &radii[2], spoint, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfildc_((U_fp)uddc_, &et, &isdec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
		normal, phssta, incsta, emista, (ftnlen)80, (ftnlen)36, (
		ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xdec = emista[1] < 0.;

/*        Maintenance note: verify that both true and false */
/*        values of XDEC are generated by this test case. */

/*        WRITE (*,*) XDEC */
	s_copy(name__, "Emission angle decreasing #*", (ftnlen)80, (ftnlen)28)
		;
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksl_(name__, &isdec, &xdec, ok, (ftnlen)80);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILDC: check slope of incidence angle angles for a list of ti"
	    "mes.", (ftnlen)68);

/*     Emission angle: */

    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the normal vector required by ZZILUSTA: */

    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(radii, &radii[1], &radii[2], spoint, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfildc_((U_fp)uddc_, &et, &isdec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
		normal, phssta, incsta, emista, (ftnlen)80, (ftnlen)36, (
		ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	xdec = incsta[1] < 0.;

/*        Maintenance note: verify that both true and false */
/*        values of XDEC are generated by this test case. */

/*        WRITE (*,*) XDEC */
	s_copy(name__, "Incidence angle decreasing #*", (ftnlen)80, (ftnlen)
		29);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksl_(name__, &isdec, &xdec, ok, (ftnlen)80);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: get illumination incidence angles for a list of times.",
	     (ftnlen)64);
    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    spkpos_("OCTL", &c_b153, fixref, "NONE", target, spoint, &lt, (ftnlen)4, (
	    ftnlen)32, (ftnlen)4, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Illumination incidence angle: */

    s_copy(angtyp, "INCIDENCE", (ftnlen)25, (ftnlen)9);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfilgq_(&et, &angle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	illumg_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, &
		trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)80, (ftnlen)
		36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(name__, "Illum incidence (*)", (ftnlen)80, (ftnlen)19);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(name__, &angle, "~/", &incdnc, &c_b291, ok, (ftnlen)80, (
		ftnlen)2);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: get emission angles for a list of times.", (ftnlen)50);

/*     Illumination incidence angle: */

    s_copy(angtyp, "EMISSION", (ftnlen)25, (ftnlen)8);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfilgq_(&et, &angle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	illumg_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, &
		trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)80, (ftnlen)
		36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(name__, "Emission angle #*", (ftnlen)80, (ftnlen)17);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(name__, &angle, "~/", &emissn, &c_b291, ok, (ftnlen)80, (
		ftnlen)2);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFILGQ: get phase angles for a list of times.", (ftnlen)47);

/*     Phase angle: */

    s_copy(angtyp, "PHASE", (ftnlen)25, (ftnlen)5);
    zzgfilin_(method, angtyp, target, illum, fixref, abcorr, obsrvr, spoint, (
	    ftnlen)80, (ftnlen)25, (ftnlen)36, (ftnlen)36, (ftnlen)32, (
	    ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et0 = 0.;
    for (i__ = 1; i__ <= 100; ++i__) {
	et = et0 + (i__ - 1) * spd_();
	zzgfilgq_(&et, &angle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	illumg_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, &
		trgepc, srfvec, &phase, &incdnc, &emissn, (ftnlen)80, (ftnlen)
		36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	s_copy(name__, "Phase angle #*", (ftnlen)80, (ftnlen)14);
	repmi_(name__, "*", &i__, name__, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(name__, &angle, "~/", &phase, &c_b291, ok, (ftnlen)80, (
		ftnlen)2);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK files. */

    spkuef_(&han1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfilu__ */

