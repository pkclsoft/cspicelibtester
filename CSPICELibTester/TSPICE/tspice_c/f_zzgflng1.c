/* f_zzgflng1.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b27 = 0.;
static doublereal c_b29 = 1.;
static integer c__3 = 3;
static integer c__40000 = 40000;
static integer c__14 = 14;
static integer c__0 = 0;
static integer c_n1 = -1;
static integer c__15 = 15;
static integer c__1 = 1;
static integer c__2 = 2;

/* $Procedure      F_ZZGFLNG1 ( Test GF longitude solver, part 1 ) */
/* Subroutine */ int f_zzgflng1__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static char dref[32];
    static doublereal dvec[3], work[600000]	/* was [40000][15] */;
    extern /* Subroutine */ int zzgflong_(char *, char *, char *, char *, 
	    char *, char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, U_fp, U_fp, logical *, 
	    U_fp, U_fp, U_fp, logical *, L_fp, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static integer obsid;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static integer trgid;
    static logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen), bodn2c_(char *, 
	    integer *, logical *, ftnlen), t_success__(logical *), str2et_(
	    char *, doublereal *, ftnlen);
    extern logical gfbail_();
    static doublereal et;
    static integer handle;
    static char vecdef[32];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), scardd_(
	    integer *, doublereal *), delfil_(char *, ftnlen);
    static doublereal cnfine[40006];
    extern /* Subroutine */ int gfrefn_(), gfrepi_();
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int gfstep_(), gfrepu_(), gfrepf_();
    static char abcorr[200], crdnam[32], crdsys[32], relate[40], method[80], 
	    obsrvr[36], target[36], timstr[35];
    static doublereal adjust, et0, et1, refval, result[40006];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen), natpck_(char *, 
	    logical *, logical *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), bodvrd_(char *, char *, integer *, integer *, 
	    doublereal *, ftnlen, ftnlen), ssized_(integer *, doublereal *), 
	    wninsd_(doublereal *, doublereal *, doublereal *), gdpool_(char *,
	     integer *, integer *, integer *, doublereal *, logical *, ftnlen)
	    ;
    static char ref[32];
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen), spkuef_(integer *);
    extern doublereal spd_(void);
    static doublereal tol;
    static logical rpt;
    static integer han2;

/* $ Abstract */

/*     Test the GF private longitude search routine ZZGFLONG. */
/*     This family tests only exception handling. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any exceptions. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests exception handling performed by the private */
/*     SPICELIB GF coordinate search routine ZZGFLONG. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */
/*     E.D. Wright    (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 15-APR-2014 (EDW) */

/*        Added explicit declaration of GFBAIL type. */

/* -    TSPICE Version 1.0.0, 21-FEB-2009 (NJB) (EDW) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     Local parameters */


/*     Maximum length of a coordinate system name. */


/*     Maximum length of a coordinate name. */


/*     Local variables */


/*     Saved all. */



/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFLNG1", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgflong.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgflong.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK and PCK/FK */
/*     kernels. */

    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/*     The following error cases involve invalid initialization */
/*     values or missing data discovered at search time. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Bad window count", (ftnlen)16);
    s_copy(abcorr, "CN+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, ref, (ftnlen)32, (ftnlen)32);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)35, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)35);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    et0 = et - spd_();
    et1 = et + spd_();
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__14, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__0, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__0, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c_n1, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad operator", (ftnlen)12);
    s_copy(relate, "==", (ftnlen)40, (ftnlen)2);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad observer or target name.", (ftnlen)28);
    s_copy(abcorr, "CN+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, ref, (ftnlen)32, (ftnlen)32);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)35, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)35);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    s_copy(target, "X", (ftnlen)36, (ftnlen)1);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    et0 = et - spd_();
    et1 = et + spd_();
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "Y", (ftnlen)36, (ftnlen)1);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("Observer equals target", (ftnlen)22);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "EARTH", (ftnlen)36, (ftnlen)5);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Negative adjustment value", (ftnlen)25);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    adjust = -1.;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive tolerance value", (ftnlen)28);

/*     Try both zero and negative tolerance values. */

    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    adjust = 0.;
    tol = 0.;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    tol = -1.;
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Reset TOL to something valid. */

    tol = .001;

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad vector definition", (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    s_copy(vecdef, "ACCELERATION", (ftnlen)32, (ftnlen)12);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad computation method", (ftnlen)22);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction", (ftnlen)25);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "None.", (ftnlen)200, (ftnlen)5);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate system", (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "GAUSSIAN", (ftnlen)32, (ftnlen)8);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-longitude/RA system", (ftnlen)23);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "RECTANGULAR", (ftnlen)32, (ftnlen)11);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate name", (ftnlen)19);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad reference frame REF", (ftnlen)23);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "EME2000", (ftnlen)32, (ftnlen)7);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("SINDEF: Reference frame REF not centered on target when VECDEF r"
	    "equires it.", (ftnlen)75);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(method, "NEAR POINT: ELLIPSOID", (ftnlen)80, (ftnlen)21);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("SINDEF: Non-existent reference frame DREF", (ftnlen)41);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "EME2000", (ftnlen)32, (ftnlen)7);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(NOFRAME)", ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad vector DVEC", (ftnlen)15);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(abcorr, "XCN+S", (ftnlen)200, (ftnlen)5);
    s_copy(crdsys, "RECTANGULAR", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "Z", (ftnlen)32, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    cleard_(&c__3, dvec);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad target radii count", (ftnlen)22);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)18);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(dref, "J2000", (ftnlen)32, (ftnlen)5);
    gdpool_("BODY399_RADII", &c__1, &c__1, &n, radii, &found, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pdpool_("BODY399_RADII", &c__2, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/*     Restore all three radii. */

    pdpool_("BODY399_RADII", &c__3, radii, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(ref, "ITRF93", (ftnlen)32, (ftnlen)6);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    trgid = 9511010;
    s_copy(ref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);

/*     This error is detected post-initialization. */

    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    trgid = 301;
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    obsid = 9511010;
    s_copy(ref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF ephemeris data available", (ftnlen)32);

/*     This error is detected post-initialization. */

    s_copy(dref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF orientation data available", (ftnlen)34);

/*     This error is detected post-initialization. */

    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    trgid = 399;
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    obsid = 301;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, "ITRF93", (ftnlen)32, (ftnlen)6);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Non-error exceptional cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Empty confinement window", (ftnlen)24);

/*     Start with an empty confinement window and non-empty */
/*     result window; make sure the result window is empty */
/*     on output. */

    s_copy(abcorr, "CN+S", (ftnlen)200, (ftnlen)4);
    s_copy(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (ftnlen)23);
    s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, ref, (ftnlen)32, (ftnlen)32);
    s_copy(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11);
    s_copy(crdnam, "LATITUDE", (ftnlen)32, (ftnlen)8);
    vpack_(&c_b27, &c_b27, &c_b29, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)35, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)35);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
    refval = 0.;
    tol = 1e-6;
    adjust = 0.;
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    et0 = et;
    et1 = et + 1.;
    scardd_(&c__0, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    rpt = FALSE_;
    bail = FALSE_;

/*     Endow RESULT with a single interval. */

    scardd_(&c__0, result);
    wninsd_(&et0, &et1, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, crdsys,
	     crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_, (U_fp)
	    gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, &bail,
	     (L_fp)gfbail_, &c__40000, &c__15, work, cnfine, result, (ftnlen)
	    32, (ftnlen)80, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
	    ftnlen)32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = wncard_(result);
    chcksi_("WNCARD(RESULT)", &i__1, "=", &c__0, &c__0, ok, (ftnlen)14, (
	    ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgflong.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgflng1__ */

