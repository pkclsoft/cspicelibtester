/* f_zzgflng2.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__40000 = 40000;
static integer c__3 = 3;
static doublereal c_b59 = 1e4;
static integer c__15 = 15;
static integer c__1 = 1;
static integer c__0 = 0;
static doublereal c_b73 = 1e-6;
static doublereal c_b85 = 0.;
static doublereal c_b146 = 1.;
static integer c__2 = 2;
static integer c__6 = 6;

/* $Procedure      F_ZZGFLNG2 ( Test GF longitude solver, part 2 ) */
/* Subroutine */ int f_zzgflng2__(logical *ok)
{
    /* Initialized data */

    static char corr[200*9] = "None                                         "
	    "                                                                "
	    "                                                                "
	    "                           " "Lt                                "
	    "                                                                "
	    "                                                                "
	    "                                      " "cN                     "
	    "                                                                "
	    "                                                                "
	    "                                                 " "XlT         "
	    "                                                                "
	    "                                                                "
	    "                                                            " 
	    "xCn                                                            "
	    "                                                                "
	    "                                                                "
	    "         " "LT + s                                              "
	    "                                                                "
	    "                                                                "
	    "                    " "xLt+ S                                   "
	    "                                                                "
	    "                                                                "
	    "                               " "xCn                           "
	    "                                                                "
	    "                                                                "
	    "                                          " " XcN +s            "
	    "                                                                "
	    "                                                                "
	    "                                                     ";
    static char cnam[32*6] = "Longitude                       " "Right  asce"
	    "nsion                " "Longitude                       " "Longi"
	    "tude                       " "Longitude                       " 
	    "Longitude                       ";
    static char csys[32*6] = "Latitudinal                     " "Ra / Dec   "
	    "                     " "Cylindrical                     " "Geode"
	    "tic                        " "Planetographic                  " 
	    "Spherical                       ";
    static char vdef[32*3] = "POSITION                        " "SUB-OBSERVE"
	    "R POINT              " "SURFACE INTERCEPT POINT         ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static logical bail;
    static char dref[32];
    static doublereal dvec[3], qtol, xlon, work[600000]	/* was [40000][15] */;
    static logical xmit;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzgflong_(
	    char *, char *, char *, char *, char *, char *, char *, 
	    doublereal *, char *, char *, char *, doublereal *, doublereal *, 
	    doublereal *, U_fp, U_fp, logical *, U_fp, U_fp, U_fp, logical *, 
	    L_fp, integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    static integer i__;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static doublereal r__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[200];
    extern /* Subroutine */ int errch_(char *, char *, ftnlen, ftnlen), 
	    vpack_(doublereal *, doublereal *, doublereal *, doublereal *), 
	    repmc_(char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), repmf_(char *, char *, doublereal *, integer *, char *, 
	    char *, ftnlen, ftnlen, ftnlen, ftnlen);
    static doublereal ltoff;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static doublereal state[6];
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime, start;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    static logical uselt;
    extern doublereal twopi_(void);
    static doublereal t1, t2;
    extern /* Subroutine */ int t_success__(logical *);
    static integer ac, cc;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    extern logical gfbail_();
    extern doublereal pi_(void);
    static doublereal et;
    static char vecdef[32];
    static integer handle;
    static doublereal lt;
    static integer vi;
    static doublereal cnfine[40006];
    extern /* Subroutine */ int gfrefn_(), gfrepi_();
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int gfstep_(), gfrepu_(), gfrepf_();
    static char abcorr[200], crdnam[32], crdsys[32], dscstr[200], relate[40], 
	    method[80], obsrvr[36], target[36];
    static doublereal adjust, alphrd[3], et0, et1, finish, refval, result[
	    40006], tmprad[3];
    extern logical odd_(integer *);
    static logical attblk[6];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen);
    static char ref[32];
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     natspk_(char *, logical *, integer *, ftnlen);
    static doublereal alt, lat;
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), bodvrd_(char *, char *,
	     integer *, integer *, doublereal *, ftnlen, ftnlen);
    extern doublereal dpr_(void), spd_(void);
    static doublereal lon;
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), cleard_(integer *, doublereal *), gfsstp_(doublereal *), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    static doublereal tol;
    extern /* Subroutine */ int wnfetd_(doublereal *, integer *, doublereal *,
	     doublereal *);
    static doublereal xet, pos[3];
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), spkezr_(
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), reclat_(doublereal 
	    *, doublereal *, doublereal *, doublereal *);
    static logical rpt;
    extern /* Subroutine */ int setmsg_(char *, ftnlen), sigerr_(char *, 
	    ftnlen), vminus_(doublereal *, doublereal *), scardd_(integer *, 
	    doublereal *), recpgr_(char *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen), 
	    wnfltd_(doublereal *, doublereal *), errint_(char *, integer *, 
	    ftnlen), spkuef_(integer *), delfil_(char *, ftnlen);
    static integer han2;

/* $ Abstract */

/*     Test the GF private longitude search routine ZZGFLONG. This */
/*     family tests equality and inequality relation processing. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF coordinate search */
/*     routine ZZGFLONG. This family tests equality and inequality */
/*     relation processing. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 15-APR-2014 (EDW) */

/*        Added explicit declaration of GFBAIL type. */

/* -    TSPICE Version 1.0.0, 21-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     Local parameters */


/*     Maximum length of a coordinate system name. */


/*     Maximum length of a coordinate name. */


/*     Number of aberration correction choices: */


/*     Number of systems that support longitude: */


/*     Number of vector definitions: */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFLNG2", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgflong.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgflong.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK and PCK/FK */
/*     kernels. */

    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Normal cases --- Nat's solar system */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Equality cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     Equality tests: */

/*     We want at least one case that exercises every combination of */
/*     aberration correction and coordinate system. This set of tests */
/*     does that. Later on, we'll test a more complete set of equality */
/*     constraints. */

/*     We're going to find times when the Sun-Beta vector crosses the */
/*     ALPHA_VIEW_XY prime meridian. This should occur every 24 hours, */
/*     starting at the J2000 epoch. */

/*     Loop over the aberration corrections. */

    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 12:04 TDB", &et0, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 12:06 TDB", &et1, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
    s_copy(target, "BETA", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(ref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    for (cc = 1; cc <= 6; ++cc) {
	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)417)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)418)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (ac = 1; ac <= 9; ++ac) {

/* --- Case: ------------------------------------------------------ */

	    s_copy(abcorr, corr + ((i__1 = ac - 1) < 9 && 0 <= i__1 ? i__1 : 
		    s_rnge("corr", i__1, "f_zzgflng2__", (ftnlen)445)) * 200, 
		    (ftnlen)200, (ftnlen)200);
	    s_copy(title, "Sun-Beta vector crosses the ALPHA_VIEW_XY prime m"
		    "eridian. #; #; #", (ftnlen)200, (ftnlen)65);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    zzprscor_(abcorr, attblk, (ftnlen)200);
	    xmit = attblk[4];
	    uselt = attblk[1];
	    cleard_(&c__3, dvec);
	    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
	    refval = 0.;
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Make sure we found 1 root. */

	    n = wncard_(result);
	    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*           Make sure that the start and finish times are */
/*           those we expect. The crossing should occur at */
/*           the midpoint of the occultation of ALPHA by BETA, */
/*           which is 5 minutes after noon TDB, plus or minus */
/*           the light time offset. */

	    if (uselt) {
		if (xmit) {
		    ltoff = -1.;
		} else {
		    ltoff = 1.;
		}
	    } else {
		ltoff = 0.;
	    }
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		xtime = (i__ - 1) * spd_() + 300. + ltoff;
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &start, "~", &xtime, &c_b73, ok, (ftnlen)200, (
			ftnlen)1);
		s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &finish, "~", &xtime, &c_b73, ok, (ftnlen)200, 
			(ftnlen)1);
	    }

/*           Check the longitude at the crossing time. */

/*           In order to decide how accurate the coordinate should be, */
/*           we'll use the angular rate of Beta together with the time */
/*           tolerance. */

	    qtol = twopi_() / spd_() * 1e-6;
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezr_(target, &start, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		reclat_(state, &r__, &lon, &lat);
		s_copy(qname, "Longitude # (0)", (ftnlen)200, (ftnlen)15);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &lon, "~", &c_b85, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }
	}

/*        End of aberration correction loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */


/*     This set of sets exercises every combination of vector definition */
/*     and coordinate system; we don't vary the aberration correction. */
/*     For sub-observer points, we vary the method but don't test every */
/*     combination of method and coordinate system. */

/*     We're going to find times when the each of the following vectors */
/*     crosses the */
/*     ALPHAFIXED prime meridian: */

/*        1) Position of Gamma with respect to Alpha */

/*        2) Sub-Gamma point on Alpha */

/*        3) Intercept of Gamma-to-Alpha ray on surface of Alpha */

/*     This should occur every 24 hours, starting at 6 hours past the */
/*     J2000 epoch. */


    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 17:59 TDB", &et0, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 2 18:01 TDB", &et1, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&et0, &et1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)627)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)628)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 3; ++vi) {

/*           Set the vector definition. */

	    s_copy(vecdef, vdef + (((i__1 = vi - 1) < 3 && 0 <= i__1 ? i__1 : 
		    s_rnge("vdef", i__1, "f_zzgflng2__", (ftnlen)654)) << 5), 
		    (ftnlen)32, (ftnlen)32);

/*           Set the definition method and ray's */
/*           direction. */

	    if (s_cmp(vecdef, "POSITION", (ftnlen)32, (ftnlen)8) == 0) {

/*              Note that for positions, the vector */
/*              is the *opposite* of the sub-observer */
/*              or position vector. It's easiest to */
/*              switch observer and target for this case. */

		s_copy(dscstr, "Alpha-Gamma position", (ftnlen)200, (ftnlen)
			20);
		s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
		s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
		s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
		s_copy(method, " ", (ftnlen)80, (ftnlen)1);
		s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
		cleard_(&c__3, dvec);
	    } else if (s_cmp(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (
		    ftnlen)18) == 0) {
		s_copy(dscstr, "Sub-Gamma point", (ftnlen)200, (ftnlen)15);
		s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
		s_copy(obsrvr, "GAMMA", (ftnlen)36, (ftnlen)5);
		s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);

/*              Vary the sub-observer point definition. */

		if (odd_(&cc)) {
		    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (
			    ftnlen)21);
		} else {
		    s_copy(method, "Intercept: ellipsoid", (ftnlen)80, (
			    ftnlen)20);
		}
		s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
		cleard_(&c__3, dvec);
	    } else if (s_cmp(vecdef, "SURFACE INTERCEPT POINT", (ftnlen)32, (
		    ftnlen)23) == 0) {
		s_copy(dscstr, "Gamma-Alpha ray intercept", (ftnlen)200, (
			ftnlen)25);
		s_copy(target, "ALPHA", (ftnlen)36, (ftnlen)5);
		s_copy(obsrvr, "GAMMA", (ftnlen)36, (ftnlen)5);
		s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
		s_copy(method, "ellipsoid", (ftnlen)80, (ftnlen)9);
		s_copy(dref, "gammafixed", (ftnlen)32, (ftnlen)10);

/*              The pointing direction is along the gammafixed */
/*              frame's +X axis. */

		vpack_(&c_b146, &c_b85, &c_b85, dvec);
	    } else {

/*              This is a backstop case. */

		setmsg_("VECDEF = #", (ftnlen)10);
		errch_("#", vecdef, (ftnlen)1, (ftnlen)32);
		sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# on Alpha 0 meridian. #; #; #; #; #", (ftnlen)200,
		     (ftnlen)36);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    repmc_(title, "#", vecdef, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", method, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    80, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
	    refval = 0.;
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Make sure we found 2 roots. */

	    n = wncard_(result);
	    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              We expect to cross the prime meridian at 6 pm TDB. */

		xtime = (i__ - 1) * spd_() + 21600.;
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &start, "~", &xtime, &c_b73, ok, (ftnlen)200, (
			ftnlen)1);
		s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &finish, "~", &xtime, &c_b73, ok, (ftnlen)200, 
			(ftnlen)1);
	    }

/*           Check the longitude at the crossing time. */

/*           In order to decide how accurate the coordinate should be, */
/*           we'll use the angular rate of Gamma together with the time */
/*           tolerance. */

	    qtol = twopi_() / spd_() * 1e-6;
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute the longitude of the vector we want. */
/*              Because of the special geometry we've set up, */
/*              we can use the observer-target state for all */
/*              cases; just negate the position for the */
/*              sub-observer and intercept definitions. */

		spkezr_(target, &start, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (s_cmp(vecdef, "SUB-OBSERVER POINT", (ftnlen)32, (ftnlen)
			18) == 0 || s_cmp(vecdef, "SURFACE INTERCEPT POINT", (
			ftnlen)32, (ftnlen)23) == 0) {
		    vminus_(state, pos);
		} else {
		    vequ_(state, pos);
		}
		reclat_(pos, &r__, &lon, &lat);
		s_copy(qname, "Longitude # (0)", (ftnlen)200, (ftnlen)15);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &lon, "~", &c_b85, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }
	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */


/*     More '=' cases follow. */

/*     At this point we've tested ZZGFLONG's ability to handle */
/*     vector definitions and aberration corrections in combination */
/*     with different coordinate systems. Now we want to perform */
/*     more robust tests on the root finding aspects of ZZGFLONG. */
/*     We'll stick with geometric position vectors from now on, */
/*     but we'll test each coordinate system. */

/*     Now we want to find the times when the Alpha-Gamma vector's */
/*     longitude hits each integer multiple of pi/8. */
/*     This should occur every 90 minutes. */

    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 17:59 TDB", &et0, (ftnlen)20);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "alphafixed", (ftnlen)32, (ftnlen)10);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "Alpha", (ftnlen)36, (ftnlen)5);
    s_copy(target, "Gamma", (ftnlen)36, (ftnlen)5);
    s_copy(dscstr, "Alpha-Gamma position", (ftnlen)200, (ftnlen)20);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)893)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)894)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 18; ++vi) {

/*           Loop over each reference value. */

	    refval = (vi - 1) * pi_() / 8.;

/*           For planetographic coordinates, set the reference */
/*           value to indicate the same position on the unit */
/*           circle as that used for the other systems. */

	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# on Alpha # (deg) meridian. #; #", (ftnlen)200, (
		    ftnlen)33);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    d__1 = refval * dpr_();
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, "=", (ftnlen)40, (ftnlen)1);
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           We're going to set the confinement window for each */
/*           search so we find two roots. */

	    scardd_(&c__0, cnfine);
	    t1 = et0 + (vi - 1) * 5400.;
	    t2 = t1 + 172800.;
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Make sure we found 2 roots. */

	    n = wncard_(result);
	    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              We expect to cross the VIth reference value at */

/*                 24*(I-1) + 4.5 + 1.5*VI hours */

/*              past noon TDB. */

		xtime = ((i__ - 1) * 24 + vi * 1.5 + 4.5) * 3600.;
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		s_copy(qname, "Interval start # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &start, "~", &xtime, &c_b73, ok, (ftnlen)200, (
			ftnlen)1);
		s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (ftnlen)20)
			;
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);
		chcksd_(qname, &finish, "~", &xtime, &c_b73, ok, (ftnlen)200, 
			(ftnlen)1);
	    }

/*           Check the longitude at the crossing time. */

/*           In order to decide how accurate the coordinate should be, */
/*           we'll use the angular rate of Gamma together with the time */
/*           tolerance. Adjust the tolerance for the units we're using */
/*           in the CHCKSD call. */

	    qtol = dpr_() * (twopi_() / spd_()) * 1e-6;
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute the longitude of the vector we want. */

		spkezr_(target, &start, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &lon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &lon, &lat);
		}
		s_copy(qname, "Longitude # (deg)", (ftnlen)200, (ftnlen)17);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);

/*              Adjust the longitude by 2*pi if it's too far */
/*              from the expected value. */

		if (lon > refval + 1.) {
		    lon -= twopi_();
		} else if (lon < refval - 1.) {
		    lon += twopi_();
		}
		d__1 = lon * dpr_();
		d__2 = refval * dpr_();
		chcksd_(qname, &d__1, "~", &d__2, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }
	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */

/* ********************************************************************* */
/* * */
/* *    "Less than" cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     Now we're going to work with inequality constraints. */

/*     We're going to start out with some very simple cases. We want to */
/*     find the times when the Alpha-Gamma vector's longitude is less */
/*     than each integer multiple of pi/8, where the confinement window */
/*     extends 1 second before and after the crossing time. Crossings of */
/*     these meridians should occur every 90 minutes. */


    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 18:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "alphafixed", (ftnlen)32, (ftnlen)10);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "Alpha", (ftnlen)36, (ftnlen)5);
    s_copy(target, "Gamma", (ftnlen)36, (ftnlen)5);
    s_copy(dscstr, "Alpha-Gamma position longitude", (ftnlen)200, (ftnlen)30);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)1133)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)1134)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 17; ++vi) {

/*           Loop over each reference value. Include both 0 and 2*pi. */

	    refval = (vi - 1) * pi_() / 8.;

/*           For planetographic coordinates, set the reference */
/*           value to indicate the same position on the unit */
/*           circle as that used for the other systems. */

	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }
	    if (refval == twopi_()) {
		refval = 0.;
	    }
	    if (refval < 0.) {
		refval += twopi_();
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# less than # (deg) #; #; 2-sec CONFINE", (ftnlen)
		    200, (ftnlen)39);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    d__1 = refval * dpr_();
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, "<", (ftnlen)40, (ftnlen)1);
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           We're going to set the confinement window for each */
/*           search so we cover one reference value crossing. */

	    scardd_(&c__0, cnfine);
	    t1 = et0 + (vi - 1) * 5400. - 1.;
	    t2 = t1 + 2.;
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Filter the result window to remove spurious */
/*           intervals that may arise due to convergence */
/*           error. */

	    wnfltd_(&tol, result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect to cross the VIth reference value at */

/*              4.5 + 1.5D0*VI hours */

/*           past noon TDB. */


	    xtime = (vi * 1.5 + 4.5) * 3600.;

/*           Normally we expect to find one solution interval, but */
/*           the result window should be empty if the constraint */
/*           can't be satisfied. */

	    n = wncard_(result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*              WRITE (*,'(A,I15,E25.16)') 'N, REFVAL = ', N, REFVAL */
	    if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) || 
		    eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14) 
		    || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)) {
		if (cos(refval) == 1.) {

/*                 The condition */

/*                    longitude < REFVAL */

/*                 is never satisfied, so the window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 Make sure we found 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else {

/*              Make sure we found 1 interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    }

/*           Note: in the code below, we don't expect to find */
/*           result windows containing multiple intervals. */
/*           But we format the code to handle multiple intervals */
/*           so we can easily re-use the code in cases where */
/*           multiple intervals are expected. */


/*           Now check the solution intervals. */

	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              Look up the Ith solution interval. */

		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    WRITE (*,*) 1, START, FINISH */

/*                    The stop time should be T2; the start time should */
/*                    be the time of the reference meridian crossing. */

			s_copy(qname, "Interval start  # (0)", (ftnlen)200, (
				ftnlen)21);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &xtime, &c_b73, ok, (
				ftnlen)200, (ftnlen)1);
			s_copy(qname, "Interval stop # (0)", (ftnlen)200, (
				ftnlen)19);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			xet = xtime;
			et = start;
		    }
		} else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)
			) {

/*                 Longitude is positive East. The branch cut is at 0. */

		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    The start time should be T1. If the reference */
/*                    value is at the branch cut, the stop time should */
/*                    be T2. Otherwise, the stop time should be the time */
/*                    of the reference meridian crossing. */


			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &t1, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			if (cos(refval) == 1.) {
			    xet = t2;
			    et = finish;
			} else {
			    xet = xtime;
			    et = finish;
			}
			chcksd_(qname, &finish, "~", &xet, &c_b73, ok, (
				ftnlen)200, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)
			8) || eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)
			9)) {

/*                 Longitude is positive East. The branch cut is at Pi. */

		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    The start time should be T1. If the reference */
/*                    value is at the branch cut, the stop time should */
/*                    be T2. Otherwise, the stop time should be the time */
/*                    of the reference meridian crossing. */


			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &t1, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			if (cos(refval) == -1.) {
			    xet = t2;
			} else {
			    xet = xtime;
			}
			et = finish;
			chcksd_(qname, &et, "~", &xet, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
		    }
		} else {
		    setmsg_("Unexpected system: #", (ftnlen)20);
		    errch_("#", crdsys, (ftnlen)1, (ftnlen)32);
		    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/*              Check the longitude at the crossing time ET. */

/*              In order to decide how accurate the coordinate should */
/*              be, we'll use the angular rate of Gamma together with */
/*              the time tolerance. Adjust the tolerance for the units */
/*              we're using in the CHCKSD call. */

		qtol = dpr_() * (twopi_() / spd_()) * 1e-6;

/*              Compute the longitude of the vector we expect and */
/*              the longitude at FINISH. */

		spkezr_(target, &xet, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &xlon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &xlon, &lat);
		}
		spkezr_(target, &et, ref, abcorr, obsrvr, state, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &lon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &lon, &lat);
		}
		s_copy(qname, "Longitude # (deg)", (ftnlen)200, (ftnlen)17);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);

/*              Adjust the longitude by 2*pi if it's too far */
/*              from the expected value. */

		if (lon > xlon + 1.) {
		    lon -= twopi_();
		} else if (lon < xlon - 1.) {
		    lon += twopi_();
		}
		d__1 = lon * dpr_();
		d__2 = xlon * dpr_();
		chcksd_(qname, &d__1, "~", &d__2, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }

/*           We're done with the Ith interval. */

	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */

/*     Repeat the previous case, but this time keep the confinement */
/*     window start time fixed at 18:00 Jan 1 2000 TDB. We want to find */
/*     the times when the Alpha-Gamma vector's longitude is less than */
/*     each integer multiple of pi/8, where the confinement window */
/*     extends 1 second before and after the crossing time. Crossings of */
/*     these meridians should occur every 90 minutes. */


    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 18:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "alphafixed", (ftnlen)32, (ftnlen)10);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "Alpha", (ftnlen)36, (ftnlen)5);
    s_copy(target, "Gamma", (ftnlen)36, (ftnlen)5);
    s_copy(dscstr, "Alpha-Gamma position longitude", (ftnlen)200, (ftnlen)30);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)1550)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)1551)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 17; ++vi) {

/*           Loop over each reference value. Include both 0 and 2*pi. */

	    refval = (vi - 1) * pi_() / 8.;

/*           For planetographic coordinates, set the reference */
/*           value to indicate the same position on the unit */
/*           circle as that used for the other systems. */

	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }
	    if (refval == twopi_()) {
		refval = 0.;
	    }
	    if (refval < 0.) {
		refval += twopi_();
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# less than # (deg) #; #; fixed CNFINE start", (
		    ftnlen)200, (ftnlen)44);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    d__1 = refval * dpr_();
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, "<", (ftnlen)40, (ftnlen)1);
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           We're going to set the confinement window for each */
/*           search so we cover one reference value crossing. */

	    scardd_(&c__0, cnfine);
	    t1 = et0 + 9.9999999999999995e-8;
	    t2 = et0 + (vi - 1) * 5400. + 1.;
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Filter the result window to remove spurious */
/*           intervals that may arise due to convergence */
/*           error. */

	    wnfltd_(&tol, result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect to cross the VIth reference value at */

/*              4.5 + 1.5D0*VI hours */

/*              past noon TDB. */


	    xtime = (vi * 1.5 + 4.5) * 3600.;

/*           Normally we expect to find one solution interval, but */
/*           the result window should be empty if the constraint */
/*           can't be satisfied. */

	    n = wncard_(result);
/*           WRITE (*,'(A,I15,E25.16)') 'N, REFVAL = ', N, REFVAL */
	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		if (cos(refval) == 1.) {

/*                 The condition */

/*                    longitude < REFVAL */

/*                 is never satisfied, so the window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 Make sure we found 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) 
		    || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)) {
		if (cos(refval) == 1.) {

/*                 The condition */

/*                    longitude < REFVAL */

/*                 is never satisfied, so the window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 Make sure we found 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) 
		    || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)) {

/*              Make sure we found 1 interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    } else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11) 
		    || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || 
		    eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)9)) {

/*              The number of expected intervals depends on the */
/*              case index. */

		if (vi == 1) {

/*                 The condition */

/*                    longitude < REFVAL */

/*                 is never satisfied, so the window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);

		} else {

/*                 Make sure we found 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    }

/*           Now check the solution intervals. */

	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              Look up the Ith solution interval. */

		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    WRITE (*,*) 1, START, FINISH */

/*                    The stop time should be T2; the start time should */
/*                    be the time of the reference meridian crossing. */

			s_copy(qname, "Interval start  # (0)", (ftnlen)200, (
				ftnlen)21);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &xtime, &c_b73, ok, (
				ftnlen)200, (ftnlen)1);
			s_copy(qname, "Interval stop # (0)", (ftnlen)200, (
				ftnlen)19);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			xet = xtime;
			et = start;
		    } else {

/*                    We don't expect to have more than one */
/*                    interval for this case. */

			setmsg_("Unexpected interval count: #; VI = #", (
				ftnlen)36);
			errint_("#", &n, (ftnlen)1);
			errint_("#", &vi, (ftnlen)1);
			sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)
			) {

/*                 Longitude is positive East. The branch cut is at 0. */

		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    The start time should be T1. If the reference */
/*                    value is at the branch cut, the stop time should */
/*                    be T2. Otherwise, the stop time should be the time */
/*                    of the reference meridian crossing. */


			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &t1, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			if (cos(refval) == 1.) {
			    xet = t2;
			    et = finish;
			} else {
			    xet = xtime;
			    et = finish;
			}
			chcksd_(qname, &finish, "~", &xet, &c_b73, ok, (
				ftnlen)200, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)
			8) || eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)
			9)) {

/*                 Longitude is positive East. The branch cut is at Pi. */

		    if (i__ == 1) {

/*                       This is the normal case. */

/*                       If REFVAL is in the open upper half of the unit */
/*                       circle, the start time should be ET0 and the */
/*                       stop time should be the time of the reference */
/*                       meridian crossing. */

/*                       If REFVAL is pi, the solution set includes the */
/*                       interval from XTIME to T2. We expect this */
/*                       interval to be merged with the interval for the */
/*                       top half of the circle, so the expected end */
/*                       time becomes T2. */

/*                       Otherwise, the first interval should be the */
/*                       singleton [ET0, ET0]. */

			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			if (sin(refval) > 0.) {
			    chcksd_(qname, &start, "~", &et0, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			} else {
			    d__1 = et0 + spd_() / 2;
			    chcksd_(qname, &start, "~", &d__1, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			}
/*                        IF ( .NOT. OK ) THEN */
/*                           WRITE (*,*) 'VI = ', VI */
/*                        END IF */
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			if (cos(refval) == -1.) {
			    xet = t2;
			} else {
			    xet = xtime;
			}
			et = finish;
			chcksd_(qname, &et, "~", &xet, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
		    } else {

/*                       We don't expect to have more than one */
/*                       interval for this case. */

			setmsg_("Unexpected interval count: #; VI = #", (
				ftnlen)36);
			errint_("#", &n, (ftnlen)1);
			errint_("#", &vi, (ftnlen)1);
			sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }
		} else {
		    setmsg_("Unexpected system: #", (ftnlen)20);
		    errch_("#", crdsys, (ftnlen)1, (ftnlen)32);
		    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/*              Check the longitude at the crossing time ET. */

/*              In order to decide how accurate the coordinate should */
/*              be, we'll use the angular rate of Gamma together with */
/*              the time tolerance. Adjust the tolerance for the units */
/*              we're using in the CHCKSD call. */

		qtol = dpr_() * (twopi_() / spd_()) * 1e-6;

/*              Compute the longitude of the vector we expect and */
/*              the longitude at FINISH. */

		spkezr_(target, &xet, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &xlon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &xlon, &lat);
		}
		spkezr_(target, &et, ref, abcorr, obsrvr, state, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &lon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &lon, &lat);
		}
		s_copy(qname, "Longitude # (deg)", (ftnlen)200, (ftnlen)17);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);

/*              Adjust the longitude by 2*pi if it's too far */
/*              from the expected value. */

		if (lon > xlon + 1.) {
		    lon -= twopi_();
		} else if (lon < xlon - 1.) {
		    lon += twopi_();
		}
		d__1 = lon * dpr_();
		d__2 = xlon * dpr_();
		chcksd_(qname, &d__1, "~", &d__2, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }

/*           We're done with the Ith interval. */

	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */

/* ********************************************************************* */
/* * */
/* *    "Greater than" cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     '>' Tests */

/*     We're going to try some very simple '>' cases. We want to */
/*     find the times when the Alpha-Gamma vector's longitude is greater */
/*     than each integer multiple of pi/8, where the confinement window */
/*     extends 1 second before and after the crossing time. Crossings of */
/*     these meridians should occur every 90 minutes. */

    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 18:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "alphafixed", (ftnlen)32, (ftnlen)10);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "Alpha", (ftnlen)36, (ftnlen)5);
    s_copy(target, "Gamma", (ftnlen)36, (ftnlen)5);
    s_copy(dscstr, "Alpha-Gamma position longitude", (ftnlen)200, (ftnlen)30);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)2059)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)2060)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 17; ++vi) {

/*           Loop over each reference value. Include both 0 and 2*pi. */

	    refval = (vi - 1) * pi_() / 8.;

/*           For planetographic coordinates, set the reference */
/*           value to indicate the same position on the unit */
/*           circle as that used for the other systems. */

	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }
	    if (refval == twopi_()) {
		refval = 0.;
	    }
	    if (refval < 0.) {
		refval += twopi_();
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# greater than # (deg) #; #; 2-sec CONFINE", (
		    ftnlen)200, (ftnlen)42);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    d__1 = refval * dpr_();
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, ">", (ftnlen)40, (ftnlen)1);
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           We're going to set the confinement window for each */
/*           search so we cover one reference value crossing. */

	    scardd_(&c__0, cnfine);
	    t1 = et0 + (vi - 1) * 5400. - 1.;
	    t2 = t1 + 2.;
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect to cross the VIth reference value at */

/*              24*(I-1) + 4.5 + 1.5D0*VI hours */

/*              past noon TDB. */


	    xtime = (vi * 1.5 + 4.5) * 3600.;

/*           Filter the result window to remove spurious */
/*           intervals that may arise due to convergence */
/*           error. */

	    wnfltd_(&tol, result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Normally we expect to find one solution interval, but */
/*           the result window should be empty if the constraint */
/*           can't be satisfied. */

	    n = wncard_(result);
/*              WRITE (*,'(A,I15,E25.16)') 'N, REFVAL = ', N, REFVAL */
	    if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) || 
		    eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14) 
		    || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)) {

/*              Make sure we found 1 interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    } else {
		if (cos(refval) == -1.) {

/*                 We don't expect to find any intervals. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 Make sure we found 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    }

/*           Note: in the code below, we don't expect to find */
/*           result windows containing multiple intervals. */
/*           But we format the code to handle multiple intervals */
/*           so we can easily re-use the code in cases where */
/*           multiple intervals are expected. */


/*           Now check the solution intervals. */

	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              Look up the Ith solution interval. */

		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    WRITE (*,*) 1, START, FINISH */

			if (cos(refval) == 1.) {

/*                       The stop time actually should be T2, since the */
/*                       interval from T1 to XTIME satisfies the */
/*                       inequality. */

			    xet = t2;
			} else {

/*                       The start time should be XTIME. The stop time */
/*                       should be T2. */

			    xet = xtime;
			}
/*                    The start time should be T1; the stop time should */
/*                    be the time of the reference meridian crossing. */

			s_copy(qname, "Interval start  # (0)", (ftnlen)200, (
				ftnlen)21);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &start, "~", &t1, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop # (0)", (ftnlen)200, (
				ftnlen)19);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			et = finish;
			chcksd_(qname, &et, "~", &xet, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)
			) {

/*                 Longitude is positive East. The branch cut is at 0. */

		    if (i__ == 1) {

/*                    This is the normal case. */

			if (cos(refval) == 1.) {

/*                       The start time actually should be T1, since the */
/*                       interval from T1 to XTIME satisfies the */
/*                       inequality. */

			    xet = t1;
			} else {

/*                       The start time should be XTIME. The stop time */
/*                       should be T2. */

			    xet = xtime;
			}
			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			et = start;
			chcksd_(qname, &et, "~", &xet, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)
			8) || eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)
			9)) {

/*                 Longitude is positive East. The branch cut is at Pi. */

		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    The start time should be XTIME. The stop time */
/*                    should be T2. */

			s_copy(qname, "Interval start # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			et = start;
			xet = xtime;
			chcksd_(qname, &et, "~", &xet, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
			s_copy(qname, "Interval stop  # (0)", (ftnlen)200, (
				ftnlen)20);
			repmi_(qname, "#", &c__1, qname, (ftnlen)200, (ftnlen)
				1, (ftnlen)200);
			chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (ftnlen)
				200, (ftnlen)1);
		    }
		} else {
		    setmsg_("Unexpected system: #", (ftnlen)20);
		    errch_("#", crdsys, (ftnlen)1, (ftnlen)32);
		    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/*              Check the longitude at the crossing time ET. */

/*              In order to decide how accurate the coordinate should */
/*              be, we'll use the angular rate of Gamma together with */
/*              the time tolerance. Adjust the tolerance for the units */
/*              we're using in the CHCKSD call. */

		qtol = dpr_() * (twopi_() / spd_()) * 1e-6;

/*              Compute the longitude of the vector we expect and */
/*              the longitude at FINISH. */

		spkezr_(target, &xet, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &xlon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &xlon, &lat);
		}
		spkezr_(target, &et, ref, abcorr, obsrvr, state, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &lon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &lon, &lat);
		}
		s_copy(qname, "Longitude # (deg)", (ftnlen)200, (ftnlen)17);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);

/*              Adjust the longitude by 2*pi if it's too far */
/*              from the expected value. */

		if (lon > xlon + 1.) {
		    lon -= twopi_();
		} else if (lon < xlon - 1.) {
		    lon += twopi_();
		}
		d__1 = lon * dpr_();
		d__2 = xlon * dpr_();
		chcksd_(qname, &d__1, "~", &d__2, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }

/*           We're done with the Ith interval. */

	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */

/*     Repeat the previous case, but this time keep the confinement */
/*     window stop time fixed at 18:00 Jan 2 2000 TDB. We want to */
/*     find the times when the Alpha-Gamma vector's longitude is greater */
/*     than each integer multiple of pi/8. Crossings of */
/*     these meridians should occur every 90 minutes. */


    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    str2et_("2000 JAN 1 18:00:00 TDB", &et0, (ftnlen)23);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et1 = et0 + spd_();
    s_copy(ref, "alphafixed", (ftnlen)32, (ftnlen)10);
    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
    s_copy(obsrvr, "Alpha", (ftnlen)36, (ftnlen)5);
    s_copy(target, "Gamma", (ftnlen)36, (ftnlen)5);
    s_copy(dscstr, "Alpha-Gamma position longitude", (ftnlen)200, (ftnlen)30);
    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
    cleard_(&c__3, dvec);
    for (cc = 1; cc <= 6; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng2__", (ftnlen)2489)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng2__", (ftnlen)2490)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	for (vi = 1; vi <= 17; ++vi) {

/*           Loop over each reference value. Include both 0 and 2*pi. */

	    refval = (vi - 1) * pi_() / 8.;

/*           For planetographic coordinates, set the reference */
/*           value to indicate the same position on the unit */
/*           circle as that used for the other systems. */

	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }
	    if (refval < 0.) {
		refval += twopi_();
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "# greater than # (deg) #; #; fixed CNFINE stop", (
		    ftnlen)200, (ftnlen)46);
	    repmc_(title, "#", dscstr, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200, (ftnlen)200);
	    d__1 = refval * dpr_();
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    tcase_(title, (ftnlen)200);
	    s_copy(relate, ">", (ftnlen)40, (ftnlen)1);
	    tol = 1e-6;
	    adjust = 0.;
	    rpt = FALSE_;
	    bail = FALSE_;

/*           Set the search step size. */

	    gfsstp_(&c_b59);

/*           We're going to set the confinement window for each */
/*           search so we cover one reference value crossing. */

	    scardd_(&c__0, cnfine);
	    t1 = et0 + (vi - 1) * 5400. - 1.;
	    t2 = et1;
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Perform the search. */

	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect to cross the VIth reference value at */

/*              24*(I-1) + 4.5 + 1.5D0*VI hours */

/*              past noon TDB. */


	    xtime = (vi * 1.5 + 4.5) * 3600.;

/*           Filter the result window to remove spurious */
/*           intervals that may arise due to convergence */
/*           error. */

	    wnfltd_(&tol, result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Normally we expect to find one solution interval, but */
/*           the result window should be empty if the constraint */
/*           can't be satisfied. */

	    n = wncard_(result);
/*           WRITE (*,'(A,I15,E25.16)') 'N, REFVAL = ', N, REFVAL */
	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*              Make sure we found 1 interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    } else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)11) 
		    || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)) {

/*              Make sure we found 1 interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    } else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)11) 
		    || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || 
		    eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)9)) {

/*              The number of expected intervals depends on the */
/*              case index. */

		if (cos(refval) == -1.) {

/*                 Make sure we found no intervals. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else if (vi == 17) {

/*                 Make sure we found no intervals. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 We should have 1 interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    }

/*           Now check the solution intervals. */

	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {

/*              Look up the Ith solution interval. */

		wnfetd_(result, &i__, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
/*               WRITE (*,*) START, FINISH */
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    if (i__ == 1) {

/*                    This is the normal case. */

/*                    WRITE (*,*) 1, START, FINISH */

			if (cos(refval) == 1.) {

/*                       This is a special case: the < condition */
/*                       is considered to be met on the entire */
/*                       confinement window. */

			    s_copy(qname, "Interval start  # (0)", (ftnlen)
				    200, (ftnlen)21);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &start, "~", &t1, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    xet = t2;
			    et = finish;
			    s_copy(qname, "Interval stop # (0)", (ftnlen)200, 
				    (ftnlen)19);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			} else {

/*                       The start time should be T1. The stop time */
/*                       should be the time of the reference meridian */
/*                       crossing. */
			    s_copy(qname, "Interval start  # (0)", (ftnlen)
				    200, (ftnlen)21);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &start, "~", &t1, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    xet = xtime;
			    et = finish;
			    s_copy(qname, "Interval stop # (0)", (ftnlen)200, 
				    (ftnlen)19);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			}
		    } else {

/*                    We don't expect to have more than one */
/*                    interval for this case. */

			setmsg_("Unexpected interval count: #; VI = #", (
				ftnlen)36);
			errint_("#", &n, (ftnlen)1);
			errint_("#", &vi, (ftnlen)1);
			sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }
		} else if (eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6)
			) {

/*                 Longitude is positive East. The branch cut is at 0. */

		    if (i__ == 1) {

/*                    This is the normal case. */

			if (cos(refval) == 1.) {

/*                       This is a special case: the > condition */
/*                       is considered to be met on the whole */
/*                       confinement window. */

			    xet = t1;
			    et = start;
			    s_copy(qname, "Interval start # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    s_copy(qname, "Interval stop  # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			} else {

/*                       The start time should be the REFVAL meridian */
/*                       crossing time; the end time should be T2. */

			    xet = xtime;
			    et = start;
			    s_copy(qname, "Interval start # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    s_copy(qname, "Interval stop  # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &finish, "~", &t2, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			}
		    }
		} else if (eqstr_(crdsys, "LATITUDINAL", (ftnlen)32, (ftnlen)
			11) || eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)
			8) || eqstr_(crdsys, "SPHERICAL", (ftnlen)32, (ftnlen)
			9)) {

/*                 Longitude is positive East. The branch cut is at Pi. */

		    if (vi < 17) {
			if (i__ == 1) {

/*                       This is the normal case. */

/*                       If REFVAL is in the upper half of the unit */
/*                       circle, including 0 and pi, the start time */
/*                       should be the time of the reference meridian */
/*                       crossing and the stop time should be ET0 + */
/*                       12 h. */

/*                       If REFVAL is in the lower half of the unit */
/*                       circle, the start time should be the time of */
/*                       the reference meridian crossing and the stop */
/*                       time should be T2 (the end of the confinement */
/*                       window). */

			    s_copy(qname, "Interval start # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    xet = xtime;
			    et = start;
			    chcksd_(qname, &start, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    s_copy(qname, "Interval stop  # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    if (sin(refval) >= 0.) {
				d__1 = et0 + spd_() / 2;
				chcksd_(qname, &finish, "~", &d__1, &c_b73, 
					ok, (ftnlen)200, (ftnlen)1);
			    } else {
				chcksd_(qname, &finish, "~", &t2, &c_b73, ok, 
					(ftnlen)200, (ftnlen)1);
			    }
			} else {

/*                       We don't expect to have more than one */
/*                       interval for this case. */

			    setmsg_("Unexpected interval count: #; VI = #", (
				    ftnlen)36);
			    errint_("#", &n, (ftnlen)1);
			    errint_("#", &vi, (ftnlen)1);
			    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			}
		    } else {

/*                    This case corresponds to the first interval */
/*                    when REFVAL is 0 and VI is 17. The first interval */
/*                    should be the singleton [ET0, ET0]. */

			if (i__ == 1) {
			    s_copy(qname, "Interval start # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chcksd_(qname, &start, "~", &et0, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    s_copy(qname, "Interval stop  # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    xet = et0;
			    et = finish;
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			} else if (i__ == 2) {

/*                       This case corresponds to the second interval */
/*                       when REFVAL is 0 and VI is 17. The second */
/*                       interval should cover from ET0+12h to ET0+24h. */

			    s_copy(qname, "Interval start # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    d__1 = et0 + spd_() / 2;
			    chcksd_(qname, &start, "~", &d__1, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			    s_copy(qname, "Interval stop  # (0)", (ftnlen)200,
				     (ftnlen)20);
			    repmi_(qname, "#", &c__1, qname, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    xet = et0 + spd_();
			    et = finish;
			    chcksd_(qname, &et, "~", &xet, &c_b73, ok, (
				    ftnlen)200, (ftnlen)1);
			} else {

/*                       We don't expect to have more than two */
/*                       intervals for this case. */

			    setmsg_("Unexpected interval count: #; VI = #", (
				    ftnlen)36);
			    errint_("#", &n, (ftnlen)1);
			    errint_("#", &vi, (ftnlen)1);
			    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			}
		    }
		} else {
		    setmsg_("Unexpected system: #", (ftnlen)20);
		    errch_("#", crdsys, (ftnlen)1, (ftnlen)32);
		    sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/*              Check the longitude at the crossing time ET. */

/*              In order to decide how accurate the coordinate should */
/*              be, we'll use the angular rate of Gamma together with */
/*              the time tolerance. Adjust the tolerance for the units */
/*              we're using in the CHCKSD call. */

		qtol = dpr_() * (twopi_() / spd_()) * 1e-6;

/*              Compute the longitude of the vector we expect and */
/*              the longitude at FINISH. */

		spkezr_(target, &xet, ref, abcorr, obsrvr, state, &lt, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &xlon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &xlon, &lat);
		}
		spkezr_(target, &et, ref, abcorr, obsrvr, state, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vequ_(state, pos);
		if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) 
			{
		    recpgr_(obsrvr, pos, &c_b146, &c_b85, &lon, &lat, &alt, (
			    ftnlen)36);
		} else {
		    reclat_(pos, &r__, &lon, &lat);
		}
		s_copy(qname, "Longitude # (deg)", (ftnlen)200, (ftnlen)17);
		repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (
			ftnlen)200);

/*              Adjust the longitude by 2*pi if it's too far */
/*              from the expected value. */

		if (lon > xlon + 1.) {
		    lon -= twopi_();
		} else if (lon < xlon - 1.) {
		    lon += twopi_();
		}
		d__1 = lon * dpr_();
		d__2 = xlon * dpr_();
		chcksd_(qname, &d__1, "~", &d__2, &qtol, ok, (ftnlen)200, (
			ftnlen)1);
	    }

/*           We're done with the Ith interval. */

	}

/*        End of vector definition loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgflong.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgflng2__ */

