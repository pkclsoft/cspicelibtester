/* f_zzgflng3.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__40000 = 40000;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c__6 = 6;
static doublereal c_b71 = 1e4;
static integer c__15 = 15;
static doublereal c_b103 = 1e-6;
static doublereal c_b456 = 1.8e4;
static integer c__24 = 24;
static doublereal c_b475 = 0.;

/* $Procedure      F_ZZGFLNG3 ( Test GF longitude solver, part 3 ) */
/* Subroutine */ int f_zzgflng3__(logical *ok)
{
    /* Initialized data */

    static char cnam[32*7] = "Longitude                       " "Right  asce"
	    "nsion                " "Longitude                       " "Longi"
	    "tude                       " "Longitude                       " 
	    "Longitude                       " "Longitude                   "
	    "    ";
    static char csys[32*7] = "Latitudinal                     " "Ra / Dec   "
	    "                     " "Cylindrical                     " "Geode"
	    "tic                        " "Planetographic                  " 
	    "Planetographic_2                " "Spherical                   "
	    "    ";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static logical bail;
    static char dref[32];
    static doublereal dvec[3], work[600000]	/* was [40000][15] */;
    extern /* Subroutine */ int zzgflong_(char *, char *, char *, char *, 
	    char *, char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, U_fp, U_fp, logical *, 
	    U_fp, U_fp, U_fp, logical *, L_fp, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer i__, n;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen), tcase_(
	    char *, ftnlen);
    static char qname[200];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static doublereal coord;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmf_(char *, char *, doublereal *, 
	    integer *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime, start;
    extern logical eqstr_(char *, char *, ftnlen, ftnlen);
    extern doublereal twopi_(void);
    static doublereal t1, t2;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal coord0, coord2;
    static integer cc;
    static doublereal xtime2;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    extern logical gfbail_();
    extern doublereal pi_(void);
    static integer handle;
    static char vecdef[32];
    static integer si;
    extern /* Subroutine */ int scardd_(integer *, doublereal *), cleard_(
	    integer *, doublereal *);
    static doublereal cnfine[40006];
    extern /* Subroutine */ int gfrefn_(), gfrepi_();
    extern integer wncard_(doublereal *);
    extern /* Subroutine */ int gfstep_(), gfrepu_(), gfrepf_();
    static char abcorr[200], crdnam[32], crdsys[32], frmtxt[200*100], lonsns[
	    4], relate[40], method[80], obsrvr[36], target[36], timstr[35];
    static doublereal adjust, alphrd[3], et0, finish, refval, result[40006], 
	    tmprad[3], tdelta;
    static integer nlines;
    static char ref[32];
    extern /* Subroutine */ int tstlsk_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen), tstpck_(char *, logical *, logical *, ftnlen),
	     tstspk_(char *, logical *, integer *, ftnlen), natpck_(char *, 
	    logical *, logical *, ftnlen), natspk_(char *, logical *, integer 
	    *, ftnlen), ssized_(integer *, doublereal *);
    extern doublereal dpr_(void), rpd_(void), spd_(void);
    extern /* Subroutine */ int pcpool_(char *, integer *, char *, ftnlen, 
	    ftnlen), bodvrd_(char *, char *, integer *, integer *, doublereal 
	    *, ftnlen, ftnlen), pdpool_(char *, integer *, doublereal *, 
	    ftnlen), wninsd_(doublereal *, doublereal *, doublereal *);
    static doublereal tol;
    extern /* Subroutine */ int gfsstp_(doublereal *), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), wnfetd_(doublereal *, integer *, doublereal *, 
	    doublereal *), chcksd_(char *, doublereal *, char *, doublereal *,
	     doublereal *, logical *, ftnlen, ftnlen);
    static logical rpt;
    extern /* Subroutine */ int lmpool_(char *, integer *, ftnlen), spkuef_(
	    integer *), delfil_(char *, ftnlen);
    static integer han2;
    extern /* Subroutine */ int zzgfcog_(doublereal *, doublereal *);

/* $ Abstract */

/*     Test the GF private longitude search routine ZZGFLONG. This */
/*     family tests processing of relations involving relative, */
/*     absolute, and adjusted absolute extrema. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF coordinate search */
/*     routine ZZGFLONG. This family tests processing of relations */
/*     involving relative, absolute, and adjusted absolute extrema. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 15-APR-2014 (EDW) */

/*        Added explicit declaration of GFBAIL type. */

/* -    TSPICE Version 1.0.0, 21-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL declarations */


/*     Local parameters */


/*     Maximum length of a coordinate system name. */


/*     Maximum length of a coordinate name. */


/*     Number of systems that support longitude: */


/*     Frame definition buffer size: */


/*     Longitude sense string length: */


/*     Local variables */

/*     INTEGER               J */

/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFLNG3", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgflong.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgflong.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create and load Nat's solar system SPK and PCK/FK */
/*     kernels. */

    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    natspk_("nat.bsp", &c_true, &han2, (ftnlen)7);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Absolute maximum tests */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find absolute maxima of the alphafixed longitude */
/*     of the Alpha-gamma vector. */


    str2et_("2000 JAN 1 18:00 TDB ", &et0, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We'll loop over the various coordinate systems. */

    for (cc = 1; cc <= 7; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng3__", (ftnlen)389)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "WEST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "WEST", (ftnlen)4, (ftnlen)4);
	} else if (eqstr_(crdsys, "Planetographic_2", (ftnlen)32, (ftnlen)16))
		 {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "EAST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14);
	} else {
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	}
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng3__", (ftnlen)429)) << 5), (
		ftnlen)32, (ftnlen)32);

/*        Loop over different confinement windows. */
/*        We pick the window end times so that the */
/*        maxima will be multiples of pi/8. */

	for (si = 1; si <= 16; ++si) {
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		t1 = et0 + (si - 1) * 1.5 * 3600.;
		t2 = et0 + spd_();

/*              Set the expected time of the maximum. */

		xtime = t1;
	    } else {
		if (si <= 9) {
		    t1 = et0;
		} else {
		    t1 = et0 + spd_() / 2;
		}
		t2 = et0 + (si - 1) * 1.5 * 3600.;

/*              Set the expected time of the maximum. */

		xtime = t2;
	    }
	    scardd_(&c__0, cnfine);
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    refval = (si - 1) * pi_() / 8.;

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Alpha-gamma maximum; #; #; ref val #", (ftnlen)200,
		     (ftnlen)36);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    d__1 = dpr_() * refval;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    tcase_(title, (ftnlen)200);

/*           Set parameters required for the search. */

	    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
	    s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
	    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	    s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
	    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	    cleard_(&c__3, dvec);
	    s_copy(relate, "ABSMAX", (ftnlen)40, (ftnlen)6);
	    refval = 0.;
	    tol = 1e-6;
	    adjust = 0.;
	    gfsstp_(&c_b71);
	    rpt = FALSE_;
	    bail = FALSE_;
	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (si == 1) {
		if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {

/*                 There's a branch cut at 0: however, we've */
/*                 made the confinement window the whole unit */
/*                 circle. So we expect a result. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else if (eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6) || 
			eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)
			14) || eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (
			ftnlen)11)) {

/*                 There's a branch cut at 0: the search window is */
/*                 bounded away from the branch cut, while the */
/*                 confinement window contains nothing but the branch */
/*                 cut. The result window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 The confinement window should be a singleton set */
/*                 at which the absolute maximum is found. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else {

/*              We expect the result window to contain one */
/*              solution interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    }
	    if (n == 1) {
		wnfetd_(result, &c__1, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
/*              Check that START is equal to XTIME. */

		s_copy(qname, "START vs XTIME", (ftnlen)200, (ftnlen)14);
		chcksd_(qname, &start, "~", &xtime, &c_b103, ok, (ftnlen)200, 
			(ftnlen)1);

/*              Check that START and FINISH match. */

		s_copy(qname, "START vs FINISH", (ftnlen)200, (ftnlen)15);
		chcksd_(qname, &start, "~", &finish, &c_b103, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
/* ********************************************************************* */
/* * */
/* *    Adjusted absolute maximum tests */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find adjusted absolute maxima of the alphafixed */
/*     longitude of the Alpha-gamma vector. */


    str2et_("2000 JAN 1 18:00 TDB ", &et0, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = rpd_() * 15. / 3600.;

/*     We'll loop over the various coordinate systems. */

    for (cc = 1; cc <= 7; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng3__", (ftnlen)626)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng3__", (ftnlen)627)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "WEST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "WEST", (ftnlen)4, (ftnlen)4);
	} else if (eqstr_(crdsys, "Planetographic_2", (ftnlen)32, (ftnlen)16))
		 {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "EAST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14);
	} else {
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	}
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Loop over different confinement windows. */
/*        We pick the window end times so that the */
/*        maxima will be multiples of pi/8. */

	for (si = 1; si <= 16; ++si) {
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		t1 = et0 + (si - 1) * 1.5 * 3600.;
		t2 = et0 + spd_();

/*              Set the expected time of the maximum and the */
/*              adjusted maximum. XTIME is the expected */
/*              start time; XTIME2 is the expected stop time. */

		xtime = t1;
/* Computing MIN */
		d__1 = t1 + 1.;
		xtime2 = min(d__1,t2);
	    } else {
		if (si <= 9) {
		    t1 = et0;
		} else {
		    t1 = et0 + spd_() / 2;
		}
		t2 = et0 + (si - 1) * 1.5 * 3600.;

/*              Set the expected time of the maximum and the */
/*              adjusted maximum. XTIME is the expected */
/*              start time; XTIME2 is the expected stop time. */
/* Computing MAX */
		d__1 = t2 - 1.;
		xtime = max(d__1,et0);
		xtime2 = t2;
	    }
	    scardd_(&c__0, cnfine);
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Contract the confinement window a bit to avoid */
/*           spurious maxima. However, don't wipe out the */
/*           confinement interval altogether. */

	    refval = (si - 1) * pi_() / 8.;
	    wnfetd_(cnfine, &c__1, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Alpha-gamma adjusted maximum; #; #; max lon #; AD"
		    "JUST = #", (ftnlen)200, (ftnlen)57);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    d__1 = dpr_() * refval;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    d__1 = dpr_() * adjust;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    tcase_(title, (ftnlen)200);

/*           Set parameters required for the search. */

	    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
	    s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
	    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	    s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
	    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	    cleard_(&c__3, dvec);
	    s_copy(relate, "ABSMAX", (ftnlen)40, (ftnlen)6);
	    refval = 0.;
	    tol = 1e-6;
	    gfsstp_(&c_b71);
	    rpt = FALSE_;
	    bail = FALSE_;
	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (si == 1) {
		if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {

/*                 We expect the result window to contain one */
/*                 solution interval. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {
		    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)
			    14) || eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (
			    ftnlen)11) || eqstr_(crdsys, "RA/DEC", (ftnlen)32,
			     (ftnlen)6)) {

/*                    These systems have a singularity at */
/*                    0 longitude/RA, so the result window */
/*                    should be empty. */

			chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
				ftnlen)1);
		    } else {

/*                    The result should be a singleton set. */

			chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
				ftnlen)1);
		    }
		}
	    } else {

/*              We expect the result window to contain one */
/*              solution interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*              IF ( .NOT. OK ) THEN */

/*                 DO J = 1, WNCARD(CNFINE) */
/*                    CALL WNFETD ( CNFINE, J, START, FINISH ) */
/*                    WRITE (*,'(2(5PE24.12))') START, FINISH */
/*                 END DO */

/*                 DO J = 1, WNCARD(RESULT) */
/*                    CALL WNFETD ( RESULT, J, START, FINISH ) */
/*                    WRITE (*,'(2(5PE24.12))') START, FINISH */
/*                 END DO */

/*              END IF */

	    }
	    if (n == 1) {
		wnfetd_(result, &c__1, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
/*              Check that START is equal to XTIME. */

		s_copy(qname, "START vs XTIME", (ftnlen)200, (ftnlen)14);
		chcksd_(qname, &start, "~", &xtime, &c_b103, ok, (ftnlen)200, 
			(ftnlen)1);

/*              Check that FINISH is equal to XTIME2. */

		s_copy(qname, "FINISH vs XTIME2", (ftnlen)200, (ftnlen)16);
		chcksd_(qname, &finish, "~", &xtime2, &c_b103, ok, (ftnlen)
			200, (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
/* ********************************************************************* */
/* * */
/* *    Absolute minimum tests */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find absolute minima of the alphafixed longitude */
/*     of the Alpha-gamma vector. */


    str2et_("2000 JAN 1 18:00 TDB ", &et0, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We'll loop over the various coordinate systems. */

    for (cc = 1; cc <= 7; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng3__", (ftnlen)899)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng3__", (ftnlen)900)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "WEST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "WEST", (ftnlen)4, (ftnlen)4);
	} else if (eqstr_(crdsys, "Planetographic_2", (ftnlen)32, (ftnlen)16))
		 {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "EAST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14);
	} else {
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	}
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Loop over different confinement windows. */
/*        We pick the window end times so that the */
/*        minima will be multiples of pi/8. */

	for (si = 1; si <= 16; ++si) {
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		t1 = et0;
		t2 = et0 + (si - 1) * 1.5 * 3600.;

/*              Set the expected time of the minimum. */

		xtime = t2;
	    } else {
		t1 = et0 + (si - 1) * 1.5 * 3600.;
		if (si <= 8) {
		    t2 = et0 + spd_() / 2;
		} else {
		    t2 = et0 + spd_();
		}

/*              Set the expected time of the minimum. */

		xtime = t1;
	    }
	    scardd_(&c__0, cnfine);
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    refval = (si - 1) * pi_() / 8.;

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Alpha-gamma abs minimum; #; #; ref val #", (ftnlen)
		    200, (ftnlen)40);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    d__1 = dpr_() * refval;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    tcase_(title, (ftnlen)200);

/*           Set parameters required for the search. */

	    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
	    s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
	    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	    s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
	    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	    cleard_(&c__3, dvec);
	    s_copy(relate, "ABSMIN", (ftnlen)40, (ftnlen)6);
	    refval = 0.;
	    tol = 1e-6;
	    adjust = 0.;
	    gfsstp_(&c_b71);
	    rpt = FALSE_;
	    bail = FALSE_;
	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (si == 1) {
		if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {

/*                 There's a branch cut at 0: the confinement window */
/*                 lies in the deleted sector that encloses the branch */
/*                 cut. The result window should be empty. */

		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else if (eqstr_(crdsys, "RA/DEC", (ftnlen)32, (ftnlen)6) || 
			eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)
			14) || eqstr_(crdsys, "CYLINDRICAL", (ftnlen)32, (
			ftnlen)11)) {

/*                 There's a branch cut at 0: however we've made */
/*                 the confinement window cover the upper half */
/*                 circle. The result window should be non-empty. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {

/*                 The confinement window should be a singleton set */
/*                 at which the absolute minimum is found. */

		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else {

/*              We expect the result window to contain one */
/*              solution interval. */

		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    }
	    if (n == 1) {
		wnfetd_(result, &c__1, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
/*              Check that START is equal to XTIME. */

		s_copy(qname, "START vs XTIME", (ftnlen)200, (ftnlen)14);
		chcksd_(qname, &start, "~", &xtime, &c_b103, ok, (ftnlen)200, 
			(ftnlen)1);

/*              Check that START and FINISH match. */

		s_copy(qname, "START vs FINISH", (ftnlen)200, (ftnlen)15);
		chcksd_(qname, &start, "~", &finish, &c_b103, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
/* ********************************************************************* */
/* * */
/* *    Adjusted absolute minimum tests */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     We're going to find adjusted absolute minima of the alphafixed */
/*     longitude of the Alpha-gamma vector. In each case we'll use */
/*     an adjustment value of 15 arcseconds, which should shift the */
/*     minima by 1 second. */


    str2et_("2000 JAN 1 18:00 TDB ", &et0, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = rpd_() * 15. / 3600.;

/*     We'll loop over the various coordinate systems. */

    for (cc = 1; cc <= 7; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng3__", (ftnlen)1140)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng3__", (ftnlen)1141)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "WEST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "WEST", (ftnlen)4, (ftnlen)4);
	} else if (eqstr_(crdsys, "Planetographic_2", (ftnlen)32, (ftnlen)16))
		 {
	    pcpool_("BODY1000_PGR_POSITIVE_LON", &c__1, "EAST", (ftnlen)25, (
		    ftnlen)4);
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14);
	} else {
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	}
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Loop over different confinement windows. */
/*        We pick the window end times so that the */
/*        minima will be multiples of pi/4. */

	for (si = 1; si <= 16; ++si) {
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		t1 = et0;
		t2 = et0 + (si - 1) * 1.5 * 3600.;

/*              Set the expected time of the minimum */
/*              and the adjusted minimum. */

/* Computing MAX */
		d__1 = t2 - 1.;
		xtime = max(d__1,t1);
		xtime2 = t2;
	    } else {
		t1 = et0 + (si - 1) * 1.5 * 3600.;
		if (si <= 8) {
		    t2 = et0 + spd_() / 2;
		} else {
		    t2 = et0 + spd_();
		}

/*              Set the expected time of the minimum */
/*              and the adjusted minimum. */

		xtime = t1;
/* Computing MIN */
		d__1 = t1 + 1.;
		xtime2 = min(d__1,t2);
	    }
	    scardd_(&c__0, cnfine);
	    wninsd_(&t1, &t2, cnfine);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    refval = (si - 1) * pi_() / 8.;
	    if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
		refval = twopi_() - refval;
	    }
	    if (refval < 0.) {
		refval += twopi_();
	    }

/* --- Case: ------------------------------------------------------ */

	    s_copy(title, "Adjusted alpha-gamma minimum; #; #; #; min lon #", 
		    (ftnlen)200, (ftnlen)48);
	    repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)
		    32, (ftnlen)200);
	    d__1 = dpr_() * adjust;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    d__1 = dpr_() * refval;
	    repmf_(title, "#", &d__1, &c__6, "F", title, (ftnlen)200, (ftnlen)
		    1, (ftnlen)1, (ftnlen)200);
	    tcase_(title, (ftnlen)200);

/*           Set parameters required for the search. */

	    s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	    s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(target, "GAMMA", (ftnlen)36, (ftnlen)5);
	    s_copy(obsrvr, "ALPHA", (ftnlen)36, (ftnlen)5);
	    s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	    s_copy(ref, "ALPHAFIXED", (ftnlen)32, (ftnlen)10);
	    s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	    cleard_(&c__3, dvec);
	    s_copy(relate, "ABSMIN", (ftnlen)40, (ftnlen)6);
	    refval = 0.;
	    tol = 1e-6;
	    gfsstp_(&c_b71);
	    rpt = FALSE_;
	    bail = FALSE_;
	    zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec,
		     crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)
		    gfstep_, (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)
		    gfrepu_, (U_fp)gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &
		    c__15, work, cnfine, result, (ftnlen)32, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (ftnlen)
		    32, (ftnlen)32, (ftnlen)32, (ftnlen)40);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    n = wncard_(result);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect the result window to contain one */
/*           solution interval. */

	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		if (si > 1) {
		    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		} else {
		    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (
			    ftnlen)1);
		}
	    } else {
		chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    }
	    if (n == 1) {
		wnfetd_(result, &c__1, &start, &finish);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check that START is equal to XTIME. */

		s_copy(qname, "START vs XTIME", (ftnlen)200, (ftnlen)14);
		chcksd_(qname, &start, "~", &xtime, &c_b103, ok, (ftnlen)200, 
			(ftnlen)1);

/*              Check that FINISH is equal to XTIME2. */

		s_copy(qname, "FINISH vs XTIME2", (ftnlen)200, (ftnlen)16);
		chcksd_(qname, &finish, "~", &xtime2, &c_b103, ok, (ftnlen)
			200, (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
/* ********************************************************************* */
/* * */
/* *    Local maximum/minimum tests */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */


/*     Local max/min tests: */

/*     We're going to find local extrema of the J2000 longitude */
/*     of the Sun-IO vector. Jupiter's orbital angular rate is */
/*     slow enough so that Io exhibits periods of retrograde */
/*     motion. */

/*     In order to be able to use geodetic and planetographic */
/*     coordinate systems, we need a reference frame aligned with */
/*     the J2000 frame, but whose center is the Sun. We create */
/*     such a frame specification below and insert it into the */
/*     kernel pool via LMPOOL. */

    s_copy(frmtxt, "FRAME_SUNJ2000             = 1400000", (ftnlen)200, (
	    ftnlen)36);
    s_copy(frmtxt + 200, "FRAME_1400000_NAME         = 'SUNJ2000' ", (ftnlen)
	    200, (ftnlen)40);
    s_copy(frmtxt + 400, "FRAME_1400000_CLASS        = 4", (ftnlen)200, (
	    ftnlen)30);
    s_copy(frmtxt + 600, "FRAME_1400000_CLASS_ID     = 1400000", (ftnlen)200, 
	    (ftnlen)36);
    s_copy(frmtxt + 800, "FRAME_1400000_CENTER       = 10", (ftnlen)200, (
	    ftnlen)31);
    s_copy(frmtxt + 1000, "TKFRAME_1400000_RELATIVE   = 'J2000' ", (ftnlen)
	    200, (ftnlen)37);
    s_copy(frmtxt + 1200, "TKFRAME_1400000_SPEC       = 'MATRIX' ", (ftnlen)
	    200, (ftnlen)38);
    s_copy(frmtxt + 1400, "TKFRAME_1400000_MATRIX     = ( 1,0,0,0,1,0,0,0,1 )"
	    , (ftnlen)200, (ftnlen)50);
    s_copy(frmtxt + 1600, "OBJECT_SUN_FRAME           = 'SUNJ2000' ", (ftnlen)
	    200, (ftnlen)40);
    nlines = 9;
    lmpool_(frmtxt, &nlines, (ftnlen)200);

/*     Our confinement window will sample from Jupiter's orbit */
/*     in such a way that all branches of ZZGFLONG's local */
/*     extremum algorithm will be exercised. We'll use the */
/*     first few days of each year out of a 12 year period */
/*     as our confinement window. */

    str2et_("2000 JAN 1 TDB", &et0, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__40000, cnfine);
    ssized_(&c__40000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 12; ++i__) {
	t1 = et0 + (i__ - 1) * jyear_();
	t2 = t1 + spd_() * 3.5;
	wninsd_(&t1, &t2, cnfine);
    }

/*     We'll loop over the various coordinate systems. */

    for (cc = 1; cc <= 7; ++cc) {

/*        Set the coordinate system and coordinate. */

	s_copy(crdsys, csys + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("csys", i__1, "f_zzgflng3__", (ftnlen)1406)) << 5), (
		ftnlen)32, (ftnlen)32);
	s_copy(crdnam, cnam + (((i__1 = cc - 1) < 7 && 0 <= i__1 ? i__1 : 
		s_rnge("cnam", i__1, "f_zzgflng3__", (ftnlen)1407)) << 5), (
		ftnlen)32, (ftnlen)32);
	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Set the longer equatorial radius of Alpha equal to */
/*           the shorter: unequal equatorial radii are not supported */
/*           for these systems. */

	    bodvrd_("ALPHA", "RADII", &c__3, &n, alphrd, (ftnlen)5, (ftnlen)5)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Note: use of the subscript "2" in the first argument */
/*           below is intentional. */

	    vpack_(&alphrd[1], &alphrd[1], &alphrd[2], tmprad);
	    pdpool_("BODY1000_RADII", &c__3, tmprad, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Sun-Io local maximum #; #", (ftnlen)200, (ftnlen)25);
	repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)32, 
		(ftnlen)200);
	repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)32, 
		(ftnlen)200);
	tcase_(title, (ftnlen)200);
	if (eqstr_(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {
	    pcpool_("BODY10_PGR_POSITIVE_LON", &c__1, "WEST", (ftnlen)23, (
		    ftnlen)4);
	    s_copy(lonsns, "WEST", (ftnlen)4, (ftnlen)4);
	} else if (eqstr_(crdsys, "Planetographic_2", (ftnlen)32, (ftnlen)16))
		 {
	    pcpool_("BODY10_PGR_POSITIVE_LON", &c__1, "EAST", (ftnlen)23, (
		    ftnlen)4);
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	    s_copy(crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14);
	} else {
	    s_copy(lonsns, "EAST", (ftnlen)4, (ftnlen)4);
	}

/*        Set parameters required for the search. */

	s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	s_copy(target, "IO", (ftnlen)36, (ftnlen)2);
	s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
	s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	s_copy(ref, "SUNJ2000", (ftnlen)32, (ftnlen)8);
	s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	cleard_(&c__3, dvec);
	s_copy(relate, "LOCMAX", (ftnlen)40, (ftnlen)6);
	refval = 0.;

/*        Use a loose tolerance to speed things up a bit. */

	tol = .01;
	adjust = 0.;
	gfsstp_(&c_b456);
	rpt = FALSE_;
	bail = FALSE_;
	zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, 
		crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_,
		 (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)
		gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &c__15, work, 
		cnfine, result, (ftnlen)32, (ftnlen)80, (ftnlen)36, (ftnlen)
		32, (ftnlen)200, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
		32, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	n = wncard_(result);

/*        We expect each confinement interval to contain two */
/*        solutions, so we expect 24 solutions in all. */

	chcksi_("N (0)", &n, "=", &c__24, &c__0, ok, (ftnlen)5, (ftnlen)1);
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    etcal_(&finish, timstr, (ftnlen)35);

/*           Check the coordinate at the event time +/- TDELTA */
/*           seconds. */

	    tdelta = 3.;
	    d__1 = finish - tdelta;
	    zzgfcog_(&d__1, &coord0);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    zzgfcog_(&finish, &coord);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = finish + tdelta;
	    zzgfcog_(&d__1, &coord2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "#: COORD vs COORD0", (ftnlen)200, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200);
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		chcksd_(qname, &coord, "<", &coord0, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    } else {
		chcksd_(qname, &coord, ">", &coord0, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	    s_copy(qname, "#: COORD2 vs COORD", (ftnlen)200, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200);
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		chcksd_(qname, &coord, "<", &coord2, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    } else {
		chcksd_(qname, &coord, ">", &coord2, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */


/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Sun-Io local minimum #; #", (ftnlen)200, (ftnlen)25);
	repmc_(title, "#", crdsys, title, (ftnlen)200, (ftnlen)1, (ftnlen)32, 
		(ftnlen)200);
	repmc_(title, "#", crdnam, title, (ftnlen)200, (ftnlen)1, (ftnlen)32, 
		(ftnlen)200);
	tcase_(title, (ftnlen)200);

/*        Set parameters required for the search. */

	s_copy(vecdef, "POSITION", (ftnlen)32, (ftnlen)8);
	s_copy(method, " ", (ftnlen)80, (ftnlen)1);
	s_copy(target, "IO", (ftnlen)36, (ftnlen)2);
	s_copy(obsrvr, "SUN", (ftnlen)36, (ftnlen)3);
	s_copy(abcorr, "NONE", (ftnlen)200, (ftnlen)4);
	s_copy(ref, "SUNJ2000", (ftnlen)32, (ftnlen)8);
	s_copy(dref, " ", (ftnlen)32, (ftnlen)1);
	cleard_(&c__3, dvec);
	s_copy(relate, "LOCMIN", (ftnlen)40, (ftnlen)6);
	refval = 0.;

/*        Use a loose tolerance to speed things up a bit. */

	tol = .01;
	adjust = 0.;
	gfsstp_(&c_b456);
	rpt = FALSE_;
	bail = FALSE_;
	zzgflong_(vecdef, method, target, ref, abcorr, obsrvr, dref, dvec, 
		crdsys, crdnam, relate, &refval, &tol, &adjust, (U_fp)gfstep_,
		 (U_fp)gfrefn_, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)
		gfrepf_, &bail, (L_fp)gfbail_, &c__40000, &c__15, work, 
		cnfine, result, (ftnlen)32, (ftnlen)80, (ftnlen)36, (ftnlen)
		32, (ftnlen)200, (ftnlen)36, (ftnlen)32, (ftnlen)32, (ftnlen)
		32, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	n = wncard_(result);

/*        We expect each confinement interval to contain two */
/*        solutions, so we expect 24 solutions in all. */

	chcksi_("N (0)", &n, "=", &c__24, &c__0, ok, (ftnlen)5, (ftnlen)1);
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    etcal_(&finish, timstr, (ftnlen)35);

/*           Check the coordinate at the event time +/- TDELTA */
/*           seconds. */

	    tdelta = 3.;
	    d__1 = finish - tdelta;
	    zzgfcog_(&d__1, &coord0);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    zzgfcog_(&finish, &coord);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    d__1 = finish + tdelta;
	    zzgfcog_(&d__1, &coord2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(qname, "#: COORD vs COORD0", (ftnlen)200, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200);
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		chcksd_(qname, &coord, ">", &coord0, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    } else {
		chcksd_(qname, &coord, "<", &coord0, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	    s_copy(qname, "#: COORD2 vs COORD", (ftnlen)200, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)200, (ftnlen)1, (ftnlen)
		    200);
	    if (s_cmp(lonsns, "WEST", (ftnlen)4, (ftnlen)4) == 0) {
		chcksd_(qname, &coord, ">", &coord2, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    } else {
		chcksd_(qname, &coord, "<", &coord2, &c_b475, ok, (ftnlen)200,
			 (ftnlen)1);
	    }
	}

/*        End of extreme value check loop. */

	if (eqstr_(crdsys, "GEODETIC", (ftnlen)32, (ftnlen)8) || eqstr_(
		crdsys, "PLANETOGRAPHIC", (ftnlen)32, (ftnlen)14)) {

/*           Restore Alpha's radii. */

	    pdpool_("BODY1000_RADII", &c__3, alphrd, (ftnlen)14);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     End of coordinate system loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgflong.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgflng3__ */

