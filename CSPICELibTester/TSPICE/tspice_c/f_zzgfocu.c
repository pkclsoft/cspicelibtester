/* f_zzgfocu.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__3 = 3;
static integer c__2 = 2;

/* $Procedure      F_ZZGFOCU ( ZZGFOCU family tests ) */
/* Subroutine */ int f_zzgfocu__(logical *ok)
{
    extern /* Subroutine */ int zzgfocin_(char *, char *, char *, char *, 
	    char *, char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static integer n;
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *), topen_(char *, ftnlen), t_success__(
	    logical *);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    static doublereal newrad[3];
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen);
    static logical ocstat;
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), spkuef_(integer *), natspk_(char *, logical *, integer *,
	     ftnlen), tstlsk_(void), zzgfocu_(char *, char *, char *, char *, 
	    char *, char *, char *, char *, char *, doublereal *, logical *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen);

/* $ Abstract */

/*     This routine tests *some functionality* of the SPICELIB routine */

/*        ZZGFOCU */

/*     To fully exercise ZZGFOCU, the test family */

/*        F_GFOCLT */

/*     must be run in addition to this family. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the low-level SPICELIB */
/*     geometry utility package ZZGFOCU. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 4.0.0, 22-FEB-2016 (NJB) */

/*        Updated expected keywords for invalid shape cases. */

/* -    TSPICE Version 3.1.0, 14-APR-2008 (NJB) */

/*        Kernel clean-up now consists only of unloading */
/*        the SPK file via SPKUEF and then deleting the */
/*        file. */

/* -    TSPICE Version 3.0.0, 12-APR-2008 (NJB) */

/*        Updated to use new names ZZGFOCU and F_ZZGFOCU. */
/*        "Header" such as it is now passes checkit tests. */

/* -    TSPICE Version 2.0.0, 29-MAR-2008 (NJB) */

/*        Updated to exercise new ZZGFOCIN error checks. */

/* -    TSPICE Version 1.1.0, 13-MAR-2008 (NJB) */

/*        Removed unnecessary variable STEP. */

/* -    TSPICE Version 1.0.0, 16-NOV-2007 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFOCU", (ftnlen)9);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    natspk_("nat.bsp", &c_true, &handle, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natpck_("nat.tpc", &c_true, &c_false, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCU: bogus entry", (ftnlen)20);
    et = 0.;
    zzgfocu_("Any", "ALPHAx", "point", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "SUN", "LT", &et, &ocstat, (ftnlen)3, (ftnlen)6, (
	    ftnlen)5, (ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, 
	    (ftnlen)2);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: Unrecognized targets or observer", (ftnlen)42);
    zzgfocin_("Any", "ALPHAx", "point", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)6, (ftnlen)5, (
	    ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzgfocin_("Any", "ALPHA", "point", "ALPHAFIXED", "BETAx", "ellipsoid", 
	    "BETAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)5, (
	    ftnlen)10, (ftnlen)5, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzgfocin_("Any", "ALPHA", "point", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "SUNx", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)5, (
	    ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: non-positive radii for back target", (ftnlen)44);
    bodvrd_("ALPHA", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    moved_(radii, &c__3, newrad);
    newrad[0] = 0.;
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
    moved_(radii, &c__3, newrad);
    newrad[1] = -1.;
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
    moved_(radii, &c__3, newrad);
    newrad[2] = -1.;
    pdpool_("BODY1000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore original radii. */

    pdpool_("BODY1000_RADII", &c__3, radii, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: non-positive radii for front target", (ftnlen)45);
    bodvrd_("BETA", "RADII", &c__3, &n, radii, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    moved_(radii, &c__3, newrad);
    newrad[0] = 0.;
    pdpool_("BODY2000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
    moved_(radii, &c__3, newrad);
    newrad[1] = -1.;
    pdpool_("BODY2000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
    moved_(radii, &c__3, newrad);
    newrad[2] = -1.;
    pdpool_("BODY2000_RADII", &c__3, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/*     Restore original radii. */

    pdpool_("BODY2000_RADII", &c__3, radii, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: bad radius count for back target", (ftnlen)42);
    bodvrd_("ALPHA", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    moved_(radii, &c__3, newrad);
    newrad[0] = 0.;
    pdpool_("BODY1000_RADII", &c__2, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BADRADIUSCOUNT)", ok, (ftnlen)21);

/*     Restore original radii. */

    pdpool_("BODY1000_RADII", &c__3, radii, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: bad radius count for front target", (ftnlen)43);
    bodvrd_("BETA", "RADII", &c__3, &n, radii, (ftnlen)4, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    moved_(radii, &c__3, newrad);
    newrad[0] = 0.;
    pdpool_("BODY2000_RADII", &c__2, newrad, (ftnlen)14);
    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BADRADIUSCOUNT)", ok, (ftnlen)21);

/*     Restore original radii. */

    pdpool_("BODY2000_RADII", &c__3, radii, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: blank frames for ellipsoidal targets", (ftnlen)46);
    zzgfocin_("Any", "ALPHA", "ELLIPSOID", " ", "BETA", "ellipsoid", "BETAFI"
	    "XED", "SUN", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)9, (ftnlen)1, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);
    zzgfocin_("Any", "ALPHA", "ELLIPSOID", "ALPHAFIXED", "BETA", "ellipsoid", 
	    " ", "SUN", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)9, (ftnlen)10, (
	    ftnlen)4, (ftnlen)9, (ftnlen)1, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: bad frame centers for ellipsoidal targets", (ftnlen)51);
    zzgfocin_("Any", "ALPHA", "ELLIPSOID", "BETAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)9, (
	    ftnlen)9, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);
    zzgfocin_("Any", "ALPHA", "ELLIPSOID", "ALPHAFIXED", "BETA", "ellipsoid", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)5, (ftnlen)9, (
	    ftnlen)10, (ftnlen)4, (ftnlen)9, (ftnlen)10, (ftnlen)3, (ftnlen)2)
	    ;
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: bad shape combination", (ftnlen)31);
    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "point", "ALPHAF"
	    "IXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (ftnlen)9, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDSHAPECOMBO)", ok, (ftnlen)24);
/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFOCIN: bad target shape", (ftnlen)26);
    zzgfocin_("Any", "BETA", "sphere", "BETAFIXED", "ALPHA", "point", "ALPHA"
	    "FIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)6, (ftnlen)9, (
	    ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);
    zzgfocin_("Any", "BETA", "ELLIPSOID", "BETAFIXED", "ALPHA", "sphere", 
	    "ALPHAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)6, (ftnlen)10, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BADMETHODSYNTAX)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFIIOC: ellipsoid case: participants coincide", (ftnlen)47);

/*     Front ellipsoid target is observer. */

    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "BETA", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
/*      ET = 0.D0 */
/*      CALL ZZGFIOCC ( ET, OCSTAT ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(NOTDISJOINT)',   OK ) */

/*     Back ellipsoid target is observer. */

    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "ALPHA", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)5, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
/*      ET = 0.D0 */
/*      CALL ZZGFIOCC ( ET, OCSTAT ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(NOTDISJOINT)',   OK ) */

/*     Front ellipsoid target is back target. */

    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "BETA", "ellipsoid", 
	    "BETAFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
/*      CALL CHCKXC ( .FALSE., ' ',   OK ) */
/*      CALL CHCKXC ( .FALSE., ' ',   OK ) */
/*      ET = 0.D0 */
/*      CALL ZZGFIOCC ( ET, OCSTAT ) */
/*      CALL CHCKXC ( .TRUE., 'SPICE(NOTDISJOINT)',   OK ) */

/* ---- Case ------------------------------------------------------------- */

    tcase_("ZZGFIIOC: point target case: participants coincide", (ftnlen)50);

/*     Front point target is observer. */

    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "ALPHA", "ellipsoid", 
	    "ALPHAFIXED", "BETA", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (
	    ftnlen)9, (ftnlen)5, (ftnlen)9, (ftnlen)10, (ftnlen)4, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*     Back point target is observer. */

    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "ALPHA", "point", 
	    "ALPHAFIXED", "ALPHA", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (
	    ftnlen)9, (ftnlen)5, (ftnlen)5, (ftnlen)10, (ftnlen)5, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*     Front point target is back target. */

    zzgfocin_("Any", "BETA", "point", "BETAFIXED", "BETA", "ellipsoid", "BET"
	    "AFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)5, (ftnlen)9, 
	    (ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*     Back point target is front target. */

    zzgfocin_("Any", "BETA", "ellipsoid", "BETAFIXED", "BETA", "point", "BET"
	    "AFIXED", "SUN", "LT", (ftnlen)3, (ftnlen)4, (ftnlen)9, (ftnlen)9, 
	    (ftnlen)4, (ftnlen)5, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad aberration correction values.", (ftnlen)33);
    zzgfocin_("Any", "ALPHA", "point", "NULL", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "SUN", "S", (ftnlen)3, (ftnlen)5, (ftnlen)5, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfocin_("Any", "ALPHA", "point", "NULL", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "SUN", "XS", (ftnlen)3, (ftnlen)5, (ftnlen)5, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfocin_("Any", "ALPHA", "point", "NULL", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "SUN", "RLT", (ftnlen)3, (ftnlen)5, (ftnlen)5, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfocin_("Any", "ALPHA", "point", "NULL", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "SUN", "XRLT", (ftnlen)3, (ftnlen)5, (ftnlen)5, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzgfocin_("Any", "ALPHA", "point", "NULL", "BETA", "ELLIPSOID", "BETAFIX"
	    "ED", "SUN", "z", (ftnlen)3, (ftnlen)5, (ftnlen)5, (ftnlen)4, (
	    ftnlen)4, (ftnlen)9, (ftnlen)9, (ftnlen)3, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up the SPK file. */

    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfocu__ */

