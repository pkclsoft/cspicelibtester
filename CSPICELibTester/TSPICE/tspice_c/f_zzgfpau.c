/* f_zzgfpau.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;

/* $Procedure F_ZZGFPAU ( ZZGFPAU family tests ) */
/* Subroutine */ int f_zzgfpau__(logical *ok)
{
    /* Initialized data */

    static char rcorr[5*5] = "nOne " "lT   " "Cn   " "Lt+s " "cN+S ";
    static char xcorr[5*4] = "xlT  " "xCn  " "XLt+s" "XcN+S";
    static char target[36*6] = "ALPHA                               " "ALPHA"
	    "                               " "X                             "
	    "      " "ALPHA                               " "SUN             "
	    "                    " "ALPHA                               ";
    static char illumn[36*6] = "SUN                                 " "X    "
	    "                               " "SUN                           "
	    "      " "ALPHA                               " "SUN             "
	    "                    " "SUN                                 ";
    static char obsrvr[36*6] = "X                                   " "BETA "
	    "                               " "BETA                          "
	    "      " "BETA                                " "ALPHA           "
	    "                    " "SUN                                 ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    logical ablk[15];
    char targ[36];
    integer xobs, yobs;
    extern /* Subroutine */ int zzgfpadc_(U_fp, doublereal *, logical *), 
	    zzgfpain_(char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), zzgfpagq_(doublereal *, doublereal *), zzvalcor_(char *, 
	    logical *, ftnlen);
    integer i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen);
    logical xablk[15];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    logical found;
    char illum[36];
    integer xtarg, ytarg;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , bods2c_(char *, integer *, logical *, ftnlen);
    doublereal et;
    integer handle;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), delfil_(char *, 
	    ftnlen), kclear_(void), chckxc_(logical *, char *, logical *, 
	    ftnlen);
    logical decres;
    char abcorr[5];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen);
    char xabcor[5];
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     natspk_(char *, logical *, integer *, ftnlen);
    integer xillum, yillum;
    extern /* Subroutine */ int furnsh_(char *, ftnlen);
    extern /* Subroutine */ int udf_();
    char obs[36];
    doublereal rvl;
    char txt[160];
    extern /* Subroutine */ int zzgfpax_(integer *, integer *, char *, 
	    integer *, logical *, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB utility package */

/*        ZZGFPAU */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB geometry utility package ZZGFPAU. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 18-NOV-2013 (EDW) */

/* -& */

/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Indices 1:3 for Invalid body name test, 4:6 for not distinct */
/*     body names test. */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFPAU", (ftnlen)9);

/*     Case 1: Create test kernels. */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    natspk_("nat.spk", &c_true, &handle, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.spk", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 2: Invalid body names. */

    s_copy(abcorr, rcorr, (ftnlen)5, (ftnlen)5);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(targ, target + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("target", i__1, "f_zzgfpau__", (ftnlen)235)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(illum, illumn + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("illumn", i__1, "f_zzgfpau__", (ftnlen)236)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_zzgfpau__", (ftnlen)237)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(txt, "Invalid body name test. TARG = #, ILLUM = #, OBS = #", (
		ftnlen)160, (ftnlen)52);
	repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", illum, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	tcase_(txt, (ftnlen)160);
	zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5,
		 (ftnlen)36);
	chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    }

/*     Case 3: Invalid aberration corrections. */


/*     Set appropriate values for TARGET, ILLUMN, OBSRVR. */

    s_copy(targ, target, (ftnlen)36, (ftnlen)36);
    s_copy(illum, illumn, (ftnlen)36, (ftnlen)36);
    s_copy(obs, obsrvr + 36, (ftnlen)36, (ftnlen)36);
    for (i__ = 1; i__ <= 4; ++i__) {
	s_copy(abcorr, xcorr + ((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("xcorr", i__1, "f_zzgfpau__", (ftnlen)267)) * 5, (
		ftnlen)5, (ftnlen)5);
	s_copy(txt, "Invalid aberration correction. ABCOR = #", (ftnlen)160, (
		ftnlen)40);
	repmc_(txt, "#", abcorr, txt, (ftnlen)160, (ftnlen)1, (ftnlen)5, (
		ftnlen)160);
	tcase_(txt, (ftnlen)160);
	zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5,
		 (ftnlen)36);
	chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    }

/*     Case 4: Not distinct body names. */


/*     Set appropriate values for ABCORR. */

    s_copy(abcorr, rcorr, (ftnlen)5, (ftnlen)5);
    for (i__ = 4; i__ <= 6; ++i__) {
	s_copy(targ, target + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("target", i__1, "f_zzgfpau__", (ftnlen)293)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(illum, illumn + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("illumn", i__1, "f_zzgfpau__", (ftnlen)294)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_zzgfpau__", (ftnlen)295)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(txt, "Not distinct body name test. TARG = #, ILLUM = #, OBS ="
		" #", (ftnlen)160, (ftnlen)57);
	repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", illum, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	tcase_(txt, (ftnlen)160);
	zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5,
		 (ftnlen)36);
	chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    }

/*     Case 5: Confirm initialized values are correctly saved. */

    s_copy(targ, target, (ftnlen)36, (ftnlen)36);
    s_copy(illum, illumn, (ftnlen)36, (ftnlen)36);
    s_copy(obs, obsrvr + 36, (ftnlen)36, (ftnlen)36);
    s_copy(abcorr, rcorr, (ftnlen)5, (ftnlen)5);
    bods2c_(targ, &ytarg, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bods2c_(illum, &yillum, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bods2c_(obs, &yobs, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Aberration correction NONE. */

    for (j = 1; j <= 6; ++j) {
	ablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("ablk", i__1, 
		"f_zzgfpau__", (ftnlen)337)] = FALSE_;
	xablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("xablk", i__1, 
		"f_zzgfpau__", (ftnlen)338)] = FALSE_;
    }
    s_copy(txt, "Initialize then check saved values in ZZGFPAIN. TARG = #, I"
	    "LLUM = #, OBS = #, ABCORR = #.", (ftnlen)160, (ftnlen)89);
    repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", illum, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", abcorr, txt, (ftnlen)160, (ftnlen)1, (ftnlen)5, (ftnlen)
	    160);
    tcase_(txt, (ftnlen)160);
    zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5, (
	    ftnlen)36);
    zzgfpax_(&xtarg, &xillum, xabcor, &xobs, xablk, (ftnlen)5);
    chcksi_("TARG X vs Y", &xtarg, "=", &ytarg, &c__0, ok, (ftnlen)11, (
	    ftnlen)1);
    chcksi_("ILLUM X vs Y", &xillum, "=", &yillum, &c__0, ok, (ftnlen)12, (
	    ftnlen)1);
    chcksi_("OBS X vs Y", &xobs, "=", &yobs, &c__0, ok, (ftnlen)10, (ftnlen)1)
	    ;
    ucase_(abcorr, abcorr, (ftnlen)5, (ftnlen)5);
    chcksc_("ABCORR vs XABCORR", xabcor, "=", abcorr, ok, (ftnlen)17, (ftnlen)
	    5, (ftnlen)1, (ftnlen)5);
    zzvalcor_(abcorr, ablk, (ftnlen)5);
    for (j = 1; j <= 6; ++j) {
	chcksl_("ABLK", &xablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : 
		s_rnge("xablk", i__1, "f_zzgfpau__", (ftnlen)364)], &ablk[(
		i__2 = j - 1) < 15 && 0 <= i__2 ? i__2 : s_rnge("ablk", i__2, 
		"f_zzgfpau__", (ftnlen)364)], ok, (ftnlen)4);
    }

/*     Aberration correction not NONE. */

    for (i__ = 2; i__ <= 5; ++i__) {
	for (j = 1; j <= 6; ++j) {
	    ablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("ablk", 
		    i__1, "f_zzgfpau__", (ftnlen)376)] = FALSE_;
	    xablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("xablk", 
		    i__1, "f_zzgfpau__", (ftnlen)377)] = FALSE_;
	}
	s_copy(txt, "Initialize then check saved values in ZZGFPAIN. TARG = "
		"#, ILLUM = #, OBS = #, ABCORR = #.", (ftnlen)160, (ftnlen)89);
	repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", illum, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	s_copy(abcorr, rcorr + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : 
		s_rnge("rcorr", i__1, "f_zzgfpau__", (ftnlen)386)) * 5, (
		ftnlen)5, (ftnlen)5);
	repmc_(txt, "#", abcorr, txt, (ftnlen)160, (ftnlen)1, (ftnlen)5, (
		ftnlen)160);
	zzvalcor_(abcorr, ablk, (ftnlen)5);
	tcase_(txt, (ftnlen)160);
	zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5,
		 (ftnlen)36);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzgfpax_(&xtarg, &xillum, xabcor, &xobs, xablk, (ftnlen)5);
	ucase_(abcorr, abcorr, (ftnlen)5, (ftnlen)5);
	chcksc_("ABCORR vs Y", xabcor, "=", abcorr, ok, (ftnlen)11, (ftnlen)5,
		 (ftnlen)1, (ftnlen)5);
	for (j = 1; j <= 6; ++j) {
	    chcksl_(txt, &xablk[(i__1 = j - 1) < 15 && 0 <= i__1 ? i__1 : 
		    s_rnge("xablk", i__1, "f_zzgfpau__", (ftnlen)403)], &ablk[
		    (i__2 = j - 1) < 15 && 0 <= i__2 ? i__2 : s_rnge("ablk", 
		    i__2, "f_zzgfpau__", (ftnlen)403)], ok, (ftnlen)160);
	}
    }

/*     Case 6: Check calculation of phase angle derivative. */


/*     Occultation of ALPHA by BETA as observed from Sol starts */
/*     at 0 TDB secs from J2000 epoch (geometric). The occultation */
/*     lasts ten minutes. */

/*     Confirm the phase angle decreases prior to occultation */
/*     and increases post occultation. */

    s_copy(targ, "ALPHA", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(obs, "BETA", (ftnlen)36, (ftnlen)4);

/*     Loop over all aberration corrections. */

    for (i__ = 1; i__ <= 5; ++i__) {
	s_copy(abcorr, rcorr + ((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : 
		s_rnge("rcorr", i__1, "f_zzgfpau__", (ftnlen)430)) * 5, (
		ftnlen)5, (ftnlen)5);
	s_copy(txt, "Phase angle decreasing prior and increasing post occult"
		"aion. ABCORR = #.", (ftnlen)160, (ftnlen)72);
	repmc_(txt, "#", abcorr, txt, (ftnlen)160, (ftnlen)1, (ftnlen)5, (
		ftnlen)160);
	tcase_(txt, (ftnlen)160);
	zzgfpain_(targ, illum, abcorr, obs, (ftnlen)36, (ftnlen)36, (ftnlen)5,
		 (ftnlen)36);
	decres = FALSE_;

/*        Ten minutes prior to start of occultation. */

	et = -600.;
	zzgfpagq_(&et, &rvl);
	zzgfpadc_((U_fp)udf_, &et, &decres);
	chcksl_(txt, &decres, &c_true, ok, (ftnlen)160);

/*        Ten minutes post to start of occultation. */

	et = 600.;
	zzgfpagq_(&et, &rvl);
	zzgfpadc_((U_fp)udf_, &et, &decres);
	chcksl_(txt, &decres, &c_false, ok, (ftnlen)160);
    }

/*     Case N: */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.spk", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfpau__ */

