/* f_zzgfrelx.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__100 = 100;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__12 = 12;
static integer c__2 = 2;
static integer c__0 = 0;
static doublereal c_b161 = 0.;
static integer c__5 = 5;
static integer c__19 = 19;
static integer c__10 = 10;
static integer c__3 = 3;
static integer c__20 = 20;
static integer c__18 = 18;
static integer c__9 = 9;
static integer c_n1 = -1;

/* $Procedure  F_ZZGFRELX ( Test ZZGFRELX ) */
/* Subroutine */ int f_zzgfrelx__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal);

    /* Local variables */
    static logical bail;
    static doublereal xbeg[50], xend[50];
    extern /* Subroutine */ int t_lt__();
    static doublereal ftol, xval[50], work[1590]	/* was [106][15] */;
    extern /* Subroutine */ int zzgfrelx_(U_fp, U_fp, U_fp, U_fp, S_fp, char *
	    , doublereal *, doublereal *, doublereal *, doublereal *, integer 
	    *, integer *, doublereal *, logical *, U_fp, U_fp, U_fp, char *, 
	    char *, logical *, L_fp, doublereal *, ftnlen, ftnlen, ftnlen);
    static doublereal e;
    static integer i__, j;
    extern /* Subroutine */ int t_dec__();
    static integer n;
    static doublereal delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int t_get__(doublereal *, doublereal *);
    static doublereal value;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), t_set__(char *, ftnlen);
    static integer ncrit;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xtime[50], start, short__[7];
    extern /* Subroutine */ int t_success__(logical *);
    extern logical gfbail_();
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal cnfine[106];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer mw;
    extern /* Subroutine */ int gfrefn_();
    static integer nw;
    extern /* Subroutine */ int gfrepf_();
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    extern /* Subroutine */ int gfrepi_();
    static char relate[80];
    extern integer wncard_(doublereal *);
    static doublereal finish, refval;
    extern /* Subroutine */ int gfrepu_(), gfstep_();
    static char srcpre[80*2];
    static doublereal adjust;
    static char srcsuf[80*2];
    static doublereal critpt[100], result[106], xbegvl[50], xendvl[50];
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *), gfsstp_(doublereal *), 
	    wnfetd_(doublereal *, integer *, doublereal *, doublereal *), 
	    wnexpd_(doublereal *, doublereal *, doublereal *);
    static doublereal stepsz, tol;
    static logical rpt;

/* $ Abstract */

/*     Test the GF scalar root finder ZZGFRELX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF scalar root finder ZZGFRELX. Code */
/*     based on the original F_ZZGFREL routine. No changes to test */
/*     logic. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     Progress reporting and interrupt handling are not currently tested */
/*     in this version of this this test family, but test cases for these */
/*     features should be added. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */
/*     E.D. Wright    (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-APR-2012 (EDW)(NJB) */

/* -& */

/*     SPICELIB functions */


/*     External routines */


/*     Local parameters */


/*     Local Variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFRELX", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Too few workspace windows.", (ftnlen)26);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    tol = 1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 4;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(TOOFEWWINDOWS)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Workspace windows too short.", (ftnlen)28);
    ssized_(&c__1, short__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    short__[6] = pi_();
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    tol = 1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 1;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, short__, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Result window too short; found before search", (ftnlen)44);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1, short__);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);
    tol = 1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, short__, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDDIMENSION)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Result window too short; found during search", (ftnlen)44);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__12, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This case requires an actual search, so set the step size. */

    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);
    tol = 1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(OUTOFROOM)", ok, (ftnlen)16);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad adjustment value", (ftnlen)20);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = -1.;
    refval = .5;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);
    tol = 1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Non-positive tolerance value", (ftnlen)28);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);
    tol = -1e-6;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);
    tol = 0.;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Bad relational operator", (ftnlen)23);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "==", (ftnlen)80, (ftnlen)2);
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) == 0.5 on [-pi,pi]", (ftnlen)31);

/*     This case requires an actual search, so set the step size. */

    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two roots. */

    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xtime[0] = pi_() / 6;
    xtime[1] = pi_() * 5 / 6;
    xval[0] = .5;
    xval[1] = .5;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xtime[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xtime", i__2, "f_zzgfrelx__", (
		    ftnlen)661)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Make sure stop time exactly matches start time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xval[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xval", i__2, "f_zzgfrelx__", (
		    ftnlen)684)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) < 0.5 on [-pi,pi]", (ftnlen)30);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "<", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two solution intervals. */

    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xbeg[0] = -pi_();
    xend[0] = pi_() / 6;
    xbeg[1] = pi_() * 5 / 6;
    xend[1] = pi_();
    xbegvl[0] = 0.;
    xendvl[0] = .5;
    xbegvl[1] = .5;
    xendvl[1] = 0.;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)783)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)798)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time and value. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)806)], &tol, ok, (ftnlen)80, (ftnlen)1);
	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)814)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) > 0.5 on [-pi,pi]", (ftnlen)30);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_();
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, ">", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect one solution interval. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xbeg[0] = pi_() / 6;
    xend[0] = pi_() * 5 / 6;
    xbegvl[0] = .5;
    xendvl[0] = .5;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)909)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)924)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time and value. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)932)], &tol, ok, (ftnlen)80, (ftnlen)1);
	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)940)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local maxima of SIN(X) on [0,10*pi]", (ftnlen)40);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 10;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 5 solutions. */

    chcksi_("N", &n, "=", &c__5, &c__0, ok, (ftnlen)1, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {
	xbeg[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)1013)] = (i__ - 1 << 1) * pi_() + pi_(
		) / 2;
	xend[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xend", i__1, 
		"f_zzgfrelx__", (ftnlen)1014)] = xbeg[(i__2 = i__ - 1) < 50 &&
		 0 <= i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		ftnlen)1014)];
	xbegvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbegvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1016)] = 1.;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)1035)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1050)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check that stop time equals start time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local minima of SIN(X) on [0,10*pi]", (ftnlen)40);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 10;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 5 solutions. */

    chcksi_("N", &n, "=", &c__5, &c__0, ok, (ftnlen)1, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {
	xbeg[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)1131)] = (i__ - 1 << 1) * pi_() + pi_(
		) * 3 / 2;
	xend[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xend", i__1, 
		"f_zzgfrelx__", (ftnlen)1132)] = xbeg[(i__2 = i__ - 1) < 50 &&
		 0 <= i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		ftnlen)1132)];
	xbegvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbegvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1134)] = -1.;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)1153)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1168)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check that stop time equals start time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find adjusted absolute maxima of SIN(X) on [0,10*pi]", (ftnlen)52)
	    ;
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 10;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = .5;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 5 solutions. */

    chcksi_("N", &n, "=", &c__5, &c__0, ok, (ftnlen)1, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {
	xbeg[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)1249)] = (i__ - 1 << 1) * pi_() + pi_(
		) / 6;
	xend[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xend", i__1, 
		"f_zzgfrelx__", (ftnlen)1250)] = (i__ - 1 << 1) * pi_() + pi_(
		) * 5 / 6;
	xbegvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbegvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1252)] = .5;
	xendvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xendvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1253)] = .5;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)1272)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1287)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)1295)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at stop time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1310)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find adjusted absolute minima of SIN(X) on [0,10*pi]", (ftnlen)52)
	    ;
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 10;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = .5;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 5 solutions. */

    chcksi_("N", &n, "=", &c__5, &c__0, ok, (ftnlen)1, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {
	xbeg[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)1384)] = (i__ - 1 << 1) * pi_() + pi_(
		) * 7 / 6;
	xend[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xend", i__1, 
		"f_zzgfrelx__", (ftnlen)1385)] = (i__ - 1 << 1) * pi_() + pi_(
		) * 11 / 6;
	xbegvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbegvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1387)] = -.5;
	xendvl[(i__1 = i__ - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xendvl", 
		i__1, "f_zzgfrelx__", (ftnlen)1388)] = -.5;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)1407)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1422)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)1430)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at stop time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)1445)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute maximum of X on [-10*pi,10*pi]", (ftnlen)44);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("X", (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 1 solution. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xbeg[0] = pi_() * 10;
    xend[0] = xbeg[0];
    xbegvl[0] = xbeg[0];

/*     Check the times of and function values at the roots. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);

/*        Check function value at start time. The function */
/*        has derivative magnitude not exceeding 1, so the */
/*        function tolerance can be set equal to the time */
/*        tolerance. */

	s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_get__(&start, &value);
	ftol = tol;
	chcksd_(qname, &value, "~", xbegvl, &ftol, ok, (ftnlen)80, (ftnlen)1);

/*        Check stop time. */

	s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute minimum of X on [-10*pi,10*pi]", (ftnlen)44);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("X", (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 1 solution. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    xbeg[0] = pi_() * -10;
    xend[0] = xbeg[0];
    xbegvl[0] = xbeg[0];

/*     Check the times of and function values at the roots. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);

/*        Check function value at start time. The function */
/*        has derivative magnitude not exceeding 1, so the */
/*        function tolerance can be set equal to the time */
/*        tolerance. */

	s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	t_get__(&start, &value);
	ftol = tol;
	chcksd_(qname, &value, "~", xbegvl, &ftol, ok, (ftnlen)80, (ftnlen)1);

/*        Check stop time. */

	s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (ftnlen)
		1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find critical points of (X**2)SIN(X) on [-10*pi,10*pi]", (ftnlen)
	    54);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to the derivative of (X**2)*SIN(X). */

    t_set__("D((X**2)*SIN(X))", (ftnlen)16);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     If we had infinite precision, we would expect 21 solution */
/*     points: there is a critical point at 0, since the function */

/*         2 */
/*        X  * SIN(X) */

/*     is asymptotic to X**3 as X -> 0. However, the root at 0 */
/*     is a local minimum of the derivative: the derivative is */
/*     tangent to the X-axis at 0. So the GF system is unlikely */
/*     to be able to to detect this root. We require only that */
/*     at least 20 roots are found. */


    chcksi_("N", &n, ">", &c__19, &c__0, ok, (ftnlen)1, (ftnlen)1);
    ncrit = n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wnfetd_(result, &i__, &start, &finish);
	critpt[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge("critpt", 
		i__2, "f_zzgfrelx__", (ftnlen)1762)] = start;
    }

/*     If we did find a critical point at zero, delete it from */
/*     the set of critical points. */

    if (ncrit == 21) {
	for (i__ = 10; i__ <= 20; ++i__) {
	    critpt[(i__1 = i__ - 1) < 100 && 0 <= i__1 ? i__1 : s_rnge("crit"
		    "pt", i__1, "f_zzgfrelx__", (ftnlen)1773)] = critpt[(i__2 =
		     i__) < 100 && 0 <= i__2 ? i__2 : s_rnge("critpt", i__2, 
		    "f_zzgfrelx__", (ftnlen)1773)];
	}
	ncrit = 20;
    }

/*     Check function values, but not the times, at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative amplitude on the order of 100, so the */
/*           function tolerance must be at least 2 orders of magnitude */
/*           looser than the time tolerance. Use 3 orders of magnitude. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol * 1e3;
	    chcksd_(qname, &value, "~", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local maxima of (X**2)SIN(X) on [-10*pi,10*pi]", (ftnlen)51);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 10 local maxima. */

    chcksi_("N", &n, "=", &c__10, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start times. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. */

    j = 1;
    for (i__ = 1; i__ <= 20; i__ += 2) {
	xbeg[(i__1 = j - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)1887)] = critpt[(i__2 = i__ - 1) < 
		100 && 0 <= i__2 ? i__2 : s_rnge("critpt", i__2, "f_zzgfrelx"
		"__", (ftnlen)1887)];
	++j;
    }

/*     Check the start times for the local maxima. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)1908)], &tol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/*     Check the 1st and 2nd derivative at each solution epoch. */

    ftol = 1e-10;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the function at the start time; the function */
/*           should be positive. */

	    s_copy(qname, "Function at time #", (ftnlen)80, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("(X**2)*SIN(X)", (ftnlen)13);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, ">", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the derivative at the start time. */

	    s_copy(qname, "Derivative at time #", (ftnlen)80, (ftnlen)20);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D((X**2)*SIN(X))", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "~", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the 2nd derivative at the start time. */

	    s_copy(qname, "2nd derivative at time #", (ftnlen)80, (ftnlen)24);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D2((X**2)*SIN(X))", (ftnlen)17);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "<", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute maximum of (X**2)SIN(X) on [-10*pi,10*pi]", (ftnlen)
	    55);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute maximum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the first local maximum should be the absolute */
/*     maximum. */

    xbeg[0] = critpt[0];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute maximum of (X**2)SIN(X) on [-9*pi,10*pi]", (ftnlen)
	    54);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -9;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute maximum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the last local maximum should be the absolute */
/*     maximum. */

    xbeg[0] = critpt[18];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local minima of (X**2)SIN(X) on [-10*pi,10*pi]", (ftnlen)51);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 10 local minima. */

    chcksi_("N", &n, "=", &c__10, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start times. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. */

    j = 1;
    for (i__ = 2; i__ <= 20; i__ += 2) {
	xbeg[(i__1 = j - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)2243)] = critpt[(i__2 = i__ - 1) < 
		100 && 0 <= i__2 ? i__2 : s_rnge("critpt", i__2, "f_zzgfrelx"
		"__", (ftnlen)2243)];
	++j;
    }

/*     Check the start times for the local minima. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)2264)], &tol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/*     Check the 1st and 2nd derivative at each solution epoch. */

    ftol = 1e-10;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the function at the start time; the function */
/*           should be negative. */

	    s_copy(qname, "Function at time #", (ftnlen)80, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("(X**2)*SIN(X)", (ftnlen)13);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "<", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the derivative at the start time. */

	    s_copy(qname, "Derivative at time #", (ftnlen)80, (ftnlen)20);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D((X**2)*SIN(X))", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "~", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the 2nd derivative at the start time. */

	    s_copy(qname, "2nd derivative at time #", (ftnlen)80, (ftnlen)24);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D2((X**2)*SIN(X))", (ftnlen)17);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, ">", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute minimum of (X**2)SIN(X) on [-10*pi,10*pi]", (ftnlen)
	    55);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 10;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute minimum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the last local minimum should be the absolute */
/*     minimum. */

    xbeg[0] = critpt[19];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute minimum of (X**2)SIN(X) on [-10*pi,9*pi]", (ftnlen)
	    54);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * -10;
    d__2 = pi_() * 9;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute minimum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the first local minimum should be the absolute */
/*     minimum. */

    xbeg[0] = critpt[1];

/*     Check the start time for the absolute minimum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }
/* ********************************************************************* */
/* * */
/* *    Normal cases with multiple-interval confinement windows */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) == 0.5 on { [0,pi/6], [pi/3,pi/2], [2pi/3,5pi/6] }", 
	    (ftnlen)63);

/*     This case requires an actual search, so set the step size. */

    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 6;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 3;
    d__2 = pi_() / 2;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 2 / 3;
    d__2 = pi_() * 5 / 6;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Expand the confinement window slightly to compensate for */
/*     round-off errors. */

    e = 1e-12;
    wnexpd_(&e, &e, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "=", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two roots. */

    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xtime[0] = pi_() / 6;
    xtime[1] = pi_() * 5 / 6;
    xval[0] = .5;
    xval[1] = .5;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xtime[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xtime", i__2, "f_zzgfrelx__", (
		    ftnlen)2636)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Make sure stop time exactly matches start time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "=", &start, &c_b161, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xval[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xval", i__2, "f_zzgfrelx__", (
		    ftnlen)2659)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) < 0.5 on { [-pi/2,-pi/4], [0,pi/6], [pi/3,2*pi/3], "
	    "[5pi/6, pi] }", (ftnlen)77);

/*     This case requires an actual search, so set the step size. */

    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_() / 2;
    d__2 = -pi_() / 4;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 6;
    wninsd_(&c_b161, &d__1, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 3;
    d__2 = pi_() * 3 / 3;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 5 / 6;
    d__2 = pi_();
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, "<", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect three solution intervals. */

    chcksi_("N", &n, "=", &c__3, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xbeg[0] = -pi_() / 2;
    xend[0] = -pi_() / 4;
    xbeg[1] = 0.;
    xend[1] = pi_() / 6;
    xbeg[2] = pi_() * 5 / 6;
    xend[2] = pi_();
    xbegvl[0] = -1.;
    xendvl[0] = -sqrt(2.) / 2;
    xbegvl[1] = 0.;
    xendvl[1] = .5;
    xbegvl[2] = .5;
    xendvl[2] = 0.;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)2772)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)2787)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time and value. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)2795)], &tol, ok, (ftnlen)80, (ftnlen)1);
	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)2803)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Solve SIN(X) > 0.5 on { [-pi/2,0], [pi/6, pi/3], [2*pi/3, 5*pi/6"
	    "], [pi, 7*pi/6 }", (ftnlen)80);

/*     This case requires an actual search, so set the step size. */

    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = -pi_() / 2;
    wninsd_(&d__1, &c_b161, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() / 6;
    d__2 = pi_() / 3;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_() * 2 / 3;
    d__2 = pi_() * 5 / 6;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = pi_();
    d__2 = pi_() * 7 / 6;
    wninsd_(&d__1, &d__2, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = .5;
    s_copy(relate, ">", (ftnlen)80, (ftnlen)1);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect two solution intervals. */

    chcksi_("N", &n, "=", &c__2, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the times of and function values at the roots. */

    xbeg[0] = pi_() / 6;
    xend[0] = pi_() / 3;
    xbeg[1] = pi_() * 2 / 3;
    xend[1] = pi_() * 5 / 6;
    xbegvl[0] = .5;
    xendvl[0] = sqrt(3.) / 2;
    xbegvl[1] = sqrt(3.) / 2;
    xendvl[1] = .5;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)2912)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)2927)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time and value. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)2935)], &tol, ok, (ftnlen)80, (ftnlen)1);
	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)2943)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local maxima of (X**2)SIN(X) on set of intervals bracketing"
	    " critical points", (ftnlen)80);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delta = .25;

/*     This confinement window contains 20 intervals. Make a check */
/*     here to guard against "code upgrades." */

    chcksi_("NCRIT", &ncrit, "=", &c__20, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = ncrit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = critpt[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		"critpt", i__2, "f_zzgfrelx__", (ftnlen)2976)] - delta;
	d__2 = critpt[(i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		"critpt", i__3, "f_zzgfrelx__", (ftnlen)2976)] + delta;
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 10 local maxima. */

    chcksi_("N", &n, "=", &c__10, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start times. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. */

    j = 1;
    for (i__ = 1; i__ <= 20; i__ += 2) {
	xbeg[(i__1 = j - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)3041)] = critpt[(i__2 = i__ - 1) < 
		100 && 0 <= i__2 ? i__2 : s_rnge("critpt", i__2, "f_zzgfrelx"
		"__", (ftnlen)3041)];
	++j;
    }

/*     Check the start times for the local maxima. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)3062)], &tol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/*     Check the 1st and 2nd derivative at each solution epoch. */

    ftol = 1e-10;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the function at the start time; the function */
/*           should be positive. */

	    s_copy(qname, "Function at time #", (ftnlen)80, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("(X**2)*SIN(X)", (ftnlen)13);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, ">", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the derivative at the start time. */

	    s_copy(qname, "Derivative at time #", (ftnlen)80, (ftnlen)20);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D((X**2)*SIN(X))", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "~", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the 2nd derivative at the start time. */

	    s_copy(qname, "2nd derivative at time #", (ftnlen)80, (ftnlen)24);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D2((X**2)*SIN(X))", (ftnlen)17);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "<", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local minima of (X**2)SIN(X) on set of intervals bracketing"
	    " critical points", (ftnlen)80);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delta = .25;

/*     This confinement window contains 20 intervals. Make a check */
/*     here to guard against "code upgrades." */

    chcksi_("NCRIT", &ncrit, "=", &c__20, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = ncrit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = critpt[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		"critpt", i__2, "f_zzgfrelx__", (ftnlen)3149)] - delta;
	d__2 = critpt[(i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		"critpt", i__3, "f_zzgfrelx__", (ftnlen)3149)] + delta;
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 10 local minima. */

    chcksi_("N", &n, "=", &c__10, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start times. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. */

    j = 1;
    for (i__ = 2; i__ <= 20; i__ += 2) {
	xbeg[(i__1 = j - 1) < 50 && 0 <= i__1 ? i__1 : s_rnge("xbeg", i__1, 
		"f_zzgfrelx__", (ftnlen)3214)] = critpt[(i__2 = i__ - 1) < 
		100 && 0 <= i__2 ? i__2 : s_rnge("critpt", i__2, "f_zzgfrelx"
		"__", (ftnlen)3214)];
	++j;
    }

/*     Check the start times for the local minima. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)3235)], &tol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/*     Check the 1st and 2nd derivative at each solution epoch. */

    ftol = 1e-10;
    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the function at the start time; the function */
/*           should be negative. */

	    s_copy(qname, "Function at time #", (ftnlen)80, (ftnlen)18);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("(X**2)*SIN(X)", (ftnlen)13);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "<", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the derivative at the start time. */

	    s_copy(qname, "Derivative at time #", (ftnlen)80, (ftnlen)20);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D((X**2)*SIN(X))", (ftnlen)16);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, "~", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);

/*           Check the 2nd derivative at the start time. */

	    s_copy(qname, "2nd derivative at time #", (ftnlen)80, (ftnlen)24);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_set__("D2((X**2)*SIN(X))", (ftnlen)17);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    t_get__(&start, &value);
	    chcksd_(qname, &value, ">", &c_b161, &ftol, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local maxima of (X**2)SIN(X) on set of intervals excluding "
	    "critical points", (ftnlen)79);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delta = .25;

/*     This confinement window contains 20 intervals. Make a check */
/*     here to guard against "code upgrades." */

    chcksi_("NCRIT", &ncrit, "=", &c__20, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = ncrit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = critpt[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		"critpt", i__2, "f_zzgfrelx__", (ftnlen)3324)] + delta;
	d__2 = critpt[(i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		"critpt", i__3, "f_zzgfrelx__", (ftnlen)3324)] + delta * 2;
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find NO local maxima. */

    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find local minima of (X**2)SIN(X) on set of intervals excluding "
	    "critical points", (ftnlen)79);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delta = .25;

/*     This confinement window contains 20 intervals. Make a check */
/*     here to guard against "code upgrades." */

    chcksi_("NCRIT", &ncrit, "=", &c__20, &c__0, ok, (ftnlen)5, (ftnlen)1);
    i__1 = ncrit;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = critpt[(i__2 = i__ - 1) < 100 && 0 <= i__2 ? i__2 : s_rnge(
		"critpt", i__2, "f_zzgfrelx__", (ftnlen)3406)] + delta;
	d__2 = critpt[(i__3 = i__ - 1) < 100 && 0 <= i__3 ? i__3 : s_rnge(
		"critpt", i__3, "f_zzgfrelx__", (ftnlen)3406)] + delta * 2;
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "LOCMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find NO local minima. */

    chcksi_("N", &n, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute maximum of (X**2)SIN(X) on { [-10*pi,-9*pi], [-8pi"
	    ",-7pi],...,[8pi,9pi] }", (ftnlen)86);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -10; i__ <= 8; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute maximum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the first local maximum should be the absolute */
/*     maximum. */

    xbeg[0] = critpt[0];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute maximum of (X**2)SIN(X) on { [-8*pi,-7*pi], [-6pi,"
	    "-5pi],...,[8pi,9pi] }", (ftnlen)85);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -8; i__ <= 8; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute maximum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local maxima occur between */
/*     0 and pi and in intervals offset from [0,pi] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the last local maximum should be the absolute */
/*     maximum. */

    xbeg[0] = critpt[18];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute minimum of (X**2)SIN(X) on { [-9*pi,-8*pi], [-8pi,"
	    "-7pi],...,[9pi,10pi] }", (ftnlen)86);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -9; i__ <= 9; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute minimum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the last local minimum should be the absolute */
/*     minimum. */

    xbeg[0] = critpt[19];

/*     Check the start time for the absolute maximum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find absolute minimum of (X**2)SIN(X) on { [-9*pi,-8*pi], [-8pi,"
	    "-7pi],...,[7pi,8pi] }", (ftnlen)85);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -9; i__ <= 7; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = 0.;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);

/*     Set the function to (X**2)*SIN(X). */

    t_set__("(X**2)*SIN(X)", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should find 1 absolute minimum. */

    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set the expected start time. Local minima occur between */
/*     -pi and 0 and in intervals offset from [-pi,0] by multiples */
/*     of 2*pi. Given the confinement interval we've picked, */
/*     the first local minimum should be the absolute */
/*     minimum. */

    xbeg[0] = critpt[1];

/*     Check the start time for the absolute minimum. */

    if (*ok) {
	wnfetd_(result, &c__1, &start, &finish);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Check start time. */

	s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	repmi_(qname, "#", &c__1, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksd_(qname, &start, "~", xbeg, &tol, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find adjusted absolute maxima of SIN(X) on { [-9*pi,-8*pi], [-7p"
	    "i,-6pi],...,[7pi,8pi] }", (ftnlen)87);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -9; i__ <= 7; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = .5;
    refval = 0.;
    s_copy(relate, "ABSMAX", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 18 solutions. */

    chcksi_("N", &n, "=", &c__18, &c__0, ok, (ftnlen)1, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	xbeg[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbeg", i__2, 
		"f_zzgfrelx__", (ftnlen)3934)] = (i__ - 1) * pi_() - pi_() * 
		9;
	xend[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xend", i__2, 
		"f_zzgfrelx__", (ftnlen)3935)] = (i__ - 1) * pi_() - pi_() * 
		9 + pi_() / 6;
	xbegvl[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbegvl", 
		i__2, "f_zzgfrelx__", (ftnlen)3937)] = 0.;
	xendvl[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xendvl", 
		i__2, "f_zzgfrelx__", (ftnlen)3938)] = -.5;
	xbeg[(i__2 = i__) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbeg", i__2, 
		"f_zzgfrelx__", (ftnlen)3940)] = (i__ - 1) * pi_() - pi_() * 
		9 + pi_() * 5 / 6;
	xend[(i__2 = i__) < 50 && 0 <= i__2 ? i__2 : s_rnge("xend", i__2, 
		"f_zzgfrelx__", (ftnlen)3941)] = (i__ - 1) * pi_() - pi_() * 
		8;
	xbegvl[(i__2 = i__) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbegvl", i__2, 
		"f_zzgfrelx__", (ftnlen)3943)] = -.5;
	xendvl[(i__2 = i__) < 50 && 0 <= i__2 ? i__2 : s_rnge("xendvl", i__2, 
		"f_zzgfrelx__", (ftnlen)3944)] = 0.;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)3963)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)3978)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)3986)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at stop time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)4001)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Find adjusted absolute minima of SIN(X) on { [-9*pi,-8*pi], [-7p"
	    "i,-6pi],...,[7pi,8pi] }", (ftnlen)87);
    stepsz = .25;
    gfsstp_(&stepsz);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__100, cnfine);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = -9; i__ <= 7; i__ += 2) {
	d__1 = i__ * pi_();
	d__2 = (i__ + 1) * pi_();
	wninsd_(&d__1, &d__2, cnfine);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    ssized_(&c__100, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    adjust = .5;
    refval = 0.;
    s_copy(relate, "ABSMIN", (ftnlen)80, (ftnlen)6);

/*     We should be able to work with a very tight tolerance, since */
/*     our independent variable is close to zero. */

    tol = 1e-13;
    rpt = FALSE_;
    bail = FALSE_;
    s_copy(srcpre, "Search prefix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcpre + 80, "Search prefix 2", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf, "Search suffix 1", (ftnlen)80, (ftnlen)15);
    s_copy(srcsuf + 80, "Search suffix 2", (ftnlen)80, (ftnlen)15);
    t_set__("SIN(X)", (ftnlen)6);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mw = 100;
    nw = 15;
    zzgfrelx_((U_fp)gfstep_, (U_fp)gfrefn_, (U_fp)t_dec__, (U_fp)t_lt__, (
	    S_fp)t_get__, relate, &refval, &tol, &adjust, cnfine, &mw, &nw, 
	    work, &rpt, (U_fp)gfrepi_, (U_fp)gfrepu_, (U_fp)gfrepf_, srcpre, 
	    srcsuf, &bail, (L_fp)gfbail_, result, (ftnlen)80, (ftnlen)80, (
	    ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect 9 solutions. */

    chcksi_("N", &n, "=", &c__9, &c__0, ok, (ftnlen)1, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xbeg[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbeg", i__2, 
		"f_zzgfrelx__", (ftnlen)4078)] = (i__ - 1 << 1) * pi_() - pi_(
		) * 9 + pi_() / 6;
	xend[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xend", i__2, 
		"f_zzgfrelx__", (ftnlen)4079)] = (i__ - 1 << 1) * pi_() - pi_(
		) * 9 + pi_() * 5 / 6;
	xbegvl[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xbegvl", 
		i__2, "f_zzgfrelx__", (ftnlen)4081)] = -.5;
	xendvl[(i__2 = i__ - 1) < 50 && 0 <= i__2 ? i__2 : s_rnge("xendvl", 
		i__2, "f_zzgfrelx__", (ftnlen)4082)] = -.5;
    }

/*     Check the times of and function values at the roots. */

    if (*ok) {
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    wnfetd_(result, &i__, &start, &finish);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check start time. */

	    s_copy(qname, "Start time #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &start, "~", &xbeg[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbeg", i__2, "f_zzgfrelx__", (
		    ftnlen)4101)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at start time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Start value #", (ftnlen)80, (ftnlen)13);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&start, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xbegvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xbegvl", i__2, "f_zzgfrelx__", (
		    ftnlen)4116)], &ftol, ok, (ftnlen)80, (ftnlen)1);

/*           Check stop time. */

	    s_copy(qname, "Stop time #", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chcksd_(qname, &finish, "~", &xend[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xend", i__2, "f_zzgfrelx__", (
		    ftnlen)4124)], &tol, ok, (ftnlen)80, (ftnlen)1);

/*           Check function value at stop time. The function */
/*           has derivative magnitude not exceeding 1, so the */
/*           function tolerance can be set equal to the time */
/*           tolerance. */

	    s_copy(qname, "Stop value #", (ftnlen)80, (ftnlen)12);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    t_get__(&finish, &value);
	    ftol = tol;
	    chcksd_(qname, &value, "~", &xendvl[(i__2 = i__ - 1) < 50 && 0 <= 
		    i__2 ? i__2 : s_rnge("xendvl", i__2, "f_zzgfrelx__", (
		    ftnlen)4139)], &ftol, ok, (ftnlen)80, (ftnlen)1);
	}
    }
    t_success__(ok);
    return 0;
} /* f_zzgfrelx__ */

/* $Procedure  T_RELX (Test utilities) */
/* Subroutine */ int t_relx__0_(int n__, U_fp udfunc, char *f, doublereal *et,
	 doublereal *value, logical *decres, logical *lssthn, ftnlen f_len)
{
    /* Initialized data */

    static char svfnam[80] = "                                              "
	    "                                  ";

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    doublereal fval, dfval;
    extern /* Subroutine */ int chkin_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen), errch_(char *, char *, ftnlen, ftnlen);
    doublereal svref;
    extern /* Subroutine */ int ljust_(char *, char *, ftnlen, ftnlen);
    logical ok;
    extern /* Subroutine */ int sigerr_(char *, ftnlen), chkout_(char *, 
	    ftnlen), setmsg_(char *, ftnlen), zzholdd_(integer *, integer *, 
	    logical *, doublereal *);

/* $ Abstract */

/*     External routines defining UDFUNC, UDCOND, and UDQDEC. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None. */

/* $ Keywords */

/*   None. */

/* $ Declarations */
/* $ Abstract */

/*     SPICE private routine intended solely for the support of SPICE */
/*     routines. Users should not call this routine directly due to the */
/*     volatile nature of this routine. */

/*     This file contains parameter declarations for the ZZHOLDD */
/*     routine. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     None. */

/* $ Declarations */

/*     None. */

/* $ Brief_I/O */

/*     None. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     GEN       general value, primarily for testing. */

/*     GF_REF    user defined GF reference value. */

/*     GF_TOL    user defined GF convergence tolerance. */

/*     GF_DT     user defined GF step for numeric differentiation. */

/* $ Exceptions */

/*     None. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright    (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0  03-DEC-2013 (EDW) */

/* -& */

/*     OP codes. The values exist in the integer domain */
/*     [ -ZZNOP, -1], */


/*     Current number of OP codes. */


/*     ID codes. The values exist in the integer domain */
/*     [ 1, NID], */


/*     General use, primarily testing. */


/*     The user defined GF reference value. */


/*     The user defined GF convergence tolerance. */


/*     The user defined GF step for numeric differentiation. */


/*     Current number of ID codes, dimension of array */
/*     in ZZHOLDD. Bad things can happen if this parameter */
/*     does not have the proper value. */


/*     End of file zzholdd.inc. */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */

/* $ Detailed_Input */

/*   None. */

/* $ Detailed_Output */

/*   None. */

/* $ Parameters */

/*   None. */

/* $ Exceptions */

/*   None. */

/* $ Files */

/*   None. */

/* $ Particulars */

/*   None. */

/* $ Examples */

/*   None. */

/* $ Restrictions */

/*   None. */

/* $ Literature_References */

/*   None. */

/* $ Author_and_Institution */

/*   None. */

/* $ Version */

/* - */

/* -& */
/* $ Index_Entries */

/* ZZGFRELX test routines */

/* -& */

/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */

    switch(n__) {
	case 1: goto L_t_get;
	case 2: goto L_t_lt;
	case 3: goto L_t_dec;
	case 4: goto L_t_set;
	}

    chkin_("T_UTIL", (ftnlen)6);
    sigerr_("SPICE(BOGUSENTRY)", (ftnlen)17);
    chkout_("T_UTIL", (ftnlen)6);
    return 0;

/*     UDFUNC: Get quantity at ET. */


L_t_get:
    chkin_("T_GET", (ftnlen)5);
    if (s_cmp(svfnam, "X", (ftnlen)80, (ftnlen)1) == 0) {
	*value = *et;
    } else if (s_cmp(svfnam, "SIN(X)", (ftnlen)80, (ftnlen)6) == 0) {
	*value = sin(*et);
    } else if (s_cmp(svfnam, "(X**2)*SIN(X)", (ftnlen)80, (ftnlen)13) == 0) {
/* Computing 2nd power */
	d__1 = *et;
	*value = d__1 * d__1 * sin(*et);
    } else if (s_cmp(svfnam, "D((X**2)*SIN(X))", (ftnlen)80, (ftnlen)16) == 0)
	     {

/*        This is the derivative of (X**2)*SIN(X). */

/* Computing 2nd power */
	d__1 = *et;
	*value = *et * 2 * sin(*et) + d__1 * d__1 * cos(*et);
    } else if (s_cmp(svfnam, "D2((X**2)*SIN(X))", (ftnlen)80, (ftnlen)17) == 
	    0) {

/*        This is the 2nd derivative of (X**2)*SIN(X). */


/* Computing 2nd power */
	d__1 = *et;
	*value = sin(*et) * 2 + *et * 2 * cos(*et) + *et * 2 * cos(*et) + 
		d__1 * d__1 * (-sin(*et));
    } else {
	setmsg_("Bad function; function name is #.", (ftnlen)33);
	errch_("#", svfnam, (ftnlen)1, (ftnlen)80);
	sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
    }
    chkout_("T_GET", (ftnlen)5);
    return 0;

/*     UDCOND: Indicate whether function is less than reference value */
/*     at specified epoch. */


L_t_lt:
    *lssthn = FALSE_;
    if (s_cmp(svfnam, "X", (ftnlen)80, (ftnlen)1) == 0) {
	fval = *et;
    } else if (s_cmp(svfnam, "SIN(X)", (ftnlen)80, (ftnlen)6) == 0) {
	fval = sin(*et);
    } else if (s_cmp(svfnam, "(X**2)*SIN(X)", (ftnlen)80, (ftnlen)13) == 0) {
/* Computing 2nd power */
	d__1 = *et;
	fval = d__1 * d__1 * sin(*et);
    } else if (s_cmp(svfnam, "D((X**2)*SIN(X))", (ftnlen)80, (ftnlen)16) == 0)
	     {

/*        This is the derivative of (X**2)*SIN(X) */

/* Computing 2nd power */
	d__1 = *et;
	fval = *et * 2 * sin(*et) + d__1 * d__1 * cos(*et);
    } else if (s_cmp(svfnam, "D2((X**2)*SIN(X))", (ftnlen)80, (ftnlen)17) == 
	    0) {

/*        This is the 2nd derivative of (X**2)*SIN(X) */

/* Computing 2nd power */
	d__1 = *et;
	fval = sin(*et) * 2 + *et * 2 * cos(*et) + *et * 2 * cos(*et) + d__1 *
		 d__1 * (-sin(*et));
    } else {
	setmsg_("Bad function; function name is #.", (ftnlen)33);
	errch_("#", svfnam, (ftnlen)1, (ftnlen)80);
	sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
    }

/*     Retrieve the reference value. */

    zzholdd_(&c_n1, &c__2, &ok, &svref);
    if (! ok) {
	setmsg_("ZZHOLDD GET failed. This indicates a logic error in the GF "
		"code due either to a failure to store the GF reference value"
		" or a post store reset of ZZHOLDD.", (ftnlen)153);
	sigerr_("SPICE(ZZHOLDDGETFAILED)", (ftnlen)23);
	return 0;
    }
    *lssthn = fval < svref;
    return 0;

/*     UDQDEC: Indicate whether function is decreasing */
/*     at specified epoch. */


L_t_dec:
    *decres = FALSE_;
    if (s_cmp(svfnam, "X", (ftnlen)80, (ftnlen)1) == 0) {
	dfval = 1.;
    } else if (s_cmp(svfnam, "SIN(X)", (ftnlen)80, (ftnlen)6) == 0) {
	dfval = cos(*et);
    } else if (s_cmp(svfnam, "(X**2)*SIN(X)", (ftnlen)80, (ftnlen)13) == 0) {
/* Computing 2nd power */
	d__1 = *et;
	dfval = *et * 2 * sin(*et) + d__1 * d__1 * cos(*et);
    } else if (s_cmp(svfnam, "D((X**2)*SIN(X))", (ftnlen)80, (ftnlen)16) == 0)
	     {

/*        The function is the derivative of (X**2)*SIN(X). */
/*        DFVAL is the 2nd derivative of this function, */
/*        in other words the derivative of */

/*           2*X*SIN(X) +  (X**2)*COS(X) */

/* Computing 2nd power */
	d__1 = *et;
	dfval = sin(*et) * 2 + *et * 2 * cos(*et) + *et * 2 * cos(*et) + d__1 
		* d__1 * (-sin(*et));
    } else {
	setmsg_("Bad function; function name is #.", (ftnlen)33);
	errch_("#", svfnam, (ftnlen)1, (ftnlen)80);
	sigerr_("SPICE(TSPICEBUG)", (ftnlen)16);
    }
    *decres = dfval < 0.;
    return 0;

/*     Select function. */


L_t_set:
    ljust_(f, svfnam, f_len, (ftnlen)80);
    ucase_(svfnam, svfnam, (ftnlen)80, (ftnlen)80);
    return 0;
} /* t_relx__ */

/* Subroutine */ int t_relx__(U_fp udfunc, char *f, doublereal *et, 
	doublereal *value, logical *decres, logical *lssthn, ftnlen f_len)
{
    return t_relx__0_(0, udfunc, f, et, value, decres, lssthn, f_len);
    }

/* Subroutine */ int t_get__(doublereal *et, doublereal *value)
{
    return t_relx__0_(1, (U_fp)0, (char *)0, et, value, (logical *)0, (
	    logical *)0, (ftnint)0);
    }

/* Subroutine */ int t_lt__(U_fp udfunc, doublereal *et, logical *lssthn)
{
    return t_relx__0_(2, udfunc, (char *)0, et, (doublereal *)0, (logical *)0,
	     lssthn, (ftnint)0);
    }

/* Subroutine */ int t_dec__(U_fp udfunc, doublereal *et, logical *decres)
{
    return t_relx__0_(3, udfunc, (char *)0, et, (doublereal *)0, decres, (
	    logical *)0, (ftnint)0);
    }

/* Subroutine */ int t_set__(char *f, ftnlen f_len)
{
    return t_relx__0_(4, (U_fp)0, f, (doublereal *)0, (doublereal *)0, (
	    logical *)0, (logical *)0, f_len);
    }

