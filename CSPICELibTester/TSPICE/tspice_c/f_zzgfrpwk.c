/* f_zzgfrpwk.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b24 = 1e-12;
static doublereal c_b27 = 0.;
static integer c__1 = 1;
static doublereal c_b126 = 1.;
static doublereal c_b221 = 2.;
static doublereal c_b222 = 3.;
static integer c__7 = 7;
static doublereal c_b226 = .5;
static logical c_true = TRUE_;

/* $Procedure      F_ZZGFRPWK ( Test ZZGFRPWK entry points ) */
/* Subroutine */ int f_zzgfrpwk__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;
    cilist ci__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer f_clos(cllist *), f_rew(alist *), s_rsfe(cilist *), do_fio(
	    integer *, char *, ftnlen), e_rsfe(void), i_dnnt(doublereal *);

    /* Local variables */
    static doublereal frac;
    static char line[80];
    static doublereal incr, freq;
    static integer unit;
    extern /* Subroutine */ int zzgfwkad_(doublereal *, integer *, char *, 
	    char *, ftnlen, ftnlen), zzgfwkin_(doublereal *), zzgfwkmo_(
	    integer *, doublereal *, doublereal *, integer *, char *, char *, 
	    doublereal *, ftnlen, ftnlen), zzgfrpwk_(integer *, doublereal *, 
	    doublereal *, integer *, char *, char *, doublereal *, ftnlen, 
	    ftnlen), zzgfwkun_(integer *), zzgftswk_(doublereal *, doublereal 
	    *, integer *, char *, char *, ftnlen, ftnlen);
    static integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int dpfmt_(doublereal *, char *, char *, ftnlen, 
	    ftnlen), repmi_(char *, char *, integer *, char *, ftnlen, ftnlen,
	     ftnlen);
    static char xline[80];
    static doublereal total, xincr, xfreq;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer xunit;
    extern /* Subroutine */ int t_success__(logical *), chcksc_(char *, char *
	    , char *, char *, logical *, ftnlen, ftnlen, ftnlen, ftnlen), 
	    delfil_(char *, ftnlen);
    static integer tcheck;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char begmsg[55];
    static integer xcheck;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksd_(char *, doublereal 
	    *, char *, doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static char endmsg[13], xbegms[55], xendms[13];
    static doublereal xtotal;
    static integer iostat;
    extern /* Subroutine */ int suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen);
    extern logical exists_(char *, ftnlen);
    static char pctstr[80];
    static integer nrport;
    extern /* Subroutine */ int txtopn_(char *, integer *, ftnlen);
    static integer xnl;

/* $ Abstract */

/*     Test the GF progress reporting entry points contained in ZZGFRPWK. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF progress reporting routines */

/*        GFREPI */
/*        GFREPU */
/*        GFREPF */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.1.0, 30-MAR-2009 (BVS) */

/*        Fixed arguments in the ZZGFRPWK call. */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFRPWK", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test ZZGFTSWK parameter storage.", (ftnlen)32);

/*     Open a text file to capture the progress report. */

    if (exists_("zzgfrpwk.txt", (ftnlen)12)) {
	delfil_("zzgfrpwk.txt", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    txtopn_("zzgfrpwk.txt", &xunit, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwkun_(&xunit);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xtotal = 200.;
    s_copy(xbegms, "@-------------------Message prefix--------------------@", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "@***suffix**@", (ftnlen)13, (ftnlen)13);
    xfreq = 2.;
    xcheck = 10;

/*     Call the progress report display initializer. */

    zzgftswk_(&xtotal, &xfreq, &xcheck, xbegms, xendms, (ftnlen)55, (ftnlen)
	    13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the current report parameters from the ZZGFRPWK monitor. */

    zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (ftnlen)
	    55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the stored values from ZZGFWKMO against the expected */
/*     values. */
    chcksi_("UNIT", &unit, "=", &xunit, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("TOTAL", &total, "~", &xtotal, &c_b24, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FREQ", &freq, "=", &xfreq, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("TCHECK", &tcheck, "=", &xcheck, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("BEGMSG", begmsg, "=", xbegms, ok, (ftnlen)6, (ftnlen)55, (ftnlen)
	    1, (ftnlen)55);
    chcksc_("ENDMSG", endmsg, "=", xendms, ok, (ftnlen)6, (ftnlen)13, (ftnlen)
	    1, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test the report file created by a call to ZZGFTSWK", (ftnlen)50);

/*     Close and delete the existing log. */

    cl__1.cerr = 0;
    cl__1.cunit = xunit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    delfil_("zzgfrpwk.txt", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open a new log file. */

    txtopn_("zzgfrpwk.txt", &xunit, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwkun_(&xunit);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a search. */

    xtotal = 200.;
    s_copy(xbegms, "@-------------------Message prefix--------------------@", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "@***suffix**@", (ftnlen)13, (ftnlen)13);
    xfreq = 2.;
    xcheck = 10;

/*     Call the progress report display initializer. */

    zzgftswk_(&xtotal, &xfreq, &xcheck, xbegms, xendms, (ftnlen)55, (ftnlen)
	    13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Rewind the file to set the record pointer to the start */
/*     of the file. */

    al__1.aerr = 1;
    al__1.aunit = xunit;
    iostat = f_rew(&al__1);
    chcksi_("REWIND IOSTAT", &iostat, "=", &c__0, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    if (iostat == 0) {

/*        The first two lines of the file should be blank. */

	for (i__ = 1; i__ <= 2; ++i__) {
	    s_copy(qname, "Read IOSTAT no. *", (ftnlen)80, (ftnlen)17);
	    repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ci__1.cierr = 1;
	    ci__1.ciend = 1;
	    ci__1.ciunit = xunit;
	    ci__1.cifmt = "(A)";
	    iostat = s_rsfe(&ci__1);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_fio(&c__1, line, (ftnlen)80);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = e_rsfe();
L100001:
	    chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    if (iostat == 0) {
		s_copy(qname, "Log file line no. *", (ftnlen)80, (ftnlen)19);
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_(qname, line, "=", " ", ok, (ftnlen)80, (ftnlen)80, (
			ftnlen)1, (ftnlen)1);
	    }
	}
    }

/*     If we haven't hit an error, read the third line. */

    if (iostat == 0) {
	ci__1.cierr = 1;
	ci__1.ciend = 1;
	ci__1.ciunit = xunit;
	ci__1.cifmt = "(A)";
	iostat = s_rsfe(&ci__1);
	if (iostat != 0) {
	    goto L100002;
	}
	iostat = do_fio(&c__1, line, (ftnlen)80);
	if (iostat != 0) {
	    goto L100002;
	}
	iostat = e_rsfe();
L100002:
	chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (iostat == 0) {

/*           Check the report line. */

	    s_copy(xline, xbegms, (ftnlen)80, (ftnlen)55);
	    suffix_("  0.00%", &c__1, xline, (ftnlen)7, (ftnlen)80);
	    suffix_(xendms, &c__1, xline, (ftnlen)13, (ftnlen)80);
	    chcksc_("LINE", line, "=", xline, ok, (ftnlen)4, (ftnlen)80, (
		    ftnlen)1, (ftnlen)80);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test ZZGFKIN parameter storage.", (ftnlen)31);
    for (i__ = 1; i__ <= 5; ++i__) {
	xincr = (doublereal) i__;
	zzgfwkin_(&xincr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (
		ftnlen)55, (ftnlen)13);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect the last work increment to be I. */

	s_copy(qname, "INCR I=1", (ftnlen)80, (ftnlen)8);
	repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_(qname, &incr, "~", &xincr, &c_b24, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test ZZGFWKAD parameter storage.", (ftnlen)32);
    xfreq = 17.;
    xcheck = 32;
    s_copy(xbegms, "$-------Message prefix--------------------------------$", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "#***suffix**#", (ftnlen)13, (ftnlen)13);
    zzgfwkad_(&xfreq, &xcheck, xbegms, xendms, (ftnlen)55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*     Fetch the current report parameters from the ZZGFRPWK monitor. */

    zzgfwkmo_(&unit, &total, &freq, &tcheck, begmsg, endmsg, &incr, (ftnlen)
	    55, (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the stored values from ZZGFWKMO against the expected */
/*     values. */

/*     GFREPF is supposed to set the poll duration and call count to 0 */
/*     seconds and 1 call, respectively. The work increment should be */
/*     set to 0.D0. These values are hard-coded in GFREPF. */

    chcksi_("UNIT", &unit, "=", &xunit, &c__0, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("TOTAL", &total, "~", &xtotal, &c_b24, ok, (ftnlen)5, (ftnlen)1);
    chcksd_("FREQ", &freq, "=", &xfreq, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksi_("TCHECK", &tcheck, "=", &xcheck, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("BEGMSG", begmsg, "=", xbegms, ok, (ftnlen)6, (ftnlen)55, (ftnlen)
	    1, (ftnlen)55);
    chcksc_("ENDMSG", endmsg, "=", xendms, ok, (ftnlen)6, (ftnlen)13, (ftnlen)
	    1, (ftnlen)13);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test a report with multiple status lines.", (ftnlen)41);

/*     Close and delete the existing log. */

    cl__1.cerr = 0;
    cl__1.cunit = xunit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    delfil_("zzgfrpwk.txt", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open a new log file. */

    txtopn_("zzgfrpwk.txt", &xunit, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwkun_(&xunit);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a search. */

    xtotal = 100.;
    s_copy(xbegms, "@-----------------------------------Message prefix----@", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "@*****suffix@", (ftnlen)13, (ftnlen)13);

/*     Check the system time every 10 increment calls. */

    xcheck = 10;

/*     Set the wait time for display to zero. */

    xfreq = 0.;

/*     Call the progress report display initializer. */

    zzgftswk_(&xtotal, &xfreq, &xcheck, xbegms, xendms, (ftnlen)55, (ftnlen)
	    13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Report status 100 times. Each time, the increment is "1". */

    for (i__ = 1; i__ <= 100; ++i__) {
	zzgfwkin_(&c_b126);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Rewind the file to set the record pointer to the start */
/*     of the file. */

    al__1.aerr = 1;
    al__1.aunit = xunit;
    iostat = f_rew(&al__1);
    chcksi_("REWIND IOSTAT", &iostat, "=", &c__0, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    if (iostat == 0) {

/*        The first two lines of the file should be blank. */

	for (i__ = 1; i__ <= 2; ++i__) {
	    s_copy(qname, "Read IOSTAT no. *", (ftnlen)80, (ftnlen)17);
	    repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ci__1.cierr = 1;
	    ci__1.ciend = 1;
	    ci__1.ciunit = xunit;
	    ci__1.cifmt = "(A)";
	    iostat = s_rsfe(&ci__1);
	    if (iostat != 0) {
		goto L100003;
	    }
	    iostat = do_fio(&c__1, line, (ftnlen)80);
	    if (iostat != 0) {
		goto L100003;
	    }
	    iostat = e_rsfe();
L100003:
	    chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    if (iostat == 0) {
		s_copy(qname, "Log file line no. *", (ftnlen)80, (ftnlen)19);
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_(qname, line, "=", " ", ok, (ftnlen)80, (ftnlen)80, (
			ftnlen)1, (ftnlen)1);
	    }
	}
    }

/*     If we haven't hit an error, read the rest of the file. */

    if (iostat == 0) {
	d__1 = xtotal / xcheck;
	i__1 = i_dnnt(&d__1) + 1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s_copy(qname, "Log file line no. * IOSTAT", (ftnlen)80, (ftnlen)
		    26);
	    i__2 = i__ + 2;
	    repmi_(qname, "*", &i__2, qname, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    ci__1.cierr = 1;
	    ci__1.ciend = 1;
	    ci__1.ciunit = xunit;
	    ci__1.cifmt = "(A)";
	    iostat = s_rsfe(&ci__1);
	    if (iostat != 0) {
		goto L100004;
	    }
	    iostat = do_fio(&c__1, line, (ftnlen)80);
	    if (iostat != 0) {
		goto L100004;
	    }
	    iostat = e_rsfe();
L100004:
	    chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    if (iostat == 0) {

/*              Check the report line. */

		d__1 = (doublereal) ((i__ - 1) * 10);
		dpfmt_(&d__1, "XXX.XX", pctstr, (ftnlen)6, (ftnlen)80);
		s_copy(xline, xbegms, (ftnlen)80, (ftnlen)55);
		suffix_(pctstr, &c__1, xline, (ftnlen)80, (ftnlen)80);
		suffix_("%", &c__0, xline, (ftnlen)1, (ftnlen)80);
		suffix_(xendms, &c__1, xline, (ftnlen)13, (ftnlen)80);
		chcksc_("LINE", line, "=", xline, ok, (ftnlen)4, (ftnlen)80, (
			ftnlen)1, (ftnlen)80);
	    }
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Test a report with multiple status lines, this time using a posi"
	    "tive time delay.", (ftnlen)80);

/*     Close and delete the existing log. */

    cl__1.cerr = 0;
    cl__1.cunit = xunit;
    cl__1.csta = 0;
    f_clos(&cl__1);
    delfil_("zzgfrpwk.txt", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Open a new log file. */

    txtopn_("zzgfrpwk.txt", &xunit, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwkun_(&xunit);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a search. The search must take long enough */
/*     so that at least one second passes per 20% of the */
/*     work. */

    xtotal = 1e8;
    s_copy(xbegms, "@-Message prefix--------------------------------------@", 
	    (ftnlen)55, (ftnlen)55);
    s_copy(xendms, "@****Suffix*@", (ftnlen)13, (ftnlen)13);

/*     Check the system time once every XTOTAL/20 increment calls. */

    d__1 = xtotal / 20;
    xcheck = i_dnnt(&d__1);

/*     Set the wait time for display in seconds. */

    xfreq = .1;

/*     Call the progress report display initializer. */

    zzgftswk_(&xtotal, &xfreq, &xcheck, xbegms, xendms, (ftnlen)55, (ftnlen)
	    13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the progress increment to 5. */

    incr = 5.;

/*     Report status XTOTAL/INCR times. So every time the */
/*     system clock is checked, we expect that */

/*          FRAC = XCHECK * INCR / XTOTAL */


/*               = 25% */

/*     progress should have been made since the last check. We expect */
/*     25% of the computation to take longer than 1 second, so we should */
/*     see a report line for every 25% progress. */

    frac = xcheck * incr / xtotal;
    d__1 = xtotal / incr;
    nrport = i_dnnt(&d__1);
    i__1 = nrport;
    for (i__ = 1; i__ <= i__1; ++i__) {
	zzgfwkin_(&incr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Rewind the file to set the record pointer to the start */
/*     of the file. */

    al__1.aerr = 1;
    al__1.aunit = xunit;
    iostat = f_rew(&al__1);
    chcksi_("REWIND IOSTAT", &iostat, "=", &c__0, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    if (iostat == 0) {

/*        The first two lines of the file should be blank. */

	for (i__ = 1; i__ <= 2; ++i__) {
	    s_copy(qname, "Read IOSTAT no. *", (ftnlen)80, (ftnlen)17);
	    repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    ci__1.cierr = 1;
	    ci__1.ciend = 1;
	    ci__1.ciunit = xunit;
	    ci__1.cifmt = "(A)";
	    iostat = s_rsfe(&ci__1);
	    if (iostat != 0) {
		goto L100005;
	    }
	    iostat = do_fio(&c__1, line, (ftnlen)80);
	    if (iostat != 0) {
		goto L100005;
	    }
	    iostat = e_rsfe();
L100005:
	    chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    if (iostat == 0) {
		s_copy(qname, "Log file line no. *", (ftnlen)80, (ftnlen)19);
		repmi_(qname, "*", &i__, qname, (ftnlen)80, (ftnlen)1, (
			ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksc_(qname, line, "=", " ", ok, (ftnlen)80, (ftnlen)80, (
			ftnlen)1, (ftnlen)1);
	    }
	}
    }

/*     If we haven't hit an error, read the rest of the file. */

/*     The expected number of status lines in the report is the */
/*     number of update calls divided by the call check ratio, */
/*     plus one, since the first line shows 0% progress. */

    xnl = nrport / xcheck + 1;
    if (iostat == 0) {
	i__1 = xnl;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    s_copy(qname, "Log file line no. * IOSTAT", (ftnlen)80, (ftnlen)
		    26);
	    i__2 = i__ + 2;
	    repmi_(qname, "*", &i__2, qname, (ftnlen)80, (ftnlen)1, (ftnlen)
		    80);
	    ci__1.cierr = 1;
	    ci__1.ciend = 1;
	    ci__1.ciunit = xunit;
	    ci__1.cifmt = "(A)";
	    iostat = s_rsfe(&ci__1);
	    if (iostat != 0) {
		goto L100006;
	    }
	    iostat = do_fio(&c__1, line, (ftnlen)80);
	    if (iostat != 0) {
		goto L100006;
	    }
	    iostat = e_rsfe();
L100006:
	    chcksi_(qname, &iostat, "=", &c__0, &c__0, ok, (ftnlen)80, (
		    ftnlen)1);
	    if (iostat == 0) {

/*              Check the report line. */

		d__1 = (i__ - 1) * 100 * frac;
		dpfmt_(&d__1, "XXX.XX", pctstr, (ftnlen)6, (ftnlen)80);
		s_copy(xline, xbegms, (ftnlen)80, (ftnlen)55);
		suffix_(pctstr, &c__1, xline, (ftnlen)80, (ftnlen)80);
		suffix_("%", &c__0, xline, (ftnlen)1, (ftnlen)80);
		suffix_(xendms, &c__1, xline, (ftnlen)13, (ftnlen)80);
		chcksc_("LINE", line, "=", xline, ok, (ftnlen)4, (ftnlen)80, (
			ftnlen)1, (ftnlen)80);
	    }
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up: delete log file.", (ftnlen)26);
    if (exists_("zzgfrpwk.txt", (ftnlen)12)) {
	cl__1.cerr = 0;
	cl__1.cunit = xunit;
	cl__1.csta = 0;
	f_clos(&cl__1);
	delfil_("zzgfrpwk.txt", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Call to umbrella routine", (ftnlen)24);
    zzgfrpwk_(&c__1, &c_b221, &c_b222, &c__7, " ", " ", &c_b226, (ftnlen)1, (
	    ftnlen)1);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
    t_success__(ok);
    return 0;
} /* f_zzgfrpwk__ */

