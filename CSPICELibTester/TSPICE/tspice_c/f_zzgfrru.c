/* f_zzgfrru.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__2 = 2;
static integer c__0 = 0;
static doublereal c_b54 = 0.;

/* $Procedure F_ZZGFRRU ( ZZGFRRU family tests ) */
/* Subroutine */ int f_zzgfrru__(logical *ok)
{
    /* Initialized data */

    static char target[36*3] = "EARTH                               " "X    "
	    "                               " "EARTH                         "
	    "      ";
    static char obsrvr[36*3] = "X                                   " "MARS "
	    "                               " "EARTH                         "
	    "      ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    char targ[36];
    integer xobs, yobs;
    extern /* Subroutine */ int zzgfrrin_(char *, char *, char *, doublereal *
	    , ftnlen, ftnlen, ftnlen);
    integer i__;
    extern /* Subroutine */ int tcase_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    logical found;
    integer xtarg, ytarg;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , bods2c_(char *, integer *, logical *, ftnlen);
    doublereal dt;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chckxc_(logical *, char *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    char abcorr[5], xabcor[5], obs[36];
    doublereal xdt;
    char txt[160];
    extern /* Subroutine */ int zzgfrrx_(integer *, char *, integer *, 
	    doublereal *, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB utility package */

/*        ZZGFRRU */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB geometry utility package ZZGFRRU. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 19-JUN-2013 (EDW)(BVS) */

/* -& */

/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Indices 1:2 for Invalid body name test, 3 for not distinct */
/*     body names test. */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFRRU", (ftnlen)9);

/*     Case 1: Invalid body names. */

    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    dt = 1.;
    for (i__ = 1; i__ <= 2; ++i__) {
	s_copy(targ, target + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("target", i__1, "f_zzgfrru__", (ftnlen)171)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_zzgfrru__", (ftnlen)172)) * 36, (
		ftnlen)36, (ftnlen)36);
	s_copy(txt, "Invalid body name test. TARG = #, OBS = #", (ftnlen)160, 
		(ftnlen)41);
	repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (
		ftnlen)160);
	tcase_(txt, (ftnlen)160);
	zzgfrrin_(targ, abcorr, obs, &dt, (ftnlen)36, (ftnlen)5, (ftnlen)36);
	chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    }

/*     Case 2: Not distinct body names. */

    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    i__ = 3;
    dt = 1.;
    s_copy(targ, target + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
	    "target", i__1, "f_zzgfrru__", (ftnlen)194)) * 36, (ftnlen)36, (
	    ftnlen)36);
    s_copy(obs, obsrvr + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
	    "obsrvr", i__1, "f_zzgfrru__", (ftnlen)195)) * 36, (ftnlen)36, (
	    ftnlen)36);
    s_copy(txt, "Not distinct body name test. TARG = #, OBS = #", (ftnlen)160,
	     (ftnlen)46);
    repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    tcase_(txt, (ftnlen)160);
    zzgfrrin_(targ, abcorr, obs, &dt, (ftnlen)36, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);

/*     Case 3: DT = 0. */

    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    dt = 0.;
    s_copy(targ, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obs, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(txt, "Delta value zero", (ftnlen)160, (ftnlen)16);
    tcase_(txt, (ftnlen)160);
    zzgfrrin_(targ, abcorr, obs, &dt, (ftnlen)36, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDVALUE)", ok, (ftnlen)19);

/*     Case 4: Confirm initialized values are correctly saved. */

    s_copy(abcorr, "NONE", (ftnlen)5, (ftnlen)4);
    dt = 1.;
    s_copy(targ, "MERCURY", (ftnlen)36, (ftnlen)7);
    s_copy(obs, "MARS", (ftnlen)36, (ftnlen)4);
    bods2c_(targ, &ytarg, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bods2c_(obs, &yobs, &found, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(txt, "Initialize then check saved values in ZZGFRRIN. TARG = #, O"
	    "BS = #, ABCORR = #, DT = #", (ftnlen)160, (ftnlen)85);
    repmc_(txt, "#", targ, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", obs, txt, (ftnlen)160, (ftnlen)1, (ftnlen)36, (ftnlen)
	    160);
    repmc_(txt, "#", abcorr, txt, (ftnlen)160, (ftnlen)1, (ftnlen)5, (ftnlen)
	    160);
    repmd_(txt, "#", &dt, &c__2, txt, (ftnlen)160, (ftnlen)1, (ftnlen)160);
    tcase_(txt, (ftnlen)160);
    zzgfrrin_(targ, abcorr, obs, &dt, (ftnlen)36, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfrrx_(&xtarg, xabcor, &xobs, &xdt, (ftnlen)5);
    chcksi_("TARG X vs Y", &xtarg, "=", &ytarg, &c__0, ok, (ftnlen)11, (
	    ftnlen)1);
    chcksi_("OBS X vs Y", &xobs, "=", &yobs, &c__0, ok, (ftnlen)10, (ftnlen)1)
	    ;
    ucase_(abcorr, abcorr, (ftnlen)5, (ftnlen)5);
    chcksc_("ABCORR vs XABCORR", xabcor, "=", abcorr, ok, (ftnlen)17, (ftnlen)
	    5, (ftnlen)1, (ftnlen)5);
    chcksd_("DT vs XDT", &dt, "=", &xdt, &c_b54, ok, (ftnlen)9, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfrru__ */

