/* f_zzgfsolv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__100 = 100;
static integer c__0 = 0;
static logical c_false = FALSE_;
static doublereal c_b14 = 93.;
static integer c__1 = 1;
static doublereal c_b27 = 0.;
static integer c__101 = 101;
static integer c__200 = 200;

/* $Procedure F_ZZGFSOLV ( ZZGFSOLV family tests ) */
/* Subroutine */ int f_zzgfsolv__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    logical bail;
    doublereal tbeg, tend, crit, step;
    integer nrpt;
    extern /* Subroutine */ int zzgfsolv_(U_fp, U_fp, U_fp, logical *, L_fp, 
	    logical *, doublereal *, doublereal *, doublereal *, doublereal *,
	     logical *, U_fp, doublereal *);
    integer n;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    logical cstep;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal start;
    extern /* Subroutine */ int t_success__(logical *);
    extern logical gfbail_();
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), scardd_(
	    integer *, doublereal *), chckxc_(logical *, char *, logical *, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen);
    extern /* Subroutine */ int gfrefn_(), t_refn__();
    extern integer wncard_(doublereal *);
    doublereal finish;
    extern /* Subroutine */ int wnfetd_(doublereal *, integer *, doublereal *,
	     doublereal *);
    extern /* Subroutine */ int gfrepu_(), gfstep_(), t_repu__();
    doublereal tolrnc;
    extern /* Subroutine */ int ssized_(integer *, doublereal *);
    doublereal result[106];
    extern /* Subroutine */ int zzrpti_(void);
    logical rpt;
    extern /* Subroutine */ int t_udqlt__();
    extern /* Subroutine */ int zzcritg_(doublereal *), zzcrits_(doublereal *)
	    , zznrptg_(integer *);

/* $ Abstract */

/*     This routine tests the SPICELIB routines */

/*        ZZGFSOLV */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 11-FEB-2017 (NJB)(EDW) */

/*        Eliminated small TOLRNC test cases to correspond to change */
/*        in ZZGFSOLVX. */

/* -    TSPICE Version 1.1.0, 26-APR-2012 (EDW) */

/*        Edited the invalid tolerance error test return */
/*        short message from SPICE(VALUEOUTOFRANGE) to */
/*        SPICE(INVALIDTOLERANCE). */

/* -    TSPICE Version 1.0.0, 11-MAR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     External routines */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFSOLV", (ftnlen)10);

/*     Set the progress report test hit counter to zero. */

    zzrpti_();

/*     Case 1 */

    tcase_("TOLRNC = 0", (ftnlen)10);
    bail = FALSE_;
    cstep = TRUE_;
    rpt = FALSE_;
    step = 1.;
    start = 0.;
    finish = 10.;
    tolrnc = 0.;
    zzgfsolv_((U_fp)t_udqlt__, (U_fp)gfstep_, (U_fp)gfrefn_, &bail, (L_fp)
	    gfbail_, &cstep, &step, &start, &finish, &tolrnc, &rpt, (U_fp)
	    gfrepu_, result);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);

/*     Case 2 */

    tcase_("START > FINISH", (ftnlen)14);
    bail = FALSE_;
    cstep = TRUE_;
    rpt = FALSE_;
    step = 1.;
    start = 10.;
    finish = 0.;
    tolrnc = 1.;
    zzgfsolv_((U_fp)t_udqlt__, (U_fp)gfstep_, (U_fp)gfrefn_, &bail, (L_fp)
	    gfbail_, &cstep, &step, &start, &finish, &tolrnc, &rpt, (U_fp)
	    gfrepu_, result);
    chckxc_(&c_true, "SPICE(BADTIMECASE)", ok, (ftnlen)18);

/*     Case 3 */

    tcase_("Step function, one interval, one critical value.", (ftnlen)48);

/*     Set a search to key on the critical value defined in */
/*     T_ZZGFSOLV. With a STEP size of one, and a critical value */
/*     a double precisiion representation of an integer, the */
/*     RESULT window returns [CRIT + TOLRNC/2, FINISH]. */

/*                            ------------ */
/*                            | */
/*           -----------------| */

    ssized_(&c__100, result);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    cstep = TRUE_;
    rpt = TRUE_;
    step = 1.;
    start = 0.;
    finish = 100.;

/*     Set a value strictly between START and FINISH. */

    zzcrits_(&c_b14);

/*     Using bisection for refinement; set TOLRNC as a power of 2. */

/*     TOLRNC = 2**-N (N=3) */

    tolrnc = .125;
    zzgfsolv_((U_fp)t_udqlt__, (U_fp)gfstep_, (U_fp)gfrefn_, &bail, (L_fp)
	    gfbail_, &cstep, &step, &start, &finish, &tolrnc, &rpt, (U_fp)
	    t_repu__, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect one root. */

    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    wnfetd_(result, &n, &tbeg, &tend);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We know the exact values for the expected interval endpoints. */

    zzcritg_(&crit);
    d__1 = crit + tolrnc / 2.;
    chcksd_("TBEG", &tbeg, "=", &d__1, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    chcksd_("TBEG", &tend, "=", &finish, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     We know the exact value for the number of report hits. */

    zznrptg_(&nrpt);
    chcksi_("NRPT", &nrpt, "=", &c__101, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Case 4 */

    tcase_("Step function, one interval, two critical values.", (ftnlen)49);

/*                            ----------------- */
/*                            |               | */
/*           -----------------|               |-- */
/*                           CRIT           2*CRIT */

    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    cstep = TRUE_;
    rpt = TRUE_;
    step = 1.;
    start = 0.;
    finish = 200.;

/*     Set a value strictly between START and FINISH with */
/*     2*CRIT + TOLRNC/2.D0 < FINISH. */

    zzcrits_(&c_b14);

/*     Using bisection for refinement; set TOLRNC as a power of 2. */

/*     TOLRNC = 2**-N (N=3) */

    tolrnc = .125;
    zzgfsolv_((U_fp)t_udqlt__, (U_fp)gfstep_, (U_fp)gfrefn_, &bail, (L_fp)
	    gfbail_, &cstep, &step, &start, &finish, &tolrnc, &rpt, (U_fp)
	    t_repu__, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect one root. */

    n = wncard_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    wnfetd_(result, &n, &tbeg, &tend);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We know the exact values for the expected interval endpoints. */

    zzcritg_(&crit);
    d__1 = crit + tolrnc / 2.;
    chcksd_("TBEG", &tbeg, "=", &d__1, &c_b27, ok, (ftnlen)4, (ftnlen)1);
    d__1 = crit * 2. + tolrnc / 2.;
    chcksd_("TBEG", &tend, "=", &d__1, &c_b27, ok, (ftnlen)4, (ftnlen)1);

/*     We know the exact value for the number of report hits. */

    zznrptg_(&nrpt);
    chcksi_("NRPT", &nrpt, "=", &c__200, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*     Case 5 */

    tcase_("Check for loop convergence error signal.", (ftnlen)40);
    ssized_(&c__100, result);
    scardd_(&c__0, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bail = FALSE_;
    cstep = TRUE_;
    rpt = TRUE_;
    step = 1.;
    start = 0.;
    finish = 100.;

/*     Set a value strictly between START and FINISH. */

    zzcrits_(&c_b14);

/*     Set TOLRNC to something we know will work for bisection. */

    tolrnc = .125;

/*     Use a test version of GFREFN to ensure a slower than */
/*     bisection convergence rate. */

    zzgfsolv_((U_fp)t_udqlt__, (U_fp)gfstep_, (U_fp)t_refn__, &bail, (L_fp)
	    gfbail_, &cstep, &step, &start, &finish, &tolrnc, &rpt, (U_fp)
	    t_repu__, result);
    chckxc_(&c_true, "SPICE(NOCONVERG)", ok, (ftnlen)16);
    t_success__(ok);
    return 0;
} /* f_zzgfsolv__ */


/*     ZZGFSOLV test family specific utility code. */

/* Subroutine */ int t_zzgfsolv__0_(int n__, doublereal *et, logical *
	in_con__, doublereal *val, doublereal *ivbeg, doublereal *ivend, 
	integer *nval)
{
    /* Initialized data */

    static integer nrpt = 0;

    static doublereal crit;
    extern /* Subroutine */ int chkin_(char *, ftnlen), sigerr_(char *, 
	    ftnlen), chkout_(char *, ftnlen);

    switch(n__) {
	case 1: goto L_t_udqlt;
	case 2: goto L_zzcrits;
	case 3: goto L_zzcritg;
	case 4: goto L_t_repu;
	case 5: goto L_zznrptg;
	case 6: goto L_zzrpti;
	}

    chkin_("T_ZZGFSOLV", (ftnlen)10);
    sigerr_("SPICE(BOGUSENTRY)", (ftnlen)17);
    chkout_("T_ZZGFSOLV", (ftnlen)10);
    return 0;

/*     A UDCOND test function. The function returns FALSE */
/*     for ET <= CRIT and ET > 2*CRIT. */



L_t_udqlt:
    *in_con__ = FALSE_;

/*        The function returns true after the CRIT value. */

    if (*et > crit) {
	*in_con__ = TRUE_;
    }

/*        The function returns false after the 2*CRIT value. */

    if (*et > crit * 2.) {
	*in_con__ = FALSE_;
    }
    return 0;

/*     Set the critical value used in the tests. */


L_zzcrits:
    crit = *val;
    return 0;

/*     Return the critical value used in the tests.TOL */


L_zzcritg:
    *val = crit;
    return 0;

/*     A UDREPU test function. */


L_t_repu:

/*        Count the number of progress report calls. */

    ++nrpt;
    return 0;

/*     Return the number of progress report calls then reset the */
/*     count variable to zero. */


L_zznrptg:
    *nval = nrpt;
    nrpt = 0;
    return 0;

/*     Initialize the progress reporter count to zero. */


L_zzrpti:
    nrpt = 0;
    return 0;
} /* t_zzgfsolv__ */

/* Subroutine */ int t_zzgfsolv__(doublereal *et, logical *in_con__, 
	doublereal *val, doublereal *ivbeg, doublereal *ivend, integer *nval)
{
    return t_zzgfsolv__0_(0, et, in_con__, val, ivbeg, ivend, nval);
    }

/* Subroutine */ int t_udqlt__(doublereal *et, logical *in_con__)
{
    return t_zzgfsolv__0_(1, et, in_con__, (doublereal *)0, (doublereal *)0, (
	    doublereal *)0, (integer *)0);
    }

/* Subroutine */ int zzcrits_(doublereal *val)
{
    return t_zzgfsolv__0_(2, (doublereal *)0, (logical *)0, val, (doublereal *
	    )0, (doublereal *)0, (integer *)0);
    }

/* Subroutine */ int zzcritg_(doublereal *val)
{
    return t_zzgfsolv__0_(3, (doublereal *)0, (logical *)0, val, (doublereal *
	    )0, (doublereal *)0, (integer *)0);
    }

/* Subroutine */ int t_repu__(doublereal *ivbeg, doublereal *ivend, 
	doublereal *et)
{
    return t_zzgfsolv__0_(4, et, (logical *)0, (doublereal *)0, ivbeg, ivend, 
	    (integer *)0);
    }

/* Subroutine */ int zznrptg_(integer *nval)
{
    return t_zzgfsolv__0_(5, (doublereal *)0, (logical *)0, (doublereal *)0, (
	    doublereal *)0, (doublereal *)0, nval);
    }

/* Subroutine */ int zzrpti_(void)
{
    return t_zzgfsolv__0_(6, (doublereal *)0, (logical *)0, (doublereal *)0, (
	    doublereal *)0, (doublereal *)0, (integer *)0);
    }


/*     Test version of GFREFN. The EPSILON value causes */
/*     an increase in the number of refinement steps */
/*     required to converge as compared to bisection. */

/*     The default GFREFN uses an EPSILON value 0.5. */

/* Subroutine */ int t_refn__(doublereal *t1, doublereal *t2, logical *s1, 
	logical *s2, doublereal *t)
{
    doublereal x;
    extern doublereal brcktd_(doublereal *, doublereal *, doublereal *);
    doublereal epsilon;

    epsilon = .999;
    x = *t1 * (1. - epsilon) + *t2 * epsilon;
    *t = brcktd_(&x, t1, t2);
    return 0;
} /* t_refn__ */

