/* f_zzgfspq.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__0 = 0;
static integer c__2 = 2;
static integer c__6 = 6;
static doublereal c_b101 = 1e-12;

/* $Procedure F_ZZGFSPQ ( ZZGFSPQ family tests ) */
/* Subroutine */ int f_zzgfspq__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static doublereal rada[3], radb[3];
    extern doublereal vsep_(doublereal *, doublereal *);
    static char time0[50];
    static integer targ1, targ2, i__, j, k;
    extern /* Subroutine */ int zztstlsk_(char *, ftnlen), tcase_(char *, 
	    ftnlen);
    static doublereal theta;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static doublereal value;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    extern doublereal vnorm_(doublereal *);
    static doublereal r1, r2;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal range1, range2;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static doublereal et;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int kclear_(void), delfil_(char *, ftnlen);
    extern doublereal dasine_(doublereal *, doublereal *);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char abcorr[80];
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    static doublereal xr[8]	/* was [2][4] */;
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen), natspk_(char *, logical *, integer *, ftnlen);
    static doublereal et0;
    extern /* Subroutine */ int furnsh_(char *, ftnlen), spkezp_(integer *, 
	    doublereal *, char *, char *, integer *, doublereal *, doublereal 
	    *, ftnlen, ftnlen);
    static doublereal pv1[3], pv2[3];
    static integer dim;
    static char ref[80];
    static integer obs;
    static doublereal ang1, ang2;
    static integer han1;
    extern /* Subroutine */ int zzgfspq_(doublereal *, integer *, integer *, 
	    doublereal *, doublereal *, integer *, char *, char *, doublereal 
	    *, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        ZZGFSPQ */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the low-level SPICELIB */
/*     geometry routine ZZGFSPQ. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.1, 13-DEC-2011 (EDW) */

/*        Deleted extraneous trailing whitespace. */

/* -    TSPICE Version 1.0.0, 08-MAR-2009 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     ATOL is a tolerance value for computing arc sine. */


/*     Local variables */


/*     Save all. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFSPQ", (ftnlen)9);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);

/*     Create an LSK, load using FURNSH. */

    zztstlsk_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the PCK for Nat's Solar System. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create the SPK for Nat's Solar System. */

    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a confinement window from ET0 and ET1. */

    s_copy(time0, "2000 JAN 1  12:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Case 1 */

    tcase_("Ephemeris data unavailable", (ftnlen)26);
    targ1 = -203;
    targ2 = 499;
    obs = 10;
    r1 = 0.;
    r2 = 0.;
    s_copy(ref, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    zzgfspq_(&et0, &targ1, &targ2, &r1, &r2, &obs, abcorr, ref, &value, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);
    bodvrd_("ALPHA", "RADII", &c__3, &dim, rada, (ftnlen)5, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);
    bodvrd_("BETA", "RADII", &c__3, &dim, radb, (ftnlen)4, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);

/*     Case 2 */

    tcase_("Negative body radius R1 and R2", (ftnlen)30);
    bodvrd_("ALPHA", "RADII", &c__3, &dim, rada, (ftnlen)5, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);
    bodvrd_("BETA", "RADII", &c__3, &dim, radb, (ftnlen)4, (ftnlen)5);
    chcksi_("BODVRD ALPHA", &dim, "=", &c__3, &c__0, ok, (ftnlen)12, (ftnlen)
	    1);
    targ1 = 1000;
    targ2 = 2000;
    obs = 10;
    r1 = -rada[0];
    r2 = 0.;
    s_copy(ref, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    zzgfspq_(&et0, &targ1, &targ2, &r1, &r2, &obs, abcorr, ref, &value, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADRADIUS)", ok, (ftnlen)16);
    r1 = 0.;
    r2 = -radb[0];
    zzgfspq_(&et0, &targ1, &targ2, &r1, &r2, &obs, abcorr, ref, &value, (
	    ftnlen)80, (ftnlen)80);
    chckxc_(&c_true, "SPICE(BADRADIUS)", ok, (ftnlen)16);

/*     Case 2 */


/*     Check a simple calculation for all CORR. */

    targ1 = 1000;
    targ2 = 2000;
    obs = 10;
    r1 = 0.;
    r2 = 0.;
    s_copy(ref, "J2000", (ftnlen)80, (ftnlen)5);
    s_copy(abcorr, "NONE", (ftnlen)80, (ftnlen)4);
    for (i__ = 1; i__ <= 9; ++i__) {
	tcase_(corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"corr", i__1, "f_zzgfspq__", (ftnlen)322)) * 80, (ftnlen)80);
	zzgfspq_(&et0, &targ1, &targ2, &r1, &r2, &obs, corr + ((i__1 = i__ - 
		1) < 9 && 0 <= i__1 ? i__1 : s_rnge("corr", i__1, "f_zzgfspq"
		"__", (ftnlen)324)) * 80, ref, &value, (ftnlen)80, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Check a simple calculation for invalid CORR. */

    for (i__ = 1; i__ <= 9; ++i__) {
/* Writing concatenation */
	i__2[0] = 1, a__1[0] = "Z";
	i__2[1] = 79, a__1[1] = corr + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? 
		i__1 : s_rnge("corr", i__1, "f_zzgfspq__", (ftnlen)336)) * 80;
	s_cat(abcorr, a__1, i__2, &c__2, (ftnlen)80);
	tcase_(abcorr, (ftnlen)80);
	zzgfspq_(&et0, &targ1, &targ2, &r1, &r2, &obs, abcorr, ref, &value, (
		ftnlen)80, (ftnlen)80);
	chckxc_(&c_true, "SPICE(SPKINVALIDOPTION)", ok, (ftnlen)23);
    }

/*     Case 3 */


/*     Run a loop over one orbit of ALPHA at 50 TDB second */
/*     intervals with each of the aborration corrections. */
/*     Any occultation event exists for longer than 50 seconds. */
/*     Compare the value returned from ZZGFSPQ with a direct */
/*     calculation. */

    targ1 = 1000;
    targ2 = 2000;
    obs = 10;
    s_copy(ref, "J2000", (ftnlen)80, (ftnlen)5);
    xr[0] = 0.;
    xr[2] = rada[0];
    xr[4] = 0.;
    xr[6] = rada[0];
    xr[1] = 0.;
    xr[3] = 0.;
    xr[5] = radb[0];
    xr[7] = radb[0];
    for (i__ = 0; i__ <= 86400; i__ += 50) {
	for (j = 1; j <= 9; ++j) {
	    s_copy(abcorr, corr + ((i__1 = j - 1) < 9 && 0 <= i__1 ? i__1 : 
		    s_rnge("corr", i__1, "f_zzgfspq__", (ftnlen)378)) * 80, (
		    ftnlen)80, (ftnlen)80);
	    for (k = 1; k <= 4; ++k) {
		r1 = xr[(i__1 = (k << 1) - 2) < 8 && 0 <= i__1 ? i__1 : 
			s_rnge("xr", i__1, "f_zzgfspq__", (ftnlen)382)];
		r2 = xr[(i__1 = (k << 1) - 1) < 8 && 0 <= i__1 ? i__1 : 
			s_rnge("xr", i__1, "f_zzgfspq__", (ftnlen)383)];
		s_copy(title, "ET = #1, R1 = #2, R2 = #3, CORR = #4", (ftnlen)
			80, (ftnlen)36);
		repmd_(title, "#1", &et, &c__6, title, (ftnlen)80, (ftnlen)2, 
			(ftnlen)80);
		repmd_(title, "#2", &r1, &c__6, title, (ftnlen)80, (ftnlen)2, 
			(ftnlen)80);
		repmd_(title, "#3", &r2, &c__6, title, (ftnlen)80, (ftnlen)2, 
			(ftnlen)80);
		repmi_(title, "#4", &j, title, (ftnlen)80, (ftnlen)2, (ftnlen)
			80);
		tcase_(title, (ftnlen)80);
		et = et0 + (doublereal) i__;
		zzgfspq_(&et, &targ1, &targ2, &r1, &r2, &obs, abcorr, ref, &
			value, (ftnlen)80, (ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		spkezp_(&targ1, &et, ref, abcorr, &obs, pv1, &lt, (ftnlen)80, 
			(ftnlen)80);
		spkezp_(&targ2, &et, ref, abcorr, &obs, pv2, &lt, (ftnlen)80, 
			(ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		range1 = vnorm_(pv1);
		range2 = vnorm_(pv2);
		d__1 = r1 / range1;
		ang1 = dasine_(&d__1, &c_b101);
		d__1 = r2 / range2;
		ang2 = dasine_(&d__1, &c_b101);
		theta = vsep_(pv1, pv2) - ang1 - ang2;

/*              The results should match to roundoff. */

		chcksd_(title, &value, "=", &theta, &c_b101, ok, (ftnlen)80, (
			ftnlen)1);
	    }
	}
    }

/* Case n */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("gfsep.tls", (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfspq__ */

