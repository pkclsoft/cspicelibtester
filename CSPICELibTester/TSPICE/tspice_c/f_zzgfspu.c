/* f_zzgfspu.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__1000 = 1000;
static integer c__2000 = 2000;
static integer c__0 = 0;
static integer c__10 = 10;
static doublereal c_b115 = 0.;
static doublereal c_b136 = 1e-12;

/* $Procedure F_ZZGFSPU ( ZZGFSPU family tests ) */
/* Subroutine */ int f_zzgfspu__(logical *ok)
{
    /* Initialized data */

    static char targt1[25*6] = "ALPHA                    " "X               "
	    "         " "ALPHA                    " "ALPHA                    "
	     "ALPHA                    " "SUN                      ";
    static char targt2[25*6] = "X                        " "BETA            "
	    "         " "BETA                     " "ALPHA                    "
	     "BETA                     " "BETA                     ";
    static char obsrvr[25*6] = "SUN                      " "SUN             "
	    "         " "X                        " "SUN                      "
	     "BETA                     " "SUN                      ";
    static char corr[25*9] = "NONE                     " "lt                "
	    "       " " lt+s                    " " cn                      " 
	    " cn + s                  " "XLT                      " "XLT + S"
	    "                  " "XCN                      " "XCN+S          "
	    "          ";
    static char shapes[80*2] = "POINT                                       "
	    "                                    " "SPHERE                   "
	    "                                                       ";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double asin(doublereal);

    /* Local variables */
    doublereal rada, radb;
    integer xbod[2];
    doublereal xrad[2];
    char from[80], xref[25*2], yref[25];
    doublereal posa[3], posb[3];
    integer xobs;
    extern doublereal vsep_(doublereal *, doublereal *);
    integer xshp[2];
    extern /* Subroutine */ int zzgftreb_(integer *, doublereal *), zzgfspin_(
	    char *, char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    integer i__, j, k;
    doublereal hanga, hangb;
    char frame[25*2], xabcr[25], shape[80*2];
    doublereal axesa[3], axesb[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen);
    doublereal theta;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer *, char *
	    , ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    extern doublereal vnorm_(doublereal *);
    extern /* Subroutine */ int t_success__(logical *);
    char of[80*2];
    doublereal et;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen);
    doublereal lt;
    extern /* Subroutine */ int kclear_(void), delfil_(char *, ftnlen), 
	    chckxc_(logical *, char *, logical *, ftnlen), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    char abcorr[25];
    extern /* Subroutine */ int natpck_(char *, logical *, logical *, ftnlen),
	     natspk_(char *, logical *, integer *, ftnlen), furnsh_(char *, 
	    ftnlen), cmprss_(char *, integer *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), tstlsk_(void), spkpos_(char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    doublereal sep;
    char txt[80];
    integer han1;
    extern /* Subroutine */ int zzgfspq_(doublereal *, integer *, integer *, 
	    doublereal *, doublereal *, integer *, char *, char *, doublereal 
	    *, ftnlen, ftnlen), zzgfspx_(char *, integer *, char *, char *, 
	    integer *, doublereal *, integer *, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        ZZGFSPU */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the low-level SPICELIB */
/*     geometry routine ZZGFSPU. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     E.D. Wright     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 18-JUN-2013 (EDW)(BVS) */

/*        Added test cases to confirm exected state of */
/*        initialized values and expected errors from ZZGFSPIN. */

/* -    TSPICE Version 1.0.0, 17-APR-2008 (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Indices 1:3 for Invalid body name test, 4:6 for not distinct */
/*     body names test. */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFSPU", (ftnlen)9);

/*     Case 1: Create kernels, load same. */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create a PCK, load using FURNSH. */

    natpck_("nat.pck", &c_false, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an SPK, load using FURNSH. */

    natspk_("nat.bsp", &c_true, &han1, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Assign the body radii. */

    zzgftreb_(&c__1000, axesa);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(axesa[0],axesa[1]);
    rada = max(d__1,axesa[2]);
    zzgftreb_(&c__2000, axesb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(axesb[0],axesb[1]);
    radb = max(d__1,axesb[2]);

/*     Case 2: Invalid body names. */

    s_copy(abcorr, corr, (ftnlen)25, (ftnlen)25);
    s_copy(shape, shapes, (ftnlen)80, (ftnlen)80);
    s_copy(shape + 80, shapes, (ftnlen)80, (ftnlen)80);
    s_copy(frame, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    s_copy(frame + 25, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(of, targt1 + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("targt1", i__1, "f_zzgfspu__", (ftnlen)273)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(of + 80, targt2 + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("targt2", i__1, "f_zzgfspu__", (ftnlen)274)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(from, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_zzgfspu__", (ftnlen)275)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(txt, "Invalid body name test. OF(1) = #, OF(2) = #, FROM = #", 
		(ftnlen)80, (ftnlen)54);
	repmc_(txt, "#", of, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (ftnlen)
		80);
	repmc_(txt, "#", of + 80, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	repmc_(txt, "#", from, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(txt, (ftnlen)80);
	zzgfspin_(of, from, shape, frame, abcorr, (ftnlen)80, (ftnlen)80, (
		ftnlen)80, (ftnlen)25, (ftnlen)25);
	chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    }

/*     Case 3: Not distinct body names. */

    s_copy(abcorr, corr, (ftnlen)25, (ftnlen)25);
    s_copy(shape, shapes, (ftnlen)80, (ftnlen)80);
    s_copy(shape + 80, shapes, (ftnlen)80, (ftnlen)80);
    s_copy(frame, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    s_copy(frame + 25, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    for (i__ = 4; i__ <= 6; ++i__) {
	s_copy(of, targt1 + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("targt1", i__1, "f_zzgfspu__", (ftnlen)303)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(of + 80, targt2 + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("targt2", i__1, "f_zzgfspu__", (ftnlen)304)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(from, obsrvr + ((i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("obsrvr", i__1, "f_zzgfspu__", (ftnlen)305)) * 25, (
		ftnlen)80, (ftnlen)25);
	s_copy(txt, "Not distinct body name test. OF(1) = #, OF(2) = #, FROM"
		" = #", (ftnlen)80, (ftnlen)59);
	repmc_(txt, "#", of, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (ftnlen)
		80);
	repmc_(txt, "#", of + 80, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	repmc_(txt, "#", from, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
		ftnlen)80);
	tcase_(txt, (ftnlen)80);
	zzgfspin_(of, from, shape, frame, abcorr, (ftnlen)80, (ftnlen)80, (
		ftnlen)80, (ftnlen)25, (ftnlen)25);
	chckxc_(&c_true, "SPICE(BODIESNOTDISTINCT)", ok, (ftnlen)24);
    }

/*     Case 4: */

    s_copy(abcorr, corr, (ftnlen)25, (ftnlen)25);
    s_copy(shape, "X", (ftnlen)80, (ftnlen)1);
    s_copy(shape + 80, shapes + 80, (ftnlen)80, (ftnlen)80);
    s_copy(frame, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    s_copy(frame + 25, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    s_copy(of, targt1, (ftnlen)80, (ftnlen)25);
    s_copy(of + 80, targt2 + 25, (ftnlen)80, (ftnlen)25);
    s_copy(from, obsrvr, (ftnlen)80, (ftnlen)25);
    s_copy(txt, "Invalid shape. SHAPE(1) = #, SHAPE(2) = #", (ftnlen)80, (
	    ftnlen)41);
    repmc_(txt, "#", shape, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (ftnlen)
	    80);
    repmc_(txt, "#", shape + 80, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
	    ftnlen)80);
    tcase_(txt, (ftnlen)80);
    zzgfspin_(of, from, shape, frame, abcorr, (ftnlen)80, (ftnlen)80, (ftnlen)
	    80, (ftnlen)25, (ftnlen)25);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);
    s_copy(shape, shapes, (ftnlen)80, (ftnlen)80);
    s_copy(shape + 80, "X", (ftnlen)80, (ftnlen)1);
    s_copy(txt, "Invalid shape. SHAPE(1) = #, SHAPE(2) = #", (ftnlen)80, (
	    ftnlen)41);
    repmc_(txt, "#", shape, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (ftnlen)
	    80);
    repmc_(txt, "#", shape + 80, txt, (ftnlen)80, (ftnlen)1, (ftnlen)80, (
	    ftnlen)80);
    tcase_(txt, (ftnlen)80);
    zzgfspin_(of, from, shape, frame, abcorr, (ftnlen)80, (ftnlen)80, (ftnlen)
	    80, (ftnlen)25, (ftnlen)25);
    chckxc_(&c_true, "SPICE(NOTRECOGNIZED)", ok, (ftnlen)20);

/*     Case 5: Confirm initialized values are correctly saved. */

    s_copy(of, "1000", (ftnlen)80, (ftnlen)4);
    s_copy(of + 80, "2000", (ftnlen)80, (ftnlen)4);
    s_copy(from, "SUN", (ftnlen)80, (ftnlen)3);
    s_copy(frame, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    s_copy(frame + 25, "ECLIPJ2000", (ftnlen)25, (ftnlen)10);
    for (i__ = 1; i__ <= 2; ++i__) {
	for (j = 1; j <= 2; ++j) {
	    for (k = 1; k <= 9; ++k) {
		s_copy(abcorr, corr + ((i__1 = k - 1) < 9 && 0 <= i__1 ? i__1 
			: s_rnge("corr", i__1, "f_zzgfspu__", (ftnlen)377)) * 
			25, (ftnlen)25, (ftnlen)25);
		s_copy(shape, shapes + ((i__1 = i__ - 1) < 2 && 0 <= i__1 ? 
			i__1 : s_rnge("shapes", i__1, "f_zzgfspu__", (ftnlen)
			378)) * 80, (ftnlen)80, (ftnlen)80);
		s_copy(shape + 80, shapes + ((i__1 = j - 1) < 2 && 0 <= i__1 ?
			 i__1 : s_rnge("shapes", i__1, "f_zzgfspu__", (ftnlen)
			379)) * 80, (ftnlen)80, (ftnlen)80);
		repmc_("ZZGFSPX #1 #2 #3", "#1", abcorr, txt, (ftnlen)16, (
			ftnlen)2, (ftnlen)25, (ftnlen)80);
		repmc_(txt, "#2", shape, txt, (ftnlen)80, (ftnlen)2, (ftnlen)
			80, (ftnlen)80);
		repmc_(txt, "#3", shape + 80, txt, (ftnlen)80, (ftnlen)2, (
			ftnlen)80, (ftnlen)80);
		tcase_(txt, (ftnlen)80);
		zzgfspin_(of, from, shape, frame, abcorr, (ftnlen)80, (ftnlen)
			80, (ftnlen)80, (ftnlen)25, (ftnlen)25);
		zzgfspx_(xabcr, xbod, yref, xref, &xobs, xrad, xshp, (ftnlen)
			25, (ftnlen)25, (ftnlen)25);

/*              Squeaky cleanize the ABCORR string. */

		ucase_(abcorr, abcorr, (ftnlen)25, (ftnlen)25);
		cmprss_(" ", &c__0, abcorr, abcorr, (ftnlen)1, (ftnlen)25, (
			ftnlen)25);
		chcksc_("XABCR", xabcr, "=", abcorr, ok, (ftnlen)5, (ftnlen)
			25, (ftnlen)1, (ftnlen)25);

/*              Check IDs for target bodies array. */

		chcksi_("XBOD 1", xbod, "=", &c__1000, &c__0, ok, (ftnlen)6, (
			ftnlen)1);
		chcksi_("XBOD 2", &xbod[1], "=", &c__2000, &c__0, ok, (ftnlen)
			6, (ftnlen)1);

/*              All calculations use J2000 frame. */

		chcksc_("YREF", yref, "=", "J2000", ok, (ftnlen)4, (ftnlen)25,
			 (ftnlen)1, (ftnlen)5);
		chcksc_("XREF(1)", xref, "=", frame, ok, (ftnlen)7, (ftnlen)
			25, (ftnlen)1, (ftnlen)25);
		chcksc_("XREF(2)", xref + 25, "=", frame + 25, ok, (ftnlen)7, 
			(ftnlen)25, (ftnlen)1, (ftnlen)25);

/*              Check ID for observer body. */

		chcksi_("XOBS", &xobs, "=", &c__10, &c__0, ok, (ftnlen)4, (
			ftnlen)1);

/*              Check Index for SHAPES array. */

		chcksi_("XSHP 1", xshp, "=", &i__, &c__0, ok, (ftnlen)6, (
			ftnlen)1);
		chcksi_("XSHP 2", &xshp[1], "=", &j, &c__0, ok, (ftnlen)6, (
			ftnlen)1);

/*              Check radii for spherical body, XSHP(I) = 2 */

		if (xshp[0] == 2) {
		    chcksd_("RAD 1", xrad, "=", &rada, &c_b115, ok, (ftnlen)5,
			     (ftnlen)1);
		}
		if (xshp[1] == 2) {
		    chcksd_("RAD 2", &xrad[1], "=", &radb, &c_b115, ok, (
			    ftnlen)5, (ftnlen)1);
		}
	    }
	}
    }

/*     Test the ZZGFSPQ result using ALPHA and BETA from Nat's Solar */
/*     System. */

/*     Step in increments of one minute from ET 0 to ET 86400 in steps */
/*     of 60 TDB seconds. Test the four combinations of Sphere and Point */
/*     object pairs. Perform each test in a block for clarity. */


/*     Perform the four test blocks for each aberration correction value. */

    for (j = 1; j <= 9; ++j) {
	s_copy(abcorr, corr + ((i__1 = j - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corr", i__1, "f_zzgfspu__", (ftnlen)461)) * 25, (
		ftnlen)25, (ftnlen)25);

/*        Case 6: Sphere-Sphere */

	et = 0.;
	for (i__ = 1; i__ <= 1440; ++i__) {
	    repmi_("ZZGFSPQ Sphere/Sphere #1, ABCORR #2", "#1", &i__, txt, (
		    ftnlen)35, (ftnlen)2, (ftnlen)80);
	    repmc_(txt, "#2", abcorr, txt, (ftnlen)80, (ftnlen)2, (ftnlen)25, 
		    (ftnlen)80);
	    tcase_(txt, (ftnlen)80);
	    zzgfspq_(&et, &c__1000, &c__2000, &rada, &radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)25, (ftnlen)5);
	    spkpos_("ALPHA", &et, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    5, (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    spkpos_("BETA", &et, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)4,
		     (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    hanga = asin(rada / vnorm_(posa));
	    hangb = asin(radb / vnorm_(posb));
	    theta = vsep_(posa, posb) - (hanga + hangb);
	    chcksd_("ZZGFSPQ", &sep, "~", &theta, &c_b136, ok, (ftnlen)7, (
		    ftnlen)1);
	    et += 60.;
	}

/*        Case 7: Point-Sphere */

	rada = 0.;
	et = 0.;
	for (i__ = 1; i__ <= 1440; ++i__) {
	    repmi_("ZZGFSPQ Point/Sphere #1, ABCORR #2", "#1", &i__, txt, (
		    ftnlen)34, (ftnlen)2, (ftnlen)80);
	    repmc_(txt, "#2", abcorr, txt, (ftnlen)80, (ftnlen)2, (ftnlen)25, 
		    (ftnlen)80);
	    tcase_(txt, (ftnlen)80);
	    zzgfspq_(&et, &c__1000, &c__2000, &rada, &radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)25, (ftnlen)5);
	    spkpos_("ALPHA", &et, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    5, (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    spkpos_("BETA", &et, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)4,
		     (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    hanga = asin(rada / vnorm_(posa));
	    hangb = asin(radb / vnorm_(posb));
	    theta = vsep_(posa, posb) - (hanga + hangb);
	    chcksd_("ZZGFSPQ", &sep, "~", &theta, &c_b136, ok, (ftnlen)7, (
		    ftnlen)1);
	    et += 60.;
	}

/*        Case 8: Sphere-Point */

/* Computing MAX */
	d__1 = max(axesa[0],axesa[1]);
	rada = max(d__1,axesa[2]);
	radb = 0.;
	et = 0.;
	for (i__ = 1; i__ <= 1440; ++i__) {
	    repmi_("ZZGFSPQ Sphere/Point #1, ABCORR #2", "#1", &i__, txt, (
		    ftnlen)34, (ftnlen)2, (ftnlen)80);
	    repmc_(txt, "#2", abcorr, txt, (ftnlen)80, (ftnlen)2, (ftnlen)25, 
		    (ftnlen)80);
	    tcase_(txt, (ftnlen)80);
	    zzgfspq_(&et, &c__1000, &c__2000, &rada, &radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)25, (ftnlen)5);
	    spkpos_("ALPHA", &et, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    5, (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    spkpos_("BETA", &et, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)4,
		     (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    hanga = asin(rada / vnorm_(posa));
	    hangb = asin(radb / vnorm_(posb));
	    theta = vsep_(posa, posb) - (hanga + hangb);
	    chcksd_("ZZGFSPQ", &sep, "~", &theta, &c_b136, ok, (ftnlen)7, (
		    ftnlen)1);
	    et += 60.;
	}

/*        Case 9: Point-Point */

	rada = 0.;
	radb = 0.;
	et = 0.;
	for (i__ = 1; i__ <= 1440; ++i__) {
	    repmi_("ZZGFSPQ Point/Point #1, ABCORR #2", "#1", &i__, txt, (
		    ftnlen)33, (ftnlen)2, (ftnlen)80);
	    repmc_(txt, "#2", abcorr, txt, (ftnlen)80, (ftnlen)2, (ftnlen)25, 
		    (ftnlen)80);
	    tcase_(txt, (ftnlen)80);
	    zzgfspq_(&et, &c__1000, &c__2000, &rada, &radb, &c__10, abcorr, 
		    "J2000", &sep, (ftnlen)25, (ftnlen)5);
	    spkpos_("ALPHA", &et, "J2000", abcorr, "SUN", posa, &lt, (ftnlen)
		    5, (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    spkpos_("BETA", &et, "J2000", abcorr, "SUN", posb, &lt, (ftnlen)4,
		     (ftnlen)5, (ftnlen)25, (ftnlen)3);
	    hanga = asin(rada / vnorm_(posa));
	    hangb = asin(radb / vnorm_(posb));
	    theta = vsep_(posa, posb) - (hanga + hangb);
	    chcksd_("ZZGFSPQ", &sep, "~", &theta, &c_b136, ok, (ftnlen)7, (
		    ftnlen)1);
	    et += 60.;
	}
    }

/*     Case N */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    kclear_();
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.pck", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfspu__ */

