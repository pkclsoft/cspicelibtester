/* f_zzgfssin.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b19 = 0.;
static doublereal c_b21 = 1.;
static integer c__3 = 3;
static integer c__0 = 0;
static doublereal c_b202 = 10.;
static integer c__1 = 1;

/* $Procedure      F_ZZGFSSIN ( Test surface intercept state computation ) */
/* Subroutine */ int f_zzgfssin__(logical *ok)
{
    /* Initialized data */

    static char corr[200*9] = "NONE                                         "
	    "                                                                "
	    "                                                                "
	    "                           " "lt                                "
	    "                                                                "
	    "                                                                "
	    "                                      " " lt+s                  "
	    "                                                                "
	    "                                                                "
	    "                                                 " " cn         "
	    "                                                                "
	    "                                                                "
	    "                                                            " 
	    " cn + s                                                        "
	    "                                                                "
	    "                                                                "
	    "         " "XLT                                                 "
	    "                                                                "
	    "                                                                "
	    "                    " "XLT + S                                  "
	    "                                                                "
	    "                                                                "
	    "                               " "XCN                           "
	    "                                                                "
	    "                                                                "
	    "                                          " "XCN+S              "
	    "                                                                "
	    "                                                                "
	    "                                                     ";
    static char eframe[32*4] = "J2000                           " "IAU_EARTH"
	    "                       " "ECLIPJ2000                      " "IAU"
	    "_MOON                        ";
    static char jframe[32*4] = "J2000                           " "IAU_JUPIT"
	    "ER                     " "ECLIPJ2000                      " "IAU"
	    "_IO                          ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    static char dref[32];
    static doublereal dvec[3];
    static integer dctr;
    static doublereal xvel[3];
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static doublereal spnt[6]	/* was [3][2] */, xpnt[3];
    extern /* Subroutine */ int zzgfssin_(char *, integer *, doublereal *, 
	    char *, char *, integer *, char *, integer *, doublereal *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    static integer i__;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static doublereal t, radii[3], delta;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen), tcase_(
	    char *, ftnlen);
    static integer obsid;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    static integer trgid;
    static logical found;
    static doublereal state[6];
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen), bodn2c_(char *, 
	    integer *, logical *, ftnlen), t_success__(logical *), chckad_(
	    char *, doublereal *, char *, doublereal *, integer *, doublereal 
	    *, logical *, ftnlen, ftnlen);
    static integer nc;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static integer nf;
    static doublereal et;
    static integer handle;
    static doublereal lt;
    static integer ns, frcode, frclid;
    static char abcorr[200];
    static doublereal negvec[3];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static char method[200], fixref[32], target[36], obsrvr[36];
    static doublereal et0;
    static char timstr[50];
    static doublereal srfvec[3], trgepc;
    static integer frclss;
    static logical attblk[6];
    extern /* Subroutine */ int tstlsk_(void), tstpck_(char *, logical *, 
	    logical *, ftnlen), tstspk_(char *, logical *, integer *, ftnlen),
	     bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen), spkpos_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), namfrm_(char *, integer *, ftnlen), frinfo_(integer *, 
	    integer *, integer *, integer *, logical *), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen), sincpt_(char *, char *, 
	    doublereal *, char *, char *, char *, char *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), qderiv_(integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), prefix_(
	    char *, integer *, char *, ftnlen, ftnlen), vminus_(doublereal *, 
	    doublereal *), spkuef_(integer *), delfil_(char *, ftnlen);
    static logical fnd;
    extern doublereal spd_(void);
    static doublereal tol;

/* $ Abstract */

/*     Test the GF private surface intercept state routine ZZGFSSIN. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF */
/*     surface intercept state routine ZZGFSSIN. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 3.0.0, 12-MAY-2009 (NJB) */

/*        Removed error checks for ID codes that don't */
/*        map to names, as these are no longer applicable. */

/* -    SPICELIB Version 2.0.0, 23-SEP-2008 (NJB) */

/*        Relaxed tolerances to enable clean test runs */
/*        on all supported platforms. Changed quantity name */
/*        "sub-point" to "intercept" in test utility calls. */

/* -    SPICELIB Version 1.0.0, 09-MAY-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local parameters */


/*     Time delta used for discrete derivatives. The 10-second */
/*     value used here was found by trial and error: it yields */
/*     better approximations than does the "standard" 1-second */
/*     value. */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFSSIN", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgfssob.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgfssob.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction", (ftnlen)25);
    s_copy(abcorr, "None.", (ftnlen)200, (ftnlen)5);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad computation method", (ftnlen)22);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)200, (ftnlen)10);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "ITRF93", (ftnlen)32, (ftnlen)6);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame not centered on target", (ftnlen)38);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);
    s_copy(abcorr, "None", (ftnlen)200, (ftnlen)4);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(fixref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(dref, fixref, (ftnlen)32, (ftnlen)32);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF ephemeris data available", (ftnlen)32);
    s_copy(abcorr, "LT", (ftnlen)200, (ftnlen)2);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "Earth", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(dref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    dctr = 9511010;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No DREF orientation data available", (ftnlen)34);
    s_copy(abcorr, "LT", (ftnlen)200, (ftnlen)2);
    s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "Earth", (ftnlen)36, (ftnlen)5);
    s_copy(fixref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(dref, "ITRF93", (ftnlen)32, (ftnlen)6);
    dctr = 399;
    vpack_(&c_b19, &c_b19, &c_b21, dvec);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &dctr, dvec, 
	    radii, state, &found, (ftnlen)200, (ftnlen)32, (ftnlen)200, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     We're going to loop over all aberration corrections and */
/*     ray direction frames. */

    for (nc = 1; nc <= 9; ++nc) {
	for (nf = 1; nf <= 4; ++nf) {

/*           Loop over the set of sample times. */

	    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
	    str2et_(timstr, &et0, (ftnlen)50);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delta = spd_() * 100;
	    for (ns = 1; ns <= 10; ++ns) {

/* --- Case: ------------------------------------------------------ */


		s_copy(abcorr, corr + ((i__1 = nc - 1) < 9 && 0 <= i__1 ? 
			i__1 : s_rnge("corr", i__1, "f_zzgfssin__", (ftnlen)
			592)) * 200, (ftnlen)200, (ftnlen)200);
		s_copy(method, "Ellipsoid", (ftnlen)200, (ftnlen)9);
		s_copy(dref, eframe + (((i__1 = nf - 1) < 4 && 0 <= i__1 ? 
			i__1 : s_rnge("eframe", i__1, "f_zzgfssin__", (ftnlen)
			594)) << 5), (ftnlen)32, (ftnlen)32);
		s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
		s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
		s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
		bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (
			ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		bodn2c_(target, &trgid, &found, (ftnlen)36);
		bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
		s_copy(title, "#-# intercept. #; #; DREF = #; #", (ftnlen)200,
			 (ftnlen)32);
		repmc_(title, "#", obsrvr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", method, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)200, (ftnlen)200);
		repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)200, (ftnlen)200);
		repmc_(title, "#", dref, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)32, (ftnlen)200);
		et = et0 + (ns - 1) * delta;
		etcal_(&et, timstr, (ftnlen)50);
		repmc_(title, "#", timstr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)50, (ftnlen)200);
		tcase_(title, (ftnlen)200);

/*              Look up the direction vector. */

		spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		namfrm_(dref, &frcode, (ftnlen)32);
		chcksi_("FRCODE ", &frcode, "!=", &c__0, &c__0, ok, (ftnlen)7,
			 (ftnlen)2);
		frinfo_(&frcode, &dctr, &frclss, &frclid, &fnd);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FRINFO FND", &fnd, &c_true, ok, (ftnlen)10);
		zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &
			dctr, dvec, radii, state, &found, (ftnlen)200, (
			ftnlen)32, (ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              By construction, the state should be found. */

		chcksl_("ZZGFSSIN found", &found, &c_true, ok, (ftnlen)14);
/*               WRITE (*,*) 'STATE = ', STATE */

/*              Use SRFVEC and FIXREF as our ray direction and frame. */

/*              Estimate the surface intercept point velocity */
/*              numerically. */

		for (i__ = 1; i__ <= 2; ++i__) {
		    t = et + ((i__ << 1) - 3) * 10.;

/*                 DVEC is constant in frame DREF, so we don't need */
/*                 to re-compute it. */

		    sincpt_(method, target, &t, fixref, abcorr, obsrvr, dref, 
			    dvec, &spnt[(i__1 = i__ * 3 - 3) < 6 && 0 <= i__1 
			    ? i__1 : s_rnge("spnt", i__1, "f_zzgfssin__", (
			    ftnlen)661)], &trgepc, srfvec, &found, (ftnlen)
			    200, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)
			    36, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("ZZGFSSIN found (0)", &found, &c_true, ok, (
			    ftnlen)18);
		}
		qderiv_(&c__3, spnt, &spnt[3], &c_b202, xvel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the velocity from ZZGFSSIN. We expect an error of */
/*              less than 1 m/s, except when the frame is IAU_EARTH, */
/*              because the velocity of the intercept is quite high in */
/*              that frame. */

		if (s_cmp(dref, "IAU_EARTH", (ftnlen)32, (ftnlen)9) != 0) {
		    tol = .001;
		} else {
		    tol = .001;
		}
		chckad_("Intercept vel (0)", &state[3], "~~", xvel, &c__3, &
			tol, ok, (ftnlen)17, (ftnlen)2);
/*               IF ( .NOT. OK ) THEN */
/*                  WRITE (*,*) '>>>>>>>>>>>>>Speed = ', VNORM(XVEL) */
/*               END IF */

/*              Check the surface intercept. */

		sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, 
			dvec, xpnt, &trgepc, srfvec, &found, (ftnlen)200, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
			ftnlen)32);

/*              The tolerance we use depends on the aberration */
/*              correction. */

		zzprscor_(abcorr, attblk, (ftnlen)200);
		if (attblk[3]) {
		    tol = .001;
		} else if (attblk[1]) {
		    tol = .001;
		} else {
		    tol = 1e-5;
		}
		chckad_("Surface intercept point (0)", state, "~~", xpnt, &
			c__3, &tol, ok, (ftnlen)27, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */


/*              Given the inputs above, make sure we do not find */
/*              the state if we reverse the ray's direction.' */

		prefix_("Earth non-intersection case:", &c__1, title, (ftnlen)
			28, (ftnlen)200);
		tcase_(title, (ftnlen)200);
		vminus_(dvec, negvec);
		vequ_(negvec, dvec);
		zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &
			dctr, dvec, radii, state, &found, (ftnlen)200, (
			ftnlen)32, (ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              By construction, the state should NOT be found. */

		chcksl_("ZZGFSSIN found (1)", &found, &c_false, ok, (ftnlen)
			18);

/* --- Case: ------------------------------------------------------ */

		s_copy(abcorr, corr + ((i__1 = nc - 1) < 9 && 0 <= i__1 ? 
			i__1 : s_rnge("corr", i__1, "f_zzgfssin__", (ftnlen)
			753)) * 200, (ftnlen)200, (ftnlen)200);
		s_copy(method, "ellipsoid", (ftnlen)200, (ftnlen)9);
		s_copy(target, "JUPITER", (ftnlen)36, (ftnlen)7);
		s_copy(obsrvr, "IO", (ftnlen)36, (ftnlen)2);
		s_copy(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
		s_copy(dref, jframe + (((i__1 = nf - 1) < 4 && 0 <= i__1 ? 
			i__1 : s_rnge("jframe", i__1, "f_zzgfssin__", (ftnlen)
			758)) << 5), (ftnlen)32, (ftnlen)32);
		s_copy(title, "#-# intercept. #; #; DREF = #; #", (ftnlen)200,
			 (ftnlen)32);
		repmc_(title, "#", obsrvr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", target, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)36, (ftnlen)200);
		repmc_(title, "#", method, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)200, (ftnlen)200);
		repmc_(title, "#", abcorr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)200, (ftnlen)200);
		repmc_(title, "#", dref, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)32, (ftnlen)200);
		etcal_(&et, timstr, (ftnlen)50);
		repmc_(title, "#", timstr, title, (ftnlen)200, (ftnlen)1, (
			ftnlen)50, (ftnlen)200);
		tcase_(title, (ftnlen)200);
		bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (
			ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		bodn2c_(target, &trgid, &found, (ftnlen)36);
		bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);

/*              Look up the direction vector. */

		spkpos_(target, &et, dref, abcorr, obsrvr, dvec, &lt, (ftnlen)
			36, (ftnlen)32, (ftnlen)200, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		namfrm_(dref, &frcode, (ftnlen)32);
		chcksi_("FRCODE ", &frcode, "!=", &c__0, &c__0, ok, (ftnlen)7,
			 (ftnlen)2);
		frinfo_(&frcode, &dctr, &frclss, &frclid, &fnd);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chcksl_("FRINFO FND", &fnd, &c_true, ok, (ftnlen)10);
		zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &
			dctr, dvec, radii, state, &found, (ftnlen)200, (
			ftnlen)32, (ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              By construction, the state should be found. */

		chcksl_("ZZGFSSIN found", &found, &c_true, ok, (ftnlen)14);
/*               WRITE (*,*) 'DREF, STATE = ', DREF, STATE */

/*              Use SRFVEC and FIXREF as our ray direction and frame. */

/*              Estimate the surface intercept point velocity */
/*              numerically. */

		for (i__ = 1; i__ <= 2; ++i__) {
		    t = et + ((i__ << 1) - 3) * 10.;

/*                 DVEC is constant in frame DREF, so we don't need */
/*                 to re-compute it. */

		    sincpt_(method, target, &t, fixref, abcorr, obsrvr, dref, 
			    dvec, &spnt[(i__1 = i__ * 3 - 3) < 6 && 0 <= i__1 
			    ? i__1 : s_rnge("spnt", i__1, "f_zzgfssin__", (
			    ftnlen)822)], &trgepc, srfvec, &found, (ftnlen)
			    200, (ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)
			    36, (ftnlen)32);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_("ZZGFSSIN found (2)", &found, &c_true, ok, (
			    ftnlen)18);
		}
		qderiv_(&c__3, spnt, &spnt[3], &c_b202, xvel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Check the velocity from ZZGFSSIN. We expect an error of */
/*              less than 1 m/s. */


/*              The tolerance we use depends on the aberration */
/*              correction. */

		zzprscor_(abcorr, attblk, (ftnlen)200);
		if (attblk[3]) {
		    if (s_cmp(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11) 
			    == 0) {
			tol = .001;
		    } else {
			tol = .001;
		    }
		} else if (attblk[1]) {
		    tol = .001;
		} else {
		    tol = .001;
		}
		chckad_("Intercept vel (1)", &state[3], "~~", xvel, &c__3, &
			tol, ok, (ftnlen)17, (ftnlen)2);
/*               IF ( .NOT. OK ) THEN */
/*                  WRITE (*,*) '>>>>>>>>>>>>>Speed = ', VNORM(XVEL) */
/*               END IF */

/*              Check the surface intercept. */

		sincpt_(method, target, &et, fixref, abcorr, obsrvr, dref, 
			dvec, xpnt, &trgepc, srfvec, &found, (ftnlen)200, (
			ftnlen)36, (ftnlen)32, (ftnlen)200, (ftnlen)36, (
			ftnlen)32);
		if (attblk[3]) {
		    tol = .001;
		} else if (attblk[1]) {
		    tol = .001;
		} else {
		    tol = 1e-5;
		}
		chckad_("Surface intercept point (1)", state, "~~", xpnt, &
			c__3, &tol, ok, (ftnlen)27, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */


/*              Given the inputs above, make sure we do not find */
/*              the state if we reverse the ray's direction.' */

		prefix_("Non-intersection:", &c__1, title, (ftnlen)17, (
			ftnlen)200);
		tcase_(title, (ftnlen)200);
		vminus_(dvec, negvec);
		vequ_(negvec, dvec);
		zzgfssin_(method, &trgid, &et, fixref, abcorr, &obsid, dref, &
			dctr, dvec, radii, state, &found, (ftnlen)200, (
			ftnlen)32, (ftnlen)200, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              By construction, the state should NOT be found. */

		chcksl_("ZZGFSSIN found (3)", &found, &c_false, ok, (ftnlen)
			18);
	    }

/*           End of cases for the current epoch. */

	}

/*        End of cases for the current method. */

    }

/*     End of cases for the current aberration correction. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgfssob.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfssin__ */

