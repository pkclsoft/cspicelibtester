/* f_zzgfssob.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static doublereal c_b130 = 10.;
static doublereal c_b136 = 5e-5;

/* $Procedure      F_ZZGFSSOB ( Test sub-observer state computation ) */
/* Subroutine */ int f_zzgfssob__(logical *ok)
{
    /* Initialized data */

    static char corr[80*9] = "NONE                                          "
	    "                                  " "lt                         "
	    "                                                     " " lt+s   "
	    "                                                                "
	    "        " " cn                                                  "
	    "                           " " cn + s                           "
	    "                                              " "XLT            "
	    "                                                                 "
	     "XLT + S                                                       "
	    "                  " "XCN                                        "
	    "                                     " "XCN+S                   "
	    "                                                        ";
    static char meth[80*2] = "Near point: ellipsoid                         "
	    "                                  " "Intercept:  ellipsoid      "
	    "                                                     ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal xvel[3], spnt[6]	/* was [3][2] */, xpnt[3];
    extern /* Subroutine */ int zzgfssob_(char *, integer *, doublereal *, 
	    char *, char *, integer *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen);
    static integer i__;
    extern /* Subroutine */ int zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static doublereal t, radii[3], delta;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen), tcase_(
	    char *, ftnlen);
    static integer obsid;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen);
    static integer trgid;
    static logical found;
    static doublereal state[6];
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen), bodn2c_(char *, 
	    integer *, logical *, ftnlen), t_success__(logical *), chckad_(
	    char *, doublereal *, char *, doublereal *, integer *, doublereal 
	    *, logical *, ftnlen, ftnlen);
    static integer nc;
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static doublereal et;
    static integer nm, handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static integer ns;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static char abcorr[80], method[80], fixref[32], target[36], obsrvr[36];
    static doublereal et0;
    static char timstr[50];
    static doublereal srfvec[3], trgepc;
    static logical attblk[6];
    extern /* Subroutine */ int tstlsk_(void), tstpck_(char *, logical *, 
	    logical *, ftnlen), tstspk_(char *, logical *, integer *, ftnlen),
	     bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen), subpnt_(char *, char *, doublereal *, char *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), qderiv_(integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), spkuef_(integer *);
    extern doublereal spd_(void);
    static doublereal tol;

/* $ Abstract */

/*     Test the GF private sub-observer state routine ZZGFSSOB. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB GF */
/*     sub-observer state routine ZZGFSSOB. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 12-MAY-2009 (NJB) */

/*        Removed error checks for ID codes that don't */
/*        map to names, as these are no longer applicable. */

/* -    SPICELIB Version 1.0.0, 19-MAY-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local parameters */


/*     Time delta used for discrete derivatives. The 10-second */
/*     value used here was found by trial and error: it yields */
/*     better approximations than does the "standard" 1-second */
/*     value. */


/*     Local variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFSSOB", (ftnlen)10);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
/*     Leapseconds:  Note that the LSK is deleted after loading, so we */
/*     don't have to clean it up later. */

    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load a PCK. */

    tstpck_("zzgfssob.tpc", &c_true, &c_false, (ftnlen)12);

/*     Load an SPK file as well. */

    tstspk_("zzgfssob.bsp", &c_true, &handle, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Actual DE-based ephemerides yield better comparisons, */
/*     since these ephemerides have less noise than do those */
/*     produced by TSTSPK. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL FURNSH ( 'jup230.bsp' ) */

/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad aberration correction", (ftnlen)25);
    s_copy(abcorr, "None.", (ftnlen)80, (ftnlen)5);
    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad computation method", (ftnlen)22);
    s_copy(abcorr, "None", (ftnlen)80, (ftnlen)4);
    s_copy(method, "Near point", (ftnlen)80, (ftnlen)10);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target orientation data available", (ftnlen)36);
    s_copy(abcorr, "None", (ftnlen)80, (ftnlen)4);
    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "ITRF93", (ftnlen)32, (ftnlen)6);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Reference frame not centered on target", (ftnlen)38);
    s_copy(abcorr, "None", (ftnlen)80, (ftnlen)4);
    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "Mars", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(INVALIDFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("No target ephemeris data available", (ftnlen)34);
    s_copy(abcorr, "None", (ftnlen)80, (ftnlen)4);
    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(fixref, "IAU_GASPRA", (ftnlen)32, (ftnlen)10);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("No observer ephemeris data available", (ftnlen)36);
    s_copy(abcorr, "None", (ftnlen)80, (ftnlen)4);
    s_copy(method, "Near point: ellipsoid", (ftnlen)80, (ftnlen)21);
    s_copy(target, "MOON", (ftnlen)36, (ftnlen)4);
    s_copy(obsrvr, "GASPRA", (ftnlen)36, (ftnlen)6);
    s_copy(fixref, "IAU_MOON", (ftnlen)32, (ftnlen)8);
    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
    str2et_(timstr, &et, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodn2c_(target, &trgid, &found, (ftnlen)36);
    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
    zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, state, (
	    ftnlen)80, (ftnlen)32, (ftnlen)80);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     We're going to loop over all aberration corrections and */
/*     computation methods. */

    for (nc = 1; nc <= 9; ++nc) {
	for (nm = 1; nm <= 2; ++nm) {
/*           Loop over the set of sample times. */

	    s_copy(timstr, "2008 MAY 1", (ftnlen)50, (ftnlen)10);
	    str2et_(timstr, &et0, (ftnlen)50);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delta = spd_() * 100;
	    for (ns = 1; ns <= 10; ++ns) {

/* --- Case: ------------------------------------------------------ */

		s_copy(abcorr, corr + ((i__1 = nc - 1) < 9 && 0 <= i__1 ? 
			i__1 : s_rnge("corr", i__1, "f_zzgfssob__", (ftnlen)
			492)) * 80, (ftnlen)80, (ftnlen)80);
		s_copy(method, meth + ((i__1 = nm - 1) < 2 && 0 <= i__1 ? 
			i__1 : s_rnge("meth", i__1, "f_zzgfssob__", (ftnlen)
			493)) * 80, (ftnlen)80, (ftnlen)80);
		s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
		s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
		s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
		s_copy(title, "sub-# point on #. #; #; #.", (ftnlen)80, (
			ftnlen)26);
		repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)36, (ftnlen)80);
		repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)36, (ftnlen)80);
		repmc_(title, "#", method, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)80, (ftnlen)80);
		repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)80, (ftnlen)80);
		et = et0 + (ns - 1) * delta;
		etcal_(&et, timstr, (ftnlen)50);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)50, (ftnlen)80);
		tcase_(title, (ftnlen)80);
		bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (
			ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		bodn2c_(target, &trgid, &found, (ftnlen)36);
		bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
		zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, 
			state, (ftnlen)80, (ftnlen)32, (ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Estimate the sub-point velocity numerically. */

		for (i__ = 1; i__ <= 2; ++i__) {
		    t = et + ((i__ << 1) - 3) * 10.;
		    subpnt_(method, target, &t, fixref, abcorr, obsrvr, &spnt[
			    (i__1 = i__ * 3 - 3) < 6 && 0 <= i__1 ? i__1 : 
			    s_rnge("spnt", i__1, "f_zzgfssob__", (ftnlen)530)]
			    , &trgepc, srfvec, (ftnlen)80, (ftnlen)36, (
			    ftnlen)32, (ftnlen)80, (ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
		qderiv_(&c__3, spnt, &spnt[3], &c_b130, xvel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*             Check the velocity from ZZGFSSOB. We expect an error of */
/*             less than 1 cm/s. */

		chckad_("Sub-point vel", &state[3], "~~", xvel, &c__3, &
			c_b136, ok, (ftnlen)13, (ftnlen)2);
		subpnt_(method, target, &et, fixref, abcorr, obsrvr, xpnt, &
			trgepc, srfvec, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
			ftnlen)80, (ftnlen)36);

/*             The tolerance we use depends on the aberration correction. */

		zzprscor_(abcorr, attblk, (ftnlen)80);
		if (attblk[3]) {
		    tol = 1e-10;
		} else if (attblk[1]) {
		    tol = 1e-8;
		} else {
		    tol = 1e-10;
		}
		chckad_("Sub-point", state, "~~/", xpnt, &c__3, &tol, ok, (
			ftnlen)9, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

		s_copy(abcorr, corr + ((i__1 = nc - 1) < 9 && 0 <= i__1 ? 
			i__1 : s_rnge("corr", i__1, "f_zzgfssob__", (ftnlen)
			574)) * 80, (ftnlen)80, (ftnlen)80);
		s_copy(method, meth + ((i__1 = nm - 1) < 2 && 0 <= i__1 ? 
			i__1 : s_rnge("meth", i__1, "f_zzgfssob__", (ftnlen)
			575)) * 80, (ftnlen)80, (ftnlen)80);
		s_copy(target, "JUPITER", (ftnlen)36, (ftnlen)7);
		s_copy(obsrvr, "IO", (ftnlen)36, (ftnlen)2);
		s_copy(fixref, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
		s_copy(title, "sub-# point on #. #; #; #.", (ftnlen)80, (
			ftnlen)26);
		repmc_(title, "#", obsrvr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)36, (ftnlen)80);
		repmc_(title, "#", target, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)36, (ftnlen)80);
		repmc_(title, "#", method, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)80, (ftnlen)80);
		repmc_(title, "#", abcorr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)80, (ftnlen)80);
		et = et0 + (ns - 1) * delta;
		etcal_(&et, timstr, (ftnlen)50);
		repmc_(title, "#", timstr, title, (ftnlen)80, (ftnlen)1, (
			ftnlen)50, (ftnlen)80);
		tcase_(title, (ftnlen)80);
		str2et_(timstr, &et, (ftnlen)50);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		bodvrd_(target, "RADII", &c__3, &n, radii, (ftnlen)36, (
			ftnlen)5);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		bodn2c_(target, &trgid, &found, (ftnlen)36);
		bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
		zzgfssob_(method, &trgid, &et, fixref, abcorr, &obsid, radii, 
			state, (ftnlen)80, (ftnlen)32, (ftnlen)80);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Estimate the sub-point velocity numerically. */

		for (i__ = 1; i__ <= 2; ++i__) {
		    t = et + ((i__ << 1) - 3) * 10.;
		    subpnt_(method, target, &t, fixref, abcorr, obsrvr, &spnt[
			    (i__1 = i__ * 3 - 3) < 6 && 0 <= i__1 ? i__1 : 
			    s_rnge("spnt", i__1, "f_zzgfssob__", (ftnlen)616)]
			    , &trgepc, srfvec, (ftnlen)80, (ftnlen)36, (
			    ftnlen)32, (ftnlen)80, (ftnlen)36);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}
		qderiv_(&c__3, spnt, &spnt[3], &c_b130, xvel);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*             Check the velocity from ZZGFSSOB. We expect an error of */
/*             less than 1 cm/s. */

		chckad_("Sub-point vel", &state[3], "~~", xvel, &c__3, &
			c_b136, ok, (ftnlen)13, (ftnlen)2);

/*             Make sure the position component of STATE matches the */
/*             result from SUBPNT. */

		subpnt_(method, target, &et, fixref, abcorr, obsrvr, xpnt, &
			trgepc, srfvec, (ftnlen)80, (ftnlen)36, (ftnlen)32, (
			ftnlen)80, (ftnlen)36);

/*             The tolerance we use depends on the aberration correction. */

		zzprscor_(abcorr, attblk, (ftnlen)80);
		if (attblk[3]) {
		    tol = 1e-10;
		} else if (attblk[1]) {
		    tol = 1e-8;
		} else {
		    tol = 1e-10;
		}
		chckad_("Sub-point", state, "~~/", xpnt, &c__3, &tol, ok, (
			ftnlen)9, (ftnlen)3);
	    }

/*           End of cases for the current epoch. */

	}

/*        End of cases for the current method. */

    }

/*     End of cases for the current aberration correction. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up:  delete kernels.", (ftnlen)26);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzgfssob.bsp", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzgfssob__ */

