/* f_zzgfwsts.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1000 = 1000;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__2 = 2;
static doublereal c_b26 = 1.;
static doublereal c_b27 = 2.;
static doublereal c_b30 = 3.;
static doublereal c_b31 = 4.;
static doublereal c_b34 = 0.;
static doublereal c_b35 = 5.;
static integer c__0 = 0;
static doublereal c_b115 = -2.;
static integer c__1 = 1;
static integer c__3 = 3;

/* $Procedure      F_ZZGFWSTS ( Test ZZGFWSTS ) */
/* Subroutine */ int f_zzgfwsts__(logical *ok)
{
    /* Initialized data */

    static char ops[2*4] = "()" "[]" "[)" "(]";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer card;
    static doublereal xwin[1006], step1, step2, wndw1[1006], wndw2[1006], 
	    wndw3[1006];
    static integer i__, j, n;
    extern /* Subroutine */ int zzgfwsts_(doublereal *, doublereal *, char *, 
	    doublereal *, ftnlen), tcase_(char *, ftnlen);
    static char qname[80];
    static integer xcard;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), copyd_(doublereal *, doublereal *), 
	    topen_(char *, ftnlen);
    static doublereal start, delta1, delta2;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     scardd_(integer *, doublereal *), chckxc_(logical *, char *, 
	    logical *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    extern integer wncard_(doublereal *);
    static doublereal finish;
    extern /* Subroutine */ int ssized_(integer *, doublereal *), wninsd_(
	    doublereal *, doublereal *, doublereal *);

/* $ Abstract */

/*     Test the GF window sift routine ZZGFWSTS. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the GF window sift routine ZZGFWSTS. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     Progress reporting and interrupt handling are not currently tested */
/*     in this version of this this test family, but test cases for these */
/*     features should be added. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 26-SEP-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZGFWSTS", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid inclusion operator", (ftnlen)26);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw1, wndw2, ") )", wndw3, (ftnlen)3);
    chckxc_(&c_true, "SPICE(UNKNOWNINCLUSION)", ok, (ftnlen)23);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Window overflow", (ftnlen)15);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__2, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b30, &c_b31, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b34, &c_b35, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw1, wndw2, "[]", wndw3, (ftnlen)2);
    chckxc_(&c_true, "SPICE(OUTOFROOM)", ok, (ftnlen)16);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("First window precedes second; one interval each.", (ftnlen)48);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b30, &c_b31, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	scardd_(&c__0, wndw3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)267)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Second window precedes first; one interval each.", (ftnlen)48);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b30, &c_b31, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	scardd_(&c__0, wndw3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)307)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("First window properly contained in second; one interval each.", (
	    ftnlen)61);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b115, &c_b30, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)351)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &c__1, &c__0, ok, (ftnlen)80, (ftnlen)1);
	s_copy(qname, "WNDW3 (I=#)", (ftnlen)80, (ftnlen)11);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (
		ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Second window properly contained in first; one interval each.", (
	    ftnlen)61);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b115, &c_b30, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw2, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)402)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*        We expect the sift result to be empty. */

	chcksi_(qname, &card, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("First window's endpoints match those of second; one interval eac"
	    "h.", (ftnlen)66);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)450)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);

/*        We have inclusion only if the second window's interval */
/*        is treated as being closed. */

	if (s_cmp(ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"ops", i__1, "f_zzgfwsts__", (ftnlen)460)) << 1), "[]", (
		ftnlen)2, (ftnlen)2) == 0) {
	    xcard = 1;
	} else {
	    xcard = 0;
	}
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &xcard, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (xcard > 0) {
	    s_copy(qname, "WNDW3 (I=#)", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("First window's left endpoint matches that of second; one interva"
	    "l each.", (ftnlen)71);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b30, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)514)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);

/*        We have inclusion only if the second window's interval */
/*        is treated as being closed on the left. */

	if (s_cmp(ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"ops", i__1, "f_zzgfwsts__", (ftnlen)524)) << 1), "[]", (
		ftnlen)2, (ftnlen)2) == 0) {
	    xcard = 1;
	} else if (s_cmp(ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)526)) << 1), 
		"[)", (ftnlen)2, (ftnlen)2) == 0) {
	    xcard = 1;
	} else {
	    xcard = 0;
	}
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &xcard, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (xcard > 0) {
	    s_copy(qname, "WNDW3 (I=#)", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("First window's right endpoint matches that of second; one interv"
	    "al each.", (ftnlen)72);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b27, &c_b30, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b26, &c_b30, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 4; ++i__) {
	zzgfwsts_(wndw1, wndw2, ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)580)) << 1)
		, wndw3, (ftnlen)2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	card = wncard_(wndw3);

/*        We have inclusion only if the second window's interval */
/*        is treated as being closed on the right. */

	if (s_cmp(ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"ops", i__1, "f_zzgfwsts__", (ftnlen)590)) << 1), "[]", (
		ftnlen)2, (ftnlen)2) == 0) {
	    xcard = 1;
	} else if (s_cmp(ops + (((i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("ops", i__1, "f_zzgfwsts__", (ftnlen)592)) << 1), 
		"(]", (ftnlen)2, (ftnlen)2) == 0) {
	    xcard = 1;
	} else {
	    xcard = 0;
	}
	s_copy(qname, "CARD (I=#)", (ftnlen)80, (ftnlen)10);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksi_(qname, &card, "=", &xcard, &c__0, ok, (ftnlen)80, (ftnlen)1);
	if (xcard > 0) {
	    s_copy(qname, "WNDW3 (I=#)", (ftnlen)80, (ftnlen)11);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (
		    ftnlen)1);
	}
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Multiple WNDW1 intervals in each WNDW2 interval.", (ftnlen)48);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step1 = 1.;
    delta1 = .2;
    step2 = 10.;
    delta2 = 4.;

/*     Create second window. */

    n = 20;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	start = i__ * step2 - delta2;
	finish = i__ * step2 + delta2;
	wninsd_(&start, &finish, wndw2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create first window. There are 7 small intervals */
/*     in each interval of the large window. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = -3; j <= 3; ++j) {
	    start = i__ * step2 + j * step1 - delta1;
	    finish = i__ * step2 + j * step1 + delta1;
	    wninsd_(&start, &finish, wndw1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    copyd_(wndw1, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw1, wndw2, "[]", wndw3, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    card = wncard_(wndw3);
    s_copy(qname, "CARD", (ftnlen)80, (ftnlen)4);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    i__1 = n * 7;
    chcksi_(qname, &card, "=", &i__1, &c__0, ok, (ftnlen)80, (ftnlen)1);
    s_copy(qname, "WNDW3", (ftnlen)80, (ftnlen)5);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (ftnlen)1)
	    ;

/* ---- Case ------------------------------------------------------------- */

    tcase_("WNDW2 intervals span multiple WNDW1 intervals.", (ftnlen)46);

/*     Use the input windows from the previous test. */

    zzgfwsts_(wndw2, wndw1, "[]", wndw3, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    card = wncard_(wndw3);
    s_copy(qname, "CARD", (ftnlen)80, (ftnlen)4);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chcksi_(qname, &card, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Multiple WNDW1 intervals in each WNDW2 interval; some intervals "
	    "not contained.", (ftnlen)78);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step1 = 1.;
    delta1 = .2;
    step2 = 10.;
    delta2 = 4.;

/*     Create second window. */

    n = 20;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	start = i__ * step2 - delta2;
	finish = i__ * step2 + delta2;
	wninsd_(&start, &finish, wndw2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create first window. There is a sequence of 9 small intervals */
/*     centered at the center of each interval of the large window; */
/*     the intervals at the ends of each of these sequences extend */
/*     beyond the corresponding interval of the second window. */

    ssized_(&c__1000, xwin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = -4; j <= 4; ++j) {
	    start = i__ * step2 + j * step1 - delta1;
	    finish = i__ * step2 + j * step1 + delta1;
	    wninsd_(&start, &finish, wndw1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (j > -4 || j < 4) {
		wninsd_(&start, &finish, xwin);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }
	}
    }
    zzgfwsts_(wndw1, wndw2, "[]", wndw3, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    card = wncard_(wndw3);
    s_copy(qname, "CARD", (ftnlen)80, (ftnlen)4);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    i__1 = n * 7;
    chcksi_(qname, &card, "=", &i__1, &c__0, ok, (ftnlen)80, (ftnlen)1);
    s_copy(qname, "WNDW3", (ftnlen)80, (ftnlen)5);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (ftnlen)1)
	    ;

/* ---- Case ------------------------------------------------------------- */

    tcase_("Multiple WNDW1 intervals between each WNDW2 interval.", (ftnlen)
	    53);
    ssized_(&c__1000, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    ssized_(&c__1000, wndw3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step1 = 1.;
    delta1 = .2;
    step2 = 10.;
    delta2 = 1.;

/*     Create second window. */

    n = 20;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	start = i__ * step2 - delta2;
	finish = i__ * step2 + delta2;
	wninsd_(&start, &finish, wndw2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create first window. There are 7 small intervals */
/*     between each interval of the large window. */

    i__1 = n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = -3; j <= 3; ++j) {
	    start = (i__ + .5) * step2 + j * step1 - delta1;
	    finish = (i__ + .5) * step2 + j * step1 + delta1;
	    wninsd_(&start, &finish, wndw1);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }
    zzgfwsts_(wndw1, wndw2, "[]", wndw3, (ftnlen)2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    card = wncard_(wndw3);
    s_copy(qname, "CARD", (ftnlen)80, (ftnlen)4);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chcksi_(qname, &card, "=", &c__0, &c__0, ok, (ftnlen)80, (ftnlen)1);
    s_copy(qname, "WNDW3", (ftnlen)80, (ftnlen)5);
    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
    chckad_(qname, wndw3, "=", xwin, &c__3, &c_b34, ok, (ftnlen)80, (ftnlen)1)
	    ;

/* ---- Case ------------------------------------------------------------- */

    tcase_("Valid inclusion operators with blanks", (ftnlen)37);
    wninsd_(&c_b26, &c_b27, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    wninsd_(&c_b115, &c_b30, wndw1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw2, wndw1, "  (   ) ", wndw3, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw2, wndw1, "  [   ] ", wndw3, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw2, wndw1, "  [   ) ", wndw3, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzgfwsts_(wndw2, wndw1, "  (   ] ", wndw3, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    return 0;
} /* f_zzgfwsts__ */

