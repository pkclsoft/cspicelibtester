/* f_zzhsc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__4 = 4;
static integer c__5003 = 5003;
static logical c_false = FALSE_;
static integer c__1 = 1;
static logical c_true = TRUE_;
static integer c__6 = 6;
static integer c__5004 = 5004;
static integer c_n1 = -1;
static integer c__5002 = 5002;

/* $Procedure F_ZZHSC ( Family of tests for ZZHSC ) */
/* Subroutine */ int f_zzhsc__(logical *ok)
{
    /* System generated locals */
    address a__1[4];
    integer i__1, i__2, i__3[4];
    char ch__1[7], ch__2[1], ch__3[10];

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    integer pids[5003];
    extern /* Subroutine */ int t_elapsd__(logical *, char *, char *, ftnlen, 
	    ftnlen), zzhscadd_(integer *, integer *, char *, char *, integer *
	    , logical *, ftnlen, ftnlen), zzhscchk_(integer *, integer *, 
	    char *, char *, integer *, ftnlen, ftnlen), zzhscinf_(integer *, 
	    integer *, char *, char *, integer *, ftnlen, ftnlen), zzhscini_(
	    integer *, integer *, integer *), zzhscavl_(integer *, integer *);
    integer i__, j, k;
    extern integer cardi_(integer *);
    integer avail;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), repmi_(char *, 
	    char *, integer *, char *, ftnlen, ftnlen, ftnlen);
    char items[32*5003], hword[32];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chcksc_(char *, char *, char *, char *, logical *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), chckxc_(logical *, char *, logical *, 
	    ftnlen), chcksi_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen);
    integer bltcod[620];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    char bltnam[36*620];
    integer hedlst[5003], itemat, hshval[5009], expfre;
    extern /* Subroutine */ int rmdupc_(integer *, char *, ftnlen), prefix_(
	    char *, integer *, char *, ftnlen, ftnlen);
    extern integer intmax_(void);
    integer collst[5009];
    extern /* Subroutine */ int ljucrs_(integer *, char *, char *, ftnlen, 
	    ftnlen), suffix_(char *, integer *, char *, ftnlen, ftnlen);
    char titems[32*5003];
    extern /* Subroutine */ int ssizei_(integer *, integer *);
    integer expusd;
    extern /* Subroutine */ int insrti_(integer *, integer *);
    logical report;
    extern integer zzhash2_(char *, integer *, ftnlen);
    integer ids[5003];
    logical new__;
    extern /* Subroutine */ int zzidmap_(integer *, char *, ftnlen);

/* $ Abstract */

/*     This routine tests add-only character hashes. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None */

/* $ Keywords */

/*   None */

/* $ Declarations */
/* $ Abstract */

/*     This include file lists the parameter collection */
/*     defining the number of SPICE ID -> NAME mappings. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     MAXL        is the maximum length of a body name. */

/*     MAXP        is the maximum number of additional names that may */
/*                 be added via the ZZBODDEF interface. */

/*     NPERM       is the count of the mapping assignments built into */
/*                 SPICE. */

/*     MAXE        is the size of the lists and hashes storing combined */
/*                 built-in and ZZBODDEF-defined name/ID mappings. To */
/*                 ensure efficient hashing this size is the set to the */
/*                 first prime number greater than ( MAXP + NPERM ). */

/*     NROOM       is the size of the lists and hashes storing the */
/*                 POOL-defined name/ID mappings. To ensure efficient */
/*                 hashing and to provide the ability to store nearly as */
/*                 many names as can fit in the POOL, this size is */
/*                 set to the first prime number less than MAXLIN */
/*                 defined in the POOL umbrella routine. */

/* $ Required_Reading */

/*     naif_ids.req */

/* $ Keywords */

/*     BODY */
/*     CONVERSION */

/* $ Author_and_Institution */

/*     B.V. Semenov (JPL) */
/*     E.D. Wright  (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 04-APR-2017 (BVS)(EDW) */

/*        Increased NROOM to 14983. Added a comment note explaining */
/*        NROOM and MAXE */

/* -    SPICELIB Version 1.0.0, 20-MAY-2010 (EDW) */

/*        N0064 version with MAXP = 150, NPERM = 563, */
/*        MAXE = MAXP + NPERM, and NROOM = 2000. */

/*     A script generates this file. Do not edit by hand. */
/*     Edit the creation script to modify the contents of */
/*     ZZBODTRN.INC. */


/*     Maximum size of a NAME string */


/*     Maximum number of additional names that may be added via the */
/*     ZZBODDEF interface. */


/*     Count of default SPICE mapping assignments. */


/*     Size of the lists and hashes storing the built-in and */
/*     ZZBODDEF-defined name/ID mappings. To ensure efficient hashing */
/*     this size is the set to the first prime number greater than */
/*     ( MAXP + NPERM ). */


/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing and to provide the ability */
/*     to store nearly as many names as can fit in the POOL, this size */
/*     is set to the first prime number less than MAXLIN defined in */
/*     the POOL umbrella routine. */

/* $ Brief_I/O */

/*   None */

/* $ Detailed_Input */

/*   None */

/* $ Detailed_Output */

/*   None */

/* $ Parameters */

/*   None */

/* $ Exceptions */

/*   None */

/* $ Files */

/*   None */

/* $ Particulars */

/*   None */

/* $ Examples */

/*   None */

/* $ Restrictions */

/*   None */

/* $ Literature_References */

/*   None */

/* $ Author_and_Institution */

/*   None */

/* $ Version */

/* -    Version 1.0.0  28-FEB-2014 (BVS) */

/* -& */
/* $ Index_Entries */

/*   None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */

/*      DOUBLE PRECISION      T_RANDD */

/*     Local parameters */

/*      PARAMETER           ( HASHSZ = 26003 ) */

/*     Local Variables */

/*      INTEGER               SEED */

/*     Set timing reporting. Set to .FALSE. to suppress screen output. */

/*      REPORT = .TRUE. */
    report = FALSE_;

/*     Begin every test family with an open call. */

    topen_("F_ZZHSC", (ftnlen)7);

/*     Initialize items and IDs -- random values. */

/*      SEED = -1 */

/*      DO I = 1, HASHSZ */

/*         IDS(I) = I * 1000 */

/*         TITEMS(I) = ' ' */

/*         DO J = 1, NINT( T_RANDD( 2.D0, 20.D0, SEED ) ) */

/*            K = NINT( T_RANDD( 2.D0, 8.D0, SEED ) ) */

/*            CALL SUFFIX( CHAR( MOD( I*K, 94 ) + 33 ), 0, TITEMS(I) ) */

/*         END DO */

/*         HWORD = '_#' */
/*         CALL REPMI ( HWORD, '#', I, HWORD ) */
/*         CALL SUFFIX( HWORD, 0, TITEMS(I) ) */

/*      END DO */

/*     Initialize items and IDs -- NAIF name like values. */

/*     First save all names built into the toolkit and a few */
/*     names based on each of them. */

    zzidmap_(bltcod, bltnam, (ftnlen)36);
    j = 1;
    for (i__ = 1; i__ <= 620; ++i__) {
	if (bltcod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge("bltc"
		"od", i__1, "f_zzhsc__", (ftnlen)219)] > 0 && j <= 4994) {
	    ljucrs_(&c__0, bltnam + ((i__1 = i__ - 1) < 620 && 0 <= i__1 ? 
		    i__1 : s_rnge("bltnam", i__1, "f_zzhsc__", (ftnlen)220)) *
		     36, titems + (((i__2 = j - 1) < 5003 && 0 <= i__2 ? i__2 
		    : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)220)) << 5),
		     (ftnlen)36, (ftnlen)32);
	    repmc_("IAU_#", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    221)) << 5), titems + (((i__2 = j) < 5003 && 0 <= i__2 ? 
		    i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)221)) 
		    << 5), (ftnlen)5, (ftnlen)1, (ftnlen)32, (ftnlen)32);
	    repmc_("#_FIXED", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    222)) << 5), titems + (((i__2 = j + 1) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    222)) << 5), (ftnlen)7, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_TOPO", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    223)) << 5), titems + (((i__2 = j + 2) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    223)) << 5), (ftnlen)6, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_1_SITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    224)) << 5), titems + (((i__2 = j + 3) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    224)) << 5), (ftnlen)8, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_2_SITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    225)) << 5), titems + (((i__2 = j + 4) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    225)) << 5), (ftnlen)8, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_3_SITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    226)) << 5), titems + (((i__2 = j + 5) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    226)) << 5), (ftnlen)8, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_ASITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    227)) << 5), titems + (((i__2 = j + 6) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    227)) << 5), (ftnlen)7, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_BSITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    228)) << 5), titems + (((i__2 = j + 7) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    228)) << 5), (ftnlen)7, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    repmc_("#_CSITE", "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)
		    229)) << 5), titems + (((i__2 = j + 8) < 5003 && 0 <= 
		    i__2 ? i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)
		    229)) << 5), (ftnlen)7, (ftnlen)1, (ftnlen)32, (ftnlen)32)
		    ;
	    j += 10;
	}
	if (bltcod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge("bltc"
		"od", i__1, "f_zzhsc__", (ftnlen)232)] < 0 && j <= 4964) {
	    ljucrs_(&c__0, bltnam + ((i__1 = i__ - 1) < 620 && 0 <= i__1 ? 
		    i__1 : s_rnge("bltnam", i__1, "f_zzhsc__", (ftnlen)233)) *
		     36, titems + (((i__2 = j - 1) < 5003 && 0 <= i__2 ? i__2 
		    : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)233)) << 5),
		     (ftnlen)36, (ftnlen)32);
	    for (k = 1; k <= 9; ++k) {
/* Writing concatenation */
		i__3[0] = 2, a__1[0] = "#_";
		*(unsigned char *)&ch__2[0] = 'A' + k - 1;
		i__3[1] = 1, a__1[1] = ch__2;
		i__3[2] = 1, a__1[2] = "_";
		i__3[3] = 3, a__1[3] = "CAM";
		s_cat(ch__1, a__1, i__3, &c__4, (ftnlen)7);
		repmc_(ch__1, "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
			i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
			ftnlen)235)) << 5), titems + (((i__2 = j + k - 1) < 
			5003 && 0 <= i__2 ? i__2 : s_rnge("titems", i__2, 
			"f_zzhsc__", (ftnlen)235)) << 5), (ftnlen)7, (ftnlen)
			1, (ftnlen)32, (ftnlen)32);
	    }
	    for (k = 10; k <= 19; ++k) {
/* Writing concatenation */
		i__3[0] = 2, a__1[0] = "#_";
		*(unsigned char *)&ch__2[0] = 'A' + k - 1;
		i__3[1] = 1, a__1[1] = ch__2;
		i__3[2] = 1, a__1[2] = "_";
		i__3[3] = 3, a__1[3] = "SPE";
		s_cat(ch__1, a__1, i__3, &c__4, (ftnlen)7);
		repmc_(ch__1, "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
			i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
			ftnlen)239)) << 5), titems + (((i__2 = j + k - 1) < 
			5003 && 0 <= i__2 ? i__2 : s_rnge("titems", i__2, 
			"f_zzhsc__", (ftnlen)239)) << 5), (ftnlen)7, (ftnlen)
			1, (ftnlen)32, (ftnlen)32);
	    }
	    for (k = 20; k <= 29; ++k) {
/* Writing concatenation */
		i__3[0] = 2, a__1[0] = "#_";
		*(unsigned char *)&ch__2[0] = 'A' + k - 20;
		i__3[1] = 1, a__1[1] = ch__2;
		i__3[2] = 1, a__1[2] = "_";
		i__3[3] = 6, a__1[3] = "DETECT";
		s_cat(ch__3, a__1, i__3, &c__4, (ftnlen)10);
		repmc_(ch__3, "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
			i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
			ftnlen)243)) << 5), titems + (((i__2 = j + k - 1) < 
			5003 && 0 <= i__2 ? i__2 : s_rnge("titems", i__2, 
			"f_zzhsc__", (ftnlen)243)) << 5), (ftnlen)10, (ftnlen)
			1, (ftnlen)32, (ftnlen)32);
	    }
	    for (k = 30; k <= 39; ++k) {
/* Writing concatenation */
		i__3[0] = 2, a__1[0] = "#_";
		*(unsigned char *)&ch__2[0] = 'A' + k - 20;
		i__3[1] = 1, a__1[1] = ch__2;
		i__3[2] = 1, a__1[2] = "_";
		i__3[3] = 6, a__1[3] = "SENSOR";
		s_cat(ch__3, a__1, i__3, &c__4, (ftnlen)10);
		repmc_(ch__3, "#", titems + (((i__1 = j - 1) < 5003 && 0 <= 
			i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
			ftnlen)247)) << 5), titems + (((i__2 = j + k - 1) < 
			5003 && 0 <= i__2 ? i__2 : s_rnge("titems", i__2, 
			"f_zzhsc__", (ftnlen)247)) << 5), (ftnlen)10, (ftnlen)
			1, (ftnlen)32, (ftnlen)32);
	    }
	    j += 40;
	}
    }

/*     Eliminate duplicate values. */

    k = j - 1;
    rmdupc_(&k, titems, (ftnlen)32);
    j = k + 1;

/*     Finish by adding some asteroid names at the end to fill up the */
/*     hash. */

    for (i__ = j; i__ <= 5003; ++i__) {
	i__2 = i__ + 2500000;
	repmi_("ASTEROID_#", "#", &i__2, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)266)) << 5), (ftnlen)10, (ftnlen)1, (ftnlen)32);
    }
    for (i__ = 1; i__ <= 5003; ++i__) {
	ids[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge("ids", i__1, 
		"f_zzhsc__", (ftnlen)270)] = i__ * 1000;
    }

/*     Calculate hash values for all names in the buffer and store them */
/*     in a set to get the expected numbers of occupied and free head */
/*     nodes. */

    ssizei_(&c__5003, hshval);
    for (i__ = 1; i__ <= 5003; ++i__) {
	j = zzhash2_(titems + (((i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : 
		s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)280)) << 5), &
		c__5003, (ftnlen)32);
	insrti_(&j, hshval);
    }
    expusd = cardi_(hshval);
    expfre = 5003 - expusd;

/*     Initialize hash. */

    tcase_("Initialize hash.", (ftnlen)16);
    zzhscini_(&c__5003, hedlst, collst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("SIZE", &collst[5], "=", &c__5003, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    for (i__ = 1; i__ <= 5003; ++i__) {
	chcksi_("HEDLST(I)", &hedlst[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? 
		i__1 : s_rnge("hedlst", i__1, "f_zzhsc__", (ftnlen)298)], 
		"=", &c__0, &c__0, ok, (ftnlen)9, (ftnlen)1);
    }

/*     Populate hash. */

    tcase_("Populate hash.", (ftnlen)14);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhscadd_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)308)) << 5), &itemat, &new__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("NEW", &new__, &c_true, ok, (ftnlen)3);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksc_("ITEMS(ITEMAT)", items + (((i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsc__", (ftnlen)314))
		 << 5), "=", titems + (((i__2 = i__ - 1) < 5003 && 0 <= i__2 ?
		 i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)314)) << 
		5), ok, (ftnlen)13, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	i__1 = i__ + 1;
	chcksi_("FIRST", &collst[4], "=", &i__1, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    pids[(i__1 = itemat - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge(
		    "pids", i__1, "f_zzhsc__", (ftnlen)319)] = ids[(i__2 = 
		    i__ - 1) < 5003 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2, 
		    "f_zzhsc__", (ftnlen)319)];
	}
	zzhscavl_(collst, &avail);
	i__1 = 5003 - i__;
	chcksi_("AVAIL", &avail, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Get information about hash. */

    tcase_("Get info about the hash.", (ftnlen)24);
    zzhscinf_(hedlst, collst, items, "HASH SIZE", &i__, (ftnlen)32, (ftnlen)9)
	    ;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HASH SIZE", &i__, "=", &c__5003, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    zzhscinf_(hedlst, collst, items, "USED HEADNODE COUNT", &i__, (ftnlen)32, 
	    (ftnlen)19);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USED HEADNODE COUNT", &i__, "=", &expusd, &c__0, ok, (ftnlen)19, 
	    (ftnlen)1);
    zzhscinf_(hedlst, collst, items, "UNUSED HEADNODE COUNT", &i__, (ftnlen)
	    32, (ftnlen)21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("UNUSED HEADNODE COUNT", &i__, "=", &expfre, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);
    zzhscinf_(hedlst, collst, items, "USED ITEM COUNT", &i__, (ftnlen)32, (
	    ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USED ITEM COUNT", &i__, "=", &c__5003, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    zzhscinf_(hedlst, collst, items, "UNUSED ITEM COUNT", &i__, (ftnlen)32, (
	    ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("UNUSED ITEM COUNT", &i__, "=", &c__0, &c__0, ok, (ftnlen)17, (
	    ftnlen)1);
    zzhscinf_(hedlst, collst, items, "LONGEST LIST SIZE", &i__, (ftnlen)32, (
	    ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("LONGEST LIST SIZE", &i__, "=", &c__6, &c__0, ok, (ftnlen)17, (
	    ftnlen)1);
    zzhscinf_(hedlst, collst, items, "Hakuna-Matata!", &i__, (ftnlen)32, (
	    ftnlen)14);
    chckxc_(&c_true, "SPICE(ITEMNOTRECOGNIZED)", ok, (ftnlen)24);
    chcksi_(" ", &i__, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Add the same items again. */

    tcase_("Add the same items again.", (ftnlen)25);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhscadd_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)367)) << 5), &itemat, &new__, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("NEW", &new__, &c_false, ok, (ftnlen)3);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksc_("ITEMS(ITEMAT)", items + (((i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsc__", (ftnlen)373))
		 << 5), "=", titems + (((i__2 = i__ - 1) < 5003 && 0 <= i__2 ?
		 i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)373)) << 
		5), ok, (ftnlen)13, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    chcksi_("PIDS(ITEMAT)", &pids[(i__1 = itemat - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("pids", i__1, "f_zzhsc__", (ftnlen)
		    378)], "=", &ids[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? 
		    i__2 : s_rnge("ids", i__2, "f_zzhsc__", (ftnlen)378)], &
		    c__0, ok, (ftnlen)12, (ftnlen)1);
	}
	zzhscavl_(collst, &avail);
	chcksi_("AVAIL", &avail, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Check that items are in the hash. */

    tcase_("Check that items are in the hash.", (ftnlen)33);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhscchk_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)394)) << 5), &itemat, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksc_("ITEMS(ITEMAT)", items + (((i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsc__", (ftnlen)399))
		 << 5), "=", titems + (((i__2 = i__ - 1) < 5003 && 0 <= i__2 ?
		 i__2 : s_rnge("titems", i__2, "f_zzhsc__", (ftnlen)399)) << 
		5), ok, (ftnlen)13, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    chcksi_("PIDS(ITEMAT)", &pids[(i__1 = itemat - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("pids", i__1, "f_zzhsc__", (ftnlen)
		    404)], "=", &ids[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? 
		    i__2 : s_rnge("ids", i__2, "f_zzhsc__", (ftnlen)404)], &
		    c__0, ok, (ftnlen)12, (ftnlen)1);
	}
    }

/*     Check that non-hashed items are not in the hash. */

    tcase_("Check that other items are not in the hash.", (ftnlen)43);
    for (i__ = 5003; i__ >= 1; --i__) {
	s_copy(hword, titems + (((i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 :
		 s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)417)) << 5), (
		ftnlen)32, (ftnlen)32);
	prefix_("z", &c__0, hword, (ftnlen)1, (ftnlen)32);
	zzhscchk_(hedlst, collst, items, hword, &itemat, (ftnlen)32, (ftnlen)
		32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
    }

/*     Exception: try to add one more item to the hash. */

    tcase_("Exception: hash overflow.", (ftnlen)25);
    zzhscadd_(hedlst, collst, items, hword, &itemat, &new__, (ftnlen)32, (
	    ftnlen)32);
    chckxc_(&c_true, "SPICE(HASHISFULL)", ok, (ftnlen)17);
    chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (ftnlen)
	    1);
    chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Exception: reset hash with negative count. */

    tcase_("Exception: reset with non-positive size.", (ftnlen)40);
    zzhscini_(&c_n1, hedlst, collst);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("SIZE", &collst[5], "=", &c__5003, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (ftnlen)
	    1);

/*     Exception: reset hash with very big count. */

    tcase_("Exception: reset with too large size.", (ftnlen)37);
    i__1 = intmax_();
    zzhscini_(&i__1, hedlst, collst);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("SIZE", &collst[5], "=", &c__5003, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (ftnlen)
	    1);

/*     Reset hash. */

    tcase_("Reset hash.", (ftnlen)11);
    zzhscini_(&c__5002, hedlst, collst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("SIZE", &collst[5], "=", &c__5002, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    for (i__ = 1; i__ <= 5002; ++i__) {
	chcksi_("HEDLST(I)", &hedlst[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? 
		i__1 : s_rnge("hedlst", i__1, "f_zzhsc__", (ftnlen)474)], 
		"=", &c__0, &c__0, ok, (ftnlen)9, (ftnlen)1);
    }
    zzhscavl_(collst, &avail);
    chcksi_("AVAIL", &avail, "=", &c__5002, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check that items are not in the empty hash. */

    tcase_("Check that items are not in empty hash.", (ftnlen)39);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhscchk_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)487)) << 5), &itemat, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
    }

/*     Timing test. */

    tcase_("Timing test.", (ftnlen)12);
    t_elapsd__(&c_false, "no report on first call", "running", (ftnlen)23, (
	    ftnlen)7);
    zzhscini_(&c__5003, hedlst, collst);
    t_elapsd__(&report, "initializing hash", "running", (ftnlen)17, (ftnlen)7)
	    ;
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhscadd_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)507)) << 5), &itemat, &new__, (ftnlen)32, (ftnlen)32);
    }
    t_elapsd__(&report, "populating hash", "running", (ftnlen)15, (ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhscadd_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)514)) << 5), &itemat, &new__, (ftnlen)32, (ftnlen)32);
    }
    t_elapsd__(&report, "adding same items to hash", "running", (ftnlen)25, (
	    ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhscchk_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)521)) << 5), &itemat, (ftnlen)32, (ftnlen)32);
    }
    t_elapsd__(&report, "checking presence in hash", "running", (ftnlen)25, (
	    ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	s_copy(hword, titems + (((i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 :
		 s_rnge("titems", i__1, "f_zzhsc__", (ftnlen)528)) << 5), (
		ftnlen)32, (ftnlen)32);
	suffix_("z", &c__0, hword, (ftnlen)1, (ftnlen)32);
	zzhscchk_(hedlst, collst, items, hword, &itemat, (ftnlen)32, (ftnlen)
		32);
    }
    t_elapsd__(&report, "checking items not in hash", "running", (ftnlen)26, (
	    ftnlen)7);
    zzhscini_(&c__5002, hedlst, collst);
    t_elapsd__(&report, "resetting hash", "running", (ftnlen)14, (ftnlen)7);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhscchk_(hedlst, collst, items, titems + (((i__1 = i__ - 1) < 5003 &&
		 0 <= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsc__", (
		ftnlen)541)) << 5), &itemat, (ftnlen)32, (ftnlen)32);
    }
    t_elapsd__(&report, "checking items in empty hash", "running", (ftnlen)28,
	     (ftnlen)7);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_zzhsc__ */

