/* f_zzhsi.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__5003 = 5003;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__1 = 1;
static logical c_true = TRUE_;
static integer c__4 = 4;
static integer c__5004 = 5004;
static integer c_n1 = -1;
static integer c__5002 = 5002;

/* $Procedure F_ZZHSI ( Family of tests for ZZHSI ) */
/* Subroutine */ int f_zzhsi__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer pids[5003], hint;
    extern /* Subroutine */ int t_elapsd__(logical *, char *, char *, ftnlen, 
	    ftnlen), zzhsiadd_(integer *, integer *, integer *, integer *, 
	    integer *, logical *), zzhsichk_(integer *, integer *, integer *, 
	    integer *, integer *), zzhsiinf_(integer *, integer *, integer *, 
	    char *, integer *, ftnlen), zzhsiini_(integer *, integer *, 
	    integer *), zzhsiavl_(integer *, integer *);
    integer i__, j, k;
    extern integer cardi_(integer *);
    integer avail;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    integer items[5003];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    , chckxc_(logical *, char *, logical *, ftnlen), chcksi_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    integer bltcod[620];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    char bltnam[36*620];
    integer hedlst[5003], itemat, hshval[5009], expfre;
    extern integer intmax_(void);
    integer collst[5009], titems[5003];
    extern /* Subroutine */ int ssizei_(integer *, integer *);
    integer expusd;
    extern /* Subroutine */ int insrti_(integer *, integer *);
    logical report;
    integer ids[5003];
    logical new__;
    extern /* Subroutine */ int zzidmap_(integer *, char *, ftnlen);
    extern integer zzhashi_(integer *, integer *);

/* $ Abstract */

/*     This routine tests add-only integer hashes. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*   None */

/* $ Keywords */

/*   None */

/* $ Declarations */
/* $ Abstract */

/*     This include file lists the parameter collection */
/*     defining the number of SPICE ID -> NAME mappings. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     MAXL        is the maximum length of a body name. */

/*     MAXP        is the maximum number of additional names that may */
/*                 be added via the ZZBODDEF interface. */

/*     NPERM       is the count of the mapping assignments built into */
/*                 SPICE. */

/*     MAXE        is the size of the lists and hashes storing combined */
/*                 built-in and ZZBODDEF-defined name/ID mappings. To */
/*                 ensure efficient hashing this size is the set to the */
/*                 first prime number greater than ( MAXP + NPERM ). */

/*     NROOM       is the size of the lists and hashes storing the */
/*                 POOL-defined name/ID mappings. To ensure efficient */
/*                 hashing and to provide the ability to store nearly as */
/*                 many names as can fit in the POOL, this size is */
/*                 set to the first prime number less than MAXLIN */
/*                 defined in the POOL umbrella routine. */

/* $ Required_Reading */

/*     naif_ids.req */

/* $ Keywords */

/*     BODY */
/*     CONVERSION */

/* $ Author_and_Institution */

/*     B.V. Semenov (JPL) */
/*     E.D. Wright  (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 04-APR-2017 (BVS)(EDW) */

/*        Increased NROOM to 14983. Added a comment note explaining */
/*        NROOM and MAXE */

/* -    SPICELIB Version 1.0.0, 20-MAY-2010 (EDW) */

/*        N0064 version with MAXP = 150, NPERM = 563, */
/*        MAXE = MAXP + NPERM, and NROOM = 2000. */

/*     A script generates this file. Do not edit by hand. */
/*     Edit the creation script to modify the contents of */
/*     ZZBODTRN.INC. */


/*     Maximum size of a NAME string */


/*     Maximum number of additional names that may be added via the */
/*     ZZBODDEF interface. */


/*     Count of default SPICE mapping assignments. */


/*     Size of the lists and hashes storing the built-in and */
/*     ZZBODDEF-defined name/ID mappings. To ensure efficient hashing */
/*     this size is the set to the first prime number greater than */
/*     ( MAXP + NPERM ). */


/*     Size of the lists and hashes storing the POOL-defined name/ID */
/*     mappings. To ensure efficient hashing and to provide the ability */
/*     to store nearly as many names as can fit in the POOL, this size */
/*     is set to the first prime number less than MAXLIN defined in */
/*     the POOL umbrella routine. */

/* $ Brief_I/O */

/*   None */

/* $ Detailed_Input */

/*   None */

/* $ Detailed_Output */

/*   None */

/* $ Parameters */

/*   None */

/* $ Exceptions */

/*   None */

/* $ Files */

/*   None */

/* $ Particulars */

/*   None */

/* $ Examples */

/*   None */

/* $ Restrictions */

/*   None */

/* $ Literature_References */

/*   None */

/* $ Author_and_Institution */

/*   None */

/* $ Version */

/* -    Version 1.1.0  01-FEB-2016 (BVS) */

/*        Updated LONGEST LIST SIZE in test case 3 (4 -> 5). */

/* -    Version 1.0.0  28-FEB-2014 (BVS) */

/* -& */
/* $ Index_Entries */

/*   None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */

/*      DOUBLE PRECISION      T_RANDD */

/*     Local Parameters */

/*      PARAMETER           ( HASHSZ = 26003 ) */

/*     Local Variables */

/*      INTEGER               SEED */

/*     Set timing reporting. Set to .FALSE. to suppress screen output. */

/*      REPORT = .TRUE. */
    report = FALSE_;

/*     Begin every test family with an open call. */

    topen_("F_ZZHSI", (ftnlen)7);

/*     Initialize items and IDs - random values. */

/*      SEED = -1 */

/*      DO I = 1, HASHSZ */

/*         IDS(I) = I * 1000 */

/*         TITEMS(I) = NINT( T_RANDD( 1.D0, 9.D0, SEED ) ) + 10 * I */

/*         IF ( MOD( TITEMS(I), 2 ) .EQ. 0 ) THEN */
/*            TITEMS(I) = -TITEMS(I) */
/*         END IF */

/*      END DO */

/*     Initialize items and IDs -- NAIF ID like values. */

/*     First save all unique IDs built into the toolkit. */

    zzidmap_(bltcod, bltnam, (ftnlen)36);
    i__ = 1;
    j = 1;
    while(i__ <= 620 && j <= 5003) {
	if (i__ > 1) {
	    if (bltcod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge(
		    "bltcod", i__1, "f_zzhsi__", (ftnlen)212)] != bltcod[(
		    i__2 = i__ - 2) < 620 && 0 <= i__2 ? i__2 : s_rnge("bltc"
		    "od", i__2, "f_zzhsi__", (ftnlen)212)]) {
		titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge(
			"titems", i__1, "f_zzhsi__", (ftnlen)213)] = bltcod[(
			i__2 = i__ - 1) < 620 && 0 <= i__2 ? i__2 : s_rnge(
			"bltcod", i__2, "f_zzhsi__", (ftnlen)213)];
		++j;
	    }
	} else {
	    titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge("titems"
		    , i__1, "f_zzhsi__", (ftnlen)217)] = bltcod[(i__2 = i__ - 
		    1) < 620 && 0 <= i__2 ? i__2 : s_rnge("bltcod", i__2, 
		    "f_zzhsi__", (ftnlen)217)];
	    ++j;
	}
	++i__;
    }

/*     Then save 63 extra IDs based on each unique each s/c ID. */

    if (j < 5003) {
	for (i__ = 2; i__ <= 620; ++i__) {
	    if (bltcod[(i__1 = i__ - 1) < 620 && 0 <= i__1 ? i__1 : s_rnge(
		    "bltcod", i__1, "f_zzhsi__", (ftnlen)228)] < 0 && bltcod[(
		    i__2 = i__ - 1) < 620 && 0 <= i__2 ? i__2 : s_rnge("bltc"
		    "od", i__2, "f_zzhsi__", (ftnlen)228)] != bltcod[(i__3 = 
		    i__ - 2) < 620 && 0 <= i__3 ? i__3 : s_rnge("bltcod", 
		    i__3, "f_zzhsi__", (ftnlen)228)]) {
		for (k = 1; k <= 9; ++k) {
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				232)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)232)] * 1000 - k * 100;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				236)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)236)] * 1000 - k * 100 - 1;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				240)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)240)] * 1000 - k * 100 - 2;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				244)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)244)] * 1000 - k * 100 - 5;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				248)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)248)] * 1000 - k * 100 - 10;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				252)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)252)] * 1000 - k * 100 - 11;
			++j;
		    }
		    if (j <= 5003) {
			titems[(i__1 = j - 1) < 5003 && 0 <= i__1 ? i__1 : 
				s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
				256)] = bltcod[(i__2 = i__ - 1) < 620 && 0 <= 
				i__2 ? i__2 : s_rnge("bltcod", i__2, "f_zzhs"
				"i__", (ftnlen)256)] * 1000 - k * 100 - 12;
			++j;
		    }
		}
	    }
	}
    }

/*     Then add some asteroid IDs at the end to fill up the hash. */

    for (i__ = j; i__ <= 5003; ++i__) {
	titems[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge("titems", 
		i__1, "f_zzhsi__", (ftnlen)268)] = i__ + 2500000;
    }
    for (i__ = 1; i__ <= 5003; ++i__) {
	ids[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge("ids", i__1, 
		"f_zzhsi__", (ftnlen)272)] = i__ * 1000;
    }

/*     Calculate hash values for all IDs in the buffer and store them in */
/*     a set to get the expected numbers of occupied and free head */
/*     nodes. */

    ssizei_(&c__5003, hshval);
    for (i__ = 1; i__ <= 5003; ++i__) {
	j = zzhashi_(&titems[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : 
		s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)282)], &c__5003);
	insrti_(&j, hshval);
    }
    expusd = cardi_(hshval);
    expfre = 5003 - expusd;

/*     Initialize hash. */

    tcase_("Initialize hash.", (ftnlen)16);
    zzhsiini_(&c__5003, hedlst, collst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("SIZE", &collst[5], "=", &c__5003, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    for (i__ = 1; i__ <= 5003; ++i__) {
	chcksi_("HEDLST(I)", &hedlst[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? 
		i__1 : s_rnge("hedlst", i__1, "f_zzhsi__", (ftnlen)300)], 
		"=", &c__0, &c__0, ok, (ftnlen)9, (ftnlen)1);
    }

/*     Populate hash. */

    tcase_("Populate hash.", (ftnlen)14);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhsiadd_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		310)], &itemat, &new__);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("NEW", &new__, &c_true, ok, (ftnlen)3);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksi_("ITEMS(ITEMAT)", &items[(i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsi__", (ftnlen)316)]
		, "=", &titems[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? i__2 : 
		s_rnge("titems", i__2, "f_zzhsi__", (ftnlen)316)], &c__0, ok, 
		(ftnlen)13, (ftnlen)1);
	i__1 = i__ + 1;
	chcksi_("FIRST", &collst[4], "=", &i__1, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    pids[(i__1 = itemat - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge(
		    "pids", i__1, "f_zzhsi__", (ftnlen)321)] = ids[(i__2 = 
		    i__ - 1) < 5003 && 0 <= i__2 ? i__2 : s_rnge("ids", i__2, 
		    "f_zzhsi__", (ftnlen)321)];
	}
	zzhsiavl_(collst, &avail);
	i__1 = 5003 - i__;
	chcksi_("AVAIL", &avail, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Get information about hash. */

    tcase_("Get info about the hash.", (ftnlen)24);
    zzhsiinf_(hedlst, collst, items, "HASH SIZE", &i__, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("HASH SIZE", &i__, "=", &c__5003, &c__0, ok, (ftnlen)9, (ftnlen)1)
	    ;
    zzhsiinf_(hedlst, collst, items, "USED HEADNODE COUNT", &i__, (ftnlen)19);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USED HEADNODE COUNT", &i__, "=", &expusd, &c__0, ok, (ftnlen)19, 
	    (ftnlen)1);
    zzhsiinf_(hedlst, collst, items, "UNUSED HEADNODE COUNT", &i__, (ftnlen)
	    21);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("UNUSED HEADNODE COUNT", &i__, "=", &expfre, &c__0, ok, (ftnlen)
	    21, (ftnlen)1);
    zzhsiinf_(hedlst, collst, items, "USED ITEM COUNT", &i__, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USED ITEM COUNT", &i__, "=", &c__5003, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    zzhsiinf_(hedlst, collst, items, "UNUSED ITEM COUNT", &i__, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("UNUSED ITEM COUNT", &i__, "=", &c__0, &c__0, ok, (ftnlen)17, (
	    ftnlen)1);

/*     Note that the expected value in the LONGEST LIST SIZE check below */
/*     depend on IDs in the built-in name/ID mapping, which is likely to */
/*     change in each new toolkit version. Such change may or may not */
/*     result in a different expected value. If it does, it's not an */
/*     error but it means that this check must be updated. */

    zzhsiinf_(hedlst, collst, items, "LONGEST LIST SIZE", &i__, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("LONGEST LIST SIZE", &i__, "=", &c__4, &c__0, ok, (ftnlen)17, (
	    ftnlen)1);
    zzhsiinf_(hedlst, collst, items, "Hakuna-Matata!", &i__, (ftnlen)14);
    chckxc_(&c_true, "SPICE(ITEMNOTRECOGNIZED)", ok, (ftnlen)24);
    chcksi_(" ", &i__, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Add the same items again. */

    tcase_("Add the same items again.", (ftnlen)25);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhsiadd_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		376)], &itemat, &new__);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("NEW", &new__, &c_false, ok, (ftnlen)3);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksi_("ITEMS(ITEMAT)", &items[(i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsi__", (ftnlen)382)]
		, "=", &titems[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? i__2 : 
		s_rnge("titems", i__2, "f_zzhsi__", (ftnlen)382)], &c__0, ok, 
		(ftnlen)13, (ftnlen)1);
	chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    chcksi_("PIDS(ITEMAT)", &pids[(i__1 = itemat - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("pids", i__1, "f_zzhsi__", (ftnlen)
		    387)], "=", &ids[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? 
		    i__2 : s_rnge("ids", i__2, "f_zzhsi__", (ftnlen)387)], &
		    c__0, ok, (ftnlen)12, (ftnlen)1);
	}
	zzhsiavl_(collst, &avail);
	chcksi_("AVAIL", &avail, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);
    }

/*     Check that items are in the hash. */

    tcase_("Check that items are in the hash.", (ftnlen)33);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhsichk_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		403)], &itemat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &i__, &c__0, ok, (ftnlen)6, (ftnlen)1)
		;
	chcksi_("ITEMS(ITEMAT)", &items[(i__1 = itemat - 1) < 5003 && 0 <= 
		i__1 ? i__1 : s_rnge("items", i__1, "f_zzhsi__", (ftnlen)408)]
		, "=", &titems[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? i__2 : 
		s_rnge("titems", i__2, "f_zzhsi__", (ftnlen)408)], &c__0, ok, 
		(ftnlen)13, (ftnlen)1);
	chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (
		ftnlen)1);
	if (itemat == i__) {
	    chcksi_("PIDS(ITEMAT)", &pids[(i__1 = itemat - 1) < 5003 && 0 <= 
		    i__1 ? i__1 : s_rnge("pids", i__1, "f_zzhsi__", (ftnlen)
		    413)], "=", &ids[(i__2 = i__ - 1) < 5003 && 0 <= i__2 ? 
		    i__2 : s_rnge("ids", i__2, "f_zzhsi__", (ftnlen)413)], &
		    c__0, ok, (ftnlen)12, (ftnlen)1);
	}
    }

/*     Check that non-hashed items are not in the hash. */

    tcase_("Check that other items are not in the hash.", (ftnlen)43);
    for (i__ = 5003; i__ >= 1; --i__) {
	hint = titems[5002] + intmax_() / 2;
	zzhsichk_(hedlst, collst, items, &hint, &itemat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
    }

/*     Exception: try to add one more item to the hash. */

    tcase_("Exception: hash overflow.", (ftnlen)25);
    zzhsiadd_(hedlst, collst, items, &hint, &itemat, &new__);
    chckxc_(&c_true, "SPICE(HASHISFULL)", ok, (ftnlen)17);
    chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (ftnlen)
	    1);
    chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Exception: reset hash with negative count. */

    tcase_("Exception: reset with non-positive size.", (ftnlen)40);
    zzhsiini_(&c_n1, hedlst, collst);
    chckxc_(&c_true, "SPICE(INVALIDDIVISOR)", ok, (ftnlen)21);
    chcksi_("SIZE", &collst[5], "=", &c__5003, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__5004, &c__0, ok, (ftnlen)5, (ftnlen)
	    1);

/*     Reset hash. */

    tcase_("Reset hash.", (ftnlen)11);
    zzhsiini_(&c__5002, hedlst, collst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("SIZE", &collst[5], "=", &c__5002, &c__0, ok, (ftnlen)4, (ftnlen)
	    1);
    chcksi_("FIRST", &collst[4], "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    for (i__ = 1; i__ <= 5002; ++i__) {
	chcksi_("HEDLST(I)", &hedlst[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? 
		i__1 : s_rnge("hedlst", i__1, "f_zzhsi__", (ftnlen)470)], 
		"=", &c__0, &c__0, ok, (ftnlen)9, (ftnlen)1);
    }
    zzhsiavl_(collst, &avail);
    chcksi_("AVAIL", &avail, "=", &c__5002, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Check that items are not in the empty hash. */

    tcase_("Check that items are not in empty hash.", (ftnlen)39);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhsichk_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		483)], &itemat);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ITEMAT", &itemat, "=", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)
		1);
    }

/*     Timing test. */

    tcase_("Timing test.", (ftnlen)12);
    t_elapsd__(&c_false, "no report on first call", "total", (ftnlen)23, (
	    ftnlen)5);
    zzhsiini_(&c__5003, hedlst, collst);
    t_elapsd__(&report, "initializing hash", "running", (ftnlen)17, (ftnlen)7)
	    ;
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhsiadd_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		503)], &itemat, &new__);
    }
    t_elapsd__(&report, "populating hash", "running", (ftnlen)15, (ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhsiadd_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		510)], &itemat, &new__);
    }
    t_elapsd__(&report, "adding same items to hash", "running", (ftnlen)25, (
	    ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	zzhsichk_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		517)], &itemat);
    }
    t_elapsd__(&report, "checking presence in hash", "running", (ftnlen)25, (
	    ftnlen)7);
    for (i__ = 5003; i__ >= 1; --i__) {
	hint = titems[(i__1 = i__ - 1) < 5003 && 0 <= i__1 ? i__1 : s_rnge(
		"titems", i__1, "f_zzhsi__", (ftnlen)524)] + titems[5002];
	zzhsichk_(hedlst, collst, items, &hint, &itemat);
    }
    t_elapsd__(&report, "checking items not in hash", "running", (ftnlen)26, (
	    ftnlen)7);
    zzhsiini_(&c__5002, hedlst, collst);
    t_elapsd__(&report, "resetting hash", "running", (ftnlen)14, (ftnlen)7);
    for (i__ = 1; i__ <= 5003; ++i__) {
	zzhsichk_(hedlst, collst, items, &titems[(i__1 = i__ - 1) < 5003 && 0 
		<= i__1 ? i__1 : s_rnge("titems", i__1, "f_zzhsi__", (ftnlen)
		536)], &itemat);
    }
    t_elapsd__(&report, "checking items in empty hash", "running", (ftnlen)28,
	     (ftnlen)7);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_zzhsi__ */

