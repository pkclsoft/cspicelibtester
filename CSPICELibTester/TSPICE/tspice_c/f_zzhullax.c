/* f_zzhullax.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__30000 = 30000;
static integer c__2 = 2;
static logical c_true = TRUE_;
static doublereal c_b10 = 0.;
static doublereal c_b12 = 1.;
static doublereal c_b16 = 2.;
static integer c__3 = 3;
static doublereal c_b26 = -1.;
static integer c__4 = 4;
static logical c_false = FALSE_;
static doublereal c_b159 = .6;
static doublereal c_b160 = -.4;
static doublereal c_b161 = 1.2;
static integer c__1 = 1;
static integer c__6 = 6;
static doublereal c_b187 = 1e-13;

/* $Procedure      F_ZZHULLAX ( Test ZZHULLAX ) */
/* Subroutine */ int f_zzhullax__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    static integer seed;
    extern logical even_(integer *);
    static doublereal axis[3];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    extern doublereal vsep_(doublereal *, doublereal *);
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), eul2m_(
	    doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    integer *, doublereal *), zzhullax_(char *, integer *, doublereal 
	    *, doublereal *, ftnlen);
    static doublereal e;
    static integer i__, j;
    static doublereal l;
    static integer n;
    static doublereal s, v[3], w, x, delta, y;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static doublereal theta;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer 
	    *, char *, ftnlen, ftnlen, ftnlen);
    static doublereal limit;
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xform[9]	/* was [3][3] */;
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen), cleard_(integer *, doublereal *), 
	    chcksd_(char *, doublereal *, char *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal center[3], bounds[30000]	/* was [3][10000] */;
    extern logical odd_(integer *);
    static doublereal sep;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    static doublereal sum[3];
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;

/* $ Abstract */

/*     Test the private FOV axis generation routine ZZHULLAX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the private FOV axis generation */
/*     routine ZZHULLAX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.2.0, 26-AUG-2008 (NJB) */

/*        Bug fix: real constants passed to EUL2M were changed to d.p. */

/* -    SPICELIB Version 1.1.0, 21-AUG-2008 (NJB) */

/*        Updated to reflect changes in SPICE errors signaled */
/*        as a result of the ZZHULLAX dot product sign bug */
/*        fix. */

/* -    SPICELIB Version 1.0.0, 31-JUL-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Test utility functions */


/*     Local parameters */


/*     Local Variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZHULLAX", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Too few boundary vectors", (ftnlen)24);
    cleard_(&c__30000, bounds);
    zzhullax_("Test", &c__2, bounds, axis, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Linearly dependent consecutive boundary vectors", (ftnlen)47);
    vpack_(&c_b10, &c_b10, &c_b12, bounds);
    vpack_(&c_b12, &c_b10, &c_b10, &bounds[3]);
    vpack_(&c_b16, &c_b10, &c_b10, &bounds[6]);
    zzhullax_("Test", &c__3, bounds, axis, (ftnlen)4);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("FOV region too close to half-space.", (ftnlen)35);
    e = 1e-13;
    n = 4;
    vpack_(&c_b12, &c_b12, &e, bounds);
    vpack_(&c_b26, &c_b12, &e, &bounds[3]);
    vpack_(&c_b26, &c_b26, &e, &bounds[6]);
    vpack_(&c_b12, &c_b26, &e, &bounds[9]);
    zzhullax_("Test", &c__4, bounds, axis, (ftnlen)4);
    chckxc_(&c_true, "SPICE(FACENOTFOUND)", ok, (ftnlen)19);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Narrow square FOV", (ftnlen)17);
    e = 1e-6;
    n = 4;
    vpack_(&e, &e, &c_b12, bounds);
    d__1 = -e;
    vpack_(&d__1, &e, &c_b12, &bounds[3]);
    d__1 = -e;
    d__2 = -e;
    vpack_(&d__1, &d__2, &c_b12, &bounds[6]);
    d__1 = -e;
    vpack_(&e, &d__1, &c_b12, &bounds[9]);
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)266)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Narrow square FOV, boundary order reversed", (ftnlen)42);
    e = 1e-6;
    n = 4;
    vpack_(&e, &e, &c_b12, &bounds[9]);
    d__1 = -e;
    vpack_(&d__1, &e, &c_b12, &bounds[6]);
    d__1 = -e;
    d__2 = -e;
    vpack_(&d__1, &d__2, &c_b12, &bounds[3]);
    d__1 = -e;
    vpack_(&e, &d__1, &c_b12, bounds);
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)299)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Long, narrow rectangular FOV", (ftnlen)28);
    w = 1e-6;
    l = 1. - w;
    n = 4;
    vpack_(&l, &e, &c_b12, bounds);
    d__1 = -l;
    vpack_(&d__1, &e, &c_b12, &bounds[3]);
    d__1 = -l;
    d__2 = -e;
    vpack_(&d__1, &d__2, &c_b12, &bounds[6]);
    d__1 = -e;
    vpack_(&l, &d__1, &c_b12, &bounds[9]);
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)334)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Pentagonal FOV consisting of \"dented\" rectangle", (ftnlen)47);
    e = .1;
    n = 5;
    vpack_(&e, &e, &c_b12, bounds);
    d__1 = e / 2;
    vpack_(&c_b10, &d__1, &c_b12, &bounds[3]);
    d__1 = -e;
    vpack_(&d__1, &e, &c_b12, &bounds[6]);
    d__1 = -e;
    d__2 = -e;
    vpack_(&d__1, &d__2, &c_b12, &bounds[9]);
    d__1 = -e;
    vpack_(&e, &d__1, &c_b12, &bounds[12]);
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)367)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("5-pointed star", (ftnlen)14);
    e = .1;
    n = 10;
    delta = twopi_() / n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (odd_(&i__)) {

/*           This boundary vector corresponds to a point */
/*           of the star. */

	    s = e;
	} else {
	    s = e / 3;
	}
	theta = (i__ - 1) * delta;
	d__1 = s * cos(theta);
	d__2 = s * sin(theta);
	vpack_(&d__1, &d__2, &c_b12, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 
		0 <= i__2 ? i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (
		ftnlen)404)]);
    }
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)416)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("5-pointed star, reversed boundary order", (ftnlen)39);
    e = .1;
    n = 10;
    delta = twopi_() / n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (odd_(&i__)) {

/*           This boundary vector corresponds to a point */
/*           of the star. */

	    s = e;
	} else {
	    s = e / 3;
	}
	theta = (i__ - 1) * delta;
	j = n + 1 - i__;
	d__1 = s * cos(theta);
	d__2 = s * sin(theta);
	vpack_(&d__1, &d__2, &c_b12, &bounds[(i__2 = j * 3 - 3) < 30000 && 0 
		<= i__2 ? i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (
		ftnlen)454)]);
    }
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)466)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("50-gon star with 5 points", (ftnlen)25);
    e = .1;
    n = 50;
    delta = twopi_() / n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (i__ % 10 == 0) {

/*           This boundary vector corresponds to a point */
/*           of the star. */

	    s = e;
	} else {
	    s = e / 3;
	}
	theta = (i__ - 1) * delta;
	d__1 = s * cos(theta);
	d__2 = s * sin(theta);
	vpack_(&d__1, &d__2, &c_b12, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 
		0 <= i__2 ? i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (
		ftnlen)503)]);
    }
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)515)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("50-gon star with 5 points, wide FOV", (ftnlen)35);
    e = 1.5;
    n = 50;
    delta = twopi_() / n;
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (i__ % 10 == 0) {

/*           This boundary vector corresponds to a point */
/*           of the star. */

	    s = e;
	} else {
	    s = e / 3;
	}
	theta = (i__ - 1) * delta;
	d__1 = s * cos(theta);
	d__2 = s * sin(theta);
	vpack_(&d__1, &d__2, &c_b12, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 
		0 <= i__2 ? i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (
		ftnlen)551)]);
    }
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)563)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */

    tcase_("Random vectors, constrained within rectangle", (ftnlen)44);
    e = 1.;
    n = 1000;
    seed = -1;

/*     Burn the first two values. */

    d__1 = e * -2;
    d__2 = e * 2;
    x = t_randd__(&d__1, &d__2, &seed);
    d__1 = -e;
    y = t_randd__(&d__1, &e, &seed);
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = e * -2;
	d__2 = e * 2;
	x = t_randd__(&d__1, &d__2, &seed);
	d__1 = -e;
	y = t_randd__(&d__1, &e, &seed);
	vpack_(&x, &y, &c_b12, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= 
		i__2 ? i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)
		595)]);
    }
    zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Validate the axis. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? i__2 :
		 s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)607)], axis);
	s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	limit = halfpi_() - 1e-12;
	chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)1);
    }

/* ---- Case ------------------------------------------------------------- */


/*     Test some regular polygons. Let the number of */
/*     sides range from 3 to 100. Let the angular extent */
/*     of the FOVs increase with the number of sides. */


/*     We'll use a transformation to a frame that's not lined */
/*     up with the original one. */

    eul2m_(&c_b159, &c_b160, &c_b161, &c__1, &c__2, &c__3, xform);
    for (n = 3; n <= 100; ++n) {
	e = n * .01;

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Regular polygon: N = #; E = #", (ftnlen)80, (ftnlen)29)
		;
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	repmd_(title, "#", &e, &c__6, title, (ftnlen)80, (ftnlen)1, (ftnlen)
		80);
	tcase_(title, (ftnlen)80);
	delta = twopi_() / n;
	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    theta = (i__ - 1) * delta;
	    d__1 = e * cos(theta);
	    d__2 = e * sin(theta);
	    vpack_(&d__1, &d__2, &c_b12, v);
	    mxv_(xform, v, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ?
		     i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)
		    657)]);
	}
	zzhullax_("Test", &n, bounds, axis, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Validate the axis. */

	i__1 = n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    sep = vsep_(&bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ? 
		    i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (ftnlen)669)
		    ], axis);
	    s_copy(qname, "Boundary # sep", (ftnlen)80, (ftnlen)14);
	    repmi_(qname, "#", &i__, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    limit = halfpi_() - 1e-12;
	    chcksd_(qname, &sep, "<", &limit, &c_b10, ok, (ftnlen)80, (ftnlen)
		    1);
	}

/*        For polygons with even numbers of edges, the axis */
/*        should be very close to the average of the boundary */
/*        vectors. */

	if (even_(&n)) {
	    cleard_(&c__3, sum);
	    i__1 = n;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		vadd_(sum, &bounds[(i__2 = i__ * 3 - 3) < 30000 && 0 <= i__2 ?
			 i__2 : s_rnge("bounds", i__2, "f_zzhullax__", (
			ftnlen)690)], v);
		vequ_(v, sum);
	    }
	    d__1 = 1. / n;
	    vscl_(&d__1, sum, center);
	    chckad_("AXIS", axis, "~", center, &c__3, &c_b187, ok, (ftnlen)4, 
		    (ftnlen)1);
	}
    }
    t_success__(ok);
    return 0;
} /* f_zzhullax__ */

