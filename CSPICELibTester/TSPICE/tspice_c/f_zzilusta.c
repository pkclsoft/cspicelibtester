/* f_zzilusta.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__399 = 399;
static doublereal c_b30 = 0.;
static doublereal c_b32 = 1.;
static integer c__25 = 25;
static integer c__0 = 0;
static integer c__12 = 12;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__3 = 3;
static integer c__14 = 14;
static doublereal c_b183 = 1e-11;
static integer c__6 = 6;
static doublereal c_b226 = 1e-10;

/* $Procedure      F_ZZILUSTA ( ZZILUSTA tests ) */
/* Subroutine */ int f_zzilusta__(logical *ok)
{
    /* Initialized data */

    static char corrs[5*5] = "NONE " "CN   " "CN+S " "LT   " "LT+S ";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal dfdt[3];
    static logical geom;
    static doublereal last, step;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), t_ilumin__(
	    char *, char *, doublereal *, char *, char *, char *, doublereal *
	    , doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    static char time0[50];
    extern /* Subroutine */ int t_dzzilu__(char *, char *, char *, doublereal 
	    *, char *, char *, char *, doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), zzilusta_(char *, char *, char *, doublereal *, 
	    char *, char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), zzprscor_(char *, logical *, ftnlen);
    static integer n;
    static doublereal r__[9]	/* was [3][3] */, radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal phase;
    static integer obsid, octid;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), repmc_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    static integer epidx;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical usecn;
    static doublereal solar;
    extern doublereal dvsep_(doublereal *, doublereal *);
    static char illum[36], title[800];
    static doublereal first, ltsrc;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static logical uselt;
    extern /* Subroutine */ int spkw08_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen), ljust_(
	    char *, char *, ftnlen, ftnlen), bodn2c_(char *, integer *, 
	    logical *, ftnlen), t_success__(logical *);
    static doublereal obsst2[6], srcst2[6];
    extern /* Subroutine */ int str2et_(char *, doublereal *, ftnlen);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal dincdn, lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), delfil_(
	    char *, ftnlen), chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal dphase;
    static char abcorr[5];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static char fixref[32], method[80], obsrvr[36], srfref[32], target[36], 
	    timstr[50], xmtcor[5];
    static doublereal demssn, emissn, emista[2], et0, et1, etsurf, lt2, 
	    normal[3], nrmsta[6], obssta[6], phssta[2], solsta[2], spoint[3], 
	    srcsta[6], srfvec[3], sstate[6], states[12]	/* was [6][2] */, 
	    tmpsta[6], trgepc, xdemit, xdsolr, xdphas;
    static integer coridx;
    static logical attblk[6], usestl;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     tstpck_(char *, logical *, logical *, ftnlen), tstlsk_(void), 
	    srfrec_(integer *, doublereal *, doublereal *, doublereal *), 
	    lmpool_(char *, integer *, ftnlen), spkopn_(char *, char *, 
	    integer *, integer *, ftnlen, ftnlen), spkcls_(integer *), 
	    furnsh_(char *, ftnlen);
    static doublereal lat;
    extern /* Subroutine */ int bodvrd_(char *, char *, integer *, integer *, 
	    doublereal *, ftnlen, ftnlen), surfnm_(doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *);
    static doublereal dlt;
    extern /* Subroutine */ int timout_(doublereal *, char *, char *, ftnlen, 
	    ftnlen);
    extern doublereal rpd_(void), spd_(void);
    static doublereal lon;
    extern /* Subroutine */ int ilumin_(char *, char *, doublereal *, char *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), spkezr_(char *, doublereal *, char *, char *, 
	    char *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen), vminug_(doublereal *, integer *, doublereal *), spkacs_(
	    integer *, doublereal *, char *, char *, integer *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), vsclip_(doublereal *,
	     doublereal *);
    static doublereal tol;
    extern /* Subroutine */ int pxform_(char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), prefix_(char *, integer *, char *, 
	    ftnlen, ftnlen), spkuef_(integer *), unload_(char *, ftnlen), 
	    mxv_(doublereal *, doublereal *, doublereal *);
    static char txt[80*25];
    static integer han2;

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        ZZILUSTA */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB */
/*     geometry routine ZZILUSTA. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 02-APR-2014 (NJB) */

/*        Added test case for zero normal vector. */

/*     Last update was 27-AUG-2013 */

/*        Tolerance ANGTOL was loosened from 1.D-14 */
/*        to 1.D-11. */

/* -    TSPICE Version 1.0.0, 02-MAY-2012 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */





/*     Tolerances for tests involving discrete derivatives */
/*     of illumination angle rate. These are applicable */
/*     when aberration corrections are turned off, or when */
/*     a "smooth" planetary ephemeris such as DE421.bsp is */
/*     used. */


/*     Tolerance for illumination angle tests using */
/*     alternate computation methods: */


/*     Number of test epochs: */


/*     Local variables */


/*     The workspace array has to handle the largest workspace */
/*     we'll use, which is that required by GFPOSC. */


/*     Saved everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZILUSTA", (ftnlen)10);
    tcase_("Setup: create and load SPK, PCK, LSK files.", (ftnlen)43);
    tstspk_("generic.bsp", &c_true, &han2, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*      Use a JPL DE for smoother planetary ephemerides. This */
/*      option is not available for a deliverable version of */
/*      this test family. */

/*      CALL FURNSH ( 'de421.bsp' ) */
/*      CALL CHCKXC ( .FALSE., ' ',   OK ) */
    tstpck_("generic.tpc", &c_true, &c_false, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstlsk_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a confinement window. Initialize this and */
/*     the result window. */

    s_copy(time0, "2000 JAN 1  00:00:00 TDB", (ftnlen)50, (ftnlen)24);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Invalid METHOD.", (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "Elipsoid", (ftnlen)80, (ftnlen)8);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b30, &c_b30, &c_b32, normal);
    zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)
	    36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDMETHOD)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Zero normal vector.", (ftnlen)19);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(abcorr, "CN+S", (ftnlen)5, (ftnlen)4);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);
    lon = rpd_() * 100.;
    lat = rpd_() * 30.;
    srfrec_(&c__399, &lon, &lat, spoint);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b30, &c_b30, &c_b30, normal);
    zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)
	    36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);
    vpack_(&c_b30, &c_b30, &c_b32, normal);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Unrecognized target, observer, or illumination source.", (ftnlen)
	    54);
    s_copy(method, "ELLIPSOID", (ftnlen)80, (ftnlen)9);
    zzilusta_(method, "X", illum, &et, fixref, abcorr, obsrvr, spoint, normal,
	     phssta, solsta, emista, (ftnlen)80, (ftnlen)1, (ftnlen)36, (
	    ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzilusta_(method, target, "X", &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)1,
	     (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);
    zzilusta_(method, target, illum, &et, fixref, abcorr, "X", spoint, normal,
	     phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)5, (ftnlen)1);
    chckxc_(&c_true, "SPICE(IDCODENOTFOUND)", ok, (ftnlen)21);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for observer", (ftnlen)24);
    zzilusta_(method, target, illum, &et, fixref, abcorr, "GASPRA", spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)
	    36, (ftnlen)32, (ftnlen)5, (ftnlen)6);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for target", (ftnlen)22);
    zzilusta_(method, "GASPRA", illum, &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)6, (ftnlen)36,
	     (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No SPK data for illumination source", (ftnlen)35);
    zzilusta_(method, target, "GASPRA", &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)6,
	     (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("No PCK orientation data for target.", (ftnlen)35);
    s_copy(fixref, "ITRF93", (ftnlen)32, (ftnlen)6);
    zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)
	    36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
    chckxc_(&c_true, "SPICE(FRAMEDATANOTFOUND)", ok, (ftnlen)24);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Aberration correction is for transmission.", (ftnlen)42);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    zzilusta_(method, target, illum, &et, fixref, "XCN", obsrvr, spoint, 
	    normal, phssta, solsta, emista, (ftnlen)80, (ftnlen)36, (ftnlen)
	    36, (ftnlen)32, (ftnlen)3, (ftnlen)36);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    Comprehensive cases using comparisons against alternate */
/* *    computations */
/* * */
/* ********************************************************************* */
/* ********************************************************************* */
/* * */
/* *    PHASE angle tests */
/* * */
/* ********************************************************************* */

/*     We'll use the earth, moon, and sun as the three participating */
/*     bodies. The surface point will be the OCTL telescope; we'll */
/*     create an SPK file for this point. We also need an FK for a */
/*     topocentric frame centered at the point. */

/*     Though it's not strictly necessary, we'll use real data for */
/*     these kernels, with one exception: we'll use the terrestrial */
/*     reference frame IAU_EARTH rather than ITRF93. */

/*     The original reference frame specifications follow: */


/*        Topocentric frame OCTL_TOPO */

/*           The Z axis of this frame points toward the zenith. */
/*           The X axis of this frame points North. */

/*           Topocentric frame OCTL_TOPO is centered at the site OCTL */
/*           which has Cartesian coordinates */

/*              X (km):                 -0.2448937761729E+04 */
/*              Y (km):                 -0.4667935793438E+04 */
/*              Z (km):                  0.3582748499430E+04 */

/*           and planetodetic coordinates */

/*              Longitude (deg):      -117.6828380000000 */
/*              Latitude  (deg):        34.3817491000000 */
/*              Altitude   (km):         0.2259489999999E+01 */

/*           These planetodetic coordinates are expressed relative to */
/*           a reference spheroid having the dimensions */

/*              Equatorial radius (km):  6.3781400000000E+03 */
/*              Polar radius      (km):  6.3567523100000E+03 */

/*           All of the above coordinates are relative to the frame */
/*           ITRF93. */


/* ---- Case ------------------------------------------------------------- */


/*     This isn't a test, but we'll call it that so we'll have */
/*     a meaningful label in any error messages that arise. */

    tcase_("Create OCTL kernels.", (ftnlen)20);

/*     As mentioned, we go with a frame that's more convenient than */
/*     ITRF93: */

    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);

/*     Prepare a frame kernel in a string array. */

    s_copy(txt, "FRAME_OCTL_TOPO            =  1398962", (ftnlen)80, (ftnlen)
	    37);
    s_copy(txt + 80, "FRAME_1398962_NAME         =  'OCTL_TOPO' ", (ftnlen)80,
	     (ftnlen)42);
    s_copy(txt + 160, "FRAME_1398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 240, "FRAME_1398962_CLASS_ID     =  1398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 320, "FRAME_1398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 400, "OBJECT_398962_FRAME        =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 480, "TKFRAME_1398962_RELATIVE   =  'IAU_EARTH' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 560, "TKFRAME_1398962_SPEC       =  'ANGLES' ", (ftnlen)80, (
	    ftnlen)39);
    s_copy(txt + 640, "TKFRAME_1398962_UNITS      =  'DEGREES' ", (ftnlen)80, 
	    (ftnlen)40);
    s_copy(txt + 720, "TKFRAME_1398962_AXES       =  ( 3, 2, 3 )", (ftnlen)80,
	     (ftnlen)41);
    s_copy(txt + 800, "TKFRAME_1398962_ANGLES     =  ( -242.3171620000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 880, "                                 -55.6182509000000,", (
	    ftnlen)80, (ftnlen)51);
    s_copy(txt + 960, "                                 180.0000000000000  )",
	     (ftnlen)80, (ftnlen)53);
    s_copy(txt + 1040, "NAIF_BODY_NAME            +=  'OCTL' ", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1120, "NAIF_BODY_CODE            +=  398962", (ftnlen)80, (
	    ftnlen)36);

/*     It will be convenient to have a version of this frame that */
/*     has the +Z axis pointed down instead of up. */

    s_copy(txt + 1200, "FRAME_OCTL_FLIP            =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1280, "FRAME_2398962_NAME         =  'OCTL_FLIP' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1360, "FRAME_2398962_CLASS        =  4", (ftnlen)80, (ftnlen)
	    31);
    s_copy(txt + 1440, "FRAME_2398962_CLASS_ID     =  2398962", (ftnlen)80, (
	    ftnlen)37);
    s_copy(txt + 1520, "FRAME_2398962_CENTER       =  398962", (ftnlen)80, (
	    ftnlen)36);
    s_copy(txt + 1600, "TKFRAME_2398962_RELATIVE   =  'OCTL_TOPO' ", (ftnlen)
	    80, (ftnlen)42);
    s_copy(txt + 1680, "TKFRAME_2398962_SPEC       =  'ANGLES' ", (ftnlen)80, 
	    (ftnlen)39);
    s_copy(txt + 1760, "TKFRAME_2398962_UNITS      =  'DEGREES' ", (ftnlen)80,
	     (ftnlen)40);
    s_copy(txt + 1840, "TKFRAME_2398962_AXES       =  ( 3, 2, 3 )", (ftnlen)
	    80, (ftnlen)41);
    s_copy(txt + 1920, "TKFRAME_2398962_ANGLES     =  ( 0, 180.0, 0 ) ", (
	    ftnlen)80, (ftnlen)46);
    lmpool_(txt, &c__25, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now create an SPK file containing a type 8 segment for OCTL. */

    spkopn_("octl_test.bsp", "octl_test.bsp", &c__0, &handle, (ftnlen)13, (
	    ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Initialize both states to zero. */

    cleard_(&c__12, states);

/*     The first position component: */

    spoint[0] = -2448.937761729;
    spoint[1] = -4667.935793438;
    spoint[2] = 3582.74849943;
    vequ_(spoint, states);

/*     The second position matches the first: we don't model */
/*     plate motion. */

    vequ_(spoint, &states[6]);

/*     Time bounds for the segment: */

    first = spd_() * -50 * 365.25;
    step = spd_() * 100 * 365.25;
    last = first + step - 1e-6;

/*     Get the OCTL ID code from the kernel we just */
/*     loaded. */

    bodn2c_("OCTL", &octid, &found, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Write the segment. */

    spkw08_(&handle, &octid, &c__399, fixref, &first, &last, "octl", &c__1, &
	    c__2, states, &first, &step, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkcls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Now load the OCTL SPK file. */

    furnsh_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create an outward normal vector at SPOINT. */

    bodvrd_("EARTH", "RADII", &c__3, &n, radii, (ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(radii, &radii[1], &radii[2], spoint, normal);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Phase angle tests: we'll compare results from GFILUM to those */
/*     obtained using GFPA. Note that the surface point must be */
/*     an ephemeris object in order to carry out these tests. */

    s_copy(method, "Ellipsoid", (ftnlen)80, (ftnlen)9);
    s_copy(target, "EARTH", (ftnlen)36, (ftnlen)5);
    s_copy(illum, "SUN", (ftnlen)36, (ftnlen)3);
    s_copy(fixref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    s_copy(obsrvr, "MOON", (ftnlen)36, (ftnlen)4);

/*     Set up the sequence of test times. */

    s_copy(time0, "2011 JAN 1", (ftnlen)50, (ftnlen)10);
    str2et_(time0, &et0, (ftnlen)50);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Search over approximately two months. Take NTIMES samples. */

    et1 = et0 + spd_() * 60;
    step = (et1 - et0) / 99;

/*     Loop over all aberration corrections and all */
/*     observation epochs. */

    for (coridx = 1; coridx <= 5; ++coridx) {
	s_copy(abcorr, corrs + ((i__1 = coridx - 1) < 5 && 0 <= i__1 ? i__1 : 
		s_rnge("corrs", i__1, "f_zzilusta__", (ftnlen)716)) * 5, (
		ftnlen)5, (ftnlen)5);

/*        Parse the aberration correction specifier. */

	zzprscor_(abcorr, attblk, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	uselt = attblk[1];
	usestl = attblk[2];
	usecn = attblk[3];
	geom = attblk[0];
	for (epidx = 1; epidx <= 100; ++epidx) {

/* ---- Case ------------------------------------------------------------- */

	    et = et0 + (epidx - 1) * step;
	    timout_(&et, "YYYY MON DD HR:MN:SC.######::TDB TDB", timstr, (
		    ftnlen)36, (ftnlen)50);
	    s_copy(title, "ZZILUSTA test: ABCORR = #; observer = #; target ="
		    " #; illum source = #; FIXREF = #; SPOINT = ( # # # ); ET"
		    " = # (#); EPIDX = #.", (ftnlen)800, (ftnlen)125);
	    repmc_(title, "#", abcorr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    5, (ftnlen)800);
	    repmc_(title, "#", obsrvr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", target, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", illum, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    36, (ftnlen)800);
	    repmc_(title, "#", fixref, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    32, (ftnlen)800);
	    repmd_(title, "#", spoint, &c__14, title, (ftnlen)800, (ftnlen)1, 
		    (ftnlen)800);
	    repmd_(title, "#", &spoint[1], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmd_(title, "#", &spoint[2], &c__14, title, (ftnlen)800, (
		    ftnlen)1, (ftnlen)800);
	    repmc_(title, "#", timstr, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    50, (ftnlen)800);
	    repmd_(title, "#", &et, &c__14, title, (ftnlen)800, (ftnlen)1, (
		    ftnlen)800);
	    repmi_(title, "#", &epidx, title, (ftnlen)800, (ftnlen)1, (ftnlen)
		    800);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tcase_(title, (ftnlen)800);
/*            CALL TOSTDO ( TITLE ) */

/*           Look up illumination angle states. */

	    zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, 
		    spoint, normal, phssta, solsta, emista, (ftnlen)80, (
		    ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           First compare angles against those from ILUMIN. */
/*           The algorithms used should be quite similar, */
/*           so we can use a tight tolerance. */

	    ilumin_(method, target, &et, fixref, abcorr, obsrvr, spoint, &
		    trgepc, srfvec, &phase, &solar, &emissn, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the phase angle. */

	    chcksd_("PHASE", phssta, "~/", &phase, &c_b183, ok, (ftnlen)5, (
		    ftnlen)2);

/*           Check the solar incidence angle. */

	    chcksd_("SOLAR", solsta, "~/", &solar, &c_b183, ok, (ftnlen)5, (
		    ftnlen)2);

/*           Check the emission angle. */

	    chcksd_("EMISTA", emista, "~/", &emissn, &c_b183, ok, (ftnlen)6, (
		    ftnlen)2);

/*           Now compute illumination angles via an independent */
/*           algorithm. */

/*           This algorithm computes aberration corrections for */
/*           the illumination source less accurately, so adjust */
/*           the tolerance when aberration corrections are used. */

	    if (attblk[0]) {
		tol = 1e-11;
	    } else {
		tol = 5e-6;
	    }
	    t_ilumin__(method, target, &et, fixref, abcorr, obsrvr, spoint, &
		    trgepc, srfvec, &phase, &solar, &emissn, (ftnlen)80, (
		    ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Check the phase angle. */

	    chcksd_("PHASE (2)", phssta, "~", &phase, &tol, ok, (ftnlen)9, (
		    ftnlen)1);

/*           Check the solar incidence angle. */

	    chcksd_("SOLAR (2)", solsta, "~", &solar, &tol, ok, (ftnlen)9, (
		    ftnlen)1);

/*           Check the emission angle. */

	    chcksd_("EMISTA (2)", emista, "~", &emissn, &tol, ok, (ftnlen)10, 
		    (ftnlen)1);

/*           Now for the interesting part: check the angular rates. */
/*           We'll compute discrete derivatives of each one and */
/*           compare these to the rates produced by ZZILUSTA. */

/*           Fit Cheby expansions to the illumination angles */
/*           and find derivatives of the expansion wrt time at ET. */

	    t_dzzilu__(method, target, illum, &et, fixref, abcorr, obsrvr, 
		    spoint, normal, &dphase, &dincdn, &demssn, (ftnlen)80, (
		    ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    dfdt[0] = dphase;
	    dfdt[1] = dincdn;
	    dfdt[2] = demssn;
	    zzilusta_(method, target, illum, &et, fixref, abcorr, obsrvr, 
		    spoint, normal, phssta, solsta, emista, (ftnlen)80, (
		    ftnlen)36, (ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*            WRITE (*,*) 'Cheby Phase rate:          ', DPHASE */
/*            WRITE (*,*) 'ZZILUSTA Phase rate:  ', PHSSTA(2) */
/*            WRITE (*,*) 'Difference:           ', DPHASE-PHSSTA(2) */
/*            WRITE (*,*) 'Phase rate rel error:      ', */
/*     .                                  (DPHASE-PHSSTA(2))/DPHASE */
/*            WRITE (*,*) 'Cheby Incidence rate:      ', DINCDN */
/*            WRITE (*,*) 'Incidence rate rel error:  ', */
/*     .                                  (DINCDN-SOLSTA(2))/DINCDN */
/*            WRITE (*,*) 'Cheby Emission rate:       ', DEMSSN */
/*            WRITE (*,*) 'Emission rate rel error:   ', */
/*     .                                  (DEMSSN-EMISTA(2))/DEMSSN */

/*            WRITE (*,*) ' ' */

/*           Check the phase angle rate. */

	    if (geom) {
		tol = 1e-6;
	    } else {
		tol = 1.5e-6;
	    }
	    chcksd_("d PHASE/dt", &phssta[1], "~/", dfdt, &tol, ok, (ftnlen)
		    10, (ftnlen)2);

/*           Check the solar incidence angle rate. */

	    tol = 5e-7;
	    chcksd_("d SOLAR/dt", &solsta[1], "~/", &dfdt[1], &tol, ok, (
		    ftnlen)10, (ftnlen)2);

/*           Check the emission angle rate. */

	    tol = 5e-7;
	    chcksd_("d EMISSN/dt", &emista[1], "~/", &dfdt[2], &tol, ok, (
		    ftnlen)11, (ftnlen)2);

/*           Now check the angular rates using an alternate */
/*           analytic method. We'll do the computation inline. */

/*           We'll use a body-fixed frame to compute the */
/*           angles and rates. Life will be easier if the */
/*           frame is centered at the surface point. */

	    s_copy(srfref, "OCTL_TOPO", (ftnlen)32, (ftnlen)9);

/*           Get the observer-surface point state at ET in the surface */
/*           point centered, body-fixed frame. Negate this state to */
/*           obtain the surface point-observer state. */

	    spkezr_("OCTL", &et, srfref, abcorr, obsrvr, sstate, &lt, (ftnlen)
		    4, (ftnlen)32, (ftnlen)5, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vminug_(sstate, &c__6, obssta);

/*           Compute the epoch associated with the surface point. */

/*           Also compute the rate of change of light time. */

	    bodn2c_(obsrvr, &obsid, &found, (ftnlen)36);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("OBSID FOUND", &found, &c_true, ok, (ftnlen)11);
	    spkacs_(&octid, &et, "J2000", abcorr, &obsid, tmpsta, &lt2, &dlt, 
		    (ftnlen)5, (ftnlen)5);
	    if (attblk[0]) {
		etsurf = et;
		dlt = 0.;
	    } else {
		etsurf = et - lt;
	    }

/*           Compute the surface point-illumination source state */
/*           at ETSURF in the surface point body-fixed frame. */

	    spkezr_(illum, &etsurf, srfref, abcorr, "OCTL", srcsta, &ltsrc, (
		    ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)4);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Adjust the surface point-illumination source velocity */
/*           for the rate of change of ETSURF with respect to ET. */

	    d__1 = 1. - dlt;
	    vsclip_(&d__1, &srcsta[3]);

/*           Create a state for the normal vector. Transform */
/*           the normal vector to frame SRFREF. */

	    pxform_(fixref, srfref, &etsurf, r__, (ftnlen)32, (ftnlen)32);
	    mxv_(r__, normal, nrmsta);
	    cleard_(&c__3, &nrmsta[3]);

/*           Compute and check expected rates. */

/*           Phase angle rate: */

	    xdphas = dvsep_(obssta, srcsta);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d PHASE/dt (2)", &phssta[1], "~/", &xdphas, &c_b226, ok, 
		    (ftnlen)14, (ftnlen)2);

/*           Emission angle rate: */

	    xdemit = dvsep_(obssta, nrmsta);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d EMISSN/dt (2)", &emista[1], "~/", &xdemit, &c_b226, ok,
		     (ftnlen)15, (ftnlen)2);

/*           Solar incidence angle rate: */

	    xdsolr = dvsep_(srcsta, nrmsta);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d SOLAR/dt (2)", &solsta[1], "~/", &xdsolr, &c_b226, ok, 
		    (ftnlen)14, (ftnlen)2);

/*           Now compute the rates using a second, alternate, */
/*           analytic method. */

/*           This time we compute rates using ETSURF as the */
/*           observation epoch. After computing the rates, */
/*           we adjust them so they express the rates of change */
/*           of the illumination angles with respect to ET. */

/*           Start out by creating a transmission aberration */
/*           correction specifier, if corrections are being */
/*           used. */

	    if (uselt) {
		ljust_(abcorr, xmtcor, (ftnlen)5, (ftnlen)5);
		prefix_("X", &c__0, xmtcor, (ftnlen)1, (ftnlen)5);
	    } else {
		dlt = 0.;
		s_copy(xmtcor, "NONE", (ftnlen)5, (ftnlen)4);
	    }

/*           Compute the surface point-observer state at ETSURF. */

	    if (usestl) {

/*              Due to the asymmetry of stellar aberration */
/*              corrections, it's easiest to compute the */
/*              surface point-observer vector in the usual */
/*              way. */

		spkezr_("OCTL", &et, srfref, abcorr, obsrvr, sstate, &lt, (
			ftnlen)4, (ftnlen)32, (ftnlen)5, (ftnlen)36);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		vminug_(sstate, &c__6, obsst2);

/*              Adjust the velocity portion of the state so */
/*              that it is the derivative of position with */
/*              respect to ETSURF rather than ET. */

		d__1 = 1. / (1. - dlt);
		vsclip_(&d__1, &obsst2[3]);
	    } else {

/*              We can compute the surface point-observer state */
/*              directly using transmission corrections and */
/*              an event time of ETSURF. */

		spkezr_(obsrvr, &etsurf, srfref, xmtcor, "OCTL", obsst2, &lt, 
			(ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)4);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
	    }

/*           Compute the surface point-illumination source */
/*           state at ETSURF. */

	    spkezr_(illum, &etsurf, srfref, abcorr, "OCTL", srcst2, &lt, (
		    ftnlen)36, (ftnlen)32, (ftnlen)5, (ftnlen)4);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    moved_(obsst2, &c__6, tmpsta);
	    d__1 = 1. - dlt;
	    vsclip_(&d__1, &tmpsta[3]);
	    moved_(srcst2, &c__6, tmpsta);
	    d__1 = 1. - dlt;
	    vsclip_(&d__1, &tmpsta[3]);

/*           We already have the state of the normal vector. */


/*           Compute the phase angle rates; scale each one */
/*           by the rate of change of ETSURF with respect to */
/*           ET. */

	    if (geom || usecn) {
		tol = 1e-10;
	    } else {
		tol = 1e-7;
	    }

/*           Phase angle rate: */

	    xdphas = dvsep_(obsst2, srcst2) * (1. - dlt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d PHASE/dt (3)", &phssta[1], "~/", &xdphas, &tol, ok, (
		    ftnlen)14, (ftnlen)2);

/*           Emission angle rate: */

	    xdemit = dvsep_(obsst2, nrmsta) * (1. - dlt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d EMISSN/dt (3)", &emista[1], "~/", &xdemit, &tol, ok, (
		    ftnlen)15, (ftnlen)2);

/*           Solar incidence angle rate: */

	    xdsolr = dvsep_(srcst2, nrmsta) * (1. - dlt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_("d SOLAR/dt (3)", &solsta[1], "~/", &xdsolr, &tol, ok, (
		    ftnlen)14, (ftnlen)2);
	}

/*        End of epoch loop. */

    }

/*     End of aberration correction loop. */


/* ---- Case ------------------------------------------------------------- */

    tcase_("Clean up. Unload and delete kernels.", (ftnlen)36);

/*     Clean up SPK files. */

    spkuef_(&han2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("generic.bsp", (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("octl_test.bsp", (ftnlen)13);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzilusta__ */

