/* f_zzinlat0.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c_n1 = -1;

/* $Procedure F_ZZINLAT0 ( ZZINLAT0 tests ) */
/* Subroutine */ int f_zzinlat0__(logical *ok)
{
    /* Initialized data */

    static doublereal minlat[6] = { -90.,-89.999999,-45.,0.,45.,89.999999 };
    static doublereal maxlat[6] = { -89.999999,-45.,0.,45.,89.999999,90. };
    static doublereal minlon[9] = { -10.,-10.,-180.,-360.,10.,179.999999,
	    -179.999999,-260.,350. };
    static doublereal maxlon[9] = { -5.,20.,180.,0.,-10.,-179.999999,
	    179.999999,200.,-350. };
    static doublereal minr[4] = { 0.,.001,1e3,9999.999 };
    static doublereal maxr[4] = { 1e4,1e4,1e4,1e4 };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer pow_ii(integer *, integer *);

    /* Local variables */
    static doublereal aeps;
    extern /* Subroutine */ int zzinlat0_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, logical *);
    static doublereal midr;
    static char stem[200];
    extern /* Subroutine */ int zznrmlon_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static integer i__, l, m, n;
    static doublereal r__;
    static logical latlb;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static logical latub;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer 
	    *, char *, ftnlen, ftnlen, ftnlen);
    static char title[200];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer lonix;
    extern /* Subroutine */ int t_success__(logical *);
    static integer latix1, latix2;
    extern doublereal pi_(void), halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static doublereal midlat;
    static logical inside;
    static doublereal midlon;
    static logical lonbds;
    static integer exclud;
    static doublereal bounds[6]	/* was [2][3] */, nrmmin, nrmmax;
    extern /* Subroutine */ int suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen);
    static logical rlb;
    static doublereal lat;
    extern doublereal dpr_(void), rpd_(void);
    static doublereal eps, lon, tol;
    static logical xin;
    static integer rix1, rix2;

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZINLAT0. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZINLAT0. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 10-OCT-2016 (NJB) */

/*        Removed non-error case with matching minimum and */
/*        maximum longitudes. */

/*        Previous version 1.0.0, 25-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Set the initial angular bounds in degrees, since we can */
/*     do this without function calls. The bounds will be */
/*     converted to radians at run time. */


/*     For the latitude boundaries, every valid combination of */
/*     minimum and maximum will be tested. */


/*     For the longitude boundaries, each pair of bounds */
/*     consisting of the Ith minimum and Ith maximum will */
/*     be tested. */


/*     For the radius boundaries, every valid combination of */
/*     minimum and maximum will be tested. */


/*     Open the test family. */

    topen_("F_ZZINLAT0", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize coordinate bounds", (ftnlen)28);

/*     Convert angular bounds to radians. */

    for (i__ = 1; i__ <= 6; ++i__) {
	minlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("minlat", 
		i__1, "f_zzinlat0__", (ftnlen)272)] = minlat[(i__2 = i__ - 1) 
		< 6 && 0 <= i__2 ? i__2 : s_rnge("minlat", i__2, "f_zzinlat0"
		"__", (ftnlen)272)] * rpd_();
	maxlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("maxlat", 
		i__1, "f_zzinlat0__", (ftnlen)273)] = maxlat[(i__2 = i__ - 1) 
		< 6 && 0 <= i__2 ? i__2 : s_rnge("maxlat", i__2, "f_zzinlat0"
		"__", (ftnlen)273)] * rpd_();
    }
    for (i__ = 1; i__ <= 9; ++i__) {
	minlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("minlon", 
		i__1, "f_zzinlat0__", (ftnlen)280)] = minlon[(i__2 = i__ - 1) 
		< 9 && 0 <= i__2 ? i__2 : s_rnge("minlon", i__2, "f_zzinlat0"
		"__", (ftnlen)280)] * rpd_();
	maxlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("maxlon", 
		i__1, "f_zzinlat0__", (ftnlen)281)] = maxlon[(i__2 = i__ - 1) 
		< 9 && 0 <= i__2 ? i__2 : s_rnge("maxlon", i__2, "f_zzinlat0"
		"__", (ftnlen)281)] * rpd_();
    }

/* --- Case: ------------------------------------------------------ */


/*     Loop over the volume element cases. */

    for (lonix = 1; lonix <= 9; ++lonix) {
	bounds[0] = minlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("minlon", i__1, "f_zzinlat0__", (ftnlen)296)];
	bounds[1] = maxlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("maxlon", i__1, "f_zzinlat0__", (ftnlen)297)];

/*        Normalize the element's longitude bounds. */

	tol = 1e-13;
	zznrmlon_(bounds, &bounds[1], &tol, &nrmmin, &nrmmax);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Indicate whether the longitude boundaries exist (vs */
/*        2*pi longitude extent). */

	lonbds = nrmmax - nrmmin < pi_() * 2 - 1e-12;
	for (latix1 = 1; latix1 <= 6; ++latix1) {
	    bounds[2] = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinlat0__", (ftnlen)316)];

/*           Indicate whether the lower latitude boundary is a surface. */

	    latlb = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinlat0__", (ftnlen)320)] > 
		    -halfpi_();
	    for (latix2 = latix1; latix2 <= 6; ++latix2) {
		bounds[3] = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? 
			i__1 : s_rnge("maxlat", i__1, "f_zzinlat0__", (ftnlen)
			325)];

/*              Indicate whether the upper latitude boundary is a */
/*              surface. */

		latub = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? i__1 : 
			s_rnge("maxlat", i__1, "f_zzinlat0__", (ftnlen)331)] <
			 halfpi_();
		for (rix1 = 1; rix1 <= 4; ++rix1) {
		    bounds[4] = minr[(i__1 = rix1 - 1) < 4 && 0 <= i__1 ? 
			    i__1 : s_rnge("minr", i__1, "f_zzinlat0__", (
			    ftnlen)336)];

/*                 Indicate whether the lower radius boundary is a */
/*                 surface. */

		    rlb = minr[(i__1 = rix1 - 1) < 4 && 0 <= i__1 ? i__1 : 
			    s_rnge("minr", i__1, "f_zzinlat0__", (ftnlen)341)]
			     > 0.;
		    for (rix2 = rix1; rix2 <= 4; ++rix2) {
			bounds[5] = maxr[(i__1 = rix2 - 1) < 4 && 0 <= i__1 ? 
				i__1 : s_rnge("maxr", i__1, "f_zzinlat0__", (
				ftnlen)346)];
			for (exclud = 0; exclud <= 3; ++exclud) {

/* -- Case: ------------------------------------------------------ */


/*                       Set the input point so that each coordinate */
/*                       is the midpoint of the element's range for */
/*                       that coordinate. */

			    s_copy(stem, "Lon #:#; Lat #:#; Rad #:#; EXCLUD "
				    "= #;", (ftnlen)200, (ftnlen)38);
			    d__1 = bounds[0] * dpr_();
			    repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)200,
				     (ftnlen)1, (ftnlen)200);
			    d__1 = bounds[1] * dpr_();
			    repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)200,
				     (ftnlen)1, (ftnlen)200);
			    d__1 = bounds[2] * dpr_();
			    repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)200,
				     (ftnlen)1, (ftnlen)200);
			    d__1 = bounds[3] * dpr_();
			    repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)200,
				     (ftnlen)1, (ftnlen)200);
			    repmd_(stem, "#", &bounds[4], &c__9, stem, (
				    ftnlen)200, (ftnlen)1, (ftnlen)200);
			    repmd_(stem, "#", &bounds[5], &c__9, stem, (
				    ftnlen)200, (ftnlen)1, (ftnlen)200);
			    repmi_(stem, "#", &exclud, stem, (ftnlen)200, (
				    ftnlen)1, (ftnlen)200);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* -- Case: ------------------------------------------------------ */

			    s_copy(title, stem, (ftnlen)200, (ftnlen)200);
			    suffix_("Midpoint case", &c__1, title, (ftnlen)13,
				     (ftnlen)200);
			    tcase_(title, (ftnlen)200);

/*                        CALL TOSTDO ( TITLE ) */

			    midlon = (nrmmin + nrmmax) / 2;
			    midlat = (bounds[2] + bounds[3]) / 2;
			    midr = (bounds[4] + bounds[5]) / 2;
			    zzinlat0_(&midr, &midlon, &midlat, bounds, &
				    exclud, &inside);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    xin = TRUE_;
			    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* -- Case: ------------------------------------------------------ */


/*                       Check interior points near each corner of */
/*                       the volume element. */

			    for (l = 1; l <= 2; ++l) {
				for (m = 1; m <= 2; ++m) {
				    for (n = 1; n <= 2; ++n) {

/* -- Case: ------------------------------------------------------ */

					s_copy(title, stem, (ftnlen)200, (
						ftnlen)200);
					suffix_("Point near corner # # #; in"
						"terior", &c__1, title, (
						ftnlen)33, (ftnlen)200);
					repmi_(title, "#", &l, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &m, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &n, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					tcase_(title, (ftnlen)200);

/*                                Set incremental offsets. */

					aeps = 1e-13;
					eps = bounds[5] * 1e-13;

/*                                Multiply the small increments by 1 or */
/*                                -1 as needed, depending on whether they */
/*                                are used as offsets from upper or lower */
/*                                bounds. */

					i__2 = m + 1;
					lat = bounds[(i__1 = m + 1) < 6 && 0 
						<= i__1 ? i__1 : s_rnge("bou"
						"nds", i__1, "f_zzinlat0__", (
						ftnlen)442)] + aeps * pow_ii(&
						c_n1, &i__2);
					i__2 = n + 1;
					r__ = bounds[(i__1 = n + 3) < 6 && 0 
						<= i__1 ? i__1 : s_rnge("bou"
						"nds", i__1, "f_zzinlat0__", (
						ftnlen)443)] + eps * pow_ii(&
						c_n1, &i__2);
					if (l == 1) {
					    lon = nrmmin + aeps;
					} else {
					    lon = nrmmax - aeps;
					}
					zzinlat0_(&r__, &lon, &lat, bounds, &
						exclud, &inside);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					xin = TRUE_;
					chcksl_("INSIDE", &inside, &xin, ok, (
						ftnlen)6);

/* -- Case: ------------------------------------------------------ */

					s_copy(title, stem, (ftnlen)200, (
						ftnlen)200);
					suffix_("Point near corner # # #; in"
						"terior; excluded coordinate "
						"out of range.", &c__1, title, 
						(ftnlen)68, (ftnlen)200);
					repmi_(title, "#", &l, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &m, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &n, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					tcase_(title, (ftnlen)200);

/*                                Set incremental offsets. */

					aeps = 1e-11;
					eps = bounds[5] * 1e-13;

/*                                Multiply the small increments by 1 or */
/*                                -1 as needed, depending on whether they */
/*                                are used as offsets from upper or lower */
/*                                bounds. */

					i__2 = m + 1;
					lat = bounds[(i__1 = m + 1) < 6 && 0 
						<= i__1 ? i__1 : s_rnge("bou"
						"nds", i__1, "f_zzinlat0__", (
						ftnlen)490)] + aeps * pow_ii(&
						c_n1, &i__2);
					if (exclud == 2) {

/*                                   Latitude is not considered in */
/*                                   the bounds comparison performed */
/*                                   by ZZINLAT0. */

					    if (m == 1) {
			  lat = -halfpi_();
					    } else {
			  lat = halfpi_();
					    }
					}
					i__2 = n + 1;
					r__ = bounds[(i__1 = n + 3) < 6 && 0 
						<= i__1 ? i__1 : s_rnge("bou"
						"nds", i__1, "f_zzinlat0__", (
						ftnlen)508)] + eps * pow_ii(&
						c_n1, &i__2);
					if (exclud == 3) {

/*                                   Radius is not considered in */
/*                                   the bounds comparison performed */
/*                                   by ZZINLAT0. */

					    if (n == 1) {
			  r__ = 0.;
					    } else {
			  r__ = bounds[5] * 2;
					    }
					}
					if (l == 1) {
					    lon = nrmmin + aeps;
					} else {
					    lon = nrmmax - aeps;
					}
					if (exclud == 1) {

/*                                   Longitude is not considered in */
/*                                   the bounds comparison performed */
/*                                   by ZZINLAT0. */

					    if (n == 1) {
			  lon = nrmmin - aeps;
					    } else {
			  lon = nrmmax + aeps;
					    }
					}
					zzinlat0_(&r__, &lon, &lat, bounds, &
						exclud, &inside);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					xin = TRUE_;
					chcksl_("INSIDE", &inside, &xin, ok, (
						ftnlen)6);

/* -- Case: ------------------------------------------------------ */

					s_copy(title, stem, (ftnlen)200, (
						ftnlen)200);
					suffix_("Point near corner # # #; ex"
						"terior; successor of exclude"
						"d coordinate out of range.", &
						c__1, title, (ftnlen)81, (
						ftnlen)200);
					repmi_(title, "#", &l, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &m, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &n, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					tcase_(title, (ftnlen)200);

/*                                Give XIN its default value. In most */
/*                                cases, we'll reset it to .FALSE. */

					xin = TRUE_;

/*                                Set incremental offsets. */

					aeps = 1e-11;
					eps = bounds[5] * 1e-13;
					if (exclud == 1) {

/*                                   Set the latitude value out of range. */
					    if (m == 1) {
			  lat = -halfpi_();

/*                                      If we managed to set the */
/*                                      latitude to a value below the */
/*                                      lower latitude bound, then we */
/*                                      have an exterior point. */

			  xin = ! latlb;
					    } else {
			  lat = halfpi_();
			  xin = ! latub;
					    }
					} else {
					    lat = midlat;
					}
					if (exclud == 2) {

/*                                   Set the radius value out of range. */

					    if (n == 1) {
			  r__ = 0.;
			  xin = ! rlb;
					    } else {
			  r__ = bounds[5] * 2;
			  xin = FALSE_;
					    }
					} else {
					    r__ = midr;
					}
					if (exclud == 3) {

/*                                   Set the longitude value out of */
/*                                   range. */

					    if (l == 1) {
			  lon = nrmmin - aeps;
			  xin = ! lonbds;
					    } else {
			  lon = nrmmax + aeps;
			  xin = ! lonbds;
					    }
					} else {
					    lon = midlon;
					}
					zzinlat0_(&r__, &lon, &lat, bounds, &
						exclud, &inside);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                XIN has been set. */

					chcksl_("INSIDE", &inside, &xin, ok, (
						ftnlen)6);

/* -- Case: ------------------------------------------------------ */

					s_copy(title, stem, (ftnlen)200, (
						ftnlen)200);
					suffix_("Point near corner # # #; ex"
						"terior; predecessor of exclu"
						"ded coordinate out of range.",
						 &c__1, title, (ftnlen)83, (
						ftnlen)200);
					repmi_(title, "#", &l, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &m, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					repmi_(title, "#", &n, title, (ftnlen)
						200, (ftnlen)1, (ftnlen)200);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
					tcase_(title, (ftnlen)200);

/*                                Give XIN its default value. In most */
/*                                cases, we'll reset it to .FALSE. */

					xin = TRUE_;

/*                                Set incremental offsets. */

					aeps = 1e-11;
					eps = bounds[5] * 1e-13;
					if (exclud == 3) {

/*                                   Set the latitude value out of range. */
					    if (m == 1) {
			  lat = -halfpi_();

/*                                      If we managed to set the */
/*                                      latitude to a value below the */
/*                                      lower latitude bound, then we */
/*                                      have an exterior point. */

			  xin = ! latlb;
					    } else {
			  lat = halfpi_();
			  xin = ! latub;
					    }
					} else {
					    lat = midlat;
					}
					if (exclud == 1) {

/*                                   Set the radius value out of range. */

					    if (n == 1) {
			  r__ = 0.;
			  xin = ! rlb;
					    } else {
			  r__ = bounds[5] * 2;
			  xin = FALSE_;
					    }
					} else {
					    r__ = midr;
					}
					if (exclud == 2) {

/*                                   Set the longitude value out of */
/*                                   range. */

					    if (l == 1) {
			  lon = nrmmin - aeps;
			  xin = ! lonbds;
					    } else {
			  lon = nrmmax + aeps;
			  xin = ! lonbds;
					    }
					} else {
					    lon = midlon;
					}
					zzinlat0_(&r__, &lon, &lat, bounds, &
						exclud, &inside);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                XIN has been set. */

					chcksl_("INSIDE", &inside, &xin, ok, (
						ftnlen)6);
				    }

/*                             End of "N" loop. N selects the radius */
/*                             upper/lower bound. */

				}

/*                          End of "M" loop. M selects the radius */
/*                          upper/lower bound. */

			    }

/*                       End of "L" loop. L selects the longitude */
/*                       upper/lower bound. */
			}

/*                    End of coordinate exclusion (EXCLUD) loop. */

		    }

/*                 End of upper radius bound loop. */

		}

/*              End of lower radius bound loop. */

	    }

/*           End of upper latitude bound loop. */

	}

/*        End of lower latitude bound loop. */

    }

/*     End of longitude loop. */

/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/*     ZZINLAT0 is "error free." */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzinlat0__ */

