/* f_zzinpdt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10 = 10;
static doublereal c_b22 = 3010.;
static doublereal c_b23 = 3020.;
static doublereal c_b24 = 2800.;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static doublereal c_b53 = 1.;
static doublereal c_b54 = 1e-6;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c_n1 = -1;
static doublereal c_b205 = 0.;
static integer c__6 = 6;

/* $Procedure F_ZZINPDT ( ZZINPDT tests ) */
/* Subroutine */ int f_zzinpdt__(logical *ok)
{
    /* Initialized data */

    static logical first = TRUE_;
    static doublereal minlat[6] = { -90.,-89.999999,-45.,0.,45.,89.999999 };
    static doublereal maxlat[6] = { -89.999999,-45.,0.,45.,89.999999,90. };
    static doublereal minlon[9] = { -10.,-10.,-180.,-360.,10.,179.999999,
	    -179.999999,-260.,350. };
    static doublereal maxlon[9] = { -5.,20.,180.,0.,-10.,-179.999999,
	    179.999999,200.,-350. };
    static doublereal minalt[3] = { -200.,-150.,200. };
    static doublereal maxalt[3] = { -100.,50.,300. };
    static doublereal eqrad[3] = { 1e4,8e3,2e4 };
    static doublereal polrad[3] = { 8e3,1e4,5e3 };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer pow_ii(integer *, integer *);

    /* Local variables */
    static doublereal eqhi, aeps;
    static char stem[300];
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzellbds_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal a, b, f;
    extern /* Subroutine */ int zznrmlon_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static integer i__, l, m, n;
    static doublereal p[3];
    static logical latlb;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal hialt, hilat;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static logical latub;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static doublereal hilon;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static doublereal polhi;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), swapd_(doublereal *, doublereal *), 
	    vlcom_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static char title[300];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal eqlow;
    static integer lonix;
    extern doublereal twopi_(void);
    static doublereal p2[3], p3[3], lowpt[3];
    extern /* Subroutine */ int t_success__(logical *);
    static integer latix1, latix2;
    static doublereal re;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     georec_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), recgeo_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), chcksl_(
	    char *, logical *, logical *, logical *, ftnlen);
    static doublereal modbds[6]	/* was [2][3] */, midlat, margin, midlon, 
	    highpt[3], midalt, bounds[6]	/* was [2][3] */, corpar[10], 
	    lowalt, lowlat, lowlon, normal[3], nrmmax, nrmmin, pollow, posmrg,
	     vertex[3];
    static integer exclud, shapix;
    static logical inside, lonbds;
    extern /* Subroutine */ int surfnm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), surfpt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *), suffix_(char *, integer *, char *, 
	    ftnlen, ftnlen), vminus_(doublereal *, doublereal *);
    static doublereal dir[3], alt, lat;
    extern doublereal dpr_(void), rpd_(void);
    static doublereal eps, lon, tol;
    static logical xin;
    extern /* Subroutine */ int zzinpdt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, logical *);

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZINPDT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZINPDT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 13-FEB-2017 (NJB) */

/*        Updated tests for revised near-polar latitude margin */
/*        and inclusion logic. */

/*        10-OCT-2016 (NJB) */

/*        Removed non-error case with matching minimum and */
/*        maximum longitudes. */

/*        Previous version 09-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Set the initial angular bounds in degrees, since we can */
/*     do this without function calls. The bounds will be */
/*     converted to radians at run time. */


/*     For the latitude boundaries, every valid combination of */
/*     minimum and maximum will be tested. */


/*     For the longitude boundaries, each pair of bounds */
/*     consisting of the Ith minimum and Ith maximum will */
/*     be tested. */


/*     For the altitude boundaries, there is one valid combination of */
/*     minimum and maximum for each shape. */


/*     Open the test family. */

    topen_("F_ZZINPDT", (ftnlen)9);
/* *********************************************************************** */

/*     Setup */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize coordinate bounds", (ftnlen)28);

/*     Convert angular bounds to radians. */


/*     First pass only! */

    if (first) {
	for (i__ = 1; i__ <= 6; ++i__) {
	    minlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("minlat",
		     i__1, "f_zzinpdt__", (ftnlen)331)] = minlat[(i__2 = i__ 
		    - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge("minlat", i__2, 
		    "f_zzinpdt__", (ftnlen)331)] * rpd_();
	    maxlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("maxlat",
		     i__1, "f_zzinpdt__", (ftnlen)332)] = maxlat[(i__2 = i__ 
		    - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge("maxlat", i__2, 
		    "f_zzinpdt__", (ftnlen)332)] * rpd_();
	}
	for (i__ = 1; i__ <= 9; ++i__) {
	    minlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("minlon",
		     i__1, "f_zzinpdt__", (ftnlen)339)] = minlon[(i__2 = i__ 
		    - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("minlon", i__2, 
		    "f_zzinpdt__", (ftnlen)339)] * rpd_();
	    maxlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("maxlon",
		     i__1, "f_zzinpdt__", (ftnlen)340)] = maxlon[(i__2 = i__ 
		    - 1) < 9 && 0 <= i__2 ? i__2 : s_rnge("maxlon", i__2, 
		    "f_zzinpdt__", (ftnlen)340)] * rpd_();
	}
	first = FALSE_;
    }

/*     Initialize CORPAR. */

    cleard_(&c__10, corpar);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: negative margin.", (ftnlen)23);
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    bounds[4] = -re / 2.;
    bounds[5] = re / 2.;
    corpar[0] = re;
    corpar[1] = f;
    vpack_(&c_b22, &c_b23, &c_b24, p);
    margin = -1e-6;
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: invalid value of EXCLUD", (ftnlen)30);
    margin = 1e-6;
    exclud = -1;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    exclud = 4;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: longitude bounds out of range.", (ftnlen)37);
    margin = 1e-6;
    exclud = 3;
    bounds[0] = pi_() * -3;
    bounds[1] = -twopi_();
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */


/*     Special non-error cases */


/* *********************************************************************** */

/*     Test special logic for positions near the poles. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Segment includes north pole. Point's longitude is out of range; "
	    "altitude is in range.", (ftnlen)85);
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = 0.;
    bounds[3] = pi_() / 2;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[3] - 1e-8;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    margin = 1e-7;
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment includes north pole. Point's longitude is out of range; "
	    "altitude is out of range.", (ftnlen)89);
    margin = 1e-7;
    a = re;
    b = re * (1. - f);
    d__1 = bounds[5] * (margin + 1);
    d__2 = bounds[4] * (1 - margin);
    zzellbds_(&a, &b, &d__1, &d__2, &eqhi, &polhi, &eqlow, &pollow);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Start with a point on the reference ellipsoid. Find the point */
/*     directly above this on the outer bounding ellipsoid. */

    lat = bounds[3] - 1e-8;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(&a, &a, &b, vertex, normal);

/*     Find the surface point on the lower bounding ellipsoid, at the */
/*     given planetodetic longitude and latitude (relative to the */
/*     reference ellipsoid). */

    surfpt_(vertex, normal, &eqhi, &eqhi, &polhi, highpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should never fail to find an intercept. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vlcom_(&c_b53, highpt, &c_b54, normal, p);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's upper latitude bound is near north pole. Point's longi"
	    "tude is out of range; altitude and latitude are in range.", (
	    ftnlen)121);
    margin = 1e-8;
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = 0.;
    bounds[3] = pi_() / 2 - margin * 1.5;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[3] + margin / 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's upper latitude bound is near north pole. Point's longi"
	    "tude is out of range; altitude is in range; latitude is out of r"
	    "ange.", (ftnlen)133);
    margin = 1e-8;
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = 0.;
    bounds[3] = pi_() / 2 - 1e-7;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[3] + margin * 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's upper latitude bound is near north pole. Point's longi"
	    "tude is out of range; latitude is in range; altitude is out of r"
	    "ange.", (ftnlen)133);
    margin = 1e-7;
    a = re;
    b = re * (1. - f);
    d__1 = bounds[5] * (margin + 1);
    d__2 = bounds[4] * (1 - margin);
    zzellbds_(&a, &b, &d__1, &d__2, &eqhi, &polhi, &eqlow, &pollow);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Start with a point on the reference ellipsoid. Find the point */
/*     directly above this on the outer bounding ellipsoid. */

    lat = bounds[3] + margin / 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(&a, &a, &b, vertex, normal);

/*     Find the surface point on the lower bounding ellipsoid, at the */
/*     given planetodetic longitude and latitude (relative to the */
/*     reference ellipsoid). */

    surfpt_(vertex, normal, &eqhi, &eqhi, &polhi, highpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should never fail to find an intercept. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vlcom_(&c_b53, highpt, &c_b54, normal, p);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment includes south pole. Point's longitude is out of range; "
	    "altitude is in range.", (ftnlen)85);
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = -pi_() / 2;
    bounds[3] = 0.;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[2] + 1e-8;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    margin = 1e-7;
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment includes south pole. Point's longitude is out of range; "
	    "altitude is out of range.", (ftnlen)89);
    margin = 1e-7;
    a = re;
    b = re * (1. - f);
    d__1 = bounds[5] * (margin + 1);
    d__2 = bounds[4] * (1 - margin);
    zzellbds_(&a, &b, &d__1, &d__2, &eqhi, &polhi, &eqlow, &pollow);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Start with a point on the reference ellipsoid. Find the point */
/*     directly above this on the outer bounding ellipsoid. */

    lat = bounds[2] + 1e-8;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(&a, &a, &b, vertex, normal);

/*     Find the surface point on the lower bounding ellipsoid, at the */
/*     given planetodetic longitude and latitude (relative to the */
/*     reference ellipsoid). */

    surfpt_(vertex, normal, &eqhi, &eqhi, &polhi, highpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should never fail to find an intercept. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vlcom_(&c_b53, highpt, &c_b54, normal, p);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's lower latitude bound is near south pole. Point's longi"
	    "tude is out of range; altitude and latitude are in range.", (
	    ftnlen)121);
    margin = 1e-8;
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = -pi_() / 2 + margin * 1.5;
    bounds[3] = 0.;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[2] - margin / 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_true, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's lower latitude bound is near north pole. Point's longi"
	    "tude is out of range; altitude is in range; latitude is out of r"
	    "ange.", (ftnlen)133);
    margin = 1e-8;
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = pi_() / 4;
    bounds[2] = 0.;
    bounds[3] = pi_() / 2 - 1e-7;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;
    lat = bounds[3] + margin * 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Segment's lower latitude bound is near south pole. Point's longi"
	    "tude is out of range; latitude is in range; altitude is out of r"
	    "ange.", (ftnlen)133);
    margin = 1e-7;
    a = re;
    b = re * (1. - f);
    d__1 = bounds[5] * (margin + 1);
    d__2 = bounds[4] * (1 - margin);
    zzellbds_(&a, &b, &d__1, &d__2, &eqhi, &polhi, &eqlow, &pollow);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Start with a point on the reference ellipsoid. Find the point */
/*     directly above this on the outer bounding ellipsoid. */

    lat = bounds[2] - margin / 2;
    lon = pi_() / 2;
    alt = 0.;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfnm_(&a, &a, &b, vertex, normal);

/*     Find the surface point on the lower bounding ellipsoid, at the */
/*     given planetodetic longitude and latitude (relative to the */
/*     reference ellipsoid). */

    surfpt_(vertex, normal, &eqhi, &eqhi, &polhi, highpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We should never fail to find an intercept. */

    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    vlcom_(&c_b53, highpt, &c_b54, normal, p);
    exclud = 0;
    zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("INSIDE", &inside, &c_false, ok, (ftnlen)6);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Choose a positive margin. We'll vary the margin in some */
/*     test cases, so we use POSMRG to save the original value. */

    posmrg = 1e-6;
    margin = posmrg;

/*     Loop over the volume element cases. */

    for (lonix = 1; lonix <= 9; ++lonix) {
	bounds[0] = minlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("minlon", i__1, "f_zzinpdt__", (ftnlen)966)];
	bounds[1] = maxlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("maxlon", i__1, "f_zzinpdt__", (ftnlen)967)];

/*        Normalize the element's longitude bounds. */

	tol = 1e-13;
	zznrmlon_(bounds, &bounds[1], &tol, &nrmmin, &nrmmax);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Indicate whether the longitude boundaries exist (vs */
/*        2*pi longitude extent). */

	lonbds = nrmmax - nrmmin < pi_() * 2 - 1e-12;
	for (latix1 = 1; latix1 <= 6; ++latix1) {
	    bounds[2] = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinpdt__", (ftnlen)986)];

/*           Indicate whether the lower latitude boundary is a surface. */

	    latlb = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinpdt__", (ftnlen)990)] > 
		    -halfpi_();
	    for (latix2 = latix1; latix2 <= 6; ++latix2) {
		bounds[3] = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? 
			i__1 : s_rnge("maxlat", i__1, "f_zzinpdt__", (ftnlen)
			995)];

/*              Indicate whether the upper latitude boundary is a */
/*              surface. */

		latub = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? i__1 : 
			s_rnge("maxlat", i__1, "f_zzinpdt__", (ftnlen)1001)] <
			 halfpi_();
		for (shapix = 1; shapix <= 3; ++shapix) {
		    a = eqrad[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? i__1 : 
			    s_rnge("eqrad", i__1, "f_zzinpdt__", (ftnlen)1006)
			    ];
		    b = polrad[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? i__1 : 
			    s_rnge("polrad", i__1, "f_zzinpdt__", (ftnlen)
			    1007)];
		    f = (a - b) / a;
		    corpar[0] = a;
		    corpar[1] = f;
		    bounds[4] = minalt[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? 
			    i__1 : s_rnge("minalt", i__1, "f_zzinpdt__", (
			    ftnlen)1013)];
		    bounds[5] = maxalt[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? 
			    i__1 : s_rnge("maxalt", i__1, "f_zzinpdt__", (
			    ftnlen)1014)];
		    for (exclud = 0; exclud <= 3; ++exclud) {

/* --- Case: ------------------------------------------------------ */


/*                    Set the input point so that each coordinate */
/*                    is the midpoint of the element's range for */
/*                    that coordinate. */

			s_copy(stem, "Lon #:#; Lat #:#; Alt #:#; EXCLUD = #;",
				 (ftnlen)300, (ftnlen)38);
			d__1 = bounds[0] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[1] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[2] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[3] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			repmd_(stem, "#", &bounds[4], &c__9, stem, (ftnlen)
				300, (ftnlen)1, (ftnlen)300);
			repmd_(stem, "#", &bounds[5], &c__9, stem, (ftnlen)
				300, (ftnlen)1, (ftnlen)300);
			repmi_(stem, "#", &exclud, stem, (ftnlen)300, (ftnlen)
				1, (ftnlen)300);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			++i__;

/* --- Case: ------------------------------------------------------ */

			s_copy(title, stem, (ftnlen)300, (ftnlen)300);
			suffix_("Midpoint case, positive margin ", &c__1, 
				title, (ftnlen)31, (ftnlen)300);
			tcase_(title, (ftnlen)300);
			midlon = (nrmmin + nrmmax) / 2;
			midlat = (bounds[2] + bounds[3]) / 2;
			midalt = (bounds[4] + bounds[5]) / 2;
			georec_(&midlon, &midlat, &midalt, &a, &f, p);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside)
				;
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			xin = TRUE_;
			chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

			s_copy(title, stem, (ftnlen)300, (ftnlen)300);
			suffix_("Midpoint case, zero margin", &c__1, title, (
				ftnlen)26, (ftnlen)300);
			tcase_(title, (ftnlen)300);
			margin = 0.;
			midlon = (nrmmin + nrmmax) / 2;
			midlat = (bounds[2] + bounds[3]) / 2;
			midalt = (bounds[4] + bounds[5]) / 2;
			georec_(&midlon, &midlat, &midalt, &a, &f, p);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			zzinpdt_(p, bounds, corpar, &margin, &exclud, &inside)
				;
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			xin = TRUE_;
			chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/*                    Restore margin. */

			margin = posmrg;

/* --- Case: ------------------------------------------------------ */


/*                    Check interior points near each corner of */
/*                    the volume element. */

			for (l = 1; l <= 2; ++l) {
			    for (m = 1; m <= 2; ++m) {
				for (n = 1; n <= 2; ++n) {
				    ++i__;

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; interi"
					    "or", &c__1, title, (ftnlen)33, (
					    ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Set incremental offsets. AEPS is the */
/*                             "angular epsilon." EPS is scaled to */
/*                             yield a "distance epsilon." */

				    aeps = 1e-11;
				    eps = a * 1e-6;

/*                             Obtain the ellipsoid radii of the lower */
/*                             and upper bounding surfaces. It is these, */
/*                             not the surfaces of minimum and maximum */
/*                             altitude relative to the body's reference */
/*                             ellipsoid, that are used for comparison. */

				    if (a > b) {
					d__1 = bounds[5] * (margin + 1);
					d__2 = bounds[4] * (1 - margin);
					zzellbds_(&a, &b, &d__1, &d__2, &eqhi,
						 &polhi, &eqlow, &pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
				    } else {

/*                                This is the prolate case. ZZELLBDS */
/*                                works only with oblate spheroids, */
/*                                so swap A and B on input. */

					d__1 = bounds[5] * (margin + 1);
					d__2 = bounds[4] * (1 - margin);
					zzellbds_(&b, &a, &d__1, &d__2, &eqhi,
						 &polhi, &eqlow, &pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                The EQ* outputs are actually for */
/*                                the polar radii of the bounding */
/*                                ellipsoids, and the POL* outputs */
/*                                are for the equatorial radii of */
/*                                the bounding ellipsoids. */

					swapd_(&eqhi, &polhi);
					swapd_(&eqlow, &pollow);
				    }

/*                             Multiply the small increments by 1 or */
/*                             -1 as needed, depending on whether they */
/*                             are used as offsets from upper or lower */
/*                             bounds. */

				    i__2 = m + 1;
				    lat = bounds[(i__1 = m + 1) < 6 && 0 <= 
					    i__1 ? i__1 : s_rnge("bounds", 
					    i__1, "f_zzinpdt__", (ftnlen)1201)
					    ] + aeps * pow_ii(&c_n1, &i__2);
				    if (l == 1) {
					lon = nrmmin + aeps;
				    } else {
					lon = nrmmax - aeps;
				    }

/*                             Find the point at the given longitude */
/*                             and latitude on the reference ellipsoid. */
/*                             We'll use this below. */

				    georec_(&lon, &lat, &c_b205, &a, &f, 
					    vertex);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    surfnm_(&a, &a, &b, vertex, normal);
				    if (n == 1) {

/*                                Find the surface point on the lower */
/*                                bounding ellipsoid, at the given */
/*                                planetodetic longitude and latitude */
/*                                (relative to the reference ellipsoid). */
					if (bounds[4] < 0.) {

/*                                   The direction to the inner bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqlow, &eqlow, &
						pollow, lowpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of LOWPT */
/*                                with respect to the reference */
/*                                ellipsoid. */

					recgeo_(lowpt, &a, &f, &lowlon, &
						lowlat, &lowalt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of LOWPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = lowalt + eps * pow_ii(&c_n1, &
						i__1);
				    } else {

/*                                This is the case for the outer */
/*                                bounding ellipsoid (N = 2). */

					if (bounds[5] < 0.) {

/*                                   The direction to the outer bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqhi, &eqhi, &
						polhi, highpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of */
/*                                HIGHPT with respect to the reference */
/*                                ellipsoid. */

					recgeo_(highpt, &a, &f, &hilon, &
						hilat, &hialt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of HIGHPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = hialt + eps * pow_ii(&c_n1, &
						i__1);
				    }

/*                             Produce the perturbed input point using */
/*                             the perturbed geodetic coordinates. */

				    georec_(&lon, &lat, &alt, &a, &f, p2);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt_(p2, bounds, corpar, &margin, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = TRUE_;
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; interi"
					    "or; excluded coordinate out of r"
					    "ange.", &c__1, title, (ftnlen)68, 
					    (ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Set initial coordinates of the input */
/*                             point. */

				    recgeo_(p2, &a, &f, &lon, &lat, &alt);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Use the perturbed point produced for */
/*                             the previous test case. Modify the */
/*                             excluded coordinate of the point. */

				    if (exclud == 2) {

/*                                Latitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. */

/*                                However, we can't move the latitude */
/*                                too far out of range, or else the */
/*                                relationship between the altitude */
/*                                of the point and the bounding ellipsoid */
/*                                will change too much. */

					if (m == 1) {
/* Computing MAX */
					    d__1 = -halfpi_(), d__2 = lat - 
						    margin * 2;
					    lat = max(d__1,d__2);
					} else {
/* Computing MIN */
					    d__1 = halfpi_(), d__2 = lat + 
						    margin * 2;
					    lat = min(d__1,d__2);
					}
				    }
				    if (exclud == 3) {

/*                                Altitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. */

					if (n == 1) {
					    alt -= abs(alt) * .1;
					} else {
					    alt += abs(alt) * .1;
					}
				    }
				    if (exclud == 1) {

/*                                Longitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. */

					if (n == 1) {
					    lon = nrmmin - aeps;
					} else {
					    lon = nrmmax + aeps;
					}
				    }
				    georec_(&lon, &lat, &alt, &a, &f, p3);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt_(p3, bounds, corpar, &margin, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = TRUE_;
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; exteri"
					    "or; successor of excluded coordi"
					    "nate out of range.", &c__1, title,
					     (ftnlen)81, (ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Set initial coordinates of the input */
/*                             point. */

				    recgeo_(p2, &a, &f, &lon, &lat, &alt);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             We're going to work with a modified */
/*                             version of the input bounds. This will */
/*                             make it easier to create coordinates that */
/*                             are slightly out of range. */

				    moved_(bounds, &c__6, modbds);

/*                             Use the perturbed point produced for */
/*                             the previous test case. Modify the */
/*                             excluded coordinate of the point. */

				    if (exclud == 1) {

/*                                Longitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. We'll move the latitude */
/*                                out of bounds. */

/*                                However, we can't move the latitude */
/*                                too far out of range, or else the */
/*                                relationship between the altitude */
/*                                of the point and the bounding ellipsoid */
/*                                will change too much. */

					if (m == 1) {
/* Computing MAX */
					    d__1 = -halfpi_(), d__2 = lat - 
						    margin * 2;
					    lat = max(d__1,d__2);
					} else {
/* Computing MIN */
					    d__1 = halfpi_(), d__2 = lat + 
						    margin * 2;
					    lat = min(d__1,d__2);
					}
				    }
				    if (exclud == 2 || exclud == 0) {

/*                                Either all coordinates are considered, */
/*                                or latitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. We'll move the altitude */
/*                                out of bounds. */

					if (n == 1) {
					    alt -= abs(alt) * .1;
					} else {
					    alt += abs(alt) * .1;
					}
				    }
				    if (exclud == 3) {

/*                                Altitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. We'll move the */
/*                                longitude out of bounds. */

/*                                To make it possible to create */
/*                                longitude values that are out */
/*                                of bounds, adjust the longitude */
/*                                bounds. We'll nudge the lower */
/*                                longitude bound upwards for this */
/*                                test. */

/*                                The magnitude of the "nudge" must */
/*                                be great enough to overcome the */
/*                                rounding margin used (even) by the */
/*                                "no margin" routine ZZINPDT for */
/*                                longitude comparisons. */

					modbds[0] += margin * 4;
					modbds[1] -= margin * 4;
					if (n == 1) {
					    lon = nrmmin - margin * 2;
					} else {
					    lon = nrmmax + margin * 2;
					}
				    }
				    georec_(&lon, &lat, &alt, &a, &f, p3);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Perform the test using modified bounds. */
/*                             See the EXCLUD = 3 case above. */

				    zzinpdt_(p3, modbds, corpar, &margin, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Except for some special cases, the */
/*                             point should be outside the element. */

				    xin = FALSE_;

/*                             Our latitude modifications won't work for */
/*                             the cases where the latitude is already */
/*                             at an extreme value. */

				    if (exclud == 1) {

/*                                Longitude is the excluded coordinate; */
/*                                this is a case for which we try to */
/*                                modify latitude. However, the */
/*                                attempted modification may have no */
/*                                effect if a boundary is already at */
/*                                the extreme value. */

					if (m == 1 && ! latlb) {
					    xin = TRUE_;
					} else if (m == 2 && ! latub) {
					    xin = TRUE_;
					}

/*                                If the latitude is within the latitude */
/*                                range extended by the margin, the */
/*                                point is in the element. */

					if (lat >= bounds[2] - margin && lat 
						<= bounds[3] + margin) {
					    xin = TRUE_;
					}
				    } else if (exclud == 3) {

/*                                Longitude will be set to an */
/*                                out-of-range value. This won't */
/*                                be considered if we're in the */
/*                                "high latitude" case. We use the */
/*                                old value of LATMRG here, because */
/*                                with the new value one cannot create */
/*                                longitudes that are out of range, due */
/*                                to the longitude margin scaling. */

					if (lat > halfpi_() - 1e-6 && lat <= 
						bounds[3]) {
					    xin = TRUE_;
					}
					if (lat < -halfpi_() + 1e-6 && lat >= 
						bounds[2]) {
					    xin = TRUE_;
					}
				    }
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);
/* *********************************************************************** */


/*     ZERO MARGIN CASES */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

				    margin = 0.;
				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; interi"
					    "or; zero margin ", &c__1, title, (
					    ftnlen)47, (ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Set incremental offsets. AEPS is the */
/*                             "angular epsilon." EPS is scaled to */
/*                             yield a "distance epsilon." */

				    aeps = 1e-9;
				    eps = a * 1e-9;

/*                             Obtain the ellipsoid radii of the lower */
/*                             and upper bounding surfaces. It is these, */
/*                             not the surfaces of minimum and maximum */
/*                             altitude relative to the body's reference */
/*                             ellipsoid, that are used for comparison. */

				    if (a > b) {
					zzellbds_(&a, &b, &bounds[5], &bounds[
						4], &eqhi, &polhi, &eqlow, &
						pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
				    } else {

/*                                This is the prolate case. ZZELLBDS */
/*                                works only with oblate spheroids, */
/*                                so swap A and B on input. */

					zzellbds_(&b, &a, &bounds[5], &bounds[
						4], &eqhi, &polhi, &eqlow, &
						pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                The EQ* outputs are actually for */
/*                                the polar radii of the bounding */
/*                                ellipsoids, and the POL* outputs */
/*                                are for the equatorial radii of */
/*                                the bounding ellipsoids. */

					swapd_(&eqhi, &polhi);
					swapd_(&eqlow, &pollow);
				    }

/*                             Multiply the small increments by 1 or */
/*                             -1 as needed, depending on whether they */
/*                             are used as offsets from upper or lower */
/*                             bounds. */

/*                             Note the scaled increment for latitude. */

				    i__2 = m + 1;
				    lat = bounds[(i__1 = m + 1) < 6 && 0 <= 
					    i__1 ? i__1 : s_rnge("bounds", 
					    i__1, "f_zzinpdt__", (ftnlen)1717)
					    ] + aeps * pow_ii(&c_n1, &i__2);
				    if (l == 1) {
					lon = nrmmin + aeps;
				    } else {
					lon = nrmmax - aeps;
				    }

/*                             Find the point at the given longitude */
/*                             and latitude on the reference ellipsoid. */
/*                             We'll use this below. */

				    georec_(&lon, &lat, &c_b205, &a, &f, 
					    vertex);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    surfnm_(&a, &a, &b, vertex, normal);
				    if (n == 1) {

/*                                Find the surface point on the lower */
/*                                bounding ellipsoid, at the given */
/*                                planetodetic longitude and latitude */
/*                                (relative to the reference ellipsoid). */
					if (bounds[4] < 0.) {

/*                                   The direction to the inner bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqlow, &eqlow, &
						pollow, lowpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of LOWPT */
/*                                with respect to the reference */
/*                                ellipsoid. */

					recgeo_(lowpt, &a, &f, &lowlon, &
						lowlat, &lowalt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of LOWPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = lowalt + eps * pow_ii(&c_n1, &
						i__1);
				    } else {

/*                                This is the case for the outer */
/*                                bounding ellipsoid (N = 2). */

					if (bounds[5] < 0.) {

/*                                   The direction to the outer bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqhi, &eqhi, &
						polhi, highpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of */
/*                                HIGHPT with respect to the reference */
/*                                ellipsoid. */

					recgeo_(highpt, &a, &f, &hilon, &
						hilat, &hialt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of HIGHPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = hialt + eps * pow_ii(&c_n1, &
						i__1);
				    }

/*                             Produce the perturbed input point using */
/*                             the perturbed geodetic coordinates. */

				    georec_(&lon, &lat, &alt, &a, &f, p2);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt_(p2, bounds, corpar, &margin, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = TRUE_;
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/* --- Case: ------------------------------------------------------ */

				    margin = 0.;
				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; exteri"
					    "or; successor of excluded coordi"
					    "nate out of range; zero margin ", 
					    &c__1, title, (ftnlen)94, (ftnlen)
					    300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);
/*                             Set initial coordinates of the input */
/*                             point. */

				    recgeo_(p2, &a, &f, &lon, &lat, &alt);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Set incremental offsets. AEPS is the */
/*                             "angular epsilon." EPS is scaled to */
/*                             yield a "distance epsilon." */

				    aeps = rpd_() * 1e-7;
				    eps = a * 1e-9;

/*                             Multiply the small increments by 1 or */
/*                             -1 as needed, depending on whether they */
/*                             are used as offsets from upper or lower */
/*                             bounds. */

/*                             Note the scaled increment for latitude. */

				    if (exclud == 1) {
					if (m == 1) {
/* Computing MAX */
					    d__1 = bounds[(i__1 = m + 1) < 6 
						    && 0 <= i__1 ? i__1 : 
						    s_rnge("bounds", i__1, 
						    "f_zzinpdt__", (ftnlen)
						    1892)] - aeps * 2, d__2 = 
						    -halfpi_();
					    lat = max(d__1,d__2);
					} else {
/* Computing MIN */
					    d__1 = bounds[(i__1 = m + 1) < 6 
						    && 0 <= i__1 ? i__1 : 
						    s_rnge("bounds", i__1, 
						    "f_zzinpdt__", (ftnlen)
						    1895)] + aeps * 2, d__2 = 
						    halfpi_();
					    lat = min(d__1,d__2);
					}
				    } else {
					i__2 = m + 1;
					lat = bounds[(i__1 = m + 1) < 6 && 0 
						<= i__1 ? i__1 : s_rnge("bou"
						"nds", i__1, "f_zzinpdt__", (
						ftnlen)1901)] + aeps * pow_ii(
						&c_n1, &i__2);
				    }
				    if (exclud == 3) {
					if (l == 1) {
					    lon = nrmmin - aeps;
					} else {
					    lon = nrmmax + aeps;
					}
				    } else {
					if (l == 1) {
					    lon = nrmmin + aeps;
					} else {
					    lon = nrmmax - aeps;
					}
				    }

/*                             Find the point at the given longitude */
/*                             and latitude on the reference ellipsoid. */
/*                             We'll use this below. */

				    georec_(&lon, &lat, &c_b205, &a, &f, 
					    vertex);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    surfnm_(&a, &a, &b, vertex, normal);
				    if (n == 1) {

/*                                Find the surface point on the lower */
/*                                bounding ellipsoid, at the given */
/*                                planetodetic longitude and latitude */
/*                                (relative to the reference ellipsoid). */
					if (bounds[4] < 0.) {

/*                                   The direction to the inner bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqlow, &eqlow, &
						pollow, lowpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of LOWPT */
/*                                with respect to the reference */
/*                                ellipsoid. */

					recgeo_(lowpt, &a, &f, &lowlon, &
						lowlat, &lowalt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of LOWPT by a */
/*                                small increment. */

					if (exclud == 2 || exclud == 0) {
					    alt = lowalt + eps * pow_ii(&c_n1,
						     &n);
					} else {
					    i__1 = n + 1;
					    alt = lowalt + eps * pow_ii(&c_n1,
						     &i__1);
					}
				    } else {

/*                                This is the case for the outer */
/*                                bounding ellipsoid (N = 2). */

					if (bounds[5] < 0.) {

/*                                   The direction to the outer bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqhi, &eqhi, &
						polhi, highpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of */
/*                                HIGHPT with respect to the reference */
/*                                ellipsoid. */

					recgeo_(highpt, &a, &f, &hilon, &
						hilat, &hialt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of HIGHPT by a */
/*                                small increment. */

					if (exclud == 2 || exclud == 0) {
					    alt = hialt + eps * pow_ii(&c_n1, 
						    &n);
					} else {
					    i__1 = n + 1;
					    alt = hialt + eps * pow_ii(&c_n1, 
						    &i__1);
					}
				    }
				    if (exclud == 2 || exclud == 0) {

/*                                Either all coordinates are considered, */
/*                                or latitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT. We'll move the altitude */
/*                                out of bounds. */

					if (n == 1) {
					    alt -= abs(alt) * .1;
					} else {
					    alt += abs(alt) * .1;
					}
				    }

/*                             Produce the perturbed input point using */
/*                             the perturbed geodetic coordinates. */

				    georec_(&lon, &lat, &alt, &a, &f, p2);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt_(p2, bounds, corpar, &margin, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = FALSE_;
				    if (exclud == 1) {

/*                                Longitude is the excluded coordinate; */
/*                                this is a case for which we try to */
/*                                modify latitude. However, the */
/*                                attempted modification may have no */
/*                                effect if a boundary is already at */
/*                                the extreme value. */

					if (lat >= bounds[2] - 1e-12 && lat <=
						 bounds[3] + 1e-12) {
					    xin = TRUE_;
					}
				    } else if (exclud == 3) {
					if (lon >= bounds[0] - 1e-12 && lon <=
						 bounds[1] + 1e-12) {
					    xin = TRUE_;
					}
					if (bounds[0] == bounds[1]) {
					    xin = TRUE_;
					}
					if (! lonbds) {
					    xin = TRUE_;
					}
					if (lonix == 8) {
					    if (l == 2) {

/*                                      In this special case, the input */
/*                                      longitude will be outside the */
/*                                      longitude boundaries. */

			  xin = FALSE_;
					    }
					}
				    }
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/*                             Restore the positive margin. */

				    margin = posmrg;
				}

/*                          End of "N" loop. N selects the altitude */
/*                          upper/lower bound. */

			    }

/*                       End of "M" loop. M selects the altitude */
/*                       upper/lower bound. */

			}

/*                    End of "L" loop. L selects the longitude */
/*                    upper/lower bound. */

		    }

/*                 End of coordinate exclusion (EXCLUD) loop. */

		}

/*              End of altitude bound loop. */

	    }

/*           End of upper latitude bound loop. */

	}

/*        End of lower latitude bound loop. */

    }

/*     End of longitude loop. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzinpdt__ */

