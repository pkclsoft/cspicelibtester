/* f_zzinpdt0.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10 = 10;
static logical c_false = FALSE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c_n1 = -1;
static doublereal c_b80 = 0.;
static logical c_true = TRUE_;
static integer c__6 = 6;

/* $Procedure F_ZZINPDT0 ( ZZINPDT0 tests ) */
/* Subroutine */ int f_zzinpdt0__(logical *ok)
{
    /* Initialized data */

    static doublereal minlat[6] = { -90.,-89.999999,-45.,0.,45.,89.999999 };
    static doublereal maxlat[6] = { -89.999999,-45.,0.,45.,89.999999,90. };
    static doublereal minlon[9] = { -10.,-10.,-180.,-360.,10.,179.999999,
	    -179.999999,-260.,350. };
    static doublereal maxlon[9] = { -5.,20.,180.,0.,-10.,-179.999999,
	    179.999999,200.,-350. };
    static doublereal minalt[3] = { -200.,-150.,200. };
    static doublereal maxalt[3] = { -100.,50.,300. };
    static doublereal eqrad[3] = { 1e4,8e3,2e4 };
    static doublereal polrad[3] = { 8e3,1e4,5e3 };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer pow_ii(integer *, integer *);

    /* Local variables */
    static doublereal eqhi, aeps;
    extern /* Subroutine */ int zzinpdt0_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, logical *);
    static char stem[300];
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzellbds_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal a, b, f;
    extern /* Subroutine */ int zznrmlon_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static integer i__, l, m, n;
    static doublereal p[3];
    static logical latlb;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal hialt, hilat;
    static logical latub;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen);
    static doublereal hilon;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    static doublereal polhi;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), swapd_(doublereal *, doublereal *);
    static char title[300];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal eqlow;
    static integer lonix;
    static doublereal lowpt[3];
    extern /* Subroutine */ int t_success__(logical *);
    static integer latix1, latix2;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     georec_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chcksl_(char *, logical *, logical *,
	     logical *, ftnlen);
    static doublereal modbds[6]	/* was [2][3] */, midlat, midalt, midlon, 
	    highpt[3], bounds[6]	/* was [2][3] */, corpar[10], lowalt, 
	    lowlat, lowlon, normal[3], nrmmax, nrmmin, pollow, vertex[3];
    static integer exclud, shapix;
    static logical inside, lonbds;
    extern /* Subroutine */ int suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen), surfnm_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), vminus_(doublereal *, doublereal *), 
	    surfpt_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *), recgeo_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal dir[3], alt, lat;
    extern doublereal dpr_(void), rpd_(void);
    static doublereal eps, lon, tol;
    static logical xin;

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZINPDT0. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZINPDT0. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 10-OCT-2016 (NJB) */

/*        Removed non-error case with matching minimum and */
/*        maximum longitudes. */

/*        Previous version 16-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Set the initial angular bounds in degrees, since we can */
/*     do this without function calls. The bounds will be */
/*     converted to radians at run time. */


/*     For the latitude boundaries, every valid combination of */
/*     minimum and maximum will be tested. */


/*     For the longitude boundaries, each pair of bounds */
/*     consisting of the Ith minimum and Ith maximum will */
/*     be tested. */


/*     For the altitude boundaries, there is one valid combination of */
/*     minimum and maximum for each shape. */


/*     Open the test family. */

    topen_("F_ZZINPDT0", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize coordinate bounds", (ftnlen)28);

/*     Convert angular bounds to radians. */

    for (i__ = 1; i__ <= 6; ++i__) {
	minlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("minlat", 
		i__1, "f_zzinpdt0__", (ftnlen)290)] = minlat[(i__2 = i__ - 1) 
		< 6 && 0 <= i__2 ? i__2 : s_rnge("minlat", i__2, "f_zzinpdt0"
		"__", (ftnlen)290)] * rpd_();
	maxlat[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("maxlat", 
		i__1, "f_zzinpdt0__", (ftnlen)291)] = maxlat[(i__2 = i__ - 1) 
		< 6 && 0 <= i__2 ? i__2 : s_rnge("maxlat", i__2, "f_zzinpdt0"
		"__", (ftnlen)291)] * rpd_();
    }
    for (i__ = 1; i__ <= 9; ++i__) {
	minlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("minlon", 
		i__1, "f_zzinpdt0__", (ftnlen)298)] = minlon[(i__2 = i__ - 1) 
		< 9 && 0 <= i__2 ? i__2 : s_rnge("minlon", i__2, "f_zzinpdt0"
		"__", (ftnlen)298)] * rpd_();
	maxlon[(i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("maxlon", 
		i__1, "f_zzinpdt0__", (ftnlen)299)] = maxlon[(i__2 = i__ - 1) 
		< 9 && 0 <= i__2 ? i__2 : s_rnge("maxlon", i__2, "f_zzinpdt0"
		"__", (ftnlen)299)] * rpd_();
    }

/*     Initialize CORPAR. */

    cleard_(&c__10, corpar);

/* --- Case: ------------------------------------------------------ */


/*     Loop over the volume element cases. */

    i__ = 0;
    for (lonix = 1; lonix <= 9; ++lonix) {
	bounds[0] = minlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("minlon", i__1, "f_zzinpdt0__", (ftnlen)319)];
	bounds[1] = maxlon[(i__1 = lonix - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("maxlon", i__1, "f_zzinpdt0__", (ftnlen)320)];

/*        Normalize the element's longitude bounds. */

	tol = 1e-13;
	zznrmlon_(bounds, &bounds[1], &tol, &nrmmin, &nrmmax);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Indicate whether the longitude boundaries exist (vs */
/*        2*pi longitude extent). */

	lonbds = nrmmax - nrmmin < pi_() * 2 - 1e-12;
	for (latix1 = 1; latix1 <= 6; ++latix1) {
	    bounds[2] = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinpdt0__", (ftnlen)339)];

/*           Indicate whether the lower latitude boundary is a surface. */

	    latlb = minlat[(i__1 = latix1 - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("minlat", i__1, "f_zzinpdt0__", (ftnlen)343)] > 
		    -halfpi_();
	    for (latix2 = latix1; latix2 <= 6; ++latix2) {
		bounds[3] = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? 
			i__1 : s_rnge("maxlat", i__1, "f_zzinpdt0__", (ftnlen)
			348)];

/*              Indicate whether the upper latitude boundary is a */
/*              surface. */

		latub = maxlat[(i__1 = latix2 - 1) < 6 && 0 <= i__1 ? i__1 : 
			s_rnge("maxlat", i__1, "f_zzinpdt0__", (ftnlen)354)] <
			 halfpi_();
		for (shapix = 1; shapix <= 3; ++shapix) {
		    a = eqrad[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? i__1 : 
			    s_rnge("eqrad", i__1, "f_zzinpdt0__", (ftnlen)359)
			    ];
		    b = polrad[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? i__1 : 
			    s_rnge("polrad", i__1, "f_zzinpdt0__", (ftnlen)
			    360)];
		    f = (a - b) / a;
		    corpar[0] = a;
		    corpar[1] = f;
		    bounds[4] = minalt[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? 
			    i__1 : s_rnge("minalt", i__1, "f_zzinpdt0__", (
			    ftnlen)366)];
		    bounds[5] = maxalt[(i__1 = shapix - 1) < 3 && 0 <= i__1 ? 
			    i__1 : s_rnge("maxalt", i__1, "f_zzinpdt0__", (
			    ftnlen)367)];
		    for (exclud = 0; exclud <= 3; ++exclud) {
			++i__;

/* --- Case: ------------------------------------------------------ */


/*                    Set the input point so that each coordinate */
/*                    is the midpoint of the element's range for */
/*                    that coordinate. */

			s_copy(stem, "Lon #:#; Lat #:#; Alt #:#; EXCLUD = #;",
				 (ftnlen)300, (ftnlen)38);
			d__1 = bounds[0] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[1] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[2] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			d__1 = bounds[3] * dpr_();
			repmd_(stem, "#", &d__1, &c__9, stem, (ftnlen)300, (
				ftnlen)1, (ftnlen)300);
			repmd_(stem, "#", &bounds[4], &c__9, stem, (ftnlen)
				300, (ftnlen)1, (ftnlen)300);
			repmd_(stem, "#", &bounds[5], &c__9, stem, (ftnlen)
				300, (ftnlen)1, (ftnlen)300);
			repmi_(stem, "#", &exclud, stem, (ftnlen)300, (ftnlen)
				1, (ftnlen)300);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			++i__;

/* --- Case: ------------------------------------------------------ */

			s_copy(title, stem, (ftnlen)300, (ftnlen)300);
			suffix_("Midpoint case", &c__1, title, (ftnlen)13, (
				ftnlen)300);
			tcase_(title, (ftnlen)300);
			midlon = (nrmmin + nrmmax) / 2;
			midlat = (bounds[2] + bounds[3]) / 2;
			midalt = (bounds[4] + bounds[5]) / 2;
			georec_(&midlon, &midlat, &midalt, &a, &f, p);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			zzinpdt0_(p, &midlon, bounds, corpar, &exclud, &
				inside);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			xin = TRUE_;
			chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */


/*                    Check interior points near each corner of */
/*                    the volume element. */

			for (l = 1; l <= 2; ++l) {
			    for (m = 1; m <= 2; ++m) {
				for (n = 1; n <= 2; ++n) {
				    ++i__;

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; interi"
					    "or", &c__1, title, (ftnlen)33, (
					    ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Set incremental offsets. AEPS is the */
/*                             "angular epsilon." EPS is scaled to */
/*                             yield a "distance epsilon." */

				    aeps = 1e-11;
				    eps = a * 1e-13;

/*                             Obtain the ellipsoid radii of the lower */
/*                             and upper bounding surfaces. It is these, */
/*                             not the surfaces of minimum and maximum */
/*                             altitude relative to the body's reference */
/*                             ellipsoid, that are used for comparison. */

				    if (a > b) {
					zzellbds_(&a, &b, &bounds[5], &bounds[
						4], &eqhi, &polhi, &eqlow, &
						pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);
				    } else {

/*                                This is the prolate case. ZZELLBDS */
/*                                works only with oblate spheroids, */
/*                                so swap A and B on input. */

					zzellbds_(&b, &a, &bounds[5], &bounds[
						4], &eqhi, &polhi, &eqlow, &
						pollow);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                The EQ* outputs are actually for */
/*                                the polar radii of the bounding */
/*                                ellipsoids, and the POL* outputs */
/*                                are for the equatorial radii of */
/*                                the bounding ellipsoids. */

					swapd_(&eqhi, &polhi);
					swapd_(&eqlow, &pollow);
				    }

/*                             Multiply the small increments by 1 or */
/*                             -1 as needed, depending on whether they */
/*                             are used as offsets from upper or lower */
/*                             bounds. */

				    i__2 = m + 1;
				    lat = bounds[(i__1 = m + 1) < 6 && 0 <= 
					    i__1 ? i__1 : s_rnge("bounds", 
					    i__1, "f_zzinpdt0__", (ftnlen)521)
					    ] + aeps * pow_ii(&c_n1, &i__2);
				    if (l == 1) {
					lon = nrmmin + aeps;
				    } else {
					lon = nrmmax - aeps;
				    }

/*                             Find the point at the given longitude */
/*                             and latitude on the reference ellipsoid. */
/*                             We'll use this below. */

				    georec_(&lon, &lat, &c_b80, &a, &f, 
					    vertex);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    surfnm_(&a, &a, &b, vertex, normal);
				    if (n == 1) {

/*                                Find the surface point on the lower */
/*                                bounding ellipsoid, at the given */
/*                                planetodetic longitude and latitude */
/*                                (relative to the reference ellipsoid). */
					if (bounds[4] < 0.) {

/*                                   The direction to the inner bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqlow, &eqlow, &
						pollow, lowpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of LOWPT */
/*                                with respect to the reference */
/*                                ellipsoid. */

					recgeo_(lowpt, &a, &f, &lowlon, &
						lowlat, &lowalt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of LOWPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = lowalt + eps * pow_ii(&c_n1, &
						i__1);
				    } else {

/*                                This is the case for the outer */
/*                                bounding ellipsoid (N = 2). */

					if (bounds[5] < 0.) {

/*                                   The direction to the outer bounding */
/*                                   ellipsoid is "down." */

					    vminus_(normal, dir);
					} else {
					    vequ_(normal, dir);
					}
					surfpt_(vertex, dir, &eqhi, &eqhi, &
						polhi, highpt, &found);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                We should never fail to find an */
/*                                intercept. */

					chcksl_("FOUND", &found, &c_true, ok, 
						(ftnlen)5);

/*                                Find the geodetic coordinates of */
/*                                HIGHPT with respect to the reference */
/*                                ellipsoid. */

					recgeo_(highpt, &a, &f, &hilon, &
						hilat, &hialt);
					chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                                Adjust the altitude of LOWPT by a */
/*                                small increment. */

					i__1 = n + 1;
					alt = hialt + eps * pow_ii(&c_n1, &
						i__1);
				    }

/*                             Produce the perturbed input point using */
/*                             the perturbed geodetic coordinates. */

				    georec_(&lon, &lat, &alt, &a, &f, p);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt0_(p, &lon, bounds, corpar, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = TRUE_;
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; interi"
					    "or; excluded coordinate out of r"
					    "ange.", &c__1, title, (ftnlen)68, 
					    (ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             Use the perturbed point produced for */
/*                             the previous test case. Modify the */
/*                             excluded coordinate of the point. */

				    i__2 = m + 1;
				    lat = bounds[(i__1 = m + 1) < 6 && 0 <= 
					    i__1 ? i__1 : s_rnge("bounds", 
					    i__1, "f_zzinpdt0__", (ftnlen)670)
					    ] + aeps * pow_ii(&c_n1, &i__2);
				    if (exclud == 2) {

/*                                Latitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. */

/*                                However, we can't move the latitude */
/*                                too far out of range, or else the */
/*                                relationship between the altitude */
/*                                of the point and the bounding ellipsoid */
/*                                will change too much. */

					if (m == 1) {
/* Computing MAX */
					    d__1 = -halfpi_(), d__2 = lat - 
						    aeps * 2;
					    lat = max(d__1,d__2);
					} else {
/* Computing MIN */
					    d__1 = halfpi_(), d__2 = lat + 
						    aeps * 2;
					    lat = min(d__1,d__2);
					}
				    }

/*                             Set the initial altitude to an innocuous */
/*                             value. */

				    alt = (bounds[4] + bounds[5]) / 2;
				    if (exclud == 3) {

/*                                Altitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. */

					if (n == 1) {
					    alt = -min(a,b) / 10;
					} else {
					    alt = min(a,b) * 1.1;
					}
				    }
				    if (exclud == 1) {

/*                                Longitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. */

					if (n == 1) {
					    lon = nrmmin - aeps;
					} else {
					    lon = nrmmax + aeps;
					}
				    }
				    georec_(&lon, &lat, &alt, &a, &f, p);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    zzinpdt0_(p, &lon, bounds, corpar, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    xin = TRUE_;
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);

/* --- Case: ------------------------------------------------------ */

				    s_copy(title, stem, (ftnlen)300, (ftnlen)
					    300);
				    suffix_("Point near corner # # #; exteri"
					    "or; successor of excluded coordi"
					    "nate out of range.", &c__1, title,
					     (ftnlen)81, (ftnlen)300);
				    repmi_(title, "#", &l, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &m, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    repmi_(title, "#", &n, title, (ftnlen)300,
					     (ftnlen)1, (ftnlen)300);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				    tcase_(title, (ftnlen)300);

/*                             We're going to work with a modified */
/*                             version of the input bounds. This will */
/*                             make it easier to create coordinates that */
/*                             are slightly out of range. */

				    moved_(bounds, &c__6, modbds);

/*                             Use the perturbed point produced for */
/*                             the previous test case. Modify the */
/*                             excluded coordinate of the point. */

				    i__2 = m + 1;
				    lat = bounds[(i__1 = m + 1) < 6 && 0 <= 
					    i__1 ? i__1 : s_rnge("bounds", 
					    i__1, "f_zzinpdt0__", (ftnlen)779)
					    ] + aeps * pow_ii(&c_n1, &i__2);
				    if (exclud == 1) {

/*                                Longitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. We'll move the latitude */
/*                                out of bounds. */

/*                                However, we can't move the latitude */
/*                                too far out of range, or else the */
/*                                relationship between the altitude */
/*                                of the point and the bounding ellipsoid */
/*                                will change too much. */

					if (m == 1) {
/* Computing MAX */
					    d__1 = -halfpi_(), d__2 = lat - 
						    aeps * 2;
					    lat = max(d__1,d__2);
					} else {
/* Computing MIN */
					    d__1 = halfpi_(), d__2 = lat + 
						    aeps * 2;
					    lat = min(d__1,d__2);
					}
					latub = maxlat[(i__1 = latix2 - 1) < 
						6 && 0 <= i__1 ? i__1 : 
						s_rnge("maxlat", i__1, "f_zz"
						"inpdt0__", (ftnlen)803)] < 
						halfpi_();
					latlb = minlat[(i__1 = latix1 - 1) < 
						6 && 0 <= i__1 ? i__1 : 
						s_rnge("minlat", i__1, "f_zz"
						"inpdt0__", (ftnlen)804)] > 
						-halfpi_();
				    }
				    if (exclud == 2 || exclud == 0) {

/*                                Either all coordinates are considered, */
/*                                or latitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. We'll move the altitude */
/*                                out of bounds. */

					if (n == 1) {
					    alt = -min(a,b) / 10;
					} else {
					    alt = min(a,b) * 1.1;
					}
				    }
				    if (exclud == 3) {

/*                                Altitude is not considered in */
/*                                the bounds comparison performed */
/*                                by ZZINPDT0. We'll move the */
/*                                longitude out of bounds. */

/*                                To make it possible to create */
/*                                longitude values that are out */
/*                                of bounds, adjust the longitude */
/*                                bounds. We'll nudge the lower */
/*                                longitude bound upwards for this */
/*                                test. */

/*                                The magnitude of the "nudge" must */
/*                                be great enough to overcome the */
/*                                rounding margin used (even) by the */
/*                                "no margin" routine ZZINPDT0 for */
/*                                longitude comparisons. */

					modbds[0] += 1e-9;
					modbds[1] += -1e-9;
					if (n == 1) {
					    lon = nrmmin - 
						    9.9999999999999994e-12;
					} else {
					    lon = nrmmax + 
						    9.9999999999999994e-12;
					}
				    }
				    georec_(&lon, &lat, &alt, &a, &f, p);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             Perform the test using modified bounds. */
/*                             See the EXCLUD = 3 case above. */

				    zzinpdt0_(p, &lon, modbds, corpar, &
					    exclud, &inside);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                             By default, we expect the point to be */
/*                             outside the segment. */

				    xin = FALSE_;

/*                             Our latitude modifications won't work for */
/*                             the cases where the latitude is already */
/*                             at an extreme value. */

				    if (exclud == 1) {

/*                                Longitude is the excluded coordinate; */
/*                                this is a case for which we try to */
/*                                modify latitude. However, the */
/*                                attempted modification may have no */
/*                                effect if a boundary is already at */
/*                                the extreme value. */

					if (m == 1 && ! latlb) {
					    xin = TRUE_;
					} else if (m == 2 && ! latub) {
					    xin = TRUE_;
					}
				    } else {
					xin = FALSE_;
				    }
				    chcksl_("INSIDE", &inside, &xin, ok, (
					    ftnlen)6);
				}

/*                          End of "N" loop. N selects the altitude */
/*                          upper/lower bound. */

			    }

/*                       End of "M" loop. M selects the altitude */
/*                       upper/lower bound. */

			}

/*                    End of "L" loop. L selects the longitude */
/*                    upper/lower bound. */

		    }

/*                 End of coordinate exclusion (EXCLUD) loop. */

		}

/*              End of altitude bound loop. */

	    }

/*           End of upper latitude bound loop. */

	}

/*        End of lower latitude bound loop. */

    }

/*     End of longitude loop. */

/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzinpdt0__ */

