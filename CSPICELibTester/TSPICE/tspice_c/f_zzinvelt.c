/* f_zzinvelt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10 = 10;
static doublereal c_b5 = 2.;
static doublereal c_b6 = -2.;
static doublereal c_b7 = 7.;
static logical c_false = FALSE_;
static doublereal c_b35 = 0.;
static doublereal c_b65 = .5;
static logical c_true = TRUE_;
static integer c_n1 = -1;
static integer c__5 = 5;
static integer c__4 = 4;

/* $Procedure F_ZZINVELT ( ZZINVELT tests ) */
/* Subroutine */ int f_zzinvelt__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    extern /* Subroutine */ int zzinvelt_(doublereal *, integer *, doublereal 
	    *, doublereal *, doublereal *, integer *, logical *);
    static doublereal l, p[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), topen_(char *, ftnlen),
	     t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), georec_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen);
    static doublereal margin;
    static logical inside;
    static integer exclud;
    static doublereal bounds[6]	/* was [2][3] */, corpar[10];
    static integer corsys;
    static logical xin;

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZINVELT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZINVELT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 03-JUN-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZINVELT", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RECSYS: interior case. Exclude none. Zero MARGIN.", (ftnlen)49);
    corsys = 3;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = 0.;
    bounds[0] = 1.;
    bounds[1] = 3.;
    bounds[2] = -3.;
    bounds[3] = -1.;
    bounds[4] = 6.;
    bounds[5] = 8.;
    vpack_(&c_b5, &c_b6, &c_b7, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("RECSYS: interior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corsys = 3;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = .01;
    bounds[0] = 1.;
    bounds[1] = 3.;
    bounds[2] = -3.;
    bounds[3] = -1.;
    bounds[4] = 6.;
    bounds[5] = 8.;
    l = bounds[5] - bounds[4];
    d__1 = margin * .5 * l + 8.;
    vpack_(&c_b5, &c_b6, &d__1, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("RECSYS: exterior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corsys = 3;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = .01;
    bounds[0] = 1.;
    bounds[1] = 3.;
    bounds[2] = -3.;
    bounds[3] = -1.;
    bounds[4] = 6.;
    bounds[5] = 8.;
    l = bounds[5] - bounds[4];
    d__1 = margin * 1.5 * l + 8.;
    vpack_(&c_b5, &c_b6, &d__1, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("RECSYS: interior case. Exclude Z. MARGIN > 0.", (ftnlen)45);
    corsys = 3;
    cleard_(&c__10, corpar);
    exclud = 3;
    margin = .01;
    bounds[0] = 1.;
    bounds[1] = 3.;
    bounds[2] = -3.;
    bounds[3] = -1.;
    bounds[4] = 6.;
    bounds[5] = 8.;
    l = bounds[5] - bounds[4];
    d__1 = margin * 1.5 * l + 8.;
    vpack_(&c_b5, &c_b6, &d__1, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("LATSYS: interior case. Exclude none. Zero MARGIN.", (ftnlen)49);
    corsys = 1;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = 0.;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    vpack_(&c_b7, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("LATSYS: interior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corsys = 1;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * .5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("LATSYS: exterior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corsys = 1;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * 1.5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("LATSYS: interior case. Exclude radius. MARGIN > 0.", (ftnlen)50);
    corsys = 1;
    cleard_(&c__10, corpar);
    exclud = 3;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * 1.5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("PDTSYS: interior case. Exclude none. Zero MARGIN.", (ftnlen)49);
    corsys = 4;
    cleard_(&c__10, corpar);
    corpar[0] = 8.;
    corpar[1] = .5;
    exclud = 0;
    margin = 0.;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = -1.;
    bounds[5] = 1.;
    georec_(&c_b35, &c_b35, &c_b65, corpar, &corpar[1], p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("PDTSYS: interior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corpar[0] = 8.;
    corpar[1] = .5;
    exclud = 0;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = -1.;
    bounds[5] = 1.;
    l = bounds[5] - bounds[4];
    d__1 = l * .5 * margin + 1.;
    georec_(&c_b35, &c_b35, &d__1, corpar, &corpar[1], p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("PDTSYS: exterior case. Exclude none. MARGIN > 0.", (ftnlen)48);
    corsys = 4;
    cleard_(&c__10, corpar);
    corpar[0] = 8.;
    corpar[1] = .5;
    exclud = 0;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = -1.;
    bounds[5] = 1.;
    l = bounds[5] - bounds[4];
    d__1 = l * 1.5 * margin + 1.;
    georec_(&c_b35, &c_b35, &d__1, corpar, &corpar[1], p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = FALSE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("PDTSYS: interior case. Exclude altitude. MARGIN > 0.", (ftnlen)52)
	    ;
    corsys = 4;
    cleard_(&c__10, corpar);
    corpar[0] = 8.;
    corpar[1] = .5;
    exclud = 3;
    margin = .01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * 1.5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xin = TRUE_;
    chcksl_("INSIDE", &inside, &xin, ok, (ftnlen)6);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: Negative margin.", (ftnlen)23);
    corsys = 4;
    cleard_(&c__10, corpar);
    corpar[0] = 8.;
    corpar[1] = .5;
    exclud = 3;
    margin = -.01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * 1.5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: Invalid coordinate system code.", (ftnlen)38);
    margin = 0.;
    zzinvelt_(p, &c_n1, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);
    zzinvelt_(p, &c__5, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: Invalid exclude index.", (ftnlen)29);
    margin = 0.;
    exclud = -1;
    zzinvelt_(p, &c__4, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    exclud = 4;
    zzinvelt_(p, &c__4, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: invalid rectangular coordinate bounds.", (ftnlen)45);
    corsys = 3;
    cleard_(&c__10, corpar);
    exclud = 0;
    margin = 0.;
    bounds[0] = 1.;
    bounds[1] = -3.;
    bounds[2] = -3.;
    bounds[3] = -1.;
    bounds[4] = 6.;
    bounds[5] = 8.;
    vpack_(&c_b5, &c_b6, &c_b7, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: invalid planetodetic coordinate parameters.", (ftnlen)50);
    corsys = 4;
    cleard_(&c__10, corpar);
    corpar[0] = -8.;
    corpar[1] = .5;
    exclud = 3;
    margin = -.01;
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 4;
    bounds[4] = 6.;
    bounds[5] = 8.;
    d__1 = margin * 1.5 * 8. + 8.;
    vpack_(&d__1, &c_b35, &c_b35, p);
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    corpar[0] = 8.;
    corpar[1] = 1.1;
    zzinvelt_(p, &corsys, corpar, bounds, &margin, &exclud, &inside);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzinvelt__ */

