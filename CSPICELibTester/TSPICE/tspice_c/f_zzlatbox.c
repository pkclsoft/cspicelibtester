/* f_zzlatbox.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__3 = 3;
static integer c__6 = 6;
static doublereal c_b233 = .5;

/* $Procedure F_ZZLATBOX ( ZZLATBOX tests ) */
/* Subroutine */ int f_zzlatbox__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static doublereal xrad, xctr[3];
    extern /* Subroutine */ int t_tstbox__(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), 
	    zzlatbox_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), tcase_(char *, ftnlen), topen_(char *
	    , ftnlen);
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal lr;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal lt;
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static doublereal lz, center[3], radius, bounds[6]	/* was [2][3] */, tol,
	     xlr, xlt, xlz;

/* $ Abstract */

/*     Exercise the latitudinal volume element bounding */
/*     box routine ZZLATBOX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the DSK support routine ZZLATBOX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 03-FEB-2015 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZLATBOX", (ftnlen)10);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad longitude range: max is less than min by more than 2*pi.", (
	    ftnlen)60);
    bounds[0] = 0.;
    bounds[1] = -twopi_() - 1e-15;
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_true, "SPICE(BADLONGITUDERANGE)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Latitude bounds out of order", (ftnlen)28);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = halfpi_();
    bounds[3] = -halfpi_();
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_true, "SPICE(BADLATITUDEBOUNDS)", ok, (ftnlen)24);

/* --- Case: ------------------------------------------------------ */

    tcase_("Minimum latitude is too small", (ftnlen)29);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_() - 1e-10;
    bounds[3] = halfpi_();
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_true, "SPICE(BADLATITUDERANGE)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Maximum latitude is too large", (ftnlen)29);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_() + 1e-10;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_true, "SPICE(BADLATITUDERANGE)", ok, (ftnlen)23);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Trivial case: element is sphere.", (ftnlen)32);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, xctr);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    xrad = bounds[5] * sqrt(3.);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);
    xlr = bounds[5] * 2;
    xlt = xlr;
    xlz = xlr;
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is north polar cap with zero thickness.", (ftnlen)47);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = halfpi_() / 2;
    bounds[3] = halfpi_();
    bounds[4] = 20.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, xctr);
    xctr[2] = bounds[5] * ((sqrt(2.) / 2 + 1.) / 2);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    xlr = bounds[5] * sqrt(2.) / 2 * 2;
    xlt = xlr;
    xlz = bounds[5] * (1. - sqrt(2.) / 2);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is north polar cap with non-zero thickness.", (ftnlen)51);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = halfpi_() / 2;
    bounds[3] = halfpi_();
    bounds[4] = 19.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, xctr);
    xctr[2] = (bounds[5] + bounds[4] * sqrt(2.) / 2) / 2;
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    xlr = bounds[5] * sqrt(2.) / 2 * 2;
    xlt = xlr;
    xlz = bounds[5] - bounds[4] * sqrt(2.) / 2;
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is south polar cap with zero thickness.", (ftnlen)47);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = -halfpi_() / 2;
    bounds[4] = 20.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, xctr);
    xctr[2] = -bounds[5] * ((sqrt(2.) / 2 + 1.) / 2);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    xlr = bounds[5] * sqrt(2.) / 2 * 2;
    xlt = xlr;
    xlz = bounds[5] * (1. - sqrt(2.) / 2);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is south polar cap with non-zero thickness.", (ftnlen)51);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -halfpi_();
    bounds[3] = -halfpi_() / 2;
    bounds[4] = 19.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, xctr);
    xctr[2] = -(bounds[5] + bounds[4] * sqrt(2.) / 2) / 2;
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    xlr = bounds[5] * sqrt(2.) / 2 * 2;
    xlt = xlr;
    xlz = bounds[5] - bounds[4] * sqrt(2.) / 2;
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is chunk above the X-Y plane, lying over the +X axis.", (
	    ftnlen)61);
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = pi_() / 4;
    bounds[3] = pi_() / 3;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is chunk below the X-Y plane, lying under the +X axis.", (
	    ftnlen)62);
    bounds[0] = -pi_() / 3;
    bounds[1] = pi_() / 3;
    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is chunk above the X-Y plane, lying over the -X axis. Lo"
	    "ngitude extent is > pi.", (ftnlen)87);
    bounds[0] = pi_() / 3;
    bounds[1] = -pi_() / 3;
    bounds[2] = pi_() / 4;
    bounds[3] = pi_() / 3;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is chunk below the X-Y plane, lying under the -X axis. L"
	    "ongitude extent is > pi.", (ftnlen)88);
    bounds[0] = pi_() / 3;
    bounds[1] = -pi_() / 3;
    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is an equatorial belt that is symmetric about the X-Y pl"
	    "ane.", (ftnlen)68);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is an equatorial belt that is asymmetric about the X-Y p"
	    "lane.", (ftnlen)69);
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 3;
    bounds[4] = 10.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is a slice ranging from the north to the south pole. Lon"
	    "gitude extent is < pi.", (ftnlen)86);
    bounds[0] = pi_() * 3 / 4;
    bounds[1] = pi_() * -3 / 4;
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    bounds[4] = 1.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is a slice ranging from the north to the south pole. Lon"
	    "gitude extent is > pi.", (ftnlen)86);
    bounds[0] = pi_() / 4;
    bounds[1] = -pi_() / 4;
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    bounds[4] = 1.;
    bounds[5] = 20.;
    zzlatbox_(bounds, center, &lr, &lt, &lz, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate expected outputs. */

    t_tstbox__(bounds, xctr, &xlr, &xlt, &xlz, &xrad);
    tol = 1e-12;
    chckad_("CENTER", center, "~", xctr, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    1);
    chcksd_("LT", &lt, "~", &xlt, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LR", &lr, "~", &xlr, &tol, ok, (ftnlen)2, (ftnlen)1);
    chcksd_("LZ", &lz, "~", &xlz, &tol, ok, (ftnlen)2, (ftnlen)1);
/* Computing 2nd power */
    d__1 = xlr / 2;
/* Computing 2nd power */
    d__2 = xlt / 2;
/* Computing 2nd power */
    d__3 = xlz / 2;
    xrad = sqrt(d__1 * d__1 + d__2 * d__2 + d__3 * d__3);
    chcksd_("RADIUS", &radius, "~", &xrad, &tol, ok, (ftnlen)6, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzlatbox__ */


/*     Utility routine T_TSTBOX: create latitude bounding */
/*     box for testing. The bounds of segment may span the */
/*     X-Y plane. */

/* Subroutine */ int t_tstbox__(doublereal *bounds, doublereal *center, 
	doublereal *lr, doublereal *lt, doublereal *lz, doublereal *radius)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal diag[3], minz, maxz, h__;
    integer i__;
    doublereal r__;
    extern /* Subroutine */ int chkin_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal minrc, maxrc;
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *);
    extern doublereal vnorm_(doublereal *);
    doublereal r1, r2, locbds[6]	/* was [2][3] */;
    extern /* Subroutine */ int reccyl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), cylrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal minlat, maxlat;
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    doublereal extent[6]	/* was [3][2] */;
    extern logical return_(void);
    doublereal ctr[6]	/* was [3][2] */, lon;
    extern /* Subroutine */ int t_mkbox__(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_TSTBOX", (ftnlen)8);
    minlat = bounds[2];
    maxlat = bounds[3];
    if (minlat >= 0. || maxlat < 0.) {

/*        The volume element doesn't cross the X-Y plane. */
/*        We can delegate the job. */

	t_mkbox__(bounds, center, lr, lt, lz, radius);
	chkout_("T_TSTBOX", (ftnlen)8);
	return 0;
    }

/*     The volume element crosses the X-Y plane. Divide the */
/*     element into upper and lower parts; find bounding boxes */
/*     for each. */

    for (i__ = 1; i__ <= 2; ++i__) {
	moved_(bounds, &c__6, locbds);
	locbds[(i__1 = i__ + 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("locbds", 
		i__1, "t_tstbox__", (ftnlen)833)] = 0.;
	t_mkbox__(locbds, &ctr[(i__1 = i__ * 3 - 3) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("ctr", i__1, "t_tstbox__", (ftnlen)835)], &extent[(
		i__2 = i__ * 3 - 3) < 6 && 0 <= i__2 ? i__2 : s_rnge("extent",
		 i__2, "t_tstbox__", (ftnlen)835)], &extent[(i__3 = i__ * 3 - 
		2) < 6 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "t_tstbo"
		"x__", (ftnlen)835)], &extent[(i__4 = i__ * 3 - 1) < 6 && 0 <= 
		i__4 ? i__4 : s_rnge("extent", i__4, "t_tstbox__", (ftnlen)
		835)], &r__);
    }

/*     At this point the longitude of the center and the tangential */
/*     extent of the box are known. We'll need to recompute the */
/*     radius and Z component of the center and the radial and Z */
/*     extents of the box. */

    *lt = extent[1];

/*     Start with the Z values. Recall that the two boxes */
/*     are bounded by the X-Y plane, which simplifies our */
/*     calculations. The box at index 1 is the upper box. */

    maxz = extent[2];
    minz = -extent[5];
/* Computing MAX */
    d__1 = 0., d__2 = maxz - minz;
    *lz = max(d__1,d__2);
    center[2] = maxz - *lz / 2;

/*     Both boxes have the maximum radial component, so we can */
/*     compute this component using the first box alone. The */
/*     minimum radial component must be derived using both boxes. */

/*     Note the radial component of the box center is always */
/*     positive, and the longitude of the box center is always */
/*     the same as that of the midpoint of the centers of any */
/*     arc of constant latitude on the outer bounding sphere. */

    reccyl_(ctr, &r1, &lon, &h__);
    maxrc = r1 + extent[0] / 2;
    reccyl_(&ctr[3], &r2, &lon, &h__);
/* Computing MIN */
    d__1 = r1 - extent[0] / 2, d__2 = r2 - extent[3] / 2;
    minrc = min(d__1,d__2);
/* Computing MAX */
    d__1 = 0., d__2 = maxrc - minrc;
    *lr = max(d__1,d__2);
    h__ = center[2];
    d__1 = maxrc - *lr / 2;
    cylrec_(&d__1, &lon, &h__, center);
    d__1 = *lt / 2;
    d__2 = *lr / 2;
    d__3 = *lz / 2;
    vpack_(&d__1, &d__2, &d__3, diag);
    *radius = vnorm_(diag);
    chkout_("T_TSTBOX", (ftnlen)8);
    return 0;
} /* t_tstbox__ */


/*     Utility routine T_MKBOX: create box using coordinates */
/*     of the corners of a volume element. This technique */
/*     is valid only for "small" elements, for which the */
/*     bounding box is not tangent at interior points of */
/*     the elements' surface. Note that this routine cannot */
/*     be used for elements that cross the X-Y plane. */

/* Subroutine */ int t_mkbox__(doublereal *bounds, doublereal *center, 
	doublereal *lr, doublereal *lt, doublereal *lz, doublereal *radius)
{
    /* Initialized data */

    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double cos(doublereal);

    /* Local variables */
    doublereal diag[3], dlon;
    extern doublereal vdot_(doublereal *, doublereal *);
    doublereal minz, maxz;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    );
    integer i__, j, k;
    doublereal r__;
    extern /* Subroutine */ int chkin_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    extern doublereal dpmin_(void);
    doublereal minrc;
    extern doublereal dpmax_(void);
    doublereal maxrc, midpt[3];
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *), ucrss_(doublereal *, doublereal *,
	     doublereal *);
    extern doublereal vnorm_(doublereal *), twopi_(void);
    extern /* Subroutine */ int nvp2pl_(doublereal *, doublereal *, 
	    doublereal *);
    extern doublereal pi_(void);
    doublereal midarc[3], cplane[4], radvec[3];
    extern /* Subroutine */ int latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *), cylrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal midlon, minlat, corner[24]	/* was [3][2][2][2] */, 
	    normal[3], minlon;
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    doublereal maxlon;
    extern logical return_(void);
    doublereal lat, lon;


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_MKBOX", (ftnlen)7);
    minlat = bounds[2];
    minlon = bounds[0];
    maxlon = bounds[1];
    if (maxlon <= minlon) {
	maxlon += twopi_();
    }
    dlon = maxlon - minlon;
    midlon = minlon + dlon / 2;

/*     Compute Cartesian coordinates of the box corners. */

    for (i__ = 1; i__ <= 2; ++i__) {
	lon = bounds[(i__1 = i__ - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("boun"
		"ds", i__1, "t_mkbox__", (ftnlen)988)];
	for (j = 1; j <= 2; ++j) {
	    lat = bounds[(i__1 = j + 1) < 6 && 0 <= i__1 ? i__1 : s_rnge(
		    "bounds", i__1, "t_mkbox__", (ftnlen)992)];
	    for (k = 1; k <= 2; ++k) {
		r__ = bounds[(i__1 = k + 3) < 6 && 0 <= i__1 ? i__1 : s_rnge(
			"bounds", i__1, "t_mkbox__", (ftnlen)996)];
		latrec_(&r__, &lon, &lat, &corner[(i__1 = (i__ + (j + (k << 1)
			 << 1)) * 3 - 21) < 24 && 0 <= i__1 ? i__1 : s_rnge(
			"corner", i__1, "t_mkbox__", (ftnlen)998)]);
	    }
	}
    }

/*     Pick the latitude of the segment so that its length */
/*     is the transverse length of the box. */

    if (minlat >= 0.) {

/*        Use the lower pair of corners on the sphere of maximum */
/*        radius. */

	i__ = 1;
    } else {
	i__ = 2;
    }

/*     Find the midpoint of the latitude boundary of maximum */
/*     radius that is closest to the X-Y plane. This is the */
/*     point with the greatest component in the radial direction. */

    latrec_(&bounds[5], &midlon, &bounds[(i__1 = i__ + 1) < 6 && 0 <= i__1 ? 
	    i__1 : s_rnge("bounds", i__1, "t_mkbox__", (ftnlen)1025)], midarc)
	    ;

/*     Create a central plane for the box: this plane contains */
/*     the midpoint of a line segment connecting a pair of */
/*     corners at the same latitude and radius, and is normal */
/*     to the segment. */
    vsub_(&corner[(i__1 = ((i__ + 4 << 1) + 2) * 3 - 21) < 24 && 0 <= i__1 ? 
	    i__1 : s_rnge("corner", i__1, "t_mkbox__", (ftnlen)1033)], &
	    corner[(i__2 = ((i__ + 4 << 1) + 1) * 3 - 21) < 24 && 0 <= i__2 ? 
	    i__2 : s_rnge("corner", i__2, "t_mkbox__", (ftnlen)1033)], normal)
	    ;
    vlcom_(&c_b233, &corner[15], &c_b233, &corner[12], midpt);
    nvp2pl_(normal, midpt, cplane);
    if (dlon <= pi_()) {

/*        The segment connecting the two corners selected above */
/*        has length equal to the tangential extent of the element. */

	*lt = vnorm_(normal);
    } else {

/*        The widest part of the element is at the longitudes 90 degrees */
/*        away from MIDLON. The width is just the width of the outer */
/*        circle formed by projecting the element orthogonally onto the */
/*        X-Y plane. */

	*lt = bounds[5] * 2. * cos(bounds[(i__1 = i__ + 1) < 6 && 0 <= i__1 ? 
		i__1 : s_rnge("bounds", i__1, "t_mkbox__", (ftnlen)1055)]);
    }

/*     For each corner on the volume face of minimum longitude, */
/*     compute the minimum and maximum Z values of the corners. */

    minz = dpmax_();
    maxz = dpmin_();
    for (j = 1; j <= 2; ++j) {
	for (k = 1; k <= 2; ++k) {
/* Computing MIN */
	    d__1 = minz, d__2 = corner[(i__1 = ((j + (k << 1) << 1) + 1) * 3 
		    - 19) < 24 && 0 <= i__1 ? i__1 : s_rnge("corner", i__1, 
		    "t_mkbox__", (ftnlen)1070)];
	    minz = min(d__1,d__2);
/* Computing MAX */
	    d__1 = maxz, d__2 = corner[(i__1 = ((j + (k << 1) << 1) + 1) * 3 
		    - 19) < 24 && 0 <= i__1 ? i__1 : s_rnge("corner", i__1, 
		    "t_mkbox__", (ftnlen)1071)];
	    maxz = max(d__1,d__2);
	}
    }

/*     Compute the Z extent of the element. */

/* Computing MAX */
    d__1 = maxz - minz;
    *lz = max(d__1,0.);

/*     Let RADVEC be a unit vector in the radial direction. */

    ucrss_(normal, z__, radvec);

/*     Find the component of MIDARC in the RADVEC (radial) direction. */

    maxrc = vdot_(midarc, radvec);
    if (dlon < pi_()) {

/*        For the corners on the intersection of the volume face of */
/*        minimum longitude and sphere of minimum radius, compute the */
/*        corners' components in the radial direction. Select the */
/*        minimum component. */

/* Computing MIN */
	d__1 = vdot_(corner, radvec), d__2 = vdot_(&corner[6], radvec);
	minrc = min(d__1,d__2);
    } else {

/*        The element "wraps" around the Z axis; the points having */
/*        minimum tangential components lie on the outer sphere. */

/* Computing MIN */
	d__1 = vdot_(&corner[12], radvec), d__2 = vdot_(&corner[18], radvec);
	minrc = min(d__1,d__2);
    }

/*     We now have the extent of the element in the radial direction. */

/* Computing MAX */
    d__1 = maxrc - minrc;
    *lr = max(d__1,0.);

/*     MIDARC lies on the central, vertical plane of the box, at the end */
/*     of maximum tangential component. We can derive the coordinates of */
/*     the center of the box from MIDARC, MINZ, and the box extents. */

    d__1 = maxrc - *lr / 2;
    d__2 = minz + *lz / 2;
    cylrec_(&d__1, &midlon, &d__2, center);
    d__1 = *lt / 2;
    d__2 = *lr / 2;
    d__3 = *lz / 2;
    vpack_(&d__1, &d__2, &d__3, diag);
    *radius = vnorm_(diag);
    chkout_("T_MKBOX", (ftnlen)7);
    return 0;
} /* t_mkbox__ */

