/* f_zzmkspin.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10000 = 10000;
static integer c__20000 = 20000;
static logical c_false = FALSE_;
static integer c__60000 = 60000;
static integer c_b10 = 100000;
static integer c__0 = 0;
static integer c_b30 = 100000001;
static integer c__60001 = 60001;
static integer c_b48 = 100001;
static integer c__6 = 6;
static integer c_n1 = -1;
static logical c_true = TRUE_;

/* $Procedure      F_ZZMKSPIN ( Test ZZMKSPIN ) */
/* Subroutine */ int f_zzmkspin__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer ncgr, nlat, nlon;
    extern integer zzvox2id_(integer *, integer *);
    static integer nvox[3], vptr;
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__;
    extern /* Subroutine */ int zzmkspin_(integer *, integer *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, integer *, doublereal *, doublereal *, integer *, integer *, 
	    integer *, integer *, integer *, doublereal *, integer *);
    static integer i__, j, k;
    static char label[40];
    extern /* Subroutine */ int zzgetvox_(doublereal *, doublereal *, integer 
	    *, doublereal *, logical *, integer *);
    static integer r__, cgoff[3];
    extern /* Subroutine */ int zzvoxcvo_(integer *, integer *, integer *, 
	    integer *, integer *, integer *), tcase_(char *, ftnlen);
    static integer cells[200000]	/* was [2][100000] */;
    extern doublereal dpmin_(void), dpmax_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical inbox;
    static integer pltco;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer voxid, hivox[3], vrtco;
    static doublereal verts[30000]	/* was [3][10000] */;
    static integer cgof1d;
    static doublereal lowpt[3];
    static integer cgxyz[3];
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    static doublereal ub;
    static integer np;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    static integer nv, nx;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen);
    static doublereal finscl;
    static logical includ;
    extern integer isrchi_(integer *, integer *, integer *);
    static doublereal highpt[3];
    static integer corscl, plates[60000]	/* was [3][20000] */;
    static doublereal mdltol, extent[6]	/* was [2][3] */;
    static integer cg3;
    static doublereal vtxbds[6]	/* was [2][3] */;
    static integer cgrptr[100000], cvoxid, ncgvox[3];
    static doublereal pltext[6]	/* was [2][3] */, voxori[3], voxsiz;
    static integer lowvox[3], nvxlst, nvxptr, nvxtot, voxcor[3], voxptr[60000]
	    , vxlist[100000], cgp, pid, loc;
    static doublereal tol;
    static integer pix, nvp, vix, nxy;

/* $ Abstract */

/*     Test the SPICELIB routine ZZMKSPIN. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZMKSPIN. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 08-AUG-2016  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZMKSPIN", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Set-up: create plates for a tessellated ellipsoid. NLON = 100; N"
	    "LAT = 50; NV = 4902; NP = 9800.", (ftnlen)95);
    a = 3e3;
    b = 2e3;
    c__ = 1e3;
    nlon = 100;
    nlat = 50;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, verts, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Setup: create spatial index using the plate set from the previou"
	    "s case. Call ZZMKSPIN directly.", (ftnlen)95);

/*     Set voxel scales. */

    finscl = 2.;
    corscl = 4;
    zzmkspin_(&np, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &c_b10,
	     cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &nvxlst, 
	    vxlist, extent, cgrptr);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/*     Check the outputs from ZZMKSPIN. */



/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel grid dimensions.", (ftnlen)28);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "voxel grid dimension @", (ftnlen)40, (ftnlen)22);
	repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &nvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("nvox", i__1, "f_zzmkspin__", (ftnlen)287)], ">", &
		c__0, &c__0, ok, (ftnlen)40, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check total voxel count.", (ftnlen)24);
    j = nvox[0] * nvox[1] * nvox[2];
    chcksi_("NVXTOT", &nvxtot, "=", &j, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NVXTOT", &nvxtot, "<", &c_b30, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel pointer array count.", (ftnlen)32);
    chcksi_("NVXPTR", &nvxptr, ">", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NVXPTR", &nvxptr, "<", &c__60001, &c__0, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check voxel list count.", (ftnlen)23);
    chcksi_("NVXLST", &nvxlst, ">", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksi_("NVXLST", &nvxlst, "<", &c_b48, &c__0, ok, (ftnlen)6, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Check the model extents.", (ftnlen)24);

/*     The extents are the minimum and maximum values of each vertex */
/*     coordinate, taken over all vertices. Compute the expected */
/*     extents. */

    for (i__ = 1; i__ <= 3; ++i__) {
	vtxbds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_zzmkspin__", (ftnlen)334)] = dpmax_();
	vtxbds[(i__1 = (i__ << 1) - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("vtx"
		"bds", i__1, "f_zzmkspin__", (ftnlen)335)] = dpmin_();
    }
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {
/* Computing MIN */
	    d__1 = vtxbds[(i__3 = (j << 1) - 2) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_zzmkspin__", (ftnlen)343)], 
		    d__2 = verts[(i__4 = j + i__ * 3 - 4) < 30000 && 0 <= 
		    i__4 ? i__4 : s_rnge("verts", i__4, "f_zzmkspin__", (
		    ftnlen)343)];
	    vtxbds[(i__2 = (j << 1) - 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_zzmkspin__", (ftnlen)343)] = min(d__1,
		    d__2);
/* Computing MAX */
	    d__1 = vtxbds[(i__3 = (j << 1) - 1) < 6 && 0 <= i__3 ? i__3 : 
		    s_rnge("vtxbds", i__3, "f_zzmkspin__", (ftnlen)344)], 
		    d__2 = verts[(i__4 = j + i__ * 3 - 4) < 30000 && 0 <= 
		    i__4 ? i__4 : s_rnge("verts", i__4, "f_zzmkspin__", (
		    ftnlen)344)];
	    vtxbds[(i__2 = (j << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "vtxbds", i__2, "f_zzmkspin__", (ftnlen)344)] = max(d__1,
		    d__2);
	}
    }
    tol = 1e-12;
    chckad_("EXTENT", extent, "~~/", vtxbds, &c__6, &tol, ok, (ftnlen)6, (
	    ftnlen)3);

/* --- Case -------------------------------------------------------- */

    tcase_("Check the voxel grid origin: compare to vertex extents.", (ftnlen)
	    55);

/*     Each element of the origin should have a lower value than the */
/*     minimum vertex extent in the same coordinate. */

    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "voxel origin @", (ftnlen)40, (ftnlen)14);
	repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-12;
	chcksd_(label, &voxori[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : 
		s_rnge("voxori", i__1, "f_zzmkspin__", (ftnlen)372)], "<", &
		vtxbds[(i__2 = (i__ << 1) - 2) < 6 && 0 <= i__2 ? i__2 : 
		s_rnge("vtxbds", i__2, "f_zzmkspin__", (ftnlen)372)], &tol, 
		ok, (ftnlen)40, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Check the voxel grid boundaries: compare to vertex extents.", (
	    ftnlen)59);

/*     The voxel grid should enclose the smallest box that includes all */
/*     vertices. We need check only the upper bounds of the coordinates */
/*     of the grid's corners. */


    for (i__ = 1; i__ <= 3; ++i__) {
	s_copy(label, "voxel grid upper bound @", (ftnlen)40, (ftnlen)24);
	repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tol = 1e-12;
	ub = voxori[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("voxori"
		, i__1, "f_zzmkspin__", (ftnlen)396)] + voxsiz * nvox[(i__2 = 
		i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("nvox", i__2, "f_z"
		"zmkspin__", (ftnlen)396)];
	chcksd_(label, &ub, ">", &vtxbds[(i__1 = (i__ << 1) - 1) < 6 && 0 <= 
		i__1 ? i__1 : s_rnge("vtxbds", i__1, "f_zzmkspin__", (ftnlen)
		398)], &tol, ok, (ftnlen)40, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Test coarse grid pointer array: make sure all pointers are in th"
	    "e range 0:NVXPTR.", (ftnlen)81);

/*     Compute the size of the coarse grid. */

/* Computing 3rd power */
    i__1 = corscl;
    cg3 = i__1 * (i__1 * i__1);
    ncgr = nvxtot / cg3;
    j = 0;
    i__1 = ncgr;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "coarse grid pointer @", (ftnlen)40, (ftnlen)21);
	repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &cgrptr[(i__2 = i__ - 1) < 100000 && 0 <= i__2 ? i__2 :
		 s_rnge("cgrptr", i__2, "f_zzmkspin__", (ftnlen)424)], ">", &
		c_n1, &c__0, ok, (ftnlen)40, (ftnlen)1);
	i__3 = nvxptr + 1;
	chcksi_(label, &cgrptr[(i__2 = i__ - 1) < 100000 && 0 <= i__2 ? i__2 :
		 s_rnge("cgrptr", i__2, "f_zzmkspin__", (ftnlen)425)], "<", &
		i__3, &c__0, ok, (ftnlen)40, (ftnlen)1);
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Test voxel-plate pointer array: make sure all pointers are eithe"
	    "r -1 or are in the range 1:NVXLST.", (ftnlen)98);
    i__1 = nvxptr;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "voxel list pointer @", (ftnlen)40, (ftnlen)20);
	repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (voxptr[(i__2 = i__ - 1) < 60000 && 0 <= i__2 ? i__2 : s_rnge(
		"voxptr", i__2, "f_zzmkspin__", (ftnlen)445)] != -1) {
	    chcksi_(label, &voxptr[(i__2 = i__ - 1) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("voxptr", i__2, "f_zzmkspin__", (ftnlen)447)
		    ], ">", &c__0, &c__0, ok, (ftnlen)40, (ftnlen)1);
	    i__3 = nvxlst + 1;
	    chcksi_(label, &voxptr[(i__2 = i__ - 1) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("voxptr", i__2, "f_zzmkspin__", (ftnlen)448)
		    ], "<", &i__3, &c__0, ok, (ftnlen)40, (ftnlen)1);
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Test plate counts in voxel-plate list: make sure all counts are "
	    "in the range 1:NP.", (ftnlen)82);

/*     Access the plate counts using the voxel pointer array. */

    i__1 = nvxptr;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = voxptr[(i__2 = i__ - 1) < 60000 && 0 <= i__2 ? i__2 : s_rnge(
		"voxptr", i__2, "f_zzmkspin__", (ftnlen)467)];
	if (j > 0) {
	    k = vxlist[(i__2 = j - 1) < 100000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vxlist", i__2, "f_zzmkspin__", (ftnlen)471)];
	    s_copy(label, "plate count for voxel pointer @", (ftnlen)40, (
		    ftnlen)31);
	    repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &k, ">", &c__0, &c__0, ok, (ftnlen)40, (ftnlen)1);
	    i__2 = np + 1;
	    chcksi_(label, &k, "<", &i__2, &c__0, ok, (ftnlen)40, (ftnlen)1);
	}
    }

/*     Below we test the indexing mechanism as a unit. We test the */
/*     voxel-plate associations defined by the coarse grid pointers, */
/*     the voxel pointer array, and the voxel-plate list. */


/* --- Case -------------------------------------------------------- */

    tcase_("Test the voxel-plate map: look up the plate list for each voxel."
	    " Check each plate to make sure its minimal bounding box intersec"
	    "ts the voxel.", (ftnlen)141);

/*     Define some variables needed for voxel ID-to-coordinate */
/*     mapping. */

    nx = nvox[0];
    nxy = nvox[1] * nx;

/*     NCGVOX contains the coarse grid dimensions. */

    for (i__ = 1; i__ <= 3; ++i__) {
	ncgvox[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("ncgvox", 
		i__1, "f_zzmkspin__", (ftnlen)510)] = nvox[(i__2 = i__ - 1) < 
		3 && 0 <= i__2 ? i__2 : s_rnge("nvox", i__2, "f_zzmkspin__", (
		ftnlen)510)] / corscl;
    }

/*     MDLTOL is a tolerance used to expand the extent of a plate */
/*     for the purpose of computing the set of voxels it intersects. */

    mdltol = voxsiz * .001;
    i__1 = nvxtot;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Map the Ith fine voxel to its plate list, if the list is */
/*        non-empty. */


/*        Get the 3-d coordinates of the Ith voxel. */

	voxcor[2] = (i__ - 1) / nxy + 1;
	r__ = i__ - (voxcor[2] - 1) * nxy;
	voxcor[1] = (r__ - 1) / nx + 1;
	voxcor[0] = r__ - (voxcor[1] - 1) * nx;

/*        Get the coordinates of the coarse voxel containing the Ith */
/*        fine voxel. Also get the offsets of the fine voxel from the */
/*        base of the enclosing coarse voxel, expressed in both 3D and */
/*        1D. Note that the offsets are 1-based. */

	zzvoxcvo_(voxcor, nvox, &corscl, cgxyz, cgoff, &cgof1d);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Get the 1-D coarse grid index corresponding to CGXYZ. */

	j = zzvox2id_(cgxyz, ncgvox);

/*        Get the coarse grid pointer for the current voxel. */

	cgp = cgrptr[(i__2 = j - 1) < 100000 && 0 <= i__2 ? i__2 : s_rnge(
		"cgrptr", i__2, "f_zzmkspin__", (ftnlen)554)];
	if (cgp > 0) {

/*           The pointer is non-null, so this coarse voxel is non-empty. */
/*           (That is, some plates intersect it.) */

/*           CGP is the index of the first entry in the voxel pointer */
/*           array for the current coarse voxel. Get the index of the */
/*           entry for the current fine voxel, and look up the pointer */
/*           at that index. */

	    k = cgp + cgof1d - 1;

/*           Make sure K is in range. */

	    chcksi_("K", &k, ">", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    i__2 = nvxptr + 1;
	    chcksi_("K", &k, "<", &i__2, &c__0, ok, (ftnlen)1, (ftnlen)1);
	    vptr = voxptr[(i__2 = k - 1) < 60000 && 0 <= i__2 ? i__2 : s_rnge(
		    "voxptr", i__2, "f_zzmkspin__", (ftnlen)575)];
	    if (vptr > 0) {

/*              This voxel is non-empty. */

/*              Let NVP be the plate count for this voxel. */

		nvp = vxlist[(i__2 = vptr - 1) < 100000 && 0 <= i__2 ? i__2 : 
			s_rnge("vxlist", i__2, "f_zzmkspin__", (ftnlen)583)];

/*              Check each plate. */

		i__2 = nvp;
		for (pix = 1; pix <= i__2; ++pix) {

/*                 Compute the extents of the vertices of the current */
/*                 plate. */

		    pid = vxlist[(i__3 = vptr + pix - 1) < 100000 && 0 <= 
			    i__3 ? i__3 : s_rnge("vxlist", i__3, "f_zzmkspin"
			    "__", (ftnlen)592)];
		    for (pltco = 1; pltco <= 3; ++pltco) {
			pltext[(i__3 = (pltco << 1) - 2) < 6 && 0 <= i__3 ? 
				i__3 : s_rnge("pltext", i__3, "f_zzmkspin__", 
				(ftnlen)596)] = dpmax_();
			pltext[(i__3 = (pltco << 1) - 1) < 6 && 0 <= i__3 ? 
				i__3 : s_rnge("pltext", i__3, "f_zzmkspin__", 
				(ftnlen)597)] = dpmin_();
		    }
		    for (pltco = 1; pltco <= 3; ++pltco) {
			vix = plates[(i__3 = pltco + pid * 3 - 4) < 60000 && 
				0 <= i__3 ? i__3 : s_rnge("plates", i__3, 
				"f_zzmkspin__", (ftnlen)603)];
			for (vrtco = 1; vrtco <= 3; ++vrtco) {
/* Computing MIN */
			    d__1 = verts[(i__4 = vrtco + vix * 3 - 4) < 30000 
				    && 0 <= i__4 ? i__4 : s_rnge("verts", 
				    i__4, "f_zzmkspin__", (ftnlen)608)], d__2 
				    = pltext[(i__5 = (vrtco << 1) - 2) < 6 && 
				    0 <= i__5 ? i__5 : s_rnge("pltext", i__5, 
				    "f_zzmkspin__", (ftnlen)608)];
			    pltext[(i__3 = (vrtco << 1) - 2) < 6 && 0 <= i__3 
				    ? i__3 : s_rnge("pltext", i__3, "f_zzmks"
				    "pin__", (ftnlen)608)] = min(d__1,d__2);
/* Computing MAX */
			    d__1 = verts[(i__4 = vrtco + vix * 3 - 4) < 30000 
				    && 0 <= i__4 ? i__4 : s_rnge("verts", 
				    i__4, "f_zzmkspin__", (ftnlen)611)], d__2 
				    = pltext[(i__5 = (vrtco << 1) - 1) < 6 && 
				    0 <= i__5 ? i__5 : s_rnge("pltext", i__5, 
				    "f_zzmkspin__", (ftnlen)611)];
			    pltext[(i__3 = (vrtco << 1) - 1) < 6 && 0 <= i__3 
				    ? i__3 : s_rnge("pltext", i__3, "f_zzmks"
				    "pin__", (ftnlen)611)] = max(d__1,d__2);
			}
		    }

/*                 Let LOWPT and HIGHPT be the coordinates of the corners */
/*                 of the box defined by the plate extents, where LOWPT */
/*                 contains the minimum coordinate values and HIGHPT */
/*                 contains the maximum values. */

/*                 Take tolerance into account. */

		    for (vrtco = 1; vrtco <= 3; ++vrtco) {
			lowpt[(i__3 = vrtco - 1) < 3 && 0 <= i__3 ? i__3 : 
				s_rnge("lowpt", i__3, "f_zzmkspin__", (ftnlen)
				627)] = pltext[(i__4 = (vrtco << 1) - 2) < 6 
				&& 0 <= i__4 ? i__4 : s_rnge("pltext", i__4, 
				"f_zzmkspin__", (ftnlen)627)] - mdltol;
			highpt[(i__3 = vrtco - 1) < 3 && 0 <= i__3 ? i__3 : 
				s_rnge("highpt", i__3, "f_zzmkspin__", (
				ftnlen)628)] = pltext[(i__4 = (vrtco << 1) - 
				1) < 6 && 0 <= i__4 ? i__4 : s_rnge("pltext", 
				i__4, "f_zzmkspin__", (ftnlen)628)] + mdltol;
		    }

/*                 Map the corner's model coordinates to voxel */
/*                 coordinates. */

		    zzgetvox_(&voxsiz, voxori, nvox, lowpt, &inbox, lowvox);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    zzgetvox_(&voxsiz, voxori, nvox, highpt, &inbox, hivox);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 The Ith voxel should be in the set bounded by LOWVOX */
/*                 and HIVOX. */

		    includ = TRUE_;
		    for (vrtco = 1; vrtco <= 3; ++vrtco) {
			includ = includ && voxcor[(i__3 = vrtco - 1) < 3 && 0 
				<= i__3 ? i__3 : s_rnge("voxcor", i__3, "f_z"
				"zmkspin__", (ftnlen)652)] >= lowvox[(i__4 = 
				vrtco - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
				"lowvox", i__4, "f_zzmkspin__", (ftnlen)652)] 
				&& voxcor[(i__5 = vrtco - 1) < 3 && 0 <= i__5 
				? i__5 : s_rnge("voxcor", i__5, "f_zzmkspin__"
				, (ftnlen)652)] <= hivox[(i__6 = vrtco - 1) < 
				3 && 0 <= i__6 ? i__6 : s_rnge("hivox", i__6, 
				"f_zzmkspin__", (ftnlen)652)];
		    }

/*                 We'd like to think that each plate on the voxel's */
/*                 plate list does intersect that voxel. But maybe it */
/*                 doesn't... */

		    if (! includ) {
			s_copy(label, "Plate @ intersects voxel @", (ftnlen)
				40, (ftnlen)26);
			repmi_(label, "@", &pid, label, (ftnlen)40, (ftnlen)1,
				 (ftnlen)40);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			repmi_(label, "@", &i__, label, (ftnlen)40, (ftnlen)1,
				 (ftnlen)40);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksl_(label, &includ, &c_true, ok, (ftnlen)40);
		    }
		}
	    }
	}
    }

/* --- Case -------------------------------------------------------- */

    tcase_("Test the voxel-plate map: find the set of  voxels touched by eac"
	    "h plate. Check each voxel associated with a given plate to verif"
	    "y that the plate is on that voxel's plate list.", (ftnlen)175);

/*     We repeat some of the code above...someday some more private */
/*     routines should be written to handle these computations. */

    i__1 = np;
    for (pid = 1; pid <= i__1; ++pid) {

/*        Find the extents of the Ith plate. */

	for (pltco = 1; pltco <= 3; ++pltco) {
	    pltext[(i__2 = (pltco << 1) - 2) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "pltext", i__2, "f_zzmkspin__", (ftnlen)704)] = dpmax_();
	    pltext[(i__2 = (pltco << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "pltext", i__2, "f_zzmkspin__", (ftnlen)705)] = dpmin_();
	}
	for (pltco = 1; pltco <= 3; ++pltco) {
	    vix = plates[(i__2 = pltco + pid * 3 - 4) < 60000 && 0 <= i__2 ? 
		    i__2 : s_rnge("plates", i__2, "f_zzmkspin__", (ftnlen)711)
		    ];
	    for (vrtco = 1; vrtco <= 3; ++vrtco) {
/* Computing MIN */
		d__1 = verts[(i__3 = vrtco + vix * 3 - 4) < 30000 && 0 <= 
			i__3 ? i__3 : s_rnge("verts", i__3, "f_zzmkspin__", (
			ftnlen)716)], d__2 = pltext[(i__4 = (vrtco << 1) - 2) 
			< 6 && 0 <= i__4 ? i__4 : s_rnge("pltext", i__4, 
			"f_zzmkspin__", (ftnlen)716)];
		pltext[(i__2 = (vrtco << 1) - 2) < 6 && 0 <= i__2 ? i__2 : 
			s_rnge("pltext", i__2, "f_zzmkspin__", (ftnlen)716)] =
			 min(d__1,d__2);
/* Computing MAX */
		d__1 = verts[(i__3 = vrtco + vix * 3 - 4) < 30000 && 0 <= 
			i__3 ? i__3 : s_rnge("verts", i__3, "f_zzmkspin__", (
			ftnlen)719)], d__2 = pltext[(i__4 = (vrtco << 1) - 1) 
			< 6 && 0 <= i__4 ? i__4 : s_rnge("pltext", i__4, 
			"f_zzmkspin__", (ftnlen)719)];
		pltext[(i__2 = (vrtco << 1) - 1) < 6 && 0 <= i__2 ? i__2 : 
			s_rnge("pltext", i__2, "f_zzmkspin__", (ftnlen)719)] =
			 max(d__1,d__2);
	    }
	}

/*        Let LOWPT and HIGHPT be the coordinates of the corners */
/*        of the box defined by the plate extents, where LOWPT */
/*        contains the minimum coordinate values and HIGHPT */
/*        contains the maximum values. */

/*        Take tolerance into account. */

	for (vrtco = 1; vrtco <= 3; ++vrtco) {
	    lowpt[(i__2 = vrtco - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("lowpt",
		     i__2, "f_zzmkspin__", (ftnlen)735)] = pltext[(i__3 = (
		    vrtco << 1) - 2) < 6 && 0 <= i__3 ? i__3 : s_rnge("pltext"
		    , i__3, "f_zzmkspin__", (ftnlen)735)] - mdltol;
	    highpt[(i__2 = vrtco - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("high"
		    "pt", i__2, "f_zzmkspin__", (ftnlen)736)] = pltext[(i__3 = 
		    (vrtco << 1) - 1) < 6 && 0 <= i__3 ? i__3 : s_rnge("plte"
		    "xt", i__3, "f_zzmkspin__", (ftnlen)736)] + mdltol;
	}

/*        Map the corner's model coordinates to voxel */
/*        coordinates. */

	zzgetvox_(&voxsiz, voxori, nvox, lowpt, &inbox, lowvox);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zzgetvox_(&voxsiz, voxori, nvox, highpt, &inbox, hivox);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        For each voxel in the box bounded by LOWVOX and HIVOX, check */
/*        the Ith plate against that voxel's plate list. */

	i__2 = hivox[0];
	for (i__ = lowvox[0]; i__ <= i__2; ++i__) {
	    voxcor[0] = i__;
	    i__3 = hivox[1];
	    for (j = lowvox[1]; j <= i__3; ++j) {
		voxcor[1] = j;
		i__4 = hivox[2];
		for (k = lowvox[2]; k <= i__4; ++k) {
		    voxcor[2] = k;

/*                 Start out assuming we have no intersection. */
/*                 Update INCLUD if we find out otherwise. */

		    includ = FALSE_;
		    zzvoxcvo_(voxcor, nvox, &corscl, cgxyz, cgoff, &cgof1d);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 Get the 1-D coarse grid index corresponding to CGXYZ. */

		    cvoxid = zzvox2id_(cgxyz, ncgvox);

/*                 Get the coarse grid pointer for the current voxel. */

		    cgp = cgrptr[(i__5 = cvoxid - 1) < 100000 && 0 <= i__5 ? 
			    i__5 : s_rnge("cgrptr", i__5, "f_zzmkspin__", (
			    ftnlen)786)];
		    if (cgp > 0) {

/*                    The pointer is non-null, so this coarse voxel is */
/*                    non-empty. (That is, some plates intersect it.) */

/*                    CGP is the index of the first entry in the voxel */
/*                    pointer array for the current coarse voxel. Get */
/*                    the index of the entry for the current fine voxel, */
/*                    and look up the pointer at that index. */

			vptr = voxptr[(i__5 = cgp + cgof1d - 2) < 60000 && 0 
				<= i__5 ? i__5 : s_rnge("voxptr", i__5, "f_z"
				"zmkspin__", (ftnlen)798)];
			if (vptr > 0) {

/*                       This voxel is non-empty. */

/*                       Let NVP be the plate count for this voxel. */

			    nvp = vxlist[(i__5 = vptr - 1) < 100000 && 0 <= 
				    i__5 ? i__5 : s_rnge("vxlist", i__5, 
				    "f_zzmkspin__", (ftnlen)806)];
			    loc = isrchi_(&pid, &nvp, &vxlist[(i__5 = vptr) < 
				    100000 && 0 <= i__5 ? i__5 : s_rnge("vxl"
				    "ist", i__5, "f_zzmkspin__", (ftnlen)808)])
				    ;
			    includ = loc > 0;
			}
		    }

/*                 Make sure the plate was found on the list for */
/*                 the current voxel. */

		    if (! includ) {
			voxid = zzvox2id_(voxcor, nvox);
			s_copy(label, "Plate @ intersects voxel @", (ftnlen)
				40, (ftnlen)26);
			repmi_(label, "@", &pid, label, (ftnlen)40, (ftnlen)1,
				 (ftnlen)40);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			repmi_(label, "@", &voxid, label, (ftnlen)40, (ftnlen)
				1, (ftnlen)40);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksl_(label, &includ, &c_true, ok, (ftnlen)40);
		    }
		}
	    }
	}
    }
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Error: bad plate count.", (ftnlen)23);
    zzmkspin_(&c__0, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &
	    c_b10, cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &
	    nvxlst, vxlist, extent, cgrptr);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    zzmkspin_(&c_n1, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &
	    c_b10, cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &
	    nvxlst, vxlist, extent, cgrptr);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Error: coarse voxel scale out of range.", (ftnlen)39);
    corscl = 0;
    zzmkspin_(&np, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &c_b10,
	     cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &nvxlst, 
	    vxlist, extent, cgrptr);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Error: too many fine voxels.", (ftnlen)28);
    finscl = .1;
    corscl = 100;
    zzmkspin_(&np, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &c_b10,
	     cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &nvxlst, 
	    vxlist, extent, cgrptr);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case -------------------------------------------------------- */

    tcase_("Error: too many coarse voxels.", (ftnlen)30);

/*     Set voxel scales. */

    finscl = .5;
    corscl = 1;

/*     Set sizes of the workspace and output arrays. */

    zzmkspin_(&np, plates, verts, &finscl, &corscl, &c__60000, &c_b10, &c_b10,
	     cells, nvox, &voxsiz, voxori, &nvxtot, &nvxptr, voxptr, &nvxlst, 
	    vxlist, extent, cgrptr);
    chckxc_(&c_true, "SPICE(COARSEGRIDOVERFLOW)", ok, (ftnlen)25);
    t_success__(ok);
    return 0;
} /* f_zzmkspin__ */

