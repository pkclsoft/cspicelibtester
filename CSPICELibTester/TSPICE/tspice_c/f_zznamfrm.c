/* f_zznamfrm.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__5 = 5;
static integer c_n99 = -99;
static integer c_n88 = -88;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__13 = 13;
static integer c__10 = 10;
static integer c_n9 = -9;
static integer c_n9999 = -9999;
static doublereal c_b334 = 0.;
static integer c__9 = 9;
static integer c__8 = 8;
static integer c__24 = 24;
static integer c__6 = 6;
static doublereal c_b590 = 1e-14;
static doublereal c_b597 = 1e-12;
static integer c__399 = 399;
static integer c__499 = 499;
static integer c__36 = 36;

/* $Procedure F_ZZNAMFRM ( Family of tests for ZZNAMFRM and its callers ) */
/* Subroutine */ int f_zznamfrm__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal dvec[3];
    extern /* Subroutine */ int ckgp_(integer *, doublereal *, doublereal *, 
	    char *, doublereal *, doublereal *, logical *, ftnlen);
    doublereal cmat1[36]	/* was [3][3][4] */, cmat2[36]	/* was [3][3][
	    4] */;
    char body2[32];
    extern /* Subroutine */ int sce2t_(integer *, doublereal *, doublereal *),
	     zznamfrm_(integer *, char *, integer *, char *, integer *, 
	    ftnlen, ftnlen);
    doublereal dist4[4];
    extern /* Subroutine */ int zzctruin_(integer *);
    integer i__, n[4];
    extern /* Subroutine */ int dafgs_(doublereal *);
    doublereal radii[12]	/* was [3][4] */;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    doublereal descr[5];
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    logical found;
    extern /* Subroutine */ int movei_(integer *, integer *, integer *);
    char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen), t_ctrbeqf__(void), 
	    spkez_(integer *, doublereal *, char *, char *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen), spkpv_(integer *, 
	    doublereal *, doublereal *, char *, doublereal *, integer *, 
	    ftnlen);
    integer handl1;
    extern /* Subroutine */ int t_success__(logical *);
    char frame1[32], frame2[32];
    doublereal incdn2[4], phase2[4];
    integer cente9[4];
    logical found1[4], found2[4];
    doublereal clkou1[4], clkou2[4];
    logical found3[4];
    doublereal state1[24]	/* was [6][4] */, trgep1[4], obspo1[12]	/* 
	    was [3][4] */, trgep2[4], emiss2[4], trgep3[4], srfve2[12]	/* 
	    was [3][4] */, srfve3[12]	/* was [3][4] */, state2[24]	/* 
	    was [6][4] */, state3[24]	/* was [6][4] */, spoin3[12]	/* 
	    was [3][4] */;
    integer ci;
    doublereal state5[24]	/* was [6][4] */, state7[24]	/* was [6][4] 
	    */, state9[24]	/* was [6][4] */, spoin4[12]	/* was [3][4] 
	    */, trgep4[4], obspo4[12]	/* was [3][4] */;
    logical found4[4];
    doublereal spoin5[12]	/* was [3][4] */, trgep5[4], srfve5[12]	/* 
	    was [3][4] */, trmpt1[96]	/* was [3][8][4] */, spoin7[12]	/* 
	    was [3][4] */, et;
    integer handle;
    doublereal trgep7[4], srfve7[12]	/* was [3][4] */;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), chckai_(char *, 
	    integer *, char *, integer *, integer *, logical *, ftnlen, 
	    ftnlen);
    doublereal lt;
    extern /* Subroutine */ int tstck3_(char *, char *, logical *, logical *, 
	    logical *, integer *, ftnlen, ftnlen);
    integer frcode;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    integer savcde;
    extern /* Subroutine */ int irfdef_(integer *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    char frname[32], buffer[80*10];
    extern /* Subroutine */ int kilfil_(char *, ftnlen), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical *, 
	    ftnlen), ckgpav_(integer *, doublereal *, doublereal *, char *, 
	    doublereal *, doublereal *, doublereal *, logical *, ftnlen);
    doublereal sclkdp;
    extern /* Subroutine */ int edterm_(char *, char *, char *, doublereal *, 
	    char *, char *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    bodvrd_(char *, char *, integer *, integer *, doublereal *, 
	    ftnlen, ftnlen), pxfrm2_(char *, char *, doublereal *, doublereal 
	    *, doublereal *, ftnlen, ftnlen);
    char savnam[32];
    extern /* Subroutine */ int cleard_(integer *, doublereal *), dafbfs_(
	    integer *), clpool_(void), ldpool_(char *, ftnlen), illumg_(char *
	    , char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    doublereal obssta[6];
    extern /* Subroutine */ int spkgeo_(integer *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen), daffna_(logical *)
	    , sincpt_(char *, char *, doublereal *, char *, char *, char *, 
	    char *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen);
    integer savctr[2];
    extern /* Subroutine */ int lmpool_(char *, integer *, ftnlen), spkcvo_(
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), spkgps_(integer *, 
	    doublereal *, char *, integer *, doublereal *, doublereal *, 
	    ftnlen), tstpck_(char *, logical *, logical *, ftnlen);
    doublereal av2[12]	/* was [3][4] */;
    extern /* Subroutine */ int spkcvt_(doublereal *, doublereal *, char *, 
	    char *, doublereal *, char *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    pxform_(char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen);
    doublereal spoint[3];
    extern /* Subroutine */ int spkezp_(integer *, doublereal *, char *, char 
	    *, integer *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    spkpos_(char *, doublereal *, char *, char *, char *, doublereal *
	    , doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), subpnt_(char *, 
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen);
    doublereal lt1[4];
    integer usrctr[2];
    doublereal lt2[4], lt3[4], lt4[4], lt5[4], lt6[4], lt7[4], lt8[4];
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     srfxpt_(char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    subslr_(char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen), sxform_(char *, char *, doublereal *, doublereal 
	    *, ftnlen, ftnlen);
    doublereal mat1[36]	/* was [3][3][4] */, mat2[36]	/* was [3][3][4] */, 
	    mat3[144]	/* was [6][6][4] */, pos4[12]	/* was [3][4] */, 
	    pos6[12]	/* was [3][4] */, pos8[12]	/* was [3][4] */;

/* $ Abstract */

/*     This routine tests ZZNAMFRM and the internal save functionality */
/*     of all routines that call it. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None */

/* $ Keywords */

/*     None */

/* $ Declarations */
/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */

/* $ Brief_I/O */

/*     None */

/* $ Detailed_Input */

/*     None */

/* $ Detailed_Output */

/*     None */

/* $ Parameters */

/*     None */

/* $ Exceptions */

/*     None */

/* $ Files */

/*     None */

/* $ Particulars */

/*     None */

/* $ Examples */

/*     None */

/* $ Restrictions */

/*     None */

/* $ Literature_References */

/*     None */

/* $ Author_and_Institution */

/*     None */

/* $ Version */

/* -    Version 1.0.0  31-MAR-2014 (BVS) */

/* -& */
/* $ Index_Entries */

/*     None */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZNAMFRM", (ftnlen)10);

/*     Check ZZNAMFRM update after counter set to user value. */

/*     Call ZZNAMFRM twice, first to make sure that there is an */
/*     update, second that there is no update. */

    tcase_("ZZNAMFRM update after initial counter setting.", (ftnlen)46);
    zzctruin_(usrctr);
    movei_(usrctr, &c__2, savctr);
    s_copy(frname, "J2000", (ftnlen)32, (ftnlen)5);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 17;
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SAVNAM", savnam, "=", "J2000", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)5);
    chcksi_("SAVCDE", &savcde, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "J2000", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)5);
    chcksi_("FRCODE", &frcode, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "J2000", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)5);
    chcksi_("SAVCDE", &savcde, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "J2000", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)5);
    chcksi_("FRCODE", &frcode, "=", &c__1, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check ZZNAMFRM update after POOL frame addition. */

/*     Call ZZNAMFRM twice, first to make sure that there is an */
/*     update, second that there is no update. */

    tcase_("ZZNAMFRM update after POOL frame insertion.", (ftnlen)43);
    s_copy(buffer, "FRAME_MYFRAME        = -99", (ftnlen)80, (ftnlen)26);
    s_copy(buffer + 80, "FRAME_-99_NAME       = 'MYFRAME'", (ftnlen)80, (
	    ftnlen)32);
    s_copy(buffer + 160, "FRAME_-99_CLASS      = 4", (ftnlen)80, (ftnlen)24);
    s_copy(buffer + 240, "FRAME_-99_CLASS_ID   = -99", (ftnlen)80, (ftnlen)26)
	    ;
    s_copy(buffer + 320, "FRAME_-99_CENTER     = 0", (ftnlen)80, (ftnlen)24);
    lmpool_(buffer, &c__5, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    s_copy(frname, "MYFRAME", (ftnlen)32, (ftnlen)7);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 17;
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n99, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n99, &c__0, ok, (ftnlen)6, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n99, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n99, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check ZZNAMFRM update after POOL frame update. */

/*     Call ZZNAMFRM twice, first to make sure that there is an */
/*     update, second that there is no update. */

    tcase_("ZZNAMFRM update after POOL frame update.", (ftnlen)40);
    s_copy(buffer, "FRAME_MYFRAME        = -88", (ftnlen)80, (ftnlen)26);
    s_copy(buffer + 80, "FRAME_-88_NAME       = 'MYFRAME'", (ftnlen)80, (
	    ftnlen)32);
    s_copy(buffer + 160, "FRAME_-88_CLASS      = 4", (ftnlen)80, (ftnlen)24);
    s_copy(buffer + 240, "FRAME_-88_CLASS_ID   = -88", (ftnlen)80, (ftnlen)26)
	    ;
    s_copy(buffer + 320, "FRAME_-88_CENTER     = 0", (ftnlen)80, (ftnlen)24);
    lmpool_(buffer, &c__5, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    s_copy(frname, "MYFRAME", (ftnlen)32, (ftnlen)7);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 17;
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("USRCTR", usrctr, "!=", savctr, &c__0, ok, (ftnlen)6, (ftnlen)2);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check ZZNAMFRM update for input saved code set to 0. */

/*     Call ZZNAMFRM twice, first to make sure that there is an */
/*     update, second that there is no update. */

    tcase_("ZZNAMFRM update after 0 saved frame ID.", (ftnlen)39);
    movei_(usrctr, &c__2, savctr);
    s_copy(frname, "MYFRAME", (ftnlen)32, (ftnlen)7);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 0;
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MYFRAME", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check ZZNAMFRM update for different input name. */

/*     Call ZZNAMFRM twice, first to make sure that there is an */
/*     update, second that there is no update. */

    tcase_("ZZNAMFRM update for different input name.", (ftnlen)41);
    movei_(usrctr, &c__2, savctr);
    s_copy(frname, "MyFrame", (ftnlen)32, (ftnlen)7);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 17;
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MyFrame", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MyFrame", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    movei_(usrctr, &c__2, savctr);
    zznamfrm_(usrctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckai_("USRCTR", usrctr, "=", savctr, &c__2, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("SAVNAM", savnam, "=", "MyFrame", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("SAVCDE", &savcde, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);
    chcksc_("FRNAME", frname, "=", "MyFrame", ok, (ftnlen)6, (ftnlen)32, (
	    ftnlen)1, (ftnlen)7);
    chcksi_("FRCODE", &frcode, "=", &c_n88, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Clear POOL and make and load the some test data. */

    clpool_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kilfil_("fun.bsp", (ftnlen)7);
    kilfil_("fun.ker", (ftnlen)7);
    kilfil_("fun.bc", (ftnlen)6);
    kilfil_("fun.tsc", (ftnlen)7);
    tstspk_("fun.bsp", &c_true, &handle, (ftnlen)7);
    tstpck_("fun.ker", &c_true, &c_true, (ftnlen)7);
    tstck3_("fun.bc", "fun.tsc", &c_true, &c_true, &c_true, &handl1, (ftnlen)
	    6, (ftnlen)7);

/*     Test all routines that call ZZNAMFRM. Do it by computing two */
/*     bench mark sets of data (at indexes 1 and 2), each for a unique, */
/*     non-overlapping set of frames, then setting two unique sets of */
/*     alias frames and computing the same sets of data for them (at */
/*     indexes 3 and 4), and checking each "alias" set against its */
/*     benchmark. If saved mapping was updated correctly set 1 should */
/*     match set 3 and set 2 should match set 4. */

/*     In addition to setting up all inputs we will call T_CTRBEQF to */
/*     adjust ZZBODS2C and ZZNAMFRM counters to the same value before */
/*     calling each of the routine that make use of saved values and */
/*     counters. This will help exposing any cases when the same counter */
/*     is erroneously used in more than one place. */

    for (i__ = 1; i__ <= 4; ++i__) {

/*        Clear POOL and reload text kernels. */

	clpool_();
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ldpool_("fun.ker", (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ldpool_("fun.tsc", (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Set up frames for each iteration. */

	if (i__ == 1) {
	    s_copy(frame1, "J2000", (ftnlen)32, (ftnlen)5);
	    s_copy(frame2, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
	    s_copy(body2, "EARTH", (ftnlen)32, (ftnlen)5);
	    irfdef_(&c__3);
	    ci = 1;
	    s_copy(title, "Check 1st bench data set against itself", (ftnlen)
		    80, (ftnlen)39);
	} else if (i__ == 2) {
	    s_copy(frame1, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
	    s_copy(frame2, "IAU_MARS", (ftnlen)32, (ftnlen)8);
	    s_copy(body2, "MARS", (ftnlen)32, (ftnlen)4);
	    irfdef_(&c__13);
	    ci = 2;
	    s_copy(title, "Check 2nd bench data set against itself", (ftnlen)
		    80, (ftnlen)39);
	} else if (i__ == 3) {
	    s_copy(frame1, "FRAME1", (ftnlen)32, (ftnlen)6);
	    s_copy(frame2, "FRAME2", (ftnlen)32, (ftnlen)6);
	    s_copy(body2, "EARTH", (ftnlen)32, (ftnlen)5);
	    irfdef_(&c__3);
	    ci = 1;
	    s_copy(buffer, "FRAME_FRAME1         = -99", (ftnlen)80, (ftnlen)
		    26);
	    s_copy(buffer + 80, "FRAME_-99_NAME       = 'FRAME1'", (ftnlen)80,
		     (ftnlen)31);
	    s_copy(buffer + 160, "FRAME_-99_CLASS      = 4", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 240, "FRAME_-99_CLASS_ID   = -99", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 320, "FRAME_-99_CENTER     = 0", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 400, "TKFRAME_-99_SPEC     = 'MATRIX'", (ftnlen)
		    80, (ftnlen)31);
	    s_copy(buffer + 480, "TKFRAME_-99_RELATIVE = 'J2000'", (ftnlen)80,
		     (ftnlen)30);
	    s_copy(buffer + 560, "TKFRAME_-99_MATRIX = ( 1,0,0,0,1,0,0,0,1 )",
		     (ftnlen)80, (ftnlen)42);
	    s_copy(buffer + 640, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(buffer + 720, " ", (ftnlen)80, (ftnlen)1);
	    lmpool_(buffer, &c__10, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(buffer, "FRAME_FRAME2         = -88", (ftnlen)80, (ftnlen)
		    26);
	    s_copy(buffer + 80, "FRAME_-88_NAME       = 'FRAME2'", (ftnlen)80,
		     (ftnlen)31);
	    s_copy(buffer + 160, "FRAME_-88_CLASS      = 4", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 240, "FRAME_-88_CLASS_ID   = -88", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 320, "FRAME_-88_CENTER     = 399", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 400, "TKFRAME_-88_SPEC     = 'MATRIX'", (ftnlen)
		    80, (ftnlen)31);
	    s_copy(buffer + 480, "TKFRAME_-88_RELATIVE = 'IAU_EARTH'", (
		    ftnlen)80, (ftnlen)34);
	    s_copy(buffer + 560, "TKFRAME_-88_MATRIX = ( 1,0,0,0,1,0,0,0,1 )",
		     (ftnlen)80, (ftnlen)42);
	    s_copy(buffer + 640, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(buffer + 720, " ", (ftnlen)80, (ftnlen)1);
	    lmpool_(buffer, &c__10, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(title, "Check 1st mapped data set against 1st benchmark", (
		    ftnlen)80, (ftnlen)47);
	} else if (i__ == 4) {
	    s_copy(frame1, "FRAME1", (ftnlen)32, (ftnlen)6);
	    s_copy(frame2, "FRAME2", (ftnlen)32, (ftnlen)6);
	    s_copy(body2, "MARS", (ftnlen)32, (ftnlen)4);
	    irfdef_(&c__13);
	    ci = 2;
	    s_copy(buffer, "FRAME_FRAME1         = -77", (ftnlen)80, (ftnlen)
		    26);
	    s_copy(buffer + 80, "FRAME_-77_NAME       = 'FRAME1'", (ftnlen)80,
		     (ftnlen)31);
	    s_copy(buffer + 160, "FRAME_-77_CLASS      = 4", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 240, "FRAME_-77_CLASS_ID   = -77", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 320, "FRAME_-77_CENTER     = 0", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 400, "TKFRAME_-77_SPEC     = 'MATRIX'", (ftnlen)
		    80, (ftnlen)31);
	    s_copy(buffer + 480, "TKFRAME_-77_RELATIVE = 'ECLIPJ2000'", (
		    ftnlen)80, (ftnlen)35);
	    s_copy(buffer + 560, "TKFRAME_-77_MATRIX = ( 1,0,0,0,1,0,0,0,1 )",
		     (ftnlen)80, (ftnlen)42);
	    s_copy(buffer + 640, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(buffer + 720, " ", (ftnlen)80, (ftnlen)1);
	    lmpool_(buffer, &c__10, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(buffer, "FRAME_FRAME2         = -66", (ftnlen)80, (ftnlen)
		    26);
	    s_copy(buffer + 80, "FRAME_-66_NAME       = 'FRAME2'", (ftnlen)80,
		     (ftnlen)31);
	    s_copy(buffer + 160, "FRAME_-66_CLASS      = 4", (ftnlen)80, (
		    ftnlen)24);
	    s_copy(buffer + 240, "FRAME_-66_CLASS_ID   = -66", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 320, "FRAME_-66_CENTER     = 499", (ftnlen)80, (
		    ftnlen)26);
	    s_copy(buffer + 400, "TKFRAME_-66_SPEC     = 'MATRIX'", (ftnlen)
		    80, (ftnlen)31);
	    s_copy(buffer + 480, "TKFRAME_-66_RELATIVE = 'IAU_MARS'", (ftnlen)
		    80, (ftnlen)33);
	    s_copy(buffer + 560, "TKFRAME_-66_MATRIX = ( 1,0,0,0,1,0,0,0,1 )",
		     (ftnlen)80, (ftnlen)42);
	    s_copy(buffer + 640, " ", (ftnlen)80, (ftnlen)1);
	    s_copy(buffer + 720, " ", (ftnlen)80, (ftnlen)1);
	    lmpool_(buffer, &c__10, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(title, "Check 2nd mapped data set against 2nd benchmark", (
		    ftnlen)80, (ftnlen)47);
	}

/*        Declare test case. */

	tcase_(title, (ftnlen)80);

/*        Call all routines that make use of ZZNAMFRM. Set arbitrary */
/*        input time. */

	et = 1e7;

/*        ----------------------------------------------------------- */

	sce2t_(&c_n9, &et, &sclkdp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	ckgp_(&c_n9999, &sclkdp, &c_b334, frame1, &cmat1[(i__1 = (i__ * 3 + 1)
		 * 3 - 12) < 36 && 0 <= i__1 ? i__1 : s_rnge("cmat1", i__1, 
		"f_zznamfrm__", (ftnlen)629)], &clkou1[(i__2 = i__ - 1) < 4 &&
		 0 <= i__2 ? i__2 : s_rnge("clkou1", i__2, "f_zznamfrm__", (
		ftnlen)629)], &found1[(i__3 = i__ - 1) < 4 && 0 <= i__3 ? 
		i__3 : s_rnge("found1", i__3, "f_zznamfrm__", (ftnlen)629)], (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("CKGP CMAT", &cmat1[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 && 0 
		<= i__1 ? i__1 : s_rnge("cmat1", i__1, "f_zznamfrm__", (
		ftnlen)633)], "=", &cmat1[(i__2 = (ci * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__2 ? i__2 : s_rnge("cmat1", i__2, "f_zznamfrm__", (
		ftnlen)633)], &c__9, &c_b334, ok, (ftnlen)9, (ftnlen)1);
	chcksd_("CKGP CKLOUT", &clkou1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("clkou1", i__1, "f_zznamfrm__", (ftnlen)635)], 
		"=", &clkou1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"clkou1", i__2, "f_zznamfrm__", (ftnlen)635)], &c_b334, ok, (
		ftnlen)11, (ftnlen)1);
	chcksl_("CKGP FOUND", &found1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found1", i__1, "f_zznamfrm__", (ftnlen)637)], &
		found1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd1", i__2, "f_zznamfrm__", (ftnlen)637)], ok, (ftnlen)10);

/*        ----------------------------------------------------------- */

	sce2t_(&c_n9, &et, &sclkdp);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	ckgpav_(&c_n9999, &sclkdp, &c_b334, frame1, &cmat2[(i__1 = (i__ * 3 + 
		1) * 3 - 12) < 36 && 0 <= i__1 ? i__1 : s_rnge("cmat2", i__1, 
		"f_zznamfrm__", (ftnlen)647)], &av2[(i__2 = i__ * 3 - 3) < 12 
		&& 0 <= i__2 ? i__2 : s_rnge("av2", i__2, "f_zznamfrm__", (
		ftnlen)647)], &clkou2[(i__3 = i__ - 1) < 4 && 0 <= i__3 ? 
		i__3 : s_rnge("clkou2", i__3, "f_zznamfrm__", (ftnlen)647)], &
		found2[(i__4 = i__ - 1) < 4 && 0 <= i__4 ? i__4 : s_rnge(
		"found2", i__4, "f_zznamfrm__", (ftnlen)647)], (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("CKGPAV CMAT", &cmat2[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 && 
		0 <= i__1 ? i__1 : s_rnge("cmat2", i__1, "f_zznamfrm__", (
		ftnlen)651)], "=", &cmat2[(i__2 = (ci * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__2 ? i__2 : s_rnge("cmat2", i__2, "f_zznamfrm__", (
		ftnlen)651)], &c__9, &c_b334, ok, (ftnlen)11, (ftnlen)1);
	chckad_("CKGPAV AV", &av2[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("av2", i__1, "f_zznamfrm__", (ftnlen)653)], 
		"=", &av2[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("av2", i__2, "f_zznamfrm__", (ftnlen)653)], &c__3, &
		c_b334, ok, (ftnlen)9, (ftnlen)1);
	chcksd_("CKGP CKLOUT", &clkou2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("clkou2", i__1, "f_zznamfrm__", (ftnlen)655)], 
		"=", &clkou2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"clkou2", i__2, "f_zznamfrm__", (ftnlen)655)], &c_b334, ok, (
		ftnlen)11, (ftnlen)1);
	chcksl_("CKGPAV FOUND", &found2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found2", i__1, "f_zznamfrm__", (ftnlen)657)], &
		found1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd1", i__2, "f_zznamfrm__", (ftnlen)657)], ok, (ftnlen)12);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	edterm_("UMBRAL", "SUN", body2, &et, frame2, "NONE", "MERCURY", &c__8,
		 &trgep1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : s_rnge(
		"trgep1", i__1, "f_zznamfrm__", (ftnlen)664)], &obspo1[(i__2 =
		 i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge("obspo1", 
		i__2, "f_zznamfrm__", (ftnlen)664)], &trmpt1[(i__3 = ((i__ << 
		3) + 1) * 3 - 27) < 96 && 0 <= i__3 ? i__3 : s_rnge("trmpt1", 
		i__3, "f_zznamfrm__", (ftnlen)664)], (ftnlen)6, (ftnlen)3, (
		ftnlen)32, (ftnlen)32, (ftnlen)4, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("EDTERM TRGEPC", &trgep1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep1", i__1, "f_zznamfrm__", (ftnlen)669)], 
		"=", &trgep1[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep1", i__2, "f_zznamfrm__", (ftnlen)669)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("EDTERM OBSPOS", &obspo1[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("obspo1", i__1, "f_zznamfrm__", (ftnlen)
		671)], "=", &obspo1[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("obspo1", i__2, "f_zznamfrm__", (ftnlen)671)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chckad_("EDTERM TRMPTS", &trmpt1[(i__1 = ((i__ << 3) + 1) * 3 - 27) < 
		96 && 0 <= i__1 ? i__1 : s_rnge("trmpt1", i__1, "f_zznamfrm__"
		, (ftnlen)673)], "=", &trmpt1[(i__2 = ((ci << 3) + 1) * 3 - 
		27) < 96 && 0 <= i__2 ? i__2 : s_rnge("trmpt1", i__2, "f_zzn"
		"amfrm__", (ftnlen)673)], &c__24, &c_b334, ok, (ftnlen)13, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	bodvrd_(body2, "RADII", &c__3, &n[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("n", i__1, "f_zznamfrm__", (ftnlen)679)], &
		radii[(i__2 = i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge(
		"radii", i__2, "f_zznamfrm__", (ftnlen)679)], (ftnlen)32, (
		ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vpack_(&radii[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"radii", i__1, "f_zznamfrm__", (ftnlen)682)], &c_b334, &
		c_b334, spoint);
	t_ctrbeqf__();
	illumg_("Ellipsoid", body2, "SUN", &et, frame2, "NONE", "MERCURY", 
		spoint, &trgep2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("trgep2", i__1, "f_zznamfrm__", (ftnlen)686)], &srfve2[
		(i__2 = i__ * 3 - 3) < 12 && 0 <= i__2 ? i__2 : s_rnge("srfv"
		"e2", i__2, "f_zznamfrm__", (ftnlen)686)], &phase2[(i__3 = i__ 
		- 1) < 4 && 0 <= i__3 ? i__3 : s_rnge("phase2", i__3, "f_zzn"
		"amfrm__", (ftnlen)686)], &incdn2[(i__4 = i__ - 1) < 4 && 0 <= 
		i__4 ? i__4 : s_rnge("incdn2", i__4, "f_zznamfrm__", (ftnlen)
		686)], &emiss2[(i__5 = i__ - 1) < 4 && 0 <= i__5 ? i__5 : 
		s_rnge("emiss2", i__5, "f_zznamfrm__", (ftnlen)686)], (ftnlen)
		9, (ftnlen)32, (ftnlen)3, (ftnlen)32, (ftnlen)4, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("ILLUMG TRGEPC", &trgep2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep2", i__1, "f_zznamfrm__", (ftnlen)692)], 
		"=", &trgep2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep2", i__2, "f_zznamfrm__", (ftnlen)692)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("ILLUMG SRFVEC", &srfve2[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve2", i__1, "f_zznamfrm__", (ftnlen)
		694)], "=", &srfve2[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve2", i__2, "f_zznamfrm__", (ftnlen)694)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("ILLUMG PHASE", &phase2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("phase2", i__1, "f_zznamfrm__", (ftnlen)696)], 
		"=", &phase2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"phase2", i__2, "f_zznamfrm__", (ftnlen)696)], &c_b334, ok, (
		ftnlen)12, (ftnlen)1);
	chcksd_("ILLUMG INCDNC", &incdn2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("incdn2", i__1, "f_zznamfrm__", (ftnlen)698)], 
		"=", &incdn2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"incdn2", i__2, "f_zznamfrm__", (ftnlen)698)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chcksd_("ILLUMG EMISSN", &emiss2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("emiss2", i__1, "f_zznamfrm__", (ftnlen)700)], 
		"=", &emiss2[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"emiss2", i__2, "f_zznamfrm__", (ftnlen)700)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	pxform_(frame1, frame2, &et, &mat1[(i__1 = (i__ * 3 + 1) * 3 - 12) < 
		36 && 0 <= i__1 ? i__1 : s_rnge("mat1", i__1, "f_zznamfrm__", 
		(ftnlen)708)], (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PXFORM ROTATE", &mat1[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 &&
		 0 <= i__1 ? i__1 : s_rnge("mat1", i__1, "f_zznamfrm__", (
		ftnlen)711)], "=", &mat1[(i__2 = (ci * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__2 ? i__2 : s_rnge("mat1", i__2, "f_zznamfrm__", (
		ftnlen)711)], &c__9, &c_b334, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	d__1 = et + 1e6;
	pxfrm2_(frame1, frame2, &et, &d__1, &mat2[(i__1 = (i__ * 3 + 1) * 3 - 
		12) < 36 && 0 <= i__1 ? i__1 : s_rnge("mat2", i__1, "f_zznam"
		"frm__", (ftnlen)719)], (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("PXFRM2 ROTATE", &mat2[(i__1 = (i__ * 3 + 1) * 3 - 12) < 36 &&
		 0 <= i__1 ? i__1 : s_rnge("mat2", i__1, "f_zznamfrm__", (
		ftnlen)722)], "=", &mat2[(i__2 = (ci * 3 + 1) * 3 - 12) < 36 
		&& 0 <= i__2 ? i__2 : s_rnge("mat2", i__2, "f_zznamfrm__", (
		ftnlen)722)], &c__9, &c_b334, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	spkpos_(body2, &et, "J2000", "NONE", "SUN", dvec, &lt, (ftnlen)32, (
		ftnlen)5, (ftnlen)4, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	sincpt_("Ellipsoid", body2, &et, frame2, "NONE", "SUN", "J2000", dvec,
		 &spoin3[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : 
		s_rnge("spoin3", i__1, "f_zznamfrm__", (ftnlen)734)], &trgep3[
		(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep3", 
		i__2, "f_zznamfrm__", (ftnlen)734)], &srfve3[(i__3 = i__ * 3 
		- 3) < 12 && 0 <= i__3 ? i__3 : s_rnge("srfve3", i__3, "f_zz"
		"namfrm__", (ftnlen)734)], &found3[(i__4 = i__ - 1) < 4 && 0 <=
		 i__4 ? i__4 : s_rnge("found3", i__4, "f_zznamfrm__", (ftnlen)
		734)], (ftnlen)9, (ftnlen)32, (ftnlen)32, (ftnlen)4, (ftnlen)
		3, (ftnlen)5);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SINCPT SPOINT", &spoin3[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin3", i__1, "f_zznamfrm__", (ftnlen)
		739)], "=", &spoin3[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin3", i__2, "f_zznamfrm__", (ftnlen)739)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SINCPT TRGEPC", &trgep3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep3", i__1, "f_zznamfrm__", (ftnlen)741)], 
		"=", &trgep3[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep3", i__2, "f_zznamfrm__", (ftnlen)741)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SINCPT SRFVEC", &srfve3[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve3", i__1, "f_zznamfrm__", (ftnlen)
		743)], "=", &srfve3[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve3", i__2, "f_zznamfrm__", (ftnlen)743)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksl_("SINCPT FOUND", &found3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found3", i__1, "f_zznamfrm__", (ftnlen)745)], &
		found3[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd3", i__2, "f_zznamfrm__", (ftnlen)745)], ok, (ftnlen)12);

/*        ----------------------------------------------------------- */

	cleard_(&c__6, obssta);
	obssta[0] = 1e3;
	t_ctrbeqf__();
	spkcvo_("SUN", &et, frame1, "OBSERVER", "LT+S", obssta, &et, body2, 
		frame2, &state1[(i__1 = i__ * 6 - 6) < 24 && 0 <= i__1 ? i__1 
		: s_rnge("state1", i__1, "f_zznamfrm__", (ftnlen)756)], &lt1[(
		i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt1", i__2, 
		"f_zznamfrm__", (ftnlen)756)], (ftnlen)3, (ftnlen)32, (ftnlen)
		8, (ftnlen)4, (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKCVO STATE", &state1[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state1", i__1, "f_zznamfrm__", (ftnlen)
		761)], "~/", &state1[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state1", i__2, "f_zznamfrm__", (ftnlen)761)], &
		c__6, &c_b590, ok, (ftnlen)12, (ftnlen)2);
	chcksd_("SPKCVO LT", &lt1[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt1", i__1, "f_zznamfrm__", (ftnlen)763)], "~/", &lt1[
		(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt1", i__2, 
		"f_zznamfrm__", (ftnlen)763)], &c_b597, ok, (ftnlen)9, (
		ftnlen)2);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkcvt_(obssta, &et, body2, frame2, &et, frame1, "OBSERVER", "LT+S", 
		"SUN", &state2[(i__1 = i__ * 6 - 6) < 24 && 0 <= i__1 ? i__1 :
		 s_rnge("state2", i__1, "f_zznamfrm__", (ftnlen)771)], &lt2[(
		i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt2", i__2, 
		"f_zznamfrm__", (ftnlen)771)], (ftnlen)32, (ftnlen)32, (
		ftnlen)32, (ftnlen)8, (ftnlen)4, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKCVT STATE", &state2[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state2", i__1, "f_zznamfrm__", (ftnlen)
		776)], "~/", &state2[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state2", i__2, "f_zznamfrm__", (ftnlen)776)], &
		c__6, &c_b590, ok, (ftnlen)12, (ftnlen)2);
	chcksd_("SPKCVT LT", &lt2[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt2", i__1, "f_zznamfrm__", (ftnlen)778)], "~/", &lt2[
		(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt2", i__2, 
		"f_zznamfrm__", (ftnlen)778)], &c_b597, ok, (ftnlen)9, (
		ftnlen)2);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkez_(&c__1, &et, frame2, "NONE", &c__399, &state3[(i__1 = i__ * 6 - 
		6) < 24 && 0 <= i__1 ? i__1 : s_rnge("state3", i__1, "f_zzna"
		"mfrm__", (ftnlen)786)], &lt3[(i__2 = i__ - 1) < 4 && 0 <= 
		i__2 ? i__2 : s_rnge("lt3", i__2, "f_zznamfrm__", (ftnlen)786)
		], (ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKEZR STATE", &state3[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state3", i__1, "f_zznamfrm__", (ftnlen)
		789)], "=", &state3[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state3", i__2, "f_zznamfrm__", (ftnlen)789)], &
		c__6, &c_b334, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SPKEZR LT", &lt3[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt3", i__1, "f_zznamfrm__", (ftnlen)791)], "=", &lt3[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt3", i__2, 
		"f_zznamfrm__", (ftnlen)791)], &c_b334, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkezp_(&c__1, &et, frame2, "NONE", &c__399, &pos4[(i__1 = i__ * 3 - 
		3) < 12 && 0 <= i__1 ? i__1 : s_rnge("pos4", i__1, "f_zznamf"
		"rm__", (ftnlen)799)], &lt4[(i__2 = i__ - 1) < 4 && 0 <= i__2 ?
		 i__2 : s_rnge("lt4", i__2, "f_zznamfrm__", (ftnlen)799)], (
		ftnlen)32, (ftnlen)4);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKPOS POS", &pos4[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("pos4", i__1, "f_zznamfrm__", (ftnlen)802)], 
		"=", &pos4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("pos4", i__2, "f_zznamfrm__", (ftnlen)802)], &c__3, &
		c_b334, ok, (ftnlen)10, (ftnlen)1);
	chcksd_("SPKPOS LT", &lt4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt4", i__1, "f_zznamfrm__", (ftnlen)804)], "=", &lt4[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt4", i__2, 
		"f_zznamfrm__", (ftnlen)804)], &c_b334, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkgeo_(&c__499, &et, frame1, &c__10, &state5[(i__1 = i__ * 6 - 6) < 
		24 && 0 <= i__1 ? i__1 : s_rnge("state5", i__1, "f_zznamfrm__"
		, (ftnlen)812)], &lt5[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? 
		i__2 : s_rnge("lt5", i__2, "f_zznamfrm__", (ftnlen)812)], (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKGEO STATE", &state5[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state5", i__1, "f_zznamfrm__", (ftnlen)
		815)], "~/", &state5[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state5", i__2, "f_zznamfrm__", (ftnlen)815)], &
		c__6, &c_b590, ok, (ftnlen)12, (ftnlen)2);
	chcksd_("SPKGEO LT", &lt5[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt5", i__1, "f_zznamfrm__", (ftnlen)817)], "~/", &lt5[
		(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt5", i__2, 
		"f_zznamfrm__", (ftnlen)817)], &c_b597, ok, (ftnlen)9, (
		ftnlen)2);
	t_ctrbeqf__();
	spkgeo_(&c__1, &et, "DEFAULT", &c__399, &state7[(i__1 = i__ * 6 - 6) <
		 24 && 0 <= i__1 ? i__1 : s_rnge("state7", i__1, "f_zznamfrm"
		"__", (ftnlen)822)], &lt7[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? 
		i__2 : s_rnge("lt7", i__2, "f_zznamfrm__", (ftnlen)822)], (
		ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKGEO STATE", &state7[(i__1 = i__ * 6 - 6) < 24 && 0 <= 
		i__1 ? i__1 : s_rnge("state7", i__1, "f_zznamfrm__", (ftnlen)
		825)], "=", &state7[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? 
		i__2 : s_rnge("state7", i__2, "f_zznamfrm__", (ftnlen)825)], &
		c__6, &c_b334, ok, (ftnlen)12, (ftnlen)1);
	chcksd_("SPKGEO LT", &lt7[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt7", i__1, "f_zznamfrm__", (ftnlen)827)], "=", &lt7[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt7", i__2, 
		"f_zznamfrm__", (ftnlen)827)], &c_b334, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	spkgps_(&c__499, &et, frame1, &c__10, &pos6[(i__1 = i__ * 3 - 3) < 12 
		&& 0 <= i__1 ? i__1 : s_rnge("pos6", i__1, "f_zznamfrm__", (
		ftnlen)835)], &lt6[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : 
		s_rnge("lt6", i__2, "f_zznamfrm__", (ftnlen)835)], (ftnlen)32)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKGPS POS", &pos6[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("pos6", i__1, "f_zznamfrm__", (ftnlen)838)], 
		"=", &pos6[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("pos6", i__2, "f_zznamfrm__", (ftnlen)838)], &c__3, &
		c_b334, ok, (ftnlen)10, (ftnlen)1);
	chcksd_("SPKGPS LT", &lt6[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt6", i__1, "f_zznamfrm__", (ftnlen)840)], "=", &lt6[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt6", i__2, 
		"f_zznamfrm__", (ftnlen)840)], &c_b334, ok, (ftnlen)9, (
		ftnlen)1);
	t_ctrbeqf__();
	spkgps_(&c__1, &et, "DEFAULT", &c__399, &pos8[(i__1 = i__ * 3 - 3) < 
		12 && 0 <= i__1 ? i__1 : s_rnge("pos8", i__1, "f_zznamfrm__", 
		(ftnlen)845)], &lt8[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 :
		 s_rnge("lt8", i__2, "f_zznamfrm__", (ftnlen)845)], (ftnlen)7)
		;
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKGPS POS", &pos8[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? 
		i__1 : s_rnge("pos8", i__1, "f_zznamfrm__", (ftnlen)848)], 
		"=", &pos8[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? i__2 : 
		s_rnge("pos8", i__2, "f_zznamfrm__", (ftnlen)848)], &c__3, &
		c_b334, ok, (ftnlen)10, (ftnlen)1);
	chcksd_("SPKGPS LT", &lt8[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? i__1 : 
		s_rnge("lt8", i__1, "f_zznamfrm__", (ftnlen)850)], "=", &lt8[(
		i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("lt8", i__2, 
		"f_zznamfrm__", (ftnlen)850)], &c_b334, ok, (ftnlen)9, (
		ftnlen)1);

/*        ----------------------------------------------------------- */

	dafbfs_(&handle);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	daffna_(&found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("DAFFNA FOUND", &found, &c_true, ok, (ftnlen)12);
	dafgs_(descr);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	spkpv_(&handle, descr, &et, frame2, &state9[(i__1 = i__ * 6 - 6) < 24 
		&& 0 <= i__1 ? i__1 : s_rnge("state9", i__1, "f_zznamfrm__", (
		ftnlen)868)], &cente9[(i__2 = i__ - 1) < 4 && 0 <= i__2 ? 
		i__2 : s_rnge("cente9", i__2, "f_zznamfrm__", (ftnlen)868)], (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SPKPV STATE", &state9[(i__1 = i__ * 6 - 6) < 24 && 0 <= i__1 
		? i__1 : s_rnge("state9", i__1, "f_zznamfrm__", (ftnlen)871)],
		 "=", &state9[(i__2 = ci * 6 - 6) < 24 && 0 <= i__2 ? i__2 : 
		s_rnge("state9", i__2, "f_zznamfrm__", (ftnlen)871)], &c__6, &
		c_b334, ok, (ftnlen)11, (ftnlen)1);
	chcksi_("SPKPV CENTER", &cente9[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("cente9", i__1, "f_zznamfrm__", (ftnlen)873)], 
		"=", &cente9[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"cente9", i__2, "f_zznamfrm__", (ftnlen)873)], &c__0, ok, (
		ftnlen)12, (ftnlen)1);

/*        ----------------------------------------------------------- */

	spkpos_(body2, &et, frame1, "NONE", "SUN", dvec, &lt, (ftnlen)32, (
		ftnlen)32, (ftnlen)4, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	t_ctrbeqf__();
	srfxpt_("Ellipsoid", body2, &et, "NONE", "SUN", frame1, dvec, &spoin4[
		(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge("spoi"
		"n4", i__1, "f_zznamfrm__", (ftnlen)885)], &dist4[(i__2 = i__ 
		- 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("dist4", i__2, "f_zzna"
		"mfrm__", (ftnlen)885)], &trgep4[(i__3 = i__ - 1) < 4 && 0 <= 
		i__3 ? i__3 : s_rnge("trgep4", i__3, "f_zznamfrm__", (ftnlen)
		885)], &obspo4[(i__4 = i__ * 3 - 3) < 12 && 0 <= i__4 ? i__4 :
		 s_rnge("obspo4", i__4, "f_zznamfrm__", (ftnlen)885)], &
		found4[(i__5 = i__ - 1) < 4 && 0 <= i__5 ? i__5 : s_rnge(
		"found4", i__5, "f_zznamfrm__", (ftnlen)885)], (ftnlen)9, (
		ftnlen)32, (ftnlen)4, (ftnlen)3, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SRFXPT SPOINT", &spoin4[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin4", i__1, "f_zznamfrm__", (ftnlen)
		890)], "=", &spoin4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin4", i__2, "f_zznamfrm__", (ftnlen)890)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SRFXPT DIST", &dist4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("dist4", i__1, "f_zznamfrm__", (ftnlen)892)], 
		"=", &dist4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"dist4", i__2, "f_zznamfrm__", (ftnlen)892)], &c_b334, ok, (
		ftnlen)11, (ftnlen)1);
	chcksd_("SRFXPT TRGEPC", &trgep4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep4", i__1, "f_zznamfrm__", (ftnlen)894)], 
		"=", &trgep4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep4", i__2, "f_zznamfrm__", (ftnlen)894)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SRFXPT OBSPOS", &obspo4[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("obspo4", i__1, "f_zznamfrm__", (ftnlen)
		896)], "=", &obspo4[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("obspo4", i__2, "f_zznamfrm__", (ftnlen)896)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksl_("SRFXPT FOUND", &found4[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("found4", i__1, "f_zznamfrm__", (ftnlen)898)], &
		found4[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("fou"
		"nd4", i__2, "f_zznamfrm__", (ftnlen)898)], ok, (ftnlen)12);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subpnt_("Near point: ellipsoid", body2, &et, frame2, "NONE", "SUN", &
		spoin5[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : s_rnge(
		"spoin5", i__1, "f_zznamfrm__", (ftnlen)906)], &trgep5[(i__2 =
		 i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep5", i__2, 
		"f_zznamfrm__", (ftnlen)906)], &srfve5[(i__3 = i__ * 3 - 3) < 
		12 && 0 <= i__3 ? i__3 : s_rnge("srfve5", i__3, "f_zznamfrm__"
		, (ftnlen)906)], (ftnlen)21, (ftnlen)32, (ftnlen)32, (ftnlen)
		4, (ftnlen)3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBPNT SPOINT", &spoin5[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin5", i__1, "f_zznamfrm__", (ftnlen)
		911)], "=", &spoin5[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin5", i__2, "f_zznamfrm__", (ftnlen)911)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SUBPNT TRGEPC", &trgep5[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep5", i__1, "f_zznamfrm__", (ftnlen)913)], 
		"=", &trgep5[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep5", i__2, "f_zznamfrm__", (ftnlen)913)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SUBPNT SRFVEC", &srfve5[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve5", i__1, "f_zznamfrm__", (ftnlen)
		915)], "=", &srfve5[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve5", i__2, "f_zznamfrm__", (ftnlen)915)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	subslr_("Near point: ellipsoid", body2, &et, frame2, "NONE", "MERCURY"
		, &spoin7[(i__1 = i__ * 3 - 3) < 12 && 0 <= i__1 ? i__1 : 
		s_rnge("spoin7", i__1, "f_zznamfrm__", (ftnlen)923)], &trgep7[
		(i__2 = i__ - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge("trgep7", 
		i__2, "f_zznamfrm__", (ftnlen)923)], &srfve7[(i__3 = i__ * 3 
		- 3) < 12 && 0 <= i__3 ? i__3 : s_rnge("srfve7", i__3, "f_zz"
		"namfrm__", (ftnlen)923)], (ftnlen)21, (ftnlen)32, (ftnlen)32, 
		(ftnlen)4, (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SUBSLR SPOINT", &spoin7[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("spoin7", i__1, "f_zznamfrm__", (ftnlen)
		928)], "=", &spoin7[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("spoin7", i__2, "f_zznamfrm__", (ftnlen)928)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);
	chcksd_("SUBSLR TRGEPC", &trgep7[(i__1 = i__ - 1) < 4 && 0 <= i__1 ? 
		i__1 : s_rnge("trgep7", i__1, "f_zznamfrm__", (ftnlen)930)], 
		"=", &trgep7[(i__2 = ci - 1) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"trgep7", i__2, "f_zznamfrm__", (ftnlen)930)], &c_b334, ok, (
		ftnlen)13, (ftnlen)1);
	chckad_("SUBSLR SRFVEC", &srfve7[(i__1 = i__ * 3 - 3) < 12 && 0 <= 
		i__1 ? i__1 : s_rnge("srfve7", i__1, "f_zznamfrm__", (ftnlen)
		932)], "=", &srfve7[(i__2 = ci * 3 - 3) < 12 && 0 <= i__2 ? 
		i__2 : s_rnge("srfve7", i__2, "f_zznamfrm__", (ftnlen)932)], &
		c__3, &c_b334, ok, (ftnlen)13, (ftnlen)1);

/*        ----------------------------------------------------------- */

	t_ctrbeqf__();
	sxform_(frame1, frame2, &et, &mat3[(i__1 = (i__ * 6 + 1) * 6 - 42) < 
		144 && 0 <= i__1 ? i__1 : s_rnge("mat3", i__1, "f_zznamfrm__",
		 (ftnlen)940)], (ftnlen)32, (ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_("SXFORM XFORM", &mat3[(i__1 = (i__ * 6 + 1) * 6 - 42) < 144 &&
		 0 <= i__1 ? i__1 : s_rnge("mat3", i__1, "f_zznamfrm__", (
		ftnlen)943)], "=", &mat3[(i__2 = (ci * 6 + 1) * 6 - 42) < 144 
		&& 0 <= i__2 ? i__2 : s_rnge("mat3", i__2, "f_zznamfrm__", (
		ftnlen)943)], &c__36, &c_b334, ok, (ftnlen)12, (ftnlen)1);
    }

/*     Clean up. */

    kilfil_("fun.bsp", (ftnlen)7);
    kilfil_("fun.ker", (ftnlen)7);
    kilfil_("fun.bc", (ftnlen)6);
    kilfil_("fun.tsc", (ftnlen)7);

/*     This is good enough for now. */

    t_success__(ok);
    return 0;
} /* f_zznamfrm__ */

