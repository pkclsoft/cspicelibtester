/* f_zznofcon.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__82 = 82;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__14 = 14;
static integer c__1 = 1;

/* $Procedure F_ZZNOFCON ( ZZNOFCON tests ) */
/* Subroutine */ int f_zznofcon__(logical *ok)
{
    /* Initialized data */

    static char bases[32*5] = "J2000                           " "BOGUS_CK_-"
	    "9999                  " "BOGUS_CK_-9999                  " "BOGU"
	    "S_CK_-10000                 " "J2000                           ";
    static char frames[32*5] = "BOGUS_TK_1                      " "BOGUS_TK_"
	    "2                      " "BOGUS_CK_-9999                  " "BOG"
	    "US_CK_-10000                 " "BOGUS_TK_3                      ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer), s_cmp(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    logical isck;
    integer path;
    extern /* Subroutine */ int zznofcon_(doublereal *, integer *, integer *, 
	    integer *, integer *, char *, ftnlen);
    integer i__, j, k, l, m, clkid;
    extern /* Subroutine */ int etcal_(doublereal *, char *, ftnlen), tcase_(
	    char *, ftnlen);
    integer class__;
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmd_(char *, char *, doublereal *, 
	    integer *, char *, ftnlen, ftnlen, ftnlen);
    logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    char title[600];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    char trans[1840];
    extern integer rtrim_(char *, ftnlen);
    char etstr[35];
    extern /* Subroutine */ int t_success__(logical *), tstck3_(char *, char *
	    , logical *, logical *, logical *, integer *, ftnlen, ftnlen);
    integer baseid[2];
    doublereal et;
    integer handle;
    extern /* Subroutine */ int chcksc_(char *, char *, char *, char *, 
	    logical *, ftnlen, ftnlen, ftnlen, ftnlen), kclear_(void);
    integer framid[2];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     ckmeta_(integer *, char *, integer *, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    static char kerbuf[80*82];
    integer center, clssid;
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen), unload_(
	    char *, ftnlen), frinfo_(integer *, integer *, integer *, integer 
	    *, logical *);
    char ckwarn[1840], closng[1840];
    integer nosclk;
    char insuff[1840], errmsg[1840], scwarn[1840];
    extern /* Subroutine */ int remsub_(char *, integer *, integer *, char *, 
	    ftnlen, ftnlen), lmpool_(char *, integer *, ftnlen), furnsh_(char 
	    *, ftnlen);
    extern logical zzsclk_(integer *, integer *);
    char timstr[1840];

/* $ Abstract */

/*     Exercise the SPICELIB routine ZZNOFCON. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include File:  SPICELIB Error Handling Parameters */

/*        errhnd.inc  Version 2    18-JUN-1997 (WLT) */

/*           The size of the long error message was */
/*           reduced from 25*80 to 23*80 so that it */
/*           will be accepted by the Microsoft Power Station */
/*           FORTRAN compiler which has an upper bound */
/*           of 1900 for the length of a character string. */

/*        errhnd.inc  Version 1    29-JUL-1997 (NJB) */



/*     Maximum length of the long error message: */


/*     Maximum length of the short error message: */


/*     End Include File:  SPICELIB Error Handling Parameters */

/* $ Abstract */

/*     The parameters below form an enumerated list of the recognized */
/*     frame types.  They are: INERTL, PCK, CK, TK, DYN.  The meanings */
/*     are outlined below. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     INERTL      an inertial frame that is listed in the routine */
/*                 CHGIRF and that requires no external file to */
/*                 compute the transformation from or to any other */
/*                 inertial frame. */

/*     PCK         is a frame that is specified relative to some */
/*                 INERTL frame and that has an IAU model that */
/*                 may be retrieved from the PCK system via a call */
/*                 to the routine TISBOD. */

/*     CK          is a frame defined by a C-kernel. */

/*     TK          is a "text kernel" frame.  These frames are offset */
/*                 from their associated "relative" frames by a */
/*                 constant rotation. */

/*     DYN         is a "dynamic" frame.  These currently are */
/*                 parameterized, built-in frames where the full frame */
/*                 definition depends on parameters supplied via a */
/*                 frame kernel. */

/*     ALL         indicates any of the above classes. This parameter */
/*                 is used in APIs that fetch information about frames */
/*                 of a specified class. */


/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     W.L. Taber      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 4.0.0, 08-MAY-2012 (NJB) */

/*       The parameter ALL was added to support frame fetch APIs. */

/* -    SPICELIB Version 3.0.0, 28-MAY-2004 (NJB) */

/*       The parameter DYN was added to support the dynamic frame class. */

/* -    SPICELIB Version 2.0.0, 12-DEC-1996 (WLT) */

/*        Various unused frames types were removed and the */
/*        frame time TK was added. */

/* -    SPICELIB Version 1.0.0, 10-DEC-1995 (WLT) */

/* -& */

/*     End of INCLUDE file frmtyp.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZNOFCON. ZZNOFCON */
/*     creates a long error message for the SPICE(NOFRAMECONNECT) */
/*     error. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.1.0, 12-MAY-2016 (EDW) */

/*        Added KCLEAR call and end of fire to clear kernel pool */
/*        of the loaded CK */

/* -    TSPICE Version 2.0.0, 09-SEP-2013 (NJB) */

/*        Expected long messages were updated to */
/*        reflect new long message text. The text */
/*        mentions the possibility of CK lookup */
/*        failure due to the absence of required */
/*        angular velocity data. */

/* -    TSPICE Version 1.0.0, 26-MAR-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Other functions */


/*     Local Parameters */


/*     Note: the name "CK" is already in use; it's a parameter */
/*     in frmtyp.inc. */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     The simple test cases. */


/*     Open the test family. */

    topen_("F_ZZNOFCON", (ftnlen)10);

/* --- Case: ------------------------------------------------------ */

    tcase_("Load kernels and POOL data", (ftnlen)26);

/*     Initialize ET. */

    et = 0.;

/*     Initialize the frame kernel buffer. */

/*     Note that the CK frame BOGUS_CK_-10000 has been altered */
/*     so that it *doesn't* refer to the CK and SCLK IDs -10000 */
/*     and -9, which are used by TSTCK3. This CK frame never */
/*     has available pointing or SCLK data. */

    s_copy(kerbuf, "      FRAME_BOGUS_TK_1      =  1400000", (ftnlen)80, (
	    ftnlen)38);
    s_copy(kerbuf + 80, "      FRAME_1400000_NAME     = 'BOGUS_TK_1'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 160, "      FRAME_1400000_CLASS    =  4", (ftnlen)80, (
	    ftnlen)33);
    s_copy(kerbuf + 240, "      FRAME_1400000_CLASS_ID =  1400000", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 320, "      FRAME_1400000_CENTER   =   400000", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 400, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 480, "      OBJECT_400000_FRAME    = 'BOGUS_TK_1'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 640, "      TKFRAME_BOGUS_TK_1_RELATIVE = 'BOGUS_CK_-999"
	    "9'", (ftnlen)80, (ftnlen)52);
    s_copy(kerbuf + 720, "      TKFRAME_BOGUS_TK_1_SPEC     = 'ANGLES'", (
	    ftnlen)80, (ftnlen)44);
    s_copy(kerbuf + 800, "      TKFRAME_BOGUS_TK_1_UNITS    = 'DEGREES'", (
	    ftnlen)80, (ftnlen)45);
    s_copy(kerbuf + 880, "      TKFRAME_BOGUS_TK_1_AXES     = ( 3, 2, 3 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 960, "      TKFRAME_BOGUS_TK_1_ANGLES   = ( -243.1264966"
	    "75,", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 1040, "                                        -54.65782"
	    "2839,", (ftnlen)80, (ftnlen)54);
    s_copy(kerbuf + 1120, "                                        180.0 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 1200, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 1280, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 1360, "      FRAME_BOGUS_TK_2      =  1400001", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 1440, "      FRAME_1400001_NAME     = 'BOGUS_TK_2'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 1520, "      FRAME_1400001_CLASS    =  4", (ftnlen)80, (
	    ftnlen)33);
    s_copy(kerbuf + 1600, "      FRAME_1400001_CLASS_ID =  1400001", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 1680, "      FRAME_1400001_CENTER   =   400001", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 1760, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 1840, "      OBJECT_400001_FRAME    = 'BOGUS_TK_2'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 1920, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 2000, "      TKFRAME_BOGUS_TK_2_RELATIVE = 'BOGUS_TK_1'", 
	    (ftnlen)80, (ftnlen)48);
    s_copy(kerbuf + 2080, "      TKFRAME_BOGUS_TK_2_SPEC     = 'ANGLES'", (
	    ftnlen)80, (ftnlen)44);
    s_copy(kerbuf + 2160, "      TKFRAME_BOGUS_TK_2_UNITS    = 'DEGREES'", (
	    ftnlen)80, (ftnlen)45);
    s_copy(kerbuf + 2240, "      TKFRAME_BOGUS_TK_2_AXES     = ( 3, 2, 3 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 2320, "      TKFRAME_BOGUS_TK_2_ANGLES   = ( -243.126496"
	    "675,", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 2400, "                                        -54.65782"
	    "2839,", (ftnlen)80, (ftnlen)54);
    s_copy(kerbuf + 2480, "                                        180.0 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 2560, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 2640, "      FRAME_BOGUS_CK_-9999        = -9999", (
	    ftnlen)80, (ftnlen)41);
    s_copy(kerbuf + 2720, "      FRAME_-9999_NAME            = 'BOGUS_CK_-99"
	    "99'", (ftnlen)80, (ftnlen)52);
    s_copy(kerbuf + 2800, "      FRAME_-9999_CLASS           =  3", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 2880, "      FRAME_-9999_CLASS_ID        = -9999", (
	    ftnlen)80, (ftnlen)41);
    s_copy(kerbuf + 2960, "      FRAME_-9999_CENTER          = -9999", (
	    ftnlen)80, (ftnlen)41);
    s_copy(kerbuf + 3040, "      CK_-9999_SCLK               = -9", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 3120, "      OBJECT_-9999_FRAME          = 'BOGUS_CK_-99"
	    "99'", (ftnlen)80, (ftnlen)52);
    s_copy(kerbuf + 3200, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 3280, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 3360, "      FRAME_BOGUS_CK_-10000       = -10000", (
	    ftnlen)80, (ftnlen)42);
    s_copy(kerbuf + 3440, "      FRAME_-10000_NAME           = 'BOGUS_CK_-10"
	    "000'", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 3520, "      FRAME_-10000_CLASS          =  3", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 3600, "      FRAME_-10000_CLASS_ID       = -10", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 3680, "      FRAME_-10000_CENTER         = -10000", (
	    ftnlen)80, (ftnlen)42);
    s_copy(kerbuf + 3760, "      CK_-10_SCLK                 = -1", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 3840, "      OBJECT_-10000_FRAME         = 'BOGUS_CK_-10"
	    "000'", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 3920, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 4000, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 4080, "      FRAME_BOGUS_TK_3      =  1400002", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 4160, "      FRAME_1400002_NAME     = 'BOGUS_TK_3'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 4240, "      FRAME_1400002_CLASS    =  4", (ftnlen)80, (
	    ftnlen)33);
    s_copy(kerbuf + 4320, "      FRAME_1400002_CLASS_ID =  1400002", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 4400, "      FRAME_1400002_CENTER   =   400002", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 4480, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 4560, "      OBJECT_400002_FRAME    = 'BOGUS_TK_3'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 4640, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 4720, "      TKFRAME_BOGUS_TK_3_RELATIVE = 'J2000'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 4800, "      TKFRAME_BOGUS_TK_3_SPEC     = 'ANGLES'", (
	    ftnlen)80, (ftnlen)44);
    s_copy(kerbuf + 4880, "      TKFRAME_BOGUS_TK_3_UNITS    = 'DEGREES'", (
	    ftnlen)80, (ftnlen)45);
    s_copy(kerbuf + 4960, "      TKFRAME_BOGUS_TK_3_AXES     = ( 3, 2, 3 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 5040, "      TKFRAME_BOGUS_TK_3_ANGLES   = ( -243.126496"
	    "675,", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 5120, "                                        -54.65782"
	    "2839,", (ftnlen)80, (ftnlen)54);
    s_copy(kerbuf + 5200, "                                        180.0 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 5280, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 5360, "      FRAME_BOGUS_TK_4      =  1400003", (ftnlen)
	    80, (ftnlen)38);
    s_copy(kerbuf + 5440, "      FRAME_1400003_NAME     = 'BOGUS_TK_4'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 5520, "      FRAME_1400003_CLASS    =  4", (ftnlen)80, (
	    ftnlen)33);
    s_copy(kerbuf + 5600, "      FRAME_1400003_CLASS_ID =  1400003", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 5680, "      FRAME_1400003_CENTER   =   400003", (ftnlen)
	    80, (ftnlen)39);
    s_copy(kerbuf + 5760, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 5840, "      OBJECT_400003_FRAME    = 'BOGUS_TK_4'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 5920, " ", (ftnlen)80, (ftnlen)1);
    s_copy(kerbuf + 6000, "      TKFRAME_BOGUS_TK_4_RELATIVE = 'J2000'", (
	    ftnlen)80, (ftnlen)43);
    s_copy(kerbuf + 6080, "      TKFRAME_BOGUS_TK_4_SPEC     = 'ANGLES'", (
	    ftnlen)80, (ftnlen)44);
    s_copy(kerbuf + 6160, "      TKFRAME_BOGUS_TK_4_UNITS    = 'DEGREES'", (
	    ftnlen)80, (ftnlen)45);
    s_copy(kerbuf + 6240, "      TKFRAME_BOGUS_TK_4_AXES     = ( 3, 2, 3 )", (
	    ftnlen)80, (ftnlen)47);
    s_copy(kerbuf + 6320, "      TKFRAME_BOGUS_TK_4_ANGLES   = ( -243.126496"
	    "675,", (ftnlen)80, (ftnlen)53);
    s_copy(kerbuf + 6400, "                                        -54.65782"
	    "2839,", (ftnlen)80, (ftnlen)54);
    s_copy(kerbuf + 6480, "                                        180.0 )", (
	    ftnlen)80, (ftnlen)47);
    lmpool_(kerbuf, &c__82, (ftnlen)80);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create CK and SCLK kernels. Don't load the kernels; keep */
/*     the SCLK kernel. */

    tstck3_("zznofcon.bc", "zznofcon.tsc", &c_false, &c_false, &c_true, &
	    handle, (ftnlen)11, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 5; ++i__) {

/*        Frame and base I map to the first frame ID, base ID pair. */

	namfrm_(frames + (((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"frames", i__1, "f_zznofcon__", (ftnlen)397)) << 5), framid, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	namfrm_(bases + (((i__1 = i__ - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		"bases", i__1, "f_zznofcon__", (ftnlen)400)) << 5), baseid, (
		ftnlen)32);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 5; ++j) {

/*           Frame and base J map to the second frame ID, base ID pair. */

	    namfrm_(frames + (((i__1 = j - 1) < 5 && 0 <= i__1 ? i__1 : 
		    s_rnge("frames", i__1, "f_zznofcon__", (ftnlen)408)) << 5)
		    , &framid[1], (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    namfrm_(bases + (((i__1 = j - 1) < 5 && 0 <= i__1 ? i__1 : s_rnge(
		    "bases", i__1, "f_zznofcon__", (ftnlen)411)) << 5), &
		    baseid[1], (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Set ET. */

	    et = i__ * 1e8 + j;
	    for (m = 1; m <= 2; ++m) {

/*              Load SCLK kernel if M is 2. */

		if (m == 2) {
		    furnsh_("zznofcon.tsc", (ftnlen)12);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		} else {
		    unload_("zznofcon.tsc", (ftnlen)12);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    lmpool_(kerbuf, &c__82, (ftnlen)80);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		}

/* --- Case: ------------------------------------------------------ */

		if (j != i__) {
		    s_copy(title, "Fr 1: #, Bs 1: #, Fr 2: #, Bs 2: #, SCLK:#"
			    , (ftnlen)600, (ftnlen)42);
		    repmc_(title, "#", frames, title, (ftnlen)600, (ftnlen)1, 
			    (ftnlen)32, (ftnlen)600);
		    repmc_(title, "#", bases, title, (ftnlen)600, (ftnlen)1, (
			    ftnlen)32, (ftnlen)600);
		    repmc_(title, "#", frames + 32, title, (ftnlen)600, (
			    ftnlen)1, (ftnlen)32, (ftnlen)600);
		    repmc_(title, "#", bases + 32, title, (ftnlen)600, (
			    ftnlen)1, (ftnlen)32, (ftnlen)600);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    if (m == 1) {
			repmc_(title, "#", "N", title, (ftnlen)600, (ftnlen)1,
				 (ftnlen)1, (ftnlen)600);
		    } else {
			repmc_(title, "#", "Y", title, (ftnlen)600, (ftnlen)1,
				 (ftnlen)1, (ftnlen)600);
		    }
		    tcase_(title, (ftnlen)600);
		    zznofcon_(&et, framid, baseid, &framid[1], &baseid[1], 
			    errmsg, (ftnlen)1840);

/*                 Indicate this is not a CK case to start. We're not */
/*                 yet missing SCLK or CK data. */

		    isck = FALSE_;
		    nosclk = 0;

/*                 Create the expected time string; if this */
/*                 string is at the start of the message, */
/*                 subtract it from the message. */

		    s_copy(timstr, "At epoch # TDB (# TDB),", (ftnlen)1840, (
			    ftnlen)23);
		    repmd_(timstr, "#", &et, &c__14, timstr, (ftnlen)1840, (
			    ftnlen)1, (ftnlen)1840);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    etcal_(&et, etstr, (ftnlen)35);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    repmc_(timstr, "#", etstr, timstr, (ftnlen)1840, (ftnlen)
			    1, (ftnlen)35, (ftnlen)1840);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    l = rtrim_(timstr, (ftnlen)1840);
		    if (s_cmp(errmsg, timstr, l, (ftnlen)1840) == 0) {
			remsub_(errmsg, &c__1, &l, errmsg, (ftnlen)1840, (
				ftnlen)1840);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }

/*                 Create the "insufficient data" string; if */
/*                 string is at the start of the message, */
/*                 subtract it from the message. */

		    s_copy(insuff, " there is insufficient information avail"
			    "able to transform from reference frame # (#) to "
			    "reference frame # (#).", (ftnlen)1840, (ftnlen)
			    110);
		    repmi_(insuff, "#", framid, insuff, (ftnlen)1840, (ftnlen)
			    1, (ftnlen)1840);
		    repmc_(insuff, "#", frames + (((i__1 = i__ - 1) < 5 && 0 
			    <= i__1 ? i__1 : s_rnge("frames", i__1, "f_zznof"
			    "con__", (ftnlen)512)) << 5), insuff, (ftnlen)1840,
			     (ftnlen)1, (ftnlen)32, (ftnlen)1840);
		    repmi_(insuff, "#", &framid[1], insuff, (ftnlen)1840, (
			    ftnlen)1, (ftnlen)1840);
		    repmc_(insuff, "#", frames + (((i__1 = j - 1) < 5 && 0 <= 
			    i__1 ? i__1 : s_rnge("frames", i__1, "f_zznofcon"
			    "__", (ftnlen)514)) << 5), insuff, (ftnlen)1840, (
			    ftnlen)1, (ftnlen)32, (ftnlen)1840);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    l = rtrim_(insuff, (ftnlen)1840);
		    if (s_cmp(errmsg, insuff, l, (ftnlen)1840) == 0) {
			remsub_(errmsg, &c__1, &l, errmsg, (ftnlen)1840, (
				ftnlen)1840);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
		    }

/*                 Now clean up the path-dependent message portions. */

		    for (path = 1; path <= 2; ++path) {

/*                    Get info for the base frame. */

			frinfo_(&baseid[(i__1 = path - 1) < 2 && 0 <= i__1 ? 
				i__1 : s_rnge("baseid", i__1, "f_zznofcon__", 
				(ftnlen)534)], &center, &class__, &clssid, &
				found);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chcksl_("FRINFO found", &found, &c_true, ok, (ftnlen)
				12);

/*                    Indicate we've got at least one CK base if the */
/*                    current base is type CK. */

			if (class__ == 3) {
			    isck = TRUE_;
			}

/*                    Let K point to the index I or J, depending */
/*                    on which path we're looking at. */

			if (path == 1) {
			    k = i__;
			} else {
			    k = j;
			}

/*                    If the frame doesn't equal its base, create the */
/*                    transformation phrase for the first path. */

			if (framid[(i__1 = path - 1) < 2 && 0 <= i__1 ? i__1 :
				 s_rnge("framid", i__1, "f_zznofcon__", (
				ftnlen)563)] != baseid[(i__2 = path - 1) < 2 
				&& 0 <= i__2 ? i__2 : s_rnge("baseid", i__2, 
				"f_zznofcon__", (ftnlen)563)]) {

/*                       The path contains more than one node. */

			    s_copy(trans, " Frame # could be transformed to "
				    "frame # (#).", (ftnlen)1840, (ftnlen)45);
			    repmc_(trans, "#", frames + (((i__1 = k - 1) < 5 
				    && 0 <= i__1 ? i__1 : s_rnge("frames", 
				    i__1, "f_zznofcon__", (ftnlen)571)) << 5),
				     trans, (ftnlen)1840, (ftnlen)1, (ftnlen)
				    32, (ftnlen)1840);
			    repmi_(trans, "#", &baseid[(i__1 = path - 1) < 2 
				    && 0 <= i__1 ? i__1 : s_rnge("baseid", 
				    i__1, "f_zznofcon__", (ftnlen)572)], 
				    trans, (ftnlen)1840, (ftnlen)1, (ftnlen)
				    1840);
			    repmc_(trans, "#", bases + (((i__1 = k - 1) < 5 &&
				     0 <= i__1 ? i__1 : s_rnge("bases", i__1, 
				    "f_zznofcon__", (ftnlen)573)) << 5), 
				    trans, (ftnlen)1840, (ftnlen)1, (ftnlen)
				    32, (ftnlen)1840);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			    l = rtrim_(trans, (ftnlen)1840);
			    if (s_cmp(errmsg, trans, l, (ftnlen)1840) == 0) {
				remsub_(errmsg, &c__1, &l, errmsg, (ftnlen)
					1840, (ftnlen)1840);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
			    }
			    if (class__ == 3) {

/*                          If the base frame is a CK frame, create */
/*                          the CK warning for this frame. */

				s_copy(ckwarn, " The latter is a CK frame; a"
					" CK file containing data for instrum"
					"ent or structure # at the epoch show"
					"n above, as well as a corresponding "
					"SCLK kernel, must be loaded in order"
					" to use this frame.", (ftnlen)1840, (
					ftnlen)191);
				repmi_(ckwarn, "#", &clssid, ckwarn, (ftnlen)
					1840, (ftnlen)1, (ftnlen)1840);
				l = rtrim_(ckwarn, (ftnlen)1840);
				if (s_cmp(errmsg, ckwarn, l, (ftnlen)1840) == 
					0) {
				    remsub_(errmsg, &c__1, &l, errmsg, (
					    ftnlen)1840, (ftnlen)1840);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				}
			    }
			} else {

/*                       The path is a singleton. */

			    if (class__ == 3) {

/*                          If the base frame is a CK frame, create */
/*                          the CK warning for this frame. */

				s_copy(ckwarn, " # is a CK frame; a CK file "
					"containing data for instrument or st"
					"ructure # at the epoch shown above, "
					"as well as a corresponding SCLK kern"
					"el, must be loaded in order to use t"
					"his frame.", (ftnlen)1840, (ftnlen)
					182);
				repmc_(ckwarn, "#", bases + (((i__1 = k - 1) <
					 5 && 0 <= i__1 ? i__1 : s_rnge("bas"
					"es", i__1, "f_zznofcon__", (ftnlen)
					634)) << 5), ckwarn, (ftnlen)1840, (
					ftnlen)1, (ftnlen)32, (ftnlen)1840);
				repmi_(ckwarn, "#", &clssid, ckwarn, (ftnlen)
					1840, (ftnlen)1, (ftnlen)1840);
				l = rtrim_(ckwarn, (ftnlen)1840);
				if (s_cmp(errmsg, ckwarn, l, (ftnlen)1840) == 
					0) {
				    remsub_(errmsg, &c__1, &l, errmsg, (
					    ftnlen)1840, (ftnlen)1840);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				}
			    }

/*                       End of CK base case. */

			}

/*                    End path length case. */

/*                    Now, if the base frame is of CK type, */
/*                    and if there is no SCLK data for this frame, */
/*                    generate the SCLK warning message. */

			if (class__ == 3) {
			    ckmeta_(&clssid, "SCLK", &clkid, (ftnlen)4);
			    if (! zzsclk_(&clssid, &clkid)) {

/*                          Increment the count of bases missing */
/*                          SCLK data. */

				++nosclk;
				s_copy(scwarn, " No SCLK kernel for instrume"
					"nt or structure #, with correspondin"
					"g SCLK ID #, is currently loaded.", (
					ftnlen)1840, (ftnlen)97);
				repmi_(scwarn, "#", &clssid, scwarn, (ftnlen)
					1840, (ftnlen)1, (ftnlen)1840);
				repmi_(scwarn, "#", &clkid, scwarn, (ftnlen)
					1840, (ftnlen)1, (ftnlen)1840);
				l = rtrim_(scwarn, (ftnlen)1840);
				if (s_cmp(errmsg, scwarn, l, (ftnlen)1840) == 
					0) {
				    remsub_(errmsg, &c__1, &l, errmsg, (
					    ftnlen)1840, (ftnlen)1840);
				    chckxc_(&c_false, " ", ok, (ftnlen)1);
				}
			    }
			}
		    }

/*                 End of path loop. */

/*                 Create the closing message, if needed. */

		    if (isck && nosclk < 2) {
			if (nosclk == 0) {

/*                       We have at least one CK base frame */
/*                       but no missing SCLK data. */

			    s_copy(closng, " Failure to find required CK dat"
				    "a could be due to one or more CK files n"
				    "ot having been loaded, or to the epoch s"
				    "hown above lying within a coverage gap o"
				    "r beyond the coverage bounds of the load"
				    "ed CK files. It is also possible that no"
				    " loaded CK file has required angular vel"
				    "ocity data for the input epoch, even if "
				    "a loaded CK does have attitude data for "
				    "that epoch. You can use CKBRIEF with the"
				    " -dump option to display coverage interv"
				    "als of a CK file.", (ftnlen)1840, (ftnlen)
				    449);
			} else {

/*                       We're missing one set of SCLK data. */

			    s_copy(closng, " For a CK frame for which the co"
				    "rresponding SCLK kernel has been loaded,"
				    " failure to find required CK data could "
				    "be due to one or more CK files not havin"
				    "g been loaded, or to the epoch shown abo"
				    "ve lying within a coverage gap or beyond"
				    " the coverage bounds of the loaded CK fi"
				    "les. It is also possible that no loaded "
				    "CK file has required angular velocity da"
				    "ta for the input epoch, even if a loaded"
				    " CK does have attitude data for that epo"
				    "ch. You can use CKBRIEF with the -dump o"
				    "ption to display coverage intervals of a"
				    " CK file.", (ftnlen)1840, (ftnlen)521);
			}

/*                    Remove the closing message. */

			l = rtrim_(closng, (ftnlen)1840);
			if (s_cmp(errmsg, closng, l, (ftnlen)1840) == 0) {
			    remsub_(errmsg, &c__1, &l, errmsg, (ftnlen)1840, (
				    ftnlen)1840);
			    chckxc_(&c_false, " ", ok, (ftnlen)1);
			}
		    }

/*                 Ok, here's the test. ERRMSG should be blank. */

		    chcksc_("ERRMSG post deletion", errmsg, "=", " ", ok, (
			    ftnlen)20, (ftnlen)1840, (ftnlen)1, (ftnlen)1);
		}

/*              End of J != I case. This is the block in which a */
/*              test message is created. */

	    }

/*           End of SCLK load loop. */

	}

/*        End of second path loop. */

    }

/*     End of first path loop. */


/*     Close out the test family. */


/*     Clean-up all kernels. */

    kclear_();
    t_success__(ok);
    return 0;
} /* f_zznofcon__ */

