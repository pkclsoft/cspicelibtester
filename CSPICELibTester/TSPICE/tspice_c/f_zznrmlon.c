/* f_zznrmlon.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;

/* $Procedure F_ZZNRMLON ( ZZNRMLON tests ) */
/* Subroutine */ int f_zznrmlon__(logical *ok)
{
    static doublereal atol;
    extern /* Subroutine */ int zznrmlon_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), tcase_(char *, ftnlen);
    static doublereal small, inmin, inmax;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xomin, xomax;
    extern doublereal twopi_(void);
    extern /* Subroutine */ int t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    static doublereal outmin, outmax, tol;

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZNRMLON. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZNRMLON. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 10-OCT-2016 (NJB) */

/*        Updated to treat as errors cases where */

/*           - the longitude bounds are equal */

/*           - the maximum bound is less than the minimum */
/*             bound and the bounds differ by an integer */
/*             multiple of 2*pi. */

/* -    TSPICE Version 1.0.0, 16-MAY-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */

/*     Local Variables */



/*     Saved values */


/*     Open the test family. */

    topen_("F_ZZNRMLON", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN = INMAX + <small>  < 0; ATOL = 0", (ftnlen)38);
    small = 1e-14;
    atol = 0.;
    inmin = pi_() * -1.7;
    inmax = inmin - small;
    xomin = inmin;
    xomax = inmax - small + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2 pi < INMIN < INMAX < -1.5 pi; ATOL = 0", (ftnlen)41);
    atol = 0.;
    inmin = pi_() * -1.7;
    inmax = pi_() * -1.6;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect equality. */

    tol = 0.;
    chcksd_("OUTMIN", &outmin, "=", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "=", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2 pi < INMIN < INMAX < -1.5 pi; ATOL > 0", (ftnlen)41);
    atol = 1e-13;
    inmin = pi_() * -1.7;
    inmax = pi_() * -1.6;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect equality. */

    tol = 0.;
    chcksd_("OUTMIN", &outmin, "=", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "=", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("1.5 pi < INMIN, INMAX < 2 pi; ATOL = 0", (ftnlen)38);
    atol = 0.;
    inmin = pi_() * 1.6;
    inmax = pi_() * 1.7;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect equality. */

    tol = 0.;
    chcksd_("OUTMIN", &outmin, "=", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "=", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("1.5 pi < INMIN, INMAX < 2 pi; ATOL > 0", (ftnlen)38);
    atol = 0.;
    inmin = pi_() * 1.6;
    inmax = pi_() * 1.7;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect equality. */

    tol = 0.;
    chcksd_("OUTMIN", &outmin, "=", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "=", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("1.5 pi < INMIN = INMAX - <small>  < 2 pi; ATOL = 0", (ftnlen)50);
    small = 1e-14;
    atol = 0.;
    inmin = pi_() * 1.7;
    inmax = inmin - small;
    xomin = inmin - twopi_();
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2 pi < INMIN < INMAX < INMIN + ATOL < -1.5 pi; ATOL > 0", (
	    ftnlen)56);

/*     The upper bound exceeds the lower bound by less than */
/*     the input tolerance. The upper bound is negative. */
/*     The upper bound should be shifted. */

    atol = 1e-13;
    inmin = pi_() * -1.7;
    inmax = inmin + atol / 10;
    xomin = inmin;
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("0 < INMIN < INMAX < INMIN + ATOL < 1.5 pi; ATOL > 0", (ftnlen)51);

/*     The upper bound exceeds the lower bound by less than */
/*     the input tolerance. The lower bound is positive. */
/*     The lower bound should be shifted. */

    atol = 1e-13;
    inmin = 1e-13;
    inmax = inmin + atol / 10;
    xomin = inmin - twopi_();
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2 pi < INMIN < INMAX < INMIN + ATOL < -1.5 pi; ATOL > 0", (
	    ftnlen)56);

/*     The upper bound exceeds the lower bound by a little more than */
/*     the input tolerance. */

    atol = 1e-13;
    inmin = pi_() * -1.7;
    inmax = inmin + atol * 1.1;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect exact agreement. */

    tol = 0.;
    chcksd_("OUTMIN", &outmin, "=", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "=", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < INMIN; INMAX < 0; |INMAX-INMIN| < 2pi; ATOL = 0", (ftnlen)
	    55);

/*     The bounds are out of order. The magnitude of the difference */
/*     is less than 2*pi. Tolerance is 0. */

    atol = 0.;
    inmin = 0.;
    inmax = pi_() * -1.7;
    xomin = inmin;
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < INMIN; INMAX < 0; |INMAX-INMIN| < 2pi; ATOL > 0", (ftnlen)
	    55);

/*     The bounds are out of order. The upper bound is negative. The */
/*     magnitude of the difference is less than 2*pi. Tolerance is > 0. */

    atol = 1e-13;
    inmin = 0.;
    inmax = pi_() * -1.7;
    xomin = inmin;
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < INMIN; INMAX > 0; |INMAX-INMIN| < 2pi; ATOL > 0", (ftnlen)
	    55);

/*     The bounds are out of order. The upper bound is positive. The */
/*     magnitude of the difference is less than 2*pi. Tolerance is > 0. */

/*     The lower bound should be shifted left. */

    atol = 1e-13;
    inmin = pi_() * 1.8;
    inmax = pi_() * .1;
    xomin = inmin - twopi_();
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < INMIN; INMAX < 0; |INMAX-INMIN| > 2pi; ATOL = 0.", (
	    ftnlen)56);

/*     This case requires both bounds to be shifted. */

    atol = 0.;
    inmin = pi_() * 1.;
    inmax = pi_() * -1.7;
    xomin = inmin - twopi_();
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN + 2*pi < INMAX; ATOL = 0", (ftnlen)30);

/*     This case requires the upper bound to be shifted. */

    atol = 0.;
    inmin = pi_() * -1.;
    inmax = pi_() * 1.7;
    xomin = inmin;
    xomax = inmax - twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN + 2*pi + ATOL < INMAX; ATOL > 0", (ftnlen)37);

/*     This case requires the upper bound to be shifted. */

    atol = 1e-13;
    inmin = pi_() * -1.;
    inmax = inmin + twopi_() + atol * 1.1;
    xomin = inmin;
    xomax = inmax - twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN + 2*pi < INMAX < INMIN + 2*pi + ATOL; ATOL > 0", (ftnlen)52)
	    ;

/*     This case requires the upper bound to be left as is. */

    atol = 1e-13;
    inmin = pi_() * -1.;
    inmax = inmin + twopi_() + atol * .5;
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2pi - ATOL < INMIN; ATOL > 0.", (ftnlen)30);

/*     This is a ridiculously large tolerance, but it is supposed */
/*     to be accepted. */

    atol = 1.;
    inmin = -twopi_() - atol + 1e-13;
    inmax = inmin + twopi_();
    xomin = -twopi_();
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < 2pi + ATOL; ATOL > 0.", (ftnlen)29);

/*     This is a ridiculously large tolerance, but it is supposed */
/*     to be accepted. */

    atol = 1.;
    inmax = twopi_() + atol - 1e-13;
    inmin = inmax - twopi_();
    xomin = inmin;
    xomax = twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2pi - ATOL < INMIN; INMAX < INMIN; ATOL > 0.", (ftnlen)45);

/*     This is a ridiculously large tolerance, but it is supposed */
/*     to be accepted. */

    atol = 1.;
    inmin = -twopi_() - atol + 1e-13;
    inmax = inmin - 1e-14;
    xomin = -twopi_();
    xomax = xomin + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < 2pi + ATOL; ATOL > 0.", (ftnlen)29);

/*     This is a ridiculously large tolerance, but it is supposed */
/*     to be accepted. */

    atol = 1.;
    inmax = twopi_() + atol - 1e-13;
    inmin = inmax + 1e-14;
    xomax = twopi_();
    xomin = xomax - twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect tight agreement. */

    tol = 1e-14;
    chcksd_("OUTMIN", &outmin, "~", &xomin, &tol, ok, (ftnlen)6, (ftnlen)1);
    chcksd_("OUTMAX", &outmax, "~", &xomax, &tol, ok, (ftnlen)6, (ftnlen)1);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Negative tolerance.", (ftnlen)19);
    atol = -1e-13;
    inmin = pi_() * -1.;
    inmax = inmin + twopi_();
    xomin = inmin;
    xomax = inmax;
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN < -2pi - ATOL", (ftnlen)19);
    atol = 1e-13;
    inmin = -twopi_() - atol * 1.1;
    inmax = inmin + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN >  2pi + ATOL", (ftnlen)19);
    atol = 1e-13;
    inmin = twopi_() + atol * 1.1;
    inmax = twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX < -2pi - ATOL", (ftnlen)19);
    atol = 1e-13;
    inmax = -twopi_() - atol * 1.1;
    inmax = inmin + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMAX >  2pi + ATOL", (ftnlen)19);
    atol = 1e-13;
    inmax = twopi_() + atol * 1.1;
    inmin = twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("INMIN = INMAX < 0; ATOL = 0", (ftnlen)27);
    atol = 0.;
    inmin = pi_() * -1.7;
    inmax = inmin;
    xomin = inmin;
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("-2 pi < INMIN = INMAX < -1.5 pi; ATOL = 0", (ftnlen)41);
    atol = 0.;
    inmin = pi_() * -1.7;
    inmax = inmin;
    xomin = inmin;
    xomax = inmax + twopi_();
    zznrmlon_(&inmin, &inmax, &atol, &outmin, &outmax);
    chckxc_(&c_true, "SPICE(ZEROBOUNDSEXTENT)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zznrmlon__ */

