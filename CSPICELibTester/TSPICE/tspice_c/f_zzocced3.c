/* f_zzocced3.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b5 = -10.;
static doublereal c_b6 = 10.;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__3 = 3;
static doublereal c_b13 = 1.;
static doublereal c_b14 = 100.;
static doublereal c_b55 = -1e3;
static doublereal c_b56 = 1e3;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static doublereal c_b64 = -1.;
static doublereal c_b65 = 4.;
static doublereal c_b75 = 0.;
static integer c__0 = 0;
static integer c_n3 = -3;
static doublereal c_b134 = 1e-12;
static integer c__2048 = 2048;
static integer c_n1 = -1;
static integer c_n2 = -2;

/* $Procedure      F_ZZOCCED3 ( Test ellipsoid occultation routine ) */
/* Subroutine */ int f_zzocced3__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_dd(doublereal *, doublereal *);
    integer s_rnge(char *, integer, char *, integer);
    double sin(doublereal);

    /* Local variables */
    doublereal diff;
    integer code;
    doublereal beta;
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    doublereal high;
    integer seed;
    doublereal limb[9], chop[4], rvec[3], dist[2], axis[3], rmat[9]	/* 
	    was [3][3] */;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    integer nitr;
    extern doublereal vsep_(doublereal *, doublereal *);
    doublereal xsep;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), mtxv_(doublereal *, 
	    doublereal *, doublereal *), eul2m_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, doublereal *);
    integer i__, j, k;
    doublereal gamma, r__[6]	/* was [3][2] */, alpha, angle, scale, y[3], 
	    z__[3];
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), tcase_(char *, ftnlen);
    doublereal basis[18]	/* was [3][3][2] */, xlimb[9];
    logical found;
    doublereal midpt, limit;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    doublereal vpoff[3];
    char title[255];
    doublereal rsign;
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *), topen_(char *, ftnlen);
    doublereal lview[3];
    extern /* Subroutine */ int xpose_(doublereal *, doublereal *);
    extern doublereal vnorm_(doublereal *);
    doublereal v1[3], v2[3], v3[3];
    integer nxpts;
    extern /* Subroutine */ int t_success__(logical *), vrotv_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), el2cgv_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), cgv2el_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal centr1[3], centr2[3], semax1[9]	/* was [3][3] */, semax2[9]	
	    /* was [3][3] */, vpoff2[3], vpctr1[3], vpctr2[3];
    extern /* Subroutine */ int nvp2pl_(doublereal *, doublereal *, 
	    doublereal *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int edlimb_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen);
    doublereal lmbmaj[3];
    extern doublereal dasine_(doublereal *, doublereal *), halfpi_(void);
    doublereal vpxpt1[3];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen), 
	    chcksi_(char *, integer *, char *, integer *, integer *, logical *
	    , ftnlen, ftnlen);
    doublereal bigsep, minang[2], maxang[2], maxrad[2], lmbmin[3], minrad[3];
    extern /* Subroutine */ int latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal lmbctr[3];
    extern /* Subroutine */ int inelpl_(doublereal *, doublereal *, integer *,
	     doublereal *, doublereal *);
    doublereal normal[3], srfdir[3], radsum;
    extern /* Subroutine */ int vhatip_(doublereal *);
    doublereal ctrsep;
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *);
    doublereal newctr[3], uvpoff[3], tstvec[3];
    extern /* Subroutine */ int surfnm_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal viewpt[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *), surfpt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    extern integer t_occed__(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal alt, lat, lon;
    extern doublereal t_randd__(doublereal *, doublereal *, integer *);
    doublereal low;
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;
    doublereal xpt[3];
    extern integer zzocced_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    doublereal xpt1[3], xpt2[3];

/* $ Abstract */

/*     This routine tests the SPICELIB routine */

/*        ZZOCCED */

/*     by searching for occultation state transitions and comparing */
/*     them to those found using the alternate occultation classification */
/*     routine */

/*        T_OCCED */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-NOV-2006 (NJB) */

/* -& */

/*     SPICELIB functions */

/* $ Abstract */

/*     Declare ZZOCCED return code parameters, comparison strings */
/*     and other parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     ELLIPSOID */
/*     GEOMETRY */
/*     GF */
/*     OCCULTATION */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 01-SEP-2005 (NJB) */

/* -& */
/*     The function returns an integer code indicating the geometric */
/*     relationship of the three bodies. */

/*     Codes and meanings are: */

/*        -3                    Total occultation of first target by */
/*                              second. */


/*        -2                    Annular occultation of first target by */
/*                              second.  The second target does not */
/*                              block the limb of the first. */


/*        -1                    Partial occultation of first target by */
/*                              second target. */


/*         0                    No occultation or transit:  both objects */
/*                              are completely visible to the observer. */


/*         1                    Partial occultation of second target by */
/*                              first target. */


/*         2                    Annular occultation of second target by */
/*                              first. */


/*         3                    Total occultation of second target by */
/*                              first. */


/*     End include file zzocced.inc */


/*     Other functions */


/*     Local parameters */


/*     Recommendation:  increase the parameters NCASE1 and NCASE2 */
/*     to 1000 for robust testing. */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZOCCED3", (ftnlen)10);

/*     The initial set of cases tests the accuracy with which */
/*     ZZOCCED can detect transitions between total occultation */
/*     and partial occultation. */

/*     In the discussion below, we use the terms "target" and */
/*     "ellipsoid" interchangeably */

/*     These cases attempt to cover a broad set of geometric cases. */
/*     In order to do this efficiently, random numbers are used */
/*     to generate most inputs: */

/*        - A random scale factor is applied uniformly to all */
/*          participating objects. */

/*        - The principal axis matrices of both targets are */
/*          generated from random Euler angles. */

/*        - The radii of each target are chosen randomly, with */
/*          each radius in the range of 1:100 prior to scaling. */

/*        - The location of the observer's sub-point on the first */
/*          target is selected using a direction vector with random */
/*          components.  The ray emanating from the center of the */
/*          first target and parallel to this direction vector */
/*          defines the sub-point. */

/*        - The altitude of the observer above its sub-point on the */
/*          first target is selected randomly. */

/*        - The original position of the center of the second target */
/*          is on the ray emanating from the observer and passing */
/*          through the center of the first target.  The distance */
/*          between the target centers is chosen randomly. */

/*          The second target is scaled to make sure it is in total */
/*          occultation by the first target. */

/*        - To search for state transitions, the second target is */
/*          displaced from its original position.  The displacement */
/*          is accomplished by rotating the vector from the observer */
/*          to the center of the second target.  This rotated vector */
/*          is constrained to lie in a plane whose normal vector is */
/*          orthogonal to the vector from the observer to the center */
/*          of the first target.  The orientation of this normal vector */
/*          is selected randomly. */




/*     Initialize the random number seed. */

    seed = -1;
    for (i__ = 1; i__ <= 200; ++i__) {
/*        WRITE (*,*) '==========================' */

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "\"General\" case:  search for transitions from total "
		"to partial occultation for ellipsoids of different shape, si"
		"ze and orientation.  Loop iteration = #.", (ftnlen)255, (
		ftnlen)151);
	repmi_(title, "#", &i__, title, (ftnlen)255, (ftnlen)1, (ftnlen)255);
	tcase_(title, (ftnlen)255);

/*        Pick a scale factor; we'll scale all of the participating */
/*        objects using this factor. */

	d__1 = t_randd__(&c_b5, &c_b6, &seed);
	scale = pow_dd(&c_b6, &d__1);

/*        Create random orientation matrices for both ellipsoids.  We */
/*        start with three Euler angles.  Also pick random radii for */
/*        the ellipsoids. */

	for (j = 1; j <= 2; ++j) {
	    d__1 = -pi_();
	    d__2 = pi_();
	    alpha = t_randd__(&d__1, &d__2, &seed);
	    d__1 = -halfpi_();
	    d__2 = halfpi_();
	    beta = t_randd__(&d__1, &d__2, &seed);
	    d__1 = -pi_();
	    d__2 = pi_();
	    gamma = t_randd__(&d__1, &d__2, &seed);
	    eul2m_(&alpha, &beta, &gamma, &c__1, &c__2, &c__3, rmat);
	    xpose_(rmat, &basis[(i__1 = (j * 3 + 1) * 3 - 12) < 18 && 0 <= 
		    i__1 ? i__1 : s_rnge("basis", i__1, "f_zzocced3__", (
		    ftnlen)284)]);

/*           Pick unscaled radius values in the range 1:10 for the */
/*           Jth ellipsoid. */

	    r__[(i__1 = j * 3 - 3) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)290)] = t_randd__(&c_b13, &c_b14,
		     &seed);
	    r__[(i__1 = j * 3 - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)291)] = t_randd__(&c_b13, &c_b14,
		     &seed);
	    r__[(i__1 = j * 3 - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)292)] = t_randd__(&c_b13, &c_b14,
		     &seed);

/*           Scale the radii using our global scale factor. */

	    vsclip_(&scale, &r__[(i__1 = j * 3 - 3) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("r", i__1, "f_zzocced3__", (ftnlen)298)]);

/*           Save the minimum and maximum radii of each ellipsoid. */

/* Computing MIN */
	    d__1 = r__[(i__2 = j * 3 - 3) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "r", i__2, "f_zzocced3__", (ftnlen)303)], d__2 = r__[(
		    i__3 = j * 3 - 2) < 6 && 0 <= i__3 ? i__3 : s_rnge("r", 
		    i__3, "f_zzocced3__", (ftnlen)303)], d__1 = min(d__1,d__2)
		    , d__2 = r__[(i__4 = j * 3 - 1) < 6 && 0 <= i__4 ? i__4 : 
		    s_rnge("r", i__4, "f_zzocced3__", (ftnlen)303)];
	    minrad[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("minrad", 
		    i__1, "f_zzocced3__", (ftnlen)303)] = min(d__1,d__2);
/* Computing MAX */
	    d__1 = r__[(i__2 = j * 3 - 3) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "r", i__2, "f_zzocced3__", (ftnlen)304)], d__2 = r__[(
		    i__3 = j * 3 - 2) < 6 && 0 <= i__3 ? i__3 : s_rnge("r", 
		    i__3, "f_zzocced3__", (ftnlen)304)], d__1 = max(d__1,d__2)
		    , d__2 = r__[(i__4 = j * 3 - 1) < 6 && 0 <= i__4 ? i__4 : 
		    s_rnge("r", i__4, "f_zzocced3__", (ftnlen)304)];
	    maxrad[(i__1 = j - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge("maxrad", 
		    i__1, "f_zzocced3__", (ftnlen)304)] = max(d__1,d__2);
/*           WRITE (*,*) 'MINRAD, MAXRAD = ', MINRAD(J), MAXRAD(J) */

/*           Create the Jth semi-axis matrix by scaling the column */
/*           vectors of RMAT. */

	    for (k = 1; k <= 3; ++k) {
		if (j == 1) {
		    vscl_(&r__[(i__1 = k + j * 3 - 4) < 6 && 0 <= i__1 ? i__1 
			    : s_rnge("r", i__1, "f_zzocced3__", (ftnlen)315)],
			     &basis[(i__2 = (k + 3) * 3 - 12) < 18 && 0 <= 
			    i__2 ? i__2 : s_rnge("basis", i__2, "f_zzocced3__"
			    , (ftnlen)315)], &semax1[(i__3 = k * 3 - 3) < 9 &&
			     0 <= i__3 ? i__3 : s_rnge("semax1", i__3, "f_zz"
			    "occed3__", (ftnlen)315)]);
		} else {
		    vscl_(&r__[(i__1 = k + j * 3 - 4) < 6 && 0 <= i__1 ? i__1 
			    : s_rnge("r", i__1, "f_zzocced3__", (ftnlen)317)],
			     &basis[(i__2 = (k + 6) * 3 - 12) < 18 && 0 <= 
			    i__2 ? i__2 : s_rnge("basis", i__2, "f_zzocced3__"
			    , (ftnlen)317)], &semax2[(i__3 = k * 3 - 3) < 9 &&
			     0 <= i__3 ? i__3 : s_rnge("semax2", i__3, "f_zz"
			    "occed3__", (ftnlen)317)]);
		}
	    }
	}

/*        Pick a center for the first ellipsoid. */

	for (j = 1; j <= 3; ++j) {
	    centr1[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("centr1", 
		    i__1, "f_zzocced3__", (ftnlen)328)] = scale * t_randd__(&
		    c_b55, &c_b56, &seed);
	}

/*        Pick a random viewing point.  Start by picking longitude */
/*        and latitude of a surface point on the first ellipsoid. */

	d__1 = -pi_();
	d__2 = pi_();
	lon = t_randd__(&d__1, &d__2, &seed);
	d__1 = -halfpi_();
	d__2 = halfpi_();
	lat = t_randd__(&d__1, &d__2, &seed);
	latrec_(&c_b13, &lon, &lat, srfdir);

/*        Since the origin of this ray is the center of the */
/*        ellipsoid, we don't have to check the found flag. */

	surfpt_(origin, srfdir, r__, &r__[1], &r__[2], xpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Pick a random altitude. */

	d__1 = t_randd__(&c_b64, &c_b65, &seed);
	alt = scale * pow_dd(&c_b6, &d__1);

/*        Find the local outward unit surface normal, scale it by */
/*        the altitude, and add it to XPT to obtain the view point. */

	surfnm_(r__, &r__[1], &r__[2], xpt, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vlcom_(&c_b13, xpt, &alt, normal, viewpt);


/*        We want to find the limb of the first ellipsoid as seen */
/*        from the viewing point. */

/*        Express the viewing point as an offset from the center */
/*        of the first ellipsoid. */

	vsub_(viewpt, centr1, vpoff);

/*        Transform the viewing point into the principal axis frame */
/*        of the first ellipsoid. */

	mtxv_(basis, vpoff, lview);

/*        Find the limb of the ellipsoid.  Rotate the limb back to the */
/*        original reference frame and shift the limb center to reflect */
/*        the offset of the first ellipsoid's center from the origin. */

	edlimb_(r__, &r__[1], &r__[2], lview, xlimb);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	el2cgv_(xlimb, v1, v2, v3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	mxv_(basis, v1, lmbctr);
	mxv_(basis, v2, lmbmaj);
	mxv_(basis, v3, lmbmin);
	vadd_(centr1, lmbctr, v1);
	vequ_(v1, lmbctr);

/*        Construct the limb in the original frame. */

	cgv2el_(lmbctr, lmbmaj, lmbmin, limb);

/*        Determine the center of the second ellipsoid.  This ellipsoid */
/*        is centered on the ray from the view point through the center */
/*        of the first ellipsoid. */

/*        The second ellipsoid must be placed far enough from the first */
/*        so that no overlap of the ellipsoids occurs. */

	dist[0] = vnorm_(vpoff);

/*        Let UVPOFF be the unit vector pointing from the view point */
/*        to the center of the first ellipsoid. */

	vminus_(vpoff, uvpoff);
	vhatip_(uvpoff);

/*        Pick a random distance between the centers; the distance */
/*        must be at least 1.25 * the sum of the maximum radii of */
/*        the ellipsoids.  Here 1.25 is an arbitrary factor "slightly" */
/*        greater than 1. */

	radsum = maxrad[0] + maxrad[1];
	d__1 = radsum * 1.25;
	d__2 = radsum * 100.;
	ctrsep = t_randd__(&d__1, &d__2, &seed);

/*        The second center is placed at distance CTRSEP along */
/*        the ray emanating from the first center in direction UVPOFF. */

	vlcom_(&c_b13, centr1, &ctrsep, uvpoff, centr2);

/*        Now make sure the second ellipsoid is occulted.  We'll */
/*        adjust its size if necessary. */

/*        Find a lower bound on the angular radius, as seen from the */
/*        view point, of the first ellipsoid. */

	d__1 = minrad[0] / dist[0];
	minang[0] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the maximum angular radius of the second ellipsoid, */
/*        based on its nominal radii. */

	dist[1] = dist[0] + ctrsep;
	d__1 = maxrad[1] / dist[1];
	maxang[1] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	minang[1] = 0.;
/*         MINANG(2) = DASINE ( MINRAD(2)/DIST(2), 0.D0 ) */
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        If we can't guarantee the second ellipsoid is occulted based */
/*        on its maximum angular radius, scale it down until it "fits." */

	if (maxang[1] >= minang[0]) {

/*           Find the limit on MAXRAD(2).  We pick LIMIT so that */

/*              ASIN ( LIMIT/DIST(2) ) < MINANG(1) */

	    limit = dist[1] * .5 * sin(minang[0]);

/*           Scale down the radii of the second ellipsoid. */

	    d__1 = limit / maxrad[1];
	    vsclip_(&d__1, &r__[3]);
/* Computing MIN */
	    d__1 = min(r__[3],r__[4]);
	    minrad[1] = min(d__1,r__[5]);
/* Computing MAX */
	    d__1 = max(r__[3],r__[4]);
	    maxrad[1] = max(d__1,r__[5]);

/*           We must re-create the second semi-axis matrix too. */

	    for (j = 1; j <= 3; ++j) {
		vscl_(&r__[(i__1 = j + 2) < 6 && 0 <= i__1 ? i__1 : s_rnge(
			"r", i__1, "f_zzocced3__", (ftnlen)484)], &basis[(
			i__2 = (j + 6) * 3 - 12) < 18 && 0 <= i__2 ? i__2 : 
			s_rnge("basis", i__2, "f_zzocced3__", (ftnlen)484)], &
			semax2[(i__3 = j * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("semax2", i__3, "f_zzocced3__", (ftnlen)484)]);
	    }
	}

/*        Sanity check:  validate MAXANG(2). */

	d__1 = maxrad[1] / dist[1];
	maxang[1] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("MAXANG(2)", &maxang[1], "<", minang, &c_b75, ok, (ftnlen)9, (
		ftnlen)1);

/*        The second ellipsoid should be in total occultation */
/*        by the first.  Verify this. */

	code = zzocced_(viewpt, centr1, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial total occultation)", &code, "=", &c__3,
		 &c__0, ok, (ftnlen)40, (ftnlen)1);
	code = t_occed__(viewpt, centr1, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial total occultation)", &code, "=", &c__3,
		 &c__0, ok, (ftnlen)40, (ftnlen)1);

/*        Repeat with arguments switched. */

	code = zzocced_(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial total occ/switched)", &code, "=", &
		c_n3, &c__0, ok, (ftnlen)41, (ftnlen)1);
	code = t_occed__(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial total occ/switched)", &code, "=", &
		c_n3, &c__0, ok, (ftnlen)41, (ftnlen)1);

/*        Now we're going to displace the second ellipsoid in */
/*        a direction orthogonal to the vector UVPOFF.  We'll */
/*        do this by selecting a rotation axis and rotating the */
/*        vector from the viewpoint to the center of the second */
/*        ellipsoid about this axis until we detect a change of */
/*        occultation classification (from total to partial). */
/*        We'll then make sure that both ZZOCCED and our alternative */
/*        computation performed by T_OCCED agree that we have a */
/*        transition at the same angle. */

/*        We're now going to pick a random vector orthogonal to */
/*        UVPOFF.  First pick an orthogonal basis, with UVPOFF the */
/*        first vector of the basis. */

	frame_(uvpoff, y, z__);

/*        Pick a random rotation angle; we'll rotate Y about X by */
/*        this angle to create a rotation axis. */

	d__1 = -pi_();
	d__2 = pi_();
	angle = t_randd__(&d__1, &d__2, &seed);
	vrotv_(y, uvpoff, &angle, axis);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We're now going to bracket the rotation angle needed to */
/*        find the occultation state transition from total to partial. */
/*        If the vector from the view point to the center of the */
/*        second ellipsoid intersects the limb of the first ellipsoid, */
/*        we definitely have a partial occultation, so determine */
/*        the angular displacement required to make this happen. */

/*        Create the plane containing the second center and normal */
/*        to the rotation axis. */

	nvp2pl_(axis, centr2, chop);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Chop the first limb with the plane. */

	inelpl_(limb, chop, &nxpts, xpt1, xpt2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        If we don't have two points of intersection, something's */
/*        wrong. */

	chcksi_("NXPTS", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Find the vector from the view point to the first */
/*        intersection point XPT1; find the angular separation */
/*        of this vector from UVPOFF. */

	vsub_(xpt1, viewpt, vpxpt1);
	xsep = vsep_(vpxpt1, uvpoff);

/*        Determine the sign of the rotation angle by which */
/*        we rotate UVPOFF to align it with VPXPT1. */

	vrotv_(uvpoff, axis, &xsep, tstvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (vsep_(tstvec, vpxpt1) > 1e-12) {
	    xsep = -xsep;
	    vrotv_(uvpoff, axis, &xsep, tstvec);
	}
	d__1 = vsep_(tstvec, vpxpt1);
	chcksd_("VSEP(TSTVEC, VPXPT1)", &d__1, "~", &c_b75, &c_b134, ok, (
		ftnlen)20, (ftnlen)1);

/*        Time for another sanity check:  rotate the vector from */
/*        the viewpoint to the center of the second ellipsoid about */
/*        AXIS by XSEP, then add this to the view point, */
/*        yielding a new center vector for the second ellipsoid. */
/*        verify that the ellipsoid is in partial occultation. */

	vsub_(centr2, viewpt, vpctr2);
	vrotv_(vpctr2, axis, &xsep, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Sanity check:  make sure RVEC points from the view point */
/*        to XPT1. */

	d__1 = vsep_(rvec, vpxpt1);
	chcksd_("VSEP(RVEC, VPXPT1)", &d__1, "~", &c_b75, &c_b134, ok, (
		ftnlen)18, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
/*         CALL VSUB  ( NEWCTR, VIEWPT, TSTVEC ) */
/*         WRITE (*,*) 'VPXPT1 sep = ', VSEP ( TSTVEC, VPXPT1 ) */
/*         WRITE (*,*) 'VPCTR2 sep = ', VSEP ( TSTVEC, VPCTR2 ) */

/*        Check for partial occultation of the second ellipsoid. */

	code = t_occed__(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE, (initial partial occultation)", &code, "=", &
		c__1, &c__0, ok, (ftnlen)43, (ftnlen)1);
	code = zzocced_(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE, (initial partial occultation)", &code, "=", &
		c__1, &c__0, ok, (ftnlen)43, (ftnlen)1);

/*        Repeat with arguments switched. */

	code = zzocced_(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial partial occ/switched)", &code, "=", &
		c_n3, &c__0, ok, (ftnlen)43, (ftnlen)1);
	code = t_occed__(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial partial occ/switched)", &code, "=", &
		c_n3, &c__0, ok, (ftnlen)43, (ftnlen)1);

/*        We now know that a rotation of the vector from the */
/*        view point to the center of the second ellipsoid about */
/*        AXIS by an angle between 0 and XSEP radians should yield */
/*        a state transition.  Find the angle via binary search. */

	if (xsep > 0.) {
	    rsign = 1.;
	} else {
	    rsign = -1.;
	}
	low = 0.;
	high = abs(xsep);
	diff = high - low;
	nitr = 0;
	while(diff > 1e-14 && *ok) {
	    ++nitr;
	    chcksi_("NITR", &nitr, "<", &c__2048, &c__0, ok, (ftnlen)4, (
		    ftnlen)1);
	    midpt = (high + low) / 2.;
	    d__1 = rsign * midpt;
	    vrotv_(vpctr2, axis, &d__1, rvec);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vadd_(viewpt, rvec, newctr);
	    code = zzocced_(viewpt, centr1, semax1, newctr, semax2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (code == 1) {
		high = midpt;
	    } else {

/*              The code had better be TOTAL2. */

		chcksi_("(bisection) CODE", &code, "=", &c__3, &c__0, ok, (
			ftnlen)16, (ftnlen)1);
		low = midpt;
	    }
	    diff = high - low;
	}

/*        Now that we've dropped out of the loop, verify that T_OCCED */
/*        says the occultation is total at rotation angle RSIGN*LOW and */
/*        partial at rotation angle RSIGN*HIGH.  We adjust each */
/*        of these angles by ADJTOL to allow for differences in */
/*        round-off between ZZOCCED and T_OCCED. */


/*        Verify that when we switch the order of the ellipsoids, */
/*        we see a transition from total occultation of ellipsoid 1 */
/*        to partial occultation of ellipsoid 1. */


/*        Check for total occultation: */

	d__1 = rsign * (low - 1e-14);
	vrotv_(vpctr2, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (total)", &code, "=", &c__3, &c__0, ok, (
		ftnlen)25, (ftnlen)1);
	code = t_occed__(viewpt, newctr, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (total, args switched)", &code, "=", &c_n3,
		 &c__0, ok, (ftnlen)40, (ftnlen)1);

/*        Check for partial occultation: */

	d__1 = rsign * (high + 1e-14);
	vrotv_(vpctr2, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 0)", &code, "=", &c__1, &c__0, ok,
		 (ftnlen)29, (ftnlen)1);
	code = t_occed__(viewpt, newctr, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 0, args switched)", &code, "=", &
		c_n1, &c__0, ok, (ftnlen)44, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "\"General\" case:  search for transitions from partia"
		"l occultation to no occultation, for ellipsoids of different"
		" shape, size and orientation.  Loop iteration = #.", (ftnlen)
		255, (ftnlen)161);
	repmi_(title, "#", &i__, title, (ftnlen)255, (ftnlen)1, (ftnlen)255);
	tcase_(title, (ftnlen)255);

/*        The next step is to test for transitions from partial */
/*        occultation to no occultation. */

/*        Let BIGSEP be a displacement angle large enough to guarantee */
/*        that no occultation will be found. */

	bigsep = pi_();
	d__1 = rsign * bigsep;
	vrotv_(vpctr2, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = zzocced_(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE", &code, "=", &c__0, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*        We now know that a rotation of the vector from the */
/*        view point to the center of the second ellipsoid about */
/*        AXIS by an angle between 0 and XSEP radians should yield */
/*        a state transition.  Find the angle via binary search. */

	low = abs(xsep);
	high = bigsep;
	diff = high - low;
	nitr = 0;
	while(diff > 1e-14 && *ok) {
	    ++nitr;
	    chcksi_("NITR", &nitr, "<", &c__2048, &c__0, ok, (ftnlen)4, (
		    ftnlen)1);
	    midpt = (high + low) / 2.;
	    d__1 = rsign * midpt;
	    vrotv_(vpctr2, axis, &d__1, rvec);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vadd_(viewpt, rvec, newctr);
	    code = zzocced_(viewpt, centr1, semax1, newctr, semax2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (code == 0) {
		high = midpt;
	    } else {

/*              The code had better be PARTL2. */

		chcksi_("(bisection) CODE", &code, "=", &c__1, &c__0, ok, (
			ftnlen)16, (ftnlen)1);
		low = midpt;
	    }
	    diff = high - low;
	}

/*        Now that we've dropped out of the loop, verify that T_OCCED */
/*        says the occultation is partial at rotation angle RSIGN*LOW and */
/*        "none" at rotation angle RSIGN*HIGH.  We adjust each */
/*        of these angles by CNVTOL to allow for differences in */
/*        round-off between ZZOCCED and T_OCCED. */


/*        Check for partial occultation: */

	d__1 = rsign * (low - 1e-14);
	vrotv_(vpctr2, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 1)", &code, "=", &c__1, &c__0, ok,
		 (ftnlen)29, (ftnlen)1);

/*        Check for no occultation: */

	d__1 = rsign * (high + 1e-14);
	vrotv_(vpctr2, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, centr1, semax1, newctr, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (none)", &code, "=", &c__0, &c__0, ok, (
		ftnlen)24, (ftnlen)1);
    }
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */


/*         Transit cases follow. */


/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */
/* ***************************************************************** */


/*     The following tests are quite similar to the preceding ones, */
/*     with the difference that now we're going to have the first */
/*     ellipsoid start out in annular transit across the second one. */

    for (i__ = 1; i__ <= 200; ++i__) {
/*        WRITE (*,*) '==========================' */

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "\"General\" case:  search for transitions from annula"
		"r to partial transit for ellipsoids of different shape, size"
		" and orientation.  Loop iteration = #.", (ftnlen)255, (ftnlen)
		149);
	repmi_(title, "#", &i__, title, (ftnlen)255, (ftnlen)1, (ftnlen)255);
	tcase_(title, (ftnlen)255);

/*        Pick a scale factor; we'll scale all of the participating */
/*        objects using this factor. */

	d__1 = t_randd__(&c_b5, &c_b6, &seed);
	scale = pow_dd(&c_b6, &d__1);

/*        Create random orientation matrices for both ellipsoids.  We */
/*        start with three Euler angles.  Also pick random radii for */
/*        the ellipsoids. */

	for (j = 1; j <= 2; ++j) {
	    d__1 = -pi_();
	    d__2 = pi_();
	    alpha = t_randd__(&d__1, &d__2, &seed);
	    d__1 = -halfpi_();
	    d__2 = halfpi_();
	    beta = t_randd__(&d__1, &d__2, &seed);
	    d__1 = -pi_();
	    d__2 = pi_();
	    gamma = t_randd__(&d__1, &d__2, &seed);
	    eul2m_(&alpha, &beta, &gamma, &c__1, &c__2, &c__3, rmat);
	    xpose_(rmat, &basis[(i__1 = (j * 3 + 1) * 3 - 12) < 18 && 0 <= 
		    i__1 ? i__1 : s_rnge("basis", i__1, "f_zzocced3__", (
		    ftnlen)964)]);

/*           Pick unscaled radius values in the range 1:10 for the */
/*           Jth ellipsoid. */

	    r__[(i__1 = j * 3 - 3) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)970)] = t_randd__(&c_b13, &c_b14,
		     &seed);
	    r__[(i__1 = j * 3 - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)971)] = t_randd__(&c_b13, &c_b14,
		     &seed);
	    r__[(i__1 = j * 3 - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("r", i__1,
		     "f_zzocced3__", (ftnlen)972)] = t_randd__(&c_b13, &c_b14,
		     &seed);

/*           Scale the radii using our global scale factor. */

	    vsclip_(&scale, &r__[(i__1 = j * 3 - 3) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("r", i__1, "f_zzocced3__", (ftnlen)978)]);

/*           Save the minimum and maximum radii of each ellipsoid. */

/* Computing MIN */
	    d__1 = r__[(i__2 = j * 3 - 3) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "r", i__2, "f_zzocced3__", (ftnlen)983)], d__2 = r__[(
		    i__3 = j * 3 - 2) < 6 && 0 <= i__3 ? i__3 : s_rnge("r", 
		    i__3, "f_zzocced3__", (ftnlen)983)], d__1 = min(d__1,d__2)
		    , d__2 = r__[(i__4 = j * 3 - 1) < 6 && 0 <= i__4 ? i__4 : 
		    s_rnge("r", i__4, "f_zzocced3__", (ftnlen)983)];
	    minrad[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("minrad", 
		    i__1, "f_zzocced3__", (ftnlen)983)] = min(d__1,d__2);
/* Computing MAX */
	    d__1 = r__[(i__2 = j * 3 - 3) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "r", i__2, "f_zzocced3__", (ftnlen)984)], d__2 = r__[(
		    i__3 = j * 3 - 2) < 6 && 0 <= i__3 ? i__3 : s_rnge("r", 
		    i__3, "f_zzocced3__", (ftnlen)984)], d__1 = max(d__1,d__2)
		    , d__2 = r__[(i__4 = j * 3 - 1) < 6 && 0 <= i__4 ? i__4 : 
		    s_rnge("r", i__4, "f_zzocced3__", (ftnlen)984)];
	    maxrad[(i__1 = j - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge("maxrad", 
		    i__1, "f_zzocced3__", (ftnlen)984)] = max(d__1,d__2);
/*           WRITE (*,*) 'MINRAD, MAXRAD = ', MINRAD(J), MAXRAD(J) */

/*           Create the Jth semi-axis matrix by scaling the column */
/*           vectors of RMAT. */

	    for (k = 1; k <= 3; ++k) {
		if (j == 1) {
		    vscl_(&r__[(i__1 = k + j * 3 - 4) < 6 && 0 <= i__1 ? i__1 
			    : s_rnge("r", i__1, "f_zzocced3__", (ftnlen)995)],
			     &basis[(i__2 = (k + 3) * 3 - 12) < 18 && 0 <= 
			    i__2 ? i__2 : s_rnge("basis", i__2, "f_zzocced3__"
			    , (ftnlen)995)], &semax1[(i__3 = k * 3 - 3) < 9 &&
			     0 <= i__3 ? i__3 : s_rnge("semax1", i__3, "f_zz"
			    "occed3__", (ftnlen)995)]);
		} else {
		    vscl_(&r__[(i__1 = k + j * 3 - 4) < 6 && 0 <= i__1 ? i__1 
			    : s_rnge("r", i__1, "f_zzocced3__", (ftnlen)997)],
			     &basis[(i__2 = (k + 6) * 3 - 12) < 18 && 0 <= 
			    i__2 ? i__2 : s_rnge("basis", i__2, "f_zzocced3__"
			    , (ftnlen)997)], &semax2[(i__3 = k * 3 - 3) < 9 &&
			     0 <= i__3 ? i__3 : s_rnge("semax2", i__3, "f_zz"
			    "occed3__", (ftnlen)997)]);
		}
	    }
	}

/*        Pick a center for the first ellipsoid. */

	for (j = 1; j <= 3; ++j) {
	    centr1[(i__1 = j - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("centr1", 
		    i__1, "f_zzocced3__", (ftnlen)1008)] = scale * t_randd__(&
		    c_b55, &c_b56, &seed);
	}

/*        Pick a random viewing point.  Start by picking longitude */
/*        and latitude of a surface point on the first ellipsoid. */

	d__1 = -pi_();
	d__2 = pi_();
	lon = t_randd__(&d__1, &d__2, &seed);
	d__1 = -halfpi_();
	d__2 = halfpi_();
	lat = t_randd__(&d__1, &d__2, &seed);
	latrec_(&c_b13, &lon, &lat, srfdir);

/*        Since the origin of this ray is the center of the */
/*        ellipsoid, we don't have to check the found flag. */

	surfpt_(origin, srfdir, r__, &r__[1], &r__[2], xpt, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Pick a random altitude.  Note the we move the observer */
/*        farther away from the target than in the total occultation */
/*        cases.  For those, the minimum exponent was -1. */

	d__1 = t_randd__(&c_b13, &c_b65, &seed);
	alt = scale * pow_dd(&c_b6, &d__1);

/*        Find the local outward unit surface normal, scale it by */
/*        the altitude, and add it to XPT to obtain the view point. */

	surfnm_(r__, &r__[1], &r__[2], xpt, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vlcom_(&c_b13, xpt, &alt, normal, viewpt);

/*        Find the offset from the viewing point to the center of the */
/*        first ellipsoid. */

	vsub_(viewpt, centr1, vpoff);
	dist[0] = vnorm_(vpoff);

/*        Let UVPOFF be the unit vector pointing from the view point */
/*        to the center of the first ellipsoid. */

	vminus_(vpoff, uvpoff);
	vhatip_(uvpoff);
/*        Determine the center of the second ellipsoid.  This ellipsoid */
/*        is centered on the ray from the view point through the center */
/*        of the first ellipsoid. */

/*        The second ellipsoid must be placed far enough from the first */
/*        so that no overlap of the ellipsoids occurs. */

/*        Pick a random distance between the centers; the distance */
/*        must be at least 1.25 * the sum of the maximum radii of */
/*        the ellipsoids.  Here 1.25 is an arbitrary factor "slightly" */
/*        larger than 1. */

	radsum = maxrad[0] + maxrad[1];
	d__1 = radsum * 1.25;
	d__2 = radsum * 100.;
	ctrsep = t_randd__(&d__1, &d__2, &seed);

/*        The second center is placed at distance CTRSEP along */
/*        the ray emanating from the first center in direction UVPOFF. */

	vlcom_(&c_b13, centr1, &ctrsep, uvpoff, centr2);

/*        Now make sure the second ellipsoid is in annular occultation */
/*        by the first.  We'll adjust the size of the first */
/*        ellipsoid if necessary. */

/*        Find a upper bound on the angular radius, as seen from the */
/*        view point, of the first ellipsoid. */

	d__1 = maxrad[0] / dist[0];
	maxang[0] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the minimum angular radius of the second ellipsoid, */
/*        based on its nominal radii. */

	dist[1] = dist[0] + ctrsep;
	maxang[1] = 0.;
/*         MAXANG(2) = DASINE ( MAXRAD(2)/DIST(2), 0.D0 ) */
/*         CALL CHCKXC ( .FALSE., ' ', OK ) */
	minang[0] = 0.;
	d__1 = minrad[1] / dist[1];
	minang[1] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        If we can't guarantee the second ellipsoid is in annular */
/*        occultation by the first, based on the second ellipsoid's */
/*        minimum angular radius, shrink the first ellipsoid until this */
/*        condition is met. */

	if (minang[1] <= maxang[0]) {

/*           Find the limit on MAXRAD(1).  We pick LIMIT so that */

/*              ASIN ( LIMIT/DIST(1) ) = MINANG(2) */

	    limit = dist[0] * sin(minang[1]);

/*           Scale down the radii of the first ellipsoid.  Use the */
/*           arbitrary safety factor 0.75. */

	    d__1 = limit * .75 / maxrad[0];
	    vsclip_(&d__1, r__);
/* Computing MIN */
	    d__1 = min(r__[0],r__[1]);
	    minrad[0] = min(d__1,r__[2]);
/* Computing MAX */
	    d__1 = max(r__[0],r__[1]);
	    maxrad[0] = max(d__1,r__[2]);

/*           We must re-create the first semi-axis matrix too. */

	    for (j = 1; j <= 3; ++j) {
		vscl_(&r__[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge(
			"r", i__1, "f_zzocced3__", (ftnlen)1137)], &basis[(
			i__2 = (j + 3) * 3 - 12) < 18 && 0 <= i__2 ? i__2 : 
			s_rnge("basis", i__2, "f_zzocced3__", (ftnlen)1137)], 
			&semax1[(i__3 = j * 3 - 3) < 9 && 0 <= i__3 ? i__3 : 
			s_rnge("semax1", i__3, "f_zzocced3__", (ftnlen)1137)])
			;
	    }
	}

/*        Sanity check:  validate MAXANG(1). */

	d__1 = maxrad[0] / dist[0];
	maxang[0] = dasine_(&d__1, &c_b75);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksd_("MAXANG(1)", maxang, "<", &minang[1], &c_b75, ok, (ftnlen)9, (
		ftnlen)1);


/*        We want to find the limb of the *second* ellipsoid as seen */
/*        from the viewing point. */

/*        Express the viewing point as an offset from the center */
/*        of the first ellipsoid. */

	vsub_(viewpt, centr2, vpoff2);

/*        Transform the viewing point into the principal axis frame */
/*        of the second ellipsoid. */

	mtxv_(&basis[9], vpoff2, lview);

/*        Find the limb of the second ellipsoid. Rotate the limb back to */
/*        the original reference frame and shift the limb center to */
/*        reflect the offset of the second ellipsoid's center from the */
/*        origin. */

	edlimb_(&r__[3], &r__[4], &r__[5], lview, xlimb);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	el2cgv_(xlimb, v1, v2, v3);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	mxv_(&basis[9], v1, lmbctr);
	mxv_(&basis[9], v2, lmbmaj);
	mxv_(&basis[9], v3, lmbmin);
	vadd_(centr2, lmbctr, v1);
	vequ_(v1, lmbctr);

/*        Construct the limb of the second ellipsoid */
/*        in the original frame. */

	cgv2el_(lmbctr, lmbmaj, lmbmin, limb);

/*        The second ellipsoid should be in annular occultation */
/*        by the first.  Verify this. */

	code = zzocced_(viewpt, centr1, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial annular occultation)", &code, "=", &
		c__2, &c__0, ok, (ftnlen)42, (ftnlen)1);
	code = t_occed__(viewpt, centr1, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial annular occultation)", &code, "=", &
		c__2, &c__0, ok, (ftnlen)42, (ftnlen)1);

/*        Repeat with arguments switched. */

	code = zzocced_(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial annular occ/switched)", &code, "=", &
		c_n2, &c__0, ok, (ftnlen)43, (ftnlen)1);
	code = t_occed__(viewpt, centr2, semax2, centr1, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial annular occ/switched)", &code, "=", &
		c_n2, &c__0, ok, (ftnlen)43, (ftnlen)1);

/*        Now we're going to displace the first ellipsoid in */
/*        a direction orthogonal to the vector UVPOFF.  We'll */
/*        do this by selecting a rotation axis and rotating the */
/*        vector from the viewpoint to the center of the first */
/*        ellipsoid about this axis until we detect a change of */
/*        transit classification (from annular to partial). */
/*        We'll then make sure that both ZZOCCED and our alternative */
/*        computation performed by T_OCCED agree that we have a */
/*        transition at the same angle. */

/*        We're now going to pick a random vector orthogonal to */
/*        UVPOFF.  First pick an orthogonal basis, with UVPOFF the */
/*        first vector of the basis. */

	frame_(uvpoff, y, z__);

/*        Pick a random rotation angle; we'll rotate Y about X by */
/*        this angle to create a rotation axis. */

	d__1 = -pi_();
	d__2 = pi_();
	angle = t_randd__(&d__1, &d__2, &seed);
	vrotv_(y, uvpoff, &angle, axis);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We're now going to bracket the rotation angle needed to find */
/*        the transit state transition from annular to partial. If */
/*        the vector from the view point to the center of the *first* */
/*        ellipsoid intersects the limb of the *second* ellipsoid, we */
/*        definitely have a partial transit, so determine the */
/*        angular displacement required to make this happen. */

/*        Create the plane containing the second center and normal */
/*        to the rotation axis. */

	nvp2pl_(axis, centr2, chop);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Chop the second limb with the plane. */

	inelpl_(limb, chop, &nxpts, xpt1, xpt2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        If we don't have two points of intersection, something's */
/*        wrong. */

	chcksi_("NXPTS", &nxpts, "=", &c__2, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        Find the vector from the view point to the first */
/*        intersection point XPT1; find the angular separation */
/*        of this vector from UVPOFF. */

	vsub_(xpt1, viewpt, vpxpt1);
	xsep = vsep_(vpxpt1, uvpoff);

/*        Determine the sign of the rotation angle by which */
/*        we rotate UVPOFF to align it with VPXPT1. */

	vrotv_(uvpoff, axis, &xsep, tstvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (vsep_(tstvec, vpxpt1) > 1e-12) {
	    xsep = -xsep;
	    vrotv_(uvpoff, axis, &xsep, tstvec);
	}
	d__1 = vsep_(tstvec, vpxpt1);
	chcksd_("VSEP(TSTVEC, VPXPT1)", &d__1, "~", &c_b75, &c_b134, ok, (
		ftnlen)20, (ftnlen)1);

/*        Time for another sanity check:  rotate the vector from */
/*        the viewpoint to the center of the first ellipsoid about */
/*        AXIS by XSEP, then add this to the view point, */
/*        yielding a new center vector for the first ellipsoid. */
/*        Verify that the first ellipsoid is in partial transit. */

	vsub_(centr1, viewpt, vpctr1);
	vrotv_(vpctr1, axis, &xsep, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Sanity check:  make sure RVEC points from the view point */
/*        to XPT1. */

	d__1 = vsep_(rvec, vpxpt1);
	chcksd_("VSEP(RVEC, VPXPT1)", &d__1, "~", &c_b75, &c_b134, ok, (
		ftnlen)18, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
/*         CALL VSUB  ( NEWCTR, VIEWPT, TSTVEC ) */
/*         WRITE (*,*) 'VPXPT1 sep = ', VSEP ( TSTVEC, VPXPT1 ) */
/*         WRITE (*,*) 'VPCTR2 sep = ', VSEP ( TSTVEC, VPCTR2 ) */

/*        Check for partial transit of the first ellipsoid across */
/*        the second. */

	code = t_occed__(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE, (initial partial transit)", &code, "=", &c__1, 
		&c__0, ok, (ftnlen)39, (ftnlen)1);
	code = zzocced_(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE, (initial partial transit)", &code, "=", &c__1, 
		&c__0, ok, (ftnlen)39, (ftnlen)1);

/*        Repeat with arguments switched. */

	code = zzocced_(viewpt, centr2, semax2, newctr, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("ZZOCCED CODE (initial partial occ/switched)", &code, "=", &
		c_n1, &c__0, ok, (ftnlen)43, (ftnlen)1);
	code = t_occed__(viewpt, centr2, semax2, newctr, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("T_OCCED CODE (initial partial occ/switched)", &code, "=", &
		c_n1, &c__0, ok, (ftnlen)43, (ftnlen)1);

/*        We now know that a rotation of the vector from the */
/*        view point to the center of the second ellipsoid about */
/*        AXIS by an angle between 0 and XSEP radians should yield */
/*        a state transition.  Find the angle via binary search. */

	if (xsep > 0.) {
	    rsign = 1.;
	} else {
	    rsign = -1.;
	}
	low = 0.;
	high = abs(xsep);
	diff = high - low;
	nitr = 0;
	while(diff > 1e-14 && *ok) {
	    ++nitr;
	    chcksi_("NITR", &nitr, "<", &c__2048, &c__0, ok, (ftnlen)4, (
		    ftnlen)1);
	    midpt = (high + low) / 2.;
	    d__1 = rsign * midpt;
	    vrotv_(vpctr1, axis, &d__1, rvec);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vadd_(viewpt, rvec, newctr);
/* XXX */
	    code = zzocced_(viewpt, newctr, semax1, centr2, semax2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (code == 1) {
		high = midpt;
	    } else {

/*              The code had better be ANNLR2. */

		chcksi_("(bisection) CODE", &code, "=", &c__2, &c__0, ok, (
			ftnlen)16, (ftnlen)1);
		low = midpt;
	    }
	    diff = high - low;
	}

/*        Now that we've dropped out of the loop, verify that T_OCCED */
/*        says the transit is total at rotation angle RSIGN*LOW and */
/*        partial at rotation angle RSIGN*HIGH.  We adjust each */
/*        of these angles by ADJTOL to allow for differences in */
/*        round-off between ZZOCCED and T_OCCED. */


/*        Verify that when we switch the order of the ellipsoids, */
/*        we see a transition from total transit of ellipsoid 1 */
/*        to partial transit of ellipsoid 1. */


/*        Check for annular transit: */

	d__1 = rsign * (low - 1e-14);
	vrotv_(vpctr1, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (annular)", &code, "=", &c__2, &c__0, ok, (
		ftnlen)27, (ftnlen)1);

/*        IF ( .NOT. OK ) THEN */

/*           WRITE (*,*) '=============' */
/*           WRITE (*,*) 'XSEP = ', XSEP */
/*           WRITE (*,*) 'MINANG: ', MINANG */
/*           WRITE (*,*) 'MAXANG: ', MAXANG */
/*           WRITE (*,*) 'LIMB   = ', LIMB */
/*           WRITE (*,*) 'VIEWPT = ', VIEWPT */
/*           WRITE (*,*) 'LOW, HIGH = ', LOW, HIGH */
/*        END IF */
	code = t_occed__(viewpt, centr2, semax2, newctr, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (annular, args switched)", &code, "=", &
		c_n2, &c__0, ok, (ftnlen)42, (ftnlen)1);

/*        Check for partial transit: */

	d__1 = rsign * (high + 1e-14);
	vrotv_(vpctr1, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 0)", &code, "=", &c__1, &c__0, ok,
		 (ftnlen)29, (ftnlen)1);

/*        IF ( .NOT. OK ) THEN */

/*           WRITE (*,*) '=============' */
/*           WRITE (*,*) 'XSEP = ', XSEP */
/*           WRITE (*,*) 'MINANG: ', MINANG */
/*           WRITE (*,*) 'MAXANG: ', MAXANG */
/*           WRITE (*,*) 'LIMB   = ', LIMB */
/*           WRITE (*,*) 'VIEWPT = ', VIEWPT */
/*           WRITE (*,*) 'LOW, HIGH = ', LOW, HIGH */
/*        END IF */

	code = t_occed__(viewpt, centr2, semax2, newctr, semax1);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 0, args switched)", &code, "=", &
		c_n1, &c__0, ok, (ftnlen)44, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "\"General\" case:  search for transitions from partia"
		"l transit to no occultation, for ellipsoids of different sha"
		"pe, size and orientation.  Loop iteration = #.", (ftnlen)255, 
		(ftnlen)157);
	repmi_(title, "#", &i__, title, (ftnlen)255, (ftnlen)1, (ftnlen)255);
	tcase_(title, (ftnlen)255);

/*        The next step is to test for transitions from partial */
/*        transit to no occultation. */

/*        Let BIGSEP be a displacement angle large enough to guarantee */
/*        that no occultation will be found. */

	bigsep = pi_();
	d__1 = rsign * bigsep;
	vrotv_(vpctr1, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = zzocced_(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE", &code, "=", &c__0, &c__0, ok, (ftnlen)4, (ftnlen)1);

/*        We now know that a rotation of the vector from the */
/*        view point to the center of the first ellipsoid about */
/*        AXIS by an angle between 0 and XSEP radians should yield */
/*        a state transition.  Find the angle via binary search. */

	low = abs(xsep);
	high = bigsep;
	diff = high - low;
	nitr = 0;
	while(diff > 1e-14 && *ok) {
	    ++nitr;
	    chcksi_("NITR", &nitr, "<", &c__2048, &c__0, ok, (ftnlen)4, (
		    ftnlen)1);
	    midpt = (high + low) / 2.;
	    d__1 = rsign * midpt;
	    vrotv_(vpctr1, axis, &d__1, rvec);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vadd_(viewpt, rvec, newctr);
	    code = zzocced_(viewpt, newctr, semax1, centr2, semax2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (code == 0) {
		high = midpt;
	    } else {

/*              The code had better be PARTL2. */

		chcksi_("(bisection) CODE", &code, "=", &c__1, &c__0, ok, (
			ftnlen)16, (ftnlen)1);
		low = midpt;
	    }
	    diff = high - low;
	}

/*        Now that we've dropped out of the loop, verify that T_OCCED */
/*        says the transit is partial at rotation angle RSIGN*LOW and */
/*        "none" at rotation angle RSIGN*HIGH.  We adjust each */
/*        of these angles by CNVTOL to allow for differences in */
/*        round-off between ZZOCCED and T_OCCED. */


/*        Check for partial transit: */

	d__1 = rsign * (low - 1e-14);
	vrotv_(vpctr1, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (partial 1)", &code, "=", &c__1, &c__0, ok,
		 (ftnlen)29, (ftnlen)1);

/*        Check for no occultation: */

	d__1 = rsign * (high + 1e-14);
	vrotv_(vpctr1, axis, &d__1, rvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	vadd_(viewpt, rvec, newctr);
	code = t_occed__(viewpt, newctr, semax1, centr2, semax2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("CODE from T_OCCED (none)", &code, "=", &c__0, &c__0, ok, (
		ftnlen)24, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_zzocced3__ */

