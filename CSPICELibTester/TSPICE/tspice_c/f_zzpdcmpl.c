/* f_zzpdcmpl.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static integer c__0 = 0;

/* $Procedure F_ZZPDCMPL ( ZZPDCMPL tests ) */
/* Subroutine */ int f_zzpdcmpl__(logical *ok)
{
    doublereal plat;
    integer xrel;
    extern /* Subroutine */ int zzpdcmpl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *);
    doublereal f, p[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    , t_success__(logical *);
    doublereal re;
    extern doublereal pi_(void);
    doublereal rp;
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), georec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal alt, lat;
    integer rel;
    extern doublereal rpd_(void);
    doublereal lon;

/* $ Abstract */

/*     Exercise the private SPICELIB geometry routine ZZPDCMPL. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB */
/*     geometry routine ZZPDCMPL. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 19-JAN-2017 (NJB) */

/*        Original version 09-MAR-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*      CHARACTER*(LNSIZE)    LABEL */
/*      CHARACTER*(MSGLEN)    TITLE */
/*      INTEGER               I */
/*      INTEGER               J */

/*     Saved values */

/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZPDCMPL", (ftnlen)10);
/* *********************************************************************** */

/*     Special cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is at origin, LAT > 0.", (ftnlen)28);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = 0.;

/*     The latitude of P is considered to be zero. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is at origin, LAT < 0.", (ftnlen)28);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -pi_() / 4;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = 0.;

/*     The latitude of P is considered to be zero. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is at origin, LAT = 0.", (ftnlen)28);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = 0.;

/*     The latitude of P is considered to be zero. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 0;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is on -Z axis, LAT > 0.", (ftnlen)29);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = -1.;

/*     The latitude of P is -pi/2. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is on -Z axis, -pi/2 < LAT < 0.", (ftnlen)37);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -pi_() / 4;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = -1.;

/*     The latitude of P is -pi/2. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Point is on -Z axis, LAT = 0.", (ftnlen)29);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 0.;
    p[1] = 0.;
    p[2] = -1.;

/*     The latitude of P is -pi/2. */

    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* *********************************************************************** */

/*     LAT = 0 tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 0 deg; P(3) > 0", (ftnlen)32);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 0 deg; P(3) = 0", (ftnlen)32);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    xrel = 0;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 0 deg; P(3) < 0", (ftnlen)32);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 0 deg; P(3) > 0", (ftnlen)32);
    re = 2.;
    rp = 1.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 0 deg; P(3) = 0", (ftnlen)32);
    re = 2.;
    rp = 1.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    xrel = 0;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 0 deg; P(3) < 0", (ftnlen)32);
    re = 2.;
    rp = 1.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* *********************************************************************** */

/*     LAT = pi/2, -pi/2 tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 90 deg; P(3) > 0, P(1)=P(2)=0", (ftnlen)46);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = halfpi_();
    p[0] = 0.;
    p[1] = 0.;
    p[2] = 1.;
    xrel = 0;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 90 deg; P(3) > 0, P(1) != 0", (ftnlen)44);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = halfpi_();
    p[0] = 1.;
    p[1] = 0.;
    p[2] = 1.;
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 90 deg; P(3) > 0, P(2) != 0", (ftnlen)44);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = halfpi_();
    p[0] = 0.;
    p[1] = 1.;
    p[2] = 1.;
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -90 deg; P(3) < 0, P(1)=P(2)=0", (ftnlen)47);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = -halfpi_();
    p[0] = 0.;
    p[1] = 0.;
    p[2] = -1.;
    xrel = 0;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -90 deg; P(3) < 0, P(1) != 0", (ftnlen)45);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = -halfpi_();
    p[0] = 1.;
    p[1] = 0.;
    p[2] = -1.;
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -90 deg; P(3) < 0, P(2) != 0", (ftnlen)45);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = -halfpi_();
    p[0] = 0.;
    p[1] = 1.;
    p[2] = -1.;
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* *********************************************************************** */

/*     Basic tests for prolate spheroids */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = 70 deg", (ftnlen)40);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 70.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = 50 deg", (ftnlen)40);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = -50 deg", (ftnlen)41);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * -50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = 60+1e-13 deg", (ftnlen)46);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 60.000000000000099;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = 60-1e-13 deg", (ftnlen)46);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 59.999999999999901;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -60 deg; lat(P) = -70 deg", (ftnlen)42);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -70.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -60 deg; lat(P) = -50 deg", (ftnlen)42);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -60 deg; lat(P) = 50 deg", (ftnlen)41);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * 50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = -60 deg; lat(P) = -60-1e-13 deg", (ftnlen)48);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -60.000000000000099;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    tcase_("RP = 2*RE; LAT = 60 deg; lat(P) = -60+1e-13 deg", (ftnlen)47);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -59.999999999999901;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* *********************************************************************** */

/*     Basic tests for oblate spheroids */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = 70 deg", (ftnlen)40);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 70.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = 50 deg", (ftnlen)40);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = -50 deg", (ftnlen)41);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * -50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = 60+1e-13 deg", (ftnlen)46);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 60.000000000000099;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = 60-1e-13 deg", (ftnlen)46);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * 60.;
    plat = rpd_() * 59.999999999999901;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = -60 deg; lat(P) = -70 deg", (ftnlen)42);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -70.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = -60 deg; lat(P) = -50 deg", (ftnlen)42);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = -60 deg; lat(P) = 50 deg", (ftnlen)41);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * 50.;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = -60 deg; lat(P) = -60-1e-13 deg", (ftnlen)48);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -60.000000000000099;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = -1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);
/* --- Case: ------------------------------------------------------ */

    tcase_("RP = RE/2; LAT = 60 deg; lat(P) = -60+1e-13 deg", (ftnlen)47);
    re = 8.;
    rp = 4.;
    f = (re - rp) / re;
    lat = rpd_() * -60.;
    plat = rpd_() * -59.999999999999901;
    lon = rpd_() * 30.;
    alt = 1.;
    georec_(&lon, &plat, &alt, &re, &f, p);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xrel = 1;
    zzpdcmpl_(&re, &f, p, &lat, &rel);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("REL", &rel, "=", &xrel, &c__0, ok, (ftnlen)3, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzpdcmpl__ */

