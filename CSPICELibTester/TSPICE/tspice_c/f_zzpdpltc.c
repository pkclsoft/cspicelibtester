/* f_zzpdpltc.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* $Procedure F_ZZPDPLTC ( ZZPDPLTC tests ) */
/* Subroutine */ int f_zzpdpltc__(logical *ok)
{
    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    logical ison;
    doublereal xxpt, xypt;
    extern logical zzpdpltc_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    extern /* Subroutine */ int zzelnaxx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal f, p[3], s;
    extern /* Subroutine */ int tcase_(char *, ftnlen), topen_(char *, ftnlen)
	    ;
    logical xison;
    extern /* Subroutine */ int t_success__(logical *);
    doublereal re;
    extern doublereal pi_(void);
    doublereal rp;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen);
    doublereal lat;

/* $ Abstract */

/*     Exercise the private SPICELIB geometry routine ZZPDPLTC. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB */
/*     geometry routine ZZPDPLTC. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 18-FEB-2017 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     This value must match that in ZZPDPLTC. */


/*     Local Variables */



/*     Saved values */

/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZPDPLTC", (ftnlen)10);
/* *********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("RE is zero.", (ftnlen)11);
    re = 0.;
    f = 0.;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("RE is negative.", (ftnlen)15);
    re = -1.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("F > 1.", (ftnlen)6);
    re = 3.;
    rp = 2.;
    f = 2.;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     Oblate tests */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = PI/4; P(3) > 0", (ftnlen)33);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -PI/4; P(3) > 0", (ftnlen)34);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = FALSE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) > 0", (ftnlen)30);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = PI/4; P(3) < 0", (ftnlen)33);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = FALSE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -PI/4; P(3) < 0", (ftnlen)34);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) < 0", (ftnlen)30);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = PI/4; P(3) = 0", (ftnlen)33);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -PI/4; P(3) = 0", (ftnlen)34);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) = 0", (ftnlen)30);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) > 0, radius > XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) > 0, radius < XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The point is accepted on the basis of its Z component */
/*     having the correct sign. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) < 0, radius > XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) < 0, radius < XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = FALSE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) = 0, radius > XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = LIMIT/2; P(3) = 0, radius < XXPT.", (ftnlen)52)
	    ;
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = .0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The point is accepted because its Z component is zero. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) = 0, radius > XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) = 0, radius < XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The point is accepted because its Z component is zero. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) = 0, radius > XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0.D0; P(3) = 0, radius < XXPT.", (ftnlen)49);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) > 0, radius > XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) > 0, radius < XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = FALSE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) < 0, radius > XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = -LIMIT/2; P(3) < 0, radius < XXPT.", (ftnlen)
	    53);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -.0050000000000000001;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The point is accepted on the basis of the sign of its Z */
/*     component. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) > 0, radius > XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) > 0, radius < XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) < 0, radius > XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) < 0, radius < XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) = 0, radius > XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt * 2;
    p[1] = s * xxpt * 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case; LAT = 0; P(3) = 0, radius < XXPT.", (ftnlen)46);
    re = 4.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    zzelnaxx_(&re, &f, &lat, &xxpt, &xypt);
    s = sqrt(2.) / 2;
    p[0] = s * xxpt / 2;
    p[1] = s * xxpt / 2;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);
/* *********************************************************************** */

/*     Prolate tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = PI/4; P(3) > 0", (ftnlen)34);
    re = 2.;
    rp = 4.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = -PI/4; P(3) > 0", (ftnlen)35);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = 0; P(3) > 0", (ftnlen)31);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = PI/4; P(3) < 0", (ftnlen)34);
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = -PI/4; P(3) < 0", (ftnlen)35);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = 0; P(3) < 0", (ftnlen)31);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = PI/4; P(3) = 0", (ftnlen)34);
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = -PI/4; P(3) = 0", (ftnlen)35);
    lat = -pi_() / 4;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Prolate case; LAT = 0; P(3) = 0", (ftnlen)31);
    lat = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);
/* *********************************************************************** */

/*     Sphere tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = PI/4; P(3) > 0", (ftnlen)33);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = -PI/4; P(3) > 0", (ftnlen)34);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = 0; P(3) > 0", (ftnlen)30);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = PI/4; P(3) < 0", (ftnlen)33);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = -PI/4; P(3) < 0", (ftnlen)34);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = 0; P(3) < 0", (ftnlen)30);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = -1.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = PI/4; P(3) = 0", (ftnlen)33);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = -PI/4; P(3) = 0", (ftnlen)34);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = -pi_() / 4;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: the prolate case ALWAYS returns .TRUE. */

    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */

    tcase_("Sphere case; LAT = 0; P(3) = 0", (ftnlen)30);
    re = 2.;
    rp = 2.;
    f = (re - rp) / re;
    lat = 0.;
    p[0] = 1.;
    p[1] = 1.;
    p[2] = 0.;
    ison = zzpdpltc_(&re, &f, p, &lat);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    xison = TRUE_;
    chcksl_("ISON", &ison, &xison, ok, (ftnlen)4);

/* --- Case: ------------------------------------------------------ */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzpdpltc__ */

