/* f_zzptpl02.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b65 = 0.;
static doublereal c_b79 = 1.;
static integer c__4 = 4;
static doublereal c_b242 = 1e-10;
static integer c__8 = 8;

/* $Procedure F_ZZPTPL02 ( ZZPTPL02 tests ) */
/* Subroutine */ int f_zzptpl02__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static integer plid, nlat, nlon;
    static doublereal maxr;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzlatbox_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal c__;
    static integer i__, j, n;
    static char label[80];
    static doublereal p[3], scale, delta;
    extern /* Subroutine */ int dskgd_(integer *, integer *, doublereal *), 
	    tcase_(char *, ftnlen), dskn02_(integer *, integer *, integer *, 
	    doublereal *), dskp02_(integer *, integer *, integer *, integer *,
	     integer *, integer *);
    static integer plate[3];
    extern /* Subroutine */ int dskv02_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), dskz02_(integer *, integer *, integer *, 
	    integer *), movei_(integer *, integer *, integer *), vlcom_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), topen_(char *, ftnlen);
    static doublereal p2[3], verts[9]	/* was [3][3] */;
    extern /* Subroutine */ int t_success__(logical *), vlcom3_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chckad_(char *, doublereal *, char *,
	     doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen)
	    , chckai_(char *, integer *, char *, integer *, integer *, 
	    logical *, ftnlen, ftnlen);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *);
    static doublereal lr;
    static integer np;
    static doublereal lt;
    extern /* Subroutine */ int kclear_(void), chckxc_(logical *, char *, 
	    logical *, ftnlen);
    static doublereal defmrg;
    static integer nv;
    static doublereal lz;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static char frname[32];
    extern logical exists_(char *, ftnlen);
    static doublereal boxctr[3], dskdsc[24], margin, normal[3], xverts[9]	
	    /* was [3][3] */;
    static integer bodyid, nxtdsc[8], surfid, xplate[3];
    static doublereal xv2[9]	/* was [3][3] */;
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     dasopr_(char *, integer *, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), dskgtl_(integer 
	    *, doublereal *), pltexp_(doublereal *, doublereal *, doublereal *
	    ), dskstl_(integer *, doublereal *), dlafns_(integer *, integer *,
	     integer *, logical *), dascls_(integer *), zzptpl02_(integer *, 
	    integer *, doublereal *, doublereal *, integer *, integer *, 
	    doublereal *, logical *);

/* $ Abstract */

/*     Exercise the routine ZZPTPL02. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZPTPL02. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-AUG-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZPTPL02", (ftnlen)10);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/*     The following simple cases are meant to assist debugging. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create tessellated ellipsoid DSK.", (ftnlen)41);

/*     Make sure no kernels are loaded before we start. */

    kclear_();

/*     Create, load, and discard generic test PCK. */

    tstpck_("zzptpl02.tpc", &c_true, &c_false, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create DSK. */

/*     TARGET = 'MARS' */
    s_copy(frname, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    surfid = 1;
    nlon = 40;
    nlat = 20;
    if (exists_("zzptpl02_0.bds", (ftnlen)14)) {
	delfil_("zzptpl02_0.bds", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_elds2z__(&bodyid, &surfid, frname, &nlon, &nlat, "zzptpl02_0.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Append a segment for Phobos. Make this segment large enough */
/*     so that the buffering capability in ZZPTPL02 will be */
/*     exercised. */

/*     TARGET = 'Phobos' */
    s_copy(frname, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    bodyid = 401;
    surfid = 1;
    nlon = 200;
    nlat = 100;
    t_elds2z__(&bodyid, &surfid, frname, &nlon, &nlat, "zzptpl02_0.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars segment: map plate centroids to plates.", (ftnlen)44);

/*     This should be easy.... */

/*     Get DLA descriptor. */

    dasopr_("zzptpl02_0.bds", &handle, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Get the DSK descriptor, which will be needed a bit later. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get vertex and plate counts. */

    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    294)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)294)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the plate's centroid. */

	c__ = .33333333333333331;
	vlcom3_(&c__, xverts, &c__, &xverts[3], &c__, &xverts[6], p);

/*        Find the plate associated with P. We're expecting plate I. */

	zzptpl02_(&handle, dladsc, dskdsc, p, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Make sure the centroid was found on the correct plate. */

	s_copy(label, "centroid PLID #.", (ftnlen)80, (ftnlen)16);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned plate. */

	s_copy(label, "plate PLID #.", (ftnlen)80, (ftnlen)13);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned vertices. We expect an exact match. */

	s_copy(label, "vertices PLID #.", (ftnlen)80, (ftnlen)16);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars segment: map off-plate points near plate centroids to plate"
	    "s.", (ftnlen)66);
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    363)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)363)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the plate's centroid. */

	c__ = .33333333333333331;
	vlcom3_(&c__, xverts, &c__, &xverts[3], &c__, &xverts[6], p);

/*        Get the normal vector of plate I; add a small upward */
/*        displacement */

	dskn02_(&handle, dladsc, &i__, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	scale = 1e-10;
	vlcom_(&c_b79, p, &scale, normal, p2);

/*        Find the plate associated with P2. We're expecting plate I. */

	zzptpl02_(&handle, dladsc, dskdsc, p2, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Make sure the offst centroid was found on the correct plate. */

	s_copy(label, "(up) offset centroid PLID #.", (ftnlen)80, (ftnlen)28);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned plate. */

	s_copy(label, "(up) plate PLID #.", (ftnlen)80, (ftnlen)18);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned vertices. We expect an exact match. */

	s_copy(label, "(up) vertices PLID #.", (ftnlen)80, (ftnlen)21);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, (
		ftnlen)1);

/*        Add a small downward displacement to the centroid. */

	dskn02_(&handle, dladsc, &i__, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = -scale;
	vlcom_(&c_b79, p, &d__1, normal, p2);

/*        Find the plate associated with P2. We're expecting plate I. */

	zzptpl02_(&handle, dladsc, dskdsc, p2, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Make sure the offset centroid was found on the correct plate. */

	s_copy(label, "(down) offset centroid PLID #.", (ftnlen)80, (ftnlen)
		30);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned plate. */

	s_copy(label, "(down) plate PLID #.", (ftnlen)80, (ftnlen)20);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned vertices. We expect an exact match. */

	s_copy(label, "(down) vertices PLID #.", (ftnlen)80, (ftnlen)23);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars segment: attempt to map off-plate points near centroids to "
	    "plates, using offsets that are too large. No matches should be f"
	    "ound.", (ftnlen)133);

/*     Get default membership margin. */

    dskgtl_(&c__4, &defmrg);

/*     Get the segment's bounding radius. */

    zzlatbox_(&dskdsc[16], boxctr, &lr, &lt, &lz, &maxr);

/*     Derive the applicable margin. */

    margin = defmrg * maxr;
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    510)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)510)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the plate's centroid. */

	c__ = .33333333333333331;
	vlcom3_(&c__, xverts, &c__, &xverts[3], &c__, &xverts[6], p);

/*        Get the normal vector of plate I; add a small upward */
/*        displacement */

	dskn02_(&handle, dladsc, &i__, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	scale = margin * 2;
	vlcom_(&c_b79, p, &scale, normal, p2);

/*        Try to find the plate associated with P2. We expect no match. */

	zzptpl02_(&handle, dladsc, dskdsc, p2, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);

/*        Add a small downward displacement to the centroid. */

	dskn02_(&handle, dladsc, &i__, normal);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	d__1 = -scale;
	vlcom_(&c_b79, p, &d__1, normal, p2);

/*        Try to find the plate associated with P2. We expect no match. */

	zzptpl02_(&handle, dladsc, dskdsc, p2, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_false, ok, (ftnlen)5);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars: map plate interior points near vertices to plates.", (
	    ftnlen)56);

/*     We're still working with the same segment as last time. */

    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    586)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)586)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Shrink the plate very slightly. The vertices of the new plate */
/*        will be inside the boundary of the original. */

/*        We can't put the vertices too near the edges of the original */
/*        plates, or the expansion of other plates may cause those */
/*        plates to "capture" our vertices. */

/*        Note the sign of DELTA. */

	delta = -1e-9;
	pltexp_(xverts, &delta, xv2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the plates associated with the vertices of the scaled */
/*        plate. */

	for (j = 1; j <= 3; ++j) {
	    vequ_(&xv2[(i__2 = j * 3 - 3) < 9 && 0 <= i__2 ? i__2 : s_rnge(
		    "xv2", i__2, "f_zzptpl02__", (ftnlen)612)], p);

/*           Find the plate associated with P. We're expecting plate I. */

	    zzptpl02_(&handle, dladsc, dskdsc, p, &plid, plate, verts, &found)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*           Make sure the vertex was found on the correct plate. */

	    s_copy(label, "vertex # PLID #.", (ftnlen)80, (ftnlen)16);
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1)
		    ;

/*           Check the returned plate. */

	    s_copy(label, "vertex # plate PLID #.", (ftnlen)80, (ftnlen)22);
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)
		    1);

/*           Check the returned vertices. We expect an exact match. */

	    s_copy(label, "vertex # vertices PLID #.", (ftnlen)80, (ftnlen)25)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, 
		    (ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Mars: map plate interior points near vertices to plates. Turn of"
	    "f default plate expansion and use a very small shrinkage factor.",
	     (ftnlen)128);

/*     Shut down plate expansion. */

    dskstl_(&c__1, &c_b65);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We're still working with the same segment as last time. */

    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    683)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)683)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Shrink the plate very slightly. The vertices of the new plate */
/*        will be inside the boundary of the original. */

/*        Use a very small scale factor. This factor won't work with */
/*        default plate expansion turned on. */

/*        Note the sign of DELTA. */

	delta = -1e-12;
	pltexp_(xverts, &delta, xv2);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Find the plates associated with the vertices of the scaled */
/*        plate. */

	for (j = 1; j <= 3; ++j) {
	    vequ_(&xv2[(i__2 = j * 3 - 3) < 9 && 0 <= i__2 ? i__2 : s_rnge(
		    "xv2", i__2, "f_zzptpl02__", (ftnlen)708)], p);

/*           Find the plate associated with P. We're expecting plate I. */

	    zzptpl02_(&handle, dladsc, dskdsc, p, &plid, plate, verts, &found)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*           Make sure the vertex was found on the correct plate. */

	    s_copy(label, "vertex # PLID #.", (ftnlen)80, (ftnlen)16);
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1)
		    ;

/*           Check the returned plate. */

	    s_copy(label, "vertex # plate PLID #.", (ftnlen)80, (ftnlen)22);
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)
		    1);

/*           Check the returned vertices. We expect an exact match. */

	    s_copy(label, "vertex # vertices PLID #.", (ftnlen)80, (ftnlen)25)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, 
		    (ftnlen)1);
	}
    }

/*     Restore default plate expansion. */

    dskstl_(&c__1, &c_b242);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Phobos segment: map plate centroids to plates.", (ftnlen)46);

/*     Make sure that a segment change is not a problem. */

/*     Get DLA descriptor. */

    dlafns_(&handle, dladsc, nxtdsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    movei_(nxtdsc, &c__8, dladsc);

/*     Get the DSK descriptor, which will be needed a bit later. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get vertex and plate counts. */

    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Get Ith plate and its vertices. */

	dskp02_(&handle, dladsc, &i__, &c__1, &n, xplate);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
	    dskv02_(&handle, dladsc, &xplate[(i__2 = j - 1) < 3 && 0 <= i__2 ?
		     i__2 : s_rnge("xplate", i__2, "f_zzptpl02__", (ftnlen)
		    799)], &c__1, &n, &xverts[(i__3 = j * 3 - 3) < 9 && 0 <= 
		    i__3 ? i__3 : s_rnge("xverts", i__3, "f_zzptpl02__", (
		    ftnlen)799)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}

/*        Compute the plate's centroid. */

	c__ = .33333333333333331;
	vlcom3_(&c__, xverts, &c__, &xverts[3], &c__, &xverts[6], p);

/*        Find the plate associated with P. We're expecting plate I. */

	zzptpl02_(&handle, dladsc, dskdsc, p, &plid, plate, verts, &found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*        Make sure the centroid was found on the correct plate. */

	s_copy(label, "centroid PLID #.", (ftnlen)80, (ftnlen)16);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &plid, "=", &i__, &c__0, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned plate. */

	s_copy(label, "plate PLID #.", (ftnlen)80, (ftnlen)13);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckai_(label, plate, "=", xplate, &c__3, ok, (ftnlen)80, (ftnlen)1);

/*        Check the returned vertices. We expect an exact match. */

	s_copy(label, "vertices PLID #.", (ftnlen)80, (ftnlen)16);
	repmi_(label, "#", &i__, label, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chckad_(label, verts, "=", xverts, &c__3, &c_b65, ok, (ftnlen)80, (
		ftnlen)1);
    }
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case setup: get valid descriptors.", (ftnlen)40);

/*     Get DLA descriptor. */

    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);

/*     Get the DSK descriptor, which will be needed a bit later. */

    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note: exceptions are only for bad segment parameters, */
/*     bad DSK descriptor and file read errors. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Invalid file handle", (ftnlen)19);
    i__1 = handle + 1;
    zzptpl02_(&i__1, dladsc, dskdsc, p, &plid, plate, verts, &found);
    chckxc_(&c_true, "SPICE(INVALIDHANDLE)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate system.", (ftnlen)22);
    dskdsc[5] = -1.;
    zzptpl02_(&handle, dladsc, dskdsc, p, &plid, plate, verts, &found);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Restore coordinate system. */

    dskdsc[5] = 1.;

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);

/*     Close DSK file; open was performed using DASOPR. */

    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzptpl02_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzptpl02__ */

