/* f_zzraybox.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 1.;
static doublereal c_b5 = 2.;
static doublereal c_b6 = 3.;
static doublereal c_b7 = 30.;
static doublereal c_b8 = 20.;
static doublereal c_b9 = 10.;
static doublereal c_b10 = 1e3;
static integer c__3 = 3;
static logical c_true = TRUE_;
static doublereal c_b23 = 0.;
static doublereal c_b28 = -1.;
static doublereal c_b60 = 15.;
static doublereal c_b62 = 5.;
static logical c_false = FALSE_;
static doublereal c_b79 = .5;

/* $Procedure F_ZZRAYBOX ( ray-box intercept tests ) */
/* Subroutine */ int f_zzraybox__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5, i__6, i__7;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static doublereal targ[3];
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal udir[3];
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int zzraybox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *);
    static integer i__, j;
    static char label[50];
    static doublereal s, delta[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), vlcom_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static integer h1, h2, nstep, v1, v2;
    extern /* Subroutine */ int t_success__(logical *);
    static integer hcidx1, hcidx2;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal bigrad;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal medrad;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksl_(char *, logical *, logical *, logical *, ftnlen);
    static doublereal center[3], smlrad, raydir[3], boxori[3], extent[3], 
	    vertex[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    static doublereal tol, xpt[3];
    static logical out1, out2;

/* $ Abstract */

/*     Exercise the ray-box intercept routine ZZRAYBOX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine test the DSKLIB ray-box intercept routine */
/*     ZZRAYBOX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 05-JUN-2014 (NJB) */

/* -& */

/*     SPICELIB functions */

/*      DOUBLE PRECISION      VDIST */

/*     Local Parameters */

/*      DOUBLE PRECISION      TIGHT */
/*      PARAMETER           ( TIGHT  = 1.D-12 ) */

/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZRAYBOX", (ftnlen)10);
/* ********************************************************************** */

/*     Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray direction is zero vector.", (ftnlen)29);
    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vpack_(&c_b10, &c_b10, &c_b10, vertex);
    cleard_(&c__3, raydir);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive extents", (ftnlen)20);
    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b10, &c_b10, &c_b10, vertex);
    vminus_(vertex, raydir);
    vpack_(&c_b23, &c_b8, &c_b9, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    vpack_(&c_b28, &c_b8, &c_b9, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    vpack_(&c_b9, &c_b23, &c_b9, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    vpack_(&c_b9, &c_b28, &c_b9, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    vpack_(&c_b9, &c_b8, &c_b23, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    vpack_(&c_b9, &c_b8, &c_b28, extent);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Vertex is inside box.", (ftnlen)21);
    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vpack_(&c_b60, &c_b9, &c_b62, vertex);
    vequ_(vertex, raydir);
    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    if (found) {
	chckad_("XPT", xpt, "=", vertex, &c__3, &c_b23, ok, (ftnlen)3, (
		ftnlen)1);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple non-intersection case. Vertices are outside the bounding "
	    "sphere.", (ftnlen)71);

/*     The ray points directly away from the box in each case. */

    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vlcom_(&c_b4, boxori, &c_b79, extent, center);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b60, &c_b9, &c_b62, vertex);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*           S is 1 for J = 1; -1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);
	    vequ_(center, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_zzraybox__", (ftnlen)298)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)298)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)298)] * 3;
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_zzraybox__", (ftnlen)301)] = s;
	    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND # #", (ftnlen)50, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (ftnlen)50)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)50);
	    chcksl_(label, &found, &c_false, ok, (ftnlen)50);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple non-intersection case. Vertices are inside the bounding s"
	    "phere.", (ftnlen)70);

/*     The ray points directly away from the box in each case. */

    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vlcom_(&c_b4, boxori, &c_b79, extent, center);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vpack_(&c_b60, &c_b9, &c_b62, vertex);
    vequ_(vertex, raydir);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*           S is 1 for J = 1; -1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);
	    vequ_(center, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_zzraybox__", (ftnlen)343)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)343)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)343)] * .500001;
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_zzraybox__", (ftnlen)346)] = s;
	    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND # #", (ftnlen)50, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (ftnlen)50)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)50);
	    chcksl_(label, &found, &c_false, ok, (ftnlen)50);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple intersection case", (ftnlen)24);

/*     The ray points directly toward from the box in each case. */

    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vlcom_(&c_b4, boxori, &c_b79, extent, center);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Select a vertex outside the bounding sphere. */

	    vequ_(center, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_zzraybox__", (ftnlen)389)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)389)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)389)] * 3;
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_zzraybox__", (ftnlen)392)] = -s;
	    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND # #", (ftnlen)50, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (ftnlen)50)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)50);
	    chcksl_(label, &found, &c_true, ok, (ftnlen)50);
	    if (found) {
		vequ_(center, xxpt);
		xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt",
			 i__1, "f_zzraybox__", (ftnlen)406)] = center[(i__2 = 
			i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", 
			i__2, "f_zzraybox__", (ftnlen)406)] + s * extent[(
			i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"extent", i__3, "f_zzraybox__", (ftnlen)406)] / 2;
		s_copy(label, "XPT # #", (ftnlen)50, (ftnlen)7);
		repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (
			ftnlen)50);
		repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)
			50);
		tol = 1e-14;
		chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)50, 
			(ftnlen)3);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Simple intersection case; vertex is inside the bounding sphere.", 
	    (ftnlen)63);

/*     The ray points directly toward from the box in each case. */

    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vlcom_(&c_b4, boxori, &c_b79, extent, center);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Select a vertex outside the bounding sphere. */

	    vequ_(center, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_zzraybox__", (ftnlen)452)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)452)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)452)] * .50001;
	    cleard_(&c__3, raydir);
	    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("raydir",
		     i__1, "f_zzraybox__", (ftnlen)455)] = -s;
	    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    s_copy(label, "FOUND # #", (ftnlen)50, (ftnlen)9);
	    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (ftnlen)50)
		    ;
	    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)50);
	    chcksl_(label, &found, &c_true, ok, (ftnlen)50);
	    if (found) {
		vequ_(center, xxpt);
		xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt",
			 i__1, "f_zzraybox__", (ftnlen)469)] = center[(i__2 = 
			i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", 
			i__2, "f_zzraybox__", (ftnlen)469)] + s * extent[(
			i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"extent", i__3, "f_zzraybox__", (ftnlen)469)] / 2;
		s_copy(label, "XPT # #", (ftnlen)50, (ftnlen)7);
		repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (
			ftnlen)50);
		repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (ftnlen)
			50);
		tol = 1e-14;
		chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)50, 
			(ftnlen)3);
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */


/*     A series of detailed intersection tests follows. Vertices */
/*     will be generated for a set of directions spanning the */
/*     unit sphere centered at the box center. All rays are */
/*     pointed at specific points on the box's surface. The */
/*     vertices are outside the bounding sphere but not at */
/*     a very great distance from it. */

/*     MEDRAD is the vertex distance from the center. */

    medrad = 100.;
    vpack_(&c_b4, &c_b5, &c_b6, boxori);
    vpack_(&c_b7, &c_b8, &c_b9, extent);
    vlcom_(&c_b4, boxori, &c_b79, extent, center);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Compute steps separating target points. */

    nstep = 20;
    for (i__ = 1; i__ <= 3; ++i__) {
	delta[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("delta", i__1,
		 "f_zzraybox__", (ftnlen)515)] = extent[(i__2 = i__ - 1) < 3 
		&& 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_zzraybox__", (
		ftnlen)515)] / nstep;
    }

/*     Loop over all coordinates and both faces for each */
/*     coordinate. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the indices of the "horizontal" coordinates; */
/*        these are the coordinate on the axes orthogonal */
/*        to the Ith axis. */

	hcidx1 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("next"
		, i__1, "f_zzraybox__", (ftnlen)529)];
	hcidx2 = next[(i__1 = hcidx1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzraybox__", (ftnlen)530)];
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Loop over the horizontal coordinates; generate */
/*           target points. */

	    xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt", 
		    i__1, "f_zzraybox__", (ftnlen)541)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)541)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)541)] / 2;
	    i__1 = nstep;
	    for (h1 = 0; h1 <= i__1; ++h1) {
		xxpt[(i__2 = hcidx1 - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"xxpt", i__2, "f_zzraybox__", (ftnlen)545)] = center[(
			i__3 = hcidx1 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"center", i__3, "f_zzraybox__", (ftnlen)545)] - 
			extent[(i__4 = hcidx1 - 1) < 3 && 0 <= i__4 ? i__4 : 
			s_rnge("extent", i__4, "f_zzraybox__", (ftnlen)545)] /
			 2 + delta[(i__5 = hcidx1 - 1) < 3 && 0 <= i__5 ? 
			i__5 : s_rnge("delta", i__5, "f_zzraybox__", (ftnlen)
			545)] * h1;
		i__2 = nstep;
		for (h2 = 0; h2 <= i__2; ++h2) {
		    xxpt[(i__3 = hcidx2 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			    "xxpt", i__3, "f_zzraybox__", (ftnlen)551)] = 
			    center[(i__4 = hcidx2 - 1) < 3 && 0 <= i__4 ? 
			    i__4 : s_rnge("center", i__4, "f_zzraybox__", (
			    ftnlen)551)] - extent[(i__5 = hcidx2 - 1) < 3 && 
			    0 <= i__5 ? i__5 : s_rnge("extent", i__5, "f_zzr"
			    "aybox__", (ftnlen)551)] / 2 + delta[(i__6 = 
			    hcidx2 - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge(
			    "delta", i__6, "f_zzraybox__", (ftnlen)551)] * h2;

/* --- Case: ------------------------------------------------------ */

		    s_copy(title, "Medium range intercept: I =  #; J = #; H1"
			    " =  #; H2 = #", (ftnlen)320, (ftnlen)54);
		    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h1, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h2, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)320);

/*                 Generate the ray's vertex for this case. */
/*                 The vertex lies along a ray emanating from */
/*                 the box's center. The distance of the vertex */
/*                 from the center is MEDRAD. */

		    vsub_(center, xxpt, raydir);
		    vhat_(raydir, udir);
		    d__1 = -medrad;
		    vlcom_(&c_b4, center, &d__1, udir, vertex);
		    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_copy(label, "FOUND # # # #", (ftnlen)50, (ftnlen)13);
		    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_(label, &found, &c_true, ok, (ftnlen)50);
		    if (found) {
			s_copy(label, "XPT # #", (ftnlen)50, (ftnlen)7);
			repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1,
				 (ftnlen)50);
			repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
				ftnlen)50);
			repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			tol = 1e-14;
			chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)50, (ftnlen)3);
		    }
		}
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

/*     Repeat the previous tests but use a very large vertex */
/*     distance. We must reduce the relative error tolerance */
/*     to a value suitable for the large expected errors in the */
/*     ray-bounding sphere intercept. */

    bigrad = 1e14;
    tol = bigrad * 1e-14 / 10;

/*     Compute steps separating target points. */

    nstep = 20;
    for (i__ = 1; i__ <= 3; ++i__) {
	delta[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("delta", i__1,
		 "f_zzraybox__", (ftnlen)636)] = extent[(i__2 = i__ - 1) < 3 
		&& 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_zzraybox__", (
		ftnlen)636)] / nstep;
    }

/*     Loop over all coordinates and both faces for each */
/*     coordinate. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the indices of the "horizontal" coordinates; */
/*        these are the coordinate on the axes orthogonal */
/*        to the Ith axis. */

	hcidx1 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("next"
		, i__1, "f_zzraybox__", (ftnlen)650)];
	hcidx2 = next[(i__1 = hcidx1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzraybox__", (ftnlen)651)];
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Loop over the horizontal coordinates; generate */
/*           target points. */

	    xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt", 
		    i__1, "f_zzraybox__", (ftnlen)663)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)663)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)663)] / 2;
	    i__1 = nstep;
	    for (h1 = 0; h1 <= i__1; ++h1) {
		xxpt[(i__2 = hcidx1 - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"xxpt", i__2, "f_zzraybox__", (ftnlen)667)] = center[(
			i__3 = hcidx1 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"center", i__3, "f_zzraybox__", (ftnlen)667)] - 
			extent[(i__4 = hcidx1 - 1) < 3 && 0 <= i__4 ? i__4 : 
			s_rnge("extent", i__4, "f_zzraybox__", (ftnlen)667)] /
			 2 + delta[(i__5 = hcidx1 - 1) < 3 && 0 <= i__5 ? 
			i__5 : s_rnge("delta", i__5, "f_zzraybox__", (ftnlen)
			667)] * h1;
		i__2 = nstep;
		for (h2 = 0; h2 <= i__2; ++h2) {
		    xxpt[(i__3 = hcidx2 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			    "xxpt", i__3, "f_zzraybox__", (ftnlen)673)] = 
			    center[(i__4 = hcidx2 - 1) < 3 && 0 <= i__4 ? 
			    i__4 : s_rnge("center", i__4, "f_zzraybox__", (
			    ftnlen)673)] - extent[(i__5 = hcidx2 - 1) < 3 && 
			    0 <= i__5 ? i__5 : s_rnge("extent", i__5, "f_zzr"
			    "aybox__", (ftnlen)673)] / 2 + delta[(i__6 = 
			    hcidx2 - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge(
			    "delta", i__6, "f_zzraybox__", (ftnlen)673)] * h2;

/* --- Case: ------------------------------------------------------ */

		    s_copy(title, "Long range intercept: I =  #; J = #; H1 ="
			    "  #; H2 = #", (ftnlen)320, (ftnlen)52);
		    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h1, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h2, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)320);

/*                 Generate the ray's vertex for this case. */
/*                 The vertex lies along a ray emanating from */
/*                 the box's center. The distance of the vertex */
/*                 from the center is BIGRAD. */

		    vsub_(center, xxpt, raydir);
		    vhat_(raydir, udir);
		    d__1 = -bigrad;
		    vlcom_(&c_b4, center, &d__1, udir, vertex);
		    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_copy(label, "FOUND # # # #", (ftnlen)50, (ftnlen)13);
		    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_(label, &found, &c_true, ok, (ftnlen)50);
		    if (found) {
			s_copy(label, "XPT # #", (ftnlen)50, (ftnlen)7);
			repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1,
				 (ftnlen)50);
			repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
				ftnlen)50);
			repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)50, (ftnlen)3);
		    }
		}
	    }
	}
    }

/* --- Case: ------------------------------------------------------ */

/*     Repeat the previous tests but use vertices slightly outside */
/*     the box. */

    smlrad = .001;
    tol = 1e-14;

/*     Compute steps separating target points. */

    nstep = 20;
    for (i__ = 1; i__ <= 3; ++i__) {
	delta[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("delta", i__1,
		 "f_zzraybox__", (ftnlen)752)] = extent[(i__2 = i__ - 1) < 3 
		&& 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_zzraybox__", (
		ftnlen)752)] / nstep;
    }

/*     Loop over all coordinates and both faces for each */
/*     coordinate. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the indices of the "horizontal" coordinates; */
/*        these are the coordinate on the axes orthogonal */
/*        to the Ith axis. */

	hcidx1 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("next"
		, i__1, "f_zzraybox__", (ftnlen)766)];
	hcidx2 = next[(i__1 = hcidx1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzraybox__", (ftnlen)767)];
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Loop over the horizontal coordinates; generate */
/*           target points. */

	    xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt", 
		    i__1, "f_zzraybox__", (ftnlen)779)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)779)] + s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)779)] / 2;
	    i__1 = nstep;
	    for (h1 = 0; h1 <= i__1; ++h1) {
		xxpt[(i__2 = hcidx1 - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"xxpt", i__2, "f_zzraybox__", (ftnlen)783)] = center[(
			i__3 = hcidx1 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"center", i__3, "f_zzraybox__", (ftnlen)783)] - 
			extent[(i__4 = hcidx1 - 1) < 3 && 0 <= i__4 ? i__4 : 
			s_rnge("extent", i__4, "f_zzraybox__", (ftnlen)783)] /
			 2 + delta[(i__5 = hcidx1 - 1) < 3 && 0 <= i__5 ? 
			i__5 : s_rnge("delta", i__5, "f_zzraybox__", (ftnlen)
			783)] * h1;
		i__2 = nstep;
		for (h2 = 0; h2 <= i__2; ++h2) {
		    xxpt[(i__3 = hcidx2 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			    "xxpt", i__3, "f_zzraybox__", (ftnlen)789)] = 
			    center[(i__4 = hcidx2 - 1) < 3 && 0 <= i__4 ? 
			    i__4 : s_rnge("center", i__4, "f_zzraybox__", (
			    ftnlen)789)] - extent[(i__5 = hcidx2 - 1) < 3 && 
			    0 <= i__5 ? i__5 : s_rnge("extent", i__5, "f_zzr"
			    "aybox__", (ftnlen)789)] / 2 + delta[(i__6 = 
			    hcidx2 - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge(
			    "delta", i__6, "f_zzraybox__", (ftnlen)789)] * h2;

/* --- Case: ------------------------------------------------------ */

		    s_copy(title, "short range intercept: I =  #; J = #; H1 "
			    "=  #; H2 = #", (ftnlen)320, (ftnlen)53);
		    repmi_(title, "#", &i__, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &j, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h1, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    repmi_(title, "#", &h2, title, (ftnlen)320, (ftnlen)1, (
			    ftnlen)320);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    tcase_(title, (ftnlen)320);

/*                 Generate the ray's vertex for this case. */
/*                 The vertex lies along a ray emanating from */
/*                 the box's center. The distance of the vertex */
/*                 from the expected intercept (NOT the center) */
/*                 is SMLRAD. */

		    vsub_(center, xxpt, raydir);
		    vhat_(raydir, udir);
		    d__1 = -smlrad;
		    vlcom_(&c_b4, xxpt, &d__1, udir, vertex);
		    zzraybox_(vertex, raydir, boxori, extent, xpt, &found);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    s_copy(label, "FOUND # # # #", (ftnlen)50, (ftnlen)13);
		    repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, (
			    ftnlen)50);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);
		    chcksl_(label, &found, &c_true, ok, (ftnlen)50);
		    if (found) {
			s_copy(label, "XPT # #", (ftnlen)50, (ftnlen)7);
			repmi_(label, "#", &i__, label, (ftnlen)50, (ftnlen)1,
				 (ftnlen)50);
			repmi_(label, "#", &j, label, (ftnlen)50, (ftnlen)1, (
				ftnlen)50);
			repmi_(label, "#", &h1, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			repmi_(label, "#", &h2, label, (ftnlen)50, (ftnlen)1, 
				(ftnlen)50);
			chckxc_(&c_false, " ", ok, (ftnlen)1);
			chckad_(label, xpt, "~~/", xxpt, &c__3, &tol, ok, (
				ftnlen)50, (ftnlen)3);
		    }
		}
	    }
	}
    }
/* ********************************************************************** */

/*     Normal non-intersection cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     A series of detailed non-intersection tests follows. */


/*     Compute steps separating target points. */

    nstep = 8;
    for (i__ = 1; i__ <= 3; ++i__) {

/*        Note that the increments are based on twice the */
/*        box extents. */

	delta[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("delta", i__1,
		 "f_zzraybox__", (ftnlen)878)] = extent[(i__2 = i__ - 1) < 3 
		&& 0 <= i__2 ? i__2 : s_rnge("extent", i__2, "f_zzraybox__", (
		ftnlen)878)] * 2 / nstep;
    }

/*     Generate target points that the rays will pass through. */

/*     Loop over all coordinates and both faces for each */
/*     coordinate. */

    for (i__ = 1; i__ <= 3; ++i__) {

/*        Set the indices of the "horizontal" coordinates; */
/*        these are the coordinate on the axes orthogonal */
/*        to the Ith axis. */

	hcidx1 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("next"
		, i__1, "f_zzraybox__", (ftnlen)895)];
	hcidx2 = next[(i__1 = hcidx1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzraybox__", (ftnlen)896)];
	for (j = 1; j <= 2; ++j) {

/*           S is -1 for J = 1; 1 for J = 2. */

	    s = (doublereal) ((j << 1) - 3);

/*           Loop over the horizontal coordinates; generate */
/*           target points. The target plane is on the far side */
/*           of the box, relative to the Ith coordinate of the */
/*           vertex. */

	    targ[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("targ", 
		    i__1, "f_zzraybox__", (ftnlen)910)] = center[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("center", i__2, 
		    "f_zzraybox__", (ftnlen)910)] - s * extent[(i__3 = i__ - 
		    1) < 3 && 0 <= i__3 ? i__3 : s_rnge("extent", i__3, "f_z"
		    "zraybox__", (ftnlen)910)] / 2;
	    i__1 = nstep;
	    for (h1 = 0; h1 <= i__1; ++h1) {
		targ[(i__2 = hcidx1 - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"targ", i__2, "f_zzraybox__", (ftnlen)914)] = center[(
			i__3 = hcidx1 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"center", i__3, "f_zzraybox__", (ftnlen)914)] - 
			extent[(i__4 = hcidx1 - 1) < 3 && 0 <= i__4 ? i__4 : 
			s_rnge("extent", i__4, "f_zzraybox__", (ftnlen)914)] 
			+ delta[(i__5 = hcidx1 - 1) < 3 && 0 <= i__5 ? i__5 : 
			s_rnge("delta", i__5, "f_zzraybox__", (ftnlen)914)] * 
			h1;
		i__2 = nstep;
		for (h2 = 0; h2 <= i__2; ++h2) {
		    targ[(i__3 = hcidx2 - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			    "targ", i__3, "f_zzraybox__", (ftnlen)920)] = 
			    center[(i__4 = hcidx2 - 1) < 3 && 0 <= i__4 ? 
			    i__4 : s_rnge("center", i__4, "f_zzraybox__", (
			    ftnlen)920)] - extent[(i__5 = hcidx2 - 1) < 3 && 
			    0 <= i__5 ? i__5 : s_rnge("extent", i__5, "f_zzr"
			    "aybox__", (ftnlen)920)] + delta[(i__6 = hcidx2 - 
			    1) < 3 && 0 <= i__6 ? i__6 : s_rnge("delta", i__6,
			     "f_zzraybox__", (ftnlen)920)] * h2;

/*                 Use only those target points that lie outside */
/*                 of the horizontal bounds of the box. */

		    out1 = (d__1 = targ[(i__3 = hcidx1 - 1) < 3 && 0 <= i__3 ?
			     i__3 : s_rnge("targ", i__3, "f_zzraybox__", (
			    ftnlen)928)] - center[(i__4 = hcidx1 - 1) < 3 && 
			    0 <= i__4 ? i__4 : s_rnge("center", i__4, "f_zzr"
			    "aybox__", (ftnlen)928)], abs(d__1)) > extent[(
			    i__5 = hcidx1 - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("extent", i__5, "f_zzraybox__", (ftnlen)
			    928)] / 2;
		    out2 = (d__1 = targ[(i__3 = hcidx2 - 1) < 3 && 0 <= i__3 ?
			     i__3 : s_rnge("targ", i__3, "f_zzraybox__", (
			    ftnlen)931)] - center[(i__4 = hcidx2 - 1) < 3 && 
			    0 <= i__4 ? i__4 : s_rnge("center", i__4, "f_zzr"
			    "aybox__", (ftnlen)931)], abs(d__1)) > extent[(
			    i__5 = hcidx2 - 1) < 3 && 0 <= i__5 ? i__5 : 
			    s_rnge("extent", i__5, "f_zzraybox__", (ftnlen)
			    931)] / 2;
		    if (out1 || out2) {

/*                    Generate vertices. We'll place the vertices */
/*                    far enough from the box that no "outside" */
/*                    target point is obscured by the box. */

			cleard_(&c__3, vertex);
			vertex[(i__3 = i__ - 1) < 3 && 0 <= i__3 ? i__3 : 
				s_rnge("vertex", i__3, "f_zzraybox__", (
				ftnlen)943)] = s * 1e3;
			i__3 = nstep;
			for (v1 = 0; v1 <= i__3; ++v1) {
			    vertex[(i__4 = hcidx1 - 1) < 3 && 0 <= i__4 ? 
				    i__4 : s_rnge("vertex", i__4, "f_zzraybo"
				    "x__", (ftnlen)947)] = center[(i__5 = 
				    hcidx1 - 1) < 3 && 0 <= i__5 ? i__5 : 
				    s_rnge("center", i__5, "f_zzraybox__", (
				    ftnlen)947)] + v1 * delta[(i__6 = hcidx1 
				    - 1) < 3 && 0 <= i__6 ? i__6 : s_rnge(
				    "delta", i__6, "f_zzraybox__", (ftnlen)
				    947)];
			    i__4 = nstep;
			    for (v2 = 0; v2 <= i__4; ++v2) {
				vertex[(i__5 = hcidx2 - 1) < 3 && 0 <= i__5 ? 
					i__5 : s_rnge("vertex", i__5, "f_zzr"
					"aybox__", (ftnlen)952)] = center[(
					i__6 = hcidx2 - 1) < 3 && 0 <= i__6 ? 
					i__6 : s_rnge("center", i__6, "f_zzr"
					"aybox__", (ftnlen)952)] + v2 * delta[(
					i__7 = hcidx2 - 1) < 3 && 0 <= i__7 ? 
					i__7 : s_rnge("delta", i__7, "f_zzra"
					"ybox__", (ftnlen)952)];

/* --- Case: ------------------------------------------------------ */

				s_copy(title, "non-intercept: I =  #; J = #;"
					" H1 =  #; H2 = #; V1 = #; V2 = #", (
					ftnlen)320, (ftnlen)61);
				repmi_(title, "#", &i__, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				repmi_(title, "#", &j, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				repmi_(title, "#", &h1, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				repmi_(title, "#", &h2, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				repmi_(title, "#", &v1, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				repmi_(title, "#", &v2, title, (ftnlen)320, (
					ftnlen)1, (ftnlen)320);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				tcase_(title, (ftnlen)320);

/*                          The ray points from the vertex toward */
/*                          the target on the far boundary plane. */

				vsub_(targ, vertex, raydir);
				zzraybox_(vertex, raydir, boxori, extent, xpt,
					 &found);
				chckxc_(&c_false, " ", ok, (ftnlen)1);
				s_copy(label, "FOUND # # # # # #", (ftnlen)50,
					 (ftnlen)17);
				repmi_(label, "#", &i__, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				repmi_(label, "#", &j, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				repmi_(label, "#", &h1, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				repmi_(label, "#", &h2, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				repmi_(label, "#", &v1, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				repmi_(label, "#", &v2, label, (ftnlen)50, (
					ftnlen)1, (ftnlen)50);
				chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                          No intercept should be found. */

				chcksl_(label, &found, &c_false, ok, (ftnlen)
					50);
			    }
			}
		    }
		}
	    }
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzraybox__ */

