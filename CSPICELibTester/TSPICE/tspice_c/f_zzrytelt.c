/* f_zzrytelt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__24 = 24;
static integer c__6 = 6;
static doublereal c_b6 = 0.;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b22 = -1.;
static doublereal c_b55 = 1e6;
static integer c__10 = 10;
static doublereal c_b101 = 1.;
static logical c_true = TRUE_;

/* $Procedure F_ZZRYTELT ( ZZRYTELT tests ) */
/* Subroutine */ int f_zzrytelt__(logical *ok)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Local variables */
    static doublereal amin, bmin, amax, bmax, xxpt[3];
    extern /* Subroutine */ int zzellbds_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal a, b, f, h__, l;
    extern /* Subroutine */ int zzrytelt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *);
    static doublereal w, delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), moved_(doublereal *, 
	    integer *, doublereal *);
    static logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    static integer nxpts;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal re;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen), georec_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chcksl_(char *, logical *, logical *,
	     logical *, ftnlen), latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal dskdsc[24], margin, bounds[6]	/* was [2][3] */, 
	    corpar[10], raydir[3], vertex[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *), surfpt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    static integer xnxpts;
    static doublereal alt, lat, lon, tol, xpt[3];

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZRYTELT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZRYTELT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-JUL-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZRYTELT", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */
/* *********************************************************************** */


/*     Rectangular coordinates */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Rectangular coordinate hit case. Zero MARGIN.", (ftnlen)45);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    l = 40.;
    w = 20.;
    h__ = 10.;
    bounds[0] = -l / 2;
    bounds[1] = l / 2;
    bounds[2] = -w / 2;
    bounds[3] = w / 2;
    bounds[4] = -h__ / 2;
    bounds[5] = h__ / 2;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 3.;
    d__1 = h__ * 2;
    vpack_(&c_b6, &c_b6, &d__1, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. */

    d__1 = h__ / 2;
    vpack_(&c_b6, &c_b6, &d__1, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Rectangular coordinate miss case. Zero MARGIN.", (ftnlen)46);

/*     Use the descriptor from the previous case. */


/*     Shift the vertex so the ray just misses the volume element on */
/*     the +X side. */

    delta = 1e-12;
    d__1 = l / 2 + delta;
    d__2 = h__ * 2;
    vpack_(&d__1, &c_b6, &d__2, vertex);
    vpack_(&c_b6, &c_b6, &c_b22, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect not to find an intercept. */

    xnxpts = 0;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Rectangular coordinate hit case. Positive MARGIN.", (ftnlen)49);

/*     Use the descriptor from the previous case. */


/*     Shift the vertex so the ray just misses the volume element on */
/*     the +X side, but hits the extended element. */

    delta = 1e-12;
    d__1 = l / 2 + delta;
    d__2 = h__ * 2;
    vpack_(&d__1, &c_b6, &d__2, vertex);
    vpack_(&c_b6, &c_b6, &c_b22, raydir);
    margin = delta * 2;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. */

    d__1 = h__ / 2 + h__ * margin;
    vpack_(vertex, &c_b6, &d__1, xxpt);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Rectangular coordinate miss case. Positive MARGIN.", (ftnlen)50);

/*     Use the descriptor from the previous case. */


/*     Shift the vertex so the ray just misses the volume element on */
/*     the +X side, but hits the extended element. */

    delta = 1e-12;
    d__1 = l / 2 + delta * l;
    d__2 = h__ * 2;
    vpack_(&d__1, &c_b6, &d__2, vertex);
    vpack_(&c_b6, &c_b6, &c_b22, raydir);
    margin = delta / 2;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We don't expect an intercept. */

    xnxpts = 0;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
/* *********************************************************************** */


/*     Latitudinal coordinates */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Latitudinal coordinate hit case. Zero MARGIN.", (ftnlen)45);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = 1e3;
    bounds[5] = 2e3;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 1.;
    d__1 = -pi_() / 4;
    d__2 = pi_() / 4;
    latrec_(&c_b55, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. */

    d__1 = -pi_() / 4;
    d__2 = pi_() / 4;
    latrec_(&bounds[5], &d__1, &d__2, xxpt);
    tol = 1e-12;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Latitudinal coordinate miss case. Zero MARGIN.", (ftnlen)46);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = 1e3;
    bounds[5] = 2e3;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 1.;
    d__1 = pi_() / 4;
    latrec_(&c_b55, &c_b6, &d__1, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We don't expect an intercept. */

    xnxpts = 0;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Latitudinal coordinate hit case. Positive MARGIN.", (ftnlen)49);
    margin = 1e-12;

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = 1e3;
    bounds[5] = 2e3;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 1.;
    delta = margin / 2;
    d__1 = -pi_() / 4;
    d__2 = pi_() / 4 + delta;
    latrec_(&c_b55, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. */

    d__1 = -pi_() / 4;
    d__2 = pi_() / 4;
    latrec_(&bounds[5], &d__1, &d__2, xxpt);
    tol = 1e-12;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Latitudinal coordinate miss case. Positive MARGIN.", (ftnlen)50);
    margin = 1e-12;

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = 1e3;
    bounds[5] = 2e3;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 1.;
    delta = margin * 2;
    d__1 = -pi_() / 4;
    d__2 = bounds[3] + delta;
    latrec_(&c_b55, &d__1, &d__2, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We don't expect an intercept. */

    xnxpts = 0;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);
/* *********************************************************************** */


/*     Planetodetic coordinates */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Planetodetic coordinate hit case. Zero MARGIN.", (ftnlen)46);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    cleard_(&c__10, corpar);
    re = 2e3;
    f = .2;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = -10.;
    bounds[5] = 10.;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 4.;
    dskdsc[6] = re;
    dskdsc[7] = f;
    lon = -pi_() / 4;
    lat = pi_() / 4;
    alt = 1e4;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = lon + pi_();
    d__2 = -lat;
    latrec_(&c_b101, &d__1, &d__2, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. We must first get */
/*     the radii of the outer bounding ellipsoid. */

    a = re;
    b = re * (1. - f);
    zzellbds_(&a, &b, &bounds[5], &bounds[4], &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfpt_(vertex, raydir, &amax, &amax, &bmax, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT FOUND", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-12;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Planetodetic coordinate miss case. Zero MARGIN.", (ftnlen)47);
    lon = -pi_() / 4;
    lat = 0.;
    alt = 1e6;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = lon + pi_();
    d__2 = -lat;
    latrec_(&c_b101, &d__1, &d__2, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We don't expect an intercept. */

    xnxpts = 0;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Planetodetic coordinate hit case. Positive MARGIN.", (ftnlen)50);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    cleard_(&c__10, corpar);
    re = 2e3;
    f = .2;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = -10.;
    bounds[5] = 10.;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 4.;
    dskdsc[6] = re;
    dskdsc[7] = f;
    margin = 1e-12;
    delta = margin / 2;
    lon = -pi_() / 4;
    lat = pi_() / 3 + delta;
    alt = 1e6;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = lon + pi_();
    d__2 = -lat;
    latrec_(&c_b101, &d__1, &d__2, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We expect an intercept. */

    xnxpts = 1;
    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Create the expected intercept point. We must first get */
/*     the radii of the outer bounding ellipsoid. */

    a = re;
    b = re * (1. - f);
    zzellbds_(&a, &b, &bounds[5], &bounds[4], &amax, &bmax, &amin, &bmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    surfpt_(vertex, raydir, &amax, &amax, &bmax, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT FOUND", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-12;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Planetodetic coordinate miss case. Positive MARGIN.", (ftnlen)51);

/*     Set the coordinate system and bounds in the DSK descriptor. */

    cleard_(&c__24, dskdsc);
    cleard_(&c__10, corpar);
    re = 2e3;
    f = .2;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = -pi_() / 3;
    bounds[1] = -pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 3;
    bounds[4] = -10.;
    bounds[5] = 10.;
    moved_(bounds, &c__6, &dskdsc[16]);
    dskdsc[5] = 4.;
    dskdsc[6] = re;
    dskdsc[7] = f;
    margin = 1e-12;
    delta = margin * 2;
    lon = -pi_() / 4;
    lat = pi_() / 3 + delta;
    alt = 1e6;
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = lon + pi_();
    d__2 = -lat;
    latrec_(&c_b101, &d__1, &d__2, raydir);
    margin = 0.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     We don't expect an intercept. */

    xnxpts = 0;
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bad coordinate system.", (ftnlen)22);
    dskdsc[5] = -1.;
    zzrytelt_(vertex, raydir, dskdsc, &margin, &nxpts, xpt);
    chckxc_(&c_true, "SPICE(BADCOORDSYS)", ok, (ftnlen)18);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzrytelt__ */

