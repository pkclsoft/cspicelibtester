/* f_zzrytlat.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 0.;
static doublereal c_b6 = 1.;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b733 = -1.;

/* $Procedure F_ZZRYTLAT ( ZZRYTLAT tests ) */
/* Subroutine */ int f_zzrytlat__(logical *ok)
{
    /* Initialized data */

    static doublereal x[3] = { 1.,0.,0. };
    static doublereal y[3] = { 0.,1.,0. };
    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt(doublereal), cos(doublereal);

    /* Local variables */
    static doublereal midr;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    ), vsub_(doublereal *, doublereal *, doublereal *), vequ_(
	    doublereal *, doublereal *);
    static doublereal xxpt[3], r__;
    extern /* Subroutine */ int zzrytlat_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *), tcase_(char 
	    *, ftnlen), vpack_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), vlcom_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), topen_(char *, ftnlen), t_success__(
	    logical *);
    static integer nxpts;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static doublereal midlat, margin;
    extern /* Subroutine */ int latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal midlon, bounds[6]	/* was [2][3] */, raydir[3], vertex[3]
	    ;
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    static doublereal mag, lat, lon, tol, xpt[3];

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZRYTLAT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZRYTLAT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 12-FEB-2017 (NJB) */

/*        Previous version 06-JUL-2016 (NJB) (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     This value must be kept in sync with the parameter */
/*     LATMRG in ZZINLAT. */


/*     Local Variables */

/*      CHARACTER*(TITLSZ)    TITLE */
/*      INTEGER               I */

/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZRYTLAT", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/*     The following tests use a volume element with a longitude extent */
/*     of less than 90 degrees and a latitude extent of 15 degrees. The */
/*     depth of the element is 50 km. The element is located above the */
/*     equator. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Hit on north polar cap. Margin = 0.", (ftnlen)35);
    bounds[0] = 0.;
    bounds[1] = pi_() * 2;
    bounds[2] = pi_() / 4;
    bounds[3] = pi_() / 2;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    vpack_(&c_b4, &c_b4, &bounds[5], xxpt);
    margin = 0.;
    mag = 1e10;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Hit on south polar cap. Margin = 0.", (ftnlen)35);
    bounds[0] = 0.;
    bounds[1] = pi_() * 2;
    bounds[2] = -pi_() / 2;
    bounds[3] = -pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    d__1 = -bounds[5];
    vpack_(&c_b4, &c_b4, &d__1, xxpt);
    margin = 0.;
    mag = 1e10;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Vertex is inside element. "
	    "Margin = 0.", (ftnlen)75);
    bounds[0] = 0.;
    bounds[1] = pi_() * 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &midlat, xxpt);
    margin = 0.;
    vequ_(xxpt, vertex);
    cleard_(&c__3, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n = 0. Longitude wrap-around case.", (ftnlen)98);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (pi_() * 2 + bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on inner sphere. Verte"
	    "x is outside outer sphere. Margin = 0.", (ftnlen)102);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vscl_(&d__1, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on inner sphere. Verte"
	    "x is inside inner sphere. Margin = 0.", (ftnlen)101);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    cleard_(&c__3, vertex);
    vequ_(xxpt, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on inner sphere. Margi"
	    "n = 0. Longitude wrap-around case.", (ftnlen)98);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (pi_() * 2 + bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vscl_(&d__1, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on upper latitude boun"
	    "dary. Margin = 0.", (ftnlen)81);

/*     This case produces a single intercept of the ray with */
/*     the upper latitude cone. */

    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[3], xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on upper latitude boun"
	    "dary. Intercept is second solution. Margin = 0.", (ftnlen)111);

/*     This case produces a single intercept of the ray with */
/*     the upper latitude cone. */

    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[3], xxpt);

/*     The magnitude of the vertex offset must be large */
/*     enough to place the vertex outside of the upper */
/*     latitude cone. */

    mag = 1e4;

/*     Pick the ray's direction vector so that we can make the */
/*     ray hit the upper latitude cone once at pi radians away */
/*     from XXPT, then hit again at XXPT. */

    d__1 = -pi_() / 5;
    latrec_(&mag, &midlon, &d__1, raydir);
    vsub_(xxpt, raydir, vertex);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on lower latitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[2], xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*     Note: there is no corresponding "second solution" case */
/*     for the lower latitude boundary of an element in the */
/*     northern hemisphere. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on west longitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, bounds, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vequ_(y, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on east longitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &bounds[1], &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vequ_(x, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     The following tests use a volume element with a longitude extent */
/*     of less than degrees and a latitude extent of 15 degrees. The */
/*     depth of the element is 50 km. The element is located below the */
/*     equator. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Vertex is inside element. "
	    "Margin = 0.", (ftnlen)75);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &midlat, xxpt);
    margin = 0.;
    vequ_(xxpt, vertex);
    cleard_(&c__3, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on outer sphere. Margi"
	    "n = 0. Longitude wrap-around case.", (ftnlen)98);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (pi_() * 2 + bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on inner sphere. Verte"
	    "x is outside outer sphere. Margin = 0.", (ftnlen)102);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vscl_(&d__1, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on inner sphere. Verte"
	    "x is inside inner sphere. Margin = 0.", (ftnlen)101);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    cleard_(&c__3, vertex);
    vequ_(xxpt, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on inner sphere. Margi"
	    "n = 0. Longitude wrap-around case.", (ftnlen)98);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4];
    midlon = (pi_() * 2 + bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vscl_(&d__1, xxpt, vertex);
    vminus_(vertex, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on upper latitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[3], xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on lower latitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[2], xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on lower latitude boun"
	    "dary. Intercept is second solution. Margin = 0.", (ftnlen)111);

/*     This case produces a single intercept of the ray with */
/*     the upper latitude cone. */

    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[2], xxpt);

/*     The magnitude of the vertex offset must be large */
/*     enough to place the vertex outside of the lower */
/*     latitude cone. */

    mag = 1e4;

/*     Pick the ray's direction vector so that we can make the */
/*     ray hit the lower latitude cone once at pi radians away */
/*     from XXPT, then hit again at XXPT. */

    d__1 = -pi_() / 5;
    latrec_(&mag, &midlon, &d__1, raydir);
    vsub_(xxpt, raydir, vertex);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */


/*     Note: there is no corresponding "second solution" case */
/*     for the upper latitude boundary of an element in the */
/*     southern hemisphere. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, bounds, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vequ_(y, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin = 0.", (ftnlen)81);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &bounds[1], &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vequ_(x, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);


/*     Following are tests for some cases in which the volume */
/*     element has special properties. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element with southern boundary on the equator. Margin = 0.",
	     (ftnlen)64);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = 0.;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[2], xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element with northern boundary on the equator. Margin = 0.",
	     (ftnlen)64);
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = -pi_() / 6;
    bounds[3] = 0.;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&midr, &midlon, &bounds[3], xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Equatorial band; longitude extent = 2pi. Margin = 0. Vertex is o"
	    "n +X axis.", (ftnlen)74);
    bounds[0] = -pi_();
    bounds[1] = pi_();
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &c_b4, &midlat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, x, vertex);
    vminus_(x, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Equatorial band; longitude extent = 2pi. Margin = 0. Vertex is o"
	    "n +Y axis.", (ftnlen)74);
    bounds[0] = 0.;
    bounds[1] = pi_() * 2;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    xxpt[0] = r__ - 1.;
/* Computing 2nd power */
    d__1 = r__;
/* Computing 2nd power */
    d__2 = r__ - 1.;
    xxpt[1] = sqrt(d__1 * d__1 - d__2 * d__2);
    xxpt[2] = 0.;
    latrec_(&r__, &c_b4, &midlat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vminus_(y, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Equatorial band; longitude extent = 2pi. Margin = 0. Vertex is o"
	    "n +Z axis.", (ftnlen)74);
    bounds[0] = 0.;
    bounds[1] = pi_() * 2;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    xxpt[0] = r__ - 1.;
    xxpt[1] = 0.;
/* Computing 2nd power */
    d__1 = r__;
/* Computing 2nd power */
    d__2 = r__ - 1.;
    xxpt[2] = sqrt(d__1 * d__1 - d__2 * d__2);
    latrec_(&r__, &c_b4, &midlat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(y, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element with wrap-around longitude. Margin = 0. Hit on wes"
	    "t (left) longitude boundary.", (ftnlen)92);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&r__, &bounds[1], &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vequ_(x, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element with wrap-around longitude. Margin = 0. Hit on eas"
	    "t (right) longitude boundary.", (ftnlen)93);
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    midr = (bounds[4] + bounds[5]) / 2;
    latrec_(&r__, &bounds[1], &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vequ_(x, raydir);
    margin = 0.;
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);


/*     The following cases use positive margins. */

/*        POSITIVE MARGINS */



/*     We start with cases involving hits on the outer sphere. */

/*        OUTER SPHERE */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Vertex is inside element. "
	    "Margin > 0. Interior case.", (ftnlen)90);
    margin = 1e-8;
    bounds[0] = pi_() / 4;
    bounds[1] = pi_() / 2;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5] + margin / 2 * r__;
    midlon = (bounds[0] + bounds[1]) / 2;
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &midlon, &midlat, xxpt);
    vequ_(xxpt, vertex);
    cleard_(&c__3, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Interior case. Hit is beyond east longitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Exterior case. Hit is beyond east longitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    lon = bounds[1] + margin * 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Longitude wrap-around case. Interior case. Hit is beyond "
	    "east longitude boundary.", (ftnlen)152);
    margin = 1e-8;
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Longitude wrap-around case. Exterior case. Hit is beyond "
	    "east longitude boundary.", (ftnlen)152);
    margin = 1e-8;
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + margin * 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Interior case. Hit is beyond west longitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Longitude wrap-around case. Interior case. Hit is beyond "
	    "west longitude boundary.", (ftnlen)152);
    margin = 1e-8;
    bounds[0] = pi_() * 5 / 6;
    bounds[1] = pi_() * -5 / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Longitude wrap-around case. Exterior case. Hit is beyond "
	    "west longitude boundary.", (ftnlen)152);
    lon = bounds[0] - margin * 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Interior case. Hit is beyond north latitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Exterior case. Hit is beyond north latitude boundary.", (
	    ftnlen)124);
    lat = bounds[3] + margin * 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Interior case. Hit is beyond north latitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Exterior case. Hit is beyond north latitude boundary.", (
	    ftnlen)124);
    lat = bounds[3] + margin * 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Interior case. Hit is beyond south latitude boundary.", (
	    ftnlen)124);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on outer sphere. Margi"
	    "n > 0. Exterior case. Hit is beyond south latitude boundary.", (
	    ftnlen)124);
    lat = bounds[2] - margin * 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vminus_(vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     The following cases deal with hits on longitude boundaries. */

/*        EAST LONGITUDE */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond south latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lon = bounds[1];
    lat = bounds[2] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vminus_(y, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond south latitude bo"
	    "undary.", (ftnlen)135);
    lat = bounds[2] - margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vminus_(y, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond north latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = -pi_() / 6;
    bounds[1] = 0.;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lon = bounds[1];
    lat = bounds[3] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond north latitude bo"
	    "undary.", (ftnlen)135);
    lat = bounds[3] + margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There is no possibility of a hit */
/*     outside of the outer bounding sphere (radius BOUNDS(2,3)). */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = -pi_() / 6;
    bounds[1] = 0.;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5] + bounds[5] * margin / 2;
    lon = bounds[1];
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = -pi_() / 6;
    bounds[1] = 0.;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    lon = bounds[1];
    midlat = (bounds[2] + bounds[3]) / 2;
    r__ = bounds[4] - bounds[4] * margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on east longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);

/*     Pick a radius value that makes it easy to miss the inner */
/*     bounding sphere. */

    r__ = bounds[4] / 2;
    latrec_(&r__, &lon, &midlat, xxpt);

/*     Choose MAG so that the vertex is inside the inner bounding */
/*     sphere. */

    mag = 1.;
    vlcom_(&c_b6, xxpt, &mag, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        WEST LONGITUDE */

/*     The cases below are mirror images of the east longitude */
/*     cases. */




/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond south latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lon = bounds[0];
    lat = bounds[2] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vequ_(y, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond south latitude bo"
	    "undary.", (ftnlen)135);
    lat = bounds[2] - margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vequ_(y, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond north latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = 0.;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lon = bounds[0];
    lat = bounds[3] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond north latitude bo"
	    "undary.", (ftnlen)135);
    lat = bounds[3] + margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There is no possibility of a hit */
/*     outside of the outer bounding sphere (radius BOUNDS(2,3)). */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = 0.;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5] + bounds[5] * margin / 2;
    lon = bounds[0];
    midlat = (bounds[2] + bounds[3]) / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = 0.;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    lon = bounds[0];
    midlat = (bounds[2] + bounds[3]) / 2;
    r__ = bounds[4] - bounds[4] * margin / 2;
    latrec_(&r__, &lon, &midlat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on west longitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);

/*     Pick a radius value that makes it easy to miss the inner */
/*     bounding sphere. */

    r__ = bounds[4] / 2;
    latrec_(&r__, &lon, &midlat, xxpt);

/*     Choose MAG so that the vertex is inside the inner bounding */
/*     sphere. */

    mag = 1.;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, y, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     The following cases deal with hits on latitude boundaries. */

/*        Volume element is below equator. */

/*        SOUTHERN HEMISPHERE */

/*        NORTH LATITUDE */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[3];
    lon = bounds[0] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    lon = bounds[0] - margin * 2 * bounds[5];
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[3];
    lon = bounds[1] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    lon = bounds[1] + margin * 2 * bounds[5];
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There's no such hit case. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;

/*     Note the upper longitude bound. This value was selected to */
/*     simplify the choice of a ray that would not hit other */
/*     bounding surfaces before hitting the expected intercept. */

    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5] + bounds[5] * margin * 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4] - bounds[4] * margin / 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;

/*     Choose a radius that precludes a valid hit on the inner */
/*     sphere. */

    r__ = bounds[4] / 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        SOUTHERN HEMISPHERE */

/*        SOUTH LATITUDE */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[2];
    lon = bounds[0] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    lon = bounds[0] - margin * 2 * bounds[5];
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[2];
    lon = bounds[1] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    lon = bounds[1] + margin * 2 * bounds[5];
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There's no such hit case. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5] + bounds[5] * margin * 2;
    lat = bounds[2];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;

/*     Choose the ray direction so that the ray skims past the */
/*     volume element. */

    vpack_(&c_b6, &c_b4, &c_b6, raydir);
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, raydir, vertex);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4] - bounds[4] * margin / 2;
    lat = bounds[2];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in southern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 4;
    bounds[3] = -pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4] / 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);


/*        Volume element is above equator. */

/*        NORTHERN HEMISPHERE */

/*        NORTH LATITUDE */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[3];
    lon = bounds[0] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    lon = bounds[0] - margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[3];
    lon = bounds[1] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    lon = bounds[1] + margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There's no such hit case. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    r__ = bounds[5] + bounds[5] * margin / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;

/*     Choose the ray direction so that the ray skims past the */
/*     volume element. */

    vpack_(&c_b6, &c_b4, &c_b733, raydir);
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, raydir, vertex);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[4] - bounds[4] * margin / 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on north latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);

/*     Choose R to ensure a miss. */

    r__ = bounds[4] / 2;
    lat = bounds[3];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);


/*        Volume element is above equator. */

/*        NORTHERN HEMISPHERE */

/*        SOUTH LATITUDE */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[2];
    lon = bounds[0] - margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond west longitude bo"
	    "undary.", (ftnlen)135);
    lon = bounds[0] - margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midr = (bounds[4] + bounds[5]) / 2;
    lat = bounds[2];
    lon = bounds[1] + margin / 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond east longitude bo"
	    "undary.", (ftnlen)135);
    lon = bounds[1] + margin * 2;
    latrec_(&midr, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);

/*     This is a place holder. There's no such hit case. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond outer radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() / 6;
    bounds[3] = pi_() / 4;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    lat = bounds[2];
    midlon = (bounds[0] + bounds[1]) / 2;
    r__ = bounds[5] + bounds[5] * margin * 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Interior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);
    margin = 1e-8;
    r__ = bounds[4] - bounds[4] * margin / 2;
    lat = bounds[2];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, x, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element in northern hemisphere. Hit on south latitude boun"
	    "dary. Margin > 0. Exterior case. Hit is beyond inner radius boun"
	    "dary.", (ftnlen)133);

/*     Choose R to ensure a miss. */

    r__ = bounds[4] / 2;
    lat = bounds[2];
    midlon = (bounds[0] + bounds[1]) / 2;
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);


/*     The following cases deal with hits on the inner bounding sphere. */


/*        INNER RADIUS BOUNDARY */



/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond east longitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond east longitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + bounds[5] * 2 * margin;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond west longitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond west longitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - bounds[5] * 2 * margin;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond north latitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond north latitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin * 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond south latitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond south latitude boun"
	    "dary. Vertex is inside inner sphere; this is the \"first endpoint"
	    "\" case.", (ftnlen)199);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin * 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    cleard_(&c__3, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond east longitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2];
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond east longitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[1] + bounds[5] * 2 * margin;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2];
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond west longitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2];
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond west longitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    midlat = (bounds[2] + bounds[3]) / 2;
    lon = bounds[0] - bounds[5] * 2 * margin;
    r__ = bounds[4];
    latrec_(&r__, &lon, &midlat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2];
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond north latitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2];
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond north latitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin * 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2] * -2;
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Interior case. Hit is beyond south latitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 6;
    bounds[3] = pi_() / 6;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin / 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2] * -2;
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Small element that spans the equator. Hit on inner radius bounda"
	    "ry. Margin > 0. Exterior case. Hit is beyond south latitude boun"
	    "dary. Vertex is outside inner sphere; this is the \"second endpo"
	    "int\" case.", (ftnlen)201);
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin * 2;
    r__ = bounds[4];
    latrec_(&r__, &midlon, &lat, xxpt);
    vertex[0] = xxpt[0] * -2;
    vertex[1] = xxpt[1] * -2;
    vertex[2] = xxpt[2] * -2;
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);


/*     SPECIAL CASES */


/*     The following cases deal with hits near the poles */


/*        NEAR NORTH POLE, OUTER SPHERE */



/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near north pole. Hit on outer sp"
	    "here. Margin > 0. Interior case. Hit is beyond north latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = 0.;
    bounds[3] = halfpi_() - 9.9900099900099912e-9;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] + margin / 2;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near north pole. Hit on outer sp"
	    "here. Margin > 0. Interior case. Hit is below north latitude bou"
	    "ndary, but above high latitude limit. Longitude is off by pi rad"
	    "ians.", (ftnlen)197);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = 0.;
    bounds[3] = halfpi_() - 2.5000000000000001e-9;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = pi_() + (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] - margin / 4;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near north pole. Hit on outer sp"
	    "here. Margin > 0. Exterior case. Hit is below north latitude bou"
	    "ndary, and below high latitude limit. Longitude is off by pi rad"
	    "ians.", (ftnlen)197);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = 0.;
    bounds[3] = halfpi_() - 5.0000000000000001e-9;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = pi_() + (bounds[0] + bounds[1]) / 2;
    lat = bounds[3] - 2e-8;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near south pole. Hit on outer sp"
	    "here. Margin > 0. Interior case. Hit is beyond south latitude bo"
	    "undary.", (ftnlen)135);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -halfpi_() + 9.9900099900099912e-9;
    bounds[3] = 0.;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] - margin / 2;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near south pole. Hit on outer sp"
	    "here. Margin > 0. Interior case. Hit is above south latitude bou"
	    "ndary, but below low latitude limit. Longitude is off by pi radi"
	    "ans.", (ftnlen)196);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -halfpi_() + 2.5000000000000001e-9;
    bounds[3] = 0.;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = pi_() + (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] + margin / 4;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element ranging from equator to near north pole. Hit on outer sp"
	    "here. Margin > 0. Exterior case. Hit is above south latitude bou"
	    "ndary, and above low latitude limit. Longitude is off by pi radi"
	    "ans.", (ftnlen)196);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -halfpi_() + 5.0000000000000001e-9;
    bounds[3] = 0.;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    midlon = pi_() + (bounds[0] + bounds[1]) / 2;
    lat = bounds[2] + 2e-8;
    r__ = bounds[5];
    latrec_(&r__, &midlon, &lat, xxpt);
    mag = 1e3;
    vscl_(&mag, xxpt, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is \"orange slice\" ranging from south to north pole. Ve"
	    "rtex is on +Z axis; ray points in -Z direction.", (ftnlen)109);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -halfpi_();
    bounds[3] = halfpi_();
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    mag = 1e10;
    vpack_(&c_b4, &c_b4, &bounds[5], xxpt);
    vpack_(&c_b4, &c_b4, &mag, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is \"orange slice\" ranging from south to north pole. Ve"
	    "rtex is on -Z axis; ray points in +Z direction.", (ftnlen)109);
    d__1 = -bounds[5];
    vpack_(&c_b4, &c_b4, &d__1, xxpt);
    d__1 = -mag;
    vpack_(&c_b4, &c_b4, &d__1, vertex);
    vsub_(xxpt, vertex, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     The following cases deal with hits near the poles but outside */
/*     of the latitude margins. */


/*        HIGH LATITUDE, OUTER SPHERE */


/* --- Case: ------------------------------------------------------ */

    tcase_("Element is high latitude patch. Ray hits beyond east longitude b"
	    "oundary. Test of longitude margin scaling. Interior case.", (
	    ftnlen)121);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() * 5 / 12;
    bounds[3] = pi_() / 2 - 2e-8;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    lat = bounds[3] - margin;
    lon = bounds[1] + margin / 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is high latitude patch. Ray hits beyond east longitude b"
	    "oundary. Test of longitude margin scaling. Exterior case.", (
	    ftnlen)121);
    r__ = bounds[5];
    lat = bounds[3] - margin;
    lon = bounds[1] + margin * 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is high latitude patch. Ray hits beyond west longitude b"
	    "oundary. Test of longitude margin scaling. Interior case.", (
	    ftnlen)121);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = pi_() * 5 / 12;
    bounds[3] = pi_() / 2 - 2e-8;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    lat = bounds[3] - margin;
    lon = bounds[0] - margin / 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is high latitude patch. Ray hits beyond west longitude b"
	    "oundary. Test of longitude margin scaling. Exterior case.", (
	    ftnlen)121);
    r__ = bounds[5];
    lat = bounds[3] - margin;
    lon = bounds[0] - margin * 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    vlcom_(&c_b6, xxpt, &mag, z__, vertex);
    vminus_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*        LOW LATITUDE, OUTER SPHERE */


/* --- Case: ------------------------------------------------------ */

    tcase_("Element is low latitude patch. Ray hits beyond east longitude bo"
	    "undary. Test of longitude margin scaling. Interior case.", (
	    ftnlen)120);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 2 + 2e-8;
    bounds[3] = pi_() * -5 / 12;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    lat = bounds[2] + margin;
    lon = bounds[1] + margin / 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is low latitude patch. Ray hits beyond east longitude bo"
	    "undary. Test of longitude margin scaling. Exterior case.", (
	    ftnlen)120);
    r__ = bounds[5];
    lat = bounds[2] + margin;
    lon = bounds[1] + margin * 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is low latitude patch. Ray hits beyond west longitude bo"
	    "undary. Test of longitude margin scaling. Interior case.", (
	    ftnlen)120);
    margin = 1e-8;
    bounds[0] = -pi_() / 6;
    bounds[1] = pi_() / 6;
    bounds[2] = -pi_() / 2 + 2e-8;
    bounds[3] = pi_() * -5 / 12;
    bounds[4] = 3e3;
    bounds[5] = 3050.;
    r__ = bounds[5];
    lat = bounds[2] + margin;
    lon = bounds[0] - margin / 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Element is low latitude patch. Ray hits beyond west longitude bo"
	    "undary. Test of longitude margin scaling. Exterior case.", (
	    ftnlen)120);
    r__ = bounds[5];
    lat = bounds[2] + margin;
    lon = bounds[0] - margin * 2 / cos(lat);
    latrec_(&r__, &lon, &lat, xxpt);
    mag = 1e3;
    d__1 = -mag;
    vlcom_(&c_b6, xxpt, &d__1, z__, vertex);
    vequ_(z__, raydir);
    zzrytlat_(vertex, raydir, bounds, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/*     ZZRYTLAT is declared to be error free, in the interest of */
/*     speed. This decision may need to be revisited. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzrytlat__ */

