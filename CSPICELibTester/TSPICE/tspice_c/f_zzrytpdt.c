/* f_zzrytpdt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 3010.;
static doublereal c_b5 = 3020.;
static doublereal c_b6 = 2800.;
static logical c_true = TRUE_;
static integer c__3 = 3;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static doublereal c_b34 = 0.;
static doublereal c_b35 = -1.;
static doublereal c_b75 = 1.;
static integer c__2 = 2;
static doublereal c_b650 = 1e-11;

/* $Procedure F_ZZRYTPDT ( ZZRYTPDT tests ) */
/* Subroutine */ int f_zzrytpdt__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };
    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt(doublereal), tan(doublereal);

    /* Local variables */
    static doublereal emin, maxd, emax, apex[3];
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    static doublereal pmin, udir[3], pmax;
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int zzellbds_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal a, b;
    extern /* Subroutine */ int zzelnaxx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal f, h__, p[3];
    extern /* Subroutine */ int zzinrypl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *)
	    ;
    static doublereal s, angle;
    extern /* Subroutine */ int zzrytpdt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, doublereal *)
	    , tcase_(char *, ftnlen);
    static doublereal colat;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static logical found;
    static doublereal veast[3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal const__, h2;
    extern /* Subroutine */ int ucrss_(doublereal *, doublereal *, doublereal 
	    *);
    extern doublereal twopi_(void);
    static doublereal vwest[3];
    extern /* Subroutine */ int t_success__(logical *);
    static integer nxpts;
    static doublereal endpt1[3], endpt2[3], targt2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal re;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal rp;
    extern doublereal halfpi_(void);
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     georec_(doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), chcksl_(char *, 
	    logical *, logical *, logical *, ftnlen);
    static doublereal negdir[3];
    extern /* Subroutine */ int latrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal margin;
    extern /* Subroutine */ int recgeo_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), incnsg_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, doublereal *, doublereal *);
    static doublereal minalt, bounds[6]	/* was [2][3] */, corpar[10], maxalt, 
	    offset[3], raydir[3], target[3], uplnml[3], vertex[3], xincpt, 
	    yincpt;
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *), surfpt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *), vhatip_(doublereal *);
    static integer xnxpts;
    static doublereal alt, lat, lon, tol, xpt[3], xyx[3], xpt1[3], xpt2[3];

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZRYTPDT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZRYTPDT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 18-JAN-2017 (NJB) (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Initial values */


/*     Saved variables */


/*     Open the test family. */

    topen_("F_ZZRYTPDT", (ftnlen)10);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: negative margin.", (ftnlen)23);
    re = 3e3;
    f = .1;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 2;
    bounds[3] = pi_() / 2;
    bounds[4] = -re / 2.;
    bounds[5] = re / 2.;
    corpar[0] = re;
    corpar[1] = f;
    vpack_(&c_b4, &c_b5, &c_b6, vertex);
    margin = -1e-6;
    vminus_(vertex, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: zero direction vector.", (ftnlen)29);
    margin = 1e-6;
    cleard_(&c__3, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error: longitude bound out of range.", (ftnlen)36);
    vminus_(vertex, raydir);
    bounds[0] = pi_() * -3;
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
    bounds[0] = pi_() * -2;
    bounds[1] = pi_() * 3;
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_true, "SPICE(VALUEOUTOFRANGE)", ok, (ftnlen)22);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */
/* *********************************************************************** */


/*     OBLATE CASES */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup for simple oblate cases.", (ftnlen)30);


/*     The following simple cases all use the same volume element: */

/*        RE:      3000.0 */
/*        F:          0.5 */

/*        Lon:     -PI()/4 : PI()/4 */
/*        Lat:     -PI()/6 : PI()/3 */
/*        Alt:     -100.0  : 100.0 */

    re = 3e3;
    f = .5;
    bounds[0] = -pi_() / 4;
    bounds[1] = pi_() / 4;
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;

/*     Use a positive margin. */

    margin = 1e-5;

/*     Get the radii of the outer and inner bounding ellipsoids. */
/*     Take margin into account. */

/*     This is the oblate case. */

    rp = re * (1. - f);
    minalt = bounds[4] - margin * abs(bounds[4]);
    maxalt = bounds[5] + margin * abs(bounds[5]);
    zzellbds_(&re, &rp, &maxalt, &minalt, &emax, &pmax, &emin, &pmin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray's vertex is inside element.", (ftnlen)31);

/*     Create a vertex at maximum altitude. This point will be */
/*     inside the outer bounding ellipsoid, so it will be */
/*     considered to be inside the volume element. */

    lon = 0.;
    lat = 0.;
    alt = bounds[5];
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vminus_(vertex, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", vertex, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray misses outer bounding spheroid.", (ftnlen)35);
    vpack_(&re, &c_b34, &re, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits outer bounding spheroid but misses element.", (ftnlen)52)
	    ;
    d__1 = rp * .99;
    vpack_(&re, &c_b34, &d__1, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on outer bounding spheroid.", (ftnlen)44);
    d__1 = re * 2;
    d__2 = rp / 2;
    vpack_(&d__1, &c_b34, &d__2, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

    surfpt_(vertex, raydir, &emax, &emax, &pmax, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on inner bounding spheroid. Vertex is outside o"
	    "uter spheroid.", (ftnlen)78);
    d__1 = re * -4;
    d__2 = rp / 2;
    vpack_(&d__1, &c_b34, &d__2, vertex);
    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. We need to approach the */
/*     inner ellipsoid from the opposite side, since the vertex */
/*     and element are on opposite sides of the Z axis. */

    vequ_(vertex, endpt2);
    endpt2[0] = -endpt2[0];
    vminus_(raydir, negdir);
    surfpt_(endpt2, negdir, &emin, &emin, &pmin, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on inner bounding spheroid. Vertex is inside in"
	    "ner spheroid.", (ftnlen)77);
    d__1 = rp / 2;
    vpack_(&c_b34, &c_b34, &d__1, vertex);
    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

    surfpt_(vertex, raydir, &emin, &emin, &pmin, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from west side.", (ftnlen)32);
    s = sqrt(2.) / 2;
    d__1 = s * re - 10.;
    d__2 = re * -2;
    vpack_(&d__1, &d__2, &c_b34, vertex);
    vpack_(&c_b34, &c_b75, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */


/*     Create the normal vector for the western boundary. */

    latrec_(&c_b75, bounds, &c_b34, vwest);
    ucrss_(vwest, z__, uplnml);

/*     The western plane contains the origin. */

    const__ = 0.;
    vhat_(raydir, udir);
    maxd = re * 4;
    zzinrypl_(vertex, udir, uplnml, &const__, &maxd, &xnxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZINRYPL XNXTPS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from east side.", (ftnlen)32);
    s = sqrt(2.) / 2;
    d__1 = s * re - 10.;
    d__2 = re * 2;
    vpack_(&d__1, &d__2, &c_b34, vertex);
    vpack_(&c_b34, &c_b35, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */


/*     Create the normal vector for the eastern boundary. */

    latrec_(&c_b75, &bounds[1], &c_b34, veast);
    ucrss_(z__, veast, uplnml);

/*     The eastern plane contains the origin. */

    const__ = 0.;
    vhat_(raydir, udir);
    maxd = re * 4;
    zzinrypl_(vertex, udir, uplnml, &const__, &maxd, &xnxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZINRYPL XNXTPS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from north side.", (ftnlen)33);
    georec_(&c_b34, &bounds[3], &c_b34, &re, &f, vertex);
    recgeo_(vertex, &re, &f, &lon, &lat, &alt);
    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the upper latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone is not the origin. */
/*     It lies on the -Z axis. Compute the intercept. */

    angle = halfpi_() - bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from south side.", (ftnlen)33);
    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone is not the origin. */
/*     It lies on the +Z axis. Compute the intercept. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     In the following cases, we change the latitude bounds of */
/*     the element. */


/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at 0 latitude. Ray hits element from n"
	    "orth side.", (ftnlen)74);
    bounds[2] = -pi_() / 3;
    bounds[3] = 0.;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "upper latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the -Z axis. However, in this case the apex */
/*     should be the origin. Compute the intercept by the usual means. */

    angle = halfpi_() - bounds[3];
    lat = bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at negative latitude. Ray hits element"
	    " from north side.", (ftnlen)81);
    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 6;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "upper latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the -Z axis. However, in this case the apex */
/*     should be the origin. Compute the intercept by the usual means. */

    angle = halfpi_() - bounds[3];
    lat = bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at 0 latitude. Ray hits element from s"
	    "outh side.", (ftnlen)74);
    bounds[2] = 0.;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the -Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "lower latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the +Z axis (for cones of negative latitude). */
/*     However, in this case the apex should be the origin. Compute */
/*     the intercept by the usual means. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at negative latitude. Ray hits element"
	    " from south side.", (ftnlen)81);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the -Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the +Z axis (for cones of negative latitude). */
/*     However, in this case the apex should be the origin. Compute */
/*     the intercept by the usual means. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     The following latitude boundary intercept cases test the handling */
/*     of multiple roots in the ray-cone intercept computation. In these */
/*     cases, the second root should be chosen. */


/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at positive latitude. Ray hits element"
	    " from the -X direction.", (ftnlen)87);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and maximum latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex in the -X direction. and lower it below the outer */
/*     bounding sphere in the -Z direction. */

    vertex[0] -= re * 10;
    vertex[2] += -10.;

/*     This ray should hit the upper latitude cone twice. The second */
/*     intercept is the one we want. */

    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the upper latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[0] = -endpt2[0];
    angle = halfpi_() - bounds[3];
    lat = bounds[3];

/*     The cone's apex should be on the -Z axis. */

    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);

/*     Note: we expect two roots. The second one is the correct one. */

    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__2, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt2, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at negative latitude. Ray hits element"
	    " from the -X direction.", (ftnlen)87);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex in the -X direction. and raise it below the outer */
/*     bounding sphere in the +Z direction. */

    vertex[0] -= re * 10;
    vertex[2] += 10.;

/*     This ray should hit the lower latitude cone twice. The second */
/*     intercept is the one we want. */

    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[0] = -endpt2[0];
    angle = halfpi_() - bounds[2];
    lat = bounds[2];

/*     The cone's apex should be on the +Z axis. */

    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);

/*     Note: we expect two roots. The second one is the correct one. */

    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__2, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt2, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);
/* *********************************************************************** */


/*     PROLATE CASES */


/* *********************************************************************** */

/*     Repeat the cases above using a prolate reference ellipsoid. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Setup for simple prolate cases.", (ftnlen)31);


/*     The following simple cases all use the same volume element: */

/*        RE:      3000.0 */
/*        F:         -1.0 */

/*        Lon:     -PI()/4 : PI()/4 */
/*        Lat:     -PI()/6 : PI()/3 */
/*        Alt:     -100.0  : 100.0 */

    re = 3e3;
    f = -1.;
    bounds[0] = -pi_() / 4;
    bounds[1] = pi_() / 4;
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;
    bounds[4] = -100.;
    bounds[5] = 100.;
    corpar[0] = re;
    corpar[1] = f;

/*     Use a positive margin. */

    margin = 1e-5;

/*     Get the radii of the outer and inner bounding ellipsoids. */
/*     Take margin into account. */

/*     This is the prolate case. */

    rp = re * (1. - f);
    minalt = bounds[4] - margin * abs(bounds[4]);
    maxalt = bounds[5] + margin * abs(bounds[5]);

/*     Careful...see the header of ZZELLBDS for info on use */
/*     with prolate ellipsoids. */

    zzellbds_(&rp, &re, &maxalt, &minalt, &pmax, &emax, &pmin, &emin);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray's vertex is inside element.", (ftnlen)31);

/*     Create a vertex at maximum altitude. This point will be */
/*     inside the outer bounding ellipsoid, so it will be */
/*     considered to be inside the volume element. */

    lon = 0.;
    lat = 0.;
    alt = bounds[5];
    georec_(&lon, &lat, &alt, &re, &f, vertex);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vminus_(vertex, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", vertex, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray misses outer bounding spheroid.", (ftnlen)35);
    vpack_(&rp, &c_b34, &rp, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits outer bounding spheroid but misses element.", (ftnlen)52)
	    ;
    d__1 = rp * .99;
    vpack_(&d__1, &c_b34, &re, vertex);
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on outer bounding spheroid.", (ftnlen)44);
    d__1 = re * 2;
    d__2 = rp / 2;
    vpack_(&d__1, &c_b34, &d__2, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

    surfpt_(vertex, raydir, &emax, &emax, &pmax, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on inner bounding spheroid. Vertex is outside o"
	    "uter spheroid.", (ftnlen)78);
    d__1 = re * -4;
    d__2 = rp / 2;
    vpack_(&d__1, &c_b34, &d__2, vertex);
    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. We need to approach the */
/*     inner ellipsoid from the opposite side, since the vertex */
/*     and element are on opposite sides of the Z axis. */

    vequ_(vertex, endpt2);
    endpt2[0] = -endpt2[0];
    vminus_(raydir, negdir);
    surfpt_(endpt2, negdir, &emin, &emin, &pmin, xxpt, &found);
/*      CALL SURFPT ( VERTEX, RAYDIR, EMAX, EMAX, PMAX, XXPT, FOUND ) */
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element on inner bounding spheroid. Vertex is inside in"
	    "ner spheroid.", (ftnlen)77);
    d__1 = rp / 2;
    vpack_(&c_b34, &c_b34, &d__1, vertex);
    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

    surfpt_(vertex, raydir, &emin, &emin, &pmin, xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from west side.", (ftnlen)32);
    s = sqrt(2.) / 2;
    d__1 = s * re - 10.;
    d__2 = re * -2;
    vpack_(&d__1, &d__2, &c_b34, vertex);
    vpack_(&c_b34, &c_b75, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */


/*     Create the normal vector for the western boundary. */

    latrec_(&c_b75, bounds, &c_b34, vwest);
    ucrss_(vwest, z__, uplnml);

/*     The western plane contains the origin. */

    const__ = 0.;
    vhat_(raydir, udir);
    maxd = re * 4;
    zzinrypl_(vertex, udir, uplnml, &const__, &maxd, &xnxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZINRYPL XNXTPS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from east side.", (ftnlen)32);
    s = sqrt(2.) / 2;
    d__1 = s * re - 10.;
    d__2 = re * 2;
    vpack_(&d__1, &d__2, &c_b34, vertex);
    vpack_(&c_b34, &c_b35, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */


/*     Create the normal vector for the eastern boundary. */

    latrec_(&c_b75, &bounds[1], &c_b34, veast);
    ucrss_(z__, veast, uplnml);

/*     The eastern plane contains the origin. */

    const__ = 0.;
    vhat_(raydir, udir);
    maxd = re * 4;
    zzinrypl_(vertex, udir, uplnml, &const__, &maxd, &xnxpts, xxpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("ZZINRYPL XNXTPS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)15, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from north side.", (ftnlen)33);
    georec_(&c_b34, &bounds[3], &c_b34, &re, &f, vertex);
    recgeo_(vertex, &re, &f, &lon, &lat, &alt);
    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the upper latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone is not the origin. */
/*     It lies on the -Z axis. Compute the intercept. */

    angle = halfpi_() - bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Ray hits element from south side.", (ftnlen)33);
    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone is not the origin. */
/*     It lies on the +Z axis. Compute the intercept. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     In the following cases, we change the latitude bounds of */
/*     the element. */


/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at 0 latitude. Ray hits element from n"
	    "orth side.", (ftnlen)74);
    bounds[2] = -pi_() / 3;
    bounds[3] = 0.;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "upper latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the -Z axis. However, in this case the apex */
/*     should be the origin. Compute the intercept by the usual means. */

    angle = halfpi_() - bounds[3];
    lat = bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at negative latitude. Ray hits element"
	    " from north side.", (ftnlen)81);
    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 6;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     raise it above the outer bounding sphere in the Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * 2;
    vpack_(&c_b34, &c_b34, &c_b35, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "upper latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the -Z axis. However, in this case the apex */
/*     should be the origin. Compute the intercept by the usual means. */

    angle = halfpi_() - bounds[3];
    lat = bounds[3];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at 0 latitude. Ray hits element from s"
	    "outh side.", (ftnlen)74);
    bounds[2] = 0.;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the -Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the "lower latitude cone." */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the +Z axis (for cones of negative latitude). */
/*     However, in this case the apex should be the origin. Compute */
/*     the intercept by the usual means. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at negative latitude. Ray hits element"
	    " from south side.", (ftnlen)81);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex toward the origin in the X direction and */
/*     lower it below the outer bounding sphere in the -Z direction. */
    vertex[0] += -100.;
    vertex[2] = re * -2;
    vpack_(&c_b34, &c_b34, &c_b75, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[2] = -endpt2[2];

/*     Caution! The apex of the latitude cone usually is not the origin. */
/*     It usually lies on the +Z axis (for cones of negative latitude). */
/*     However, in this case the apex should be the origin. Compute */
/*     the intercept by the usual means. */

    angle = halfpi_() - bounds[2];
    lat = bounds[2];
    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);
    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__1, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt1, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/*     The following latitude boundary intercept cases test the handling */
/*     of multiple roots in the ray-cone intercept computation. In these */
/*     cases, the second root should be chosen. */


/* --- Case: ------------------------------------------------------ */

    tcase_("North element boundary is at positive latitude. Ray hits element"
	    " from the -X direction.", (ftnlen)87);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and maximum latitude, longitude zero. */

    georec_(&c_b34, &bounds[3], &bounds[5], &re, &f, vertex);

/*     Shift the vertex in the -X direction. and lower it below the outer */
/*     bounding sphere in the -Z direction. */

    vertex[0] -= re * 10;
    vertex[2] += -10.;

/*     This ray should hit the upper latitude cone twice. The second */
/*     intercept is the one we want. */

    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the upper latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[0] = -endpt2[0];
    angle = halfpi_() - bounds[3];
    lat = bounds[3];

/*     The cone's apex should be on the -Z axis. */

    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);

/*     Note: we expect two roots. The second one is the correct one. */

    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__2, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt2, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("South element boundary is at negative latitude. Ray hits element"
	    " from the -X direction.", (ftnlen)87);
    bounds[2] = -pi_() / 3;
    bounds[3] = pi_() / 3;

/*     Find the point on the reference ellipsoid at maximum altitude */
/*     and minimum latitude, longitude zero. */

    georec_(&c_b34, &bounds[2], &bounds[5], &re, &f, vertex);

/*     Shift the vertex in the -X direction. and raise it below the outer */
/*     bounding sphere in the +Z direction. */

    vertex[0] -= re * 10;
    vertex[2] += 10.;

/*     This ray should hit the lower latitude cone twice. The second */
/*     intercept is the one we want. */

    vpack_(&c_b75, &c_b34, &c_b34, raydir);
    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Compute the expected intercept. */

/*     The intercept should be on the lower latitude cone. */

    vequ_(vertex, endpt1);
    vequ_(endpt1, endpt2);
    endpt2[0] = -endpt2[0];
    angle = halfpi_() - bounds[2];
    lat = bounds[2];

/*     The cone's apex should be on the +Z axis. */

    zzelnaxx_(&re, &rp, &lat, &xincpt, &yincpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(origin, apex);
    apex[2] = yincpt;
    incnsg_(apex, z__, &angle, endpt1, endpt2, &xnxpts, xpt1, xpt2);

/*     Note: we expect two roots. The second one is the correct one. */

    chcksi_("INCNSG XNXPTS", &xnxpts, "=", &c__2, &c__0, ok, (ftnlen)13, (
	    ftnlen)1);
    tol = 1e-11;
    chckad_("XPT", xpt, "~~/", xpt2, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case: ray hits positive latitude cone below X-Y plane. No"
	    " valid roots.", (ftnlen)77);

/*     In the following case, the ray hits a latitude cone at */
/*     a point having longitude and altitude within the element's */
/*     bounds, but the intercept is nevertheless outside the element. */

/*     This type of case should not arise if we have a planetodetic */
/*     volume element boundary created using DSKW02, because that */
/*     routine limits the lower altitude. However, it is possible to */
/*     pass to ZZRYTPDT inputs that define such a case, and */
/*     we want to make sure that it's handled properly. */

/*     Use a highly eccentric spheroid. */

    re = 3e3;
    f = .8;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = 0.;
    bounds[3] = pi_() / 8;

/*     Create a point on the spheroid having maximum latitude. */

    lat = bounds[3];
    georec_(&c_b34, &lat, &c_b34, &re, &f, p);
    a = re;
    b = re * (1. - f);
    zzelnaxx_(&a, &b, &lat, &xincpt, &yincpt);
    vpack_(&c_b34, &c_b34, &yincpt, apex);
    vpack_(&xincpt, &c_b34, &c_b34, xyx);

/*     Pick a lower altitude bound that will get past ZZELLBDS. */

/* Computing 2nd power */
    d__1 = b;
    bounds[4] = -(d__1 * d__1) / a + 1.;
    bounds[5] = 0.;

/*     Consider the vector from the cone's apex to P. We'll pick a */
/*     target point on this vector, such that the target has negative Z */
/*     component. The target's altitude is within the element's altitude */
/*     bounds, because the altitude is measured from the surface on the */
/*     -Z side. */

    colat = halfpi_() - lat;
    h__ = -b - bounds[4] / 2;
    d__1 = -tan(colat) * (yincpt - h__);
    vpack_(&d__1, &c_b34, &h__, target);

/*     Check target's off-axis angle: the target should lie on the cone. */

    vsub_(target, apex, offset);

/*     Create a horizontal ray that contacts the cone at TARGET. */

    d__1 = a * 2;
    vpack_(&d__1, &c_b34, &h__, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);

/*     Find the intercept of the ray with the boundary of the */
/*     volume element. The ray will hit the upper latitude cone */
/*     below the X-Y plane. This should be considered a non-intercept */
/*     case. */

    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case: ray hits positive latitude cone below X-Y plane, th"
	    "en above X-Y plane. First root is invalid; second root is valid.",
	     (ftnlen)128);

/*     In the following case, the ray hits a latitude cone at */
/*     a point having longitude and altitude within the element's */
/*     bounds, but the intercept is nevertheless outside the element. */

/*     This type of case should not arise if we have a planetodetic */
/*     volume element boundary created using DSKW02, because that */
/*     routine limits the lower altitude. However, it is possible to */
/*     pass to ZZRYTPDT inputs that define such a case, and */
/*     we want to make sure that it's handled properly. */

/*     Use a highly eccentric spheroid. */

    re = 3e3;
    f = .8;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = 0.;
    bounds[3] = pi_() / 8;

/*     Create a point on the spheroid having maximum latitude. */

    lat = bounds[3];
    georec_(&c_b34, &lat, &c_b34, &re, &f, p);
    a = re;
    b = re * (1. - f);
    zzelnaxx_(&a, &b, &lat, &xincpt, &yincpt);
    vpack_(&c_b34, &c_b34, &yincpt, apex);
    vpack_(&xincpt, &c_b34, &c_b34, xyx);

/*     Pick a lower altitude bound that will get past ZZELLBDS. */

/* Computing 2nd power */
    d__1 = b;
    bounds[4] = -(d__1 * d__1) / a + 1.;
    bounds[5] = 100.;

/*     Consider the vector from the cone's apex to P. We'll pick a */
/*     target point on this vector, such that the target has negative Z */
/*     component. The target's altitude is within the element's altitude */
/*     bounds, because the altitude is measured from the surface on the */
/*     -Z side. */

    colat = halfpi_() - lat;
    h__ = -b - bounds[4] / 2;
    d__1 = -tan(colat) * (yincpt - h__);
    vpack_(&d__1, &c_b34, &h__, target);

/*     Create a second target point that does have latitude within */
/*     bounds. */

    h2 = b / 8;
    d__1 = -tan(colat) * (h2 - yincpt);
    vpack_(&d__1, &c_b34, &h2, targt2);

/*     Check second target's off-axis angle: the target should lie on */
/*     the cone. */

    vsub_(targt2, apex, offset);

/*     Create a ray that contacts the cone first at TARGET, then at */
/*     TARGT2. */

    vsub_(targt2, target, raydir);
    vsub_(target, raydir, vertex);
    vhatip_(raydir);

/*     Find the intercept of the ray with the boundary of the */
/*     volume element. The ray will hit the upper latitude cone */
/*     below the X-Y plane. This should be considered a non-intercept */
/*     case. */

    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     The intercept that was found should be the second target. */

    if (*ok) {
	chckad_("XPT", xpt, "~~/", targt2, &c__3, &c_b650, ok, (ftnlen)3, (
		ftnlen)3);
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case: ray hits negative latitude cone above X-Y plane. No"
	    " valid roots.", (ftnlen)77);

/*     In the following case, the ray hits a latitude cone at */
/*     a point having longitude and altitude within the element's */
/*     bounds, but the intercept is nevertheless outside the element. */

/*     This type of case should not arise if we have a planetodetic */
/*     volume element boundary created using DSKW02, because that */
/*     routine limits the lower altitude. However, it is possible to */
/*     pass to ZZRYTPDT inputs that define such a case, and */
/*     we want to make sure that it's handled properly. */

/*     Use a highly eccentric spheroid. */

    re = 3e3;
    f = .8;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 8;
    bounds[3] = 0.;

/*     Create a point on the spheroid having minimum latitude. */

    lat = bounds[2];
    georec_(&c_b34, &lat, &c_b34, &re, &f, p);
    a = re;
    b = re * (1. - f);
    zzelnaxx_(&a, &b, &lat, &xincpt, &yincpt);
    vpack_(&c_b34, &c_b34, &yincpt, apex);
    vpack_(&xincpt, &c_b34, &c_b34, xyx);

/*     Pick a lower altitude bound that will get past ZZELLBDS. */

/* Computing 2nd power */
    d__1 = b;
    bounds[4] = -(d__1 * d__1) / a + 1.;
    bounds[5] = 100.;

/*     Consider the vector from the cone's apex to P. We'll pick a */
/*     target point on this vector, such that the target has positive Z */
/*     component. The target's altitude is within the element's altitude */
/*     bounds, because the altitude is measured from the surface on the */
/*     +Z side. */

    colat = halfpi_() - lat;
    h__ = b - bounds[4] / 2;
    d__1 = -tan(colat) * (yincpt - h__);
    vpack_(&d__1, &c_b34, &h__, target);

/*     Check target's off-axis angle: the target should lie on the cone. */

    vsub_(target, apex, offset);

/*     Create a horizontal ray that contacts the cone at TARGET. */

    d__1 = a * 2;
    vpack_(&d__1, &c_b34, &h__, vertex);
    vpack_(&c_b35, &c_b34, &c_b34, raydir);

/*     Find the intercept of the ray with the boundary of the */
/*     volume element. The ray will hit the upper latitude cone */
/*     below the X-Y plane. This should be considered a non-intercept */
/*     case. */

    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__0, &c__0, ok, (ftnlen)5, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Oblate case: ray hits negative latitude cone above X-Y plane, th"
	    "en below X-Y plane. First root is invalid; second root is valid.",
	     (ftnlen)128);

/*     In the following case, the ray hits a latitude cone at */
/*     a point having longitude and altitude within the element's */
/*     bounds, but the intercept is nevertheless outside the element. */

/*     This type of case should not arise if we have a planetodetic */
/*     volume element boundary created using DSKW02, because that */
/*     routine limits the lower altitude. However, it is possible to */
/*     pass to ZZRYTPDT inputs that define such a case, and */
/*     we want to make sure that it's handled properly. */

/*     Use a highly eccentric spheroid. */

    re = 3e3;
    f = .8;
    corpar[0] = re;
    corpar[1] = f;
    bounds[0] = 0.;
    bounds[1] = twopi_();
    bounds[2] = -pi_() / 8;
    bounds[3] = 0.;

/*     Create a point on the spheroid having minimum latitude. */

    lat = bounds[2];
    georec_(&c_b34, &lat, &c_b34, &re, &f, p);
    a = re;
    b = re * (1. - f);
    zzelnaxx_(&a, &b, &lat, &xincpt, &yincpt);
    vpack_(&c_b34, &c_b34, &yincpt, apex);
    vpack_(&xincpt, &c_b34, &c_b34, xyx);

/*     Pick a lower altitude bound that will get past ZZELLBDS. */

/* Computing 2nd power */
    d__1 = b;
    bounds[4] = -(d__1 * d__1) / a + 1.;
    bounds[5] = 100.;

/*     Consider the vector from the cone's apex to P. We'll pick a */
/*     target point on this vector, such that the target has negative Z */
/*     component. The target's altitude is within the element's altitude */
/*     bounds, because the altitude is measured from the surface on the */
/*     -Z side. */

    colat = halfpi_() - lat;
    h__ = b - bounds[4] / 2;
    d__1 = -tan(colat) * (yincpt - h__);
    vpack_(&d__1, &c_b34, &h__, target);

/*     Create a second target point that does have latitude within */
/*     bounds. */

    h2 = -b / 8;
    d__1 = tan(colat) * (yincpt - h2);
    vpack_(&d__1, &c_b34, &h2, targt2);

/*     Check second target's off-axis angle: the target should lie on */
/*     the cone. */

    vsub_(targt2, apex, offset);

/*     Create a ray that contacts the cone first at TARGET, then at */
/*     TARGT2. */

    vsub_(targt2, target, raydir);
    vsub_(target, raydir, vertex);
    vhatip_(raydir);

/*     Find the intercept of the ray with the boundary of the */
/*     volume element. The ray will hit the upper latitude cone */
/*     below the X-Y plane. This should be considered a non-intercept */
/*     case. */

    zzrytpdt_(vertex, raydir, bounds, corpar, &margin, &nxpts, xpt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("NXPTS", &nxpts, "=", &c__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     The intercept that was found should be the second target. */

    if (*ok) {
	chckad_("XPT", xpt, "~~/", targt2, &c__3, &c_b650, ok, (ftnlen)3, (
		ftnlen)3);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzrytpdt__ */

