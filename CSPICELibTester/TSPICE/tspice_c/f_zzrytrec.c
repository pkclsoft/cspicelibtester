/* f_zzrytrec.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__3 = 3;
static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b169 = 0.;
static logical c_true = TRUE_;

/* $Procedure F_ZZRYTREC ( ZZRYTREC tests ) */
/* Subroutine */ int f_zzrytrec__(logical *ok)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };

    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static doublereal xxpt[3], d__[3];
    static integer f;
    static doublereal h__;
    static integer i__, j, k;
    static doublereal l;
    static integer m;
    extern /* Subroutine */ int zzrytrec_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *);
    static integer p, s;
    static doublereal w, delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), swapd_(doublereal *, 
	    doublereal *);
    static integer c1, c2;
    extern /* Subroutine */ int topen_(char *, ftnlen), t_success__(logical *)
	    ;
    static integer nxpts;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     cleard_(integer *, doublereal *), chckxc_(logical *, char *, 
	    logical *, ftnlen), chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    static doublereal margin, bounds[6]	/* was [2][3] */, raydir[3];
    static integer perpco[2];
    static doublereal vertex[3];
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    static integer xnxpts;
    static doublereal tol, xpt[3];

/* $ Abstract */

/*     Exercise the private SPICELIB routine ZZRYTREC. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */


/*     File: dsktol.inc */


/*     This file contains declarations of tolerance and margin values */
/*     used by the DSK subsystem. */

/*     It is recommended that the default values defined in this file be */
/*     changed only by expert SPICE users. */

/*     The values declared in this file are accessible at run time */
/*     through the routines */

/*        DSKGTL  {DSK, get tolerance value} */
/*        DSKSTL  {DSK, set tolerance value} */

/*     These are entry points of the routine DSKTOL. */

/*        Version 1.0.0 27-FEB-2016 (NJB) */




/*     Parameter declarations */
/*     ====================== */

/*     DSK type 2 plate expansion factor */
/*     --------------------------------- */

/*     The factor XFRACT is used to slightly expand plates read from DSK */
/*     type 2 segments in order to perform ray-plate intercept */
/*     computations. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     Plate expansion is done by computing the difference vectors */
/*     between a plate's vertices and the plate's centroid, scaling */
/*     those differences by (1 + XFRACT), then producing new vertices by */
/*     adding the scaled differences to the centroid. This process */
/*     doesn't affect the stored DSK data. */

/*     Plate expansion is also performed when surface points are mapped */
/*     to plates on which they lie, as is done for illumination angle */
/*     computations. */

/*     This parameter is user-adjustable. */


/*     The keyword for setting or retrieving this factor is */


/*     Greedy segment selection factor */
/*     ------------------------------- */

/*     The factor SGREED is used to slightly expand DSK segment */
/*     boundaries in order to select segments to consider for */
/*     ray-surface intercept computations. The effect of this factor is */
/*     to make the multi-segment intercept algorithm consider all */
/*     segments that are sufficiently close to the ray of interest, even */
/*     if the ray misses those segments. */

/*     This expansion is performed to prevent rays from passing through */
/*     a target object without any intersection being detected. Such */
/*     "false miss" conditions can occur due to round-off errors. */

/*     The exact way this parameter is used is dependent on the */
/*     coordinate system of the segment to which it applies, and the DSK */
/*     software implementation. This parameter may be changed in a */
/*     future version of SPICE. */


/*     The keyword for setting or retrieving this factor is */


/*     Segment pad margin */
/*     ------------------ */

/*     The segment pad margin is a scale factor used to determine when a */
/*     point resulting from a ray-surface intercept computation, if */
/*     outside the segment's boundaries, is close enough to the segment */
/*     to be considered a valid result. */

/*     This margin is required in order to make DSK segment padding */
/*     (surface data extending slightly beyond the segment's coordinate */
/*     boundaries) usable: if a ray intersects the pad surface outside */
/*     the segment boundaries, the pad is useless if the intercept is */
/*     automatically rejected. */

/*     However, an excessively large value for this parameter is */
/*     detrimental, since a ray-surface intercept solution found "in" a */
/*     segment can supersede solutions in segments farther from the */
/*     ray's vertex. Solutions found outside of a segment thus can mask */
/*     solutions that are closer to the ray's vertex by as much as the */
/*     value of this margin, when applied to a segment's boundary */
/*     dimensions. */

/*     The keyword for setting or retrieving this factor is */


/*     Surface-point membership margin */
/*     ------------------------------- */

/*     The surface-point membership margin limits the distance */
/*     between a point and a surface to which the point is */
/*     considered to belong. The margin is a scale factor applied */
/*     to the size of the segment containing the surface. */

/*     This margin is used to map surface points to outward */
/*     normal vectors at those points. */

/*     If this margin is set to an excessively small value, */
/*     routines that make use of the surface-point mapping won't */
/*     work properly. */


/*     The keyword for setting or retrieving this factor is */


/*     Angular rounding margin */
/*     ----------------------- */

/*     This margin specifies an amount by which angular values */
/*     may deviate from their proper ranges without a SPICE error */
/*     condition being signaled. */

/*     For example, if an input latitude exceeds pi/2 radians by a */
/*     positive amount less than this margin, the value is treated as */
/*     though it were pi/2 radians. */

/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     Longitude alias margin */
/*     ---------------------- */

/*     This margin specifies an amount by which a longitude */
/*     value can be outside a given longitude range without */
/*     being considered eligible for transformation by */
/*     addition or subtraction of 2*pi radians. */

/*     A longitude value, when compared to the endpoints of */
/*     a longitude interval, will be considered to be equal */
/*     to an endpoint if the value is outside the interval */
/*     differs from that endpoint by a magnitude less than */
/*     the alias margin. */


/*     Units are radians. */


/*     This parameter is not user-adjustable. */

/*     The keyword for retrieving this parameter is */


/*     End of include file dsktol.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private SPICELIB routine ZZRYTREC. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 22-JUL-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZRYTREC", (ftnlen)10);

/*     We rely on ZZRAYBOX to work correctly; these tests simply */
/*     validate the use by ZZRYTREC of ZZRAYBOX. */

/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */
/* *********************************************************************** */


/*     Simple hit/miss cases */


/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Hit case. Zero MARGIN.", (ftnlen)22);
    margin = 0.;


/*     Set the coordinate system and bounds. */
    l = 40.;
    w = 20.;
    h__ = 10.;
    bounds[0] = -l / 2;
    bounds[1] = l / 2;
    bounds[2] = -w / 2;
    bounds[3] = w / 2;
    bounds[4] = -h__ / 2;
    bounds[5] = h__ / 2;
    d__[0] = l;
    d__[1] = w;
    d__[2] = h__;

/*     Try an intercept using rays emanating from points on */
/*     each side of the volume element. */

    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 2; ++j) {
	    m = (j << 1) - 3;
	    cleard_(&c__3, vertex);
	    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex",
		     i__1, "f_zzrytrec__", (ftnlen)226)] = (m << 1) * d__[(
		    i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("d", 
		    i__2, "f_zzrytrec__", (ftnlen)226)];
	    vminus_(vertex, raydir);
	    zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           We expect an intercept. */

	    xnxpts = 1;
	    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)5, (
		    ftnlen)1);

/*           Create the expected intercept point. */

	    cleard_(&c__3, xxpt);
	    xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("xxpt", 
		    i__1, "f_zzrytrec__", (ftnlen)248)] = m * d__[(i__2 = i__ 
		    - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("d", i__2, "f_zzry"
		    "trec__", (ftnlen)248)] / 2;
	    tol = 1e-14;
	    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (
		    ftnlen)3);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Miss case. Zero MARGIN.", (ftnlen)23);
    margin = 0.;

/*     Try an intercept using rays emanating from points on */
/*     each side of the volume element. For each face, try */
/*     rays that miss on all sides. */

    delta = 1e-12;
    for (i__ = 1; i__ <= 3; ++i__) {
	perpco[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzrytrec__", (ftnlen)275)];
	perpco[1] = next[(i__2 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? 
		i__1 : s_rnge("next", i__1, "f_zzrytrec__", (ftnlen)276)] - 1)
		 < 3 && 0 <= i__2 ? i__2 : s_rnge("next", i__2, "f_zzrytrec__"
		, (ftnlen)276)];
	for (j = 1; j <= 2; ++j) {
	    f = (j << 1) - 3;

/*           Loop over the coordinates orthogonal to coordinate I. */

	    for (k = 1; k <= 2; ++k) {
		c1 = perpco[(i__1 = k - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
			"perpco", i__1, "f_zzrytrec__", (ftnlen)287)];
		c2 = perpco[(i__1 = 3 - k - 1) < 2 && 0 <= i__1 ? i__1 : 
			s_rnge("perpco", i__1, "f_zzrytrec__", (ftnlen)288)];
		for (p = 1; p <= 2; ++p) {

/*                 S is the sign of the displacement in the direction of */
/*                 C1. */

		    s = (p << 1) - 3;
		    cleard_(&c__3, vertex);
		    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)300)] = (
			    f << 1) * d__[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)
			    300)];
		    vertex[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)301)] = s 
			    * (d__[(i__2 = c1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)301)] / 
			    2 + delta);
		    vertex[(i__1 = c2 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)302)] = 
			    0.;
		    cleard_(&c__3, raydir);
		    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "raydir", i__1, "f_zzrytrec__", (ftnlen)306)] = (
			    doublereal) (-f);
		    margin = 0.;
		    zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 We expect to find no intercept. */

		    xnxpts = 0;
		    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)
			    5, (ftnlen)1);
		}

/*              End of perpendicular coordinate face loop. */

	    }

/*           End of perpendicular coordinate loop. */

	}

/*        End of primary coordinate face loop. */

    }

/*     End of primary coordinate loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Hit case. Positive MARGIN.", (ftnlen)26);
    margin = 1e-11;

/*     Try an intercept using rays emanating from points on */
/*     each side of the volume element. For each face, try */
/*     rays that hits slightly beyond the edges of the */
/*     unexpanded face, but within the edges of the expanded */
/*     face, on all sides. */

    for (i__ = 1; i__ <= 3; ++i__) {
	perpco[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzrytrec__", (ftnlen)358)];
	perpco[1] = next[(i__2 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? 
		i__1 : s_rnge("next", i__1, "f_zzrytrec__", (ftnlen)359)] - 1)
		 < 3 && 0 <= i__2 ? i__2 : s_rnge("next", i__2, "f_zzrytrec__"
		, (ftnlen)359)];
	for (j = 1; j <= 2; ++j) {
	    f = (j << 1) - 3;

/*           Loop over the coordinates orthogonal to coordinate I. */

	    for (k = 1; k <= 2; ++k) {
		c1 = perpco[(i__1 = k - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
			"perpco", i__1, "f_zzrytrec__", (ftnlen)371)];
		c2 = perpco[(i__1 = 3 - k - 1) < 2 && 0 <= i__1 ? i__1 : 
			s_rnge("perpco", i__1, "f_zzrytrec__", (ftnlen)372)];
		delta = margin / 2 * d__[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? 
			i__1 : s_rnge("d", i__1, "f_zzrytrec__", (ftnlen)374)]
			;
		for (p = 1; p <= 2; ++p) {

/*                 S is the sign of the displacement in the direction of */
/*                 C1. */

		    s = (p << 1) - 3;
		    cleard_(&c__3, vertex);
		    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)386)] = (
			    f << 1) * d__[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)
			    386)];
		    vertex[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)387)] = s 
			    * (d__[(i__2 = c1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)387)] / 
			    2 + delta);
		    vertex[(i__1 = c2 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)388)] = 
			    0.;
		    cleard_(&c__3, raydir);
		    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "raydir", i__1, "f_zzrytrec__", (ftnlen)392)] = (
			    doublereal) (-f);
		    zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 We expect to find an intercept. */

		    xnxpts = 1;
		    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)
			    5, (ftnlen)1);

/*                 Create the expected intercept point. */

		    cleard_(&c__3, xxpt);
		    xxpt[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "xxpt", i__1, "f_zzrytrec__", (ftnlen)411)] = f * 
			    (d__[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)411)] / 
			    2 + margin * d__[(i__3 = i__ - 1) < 3 && 0 <= 
			    i__3 ? i__3 : s_rnge("d", i__3, "f_zzrytrec__", (
			    ftnlen)411)]);
		    xxpt[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "xxpt", i__1, "f_zzrytrec__", (ftnlen)412)] = 
			    vertex[(i__2 = c1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("vertex", i__2, "f_zzrytrec__", (ftnlen)
			    412)];
		    xxpt[(i__1 = c2 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "xxpt", i__1, "f_zzrytrec__", (ftnlen)413)] = 
			    vertex[(i__2 = c2 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("vertex", i__2, "f_zzrytrec__", (ftnlen)
			    413)];
		    tol = 1e-14;
		    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)
			    3, (ftnlen)3);
		}

/*              End of perpendicular coordinate face loop. */

	    }

/*           End of perpendicular coordinate loop. */

	}

/*        End of primary coordinate face loop. */

    }

/*     End of primary coordinate loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Miss case. Positive MARGIN.", (ftnlen)27);
    margin = 1e-11;

/*     Try an intercept using rays emanating from points on */
/*     each side of the volume element. For each face, try */
/*     rays that hits slightly beyond the edges of the */
/*     expanded face on all sides. */

    for (i__ = 1; i__ <= 3; ++i__) {
	perpco[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzrytrec__", (ftnlen)455)];
	perpco[1] = next[(i__2 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? 
		i__1 : s_rnge("next", i__1, "f_zzrytrec__", (ftnlen)456)] - 1)
		 < 3 && 0 <= i__2 ? i__2 : s_rnge("next", i__2, "f_zzrytrec__"
		, (ftnlen)456)];
	for (j = 1; j <= 2; ++j) {
	    f = (j << 1) - 3;

/*           Loop over the coordinates orthogonal to coordinate I. */

	    for (k = 1; k <= 2; ++k) {
		c1 = perpco[(i__1 = k - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
			"perpco", i__1, "f_zzrytrec__", (ftnlen)468)];
		c2 = perpco[(i__1 = 3 - k - 1) < 2 && 0 <= i__1 ? i__1 : 
			s_rnge("perpco", i__1, "f_zzrytrec__", (ftnlen)469)];
		delta = margin * 2 * d__[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? 
			i__1 : s_rnge("d", i__1, "f_zzrytrec__", (ftnlen)471)]
			;
		for (p = 1; p <= 2; ++p) {

/*                 S is the sign of the displacement in the direction of */
/*                 C1. */

		    s = (p << 1) - 3;
		    cleard_(&c__3, vertex);
		    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)483)] = (
			    f << 1) * d__[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? 
			    i__2 : s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)
			    483)];
		    vertex[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)484)] = s 
			    * (d__[(i__2 = c1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)484)] / 
			    2 + delta);
		    vertex[(i__1 = c2 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)485)] = 
			    0.;
		    cleard_(&c__3, raydir);
		    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "raydir", i__1, "f_zzrytrec__", (ftnlen)489)] = (
			    doublereal) (-f);
		    zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 We expect to find no intercept. */

		    xnxpts = 0;
		    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)
			    5, (ftnlen)1);
		}

/*              End of perpendicular coordinate face loop. */

	    }

/*           End of perpendicular coordinate loop. */

	}

/*        End of primary coordinate face loop. */

    }

/*     End of primary coordinate loop. */

/* *********************************************************************** */

/*     Non-error exceptional cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Vertex inside element. Positive MARGIN.", (ftnlen)39);
    margin = 1e-11;

/*     Try an intercept using rays emanating from points on */
/*     each side of the volume element. For each face, try */
/*     rays having vertices slightly beyond unexpanded element */
/*     but within the expanded element. */

    for (i__ = 1; i__ <= 3; ++i__) {
	perpco[0] = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"next", i__1, "f_zzrytrec__", (ftnlen)546)];
	perpco[1] = next[(i__2 = next[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? 
		i__1 : s_rnge("next", i__1, "f_zzrytrec__", (ftnlen)547)] - 1)
		 < 3 && 0 <= i__2 ? i__2 : s_rnge("next", i__2, "f_zzrytrec__"
		, (ftnlen)547)];
	for (j = 1; j <= 2; ++j) {
	    f = (j << 1) - 3;

/*           Loop over the coordinates orthogonal to coordinate I. */

	    for (k = 1; k <= 2; ++k) {
		c1 = perpco[(i__1 = k - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
			"perpco", i__1, "f_zzrytrec__", (ftnlen)559)];
		c2 = perpco[(i__1 = 3 - k - 1) < 2 && 0 <= i__1 ? i__1 : 
			s_rnge("perpco", i__1, "f_zzrytrec__", (ftnlen)560)];

/*              The margin used by ZZRYTREC for this test is twice the */
/*              input margin. Use a large delta to verify this. */

		delta = margin * 1.999 * d__[(i__1 = c1 - 1) < 3 && 0 <= i__1 
			? i__1 : s_rnge("d", i__1, "f_zzrytrec__", (ftnlen)
			566)];
		for (p = 1; p <= 2; ++p) {

/*                 S is the sign of the displacement in the direction of */
/*                 C1. */

		    s = (p << 1) - 3;
		    cleard_(&c__3, vertex);
		    vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)578)] = 
			    d__[(i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)578)] / 
			    2 * delta;
		    vertex[(i__1 = c1 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)579)] = s 
			    * (d__[(i__2 = c1 - 1) < 3 && 0 <= i__2 ? i__2 : 
			    s_rnge("d", i__2, "f_zzrytrec__", (ftnlen)579)] / 
			    2 + delta);
		    vertex[(i__1 = c2 - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "vertex", i__1, "f_zzrytrec__", (ftnlen)580)] = 
			    0.;
		    cleard_(&c__3, raydir);
		    raydir[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			    "raydir", i__1, "f_zzrytrec__", (ftnlen)584)] = (
			    doublereal) (-f);
		    zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
		    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*                 We expect to find an intercept. */

		    xnxpts = 1;
		    chcksi_("NXPTS", &nxpts, "=", &xnxpts, &c__0, ok, (ftnlen)
			    5, (ftnlen)1);

/*                 The expected intercept point is the vertex itself. */

		    vequ_(vertex, xxpt);
		    chckad_("XPT", xpt, "=", xxpt, &c__3, &c_b169, ok, (
			    ftnlen)3, (ftnlen)1);
		}

/*              End of perpendicular coordinate face loop. */

	    }

/*           End of perpendicular coordinate loop. */

	}

/*        End of primary coordinate face loop. */

    }

/*     End of primary coordinate loop. */

/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Bounds out of order.", (ftnlen)20);
    margin = 0.;
    bounds[0] = -l / 2;
    bounds[1] = l / 2;
    bounds[2] = -w / 2;
    bounds[3] = w / 2;
    bounds[4] = -h__ / 2;
    bounds[5] = h__ / 2;
    for (i__ = 1; i__ <= 3; ++i__) {
	swapd_(&bounds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("bounds", i__1, "f_zzrytrec__", (ftnlen)646)], &bounds[
		(i__2 = (i__ << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		"bounds", i__2, "f_zzrytrec__", (ftnlen)646)]);
	cleard_(&c__3, vertex);
	vertex[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("vertex", 
		i__1, "f_zzrytrec__", (ftnlen)649)] = d__[(i__2 = i__ - 1) < 
		3 && 0 <= i__2 ? i__2 : s_rnge("d", i__2, "f_zzrytrec__", (
		ftnlen)649)] * 2;
	vminus_(vertex, raydir);
	zzrytrec_(vertex, raydir, bounds, &margin, &nxpts, xpt);
	chckxc_(&c_true, "SPICE(BADCOORDBOUNDS)", ok, (ftnlen)21);

/*        Restore bounds. */

	swapd_(&bounds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : 
		s_rnge("bounds", i__1, "f_zzrytrec__", (ftnlen)660)], &bounds[
		(i__2 = (i__ << 1) - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		"bounds", i__2, "f_zzrytrec__", (ftnlen)660)]);
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzrytrec__ */

