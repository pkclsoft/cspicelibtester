/* f_zzsegbox.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10 = 10;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__8 = 8;
static integer c__1 = 1;

/* $Procedure F_ZZSEGBOX ( ZZSEGBOX tests ) */
/* Subroutine */ int f_zzsegbox__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal xrad;
    static integer nlat;
    static doublereal last;
    static char dsks[255*3];
    static integer nlon;
    static doublereal minv[3], maxv[3], xctr[3];
    extern /* Subroutine */ int zzrecbox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), 
	    zzsegbox_(doublereal *, doublereal *, doublereal *), zzlatbox_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal a, b, c__;
    extern /* Subroutine */ int zzpdtbox_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal f;
    static integer i__, j, n;
    extern /* Subroutine */ int dskgd_(integer *, integer *, doublereal *), 
	    tcase_(char *, ftnlen);
    extern doublereal dpmin_(void), dpmax_(void), jyear_(void);
    static logical found;
    extern /* Subroutine */ int dskv02_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *), dskz02_(integer *, integer *,
	     integer *, integer *), topen_(char *, ftnlen);
    static doublereal first;
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    extern doublereal pi_(void);
    static integer dladsc[8], handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    static doublereal lr;
    static integer np;
    static doublereal lt;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chckxc_(logical *, 
	    char *, logical *, ftnlen);
    static integer nv;
    extern /* Subroutine */ int dlabfs_(integer *, integer *, logical *);
    static doublereal lz;
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static char frname[32];
    extern /* Subroutine */ int dascls_(integer *);
    static doublereal dskdsc[24];
    static integer bodyid;
    static doublereal bounds[6]	/* was [2][3] */, corpar[10], radius, boxctr[
	    3];
    static integer surfid;
    static doublereal vertex[3];
    static logical makvtl;
    extern logical exists_(char *, ftnlen);
    static integer corsys;
    static logical usepad;
    extern /* Subroutine */ int dasopr_(char *, integer *, ftnlen), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen), kclear_(void);
    static doublereal tol;
    extern /* Subroutine */ int t_secds2__(integer *, integer *, char *, 
	    doublereal *, doublereal *, integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    logical *, logical *, char *, ftnlen, ftnlen);

/* $ Abstract */

/*     Exercise the SPICELIB routine ZZSEGBOX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     Include file dla.inc */

/*     This include file declares parameters for DLA format */
/*     version zero. */

/*        Version 3.0.1 17-OCT-2016 (NJB) */

/*           Corrected comment: VERIDX is now described as a DAS */
/*           integer address rather than a d.p. address. */

/*        Version 3.0.0 20-JUN-2006 (NJB) */

/*           Changed name of parameter DSCSIZ to DLADSZ. */

/*        Version 2.0.0 09-FEB-2005 (NJB) */

/*           Changed descriptor layout to make backward pointer */
/*           first element.  Updated DLA format version code to 1. */

/*           Added parameters for format version and number of bytes per */
/*           DAS comment record. */

/*        Version 1.0.0 28-JAN-2004 (NJB) */


/*     DAS integer address of DLA version code. */


/*     Linked list parameters */

/*     Logical arrays (aka "segments") in a DAS linked array (DLA) file */
/*     are organized as a doubly linked list.  Each logical array may */
/*     actually consist of character, double precision, and integer */
/*     components.  A component of a given data type occupies a */
/*     contiguous range of DAS addresses of that type.  Any or all */
/*     array components may be empty. */

/*     The segment descriptors in a SPICE DLA (DAS linked array) file */
/*     are connected by a doubly linked list.  Each node of the list is */
/*     represented by a pair of integers acting as forward and backward */
/*     pointers.  Each pointer pair occupies the first two integers of a */
/*     segment descriptor in DAS integer address space.  The DLA file */
/*     contains pointers to the first integers of both the first and */
/*     last segment descriptors. */

/*     At the DLA level of a file format implementation, there is */
/*     no knowledge of the data contents.  Hence segment descriptors */
/*     provide information only about file layout (in contrast with */
/*     the DAF system).  Metadata giving specifics of segment contents */
/*     are stored within the segments themselves in DLA-based file */
/*     formats. */


/*     Parameter declarations follow. */

/*     DAS integer addresses of first and last segment linked list */
/*     pointer pairs.  The contents of these pointers */
/*     are the DAS addresses of the first integers belonging */
/*     to the first and last link pairs, respectively. */

/*     The acronyms "LLB" and "LLE" denote "linked list begin" */
/*     and "linked list end" respectively. */


/*     Null pointer parameter. */


/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS integer addresses. */

/*        The segment descriptor layout is: */

/*           +---------------+ */
/*           | BACKWARD PTR  | Linked list backward pointer */
/*           +---------------+ */
/*           | FORWARD PTR   | Linked list forward pointer */
/*           +---------------+ */
/*           | BASE INT ADDR | Base DAS integer address */
/*           +---------------+ */
/*           | INT COMP SIZE | Size of integer segment component */
/*           +---------------+ */
/*           | BASE DP ADDR  | Base DAS d.p. address */
/*           +---------------+ */
/*           | DP COMP SIZE  | Size of d.p. segment component */
/*           +---------------+ */
/*           | BASE CHR ADDR | Base DAS character address */
/*           +---------------+ */
/*           | CHR COMP SIZE | Size of character segment component */
/*           +---------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Descriptor size: */


/*     Other DLA parameters: */


/*     DLA format version.  (This number is expected to occur very */
/*     rarely at integer address VERIDX in uninitialized DLA files.) */


/*     Characters per DAS comment record. */


/*     End of include file dla.inc */


/*     Include file dskdsc.inc */

/*     This include file declares parameters for DSK segment descriptors. */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*           Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Added parameter for data class 2. Changed name of data */
/*              class 1 parameter. Corrected data class descriptions. */

/*           13-MAY-2010 (NJB) */

/*              Descriptor now contains two ID codes, one for the */
/*              surface, one for the associated ephemeris object. This */
/*              supports association of multiple surfaces with one */
/*              ephemeris object without creating file management */
/*              issues. */

/*              Room was added for coordinate system definition */
/*              parameters. */

/*               Flag arrays and model ID/component entries were deleted. */

/*            11-SEP-2008 (NJB) */


/*     DSK segment descriptors are implemented as an array of d.p. */
/*     numbers.  Note that each integer descriptor datum occupies one */
/*     d.p. value. */




/*     Segment descriptor parameters */

/*     Each segment descriptor occupies a contiguous */
/*     range of DAS d.p. addresses. */

/*        The DSK segment descriptor layout is: */

/*           +---------------------+ */
/*           | Surface ID code     | */
/*           +---------------------+ */
/*           | Center ID code      | */
/*           +---------------------+ */
/*           | Data class code     | */
/*           +---------------------+ */
/*           | Data type           | */
/*           +---------------------+ */
/*           | Ref frame code      | */
/*           +---------------------+ */
/*           | Coord sys code      | */
/*           +---------------------+ */
/*           | Coord sys parameters|  {10 elements} */
/*           +---------------------+ */
/*           | Min coord 1         | */
/*           +---------------------+ */
/*           | Max coord 1         | */
/*           +---------------------+ */
/*           | Min coord 2         | */
/*           +---------------------+ */
/*           | Max coord 2         | */
/*           +---------------------+ */
/*           | Min coord 3         | */
/*           +---------------------+ */
/*           | Max coord 3         | */
/*           +---------------------+ */
/*           | Start time          | */
/*           +---------------------+ */
/*           | Stop time           | */
/*           +---------------------+ */

/*     Parameters defining offsets for segment descriptor elements */
/*     follow. */


/*     Surface ID code: */


/*     Central ephemeris object NAIF ID: */


/*     Data class: */

/*     The "data class" is a code indicating the category of */
/*     data contained in the segment. */


/*     Data type: */


/*     Frame ID: */


/*     Coordinate system code: */


/*     Coordinate system parameter start index: */


/*     Number of coordinate system parameters: */


/*     Ranges for coordinate bounds: */


/*     Coverage time bounds: */


/*     Descriptor size (24): */


/*     Data class values: */

/*        Class 1 indicates a surface that can be represented as a */
/*                single-valued function of its domain coordinates. */

/*                An example is a surface defined by a function that */
/*                maps each planetodetic longitude and latitude pair to */
/*                a unique altitude. */


/*        Class 2 indicates a general surface. Surfaces that */
/*                have multiple points for a given pair of domain */
/*                coordinates---for example, multiple radii for a given */
/*                latitude and longitude---belong to class 2. */



/*     Coordinate system values: */

/*        The coordinate system code indicates the system to which the */
/*        tangential coordinate bounds belong. */

/*        Code 1 refers to the planetocentric latitudinal system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is latitude. The third */
/*        coordinate is radius. */



/*        Code 2 refers to the cylindrical system. */

/*        In this system, the first tangential coordinate is radius and */
/*        the second tangential coordinate is longitude. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 3 refers to the rectangular system. */

/*        In this system, the first tangential coordinate is X and */
/*        the second tangential coordinate is Y. The third, */
/*        orthogonal coordinate is Z. */



/*        Code 4 refers to the planetodetic/geodetic system. */

/*        In this system, the first tangential coordinate is longitude */
/*        and the second tangential coordinate is planetodetic */
/*        latitude. The third, orthogonal coordinate is altitude. */



/*     End of include file dskdsc.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZSEGBOX. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-FEB-2017 (NJB) */

/*        11-JUL-2016 (NJB) */

/*          Original version. */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZSEGBOX", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Check bounding sphere for segment using the Latitudinal coordina"
	    "te system.", (ftnlen)74);

/*     Strictly speaking, we don't need an actual DSK file; we just */
/*     need a DSK descriptor. However, we'll create a file in order */
/*     to work with a realistic descriptor. */

    bodyid = 499;
    surfid = 1;
    s_copy(frname, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    first = jyear_() * -100;
    last = jyear_() * 100;
    corsys = 1;
    a = 3400.;
    b = a;
    c__ = 3375.;
    cleard_(&c__10, corpar);

/*     Longitude bounds: */

    bounds[0] = pi_() / 6;
    bounds[1] = pi_() / 3;

/*     Latitude bounds: */

    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;

/*     Radius bounds are not used by T_SECDS2. */

    bounds[4] = 0.;
    bounds[5] = 0.;
    nlon = 100;
    nlat = 50;
    makvtl = FALSE_;
    usepad = TRUE_;
    s_copy(dsks, "zzsegbox_lat.bds", (ftnlen)255, (ftnlen)16);
    if (exists_(dsks, (ftnlen)255)) {
	delfil_(dsks, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create the DSK file. */

    t_secds2__(&bodyid, &surfid, frname, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, &usepad, dsks, (
	    ftnlen)32, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the DSK descriptor for the segment we just created. */

    dasopr_(dsks, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA segment found", &found, &c_true, ok, (ftnlen)17);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the segment's radius bounds from the DSK descriptor. */

    bounds[4] = dskdsc[20];
    bounds[5] = dskdsc[21];

/*     Create a bounding box and sphere for the segment using both */
/*     ZZSEGBOX and ZZLATBOX. The results should match. */

    zzsegbox_(dskdsc, boxctr, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzlatbox_(bounds, xctr, &lr, &lt, &lz, &xrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("RADIUS", &radius, "~/", &xrad, &tol, ok, (ftnlen)6, (ftnlen)2);
    chckad_("BOXCTR", boxctr, "~~/", xctr, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check bounding sphere for segment using the Planetodetic coordin"
	    "ate system.", (ftnlen)75);

/*     Strictly speaking, we don't need an actual DSK file; we just */
/*     need a DSK descriptor. However, we'll create a file in order */
/*     to work with a realistic descriptor. */

    bodyid = 599;
    surfid = 1;
    s_copy(frname, "IAU_JUPITER", (ftnlen)32, (ftnlen)11);
    first = jyear_() * -100;
    last = jyear_() * 100;
    corsys = 4;
    a = 71492.;
    b = a;
    c__ = 66854.;
    f = (a - c__) / a;
    corpar[0] = a;
    corpar[1] = f;
    cleard_(&c__8, &corpar[2]);

/*     Longitude bounds: */

    bounds[0] = pi_() / 6;
    bounds[1] = pi_() / 3;

/*     Latitude bounds: */

    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;

/*     Altitude bounds are not used by T_SECDS2. */

    bounds[4] = 0.;
    bounds[5] = 0.;
    nlon = 100;
    nlat = 50;
    makvtl = FALSE_;
    usepad = FALSE_;
    s_copy(dsks + 255, "zzsegbox_pdt.bds", (ftnlen)255, (ftnlen)16);
    if (exists_(dsks + 255, (ftnlen)255)) {
	delfil_(dsks + 255, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create the DSK file. */

    t_secds2__(&bodyid, &surfid, frname, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, &usepad, dsks + 255, 
	    (ftnlen)32, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the DSK descriptor for the segment we just created. */

    dasopr_(dsks + 255, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA segment found", &found, &c_true, ok, (ftnlen)17);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the segment's altitude bounds from the DSK descriptor. */

    bounds[4] = dskdsc[20];
    bounds[5] = dskdsc[21];

/*     Create a bounding box and sphere for the segment using both */
/*     ZZSEGBOX and ZZLATBOX. The results should match. */

    zzsegbox_(dskdsc, boxctr, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzpdtbox_(bounds, corpar, xctr, &lr, &lt, &lz, &xrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("RADIUS", &radius, "~/", &xrad, &tol, ok, (ftnlen)6, (ftnlen)2);
    chckad_("BOXCTR", boxctr, "~~/", xctr, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check bounding sphere for segment using the Rectangular coordina"
	    "te system.", (ftnlen)74);

/*     Strictly speaking, we don't need an actual DSK file; we just */
/*     need a DSK descriptor. However, we'll create a file in order */
/*     to work with a realistic descriptor. */

/*     We're going to start out as though we're using the latitudinal */
/*     system, since the plate set generation utilities work with a */
/*     lon/lat coverage specification. */

    bodyid = 699;
    surfid = 1;
    s_copy(frname, "IAU_SATURN", (ftnlen)32, (ftnlen)10);
    first = jyear_() * -100;
    last = jyear_() * 100;
    corsys = 1;
    a = 60268.;
    b = a;
    c__ = 54364.;
    cleard_(&c__10, corpar);

/*     Longitude bounds: */

    bounds[0] = pi_() / 6;
    bounds[1] = pi_() / 3;

/*     Latitude bounds: */

    bounds[2] = -pi_() / 3;
    bounds[3] = -pi_() / 4;

/*     Radius bounds are not used by T_SECDS2. */

    bounds[4] = 0.;
    bounds[5] = 0.;
    nlon = 100;
    nlat = 50;
    makvtl = FALSE_;
    usepad = TRUE_;
    s_copy(dsks + 510, "zzsegbox_rec.bds", (ftnlen)255, (ftnlen)16);
    if (exists_(dsks + 510, (ftnlen)255)) {
	delfil_(dsks + 510, (ftnlen)255);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Create the DSK file. */

    t_secds2__(&bodyid, &surfid, frname, &first, &last, &corsys, corpar, 
	    bounds, &a, &b, &c__, &nlon, &nlat, &makvtl, &usepad, dsks, (
	    ftnlen)32, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Fetch the DSK descriptor for the segment we just created. */

    dasopr_(dsks, &handle, (ftnlen)255);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    dlabfs_(&handle, dladsc, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DLA segment found", &found, &c_true, ok, (ftnlen)17);
    dskgd_(&handle, dladsc, dskdsc);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Here's where we deviate from the latitudinal case. We're going */
/*     to convert the descriptor to one using rectangular coordinates. */

/*     Before closing the file, fetch the vertices of the file */
/*     and obtain the extents of the plate set. */

    dskz02_(&handle, dladsc, &nv, &np);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Let MINV(I) and MAXV(I) store the minimum and maximum values of */
/*     the Ith components of the vertices. */

    for (i__ = 1; i__ <= 3; ++i__) {
	minv[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("minv", i__1, 
		"f_zzsegbox__", (ftnlen)535)] = dpmax_();
	maxv[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("maxv", i__1, 
		"f_zzsegbox__", (ftnlen)536)] = dpmin_();
    }
    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dskv02_(&handle, dladsc, &i__, &c__1, &n, vertex);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	for (j = 1; j <= 3; ++j) {
/* Computing MIN */
	    d__1 = vertex[(i__3 = j - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		    "vertex", i__3, "f_zzsegbox__", (ftnlen)547)], d__2 = 
		    minv[(i__4 = j - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		    "minv", i__4, "f_zzsegbox__", (ftnlen)547)];
	    minv[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("minv", i__2,
		     "f_zzsegbox__", (ftnlen)547)] = min(d__1,d__2);
/* Computing MAX */
	    d__1 = vertex[(i__3 = j - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
		    "vertex", i__3, "f_zzsegbox__", (ftnlen)548)], d__2 = 
		    maxv[(i__4 = j - 1) < 3 && 0 <= i__4 ? i__4 : s_rnge(
		    "maxv", i__4, "f_zzsegbox__", (ftnlen)548)];
	    maxv[(i__2 = j - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("maxv", i__2,
		     "f_zzsegbox__", (ftnlen)548)] = max(d__1,d__2);
	}
    }
    dascls_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Turn the descriptor into one for rectangular coordinates. */

    dskdsc[5] = 3.;
    dskdsc[16] = minv[0];
    dskdsc[17] = maxv[0];
    dskdsc[18] = minv[1];
    dskdsc[19] = maxv[1];
    dskdsc[20] = minv[2];
    dskdsc[21] = maxv[2];

/*     Create a bounding box and sphere for the segment using both */
/*     ZZSEGBOX and ZZLATBOX. The results should match. */

    zzsegbox_(dskdsc, boxctr, &radius);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	bounds[(i__1 = (i__ << 1) - 2) < 6 && 0 <= i__1 ? i__1 : s_rnge("bou"
		"nds", i__1, "f_zzsegbox__", (ftnlen)578)] = minv[(i__2 = i__ 
		- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("minv", i__2, "f_zzseg"
		"box__", (ftnlen)578)];
	bounds[(i__1 = (i__ << 1) - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("bou"
		"nds", i__1, "f_zzsegbox__", (ftnlen)579)] = maxv[(i__2 = i__ 
		- 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("maxv", i__2, "f_zzseg"
		"box__", (ftnlen)579)];
    }
    zzrecbox_(bounds, xctr, &lr, &lt, &lz, &xrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("RADIUS", &radius, "~/", &xrad, &tol, ok, (ftnlen)6, (ftnlen)2);
    chckad_("BOXCTR", boxctr, "~~/", xctr, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);
/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --------------------------------------------------------- */

    tcase_("Error: try segment using cylindrical coordinates.", (ftnlen)49);

/*     Use descriptor from last case; change the coordinate system */
/*     parameter to indicate cylindrical coordinates. */

    dskdsc[5] = 2.;
    zzsegbox_(dskdsc, boxctr, &radius);
    chckxc_(&c_true, "SPICE(NOTSUPPORTED)", ok, (ftnlen)19);

/*     Clean up. */


/* --------------------------------------------------------- */

    tcase_("Clean up", (ftnlen)8);
    kclear_();
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (i__ = 1; i__ <= 3; ++i__) {
	if (exists_(dsks + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		"dsks", i__1, "f_zzsegbox__", (ftnlen)630)) * 255, (ftnlen)
		255)) {
	    delfil_(dsks + ((i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "dsks", i__1, "f_zzsegbox__", (ftnlen)632)) * 255, (
		    ftnlen)255);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
    }

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzsegbox__ */

