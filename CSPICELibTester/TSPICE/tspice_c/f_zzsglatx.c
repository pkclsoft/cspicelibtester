/* f_zzsglatx.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 1.;
static logical c_false = FALSE_;
static doublereal c_b11 = 0.;
static integer c__3 = 3;
static doublereal c_b16 = 1e-14;
static doublereal c_b80 = 3.;
static doublereal c_b84 = .5;
static doublereal c_b127 = -2.;
static doublereal c_b149 = 2.;
static doublereal c_b157 = 1e-12;
static doublereal c_b160 = -1e-14;
static doublereal c_b301 = -1.;

/* $Procedure      F_ZZSGLATX ( Test segment latitude extent routine ) */
/* Subroutine */ int f_zzsglatx__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double sqrt(doublereal), asin(doublereal);

    /* Local variables */
    doublereal diff[3], dist, minp[3], maxp[3];
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), zzsglatx_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *), vlcom_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), topen_(
	    char *, ftnlen);
    doublereal xminp[3], xmaxp[3];
    extern /* Subroutine */ int unorm_(doublereal *, doublereal *, doublereal 
	    *);
    doublereal p1[3], p2[3];
    extern /* Subroutine */ int t_success__(logical *), chckad_(char *, 
	    doublereal *, char *, doublereal *, integer *, doublereal *, 
	    logical *, ftnlen, ftnlen);
    doublereal dp;
    extern doublereal pi_(void);
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    doublereal minlat, maxlat, xminlt;
    extern /* Subroutine */ int nplnpt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    doublereal xmaxlt;
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *);
    doublereal sq2, mag, dir[3], sq22;

/* $ Abstract */

/*     Test the SPICELIB line segment latitude extent computation */
/*     routine ZZSGLATX. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the routine ZZSGLATX. */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 31-AUG-2016 (NJB) */

/*        19-MAY-2013 (NJB) */

/*           Bugs in calls to CHCKAD were corrected. Errors in cases 4 */
/*           and 6 were corrected. */

/*        04-DEC-2012 (NJB) */

/*           Original version. */

/* -& */
/* $ Revisions */

/*        19-MAY-2013 (NJB) */

/*           Bugs in calls to CHCKAD were corrected. Errors in cases 4 */
/*           and 6 were corrected. */

/*           There was an error in the title of case 4. In case 4, there */
/*           was a single precision constant used as an input argument */
/*           to VLCOM. */

/*           In case 6, XMAXP was not initialized correctly. */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */

/*      INTEGER               LNSIZE */
/*      PARAMETER           ( LNSIZE = 80 ) */

/*     Local Variables */

/*      CHARACTER*(LNSIZE)    CASNAM */
/*      INTEGER               I */
/*      INTEGER               N */

/*     Begin every test family with an open call. */

    topen_("F_ZZSGLATX", (ftnlen)10);

/* --- Case -------------------------------------------------------- */

    tcase_("Tangent case: segment parallel to X-Y plane, Z>0", (ftnlen)48);
    sq2 = sqrt(2.);
    sq22 = sq2 / 2.;
    d__1 = -sq2;
    vpack_(&c_b4, &d__1, &c_b4, p1);
    vpack_(&c_b4, &sq2, &c_b4, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at the segment midpoint. */

    vpack_(&c_b4, &c_b11, &c_b4, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude occurs at an endpoint, but which one is */
/*     not specified. We use the sign of the Y component of MINP */
/*     to indicate which endpoint was selected. */

    vequ_(p1, xminp);
    if (minp[1] > 0.) {
	xminp[1] = -xminp[1];
    }
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/2, so the latitude is 30 deg. */

    xminlt = pi_() / 6;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Tangent case: segment parallel to X-Y plane, Z>0, segment max Y "
	    "is at X-Z plane.", (ftnlen)80);
    d__1 = -sq2;
    vpack_(&c_b4, &d__1, &c_b4, p1);
    vpack_(&c_b4, &c_b11, &c_b4, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at the segment midpoint. */

    vpack_(&c_b4, &c_b11, &c_b4, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude occurs at the endpoint where Y is */
/*     minimized. */

    vequ_(p1, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/2, so the latitude is 30 deg. */

    xminlt = pi_() / 6;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Tangent case: segment parallel to X-Y plane, Z>0, segment min Y "
	    "is at X-Z plane.", (ftnlen)80);
    vpack_(&c_b4, &c_b11, &c_b4, p1);
    vpack_(&c_b4, &sq2, &c_b4, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at the segment starting */
/*     point. */

    vpack_(&c_b4, &c_b11, &c_b4, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude occurs at the endpoint where Y is */
/*     maximized. */

    vequ_(p2, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/2, so the latitude is 30 deg. */

    xminlt = pi_() / 6;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */


/*     Adding multiples of the tangent point to the line segment */
/*     generates new segments in the same tangent plane. Try such */
/*     a case. */

    tcase_("Tangent case: segment non parallel to X-Y plane, Z>0, segment mi"
	    "n Y is negative.", (ftnlen)80);
    d__1 = -sq2;
    vpack_(&c_b4, &d__1, &c_b4, p1);
    vpack_(&c_b80, &sq2, &c_b80, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at the segment midpoint. */

    vlcom_(&c_b84, p1, &c_b84, p2, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude will occur at the endpoint at which Y */
/*     is minimum. */

    vequ_(p1, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/2, so the latitude is 30 deg. */

    xminlt = pi_() / 6;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Tangent case: segment parallel to X-Y plane, Z>0, segment max Y "
	    "is at X-Z plane.", (ftnlen)80);
    d__1 = -sq2;
    vpack_(&c_b4, &d__1, &c_b4, p1);
    vpack_(&c_b4, &c_b11, &c_b4, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at the segment midpoint. */

    vpack_(&c_b4, &c_b11, &c_b4, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude occurs at the endpoint where Y is */
/*     minimized. */

    vequ_(p1, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/2, so the latitude is 30 deg. */

    xminlt = pi_() / 6;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Tangent *miss* case: segment parallel to X-Y plane, Z>0, segment"
	    " max Y < 0.", (ftnlen)75);
    vpack_(&c_b4, &c_b127, &c_b4, p1);
    d__1 = -sq2;
    vpack_(&c_b4, &d__1, &c_b4, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude should occur at P2. */

    vequ_(p2, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);
    xmaxlt = pi_() / 6;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     The minimum latitude occurs at the endpoint where Y is */
/*     minimized. */

    vequ_(p1, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude has sine = 1/sqrt(6). */

    xminlt = asin(1. / sqrt(6.));
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("P2 = 2 * P1", (ftnlen)11);
    d__1 = -sq22;
    vpack_(&sq22, &d__1, &c_b4, p1);
    vscl_(&c_b149, p1, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found somewhere on the */
/*     line segment. Find the nearest point to MAXP on the line */
/*     containing the segment; make sure this point is actually */
/*     on the segment. */

    vsub_(p2, p1, diff);
    unorm_(diff, dir, &mag);
    nplnpt_(p1, dir, maxp, xmaxp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(maxp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     The same conditions apply to MINP. */

    nplnpt_(p1, dir, minp, xminp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(minp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     Latitude is pi/4 all along this segment. */

    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    xminlt = pi_() / 4;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("P2 = - P1", (ftnlen)9);
    d__1 = -sq22;
    vpack_(&sq22, &d__1, &c_b4, p1);
    vminus_(p1, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found somewhere on the */
/*     line segment. Find the nearest point to MAXP on the line */
/*     containing the segment; make sure this point is actually */
/*     on the segment. */

    vsub_(p2, p1, diff);
    unorm_(diff, dir, &mag);
    nplnpt_(p1, dir, maxp, xmaxp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(maxp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     The same conditions apply to MINP. */

    nplnpt_(p1, dir, minp, xminp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(minp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     Latitude is pi/4 on portion of this segment above the plane Z=0, */
/*     -pi/4 below it. */

    xmaxlt = pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    xminlt = -pi_() / 4;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Segment is in X-Y plane.", (ftnlen)24);
    d__1 = -sq22;
    vpack_(&sq22, &d__1, &c_b11, p1);
    vpack_(&sq22, &sq22, &c_b11, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found somewhere on the */
/*     line segment. Find the nearest point to MAXP on the line */
/*     containing the segment; make sure this point is actually */
/*     on the segment. */

    vsub_(p2, p1, diff);
    unorm_(diff, dir, &mag);
    nplnpt_(p1, dir, maxp, xmaxp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(maxp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     The same conditions apply to MINP. */

    nplnpt_(p1, dir, minp, xminp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(minp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     Latitude is 0 on the whole segment. */

    xmaxlt = 0.;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    xminlt = 0.;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Segment is contained in +Z axis", (ftnlen)31);
    vpack_(&c_b11, &c_b11, &c_b4, p2);
    vpack_(&c_b11, &c_b11, &c_b149, p1);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found somewhere on the */
/*     line segment. Find the nearest point to MAXP on the line */
/*     containing the segment; make sure this point is actually */
/*     on the segment. */

    vsub_(p2, p1, diff);
    unorm_(diff, dir, &mag);
    nplnpt_(p1, dir, maxp, xmaxp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(maxp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     The same conditions apply to MINP. */

    nplnpt_(p1, dir, minp, xminp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(minp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     Latitude is pi/2 on the whole segment. */

    xmaxlt = pi_() / 2;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    xminlt = pi_() / 2;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Segment is contained in -Z axis", (ftnlen)31);
    vpack_(&c_b11, &c_b11, &c_b301, p1);
    vpack_(&c_b11, &c_b11, &c_b127, p2);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found somewhere on the */
/*     line segment. Find the nearest point to MAXP on the line */
/*     containing the segment; make sure this point is actually */
/*     on the segment. */

    vsub_(p2, p1, diff);
    unorm_(diff, dir, &mag);
    nplnpt_(p1, dir, maxp, xmaxp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(maxp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     The same conditions apply to MINP. */

    nplnpt_(p1, dir, minp, xminp, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksd_("DIST", &dist, "~", &c_b11, &c_b157, ok, (ftnlen)4, (ftnlen)1);
    vsub_(minp, p1, diff);
    dp = vdot_(diff, dir);
    chcksd_("DP", &dp, ">", &c_b160, &c_b11, ok, (ftnlen)2, (ftnlen)1);
    d__1 = mag + 1e-14;
    chcksd_("DP", &dp, "<", &d__1, &c_b11, ok, (ftnlen)2, (ftnlen)1);

/*     Latitude is -pi/2 on the whole segment. */

    xmaxlt = -pi_() / 2;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    xminlt = -pi_() / 2;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Segment intersects but is not contained in +Z axis", (ftnlen)50);
    vpack_(&c_b4, &c_b11, &c_b4, p2);
    vpack_(&c_b301, &c_b11, &c_b4, p1);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The maximum latitude will be found on the Z axis. */

    vpack_(&c_b11, &c_b11, &c_b4, xmaxp);
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The minimum latitude will be found at one of the */
/*     segment's endpoints. */

    vequ_(p1, xminp);
    if (minp[0] > 0.) {
	xminp[0] = -xminp[0];
    }
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     Latitude is pi/2 at the maximum. */

    xmaxlt = pi_() / 2;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     Latitude is pi/4 at the minimum. */

    xminlt = pi_() / 4;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/* --- Case -------------------------------------------------------- */

    tcase_("Segment intersects but is not contained in -Z axis", (ftnlen)50);
    vpack_(&c_b4, &c_b11, &c_b301, p2);
    vpack_(&c_b301, &c_b11, &c_b301, p1);
    zzsglatx_(p1, p2, &minlat, minp, &maxlat, maxp);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The minimum latitude will be found on the Z axis. */

    vpack_(&c_b11, &c_b11, &c_b301, xminp);
    chckad_("MINP", minp, "~~/", xminp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     The maximum latitude will be found at one of the */
/*     segment's endpoints. */

    vequ_(p1, xmaxp);
    if (maxp[0] > 0.) {
	xmaxp[0] = -xmaxp[0];
    }
    chckad_("MAXP", maxp, "~~/", xmaxp, &c__3, &c_b16, ok, (ftnlen)4, (ftnlen)
	    3);

/*     Latitude is -pi/2 at the minimum. */

    xminlt = -pi_() / 2;
    chcksd_("MINLAT", &minlat, "~", &xminlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;

/*     Latitude is -pi/4 at the maximum. */

    xmaxlt = -pi_() / 4;
    chcksd_("MAXLAT", &maxlat, "~", &xmaxlt, &c_b16, ok, (ftnlen)6, (ftnlen)1)
	    ;
    t_success__(ok);
    return 0;
} /* f_zzsglatx__ */

