/* f_zzsinutl.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static doublereal c_b33 = .5;
static doublereal c_b146 = -5e3;
static doublereal c_b147 = 6e3;
static doublereal c_b148 = 1e4;
static integer c__1 = 1;
static doublereal c_b265 = 5e3;
static doublereal c_b266 = -6e3;
static doublereal c_b267 = 0.;

/* $Procedure F_ZZSINUTL ( ZZSINUTL tests ) */
/* Subroutine */ int f_zzsinutl__(logical *ok)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);
    static integer nlat;
    static doublereal dist;
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal xmin, xmax;
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *);
    static doublereal xxpt[3];
    extern /* Subroutine */ int zzminrad_(doublereal *), zzmaxrad_(doublereal 
	    *), zzdsksph_(integer *, integer *, integer *, doublereal *, 
	    doublereal *), zzsuelin_(integer *), zzsudski_(integer *, integer 
	    *, integer *, integer *);
    static integer n;
    extern /* Subroutine */ int zzraysfx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *), zzsinutl_(integer *, 
	    integer *, integer *, doublereal *, integer *, doublereal *, 
	    doublereal *, doublereal *, logical *, doublereal *, doublereal *,
	     doublereal *, doublereal *);
    static doublereal radii[3];
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal pnear[3];
    extern doublereal jyear_(void);
    static logical found;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal xdist;
    static integer nsurf;
    extern /* Subroutine */ int dskxv_(logical *, char *, integer *, integer *
	    , doublereal *, char *, integer *, doublereal *, doublereal *, 
	    doublereal *, logical *, ftnlen, ftnlen), t_success__(logical *), 
	    chckad_(char *, doublereal *, char *, doublereal *, integer *, 
	    doublereal *, logical *, ftnlen, ftnlen);
    static doublereal et;
    extern /* Subroutine */ int delfil_(char *, ftnlen), chcksd_(char *, 
	    doublereal *, char *, doublereal *, doublereal *, logical *, 
	    ftnlen, ftnlen), bodvcd_(integer *, char *, integer *, integer *, 
	    doublereal *, ftnlen), chckxc_(logical *, char *, logical *, 
	    ftnlen), chcksl_(char *, logical *, logical *, logical *, ftnlen),
	     t_pck08__(char *, logical *, logical *, ftnlen);
    static integer fixfid;
    static doublereal minrad, maxrad;
    static integer bodyid;
    static doublereal xradii[3];
    static char kvname[32];
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen);
    static char fixref[32];
    extern /* Subroutine */ int npedln_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), unload_(char *, ftnlen);
    static char target[32];
    static doublereal raydir[3];
    static integer surfid;
    static doublereal xpnear[3];
    extern /* Subroutine */ int pdpool_(char *, integer *, doublereal *, 
	    ftnlen), furnsh_(char *, ftnlen);
    static doublereal spoint[3], vertex[3];
    static integer srflst[100];
    extern logical exists_(char *, ftnlen);
    extern /* Subroutine */ int vminus_(doublereal *, doublereal *), surfpt_(
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    static doublereal tol, xpt[3];
    extern /* Subroutine */ int zzraynp_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);

/* $ Abstract */

/*     Exercise the entry points of routine ZZSINUTL. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the entry points of the SPICELIB routine */
/*     ZZSINUTL: */


/*        ZZSUELIN:   Initialize ray intercept and near point utilities */
/*                    for ellipsoidal target surface */


/*        ZZSUDSKI:   Initialize ray intercept and near point utilities */
/*                    for DSK target surface */

/*        ZZMINRAD:   Return minimum spherical bounding radius for */
/*                    surface */

/*        ZZMAXRAD:   Return maximum spherical bounding radius for */
/*                    surface */

/*        ZZRAYSFX:   Compute ray-surface intercept given inputs */
/*                    set by ZZSUELIN or ZZSUDSKI */


/*        ZZRAYNP:    Compute nearest point on surface to ray given */
/*                    inputs set by ZZSUELIN or ZZSUDSKI */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 01-JUN-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */

/*     DOUBLE PRECISION      TIGHT */
/*     PARAMETER           ( TIGHT  = 1.D-11 ) */

/*     Local Variables */

/*      CHARACTER*(LNSIZE)    TITLE */

/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZSINUTL", (ftnlen)10);

/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create PCK.", (ftnlen)19);
    if (exists_("zzsinutl.tpc", (ftnlen)12)) {
	delfil_("zzsinutl.tpc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_pck08__("zzsinutl.tpc", &c_true, &c_true, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create Mars DSK.", (ftnlen)24);
    if (exists_("zzsinutl_0.bds", (ftnlen)14)) {
	delfil_("zzsinutl_0.bds", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    bodyid = 499;
    surfid = 1;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    nlon = 40;
    nlat = 20;
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zzsinutl_0.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create scaled-down Mars DSK.", (ftnlen)36);
    if (exists_("zzsinutl_1.bds", (ftnlen)14)) {
	delfil_("zzsinutl_1.bds", (ftnlen)14);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Temporarily replace radii in pool with scaled */
/*     radii. */

    vscl_(&c_b33, xradii, radii);
    s_copy(kvname, "BODY499_RADII", (ftnlen)32, (ftnlen)13);
    pdpool_(kvname, &c__3, radii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     This file contains "surface 2" for Mars. */

    surfid = 2;
    bodyid = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    nlon = 40;
    nlat = 20;
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zzsinutl_1.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Restore original Mars radii. */

    pdpool_(kvname, &c__3, xradii, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     ZZSINUTL error case */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZSINUTL error: direct call to ZZSINUTL.", (ftnlen)40);
    zzsinutl_(&bodyid, &nsurf, srflst, &et, &fixfid, vertex, raydir, spoint, &
	    found, &minrad, &maxrad, pnear, &dist);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
/* ********************************************************************** */

/*     ZZMAXRAD / ZZSUDSKI / ZZSUELIN tests */

/* ********************************************************************** */
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMAXRAD: ellipsoid shape.", (ftnlen)26);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(xradii[0],xradii[1]);
    xmax = max(d__1,xradii[2]);

/*     Get the max target radius. */

    zzmaxrad_(&maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MAXRAD", &maxrad, "~", &xmax, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMAXRAD: DSK shape; use two surfaces.", (ftnlen)38);

/*     Load DSKs. */

    furnsh_("zzsinutl_0.bds", (ftnlen)14);
    furnsh_("zzsinutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set target parameters. */

    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(xradii[0],xradii[1]);
    xmax = max(d__1,xradii[2]);

/*     Get the max target radius. We expect surface 2 not to */
/*     contribute. */

/*     In principle, the maximum radius should match that of the */
/*     ellipsoid, since the tessellation, which uses an even number of */
/*     latitude bands, places some vertices on the target body's */
/*     equator. However, round-off error should be expected. */

    zzmaxrad_(&maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MAXRAD", &maxrad, "~/", &xmax, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMAXRAD: DSK shape; use empty surface list.", (ftnlen)44);

/*     Set target parameters. */

    bodyid = 499;
    nsurf = 0;
    srflst[0] = 1;
    srflst[1] = 2;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(xradii[0],xradii[1]);
    xmax = max(d__1,xradii[2]);

/*     Get the max target radius. We expect surface 2 not to */
/*     contribute. */

    zzmaxrad_(&maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MAXRAD", &maxrad, "~/", &xmax, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMAXRAD: DSK shape; use surface 2 only.", (ftnlen)40);

/*     Set target parameters. */

    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MAX */
    d__1 = max(xradii[0],xradii[1]);
    xmax = max(d__1,xradii[2]) * .5;

/*     Get the max target radius. We expect surface 2 not to */
/*     contribute. */

    zzmaxrad_(&maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MAXRAD", &maxrad, "~/", &xmax, &tol, ok, (ftnlen)6, (ftnlen)2);
/* ********************************************************************** */

/*     ZZSUELIN error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZSUELIN: ellipsoid radii are not available.", (ftnlen)44);

/*     Radii for the target are not loaded. */

    bodyid = -777;
    zzsuelin_(&bodyid);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
/* ********************************************************************** */

/*     ZZSUDSKI error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZSUDSKI: surface count too large.", (ftnlen)34);
    nsurf = 101;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZSUDSKI: surface count too small.", (ftnlen)34);
    nsurf = -1;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_true, "SPICE(INVALIDCOUNT)", ok, (ftnlen)19);
/* ********************************************************************** */

/*     ZZMINRAD / ZZSUDSKI / ZZSUELIN tests */

/* ********************************************************************** */
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMINRAD: ellipsoid shape.", (ftnlen)26);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MIN */
    d__1 = min(xradii[0],xradii[1]);
    xmin = min(d__1,xradii[2]);

/*     Get the min target radius. */

    zzminrad_(&minrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MINRAD", &minrad, "~", &xmin, &tol, ok, (ftnlen)6, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMINRAD: DSK shape; use two surfaces.", (ftnlen)38);

/*     The DSKs are already loaded. */


/*     Set target parameters. */

    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Get the expected minimum radius via a brute force */
/*     computation which considers each plate. */

    zzdsksph_(&bodyid, &nsurf, srflst, &xmin, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzminrad_(&minrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MINRAD", &minrad, "~/", &xmin, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMINRAD: DSK shape; use empty surface list.", (ftnlen)44);

/*     Set target parameters. */

    bodyid = 499;
    nsurf = 0;
    srflst[0] = 1;
    srflst[1] = 2;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the expected minimum radius via a brute force */
/*     computation which considers each plate. */

    zzdsksph_(&bodyid, &nsurf, srflst, &xmin, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the min target radius. We expect surface 2 to */
/*     contribute this value. */

    zzminrad_(&minrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MINRAD", &minrad, "~/", &xmin, &tol, ok, (ftnlen)6, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZMINRAD: DSK shape; use surface 2 only.", (ftnlen)40);

/*     Set target parameters. */

    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;
    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the expected minimum radius via a brute force */
/*     computation which considers each plate. */

    zzdsksph_(&bodyid, &nsurf, srflst, &xmin, &maxrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get the min target radius. We expect surface 2 to */
/*     contribute this value. */
    zzminrad_(&minrad);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("MINRAD", &minrad, "~/", &xmin, &tol, ok, (ftnlen)6, (ftnlen)2);
/* ********************************************************************** */

/*     ZZRAYSFX tests */

/* ********************************************************************** */
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: Ellipsoid intercept.", (ftnlen)30);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vminus_(vertex, raydir);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MIN */
    d__1 = min(xradii[0],xradii[1]);
    xmin = min(d__1,xradii[2]);

/*     Get expected intercept. */

    surfpt_(vertex, raydir, xradii, &xradii[1], &xradii[2], xxpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("SURFPT found", &found, &c_true, ok, (ftnlen)12);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_true, ok, (ftnlen)14);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: Ellipsoid non-intercept.", (ftnlen)34);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vequ_(vertex, raydir);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* Computing MIN */
    d__1 = min(xradii[0],xradii[1]);
    xmin = min(d__1,xradii[2]);

/*     Now try to compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_false, ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: DSK intercept; use 2 surfaces.", (ftnlen)40);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vminus_(vertex, raydir);

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch IS used for the DSK case; it's used to */
/*     select applicable segments. */

    et = 0.;

/*     Compute the expected intercept. */

    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, 
	    raydir, xxpt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DSKXV found", &found, &c_true, ok, (ftnlen)11);

/*     Now compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_true, ok, (ftnlen)14);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: DSK intercept; use an empty surface list.", (ftnlen)51);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 0;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch IS used for the DSK case; it's used to */
/*     select applicable segments. */

    et = 0.;

/*     Compute the expected intercept. */

    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, 
	    raydir, xxpt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DSKXV found", &found, &c_true, ok, (ftnlen)11);

/*     Now compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_true, ok, (ftnlen)14);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: DSK intercept; use the second surface only.", (ftnlen)
	    53);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch IS used for the DSK case; it's used to */
/*     select applicable segments. */

    et = 0.;

/*     Compute the expected intercept. */

    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, 
	    raydir, xxpt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DSKXV found", &found, &c_true, ok, (ftnlen)11);

/*     Now compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_true, ok, (ftnlen)14);
    tol = 1e-14;
    chckad_("XPT", xpt, "~~/", xxpt, &c__3, &tol, ok, (ftnlen)3, (ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: DSK non-intercept due to ray direction; use 2 surfaces."
	    , (ftnlen)65);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vequ_(vertex, raydir);

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch IS used for the DSK case; it's used to */
/*     select applicable segments. */

    et = 0.;

/*     Now try to compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_false, ok, (ftnlen)14);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYSFX: DSK non-intercept due to epoch; use 2 surfaces.", (
	    ftnlen)57);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Set ray vertex and direction so that an intercept */
/*     will occur if the required surface data are */
/*     available. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vminus_(vertex, raydir);

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch IS used for the DSK case; it's used to */
/*     select applicable segments. */

    et = jyear_() * -500;

/*     Now try to compute the intercept using the generalized callback. */

    zzraysfx_(vertex, raydir, &et, xpt, &found);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("ZZRAYSFX found", &found, &c_false, ok, (ftnlen)14);
/* ********************************************************************** */

/*     ZZRAYNP tests */

/* ********************************************************************** */
/* ********************************************************************** */

/*     Normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: find near point to ray on ellipsoid. Non-intercept case."
	    , (ftnlen)65);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. */

    npedln_(xradii, &xradii[1], &xradii[2], vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: find near point to ray on ellipsoid. Intercept case.", (
	    ftnlen)61);

/*     Set target parameters. */

    bodyid = 499;
    zzsuelin_(&bodyid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b267, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get target radii from PCK file. */

    bodvcd_(&bodyid, "RADII", &c__3, &n, xradii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. */

    npedln_(xradii, &xradii[1], &xradii[2], vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;

/*     For the intercept cases, use an absolute error test for */
/*     distance, rather than a relative error test. */

    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: DSK case: find near point to ray on bounding ellipsoid."
	    " Use two surfaces. Non-intercept case.", (ftnlen)102);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b267, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get maximum bounding radius. */

    zzmaxrad_(xradii);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. Note that we have a spherical */
/*     bounding surface. */

    npedln_(xradii, xradii, xradii, vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: DSK case: find near point to ray on bounding ellipsoid."
	    " Use two surfaces, this time with an empty surface list. Non-int"
	    "ercept case.", (ftnlen)140);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 0;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get maximum bounding radius. */

    zzmaxrad_(xradii);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. Note that we have a spherical */
/*     bounding surface. */

    npedln_(xradii, xradii, xradii, vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: DSK case: find near point to ray on bounding ellipsoid."
	    " Use the second surface only. Non-intercept case.", (ftnlen)113);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b148, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get maximum bounding radius. */

    zzmaxrad_(xradii);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. Note that we have a spherical */
/*     bounding surface. */

    npedln_(xradii, xradii, xradii, vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~/", &xdist, &tol, ok, (ftnlen)4, (ftnlen)2);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZRAYNP: DSK case: find near point to ray on bounding ellipsoid."
	    " Use the second surface only. Intercept case.", (ftnlen)109);

/*     Set target parameters. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = 499;
    nsurf = 1;
    srflst[0] = 2;

/*     Set the target parameters. */

    zzsudski_(&bodyid, &nsurf, srflst, &fixfid);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set ray vertex and direction. */

    vpack_(&c_b146, &c_b147, &c_b267, vertex);
    vpack_(&c_b265, &c_b266, &c_b267, raydir);

/*     Get maximum bounding radius. */

    zzmaxrad_(xradii);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Get expected near point. Note that we have a spherical */
/*     bounding surface. */

    npedln_(xradii, xradii, xradii, vertex, raydir, xpnear, &xdist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The epoch is unused for the ellipsoid case. */

    et = 0.;

/*     Now compute the near point using the generalized callback. */

    zzraynp_(vertex, raydir, &et, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-14;
    chcksd_("DIST", &dist, "~", &xdist, &tol, ok, (ftnlen)4, (ftnlen)1);
    chckad_("PNEAR", pnear, "~~/", xpnear, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);
    delfil_("zzsinutl.tpc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzsinutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzsinutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zzsinutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zzsinutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zzsinutl__ */

