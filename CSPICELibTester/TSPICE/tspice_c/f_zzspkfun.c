/* f_zzspkfun.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__0 = 0;
static integer c__3 = 3;
static doublereal c_b32 = 0.;
static integer c__14 = 14;
static doublereal c_b107 = 1e-11;
static doublereal c_b628 = 1.;
static integer c_b741 = 301001;
static integer c_b744 = 399001;
static integer c__6 = 6;
static integer c_b779 = 401001;
static integer c__399 = 399;
static doublereal c_b813 = 1e-13;

/* $Procedure F_ZZSPKFUN ( Family of tests for SPK function routines ) */
/* Subroutine */ int f_zzspkfun__(logical *ok)
{
    /* Initialized data */

    static char abcorr[25*9] = " nOne                    " " lT             "
	    "         " " Lt + s                  " "  Cn                     "
	     "cN + S                   " " xlT                     " " XLt +"
	    " s                 " "  xCn                    " "XcN + S       "
	    "           ";
    static char frames[32*5] = "J2000                           " "MARSIAU  "
	    "                       " "IAU_EARTH                       " "IAU"
	    "_MARS                        " "IAU_JUPITER                     ";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal xdlt;
    extern /* Subroutine */ int mxvg_(doublereal *, doublereal *, integer *, 
	    integer *, doublereal *), zzspkfao_(integer *, doublereal *, char 
	    *, char *, S_fp, doublereal *, doublereal *, doublereal *, ftnlen,
	     ftnlen), zzspkfap_(S_fp, doublereal *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), zzspkfat_(S_fp, doublereal *, char 
	    *, char *, integer *, doublereal *, doublereal *, doublereal *, 
	    ftnlen, ftnlen), zzspkflt_(S_fp, doublereal *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen, 
	    ftnlen);
    integer i__, j;
    extern /* Subroutine */ int zzspkfzo_(integer *, doublereal *, char *, 
	    char *, S_fp, doublereal *, doublereal *, ftnlen, ftnlen), 
	    zzspkfzt_(S_fp, doublereal *, char *, char *, integer *, 
	    doublereal *, doublereal *, ftnlen, ftnlen);
    doublereal delta;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmc_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), repmd_(char *, 
	    char *, doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    doublereal state[6];
    char title[320];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    char error[320];
    doublereal stobs[6], xform[36]	/* was [6][6] */;
    extern /* Subroutine */ int spkez_(integer *, doublereal *, char *, char *
	    , integer *, doublereal *, doublereal *, ftnlen, ftnlen), 
	    t_success__(logical *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal et, accobs[3];
    integer handle;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    integer obscde;
    doublereal lt;
    extern /* Subroutine */ int chcksd_(char *, doublereal *, char *, 
	    doublereal *, doublereal *, logical *, ftnlen, ftnlen), chckxc_(
	    logical *, char *, logical *, ftnlen);
    integer ctrcde;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen);
    integer trgcde;
    extern /* Subroutine */ int kilfil_(char *, ftnlen), spkacs_(integer *, 
	    doublereal *, char *, char *, integer *, doublereal *, doublereal 
	    *, doublereal *, ftnlen, ftnlen);
    doublereal estate[6];
    integer coridx;
    doublereal istate[6];
    integer frmidx;
    doublereal obsssb[12]	/* was [6][2] */;
    extern /* Subroutine */ int spkgeo_(integer *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen);
    integer timidx;
    extern /* Subroutine */ int tparse_(char *, doublereal *, char *, ftnlen, 
	    ftnlen), qderiv_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), spkltc_(integer *, doublereal *, 
	    char *, char *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen), spkuef_(integer *);
    doublereal tstate[6];
    extern /* Subroutine */ int spkssb_(integer *, doublereal *, char *, 
	    doublereal *, ftnlen);
    doublereal xstate[6];
    extern /* Subroutine */ int tstpck_(char *, logical *, logical *, ftnlen),
	     sxform_(char *, char *, doublereal *, doublereal *, ftnlen, 
	    ftnlen);
    integer outctr;
    extern /* Subroutine */ int tstspk_(char *, logical *, integer *, ftnlen),
	     f_zzspkget__(doublereal *, char *, integer *, doublereal *, 
	    ftnlen), f_zzspkset__(integer *, integer *);
    char ref[32];
    doublereal clt, dlt, elt, xlt;

/* $ Abstract */

/*     This routine performs a series of tests on the private */
/*     SPK routines that support a target or an observer whose */
/*     ephemeris is given by a subroutine rather than by SPK data. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB private, function-based */
/*     SPK routines */

/*        ZZSPKFAO */
/*        ZZSPKFAP */
/*        ZZSPKFAT */
/*        ZZSPKFLT */
/*        ZZSPKFZO */
/*        ZZSPKFZT */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 10-OCT-2013 (NJB) */

/*        Various test case names were expanded to be more */
/*        meaningful. Tolerances were loosened to enable */
/*        tests to pass in some C environments. */

/* -    TSPICE Version 1.0.0, 10-JAN-2012 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved variables */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZSPKFUN", (ftnlen)10);
/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    Set-up                                                     * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */


/*     Create and load SPK and PCK. */

    tcase_("Create kernels.", (ftnlen)15);
    kilfil_("spkfun.bsp", (ftnlen)10);
    kilfil_("spkfun.ker", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstspk_("spkfun.bsp", &c_true, &handle, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tstpck_("spkfun.ker", &c_true, &c_false, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tparse_("1 JAN 1995", &et, error, (ftnlen)10, (ftnlen)320);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */


/*     Exercise the test utility routines located at the bottom */
/*     of this fiie. */

    tcase_("Exercise the local SPK set/get utilities.", (ftnlen)41);
    trgcde = 399;
    ctrcde = 3;
    f_zzspkset__(&trgcde, &ctrcde);
    et = jyear_() * 10;
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    spkgeo_(&trgcde, &et, ref, &ctrcde, xstate, &xlt, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    f_zzspkget__(&et, ref, &outctr, state, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Make sure the correct center of motion is returned. */

    chcksi_("Center", &outctr, "=", &ctrcde, &c__0, ok, (ftnlen)6, (ftnlen)1);

/*     Check the ouput state. We expect exact equality. */

    chckad_("Position", state, "=", xstate, &c__3, &c_b32, ok, (ftnlen)8, (
	    ftnlen)1);
    chckad_("Velocity", &state[3], "=", &xstate[3], &c__3, &c_b32, ok, (
	    ftnlen)8, (ftnlen)1);
/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFZT tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzt_((S_fp)f_zzspkget__, &et, ref, "L+S", &obscde, state, &lt, (
	    ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    zzspkfzt_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzt_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target center data.", (ftnlen)34);
    trgcde = 7;
    ctrcde = 777;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzt_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer data.", (ftnlen)29);
    trgcde = 5;
    ctrcde = 3;
    obscde = 444;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzt_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFZT normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFZT for every aberration correction. Use different fr"
	    "ames. Target = Earth. Center = Earth-Moon barycenter. Observer ="
	    " Mars.", (ftnlen)134);
    trgcde = 399;
    ctrcde = 3;
    obscde = 499;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)445)
		    ) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFZT: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)466)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkf"
			"un__", (ftnlen)474)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfzt_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)479)) * 25, &obscde, 
			state, &lt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFZT: change observer, target, and center, and repeat "
	    "previous tests.", (ftnlen)79);
    trgcde = 599;
    ctrcde = 5;
    obscde = 399;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)523)
		    ) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFZT: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)544)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkf"
			"un__", (ftnlen)552)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfzt_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)557)) * 25, &obscde, 
			state, &lt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFZO tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzo_(&trgcde, &et, ref, "L+S", (S_fp)f_zzspkget__, state, &lt, (
	    ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    zzspkfzo_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzo_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer data.", (ftnlen)29);
    trgcde = 5;
    ctrcde = 3;
    obscde = 444;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzo_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer center data.", (ftnlen)36);
    trgcde = 5;
    ctrcde = 455;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfzo_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFZO normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFZO for every aberration correction. Use different fr"
	    "ames. Target = Earth. Center = Earth-Moon barycenter. Observer ="
	    " Mars.", (ftnlen)134);
    trgcde = 399;
    ctrcde = 3;
    obscde = 499;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)733)
		    ) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFZO: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)754)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkf"
			"un__", (ftnlen)763)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfzo_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 
			9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zz"
			"spkfun__", (ftnlen)768)) * 25, (S_fp)f_zzspkget__, 
			state, &lt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFZO: change observer, target, and center, and repeat "
	    "previous tests.", (ftnlen)79);
    trgcde = 599;
    ctrcde = 5;
    obscde = 399;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {
	for (frmidx = 1; frmidx <= 5; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)814)
		    ) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFZO: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)835)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkez_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 &&
			 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkf"
			"un__", (ftnlen)844)) * 25, &obscde, xstate, &xlt, (
			ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfzo_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 
			9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zz"
			"spkfun__", (ftnlen)849)) * 25, (S_fp)f_zzspkget__, 
			state, &lt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFAT tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "L+S", &obscde, state, &lt, &dlt, 
	    (ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: non-inertial frame", (ftnlen)30);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target center data.", (ftnlen)34);
    trgcde = 7;
    ctrcde = 777;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer data.", (ftnlen)29);
    trgcde = 5;
    ctrcde = 3;
    obscde = 444;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfat_((S_fp)f_zzspkget__, &et, ref, "LT+S", &obscde, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFAT normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFAT for every aberration correction. Use different fr"
	    "ames. Target = Earth. Center = Earth-Moon barycenter. Observer ="
	    " Mars.", (ftnlen)134);
    trgcde = 399;
    ctrcde = 3;
    obscde = 499;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use only inertial frames. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1047)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFAT: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1068)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkacs_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1076)) * 25, &obscde, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfat_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)1081)) * 25, &obscde, 
			state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFAT: change observer, target, and center, and repeat "
	    "previous tests.", (ftnlen)79);
    trgcde = 599;
    ctrcde = 5;
    obscde = 399;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use only inertial frames. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1132)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFAT: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1153)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkacs_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1162)) * 25, &obscde, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfat_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)1167)) * 25, &obscde, 
			state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFAO tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "L+S", (S_fp)f_zzspkget__, state, &lt, &dlt, 
	    (ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: non-inertial frame.", (ftnlen)31);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer data.", (ftnlen)29);
    trgcde = 5;
    ctrcde = 3;
    obscde = 444;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no observer center data.", (ftnlen)36);
    trgcde = 5;
    ctrcde = 455;
    obscde = 4;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    zzspkfao_(&trgcde, &et, ref, "Lt+S", (S_fp)f_zzspkget__, state, &lt, &dlt,
	     (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFAO normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFAO for every aberration correction. Use different fr"
	    "ames. Target = Earth. Center = Earth-Moon barycenter. Observer ="
	    " Mars.", (ftnlen)134);
    trgcde = 399;
    ctrcde = 3;
    obscde = 499;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use only inertial frames. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1374)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFAO: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1395)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkacs_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1403)) * 25, &obscde, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfao_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 
			9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zz"
			"spkfun__", (ftnlen)1407)) * 25, (S_fp)f_zzspkget__, 
			state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */


/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFAO: change observer, target, and center, and repeat "
	    "previous tests.", (ftnlen)79);
    trgcde = 599;
    ctrcde = 5;
    obscde = 399;
    f_zzspkset__(&obscde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use only inertial frames. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1460)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFAO: Target = #; Observer = #; Ref = # A"
			"bcorr = #; ET = #.", (ftnlen)320, (ftnlen)63);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1481)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);
		spkacs_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1490)) * 25, &obscde, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfao_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 
			9 && 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zz"
			"spkfun__", (ftnlen)1495)) * 25, (S_fp)f_zzspkget__, 
			state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFAP tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cleard_(&c__3, accobs);
    zzspkfap_((S_fp)f_zzspkget__, &et, ref, "L+S", stobs, accobs, state, &lt, 
	    &dlt, (ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    spkssb_(&obscde, &et, "J2000", stobs, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkfap_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, accobs, state, &lt,
	     &dlt, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: non-inertial frame", (ftnlen)30);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "IAU_EARTH", (ftnlen)32, (ftnlen)9);
    et = jyear_();
    spkssb_(&obscde, &et, "J2000", stobs, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkfap_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, accobs, state, &lt,
	     &dlt, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(BADFRAME)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkfap_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, accobs, state, &lt,
	     &dlt, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target center data.", (ftnlen)34);
    trgcde = 5;
    ctrcde = 333;
    obscde = 3;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&c__3, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkfap_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, accobs, state, &lt,
	     &dlt, (ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFAP normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFAP for every aberration correction. Use the J2000 fr"
	    "ame. Target = Earth. Center = Moon. Observer = Jupiter barycente"
	    "r.", (ftnlen)130);
    trgcde = 399;
    ctrcde = 301;
    obscde = 5;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use J2000 and a second inertial frame. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1699)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFAP: Target = #; Observer = #; Center = "
			"#; Ref = # Abcorr = #; ET = #.", (ftnlen)320, (ftnlen)
			75);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1722)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              We need the state of the observer relative to the */
/*              SSB at ET. */

		spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Compute observer acceleration relative to the */
/*              solar system barycenter. */

		for (i__ = 1; i__ <= 2; ++i__) {
		    delta = (doublereal) ((i__ << 1) - 3);
		    d__1 = et + delta;
		    spkssb_(&obscde, &d__1, ref, &obsssb[(i__1 = i__ * 6 - 6) 
			    < 12 && 0 <= i__1 ? i__1 : s_rnge("obsssb", i__1, 
			    "f_zzspkfun__", (ftnlen)1746)], (ftnlen)32);
		}
		qderiv_(&c__3, &obsssb[3], &obsssb[9], &c_b628, accobs);

/*              Get expected state, light time, and light time rate. */

		spkacs_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1755)) * 25, &obscde, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkfap_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)1760)) * 25, stobs, 
			accobs, state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    ZZSPKFLT tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad aberration correction.", (ftnlen)38);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkflt_((S_fp)f_zzspkget__, &et, ref, "L+S", stobs, state, &lt, &dlt, (
	    ftnlen)32, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: bad frame name", (ftnlen)26);
    trgcde = 399;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J200", (ftnlen)32, (ftnlen)4);
    et = jyear_();
    spkssb_(&obscde, &et, "J2000", stobs, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkflt_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, state, &lt, &dlt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(UNKNOWNFRAME)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target data.", (ftnlen)27);
    trgcde = 777;
    ctrcde = 3;
    obscde = 4;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkflt_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, state, &lt, &dlt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Error case: no target center data.", (ftnlen)34);
    trgcde = 5;
    ctrcde = 333;
    obscde = 3;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    s_copy(ref, "J2000", (ftnlen)32, (ftnlen)5);
    et = jyear_();
    spkssb_(&c__3, &et, ref, stobs, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zzspkflt_((S_fp)f_zzspkget__, &et, ref, "LT+S", stobs, state, &lt, &dlt, (
	    ftnlen)32, (ftnlen)4);
    chckxc_(&c_true, "SPICE(SPKINSUFFDATA)", ok, (ftnlen)20);


/*     ZZSPKFLT normal cases: */



/* --- Case: ------------------------------------------------------ */

    tcase_("Check ZZSPKFLT for every aberration correction. Use the J2000 fr"
	    "ame. Target = Earth. Center = Moon. Observer = Jupiter barycente"
	    "r.", (ftnlen)130);
    trgcde = 399;
    ctrcde = 301;
    obscde = 5;
    f_zzspkset__(&trgcde, &ctrcde);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    for (coridx = 1; coridx <= 9; ++coridx) {

/*        Use J2000 and a second inertial frame. */

	for (frmidx = 1; frmidx <= 2; ++frmidx) {
	    s_copy(ref, frames + (((i__1 = frmidx - 1) < 5 && 0 <= i__1 ? 
		    i__1 : s_rnge("frames", i__1, "f_zzspkfun__", (ftnlen)
		    1932)) << 5), (ftnlen)32, (ftnlen)32);
	    for (timidx = 1; timidx <= 10; ++timidx) {
		et = timidx * jyear_();

/* --- Case: ------------------------------------------------------ */

		s_copy(title, "ZZSPKFLT: Target = #; Observer = #; Center = "
			"#; Ref = # Abcorr = #; ET = #.", (ftnlen)320, (ftnlen)
			75);
		repmi_(title, "#", &trgcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &obscde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmi_(title, "#", &ctrcde, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", ref, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)32, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmc_(title, "#", abcorr + ((i__1 = coridx - 1) < 9 && 0 <= 
			i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzspkfun__", (
			ftnlen)1955)) * 25, title, (ftnlen)320, (ftnlen)1, (
			ftnlen)25, (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		repmd_(title, "#", &et, &c__14, title, (ftnlen)320, (ftnlen)1,
			 (ftnlen)320);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		tcase_(title, (ftnlen)320);

/*              We need the state of the observer relative to the */
/*              SSB at ET. */

		spkssb_(&obscde, &et, ref, stobs, (ftnlen)32);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Get expected state, light time, and light time rate. */

		spkltc_(&trgcde, &et, ref, abcorr + ((i__1 = coridx - 1) < 9 
			&& 0 <= i__1 ? i__1 : s_rnge("abcorr", i__1, "f_zzsp"
			"kfun__", (ftnlen)1974)) * 25, stobs, xstate, &xlt, &
			xdlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		zzspkflt_((S_fp)f_zzspkget__, &et, ref, abcorr + ((i__1 = 
			coridx - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge("abcorr",
			 i__1, "f_zzspkfun__", (ftnlen)1979)) * 25, stobs, 
			state, &lt, &dlt, (ftnlen)32, (ftnlen)25);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chckad_("Position", state, "~~/", xstate, &c__3, &c_b107, ok, 
			(ftnlen)8, (ftnlen)3);
		chckad_("Velocity", &state[3], "~~/", &xstate[3], &c__3, &
			c_b107, ok, (ftnlen)8, (ftnlen)3);
		chcksd_("Light time", &lt, "~/", &xlt, &c_b107, ok, (ftnlen)
			10, (ftnlen)2);
		chcksd_("Light time rate", &dlt, "~/", &xdlt, &c_b107, ok, (
			ftnlen)15, (ftnlen)2);
	    }

/*           End of time loop. */

	}

/*        End of frame loop. */

    }

/*     End of aberration correction loop. */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    SPKACS tests                                             * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Make sure that SPKEZ returns the same thing as SPKACS when an in"
	    "ertial frame is the requested output frame. Converged Newtonian "
	    "plus stellar aberation. Reception case.", (ftnlen)167);
    spkez_(&c_b741, &et, "J2000", "CN+S", &c_b744, state, &lt, (ftnlen)5, (
	    ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b741, &et, "J2000", "CN+S", &c_b744, estate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "=", estate, &c__6, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksd_("LT", &lt, "=", &elt, &c_b32, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Make sure that SPKEZ returns the same thing as SPKACS when an in"
	    "ertial frame is the requested output frame. Converged Newtonian "
	    "plus stellar aberation. Transmission case.", (ftnlen)170);
    spkez_(&c_b741, &et, "J2000", "XCN+S", &c_b744, state, &lt, (ftnlen)5, (
	    ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b741, &et, "J2000", "XCN+S", &c_b744, estate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)5);
    chckad_("STATE", state, "=", estate, &c__6, &c_b32, ok, (ftnlen)5, (
	    ftnlen)1);
    chcksd_("LT", &lt, "=", &elt, &c_b32, ok, (ftnlen)2, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is not t"
	    "arget or observer. CN+S correction. Reception case.", (ftnlen)177)
	    ;
    spkez_(&c_b779, &et, "IAU_EARTH", "CN+S", &c_b741, state, &lt, (ftnlen)9, 
	    (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b779, &et, "J2000", "CN+S", &c_b741, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkssb_(&c_b741, &et, "J2000", stobs, (ftnlen)5);
    spkltc_(&c__399, &et, "J2000", "CN+S", stobs, tstate, &clt, &dlt, (ftnlen)
	    5, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et - clt;
    sxform_("J2000", "IAU_EARTH", &d__1, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the derivative block to account for the rate of change */
/*     of light time. */

    for (j = 4; j <= 6; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    xform[(i__1 = j + i__ * 6 - 7) < 36 && 0 <= i__1 ? i__1 : s_rnge(
		    "xform", i__1, "f_zzspkfun__", (ftnlen)2094)] = (1. - dlt)
		     * xform[(i__2 = j + i__ * 6 - 7) < 36 && 0 <= i__2 ? 
		    i__2 : s_rnge("xform", i__2, "f_zzspkfun__", (ftnlen)2094)
		    ];
	}
    }
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is not t"
	    "arget or observer. CN+S correction. Transmission case.", (ftnlen)
	    180);
    spkez_(&c_b779, &et, "IAU_EARTH", "XCN+S", &c_b741, state, &lt, (ftnlen)9,
	     (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b779, &et, "J2000", "XCN+S", &c_b741, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkssb_(&c_b741, &et, "J2000", stobs, (ftnlen)5);
    spkltc_(&c__399, &et, "J2000", "XCN+S", stobs, tstate, &clt, &dlt, (
	    ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et + clt;
    sxform_("J2000", "IAU_EARTH", &d__1, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the derivative block to account for the rate of change */
/*     of light time. */

    for (j = 4; j <= 6; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    xform[(i__1 = j + i__ * 6 - 7) < 36 && 0 <= i__1 ? i__1 : s_rnge(
		    "xform", i__1, "f_zzspkfun__", (ftnlen)2141)] = (dlt + 1.)
		     * xform[(i__2 = j + i__ * 6 - 7) < 36 && 0 <= i__2 ? 
		    i__2 : s_rnge("xform", i__2, "f_zzspkfun__", (ftnlen)2141)
		    ];
	}
    }
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is targe"
	    "t. CN+S correction. Reception case.", (ftnlen)161);
    spkez_(&c__399, &et, "IAU_EARTH", "CN+S", &c_b741, state, &lt, (ftnlen)9, 
	    (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c__399, &et, "J2000", "CN+S", &c_b741, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et - elt;
    sxform_("J2000", "IAU_EARTH", &d__1, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the derivative block to account for the rate of change */
/*     of light time. */

    for (j = 4; j <= 6; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    xform[(i__1 = j + i__ * 6 - 7) < 36 && 0 <= i__1 ? i__1 : s_rnge(
		    "xform", i__1, "f_zzspkfun__", (ftnlen)2184)] = (1. - dlt)
		     * xform[(i__2 = j + i__ * 6 - 7) < 36 && 0 <= i__2 ? 
		    i__2 : s_rnge("xform", i__2, "f_zzspkfun__", (ftnlen)2184)
		    ];
	}
    }
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is targe"
	    "t. CN+S correction. Transmission case.", (ftnlen)164);
    spkez_(&c__399, &et, "IAU_EARTH", "XCN+S", &c_b741, state, &lt, (ftnlen)9,
	     (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c__399, &et, "J2000", "XCN+S", &c_b741, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    d__1 = et + elt;
    sxform_("J2000", "IAU_EARTH", &d__1, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Scale the derivative block to account for the rate of change */
/*     of light time. */

    for (j = 4; j <= 6; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    xform[(i__1 = j + i__ * 6 - 7) < 36 && 0 <= i__1 ? i__1 : s_rnge(
		    "xform", i__1, "f_zzspkfun__", (ftnlen)2226)] = (dlt + 1.)
		     * xform[(i__2 = j + i__ * 6 - 7) < 36 && 0 <= i__2 ? 
		    i__2 : s_rnge("xform", i__2, "f_zzspkfun__", (ftnlen)2226)
		    ];
	}
    }
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is obser"
	    "ver. CN+S correction. Reception case.", (ftnlen)163);
    spkez_(&c_b741, &et, "IAU_EARTH", "CN+S", &c__399, state, &lt, (ftnlen)9, 
	    (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b741, &et, "J2000", "CN+S", &c__399, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sxform_("J2000", "IAU_EARTH", &et, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Perform an independent test to see if \"apparent\" positions in "
	    "non-inertial frames are properly computed. Frame center is obser"
	    "ver. CN+S correction. Transmission case.", (ftnlen)166);
    spkez_(&c_b741, &et, "IAU_EARTH", "XCN+S", &c__399, state, &lt, (ftnlen)9,
	     (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkacs_(&c_b741, &et, "J2000", "XCN+S", &c__399, istate, &elt, &dlt, (
	    ftnlen)5, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    sxform_("J2000", "IAU_EARTH", &et, xform, (ftnlen)5, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    mxvg_(xform, istate, &c__6, &c__6, estate);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chckad_("STATE", state, "~/", estate, &c__6, &c_b813, ok, (ftnlen)5, (
	    ftnlen)2);
    chcksd_("LT", &lt, "~/", &elt, &c_b813, ok, (ftnlen)2, (ftnlen)2);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kilfil_("spkfun.bsp", (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    t_success__(ok);
    return 0;
} /* f_zzspkfun__ */

/* ***************************************************************** */
/* *                                                               * */
/* *                                                               * */
/* *    Utility Routines                                           * */
/* *                                                               * */
/* *                                                               * */
/* ***************************************************************** */

/*     Subroutine package for referencing SPK ephemerides */
/*     via external calls: */

/* Subroutine */ int f_zzextspk__0_(int n__, integer *trgcde, doublereal *et, 
	char *ref, integer *inctr, integer *outctr, doublereal *state, ftnlen 
	ref_len)
{
    extern /* Subroutine */ int chkin_(char *, ftnlen);
    doublereal lt;
    static integer svccde, svtcde;
    extern /* Subroutine */ int spkgeo_(integer *, doublereal *, char *, 
	    integer *, doublereal *, doublereal *, ftnlen), chkout_(char *, 
	    ftnlen);


/*     Local parameters */


/*     Local variables */


/*     Saved variables */

    /* Parameter adjustments */
    if (state) {
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_f_zzspkset;
	case 2: goto L_f_zzspkget;
	}

    return 0;

/*     Save target and center ID codes. */


L_f_zzspkset:
    svtcde = *trgcde;
    svccde = *inctr;
    return 0;

/*     Look up state of target relative to center. */


L_f_zzspkget:
    chkin_("F_SPKGET", (ftnlen)8);
    *outctr = svccde;
    spkgeo_(&svtcde, et, ref, outctr, state, &lt, ref_len);
    chkout_("F_SPKGET", (ftnlen)8);
    return 0;
} /* f_zzextspk__ */

/* Subroutine */ int f_zzextspk__(integer *trgcde, doublereal *et, char *ref, 
	integer *inctr, integer *outctr, doublereal *state, ftnlen ref_len)
{
    return f_zzextspk__0_(0, trgcde, et, ref, inctr, outctr, state, ref_len);
    }

/* Subroutine */ int f_zzspkset__(integer *trgcde, integer *inctr)
{
    return f_zzextspk__0_(1, trgcde, (doublereal *)0, (char *)0, inctr, (
	    integer *)0, (doublereal *)0, (ftnint)0);
    }

/* Subroutine */ int f_zzspkget__(doublereal *et, char *ref, integer *outctr, 
	doublereal *state, ftnlen ref_len)
{
    return f_zzextspk__0_(2, (integer *)0, et, ref, (integer *)0, outctr, 
	    state, ref_len);
    }

