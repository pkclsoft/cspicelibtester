/* f_zzstelab.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b3 = .2;
static doublereal c_b4 = -.3;
static doublereal c_b5 = .4;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__3 = 3;
static doublereal c_b9 = 1.;
static doublereal c_b12 = 10.;
static integer c__10 = 10;
static doublereal c_b18 = 30.;
static doublereal c_b19 = -30.;
static integer c__14 = 14;
static logical c_false = FALSE_;
static doublereal c_b37 = 1e-4;
static doublereal c_b41 = 1e-9;
static doublereal c_b45 = 1e-7;
static doublereal c_b55 = 1e-10;

/* $Procedure      F_ZZSTELAB ( Tests for stellar aberration corrections ) */
/* Subroutine */ int f_zzstelab__(logical *ok)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    double sqrt(doublereal);
    integer s_rnge(char *, integer, char *, integer);
    double pow_dd(doublereal *, doublereal *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    extern logical even_(integer *);
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    doublereal pobs[3], rmat[9]	/* was [3][3] */;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    doublereal vobs[3];
    extern doublereal vdot_(doublereal *, doublereal *), vsep_(doublereal *, 
	    doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    logical xmit;
    extern /* Subroutine */ int zzstelab_(logical *, doublereal *, doublereal 
	    *, doublereal *, doublereal *, doublereal *);
    doublereal pobs0[3];
    extern /* Subroutine */ int eul2m_(doublereal *, doublereal *, doublereal 
	    *, integer *, integer *, integer *, doublereal *);
    integer i__, q;
    doublereal r__, s, t, w;
    extern /* Subroutine */ int tcase_(char *, ftnlen), repmd_(char *, char *,
	     doublereal *, integer *, char *, ftnlen, ftnlen, ftnlen);
    extern doublereal jyear_(void);
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    doublereal ptarg[3], starg[6];
    char title[80];
    extern /* Subroutine */ int vlcom_(doublereal *, doublereal *, doublereal 
	    *, doublereal *, doublereal *);
    doublereal vtarg[3], pcorr[3], e1[3], e2[3], e3[3], upobs[3];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    doublereal t0;
    extern /* Subroutine */ int t_success__(logical *);
    doublereal dpcor2[3], ptarg0[3], pcorr2[3];
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    doublereal uvobs0[3];
    integer cn;
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    integer rm, sn;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    doublereal angles[15], tdelta;
    extern /* Subroutine */ int rmaini_(integer *, integer *, integer *, 
	    integer *);
    doublereal dpcorr[3];
    extern /* Subroutine */ int vsclip_(doublereal *, doublereal *), vminus_(
	    doublereal *, doublereal *), t_zzstelab__(logical *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal acc[3];
    extern /* Subroutine */ int t_zzstlabn__(logical *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    doublereal sep, tau;

/* $ Abstract */

/*     This routine performs a series of tests on the routine ZZSTELAB */
/*     to make its aberration correction features work as expected. The */
/*     tests focus on singular and near-singular geometric cases where */
/*     the observer's velocity and observer-target position vector are */
/*     linearly dependent or nearly so. */

/*     See the test family F_SPKCOR for "normal" test cases that */
/*     exercise ZZSTELAB. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     Test cases performed by this routine include the following: */

/*        - Perform tests with the target */

/*             > motionless */
/*             > moving directly toward or away from the observer */
/*             > moving in a direction orthogonal to */
/*               the observer-target position vector, with */
/*               velocity direction either parallel or opposite to the */
/*               observer's acceleration vector */

/*        - For each geometric case, use both reception and transmission */
/*          corrections. */

/*        - For each combination of target velocity and radiation */
/*          direction, test with a range of angular separations of */
/*          observer velocity and observer-target position. The ranges */
/*          include values close to zero and close to pi radians. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 25-FEB-2008 (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Local parameters */


/*     NUMLIM is the approximate limiting value of the angular */
/*     separation of the observer's velocity and the observer- */
/*     target position vector used by ZZSTELAB to determine */
/*     whether to compute the stellar aberration correction */
/*     velocity analytically or numerically. The numerical */
/*     branch is taken when the separation angle is less than */
/*     NUMLIM. */


/*     Tolerance level that can be used with DE-418 on PC/Linux/g77 */
/*     platform: 5.D-8 */


/*     Tolerance level that can be used with DE-418 on PC/Linux/g77 */
/*     platform: 5.D-8 */


/*     Local Variables */

/*     Begin every test family with an open call. */

    topen_("F_ZZSTELAB", (ftnlen)10);

/*     We're going to do all of our work in a reference frame that's */
/*     not aligned with the standard basis. The basis vectors of our */
/*     working frame will be called E1, E2, and E3. */

    eul2m_(&c_b3, &c_b4, &c_b5, &c__1, &c__2, &c__3, rmat);
    vequ_(rmat, e1);
    vequ_(&rmat[3], e2);
    vequ_(&rmat[6], e3);

/*     We're going to work with an artificial ephemeris defined */
/*     by a simple, analytic formula. There is an observer whose */
/*     orbit is circular. The observer's orbit is centered at */
/*     the origin of our frame, and the radius of the orbit is */
/*     about that of the Earth's orbit. There is a target whose */
/*     motion is either linear or which does not move. */

/*     We'll let the observer's orbital radius be R and its */
/*     orbital period be TAU. The orbital plane will be that */
/*     spanned by E1 and E2. The observer's location at time T */
/*     will be given by */

/*        POS(T) = R * ( cos( W*T )*E1  +  sin ( W*T )*E2 ) */

/*     where */

/*        W = 2 * Pi / TAU */


/*     The observer's velocity has the formula */

/*        W * R * ( -sin( W*T )*E1  +  cos( W*T )*E2 ) */

/*     and the observer's acceleration has the formula */

/*           2 */
/*        - W  * R * ( cos( W*T )*E1 + sin( W*T )*E2 ) */


/*     Since we wish to avoid using time values too close to */
/*     zero, we'll let the central epoch of each sequence of */
/*     test samples be */

/*        T0 = TAU / 6. */

/*     At epoch TAU the observer is located at */

/*        ( R/2, R*SQRT(3)/2, 0 ) */

/*     and is moving in the direction */

/*        ( -R*SQRT(3)/2, R/2, 0 ) */

/*     For all target motion cases, we'll place the target at the point */
/*     at distance 2*R from the observer along the observer's velocity */
/*     vector at epoch T0. We'll call this location PTARG0. */

    r__ = 1.5e8;
    tau = jyear_();
    w = pi_() * 2 / tau;
    t0 = tau / 6;

/*     Compute the observer and target positions at T0. */

    d__1 = r__ / 2.;
    d__2 = r__ * sqrt(3.) / 2.;
    vlcom_(&d__1, e1, &d__2, e2, pobs0);
    d__1 = -r__ * sqrt(3.) / 2.;
    d__2 = r__ / 2.;
    vlcom_(&d__1, e1, &d__2, e2, vobs);
    vhat_(vobs, uvobs0);
    d__1 = r__ * 2;
    vlcom_(&c_b9, pobs0, &d__1, uvobs0, ptarg0);

/*     For all of our test cases, we'll choose times such that the */
/*     angular separation of the observer's velocity and the observer- */
/*     target vector SEP passes through the set of values (units are */
/*     radians): */

/*        -1.D-5, -1.D-6, ..., -1.D-11, 0, 1.D-11, ..., 1.D5 */

/*     We'll store these values in the array ANGLES. */

    angles[7] = 0.;
    for (i__ = 1; i__ <= 7; ++i__) {
	d__1 = -(i__ + 4.);
	angles[(i__1 = i__ - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("angles", 
		i__1, "f_zzstelab__", (ftnlen)344)] = -pow_dd(&c_b12, &d__1);
	d__1 = -(i__ + 4.);
	angles[(i__1 = 16 - i__ - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge("angl"
		"es", i__1, "f_zzstelab__", (ftnlen)345)] = pow_dd(&c_b12, &
		d__1);
    }
    for (cn = 1; cn <= 20; ++cn) {

/*        Use reception corrections for the odd-numbered cases; */
/*        use transmission corrections for the even-numbered ones. */

	xmit = even_(&cn);

/*        For the second set of cases, negate the observer's */
/*        velocity and acceleration. */

	if (cn <= 10) {
	    s = 1.;
	} else {
	    s = -1.;
	}
	rmaini_(&cn, &c__10, &q, &rm);

/*        For odd values of RM, we change the target's velocity. */
/*        For even values we keep the geometry constant and use */
/*        transmission corrections. */

/*        Note that the 10th and 20th cases correspond to RM values */
/*        of zero. */

	if (rm == 1) {

/*           Set the target's velocity to zero. The target rests at */
/*           PTARG0. */

	    cleard_(&c__3, vtarg);
	} else if (rm == 3) {

/*           Set the target's velocity to 30 km/s in the direction */
/*           parallel to the observer's position vector (relative to the */
/*           origin) at T0 (hence orthogonal to the observer's velocity */
/*           and anti-parallel to the observer's acceleration). */

	    vhat_(ptarg0, upobs);
	    vscl_(&c_b18, upobs, vtarg);
	} else if (rm == 5) {

/*           Set the target's velocity to 30 km/s in the direction */
/*           anit-parallel to the observer's position vector (relative */
/*           to the origin) at T0 (hence orthogonal to the observer's */
/*           velocity and anti-parallel to the observer's acceleration). */

	    vhat_(ptarg0, upobs);
	    vscl_(&c_b19, upobs, vtarg);
	} else if (rm == 7) {

/*           Set the target's velocity to 30 km/s in the direction */
/*           parallel to the observer's velocity vector (relative */
/*           to the origin) at T0. */

	    vscl_(&c_b18, uvobs0, vtarg);
	} else if (rm == 9) {

/*           Set the target's velocity to 30 km/s in the direction */
/*           anti-parallel to the observer's velocity vector (relative */
/*           to the origin) at T0. */

	    vscl_(&c_b19, uvobs0, vtarg);
	}
	for (sn = 1; sn <= 15; ++sn) {

/* ---- Case ------------------------------------------------------------- */


/*           Note: the TCASE call setting the test case title will */
/*           be made AFTER we compute the separation angle; this */
/*           angle will appear in the title. */

	    s_copy(title, "Case #; sample number #, separation angle #", (
		    ftnlen)80, (ftnlen)43);
	    repmi_(title, "#", &cn, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    repmi_(title, "#", &sn, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);

/*           The time increment TDELTA refers to the difference */

/*              T - T0 */

/*           This difference is a function of the offset angle */

/*              ANGLES(SN) */

/*           When the target is motionless, the offset angle is very */
/*           small, so the angle is primarily determined by the */
/*           observer's velocity direction, which changes with angular */
/*           velocity W. So we can approximate TDELTA by */

/*              ANGLES(SN) / W */

/*           When the target is moving, this approximation still is */
/*           of the correct magnitude, so we'll use it for these */
/*           cases as well. */

	    tdelta = angles[(i__1 = sn - 1) < 15 && 0 <= i__1 ? i__1 : s_rnge(
		    "angles", i__1, "f_zzstelab__", (ftnlen)461)] / w;
	    t = t0 + tdelta;

/*           Compute the position, velocity, and acceleration of the */
/*           observer relative to the solar system barycenter (that */
/*           is, the origin of the frame). */

	    d__1 = r__ * cos(w * t);
	    d__2 = r__ * sin(w * t);
	    vlcom_(&d__1, e1, &d__2, e2, pobs);
	    d__1 = -w * r__ * sin(w * t);
	    d__2 = w * r__ * cos(w * t);
	    vlcom_(&d__1, e1, &d__2, e2, vobs);
/* Computing 2nd power */
	    d__2 = w;
	    d__1 = -(d__2 * d__2) * r__ * cos(w * t);
/* Computing 2nd power */
	    d__4 = w;
	    d__3 = -(d__4 * d__4) * r__ * sin(w * t);
	    vlcom_(&d__1, e1, &d__3, e2, acc);

/*           Adjust the signs of the velocity and acceleration. */

	    vsclip_(&s, vobs);
	    vsclip_(&s, acc);

/*           Compute the target's position. */

	    vlcom_(&c_b9, ptarg0, &tdelta, vtarg, ptarg);

/*           Compute the target's state relative to the observer. */

	    vsub_(ptarg, pobs, starg);
	    vsub_(vtarg, vobs, &starg[3]);

/*           If this case is supposed to be singular, make sure */
/*           the observer's velocity is pointed directly toward */
/*           or away from the target. Due to round-off errors, */
/*           we cannot ensure that the angular separation of */
/*           the observer's velocity and the observer-target */
/*           position vector is exactly zero unless we play */
/*           some dirty tricks. */

	    if (sn == 8) {
		if (vdot_(vobs, starg) >= 0.) {
		    vequ_(vobs, starg);
		} else {
		    vminus_(vobs, starg);
		}
	    }

/*           Check the angular separation of the observer's velocity */
/*           and the observer-target position vector. */

	    sep = vsep_(vobs, starg);
	    repmd_(title, "#", &sep, &c__14, title, (ftnlen)80, (ftnlen)1, (
		    ftnlen)80);
	    tcase_(title, (ftnlen)80);
/*            WRITE (*,*) '----------' */
/*            WRITE (*,*) 'SN         = ', SN */
/*            WRITE (*,*) 'ANGLES(SN) = ', ANGLES(SN) */
/*            WRITE (*,*) 'SEP        = ', SEP */

/*           Compute the stellar aberration offset and velocity. */

	    zzstelab_(&xmit, acc, vobs, starg, pcorr, dpcorr);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
/*            WRITE (*,*) '||DPCORR|| = ', VNORM(DPCORR) */
/*            WRITE (*,*) '||PCORR||  = ', VNORM(PCORR) */
	    if (pi_() - sep > 1e-12 && sep > 1e-12) {

/*              Compare the results to our analytic formulation. */

		t_zzstelab__(&xmit, acc, vobs, starg, pcorr2, dpcor2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Test the relative position error. */

		chckad_("PCORR", pcorr, "~~/", pcorr2, &c__3, &c_b37, ok, (
			ftnlen)5, (ftnlen)3);

/*              Test the absolute position error. */

		chckad_("PCORR", pcorr, "~", pcorr2, &c__3, &c_b41, ok, (
			ftnlen)5, (ftnlen)1);

/*              Test the relative velocity error. */

		chckad_("DPCORR", dpcorr, "~~/", dpcor2, &c__3, &c_b45, ok, (
			ftnlen)6, (ftnlen)3);

/*              Test the absolute velocity error. */

		chckad_("DPCORR", dpcorr, "~", dpcor2, &c__3, &c_b41, ok, (
			ftnlen)6, (ftnlen)1);
	    }
	    if (pi_() - sep < 1e-6 || sep < 1e-6) {

/*              Compare the results to our numeric formulation. */
/*              Ideally, we would expect EXACT matches, but */
/*              on the PC-CYGWIN_C platform, we don't get them. */
/*              So instead, look for small relative */
/*              errors. */

		t_zzstlabn__(&xmit, acc, vobs, starg, pcorr2, dpcor2);
		chckxc_(&c_false, " ", ok, (ftnlen)1);

/*              Test the relative position and velocity errors. */

		chckad_("PCORR", pcorr, "~~/", pcorr2, &c__3, &c_b55, ok, (
			ftnlen)5, (ftnlen)3);
		chckad_("DPCORR", dpcorr, "~~/", dpcor2, &c__3, &c_b55, ok, (
			ftnlen)6, (ftnlen)3);
	    }
	}

/*        End of time loop. */

    }

/*     End of case loop. */

    t_success__(ok);
    return 0;
} /* f_zzstelab__ */

