/* f_zztanslv.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2000 = 2000;
static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b32 = .5;
static logical c_true = TRUE_;
static integer c__6000 = 6000;
static integer c__2 = 2;
static doublereal c_b176 = 1.;
static doublereal c_b177 = 2.;
static doublereal c_b178 = 3.;
static integer c__3 = 3;
static doublereal c_b200 = 4.;
static doublereal c_b201 = 5.;
static integer c__8 = 8;
static integer c__9 = 9;

/* $Procedure F_ZZTANSLV ( ZZTANSLV tests ) */
/* Subroutine */ int f_zztanslv__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static doublereal step;
    extern /* Subroutine */ int t_tansta__();
    static integer i__, j;
    extern /* Subroutine */ int zztanslv_(U_fp, U_fp, U_fp, logical *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *);
    static integer n;
    extern /* Subroutine */ int tcase_(char *, ftnlen), vpack_(doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static logical cstep;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal start;
    extern /* Subroutine */ int t_success__(logical *);
    static doublereal xpnts[6000]	/* was [3][2000] */;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     cleard_(integer *, doublereal *);
    static logical endflg[2];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    static integer xn;
    extern /* Subroutine */ int gfrefn_();
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    extern integer wncard_(doublereal *);
    static doublereal finish;
    extern /* Subroutine */ int gfstep_();
    extern /* Subroutine */ int ssized_(integer *, doublereal *);
    static doublereal cnvtol;
    extern /* Subroutine */ int gfsstp_(doublereal *);
    static doublereal points[6000]	/* was [3][2000] */, result[2006], 
	    xreslt[2000], tol;

/* $ Abstract */

/*     Exercise the routine ZZTANSLV. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the SPICELIB routine ZZTANSLV. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 30-JUN-2016 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     EXTERNAL routines */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZTANSLV", (ftnlen)10);
/* ********************************************************************** */

/*     ZZTANSLV normal cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [1,10]. Use constant step"
	    " of 0.5.", (ftnlen)72);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 1.;
    finish = 10.;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)208)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints are */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_false, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_false, ok, (ftnlen)18);

/*     Check the returned points. The values are defined only for */
/*     transitions, so the first and last points in the array are */
/*     not checked. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)231)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)232)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)232)];
	}
    }
    i__1 = ((xn << 1) - 2) * 3;
    chckad_("POINTS", &points[3], "~", &xpnts[3], &i__1, &tol, ok, (ftnlen)6, 
	    (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [1,10]. Use a step of 0.5"
	    ", but use the GF step callback.", (ftnlen)95);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = FALSE_;
    gfsstp_(&c_b32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = .5;
    start = 1.;
    finish = 10.;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)280)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints are */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_false, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_false, ok, (ftnlen)18);

/*     Check the returned points. The values are defined only for */
/*     transitions, so the first and last points in the array are */
/*     not checked. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)303)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)304)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)304)];
	}
    }
    i__1 = ((xn << 1) - 2) * 3;
    chckad_("POINTS", &points[3], "~", &xpnts[3], &i__1, &tol, ok, (ftnlen)6, 
	    (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [1,10]. Use constant step"
	    " of 0.99.", (ftnlen)73);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .99;
    start = 1.;
    finish = 10.;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)347)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints are */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_false, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_false, ok, (ftnlen)18);

/*     Check the returned points. The values are defined only for */
/*     transitions, so the first and last points in the array are */
/*     not checked. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)370)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)371)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)371)];
	}
    }
    i__1 = ((xn << 1) - 2) * 3;
    chckad_("POINTS", &points[3], "~", &xpnts[3], &i__1, &tol, ok, (ftnlen)6, 
	    (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [0.5,10.5]. Use constant "
	    "step of 0.5.", (ftnlen)76);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = .5;
    finish = 10.5;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)416)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints ARE */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_true, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_true, ok, (ftnlen)18);

/*     Check the returned points. All of the points should be valid. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)437)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)438)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)438)];
	}
    }
    i__1 = xn * 6;
    chckad_("POINTS", points, "~", xpnts, &i__1, &tol, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [0.5,10.5]. Use a step of"
	    " 0.5, but use the GF step callback.", (ftnlen)99);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = FALSE_;
    gfsstp_(&c_b32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    step = .5;
    start = .5;
    finish = 10.5;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)487)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints ARE */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_true, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_true, ok, (ftnlen)18);

/*     Check the returned points. All of the points should be valid. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)508)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)509)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)509)];
	}
    }
    i__1 = xn * 6;
    chckad_("POINTS", points, "~", xpnts, &i__1, &tol, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Find all state transitions on interval [0.5,10.5]. Use constant "
	    "step of 0.99.", (ftnlen)77);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .99;
    start = .5;
    finish = 10.5;
    cnvtol = 1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. */

    xn = 5;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Check the interval endpoints. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xreslt[(i__2 = i__ - 1) < 2000 && 0 <= i__2 ? i__2 : s_rnge("xreslt", 
		i__2, "f_zztanslv__", (ftnlen)551)] = (doublereal) i__;
    }
    tol = cnvtol * 2;
    i__1 = xn << 1;
    chckad_("RESULT", &result[6], "~", xreslt, &i__1, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     Check the endpoint flags. The first and last endpoints ARE */
/*     not considered to be state transitions. */

    chcksl_("First endpoint flag", endflg, &c_true, ok, (ftnlen)19);
    chcksl_("Last endpoint flag", &endflg[1], &c_true, ok, (ftnlen)18);

/*     Check the returned points. All of the points should be valid. */

    i__1 = xn << 1;
    for (i__ = 1; i__ <= i__1; i__ += 2) {
	for (j = 1; j <= 3; ++j) {
	    xpnts[(i__2 = j + i__ * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)572)] = (
		    doublereal) (i__ + j - 1);
	    xpnts[(i__2 = j + (i__ + 1) * 3 - 4) < 6000 && 0 <= i__2 ? i__2 : 
		    s_rnge("xpnts", i__2, "f_zztanslv__", (ftnlen)573)] = 
		    xpnts[(i__3 = j + i__ * 3 - 4) < 6000 && 0 <= i__3 ? i__3 
		    : s_rnge("xpnts", i__3, "f_zztanslv__", (ftnlen)573)];
	}
    }
    i__1 = xn * 6;
    chckad_("POINTS", points, "~", xpnts, &i__1, &tol, ok, (ftnlen)6, (ftnlen)
	    1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Look for state transition on interval [2.0,2.5]. Use constant st"
	    "ep of 0.5. A transition should be found. The first interval endp"
	    "oint should be considered a non-transition endpoint, while the s"
	    "econd one should be considered a transition.", (ftnlen)236);

/*     In this case the state function is .TRUE. at X = 1.0 and */
/*     nowhere else. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.;
    finish = 2.5;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. We should have a transition */
/*     at the right endpoint of the search interval. */

    xn = 1;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chcksl_("First endpoint flag", endflg, &c_false, ok, (ftnlen)19);

/*     The last endpoint flag should indicate a transition. */

    chcksl_("Last endpoint flag", &endflg[1], &c_true, ok, (ftnlen)18);

/*     Check the result window. The second endpoint is determined */
/*     by the convergence algorithm. We expect the actual result */
/*     only to be close to the expected value. */

    xreslt[0] = start;
    xreslt[1] = start + cnvtol / 2;
    tol = cnvtol * 2;
    chckad_("RESULT", &result[6], "~", xreslt, &c__2, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     The point value at X = 2 is the same as at X = 1. */

    vpack_(&c_b176, &c_b177, &c_b178, xpnts);
    chckad_("POINTS", &points[3], "~~", xpnts, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)2);

/* --- Case: ------------------------------------------------------ */

    tcase_("Look for state transition on interval [2.5,3.0]. Use constant st"
	    "ep of 0.5. A transition should be found. The first interval endp"
	    "oint should be considered a transition endpoint, while the secon"
	    "d one should NOT be considered a transition.", (ftnlen)236);

/*     In this case the state function is .TRUE. at X = 1.0 and */
/*     nowhere else. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = 3.;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Check the number of intervals found. We should have a transition */
/*     at the right endpoint of the search interval. */

    xn = 1;
    n = wncard_(result);
    chcksi_("N", &n, "=", &xn, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chcksl_("First endpoint flag", endflg, &c_true, ok, (ftnlen)19);

/*     The last endpoint flag should indicate a transition. */

    chcksl_("Last endpoint flag", &endflg[1], &c_false, ok, (ftnlen)18);

/*     Check the result window. The second endpoint is determined */
/*     by the convergence algorithm. We expect the actual result */
/*     only to be close to the expected value. */

    xreslt[0] = finish - cnvtol / 2;
    xreslt[1] = finish;
    tol = cnvtol * 2;
    chckad_("RESULT", &result[6], "~", xreslt, &c__2, &tol, ok, (ftnlen)6, (
	    ftnlen)1);

/*     The point value should correspond to the left endpoint. */

    vpack_(&c_b178, &c_b200, &c_b201, xpnts);
    chckad_("POINTS", points, "~~", xpnts, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)2);
/* ********************************************************************** */

/*     ZZTANSLV Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive tolerance.", (ftnlen)23);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = 3.;
    cnvtol = 0.;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);
    cnvtol = -1e-12;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance too small compared to START.", (ftnlen)38);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = 3.;
    cnvtol = start * 1e-20;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Tolerance too small compared to FINISH.", (ftnlen)39);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = 3.;
    cnvtol = finish * 1e-20;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDTOLERANCE)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Non-positive constant step.", (ftnlen)27);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = 0.;
    start = 2.5;
    finish = 3.;
    cnvtol = finish * .001;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDCONSTSTEP)", ok, (ftnlen)23);
    step = -1.;
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDCONSTSTEP)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("Constant step is positive but too small.", (ftnlen)40);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    start = 2.5;
    finish = 3.;
    step = finish * 1e-19;
    cnvtol = finish * .001;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(INVALIDCONSTSTEP)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("START > FINISH.", (ftnlen)15);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = start - 1.;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(BOUNDSOUTOFORDER)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("START = FINISH.", (ftnlen)15);

/*     This is a non-error exception. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = 2.5;
    finish = start;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Points array too small.", (ftnlen)23);
    ssized_(&c__8, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = .5;
    finish = 10.5;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(ARRAYTOOSMALL)", ok, (ftnlen)20);

/* --- Case: ------------------------------------------------------ */

    tcase_("Result window too small.", (ftnlen)24);
    ssized_(&c__9, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    cstep = TRUE_;
    step = .5;
    start = .5;
    finish = 10.5;
    cnvtol = 1e-12;
    cleard_(&c__6000, points);
    zztanslv_((U_fp)t_tansta__, (U_fp)gfstep_, (U_fp)gfrefn_, &cstep, &step, &
	    start, &finish, &cnvtol, result, points, endflg);
    chckxc_(&c_true, "SPICE(WINDOWEXCESS)", ok, (ftnlen)19);

/*     End of error cases. */


/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zztanslv__ */


/*     State function for use by F_ZZTANSLV. */

/*     Let LOWER be defined as */

/*        LOWER = floor( X ). */

/*     The function returns .TRUE. if */

/*        LOWER */

/*     is odd and */

/*        0  <  LOWER  <  X  <  LOWER+1 */
/*           -         -     - */

/*     For all other X, the function returns .FALSE. */

/*     When the function returns .TRUE., the returned */
/*     value of POINT is */

/*        ( LOWER, LOWER+1, LOWER+2 ) */

/*     When the function returns .FALSE., the returned */
/*     value of POINT is */

/*        ( 0, 0, 0 ) */


/* Subroutine */ int t_tansta__(doublereal *x, logical *state, doublereal *
	point)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double d_int(doublereal *);

    /* Local variables */
    doublereal dval;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    doublereal floor;
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    integer ifloor;
    extern logical odd_(integer *);


/*     SPICELIB functions */


/*     Local variables */

    if (*x < 1.) {
	*state = FALSE_;
	cleard_(&c__3, point);
    } else {
	floor = d_int(x);
	ifloor = (integer) floor;
	if (odd_(&ifloor)) {
	    *state = TRUE_;
	    dval = floor;
	} else if (*x == floor) {

/*           IFLOOR is even. */

	    *state = TRUE_;
	    dval = floor - 1.;
	} else {
	    *state = FALSE_;
	}
    }
    if (*state) {
	d__1 = dval + 1.;
	d__2 = dval + 2.;
	vpack_(&dval, &d__1, &d__2, point);
    } else {
	cleard_(&c__3, point);
    }
    return 0;
} /* t_tansta__ */

