/* f_zztanutl.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__4 = 4;
static doublereal c_b73 = 0.;
static doublereal c_b74 = 1.;
static integer c__3 = 3;
static doublereal c_b111 = 2.;
static integer c__2000 = 2000;
static integer c__499 = 499;
static doublereal c_b259 = 1e-6;
static doublereal c_b266 = -1e-6;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c__401 = 401;
static integer c__10 = 10;
static doublereal c_b456 = 1e-5;
static doublereal c_b469 = -1e-5;
static integer c__1000 = 1000;
static integer c__100 = 100;
static integer c__400 = 400;
static integer c__14 = 14;
static integer c__6 = 6;
static doublereal c_b1886 = 100.;
static doublereal c_b1888 = -100.;

/* $Procedure F_ZZTANUTL ( ZZTANUTL/ZZTANGNT tests ) */
/* Subroutine */ int f_zztanutl__(logical *ok)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double atan2(doublereal, doublereal);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    extern /* Subroutine */ int t_elds2z__(integer *, integer *, char *, 
	    integer *, integer *, char *, ftnlen, ftnlen), vadd_(doublereal *,
	     doublereal *, doublereal *);
    static doublereal cdir, limb[9];
    static integer nlat;
    static doublereal sdir, dist, axis[3], xvec[3], yvec[3], zvec[3];
    static integer nlon;
    extern /* Subroutine */ int vscl_(doublereal *, doublereal *, doublereal *
	    );
    static doublereal proj[3];
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    extern doublereal vdot_(doublereal *, doublereal *), vsep_(doublereal *, 
	    doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *), zztanini_(integer *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, doublereal *, doublereal *), zztansta_(
	    doublereal *, logical *, doublereal *), zztangnt_(integer *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal a, b, c__, d__;
    static integer i__, j;
    extern /* Subroutine */ int zztanutl_(integer *, doublereal *, integer *, 
	    integer *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, logical *, doublereal *)
	    ;
    extern integer cardd_(doublereal *);
    static integer n;
    static char label[32];
    static doublereal r__, angle, radii[3];
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), tcase_(char *, ftnlen);
    static integer shape;
    extern /* Subroutine */ int vpack_(doublereal *, doublereal *, doublereal 
	    *, doublereal *);
    static doublereal pnear[3], theta;
    extern /* Subroutine */ int repmd_(char *, char *, doublereal *, integer *
	    , char *, ftnlen, ftnlen, ftnlen), edpnt_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), vlcom_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static char title[240];
    static integer curve;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal point[3], vtemp[3];
    static integer ncuts, nsurf;
    extern /* Subroutine */ int ucrss_(doublereal *, doublereal *, doublereal 
	    *), dskxv_(logical *, char *, integer *, integer *, doublereal *, 
	    char *, integer *, doublereal *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen), vprjp_(doublereal *, doublereal *, 
	    doublereal *), t_success__(logical *);
    static integer nxpts;
    extern /* Subroutine */ int vrotv_(doublereal *, doublereal *, doublereal 
	    *, doublereal *), chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen);
    static doublereal dp;
    extern /* Subroutine */ int psv2pl_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    extern doublereal pi_(void);
    static doublereal et;
    static integer handle;
    extern /* Subroutine */ int delfil_(char *, ftnlen);
    static doublereal lt;
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), bodvcd_(integer *, char *, 
	    integer *, integer *, doublereal *, ftnlen), edlimb_(doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *), chcksd_(
	    char *, doublereal *, char *, doublereal *, doublereal *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static integer trgcde;
    static doublereal dtheta, refvec[3];
    static integer bodyid, fixfid;
    static doublereal srcrad;
    static integer srfbod[4];
    static doublereal center[3];
    static char fixref[32];
    extern /* Subroutine */ int t_pck08__(char *, logical *, logical *, 
	    ftnlen);
    static char target[32];
    static doublereal normal[3], offset[3];
    static char srfnms[32*4];
    static doublereal lpoint[3], plnvec[3], raydir[3], points[6000]	/* 
	    was [3][2000] */, rcross, result[2006];
    extern logical exists_(char *, ftnlen);
    static doublereal rpoint[3], schstp, soltol, sunrad[3], tanpnt[3], sunlpt[
	    3], vertex[3];
    static integer srfids[4], srflst[100], surfid;
    static logical ocultd;
    extern /* Subroutine */ int furnsh_(char *, ftnlen), pcpool_(char *, 
	    integer *, char *, ftnlen, ftnlen), pipool_(char *, integer *, 
	    integer *, ftnlen), namfrm_(char *, integer *, ftnlen), clpool_(
	    void), unload_(char *, ftnlen), ssized_(integer *, doublereal *), 
	    inelpl_(doublereal *, doublereal *, integer *, doublereal *, 
	    doublereal *), nplnpt_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), chcksi_(char *, integer *, char *, 
	    integer *, integer *, logical *, ftnlen, ftnlen), vminus_(
	    doublereal *, doublereal *), natspk_(char *, logical *, integer *,
	     ftnlen), natpck_(char *, logical *, logical *, ftnlen), spkpos_(
	    char *, doublereal *, char *, char *, char *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen), spkuef_(integer *),
	     kclear_(void);
    extern logical odd_(integer *);
    static doublereal dir[3];
    extern doublereal dpr_(void);
    static doublereal cut[4], tol, npt[3];
    extern /* Subroutine */ int t_torus__(integer *, integer *, char *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, char *, ftnlen, ftnlen);
    static doublereal xpt1[3], xpt2[3];

/* $ Abstract */

/*     Exercise the entry points of routine ZZTANUTL and the */
/*     separate routine ZZTANGNT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */

/*     File: dsk.inc */


/*     Version 1.0.0 05-FEB-2016 (NJB) */

/*     Maximum size of surface ID list. */


/*     End of include file dsk.inc */


/*     File: zzdsk.inc */


/*     Version 4.0.0 13-NOV-2015 (NJB) */

/*        Changed parameter LBTLEN to CVTLEN. */
/*        Added parameter LMBCRV. */

/*     Version 3.0.0 05-NOV-2015 (NJB) */

/*        Added parameters */

/*           CTRCOR */
/*           ELLCOR */
/*           GUIDED */
/*           LBTLEN */
/*           PNMBRL */
/*           TANGNT */
/*           TMTLEN */
/*           UMBRAL */

/*     Version 2.0.0 04-MAR-2015 (NJB) */

/*        Removed declaration of parameter SHPLEN. */
/*        This name is already in use in the include */
/*        file gf.inc. */

/*     Version 1.0.0 26-JAN-2015 (NJB) */


/*     Parameters supporting METHOD string parsing: */


/*     Local method length. */


/*     Length of sub-point type string. */


/*     Length of curve type string. */


/*     Limb type parameter codes. */


/*     Length of terminator type string. */


/*     Terminator type and limb parameter codes. */


/*     Length of aberration correction locus string. */


/*     Aberration correction locus codes. */


/*     End of include file zzdsk.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the entry points of the SPICELIB routine */
/*     ZZTANUTL: */


/*        ZZTANINI:   Initialize tangent ray utilities for limb or */
/*                    terminator computation. Terminators may be */
/*                    umbral or penumbral. */

/*        ZZTANSTA:   Compute state of ray for a given input angle. */
/*                    State is "occulted" or "not occulted." When */
/*                    the state is occulted, return the ray-surface */
/*                    intercept point. */

/*     This routine also tests the related routine */

/*        ZZTANGNT */


/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     E.D. Wright      (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 23-AUG-2016 (NJB) (EDW) */

/* -& */

/*     SPICELIB functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved values */

/*     Save variables in order to avoid stack room problems. */


/*     Initial values */


/*     Open the test family. */

    topen_("F_ZZTANUTL", (ftnlen)10);
/* ********************************************************************** */

/*     Setup for ZZTANINI/ZZTANSTA tests */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Set-up: create PCK.", (ftnlen)19);
    if (exists_("zztanutl.tpc", (ftnlen)12)) {
	delfil_("zztanutl.tpc", (ftnlen)12);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    t_pck08__("zztanutl.tpc", &c_true, &c_true, (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Setup: create DSK files.", (ftnlen)24);

/*     For Mars, surface 1 is the "main" surface. */

    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    bodyid = trgcde;
    surfid = 1;
    nlon = 220;
    nlat = 110;
    if (exists_("zztanutl_0.bds", (ftnlen)14)) {
	delfil_("zztanutl_0.bds", (ftnlen)14);
    }
    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zztanutl_0.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Load main Mars DSK. */

    furnsh_("zztanutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Mars has slightly lower resolution. */

    bodyid = trgcde;
    surfid = 2;
    nlon = 190;
    nlat = 95;
    if (exists_("zztanutl_1.bds", (ftnlen)14)) {
	delfil_("zztanutl_1.bds", (ftnlen)14);
    }

/*     Create and load the second DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zztanutl_1.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 1 for Phobos is high-res. */

    bodyid = 401;
    surfid = 1;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 200;
    nlat = 100;
    if (exists_("zztanutl_2.bds", (ftnlen)14)) {
	delfil_("zztanutl_2.bds", (ftnlen)14);
    }

/*     Create and load the first Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zztanutl_2.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Surface 2 for Phobos is lower-res. */

    bodyid = 401;
    surfid = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    nlon = 80;
    nlat = 40;
    if (exists_("zztanutl_3.bds", (ftnlen)14)) {
	delfil_("zztanutl_3.bds", (ftnlen)14);
    }

/*     Create and load the second Phobos DSK. */

    t_elds2z__(&bodyid, &surfid, fixref, &nlon, &nlat, "zztanutl_3.bds", (
	    ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_3.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Set up a surface name-ID map. */

    srfbod[0] = 499;
    srfids[0] = 1;
    s_copy(srfnms, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[1] = 499;
    srfids[1] = 2;
    s_copy(srfnms + 32, "low-res", (ftnlen)32, (ftnlen)7);
    srfbod[2] = 401;
    srfids[2] = 1;
    s_copy(srfnms + 64, "high-res", (ftnlen)32, (ftnlen)8);
    srfbod[3] = 401;
    srfids[3] = 2;
    s_copy(srfnms + 96, "low-res", (ftnlen)32, (ftnlen)7);
    pcpool_("NAIF_SURFACE_NAME", &c__4, srfnms, (ftnlen)17, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_CODE", &c__4, srfids, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    pipool_("NAIF_SURFACE_BODY", &c__4, srfbod, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     ZZTANUTL Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Call ZZTANUTL directly.", (ftnlen)23);
    zztanutl_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &angle, &ocultd, point);
    chckxc_(&c_true, "SPICE(BOGUSENTRY)", ok, (ftnlen)17);
/* ********************************************************************** */

/*     ZZTANINI Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: Zero axis vector.", (ftnlen)27);
    curve = 0;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    cleard_(&c__3, axis);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: Zero half-plane reference vector.", (ftnlen)43);
    curve = 0;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    cleard_(&c__3, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: invalid curve type.", (ftnlen)29);
    curve = -1;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(BADCURVETYPE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: invalid source radius", (ftnlen)31);
    curve = 1;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(BADSOURCERADIUS)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: axis and reference vector are linearly dependent.", (
	    ftnlen)59);
    curve = 1;
    srcrad = 6.96e5;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vscl_(&c_b111, axis, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: invalid target shape.", (ftnlen)31);
    curve = 1;
    srcrad = 6.96e5;
    shape = -1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(BADSHAPE)", ok, (ftnlen)15);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANINI: missing PCK data.", (ftnlen)27);
    curve = 1;
    srcrad = 6.96e5;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    clpool_();
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(KERNELVARNOTFOUND)", ok, (ftnlen)24);
    furnsh_("zztanutl.tpc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     ZZTANSTA Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANSTA: no DSKs loaded.", (ftnlen)25);
    curve = 1;
    srcrad = 6.96e5;
    shape = 2;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_3.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztansta_(&c_b73, &ocultd, point);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANSTA: no DSK data for target", (ftnlen)32);
    curve = 1;
    srcrad = 6.96e5;
    shape = 2;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_true, "SPICE(NOLOADEDDSKFILES)", ok, (ftnlen)23);
    unload_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
/* ********************************************************************** */

/*     ZZTANGNT Error cases */

/* ********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Zero axis vector.", (ftnlen)27);
    curve = 1;
    srcrad = 6.96e5;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    cleard_(&c__3, axis);
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Zero plane reference vector.", (ftnlen)38);
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    cleard_(&c__3, plnvec);
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(ZEROVECTOR)", ok, (ftnlen)17);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Bad curve type.", (ftnlen)25);
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    curve = -1;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(BADCURVETYPE)", ok, (ftnlen)19);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Bad source radius.", (ftnlen)28);
    curve = 1;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(BADSOURCERADIUS)", ok, (ftnlen)22);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Axis and plane reference vector are linearly dependent."
	    , (ftnlen)65);
    curve = 1;
    srcrad = 6.96e5;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vscl_(&c_b111, axis, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT: Bad target shape.", (ftnlen)27);
    curve = 1;
    srcrad = 0.;
    shape = -1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_true, "SPICE(BADSOURCERADIUS)", ok, (ftnlen)22);
/* ********************************************************************** */

/*     ZZTANINI/ZZTANSTA normal cases */

/* ********************************************************************** */

/*     These cases are combined because the two entry points */
/*     work in a cooperative fashion, and because the results of */
/*     initialization alone are not observable. */

/* ********************************************************************** */

/*     ELLIPSOID target shape */

/* ********************************************************************** */

/*     LIMB cases */


/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using ellipsoidal target shape. (Mar"
	    "s)", (ftnlen)66);
    curve = 0;
    srcrad = 0.;
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near limb point in cutting half-plane of prev"
	    "ious case. Non-occultation case. (Mars)", (ftnlen)103);

/*     Find limb point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    edlimb_(&a, &b, &c__, axis, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, lpoint);
    } else {
	vequ_(xpt2, lpoint);
    }

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 mm above the */
/*     surface. */

    vlcom_(&c_b74, lpoint, &c_b259, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near limb point in cutting half-plane of prev"
	    "ious case. Occultation case. (Mars)", (ftnlen)99);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 mm below the */
/*     surface. */

    vlcom_(&c_b74, lpoint, &c_b266, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("POINT", point, "~~", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)2);

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: create limb point matching that from the previous"
	    " case. (Mars)", (ftnlen)77);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set absolute tolerance to 1 m. (Can use 10 cm on PC/Linux). */

    vequ_(points, tanpnt);
    tol = .001;
    chckad_("TANPNT", tanpnt, "~~", lpoint, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)2);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(tanpnt, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("TANPNT", tanpnt, "~~", npt, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    2);

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(tanpnt, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, tanpnt, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(tanpnt, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Repeat the test with a different ellipsoidal target. Initialize "
	    "the system for Phobos.", (ftnlen)86);
    curve = 0;
    srcrad = 0.;
    shape = 1;
    trgcde = 401;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near limb point in cutting half-plane of prev"
	    "ious case. Non-occultation case. (Phobos)", (ftnlen)105);

/*     Find limb point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];
    edlimb_(&a, &b, &c__, axis, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (vdot_(xpt1, plnvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, lpoint);
    } else {
	vequ_(xpt2, lpoint);
    }

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-ellipsoid point. */

    vlcom_(&c_b74, lpoint, &c_b259, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near limb point in cutting half-plane of prev"
	    "ious case. Occultation case. (Phobos)", (ftnlen)101);
    vlcom_(&c_b74, lpoint, &c_b266, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-11;
    chckad_("POINT", point, "~~", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)2);

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: create limb point matching that from the previous"
	    " case. (Phobos)", (ftnlen)79);
    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*     Set absolute tolerance to 1 m. (Can use 10 cm on PC/Linux). */

    vequ_(points, tanpnt);
    tol = .001;
    chckad_("TANPNT", tanpnt, "~~", lpoint, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)2);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(tanpnt, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("TANPNT", tanpnt, "~~", npt, &c__3, &tol, ok, (ftnlen)6, (ftnlen)
	    2);

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(tanpnt, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, tanpnt, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-6;
    chckad_("PNEAR", pnear, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(tanpnt, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     UMBRAL TERMINATOR cases */


/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using ellipsoidal targe"
	    "t shape. (Mars)", (ftnlen)79);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 1;
    srcrad = sunrad[0];
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near umbral terminator point in cutting half-"
	    "plane of previous case. Non-occultation case. (Mars)", (ftnlen)
	    116);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-ellipsoid point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near umbral terminator point in cutting half-"
	    "plane of previous case. Occultation case. (Mars)", (ftnlen)112);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     Repeat previous tests, this time using Phobos as the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using ellipsoidal targe"
	    "t shape. (Phobos)", (ftnlen)81);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 1;
    srcrad = sunrad[0];
    shape = 1;
    trgcde = 401;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near umbral terminator point in cutting half-"
	    "plane of previous case. Non-occultation case. (Phobos)", (ftnlen)
	    118);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-ellipsoid point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near umbral terminator point in cutting half-"
	    "plane of previous case. Occultation case. (Phobos)", (ftnlen)114);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     PENUMBRAL TERMINATOR cases */


/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using ellipsoidal ta"
	    "rget shape. (Mars)", (ftnlen)82);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 2;
    srcrad = sunrad[0];
    shape = 1;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near penumbral terminator point in cutting ha"
	    "lf-plane of previous case. Non-occultation case. (Mars)", (ftnlen)
	    119);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-ellipsoid point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);

/*     For the penumbral case, the sun limb point should be */
/*     on the -Y side of the axis. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the axis bounding the */
/*        half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near penumbral terminator  point in cutting h"
	    "alf-plane of previous case. Occultation case. (Mars)", (ftnlen)
	    116);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

/*     For the penumbral case, the sun limb point should be */
/*     on the -Y side of the axis. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the axis bounding the */
/*        half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     Repeat previous tests, this time using Phobos as the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using ellipsoidal ta"
	    "rget shape. (Phobos)", (ftnlen)84);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 2;
    srcrad = sunrad[0];
    shape = 1;
    trgcde = 401;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near penumbral terminator point in cutting ha"
	    "lf-plane of previous case. Non-occultation case. (Phobos)", (
	    ftnlen)121);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-ellipsoid point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);

/*     For the penumbral case, the sun limb point should be */
/*     on the -Y side of the axis. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the axis bounding the */
/*        half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near penumbral terminator  point in cutting h"
	    "alf-plane of previous case. Occultation case. (Phobos)", (ftnlen)
	    118);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, lpoint, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

/*     For the penumbral case, the sun limb point should be */
/*     on the -Y side of the axis. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the axis bounding the */
/*        half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the ellipsoid. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be on the ellipsoid. Map the */
/*     point to a point that is on the ellipsoid and check the */
/*     distance between the two. */

    edpnt_(point, &a, &b, &c__, npt);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);
/* ********************************************************************** */

/*     DSK target shape */

/* ********************************************************************** */

/*     LIMB cases */


/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Empty surfac"
	    "e list. (Mars)", (ftnlen)78);

/*     Load DSKs. */

    furnsh_("zztanutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_3.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 499;
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Non-occultation case. (Mars)", (ftnlen)107);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm above the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Occultation case. (Mars)", (ftnlen)103);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Specify both"
	    " surfaces in surface list. (Mars)", (ftnlen)97);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK limb point to that found in previous "
	    "case. (Mars)", (ftnlen)76);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("LPOINT", lpoint, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Use surface "
	    "2. (Mars)", (ftnlen)73);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 499;
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Non-occultation case. Use surface 2. (Mars)", (
	    ftnlen)122);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm above the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Occultation case.  Use surface 2. (Mars)", (ftnlen)
	    119);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     Use Phobos as the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Empty surfac"
	    "e list. (Phobos)", (ftnlen)80);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 401;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Non-occultation case. (Phobos)", (ftnlen)109);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm above the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Occultation case. (Phobos)", (ftnlen)105);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Specify both"
	    " surfaces in surface list. (Phobos)", (ftnlen)99);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 401;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK limb point to that found in previous "
	    "case. (Phobos)", (ftnlen)78);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, lpoint);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("LPOINT", lpoint, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)6, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize limb computation using DSK target shape. Use surface "
	    "2. (Phobos)", (ftnlen)75);

/*     Use all surfaces (empty surface list). */

    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 401;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e4;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Non-occultation case. Use surface 2. (Phobos)", (
	    ftnlen)124);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm above the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK limb point in cutting half-plane of "
	    "previous case. Occultation case.  Use surface 2. (Phobos)", (
	    ftnlen)121);

/*     Generate off-ellipsoid point RPOINT. RPOINT is 1 cm below the */
/*     surface. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, raydir);
    angle = vsep_(axis, raydir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)2);

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(axis, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, axis, offset);
    dp = vdot_(offset, raydir);
    chcksd_("AXIS dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     DSK UMBRAL TERMINATOR cases */



/*     Mars is the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Use empty surface list. (Mars)", (ftnlen)95);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 499;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Non-occultation case. Empty surface "
	    "list. (Mars)", (ftnlen)140);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Occultation case. Empty surface list"
	    ". (Mars)", (ftnlen)136);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Specify both surfaces in surface list. (Mars)", (ftnlen)110);

/*     Use all surfaces (empty surface list). */

    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK umbral terminator point to that found"
	    " in previous case. (Mars)", (ftnlen)89);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, point);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("POINT", point, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Use surface 2. (Mars)", (ftnlen)86);

/*     Use all surfaces (empty surface list). */

    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 499;
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Non-occultation case. Use surface 2."
	    " (Mars)", (ftnlen)135);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Occultation case. Use surface 2. (Ma"
	    "rs)", (ftnlen)131);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-10;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     Phobos is the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Use empty surface list. (Phobos)", (ftnlen)97);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 401;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Non-occultation case. Empty surface "
	    "list. (Phobos)", (ftnlen)142);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Occultation case. Empty surface list"
	    ". (Phobos)", (ftnlen)138);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Specify both surfaces in surface list. (Phobos)", (ftnlen)112);

/*     Use all surfaces (empty surface list). */

    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    trgcde = 401;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK umbral terminator point to that found"
	    " in previous case. (Phobos)", (ftnlen)91);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, point);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("POINT", point, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize umbral terminator computation using DSK target shape."
	    " Use surface 2. (Phobos)", (ftnlen)88);

/*     Use all surfaces (empty surface list). */

    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 401;
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Non-occultation case. Use surface 2."
	    " (Phobos)", (ftnlen)137);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK umbral terminator point in cutting h"
	    "alf-plane of previous case. Occultation case. Use surface 2. (Ph"
	    "obos)", (ftnlen)133);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) > 0.) {

/*        XPT1 is in the half-plane defined by AXIS and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     DSK PENUMBRAL TERMINATOR cases */



/*     Mars is the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Use empty surface list. (Mars)", (ftnlen)98);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 499;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Non-occultation case. Empty surfa"
	    "ce list. (Mars)", (ftnlen)143);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Occultation case. Empty surface l"
	    "ist. (Mars)", (ftnlen)139);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Specify both surfaces in surface list. (Mars)", (ftnlen)113);

/*     Use all surfaces (empty surface list). */

    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    s_copy(target, "MARS", (ftnlen)32, (ftnlen)4);
    trgcde = 499;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK penumbral terminator point to that fo"
	    "und in previous case. (Mars)", (ftnlen)92);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, point);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("POINT", point, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Use surface 2. (Mars)", (ftnlen)89);

/*     Use all surfaces (empty surface list). */

    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 499;
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_MARS", (ftnlen)32, (ftnlen)8);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Non-occultation case. Use surface"
	    " 2. (Mars)", (ftnlen)138);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__499, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Occultation case. Use surface 2. "
	    "(Mars)", (ftnlen)134);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-7;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);


/*     Phobos is the target. */



/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Use empty surface list. (Phobos)", (ftnlen)100);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 401;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Non-occultation case. Empty surfa"
	    "ce list. (Phobos)", (ftnlen)145);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Occultation case. Empty surface l"
	    "ist. (Phobos)", (ftnlen)141);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Specify both surfaces in surface list. (Phobos)", (ftnlen)115)
	    ;

/*     Use all surfaces (empty surface list). */

    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    s_copy(target, "PHOBOS", (ftnlen)32, (ftnlen)6);
    trgcde = 401;
    nsurf = 2;
    srflst[0] = 1;
    srflst[1] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("ZZTANGNT test: compare DSK penumbral terminator point to that fo"
	    "und in previous case. (Phobos)", (ftnlen)94);

/*     Find limb point in cutting half-plane of previous case. */

    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, point);

/*     We expect an exact match. */

    tol = 0.;
    chckad_("POINT", point, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)5, (
	    ftnlen)3);

/* --- Case: ------------------------------------------------------ */

    tcase_("Initialize penumbral terminator computation using DSK target sha"
	    "pe. Use surface 2. (Phobos)", (ftnlen)91);

/*     Use all surfaces (empty surface list). */

    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 401;
    nsurf = 1;
    srflst[0] = 2;
    s_copy(fixref, "IAU_PHOBOS", (ftnlen)32, (ftnlen)10);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    d__ = 1e8;
    d__1 = d__ * 2;
    d__2 = d__ * 3;
    vpack_(&d__, &d__1, &d__2, axis);
    vpack_(&c_b73, &c_b74, &c_b74, plnvec);
    zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Non-occultation case. Use surface"
	    " 2. (Phobos)", (ftnlen)140);

/*     Find umbral point in cutting half-plane of previous case. */

    bodvcd_(&c__401, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    a = radii[0];
    b = radii[1];
    c__ = radii[2];

/*     We can use ZZTANGNT to "suggest" a solution */
/*     point. */

    ssized_(&c__2000, result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    schstp = 4.;
    soltol = 1e-15;
    zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &et, 
	    plnvec, axis, &schstp, &soltol, result, points);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Note that RESULT is a cell, not a window. */

    n = cardd_(result);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("N", &n, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    vequ_(points, tanpnt);

/*     Get unit direction vector parallel to PLNVEC. Generate */
/*     off-DSK surface point RPOINT. RPOINT is 1 cm above the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b456, yvec, rpoint);

/*     To generate a near-tangent ray passing through RPOINT, */
/*     we need to find the ray's vertex on the source. We can */
/*     do this by finding the limb on the source as seen from */
/*     RPOINT. */

    vsub_(rpoint, axis, offset);
    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Generate basis vectors defined by AXIS (primary) */
/*     and PLNVEC (secondary). */

    ucrss_(axis, plnvec, zvec);
    vhat_(axis, xvec);
    ucrss_(zvec, xvec, yvec);
    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should miss the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_false, ok, (ftnlen)6);

/* --- Case: ------------------------------------------------------ */

    tcase_("Check state of ray near DSK penumbral terminator point in cuttin"
	    "g half-plane of previous case. Occultation case. Use surface 2. "
	    "(Phobos)", (ftnlen)136);

/*     Generate off-DSK point RPOINT. RPOINT is 1 cm below the */
/*     terminator point found by ZZTANGNT. */

    vlcom_(&c_b74, tanpnt, &c_b469, yvec, rpoint);
    vsub_(rpoint, axis, offset);

/*     Find limb on the source (sun) as seen from the tangent point. */

    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    psv2pl_(origin, axis, plnvec, cut);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Caution: LIMB and CUT are expressed with respect to different */
/*     origins. Shift LIMB so its center is an offset from the */
/*     target center. */

    vadd_(axis, limb, vtemp);
    vequ_(vtemp, limb);
    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vequ_(cut, normal);

/*     The basis from the previous case still applies. */

    if (vdot_(xpt1, yvec) < 0.) {

/*        XPT1 is on the opposite side of the half-plane defined by AXIS */
/*        and PLNVEC. */

	vequ_(xpt1, sunlpt);
    } else {
	vequ_(xpt2, sunlpt);
    }

/*     Now generate the ray's direction vector. */

    vsub_(rpoint, sunlpt, raydir);

/*     We can't just use VSEP to find the rotation angle of the */
/*     tangent ray with respect to AXIS, because the angle may */
/*     be larger than pi. Compute the angle from the components */
/*     of RAYDIR. */

    cdir = vdot_(raydir, xvec);
    sdir = vdot_(raydir, yvec);
    angle = atan2(sdir, cdir);

/*     Find intersection state, given the angle between AXIS and RAYDIR. */

    zztansta_(&angle, &ocultd, point);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The ray should hit the DSK surface. */

    chcksl_("OCULTD", &ocultd, &c_true, ok, (ftnlen)6);

/*     The returned point should be very close to the DSK surface. Map */
/*     the point to a point that is on the surface and check the */
/*     distance between the two. */

    vscl_(&c_b111, point, vertex);
    vminus_(vertex, dir);
    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, vertex, dir, 
	    npt, &found, (ftnlen)32, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
    tol = 1e-10;
    chckad_("POINT", point, "~~/", npt, &c__3, &tol, ok, (ftnlen)5, (ftnlen)3)
	    ;

/*     The returned point should be in the plane containing */
/*     the cutting half-plane. */

    vprjp_(point, cut, proj);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PROJ", proj, "~~/", point, &c__3, &tol, ok, (ftnlen)4, (ftnlen)3)
	    ;

/*     The returned point should be the correct half-plane. */

    dp = vdot_(point, yvec);
    tol = 1e-10;
    chcksd_("YVEC dot", &dp, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);

/*     The returned point should be on the line containing the ray. */

    nplnpt_(sunlpt, raydir, point, pnear, &dist);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    tol = 1e-5;
    chckad_("PNEAR", pnear, "~~/", point, &c__3, &tol, ok, (ftnlen)5, (ftnlen)
	    3);

/*     The returned point should be on the correct side of the vertex of */
/*     the line containing the ray. */

    vsub_(point, sunlpt, offset);
    d__ = vdot_(offset, raydir);
    chcksd_("AXIS dot", &d__, ">", &c_b73, &tol, ok, (ftnlen)8, (ftnlen)1);
/* *********************************************************************** */

/*     TORUS tests */

/* *********************************************************************** */

/* --- Case: ------------------------------------------------------ */

    tcase_("Torus test setup", (ftnlen)16);

/*     Create and load Nat's SPK and PCK. */

    if (exists_("nat.bsp", (ftnlen)7)) {
	delfil_("nat.bsp", (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    natspk_("nat.bsp", &c_true, &handle, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (exists_("nat.tpc", (ftnlen)7)) {
	delfil_("nat.tpc", (ftnlen)7);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    natpck_("nat.tpc", &c_true, &c_true, (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     Create DSK containing three nested tori. The object is */
/*     centered at body Alpha. */

    if (exists_("zztanutl_4.bds", (ftnlen)14)) {
	delfil_("zztanutl_4.bds", (ftnlen)14);
    }
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_("FIXFID", &fixfid, ">", &c__0, &c__0, ok, (ftnlen)6, (ftnlen)1);
    r__ = 1.1e5;
    rcross = 1e4;
    vpack_(&c_b73, &c_b73, &c_b73, center);
    vpack_(&c_b74, &c_b73, &c_b73, normal);
    surfid = 1;
    t_torus__(&c__1000, &surfid, fixref, &c__100, &c__400, &r__, &rcross, 
	    center, normal, "zztanutl_4.bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = 8e4;
    vpack_(&c_b73, &c_b73, &c_b73, center);
    vpack_(&c_b74, &c_b73, &c_b73, normal);
    surfid = 2;
    t_torus__(&c__1000, &surfid, fixref, &c__100, &c__400, &r__, &rcross, 
	    center, normal, "zztanutl_4.bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    r__ = 5e4;
    vpack_(&c_b73, &c_b73, &c_b73, center);
    vpack_(&c_b74, &c_b73, &c_b73, normal);
    surfid = 3;
    t_torus__(&c__1000, &surfid, fixref, &c__100, &c__400, &r__, &rcross, 
	    center, normal, "zztanutl_4.bds", (ftnlen)32, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    furnsh_("zztanutl_4.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);


/*     NESTED TORUS LIMB CASES */



/* --- Case: ------------------------------------------------------ */

    tcase_("Limb computation using nestedtorus target shape. Use empty surfa"
	    "ce list. (ALPHA)", (ftnlen)80);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 0;
    srcrad = 0.;
    shape = 2;
    trgcde = 1000;
    s_copy(target, "ALPHA", (ftnlen)32, (ftnlen)5);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    spkpos_("SUN", &et, fixref, "NONE", target, axis, &lt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)4, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvcd_(&trgcde, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vhat_(axis, zvec);
    frame_(zvec, refvec, vtemp);

/*     The ZZTANINI call requires a plane reference vector, */
/*     so this call must be made for each cutting half-plane. */

    ncuts = 10;
    dtheta = pi_() * 2 / ncuts;
    i__1 = ncuts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	theta = (i__ - 1) * dtheta;

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Nested torus limb test: THETA (deg) = #.", (ftnlen)240,
		 (ftnlen)40);
	d__1 = theta * dpr_();
	repmd_(title, "#", &d__1, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	vrotv_(refvec, axis, &theta, plnvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ssized_(&c__2000, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	schstp = 1e-4;
	soltol = 1e-15;
	zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis, &schstp, &soltol, result, points);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Note that RESULT is a cell, not a window. */

	n = cardd_(result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect to find six limb points in each cutting half-plane. */

	chcksi_("N", &n, "=", &c__6, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*        Check the points and the angles in the RESULT array. */

	i__2 = n;
	for (j = 1; j <= i__2; ++j) {
	    vequ_(&points[(i__3 = j * 3 - 3) < 6000 && 0 <= i__3 ? i__3 : 
		    s_rnge("points", i__3, "f_zztanutl__", (ftnlen)6444)], 
		    tanpnt);

/*           The returned point should be very close to the DSK surface. */
/*           Map the point to a point that is on the surface and check */
/*           the distance between the two. */

	    if (odd_(&j)) {
		vlcom_(&c_b74, tanpnt, &c_b1886, plnvec, vertex);
		vminus_(plnvec, dir);
	    } else {
		vlcom_(&c_b74, tanpnt, &c_b1888, plnvec, vertex);
		vequ_(plnvec, dir);
	    }
	    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, 
		    vertex, dir, npt, &found, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    tol = 1e-10;
	    s_copy(label, "TANPNT #", (ftnlen)32, (ftnlen)8);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, tanpnt, "~~/", npt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)3);

/*           The returned point should be in the plane containing */
/*           the cutting half-plane. */

	    psv2pl_(origin, axis, plnvec, cut);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vprjp_(tanpnt, cut, proj);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-7;
	    s_copy(label, "PROJ #", (ftnlen)32, (ftnlen)6);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, proj, "~~", tanpnt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)2);

/*           The returned point should be in the correct half-plane. */

	    dp = vdot_(tanpnt, plnvec);
	    tol = 1e-10;
	    s_copy(label, "YVEC dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           Compute the direction vector of the current tangent ray. */

	    vsub_(tanpnt, axis, raydir);

/*           The returned point should be on the line containing the */
/*           ray. */

	    nplnpt_(axis, raydir, tanpnt, pnear, &dist);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    s_copy(label, "PNEAR #", (ftnlen)32, (ftnlen)7);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, pnear, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)32, 
		    (ftnlen)3);

/*           The returned point should be on the correct side of the */
/*           vertex of the line containing the ray. */

	    vsub_(tanpnt, axis, offset);
	    dp = vdot_(offset, raydir);
	    s_copy(label, "AXIS dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           The angle between RAYDIR and AXIS should match the */
/*           Jth element of RESULT. */

	    angle = vsep_(axis, raydir);
/*             WRITE (*,*) '-------' */
/*             WRITE (*,*) 'ANGLE  = ', ANGLE */
/*             WRITE (*,*) 'RESULT = ', RESULT(J) */
	    s_copy(label, "RESULT(#)", (ftnlen)32, (ftnlen)9);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    chcksd_(label, &result[(i__3 = j + 5) < 2006 && 0 <= i__3 ? i__3 :
		     s_rnge("result", i__3, "f_zztanutl__", (ftnlen)6562)], 
		    "~", &angle, &tol, ok, (ftnlen)32, (ftnlen)1);
	}
    }


/*     NESTED TORUS UMBRAL TERMINATOR CASES */



/* --- Case: ------------------------------------------------------ */

    tcase_("Umbral terminator computation using nestedtorus target shape. Us"
	    "e empty surface list. (ALPHA)", (ftnlen)93);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 1;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 1000;
    s_copy(target, "ALPHA", (ftnlen)32, (ftnlen)5);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    spkpos_("SUN", &et, fixref, "NONE", target, axis, &lt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)4, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvcd_(&trgcde, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vhat_(axis, zvec);
    frame_(zvec, refvec, vtemp);

/*     The ZZTANINI call requires a plane reference vector, */
/*     so this call must be made for each cutting half-plane. */

    ncuts = 10;
    dtheta = pi_() * 2 / ncuts;
    i__1 = ncuts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	theta = (i__ - 1) * dtheta;

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Nested torus umbral terminator test: THETA (deg) = #.",
		 (ftnlen)240, (ftnlen)53);
	d__1 = theta * dpr_();
	repmd_(title, "#", &d__1, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	vrotv_(refvec, axis, &theta, plnvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ssized_(&c__2000, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	schstp = 1e-4;
	soltol = 1e-15;
	zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis, &schstp, &soltol, result, points);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Note that RESULT is a cell, not a window. */

	n = cardd_(result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect to find six umbral terminator points in each cutting */
/*        half-plane. */

	chcksi_("N", &n, "=", &c__6, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*        Check the points and the angles in the RESULT array. */

	i__2 = n;
	for (j = 1; j <= i__2; ++j) {
	    vequ_(&points[(i__3 = j * 3 - 3) < 6000 && 0 <= i__3 ? i__3 : 
		    s_rnge("points", i__3, "f_zztanutl__", (ftnlen)6678)], 
		    tanpnt);

/*           The returned point should be very close to the DSK surface. */
/*           Map the point to a point that is on the surface and check */
/*           the distance between the two. */

	    if (odd_(&j)) {
		vlcom_(&c_b74, tanpnt, &c_b1886, plnvec, vertex);
		vminus_(plnvec, dir);
	    } else {
		vlcom_(&c_b74, tanpnt, &c_b1888, plnvec, vertex);
		vequ_(plnvec, dir);
	    }
	    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, 
		    vertex, dir, npt, &found, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    tol = 1e-10;
	    s_copy(label, "TANPNT #", (ftnlen)32, (ftnlen)8);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, tanpnt, "~~/", npt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)3);

/*           The returned point should be in the plane containing */
/*           the cutting half-plane. */

	    psv2pl_(origin, axis, plnvec, cut);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vprjp_(tanpnt, cut, proj);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-7;
	    s_copy(label, "PROJ #", (ftnlen)32, (ftnlen)6);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, proj, "~~", tanpnt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)2);

/*           The returned point should be in the correct half-plane. */

	    dp = vdot_(tanpnt, plnvec);
	    tol = 1e-10;
	    s_copy(label, "YVEC dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           Compute the direction vector of the current tangent ray. */

/*           To generate a near-tangent ray passing through TANPNT, we */
/*           need to find the ray's vertex on the source. We can do this */
/*           by finding the limb on the source as seen from TANPNT. */

	    vsub_(tanpnt, axis, offset);
	    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Caution: LIMB and CUT are expressed with respect to */
/*           different origins. Shift LIMB so its center is an offset */
/*           from the target center. */

	    vadd_(axis, limb, vtemp);
	    vequ_(vtemp, limb);
	    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Generate basis vectors defined by AXIS (primary) */
/*           and PLNVEC (secondary). */

	    ucrss_(axis, plnvec, zvec);
	    vhat_(axis, xvec);
	    ucrss_(zvec, xvec, yvec);
	    if (vdot_(xpt1, yvec) > 0.) {

/*              XPT1 is in the half-plane defined by AXIS and PLNVEC. */

		vequ_(xpt1, sunlpt);
	    } else {
		vequ_(xpt2, sunlpt);
	    }

/*           Now generate the ray's direction vector. */

	    vsub_(tanpnt, sunlpt, raydir);

/*           The returned point should be on the line containing the */
/*           ray. */

	    nplnpt_(sunlpt, raydir, tanpnt, pnear, &dist);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    s_copy(label, "PNEAR #", (ftnlen)32, (ftnlen)7);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, pnear, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)32, 
		    (ftnlen)3);

/*           The returned point should be on the correct side of the */
/*           vertex of the line containing the ray. */

	    vsub_(tanpnt, axis, offset);
	    dp = vdot_(offset, raydir);
	    s_copy(label, "AXIS dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           The angle between RAYDIR and AXIS should match the */
/*           Jth element of RESULT. */

/*           We can't just use VSEP to find the rotation angle of the */
/*           tangent ray with respect to AXIS, because the angle may be */
/*           larger than pi. Compute the angle from the components of */
/*           RAYDIR. */

	    cdir = vdot_(raydir, xvec);
	    sdir = vdot_(raydir, yvec);
	    angle = atan2(sdir, cdir);
/*             WRITE (*,*) '-------' */
/*             WRITE (*,*) 'ANGLE  = ', ANGLE */
/*             WRITE (*,*) 'RESULT = ', RESULT(J) */
	    s_copy(label, "RESULT(#)", (ftnlen)32, (ftnlen)9);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    chcksd_(label, &result[(i__3 = j + 5) < 2006 && 0 <= i__3 ? i__3 :
		     s_rnge("result", i__3, "f_zztanutl__", (ftnlen)6843)], 
		    "~", &angle, &tol, ok, (ftnlen)32, (ftnlen)1);
	}
    }


/*     NESTED TORUS PENUMBRAL TERMINATOR CASES */



/* --- Case: ------------------------------------------------------ */

    tcase_("Penumbral terminator computation using nestedtorus target shape."
	    " Use empty surface list. (ALPHA)", (ftnlen)96);

/*     Get solar radius. */

    bodvcd_(&c__10, "RADII", &c__3, &n, sunrad, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    curve = 2;
    srcrad = sunrad[0];
    shape = 2;
    trgcde = 1000;
    s_copy(target, "ALPHA", (ftnlen)32, (ftnlen)5);
    nsurf = 0;
    srflst[0] = 0;
    s_copy(fixref, "ALPHA_VIEW_XY", (ftnlen)32, (ftnlen)13);
    namfrm_(fixref, &fixfid, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    et = 0.;
    spkpos_("SUN", &et, fixref, "NONE", target, axis, &lt, (ftnlen)3, (ftnlen)
	    32, (ftnlen)4, (ftnlen)32);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    bodvcd_(&trgcde, "RADII", &c__3, &n, radii, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    vhat_(axis, zvec);
    frame_(zvec, refvec, vtemp);

/*     The ZZTANINI call requires a plane reference vector, */
/*     so this call must be made for each cutting half-plane. */

    ncuts = 10;
    dtheta = pi_() * 2 / ncuts;
    i__1 = ncuts;
    for (i__ = 1; i__ <= i__1; ++i__) {
	theta = (i__ - 1) * dtheta;

/* --- Case: ------------------------------------------------------ */

	s_copy(title, "Nested torus penumbral terminator test: THETA (deg) ="
		" #.", (ftnlen)240, (ftnlen)56);
	d__1 = theta * dpr_();
	repmd_(title, "#", &d__1, &c__14, title, (ftnlen)240, (ftnlen)1, (
		ftnlen)240);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	tcase_(title, (ftnlen)240);
	vrotv_(refvec, axis, &theta, plnvec);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	zztanini_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	ssized_(&c__2000, result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	schstp = 1e-4;
	soltol = 1e-15;
	zztangnt_(&curve, &srcrad, &shape, &trgcde, &nsurf, srflst, &fixfid, &
		et, plnvec, axis, &schstp, &soltol, result, points);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Note that RESULT is a cell, not a window. */

	n = cardd_(result);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        We expect to find six penumbral terminator points in each */
/*        cutting half-plane. */

	chcksi_("N", &n, "=", &c__6, &c__0, ok, (ftnlen)1, (ftnlen)1);

/*        Check the points and the angles in the RESULT array. */

	i__2 = n;
	for (j = 1; j <= i__2; ++j) {
	    vequ_(&points[(i__3 = j * 3 - 3) < 6000 && 0 <= i__3 ? i__3 : 
		    s_rnge("points", i__3, "f_zztanutl__", (ftnlen)6957)], 
		    tanpnt);

/*           The returned point should be very close to the DSK surface. */
/*           Map the point to a point that is on the surface and check */
/*           the distance between the two. */

	    if (odd_(&j)) {
		vlcom_(&c_b74, tanpnt, &c_b1886, plnvec, vertex);
		vminus_(plnvec, dir);
	    } else {
		vlcom_(&c_b74, tanpnt, &c_b1888, plnvec, vertex);
		vequ_(plnvec, dir);
	    }
	    dskxv_(&c_false, target, &nsurf, srflst, &et, fixref, &c__1, 
		    vertex, dir, npt, &found, (ftnlen)32, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("FOUND", &found, &c_true, ok, (ftnlen)5);
	    tol = 1e-10;
	    s_copy(label, "TANPNT #", (ftnlen)32, (ftnlen)8);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, tanpnt, "~~/", npt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)3);

/*           The returned point should be in the plane containing */
/*           the cutting half-plane. */

	    psv2pl_(origin, axis, plnvec, cut);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    vprjp_(tanpnt, cut, proj);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-7;
	    s_copy(label, "PROJ #", (ftnlen)32, (ftnlen)6);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, proj, "~~", tanpnt, &c__3, &tol, ok, (ftnlen)32, (
		    ftnlen)2);

/*           The returned point should be in the correct half-plane. */

	    dp = vdot_(tanpnt, plnvec);
	    tol = 1e-10;
	    s_copy(label, "YVEC dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           Compute the direction vector of the current tangent ray. */

/*           To generate a near-tangent ray passing through TANPNT, we */
/*           need to find the ray's vertex on the source. We can do this */
/*           by finding the limb on the source as seen from TANPNT. */

	    vsub_(tanpnt, axis, offset);
	    edlimb_(sunrad, &sunrad[1], &sunrad[2], offset, limb);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Caution: LIMB and CUT are expressed with respect to */
/*           different origins. Shift LIMB so its center is an offset */
/*           from the target center. */

	    vadd_(axis, limb, vtemp);
	    vequ_(vtemp, limb);
	    inelpl_(limb, cut, &nxpts, xpt1, xpt2);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*           Generate basis vectors defined by AXIS (primary) */
/*           and PLNVEC (secondary). */

	    ucrss_(axis, plnvec, zvec);
	    vhat_(axis, xvec);
	    ucrss_(zvec, xvec, yvec);
	    if (vdot_(xpt1, yvec) < 0.) {

/*              XPT1 is in the half-plane on the opposite side */
/*              of AXI from the one defined by AXIS and PLNVEC. */

		vequ_(xpt1, sunlpt);
	    } else {
		vequ_(xpt2, sunlpt);
	    }

/*           Now generate the ray's direction vector. */

	    vsub_(tanpnt, sunlpt, raydir);

/*           The returned point should be on the line containing the */
/*           ray. */

	    nplnpt_(sunlpt, raydir, tanpnt, pnear, &dist);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    s_copy(label, "PNEAR #", (ftnlen)32, (ftnlen)7);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chckad_(label, pnear, "~~/", tanpnt, &c__3, &tol, ok, (ftnlen)32, 
		    (ftnlen)3);

/*           The returned point should be on the correct side of the */
/*           vertex of the line containing the ray. */

	    vsub_(tanpnt, axis, offset);
	    dp = vdot_(offset, raydir);
	    s_copy(label, "AXIS dot #", (ftnlen)32, (ftnlen)10);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksd_(label, &dp, ">", &c_b73, &tol, ok, (ftnlen)32, (ftnlen)1);

/*           The angle between RAYDIR and AXIS should match the */
/*           Jth element of RESULT. */

/*           We can't just use VSEP to find the rotation angle of the */
/*           tangent ray with respect to AXIS, because the angle may be */
/*           larger than pi. Compute the angle from the components of */
/*           RAYDIR. */

	    cdir = vdot_(raydir, xvec);
	    sdir = vdot_(raydir, yvec);
	    angle = atan2(sdir, cdir);
	    s_copy(label, "RESULT(#)", (ftnlen)32, (ftnlen)9);
	    repmi_(label, "#", &j, label, (ftnlen)32, (ftnlen)1, (ftnlen)32);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    tol = 1e-10;
	    chcksd_(label, &result[(i__3 = j + 5) < 2006 && 0 <= i__3 ? i__3 :
		     s_rnge("result", i__3, "f_zztanutl__", (ftnlen)7120)], 
		    "~", &angle, &tol, ok, (ftnlen)32, (ftnlen)1);
	}
    }

/* --- Case: ------------------------------------------------------ */

    tcase_("Clean up.", (ftnlen)9);

/*     Clean up. */

    delfil_("zztanutl.tpc", (ftnlen)12);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.tpc", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    spkuef_(&handle);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("nat.bsp", (ftnlen)7);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zztanutl_0.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zztanutl_1.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zztanutl_2.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_3.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zztanutl_3.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    unload_("zztanutl_4.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    delfil_("zztanutl_4.bds", (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    kclear_();

/*     Close out the test family. */

    t_success__(ok);
    return 0;
} /* f_zztanutl__ */

