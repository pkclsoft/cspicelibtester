/* f_zztime.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__3 = 3;
static integer c__1000 = 1000;
static integer c_b436 = 1000000;
static integer c__6 = 6;
static doublereal c_b503 = 0.;
static integer c__5 = 5;
static integer c__64 = 64;
static integer c__1 = 1;
static integer c__320 = 320;

/* $Procedure      F_ZZTIME ( Family of tests for ZZTIME ) */
/* Subroutine */ int f_zztime__(logical *ok)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer i_indx(char *, char *, ftnlen, ftnlen), s_rnge(char *, integer, 
	    char *, integer), s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern logical zztokns_(char *, char *, ftnlen, ftnlen);
    char good[320], erep[64];
    doublereal tvec[10];
    integer last, expn, b, e, i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen);
    doublereal etvec[10];
    extern /* Subroutine */ int repmc_(char *, char *, char *, char *, ftnlen,
	     ftnlen, ftnlen, ftnlen), repmi_(char *, char *, integer *, char *
	    , ftnlen, ftnlen, ftnlen);
    integer ntvec;
    extern /* Subroutine */ int topen_(char *, ftnlen);
    char error[640];
    extern integer rtrim_(char *, ftnlen);
    char m1[2], m2[2], w1[64], w2[64];
    extern /* Subroutine */ int t_success__(logical *);
    extern logical zzist_(char *, ftnlen);
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     chcksc_(char *, char *, char *, char *, logical *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);
    logical shdiag;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen);
    char cmline[320], pieces[64*87];
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen), getcml_(char *, ftnlen);
    char onechr[1], picerr[640];
    logical yabbrv;
    char etrans[64], transl[64], string[640], experr[640], tknerr[640];
    extern logical zzcmbt_(char *, char *, logical *, ftnlen, ftnlen);
    extern /* Subroutine */ int nextwd_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen), tstlog_(char *, logical *, ftnlen);
    char mstyle[320];
    extern /* Subroutine */ int tstmsc_(char *, ftnlen);
    logical l2r;
    extern logical zzgrep_(char *, ftnlen);
    logical r2l;
    extern /* Subroutine */ int tstmsg_(char *, char *, ftnlen, ftnlen), 
	    tstlgs_(char *, char *, ftnlen, ftnlen);
    extern logical zznote_(char *, integer *, integer *, ftnlen), zzvalt_(
	    char *, integer *, integer *, char *, ftnlen, ftnlen), zzremt_(
	    char *, ftnlen), zzrept_(char *, char *, logical *, ftnlen, 
	    ftnlen), zzsubt_(char *, char *, logical *, ftnlen, ftnlen), 
	    zzispt_(char *, integer *, integer *, ftnlen);
    char bad[320];
    extern /* Subroutine */ int tststy_(char *, char *, ftnlen, ftnlen);
    logical did;
    char pic[320], rep[64];
    logical got;
    extern logical zzunpck_(char *, logical *, doublereal *, integer *, char *
	    , char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

/* $ Abstract */

/*     This is a family of tests for the private routine ZZTIME. */

/*     Part of the utility of ZZTIME is it's ability to diagnose */
/*     problems with an input time string.  Although it is probably */
/*     not desirable to construct expected diagnostic strings, it */
/*     is useful to review them.  This test family has incorporated */
/*     test cases designed to produce every type of diagnostic that */
/*     can be returned from ZZUNPCK.  To see these, you should */
/*     add the following parameter to the command line when */
/*     you run the parent test program that calls F_ZZTIME */

/*        T_<program> [verbosity options] -diags */

/*     The diagnostics will then be printed on the screen and in */
/*     the output log file allowing you to inspect these strings */
/*     for grammar, spelling, etc. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the low-level SPICELIB time system utility */
/*     suite ZZTIME and its entry points. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     B.V. Semenov     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 3.1.0, 05-FEB-2014 (BVS) */

/*        Added tests for ZZTOKNS token buffer and picture overflow */
/*        exceptions. */

/* -    TSPICE Version 3.0.0, 08-MAR-2009 (NJB) */

/*        Added tests for detection of non-printing characters */
/*        with ASCII codes 127, 128. */

/* -    TSPICE Version 2.0.0, 27-OCT-2006 (BVS) */

/*        Added ZZTOKNS tests with blank input strings. */

/* -    TSPICE Version 1.0.0, 01-JAN-2000 (WLT) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB Functions */


/*     Parameters from ZZTIME: maximum number of tokens that a valid */
/*     time string can contain and length of the string buffer */
/*     containing the time string picture. */


/*     Local Variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZTIME", (ftnlen)8);
    s_copy(mstyle, "LEFT 5 RIGHT 75 FLAG Diagnostic: ", (ftnlen)320, (ftnlen)
	    33);
    l2r = TRUE_;
    r2l = ! l2r;
    yabbrv = FALSE_;
    getcml_(cmline, (ftnlen)320);
    ucase_(cmline, cmline, (ftnlen)320, (ftnlen)320);
    shdiag = i_indx(cmline, "-DIAGS", (ftnlen)320, (ftnlen)6) > 0;
    tcase_("Check that that tokenization process behaves as expected. ", (
	    ftnlen)58);
    s_copy(pieces, "PDT Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 64, "pst Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 128, "CDT Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 192, "CST Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 256, "MST Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 320, "MDT Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 384, "EST Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 448, "EDT Z", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 512, "UTC+ O", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 576, "UTC- o", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 640, "TDT s", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 704, "TDB s", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 768, "JD j", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 832, "JD j", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 896, "( [", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 960, ") ]", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 1024, ". .", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 1088, ": :", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 1152, ":: d", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 1216, "// d", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 1280, "/ /", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 1344, "January m", (ftnlen)64, (ftnlen)9);
    s_copy(pieces + 1408, "February m", (ftnlen)64, (ftnlen)10);
    s_copy(pieces + 1472, "March m", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 1536, "April m", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 1600, "May m", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 1664, "June m", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 1728, "July m", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 1792, "August m", (ftnlen)64, (ftnlen)8);
    s_copy(pieces + 1856, "September m", (ftnlen)64, (ftnlen)11);
    s_copy(pieces + 1920, "October m", (ftnlen)64, (ftnlen)9);
    s_copy(pieces + 1984, "November m", (ftnlen)64, (ftnlen)10);
    s_copy(pieces + 2048, "December m", (ftnlen)64, (ftnlen)10);
    s_copy(pieces + 2112, "jan, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2176, "feb m", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 2240, "mar, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2304, "apr m", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 2368, "may m", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 2432, "jun, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2496, "jul m", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 2560, "aug, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2624, "sept, m,", (ftnlen)64, (ftnlen)8);
    s_copy(pieces + 2688, "oct, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2752, "nov, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2816, "dec, m,", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2880, "mon. w.", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 2944, "tues. w.", (ftnlen)64, (ftnlen)8);
    s_copy(pieces + 3008, "wed. w.", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 3072, "thu w", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 3136, "fri. w.", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 3200, "sat. w.", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 3264, "sun. w.", (ftnlen)64, (ftnlen)7);
    s_copy(pieces + 3328, "a.d. e", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 3392, "ad e", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 3456, "b.c. e", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 3520, "bc e", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 3584, "1768 i", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 3648, "12 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 3712, "1 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 3776, "2 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 3840, "3 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 3904, "4 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 3968, "5 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4032, "6 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4096, "7 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4160, "8 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4224, "9 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4288, "0 i", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4352, "10 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4416, "21 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4480, "22 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4544, "23 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4608, "24 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4672, "25 i", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 4736, ". .", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4800, ", ,", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4864, "' '", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4928, "- -", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 4992, "am N", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 5056, "a.m. N", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 5120, "pm N", (ftnlen)64, (ftnlen)4);
    s_copy(pieces + 5184, "p.m. N", (ftnlen)64, (ftnlen)6);
    s_copy(pieces + 5248, "t t", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 5312, "T t", (ftnlen)64, (ftnlen)3);
    s_copy(pieces + 5376, "TDB s", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 5440, "TDT s", (ftnlen)64, (ftnlen)5);
    s_copy(pieces + 5504, "UTC s", (ftnlen)64, (ftnlen)5);
    for (i__ = 80; i__ <= 87; ++i__) {
	nextwd_(pieces + (((i__1 = i__ - 1) < 87 && 0 <= i__1 ? i__1 : s_rnge(
		"pieces", i__1, "f_zztime__", (ftnlen)336)) << 6), w1, m1, (
		ftnlen)64, (ftnlen)64, (ftnlen)2);
	for (j = 1; j <= 20; ++j) {
	    nextwd_(pieces + (((i__1 = j - 1) < 87 && 0 <= i__1 ? i__1 : 
		    s_rnge("pieces", i__1, "f_zztime__", (ftnlen)339)) << 6), 
		    w2, m2, (ftnlen)64, (ftnlen)64, (ftnlen)2);
	    s_copy(erep, m1, (ftnlen)64, (ftnlen)2);
	    suffix_(m2, &c__0, erep, (ftnlen)2, (ftnlen)64);
	    s_copy(string, w1, (ftnlen)640, (ftnlen)64);
	    suffix_(w2, &c__0, string, (ftnlen)64, (ftnlen)640);
	    if (s_cmp(erep, "ii", (ftnlen)64, (ftnlen)2) == 0) {
		s_copy(erep, "i", (ftnlen)64, (ftnlen)1);
	    } else if (s_cmp(string, "::", (ftnlen)640, (ftnlen)2) == 0) {
		s_copy(erep, "d", (ftnlen)64, (ftnlen)1);
	    } else if (s_cmp(string, ":::", (ftnlen)640, (ftnlen)3) == 0) {
		s_copy(erep, "d:", (ftnlen)64, (ftnlen)2);
	    } else if (s_cmp(string, "//", (ftnlen)640, (ftnlen)2) == 0) {
		s_copy(erep, "d", (ftnlen)64, (ftnlen)1);
	    } else if (s_cmp(string, "///", (ftnlen)640, (ftnlen)3) == 0) {
		s_copy(erep, "d/", (ftnlen)64, (ftnlen)2);
	    } else if (s_cmp(string, "UTC-", (ftnlen)640, (ftnlen)4) == 0) {
		s_copy(erep, "o", (ftnlen)64, (ftnlen)1);
	    }
	    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
	    got = zzgrep_(rep, (ftnlen)64);
	    tstmsg_("#", "The value of string was: \"#\"", (ftnlen)1, (ftnlen)
		    28);
	    tstmsc_(string, (ftnlen)640);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
	    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
	    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)
		    1, (ftnlen)64);
	    s_copy(erep, m1, (ftnlen)64, (ftnlen)2);
	    suffix_("b", &c__0, erep, (ftnlen)1, (ftnlen)64);
	    suffix_(m2, &c__0, erep, (ftnlen)2, (ftnlen)64);
	    s_copy(string, w1, (ftnlen)640, (ftnlen)64);
	    suffix_(w2, &c__3, string, (ftnlen)64, (ftnlen)640);
	    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
	    got = zzgrep_(rep, (ftnlen)64);
	    tstmsg_("#", "The value of string was: \"#\"", (ftnlen)1, (ftnlen)
		    28);
	    tstmsc_(string, (ftnlen)640);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
	    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
	    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)
		    1, (ftnlen)64);
	}
    }
    tstmsg_(" ", " ", (ftnlen)1, (ftnlen)1);
    tcase_("Make sure that ZZNOTE can retrieve components from a date proper"
	    "ly and that the representation is properly reduced. ", (ftnlen)
	    116);
    s_copy(string, "12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)31)
	    ;
    s_copy(erep, "ibmbibe,bi:i:ibN", (ftnlen)64, (ftnlen)16);
    s_copy(error, "xxx", (ftnlen)640, (ftnlen)3);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zznote_("e", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "ibmbib,bi:i:ibN", (ftnlen)64, (ftnlen)15);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksc_("STRING(B:E)", string + (b - 1), "=", "A.D.", ok, (ftnlen)11, e - 
	    (b - 1), (ftnlen)1, (ftnlen)4);
    tcase_("Make sure that items are properly removed by calls to ZZREMT and"
	    " that ZZNOTE can still locate tokens after such removals. ", (
	    ftnlen)122);
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);
    s_copy(erep, "bibmbibe,bi:i:ibN", (ftnlen)64, (ftnlen)17);
    s_copy(error, "xxx", (ftnlen)640, (ftnlen)3);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID1", &did, &c_true, ok, (ftnlen)4);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zzremt_("b", (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:i:iN", (ftnlen)64, (ftnlen)11);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID3", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zzremt_("b", (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:i:iN", (ftnlen)64, (ftnlen)11);
    chcksl_("GOT", &got, &c_false, ok, (ftnlen)3);
    chcksl_("DID4", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zznote_("e", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imi,i:i:iN", (ftnlen)64, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID5", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksc_("STRING(B:E)", string + (b - 1), "=", "A.D.", ok, (ftnlen)11, e - 
	    (b - 1), (ftnlen)1, (ftnlen)4);
    tcase_("Make sure that tokens can be combined successfully. ", (ftnlen)52)
	    ;
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);
    s_copy(erep, "bibmbibe,bi:i:ibN", (ftnlen)64, (ftnlen)17);
    s_copy(error, "xxx", (ftnlen)640, (ftnlen)3);

/*        First tokenize the string and make sure it has the */
/*        expected tokenization. */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID1", &did, &c_true, ok, (ftnlen)4);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP1", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        Now remove 'b' from the tokenization and check the */
/*        representation. */

    got = zzremt_("b", (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:i:iN", (ftnlen)64, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID3", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP2", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        Combine a few tokens and see if we have the expected */
/*        representation. */

    got = zzcmbt_("i:i", "K", &r2l, (ftnlen)3, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:KN", (ftnlen)64, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID4", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP3", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        See if the 'K' in the current tokenization maps to the */
/*        expected substring and check the NOTEd representation. */

    got = zznote_("K", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:N", (ftnlen)64, (ftnlen)8);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID5", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP4", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksc_("STRING(B:E)", string + (b - 1), "=", "12:18", ok, (ftnlen)11, e 
	    - (b - 1), (ftnlen)1, (ftnlen)5);

/*        Combine tokens again and go through the same steps as */
/*        the last block of code. */

    got = zzcmbt_("imie", "K", &l2r, (ftnlen)4, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "K,i:N", (ftnlen)64, (ftnlen)5);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID6", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP5", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zznote_("K", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, ",i:N", (ftnlen)64, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID7", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP6", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksc_("STRING(B:E)", string + (b - 1), "=", "12 JAN 1992 A.D.", ok, (
	    ftnlen)11, e - (b - 1), (ftnlen)1, (ftnlen)16);

/*        Now try combining something that's not present. */

    got = zzcmbt_("i:i", "K", &l2r, (ftnlen)3, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, ",i:N", (ftnlen)64, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_false, ok, (ftnlen)3);
    chcksl_("DID6", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP5", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zznote_("K", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, ",i:N", (ftnlen)64, (ftnlen)4);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_false, ok, (ftnlen)3);
    chcksl_("DID7", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP6", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksi_("B", &b, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chcksi_("E", &e, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    tcase_("Make sure that the entry point ZZSUBT works as expected. ", (
	    ftnlen)57);
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string and remove blanks. */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    got = zzremt_("b", (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,i:i:iN", (ftnlen)64, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID1", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP1", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        Now perform a left to right substitution. */

    got = zzsubt_("i:i", "H:M", &l2r, (ftnlen)3, (ftnlen)3);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,H:M:iN", (ftnlen)64, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP2", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        Try another left to right substitution, this should */
/*        turn up with no substitution. */

    got = zzsubt_("i:i", "M:S", &l2r, (ftnlen)3, (ftnlen)3);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,H:M:iN", (ftnlen)64, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_false, ok, (ftnlen)3);
    chcksl_("DID3", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP3", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);

/*        Get the value associated with H and make sure */
/*        it's the correct value. */

    got = zznote_("H", &b, &e, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "imie,:M:iN", (ftnlen)64, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID4", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP4", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    chcksc_("STRING(B:E)", string + (b - 1), "=", "11", ok, (ftnlen)11, e - (
	    b - 1), (ftnlen)1, (ftnlen)2);

/*        Perform a right to left substitution and make sure the */
/*        representation changes as expected. */

    got = zzsubt_("imi", "YmD", &r2l, (ftnlen)3, (ftnlen)3);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "YmDe,:M:iN", (ftnlen)64, (ftnlen)10);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID5", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP5", rep, "=", erep, ok, (ftnlen)4, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    tcase_("Check to make sure that a pair of consecutive delimiters is reco"
	    "gnized by ZZISPT. ", (ftnlen)82);
    s_copy(string, " 12 JAN 1992 A.D.,/ 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    33);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    got = zzispt_(",/:", &b, &e, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_true, ok, (ftnlen)3);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("STRING(B:E)", string + (b - 1), "=", ",/", ok, (ftnlen)11, e - (
	    b - 1), (ftnlen)1, (ftnlen)2);
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    got = zzispt_(string, &b, &e, (ftnlen)640);
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    got = zzispt_(",/:", &b, &e, (ftnlen)3);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT", &got, &c_false, ok, (ftnlen)3);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksi_("B", &b, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    chcksi_("E", &e, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    tcase_("Make sure that the function ZZIST works as expected. ", (ftnlen)
	    53);
    s_copy(string, " 12 JAN 1992 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    got = zzist_("i", (ftnlen)1);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    got = zzist_("m", (ftnlen)1);
    chcksl_("GOT2", &got, &c_true, ok, (ftnlen)4);
    got = zzist_(":", (ftnlen)1);
    chcksl_("GOT3", &got, &c_true, ok, (ftnlen)4);
    got = zzist_("e", (ftnlen)1);
    chcksl_("GOT4", &got, &c_true, ok, (ftnlen)4);
    got = zzist_("q", (ftnlen)1);
    chcksl_("GOT5", &got, &c_false, ok, (ftnlen)4);
    got = zzist_("N", (ftnlen)1);
    chcksl_("GOT6", &got, &c_true, ok, (ftnlen)4);
    got = zzist_("Z", (ftnlen)1);
    chcksl_("GOT7", &got, &c_false, ok, (ftnlen)4);
    tcase_("Verify that ZZVALT replaces tokens when it should and that it le"
	    "aves them alone when there is nothing to do. ", (ftnlen)109);
    s_copy(string, " 12 JAN 2012 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    got = zzvalt_(string, &c__1000, &c_b436, "Y", (ftnlen)640, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "bibmbYbe,bi:i:ibN", (ftnlen)64, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zzvalt_(string, &c__1000, &c_b436, "K", (ftnlen)640, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "bibmbYbe,bi:i:ibN", (ftnlen)64, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    tcase_("Check to make sure that ZZUNPCK can unpack items from a string. ",
	     (ftnlen)64);
    s_copy(string, " 12 JAN 2012 A.D., 11:12:18 P.M.", (ftnlen)640, (ftnlen)
	    32);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    got = zzvalt_(string, &c__1000, &c_b436, "Y", (ftnlen)640, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "bibmbYbe,bi:i:ibN", (ftnlen)64, (ftnlen)17);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    did = zzremt_("b", (ftnlen)1);
    did = zzremt_(":", (ftnlen)1);
    did = zzremt_(",", (ftnlen)1);
    did = zzremt_("e", (ftnlen)1);
    did = zzremt_("N", (ftnlen)1);
    did = zzsubt_("imYiii", "DmYHMS", &l2r, (ftnlen)6, (ftnlen)6);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    etvec[0] = 2012.;
    etvec[1] = 1.;
    etvec[2] = 12.;
    etvec[3] = 11.;
    etvec[4] = 12.;
    etvec[5] = 18.;
    s_copy(experr, " ", (ftnlen)640, (ftnlen)1);
    s_copy(etrans, "YMD", (ftnlen)64, (ftnlen)3);
    expn = 6;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    chcksi_("NTVEC", &ntvec, "=", &c__6, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksc_("TRANSL", transl, "=", "YMD", ok, (ftnlen)6, (ftnlen)64, (ftnlen)
	    1, (ftnlen)3);
    chckad_("TVEC", tvec, "=", etvec, &c__6, &c_b503, ok, (ftnlen)4, (ftnlen)
	    1);
    s_copy(string, "2012 A.D. 129// 11:12:18 P.M.", (ftnlen)640, (ftnlen)29);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    got = zzvalt_(string, &c__1000, &c_b436, "Y", (ftnlen)640, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "Ybebidbi:i:ibN", (ftnlen)64, (ftnlen)14);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    did = zzremt_("b", (ftnlen)1);
    did = zzremt_(":", (ftnlen)1);
    did = zzremt_(",", (ftnlen)1);
    did = zzremt_("e", (ftnlen)1);
    did = zzremt_("d", (ftnlen)1);
    did = zzremt_("N", (ftnlen)1);
    did = zzsubt_("Yiiii", "YyHMS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    etvec[0] = 2012.;
    etvec[1] = 129.;
    etvec[2] = 11.;
    etvec[3] = 12.;
    etvec[4] = 18.;
    s_copy(experr, " ", (ftnlen)640, (ftnlen)1);
    s_copy(etrans, "YMD", (ftnlen)64, (ftnlen)3);
    expn = 6;
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    chcksi_("NTVEC", &ntvec, "=", &c__5, &c__0, ok, (ftnlen)5, (ftnlen)1);
    chcksc_("TRANSL", transl, "=", "YD", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1,
	     (ftnlen)2);
    chckad_("TVEC", tvec, "=", etvec, &c__6, &c_b503, ok, (ftnlen)4, (ftnlen)
	    1);
    tcase_("Check to make sure that an unresolved string is diagnosed as one"
	    " by ZZUNPCK. ", (ftnlen)77);
    s_copy(string, "2012 A.D. 129// 11:12:18 P.M.", (ftnlen)640, (ftnlen)29);

/*        First tokenize the string */

    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    got = zzvalt_(string, &c__1000, &c_b436, "Y", (ftnlen)640, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "Ybebidbi:i:ibN", (ftnlen)64, (ftnlen)14);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    did = zzremt_("b", (ftnlen)1);
    did = zzremt_(":", (ftnlen)1);
    did = zzremt_(",", (ftnlen)1);
    did = zzremt_("e", (ftnlen)1);
    did = zzremt_("d", (ftnlen)1);
    did = zzremt_("N", (ftnlen)1);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YmDHMn", (ftnlen)35);
    s_copy(string, " 1995JAN19 12 13 12", (ftnlen)640, (ftnlen)19);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("imiiii", "YmDHMn", &l2r, (ftnlen)6, (ftnlen)6);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YmYHMS", (ftnlen)35);
    s_copy(string, " 1995JAN1995 12 13 12", (ftnlen)640, (ftnlen)21);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("imiiii", "YmYHMS", &l2r, (ftnlen)6, (ftnlen)6);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDHMS", (ftnlen)34);
    s_copy(string, " 1995 19 12 13 12", (ftnlen)640, (ftnlen)17);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iiiii", "YDHMS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YmDyHMS", (ftnlen)36);
    s_copy(string, " 1995JAN19 95 12 13 12", (ftnlen)640, (ftnlen)22);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("imiiiii", "YmDyHMS", &l2r, (ftnlen)7, (ftnlen)7);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages mDHMS", (ftnlen)34);
    s_copy(string, "JAN 19 12 13 12", (ftnlen)640, (ftnlen)15);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("miiii", "mDHMS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YmmDHMS", (ftnlen)36);
    s_copy(string, "1995JAN 11 5 12 13 12", (ftnlen)640, (ftnlen)21);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("imiiiii", "YmmDHMS", &l2r, (ftnlen)7, (ftnlen)7);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YyyHMS", (ftnlen)35);
    s_copy(string, "1995 12 11 12 13 12", (ftnlen)640, (ftnlen)19);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iiiii", "YyyHMS", &l2r, (ftnlen)5, (ftnlen)6);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmDHMS", (ftnlen)36);
    s_copy(string, "1995 12 JAN 11 12 13 12", (ftnlen)640, (ftnlen)23);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimiiii", "YDmDHMS", &l2r, (ftnlen)7, (ftnlen)7);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmHH", (ftnlen)34);
    s_copy(string, "1995 12 JAN 11 12 ", (ftnlen)640, (ftnlen)18);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimii", "YDmHH", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmSS", (ftnlen)34);
    s_copy(string, "1995 12 JAN 12 13 12", (ftnlen)640, (ftnlen)20);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimii", "YDmSS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmMM", (ftnlen)34);
    s_copy(string, "1995 12 JAN 11 12 ", (ftnlen)640, (ftnlen)18);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimii", "YDmMM", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmMS", (ftnlen)34);
    s_copy(string, "1995 12 JAN 11 12 ", (ftnlen)640, (ftnlen)18);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimii", "YDmMS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Checking Diagnostic Messages YDmHS", (ftnlen)34);
    s_copy(string, "1995 12 JAN 11 12 ", (ftnlen)640, (ftnlen)18);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    did = zzremt_("b", (ftnlen)1);
    did = zzsubt_("iimii", "YDmHS", &l2r, (ftnlen)5, (ftnlen)5);
    got = zzunpck_(string, &yabbrv, tvec, &ntvec, transl, pic, error, (ftnlen)
	    640, (ftnlen)64, (ftnlen)320, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_false, ok, (ftnlen)4);
    chcksc_("TRANSL", transl, "=", " ", ok, (ftnlen)6, (ftnlen)64, (ftnlen)1, 
	    (ftnlen)1);
    chcksc_("ERROR", error, "!=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)2, 
	    (ftnlen)1);
    if (shdiag) {
	tststy_(good, bad, (ftnlen)320, (ftnlen)320);
	tstlgs_(mstyle, mstyle, (ftnlen)320, (ftnlen)320);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlog_(error, &c_false, (ftnlen)640);
	tstlog_(" ", &c_false, (ftnlen)1);
	tstlgs_(good, bad, (ftnlen)320, (ftnlen)320);
    }
    tcase_("Make sure that we can perform the substitution and removal of *'"
	    "d tokens. ", (ftnlen)74);
    s_copy(string, "Monday April 22, 9:24:18.19 PST 1997", (ftnlen)640, (
	    ftnlen)36);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    got = zzremt_("b", (ftnlen)1);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT1", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID1", &did, &c_true, ok, (ftnlen)4);
    got = zzcmbt_("i.i", "n", &l2r, (ftnlen)3, (ftnlen)1);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "wmi,i:i:nZi", (ftnlen)64, (ftnlen)11);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    got = zzrept_("i:i:n", "H*M*S", &r2l, (ftnlen)5, (ftnlen)5);
    did = zzgrep_(rep, (ftnlen)64);
    s_copy(erep, "wmi,HMSZi", (ftnlen)64, (ftnlen)9);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("GOT2", &got, &c_true, ok, (ftnlen)4);
    chcksl_("DID2", &did, &c_true, ok, (ftnlen)4);
    chcksc_("REP", rep, "=", erep, ok, (ftnlen)3, (ftnlen)64, (ftnlen)1, (
	    ftnlen)64);
    tcase_("Check that blank strings are properly identified.", (ftnlen)49);
    s_copy(experr, "The input time string is blank.", (ftnlen)640, (ftnlen)31)
	    ;
    s_copy(string, " ", (ftnlen)640, (ftnlen)1);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID1", &did, &c_false, ok, (ftnlen)4);
    chcksc_("ERR1", error, "=", experr, ok, (ftnlen)4, (ftnlen)640, (ftnlen)1,
	     (ftnlen)640);
    s_copy(string, "  ", (ftnlen)640, (ftnlen)2);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID2", &did, &c_false, ok, (ftnlen)4);
    chcksc_("ERR2", error, "=", experr, ok, (ftnlen)4, (ftnlen)640, (ftnlen)1,
	     (ftnlen)640);
    *(unsigned char *)onechr = ' ';
    did = zztokns_(onechr, error, (ftnlen)1, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID3", &did, &c_false, ok, (ftnlen)4);
    chcksc_("ERR3", error, "=", experr, ok, (ftnlen)4, (ftnlen)640, (ftnlen)1,
	     (ftnlen)640);
    did = zztokns_(" ", error, (ftnlen)1, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID4", &did, &c_false, ok, (ftnlen)4);
    chcksc_("ERR4", error, "=", experr, ok, (ftnlen)4, (ftnlen)640, (ftnlen)1,
	     (ftnlen)640);
    did = zztokns_("  ", error, (ftnlen)2, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID5", &did, &c_false, ok, (ftnlen)4);
    chcksc_("ERR5", error, "=", experr, ok, (ftnlen)4, (ftnlen)640, (ftnlen)1,
	     (ftnlen)640);
    tcase_("Check error detection for ASCII character 127", (ftnlen)45);
    s_copy(string, "1 JAN 2000", (ftnlen)640, (ftnlen)10);
    *(unsigned char *)&string[1] = '\177';
    s_copy(experr, "There is a non-printing, non-tab character (ASCII 127) a"
	    "t position 2 of the time string: 1<>JAN 2000", (ftnlen)640, (
	    ftnlen)100);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for ASCII character 128", (ftnlen)45);
    s_copy(string, "1 JAN 2000", (ftnlen)640, (ftnlen)10);
    *(unsigned char *)&string[5] = 128;
    s_copy(experr, "There is a non-printing, non-tab character (ASCII 128) a"
	    "t position 6 of the time string: 1 JAN<>2000", (ftnlen)640, (
	    ftnlen)100);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);

/*     Check ZZTOKNS token count overflow exceptions. */
/*     Set expected error templates to match the one in ZZTOKNS. */

    s_copy(tknerr, "The input time string '#' cannot be processed because it"
	    " contains more than @ recognizable tokens. The token that could "
	    "not be processed was '#'.", (ftnlen)640, (ftnlen)145);
    repmi_(tknerr, "@", &c__64, tknerr, (ftnlen)640, (ftnlen)1, (ftnlen)640);
    tcase_("Check that ZZTOKNS can process a string with the maximum number "
	    "of tokens.", (ftnlen)74);
    s_copy(string, " 2", (ftnlen)640, (ftnlen)2);
    for (i__ = 4; i__ <= 64; i__ += 2) {
	suffix_("#", &c__1, string, (ftnlen)1, (ftnlen)640);
	repmi_(string, "#", &i__, string, (ftnlen)640, (ftnlen)1, (ftnlen)640)
		;
    }
    last = rtrim_(string, (ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    tcase_("Check error detection for token count overflow, unsigned integer"
	    " branch of ZZTOKNS", (ftnlen)82);
    i__1 = last - 2;
    s_copy(string + i__1, "pm0", 640 - i__1, (ftnlen)3);
    repmc_(tknerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "0", experr, (ftnlen)640, (ftnlen)1, (ftnlen)1, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for token count overflow, blanks branch of"
	    " ZZTOKNS", (ftnlen)72);
    i__1 = last - 2;
    s_copy(string + i__1, "pm 0", 640 - i__1, (ftnlen)4);
    repmc_(tknerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", " ", experr, (ftnlen)640, (ftnlen)1, (ftnlen)1, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for token count overflow, TABs branch of Z"
	    "ZTOKNS", (ftnlen)70);
    i__1 = last - 2;
    s_copy(string + i__1, "pm\t", 640 - i__1, (ftnlen)3);
    repmc_(tknerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "<TAB>", experr, (ftnlen)640, (ftnlen)1, (ftnlen)5, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for token count overflow, other tokens bra"
	    "nch of ZZTOKNS", (ftnlen)78);
    i__1 = last - 2;
    s_copy(string + i__1, "0B.C.", 640 - i__1, (ftnlen)5);
    repmc_(tknerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "B.C.", experr, (ftnlen)640, (ftnlen)1, (ftnlen)4, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);

/*     Check ZZTOKNS picture length overflow exceptions. */
/*     Set expected error template to match the one in ZZTOKNS. */

    s_copy(picerr, "The input time string '#' cannot be processed because th"
	    "e internal picture describing it requires more than @ characters"
	    ". The token that could not be processed was '#'.", (ftnlen)640, (
	    ftnlen)168);
    repmi_(picerr, "@", &c__320, picerr, (ftnlen)640, (ftnlen)1, (ftnlen)640);
    tcase_("Check that ZZTOKNS can process a string with the maximum possibl"
	    "e picture length.", (ftnlen)81);
    s_copy(string, "2007-08-09", (ftnlen)640, (ftnlen)10);
    s_copy(string + 312, "12:34:56", (ftnlen)328, (ftnlen)8);
    last = rtrim_(string, (ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_true, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", " ", ok, (ftnlen)5, (ftnlen)640, (ftnlen)1, (
	    ftnlen)1);
    tcase_("Check error detection for picture overflow, unsigned integer bra"
	    "nch of ZZTOKNS", (ftnlen)78);
    i__1 = last;
    s_copy(string + i__1, "0", 640 - i__1, (ftnlen)1);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "560", experr, (ftnlen)640, (ftnlen)1, (ftnlen)3, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, blanks branch of ZZT"
	    "OKNS", (ftnlen)68);
    i__1 = last;
    s_copy(string + i__1, " 0", 640 - i__1, (ftnlen)2);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", " ", experr, (ftnlen)640, (ftnlen)1, (ftnlen)1, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, TABs branch of ZZTOK"
	    "NS", (ftnlen)66);
    i__1 = last;
    s_copy(string + i__1, "\t", 640 - i__1, (ftnlen)1);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "<TAB>", experr, (ftnlen)640, (ftnlen)1, (ftnlen)5, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, month branch of ZZTO"
	    "KNS", (ftnlen)67);
    i__1 = last;
    s_copy(string + i__1, "May", 640 - i__1, (ftnlen)3);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "May", experr, (ftnlen)640, (ftnlen)1, (ftnlen)3, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, month branch of ZZTO"
	    "KNS", (ftnlen)67);
    i__1 = last;
    s_copy(string + i__1, "Sunday", 640 - i__1, (ftnlen)6);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "Sunday", experr, (ftnlen)640, (ftnlen)1, (ftnlen)6, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, era branch of ZZTOKNS"
	    , (ftnlen)65);
    i__1 = last;
    s_copy(string + i__1, "B.C.", 640 - i__1, (ftnlen)4);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "B.C.", experr, (ftnlen)640, (ftnlen)1, (ftnlen)4, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, am/pm branch of ZZTO"
	    "KNS", (ftnlen)67);
    i__1 = last;
    s_copy(string + i__1, "AM", 640 - i__1, (ftnlen)2);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "AM", experr, (ftnlen)640, (ftnlen)1, (ftnlen)2, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    tcase_("Check error detection for picture overflow, other tokens branch "
	    "of ZZTOKNS", (ftnlen)74);
    i__1 = last;
    s_copy(string + i__1, "/", 640 - i__1, (ftnlen)1);
    repmc_(picerr, "#", string, experr, (ftnlen)640, (ftnlen)1, (ftnlen)640, (
	    ftnlen)640);
    repmc_(experr, "#", "/", experr, (ftnlen)640, (ftnlen)1, (ftnlen)1, (
	    ftnlen)640);
    did = zztokns_(string, error, (ftnlen)640, (ftnlen)640);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksl_("DID", &did, &c_false, ok, (ftnlen)3);
    chcksc_("ERROR", error, "=", experr, ok, (ftnlen)5, (ftnlen)640, (ftnlen)
	    1, (ftnlen)640);
    t_success__(ok);
    return 0;
} /* f_zztime__ */

