/* f_zzvalcor.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__0 = 0;
static integer c__1 = 1;

/* $Procedure      F_ZZVALCOR ( Test ZZVALCOR ) */
/* Subroutine */ int f_zzvalcor__(logical *ok)
{
    /* Initialized data */

    static char corlst[80*9] = "None                                        "
	    "                                    " "lT                       "
	    "                                                       " "Xlt   "
	    "                                                                "
	    "          " "lt + s                                             "
	    "                             " "XLt + S                         "
	    "                                                " "Cn           "
	    "                                                                "
	    "   " "xCn                                                       "
	    "                      " "cN  +  s                               "
	    "                                         " "xCN +  S            "
	    "                                                            ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int zzvalcor_(char *, logical *, ftnlen);
    static integer i__, j;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static char qname[80];
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen), chckxc_(logical *
	    , char *, logical *, ftnlen);
    extern logical matchi_(char *, char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen, ftnlen);
    extern /* Subroutine */ int chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static logical attblk[6], xatblk[6];
    extern /* Subroutine */ int suffix_(char *, integer *, char *, ftnlen, 
	    ftnlen);

/* $ Abstract */

/*     Test the aberration correction parsing routine ZZVALCOR. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the aberration correction parsing */
/*     utility routine ZZVALCOR. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 26-FEB-2009 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Saved everything */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZVALCOR", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Reject stellar aberration correction without light time correcti"
	    "on", (ftnlen)66);
    zzvalcor_("S", attblk, (ftnlen)1);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_(" s", attblk, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_("+s", attblk, (ftnlen)2);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_(" + s", attblk, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_(" + S", attblk, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);

/* ---- Case ------------------------------------------------------------- */

    tcase_("Reject relativistic light time correction", (ftnlen)41);
    zzvalcor_("RLT", attblk, (ftnlen)3);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_(" rlT", attblk, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_("xRLT", attblk, (ftnlen)4);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_(" XrlT", attblk, (ftnlen)5);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_("RLT + S", attblk, (ftnlen)7);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
    zzvalcor_("xRLT + s", attblk, (ftnlen)8);
    chckxc_(&c_true, "SPICE(INVALIDOPTION)", ok, (ftnlen)20);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Check attribute block for each normal aberration correction flag."
	    , (ftnlen)65);
    for (i__ = 1; i__ <= 9; ++i__) {

/*        Parse the correction. */

	zzvalcor_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
		"corlst", i__1, "f_zzvalcor__", (ftnlen)263)) * 80, attblk, (
		ftnlen)80);
	chckxc_(&c_false, " ", ok, (ftnlen)1);

/*        Set up the expected attribute block. */

	for (j = 1; j <= 6; ++j) {
	    xatblk[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : s_rnge("xatblk", 
		    i__1, "f_zzvalcor__", (ftnlen)271)] = FALSE_;
	}

/*        Set the expected geometric element. */

	xatblk[0] = matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? 
		i__1 : s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)277)) * 
		80, "*NONE*", "*", "?", (ftnlen)80, (ftnlen)6, (ftnlen)1, (
		ftnlen)1);

/*        Set the expected transmission element. */

	xatblk[4] = matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? 
		i__1 : s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)282)) * 
		80, "*X*", "*", "?", (ftnlen)80, (ftnlen)3, (ftnlen)1, (
		ftnlen)1);

/*        Set the expected stellar aberration element. */

	xatblk[2] = matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? 
		i__1 : s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)287)) * 
		80, "*+*S*", "*", "?", (ftnlen)80, (ftnlen)5, (ftnlen)1, (
		ftnlen)1);

/*        Set the expected relativistic correction element. */

	xatblk[5] = matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? 
		i__1 : s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)292)) * 
		80, "*RLT*", "*", "?", (ftnlen)80, (ftnlen)5, (ftnlen)1, (
		ftnlen)1);

/*        Set the expected light time and converted Newtonian */
/*        elements. */

	if (matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 : 
		s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)298)) * 80, 
		"*LT*", "*", "?", (ftnlen)80, (ftnlen)4, (ftnlen)1, (ftnlen)1)
		) {

/*           The light time element should be set; the converged */
/*           Newtonian element should not. */

	    xatblk[1] = TRUE_;
	    xatblk[3] = FALSE_;
	} else if (matchi_(corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 
		: s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)306)) * 80, 
		"*CN*", "*", "?", (ftnlen)80, (ftnlen)4, (ftnlen)1, (ftnlen)1)
		) {
	    xatblk[1] = TRUE_;
	    xatblk[3] = TRUE_;
	} else {
	    xatblk[1] = FALSE_;
	    xatblk[3] = FALSE_;
	}

/*        Check each element of the attribute block. */

	for (j = 1; j <= 6; ++j) {
	    s_copy(qname, corlst + ((i__1 = i__ - 1) < 9 && 0 <= i__1 ? i__1 :
		     s_rnge("corlst", i__1, "f_zzvalcor__", (ftnlen)324)) * 
		    80, (ftnlen)80, (ftnlen)80);
	    suffix_("/", &c__0, qname, (ftnlen)1, (ftnlen)80);
	    suffix_("attblk elt. #", &c__1, qname, (ftnlen)13, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    repmi_(qname, "#", &j, qname, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(qname, &attblk[(i__1 = j - 1) < 6 && 0 <= i__1 ? i__1 : 
		    s_rnge("attblk", i__1, "f_zzvalcor__", (ftnlen)332)], &
		    xatblk[(i__2 = j - 1) < 6 && 0 <= i__2 ? i__2 : s_rnge(
		    "xatblk", i__2, "f_zzvalcor__", (ftnlen)332)], ok, (
		    ftnlen)80);
	}
    }
    return 0;
} /* f_zzvalcor__ */

