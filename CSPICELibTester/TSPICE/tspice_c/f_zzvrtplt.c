/* f_zzvrtplt.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__10000 = 10000;
static integer c__20000 = 20000;
static logical c_false = FALSE_;
static integer c__60000 = 60000;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__6 = 6;
static logical c_true = TRUE_;
static integer c_n1 = -1;

/* $Procedure      F_ZZVRTPLT ( Test ZZVRTPLT ) */
/* Subroutine */ int f_zzvrtplt__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer nlat, nlon;
    extern /* Subroutine */ int zzellplt_(doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, doublereal *, integer *, integer *);
    static doublereal a, b, c__;
    static integer i__, j, k, n;
    static char label[40];
    static integer p;
    extern /* Subroutine */ int zzvrtplt_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     tcase_(char *, ftnlen);
    static integer cells[120000]	/* was [2][60000] */;
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), topen_(char *, ftnlen);
    static integer nlist;
    static doublereal verts[30000]	/* was [3][10000] */;
    extern /* Subroutine */ int t_success__(logical *);
    static integer np;
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen);
    static integer nv;
    extern /* Subroutine */ int chcksi_(char *, integer *, char *, integer *, 
	    integer *, logical *, ftnlen, ftnlen), chcksl_(char *, logical *, 
	    logical *, logical *, ftnlen);
    static integer plates[60000]	/* was [3][20000] */, celsiz, maxlst, 
	    pltlst[60000], vrtptr[10000], plt, vtx;

/* $ Abstract */

/*     Test the private SPICELIB routine ZZVRTPLT. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DSK */

/* $ Keywords */

/*     TEST */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting. The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine tests the private routine ZZVRTPLT. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */
/*     W.L. Taber       (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 20-MAY-2016  (NJB) */

/* -& */

/*     Test Utility Functions */


/*     SPICELIB functions */


/*     Local parameters */


/*     Include file dsk02.inc */

/*     This include file declares parameters for DSK data type 2 */
/*     (plate model). */

/* -       SPICELIB Version 1.0.0 08-FEB-2017 (NJB) */

/*          Updated version info. */

/*           22-JAN-2016 (NJB) */

/*              Now includes spatial index parameters. */

/*           26-MAR-2015 (NJB) */

/*              Updated to increase MAXVRT to 16000002. MAXNPV */
/*              has been changed to (3/2)*MAXPLT. Set MAXVOX */
/*              to 100000000. */

/*           13-MAY-2010 (NJB) */

/*              Updated to reflect new no-record design. */

/*           04-MAY-2010 (NJB) */

/*              Updated for new type 2 segment design. Now uses */
/*              a local parameter to represent DSK descriptor */
/*              size (NB). */

/*           13-SEP-2008 (NJB) */

/*              Updated to remove albedo information. */
/*              Updated to use parameter for DSK descriptor size. */

/*           27-DEC-2006 (NJB) */

/*              Updated to remove minimum and maximum radius information */
/*              from segment layout.  These bounds are now included */
/*              in the segment descriptor. */

/*           26-OCT-2006 (NJB) */

/*              Updated to remove normal, center, longest side, albedo, */
/*              and area keyword parameters. */

/*           04-AUG-2006 (NJB) */

/*              Updated to support coarse voxel grid.  Area data */
/*              have now been removed. */

/*           10-JUL-2006 (NJB) */


/*     Each type 2 DSK segment has integer, d.p., and character */
/*     components.  The segment layout in DAS address space is as */
/*     follows: */


/*        Integer layout: */

/*           +-----------------+ */
/*           | NV              |  (# of vertices) */
/*           +-----------------+ */
/*           | NP              |  (# of plates ) */
/*           +-----------------+ */
/*           | NVXTOT          |  (total number of voxels) */
/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | PLATES          |  (NP 3-tuples of vertex IDs) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */



/*        D.p. layout: */

/*           +-----------------+ */
/*           | DSK descriptor  |  DSKDSZ elements */
/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */
/*           | Vertices        |  3*NV elements */
/*           +-----------------+ */


/*     This local parameter MUST be kept consistent with */
/*     the parameter DSKDSZ which is declared in dskdsc.inc. */


/*     Integer item keyword parameters used by fetch routines: */


/*     Double precision item keyword parameters used by fetch routines: */


/*     The parameters below formerly were declared in pltmax.inc. */

/*     Limits on plate model capacity: */

/*     The maximum number of bodies, vertices and */
/*     plates in a plate model or collective thereof are */
/*     provided here. */

/*     These values can be used to dimension arrays, or to */
/*     use as limit checks. */

/*     The value of MAXPLT is determined from MAXVRT via */
/*     Euler's Formula for simple polyhedra having triangular */
/*     faces. */

/*     MAXVRT is the maximum number of vertices the triangular */
/*            plate model software will support. */


/*     MAXPLT is the maximum number of plates that the triangular */
/*            plate model software will support. */


/*     MAXNPV is the maximum allowed number of vertices, not taking into */
/*     account shared vertices. */

/*     Note that this value is not sufficient to create a vertex-plate */
/*     mapping for a model of maximum plate count. */


/*     MAXVOX is the maximum number of voxels. */


/*     MAXCGR is the maximum size of the coarse voxel grid. */


/*     MAXEDG is the maximum allowed number of vertex or plate */
/*     neighbors a vertex may have. */

/*     DSK type 2 spatial index parameters */
/*     =================================== */

/*        DSK type 2 spatial index integer component */
/*        ------------------------------------------ */

/*           +-----------------+ */
/*           | VGREXT          |  (voxel grid extents, 3 integers) */
/*           +-----------------+ */
/*           | CGRSCL          |  (coarse voxel grid scale, 1 integer) */
/*           +-----------------+ */
/*           | VOXNPT          |  (size of voxel-plate pointer list) */
/*           +-----------------+ */
/*           | VOXNPL          |  (size of voxel-plate list) */
/*           +-----------------+ */
/*           | VTXNPL          |  (size of vertex-plate list) */
/*           +-----------------+ */
/*           | CGRPTR          |  (coarse grid occupancy pointers) */
/*           +-----------------+ */
/*           | VOXPTR          |  (voxel-plate pointer array) */
/*           +-----------------+ */
/*           | VOXPLT          |  (voxel-plate list) */
/*           +-----------------+ */
/*           | VTXPTR          |  (vertex-plate pointer array) */
/*           +-----------------+ */
/*           | VTXPLT          |  (vertex-plate list) */
/*           +-----------------+ */


/*        Index parameters */


/*     Grid extent: */


/*     Coarse grid scale: */


/*     Voxel pointer count: */


/*     Voxel-plate list count: */


/*     Vertex-plate list count: */


/*     Coarse grid pointers: */


/*     Size of fixed-size portion of integer component: */


/*        DSK type 2 spatial index double precision component */
/*        --------------------------------------------------- */

/*           +-----------------+ */
/*           | Vertex bounds   |  6 values (min/max for each component) */
/*           +-----------------+ */
/*           | Voxel origin    |  3 elements */
/*           +-----------------+ */
/*           | Voxel size      |  1 element */
/*           +-----------------+ */



/*        Index parameters */

/*     Vertex bounds: */


/*     Voxel grid origin: */


/*     Voxel size: */


/*     Size of fixed-size portion of double precision component: */


/*     The limits below are used to define a suggested maximum */
/*     size for the integer component of the spatial index. */


/*     Maximum number of entries in voxel-plate pointer array: */


/*     Maximum cell size: */


/*     Maximum number of entries in voxel-plate list: */


/*     Spatial index integer component size: */


/*     End of include file dsk02.inc */


/*     Local Variables */


/*     Saved variables */


/*     Begin every test family with an open call. */

    topen_("F_ZZVRTPLT", (ftnlen)10);
/* *********************************************************************** */

/*     Normal cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Set-up: create plates for a tessellated ellipsoid. NLON = 100; N"
	    "LAT = 50; NV = 4902; NP = 9800.", (ftnlen)95);
    a = 3e3;
    b = 2e3;
    c__ = 1e3;
    nlon = 100;
    nlat = 50;
    zzellplt_(&a, &b, &c__, &nlon, &nlat, &c__10000, &c__20000, &nv, verts, &
	    np, plates);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/* --- Case -------------------------------------------------------- */

    tcase_("Create vertex-plate mapping for tessellated ellipsoid.", (ftnlen)
	    54);
    maxlst = 70000;
    zzvrtplt_(&nv, &np, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_false, " ", ok, (ftnlen)1);

/*     The output list size should be exactly */

/*        NV  +  3*NP */

/*     since each plate is on exactly three vertex lists, and */
/*     since there is one plate count per vertex. */

    i__1 = nv + np * 3;
    chcksi_("NLIST", &nlist, "=", &i__1, &c__0, ok, (ftnlen)5, (ftnlen)1);

/*     Each vertex pointer should be in the range 1:NLIST-1. */

    i__1 = nv;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(label, "VRTPTR(#)", (ftnlen)40, (ftnlen)9);
	repmi_(label, "#", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, vrtptr, ">=", &c__1, &c__0, ok, (ftnlen)40, (ftnlen)2);
	i__2 = nlist - 1;
	chcksi_(label, vrtptr, "<=", &i__2, &c__0, ok, (ftnlen)40, (ftnlen)2);
    }

/*     Given the tiling pattern, except for the polar vertices, */
/*     each vertex's plate count should be in the range 1:6. */

    i__1 = nv - 2;
    for (i__ = 1; i__ <= i__1; ++i__) {
	n = pltlst[(i__3 = vrtptr[(i__2 = i__ - 1) < 10000 && 0 <= i__2 ? 
		i__2 : s_rnge("vrtptr", i__2, "f_zzvrtplt__", (ftnlen)244)] - 
		1) < 60000 && 0 <= i__3 ? i__3 : s_rnge("pltlst", i__3, "f_z"
		"zvrtplt__", (ftnlen)244)];
	s_copy(label, "Plate count(#)", (ftnlen)40, (ftnlen)14);
	repmi_(label, "#", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_(label, &n, ">=", &c__1, &c__0, ok, (ftnlen)40, (ftnlen)2);
	chcksi_(label, &n, "<=", &c__6, &c__0, ok, (ftnlen)40, (ftnlen)2);
    }

/*     The polar vertices are shared by NLON plates each. */

    i__ = nv - 1;
    n = pltlst[(i__2 = vrtptr[(i__1 = i__ - 1) < 10000 && 0 <= i__1 ? i__1 : 
	    s_rnge("vrtptr", i__1, "f_zzvrtplt__", (ftnlen)259)] - 1) < 60000 
	    && 0 <= i__2 ? i__2 : s_rnge("pltlst", i__2, "f_zzvrtplt__", (
	    ftnlen)259)];
    s_copy(label, "Plate count(#)", (ftnlen)40, (ftnlen)14);
    repmi_(label, "#", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_(label, &n, "=", &nlon, &c__0, ok, (ftnlen)40, (ftnlen)1);
    i__ = nv;
    n = pltlst[(i__2 = vrtptr[(i__1 = i__ - 1) < 10000 && 0 <= i__1 ? i__1 : 
	    s_rnge("vrtptr", i__1, "f_zzvrtplt__", (ftnlen)268)] - 1) < 60000 
	    && 0 <= i__2 ? i__2 : s_rnge("pltlst", i__2, "f_zzvrtplt__", (
	    ftnlen)268)];
    s_copy(label, "Plate count(#)", (ftnlen)40, (ftnlen)14);
    repmi_(label, "#", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    chcksi_(label, &n, "=", &nlon, &c__0, ok, (ftnlen)40, (ftnlen)1);

/*     Make sure each plate is on the plate lists of */
/*     its three vertices. */

    i__1 = np;
    for (i__ = 1; i__ <= i__1; ++i__) {
	for (j = 1; j <= 3; ++j) {

/*           P is the pointer from vertex J of plate I */
/*           into the plate list. N is the plate count */
/*           for that vertex. */

	    vtx = plates[(i__2 = j + i__ * 3 - 4) < 60000 && 0 <= i__2 ? i__2 
		    : s_rnge("plates", i__2, "f_zzvrtplt__", (ftnlen)287)];
	    p = vrtptr[(i__2 = vtx - 1) < 10000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vrtptr", i__2, "f_zzvrtplt__", (ftnlen)289)];
	    n = pltlst[(i__2 = p - 1) < 60000 && 0 <= i__2 ? i__2 : s_rnge(
		    "pltlst", i__2, "f_zzvrtplt__", (ftnlen)291)];
	    found = FALSE_;
	    k = 0;
	    while(k < n && ! found) {

/*              Examine the next plate in the list of the */
/*              current vertex. */

		++k;
		plt = pltlst[(i__2 = p + k - 1) < 60000 && 0 <= i__2 ? i__2 : 
			s_rnge("pltlst", i__2, "f_zzvrtplt__", (ftnlen)303)];
		found = plt == i__;
	    }
	    s_copy(label, "Entry for plate(#), vertex(#)", (ftnlen)40, (
		    ftnlen)29);
	    repmi_(label, "#", &i__, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    repmi_(label, "#", &vtx, label, (ftnlen)40, (ftnlen)1, (ftnlen)40)
		    ;
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chcksl_(label, &found, &c_true, ok, (ftnlen)40);
	}
    }

/*     If the last test passed, each plate is on at least the three */
/*     plate lists of its own vertices. The plate is not on any other */
/*     vertex's list, since total entry count is correct. The plate list */
/*     is valid. */

/* *********************************************************************** */

/*     Error cases */

/* *********************************************************************** */

/* --- Case -------------------------------------------------------- */

    tcase_("Bad plate count.", (ftnlen)16);
    maxlst = 60000;
    zzvrtplt_(&nv, &c__0, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(BADPLATECOUNT)", ok, (ftnlen)20);
    zzvrtplt_(&nv, &c_n1, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(BADPLATECOUNT)", ok, (ftnlen)20);

/* --- Case -------------------------------------------------------- */

    tcase_("Bad vertex count.", (ftnlen)17);
    maxlst = 60000;
    zzvrtplt_(&c__0, &np, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(BADVERTEXCOUNT)", ok, (ftnlen)21);
    zzvrtplt_(&c_n1, &np, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(BADVERTEXCOUNT)", ok, (ftnlen)21);

/* --- Case -------------------------------------------------------- */

    tcase_("Cell array too small.", (ftnlen)21);
    maxlst = 60000;
    celsiz = np * 3 - 1;
    zzvrtplt_(&nv, &np, plates, &celsiz, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(CELLARRAYTOOSMALL)", ok, (ftnlen)24);

/* --- Case -------------------------------------------------------- */

    tcase_("Plate list too small.", (ftnlen)21);
    maxlst = np * 3 + nv - 1;
    zzvrtplt_(&nv, &np, plates, &c__60000, &maxlst, cells, vrtptr, &nlist, 
	    pltlst);
    chckxc_(&c_true, "SPICE(PLATELISTTOOSMALL)", ok, (ftnlen)24);
    t_success__(ok);
    return 0;
} /* f_zzvrtplt__ */

