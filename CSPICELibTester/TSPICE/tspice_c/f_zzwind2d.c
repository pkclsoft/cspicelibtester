/* f_zzwind2d.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static integer c__1 = 1;
static integer c__0 = 0;
static integer c_n1 = -1;

/* $Procedure      F_ZZWIND2D ( Test ZZWIND2D ) */
/* Subroutine */ int f_zzwind2d__(logical *ok)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    extern integer zzwind2d_(integer *, doublereal *, doublereal *);
    static integer j, m, n, w;
    extern /* Subroutine */ int tcase_(char *, ftnlen);
    static doublereal theta;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80];
    extern /* Subroutine */ int topen_(char *, ftnlen);
    static doublereal point[2];
    extern /* Subroutine */ int t_success__(logical *);
    extern doublereal pi_(void);
    extern /* Subroutine */ int cleard_(integer *, doublereal *), chckxc_(
	    logical *, char *, logical *, ftnlen), chcksi_(char *, integer *, 
	    char *, integer *, integer *, logical *, ftnlen, ftnlen);
    static doublereal vertcs[20000]	/* was [2][10000] */;

/* $ Abstract */

/*     Test the private 2D winding number routine ZZWIND2D. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GF */
/*     TEST */
/*     SEARCH */

/* $ Declarations */
/* $ Abstract */

/*     This file contains public, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     L.E. Elson        (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0  29-NOV-2016 (NJB) */

/*        Upgraded to support surfaces represented by DSKs. */

/*        Bug fix: removed declaration of NVRMAX parameter. */

/* -    SPICELIB Version 1.3.0, 01-OCT-2011 (NJB) */

/*       Added NWILUM parameter. */

/* -    SPICELIB Version 1.2.0, 14-SEP-2010 (EDW) */

/*       Added NWPA parameter. */

/* -    SPICELIB Version 1.1.0, 08-SEP-2009 (EDW) */

/*       Added NWRR parameter. */
/*       Added NWUDS parameter. */

/* -    SPICELIB Version 1.0.0, 21-FEB-2009 (NJB) (LSE) (EDW) */

/* -& */

/*     Root finding parameters: */

/*     CNVTOL is the default convergence tolerance used by the */
/*     high-level GF search API routines. This tolerance is */
/*     used to terminate searches for binary state transitions: */
/*     when the time at which a transition occurs is bracketed */
/*     by two times that differ by no more than CNVTOL, the */
/*     transition time is considered to have been found. */

/*     Units are TDB seconds. */


/*     NWMAX is the maximum number of windows allowed for user-defined */
/*     workspace array. */

/*        DOUBLE PRECISION      WORK   ( LBCELL : MW, NWMAX ) */

/*     Currently no more than twelve windows are required; the three */
/*     extra windows are spares. */

/*     Callers of GFEVNT can include this file and use the parameter */
/*     NWMAX to declare the second dimension of the workspace array */
/*     if necessary. */


/*     Callers of GFIDST should declare their workspace window */
/*     count using NWDIST. */


/*     Callers of GFSEP should declare their workspace window */
/*     count using NWSEP. */


/*     Callers of GFRR should declare their workspace window */
/*     count using NWRR. */


/*     Callers of GFUDS should declare their workspace window */
/*     count using NWUDS. */


/*     Callers of GFPA should declare their workspace window */
/*     count using NWPA. */


/*     Callers of GFILUM should declare their workspace window */
/*     count using NWILUM. */


/*     ADDWIN is a parameter used to expand each interval of the search */
/*     (confinement) window by a small amount at both ends in order to */
/*     accommodate searches using equality constraints. The loaded */
/*     kernel files must accommodate these expanded time intervals. */


/*     FRMNLN is a string length for frame names. */


/*     FOVTLN -- maximum length for FOV string. */


/*     Specify the character strings that are allowed in the */
/*     specification of field of view shapes. */


/*     Character strings that are allowed in the */
/*     specification of occultation types: */


/*     Occultation target shape specifications: */


/*     Specify the number of supported occultation types and occultation */
/*     type string length: */


/*     Instrument field-of-view (FOV) parameters */

/*     Maximum number of FOV boundary vectors: */


/*     FOV shape parameters: */

/*        circle */
/*        ellipse */
/*        polygon */
/*        rectangle */


/*     End of file gf.inc. */

/* $ Abstract */

/*     SPICE private include file intended solely for the support of */
/*     SPICE routines. Users should not include this routine in their */
/*     source code due to the volatile nature of this file. */

/*     This file contains private, global parameter declarations */
/*     for the SPICELIB Geometry Finder (GF) subsystem. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */

/* $ Keywords */

/*     GEOMETRY */
/*     ROOT */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     E.D. Wright       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 17-FEB-2009 (NJB) (EDW) */

/* -& */

/*     The set of supported coordinate systems */

/*        System          Coordinates */
/*        ----------      ----------- */
/*        Rectangular     X, Y, Z */
/*        Latitudinal     Radius, Longitude, Latitude */
/*        Spherical       Radius, Colatitude, Longitude */
/*        RA/Dec          Range, Right Ascension, Declination */
/*        Cylindrical     Radius, Longitude, Z */
/*        Geodetic        Longitude, Latitude, Altitude */
/*        Planetographic  Longitude, Latitude, Altitude */

/*     Below we declare parameters for naming coordinate systems. */
/*     User inputs naming coordinate systems must match these */
/*     when compared using EQSTR. That is, user inputs must */
/*     match after being left justified, converted to upper case, */
/*     and having all embedded blanks removed. */


/*     Below we declare names for coordinates. Again, user */
/*     inputs naming coordinates must match these when */
/*     compared using EQSTR. */


/*     Note that the RA parameter value below matches */

/*        'RIGHT ASCENSION' */

/*     when extra blanks are compressed out of the above value. */


/*     Parameters specifying types of vector definitions */
/*     used for GF coordinate searches: */

/*     All string parameter values are left justified, upper */
/*     case, with extra blanks compressed out. */

/*     POSDEF indicates the vector is defined by the */
/*     position of a target relative to an observer. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the sub-observer point on */
/*     that body, for a given observer and target. */


/*     SOBDEF indicates the vector points from the center */
/*     of a target body to the surface intercept point on */
/*     that body, for a given observer, ray, and target. */


/*     Number of workspace windows used by ZZGFREL: */


/*     Number of additional workspace windows used by ZZGFLONG: */


/*     Index of "existence window" used by ZZGFCSLV: */


/*     Progress report parameters: */

/*     MXBEGM, */
/*     MXENDM    are, respectively, the maximum lengths of the progress */
/*               report message prefix and suffix. */

/*     Note: the sum of these lengths, plus the length of the */
/*     "percent complete" substring, should not be long enough */
/*     to cause wrap-around on any platform's terminal window. */


/*     Total progress report message length upper bound: */


/*     End of file zzgf.inc. */

/* $ Abstract */

/*     Include file zzabcorr.inc */

/*     SPICE private file intended solely for the support of SPICE */
/*     routines.  Users should not include this file directly due */
/*     to the volatile nature of this file */

/*     The parameters below define the structure of an aberration */
/*     correction attribute block. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     An aberration correction attribute block is an array of logical */
/*     flags indicating the attributes of the aberration correction */
/*     specified by an aberration correction string.  The attributes */
/*     are: */

/*        - Is the correction "geometric"? */

/*        - Is light time correction indicated? */

/*        - Is stellar aberration correction indicated? */

/*        - Is the light time correction of the "converged */
/*          Newtonian" variety? */

/*        - Is the correction for the transmission case? */

/*        - Is the correction relativistic? */

/*    The parameters defining the structure of the block are as */
/*    follows: */

/*       NABCOR    Number of aberration correction choices. */

/*       ABATSZ    Number of elements in the aberration correction */
/*                 block. */

/*       GEOIDX    Index in block of geometric correction flag. */

/*       LTIDX     Index of light time flag. */

/*       STLIDX    Index of stellar aberration flag. */

/*       CNVIDX    Index of converged Newtonian flag. */

/*       XMTIDX    Index of transmission flag. */

/*       RELIDX    Index of relativistic flag. */

/*    The following parameter is not required to define the block */
/*    structure, but it is convenient to include it here: */

/*       CORLEN    The maximum string length required by any aberration */
/*                 correction string */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 18-DEC-2004 (NJB) */

/* -& */
/*     Number of aberration correction choices: */


/*     Aberration correction attribute block size */
/*     (number of aberration correction attributes): */


/*     Indices of attributes within an aberration correction */
/*     attribute block: */


/*     Maximum length of an aberration correction string: */


/*     End of include file zzabcorr.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This family tests the private 2D winding number */
/*     routine ZZWIND2D. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 08-JUL-2008 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local Variables */


/*     Save everything. */


/*     Initial values */


/*     Begin every test family with an open call. */

    topen_("F_ZZWIND2D", (ftnlen)10);
/* ********************************************************************* */
/* * */
/* *    Error cases */
/* * */
/* ********************************************************************* */

/* ---- Case ------------------------------------------------------------- */

    tcase_("Number of vertices is less than 3.", (ftnlen)34);
    cleard_(&c__2, point);
    n = 1;
    i__1 = n << 1;
    cleard_(&i__1, vertcs);
    w = zzwind2d_(&n, vertcs, point);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
    n = 2;
    i__1 = n << 1;
    cleard_(&i__1, vertcs);
    w = zzwind2d_(&n, vertcs, point);
    chckxc_(&c_true, "SPICE(DEGENERATECASE)", ok, (ftnlen)21);
/* ********************************************************************* */
/* * */
/* *    Normal cases */
/* * */
/* ********************************************************************* */

/*     Try some regular polygons; path direction is positive. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Positively oriented, regular polygon. Number of verti"
		"ces is #; POINT is origin.", (ftnlen)80, (ftnlen)79);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	cleard_(&c__2, point);
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    theta = (j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)212)] = cos(theta)
		    ;
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)213)] = sin(theta)
		    ;
	}
	w = zzwind2d_(&n, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Try some regular polygons; path direction is negative. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Negatively oriented, regular polygon. Number of verti"
		"ces is #; POINT is origin.", (ftnlen)80, (ftnlen)79);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	cleard_(&c__2, point);
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    theta = -(j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)248)] = cos(theta)
		    ;
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)249)] = sin(theta)
		    ;
	}
	w = zzwind2d_(&n, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c_n1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Check the cases above for an exterior point. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Negatively oriented, regular polygon. Number of verti"
		"ces is #; POINT is origin.", (ftnlen)80, (ftnlen)79);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 2.;
	point[1] = 3.;
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    theta = -(j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)283)] = cos(theta)
		    ;
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)284)] = sin(theta)
		    ;
	}
	w = zzwind2d_(&n, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Repeat the previous cases with non-zero central point. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Positively oriented, regular polygon. N verts is #; P"
		"OINT is not origin.", (ftnlen)80, (ftnlen)72);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 2.;
	point[1] = 3.;
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    theta = (j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)319)] = cos(theta)
		     + point[0];
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)320)] = sin(theta)
		     + point[1];
	}
	w = zzwind2d_(&n, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }
    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Negatively oriented, regular polygon. N verts = #; PO"
		"INT is not origin.", (ftnlen)80, (ftnlen)71);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 2.;
	point[1] = 3.;
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    theta = -(j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)352)] = cos(theta)
		     + point[0];
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)353)] = sin(theta)
		     + point[1];
	}
	w = zzwind2d_(&n, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c_n1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Try some polygons with multiple wraps; path direction is */
/*     positive. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Positively oriented, regular polygon. Wrap > 1. N ver"
		"ts = #; POINT is not 0.", (ftnlen)80, (ftnlen)76);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 2.;
	point[1] = 3.;
	m = n * n;
	i__1 = m;
	for (j = 1; j <= i__1; ++j) {
	    theta = (j - 1) * (pi_() * 2 / n);
	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)391)] = cos(theta)
		     + point[0];
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)392)] = sin(theta)
		     + point[1];
	}
	w = zzwind2d_(&m, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &n, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Try some irregualr polygons with multiple wraps; path */
/*     direction is negative. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Negatively oriented, irregular polygon. Wrap > 1. N v"
		"erts = #; POINT is not 0.", (ftnlen)80, (ftnlen)78);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 2.;
	point[1] = 3.;
	m = n * n;
	i__1 = m;
	for (j = 1; j <= i__1; ++j) {
	    theta = -(j - 1) * (pi_() * 2 / n);

/*           Scale the length of the Jth vertex. */

	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)431)] = j * cos(
		    theta) + point[0];
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)432)] = j * sin(
		    theta) + point[1];
	}
	w = zzwind2d_(&m, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	i__1 = -n;
	chcksi_("W", &w, "=", &i__1, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }

/*     Try some irregualr polygons with multiple wraps; path */
/*     direction is negative. POINT is outside polygon. */

    for (n = 3; n <= 10; ++n) {

/* ---- Case ------------------------------------------------------------- */

	s_copy(title, "Negatively oriented, irregular polygon. Wrap < -1. N "
		"verts = #; exterior POINT.", (ftnlen)80, (ftnlen)79);
	repmi_(title, "#", &n, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	tcase_(title, (ftnlen)80);
	point[0] = 200.;
	point[1] = 300.;
	m = n * n;
	i__1 = m;
	for (j = 1; j <= i__1; ++j) {
	    theta = -(j - 1) * (pi_() * 2 / n);

/*           Scale the length of the Jth vertex. */

	    vertcs[(i__2 = (j << 1) - 2) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)472)] = j * cos(
		    theta);
	    vertcs[(i__2 = (j << 1) - 1) < 20000 && 0 <= i__2 ? i__2 : s_rnge(
		    "vertcs", i__2, "f_zzwind2d__", (ftnlen)473)] = j * sin(
		    theta);
	}
	w = zzwind2d_(&m, vertcs, point);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chcksi_("W", &w, "=", &c__0, &c__0, ok, (ftnlen)1, (ftnlen)1);
    }
    t_success__(ok);
    return 0;
} /* f_zzwind2d__ */

