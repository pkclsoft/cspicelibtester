/* t_bgodas.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__1024 = 1024;
static integer c__256 = 256;
static integer c__128 = 128;

/* $Procedure T_BGODAS ( BINGO: Process DAS files to alternate BFFs ) */
/* Subroutine */ int t_bgodas__(char *iname, char *oname, integer *obff, 
	ftnlen iname_len, ftnlen oname_len)
{
    /* Initialized data */

    static integer next[3] = { 2,3,1 };
    static logical pass1 = TRUE_;
    static integer prev[3] = { 3,1,2 };
    static integer recsiz[3] = { 1024,128,256 };

    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer ihan, free, ohan, unit;
    extern /* Subroutine */ int t_daswfr__(integer *, integer *, char *, char 
	    *, integer *, integer *, integer *, integer *, logical *, ftnlen, 
	    ftnlen), zzddhnfc_(integer *), t_xltfwd__(doublereal *, integer *,
	     integer *, char *, ftnlen), zzddhhlu_(integer *, char *, logical 
	    *, integer *, ftnlen), t_xltfwi__(integer *, integer *, integer *,
	     char *, ftnlen), zzddhunl_(integer *, char *, ftnlen);
    integer i__, j;
    char datac[1024];
    doublereal datad[128];
    integer datai[256];
    extern /* Subroutine */ int chkin_(char *, ftnlen);
    integer recno;
    char ftype[4];
    extern logical failed_(void);
    integer hiaddr[3];
    char ifname[60];
    static integer natbff;
    extern /* Subroutine */ int delfil_(char *, ftnlen), dasioc_(char *, 
	    integer *, integer *, char *, ftnlen, ftnlen), dasllc_(integer *);
    integer dirrec[256];
    extern /* Subroutine */ int dashfs_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     dascls_(integer *);
    integer dirloc, mxaddr[3], lastla[3];
    extern /* Subroutine */ int dasrfr_(integer *, char *, char *, integer *, 
	    integer *, integer *, integer *, ftnlen, ftnlen), dasrrc_(integer 
	    *, integer *, integer *, integer *, char *, ftnlen), daswrc_(
	    integer *, integer *, char *, ftnlen), daswfr_(integer *, char *, 
	    char *, integer *, integer *, integer *, integer *, ftnlen, 
	    ftnlen), dasrri_(integer *, integer *, integer *, integer *, 
	    integer *);
    char idword[8];
    integer clsize, lastrc[3];
    extern /* Subroutine */ int dasopr_(char *, integer *, ftnlen), dasonw_(
	    char *, char *, char *, integer *, integer *, ftnlen, ftnlen, 
	    ftnlen), chkout_(char *, ftnlen);
    integer lastwd[3], cltype;
    extern /* Subroutine */ int daswri_(integer *, integer *, integer *), 
	    dasrrd_(integer *, integer *, integer *, integer *, doublereal *),
	     daswrd_(integer *, integer *, doublereal *), setmsg_(char *, 
	    ftnlen), errhan_(char *, integer *, ftnlen);
    integer fwdptr;
    extern /* Subroutine */ int errint_(char *, integer *, ftnlen), sigerr_(
	    char *, ftnlen), daswbr_(integer *);
    extern logical exists_(char *, ftnlen), return_(void);
    integer ncc, loc, ncr, nrc, nrr;

/* $ Abstract */

/*     Convert DAS files from one supported binary file format to */
/*     another. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST ROUTINE */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     INAME      I   Name of source file to convert. */
/*     ONAME      I   Name of output file to create. */
/*     OBFF       I   Integer code for binary file format of ONAME. */

/* $ Detailed_Input */

/*     INAME      is the name of the DAS file to convert to an alternate */
/*                binary file format. */

/*     ONAME      is the name of the converted DAS file to create. The */
/*                file named by ONAME, if extant at the time this */
/*                routine is called, will be deleted and replaced with */
/*                the converted DAS. */

/*     OBFF       is an integer code that indicates the binary file */
/*                format targeted for ONAME.  Acceptable values */
/*                are the parameters: */

/*                   BIGI3E */
/*                   LTLI3E */
/*                   VAXGFL */
/*                   VAXDFL */

/*                as defined in the include file 'zzddhman.inc'. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1)  If an error occurs while reading records from INAME, the */
/*         error is diagnosed by a routine in the call tree of this */
/*         routine. */

/*     2)  If an error occurs while writing records to ONAME, the */
/*         error is diagnosed by a routine in the call tree of this */
/*         routine. */

/*     3)  Routines in the call tree of this routine may signal errors */
/*         as a result of improper inputs or other exceptional cases. */

/* $ Files */

/*     This routine opens the file named by INAME for read access and */
/*     creates the file named by ONAME. */

/* $ Particulars */

/*      This test routine allows existing test software that creates */
/*      DAS files using the high level writers to create native format */
/*      files and convert them to non-native format for testing */
/*      purposes. */

/*      As new binary file formats are added to the list of those */
/*      supported, this routine and routines it calls may require */
/*      updates. */

/* $ Examples */

/*     See F_EK01NN for an example of usage. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-FEB-2015 (NJB) (FST) */


/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     DAS data type codes */


/*     Local variables */


/*     Saved variables */


/*     Initial values */

    if (return_()) {
	return 0;
    }
    chkin_("T_BGODAS", (ftnlen)8);
    if (pass1) {

/*        Obtain the code for the native binary file format. */

	zzddhnfc_(&natbff);
	if (failed_()) {
	    chkout_("T_BGODAS", (ftnlen)8);
	    return 0;
	}
	pass1 = FALSE_;
    }

/*     Open the existing input DAS file. Read the file's file record. */

    dasopr_(iname, &ihan, iname_len);
    if (failed_()) {
	chkout_("T_BGODAS", (ftnlen)8);
	return 0;
    }

/*     Read the file record of the input DAS. */

    dasrfr_(&ihan, idword, ifname, &nrr, &nrc, &ncr, &ncc, (ftnlen)8, (ftnlen)
	    60);
    s_copy(ftype, idword + 4, (ftnlen)4, (ftnlen)4);

/*     Get the file summary of the input DAS. */

    dashfs_(&ihan, &nrr, &nrc, &ncr, &ncc, &free, lastla, lastrc, lastwd);
    if (exists_(oname, oname_len)) {
	delfil_(oname, oname_len);
    }
    if (failed_()) {
	chkout_("T_BGODAS", (ftnlen)8);
	return 0;
    }
    dasonw_(oname, ftype, ifname, &ncr, &ohan, oname_len, (ftnlen)4, (ftnlen)
	    60);
    zzddhhlu_(&ohan, "DAS", &c_true, &unit, (ftnlen)3);
    if (failed_()) {
	chkout_("T_BGODAS", (ftnlen)8);
	return 0;
    }

/*     Transfer the file record to the output file. */

    if (*obff == natbff) {

/*        The output format is native. */

	daswfr_(&ohan, idword, ifname, &nrr, &nrc, &ncr, &ncc, (ftnlen)8, (
		ftnlen)60);
    } else {

/*        This is a non-native write. We always add the FTP */
/*        string to the output file record. */

	t_daswfr__(&unit, obff, idword, ifname, &nrr, &nrc, &ncr, &ncc, &
		c_true, (ftnlen)8, (ftnlen)60);
    }

/*     Transfer any reserved records and the comment area. */

    i__1 = nrr;
    for (i__ = 1; i__ <= i__1; ++i__) {
	recno = i__ + 1;
	dasrrc_(&ihan, &recno, &c__1, &c__1024, datac, (ftnlen)1024);
	daswrc_(&ohan, &recno, datac, (ftnlen)1024);
    }
    i__1 = ncr;
    for (i__ = 1; i__ <= i__1; ++i__) {
	recno = i__ + nrr + 1;
	dasrrc_(&ihan, &recno, &c__1, &c__1024, datac, (ftnlen)1024);
	daswrc_(&ohan, &recno, datac, (ftnlen)1024);
    }

/*     Transfer directories and the data records that follow them. */

    dirloc = nrr + 2 + ncr;
    while(dirloc != 0) {

/*        Read the current directory record; extract the forward pointer, */
/*        data address ranges, and first cluster descriptor. */

	dasrri_(&ihan, &dirloc, &c__1, &c__256, dirrec);
	if (failed_()) {
	    chkout_("T_BGODAS", (ftnlen)8);
	    return 0;
	}
	fwdptr = dirrec[1];
	for (i__ = 1; i__ <= 3; ++i__) {

/*           HIADDR contains the highest address of each cluster that */
/*           has been processed, for each data type. MXADDR is the */
/*           maximum address of each data type for the current */
/*           directory. */

/*           Directories containing no clusters of a given data type */
/*           have address ranges of 0:0 for that type. Set HIADDR */
/*           accordingly. */

	    j = (i__ << 1) - 1;
	    mxaddr[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("mxaddr",
		     i__1, "t_bgodas__", (ftnlen)381)] = dirrec[(i__2 = j + 2)
		     < 256 && 0 <= i__2 ? i__2 : s_rnge("dirrec", i__2, "t_b"
		    "godas__", (ftnlen)381)];
	    if (mxaddr[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "mxaddr", i__1, "t_bgodas__", (ftnlen)383)] > 0) {
		hiaddr[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"hiaddr", i__1, "t_bgodas__", (ftnlen)385)] = dirrec[(
			i__2 = j + 1) < 256 && 0 <= i__2 ? i__2 : s_rnge(
			"dirrec", i__2, "t_bgodas__", (ftnlen)385)] - 1;
	    } else {
		hiaddr[(i__1 = i__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
			"hiaddr", i__1, "t_bgodas__", (ftnlen)387)] = 0;
	    }
	}
	cltype = dirrec[8];
	clsize = dirrec[9];
	loc = 10;

/*        Copy the directory record to the output file. */

	if (*obff == natbff) {
	    daswri_(&ohan, &dirloc, dirrec);
	} else {

/*           This is the non-native output case. */

	    t_xltfwi__(dirrec, &c__256, obff, datac, (ftnlen)1024);
	    dasioc_("WRITE", &unit, &dirloc, datac, (ftnlen)5, (ftnlen)1024);
	}
	if (failed_()) {
	    chkout_("T_BGODAS", (ftnlen)8);
	    return 0;
	}

/*        Traverse the directory record and copy the data records from */
/*        its associated clusters. The code below does not assume that */
/*        directory records are zero-padded. */

	recno = dirloc + 1;
	while(hiaddr[0] < mxaddr[0] || hiaddr[1] < mxaddr[1] || hiaddr[2] < 
		mxaddr[2]) {
	    i__1 = clsize;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		if (cltype == 1) {

/*                 Character records are written as is. */

		    dasrrc_(&ihan, &recno, &c__1, &c__1024, datac, (ftnlen)
			    1024);
		    daswrc_(&ohan, &recno, datac, (ftnlen)1024);
		} else if (cltype == 2) {
		    dasrrd_(&ihan, &recno, &c__1, &c__128, datad);
		    if (*obff == natbff) {

/*                    The output binary file format is native. */
/*                    Write the record as is. */

			daswrd_(&ohan, &recno, datad);
		    } else {

/*                    Translate the d.p. record to the output */
/*                    format. The result is stored in a character */
/*                    string. */

			t_xltfwd__(datad, &c__128, obff, datac, (ftnlen)1024);
			dasioc_("WRITE", &unit, &recno, datac, (ftnlen)5, (
				ftnlen)1024);
		    }
		} else if (cltype == 3) {
		    dasrri_(&ihan, &recno, &c__1, &c__256, datai);
		    if (*obff == natbff) {
			daswri_(&ohan, &recno, datai);
		    } else {
			t_xltfwi__(datai, &c__256, obff, datac, (ftnlen)1024);
			dasioc_("WRITE", &unit, &recno, datac, (ftnlen)5, (
				ftnlen)1024);
		    }
		} else {

/*                 We found a "new" data type code. The input file is */
/*                 probably corrupted. */

		    setmsg_("In DAS file #; directory record having record n"
			    "umber # contains cluster type of #. The valid ra"
			    "nge is 1-3.", (ftnlen)106);
		    errhan_("#", &ihan, (ftnlen)1);
		    errint_("#", &dirloc, (ftnlen)1);
		    errint_("#", &cltype, (ftnlen)1);
		    sigerr_("SPICE(DASBADDIRECTORY)", (ftnlen)22);
		    chkout_("T_BGODAS", (ftnlen)8);
		    return 0;
		}

/*              Process the next record of the cluster. */

		++recno;
	    }

/*           Update the highest data address read for the data */
/*           type of the cluster that was just processed. */

	    hiaddr[(i__1 = cltype - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("hia"
		    "ddr", i__1, "t_bgodas__", (ftnlen)502)] = hiaddr[(i__2 = 
		    cltype - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("hiaddr", 
		    i__2, "t_bgodas__", (ftnlen)502)] + clsize * recsiz[(i__3 
		    = cltype - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge("recsiz", 
		    i__3, "t_bgodas__", (ftnlen)502)];

/*           Get the type and size of the next cluster. */

	    ++loc;
	    if (dirrec[(i__1 = loc - 1) < 256 && 0 <= i__1 ? i__1 : s_rnge(
		    "dirrec", i__1, "t_bgodas__", (ftnlen)508)] < 0) {
		cltype = prev[(i__1 = cltype - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("prev", i__1, "t_bgodas__", (ftnlen)509)];
	    } else {
		cltype = next[(i__1 = cltype - 1) < 3 && 0 <= i__1 ? i__1 : 
			s_rnge("next", i__1, "t_bgodas__", (ftnlen)511)];
	    }
	    clsize = (i__2 = dirrec[(i__1 = loc - 1) < 256 && 0 <= i__1 ? 
		    i__1 : s_rnge("dirrec", i__1, "t_bgodas__", (ftnlen)514)],
		     abs(i__2));
	}

/*        Set the directory record to the forward pointer. */
/*        Note that the indicated directory may be empty; */
/*        we still want to copy it to the output file. */

	dirloc = fwdptr;
    }

/*     Make sure all buffered data are written to the output file. */

/*     Note that for non-native output files, the only buffered */
/*     records will be of character type. These records don't */
/*     require translation, so DASWBR may be used to write them. */

/*     An important side effect of this call is to deallocate */
/*     buffer entries for this file in DASRWR. If these entries */
/*     remain allocated, later calls to DASRRC will probably fail, */
/*     as the buffered entries can't be written to make room for */
/*     new data. */

    daswbr_(&ohan);

/*     Close the output file without segregating it, since we're */
/*     duplicating the structure of the input file. If the input file is */
/*     segregated, the output file will be segregated as well, */
/*     automatically. */

    dasllc_(&ohan);

/*     We're done with the output DAS logical unit. Unlock it. */

    zzddhunl_(&ohan, "DAS", (ftnlen)3);

/*     We're done with the input file. */

    dascls_(&ihan);
    chkout_("T_BGODAS", (ftnlen)8);
    return 0;
} /* t_bgodas__ */

