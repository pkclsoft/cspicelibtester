/* t_ckw06.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__6 = 6;
static integer c__0 = 0;
static integer c__3 = 3;
static integer c__1 = 1;

/* $Procedure      T_CKW06 ( CK, Write segment, type 6 ) */
/* Subroutine */ int t_ckw06__(integer *handle, integer *inst, char *ref, 
	logical *avflag, doublereal *first, doublereal *last, char *segid, 
	integer *nintvl, integer *npkts, integer *subtps, integer *degres, 
	doublereal *packts, doublereal *rates, doublereal *sclkdp, doublereal 
	*ivlbds, logical *sellst, ftnlen ref_len, ftnlen segid_len)
{
    /* Initialized data */

    static integer pktszs[4] = { 8,4,14,7 };

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer isel, ndir, i__, j, k;
    extern /* Subroutine */ int chkin_(char *, ftnlen), dafps_(integer *, 
	    integer *, doublereal *, integer *, doublereal *);
    doublereal descr[5];
    integer bepix, eepix;
    extern /* Subroutine */ int dafada_(doublereal *, integer *);
    doublereal dc[2];
    extern /* Subroutine */ int dafbna_(integer *, doublereal *, char *, 
	    ftnlen);
    integer ic[6];
    extern logical failed_(void);
    extern /* Subroutine */ int dafena_(void);
    integer segbeg, refcod, segend, pktbeg;
    extern integer brckti_(integer *, integer *, integer *);
    extern /* Subroutine */ int namfrm_(char *, integer *, ftnlen);
    integer pktend;
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    integer minisz;
    extern logical return_(void);
    integer pktdsz, winsiz, pktsiz, subtyp;
    extern logical odd_(integer *);

/* $ Abstract */

/*     This is a TSPICE test routine. */

/*     Write a type 6 segment to a CK file, without performing */
/*     the usual error checks on the input data. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DAF */
/*     NAIF_IDS */
/*     SPC */
/*     CK */
/*     TIME */

/* $ Keywords */

/*     ATTITUDE */
/*     FILES */
/*     POINTING */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to CK type 06. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 10-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this CK type. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     CK type 6 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 8-element packets. Quaternion */
/*                 and quaternion derivatives only, no angular velocity */
/*                 vector provided. Quaternion elements are listed */
/*                 first, followed by derivatives. Angular velocity is */
/*                 derived from the quaternions and quaternion */
/*                 derivatives. */


/*     Subtype 1:  Lagrange interpolation, 4-element packets. Quaternion */
/*                 only. Angular velocity is derived by differentiating */
/*                 the interpolating polynomials. */


/*     Subtype 2:  Hermite interpolation, 14-element packets. */
/*                 Quaternion and angular angular velocity vector, as */
/*                 well as derivatives of each, are provided. The */
/*                 quaternion comes first, then quaternion derivatives, */
/*                 then angular velocity and its derivatives. */


/*     Subtype 3:  Lagrange interpolation, 7-element packets. Quaternion */
/*                 and angular velocity vector provided.  The quaternion */
/*                 comes first. */


/*     Number of subtypes: */


/*     Packet sizes associated with the various subtypes: */


/*     Maximum packet size for type 6: */


/*     Minimum packet size for type 6: */


/*     The CKPFS record size declared in ckparam.inc must be at least as */
/*     large as the maximum possible size of a CK type 6 record. */

/*     The largest possible CK type 6 record has subtype 3 (note that */
/*     records of subtype 2 have half as many epochs as those of subtype */
/*     3, for a given polynomial degree). A subtype 3 record contains */

/*        - The evaluation epoch */
/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size C06PS3 */
/*        - MAXDEG+1 time tags */


/*     End of file ck06.inc. */

/* $ Abstract */

/*     Declarations of the CK data type specific and general CK low */
/*     level routine parameters. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     CK.REQ */

/* $ Keywords */

/*     CK */

/* $ Restrictions */

/*     1) If new CK types are added, the size of the record passed */
/*        between CKRxx and CKExx must be registered as separate */
/*        parameter. If this size will be greater than current value */
/*        of the CKMRSZ parameter (which specifies the maximum record */
/*        size for the record buffer used inside CKPFS) then it should */
/*        be assigned to CKMRSZ as a new value. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     CK Required Reading. */

/* $ Version */

/* -    SPICELIB Version 3.0.0, 27-JAN-2014 (NJB) */

/*        Updated to support CK type 6. Maximum degree for */
/*        type 5 was updated to be consistent with the */
/*        maximum degree for type 6. */

/* -    SPICELIB Version 2.0.0, 19-AUG-2002 (NJB) */

/*        Updated to support CK type 5. */

/* -    SPICELIB Version 1.0.0, 05-APR-1999 (BVS) */

/* -& */

/*     Number of quaternion components and number of quaternion and */
/*     angular rate components together. */


/*     CK Type 1 parameters: */

/*     CK1DTP   CK data type 1 ID; */

/*     CK1RSZ   maximum size of a record passed between CKR01 */
/*              and CKE01. */


/*     CK Type 2 parameters: */

/*     CK2DTP   CK data type 2 ID; */

/*     CK2RSZ   maximum size of a record passed between CKR02 */
/*              and CKE02. */


/*     CK Type 3 parameters: */

/*     CK3DTP   CK data type 3 ID; */

/*     CK3RSZ   maximum size of a record passed between CKR03 */
/*              and CKE03. */


/*     CK Type 4 parameters: */

/*     CK4DTP   CK data type 4 ID; */

/*     CK4PCD   parameter defining integer to DP packing schema that */
/*              is applied when seven number integer array containing */
/*              polynomial degrees for quaternion and angular rate */
/*              components packed into a single DP number stored in */
/*              actual CK records in a file; the value of must not be */
/*              changed or compatibility with existing type 4 CK files */
/*              will be lost. */

/*     CK4MXD   maximum Chebychev polynomial degree allowed in type 4 */
/*              records; the value of this parameter must never exceed */
/*              value of the CK4PCD; */

/*     CK4SFT   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 4 */
/*              CK record that passed between routines CKR04 and CKE04; */

/*     CK4RSZ   maximum size of type 4 CK record passed between CKR04 */
/*              and CKE04; CK4RSZ is computed as follows: */

/*                 CK4RSZ = ( CK4MXD + 1 ) * QAVSIZ + CK4SFT */


/*     CK Type 5 parameters: */


/*     CK5DTP   CK data type 5 ID; */

/*     CK5MXD   maximum polynomial degree allowed in type 5 */
/*              records. */

/*     CK5MET   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 5 */
/*              CK record that passed between routines CKR05 and CKE05; */

/*     CK5MXP   maximum packet size for any subtype.  Subtype 2 */
/*              has the greatest packet size, since these packets */
/*              contain a quaternion, its derivative, an angular */
/*              velocity vector, and its derivative.  See ck05.inc */
/*              for a description of the subtypes. */

/*     CK5RSZ   maximum size of type 5 CK record passed between CKR05 */
/*              and CKE05; CK5RSZ is computed as follows: */

/*                 CK5RSZ = ( CK5MXD + 1 ) * CK5MXP + CK5MET */


/*     CK Type 6 parameters: */


/*     CK6DTP   CK data type 6 ID; */

/*     CK6MXD   maximum polynomial degree allowed in type 6 */
/*              records. */

/*     CK6MET   number of additional DPs, which are not polynomial */
/*              coefficients, located at the beginning of a type 6 */
/*              CK record that passed between routines CKR06 and CKE06; */

/*     CK6MXP   maximum packet size for any subtype.  Subtype 2 */
/*              has the greatest packet size, since these packets */
/*              contain a quaternion, its derivative, an angular */
/*              velocity vector, and its derivative.  See ck06.inc */
/*              for a description of the subtypes. */

/*     CK6RSZ   maximum size of type 6 CK record passed between CKR06 */
/*              and CKE06; CK6RSZ is computed as follows: */

/*                 CK6RSZ = CK6MET + ( CK6MXD + 1 ) * ( CK6PS3 + 1 ) */

/*              where CK6PS3 is equal to the parameter CK06PS3 defined */
/*              in ck06.inc. Note that the subtype having the largest */
/*              packet size (subtype 2) does not give rise to the */
/*              largest record size, because that type is Hermite and */
/*              requires half the window size used by subtype 3 for a */
/*              given polynomial degree. */


/*     The parameter CK6PS3 must be in sync with C06PS3 defined in */
/*     ck06.inc. */



/*     Maximum record size that can be handled by CKPFS. This value */
/*     must be set to the maximum of all CKxRSZ parameters (currently */
/*     CK5RSZ.) */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     HANDLE     I   Handle of a CK file open for writing. */
/*     INST       I   NAIF instrument ID code. */
/*     REF        I   Reference frame name. */
/*     AVFLAG     I   True if the segment will contain angular velocity. */
/*     FIRST      I   Start time of interval covered by segment. */
/*     LAST       I   End time of interval covered by segment. */
/*     SEGID      I   Segment identifier. */
/*     NINTVL     I   Number of mini-segments and interpolation */
/*                    intervals. */
/*     NPKTS      I   Array of packet counts of mini-segments. */
/*     SUBTPS     I   Array of segment subtypes of mini-segments. */
/*     DEGRES     I   Array of polynomial degrees of mini-segments. */
/*     PACKTS     I   Array of data packets of mini-segments. */
/*     RATES      I   Nominal SCLK rates in seconds per tick. */
/*     SCLKDP     I   Array of epochs of mini-segments. */
/*     IVLBDS     I   Interpolation interval bounds. */
/*     SELLST     I   Interval selection flag. */
/*     MAXDEG     P   Maximum allowed degree of interpolating polynomial. */

/* $ Detailed_Input */

/*     HANDLE         is the handle of a CK file that has been opened */
/*                    for writing. */


/*     INST           is a NAIF integer code associated with an */
/*                    instrument or spacecraft structure whose */
/*                    orientation is described by the segment to be */
/*                    created. INST is treated by the SPICE frame */
/*                    subsystem as a CK frame class ID (see the */
/*                    Frames Required Reading for details). */


/*     AVFLAG         is a logical flag which indicates whether or not */
/*                    the segment will contain angular velocity. */


/*     REF            is the NAIF name for a reference frame relative to */
/*                    which the pointing (attitude) information for INST */
/*                    is specified. */

/*     FIRST, */
/*     LAST           are, respectively, the bounds of the time interval */
/*                    over which the segment defines the attitude of */
/*                    INST. FIRST and LAST are encoded SCLK times. */

/*                    FIRST must be greater than or equal to the first */
/*                    interpolation interval start time; LAST must be */
/*                    less than or equal to the last interpolation */
/*                    interval stop time. See the description of IVLBDS */
/*                    below. */


/*     SEGID          is the segment identifier. A CK segment */
/*                    identifier may contain up to 40 characters. */


/*     NINTVL         is the number of interpolation intervals */
/*                    associated with the input data. The interpolation */
/*                    intervals are associated with data sets referred */
/*                    to as "mini-segments." */

/*                    The input data comprising each mini-segment are: */

/*                       - a packet count */
/*                       - a type 6 subtype */
/*                       - an interpolating polynomial degree */
/*                       - a sequence of type 6 data packets */
/*                       - a sequence of SCLK rates */
/*                       - a sequence of packet epochs */

/*                    These inputs are described below. */


/*     NPKTS          is an array of packet counts. The Ith element of */
/*                    NPKTS is the packet count of the Ith interpolation */
/*                    interval/mini-segment. */

/*                    NPKTS has dimension NINTVL. */


/*     SUBTPS         is an array of type 6 subtypes. The Ith element */
/*                    of SUBTPS is the subtype of the packets associated */
/*                    with the Ith interpolation interval/mini-segment. */

/*                    SUBTPS has dimension NINTVL. */

/*                    *** If an element of SUBTPS is out of range, */
/*                        that element is written to the corresponding */
/*                        mini-segment control area. For the purpose */
/*                        of writing data to the mini-segment, the actual */
/*                        subtype (which indicates the packet size) is */
/*                        is assumed to be 1. *** */


/*     DEGRES         is an array of interpolating polynomial degrees. */
/*                    The Ith element of DEGRES is the polynomial degree */
/*                    of the packets associated with the Ith */
/*                    interpolation interval/mini-segment. */

/*                    For subtypes 0 and 2, interpolation degrees must be */
/*                    equivalent to 3 mod 4, that is, they must be in */
/*                    the set */

/*                       { 3, 7, 11, ..., MAXDEG } */

/*                    For subtypes 1 and 3, interpolation degrees must */
/*                    be odd and must be in the range 1:MAXDEG. */

/*                    DEGRES has dimension NINTVL. */


/*     PACKTS         is an array of data packets representing the */
/*                    orientation of INST relative to the frame REF. The */
/*                    packets for a given mini-segment are stored */
/*                    contiguously in increasing time order. The order */
/*                    of the sets of packets for different mini-segments */
/*                    is the same as the order of their corresponding */
/*                    interpolation intervals. */

/*                    Each packet contains a SPICE-style quaternion and */
/*                    optionally, depending on the segment subtype, */
/*                    attitude derivative data, from which a C-matrix */
/*                    and an angular velocity vector may be derived. */

/*                    See the discussion of quaternion styles in */
/*                    Particulars below. */

/*                    The C-matrix CMAT represented by the Ith data */
/*                    packet is a rotation matrix that transforms the */
/*                    components of a vector expressed in the base frame */
/*                    specified by REF to components expressed in the */
/*                    instrument fixed frame at the time SCLKDP(I). */

/*                    Thus, if a vector V has components x, y, z in the */
/*                    base frame, then V has components x', y', z' */
/*                    in the instrument fixed frame where: */

/*                       [ x' ]     [          ] [ x ] */
/*                       | y' |  =  |   CMAT   | | y | */
/*                       [ z' ]     [          ] [ z ] */

/*                    Attitude derivative information either explicitly */
/*                    contained in, or else derived from, PACKTS(I) */
/*                    gives the angular velocity of the instrument fixed */
/*                    frame at time SCLKDP(I) with respect to the */
/*                    reference frame specified by REF. */

/*                    The direction of an angular velocity vector gives */
/*                    the right-handed axis about which the instrument */
/*                    fixed reference frame is rotating. The magnitude */
/*                    of the vector is the magnitude of the */
/*                    instantaneous velocity of the rotation, in radians */
/*                    per second. */

/*                    Packet contents and the corresponding */
/*                    interpolation methods depend on the segment */
/*                    subtype, and are as follows: */

/*                       Subtype 0:  Hermite interpolation, 8-element */
/*                                   packets. Quaternion and quaternion */
/*                                   derivatives only, no angular */
/*                                   velocity vector provided. */
/*                                   Quaternion elements are listed */
/*                                   first, followed by derivatives. */
/*                                   Angular velocity is derived from */
/*                                   the quaternions and quaternion */
/*                                   derivatives. */

/*                       Subtype 1:  Lagrange interpolation, 4-element */
/*                                   packets. Quaternion only. Angular */
/*                                   velocity is derived by */
/*                                   differentiating the interpolating */
/*                                   polynomials. */

/*                       Subtype 2:  Hermite interpolation, 14-element */
/*                                   packets.  Quaternion and angular */
/*                                   angular velocity vector, as well as */
/*                                   derivatives of each, are provided. */
/*                                   The quaternion comes first, then */
/*                                   quaternion derivatives, then */
/*                                   angular velocity and its */
/*                                   derivatives. */

/*                       Subtype 3:  Lagrange interpolation, 7-element */
/*                                   packets. Quaternion and angular */
/*                                   velocity vector provided.  The */
/*                                   quaternion comes first. */

/*                    Angular velocity is always specified relative to */
/*                    the base frame. */

/*                    Units of angular velocity, and of quaternion */
/*                    derivatives, are radians/second. */

/*                    For the Hermite subtypes (0 and 2), quaternion */
/*                    representations must be selected so that, for */
/*                    consecutive quaternions Q(I) and Q(I+1) in a */
/*                    mini-segment, the distance between Q and Q(I+1) is */
/*                    less than the distance between Q and -Q(I+1). The */
/*                    Lagrange subtypes do not have this restriction. */


/*     RATES          is an array of nominal rates of the spacecraft */
/*                    clock associated with INST. The Ith element of */
/*                    rates is the clock rate for the packets associated */
/*                    with the Ith interpolation interval/mini-segment. */
/*                    Units are seconds per tick. Spacecraft clock rates */
/*                    are used to scale angular velocity to */
/*                    radians/second. */


/*     SCLKDP         is an array containing epochs for all input */
/*                    mini-segments. The epochs have a one-to-one */
/*                    relationship with the packets in the input */
/*                    packet array. The epochs are encoded SCLK times. */

/*                    The epochs for a given mini-segment are stored */
/*                    contiguously in increasing order. The order of the */
/*                    sets of epochs for different mini-segments is the */
/*                    same as the order of their corresponding */
/*                    interpolation intervals. */

/*                    For each mini-segment, "padding" is allowed: the */
/*                    sequence of epochs for that mini-segment may start */
/*                    before the corresponding interpolation interval */
/*                    start time and end after the corresponding */
/*                    interpolation interval stop time. Padding is used */
/*                    to control behavior of interpolating polynomials */
/*                    near interpolation interval boundaries. */

/*                    Due to possible use of padding, the elements of */
/*                    SCLKDP, taken as a whole, may not be in increasing */
/*                    order. */


/*     IVLBDS         is an array of interpolation interval boundary */
/*                    times. This array is a strictly increasing list of */
/*                    the interpolation interval start times, to which */
/*                    the end time for the last interval is appended. */
/*                    The interval bounds are encoded SCLK times. */

/*                    The Ith interpolation interval is the time */
/*                    coverage interval of the Ith mini-segment (see the */
/*                    description of NPKTS above). */

/*                    For each mini-segment, the corresponding */
/*                    interpolation interval's start time is greater */
/*                    than or equal to the mini-segment's first epoch. */
/*                    The interval's stop time may exceed the */
/*                    mini-segment's last epoch, allowing a single */
/*                    coverage gap to exist between a mini-segment's */
/*                    last epoch and its interval stop time. */

/*                    For each interpolation interval other than the */
/*                    last, the interval's coverage stop time coincides */
/*                    with the coverage start time of the next interval. */

/*                    IVLBDS has dimension NINTVL+1. */


/*     SELLST         is a logical flag indicating to the CK type 6 */
/*                    segment reader CKR06 how to select the */
/*                    interpolation interval when a request time */
/*                    coincides with a time boundary shared by two */
/*                    interpolation intervals. When SELLST ("select */
/*                    last") is .TRUE., the later interval is selected; */
/*                    otherwise the earlier interval is selected. */

/* $ Detailed_Output */

/*     None.  See $Particulars for a description of the effect of this */
/*     routine. */

/* $ Parameters */

/*     MAXDEG         is the maximum allowed degree of the interpolating */
/*                    polynomial. */

/*                    See the INCLUDE file ck06.inc for the value of */
/*                    MAXDEG. */

/* $ Exceptions */

/*     This routine does not check its inputs. */

/* $ Files */

/*     A new type 6 CK segment is written to the CK file attached */
/*     to HANDLE. */

/* $ Particulars */

/*     This is a TSPICE test routine. */

/*     This routine writes a CK type 6 data segment to the open CK */
/*     file according to the format described in the type 6 section of */
/*     the CK Required Reading. The CK file must have been opened with */
/*     write access. */


/*     Quaternion Styles */
/*     ----------------- */

/*     There are different "styles" of quaternions used in */
/*     science and engineering applications. Quaternion styles */
/*     are characterized by */

/*        - The order of quaternion elements */

/*        - The quaternion multiplication formula */

/*        - The convention for associating quaternions */
/*          with rotation matrices */

/*     Two of the commonly used styles are */

/*        - "SPICE" */

/*           > Invented by Sir William Rowan Hamilton */
/*           > Frequently used in mathematics and physics textbooks */

/*        - "Engineering" */

/*           > Widely used in aerospace engineering applications */


/*     SPICELIB subroutine interfaces ALWAYS use SPICE quaternions. */
/*     Quaternions of any other style must be converted to SPICE */
/*     quaternions before they are passed to SPICELIB routines. */


/*     Relationship between SPICE and Engineering Quaternions */
/*     ------------------------------------------------------ */

/*     Let M be a rotation matrix such that for any vector V, */

/*        M*V */

/*     is the result of rotating V by theta radians in the */
/*     counterclockwise direction about unit rotation axis vector A. */
/*     Then the SPICE quaternions representing M are */

/*        (+/-) (  cos(theta/2), */
/*                 sin(theta/2) A(1), */
/*                 sin(theta/2) A(2), */
/*                 sin(theta/2) A(3)  ) */

/*     while the engineering quaternions representing M are */

/*        (+/-) ( -sin(theta/2) A(1), */
/*                -sin(theta/2) A(2), */
/*                -sin(theta/2) A(3), */
/*                 cos(theta/2)       ) */

/*     For both styles of quaternions, if a quaternion q represents */
/*     a rotation matrix M, then -q represents M as well. */

/*     Given an engineering quaternion */

/*        QENG   = ( q0,  q1,  q2,  q3 ) */

/*     the equivalent SPICE quaternion is */

/*        QSPICE = ( q3, -q0, -q1, -q2 ) */


/*     Associating SPICE Quaternions with Rotation Matrices */
/*     ---------------------------------------------------- */

/*     Let FROM and TO be two right-handed reference frames, for */
/*     example, an inertial frame and a spacecraft-fixed frame. Let the */
/*     symbols */

/*        V    ,   V */
/*         FROM     TO */

/*     denote, respectively, an arbitrary vector expressed relative to */
/*     the FROM and TO frames. Let M denote the transformation matrix */
/*     that transforms vectors from frame FROM to frame TO; then */

/*        V   =  M * V */
/*         TO         FROM */

/*     where the expression on the right hand side represents left */
/*     multiplication of the vector by the matrix. */

/*     Then if the unit-length SPICE quaternion q represents M, where */

/*        q = (q0, q1, q2, q3) */

/*     the elements of M are derived from the elements of q as follows: */

/*          +-                                                         -+ */
/*          |           2    2                                          | */
/*          | 1 - 2*( q2 + q3 )   2*(q1*q2 - q0*q3)   2*(q1*q3 + q0*q2) | */
/*          |                                                           | */
/*          |                                                           | */
/*          |                               2    2                      | */
/*      M = | 2*(q1*q2 + q0*q3)   1 - 2*( q1 + q3 )   2*(q2*q3 - q0*q1) | */
/*          |                                                           | */
/*          |                                                           | */
/*          |                                                   2    2  | */
/*          | 2*(q1*q3 - q0*q2)   2*(q2*q3 + q0*q1)   1 - 2*( q1 + q2 ) | */
/*          |                                                           | */
/*          +-                                                         -+ */

/*     Note that substituting the elements of -q for those of q in the */
/*     right hand side leaves each element of M unchanged; this shows */
/*     that if a quaternion q represents a matrix M, then so does the */
/*     quaternion -q. */

/*     To map the rotation matrix M to a unit quaternion, we start by */
/*     decomposing the rotation matrix as a sum of symmetric */
/*     and skew-symmetric parts: */

/*                                        2 */
/*        M = [ I  +  (1-cos(theta)) OMEGA  ] + [ sin(theta) OMEGA ] */

/*                     symmetric                   skew-symmetric */


/*     OMEGA is a skew-symmetric matrix of the form */

/*                   +-             -+ */
/*                   |  0   -n3   n2 | */
/*                   |               | */
/*         OMEGA  =  |  n3   0   -n1 | */
/*                   |               | */
/*                   | -n2   n1   0  | */
/*                   +-             -+ */

/*     The vector N of matrix entries (n1, n2, n3) is the rotation axis */
/*     of M and theta is M's rotation angle.  Note that N and theta */
/*     are not unique. */

/*     Let */

/*        C = cos(theta/2) */
/*        S = sin(theta/2) */

/*     Then the unit quaternions Q corresponding to M are */

/*        Q = +/- ( C, S*n1, S*n2, S*n3 ) */

/*     The mappings between quaternions and the corresponding rotations */
/*     are carried out by the SPICELIB routines */

/*        Q2M {quaternion to matrix} */
/*        M2Q {matrix to quaternion} */

/*     M2Q always returns a quaternion with scalar part greater than */
/*     or equal to zero. */


/*     SPICE Quaternion Multiplication Formula */
/*     --------------------------------------- */

/*     Given a SPICE quaternion */

/*        Q = ( q0, q1, q2, q3 ) */

/*     corresponding to rotation axis A and angle theta as above, we can */
/*     represent Q using "scalar + vector" notation as follows: */

/*        s =   q0           = cos(theta/2) */

/*        v = ( q1, q2, q3 ) = sin(theta/2) * A */

/*        Q = s + v */

/*     Let Q1 and Q2 be SPICE quaternions with respective scalar */
/*     and vector parts s1, s2 and v1, v2: */

/*        Q1 = s1 + v1 */
/*        Q2 = s2 + v2 */

/*     We represent the dot product of v1 and v2 by */

/*        <v1, v2> */

/*     and the cross product of v1 and v2 by */

/*        v1 x v2 */

/*     Then the SPICE quaternion product is */

/*        Q1*Q2 = s1*s2 - <v1,v2>  + s1*v2 + s2*v1 + (v1 x v2) */

/*     If Q1 and Q2 represent the rotation matrices M1 and M2 */
/*     respectively, then the quaternion product */

/*        Q1*Q2 */

/*     represents the matrix product */

/*        M1*M2 */


/* $ Examples */

/*     Suppose that you have states and are prepared to produce */
/*     a segment of type 6 in a CK file. */

/*     The following code fragment could be used to add the new segment */
/*     to a previously opened CK file attached to HANDLE. The file must */
/*     have been opened with write access. */

/*        C */
/*        C     Create a segment identifier. */
/*        C */
/*              SEGID = 'MY_SAMPLE_CK_TYPE_6_SEGMENT' */

/*        C */
/*        C     Write the segment. */
/*        C */
/*              CALL T_CKW06 ( HANDLE,  INST,    REF,     AVFLAG, */
/*             .             FIRST,   LAST,    SEGID,   NINTVL, */
/*             .             NPKTS,   SUBTPS,  DEGRES,  PACKTS, */
/*             .             RATES,   SCLKDP,  IVLBDS,  SELLST  ) */


/* $ Restrictions */

/*     This routine should not be used outside of TSPICE. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */
/*     B.V. Semenov   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 05-FEB-2014 (NJB) (BVS) */

/* -& */
/* $ Index_Entries */

/*     write spk type_6 ephemeris data segment */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Packet structure parameters */


/*     Local variables */


/*     Saved values */


/*     Initial values */


/*     Standard SPICE error handling. */

    if (return_()) {
	return 0;
    }
    chkin_("T_CKW06", (ftnlen)7);

/*     Check the input data before writing to the file. */

/*     This order of operations entails some redundant */
/*     calculations, but it allows for rapid error */
/*     detection. */

/*     Initialize the mini-segment packet array indices, */
/*     and those of the mini-segment epoch array as well. */

    pktbeg = 0;
    pktend = 0;
    bepix = 0;
    eepix = 0;

/*     If we made it this far, we're ready to start writing the segment. */

/*     The type 6 segment structure is eloquently described by this */
/*     diagram from the CK Required Reading: */

/*        +--------------------------------+ */
/*        | Interval 1 mini-segment        | */
/*        +--------------------------------+ */
/*              . */
/*              . */
/*              . */
/*        +--------------------------------+ */
/*        | Interval N mini-segment        | */
/*        +--------------------------------+ */
/*        | Interval 1 start time          | */
/*        +--------------------------------+ */
/*              . */
/*              . */
/*              . */
/*        +--------------------------------+ */
/*        | Interval N start time          | */
/*        +--------------------------------+ */
/*        | Interval N stop time           | */
/*        +--------------------------------+ */
/*        | Interval start 100             | (First interval directory) */
/*        +--------------------------------+ */
/*              . */
/*              . */
/*              . */
/*        +--------------------------------+ */
/*        | Interval start (N/100)*100     | (Last interval directory) */
/*        +--------------------------------+ */
/*        | Interval 1 start pointer       | */
/*        +--------------------------------+ */
/*              . */
/*              . */
/*              . */
/*        +--------------------------------+ */
/*        | Interval N start pointer       | */
/*        +--------------------------------+ */
/*        | Interval N stop pointer + 1    | */
/*        +--------------------------------+ */
/*        | Boundary choice flag           | */
/*        +--------------------------------+ */
/*        | Number of intervals            | */
/*        +--------------------------------+ */

/*     CK type 6 mini-segments have the following structure: */

/*        +-----------------------+ */
/*        | Packet 1              | */
/*        +-----------------------+ */
/*                    . */
/*                    . */
/*                    . */
/*        +-----------------------+ */
/*        | Packet M              | */
/*        +-----------------------+ */
/*        | Epoch 1               | */
/*        +-----------------------+ */
/*                    . */
/*                    . */
/*                    . */
/*        +-----------------------+ */
/*        | Epoch M               | */
/*        +-----------------------+ */
/*        | Epoch 100             | (First time tag directory) */
/*        +-----------------------+ */
/*                    . */
/*                    . */
/*                    . */
/*        +-----------------------+ */
/*        | Epoch ((M-1)/100)*100 | (Last time tag directory) */
/*        +-----------------------+ */
/*        | Clock rate (sec/tick) | */
/*        +-----------------------+ */
/*        | Subtype code          | */
/*        +-----------------------+ */
/*        | Window size           | */
/*        +-----------------------+ */
/*        | Number of packets     | */
/*        +-----------------------+ */

/*     Note that the set of parameters at the end of a mini-segment does */
/*     not contain an interpolation interval count. This is because, */
/*     unlike a CK type 5 segment, a CK type 6 segment can contain at */
/*     most one gap. If present, the gap is located at the end of */
/*     mini-segment's interpolation interval. */


/*     Get the NAIF integer code for the reference frame. */

    namfrm_(ref, &refcod, ref_len);
/*     Create the segment descriptor. We don't use CKPDS because */
/*     that routine doesn't allow creation of a singleton segment. */

    ic[0] = *inst;
    ic[1] = refcod;
    ic[2] = 6;
    if (*avflag) {
	ic[3] = 1;
    } else {
	ic[3] = 0;
    }
    dc[0] = *first;
    dc[1] = *last;
    dafps_(&c__2, &c__6, dc, ic, descr);

/*     Begin a new segment. */

    dafbna_(handle, descr, segid, segid_len);
    if (failed_()) {
	chkout_("T_CKW06", (ftnlen)7);
	return 0;
    }

/*     Re-initialize the mini-segment packet array indices, */
/*     and those of the mini-segment epoch array as well. */

    pktbeg = 0;
    pktend = 0;
    bepix = 0;
    eepix = 0;

/*     Write data for each mini-segment to the file. */

    i__1 = *nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the packet size, which is a function of the subtype. */

	subtyp = brckti_(&subtps[i__ - 1], &c__0, &c__3);
	if (subtyp != subtps[i__ - 1]) {
	    subtyp = 1;
	}
	pktsiz = pktszs[(i__2 = subtyp) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"pktszs", i__2, "t_ckw06__", (ftnlen)873)];
	if (odd_(&subtyp)) {
	    winsiz = degres[i__ - 1] + 1;
	} else {
	    winsiz = (degres[i__ - 1] + 1) / 2;
	}

/*        Now that we have the packet size, we can compute */
/*        mini-segment packet index range. We'll let PKTDSZ */
/*        be the total count of packet data entries for this */
/*        mini-segment. */

	pktdsz = npkts[i__ - 1] * pktsiz;
	pktbeg = pktend + 1;
	pktend = pktbeg - 1 + pktdsz;

/*        At this point, we're read to start writing the */
/*        current mini-segment to the file. Start with the */
/*        packet data. */

	dafada_(&packts[pktbeg - 1], &pktdsz);

/*        Write the epochs for this mini-segment. */

	bepix = eepix + 1;
	eepix = bepix - 1 + npkts[i__ - 1];
	dafada_(&sclkdp[bepix - 1], &npkts[i__ - 1]);

/*        Compute the number of epoch directories for the */
/*        current mini-segment. */

	ndir = (npkts[i__ - 1] - 1) / 100;

/*        Write the epoch directories to the segment. */

	i__2 = ndir;
	for (j = 1; j <= i__2; ++j) {
	    k = bepix - 1 + j * 100;
	    dafada_(&sclkdp[k - 1], &c__1);
	}

/*        Write the mini-segment's SCLK rate, subtype, window size, and */
/*        packet count to the segment. */

	dafada_(&rates[i__ - 1], &c__1);
	d__1 = (doublereal) subtps[i__ - 1];
	dafada_(&d__1, &c__1);
	d__1 = (doublereal) winsiz;
	dafada_(&d__1, &c__1);
	d__1 = (doublereal) npkts[i__ - 1];
	dafada_(&d__1, &c__1);
	if (failed_()) {
	    chkout_("T_CKW06", (ftnlen)7);
	    return 0;
	}
    }

/*     We've finished writing the mini-segments. */

/*     Next write the interpolation interval bounds. */

    i__1 = *nintvl + 1;
    dafada_(ivlbds, &i__1);

/*     Create and write directories for the interval */
/*     bounds. */

/*     The directory count is the interval bound count */
/*     (N+1), minus 1, divided by the directory size. */

    ndir = *nintvl / 100;
    i__1 = ndir;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dafada_(&ivlbds[i__ * 100 - 1], &c__1);
    }

/*     Now we compute and write the start/stop pointers */
/*     for each mini-segment. */

/*     The pointers are relative to the DAF address */
/*     preceding the segment. For example, a pointer */
/*     to the first DAF address in the segment has */
/*     value 1. */

    segend = 0;
    i__1 = *nintvl;
    for (i__ = 1; i__ <= i__1; ++i__) {

/*        Set the packet size, which is a function of the subtype. Also */
/*        set the window size. First check the subtype, which will be */
/*        used as an array index. */

	subtyp = brckti_(&subtps[i__ - 1], &c__0, &c__3);
	if (subtyp != subtps[i__ - 1]) {
	    subtyp = 1;
	}
	pktsiz = pktszs[(i__2 = subtyp) < 4 && 0 <= i__2 ? i__2 : s_rnge(
		"pktszs", i__2, "t_ckw06__", (ftnlen)984)];

/*        In order to compute the end pointer of the current */
/*        mini-segment, we must compute the size, in terms */
/*        of DAF addresses, of this mini-segment. The formula */
/*        for the size is */

/*            size =     n_packets * packet_size */
/*                    +  n_epochs */
/*                    +  n_epoch_directories */
/*                    +  4 */

/*                 =     n_packets * ( packet_size + 1 ) */
/*                    +  ( n_packets - 1 ) / DIRSIZ */
/*                    +  4 */

	minisz = npkts[i__ - 1] * (pktsiz + 1) + (npkts[i__ - 1] - 1) / 100 + 
		4;
	segbeg = segend + 1;
	segend = segbeg + minisz - 1;

/*        Write the mini-segment begin pointer. */

/*        After the loop terminates, the final end pointer, incremented */
/*        by 1, will be written. */

	d__1 = (doublereal) segbeg;
	dafada_(&d__1, &c__1);
    }

/*     Write the last mini-segment end pointer, incremented by one. */
/*     SEGEND was computed on the last iteration of the above loop. */

    d__1 = (doublereal) (segend + 1);
    dafada_(&d__1, &c__1);

/*     Write out the interval selection flag. The input */
/*     boolean value is represented by a numeric constant. */

    if (*sellst) {
	isel = 1;
    } else {
	isel = -1;
    }
    d__1 = (doublereal) isel;
    dafada_(&d__1, &c__1);

/*     Write the mini-segment/interpolation interval count. */

    d__1 = (doublereal) (*nintvl);
    dafada_(&d__1, &c__1);

/*     End the segment. */

    dafena_();
    chkout_("T_CKW06", (ftnlen)7);
    return 0;
} /* t_ckw06__ */

