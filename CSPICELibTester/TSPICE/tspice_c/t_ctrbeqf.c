/* t_ctrbeqf.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__10 = 10;
static integer c__1 = 1;

/* $Procedure T_CTRBEQF ( Set Equal ZZBODS2C and ZZNAMFRM Counters ) */
/* Subroutine */ int t_ctrbeqf__(void)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int zzbods2c_(integer *, char *, integer *, 
	    logical *, char *, integer *, logical *, ftnlen, ftnlen);
    char body[32];
    extern /* Subroutine */ int zzctrchk_(integer *, integer *, logical *), 
	    zznamfrm_(integer *, char *, integer *, char *, integer *, ftnlen,
	     ftnlen), zzctruin_(integer *), chkin_(char *, ftnlen);
    logical found;
    integer ivals[1];
    extern /* Subroutine */ int movei_(integer *, integer *, integer *);
    logical svfnd;
    extern /* Subroutine */ int boddef_(char *, integer *, ftnlen);
    extern logical failed_(void);
    integer frcode, savcde;
    char frname[32];
    integer bodyid, svbdid, bodctr[2];
    logical update;
    char savnam[32];
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    integer frmctr[2];
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    integer savctr[2];
    extern /* Subroutine */ int setmsg_(char *, ftnlen), errint_(char *, 
	    integer *, ftnlen);
    char svbody[32];
    extern /* Subroutine */ int pipool_(char *, integer *, integer *, ftnlen);
    extern logical return_(void);

/* $ Abstract */

/*     This routine makes the ZZBODTRN and ZZNAMFRM state counters equal */
/*     to each other by ``running'' up the counter that's behind to the */
/*     value of the counter that's ahead. */

/*     Setting these counters to the same value is instrumental in */
/*     testing the routines that uses these counters to verify that the */
/*     saved body and frame IDs can be stale and have to be re-looked */
/*     up. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     None. */

/* $ Detailed_Input */

/*     None. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     Error free. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     WARNING: this routine relies on the counter implementation as of */
/*     N0065. If that implementation changes, this routine should change */
/*     to match the new implementation. */

/*     This routine checks which of the two counters (ZZBODTRN and */
/*     ZZBODS2C) is behind and runs that counter up to make the counters */
/*     equal by either repeatedly calling BODDEF (if ZZBODTRN counter is */
/*     behind) or by calling PIPOOL (if ZZNAMFRM counter is behind). */

/* $ Examples */

/*     See F_ZZBODS2C and F_ZZNAMFRM. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     B.V. Semenov     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 31-MAR-2014 (BVS) */


/* -& */

/*     SPICELIB functions. */

/* $ Abstract */

/*     This include file defines the dimension of the counter */
/*     array used by various SPICE subsystems to uniquely identify */
/*     changes in their states. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Parameters */

/*     CTRSIZ      is the dimension of the counter array used by */
/*                 various SPICE subsystems to uniquely identify */
/*                 changes in their states. */

/* $ Author_and_Institution */

/*     B.V. Semenov    (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 29-JUL-2013 (BVS) */

/* -& */

/*     End of include file. */


/*     Local parameters */


/*     Local variables */


/*     Standard SPICE error handling. */

    if (return_()) {
	return 0;
    } else {
	chkin_("T_CTRBEQF", (ftnlen)9);
    }

/*     Get the current body name/ID translation state counter. */

    s_copy(body, "SUN", (ftnlen)32, (ftnlen)3);
    bodyid = -10;
    found = FALSE_;
    s_copy(svbody, "EARTH", (ftnlen)32, (ftnlen)5);
    svbdid = 399;
    svfnd = TRUE_;
    zzctruin_(bodctr);
    zzbods2c_(bodctr, svbody, &svbdid, &svfnd, body, &bodyid, &found, (ftnlen)
	    32, (ftnlen)32);
    if (failed_()) {
	chkout_("T_CTRBEQF", (ftnlen)9);
	return 0;
    }
    if (! found) {
	setmsg_("ZZBODS2C error: expected FOUND = .TRUE., got FOUND = .FALSE."
		, (ftnlen)60);
	sigerr_("SPICE(ZZBODS2CFAILED1)", (ftnlen)22);
	chkout_("T_CTRBEQF", (ftnlen)9);
	return 0;
    }
    if (bodyid != 10) {
	setmsg_("ZZBODS2C error: expected BODYID = 10, got BODYID = #.", (
		ftnlen)53);
	errint_("#", &bodyid, (ftnlen)1);
	sigerr_("SPICE(ZZBODS2CFAILED2)", (ftnlen)22);
	chkout_("T_CTRBEQF", (ftnlen)9);
	return 0;
    }

/*     Get the current frame name/ID translation state counter. */

    s_copy(frname, "J2000", (ftnlen)32, (ftnlen)5);
    frcode = -1;
    s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
    savcde = 17;
    zzctruin_(frmctr);
    zznamfrm_(frmctr, savnam, &savcde, frname, &frcode, (ftnlen)32, (ftnlen)
	    32);
    if (failed_()) {
	chkout_("T_CTRBEQF", (ftnlen)9);
	return 0;
    }
    if (frcode != 1) {
	setmsg_("ZZNAMFRM error: expected FRCODE = 1, got FRCODE = #.", (
		ftnlen)52);
	errint_("#", &frcode, (ftnlen)1);
	sigerr_("SPICE(ZZNAMFRMFAILED1)", (ftnlen)22);
	chkout_("T_CTRBEQF", (ftnlen)9);
	return 0;
    }

/*     Check counters against each other to see if one of them */
/*     needs to be run up to match the other. */

    movei_(bodctr, &c__2, savctr);
    zzctrchk_(frmctr, savctr, &update);
    if (update) {

/*        They are different. Which one is behind? NOTE: this check */
/*        relies on the two-integer counter implementation as of N0065. */

	if (frmctr[1] > bodctr[1] || frmctr[1] == bodctr[1] && frmctr[0] > 
		bodctr[0]) {

/*           The body counter is behind. Run it up to the frame */
/*           counter by repeatedly calling BODDEF. */

	    while(update) {
		boddef_("SUN", &c__10, (ftnlen)3);
		if (failed_()) {
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		s_copy(body, "SUN", (ftnlen)32, (ftnlen)3);
		bodyid = -10;
		found = FALSE_;
		s_copy(svbody, "EARTH", (ftnlen)32, (ftnlen)5);
		svbdid = 399;
		svfnd = TRUE_;
		zzctruin_(bodctr);
		zzbods2c_(bodctr, svbody, &svbdid, &svfnd, body, &bodyid, &
			found, (ftnlen)32, (ftnlen)32);
		if (failed_()) {
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		if (! found) {
		    setmsg_("ZZBODS2C error: expected FOUND = .TRUE., got FO"
			    "UND = .FALSE.", (ftnlen)60);
		    sigerr_("SPICE(ZZBODS2CFAILED3)", (ftnlen)22);
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		if (bodyid != 10) {
		    setmsg_("ZZBODS2C error: expected BODYID = 10, got BODYI"
			    "D = #.", (ftnlen)53);
		    errint_("#", &bodyid, (ftnlen)1);
		    sigerr_("SPICE(ZZBODS2CFAILED4)", (ftnlen)22);
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		movei_(bodctr, &c__2, savctr);
		zzctrchk_(frmctr, savctr, &update);
	    }
	} else {

/*           The frame counter is behind. Run it up to the body */
/*           counter by repeatedly calling PIPOOL. */

	    while(update) {
		ivals[0] = 0;
		pipool_("F_CTRBEQF_DUMMY", &c__1, ivals, (ftnlen)15);
		if (failed_()) {
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		s_copy(frname, "J2000", (ftnlen)32, (ftnlen)5);
		frcode = -1;
		s_copy(savnam, "ECLIPJ2000", (ftnlen)32, (ftnlen)10);
		savcde = 17;
		zzctruin_(frmctr);
		zznamfrm_(frmctr, savnam, &savcde, frname, &frcode, (ftnlen)
			32, (ftnlen)32);
		if (failed_()) {
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		if (frcode != 1) {
		    setmsg_("ZZNAMFRM error: expected FRCODE = 1, got FRCODE"
			    " = #.", (ftnlen)52);
		    errint_("#", &frcode, (ftnlen)1);
		    sigerr_("SPICE(ZZNAMFRMFAILED2)", (ftnlen)22);
		    chkout_("T_CTRBEQF", (ftnlen)9);
		    return 0;
		}
		movei_(bodctr, &c__2, savctr);
		zzctrchk_(frmctr, savctr, &update);
	    }
	}
    }
    chkout_("T_CTRBEQF", (ftnlen)9);
    return 0;
} /* t_ctrbeqf__ */

