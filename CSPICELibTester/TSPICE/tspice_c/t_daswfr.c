/* t_daswfr.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__4 = 4;
static integer c__3 = 3;
static integer c__1 = 1;

/* $Procedure T_DASWFR ( Write a DAS file record to a test DAS ) */
/* Subroutine */ int t_daswfr__(integer *unit, integer *outbff, char *idword, 
	char *ifname, integer *nresvr, integer *nresvc, integer *ncomr, 
	integer *ncomc, logical *addftp, ftnlen idword_len, ftnlen ifname_len)
{
    /* Initialized data */

    static logical first = TRUE_;
    static integer natbff = 0;

    /* System generated locals */
    address a__1[3];
    integer i__1, i__2[3];

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wdue(cilist *), do_uio(integer *, char *, ftnlen), e_wdue(void);

    /* Local variables */
    extern /* Subroutine */ int zzddhgsd_(char *, integer *, char *, ftnlen, 
	    ftnlen), t_xltfwi__(integer *, integer *, integer *, char *, 
	    ftnlen), zzplatfm_(char *, char *, ftnlen, ftnlen);
    integer i__;
    extern /* Subroutine */ int zzftpstr_(char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    char delim[1];
    extern /* Subroutine */ int chkin_(char *, ftnlen), ucase_(char *, char *,
	     ftnlen, ftnlen), errch_(char *, char *, ftnlen, ftnlen);
    extern integer rtrim_(char *, ftnlen);
    extern logical failed_(void);
    char locifn[60];
    extern integer isrchc_(char *, integer *, char *, ftnlen, ftnlen);
    char holder[4], record[1024], locidw[8], locfmt[8], prespc[599];
    static char strbff[8*4];
    char lftbkt[6], rgtbkt[6];
    static integer ftplen;
    integer iostat;
    extern /* Subroutine */ int setmsg_(char *, ftnlen), sigerr_(char *, 
	    ftnlen), chkout_(char *, ftnlen), errint_(char *, integer *, 
	    ftnlen);
    extern logical return_(void);
    static char ftpstr[28];
    extern /* Subroutine */ int errfnm_(char *, integer *, ftnlen);
    char tmpstr[8], tststr[16];

    /* Fortran I/O blocks */
    static cilist io___18 = { 1, 0, 0, 0, 1 };
    static cilist io___19 = { 1, 0, 0, 0, 1 };
    static cilist io___21 = { 1, 0, 0, 0, 1 };


/* $ Abstract */

/*     Write file record to a test DAS having a specified binary file */
/*     format. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST ROUTINE */

/* $ Declarations */

/* $ Abstract */

/*     Parameter declarations for the DAF/DAS handle manager. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DAF, DAS */

/* $ Keywords */

/*     PRIVATE */

/* $ Particulars */

/*     This include file contains parameters defining limits and */
/*     integer codes that are utilized in the DAF/DAS handle manager */
/*     routines. */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     F.S. Turner       (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.5.0, 10-MAR-2014 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-INTEL. */

/* -    SPICELIB Version 2.4.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-LINUX-64BIT-IFORT. */

/* -    SPICELIB Version 2.3.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-GFORTRAN. */

/* -    SPICELIB Version 2.2.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GFORTRAN. */

/* -    SPICELIB Version 2.1.0, 10-MAR-2014 (BVS) */

/*        Updated for PC-CYGWIN-64BIT-GCC_C. */

/* -    SPICELIB Version 2.0.0, 12-APR-2012 (BVS) */

/*        Increased FTSIZE (from 1000 to 5000). */

/* -    SPICELIB Version 1.20.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL. */

/* -    SPICELIB Version 1.19.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-CC_C. */

/* -    SPICELIB Version 1.18.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-INTEL-64BIT-CC_C. */

/* -    SPICELIB Version 1.17.0, 13-MAY-2010 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-NATIVE_C. */

/* -    SPICELIB Version 1.16.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-WINDOWS-64BIT-IFORT. */

/* -    SPICELIB Version 1.15.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-LINUX-64BIT-GFORTRAN. */

/* -    SPICELIB Version 1.14.0, 13-MAY-2010 (BVS) */

/*        Updated for PC-64BIT-MS_C. */

/* -    SPICELIB Version 1.13.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-INTEL_C. */

/* -    SPICELIB Version 1.12.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-IFORT. */

/* -    SPICELIB Version 1.11.0, 13-MAY-2010 (BVS) */

/*        Updated for MAC-OSX-64BIT-GFORTRAN. */

/* -    SPICELIB Version 1.10.0, 18-MAR-2009 (BVS) */

/*        Updated for PC-LINUX-GFORTRAN. */

/* -    SPICELIB Version 1.9.0, 18-MAR-2009 (BVS) */

/*        Updated for MAC-OSX-GFORTRAN. */

/* -    SPICELIB Version 1.8.0, 19-FEB-2008 (BVS) */

/*        Updated for PC-LINUX-IFORT. */

/* -    SPICELIB Version 1.7.0, 14-NOV-2006 (BVS) */

/*        Updated for PC-LINUX-64BIT-GCC_C. */

/* -    SPICELIB Version 1.6.0, 14-NOV-2006 (BVS) */

/*        Updated for MAC-OSX-INTEL_C. */

/* -    SPICELIB Version 1.5.0, 14-NOV-2006 (BVS) */

/*        Updated for MAC-OSX-IFORT. */

/* -    SPICELIB Version 1.4.0, 14-NOV-2006 (BVS) */

/*        Updated for PC-WINDOWS-IFORT. */

/* -    SPICELIB Version 1.3.0, 26-OCT-2005 (BVS) */

/*        Updated for SUN-SOLARIS-64BIT-GCC_C. */

/* -    SPICELIB Version 1.2.0, 03-JAN-2005 (BVS) */

/*        Updated for PC-CYGWIN_C. */

/* -    SPICELIB Version 1.1.0, 03-JAN-2005 (BVS) */

/*        Updated for PC-CYGWIN. */

/* -    SPICELIB Version 1.0.1, 17-JUL-2002 */

/*        Added MAC-OSX environments. */

/* -    SPICELIB Version 1.0.0, 07-NOV-2001 */

/* -& */

/*     Unit and file table size parameters. */

/*     FTSIZE     is the maximum number of files (DAS and DAF) that a */
/*                user may have open simultaneously. */


/*     RSVUNT     is the number of units protected from being locked */
/*                to a particular handle by ZZDDHHLU. */


/*     SCRUNT     is the number of units protected for use by scratch */
/*                files. */


/*     UTSIZE     is the maximum number of logical units this manager */
/*                will utilize at one time. */


/*     Access method enumeration.  These parameters are used to */
/*     identify which access method is associated with a particular */
/*     handle.  They need to be synchronized with the STRAMH array */
/*     defined in ZZDDHGSD in the following fashion: */

/*        STRAMH ( READ   ) = 'READ' */
/*        STRAMH ( WRITE  ) = 'WRITE' */
/*        STRAMH ( SCRTCH ) = 'SCRATCH' */
/*        STRAMH ( NEW    ) = 'NEW' */

/*     These values are used in the file table variable FTAMH. */


/*     Binary file format enumeration.  These parameters are used to */
/*     identify which binary file format is associated with a */
/*     particular handle.  They need to be synchronized with the STRBFF */
/*     array defined in ZZDDHGSD in the following fashion: */

/*        STRBFF ( BIGI3E ) = 'BIG-IEEE' */
/*        STRBFF ( LTLI3E ) = 'LTL-IEEE' */
/*        STRBFF ( VAXGFL ) = 'VAX-GFLT' */
/*        STRBFF ( VAXDFL ) = 'VAX-DFLT' */

/*     These values are used in the file table variable FTBFF. */


/*     Some random string lengths... more documentation required. */
/*     For now this will have to suffice. */


/*     Architecture enumeration.  These parameters are used to identify */
/*     which file architecture is associated with a particular handle. */
/*     They need to be synchronized with the STRARC array defined in */
/*     ZZDDHGSD in the following fashion: */

/*        STRARC ( DAF ) = 'DAF' */
/*        STRARC ( DAS ) = 'DAS' */

/*     These values will be used in the file table variable FTARC. */


/*     For the following environments, record length is measured in */
/*     characters (bytes) with eight characters per double precision */
/*     number. */

/*     Environment: Sun, Sun FORTRAN */
/*     Source:      Sun Fortran Programmer's Guide */

/*     Environment: PC, MS FORTRAN */
/*     Source:      Microsoft Fortran Optimizing Compiler User's Guide */

/*     Environment: Macintosh, Language Systems FORTRAN */
/*     Source:      Language Systems FORTRAN Reference Manual, */
/*                  Version 1.2, page 12-7 */

/*     Environment: PC/Linux, g77 */
/*     Source:      Determined by experiment. */

/*     Environment: PC, Lahey F77 EM/32 Version 4.0 */
/*     Source:      Lahey F77 EM/32 Language Reference Manual, */
/*                  page 144 */

/*     Environment: HP-UX 9000/750, FORTRAN/9000 Series 700 computers */
/*     Source:      FORTRAN/9000 Reference-Series 700 Computers, */
/*                  page 5-110 */

/*     Environment: NeXT Mach OS (Black Hardware), */
/*                  Absoft Fortran Version 3.2 */
/*     Source:      NAIF Program */


/*     The following parameter defines the size of a string used */
/*     to store a filenames on this target platform. */


/*     The following parameter controls the size of the character record */
/*     buffer used to read data from non-native files. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */


/*     Include Section:  Private FTP Validation String Parameters */

/*        zzftprms.inc Version 1    01-MAR-1999 (FST) */

/*     This include file centralizes the definition of string sizes */
/*     and other parameters that are necessary to properly implement */
/*     the FTP error detection scheme for binary kernels. */

/*     Before making any alterations to the contents of this file, */
/*     refer to the header of ZZFTPSTR for a detailed discussion of */
/*     the FTP validation string. */

/*     Size of FTP Test String Component: */


/*     Size of Maximum Expanded FTP Validation String: */

/*      (This indicates the size of a buffer to hold the test */
/*       string sequence from a possibly corrupt file. Empirical */
/*       evidence strongly indicates that expansion due to FTP */
/*       corruption at worst doubles the number of characters. */
/*       So take 3*SIZSTR to be on the safe side.) */


/*     Size of FTP Validation String Brackets: */


/*     Size of FTP Validation String: */


/*     Size of DELIM. */


/*     Number of character clusters present in the validation string. */


/*     End Include Section:  Private FTP Validation String Parameters */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     UNIT       I   Unit attached to DAS to receive new file record. */
/*     OUTBFF     I   Binary file format code for d.p. values in OUTPUT. */
/*     IDWORD     I   DAS ID Word. */
/*     IFNAME     I   Internal filename. */
/*     NRESVR     I   Number of reserved records in file. */
/*     NRESVC     I   Number of characters in use in reserved rec. area. */
/*     NCOMR      I   Number of comment records in file. */
/*     NCOMC      I   Number of characters in use in comment area. */
/*     ADDFTP     I   Logical that indicates whether to add FTP string. */

/* $ Detailed_Input */

/*     UNIT       is the logical unit attached to the DAS to receive */
/*                the new file record. */

/*     OUTBFF     is an integer code that indicates the binary file */
/*                format targeted for OUTPUT.  Acceptable values */
/*                are the parameters: */

/*                   BIGI3E */
/*                   LTLI3E */
/*                   VAXGFL */
/*                   VAXDFL */

/*                as defined in the include file 'zzddhman.inc'. */

/*     IDWORD     a string containing the DAS ID word. */


/*     IFNAME     is the internal file name to be stored in the first */
/*                (or file) record of the specified file. */

/*     NRESVR     is the number of reserved records in the DAS file */
/*                specified by UNIT. */

/*     NRESVC     is the number of characters in use in the reserved */
/*                record area of the DAS file specified by UNIT. */

/*     NCOMR      is the number of comment records in the DAS file */
/*                specified by UNIT. */

/*     NCOMC      is the number of characters in use in the comment area */
/*                of the DAS file specified by UNIT. */

/*     ADDFTP     is a string indicating whether to add the FTP */
/*                diagnostic string and the binary file format ID */
/*                string to the file record. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1)  If the binary file format specified by OUTBFF is */
/*         unrecognized, the error SPICE(BADFILEFORMAT) is signaled. */

/*     2)  If an error occurs while trying to write the output file, */
/*         the error SPICE(FILEWRITEFAILED) is signaled. */

/* $ Files */

/*     This routine updates the first record of the file attached */
/*     to UNIT. */

/* $ Particulars */

/*     Note: This code is not a model of perfection.  It is hacked */
/*     together from pieces of code written elsewhere to support */
/*     creation of non-native binaries for testing purposes only. */

/* $ Examples */

/*     See T_DASBGO for usage. */

/* $ Restrictions */

/*     1) The output DAS file is assumed to have been opened */
/*        by DASONW. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */
/*     F.S. Turner     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 11-FEB-2015 (NJB) (FST) */


/* -& */

/*     SPICELIB Functions */


/*     Local Parameters */


/*     Local Variables */


/*     Saved Variables */


/*     Data Statements */


/*     Standard SPICE error handling. */

    if (return_()) {
	return 0;
    }
    chkin_("T_DASWFR", (ftnlen)8);

/*     Perform some initialization tasks. */

    if (first) {

/*        Populate STRBFF with the appropriate binary file */
/*        format labels. */

	for (i__ = 1; i__ <= 4; ++i__) {
	    zzddhgsd_("BFF", &i__, strbff + (((i__1 = i__ - 1) < 4 && 0 <= 
		    i__1 ? i__1 : s_rnge("strbff", i__1, "t_daswfr__", (
		    ftnlen)253)) << 3), (ftnlen)3, (ftnlen)8);
	}

/*        Fetch the native binary file format. */

	zzplatfm_("FILE_FORMAT", tmpstr, (ftnlen)11, (ftnlen)8);
	ucase_(tmpstr, tmpstr, (ftnlen)8, (ftnlen)8);
	natbff = isrchc_(tmpstr, &c__4, strbff, (ftnlen)8, (ftnlen)8);
	if (natbff == 0) {
	    setmsg_("The binary file format, '#', is not supported by this v"
		    "ersion of the toolkit. This is a serious problem, contac"
		    "t NAIF.", (ftnlen)118);
	    errch_("#", tmpstr, (ftnlen)1, (ftnlen)8);
	    sigerr_("SPICE(BUG)", (ftnlen)10);
	    chkout_("T_DASWFR", (ftnlen)8);
	    return 0;
	}

/*        Fetch the FTP string. */

	zzftpstr_(tststr, lftbkt, rgtbkt, delim, (ftnlen)16, (ftnlen)6, (
		ftnlen)6, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = rtrim_(lftbkt, (ftnlen)6), a__1[0] = lftbkt;
	i__2[1] = rtrim_(tststr, (ftnlen)16), a__1[1] = tststr;
	i__2[2] = rtrim_(rgtbkt, (ftnlen)6), a__1[2] = rgtbkt;
	s_cat(ftpstr, a__1, i__2, &c__3, (ftnlen)28);
	ftplen = rtrim_(ftpstr, (ftnlen)28);
	if (failed_()) {
	    chkout_("T_DASWFR", (ftnlen)8);
	    return 0;
	}

/*        Do not perform initialization tasks again. */

	first = FALSE_;
    }

/*     Check to see if OUTBFF is valid.  This should never occur if this */
/*     routine is called properly. */

    if (*outbff < 1 || *outbff > 4) {
	setmsg_("The integer code used to indicate the binary file format of"
		" the input integers, #, is out of range.  This error should "
		"never occur.", (ftnlen)131);
	errint_("#", outbff, (ftnlen)1);
	sigerr_("SPICE(BADFILEFORMAT)", (ftnlen)20);
	chkout_("T_DASWFR", (ftnlen)8);
	return 0;
    }

/*     Prepare the local string buffers to hold the possible */
/*     string arguments. */

    s_copy(locidw, idword, (ftnlen)8, idword_len);
    s_copy(locifn, ifname, (ftnlen)60, ifname_len);
    s_copy(locfmt, strbff + (((i__1 = *outbff - 1) < 4 && 0 <= i__1 ? i__1 : 
	    s_rnge("strbff", i__1, "t_daswfr__", (ftnlen)322)) << 3), (ftnlen)
	    8, (ftnlen)8);
    s_copy(prespc, " ", (ftnlen)599, (ftnlen)1);

/*     First, determine if we are to write to the native file format. */

    if (*outbff == natbff) {

/*        Initialize RECORD. */

	for (i__ = 1; i__ <= 1024; ++i__) {
	    *(unsigned char *)&record[i__ - 1] = '\0';
	}
	if (*addftp) {
	    io___18.ciunit = *unit;
	    iostat = s_wdue(&io___18);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, locidw, (ftnlen)8);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, locifn, (ftnlen)60);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, (char *)&(*nresvr), (ftnlen)sizeof(integer)
		    );
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, (char *)&(*nresvc), (ftnlen)sizeof(integer)
		    );
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, (char *)&(*ncomr), (ftnlen)sizeof(integer))
		    ;
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, (char *)&(*ncomc), (ftnlen)sizeof(integer))
		    ;
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, locfmt, (ftnlen)8);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, prespc, (ftnlen)599);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, ftpstr, (ftnlen)28);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = do_uio(&c__1, record, (ftnlen)297);
	    if (iostat != 0) {
		goto L100001;
	    }
	    iostat = e_wdue();
L100001:
	    ;
	} else {
	    io___19.ciunit = *unit;
	    iostat = s_wdue(&io___19);
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, locidw, (ftnlen)8);
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, locifn, (ftnlen)60);
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, (char *)&(*nresvr), (ftnlen)sizeof(integer)
		    );
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, (char *)&(*nresvc), (ftnlen)sizeof(integer)
		    );
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, (char *)&(*ncomr), (ftnlen)sizeof(integer))
		    ;
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, (char *)&(*ncomc), (ftnlen)sizeof(integer))
		    ;
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = do_uio(&c__1, record, (ftnlen)940);
	    if (iostat != 0) {
		goto L100002;
	    }
	    iostat = e_wdue();
L100002:
	    ;
	}
	if (iostat != 0) {
	    setmsg_("Unable to write to #. IOSTAT was #", (ftnlen)34);
	    errfnm_("#", unit, (ftnlen)1);
	    errint_("#", &iostat, (ftnlen)1);
	    sigerr_("SPICE(FILEWRITEFAILED)", (ftnlen)22);
	    chkout_("T_DASWFR", (ftnlen)8);
	    return 0;
	}

/*     Handle the non-native case. */

    } else {

/*        Initialize RECORD. */

	for (i__ = 1; i__ <= 1024; ++i__) {
	    *(unsigned char *)&record[i__ - 1] = '\0';
	}

/*        Populate RECORD. */

	s_copy(record, locidw, (ftnlen)8, (ftnlen)8);
	s_copy(record + 8, locifn, (ftnlen)60, (ftnlen)60);

/*        Convert and place reserved record and comment */
/*        parameters. */

	t_xltfwi__(nresvr, &c__1, outbff, holder, (ftnlen)4);
	s_copy(record + 68, holder, (ftnlen)4, (ftnlen)4);
	t_xltfwi__(nresvc, &c__1, outbff, holder, (ftnlen)4);
	s_copy(record + 72, holder, (ftnlen)4, (ftnlen)4);
	t_xltfwi__(ncomr, &c__1, outbff, holder, (ftnlen)4);
	s_copy(record + 76, holder, (ftnlen)4, (ftnlen)4);
	t_xltfwi__(ncomc, &c__1, outbff, holder, (ftnlen)4);
	s_copy(record + 80, holder, (ftnlen)4, (ftnlen)4);

/*        Add the FTP string if appropriate. */

	if (*addftp) {
	    s_copy(record + 84, locfmt, (ftnlen)8, (ftnlen)8);
	    s_copy(record + 699, ftpstr, ftplen, (ftnlen)28);
	}

/*        Dump the record to the file. */

	io___21.ciunit = *unit;
	iostat = s_wdue(&io___21);
	if (iostat != 0) {
	    goto L100003;
	}
	iostat = do_uio(&c__1, record, (ftnlen)1024);
	if (iostat != 0) {
	    goto L100003;
	}
	iostat = e_wdue();
L100003:
	if (iostat != 0) {
	    setmsg_("Unable to write to #. IOSTAT was #", (ftnlen)34);
	    errfnm_("#", unit, (ftnlen)1);
	    errint_("#", &iostat, (ftnlen)1);
	    sigerr_("SPICE(FILEWRITEFAILED)", (ftnlen)22);
	    chkout_("T_DASWFR", (ftnlen)8);
	    return 0;
	}
    }
    chkout_("T_DASWFR", (ftnlen)8);
    return 0;
} /* t_daswfr__ */

