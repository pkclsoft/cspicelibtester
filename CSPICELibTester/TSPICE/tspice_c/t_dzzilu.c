/* t_dzzilu.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static doublereal c_b4 = 240.;

/* $Procedure T_DZZILU ( Illumination angle rates ) */
/* Subroutine */ int t_dzzilu__(char *method, char *target, char *illum, 
	doublereal *et, char *fixref, char *abcorr, char *obsrvr, doublereal *
	spoint, doublereal *normal, doublereal *dphase, doublereal *dincdn, 
	doublereal *demssn, ftnlen method_len, ftnlen target_len, ftnlen 
	illum_len, ftnlen fixref_len, ftnlen abcorr_len, ftnlen obsrvr_len)
{
    extern doublereal t_iluinc__(), t_iluemi__(), t_iluini__(char *, char *, 
	    char *, char *, char *, char *, doublereal *, doublereal *, 
	    ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), t_iluphs__();
    doublereal f;
    extern /* Subroutine */ int chkin_(char *, ftnlen);
    extern logical failed_(void);
    integer nrtain;
    doublereal retval;
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    integer nterms;
    extern logical return_(void);
    extern /* Subroutine */ int t_dcheb__(D_fp, doublereal *, doublereal *, 
	    integer *, integer *, doublereal *, doublereal *);

/* $ Abstract */

/*     Compute by numeric methods the derivatives with respect to */
/*     time of illumination angles. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     METHOD     I   Computation method. */
/*     TARGET     I   Name of target body associated with surface point. */
/*     ILLUM      I   Name of illumination source. */
/*     ET         I   Observation epoch (TDB). */
/*     FIXREF     I   Body-fixed reference frame. */
/*     ABCORR     I   Aberration correction. */
/*     OBSRVR     I   Name of observer. */
/*     SPOINT     I   Surface point. */
/*     NORMAL     I   Outward normal vector at surface point. */
/*     DPHASE     O   Phase angle rate. */
/*     DINCDN     O   Illumination source incidence angle rate. */
/*     DEMSSN     O   Emission angle rate. */

/* $ Detailed_Input */

/*     METHOD         is a string specifying the computation method to */
/*                    be used by this routine. The only value currently */
/*                    allowed is 'ELLIPSOID'. This indicates that the */
/*                    target body shape is modeled as an ellipsoid. */

/*                    Case and leading and trailing blanks are not */
/*                    significant in METHOD. */

/*     TARGET         is the name of the target body associated with the */
/*                    surface point SPOINT. TARGET may be a body name or */
/*                    an ID code provided as as string. */

/*     ILLUM          is the name of the illumination source used to */
/*                    define the illumination angles computed by this */
/*                    routine. ILLUM may be a body name or an ID code */
/*                    provided as as string. */

/*     ET             is the observation time. ET is expressed as */
/*                    seconds past J2000 TDB. */

/*     FIXREF         is the name of the body-centered, body-fixed */
/*                    reference frame relative to which SPOINT is */
/*                    specified. The frame's center must coincide with */
/*                    TARGET. */

/*     ABCORR         indicates the aberration corrections to be */
/*                    applied. Only reception corrections are supported. */
/*                    See the header of ILUMIN for a discussion of */
/*                    aberration corrections used in illumination angle */
/*                    computations. */

/*     OBSRVR         is the name of the observing body. OBSRVR may be a */
/*                    body name or an ID code provided as as string. */

/*     SPOINT         is a 3-vector containing the cartesian coordinates */
/*                    of the surface point at which the illumination */
/*                    angle rates are to be computed. SPOINT is */
/*                    expressed in the body-centered, body-fixed frame */
/*                    designated by FIXREF (see description above). */

/*                    Units are km. */

/*     NORMAL         is an outward normal vector to be used for */
/*                    emission angle and illumination source incidence */
/*                    angle calculations. NORMAL should be orthogonal */
/*                    at SPOINT to the target body's surface. */

/* $ Detailed_Output */

/*     DPHASE         is the rate of change of the phase angle with */
/*                    respect to TDB, evaluated at ET. */

/*     DINCDN         is the rate of change of the illumination source */
/*                    incidence angle with respect to TDB, evaluated at */
/*                    ET. */

/*     DEMSSN         is the rate of change of the emission angle with */
/*                    respect to TDB, evaluated at ET. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1)  If the computation method is not recognized, the error */
/*         will be signaled by a routine in the call tree of this */
/*         routine. */

/*     2)  If ABCORR specifies a transmission aberration correction, the */
/*         error will be signaled by a routine in the call tree of this */
/*         routine. */

/*     3)  If an error occurs while looking up a state vector, the */
/*         error will be signaled by a routine in the call tree of */
/*         this routine. */

/* $ Files */

/*     See GFILUM. */

/* $ Particulars */

/*     This is a specialized routine intended for use only */
/*     by F_ZZILUSTA. */

/* $ Examples */

/*     See usage in F_ZZILUSTA. */

/* $ Restrictions */

/*     1) This routine is intended for use only by the GF subsystem. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.1.0, 27-AUG-2013 (NJB) */

/*        Bug fix: T_ILUINI is now called using function */
/*        call syntax. */

/* -    SPICELIB Version 2.0.0, 09-APR-2012 (NJB) */

/* -& */

/*     External routines */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_DZZILU", (ftnlen)8);

/*     Initialize angle computation functions: */
/*     store inputs to ZZILUMIN. */

    retval = t_iluini__(method, target, illum, fixref, abcorr, obsrvr, spoint,
	     normal, method_len, target_len, illum_len, fixref_len, 
	    abcorr_len, obsrvr_len);
    if (failed_()) {
	chkout_("T_DZZILU", (ftnlen)8);
	return 0;
    }

/*     Set the number of terms in the Chebyshev expansions */
/*     we'll use. Set the number of retained terms as well. */

    nterms = 25;
    nrtain = 6;

/*     Estimate the derivatives of the illumination angles */
/*     at ET. */

    t_dcheb__((D_fp)t_iluphs__, et, &c_b4, &nterms, &nrtain, &f, dphase);
    t_dcheb__((D_fp)t_iluinc__, et, &c_b4, &nterms, &nrtain, &f, dincdn);
    t_dcheb__((D_fp)t_iluemi__, et, &c_b4, &nterms, &nrtain, &f, demssn);
    chkout_("T_DZZILU", (ftnlen)8);
    return 0;
} /* t_dzzilu__ */

