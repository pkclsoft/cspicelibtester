/* t_ilufun.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* $Procedure T_ILUFUN ( TSPICE illumination angle functions ) */
doublereal t_ilufun__0_(int n__, char *method, char *target, char *illum, 
	doublereal *et, char *fixref, char *abcorr, char *obsrvr, doublereal *
	spoint, doublereal *normal, ftnlen method_len, ftnlen target_len, 
	ftnlen illum_len, ftnlen fixref_len, ftnlen abcorr_len, ftnlen 
	obsrvr_len)
{
    /* System generated locals */
    doublereal ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int vequ_(doublereal *, doublereal *), zzilusta_(
	    char *, char *, char *, doublereal *, char *, char *, char *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), 
	    chkin_(char *, ftnlen);
    static char svref[32], svobs[36];
    static doublereal svpnt[3];
    doublereal locinc[2], locemi[2], locphs[2];
    extern /* Subroutine */ int sigerr_(char *, ftnlen), chkout_(char *, 
	    ftnlen);
    static char svmeth[50], svtarg[36], svcorr[10];
    extern logical return_(void);
    static char svilum[36];
    static doublereal svnrml[3];

/* $ Abstract */

/*     This is an umbrella routine for functions that return */
/*     illumination angles. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Entry point */
/*     --------  ---  -------------------------------------------------- */
/*     METHOD     I   T_ILUINI */
/*     TARGET     I   T_ILUINI */
/*     ILLUM      I   T_ILUINI */
/*     ET         I   T_ILUPHS, T_ILUINC, T_ILUEMI */
/*     FIXREF     I   T_ILUINI */
/*     ABCORR     I   T_ILUINI */
/*     OBSRVR     I   T_ILUINI */
/*     SPOINT     I   T_ILUINI */
/*     NORMAL     I   T_ILUINI */

/* $ Detailed_Input */

/*     See the headers of the entry points. */

/* $ Detailed_Output */

/*     See the headers of the entry points. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1)  If this routine is called directly, the error */
/*         SPICE(BOGUSENTRY) is signaled. */

/* $ Files */

/*     See ILLUMG. */

/* $ Particulars */

/*     This is a specialized routine intended for use only */
/*     by F_ZZILUSTA. */

/*     This suite of routines is intended to be used as follows: */

/*        1) The calling test program should first call */

/*              T_ILUINI */

/*           to store the inputs, other than time, that will */
/*           be used to compute illumination angles. */

/*        2) Call any of the entry points */

/*              T_ILUPHS */
/*              T_ILUINC */
/*              T_ILUEMI */

/*           to compute illumination angles at a time of interest. There */
/*           is no need to call T_ILUINI between calls to these routines */
/*           unless any of the inputs, other than time, have changed. */

/* $ Examples */

/*     See usage in F_ZZILUSTA. */

/* $ Restrictions */

/*     1) This routine is intended for use only by the GF subsystem. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 06-APR-2012 (NJB) */

/* -& */

/*     Entry points */


/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */

    /* Parameter adjustments */
    if (spoint) {
	}
    if (normal) {
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_t_iluini;
	case 2: goto L_t_iluphs;
	case 3: goto L_t_iluinc;
	case 4: goto L_t_iluemi;
	}

    chkin_("T_ILUFUN", (ftnlen)8);
    ret_val = 0.;
    sigerr_("SPICE(BOGUSENTRY)", (ftnlen)17);
    chkout_("T_ILUFUN", (ftnlen)8);
    return ret_val;
/* $Procedure T_ILUINI ( TSPICE: initialize entry points. ) */

L_t_iluini:
/* $ Abstract */

/*     Initialize the illumination angle functions in this */
/*     suite of test routines: store inputs to ZZILUMIN. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */

/*     IMPLICIT NONE */

/*     CHARACTER*(*)         METHOD */
/*     CHARACTER*(*)         TARGET */
/*     CHARACTER*(*)         ILLUM */
/*     CHARACTER*(*)         FIXREF */
/*     CHARACTER*(*)         ABCORR */
/*     CHARACTER*(*)         OBSRVR */
/*     DOUBLE PRECISION      SPOINT ( 3 ) */
/*     DOUBLE PRECISION      NORMAL ( 3 ) */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     METHOD     I   Computation method. */
/*     TARGET     I   Name of target body associated with surface point. */
/*     ILLUM      I   Name of illumination source. */
/*     FIXREF     I   Body-fixed reference frame. */
/*     ABCORR     I   Aberration correction. */
/*     OBSRVR     I   Name of observer. */
/*     SPOINT     I   Surface point. */
/*     NORMAL     I   Outward normal vector at surface point. */

/* $ Detailed_Input */

/*     METHOD         is a string specifying the computation method to */
/*                    be used by this routine. The only value currently */
/*                    allowed is 'ELLIPSOID'. This indicates that the */
/*                    target body shape is modeled as an ellipsoid. */

/*                    Case and leading and trailing blanks are not */
/*                    significant in METHOD. */

/*     TARGET         is the name of the target body associated with the */
/*                    surface point SPOINT. TARGET may be a body name or */
/*                    an ID code provided as as string. */

/*     ILLUM          is the name of the illumination source used to */
/*                    define the illumination angles computed by this */
/*                    routine. ILLUM may be a body name or an ID code */
/*                    provided as as string. */

/*     FIXREF         is the name of the body-centered, body-fixed */
/*                    reference frame relative to which SPOINT is */
/*                    specified. The frame's center must coincide with */
/*                    TARGET. */

/*     ABCORR         indicates the aberration corrections to be */
/*                    applied. Only reception corrections are supported. */
/*                    See the header of ILUMIN for a discussion of */
/*                    aberration corrections used in illumination angle */
/*                    computations. */

/*     OBSRVR         is the name of the observing body. OBSRVR may be a */
/*                    body name or an ID code provided as as string. */

/*     SPOINT         is a 3-vector containing the cartesian coordinates */
/*                    of the surface point at which the illumination */
/*                    angle rates are to be computed. SPOINT is */
/*                    expressed in the body-centered, body-fixed frame */
/*                    designated by FIXREF (see description above). */

/*                    Units are km. */

/*     NORMAL         is an outward normal vector to be used for */
/*                    emission angle and illumination source incidence */
/*                    angle calculations. NORMAL should be orthogonal */
/*                    at SPOINT to the target body's surface. */

/* $ Detailed_Output */

/*     None. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     None. */

/* $ Files */

/*     See ILLUMG. */

/* $ Particulars */

/*     This routine stores inputs required by ZZILUSTA. The stored inputs */
/*     are used by the other entry points to compute illumination angles */
/*     a specified times. */

/* $ Examples */

/*     See usage in T_DZZILU. */

/* $ Restrictions */

/*     1) This is a specialized routine intended for use only */
/*         by F_ZZILUSTA. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 06-APR-2012 (NJB) */

/* -& */

/*     Give this function a return value. */

    ret_val = 0.;
    if (return_()) {
	return ret_val;
    }
    chkin_("T_ILUINI", (ftnlen)8);
    s_copy(svmeth, method, (ftnlen)50, method_len);
    s_copy(svtarg, target, (ftnlen)36, target_len);
    s_copy(svilum, illum, (ftnlen)36, illum_len);
    s_copy(svref, fixref, (ftnlen)32, fixref_len);
    s_copy(svcorr, abcorr, (ftnlen)10, abcorr_len);
    s_copy(svobs, obsrvr, (ftnlen)36, obsrvr_len);
    vequ_(spoint, svpnt);
    vequ_(normal, svnrml);
    chkout_("T_ILUINI", (ftnlen)8);
    return ret_val;
/* $Procedure T_ILUPHS ( TSPICE: Compute phase angle ) */

L_t_iluphs:
/* $ Abstract */

/*     Compute the phase angle at ET. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */

/*     DOUBLE PRECISION      ET */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     ET         I   Epoch at which to compute phase angle. */

/*     The function returns the phase angle at ET defined by */
/*     the stored inputs. */

/* $ Detailed_Input */

/*     ET             is the epoch at which a phase angle is */
/*                    to be computed. ET is expressed as seconds */
/*                    past J2000 TDB. */

/* $ Detailed_Output */

/*     The function returns the phase angle at ET defined by */
/*     the inputs saved by the last call to T_ILUINI. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     See the Exceptions section of the header of ZZILUSTA. */

/* $ Files */

/*     See ILLUMG. */

/* $ Particulars */

/*     This routine uses ZZILUSTA to compute a specified phase angle */
/*     at a given time. The inputs other than time are saved values */
/*     stored by the last call to T_ILUINI. */

/*     See ILLUMG for a description of this angle. */

/* $ Examples */

/*     See usage in T_DZZILU. */

/* $ Restrictions */

/*     1) This is a specialized routine intended for use only */
/*         by F_ZZILUSTA. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 06-APR-2012 (NJB) */

/* -& */
    ret_val = 0.;
    if (return_()) {
	return ret_val;
    }
    chkin_("T_ILUPHS", (ftnlen)8);
    zzilusta_(svmeth, svtarg, svilum, et, svref, svcorr, svobs, svpnt, svnrml,
	     locphs, locinc, locemi, (ftnlen)50, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    ret_val = locphs[0];
    chkout_("T_ILUPHS", (ftnlen)8);
    return ret_val;
/* $Procedure T_ILUINC ( TSPICE: Compute incidence angle ) */

L_t_iluinc:
/* $ Abstract */

/*     Compute the illumination source incidence angle at ET. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */

/*     DOUBLE PRECISION      ET */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     ET         I   Epoch at which to compute incidence angle. */

/*     The function returns the illumination source angle at ET defined */
/*     by the stored inputs. */

/* $ Detailed_Input */

/*     ET             is the epoch at which the illumination source */
/*                    incidence angle is to be computed. ET is expressed */
/*                    as seconds past J2000 TDB. */

/* $ Detailed_Output */

/*     The function returns the illumination source incidence angle at */
/*     ET defined by the inputs saved by the last call to T_ILUINI. */

/*     See ILLUMG for a description of this angle. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     See the Exceptions section of the header of ZZILUSTA. */

/* $ Files */

/*     See ILLUMG. */

/* $ Particulars */

/*     This routine uses ZZILUSTA to compute a specified illumination */
/*     source incidence angle at a given time. The inputs other than */
/*     time are saved values stored by the last call to T_ILUINI. */

/* $ Examples */

/*     See usage in T_DZZILU. */

/* $ Restrictions */

/*     1) This is a specialized routine intended for use only */
/*         by F_ZZILUSTA. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 06-APR-2012 (NJB) */

/* -& */
    ret_val = 0.;
    if (return_()) {
	return ret_val;
    }
    chkin_("T_ILUINC", (ftnlen)8);
    zzilusta_(svmeth, svtarg, svilum, et, svref, svcorr, svobs, svpnt, svnrml,
	     locphs, locinc, locemi, (ftnlen)50, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    ret_val = locinc[0];
    chkout_("T_ILUINC", (ftnlen)8);
    return ret_val;
/* $Procedure T_ILUEMI ( TSPICE: Compute emission angle ) */

L_t_iluemi:
/* $ Abstract */

/*     Compute the emission angle at ET. */

/*     This is a TSPICE utility that supports F_ZZILUSTA. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     GF */
/*     PCK */
/*     TIME */
/*     SPK */

/* $ Keywords */

/*     ANGLE */
/*     GEOMETRY */
/*     SEARCH */
/*     UTILITY */

/* $ Declarations */

/*     DOUBLE PRECISION      ET */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     ET         I   Epoch at which to compute emission angle. */

/*     The function returns the emission angle at ET defined */
/*     by the stored inputs. */

/* $ Detailed_Input */

/*     ET             is the epoch at which the emission angle is */
/*                    to be computed. ET is expressed as seconds */
/*                    past J2000 TDB. */

/* $ Detailed_Output */

/*     The function returns the emission angle at ET defined by the */
/*     inputs saved by the last call to T_ILUINI. */

/*     See ILLUMG for a description of this angle. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     See the Exceptions section of the header of ZZILUSTA. */

/* $ Files */

/*     See ILLUMG. */

/* $ Particulars */

/*     This routine uses ZZILUSTA to compute a specified emission angle */
/*     at a given time. The inputs other than time are saved values */
/*     stored by the last call to T_ILUINI. */

/* $ Examples */

/*     See usage in T_DZZILU. */

/* $ Restrictions */

/*     1) This is a specialized routine intended for use only */
/*         by F_ZZILUSTA. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman    (JPL) */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 06-APR-2012 (NJB) */

/* -& */
    ret_val = 0.;
    if (return_()) {
	return ret_val;
    }
    chkin_("T_ILUEMI", (ftnlen)8);
    zzilusta_(svmeth, svtarg, svilum, et, svref, svcorr, svobs, svpnt, svnrml,
	     locphs, locinc, locemi, (ftnlen)50, (ftnlen)36, (ftnlen)36, (
	    ftnlen)32, (ftnlen)10, (ftnlen)36);
    ret_val = locemi[0];
    chkout_("T_ILUEMI", (ftnlen)8);
    return ret_val;
} /* t_ilufun__ */

doublereal t_ilufun__(char *method, char *target, char *illum, doublereal *et,
	 char *fixref, char *abcorr, char *obsrvr, doublereal *spoint, 
	doublereal *normal, ftnlen method_len, ftnlen target_len, ftnlen 
	illum_len, ftnlen fixref_len, ftnlen abcorr_len, ftnlen obsrvr_len)
{
    return t_ilufun__0_(0, method, target, illum, et, fixref, abcorr, obsrvr, 
	    spoint, normal, method_len, target_len, illum_len, fixref_len, 
	    abcorr_len, obsrvr_len);
    }

doublereal t_iluini__(char *method, char *target, char *illum, char *fixref, 
	char *abcorr, char *obsrvr, doublereal *spoint, doublereal *normal, 
	ftnlen method_len, ftnlen target_len, ftnlen illum_len, ftnlen 
	fixref_len, ftnlen abcorr_len, ftnlen obsrvr_len)
{
    return t_ilufun__0_(1, method, target, illum, (doublereal *)0, fixref, 
	    abcorr, obsrvr, spoint, normal, method_len, target_len, illum_len,
	     fixref_len, abcorr_len, obsrvr_len);
    }

doublereal t_iluphs__(doublereal *et)
{
    return t_ilufun__0_(2, (char *)0, (char *)0, (char *)0, et, (char *)0, (
	    char *)0, (char *)0, (doublereal *)0, (doublereal *)0, (ftnint)0, 
	    (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

doublereal t_iluinc__(doublereal *et)
{
    return t_ilufun__0_(3, (char *)0, (char *)0, (char *)0, et, (char *)0, (
	    char *)0, (char *)0, (doublereal *)0, (doublereal *)0, (ftnint)0, 
	    (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

doublereal t_iluemi__(doublereal *et)
{
    return t_ilufun__0_(4, (char *)0, (char *)0, (char *)0, et, (char *)0, (
	    char *)0, (char *)0, (doublereal *)0, (doublereal *)0, (ftnint)0, 
	    (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

