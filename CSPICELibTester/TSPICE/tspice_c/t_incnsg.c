/* t_incnsg.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* $Procedure   T_INCNSG ( Test intersection of cone and line segment ) */
/* Subroutine */ int t_incnsg__(doublereal *apex, doublereal *axis, 
	doublereal *angle, doublereal *endpt1, doublereal *endpt2, integer *
	nxpts, doublereal *xpt1, doublereal *xpt2)
{
    /* Initialized data */

    static doublereal origin[3] = { 0.,0.,0. };
    static doublereal y[3] = { 0.,1.,0. };
    static doublereal z__[3] = { 0.,0.,1. };

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    double cos(doublereal);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal dmag;
    extern /* Subroutine */ int vadd_(doublereal *, doublereal *, doublereal *
	    );
    doublereal minp[3], maxp[3], udir[3];
    extern /* Subroutine */ int vhat_(doublereal *, doublereal *);
    extern doublereal vdot_(doublereal *, doublereal *);
    extern /* Subroutine */ int vsub_(doublereal *, doublereal *, doublereal *
	    ), vequ_(doublereal *, doublereal *);
    doublereal plnx[3];
    extern /* Subroutine */ int mtxv_(doublereal *, doublereal *, doublereal *
	    );
    doublereal uoff1[3], uoff2[3], xoff1[3], xoff2[3];
    extern /* Subroutine */ int zzsglatx_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    integer i__;
    extern /* Subroutine */ int zzcxbrut_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, logical *)
	    ;
    doublereal x[3];
    extern /* Subroutine */ int frame_(doublereal *, doublereal *, doublereal 
	    *), chkin_(char *, ftnlen);
    doublereal axmag, colat;
    extern /* Subroutine */ int errdp_(char *, doublereal *, ftnlen);
    extern doublereal vdist_(doublereal *, doublereal *);
    doublereal uaxis[3], vtemp[3], xform[9]	/* was [3][3] */;
    integer nplnx;
    extern /* Subroutine */ int unorm_(doublereal *, doublereal *, doublereal 
	    *);
    doublereal vtemp2[3];
    extern /* Subroutine */ int nvp2pl_(doublereal *, doublereal *, 
	    doublereal *);
    extern logical failed_(void);
    extern doublereal pi_(void), halfpi_(void);
    doublereal locang, cosang;
    logical isbrck;
    doublereal minlat, maxlat;
    extern /* Subroutine */ int sigerr_(char *, ftnlen), chkout_(char *, 
	    ftnlen), setmsg_(char *, ftnlen);
    doublereal dp1, dp2, nrmpln[4];
    logical in1, in2;
    extern /* Subroutine */ int inrypl_(doublereal *, doublereal *, 
	    doublereal *, integer *, doublereal *);
    extern logical return_(void);
    doublereal dir[3];
    extern /* Subroutine */ int mxv_(doublereal *, doublereal *, doublereal *)
	    ;
    logical neg1;
    doublereal off1[3], off2[3];
    logical neg2;

/* $ Abstract */

/*     Test utility: exercise the "brute force" logic of INCNSG. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     CONE */
/*     GEOMETRY */
/*     INTERSECTION */
/*     LINE */
/*     MATH */
/*     SEGMENT */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     APEX       I   Apex of cone. */
/*     AXIS       I   Axis of cone. */
/*     ANGLE      I   Angle of cone. */
/*     ENDPT1, */
/*     ENDPT2     I   Endpoints of line segment. */
/*     NXPTS      O   Number of intersection points. */
/*     XPT1       O   First intersection point, if it exists. */
/*     XPT2       O   Second intersection point, if it exists. */

/* $ Detailed_Input */

/*     APEX       is the apex (tip) of the cone. In this routine's */
/*                documentation, we'll consider the cone to be a */
/*                semi-infinite pyramid with circular cross-section. In */
/*                some contexts, this object is called one "nappe" of */
/*                the complete cone. */

/*     AXIS       is an axis vector of the cone. */

/*     ANGLE      is the angular separation from AXIS of the rays */
/*                comprising the cone. Let the notation */

/*                   < A, B > */

/*                denote the dot product of vectors A and B, and let */

/*                   ||A|| */

/*                denote the norm of vector A. Then the cone is the set */
/*                of points */

/*                             X-APEX       AXIS */
/*                   { X:  < ----------,  -------- >  =  cos(ANGLE) } */
/*                           ||X-APEX||   ||AXIS|| */


/*     ENDPT1, */
/*     ENDPT2     are endpoints of a line segment. These points */
/*                must be distinct. */

/* $ Detailed_Output */

/*     NXPTS      is the number of points of intersection of the input */
/*                line segment and cone. */

/*     XPT1       is the point of intersection of the segment and cone */
/*                that is closest to ENDPT1, if an intersection exists. */
/*                If there are no intersections, XPT1 is undefined. */

/*     XPT2       is the point of intersection of the segment and cone */
/*                that is farthest from ENDPT1, if two points of */
/*                intersection exist. If there are not two */
/*                intersections, XPT2 is undefined. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1)  If AXIS is the zero vector, the error SPICE(ZEROVECTOR) */
/*         will be signaled. */

/*     2)  If ANGLE is less than zero, the error SPICE(INVALIDANGLE) */
/*         will be signaled. */

/*     3)  If ENDPT1 and ENDPT2 coincide, the error */
/*         SPICE(ENDPOINTSMATCH) will be signaled. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine is used by the SPICELIB DSK subsystem. In */
/*     particular, it is used to determine whether a ray contacts a */
/*     latitude boundary of a volume element in either planetocentric */
/*     latitudinal or planetodetic coordinates. */

/* $ Examples */

/*     The numerical results shown for these examples may differ across */
/*     platforms. The results depend on the SPICE kernels used as input */
/*     (if any), the compiler and supporting libraries, and the machine */
/*     specific arithmetic implementation. */

/*     1) Compute the intersection of a line segment and cone in */
/*        a simple case for which the results can easily be checked. */

/*        Let the apex of the cone be at the origin. Let the axis */
/*        of the cone lie on the +X axis. Let the angle of the cone */
/*        be 45 degrees. Let the line segment have endpoints */

/*           ENDPT1 = ( 1,   -2, sqrt(3)/2 ) */
/*           ENDPT2 = ( 1,    2, sqrt(3)/2 ) */

/*        We expect there to be two points of intersection: */

/*           XPT1   = ( 1, -1/2, sqrt(3)/2 ) */
/*           XPT2   = ( 1,  1/2, sqrt(3)/2 ) */


/*        Example code begins here. */


/*              PROGRAM EX1 */
/*              IMPLICIT NONE */
/*        C */
/*        C     SPICELIB functions */
/*        C */
/*              DOUBLE PRECISION      RPD */
/*        C */
/*        C     Local parameters */
/*        C */
/*              CHARACTER*(*)         FMT1 */
/*              PARAMETER           ( FMT1 = '(A,3F13.8)' ) */

/*              CHARACTER*(*)         FMT2 */
/*              PARAMETER           ( FMT2 = '(A,I2)' ) */
/*        C */
/*        C     Local variables */
/*        C */
/*              DOUBLE PRECISION      ANGLE */
/*              DOUBLE PRECISION      APEX   ( 3 ) */
/*              DOUBLE PRECISION      AXIS   ( 3 ) */
/*              DOUBLE PRECISION      ENDPT1 ( 3 ) */
/*              DOUBLE PRECISION      ENDPT2 ( 3 ) */
/*              DOUBLE PRECISION      SQ3 */
/*              DOUBLE PRECISION      XPT1   ( 3 ) */
/*              DOUBLE PRECISION      XPT2   ( 3 ) */

/*              INTEGER               NXPTS */

/*        C */
/*        C     Set up the cone's geometric attributes. */
/*        C */
/*              CALL VPACK ( 0.D0, 0.D0, 0.D0, APEX ) */
/*              CALL VPACK ( 1.D0, 0.D0, 0.D0, AXIS ) */

/*              ANGLE = 45.D0 * RPD() */
/*        C */
/*        C     Initialize the line segment's endpoints. */
/*        C */
/*              SQ3 = SQRT( 3.D0  ) */

/*              CALL VPACK ( 1.D0, -2.D0, SQ3/2, ENDPT1 ) */
/*              CALL VPACK ( 1.D0,  2.D0, SQ3/2, ENDPT2 ) */
/*        C */
/*        C     Find the points of intersection. */
/*        C */
/*              CALL INCNSG ( APEX,   AXIS,  ANGLE, ENDPT1, */
/*             .              ENDPT2, NXPTS, XPT1,  XPT2   ) */

/*              WRITE (*,*) ' ' */
/*              WRITE (*,FMT1) 'Apex:        ', APEX */
/*              WRITE (*,FMT1) 'Axis:        ', AXIS */
/*              WRITE (*,FMT1) 'Angle (deg): ', ANGLE/RPD() */
/*              WRITE (*,FMT1) 'Endpoint 1:  ', ENDPT1 */
/*              WRITE (*,FMT1) 'Endpoint 2:  ', ENDPT2 */
/*              WRITE (*,*) ' ' */
/*              WRITE (*,FMT2) 'Number of intersection points: ', */
/*             .            NXPTS */
/*              WRITE (*,*) ' ' */
/*              WRITE (*,FMT1) 'Point 1:    ', XPT1 */
/*              WRITE (*,FMT1) 'Point 2:    ', XPT2 */
/*              WRITE (*,*) ' ' */

/*              END */


/*     When this program was executed on a PC/Linux/gfortran/64-bit */
/*     platform, the output was: */


/*        Apex:           0.00000000   0.00000000   0.00000000 */
/*        Axis:           1.00000000   0.00000000   0.00000000 */
/*        Angle (deg):   45.00000000 */
/*        Endpoint 1:     1.00000000  -2.00000000   0.86602540 */
/*        Endpoint 2:     1.00000000   2.00000000   0.86602540 */

/*        Number of intersection points:  2 */

/*        Point 1:       1.00000000  -0.50000000   0.86602540 */
/*        Point 2:       1.00000000   0.50000000   0.86602540 */


/* $ Restrictions */

/*     1)  If the sine of the input cone angle is 1, this routine treats */
/*         the cone as though it were a plane normal to the input axis */
/*         and containing the apex. */

/*     2)  This routine is designed to avoid arithmetic overflow in */
/*         normal cases, such as those in which the line segment is */
/*         nearly parallel to the cone. However, it is possible to cause */
/*         arithmetic overflow by using input vectors with extremely */
/*         large magnitudes. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    SPICELIB Version 1.0.0 30-SEP-2016 (NJB) */

/* -& */
/* $ Index_Entries */

/*     intersection of line segment and cone */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved values */


/*     Initial values */


/*     Use quasi-discovery check-in. We'll check in before */
/*     code sections that can generate SPICE errors, and check */
/*     out afterwards. When those code sections are skipped, */
/*     we avoid traceback participation. */

    if (return_()) {
	return 0;
    }

/*     No intersection was found so far. */

    *nxpts = 0;

/*     The cone's axis vector must be non-zero. */

    unorm_(axis, uaxis, &axmag);
    if (axmag == 0.) {
	chkin_("INCNSG", (ftnlen)6);
	setmsg_("The cone's axis vector must be non-zero but sadly, it faile"
		"d to meet this criterion.", (ftnlen)84);
	sigerr_("SPICE(ZEROVECTOR)", (ftnlen)17);
	chkout_("INCNSG", (ftnlen)6);
	return 0;
    }

/*     The cone's angular radius must be non-negative. */

    if (*angle < 0.) {
	chkin_("INCNSG", (ftnlen)6);
	setmsg_("The cone's angular radius must be  non-negative but was # ("
		"radians).", (ftnlen)68);
	errdp_("#", angle, (ftnlen)1);
	sigerr_("SPICE(INVALIDANGLE)", (ftnlen)19);
	chkout_("INCNSG", (ftnlen)6);
	return 0;
    }

/*     The endpoints of the segment must be distinct. Check this after */
/*     computing a unit direction vector for the line segment. */

    vsub_(endpt2, endpt1, dir);
    unorm_(dir, udir, &dmag);
    if (dmag == 0.) {
	chkin_("INCNSG", (ftnlen)6);
	setmsg_("The distance between the segment's endpoints was zero. Firs"
		"t endpoint: (# # #).", (ftnlen)79);
	errdp_("#", endpt1, (ftnlen)1);
	errdp_("#", &endpt1[1], (ftnlen)1);
	errdp_("#", &endpt1[2], (ftnlen)1);
	sigerr_("SPICE(ENDPOINTSMATCH)", (ftnlen)21);
	chkout_("INCNSG", (ftnlen)6);
	return 0;
    }

/*     Store the cosine of the cone's angular radius. We'll treat all */
/*     cases with COSANG equal to 0 as though the cone is actually a */
/*     plane normal to the axis and containing the apex. */

    cosang = cos(*angle);
    locang = *angle;

/*     We'll work with a local axis that has angular separation of */
/*     no more than pi/2 from the nappe. */

    if (cosang < 0.) {
	locang = pi_() - *angle;
	cosang = -cosang;
	uaxis[0] = -uaxis[0];
	uaxis[1] = -uaxis[1];
	uaxis[2] = -uaxis[2];
    }

/*     Compute the offsets of the endpoints of the segment from */
/*     the cone's apex. */

    vsub_(endpt1, apex, off1);
    vsub_(endpt2, apex, off2);

/*     Deal with some of the simple cases first. */

    vhat_(off1, uoff1);
    vhat_(off2, uoff2);
    dp1 = vdot_(uoff1, uaxis);
    dp2 = vdot_(uoff2, uaxis);

/*     The given axis is inside the nappe defined by the angular radius. */

/*     There's no intersection if both endpoints are in the interior of */
/*     the nappe of the cone (since the nappe is convex). */

    in1 = dp1 >= cosang;
    in2 = dp2 >= cosang;

/*     If the line segment lies on the far side of the plane that */
/*     contains the apex and is orthogonal to the axis, there's no */
/*     intersection. */

    neg1 = dp1 < 0.;
    neg2 = dp2 < 0.;
    if (in1 && in2 || neg1 && neg2) {

/*        The segment is in the interior of the cone or */
/*        on the far side of the plane. */

	*nxpts = 0;
	return 0;
    }

/*     Here's where we handle the half-space case. */

    if (cosang == 0.) {

/*        See whether the ray emanating from the first endpoint and */
/*        having direction UDIR hits the plane normal to the axis and */
/*        containing the apex. We'll call this plane NRMPLN. */

/*        NVP2PL can signal an error only if the input axis is the */
/*        zero vector. We've ensured that it isn't. */

	nvp2pl_(uaxis, apex, nrmpln);
	inrypl_(endpt1, udir, nrmpln, &nplnx, plnx);

/*        If the ray doesn't hit the plane, we're done. Otherwise, */
/*        check the intercept. */

	if (nplnx == 1) {

/*           The ray does hit the plane. If the intersection is on the */
/*           line segment, we have a solution. */

	    if (vdist_(plnx, endpt1) <= dmag) {

/*              The intercept is not further along the ray than the */
/*              second endpoint. It's a valid solution. */

		*nxpts = 1;
		vequ_(plnx, xpt1);
	    }
	}

/*        This is the end of the half-space case. */

	return 0;
    }
    if (*nxpts < 2) {

/*        We must determine the expected number of roots, and if */
/*        we didn't come up with them, we must find the roots */
/*        by an alternate method. */

/*        We'll examine the containment of the endpoints within the */
/*        cone. */

/*        The case where both endpoints are inside the cone was handled */
/*        earlier. */

/*        If one endpoint is inside the cone and one is outside, */
/*        we expect to have one root. */

	if (in1 && ! in2 || in2 && ! in1) {

/*           There's supposed to be one root. If we found none, find one */
/*           now. */

	    if (*nxpts == 0) {

/*              ZZCXBRUT signals an error if the axis is the zero */
/*              vector, but not otherwise. We've already ruled out this */
/*              situation. Therefore, we don't check in before the */
/*              following call. */

		zzcxbrut_(apex, uaxis, &locang, endpt1, endpt2, xpt1, &isbrck)
			;
		if (isbrck) {

/*                 As long as the root was bracketed, XPT1 is a */
/*                 solution. */

		    *nxpts = 1;
		}
	    }
	} else {
	    chkin_("INCNSG", (ftnlen)6);

/*           Both endpoints are outside the cone. We could have zero to */
/*           two roots. If the minimum angular separation of the segment */
/*           from the axis is less than ANGLE, we expect to find two */
/*           roots; if it's equal to ANGLE, we expect to find one, and */
/*           if it's greater than ANGLE, none. */

/*           We'll transform OFF1 and OFF2 into a reference frame in */
/*           which angular separation from the axis is equivalent to */
/*           colatitude. Then we'll find the maximum latitude attained */
/*           on the segment. */

/*           We'll count the roots we find, so we'll start at zero. */

	    *nxpts = 0;
	    frame_(uaxis, x, y);
	    for (i__ = 1; i__ <= 3; ++i__) {
		xform[(i__1 = i__ * 3 - 3) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"xform", i__1, "t_incnsg__", (ftnlen)602)] = x[(i__2 =
			 i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("x", i__2, 
			"t_incnsg__", (ftnlen)602)];
		xform[(i__1 = i__ * 3 - 2) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"xform", i__1, "t_incnsg__", (ftnlen)603)] = y[(i__2 =
			 i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("y", i__2, 
			"t_incnsg__", (ftnlen)603)];
		xform[(i__1 = i__ * 3 - 1) < 9 && 0 <= i__1 ? i__1 : s_rnge(
			"xform", i__1, "t_incnsg__", (ftnlen)604)] = uaxis[(
			i__2 = i__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"uaxis", i__2, "t_incnsg__", (ftnlen)604)];
	    }
	    mxv_(xform, off1, xoff1);
	    mxv_(xform, off2, xoff2);
	    zzsglatx_(xoff1, xoff2, &minlat, minp, &maxlat, maxp);
	    if (failed_()) {
		chkout_("INCNSG", (ftnlen)6);
		return 0;
	    }

/*           COLAT is the colatitude of the point of maximum latitude. */

	    colat = halfpi_() - maxlat;
	    if (colat < locang) {

/*              MAXP is inside the cone. There should be an intersection */
/*              on the segment between XOFF1 and MAXP and another */
/*              between MAXP and XOFF2. */

		zzcxbrut_(origin, z__, &locang, xoff1, maxp, vtemp, &isbrck);
		if (isbrck) {

/*                 Convert VTEMP to the original frame, then translate */
/*                 it so that it's represented as an offset from the */
/*                 origin. */

		    mtxv_(xform, vtemp, vtemp2);
		    vadd_(vtemp2, apex, xpt1);
		    *nxpts = 1;
		}
		zzcxbrut_(origin, z__, &locang, maxp, xoff2, vtemp, &isbrck);
		if (isbrck) {

/*                 Convert VTEMP to the original frame, then translate */
/*                 it so that it's represented as an offset from the */
/*                 origin. */

		    mtxv_(xform, vtemp, vtemp2);
		    vadd_(vtemp2, apex, xpt2);
		    if (*nxpts == 1) {

/*                    Both roots are valid. */

			*nxpts = 2;
		    } else {

/*                    The second root is the only valid root. Move it */
/*                    into XPT1. */

			vequ_(xpt2, xpt1);
			*nxpts = 1;
		    }
		}
	    } else if (colat == locang) {

/*              The root corresponds to a point of tangency of */
/*              the segment and cone. This occurs at the point */
/*              having maximum latitude: MAXP. */

		vequ_(maxp, xpt1);
		*nxpts = 1;

/*           Note that if COLAT > LOCANG, there are no roots. */

	    }
	    chkout_("INCNSG", (ftnlen)6);
	}

/*        This is the end of the "brute force" case with both endpoints */
/*        outside the cone. */

    }

/*     NXPTS  has been set. */

/*     If NXPTS is 1, then XPT1 is set. */

/*     If NXPTS is 2, then both XPT1 and XPT2 are set. */

    return 0;
} /* t_incnsg__ */

