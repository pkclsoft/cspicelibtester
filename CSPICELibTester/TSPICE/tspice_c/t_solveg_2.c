/* t_solveg_2.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* $Procedure      T_SOLVEG_2 ( Solve a system of algebraic equations ) */
/* Subroutine */ int t_solveg_2__(doublereal *b, integer *dim, integer *neq, 
	doublereal *a, doublereal *x, logical *found)
{
    /* System generated locals */
    integer b_dim1, b_dim2, b_offset, a_dim1, a_dim2, a_offset, x_dim1, 
	    x_dim2, x_offset, i__1, i__2, i__3, i__4, i__5, i__6;
    doublereal d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);
    double sqrt(doublereal);

    /* Local variables */
    doublereal temp, part;
    integer move, i__, j, k;
    extern /* Subroutine */ int swapd_(doublereal *, doublereal *);
    integer start;
    doublereal maxmag;
    extern logical return_(void);
    doublereal mag;

/* $ Abstract */

/*     Solve the matrix equation  B(i) = A X(i), i = 1, ... , n, */
/*     where A is a DIM by DIM matrix, B(i) is a set of n constant */
/*     vectors, and X(i) is a set of n vectors having dimension DIM. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     MATH */
/*     MATRIX */
/*     ROOT */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     DIM        I   Dimension of the constant and solution vectors. */
/*     NEQ        I   Number of equations. */
/*     B         I/O  Array of constant vectors of the matrix equation. */
/*     A         I/O  Coefficient matrix */
/*     X          O   Array of solution vectors. */
/*     FOUND      O   Found flag. */

/* $ Detailed_Input */

/*     DIM       Dimension of the constant vectors and solution vectors. */

/*     NEQ       Number of equations. */

/*     B         Array of constant vectors of the matrix equation B = */
/*               Ax.  The input is destroyed by the process of solving */
/*               the system of equations for the vector X. */

/*     A         A DIM by DIM matrix of coefficients for the system of */
/*               equations.  The coefficient matrix is destroyed by the */
/*               solution process.  Note: It is assumed that A is */
/*               declared as: */

/*                  DOUBLE PRECISION      A(DIM,DIM) */

/*               If this is not the case A should be packed so that */
/*               in it can be treated as if it had been declared */
/*               as above. */

/*               (That is in physical memory, data should be arranged */
/*               as */

/*                A(1,1) ... A(n,1), A(2,1), ... A(n,2), A(1,3) ... etc. */

/* $ Detailed_Output */

/*     X         is the solution to the system of equations */

/*                  B(i) = A * X(i), i = 1, ... , NEQ */

/*     FOUND     is a logical flag indicating whether a solution was */
/*               found.  FOUND will be set to .FALSE. if the coefficient */
/*               matrix A is singular. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     Error free. */

/*     1) If the routine detects that the matrix is singular, it will */
/*        return with FOUND set to .FALSE. */

/*     2) This routine takes no action regarding matrices that are */
/*        nearly singular.  The user should read particulars and */
/*        determine if his/her situation warrants special consideration */
/*        for nearly singular matrices. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     The problem of solving a system of N equations and N */
/*     unknowns can run into many numerical difficulties.  However, there */
/*     are many situations in which the system to be solved is small */
/*     and known to be well conditioned.  Under such situations, one */
/*     need not resort to the use of sophisticated black box routines */
/*     but may rely on techniques learned in elementary linear algebra */
/*     courses. */

/*     This routine is designed for accurate and stable solution of */
/*     systems of DIM equations in DIM variables where DIM is a small */
/*     integer (say less than 100).  It uses the method of Guassian */
/*     elimination with partial pivots.  This method while not the */
/*     fastest known method, is stable.  Moreover it is simple and */
/*     easy to maintain. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     This routine does not handle problems of nearly singular matrices. */
/*     If you suspect your coefficient matrix is close to singular, you */
/*     should consider alternate methods. */

/* $ Literature_References */

/*     "Numerical Recipes --- The Art of Scientific Computing" by William */
/*     Press, Brian Flannery, Saul Teukolsky, William Vetterling. */
/*     Cambridge University Press 1986. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */
/*     W.L. Taber     (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 19-OCT-2012 (NJB) */

/*        Renamed for use in TSPICE. Several misspellings in */
/*        comments were corrected. */

/* -    Beta Version 1.0.0, 24-MAR-2003 (WLT) (NJB) */

/* -& */

/*     SPICELIB Functions */


/*     Local parameters */


/*     Local Variables */


/*     Standard Error Handling. */

    /* Parameter adjustments */
    a_dim1 = *dim;
    a_dim2 = *dim;
    a_offset = a_dim1 + 1;
    x_dim1 = *dim;
    x_dim2 = *neq;
    x_offset = x_dim1 + 1;
    b_dim1 = *dim;
    b_dim2 = *neq;
    b_offset = b_dim1 + 1;

    /* Function Body */
    if (return_()) {
	return 0;
    }

/*     Nothing found yet. */

    *found = FALSE_;
    start = 1;
    while(start <= *dim) {

/*        First normalize each row of the matrix. */

	i__1 = *dim;
	for (i__ = start; i__ <= i__1; ++i__) {
	    maxmag = 0.;
	    mag = 0.;
	    i__2 = *dim;
	    for (j = start; j <= i__2; ++j) {
		if ((d__1 = a[(i__3 = i__ + j * a_dim1 - a_offset) < a_dim1 * 
			a_dim2 && 0 <= i__3 ? i__3 : s_rnge("a", i__3, "t_so"
			"lveg_2__", (ftnlen)221)], abs(d__1)) > maxmag) {
		    maxmag = (d__1 = a[(i__3 = i__ + j * a_dim1 - a_offset) < 
			    a_dim1 * a_dim2 && 0 <= i__3 ? i__3 : s_rnge(
			    "a", i__3, "t_solveg_2__", (ftnlen)222)], abs(
			    d__1));
		}
	    }
	    if (maxmag == 0.) {

/*              The found flag is already set. */

		return 0;
	    }
	    i__2 = *dim;
	    for (j = start; j <= i__2; ++j) {
		temp = a[(i__3 = i__ + j * a_dim1 - a_offset) < a_dim1 * 
			a_dim2 && 0 <= i__3 ? i__3 : s_rnge("a", i__3, "t_so"
			"lveg_2__", (ftnlen)234)] / maxmag;
		mag += temp * temp;
	    }
	    mag = maxmag * sqrt(mag);
	    i__2 = *dim;
	    for (j = start; j <= i__2; ++j) {
		a[(i__3 = i__ + j * a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 
			<= i__3 ? i__3 : s_rnge("a", i__3, "t_solveg_2__", (
			ftnlen)241)] = a[(i__4 = i__ + j * a_dim1 - a_offset) 
			< a_dim1 * a_dim2 && 0 <= i__4 ? i__4 : s_rnge("a", 
			i__4, "t_solveg_2__", (ftnlen)241)] / mag;
	    }
	    i__2 = *neq;
	    for (k = 1; k <= i__2; ++k) {
		b[(i__3 = i__ + k * b_dim1 - b_offset) < b_dim1 * b_dim2 && 0 
			<= i__3 ? i__3 : s_rnge("b", i__3, "t_solveg_2__", (
			ftnlen)245)] = b[(i__4 = i__ + k * b_dim1 - b_offset) 
			< b_dim1 * b_dim2 && 0 <= i__4 ? i__4 : s_rnge("b", 
			i__4, "t_solveg_2__", (ftnlen)245)] / mag;
	    }
	}

/*        Find the row with the maximum first entry. */

	move = start;
	maxmag = (d__1 = a[(i__1 = start + start * a_dim1 - a_offset) < 
		a_dim1 * a_dim2 && 0 <= i__1 ? i__1 : s_rnge("a", i__1, "t_s"
		"olveg_2__", (ftnlen)254)], abs(d__1));
	i__1 = *dim;
	for (i__ = start + 1; i__ <= i__1; ++i__) {
	    if ((d__1 = a[(i__2 = i__ + start * a_dim1 - a_offset) < a_dim1 * 
		    a_dim2 && 0 <= i__2 ? i__2 : s_rnge("a", i__2, "t_solveg"
		    "_2__", (ftnlen)258)], abs(d__1)) > maxmag) {
		maxmag = (d__1 = a[(i__2 = i__ + start * a_dim1 - a_offset) < 
			a_dim1 * a_dim2 && 0 <= i__2 ? i__2 : s_rnge("a", 
			i__2, "t_solveg_2__", (ftnlen)260)], abs(d__1));
		move = i__;
	    }
	}

/*        If the first row is not the one with the largest */
/*        component of interest swap with the one that does. */

	if (move != start) {
	    i__1 = *dim;
	    for (i__ = start; i__ <= i__1; ++i__) {
		swapd_(&a[(i__2 = move + i__ * a_dim1 - a_offset) < a_dim1 * 
			a_dim2 && 0 <= i__2 ? i__2 : s_rnge("a", i__2, "t_so"
			"lveg_2__", (ftnlen)273)], &a[(i__3 = start + i__ * 
			a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 <= i__3 ? 
			i__3 : s_rnge("a", i__3, "t_solveg_2__", (ftnlen)273)]
			);
	    }
	    i__1 = *neq;
	    for (k = 1; k <= i__1; ++k) {
		swapd_(&b[(i__2 = move + k * b_dim1 - b_offset) < b_dim1 * 
			b_dim2 && 0 <= i__2 ? i__2 : s_rnge("b", i__2, "t_so"
			"lveg_2__", (ftnlen)277)], &b[(i__3 = start + k * 
			b_dim1 - b_offset) < b_dim1 * b_dim2 && 0 <= i__3 ? 
			i__3 : s_rnge("b", i__3, "t_solveg_2__", (ftnlen)277)]
			);
	    }
	}

/*        Now normalize the START row so that it begins with a 1. */

	mag = a[(i__1 = start + start * a_dim1 - a_offset) < a_dim1 * a_dim2 
		&& 0 <= i__1 ? i__1 : s_rnge("a", i__1, "t_solveg_2__", (
		ftnlen)284)];
	i__1 = *neq;
	for (k = 1; k <= i__1; ++k) {
	    b[(i__2 = start + k * b_dim1 - b_offset) < b_dim1 * b_dim2 && 0 <=
		     i__2 ? i__2 : s_rnge("b", i__2, "t_solveg_2__", (ftnlen)
		    287)] = b[(i__3 = start + k * b_dim1 - b_offset) < b_dim1 
		    * b_dim2 && 0 <= i__3 ? i__3 : s_rnge("b", i__3, "t_solv"
		    "eg_2__", (ftnlen)287)] / mag;
	}
	i__1 = *dim;
	for (i__ = start + 1; i__ <= i__1; ++i__) {
	    a[(i__2 = start + i__ * a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 
		    <= i__2 ? i__2 : s_rnge("a", i__2, "t_solveg_2__", (
		    ftnlen)291)] = a[(i__3 = start + i__ * a_dim1 - a_offset) 
		    < a_dim1 * a_dim2 && 0 <= i__3 ? i__3 : s_rnge("a", i__3, 
		    "t_solveg_2__", (ftnlen)291)] / mag;
	}
	i__1 = *dim;
	for (i__ = start + 1; i__ <= i__1; ++i__) {
	    i__2 = *neq;
	    for (k = 1; k <= i__2; ++k) {
		b[(i__3 = i__ + k * b_dim1 - b_offset) < b_dim1 * b_dim2 && 0 
			<= i__3 ? i__3 : s_rnge("b", i__3, "t_solveg_2__", (
			ftnlen)297)] = b[(i__4 = i__ + k * b_dim1 - b_offset) 
			< b_dim1 * b_dim2 && 0 <= i__4 ? i__4 : s_rnge("b", 
			i__4, "t_solveg_2__", (ftnlen)297)] - a[(i__5 = i__ + 
			start * a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 <= 
			i__5 ? i__5 : s_rnge("a", i__5, "t_solveg_2__", (
			ftnlen)297)] * b[(i__6 = start + k * b_dim1 - 
			b_offset) < b_dim1 * b_dim2 && 0 <= i__6 ? i__6 : 
			s_rnge("b", i__6, "t_solveg_2__", (ftnlen)297)];
	    }
	    i__2 = *dim;
	    for (j = start + 1; j <= i__2; ++j) {
		a[(i__3 = i__ + j * a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 
			<= i__3 ? i__3 : s_rnge("a", i__3, "t_solveg_2__", (
			ftnlen)301)] = a[(i__4 = i__ + j * a_dim1 - a_offset) 
			< a_dim1 * a_dim2 && 0 <= i__4 ? i__4 : s_rnge("a", 
			i__4, "t_solveg_2__", (ftnlen)301)] - a[(i__5 = i__ + 
			start * a_dim1 - a_offset) < a_dim1 * a_dim2 && 0 <= 
			i__5 ? i__5 : s_rnge("a", i__5, "t_solveg_2__", (
			ftnlen)301)] * a[(i__6 = start + j * a_dim1 - 
			a_offset) < a_dim1 * a_dim2 && 0 <= i__6 ? i__6 : 
			s_rnge("a", i__6, "t_solveg_2__", (ftnlen)301)];
	    }
	}
	++start;
    }

/*     Back substitute to get the solution. */

    i__1 = *neq;
    for (k = 1; k <= i__1; ++k) {
	for (i__ = *dim; i__ >= 1; --i__) {
	    part = 0.;
	    i__2 = i__ + 1;
	    for (j = *dim; j >= i__2; --j) {
		part += a[(i__3 = i__ + j * a_dim1 - a_offset) < a_dim1 * 
			a_dim2 && 0 <= i__3 ? i__3 : s_rnge("a", i__3, "t_so"
			"lveg_2__", (ftnlen)321)] * x[(i__4 = j + k * x_dim1 - 
			x_offset) < x_dim1 * x_dim2 && 0 <= i__4 ? i__4 : 
			s_rnge("x", i__4, "t_solveg_2__", (ftnlen)321)];
	    }
	    x[(i__2 = i__ + k * x_dim1 - x_offset) < x_dim1 * x_dim2 && 0 <= 
		    i__2 ? i__2 : s_rnge("x", i__2, "t_solveg_2__", (ftnlen)
		    325)] = b[(i__3 = i__ + k * b_dim1 - b_offset) < b_dim1 * 
		    b_dim2 && 0 <= i__3 ? i__3 : s_rnge("b", i__3, "t_solveg"
		    "_2__", (ftnlen)325)] - part;
	}
    }

/*     We've got a solution if we made it this far. */

    *found = TRUE_;
    return 0;
} /* t_solveg_2__ */

