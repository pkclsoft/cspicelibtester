/* t_spks19.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__0 = 0;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__2 = 2;
static integer c__6 = 6;
static doublereal c_b82 = 0.;
static integer c__4 = 4;

/* $Procedure T_SPKS19 ( SPK data type 19 subset comparison support ) */
/* Subroutine */ int t_spks19__(integer *handle, doublereal *descr, char *
	segid, doublereal *begin, doublereal *end, char *subspk, integer *
	body, integer *center, char *frame, doublereal *first, doublereal *
	last, integer *nintvl, integer *npkts, integer *subtps, integer *
	degres, doublereal *packts, doublereal *epochs, doublereal *ivlbds, 
	logical *sellst, logical *delsub, logical *ok, ftnlen segid_len, 
	ftnlen subspk_len, ftnlen frame_len)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4, i__5;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static integer newh, size[2], i__;
    extern /* Subroutine */ int dafgs_(doublereal *);
    static integer nread;
    extern /* Subroutine */ int chkin_(char *, ftnlen), dafus_(doublereal *, 
	    integer *, integer *, doublereal *, integer *);
    static logical found;
    extern /* Subroutine */ int repmi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char title[80];
    extern /* Subroutine */ int spkw19_(integer *, integer *, integer *, char 
	    *, doublereal *, doublereal *, char *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    logical *, ftnlen, ftnlen), dafgda_(integer *, integer *, integer 
	    *, doublereal *);
    static doublereal dc[4]	/* was [2][2] */;
    static integer ic[12]	/* was [6][2] */;
    extern /* Subroutine */ int chckad_(char *, doublereal *, char *, 
	    doublereal *, integer *, doublereal *, logical *, ftnlen, ftnlen),
	     daffna_(logical *), chckai_(char *, integer *, char *, integer *,
	     integer *, logical *, ftnlen, ftnlen), dafbfs_(integer *), 
	    dafcls_(integer *), delfil_(char *, ftnlen);
    static integer bufbas[2];
    extern /* Subroutine */ int chckxc_(logical *, char *, logical *, ftnlen),
	     chcksi_(char *, integer *, char *, integer *, integer *, logical 
	    *, ftnlen, ftnlen), chcksl_(char *, logical *, logical *, logical 
	    *, ftnlen);
    static doublereal buffer[200]	/* was [100][2] */;
    static integer remain;
    extern /* Subroutine */ int dafopr_(char *, integer *, ftnlen);
    static doublereal subdsc[5];
    extern /* Subroutine */ int chkout_(char *, ftnlen), spkcls_(integer *), 
	    spksub_(integer *, doublereal *, char *, doublereal *, doublereal 
	    *, integer *, ftnlen), spkopn_(char *, char *, integer *, integer 
	    *, ftnlen, ftnlen);
    extern logical return_(void);

/* $ Abstract */

/*     Compare a segment produced by the SPK type 19 subsetter to one */
/*     produced by the SPK type 19 writer. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     TEST FAMILY */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 19. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */
/*     B.V. Semenov      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 11-MAY-2015 (NJB) */

/*        Updated to support subtype 2. */

/* -    SPICELIB Version 1.0.0, 07-MAR-2014 (NJB) (BVS) */

/* -& */

/*     Maximum polynomial degree supported by the current */
/*     implementation of this SPK type. */

/*     The degree is compatible with the maximum degrees */
/*     supported by types 13 and 21. */


/*     Integer code indicating `true': */


/*     Integer code indicating `false': */


/*     SPK type 19 subtype codes: */


/*     Subtype 0:  Hermite interpolation, 12-element packets. */


/*     Subtype 1:  Lagrange interpolation, 6-element packets. */


/*     Subtype 2:  Hermite interpolation, 6-element packets. */


/*     Packet sizes associated with the various subtypes: */


/*     Number of subtypes: */


/*     Maximum packet size for type 19: */


/*     Minimum packet size for type 19: */


/*     The SPKPVN record size declared in spkrec.inc must be at least as */
/*     large as the maximum possible size of an SPK type 19 record. */

/*     The largest possible SPK type 19 record has subtype 1 (note that */
/*     records of subtype 0 have half as many epochs as those of subtype */
/*     1, for a given polynomial degree). A type 1 record contains */

/*        - The subtype and packet count */
/*        - MAXDEG+1 packets of size S19PS1 */
/*        - MAXDEG+1 time tags */


/*     End of include file spk19.inc. */

/* $ Abstract */

/*     Declare SPK data record size.  This record is declared in */
/*     SPKPVN and is passed to SPK reader (SPKRxx) and evaluator */
/*     (SPKExx) routines. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     1) If new SPK types are added, it may be necessary to */
/*        increase the size of this record.  The header of SPKPVN */
/*        should be updated as well to show the record size */
/*        requirement for each data type. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 2.0.0, 05-OCT-2012 (NJB) */

/*        Updated to support increase of maximum degree to 27 for types */
/*        2, 3, 8, 9, 12, 13, 18, and 19. See SPKPVN for a list */
/*        of record size requirements as a function of data type. */

/* -    SPICELIB Version 1.0.0, 16-AUG-2002 (NJB) */

/* -& */

/*     End include file spkrec.inc */

/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     HANDLE     I   Handle of SPK to be subsetted. */
/*     DESCR      I   Descriptor of segment to be subsetted. */
/*     SEGID      I   ID of segment to be subsetted. */
/*     BEGIN      I   Begin time of subset coverage. */
/*     END        I   End time of subset coverage. */
/*     SUBSPK     I   Name of SPK containing subset. */
/*     BODY       I   Body ID associated with segment. */
/*     CENTER     I   Center ID associated with segment. */
/*     FRAME      I   Frame associated with segment. */
/*     FIRST      I   Start time passed to type 19 writer. */
/*     LAST       I   Stop time passed to type 19 writer. */
/*     NINTVL     I   Interval count passed to type 19 writer. */
/*     NPKTS      I   Packet counts passed to type 19 writer. */
/*     SUBPTS     I   Subtypes passed to type 19 writer. */
/*     DEGRES     I   Degrees passed to type 19 writer. */
/*     PACKTS     I   Packets passed to type 19 writer. */
/*     EPOCHS     I   Epochs passed to type 19 writer. */
/*     IVLBDS     I   Interval bounds passed to type 19 writer. */
/*     SELLST     I   Selection flag passed to type 19 writer. */
/*     DELSUB     I   Flag indicating whether to delete subset SPK. */
/*     OK         O   logical indicating test status. */

/* $ Detailed_Input */

/*     HANDLE         is the DAF handle of the SPK to be subsetted. The */
/*                    SPK must be open for read access. */

/*     DESCR          is the DAF segment descriptor of the segment to be */
/*                    subsetted. DESCR refers to a segment of the SPK */
/*                    file designated by HANDLE. */

/*     SEGID          is the SPK segment ID of the segment to be */
/*                    subsetted. */

/*     BEGIN          is the begin time of coverage of the subset */
/*                    segment. BEGIN contains a time value in the form */
/*                    of seconds past J2000 TDB. */

/*     END            is the begin time of coverage of the subset */
/*                    segment. END contains a time value in the form */
/*                    of seconds past J2000 TDB. */

/*     SUBSPK         is the name of an SPK file to be created by this */
/*                    routine. This SPK will contain both a subset */
/*                    segment created by SPKS19 and a comparison segment */
/*                    created from the input data by SPKW19. */

/*     BODY           is the body ID associated with the input segment */
/*                    to be subsetted. */

/*     CENTER         is the center ID associated with the input segment */
/*                    to be subsetted. */

/*     FRAME          is the frame name associated with the input */
/*                    segment to be subsetted. */

/*     FIRST          is the start time passed to the type 19 writer. */
/*                    Normally FIRST should be equal to BEGIN. */

/*     LAST           is the stop time passed to the type 19 writer. */
/*                    Normally LAST should be equal to END */

/*     NINTVL         is the type 19 interpolation interval count */
/*                    passed to the type 19 writer. NINTVL is the */
/*                    count of interpolation intervals in the */
/*                    subset segment (not in the source segment). */

/*     NPKTS          is an array of type 19 mini-segment packet */
/*                    counts passed to the type 19 writer. The Ith */
/*                    element of NPKTS is the number of data packets */
/*                    in the Ith mini-segment of the subset segment. */

/*     SUBPTS         is an array of mini-segment type 19 subtypes */
/*                    passed to the type 19 writer. The Ith element of */
/*                    SUBTPS is the subtype of the Ith mini-segment of */
/*                    the subset segment. */

/*     DEGRES         is an array of mini-segment polynomial degrees */
/*                    passed to the type 19 writer. The Ith element of */
/*                    DEGRES is the interpolating polynomial degree */
/*                    of the Ith mini-segment of the subset segment. */

/*     PACKTS         is an array of data packets passed to the type 19 */
/*                    writer. The set of data packets associated with a */
/*                    given mini-segment is arranged in increasing time */
/*                    order, and the sets of data packets are ordered in */
/*                    parallel with their associated mini-segments. The */
/*                    number of packets associated with the Ith */
/*                    mini-segment is given by NPKTS(I). */

/*     EPOCHS         is an array of epochs passed to the type 19 writer. */
/*                    The Ith epoch is associated with the Ith data */
/*                    packet. Note that because mini-segments can employ */
/*                    padding, the contents of the epochs array are not */
/*                    necessarily strictly increasing. However the */
/*                    subset of epochs associated with a given */
/*                    mini-segment is always strictly increasing. */

/*     IVLBDS         is an array of interval bounds passed to the type */
/*                    19 writer. The Ith element of IVLBDS is the start */
/*                    time of the Ith interpolation interval of the */
/*                    subset segment. */

/*     SELLST         is the an interval selection flag passed to the */
/*                    type 19 writer. Normally this flag should be */
/*                    copied from the input segment. */

/*     DELSUB         is a flag indicating whether to delete the */
/*                    subset SPK created by this routine. Normally */
/*                    DELSUB should be set to .TRUE. */

/* $ Detailed_Output */

/*     OK         is a logical that indicates the test status to the */
/*                caller. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     This routine does not generate any errors. Routines in its */
/*     call tree may generate errors that are either intentional and */
/*     trapped or unintentional and need reporting.  The test family */
/*     utilities manage this. */

/* $ Files */

/*     The input SPK file from which a subset segment is produced */
/*     is identified by the input argument HANDLE. */

/*     This routine produces an SPK file used for segment */
/*     comparisons. See Particulars. */

/* $ Particulars */

/*     This routine tests routines that write and subset type 19 SPK */
/*     data. */

/*     This routine produces an SPK file containing a segment */
/*     produced by SPKS19 and one produced by SPKW19. The two */
/*     segments will have identical contents if both of these */
/*     routines are working properly. */

/* $ Examples */

/*     See use in F_SPKS19. */

/* $ Restrictions */

/*     None. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 08-FEB-2012 (NJB) */


/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */


/*     Saved variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_SPKS19", (ftnlen)8);

/*     Step 1: create an SPK file containing the specified */
/*     subset segment. */


/*     Open a new SPK file having the name supplied by the caller. */
/*     Write the specified segment subset to the new SPK file. */
/*     Leave the file open, since we'll write a second segment */
/*     to it. */

    spkopn_(subspk, subspk, &c__0, &newh, subspk_len, subspk_len);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
    spksub_(handle, descr, segid, begin, end, &newh, segid_len);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
/*      WRITE (*,*) 'Wrote subset segment' */

/*     Step 2: Write a second segment to the subset file, this time */
/*     creating the segment directly from the input data passed to */
/*     this routine. Use the SPK type 19 writer to create the segment. */

    spkw19_(&newh, body, center, frame, first, last, segid, nintvl, npkts, 
	    subtps, degres, packts, epochs, ivlbds, sellst, frame_len, 
	    segid_len);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
/*      WRITE (*,*) 'Wrote segment using writer' */

/*     Close the subset file. */

    spkcls_(&newh);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }

/*     Search the file we just created to obtain segment */
/*     descriptors for both segments. Fetch the d.p. and */
/*     integer components of the respective descriptors. */

    dafopr_(subspk, &newh, subspk_len);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
    dafbfs_(&newh);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
    for (i__ = 1; i__ <= 2; ++i__) {
	daffna_(&found);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (! (*ok)) {
	    dafcls_(&newh);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delfil_(subspk, subspk_len);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chkout_("T_SPKS19", (ftnlen)8);
	    return 0;
	}
	s_copy(title, "FOUND flag for segment #.", (ftnlen)80, (ftnlen)25);
	repmi_(title, "#", &i__, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chcksl_(title, &found, &c_true, ok, (ftnlen)80);
	if (! (*ok)) {
	    dafcls_(&newh);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delfil_(subspk, subspk_len);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chkout_("T_SPKS19", (ftnlen)8);
	    return 0;
	}
	dafgs_(subdsc);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (! (*ok)) {
	    dafcls_(&newh);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delfil_(subspk, subspk_len);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chkout_("T_SPKS19", (ftnlen)8);
	    return 0;
	}

/*        Unpack the descriptor of the current segment. */

	dafus_(subdsc, &c__2, &c__6, &dc[(i__1 = (i__ << 1) - 2) < 4 && 0 <= 
		i__1 ? i__1 : s_rnge("dc", i__1, "t_spks19__", (ftnlen)480)], 
		&ic[(i__2 = i__ * 6 - 6) < 12 && 0 <= i__2 ? i__2 : s_rnge(
		"ic", i__2, "t_spks19__", (ftnlen)480)]);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (! (*ok)) {
	    dafcls_(&newh);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    delfil_(subspk, subspk_len);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    chkout_("T_SPKS19", (ftnlen)8);
	    return 0;
	}
    }

/*     We're ready to start comparing segments. Check the */
/*     descriptors first. */

    chckad_("DC", dc, "=", &dc[2], &c__2, &c_b82, ok, (ftnlen)2, (ftnlen)1);

/*     The first four elements of each integer component should */
/*     agree. */

    chckai_("IC", ic, "=", &ic[6], &c__4, ok, (ftnlen)2, (ftnlen)1);

/*     The sizes of the address ranges occupied by each segment */
/*     should agree. */

    for (i__ = 1; i__ <= 2; ++i__) {
	size[(i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge("size", i__1, 
		"t_spks19__", (ftnlen)517)] = ic[(i__2 = i__ * 6 - 1) < 12 && 
		0 <= i__2 ? i__2 : s_rnge("ic", i__2, "t_spks19__", (ftnlen)
		517)] - ic[(i__3 = i__ * 6 - 2) < 12 && 0 <= i__3 ? i__3 : 
		s_rnge("ic", i__3, "t_spks19__", (ftnlen)517)] + 1;
    }
    chcksi_("Segment size", size, "=", &size[1], &c__0, ok, (ftnlen)12, (
	    ftnlen)1);

/*     There's no point comparing segment data if we don't have */
/*     descriptor agreement. */

    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }

/*     Compare data from both segments. */

    remain = size[0];

/*     Initialize the base addresses of the ranges */
/*     we'll read from. */

    bufbas[0] = ic[4] - 1;
    bufbas[1] = ic[10] - 1;
    while(remain > 0) {

/*        Read data from both files. */

	nread = min(remain,100);
	for (i__ = 1; i__ <= 2; ++i__) {
	    i__4 = bufbas[(i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge(
		    "bufbas", i__1, "t_spks19__", (ftnlen)561)] + 1;
	    i__5 = bufbas[(i__2 = i__ - 1) < 2 && 0 <= i__2 ? i__2 : s_rnge(
		    "bufbas", i__2, "t_spks19__", (ftnlen)561)] + nread;
	    dafgda_(&newh, &i__4, &i__5, &buffer[(i__3 = i__ * 100 - 100) < 
		    200 && 0 <= i__3 ? i__3 : s_rnge("buffer", i__3, "t_spks"
		    "19__", (ftnlen)561)]);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	    if (! (*ok)) {
		dafcls_(&newh);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		delfil_(subspk, subspk_len);
		chckxc_(&c_false, " ", ok, (ftnlen)1);
		chkout_("T_SPKS19", (ftnlen)8);
		return 0;
	    }
	}
/*         WRITE (*,*) 'Fetch loop end' */

/*        Compare the data we just read. */

/*        First create a title for the data chunk to compare. */
/*        Use 1-based indices relative to the segment start */
/*        addresses to identify the address ranges. */

	s_copy(title, "Relative range #:#", (ftnlen)80, (ftnlen)18);
	i__1 = bufbas[0] - ic[4] + 2;
	repmi_(title, "#", &i__1, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	i__1 = bufbas[0] - ic[4] + 1 + nread;
	repmi_(title, "#", &i__1, title, (ftnlen)80, (ftnlen)1, (ftnlen)80);
	chckad_(title, buffer, "=", &buffer[100], &nread, &c_b82, ok, (ftnlen)
		80, (ftnlen)1);
	if (! (*ok)) {
	    chkout_("T_SPKS19", (ftnlen)8);
	    return 0;
	}

/*        Update the count of remaining items and the base addresses. */

	remain -= nread;
	for (i__ = 1; i__ <= 2; ++i__) {
	    bufbas[(i__1 = i__ - 1) < 2 && 0 <= i__1 ? i__1 : s_rnge("bufbas",
		     i__1, "t_spks19__", (ftnlen)611)] = bufbas[(i__2 = i__ - 
		    1) < 2 && 0 <= i__2 ? i__2 : s_rnge("bufbas", i__2, "t_s"
		    "pks19__", (ftnlen)611)] + nread;
	}
    }

/*     We're done with the comparison. Close the subset file. Delete the */
/*     subset file if commanded to do so. */

    spkcls_(&newh);
    chckxc_(&c_false, " ", ok, (ftnlen)1);
    if (! (*ok)) {
	dafcls_(&newh);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
	if (*delsub) {
	    delfil_(subspk, subspk_len);
	    chckxc_(&c_false, " ", ok, (ftnlen)1);
	}
	chkout_("T_SPKS19", (ftnlen)8);
	return 0;
    }
    if (*delsub) {
	delfil_(subspk, subspk_len);
	chckxc_(&c_false, " ", ok, (ftnlen)1);
    }
    chkout_("T_SPKS19", (ftnlen)8);
    return 0;
} /* t_spks19__ */

