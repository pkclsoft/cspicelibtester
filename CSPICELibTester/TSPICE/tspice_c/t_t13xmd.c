/* t_t13xmd.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__23 = 23;
static integer c__3 = 3;
static integer c__25 = 25;
static doublereal c_b46 = 25.;

/* $Procedure      T_T13XMD ( Type 13 SPK record to extended MDA ) */
/* Subroutine */ int t_t13xmd__(doublereal *t13rec, doublereal *mdabeg, 
	doublereal *mdaend, doublereal *t21rec, logical *found)
{
    /* System generated locals */
    integer i__1, i__2, i__3, i__4;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer i_dnnt(doublereal *), s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer degp, lidx;
    doublereal span;
    integer from;
    doublereal work[90000]	/* was [300][300] */, rsys[72];
    extern /* Subroutine */ int t_taymat__(integer *, doublereal *, 
	    doublereal *, doublereal *), t_tayhrm__(integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, logical *);
    doublereal d__;
    integer i__, j, k, n;
    doublereal s, dbuff[72], x;
    extern /* Subroutine */ int filld_(doublereal *, integer *, doublereal *),
	     chkin_(char *, ftnlen);
    doublereal mbuff[576];
    extern /* Subroutine */ int moved_(doublereal *, integer *, doublereal *),
	     errdp_(char *, doublereal *, ftnlen);
    doublereal tvals[24], accder[25], dt[25];
    extern /* Subroutine */ int cleard_(integer *, doublereal *);
    integer mdaloc, to, dlsize;
    extern /* Subroutine */ int sigerr_(char *, ftnlen);
    doublereal derivs[24];
    extern /* Subroutine */ int chkout_(char *, ftnlen);
    doublereal shrink;
    extern /* Subroutine */ int setmsg_(char *, ftnlen), errint_(char *, 
	    integer *, ftnlen);
    extern integer lstltd_(doublereal *, integer *, doublereal *);
    extern logical return_(void);
    integer tstart;
    extern /* Subroutine */ int t_pd2dt__(integer *, doublereal *, doublereal 
	    *, doublereal *);
    extern logical odd_(integer *);

/* $ Abstract */

/*     Convert a type 13 SPK record to a type 21 (extended MDA) SPK */
/*     record. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     SPK */

/* $ Declarations */
/* $ Abstract */

/*     Declare parameters specific to SPK type 21. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     SPK */

/* $ Keywords */

/*     SPK */

/* $ Restrictions */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman      (JPL) */

/* $ Literature_References */

/*     None. */

/* $ Version */

/* -    SPICELIB Version 1.0.0, 25-DEC-2013 (NJB) */

/* -& */

/*     MAXTRM      is the maximum number of terms allowed in each */
/*                 component of the difference table contained in a type */
/*                 21 SPK difference line. MAXTRM replaces the fixed */
/*                 table parameter value of 15 used in SPK type 1 */
/*                 segments. */

/*                 Type 21 segments have variable size. Let MAXDIM be */
/*                 the dimension of each component of the difference */
/*                 table within each difference line. Then the size */
/*                 DLSIZE of the difference line is */

/*                    ( 4 * MAXDIM ) + 11 */

/*                 MAXTRM is the largest allowed value of MAXDIM. */



/*     End of include file spk21.inc. */

/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     T13REC     I   Type 13 SPK record. */
/*     MDABEG     I   Start time of expansion coverage interval. */
/*     MDAEND     I   End time of expansion coverage interval. */
/*     T21REC     O   Type 21 SPK record. */
/*     FOUND      O   Flag indicating whether solution was found. */
/*     MAXDEG     P   Maximum type 13 polynomial degree. */
/*     MAXDIM     P   Maximum difference line table size. */

/* $ Detailed_Input */

/*     T13REC         is a type 13 SPK record.  Each such record is */
/*                    structured as follows: */

/*                       +----------------------+ */
/*                       | number of states (n) | */
/*                       +----------------------+ */
/*                       | state 1 (6 elts.)    | */
/*                       +----------------------+ */
/*                       | state 2 (6 elts.)    | */
/*                       +----------------------+ */
/*                                   . */
/*                                   . */
/*                                   . */
/*                       +----------------------+ */
/*                       | state n (6 elts.)    | */
/*                       +----------------------+ */
/*                       | epochs 1--n          | */
/*                       +----------------------+ */


/*     MDABEG         is the start time of the coverage interval of */
/*                    the MDA created by this routine. */

/*     MDAEND         is the end time of the coverage interval of the */
/*                    MDA created by this routine. This is also the time */
/*                    value about which the Taylor expansions of each */
/*                    position component are centered. */


/* $ Detailed_Output */

/*     T21REC         is a type 21 SPK record. Each such record contains */
/*                    DLSIZE+1 double precision numbers, where DLSIZE is */

/*                       ( 4 * MAXDIM ) + 11 */

/*                    and MAXDIM is in the range 15:MAXTRM. */

/*                    The layout of data in the record is as follows: */

/*                        T21REC( 1 ):             MAXDIM */
/*                        T21REC( 2 : DLSIZE ):    an extended difference */
/*                                                 line. */

/*                    The contents of an extended difference line are as */
/*                    shown below: */

/*                       Dimension  Description */
/*                       ---------  ---------------------------------- */
/*                       1          Reference epoch of difference line */
/*                       MAXDIM     Stepsize function vector */
/*                       1          Reference position vector,  x */
/*                       1          Reference velocity vector,  x */
/*                       1          Reference position vector,  y */
/*                       1          Reference velocity vector,  y */
/*                       1          Reference position vector,  z */
/*                       1          Reference velocity vector,  z */
/*                       MAXDIM,3   Modified divided difference */
/*                                  arrays (MDAs) */
/*                       1          Maximum integration order plus 1 */
/*                       3          Integration order array */

/*                    The reference position and velocity are those of */
/*                    BODY relative to CENTER at the reference epoch. */
/*                    (A difference line is essentially a polynomial */
/*                    expansion of acceleration about the reference */
/*                    epoch.) */

/*                    See the TSPICE routine T_PD2DT for details on how */
/*                    the MDAs are derived. */


/*     FOUND          is a logical flag indicating whether the desired */
/*                    output record was computable.  If the solution */
/*                    process gives rise to a singular coefficient */
/*                    matrix, no solution is found. */

/* $ Parameters */

/*     MAXDEG        is the maximum allowed degree of the polynomials */
/*                   defined by the type 13 input record. */

/*     MAXDIM        is the maximum extended difference line table size */
/*                   per component. */

/* $ Exceptions */

/*     1) If the linear algebraic system defining the Taylor */
/*        coefficients of the trajectory polynomials is singular, */
/*        FOUND will be returned .FALSE. */

/*     2) If the input MDAEND is not within the closed interval */
/*        defined by the time tags in the input record, the */
/*        error SPICE(TIMEOUTOFRANGE) is signaled. */

/*     3) If the polynomial degree associated with the input record */
/*        is outside of the range [1:MAXDEG], the error */
/*        SPICE(VALUEOUTOFRANGE) is signaled. */

/*     4) If the interpolation window size of the input type 13 */
/*        record is odd, the error SPICE(INVALIDSIZE) is signaled. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     None. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     1) This routine is intended for use only within the TSPICE */
/*        program. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 20-OCT-2012 (NJB) */

/*        This routine was renamed for use in TSPICE. Calls to SPKNIO */
/*        utilities TAYMAT, TAYHRM, PD2DT were replaced by calls to */
/*        routines T_TAYMAT, T_TAYHRM, and T_PD2DT. Unused variable */
/*        RIDX was removed. Several typos in comments were corrected. */

/* -    SPKNIO Version 1.0.0, 24-MAR-2003 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */


/*     Local variables */

    if (return_()) {
	return 0;
    }
    chkin_("T_T13XMD", (ftnlen)8);

/*     Nothing found yet. */

    *found = FALSE_;

/*     For each position component, find the Taylor expansion of its */
/*     polynomial representation at X. Convert the derivative set to a */
/*     difference line. Store the difference line in the type 21 record. */

/*     Get the type 13 polynomial degree from the state count. */

    n = i_dnnt(t13rec);
    if (odd_(&n)) {
	setmsg_("Window size is #; must be even", (ftnlen)30);
	errint_("#", &n, (ftnlen)1);
	sigerr_("SPICE(INVALIDSIZE)", (ftnlen)18);
	chkout_("T_T13XMD", (ftnlen)8);
	return 0;
    }
    degp = (n << 1) - 1;
    if (degp < 1 || degp > 23) {
	setmsg_("Polynomial degree is #; must be in range 1:#.", (ftnlen)45);
	errint_("#", &degp, (ftnlen)1);
	errint_("#", &c__23, (ftnlen)1);
	sigerr_("SPICE(VALUEOUTOFRANGE)", (ftnlen)22);
	chkout_("T_T13XMD", (ftnlen)8);
	return 0;
    }

/*     Let TSTART be the index in T13REC where the time values start. */

    tstart = n * 6 + 2;

/*     Find the span of the time values in the record. */

    span = t13rec[tstart + n - 2] - t13rec[tstart - 1];

/*     Ensure that at least one time tag in the input record */
/*     strictly precedes MDAEND. */

    lidx = lstltd_(mdaend, &n, &t13rec[tstart - 1]);
    if (lidx == 0) {
	setmsg_("Input time MDAEND = #; is not greater than first epoch in t"
		"ype 13 record #.", (ftnlen)75);
	errdp_("#", mdaend, (ftnlen)1);
	errdp_("#", &t13rec[tstart - 1], (ftnlen)1);
	sigerr_("SPICE(TIMEOUTOFRANGE)", (ftnlen)21);
	chkout_("T_T13XMD", (ftnlen)8);
	return 0;
    }
    if (*mdaend > t13rec[tstart + n - 2]) {
	setmsg_("Input time MDAEND = #; exceeds last epoch in type 13 record"
		" #.", (ftnlen)62);
	errdp_("#", mdaend, (ftnlen)1);
	errdp_("#", &t13rec[tstart + n - 2], (ftnlen)1);
	sigerr_("SPICE(TIMEOUTOFRANGE)", (ftnlen)21);
	chkout_("T_T13XMD", (ftnlen)8);
	return 0;
    }

/*     Transform the time tags by scaling their distance from */
/*     the central point of the expansion.  We pick the scale to */
/*     make the width of the interval from first to last */
/*     transformed tag equal to 1. */

    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	tvals[(i__2 = i__ - 1) < 24 && 0 <= i__2 ? i__2 : s_rnge("tvals", 
		i__2, "t_t13xmd__", (ftnlen)363)] = (t13rec[tstart - 1 + i__ 
		- 1] - *mdaend) / span;
    }

/*     Let X be the transformed center of the expansion. */

    x = 0.;

/*     Let D be the step size for the difference line. */
/*     D will be equal to the length of the interval */
/*     over which the polynomial expansion is applicable. */

    d__ = *mdaend - *mdabeg;

/*     Rather than evaluating the derivatives of our expansion, we'll */
/*     evaluate the derivatives of a related expansion that has support */
/*     on a shorter interval. */

/*        This shrinking operation was originally done to work around a */
/*        numeric overflow problem in separate software run on a VAX and */
/*        subject to configuration control restrictions that prevented */
/*        correction of the problem. */

/*        Since the algorithm works and since there is no strong */
/*        reason to change it, it is left in its original form. */


/* Computing MIN */
    d__1 = 1., d__2 = 1. / span;
    shrink = min(d__1,d__2);

/*     Pack the function and derivative values. */

    for (i__ = 1; i__ <= 3; ++i__) {
	to = (n << 1) * (i__ - 1) + 1;
	i__1 = n;
	for (j = 1; j <= i__1; ++j) {
	    from = (j - 1) * 6 + 1 + i__;
	    rsys[(i__2 = to - 1) < 72 && 0 <= i__2 ? i__2 : s_rnge("rsys", 
		    i__2, "t_t13xmd__", (ftnlen)405)] = t13rec[from - 1];
	    from = (j - 1) * 6 + 1 + i__ + 3;
	    rsys[(i__2 = to + n - 1) < 72 && 0 <= i__2 ? i__2 : s_rnge("rsys",
		     i__2, "t_t13xmd__", (ftnlen)408)] = t13rec[from - 1] * 
		    span;
	    ++to;
	}
    }


/*     Construct a coefficient matrix. */

    t_taymat__(&n, tvals, &x, mbuff);

/*     Solve for the Taylor coefficients at X, and compute derivatives */
/*     for all three vector components. */

    t_tayhrm__(&n, &c__3, mbuff, rsys, dbuff, found);
    if (! (*found)) {
	chkout_("T_T13XMD", (ftnlen)8);
	return 0;
    }
    for (i__ = 1; i__ <= 3; ++i__) {

/*        Unpack the derivatives for the Ith component. */

	from = (n << 1) * (i__ - 1) + 1;
	i__2 = n << 1;
	moved_(&dbuff[(i__1 = from - 1) < 72 && 0 <= i__1 ? i__1 : s_rnge(
		"dbuff", i__1, "t_t13xmd__", (ftnlen)439)], &i__2, derivs);

/*        Now that we have the derivatives, obtain the corresponding */
/*        difference line and write it into the type 21 output record. */
/*        The record description from SPKE21 is copied below. */

/*        Name     Dimension  Description */
/*        ------   ---------  ------------------------------- */
/*        TL               1  Final epoch of record */
/*        G           MAXDIM  Stepsize function vector */
/*        REFPOS           3  Reference position vector */
/*        REFVEL           3  Reference velocity vector */
/*        DT      MAXDIM,NTE  Modified divided difference arrays */
/*        KQMAX1           1  Maximum integration order plus 1 */
/*        KQ             NTE  Integration order array */


/*        Scale velocity and acceleration to compensate for the */
/*        shrinkage factor. The higher derivatives are scaled */
/*        separately. */

	derivs[1] *= shrink;
/* Computing 2nd power */
	d__1 = shrink;
	derivs[2] *= d__1 * d__1;

/*        Compute the scaled step size S. */

	s = d__ * shrink;

/*        We're going to scale the Jth element of the acceleration */
/*        portion (starting with the derivative of acceleration) of our */
/*        derivatives array by SHRINK**2 * S**J.  Since S can be large, */
/*        we must do this carefully.  We arrange our multiplications so */
/*        that we never compute a large power of S directly. */

	cleard_(&c__25, accder);
	i__1 = degp - 2;
	moved_(&derivs[3], &i__1, accder);
	i__1 = degp - 2;
	for (j = 1; j <= i__1; ++j) {
/* Computing 2nd power */
	    d__1 = shrink;
	    accder[(i__2 = j - 1) < 25 && 0 <= i__2 ? i__2 : s_rnge("accder", 
		    i__2, "t_t13xmd__", (ftnlen)482)] = accder[(i__3 = j - 1) 
		    < 25 && 0 <= i__3 ? i__3 : s_rnge("accder", i__3, "t_t13"
		    "xmd__", (ftnlen)482)] * s * (d__1 * d__1);
	    i__2 = j - 1;
	    for (k = 1; k <= i__2; ++k) {
		accder[(i__3 = j - 1) < 25 && 0 <= i__3 ? i__3 : s_rnge("acc"
			"der", i__3, "t_t13xmd__", (ftnlen)485)] = accder[(
			i__4 = j - 1) < 25 && 0 <= i__4 ? i__4 : s_rnge("acc"
			"der", i__4, "t_t13xmd__", (ftnlen)485)] * s;
	    }
	}
	i__1 = degp - 2;
	moved_(accder, &i__1, &derivs[3]);
	cleard_(&c__25, dt);

/*        Compute the difference table for the current polynomial: */

	i__1 = degp - 2;
	t_pd2dt__(&i__1, &derivs[2], work, dt);
	mdaloc = (i__ - 1) * 25 + 33;
	moved_(dt, &c__25, &t21rec[mdaloc - 1]);

/*        Put into the output array the reference position and velocity */
/*        values for the current state component. */

	mdaloc = (i__ - 1 << 1) + 27;
	t21rec[mdaloc - 1] = derivs[0];
	++mdaloc;
	t21rec[mdaloc - 1] = derivs[1];
    }

/*     Fill in the rest of the type 21 record. */

/*     Reference epoch.  This is also the final epoch covered by */
/*     the record: */

    t21rec[0] = *mdaend;

/*     Stepsize values: */

    filld_(&d__, &c__25, &t21rec[1]);

/*     Maximum integration order plus 1: */

    mdaloc = 108;
    t21rec[mdaloc - 1] = 26.;

/*     Integration orders for each component: */

    ++mdaloc;
    filld_(&c_b46, &c__3, &t21rec[mdaloc - 1]);

/*     Shift the record to make room for the initial size value. */

    dlsize = 111;
    for (i__ = dlsize; i__ >= 1; --i__) {
	t21rec[i__] = t21rec[i__ - 1];
    }
    t21rec[0] = 25.;

/*     FOUND is set; it was set by TAYHRM. */

    chkout_("T_T13XMD", (ftnlen)8);
    return 0;
} /* t_t13xmd__ */

