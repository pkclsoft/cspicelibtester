/* t_tayhrm.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* $Procedure      T_TAYHRM ( Taylor expansions of Hermite polynomials ) */
/* Subroutine */ int t_tayhrm__(integer *n, integer *neq, doublereal *mbuff, 
	doublereal *rsys, doublereal *derivs, logical *found)
{
    /* System generated locals */
    integer rsys_dim1, rsys_offset, derivs_dim1, derivs_dim2, derivs_offset, 
	    i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    doublereal fact;
    integer i__, j;
    extern /* Subroutine */ int chkin_(char *, ftnlen), t_solveg_2__(
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    logical *), chkout_(char *, ftnlen);
    extern logical return_(void);

/* $ Abstract */

/*     Find the Taylor expansions about a specified point of a set of */
/*     polynomials defined by Hermite-style constraints.  Return the */
/*     derivatives of the expansions. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     SPKNIO */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     N          I   Number of abscissa points. */
/*     NEQ        I   Number of equations. */
/*     MBUFF      I   Buffer containing matrix of Hermite constraints. */
/*     RSYS       I   Array of RHS vectors of equations. */
/*     DERIVS     O   Array of derivatives of Taylor expansions. */
/*     FOUND      O   Flag indicating whether solution was found. */

/* $ Detailed_Input */

/*     N              is the size of a grid of abscissa points on */
/*                    which a set of polynomials is defined. */

/*     NEQ            is the number of matrix equations defining the set */
/*                    of polynomials.  There is one matrix equation */
/*                    for each polynomial. */

/*     MBUFF          is a buffer into which are packed the columns of a */
/*                    constraint matrix M. Let XVALs be a set of */
/*                    abscissa points, and let X be the central point of */
/*                    a Taylor expansion. Then the matrix M has the form */

/*  +-                                                                 -+ */
/*  | 1  [XVALS(1)-X]   [XVALS(1)-X]**2 ...       [XVALS(1)-X]**(2*N-1) | */
/*  | 1  [XVALS(2)-X]   [XVALS(2)-X]**2 ...       [XVALS(2)-X]**(2*N-1) | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  | 1  [XVALS(N)-X]   [XVALS(N)-X]**2 ...       [XVALS(N)-X]**(2*N-1) | */
/*  | 0      1        2*[XVALS(1)-X]**1 ... (N-1)*[XVALS(1)-X]**(2*N-2) | */
/*  | 0      1        2*[XVALS(2)-X]**1 ... (N-1)*[XVALS(2)-X]**(2*N-2) | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  | 0      1        2*[XVALS(N)-X]**1 ... (N-1)*[XVALS(N)-X]**(2*N-2) | */
/*  +-                                                                 -+ */

/*                    The matrix M is packed into MBUFF in column-major */
/*                    order: the first 2*N elements of MBUFF contain the */
/*                    first column of the matrix, the next 2*N elements */
/*                    contain the second column of the matrix, and so */
/*                    on. */

/*     RSYS           is an array of right-hand-sides of the NEQ matrix */
/*                    equations that this routine is meant to solve. Let */
/*                    M be the coefficient matrix described above and */
/*                    let C(*,i) be the ith array of Taylor coefficients */
/*                    which constitutes the solution of the ith */
/*                    equation. The ith column of RSYS is the RHS of the */
/*                    ith equation: */

/*                       M C(*,i) = RSYS(*,i),  i = 1, ..., NEQ */

/* $ Detailed_Output */

/*     DERIVS         is an array containing the derivatives of the */
/*                    polynomial expansions defined by MBUFF and RSYS, */
/*                    all evaluated at X.  DERIVS(I,J) contains the */
/*                    (I-1)st derivative of the Jth expansion. */

/*     FOUND          is a logical flag which has the value .TRUE. if */
/*                    DERIVS was computed and is .FALSE. otherwise. */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     1) If a solution cannot be found due to singularity of */
/*        the input matrix, the FOUND flag is set to .FALSE. */
/*        This case is not considered an error. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     See the routine TAYMAT for a description of the type of */
/*     matrix equation this routine solves.  Since this routine is */
/*     typically used to find derivatives of a position vector, there */
/*     is normally one equation for each position component. */

/* $ Examples */

/*     See the SPKNIO routine T13MDA. */

/* $ Restrictions */

/*     1) This routine is for use only within the program TSPICE. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 19-OCT-2012 (NJB) */

/*        Renamed for use in TSPICE. Inputs XVALS and X, which were */
/*        included only for consistency with the routine TAYMAT, were */
/*        deleted. Unused local variable DEG was deleted. */

/* -    SPKNIO Version 1.0.0, 24-MAR-2003 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local variables */

    /* Parameter adjustments */
    derivs_dim1 = *n << 1;
    derivs_dim2 = *neq;
    derivs_offset = derivs_dim1 + 1;
    rsys_dim1 = *n << 1;
    rsys_offset = rsys_dim1 + 1;

    /* Function Body */
    if (return_()) {
	return 0;
    }
    chkin_("T_TAYHRM", (ftnlen)8);

/*     Solve the systems of equations */


/*        M * COEFFS = RSYS(1,*) */

/*     where M is the coefficient matrix packed into MBUFF. */

    i__1 = *n << 1;
    t_solveg_2__(rsys, &i__1, neq, mbuff, derivs, found);
    if (! (*found)) {
	chkout_("T_TAYHRM", (ftnlen)8);
	return 0;
    }

/*     Compute the derivatives of the Taylor expansion at X. */

    fact = 1.;
    i__1 = *n << 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (i__ > 2) {
	    fact *= i__ - 1;
	}
	i__2 = *neq;
	for (j = 1; j <= i__2; ++j) {
	    derivs[(i__3 = i__ + j * derivs_dim1 - derivs_offset) < 
		    derivs_dim1 * derivs_dim2 && 0 <= i__3 ? i__3 : s_rnge(
		    "derivs", i__3, "t_tayhrm__", (ftnlen)219)] = derivs[(
		    i__4 = i__ + j * derivs_dim1 - derivs_offset) < 
		    derivs_dim1 * derivs_dim2 && 0 <= i__4 ? i__4 : s_rnge(
		    "derivs", i__4, "t_tayhrm__", (ftnlen)219)] * fact;
	}
    }
    chkout_("T_TAYHRM", (ftnlen)8);
    return 0;
} /* t_tayhrm__ */

