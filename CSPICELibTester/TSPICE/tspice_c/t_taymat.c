/* t_taymat.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__25 = 25;

/* $Procedure T_TAYMAT ( Taylor expansion coefficient matrix ) */
/* Subroutine */ int t_taymat__(integer *n, doublereal *xvals, doublereal *x, 
	doublereal *mbuff)
{
    /* System generated locals */
    integer xvals_dim1, mbuff_dim1, i__1, i__2, i__3, i__4, i__5;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer i__, j;
    doublereal m[2500]	/* was [50][50] */;
    extern /* Subroutine */ int chkin_(char *, ftnlen), moved_(doublereal *, 
	    integer *, doublereal *), sigerr_(char *, ftnlen), chkout_(char *,
	     ftnlen), setmsg_(char *, ftnlen), errint_(char *, integer *, 
	    ftnlen);
    extern logical return_(void);
    integer deg;

/* $ Abstract */

/*     Find the coefficent matrix required to find the Taylor expansion */
/*     about a specified point of a polynomial defined by Hermite-style */
/*     constraints. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     SPKNIO */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     N          I   Number of abscissa points. */
/*     XVALS      I   Array of abscissa points. */
/*     X          I   Central point of the expansion. */
/*     MBUFF      O   Buffer containing packed coefficient matrix. */
/*     MAXN       P   Parameter giving maximum grid size. */

/* $ Detailed_Input */

/*     N              is the size of a grid of abscissa points on */
/*                    which a set of polynomials is defined. */

/*     XVALS          is the set of abscissa points. */

/*     X              is the central point of a Taylor expansion. */

/* $ Detailed_Output */

/*     MBUFF          is a buffer into which is packed a coefficient */
/*                    matrix M defined by the input grid.  The */
/*                    coefficient matrix M is */
/*  +-                                                                 -+ */
/*  | 1  [XVALS(1)-X]   [XVALS(1)-X]**2 ...       [XVALS(1)-X]**(2*N-1) | */
/*  | 1  [XVALS(2)-X]   [XVALS(2)-X]**2 ...       [XVALS(2)-X]**(2*N-1) | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  | 1  [XVALS(N)-X]   [XVALS(N)-X]**2 ...       [XVALS(N)-X]**(2*N-1) | */
/*  | 0      1        2*[XVALS(1)-X]**1 ... (N-1)*[XVALS(1)-X]**(2*N-2) | */
/*  | 0      1        2*[XVALS(2)-X]**1 ... (N-1)*[XVALS(2)-X]**(2*N-2) | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  |                          .                                        | */
/*  | 0      1        2*[XVALS(N)-X]**1 ... (N-1)*[XVALS(N)-X]**(2*N-2) | */
/*  +-                                                                 -+ */

/*                    M is packed into MBUFF in column-major order: */
/*                    the first 2*N elements of MBUFF contain the first */
/*                    column of M, the next 2*N elements contain the */
/*                    second column of M, and so on. */


/* $ Parameters */

/*     MAXN           is the maximum size of the input grid. */

/* $ Exceptions */

/*     1)  If the input grid size N exceeds MAXN/2, the error */
/*         SPICE(OVERFLOW) is signaled. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     If a Hermite polynomial H is defined by the contraints */

/*        H (x(i))  =   a(i) */
/*        H'(x(i))  =   b(i) */

/*     for i = 1, ..., N, and if T(y-x) is the Taylor expansion */
/*     of H about x, evaluated at y, then T must satisfy the */
/*     2*N constraints */


/*                      2*N-1 */
/*                       ___ */
/*                       \               K */
/*        T(x(i)-x) =    /     C (x(i)-x)      =   a(i), i = 1,...,N */
/*                       ---    K */
/*                      K = 0 */


/*                      2*N-1 */
/*                       ___ */
/*                       \                  K-1 */
/*        T'(x(i)-x) =   /    K * C (x(i)-x)    =   b(i), i = 1,...,N */
/*                       ---       K */
/*                      K = 1 */


/*     where the coefficients */

/*        C ,  K = 0, ..., 2*N-1 */
/*         K */

/*     are the Taylor coefficients comprising T.  The set of equations */
/*     above can be written as a matrix equation */


/*        M C = B */

/*     where M is the coefficient matrix described in the Detailed_Output */
/*     section above, C is the vector of Taylor coefficents, and B is */
/*     the contraint vector consisting of the values */

/*        a , ..., a , b , ..., b */
/*         1        n   1        b */

/*     The output buffer MBUFF contains the matrix M in a convenient */
/*     form for solving the matrix equation. */

/* $ Examples */

/*     See the SPKNIO routine T13MDA. */

/* $ Restrictions */

/*     1) This routine is for use only within the program TSPICE. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 2.0.0, 19-OCT-2012 (NJB) */

/*        Renamed for use in TSPICE. Typo in Particulars header section */
/*        was corrected: lower bound on summation for T' is now 1. */

/* -    SPKNIO Version 1.0.0, 24-MAR-2003 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local variables */

    /* Parameter adjustments */
    mbuff_dim1 = (*n << 2) * *n;
    xvals_dim1 = *n;

    /* Function Body */
    if (return_()) {
	return 0;
    }
    chkin_("T_TAYMAT", (ftnlen)8);
    if (*n > 25) {
	setmsg_("Grid size # exceeds maximum allowed value #.", (ftnlen)44);
	errint_("#", n, (ftnlen)1);
	errint_("#", &c__25, (ftnlen)1);
	sigerr_("SPICE(OVERFLOW)", (ftnlen)15);
	chkout_("T_TAYMAT", (ftnlen)8);
	return 0;
    }

/*     Let DEG be the Hermite polynomial degree. */

    deg = (*n << 1) - 1;

/*     Fill in the top half of M. */

    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	m[(i__2 = i__ - 1) < 2500 && 0 <= i__2 ? i__2 : s_rnge("m", i__2, 
		"t_taymat__", (ftnlen)231)] = 1.;
	i__2 = deg;
	for (j = 1; j <= i__2; ++j) {
	    m[(i__3 = i__ + j * 50 - 1) < 2500 && 0 <= i__3 ? i__3 : s_rnge(
		    "m", i__3, "t_taymat__", (ftnlen)235)] = (xvals[(i__4 = 
		    i__ - 1) < xvals_dim1 && 0 <= i__4 ? i__4 : s_rnge("xvals"
		    , i__4, "t_taymat__", (ftnlen)235)] - *x) * m[(i__5 = i__ 
		    + (j - 1) * 50 - 1) < 2500 && 0 <= i__5 ? i__5 : s_rnge(
		    "m", i__5, "t_taymat__", (ftnlen)235)];
	}
    }

/*     Fill in the bottom half of M. */

    i__1 = *n << 1;
    for (i__ = *n + 1; i__ <= i__1; ++i__) {
	m[(i__2 = i__ - 1) < 2500 && 0 <= i__2 ? i__2 : s_rnge("m", i__2, 
		"t_taymat__", (ftnlen)246)] = 0.;
	m[(i__2 = i__ + 49) < 2500 && 0 <= i__2 ? i__2 : s_rnge("m", i__2, 
		"t_taymat__", (ftnlen)247)] = 1.;
	i__2 = deg;
	for (j = 2; j <= i__2; ++j) {
	    m[(i__3 = i__ + j * 50 - 1) < 2500 && 0 <= i__3 ? i__3 : s_rnge(
		    "m", i__3, "t_taymat__", (ftnlen)251)] = m[(i__4 = i__ - *
		    n + (j - 1) * 50 - 1) < 2500 && 0 <= i__4 ? i__4 : s_rnge(
		    "m", i__4, "t_taymat__", (ftnlen)251)] * j;
	}
    }

/*     Pack the significant portion of M into a buffer of */
/*     contiguous elements. */

    j = 1;
    i__1 = deg;
    for (i__ = 0; i__ <= i__1; ++i__) {
	i__4 = *n << 1;
	moved_(&m[(i__2 = i__ * 50) < 2500 && 0 <= i__2 ? i__2 : s_rnge("m", 
		i__2, "t_taymat__", (ftnlen)267)], &i__4, &mbuff[(i__3 = j - 
		1) < mbuff_dim1 && 0 <= i__3 ? i__3 : s_rnge("mbuff", i__3, 
		"t_taymat__", (ftnlen)267)]);
	j += *n << 1;
    }
    chkout_("T_TAYMAT", (ftnlen)8);
    return 0;
} /* t_taymat__ */

