/* t_urand2.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* $Procedure T_URAND2 ( Mirror T_URAND ) */
doublereal t_urand2__(integer *idum)
{
    /* Initialized data */

    static integer idum2 = 123456789;
    static integer iv[32] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0 };
    static integer iy = 0;

    /* System generated locals */
    integer i__1;
    doublereal ret_val, d__1;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    integer j, k;

/* $ Abstract */

/*     Alternate implementation of T_URAND. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     None. */

/* $ Keywords */

/*     MATH */
/*     NUMBERS */

/* $ Declarations */
/* $ Brief_I/O */

/*     Variable  I/O  Description */
/*     --------  ---  -------------------------------------------------- */
/*     IDUM      I-O  Seed for random number generation. */

/*     The function returns values that are uniformly distributed over */
/*     the open interval (0,1). */

/* $ Detailed_Input */

/*     IDUM           is a seed for the random number generator. On the */
/*                    first call to T_URAND, IDUM should be set equal to */
/*                    a negative number.  After the first call, IDUM */
/*                    should not be modified by the caller, except to */
/*                    start a new sequence of random numbers. */

/* $ Detailed_Output */

/*     IDUM           is the seed value to be used as input on the next */
/*                    call to T_URAND to produce the next random number */
/*                    in the sequence. */

/*     The function returns values that are approximately uniformly */
/*     distributed over the open interval (0, 1). */

/* $ Parameters */

/*     None. */

/* $ Exceptions */

/*     Error free. */

/* $ Files */

/*     None. */

/* $ Particulars */

/*     This routine supports testing of T_URAND. This is simply */
/*     an independently coded version of the same algorithm used */
/*     by that routine. */

/* $ Examples */

/*     None. */

/* $ Restrictions */

/*     This routine is to be used only within TSPICE. */

/* $ Literature_References */

/*     [1]  "Numerical Recipes---The Art of Scientific Computing" by */
/*           William H. Press, Brian P. Flannery, Saul A. Teukolsky, */
/*           William T. Vetterling (pp 272-273). */

/* $ Author_and_Institution */

/*     N.J. Bachman   (JPL) */

/* $ Version */

/* -    TSPICE Version 1.0.0, 17-OCT-2011 (NJB) */

/* -& */

/*     Local parameters */


/*     Local variables */


/*     Saved variables */


/*     Initial values */


/*     Initialize the output to an invalid value. */

    ret_val = -1.;
    if (*idum <= 0) {

/*        Initialize. */

/*        Be sure to prevent IDUM == 0. */

/* Computing MAX */
	i__1 = -(*idum);
	*idum = max(i__1,1);
	idum2 = *idum;

/*        Load the shuffle table (after 8 warm-ups). */

	for (j = 40; j >= 1; --j) {
	    k = *idum / 53668;
	    *idum = (*idum - k * 53668) * 40014 - k * 12211;
	    if (*idum < 0) {
		*idum += 2147483563;
	    }
	    if (j <= 32) {
		iv[(i__1 = j - 1) < 32 && 0 <= i__1 ? i__1 : s_rnge("iv", 
			i__1, "t_urand2__", (ftnlen)217)] = *idum;
	    }
	}
	iy = iv[0];
    }

/*     Start here when not initializing. */

    k = *idum / 53668;

/*     Compute IDUM = MOD(IA1*IDUM,IM1) without */
/*     overflows by Schrage's method. */

    *idum = (*idum - k * 53668) * 40014 - k * 12211;
    if (*idum < 0) {
	*idum += 2147483563;
    }
    k = idum2 / 52774;

/*     Compute IDUM2 = MOD(IA2*IDUM2,IM2) likewise. */

    idum2 = (idum2 - k * 52774) * 40692 - k * 3791;
    if (idum2 < 0) {
	idum2 += 2147483399;
    }

/*     J will be in the range 1:NTAB */

    j = iy / 67108862 + 1;

/*     Here IDUM is shuffled, IDUM and IDUM2 are */
/*     combined to generate output. */

    iy = iv[(i__1 = j - 1) < 32 && 0 <= i__1 ? i__1 : s_rnge("iv", i__1, 
	    "t_urand2__", (ftnlen)260)] - idum2;
    iv[(i__1 = j - 1) < 32 && 0 <= i__1 ? i__1 : s_rnge("iv", i__1, "t_urand"
	    "2__", (ftnlen)261)] = *idum;
    if (iy < 1) {
	iy += 2147483562;
    }

/*     Because users don't expect endpoint values... */

/* Computing MIN */
    d__1 = iy * 4.6566130573917691e-10;
    ret_val = min(d__1,.99999987999999995);
    return ret_val;
} /* t_urand2__ */

