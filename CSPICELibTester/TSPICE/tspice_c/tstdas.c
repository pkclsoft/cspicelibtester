/* tstdas.f -- translated by f2c (version 19980913).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__5000 = 5000;
static integer c__3 = 3;
static integer c__10000 = 10000;
static integer c__1 = 1;
static integer c__1024 = 1024;

/* $Procedure      TSTDAS ( Test DAS address calculation routine ) */
/* Subroutine */ int tstdas_(char *file, char *ftype, integer *ncomrc, 
	integer *fileno, integer *nclust, integer *cltyps, integer *clnwds, 
	logical *sgrgat, logical *fclose, integer *nschem, integer *handle, 
	ftnlen file_len, ftnlen ftype_len)
{
    /* Initialized data */

    static integer nwrec[3] = { 1024,128,256 };

    /* System generated locals */
    integer i__1, i__2, i__3, i__4;

    /* Builtin functions */
    integer s_rnge(char *, integer, char *, integer);

    /* Local variables */
    static char crec[1024*20];
    static doublereal drec[2560]	/* was [128][20] */;
    static integer irec[5120]	/* was [256][20] */, type__, i__, j, k;
    extern /* Subroutine */ int chkin_(char *, ftnlen);
    static integer clidx, recno;
    extern /* Subroutine */ int dasadc_(integer *, integer *, integer *, 
	    integer *, char *, ftnlen), dasadd_(integer *, integer *, 
	    doublereal *);
    extern logical failed_(void);
    extern /* Subroutine */ int dasadi_(integer *, integer *, integer *), 
	    cleari_(integer *, integer *), dasllc_(integer *);
    static integer nw;
    extern /* Subroutine */ int dascls_(integer *);
    static integer remain;
    extern /* Subroutine */ int daswbr_(integer *), sigerr_(char *, ftnlen), 
	    dasonw_(char *, char *, char *, integer *, integer *, ftnlen, 
	    ftnlen, ftnlen), chkout_(char *, ftnlen), setmsg_(char *, ftnlen),
	     errint_(char *, integer *, ftnlen);
    static integer nwrite;
    extern logical return_(void);
    static integer nwritn[3], ncl[3];

/* $ Abstract */

/*     Create DAS files used to test the SPICELIB DAS address */
/*     calculation routine DASA2L. */

/* $ Disclaimer */

/*     THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE */
/*     CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S. */
/*     GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE */
/*     ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE */
/*     PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS" */
/*     TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY */
/*     WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A */
/*     PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC */
/*     SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE */
/*     SOFTWARE AND RELATED MATERIALS, HOWEVER USED. */

/*     IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA */
/*     BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT */
/*     LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, */
/*     INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS, */
/*     REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE */
/*     REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY. */

/*     RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF */
/*     THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY */
/*     CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE */
/*     ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE. */

/* $ Required_Reading */

/*     DAS */

/* $ Keywords */

/*     DAS */
/*     TEST FAMILY */
/*     TSPICE */
/*     TEST UTILITY */

/* $ Declarations */
/* $ Brief_I/O */

/*     VARIABLE  I/O  DESCRIPTION */
/*     --------  ---  -------------------------------------------------- */
/*     SCHADD     P   Generate data by address. */
/*     SCHCOD     P   Generate encoded data. */
/*     MAXCL      P   Maximum cluster size. */
/*     MAXCLW     P   Maximum cluster record count. */
/*     MAXFIL     P   Maximum file number. */
/*     FILE       I   Name of DAS file to create. */
/*     FTYPE      I   File type to use in DAS ID word. */
/*     NCOMRC     I   Number of comment records to reserve. */
/*     FILENO     I   File number used in data generation. */
/*     NCLUST     I   Number of data clusters. */
/*     CLTYPS     I   Data types of data clusters. */
/*     CLNWDS     I   Word counts of data clusters. */
/*     SGRGAT     I   Flag indicating whether to segregate file. */
/*     FCLOSE     I   Flag indicating whether to close file. */
/*     NSCHEM     I   Number scheme used for data generation. */
/*     HANDLE     O   DAS file handle. */

/* $ Detailed_Input */

/*     FILE       is the name of a new DAS file to create. */

/*     FTYPE      is the file type to use in the DAS ID word. This */
/*                string will be appended after the forward slash. The */
/*                maximum length of FTYPE is 4 characters. */

/*     NCOMRC     is the number of comment records to reserve in the */
/*                new DAS file. */

/*     FILENO     is the number of the new DAS file. This is an */
/*                arbitrary integer used to create file-specific */
/*                data when the data generation scheme is SCHCOD. */

/*     NCLUST     is the number of data clusters to write to the */
/*                new DAS file. */

/*     CLTYPS     is an array of cluster data types; the Ith element of */
/*                CLTYPS is the data type of the Ith cluster. */

/*     CLNWDS     is an array of cluster word counts; the Ith element of */
/*                CLNWDS is the word count of the Ith cluster. Note that */
/*                only the final cluster of each data type may be */
/*                partially filled. */

/*     SGRGAT     is a logical flag indicating whether the new file is */
/*                to be segregated. This flag is ignore if the file is */
/*                left open; in that case the file will be left in */
/*                in its final state after writing is complete. */
/*                Normally in this case the file will be unsegregated. */

/*     FCLOSE     is a logical flag indicating whether the new file is */
/*                to be closed. If FLCOSE is .TRUE., then SGRGAT is used */
/*                to determine whether the file is segregated. If the */
/*                file is closed, the output handle is undefined. */

/*     NSCHEM     is a parameter indicating which data generation scheme */
/*                is to be used. See the Parameters section below for */
/*                details. */

/* $ Detailed_Output */

/*     HANDLE     is a DAS handle associated with the new DAS file */
/*                created by this routine. HANDLE is valid only if the */
/*                file is left open, which happens if the input FLCOSE */
/*                is set to .FALSE. */

/* $ Parameters */

/*     SCHADD     indicates that each numeric word contains its own */
/*                address. Character words contain their addresses mod */
/*                128. */

/*     SCHCOD     indicates that each numeric word contains a code based */
/*                on the file number, record, and address. */

/*     MAXCL      is the maximum cluster size supported by this routine; */
/*                units are records. */


/*     MAXREC     is the maximum number of records in a file created by */
/*                this routine. */

/*     MAXFIL     is the maximum value of FILENO that is supported by */
/*                this routine. */

/* $ Exceptions */

/*     1)  If FILENO is larger than MAXFIL, the error */
/*         SPICE(VALUEOUTOFRANGE) is signaled. */

/*     2)  If an unsupported data type is found in the array CLTYPS, the */
/*         error SPICE(BADDATATYPE) is signaled. */

/*     3)  If a cluster word count exceeds the maximum value for */
/*         the cluster's data type, the error SPICE(INVALIDCOUNT) */
/*         is signaled. No cluster written by this routine */
/*         may exceed MAXCL records. */

/*     4)  If a cluster is not the last of its data type and has */
/*         a word count that does not correspond to an integer */
/*         number of data records of that type, the error */
/*         SPICE(INVALIDCOUNT) is signaled. */

/* $ Files */

/*     This routine creates DAS files having specified cluster types */
/*     and sizes. The caller can control the scheme used to create */
/*     data via the data scheme input NSCHEM. */

/*     The caller can indicate whether the file to be created should */
/*     be left open or closed, and if the file is closed, whether it */
/*     is to be segregated. */

/* $ Particulars */

/*     See the Files and Parameters sections above. */

/* $ Examples */

/*     See use in F_DASA2L. */

/* $ Restrictions */

/*     This is a SPICE test routine; its specification and interface */
/*     may be updated without notice. */

/* $ Literature_References */

/*     None. */

/* $ Author_and_Institution */

/*     N.J. Bachman     (JPL) */

/* $ Version */

/* -    TSPICE Version 1.1.0, 13-FEB-2015 (NJB) */

/*        Bug fix: now writes buffered records via DASWBR */
/*        in the branch that performs an unsegregated close. */

/*        Bug fix: test of file count against MAXFIL now uses */
/*        strict inequality (.GT.) rather than .GE. */

/*        Updated MAXFIL to 5000. */


/* -    TSPICE Version 1.0.0, 11-APR-2014 (NJB) */

/* -& */

/*     SPICELIB functions */


/*     Local parameters */

/*     These parameters are named to enhance ease of maintenance of */
/*     the code; the values should not be changed. */

/*     File number factor used for creating data: */


/*     Local variables */


/*     Saved variables */


/*     Initial values */

    if (return_()) {
	return 0;
    }
    chkin_("TSTDAS", (ftnlen)6);
    if (*fileno > 5000) {
	setmsg_("FILENO = #; max allowed value is #.", (ftnlen)35);
	errint_("#", fileno, (ftnlen)1);
	errint_("#", &c__5000, (ftnlen)1);
	sigerr_("SPICE(VALUEOUTOFRANGE)", (ftnlen)22);
	chkout_("TSTDAS", (ftnlen)6);
	return 0;
    }

/*     Check cluster data types and sizes. */

    cleari_(&c__3, ncl);
    for (clidx = *nclust; clidx >= 1; --clidx) {
	type__ = cltyps[clidx - 1];

/*        Check the data type before using it as an array index. */

	if (type__ < 1 || type__ > 3) {
	    setmsg_("Invalid data type code # found for cluster #.", (ftnlen)
		    45);
	    errint_("#", &type__, (ftnlen)1);
	    errint_("#", &clidx, (ftnlen)1);
	    sigerr_("SPICE(BADDATATYPE)", (ftnlen)18);
	    chkout_("TSTDAS", (ftnlen)6);
	    return 0;
	}

/*        Check the cluster size. The upper bound check is */
/*        loose, but it may catch uninitialized junk. */

	i__ = clnwds[clidx - 1];
	if (i__ < 1 || i__ > nwrec[(i__1 = type__ - 1) < 3 && 0 <= i__1 ? 
		i__1 : s_rnge("nwrec", i__1, "tstdas_", (ftnlen)354)] * 20) {
	    setmsg_("Invalid cluster word count # found for cluster #.", (
		    ftnlen)49);
	    errint_("#", &i__, (ftnlen)1);
	    errint_("#", &clidx, (ftnlen)1);
	    sigerr_("SPICE(INVALIDCOUNT)", (ftnlen)19);
	    chkout_("TSTDAS", (ftnlen)6);
	    return 0;
	}

/*        The cluster must be "full" unless this is */
/*        the final cluster of this data type. Check it. */

	ncl[(i__1 = type__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("ncl", i__1, 
		"tstdas_", (ftnlen)371)] = ncl[(i__2 = type__ - 1) < 3 && 0 <=
		 i__2 ? i__2 : s_rnge("ncl", i__2, "tstdas_", (ftnlen)371)] + 
		1;
	nw = nwrec[(i__1 = type__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge("nwr"
		"ec", i__1, "tstdas_", (ftnlen)373)];
	if (i__ / nw * nw != i__) {

/*           The word count is not an integral multiple */
/*           of the number of words of TYPE that fit in a */
/*           record. */

	    if (ncl[(i__1 = type__ - 1) < 3 && 0 <= i__1 ? i__1 : s_rnge(
		    "ncl", i__1, "tstdas_", (ftnlen)381)] > 1) {

/*              We're searching backward in the cluster lists, */
/*              so a cluster count greater than 1 means we're */
/*              not looking at the last cluster of data type */
/*              TYPE. */

		setmsg_("Partial cluster found prior to last cluster of data"
			" type #. Cluster index is #. Cluster word count is #."
			, (ftnlen)104);
		errint_("#", &type__, (ftnlen)1);
		errint_("#", &clidx, (ftnlen)1);
		errint_("#", &i__, (ftnlen)1);
		sigerr_("SPICE(INVALIDCOUNT)", (ftnlen)19);
		chkout_("TSTDAS", (ftnlen)6);
		return 0;
	    }
	}
    }

/*     Open the file. */

    dasonw_(file, ftype, file, ncomrc, handle, file_len, ftype_len, file_len);
    if (failed_()) {
	chkout_("TSTDAS", (ftnlen)6);
	return 0;
    }

/*     Write data to the file. */

    cleari_(&c__3, nwritn);
    recno = 1;
    i__1 = *nclust;
    for (clidx = 1; clidx <= i__1; ++clidx) {
	if (recno >= 10000) {
	    setmsg_("RECNO = #; max allowed value is #.", (ftnlen)34);
	    errint_("#", &recno, (ftnlen)1);
	    errint_("#", &c__10000, (ftnlen)1);
	    sigerr_("SPICE(VALUEOUTOFRANGE)", (ftnlen)22);
	    chkout_("TSTDAS", (ftnlen)6);
	    return 0;
	}
	type__ = cltyps[clidx - 1];
	nw = nwrec[(i__2 = type__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge("nwr"
		"ec", i__2, "tstdas_", (ftnlen)436)];

/*        Create data current cluster, one record at a time. Write */
/*        the entire cluster using a single call. */

	if (type__ == 3) {

/*           Populate a buffer with data of the indicated type. */

	    remain = clnwds[clidx - 1];
	    j = 1;
	    while(remain > 0) {
		nwrite = min(nw,remain);
		i__2 = nwrite;
		for (i__ = 1; i__ <= i__2; ++i__) {
		    if (*nschem == 1) {
			irec[(i__3 = i__ + (j << 8) - 257) < 5120 && 0 <= 
				i__3 ? i__3 : s_rnge("irec", i__3, "tstdas_", 
				(ftnlen)457)] = i__ + nwritn[(i__4 = type__ - 
				1) < 3 && 0 <= i__4 ? i__4 : s_rnge("nwritn", 
				i__4, "tstdas_", (ftnlen)457)];
		    } else {
			irec[(i__3 = i__ + (j << 8) - 257) < 5120 && 0 <= 
				i__3 ? i__3 : s_rnge("irec", i__3, "tstdas_", 
				(ftnlen)461)] = *fileno * 100000 + recno * 
				1000 + i__;
		    }
		}
		nwritn[(i__2 = type__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"nwritn", i__2, "tstdas_", (ftnlen)468)] = nwritn[(
			i__3 = type__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"nwritn", i__3, "tstdas_", (ftnlen)468)] + nwrite;
		++j;
		++recno;
		remain -= nwrite;
	    }
	    dasadi_(handle, &clnwds[clidx - 1], irec);
	    if (failed_()) {
		chkout_("TSTDAS", (ftnlen)6);
		return 0;
	    }
	} else if (type__ == 2) {
	    remain = clnwds[clidx - 1];
	    j = 1;
	    while(remain > 0) {
		nwrite = min(nw,remain);
		i__2 = nwrite;
		for (i__ = 1; i__ <= i__2; ++i__) {
		    if (*nschem == 1) {
			drec[(i__3 = i__ + (j << 7) - 129) < 2560 && 0 <= 
				i__3 ? i__3 : s_rnge("drec", i__3, "tstdas_", 
				(ftnlen)497)] = (doublereal) i__ + nwritn[(
				i__4 = type__ - 1) < 3 && 0 <= i__4 ? i__4 : 
				s_rnge("nwritn", i__4, "tstdas_", (ftnlen)497)
				];
		    } else {
			drec[(i__3 = i__ + (j << 7) - 129) < 2560 && 0 <= 
				i__3 ? i__3 : s_rnge("drec", i__3, "tstdas_", 
				(ftnlen)500)] = *fileno * 100000 + recno * 
				1000 + (doublereal) i__;
		    }
		}
		nwritn[(i__2 = type__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"nwritn", i__2, "tstdas_", (ftnlen)507)] = nwritn[(
			i__3 = type__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"nwritn", i__3, "tstdas_", (ftnlen)507)] + nwrite;
		++j;
		++recno;
		remain -= nwrite;
	    }
	    dasadd_(handle, &clnwds[clidx - 1], drec);
	    if (failed_()) {
		chkout_("TSTDAS", (ftnlen)6);
		return 0;
	    }
	} else if (type__ == 1) {
	    remain = clnwds[clidx - 1];
	    j = 1;
	    while(remain > 0) {
		nwrite = min(nw,remain);
		i__2 = nwrite;
		for (i__ = 1; i__ <= i__2; ++i__) {
		    if (*nschem == 1) {
			k = i__ + nwritn[(i__3 = type__ - 1) < 3 && 0 <= i__3 
				? i__3 : s_rnge("nwritn", i__3, "tstdas_", (
				ftnlen)537)];
		    } else {
			k = *fileno * 100000 + recno * 1000 + i__;
		    }
		    *(unsigned char *)&crec[(((i__3 = j - 1) < 20 && 0 <= 
			    i__3 ? i__3 : s_rnge("crec", i__3, "tstdas_", (
			    ftnlen)544)) << 10) + (i__ - 1)] = (char) (k % 
			    128);
		}
		nwritn[(i__2 = type__ - 1) < 3 && 0 <= i__2 ? i__2 : s_rnge(
			"nwritn", i__2, "tstdas_", (ftnlen)548)] = nwritn[(
			i__3 = type__ - 1) < 3 && 0 <= i__3 ? i__3 : s_rnge(
			"nwritn", i__3, "tstdas_", (ftnlen)548)] + nwrite;
		++j;
		++recno;
		remain -= nwrite;
	    }
	    dasadc_(handle, &clnwds[clidx - 1], &c__1, &c__1024, crec, (
		    ftnlen)1024);
	    if (failed_()) {
		chkout_("TSTDAS", (ftnlen)6);
		return 0;
	    }
	} else {
	    setmsg_("Bad data type # found for cluster #.", (ftnlen)36);
	    errint_("#", &type__, (ftnlen)1);
	    errint_("#", &clidx, (ftnlen)1);
	    sigerr_("SPICE(BADDATATYPE)", (ftnlen)18);
	    chkout_("TSTDAS", (ftnlen)6);
	    return 0;
	}
    }
    if (*fclose) {

/*        Close the file. Segregate the records if so commanded. */

	if (*sgrgat) {
	    dascls_(handle);
	} else {
	    daswbr_(handle);
	    dasllc_(handle);
	}
    }
    chkout_("TSTDAS", (ftnlen)6);
    return 0;
} /* tstdas_ */

