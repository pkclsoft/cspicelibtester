//
//  ViewController.m
//  CSPICELibTester
//
//  Created by Peter Easdown on 24/8/18.
//  Copyright © 2018 PKCLsoft. All rights reserved.
//

#import "ViewController.h"

#include <string.h>
#include <assert.h>
#include "SpiceUsr.h"
#include "SpiceZfc.h"
#include "SpiceZst.h"
#include "tutils_c.h"

#include "sandbox_interface.h"
#import "Persistency.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [Persistency removeAllDocuments];

    // This must be done on iOS so that file IO is done within the sandbox, within a folder to which the app has write permissions.
    //
//    setDocumentsFolder([[Persistency documentPath] cStringUsingEncoding:NSASCIIStringEncoding]);
    setDocumentsFolder([[[[NSBundle mainBundle] pathForResource:@"naif0012" ofType:@"tls"] stringByDeletingLastPathComponent] cStringUsingEncoding:NSASCIIStringEncoding]);

//    [self testSpice];
    [self findMars];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) findMars {
    // first load the data files.
    //
    // load the
    furnsh_c("naif0012.tls");
    furnsh_c("de432s.bsp");

    // now get the position of mars.
    //
    SpiceDouble tdb;
    SpiceDouble lt;
    SpiceDouble position[3];

    str2et_c([[self date2str:[NSDate date] onlyDate:NO] cStringUsingEncoding:NSASCIIStringEncoding], &tdb);
    spkpos_c("MARS BARYCENTER", tdb, "J2000", "LT+S", "SUN", position, &lt);
    NSLog(@"%2.4f, %2.4f, %2.4f", position[0], position[1], position[2]);
}

- (NSString*) date2str:(NSDate*)date onlyDate:(BOOL)onlyDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    if (onlyDate) {
        [formatter setDateFormat:@"yyyy-MM-dd"];
    }else{
        [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    }

    return [formatter stringFromDate:date];
}

- (void) testSpice {
    /* Begin tspice_c.pgm */


    /*
     Prototypes for test families generated from FORTRAN.
     */

    int f_aaaaphsh__ ( logical * plain_ok );
    int f_ab__ ( logical * plain_ok );
    int f_asnacsn__ ( logical * plain_ok );
    int f_badkpv__ ( logical * plain_ok );
    int f_bodcod__ ( logical * plain_ok );
    int f_bodvar__ ( logical * plain_ok );
    int f_ccifrm__ ( logical * plain_ok );
    int f_chbigr__ ( logical * plain_ok );
    int f_chgirf__ ( logical * plain_ok );
    int f_chgrfx__ ( logical * plain_ok );
    int f_ck05__ ( logical * plain_ok );
    int f_ck06__ ( logical * plain_ok );
    int f_ckbsr__ ( logical * plain_ok );
    int f_ckcov__ ( logical * plain_ok );
    int f_ckfrot__ ( logical * plain_ok );
    int f_ckfxfm__ ( logical * plain_ok );
    int f_ckgp__ ( logical * plain_ok );
    int f_ckmeta__ ( logical * plain_ok );
    int f_ckw01__ ( logical * plain_ok );
    int f_ckw02__ ( logical * plain_ok );
    int f_ckw03__ ( logical * plain_ok );
    int f_convrt__ ( logical * plain_ok );
    int f_crdcnv__ ( logical * plain_ok );
    int f_cyip__ ( logical * plain_ok );
    int f_dafah__ ( logical * plain_ok );
    int f_dafana__ ( logical * plain_ok );
    int f_dafnn__ ( logical * plain_ok );
    int f_dasa2l__ ( logical * plain_ok );
    int f_dascud__ ( logical * plain_ok );
    int f_dasfr__ ( logical * plain_ok );
    int f_dasmul__ ( logical * plain_ok );
    int f_ddhcls__ ( logical * plain_ok );
    int f_ddhclu__ ( logical * plain_ok );
    int f_ddhf2h__ ( logical * plain_ok );
    int f_ddhfnh__ ( logical * plain_ok );
    int f_ddhftsize__ ( logical * plain_ok );
    int f_ddhgsd__ ( logical * plain_ok );
    int f_ddhgtu__ ( logical * plain_ok );
    int f_ddhhlu__ ( logical * plain_ok );
    int f_ddhisn__ ( logical * plain_ok );
    int f_ddhivf__ ( logical * plain_ok );
    int f_ddhluh__ ( logical * plain_ok );
    int f_ddhnfo__ ( logical * plain_ok );
    int f_ddhopn__ ( logical * plain_ok );
    int f_ddhppf__ ( logical * plain_ok );
    int f_ddhrcm__ ( logical * plain_ok );
    int f_ddhrmu__ ( logical * plain_ok );
    int f_ddhunl__ ( logical * plain_ok );
    int f_dhfa__ ( logical * plain_ok );
    int f_dla__ ( logical * plain_ok );
    int f_dnearp__ ( logical * plain_ok );
    int f_dpfmt__ ( logical * plain_ok );
    int f_dpstrf__ ( logical * plain_ok );
    int f_dsk02__ ( logical * plain_ok );
    int f_dskkpr__ ( logical * plain_ok );
    int f_dskmi2__ ( logical * plain_ok );
    int f_dskobj__ ( logical * plain_ok );
    int f_dskrb2__ ( logical * plain_ok );
    int f_dsktol__ ( logical * plain_ok );
    int f_dskw02__ ( logical * plain_ok );
    int f_dskx02__ ( logical * plain_ok );
    int f_dskxsi__ ( logical * plain_ok );
    int f_dskxv__ ( logical * plain_ok );
    int f_dvops__ ( logical * plain_ok );
    int f_dvsep__ ( logical * plain_ok );
    int f_dwpool__ ( logical * plain_ok );
    int f_dyn01__ ( logical * plain_ok );
    int f_dyn02__ ( logical * plain_ok );
    int f_dyn03__ ( logical * plain_ok );
    int f_dyn04__ ( logical * plain_ok );
    int f_dyn05__ ( logical * plain_ok );
    int f_dyn06__ ( logical * plain_ok );
    int f_dyn07__ ( logical * plain_ok );
    int f_dyn08__ ( logical * plain_ok );
    int f_ednmpt__ ( logical * plain_ok );
    int f_edpnt__ ( logical * plain_ok );
    int f_ek01__ ( logical * plain_ok );
    int f_ek02__ ( logical * plain_ok );
    int f_ek03__ ( logical * plain_ok );
    int f_ek04__ ( logical * plain_ok );
    int f_eqncpv__ ( logical * plain_ok );
    int f_et2lst__ ( logical * plain_ok );
    int f_et2utc__ ( logical * plain_ok );
    int f_etcal__ ( logical * plain_ok );
    int f_euler__ ( logical * plain_ok );
    int f_fndcmp__ ( logical * plain_ok );
    int f_fovray__ ( logical * plain_ok );
    int f_fovtrg__ ( logical * plain_ok );
    int f_framex__ ( logical * plain_ok );
    int f_frftch__ ( logical * plain_ok );
    int f_frmchg__ ( logical * plain_ok );
    int f_frmget__ ( logical * plain_ok );
    int f_ge01__ ( logical * plain_ok );
    int f_getfov__ ( logical * plain_ok );
    int f_getfv2__ ( logical * plain_ok );
    int f_gfbail__ ( logical * plain_ok );
    int f_gfbirp__ ( logical * plain_ok );
    int f_gfdirp__ ( logical * plain_ok );
    int f_gfdist__ ( logical * plain_ok );
    int f_gfevnt__ ( logical * plain_ok );
    int f_gffove__ ( logical * plain_ok );
    int f_gfilum__ ( logical * plain_ok );
    int f_gflorp__ ( logical * plain_ok );
    int f_gfocce__ ( logical * plain_ok );
    int f_gfoclt__ ( logical * plain_ok );
    int f_gfpa__ ( logical * plain_ok );
    int f_gfpcrp__ ( logical * plain_ok );
    int f_gfposc__ ( logical * plain_ok );
    int f_gfrefn__ ( logical * plain_ok );
    int f_gfrfov__ ( logical * plain_ok );
    int f_gfrprt__ ( logical * plain_ok );
    int f_gfrr__ ( logical * plain_ok );
    int f_gfscrp__ ( logical * plain_ok );
    int f_gfsep__ ( logical * plain_ok );
    int f_gfsntc__ ( logical * plain_ok );
    int f_gfsprp__ ( logical * plain_ok );
    int f_gfstep__ ( logical * plain_ok );
    int f_gfsubc__ ( logical * plain_ok );
    int f_gftfov__ ( logical * plain_ok );
    int f_gfudb__ ( logical * plain_ok );
    int f_gfuds__ ( logical * plain_ok );
    int f_gfxcrp__ ( logical * plain_ok );
    int f_hrmite__ ( logical * plain_ok );
    int f_illumf__ ( logical * plain_ok );
    int f_illumg__ ( logical * plain_ok );
    int f_ilumin__ ( logical * plain_ok );
    int f_incnsg__ ( logical * plain_ok );
    int f_inelpl__ ( logical * plain_ok );
    int f_inrypl__ ( logical * plain_ok );
    int f_insang__ ( logical * plain_ok );
    int f_insert__ ( logical * plain_ok );
    int f_iovcmp__ ( logical * plain_ok );
    int f_jul2gr__ ( logical * plain_ok );
    int f_keeper__ ( logical * plain_ok );
    int f_kpbug__ ( logical * plain_ok );
    int f_kpsolv__ ( logical * plain_ok );
    int f_lagrng__ ( logical * plain_ok );
    int f_latsrf__ ( logical * plain_ok );
    int f_limbpt__ ( logical * plain_ok );
    int f_locati__ ( logical * plain_ok );
    int f_lparse__ ( logical * plain_ok );
    int f_ls__ ( logical * plain_ok );
    int f_ltime__ ( logical * plain_ok );
    int f_m2q__ ( logical * plain_ok );
    int f_matrix3__ ( logical * plain_ok );
    int f_matrixg__ ( logical * plain_ok );
    int f_moved__ ( logical * plain_ok );
    int f_nearpt__ ( logical * plain_ok );
    int f_nnek01__ ( logical * plain_ok );
    int f_nnek03__ ( logical * plain_ok );
    int f_nnek04__ ( logical * plain_ok );
    int f_npedln__ ( logical * plain_ok );
    int f_npelpt__ ( logical * plain_ok );
    int f_nplnpt__ ( logical * plain_ok );
    int f_npsgpt__ ( logical * plain_ok );
    int f_occult__ ( logical * plain_ok );
    int f_oscelt__ ( logical * plain_ok );
    int f_oscltx__ ( logical * plain_ok );
    int f_pck20__ ( logical * plain_ok );
    int f_pckbsr__ ( logical * plain_ok );
    int f_pckbuf__ ( logical * plain_ok );
    int f_pckcov__ ( logical * plain_ok );
    int f_pgr__ ( logical * plain_ok );
    int f_phaseq__ ( logical * plain_ok );
    int f_pln__ ( logical * plain_ok );
    int f_plt__ ( logical * plain_ok );
    int f_pltnp__ ( logical * plain_ok );
    int f_pool__ ( logical * plain_ok );
    int f_pxform__ ( logical * plain_ok );
    int f_pxfrm2__ ( logical * plain_ok );
    int f_q2m__ ( logical * plain_ok );
    int f_qderiv__ ( logical * plain_ok );
    int f_quat__ ( logical * plain_ok );
    int f_rc2grd__ ( logical * plain_ok );
    int f_rdpck__ ( logical * plain_ok );
    int f_refchg__ ( logical * plain_ok );
    int f_reglon__ ( logical * plain_ok );
    int f_repmx__ ( logical * plain_ok );
    int f_rotget__ ( logical * plain_ok );
    int f_saelgv__ ( logical * plain_ok );
    int f_sbf__ ( logical * plain_ok );
    int f_sclk__ ( logical * plain_ok );
    int f_sctran__ ( logical * plain_ok );
    int f_sepool__ ( logical * plain_ok );
    int f_sgmeta__ ( logical * plain_ok );
    int f_sharpr__ ( logical * plain_ok );
    int f_sincpt__ ( logical * plain_ok );
    int f_slice__ ( logical * plain_ok );
    int f_spk01__ ( logical * plain_ok );
    int f_spk02__ ( logical * plain_ok );
    int f_spk03__ ( logical * plain_ok );
    int f_spk05__ ( logical * plain_ok );
    int f_spk08__ ( logical * plain_ok );
    int f_spk09__ ( logical * plain_ok );
    int f_spk10__ ( logical * plain_ok );
    int f_spk12__ ( logical * plain_ok );
    int f_spk13__ ( logical * plain_ok );
    int f_spk14__ ( logical * plain_ok );
    int f_spk17__ ( logical * plain_ok );
    int f_spk18__ ( logical * plain_ok );
    int f_spk19__ ( logical * plain_ok );
    int f_spk20__ ( logical * plain_ok );
    int f_spk21__ ( logical * plain_ok );
    int f_spkapo__ ( logical * plain_ok );
    int f_spkapp__ ( logical * plain_ok );
    int f_spkbsr__ ( logical * plain_ok );
    int f_spkcor__ ( logical * plain_ok );
    int f_spkcov__ ( logical * plain_ok );
    int f_spkcpv__ ( logical * plain_ok );
    int f_spke15__ ( logical * plain_ok );
    int f_spkez__ ( logical * plain_ok );
    int f_spkezp__ ( logical * plain_ok );
    int f_spkf15__ ( logical * plain_ok );
    int f_spkgeo__ ( logical * plain_ok );
    int f_spkgps__ ( logical * plain_ok );
    int f_spkgpx__ ( logical * plain_ok );
    int f_spkgxc__ ( logical * plain_ok );
    int f_spkpds__ ( logical * plain_ok );
    int f_spkpvn__ ( logical * plain_ok );
    int f_spks19__ ( logical * plain_ok );
    int f_spkspv__ ( logical * plain_ok );
    int f_spkw01__ ( logical * plain_ok );
    int f_srfnrm__ ( logical * plain_ok );
    int f_srftrn__ ( logical * plain_ok );
    int f_srfxpt__ ( logical * plain_ok );
    int f_stlabx__ ( logical * plain_ok );
    int f_stmp03__ ( logical * plain_ok );
    int f_stpool__ ( logical * plain_ok );
    int f_str2et__ ( logical * plain_ok );
    int f_string__ ( logical * plain_ok );
    int f_subpnt__ ( logical * plain_ok );
    int f_subslr__ ( logical * plain_ok );
    int f_surfnm__ ( logical * plain_ok );
    int f_surfpv__ ( logical * plain_ok );
    int f_swapac__ ( logical * plain_ok );
    int f_swapad__ ( logical * plain_ok );
    int f_swapai__ ( logical * plain_ok );
    int f_sxform__ ( logical * plain_ok );
    int f_symtbc__ ( logical * plain_ok );
    int f_symtbd__ ( logical * plain_ok );
    int f_symtbi__ ( logical * plain_ok );
    int f_t_urand__ ( logical * plain_ok );
    int f_tabtxt__ ( logical * plain_ok );
    int f_tcheck__ ( logical * plain_ok );
    int f_term__ ( logical * plain_ok );
    int f_termpt__ ( logical * plain_ok );
    int f_texpyr__ ( logical * plain_ok );
    int f_timcvr__ ( logical * plain_ok );
    int f_timdef__ ( logical * plain_ok );
    int f_timout__ ( logical * plain_ok );
    int f_tkfram__ ( logical * plain_ok );
    int f_tncnsg__ ( logical * plain_ok );
    int f_tparse__ ( logical * plain_ok );
    int f_tpartv1__ ( logical * plain_ok );
    int f_tpartv2__ ( logical * plain_ok );
    int f_tstck3__ ( logical * plain_ok );
    int f_ttrans__ ( logical * plain_ok );
    int f_twovxf__ ( logical * plain_ok );
    int f_utc2et__ ( logical * plain_ok );
    int f_vector3__ ( logical * plain_ok );
    int f_vectorg__ ( logical * plain_ok );
    int f_voxel__ ( logical * plain_ok );
    int f_vstrng__ ( logical * plain_ok );
    int f_win__ ( logical * plain_ok );
    int f_xdda__ ( logical * plain_ok );
    int f_xfmsta__ ( logical * plain_ok );
    int f_xfneul__ ( logical * plain_ok );
    int f_xfrav__ ( logical * plain_ok );
    int f_xlated__ ( logical * plain_ok );
    int f_xlatei__ ( logical * plain_ok );
    int f_zzasc1__ ( logical * plain_ok );
    int f_zzasc2__ ( logical * plain_ok );
    int f_zzasryel__ ( logical * plain_ok );
    int f_zzbdin__ ( logical * plain_ok );
    int f_zzbdkr__ ( logical * plain_ok );
    int f_zzbdtn__ ( logical * plain_ok );
    int f_zzbdtrn__ ( logical * plain_ok );
    int f_zzbods__ ( logical * plain_ok );
    int f_zzbods2c__ ( logical * plain_ok );
    int f_zzbodvcd__ ( logical * plain_ok );
    int f_zzbquad__ ( logical * plain_ok );
    int f_zzcke06__ ( logical * plain_ok );
    int f_zzcnquad__ ( logical * plain_ok );
    int f_zzcorsxf__ ( logical * plain_ok );
    int f_zzctr__ ( logical * plain_ok );
    int f_zzcxbrut__ ( logical * plain_ok );
    int f_zzdasgrc__ ( logical * plain_ok );
    int f_zzddhnfc__ ( logical * plain_ok );
    int f_zzdgdr__ ( logical * plain_ok );
    int f_zzdgfr__ ( logical * plain_ok );
    int f_zzdgsr__ ( logical * plain_ok );
    int f_zzdm__ ( logical * plain_ok );
    int f_zzdskbsr__ ( logical * plain_ok );
    int f_zzdskbux__ ( logical * plain_ok );
    int f_zzdsksel__ ( logical * plain_ok );
    int f_zzdsksgr__ ( logical * plain_ok );
    int f_zzdsksgx__ ( logical * plain_ok );
    int f_zzdsksph__ ( logical * plain_ok );
    int f_zzedtmpt__ ( logical * plain_ok );
    int f_zzellbds__ ( logical * plain_ok );
    int f_zzelnaxx__ ( logical * plain_ok );
    int f_zzelvupy0__ ( logical * plain_ok );
    int f_zzfdat__ ( logical * plain_ok );
    int f_zzfgeo__ ( logical * plain_ok );
    int f_zzfovaxi__ ( logical * plain_ok );
    int f_zzgetelm__ ( logical * plain_ok );
    int f_zzgfcoq__ ( logical * plain_ok );
    int f_zzgfcost__ ( logical * plain_ok );
    int f_zzgfcou__ ( logical * plain_ok );
    int f_zzgfcprx__ ( logical * plain_ok );
    int f_zzgfcslv__ ( logical * plain_ok );
    int f_zzgfdiq__ ( logical * plain_ok );
    int f_zzgfdiu__ ( logical * plain_ok );
    int f_zzgffvu__ ( logical * plain_ok );
    int f_zzgfilu__ ( logical * plain_ok );
    int f_zzgflng1__ ( logical * plain_ok );
    int f_zzgflng2__ ( logical * plain_ok );
    int f_zzgflng3__ ( logical * plain_ok );
    int f_zzgfocu__ ( logical * plain_ok );
    int f_zzgfpau__ ( logical * plain_ok );
    int f_zzgfrel__ ( logical * plain_ok );
    int f_zzgfrelx__ ( logical * plain_ok );
    int f_zzgfrpwk__ ( logical * plain_ok );
    int f_zzgfrru__ ( logical * plain_ok );
    int f_zzgfsolv__ ( logical * plain_ok );
    int f_zzgfsolvx__ ( logical * plain_ok );
    int f_zzgfspq__ ( logical * plain_ok );
    int f_zzgfspu__ ( logical * plain_ok );
    int f_zzgfssin__ ( logical * plain_ok );
    int f_zzgfssob__ ( logical * plain_ok );
    int f_zzgfwsts__ ( logical * plain_ok );
    int f_zzhsc__ ( logical * plain_ok );
    int f_zzhsi__ ( logical * plain_ok );
    int f_zzhullax__ ( logical * plain_ok );
    int f_zzilusta__ ( logical * plain_ok );
    int f_zzinlat__ ( logical * plain_ok );
    int f_zzinlat0__ ( logical * plain_ok );
    int f_zzinpdt__ ( logical * plain_ok );
    int f_zzinpdt0__ ( logical * plain_ok );
    int f_zzinrec__ ( logical * plain_ok );
    int f_zzinvelt__ ( logical * plain_ok );
    int f_zzlatbox__ ( logical * plain_ok );
    int f_zzmkspin__ ( logical * plain_ok );
    int f_zzmsxf__ ( logical * plain_ok );
    int f_zznamfrm__ ( logical * plain_ok );
    int f_zznofcon__ ( logical * plain_ok );
    int f_zznrmlon__ ( logical * plain_ok );
    int f_zzocced__ ( logical * plain_ok );
    int f_zzocced2__ ( logical * plain_ok );
    int f_zzocced3__ ( logical * plain_ok );
    int f_zzpdcmpl__ ( logical * plain_ok );
    int f_zzpdpltc__ ( logical * plain_ok );
    int f_zzpdtbox__ ( logical * plain_ok );
    int f_zzplat__ ( logical * plain_ok );
    int f_zzptpl02__ ( logical * plain_ok );
    int f_zzraybox__ ( logical * plain_ok );
    int f_zzrecbox__ ( logical * plain_ok );
    int f_zzrtnmat__ ( logical * plain_ok );
    int f_zzrxr__ ( logical * plain_ok );
    int f_zzrytelt__ ( logical * plain_ok );
    int f_zzrytlat__ ( logical * plain_ok );
    int f_zzrytpdt__ ( logical * plain_ok );
    int f_zzrytrec__ ( logical * plain_ok );
    int f_zzsegbox__ ( logical * plain_ok );
    int f_zzsfxcor__ ( logical * plain_ok );
    int f_zzsglatx__ ( logical * plain_ok );
    int f_zzsinutl__ ( logical * plain_ok );
    int f_zzspkfun__ ( logical * plain_ok );
    int f_zzstelab__ ( logical * plain_ok );
    int f_zztanslv__ ( logical * plain_ok );
    int f_zztanutl__ ( logical * plain_ok );
    int f_zztime__ ( logical * plain_ok );
    int f_zzutc__ ( logical * plain_ok );
    int f_zzvalcor__ ( logical * plain_ok );
    int f_zzvrtplt__ ( logical * plain_ok );
    int f_zzwind2d__ ( logical * plain_ok );

    /*
     Prototypes for test families delivered to C.
     */

    void f_a001_c ( SpiceBoolean * spice_ok );
    void f_bod_c ( SpiceBoolean * spice_ok );
    void f_cc01_c ( SpiceBoolean * spice_ok );
    void f_cc02_c ( SpiceBoolean * spice_ok );
    void f_cell_c ( SpiceBoolean * spice_ok );
    void f_ck05_c ( SpiceBoolean * spice_ok );
    void f_ck_c ( SpiceBoolean * spice_ok );
    void f_ckcov_c ( SpiceBoolean * spice_ok );
    void f_cnst_c ( SpiceBoolean * spice_ok );
    void f_daf_c ( SpiceBoolean * spice_ok );
    void f_das01_c ( SpiceBoolean * spice_ok );
    void f_dla_c ( SpiceBoolean * spice_ok );
    void f_dsk02_c ( SpiceBoolean * spice_ok );
    void f_dskg02_c ( SpiceBoolean * spice_ok );
    void f_dskg02b_c ( SpiceBoolean * spice_ok );
    void f_dskkpr_c ( SpiceBoolean * spice_ok );
    void f_dskobj_c ( SpiceBoolean * spice_ok );
    void f_dsktol_c ( SpiceBoolean * spice_ok );
    void f_dskxsi_c ( SpiceBoolean * spice_ok );
    void f_dskxv_c ( SpiceBoolean * spice_ok );
    void f_dvops_c ( SpiceBoolean * spice_ok );
    void f_dvsep_c ( SpiceBoolean * spice_ok );
    void f_ec01_c ( SpiceBoolean * spice_ok );
    void f_edln_c ( SpiceBoolean * spice_ok );
    void f_ek01_c ( SpiceBoolean * spice_ok );
    void f_ek02_c ( SpiceBoolean * spice_ok );
    void f_ek03_c ( SpiceBoolean * spice_ok );
    void f_ell_c ( SpiceBoolean * spice_ok );
    void f_eqncpv_c ( SpiceBoolean * spice_ok );
    void f_errh_c ( SpiceBoolean * spice_ok );
    void f_extr_c ( SpiceBoolean * spice_ok );
    void f_file_c ( SpiceBoolean * spice_ok );
    void f_fovray_c ( SpiceBoolean * spice_ok );
    void f_fovtrg_c ( SpiceBoolean * spice_ok );
    void f_fram_c ( SpiceBoolean * spice_ok );
    void f_frftch_c ( SpiceBoolean * spice_ok );
    void f_ge01_c ( SpiceBoolean * spice_ok );
    void f_ge02_c ( SpiceBoolean * spice_ok );
    void f_getfov_c ( SpiceBoolean * spice_ok );
    void f_gfbail_c ( SpiceBoolean * spice_ok );
    void f_gfdist_c ( SpiceBoolean * spice_ok );
    void f_gfevnt_c ( SpiceBoolean * spice_ok );
    void f_gffove_c ( SpiceBoolean * spice_ok );
    void f_gfilum_c ( SpiceBoolean * spice_ok );
    void f_gfocce_c ( SpiceBoolean * spice_ok );
    void f_gfoclt_c ( SpiceBoolean * spice_ok );
    void f_gfpa_c ( SpiceBoolean * spice_ok );
    void f_gfposc_c ( SpiceBoolean * spice_ok );
    void f_gfrefn_c ( SpiceBoolean * spice_ok );
    void f_gfrfov_c ( SpiceBoolean * spice_ok );
    void f_gfrprt_c ( SpiceBoolean * spice_ok );
    void f_gfrr_c ( SpiceBoolean * spice_ok );
    void f_gfsep_c ( SpiceBoolean * spice_ok );
    void f_gfsntc_c ( SpiceBoolean * spice_ok );
    void f_gfstep_c ( SpiceBoolean * spice_ok );
    void f_gfstol_c ( SpiceBoolean * spice_ok );
    void f_gfsubc_c ( SpiceBoolean * spice_ok );
    void f_gftfov_c ( SpiceBoolean * spice_ok );
    void f_gfudb_c ( SpiceBoolean * spice_ok );
    void f_gfuds_c ( SpiceBoolean * spice_ok );
    void f_illumf_c ( SpiceBoolean * spice_ok );
    void f_illumg_c ( SpiceBoolean * spice_ok );
    void f_inry_c ( SpiceBoolean * spice_ok );
    void f_jac_c ( SpiceBoolean * spice_ok );
    void f_keep_c ( SpiceBoolean * spice_ok );
    void f_latsrf_c ( SpiceBoolean * spice_ok );
    void f_limbpt_c ( SpiceBoolean * spice_ok );
    void f_ls_c ( SpiceBoolean * spice_ok );
    void f_m001_c ( SpiceBoolean * spice_ok );
    void f_m002_c ( SpiceBoolean * spice_ok );
    void f_nnck_c ( SpiceBoolean * spice_ok );
    void f_nndaf_c ( SpiceBoolean * spice_ok );
    void f_nnpool_c ( SpiceBoolean * spice_ok );
    void f_nnsp10_c ( SpiceBoolean * spice_ok );
    void f_nnsp15_c ( SpiceBoolean * spice_ok );
    void f_nnsp17_c ( SpiceBoolean * spice_ok );
    void f_nnspk_c ( SpiceBoolean * spice_ok );
    void f_num_c ( SpiceBoolean * spice_ok );
    void f_occult_c ( SpiceBoolean * spice_ok );
    void f_pck_c ( SpiceBoolean * spice_ok );
    void f_pckcov_c ( SpiceBoolean * spice_ok );
    void f_phaseq_c ( SpiceBoolean * spice_ok );
    void f_pln_c ( SpiceBoolean * spice_ok );
    void f_plt_c ( SpiceBoolean * spice_ok );
    void f_pool_c ( SpiceBoolean * spice_ok );
    void f_prjp_c ( SpiceBoolean * spice_ok );
    void f_pxfrm2_c ( SpiceBoolean * spice_ok );
    void f_quat_c ( SpiceBoolean * spice_ok );
    void f_rot_c ( SpiceBoolean * spice_ok );
    void f_sclk_c ( SpiceBoolean * spice_ok );
    void f_set_c ( SpiceBoolean * spice_ok );
    void f_sort_c ( SpiceBoolean * spice_ok );
    void f_sp10_c ( SpiceBoolean * spice_ok );
    void f_sp15_c ( SpiceBoolean * spice_ok );
    void f_sp17_c ( SpiceBoolean * spice_ok );
    void f_spk18_c ( SpiceBoolean * spice_ok );
    void f_spk_c ( SpiceBoolean * spice_ok );
    void f_spkcov_c ( SpiceBoolean * spice_ok );
    void f_spkcpv_c ( SpiceBoolean * spice_ok );
    void f_spklow_c ( SpiceBoolean * spice_ok );
    void f_srfnrm_c ( SpiceBoolean * spice_ok );
    void f_srftrn_c ( SpiceBoolean * spice_ok );
    void f_st01_c ( SpiceBoolean * spice_ok );
    void f_st02_c ( SpiceBoolean * spice_ok );
    void f_st03_c ( SpiceBoolean * spice_ok );
    void f_st04_c ( SpiceBoolean * spice_ok );
    void f_surfpv_c ( SpiceBoolean * spice_ok );
    void f_term_c ( SpiceBoolean * spice_ok );
    void f_termpt_c ( SpiceBoolean * spice_ok );
    void f_tm01_c ( SpiceBoolean * spice_ok );
    void f_trace_c ( SpiceBoolean * spice_ok );
    void f_util_c ( SpiceBoolean * spice_ok );
    void f_v001_c ( SpiceBoolean * spice_ok );
    void f_v002_c ( SpiceBoolean * spice_ok );
    void f_wind_c ( SpiceBoolean * spice_ok );
    void f_xfmsta_c ( SpiceBoolean * spice_ok );
    void f_zzadapt_c ( SpiceBoolean * spice_ok );

    /*
     Local variables
     */
    SpiceBoolean            spice_ok;
    SpiceChar               cmline[1025];
    SpiceInt                i;
    SpiceInt                len = 0;

    logical                 plain_ok;

    int argc = 0;
    char **argv = nil;

    /*
     Participate in error tracing.
     */
    chkin_c ( "tspice_c" );

    /*
     Store command line for future calls to zzgetcml_c (or anything
     that calls it).
     */
    putcml_c( argc, argv );

    /*
     Clear the string that will hold command line arguments.
     */
    memset( cmline, 0, sizeof(cmline) );

    /*
     Assemble the command line arguments into a space delimited string for
     use as an argument in tsetup_c.
     */
    assert( 2*argc < sizeof(cmline) );

    /*
     Initialize the command line string to a blank. The 'tsetup_c' call
     logic requires a non-null string to pass to the f2c'd code.
     */
    strcat( cmline, " " );
    len++;

    for (i=1; i<argc; i++ )
    {
        len += strlen(argv[i] );

        assert( len < sizeof(cmline) );

        strcat( cmline, argv[i] );

        strcat( cmline, " " );
        len++;
    }

    /*
     Set up for testing.  Establish the name of the log file and
     the version number of the test program.
     */
    tsetup_c ( cmline, "cspice{0-9}{0-9}.log", "1.0.0" );

    /*
     Invoke test families generated from tspice.
     */

    f_aaaaphsh__ ( &plain_ok );
    f_ab__ ( &plain_ok );
    f_asnacsn__ ( &plain_ok );
    f_badkpv__ ( &plain_ok );
    f_bodcod__ ( &plain_ok );
    f_bodvar__ ( &plain_ok );
    f_ccifrm__ ( &plain_ok );
    f_chbigr__ ( &plain_ok );
    f_chgirf__ ( &plain_ok );
    f_chgrfx__ ( &plain_ok );
    f_ck05__ ( &plain_ok );
    f_ck06__ ( &plain_ok );
    f_ckbsr__ ( &plain_ok );
    f_ckcov__ ( &plain_ok );
    f_ckfrot__ ( &plain_ok );
    f_ckfxfm__ ( &plain_ok );
    f_ckgp__ ( &plain_ok );
    f_ckmeta__ ( &plain_ok );
    f_ckw01__ ( &plain_ok );
    f_ckw02__ ( &plain_ok );
    f_ckw03__ ( &plain_ok );
    f_convrt__ ( &plain_ok );
    f_crdcnv__ ( &plain_ok );
    f_cyip__ ( &plain_ok );
    f_dafah__ ( &plain_ok );
    f_dafana__ ( &plain_ok );
    f_dafnn__ ( &plain_ok );
    f_dasa2l__ ( &plain_ok );
    f_dascud__ ( &plain_ok );
    f_dasfr__ ( &plain_ok );
    f_dasmul__ ( &plain_ok );
    f_ddhcls__ ( &plain_ok );
    f_ddhclu__ ( &plain_ok );
//    f_ddhf2h__ ( &plain_ok ); // EXC_BAD_ACCESS
    f_ddhfnh__ ( &plain_ok );
    f_ddhftsize__ ( &plain_ok );
    f_ddhgsd__ ( &plain_ok );
    f_ddhgtu__ ( &plain_ok );
    f_ddhhlu__ ( &plain_ok );
    f_ddhisn__ ( &plain_ok );
    f_ddhivf__ ( &plain_ok );
    f_ddhluh__ ( &plain_ok );
    f_ddhnfo__ ( &plain_ok );
    f_ddhopn__ ( &plain_ok );
    f_ddhppf__ ( &plain_ok );
    f_ddhrcm__ ( &plain_ok );
    f_ddhrmu__ ( &plain_ok );
    f_ddhunl__ ( &plain_ok );
    f_dhfa__ ( &plain_ok );
    f_dla__ ( &plain_ok );
    f_dnearp__ ( &plain_ok );
    f_dpfmt__ ( &plain_ok );
    f_dpstrf__ ( &plain_ok );
    f_dsk02__ ( &plain_ok );
//    f_dskkpr__ ( &plain_ok ); // EXC_BAD_ACCESS
    f_dskmi2__ ( &plain_ok );
    f_dskobj__ ( &plain_ok );
    f_dskrb2__ ( &plain_ok );
    f_dsktol__ ( &plain_ok );
    f_dskw02__ ( &plain_ok );
    f_dskx02__ ( &plain_ok );
    f_dskxsi__ ( &plain_ok );
    f_dskxv__ ( &plain_ok );
    f_dvops__ ( &plain_ok );
    f_dvsep__ ( &plain_ok );
    f_dwpool__ ( &plain_ok );
    f_dyn01__ ( &plain_ok );
    f_dyn02__ ( &plain_ok );
    f_dyn03__ ( &plain_ok );
    f_dyn04__ ( &plain_ok );
    f_dyn05__ ( &plain_ok );
    f_dyn06__ ( &plain_ok );
    f_dyn07__ ( &plain_ok );
    f_dyn08__ ( &plain_ok );
    f_ednmpt__ ( &plain_ok );
    f_edpnt__ ( &plain_ok );
    f_ek01__ ( &plain_ok );
    f_ek02__ ( &plain_ok );
    f_ek03__ ( &plain_ok );
    f_ek04__ ( &plain_ok );
    f_eqncpv__ ( &plain_ok );
    f_et2lst__ ( &plain_ok );
    f_et2utc__ ( &plain_ok );
    f_etcal__ ( &plain_ok );
    f_euler__ ( &plain_ok );
    f_fndcmp__ ( &plain_ok );
    f_fovray__ ( &plain_ok );
    f_fovtrg__ ( &plain_ok );
    f_framex__ ( &plain_ok );
    f_frftch__ ( &plain_ok );
    f_frmchg__ ( &plain_ok );
    f_frmget__ ( &plain_ok );
    f_ge01__ ( &plain_ok );
    f_getfov__ ( &plain_ok );
    f_getfv2__ ( &plain_ok );
    f_gfbail__ ( &plain_ok );
    f_gfbirp__ ( &plain_ok );
    f_gfdirp__ ( &plain_ok );
    f_gfdist__ ( &plain_ok );
    f_gfevnt__ ( &plain_ok );
    f_gffove__ ( &plain_ok );
    f_gfilum__ ( &plain_ok );
    f_gflorp__ ( &plain_ok );
    f_gfocce__ ( &plain_ok );
    f_gfoclt__ ( &plain_ok );
    f_gfpa__ ( &plain_ok );
    f_gfpcrp__ ( &plain_ok );
    f_gfposc__ ( &plain_ok );
    f_gfrefn__ ( &plain_ok );
    f_gfrfov__ ( &plain_ok );
    f_gfrprt__ ( &plain_ok );
    f_gfrr__ ( &plain_ok );
    f_gfscrp__ ( &plain_ok );
    f_gfsep__ ( &plain_ok );
    f_gfsntc__ ( &plain_ok );
    f_gfsprp__ ( &plain_ok );
    f_gfstep__ ( &plain_ok );
    f_gfsubc__ ( &plain_ok );
    f_gftfov__ ( &plain_ok );
    f_gfudb__ ( &plain_ok );
    f_gfuds__ ( &plain_ok );
    f_gfxcrp__ ( &plain_ok );
    f_hrmite__ ( &plain_ok );
    f_illumf__ ( &plain_ok );
    f_illumg__ ( &plain_ok );
    f_ilumin__ ( &plain_ok );
    f_incnsg__ ( &plain_ok );
    f_inelpl__ ( &plain_ok );
    f_inrypl__ ( &plain_ok );
    f_insang__ ( &plain_ok );
    f_insert__ ( &plain_ok );
    f_iovcmp__ ( &plain_ok );
    f_jul2gr__ ( &plain_ok );
    f_keeper__ ( &plain_ok );
    f_kpbug__ ( &plain_ok );
    f_kpsolv__ ( &plain_ok );
    f_lagrng__ ( &plain_ok );
    f_latsrf__ ( &plain_ok );
    f_limbpt__ ( &plain_ok );
    f_locati__ ( &plain_ok );
    f_lparse__ ( &plain_ok );
    f_ls__ ( &plain_ok );
    f_ltime__ ( &plain_ok );
    f_m2q__ ( &plain_ok );
    f_matrix3__ ( &plain_ok );
    f_matrixg__ ( &plain_ok );
    f_moved__ ( &plain_ok );
    f_nearpt__ ( &plain_ok );
    f_nnek01__ ( &plain_ok );
    f_nnek03__ ( &plain_ok );
    f_nnek04__ ( &plain_ok );
    f_npedln__ ( &plain_ok );
    f_npelpt__ ( &plain_ok );
    f_nplnpt__ ( &plain_ok );
    f_npsgpt__ ( &plain_ok );
    f_occult__ ( &plain_ok );
    f_oscelt__ ( &plain_ok );
    f_oscltx__ ( &plain_ok );
    f_pck20__ ( &plain_ok );
    f_pckbsr__ ( &plain_ok );
    f_pckbuf__ ( &plain_ok );
    f_pckcov__ ( &plain_ok );
    f_pgr__ ( &plain_ok );
    f_phaseq__ ( &plain_ok );
    f_pln__ ( &plain_ok );
    f_plt__ ( &plain_ok );
    f_pltnp__ ( &plain_ok );
    f_pool__ ( &plain_ok );
    f_pxform__ ( &plain_ok );
    f_pxfrm2__ ( &plain_ok );
    f_q2m__ ( &plain_ok );
    f_qderiv__ ( &plain_ok );
    f_quat__ ( &plain_ok );
    f_rc2grd__ ( &plain_ok );
    f_rdpck__ ( &plain_ok );
    f_refchg__ ( &plain_ok );
    f_reglon__ ( &plain_ok );
    f_repmx__ ( &plain_ok );
    f_rotget__ ( &plain_ok );
    f_saelgv__ ( &plain_ok );
    f_sbf__ ( &plain_ok );
    f_sclk__ ( &plain_ok );
    f_sctran__ ( &plain_ok );
    f_sepool__ ( &plain_ok );
    f_sgmeta__ ( &plain_ok );
    f_sharpr__ ( &plain_ok );
    f_sincpt__ ( &plain_ok );
    f_slice__ ( &plain_ok );
    f_spk01__ ( &plain_ok );
    f_spk02__ ( &plain_ok );
//    f_spk03__ ( &plain_ok ); //EXC_BAD_ACCESS
    f_spk05__ ( &plain_ok );
    f_spk08__ ( &plain_ok );
    f_spk09__ ( &plain_ok );
    f_spk10__ ( &plain_ok );
    f_spk12__ ( &plain_ok );
    f_spk13__ ( &plain_ok );
    f_spk14__ ( &plain_ok );
    f_spk17__ ( &plain_ok );
    f_spk18__ ( &plain_ok );
    f_spk19__ ( &plain_ok );
    f_spk20__ ( &plain_ok );
    f_spk21__ ( &plain_ok );
    f_spkapo__ ( &plain_ok );
    f_spkapp__ ( &plain_ok );
    f_spkbsr__ ( &plain_ok );
    f_spkcor__ ( &plain_ok );
    f_spkcov__ ( &plain_ok );
    f_spkcpv__ ( &plain_ok );
    f_spke15__ ( &plain_ok );
    f_spkez__ ( &plain_ok );
    f_spkezp__ ( &plain_ok );
    f_spkf15__ ( &plain_ok );
    f_spkgeo__ ( &plain_ok );
    f_spkgps__ ( &plain_ok );
    f_spkgpx__ ( &plain_ok );
    f_spkgxc__ ( &plain_ok );
    f_spkpds__ ( &plain_ok );
    f_spkpvn__ ( &plain_ok );
    f_spks19__ ( &plain_ok );
    f_spkspv__ ( &plain_ok );
    f_spkw01__ ( &plain_ok );
    f_srfnrm__ ( &plain_ok );
    f_srftrn__ ( &plain_ok );
    f_srfxpt__ ( &plain_ok );
    f_stlabx__ ( &plain_ok );
    f_stmp03__ ( &plain_ok );
    f_stpool__ ( &plain_ok );
    f_str2et__ ( &plain_ok );
    f_string__ ( &plain_ok );
    f_subpnt__ ( &plain_ok );
    f_subslr__ ( &plain_ok );
    f_surfnm__ ( &plain_ok );
    f_surfpv__ ( &plain_ok );
    f_swapac__ ( &plain_ok );
    f_swapad__ ( &plain_ok );
    f_swapai__ ( &plain_ok );
    f_sxform__ ( &plain_ok );
    f_symtbc__ ( &plain_ok );
    f_symtbd__ ( &plain_ok );
    f_symtbi__ ( &plain_ok );
    f_t_urand__ ( &plain_ok );
    f_tabtxt__ ( &plain_ok );
    f_tcheck__ ( &plain_ok );
    f_term__ ( &plain_ok );
    f_termpt__ ( &plain_ok );
    f_texpyr__ ( &plain_ok );
    f_timcvr__ ( &plain_ok );
    f_timdef__ ( &plain_ok );
    f_timout__ ( &plain_ok );
    f_tkfram__ ( &plain_ok );
    f_tncnsg__ ( &plain_ok );
    f_tparse__ ( &plain_ok );
    f_tpartv1__ ( &plain_ok );
    f_tpartv2__ ( &plain_ok );
    f_tstck3__ ( &plain_ok );
    f_ttrans__ ( &plain_ok );
    f_twovxf__ ( &plain_ok );
    f_utc2et__ ( &plain_ok );
    f_vector3__ ( &plain_ok );
    f_vectorg__ ( &plain_ok );
    f_voxel__ ( &plain_ok );
    f_vstrng__ ( &plain_ok );
    f_win__ ( &plain_ok );
    f_xdda__ ( &plain_ok );
    f_xfmsta__ ( &plain_ok );
    f_xfneul__ ( &plain_ok );
    f_xfrav__ ( &plain_ok );
    f_xlated__ ( &plain_ok );
    f_xlatei__ ( &plain_ok );
    f_zzasc1__ ( &plain_ok );
    f_zzasc2__ ( &plain_ok );
    f_zzasryel__ ( &plain_ok );
    f_zzbdin__ ( &plain_ok );
    f_zzbdkr__ ( &plain_ok );
    f_zzbdtn__ ( &plain_ok );
    f_zzbdtrn__ ( &plain_ok );
    f_zzbods__ ( &plain_ok );
    f_zzbods2c__ ( &plain_ok );
    f_zzbodvcd__ ( &plain_ok );
    f_zzbquad__ ( &plain_ok );
    f_zzcke06__ ( &plain_ok );
    f_zzcnquad__ ( &plain_ok );
    f_zzcorsxf__ ( &plain_ok );
    f_zzctr__ ( &plain_ok );
    f_zzcxbrut__ ( &plain_ok );
    f_zzdasgrc__ ( &plain_ok );
    f_zzddhnfc__ ( &plain_ok );
    f_zzdgdr__ ( &plain_ok );
    f_zzdgfr__ ( &plain_ok );
    f_zzdgsr__ ( &plain_ok );
    f_zzdm__ ( &plain_ok );
    f_zzdskbsr__ ( &plain_ok );
    f_zzdskbux__ ( &plain_ok );
    f_zzdsksel__ ( &plain_ok );
    f_zzdsksgr__ ( &plain_ok );
    f_zzdsksgx__ ( &plain_ok );
    f_zzdsksph__ ( &plain_ok );
    f_zzedtmpt__ ( &plain_ok );
    f_zzellbds__ ( &plain_ok );
    f_zzelnaxx__ ( &plain_ok );
    f_zzelvupy0__ ( &plain_ok );
    f_zzfdat__ ( &plain_ok );
    f_zzfgeo__ ( &plain_ok );
    f_zzfovaxi__ ( &plain_ok );
    f_zzgetelm__ ( &plain_ok );
    f_zzgfcoq__ ( &plain_ok );
    f_zzgfcost__ ( &plain_ok );
    f_zzgfcou__ ( &plain_ok );
    f_zzgfcprx__ ( &plain_ok );
    f_zzgfcslv__ ( &plain_ok );
    f_zzgfdiq__ ( &plain_ok );
    f_zzgfdiu__ ( &plain_ok );
    f_zzgffvu__ ( &plain_ok );
    f_zzgfilu__ ( &plain_ok );
    f_zzgflng1__ ( &plain_ok );
    f_zzgflng2__ ( &plain_ok );
    f_zzgflng3__ ( &plain_ok );
    f_zzgfocu__ ( &plain_ok );
    f_zzgfpau__ ( &plain_ok );
    f_zzgfrel__ ( &plain_ok );
    f_zzgfrelx__ ( &plain_ok );
    f_zzgfrpwk__ ( &plain_ok );
    f_zzgfrru__ ( &plain_ok );
    f_zzgfsolv__ ( &plain_ok );
    f_zzgfsolvx__ ( &plain_ok );
    f_zzgfspq__ ( &plain_ok );
    f_zzgfspu__ ( &plain_ok );
    f_zzgfssin__ ( &plain_ok );
    f_zzgfssob__ ( &plain_ok );
    f_zzgfwsts__ ( &plain_ok );
    f_zzhsc__ ( &plain_ok );
    f_zzhsi__ ( &plain_ok );
    f_zzhullax__ ( &plain_ok );
    f_zzilusta__ ( &plain_ok );
    f_zzinlat__ ( &plain_ok );
    f_zzinlat0__ ( &plain_ok );
    f_zzinpdt__ ( &plain_ok );
    f_zzinpdt0__ ( &plain_ok );
    f_zzinrec__ ( &plain_ok );
    f_zzinvelt__ ( &plain_ok );
    f_zzlatbox__ ( &plain_ok );
    f_zzmkspin__ ( &plain_ok );
    f_zzmsxf__ ( &plain_ok );
    f_zznamfrm__ ( &plain_ok );
    f_zznofcon__ ( &plain_ok );
    f_zznrmlon__ ( &plain_ok );
    f_zzocced__ ( &plain_ok );
    f_zzocced2__ ( &plain_ok );
    f_zzocced3__ ( &plain_ok );
    f_zzpdcmpl__ ( &plain_ok );
    f_zzpdpltc__ ( &plain_ok );
    f_zzpdtbox__ ( &plain_ok );
    f_zzplat__ ( &plain_ok );
    f_zzptpl02__ ( &plain_ok );
    f_zzraybox__ ( &plain_ok );
    f_zzrecbox__ ( &plain_ok );
    f_zzrtnmat__ ( &plain_ok );
    f_zzrxr__ ( &plain_ok );
    f_zzrytelt__ ( &plain_ok );
    f_zzrytlat__ ( &plain_ok );
    f_zzrytpdt__ ( &plain_ok );
    f_zzrytrec__ ( &plain_ok );
    f_zzsegbox__ ( &plain_ok );
    f_zzsfxcor__ ( &plain_ok );
    f_zzsglatx__ ( &plain_ok );
    f_zzsinutl__ ( &plain_ok );
    f_zzspkfun__ ( &plain_ok );
    f_zzstelab__ ( &plain_ok );
    f_zztanslv__ ( &plain_ok );
    f_zztanutl__ ( &plain_ok );
    f_zztime__ ( &plain_ok );
    f_zzutc__ ( &plain_ok );
    f_zzvalcor__ ( &plain_ok );
    f_zzvrtplt__ ( &plain_ok );
    f_zzwind2d__ ( &plain_ok );

    /*
     Invoke test families delivered to tspice_c.
     */

    f_a001_c ( &spice_ok );
    f_bod_c ( &spice_ok );
    f_cc01_c ( &spice_ok );
    f_cc02_c ( &spice_ok );
    f_cell_c ( &spice_ok );
    f_ck05_c ( &spice_ok );
    f_ck_c ( &spice_ok );
    f_ckcov_c ( &spice_ok );
    f_cnst_c ( &spice_ok );
    f_daf_c ( &spice_ok );
    f_das01_c ( &spice_ok );
    f_dla_c ( &spice_ok );
    f_dsk02_c ( &spice_ok );
    f_dskg02_c ( &spice_ok );
//    f_dskg02b_c ( &spice_ok ); // EXC_BAD_ACCESS
//    f_dskkpr_c ( &spice_ok ); //EXC_BAD_ACCESS
    f_dskobj_c ( &spice_ok );
    f_dsktol_c ( &spice_ok );
    f_dskxsi_c ( &spice_ok );
    f_dskxv_c ( &spice_ok );
    f_dvops_c ( &spice_ok );
    f_dvsep_c ( &spice_ok );
    f_ec01_c ( &spice_ok );
    f_edln_c ( &spice_ok );
    f_ek01_c ( &spice_ok );
    f_ek02_c ( &spice_ok );
//    f_ek03_c ( &spice_ok ); // EXC_BAD_ACCESS
    f_ell_c ( &spice_ok );
    f_eqncpv_c ( &spice_ok );
    f_errh_c ( &spice_ok );
    f_extr_c ( &spice_ok );
    f_file_c ( &spice_ok );
    f_fovray_c ( &spice_ok );
    f_fovtrg_c ( &spice_ok );
    f_fram_c ( &spice_ok );
    f_frftch_c ( &spice_ok );
    f_ge01_c ( &spice_ok );
    f_ge02_c ( &spice_ok );
    f_getfov_c ( &spice_ok );
//    f_gfbail_c ( &spice_ok ); //SIGINT
    f_gfdist_c ( &spice_ok );
    f_gfevnt_c ( &spice_ok );
    f_gffove_c ( &spice_ok );
    f_gfilum_c ( &spice_ok );
    f_gfocce_c ( &spice_ok );
    f_gfoclt_c ( &spice_ok );
    f_gfpa_c ( &spice_ok );
    f_gfposc_c ( &spice_ok );
    f_gfrefn_c ( &spice_ok );
    f_gfrfov_c ( &spice_ok );
    f_gfrprt_c ( &spice_ok );
    f_gfrr_c ( &spice_ok );
    f_gfsep_c ( &spice_ok );
    f_gfsntc_c ( &spice_ok );
    f_gfstep_c ( &spice_ok );
    f_gfstol_c ( &spice_ok );
    f_gfsubc_c ( &spice_ok );
    f_gftfov_c ( &spice_ok );
    f_gfudb_c ( &spice_ok );
    f_gfuds_c ( &spice_ok );
    f_illumf_c ( &spice_ok );
    f_illumg_c ( &spice_ok );
    f_inry_c ( &spice_ok );
    f_jac_c ( &spice_ok );
    f_keep_c ( &spice_ok );
    f_latsrf_c ( &spice_ok );
//    f_limbpt_c ( &spice_ok );// EXC_BAD_ACCESS
    f_ls_c ( &spice_ok );
    f_m001_c ( &spice_ok );
    f_m002_c ( &spice_ok );
    f_nnck_c ( &spice_ok );
    f_nndaf_c ( &spice_ok );
//    f_nnpool_c ( &spice_ok ); // EXC_BAD_ACCESS
    f_nnsp10_c ( &spice_ok );
    f_nnsp15_c ( &spice_ok );
    f_nnsp17_c ( &spice_ok );
    f_nnspk_c ( &spice_ok );
    f_num_c ( &spice_ok );
    f_occult_c ( &spice_ok );
    f_pck_c ( &spice_ok );
    f_pckcov_c ( &spice_ok );
    f_phaseq_c ( &spice_ok );
    f_pln_c ( &spice_ok );
    f_plt_c ( &spice_ok );
    f_pool_c ( &spice_ok );
    f_prjp_c ( &spice_ok );
    f_pxfrm2_c ( &spice_ok );
    f_quat_c ( &spice_ok );
    f_rot_c ( &spice_ok );
    f_sclk_c ( &spice_ok );
    f_set_c ( &spice_ok );
    f_sort_c ( &spice_ok );
    f_sp10_c ( &spice_ok );
    f_sp15_c ( &spice_ok );
    f_sp17_c ( &spice_ok );
    f_spk18_c ( &spice_ok );
    f_spk_c ( &spice_ok );
    f_spkcov_c ( &spice_ok );
    f_spkcpv_c ( &spice_ok );
    f_spklow_c ( &spice_ok );
    f_srfnrm_c ( &spice_ok );
    f_srftrn_c ( &spice_ok );
    f_st01_c ( &spice_ok );
    f_st02_c ( &spice_ok );
    f_st03_c ( &spice_ok );
    f_st04_c ( &spice_ok );
    f_surfpv_c ( &spice_ok );
    f_term_c ( &spice_ok );
//    f_termpt_c ( &spice_ok ); // EXC_BAD_ACCESS
    f_tm01_c ( &spice_ok );
    f_trace_c ( &spice_ok );
    f_util_c ( &spice_ok );
    f_v001_c ( &spice_ok );
    f_v002_c ( &spice_ok );
    f_wind_c ( &spice_ok );
    f_xfmsta_c ( &spice_ok );
    f_zzadapt_c ( &spice_ok );

    /*
     Close up the testing.
     */
    tclose_c();

    chkout_c ( "tspice_c" );

//    if ( spice_ok && plain_ok )
//    {
//        return ( 0 );
//    }
//    else
//    {
//        return ( 1 );
//    }

}
@end
