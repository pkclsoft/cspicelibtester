//
//  main.m
//  CSPICELibTester
//
//  Created by Peter Easdown on 24/8/18.
//  Copyright © 2018 PKCLsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
